<?php
require_once 'inc_header.php';
if(checkIntRef($_REQUEST['i'])) {
	$call_id = $_REQUEST['i'];
	$call = mysql_fetch_all1("H","SELECT * FROM help_client_call WHERE call_id = ".$call_id." AND call_active = true","call_id");
	if(count($call)>0) {
		$call = $call[$call_id];
	} else {
		$result = array("error","It appears that your help request has been cancelled.  Please contact your reseller for further assistance.");
	}
} else {
	$result = array("error","An error occurred while trying to retrieve your help request.  Please go back and try again.");
}
if(isset($result)) { displayResult($result); }
?>
<table id=call>
<?php
foreach($headings['C_CALL'] as $fld => $h) {
	echo	"<tr>
				<th width=120>".$h['h_text'].":</th>
				<td width=400>";
	switch($h['h_type']) {
		case "DATE":
			echo date("d M Y H:i:s",strtotime($call[$fld]));
			break;
		case "CMP":
			echo $cmpname;
			break;
		case "MOD":
			$m = explode("_",$call[$fld]);
			echo $m[1];
			break;
		case "RSL":
			echo $bpa_details['cmpname'];
			break;
		case "TK":
			if(strlen($call[$fld])>0) {
				$admin_users = mysql_fetch_all1($bpa,"SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as tkn FROM assist_".strtolower($bpa)."_timekeep WHERE tkid = '".$call[$fld]."' AND tkstatus = 1","tkid");
				if(count($admin_users)>0) {
					echo $admin_users[$call[$fld]]['tkn'];
				} else {
					echo "Invalid user";
				}
			} else {
				echo "Unassigned";
			}
			break;
		case "LIST":
			$l = $lists[$h['h_table']]['data'][$call[$fld]];
			if(isset($l['color']) && strlen($l['color'])>0) { echo "<span style=\"background-color: #".$l['color']."; color: #FFFFFF;\">&nbsp;"; }
			echo $l['value']."&nbsp;</span>";
			break;
		default:
			echo str_replace(chr(10),"<br />",$call[$fld]);
			break;
	}		
	echo		"</td>
			</tr>";
}
?>
	<tr>
		<th>&nbsp;</th>
		<td class=right><input type=button value=Cancel class=idelete id=<?php echo $call_id; ?> /></td>
	</tr>
</table>
<?php displayGoBack("",""); ?>
<script type=text/javascript>
	$(function() {
		$("#call th").addClass("left top");
		$("#call .idelete").click(function() {
			i = $(this).attr("id");
			if(confirm("Are you sure you wish to cancel this help request?")) {
				document.location.href = 'manage.php?act=CANCEL&i='+i;
			}
		});
	});
</script>
</body>
</html>