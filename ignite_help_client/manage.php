<?php
require_once 'inc_header.php';

if(isset($_REQUEST['act']) && strlen($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
	case "CANCEL":
		if(isset($_REQUEST['i']) && checkIntRef($_REQUEST['i'])) {
			$mar = db_query("H","UPDATE help_client_call SET call_active = false WHERE call_id = ".$_REQUEST['i']);
			if($mar>0) {	
				$result = array("ok","Help request successfully cancelled.");
			} else {
				$result = array("error","An error occurred trying to cancel your help request.  Please try again.");
			}
		} else {
			$result = array("error","An error occurred trying to cancel your help request.  Please try again.");
		}
		break;
	}
}
if(isset($result)) { displayResult($result); }
?>
<table id=tbl>
	<tr>
		<th rowspan=2>Reference</th>
		<th rowspan=2>Date Logged</th>
		<th rowspan=2>Module</th>
		<th rowspan=2>Message</th>
		<th colspan=2>Reseller</th>
		<th rowspan=2>Status</th>
		<th rowspan=2></th>
	</tr>
	<tr>
		<th>Company</th>
		<th>Respondant</th>
	</tr>
<?php 
$sql = "SELECT c.*, s.* FROM help_client_call c
			INNER JOIN help_list_status s ON s.id = c.call_statusid
		WHERE c.call_cmpcode = '".strtoupper($cmpcode)."'
			AND c.call_tkid = '$tkid' 
			AND c.call_active = true
		ORDER BY call_date DESC";
$rs = db_query("H",$sql);
if(mysql_num_rows($rs)>0) {
	$admin_users = mysql_fetch_all1($bpa,"SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as tkn FROM assist_".strtolower($bpa)."_timekeep WHERE tkstatus = 1","tkid");
}
while($obj = mysql_fetch_assoc($rs)) {
	/* Module */
	/*if($obj['call_modref']=="GEN") {
		$mod = "General";
	} elseif(strlen($obj['call_modref'])==0 || !isset($modules[$obj['call_modref']])) {
		$mod = "N/A";
	} else {
		$mod = $modules[$obj['call_modref']]['modtext'];
	}*/
	$m = explode("_",$obj['call_modref']);
	$mod = $m[1];
	/* Reseller Company */
	if($obj['call_admin']==$bpa) {
		$rsl_cmp = $bpa_details['cmpname'];
	} else {
		$rsl_cmp = "Unknown [".$obj['call_admin']."]";
	}
	if(strlen($obj['call_admin_tkid'])>0) {
		if(isset($admin_users[$obj['call_admin_tkid']])) {
			$rsl_admin = $admin_users[$obj['call_admin_tkid']]['tkn'];
		} else {
			$rsl_admin = "Invalid user";
		}
	} else {
		$rsl_admin = "<span style=\"color: #".$obj['color']."\">Unassigned</span>";
	}
	echo "<tr>";
		echo "<td class=center style=\"font-weight: bold;\">".str_replace("/","/<br />",$obj['call_ref'])."</td>";
		echo "<td class=center>".date("d M Y",strtotime($obj['call_date']))."<br />".date("H:i",strtotime($obj['call_date']))."</td>";
		echo "<td>".$mod."</td>";
		echo "<td>".$obj['call_message']."</td>";
		echo "<td>".$rsl_cmp."</td>";
		echo "<td>".$rsl_admin."</td>";
		echo "<td class=center><div style=\"background-color: #".$obj['color']."; color: #FFFFFF;\">".$obj['value']."</div></td>";
		echo "<td><input type=button value=View id=".$obj['call_id']." /></td>";
	echo "</tr>";
}
?>
</table>
<script type=text/javascript>
	$(function() {
		$("#tbl input:button").click(function() {
			var i = $(this).attr("id");
			document.location.href = 'manage_view.php?i='+i;
		});
	});
</script>
</body>
</html>