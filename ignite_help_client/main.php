<?php
	require_once 'inc_header.php';
?>
<script type=text/javascript>
$(function() {
	$(".isubmit").click(function() {
		var txt = $("#msg").attr("value");
		if(txt.length==0) {
			alert("Please enter a message.");
		} else {
			$("#help").submit();
		}
	});
});
</script>
<?php
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	$sql = "SHOW TABLE STATUS LIKE 'help_client_call'";
	$rs = db_query("H",$sql);
	$status = mysql_fetch_assoc($rs);
	$call_id = $status['Auto_increment'];
	
	$call_ref = strtoupper($cmpcode)."/C".$call_id;
	/*if(!isset($_REQUEST['call_modref']) || $_REQUEST['call_modref']=="GEN") {
		$mod = "GEN";
	} else {
		$m = explode("_",$_REQUEST['call_modref']);
		$mod = $m[0];
	}*/
	$mod = $_REQUEST['call_modref'];
	$sql = "INSERT INTO help_client_call (call_id, call_ref, call_cmpcode, call_tkid, call_tkname, call_modref, call_message, call_date, call_statusid, call_active, call_admin, call_admin_tkid, call_email) VALUES
			(".$call_id.", '".$call_ref."', '".strtoupper($cmpcode)."', '".$tkid."', '".$tkname."', '$mod', '".code($_REQUEST['msg'])."', now(), 1, true, '".$bpa."', '',";
	
	$msg = "Ref:".$call_ref."\nUser Name: ".$tkname."\nCompany: ".$cmpname." [".$cmpcode."]\nModule: ".$m[1]."\n\nMessage:\n".$_REQUEST['msg'];
	$subject = "New Ignite Assist Support Call [Ref: ".$call_ref."]";
	$to = $bpa_details['help_email'];
	$from = "no-reply@ignite4u.co.za";
	$header = "From:".$from;
	$res = mail($to,$subject,$msg,$header);
	
	$sql.=$res.")";
	db_sql("H",$sql);
	
	if($res) {
		$result_ok = array("ok","Your help request has been sent to ".$bpa_details['help_email'].".  Someone at ".$bpa_details['cmpname']." will contact you shortly.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your reference number is '<b>".$call_ref."</b>'.  Please quote this number in any future communication.");
		displayResult($result_ok);
	} else {
		$result_error = array("error","Your help request has not been submitted.  Please try again.");
		displayResult($result_error);
	}
}
?>
<form id=help action=main.php method=post>
<input type=hidden name=act value=SAVE />
<table>
	<tr>
		<th class=left>Your Name:</th>
		<td><?php echo $tkname; ?></td>
	</tr>
	<tr>
		<th class=left>Your Company:</th>
		<td><?php echo $cmpname." [".strtoupper($cmpcode)."]"; ?></td>
	</tr>
	<tr>
		<th class=left>Business Partner:</th>
		<td><?php echo $bpa_details['cmpname']; ?></td>
	</tr>
	<tr>
		<th class=left>Contact Details:</th>
		<td><?php 
			$contact = explode("|",$bpa_details['help_contact']);
			foreach($contact as $c) {
				$d = explode("_",$c);
				echo "<b>".$d[0].":</b>&nbsp;".$d[1]."<br />";
			}
		?></td>
	</tr>
	<tr>
		<th class=left>Module:</th>
		<td>
			<?php
				$sql = "SELECT m.* FROM assist_menu_modules m 
						INNER JOIN assist_".$cmpcode."_menu_modules_users mu
						ON m.modref = mu.usrmodref AND mu.usrtkid = '$tkid'
						WHERE m.modyn = 'Y'";
			?>
			<select name=call_modref>
				<option selected value=GEN_General>General</option>
				<?php
				$rs = db_query("C",$sql);
				while($row = mysql_fetch_assoc($rs)) {
					echo "<option value=\"".$row['modref']."_".$row['modtext']."\">".$row['modtext']."</option>";
				} 
				?>
			</select>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>Please provide a brief description of your problem:
		<p><textarea rows=10 cols=50 id=msg name=msg ></textarea></p>
	</tr>
	<tr>
		<th></th>
		<td><input type=button value="Submit Help Request" class=isubmit /></td>
	</tr>
</table>
</form>
</body>
</html>