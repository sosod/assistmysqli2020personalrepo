<?php
    require 'inc_head.php';
?>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> <a href=admin_maintain.php class=breadcrumb>Maintain</a> >> Edit Category</b></h1>
<?php
$variables = $_REQUEST;
$cateid = $variables['cateid'];


if($variables['act']=="DELETE") {

if($doc0->checkIntRef($cateid)) {
	$sql = "UPDATE ".$dbref."_categories SET cateyn = 'N' WHERE cateid = ".$cateid;
	$mar = $doc0->db_update($sql);
	if($mar>0) {
		logAct("Deleted category $cateid",$sql,"CATE",$cateid);
		$result = array("ok","Deleted category ".$cateid);
	} else {
		$result = array("info","Category ".$cateid." has already been deleted.");
	}
} else {
	$result = array("error","Ignite Assist could not identify the category you are trying to delete.  Please try again.");
}

} else {

$catetitle = ASSIST_HELPER::code($variables['catetitle']);
$cateyn = "Y";
$catesubarray = $variables['catesubid'];
$cateowner = $variables['cateowner'];
//$view = $variables['cateview'];
//$up = $variables['cateupload'];
$parent = $catesubarray[0];
foreach($catesubarray as $c) {
	if($c!="X") {
		$catesubid = $c;
	} else {
		break;
	}
}
if($catesubid==0 || !$doc0->checkIntRef($catesubid)) {
	$view = $variables['view'];
} else {
	$sql = "SELECT cutkid FROM ".$dbref."_categories_users WHERE cucateid = $parent AND cuyn = 'Y' AND cutype = 'VIEW'";
	$view = $doc0->mysql_fetch_fld_one($sql,"cutkid");
}
$up = $variables['upload'];
$maint = $variables['maintain'];
if(!in_array($cateowner,$view)) { $view[] = $cateowner; }
if(!in_array($cateowner,$up)) { $up[] = $cateowner; }
if(!in_array($cateowner,$maint)) { $maint[] = $cateowner; }


if(strlen($catetitle)>0 && strlen($cateowner)>0 && $cateowner!="X" && is_numeric($catesubid)) {
    $sql = "UPDATE ".$dbref."_categories SET catetitle = '$catetitle', catesubid = $catesubid, cateowner = '$cateowner' WHERE cateid = $cateid ";
    $mar = $doc0->db_update($sql);
	if($mar>0) {
		
		$l = array(
			'section'=>"CATE",
			'ref'=>$cateid,
			'cateid'=>$cateid,
			'action'=>"E",
			'field'=>serialize(array("catetitle","catesubid","cateowner")),
			'transaction'=>"Edited category $cateid as follows:",
			'old'=>"",
			'new'=>serialize(array("catetitle"=>$catetitle,"catesubid"=>$catesubid,"cateowner"=>$cateowner)),
		);
		logTransaction('ACTIVITY',$l,$sql);
	}
	//remove existing user access
	$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'N' WHERE cucateid = $cateid";
	$mar+=$doc0->db_update($sql);
	//restore existing users
		//view
	$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'Y' WHERE cucateid = $cateid AND cutkid IN ('".implode("','",$view)."') AND cutype = 'VIEW'";
	$mar+=$doc0->db_update($sql);
		//maintain
	$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'Y' WHERE cucateid = $cateid AND cutkid IN ('".implode("','",$maint)."') AND cutype = 'MAIN'";
	$mar+=$doc0->db_update($sql);
		//upload
	$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'Y' WHERE cucateid = $cateid AND cutkid IN ('".implode("','",$up)."') AND cutype = 'UP'";
	$mar+=$doc0->db_update($sql);
	//check for new users
	$old_view = array();
	$old_main = array();
	$old_up = array();
	$sql = "SELECT cutype, cutkid FROM ".$dbref."_categories_users WHERE cucateid = $cateid AND cuyn = 'Y'";
	$rs = $doc0->mysql_fetch_all($sql);
	foreach($rs as $row) {
		switch($row['cutype']) {
		case "VIEW":	$old_view[] = $row['cutkid'];	break;
		case "MAIN":	$old_main[] = $row['cutkid'];	break;
		case "UP":		$old_up[] = $row['cutkid'];		break;
		}
	}
	//mysql_close();
	$new_view = array_diff($view,$old_view);
	$new_main = array_diff($maint,$old_main);
	$new_up = array_diff($up,$old_up);
	if( (count($new_view) + count($new_main) + count($new_up)) > 0) {
		$sql2 = array();
		if(count($new_view)>0) {
			$sql2[] = "(null,$cateid,'".implode("','VIEW','Y'),(null,$cateid,'",$new_view)."','VIEW','Y')";
		}
		if(count($new_main)>0) {
			$sql2[] = "(null,$cateid,'".implode("','MAIN','Y'),(null,$cateid,'",$new_main)."','MAIN','Y')";
		}
		if(count($new_up)>0) {
			$sql2[] = "(null,$cateid,'".implode("','UP','Y'),(null,$cateid,'",$new_up)."','UP','Y')";
		}
		$sql = "INSERT INTO ".$dbref."_categories_users VALUES ".implode(",",$sql2);
		$mar+=$doc0->db_insert($sql);
	}
	
    if($mar>0)
    {
        logAct("Edited category $cateid",$sql,"CATE",$cateid);
		

		
		
		$result = array("ok","Category $cateid has been successfully updated.");
    } else {
        $result = array("info","No change was made.");
    }
} else {
	$result = array("error","Please complete all the required fields.");
    //die("<p>Please complete all the required fields.</p>");
}
}

echo "<script type=text/javascript>
		document.location.href = 'admin_maintain.php?r[]=".$result[0]."&r[]=".$result[1]."';
	</script>";
?>
</body>
</html>
