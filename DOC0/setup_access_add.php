<?php
include("inc_head.php");
//include("inc_admin.php");
?>
<h1><b><?php echo($moduletitle); ?>: Setup - User Access</b></h1>
<p>Processing...</p>
<?php
//GET VARIABLES PASSED BY FORM
$act = $_POST['act'];
$start = $_POST['s'];
if(!is_numeric($start) || strlen($start)==0) { $start = 0; }
$tid = $_POST['tid'];
$a = $_POST['a'];
$c = $_POST['c'];
$d = $_POST['d'];
$v = $_POST['v'];
if($act=="single")
{
                $tk = $tid;
                $uv = $v;
                $ur = $r;
                $ua = $a;
                $ud = $d;
                $uc = $c;
                if($ua == "Y") { $ud = "A"; $uc = "Y"; $uv = "Y"; }
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, docadmin, addcate, delcate, view) VALUES ";
                $sql.= "('$tk','Y','$ua','$uc','$ud','$uv')";
                //include("inc_db_con.php")
				$doc0->db_query($sql);
                    $tsql = $sql;
                    $trans = "Added user access for user ".$tk;
                    //include("inc_transaction_log.php");
	$v_log = array(
		'sec' => $section,
		'ref' => $id,
		'action' => "D",
		'fld' => "diryn",
		'trans' => addslashes("Deactivated ".$head_dir." '".$id."'."),
		'old' => "Y",
		'new' => "N",
		'sql' => addslashes($sql),
	);
	$me->createLog($v_log);
					logAct("Set user access for user ".$tk,$sql,"S_USER",$tk);
                $res = "User added to ".$modtitle;
}
else
{
    if($act == "all")
    {
        $tc = count($tid);
        $vc = count($v);
        $ac = count($a);
        $dc = count($d);
        $cc = count($c);
        $c2 = $tc+$dc+$vc+$ac+$cc;
        $c2 = $c2/5;
        if($c2!=$tc || $c2 != $dc || $c2 != $vc || $c2 != $ac || $c2 != $cc || $c2 != round($c2))
        {
            $res = "ERROR - Incomplete data";
        }
        else
        {
            $steps = count($tid);
            for($i=0;$i<$steps;$i++)
            {
                $tk = $tid[$i];
                $uv = $v[$i];
                $ua = $a[$i];
                $ud = $d[$i];
                $uc = $c[$i];
                if($ua == "Y") { $ud = "A"; $uc = "Y"; $uv = "Y"; }
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, docadmin, addcate, delcate, view) VALUES ";
                $sql.= "('$tk','Y','$ua','$uc','$ud','$uv')";
                            //include("inc_db_con.php");
				$doc0->db_query($sql);
                                $tsql = $sql;
                                $trans = "Added user access for user ".$tk;
                                //include("inc_transaction_log.php");
					logAct("Set user access for user ".$tk,$sql,"S_USER",$tk);
            }
            $res = "Users added to ".$moduletitle;
        }
    }
    else
    {
        $res = "ERROR - Unknown error.";
    }
}

?>
<script type=text/javascript>
	$("input:button").button().css("font-size","75%");
	$("input:submit").button().css("font-size","75%");
var res = "<?php echo($res); ?>";
res = escape(res);
document.location.href = "setup_access.php?mr=<?php echo $tref; ?>&a=access&s=<?php echo($start); ?>&r="+res;
</script>
</body></html>
