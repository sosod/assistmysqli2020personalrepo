<?php
    require 'inc_head.php';
?>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> Upload a document</h1>
<?php
set_time_limit(1800);
$step = 1;
echo("<P>$step. Getting document details...");  $step++;
$doc_file_error = isset($_FILES["docfile"]["error"])?$_FILES["docfile"]["error"]: 0;
if($doc_file_error > 0) //IF ERROR WITH UPLOAD FILE
{
    if($_FILES["docfile"]["error"]==2)
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["docfile"]["error"]." - File size exceeds max file size limit.</p>");
    }
    else
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["docfile"]["error"]."</p>");
    }
}
else    //IF ERROR WITH UPLOAD FILE
{
    $variables = $_REQUEST;
    $doctitle = isset($variables['doctitle'])?ASSIST_HELPER::code($variables['doctitle']): "";
    $doccontents = isset($variables['doccontents'])?ASSIST_HELPER::code($variables['doccontents']): "";
    $docdate = isset($variables['docdate'])?$variables['docdate']: "";
    $docyn = "T";
    $doccateid = isset($variables['doccateid'])?$variables['doccateid']: "";
	$doclistid = isset($variables['doclistid'])?$variables['doclistid']: "";
    $doctypeid = isset($variables['doctypeid'])?$variables['doctypeid']: "";
    $docadddate = $today;
    $docadduser = $tkid;
	$doc_file_name = isset($_FILES["docfile"]["name"])?$_FILES["docfile"]["name"]: "";
    $docfile = str_replace("'","",$doc_file_name);

    //PHP Validation check
    if(strlen($doctitle)>0 && $doc0->checkIntRef($doccateid) && $doc0->checkIntRef($doctypeid) && strlen($docdate)>0 && strlen($docfile) > 0) {
echo("Done.</p><p>$step. Creating temporary document..."); $step++;
        //format docdate
        //$docdt1 = strFn("explode",$docdate,"/","");
        //$docdt = mktime(12,0,0,$docdt1[1],$docdt1[2],$docdt1[0]);
		$docdt = strtotime($docdate." 12:00:00");
        //first sql load
        $sql = "INSERT INTO ".$dbref."_content (doctitle, doccontent, doccateid, docdate, doclocation, docfilename, doctypeid, docyn, docadduser, docadddate, docmoduser, docmoddate, doclistid) VALUES ";
        $sql.= "('$doctitle','$doccontents',$doccateid,$docdt,'','$docfile',$doctypeid,'$docyn','$docadduser',$docadddate,'$docadduser',$docadddate, $doclistid)";
        //include("inc_db_con.php");
		$docid = $doc0->db_insert($sql);
        $lsql = $sql;
        //get docid
        //$docid = mysql_insert_id($con);
        //validate docid
        if(!$doc0->checkIntRef($docid)) { die("<p>An error has occurred.  Please go back and try again.</p>"); }
        //format filelocation /files/$cmpcode/$moduledb/docdate(yyyy)/docdate(mm)/docid_docdate(yyyymmdd)_today(yyyymmddhhiiss).ext
        $doc = strFn("explode",$docfile,".","");
        $docc = count($doc)-1;
        $docext = $doc[$docc];
        $doclocation = "/files/".$cmpcode."/".$moduledb."/".date("Y",$docdt)."/".date("m",$docdt)."/".$docid."_".date("Ymd",$docdt)."_".date("YmdHis",$docadddate).".".$docext;
echo("Done.</p> <p>$step. Uploading document..."); $step++;
        //upload document
        $chkloc = "../files/".$cmpcode;
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb;
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb."/".date("Y",$docdt);
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb."/".date("Y",$docdt)."/".date("m",$docdt);
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        copy($_FILES["docfile"]["tmp_name"], "..".$doclocation);
echo("Done.</p> <p>$step. Updating document..."); $step++;
        //update sql - docfilename + docyn = Y
        $sql = "UPDATE ".$dbref."_content SET doclocation = '$doclocation', docyn = 'Y' WHERE docid = $docid";
        //include("inc_db_con.php");
		$doc0->db_update($sql);
        $lsql.=chr(10).$sql;
echo("Done.</p>");
        logAct("Uploaded document $docid",$lsql,"DOC",$docid);

        //display result
        $result = array("ok","Success!  Your document has been successfully uploaded.");
		ASSIST_HELPER::displayResult($result);
    } else {
        die("<h2>Error</h2><p>An error has occurred - some form data appears to be missing.  Please go back and try again.</p>");
    }
}

$ucate = getCategory($doccateid);
if(strlen($ucate['catetitle'])==0) { $ucate['catetitle'] = "Category"; }
?>
<table class=noborder cellpadding=5><tr><td class=noborder><img src="/pics/tri_left.gif"></td><td class=noborder><a href="admin.php">Back to Admin</a>
<?php
if($doc0->checkIntRef($doccateid))
{ 
	echo("</td><td class=noborder><a href=admin_upload.php?c=".$doccateid.">Upload another document</a>"); 
	if(strlen($ucate['catetitle'])>0)
	{
		echo("</td><td class=noborder><a href=admin_maintain_doc.php?c=".$doccateid.">Go to ".$ucate['catetitle']."</a>"); 
	}
}
?>
</td><td class=noborder><img src="/pics/tri_right.gif"></td></tr></table>
</body>
</html>
