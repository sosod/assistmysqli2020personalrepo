<?php
    require 'inc_head.php';

$catid = (isset($_REQUEST['c']) && $doc0->checkIntRef($_REQUEST['c'])) ? $_REQUEST['c'] : "X";

function subFunction($sub,$level,$title) {
	global $categories;
	global $access;
	global $my_access;
	global $tkid;
	global $catid;
	global $catedisplay;
	$echo = "";
	$doc0 = new doc0();
	if (!isset($eliminated)){$eliminated = 0;}
	if (!isset($displayed)){$displayed = "";}

	foreach($sub as $c) {
		if($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']])) {
				$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
				$echo.= "<option ".($c['cateid']==$catid ? "selected" : "")." value=".$c['cateid'].">".$text."</option>"; //."</span></p>";
		}
		$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
		if(count($sub)>0) {
			$cut = $doc0->checkIntRef($catedisplay) ? $catedisplay : 20;
			$sub_title = $title.(strlen($title)>0 ? " >> " : "").substr(ASSIST_HELPER::decode($c['catetitle']),0,$cut).(strlen(ASSIST_HELPER::decode($c['catetitle']))>$cut ? "..." : "");
			//$sub_title = $title.(strlen($title)>0 ? " >> " : "").$c['catetitle'];
			$echo.=subFunction($sub,$level+1,$sub_title,$eliminated,$displayed);
		}
	}
	return $echo;
}

/** GET CATEGORIES **/
$categories = array();
$cate_count = 0;
$cate_owner = array();

$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
$rs = $doc0->mysql_fetch_all($sql);
foreach($rs as $row) {
	$categories[$row['catesubid']][] = $row;
	$cate_count++;
	if($row['cateowner']==$tkid) {
		$cate_owner[$row['cateid']] = "Y";
	}
}
//mysql_close();

/** USER ACCESS **/
$my_access = array();
if($access['docadmin']!="Y") {
	$sql = "SELECT * FROM ".$dbref."_categories_users 
			INNER JOIN ".$dbref."_categories
			  ON cucateid = cateid AND cateyn = 'Y'
			WHERE cuyn = 'Y' AND cutkid = '$tkid' AND (cutype = 'UP' OR cutype = 'MAIN')";
	$rs = $doc0->mysql_fetch_all($sql);
	foreach($rs as $row) {
		$my_access[$row['cucateid']] = "Y";
		if(isset($cate_owner[$row['cucateid']])) { unset($cate_owner[$row['cucateid']]); }
	}
	//mysql_close();
	if(count($cate_owner)>0) {
		foreach($cate_owner as $i => $y) {
			$my_access[$i] = $y;
		}
	}
	$total_cate = count($my_access);
} else {
	$total_cate = $cate_count;
}

?>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> Upload a Document</h1>
<p>Fields marked with a * are required.</p>
<form id=frm_upload method=post action=admin_upload_process.php enctype="multipart/form-data" onsubmit="return Validate();" language=jscript>
<table id=tbl_upload><tbody>
    <tr>
        <th>Document Title:*</th>
        <td><input type=text name=doctitle size=50 maxlength=200></td>
    </tr>
    <tr>
        <th>Document Contents:</th>
        <td><textarea name=doccontents rows=3 cols=40></textarea></td>
    </tr>
    <tr>
        <th>Category:*</th>
        <td><select name=doccateid>            
			<option <?php echo $catid=="X" ? "selected":"";?> value=X>--- SELECT ---</option>
			<?php echo subFunction($categories[0],0,""); ?>
		</select></td>
    </tr>
    <tr>
        <th>Document Type:*</th>
        <td><select name=doclistid>
			<option value=0 selected>Unspecified</option><?php
			$sql = "SELECT * FROM ".$dbref."_list_type WHERE yn = 'Y' ORDER BY value";
				$rs = $doc0->mysql_fetch_all($sql);
				foreach($rs as $row) {
				echo "<option value=".$row['id'].">".$row['value']."</option>";
			}
			//mysql_close();
		?></select></td>
    </tr>
    <tr>
        <th>Document Date:*</th>
        <td><input type=text name=docdate readonly=readonly class=jdate2012 value="<?php echo(date("d-M-Y")); ?>"></td>
    </tr>
    <tr>
        <th>Document:*</th>
        <td><input type=file name=docfile></td>
    </tr>
    <tr>
        <th>Document Format:*</th>
        <td><select name=doctypeid>
            <option selected value=X>--- SELECT ---</option>
            <?php
            $sql = "SELECT * FROM ".$dbref."_list_doctype WHERE yn = 'Y'";
            $doctype = $doc0->mysql_fetch_all($sql);
                //while($row = mysql_fetch_array($rs))
                foreach($doctype as $row) {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option value=$id> $val </option>");
                }
            //mysql_close($con);
            ?>
        </select></td>
    </tr>
    <tr>
        <td colspan=2><input type=submit value=Upload> <input type=reset></th>
    </tr>
</tbody></table>
</form>
<script type=text/javascript>
$(document).ready(function() {
	$("input:submit").button().css("font-size","75%");
	$("input:reset").button().css("font-size","75%");
	$("input:button").button().css("font-size","75%");
	$("#tbl_upload th").addClass("left");
});
</script>
</body></html>