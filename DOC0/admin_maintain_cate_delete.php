<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Delete a Category</b></h1>
<?php
$variables = $_REQUEST;
$cateid = $variables['c'];

if($doc0->checkIntRef($cateid))
{

    $sql = "SELECT * FROM ".$dbref."_content WHERE doccateid = $cateid ";
    $rs = $doc0->mysql_fetch_all($sql);
	$dc = array();
	foreach($rs as $row) {

            $dc[] = $doc['docid'];
            $old = $doc['doclocation'];
            $loc = strFn("explode",$old,"/","");
            $new = "/files/".$cmpcode."/deleted_".date("YmdHis")."_".$loc[count($loc)-1];
            rename("..".$old,"..".$new);
        }
    //mysql_close($con);
    $d = strFn("implode",$dc,", ","");
    $sql = "UPDATE ".$dbref."_content SET docyn = 'N' WHERE doccateid = $cateid ";
    $doc0->db_update($sql);
                logAct("Deleted documents (".$d.") belonging to category $cateid due to category deletion",$sql,"DOC",0);

    $sql = "UPDATE ".$dbref."_categories SET cateyn = 'N' WHERE cateid = $cateid ";
    $mar = $doc0->db_update($sql);
    if($mar>0)
    {
                logAct("Deleted category $cateid",$sql,"CATE",$cateid);
        echo("<h3>Success!</h3>");
        echo("<p>Category $cateid has been successfully deleted.</p>");
        $urlback = "admin_maintain.php";
        displayGoBack($urlback);
    } else {
        die("<p>An error has occurred and Ignite Assist was unable to delete the category.  Please go back and try again.</p>");
    }
} else {
    die("An error has occurred in identifying the category you wish to delete.  Please go back and try again.");
}
?>
</body>
</html>
