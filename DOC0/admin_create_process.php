<?php
    require 'inc_head.php';
?>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> Create a Category</h1>
<p>Processing.....</p>
<?php

//arrPrint($_REQUEST); 

$variables = $_REQUEST;
$catetitle = ASSIST_HELPER::code($variables['catetitle']);
$cateyn = "Y";
$catesubarray = $variables['catesubid'];
$cateowner = $variables['cateowner'];
//$view = $variables['cateview'];
//$up = $variables['cateupload'];
$up = $variables['upload'];
if(!in_array($cateowner,$up)) { $up[] = $cateowner; }
$maint = $variables['maintain'];
if(!in_array($cateowner,$maint)) { $maint[] = $cateowner; }

$parent = $catesubarray[0];
foreach($catesubarray as $c) {
	if($c!="X") {
		$catesubid = $c;
	} else {
		break;
	}
}
if($catesubid==0 || !$doc0->checkIntRef($catesubid)) {
	$view = $variables['view'];
} else {
	$sql = "SELECT cutkid FROM ".$dbref."_categories_users WHERE cucateid = $parent AND cuyn = 'Y'";
	$view = $doc0->mysql_fetch_fld_one($sql,"cutkid");
}
if(!in_array($cateowner,$view)) { $view[] = $cateowner; }

if(strlen($catetitle)>0 && strlen($cateowner)>0 && $cateowner!="X" && is_numeric($catesubid))
{
    $sql = "INSERT INTO ".$dbref."_categories VALUES (null,'$catetitle','Y',$catesubid,'$cateowner')";
    $cateid = $doc0->db_insert($sql);
	$l = array(
		'section'=>"CATE",
		'ref'=>$cateid,
		'cateid'=>$cateid,
		'action'=>"C",
		'field'=>serialize(array("catetitle","catesubid","cateowner")),
		'transaction'=>"Created new category '".ASSIST_HELPER::decode($catetitle)."' ($cateid).",
		'old'=>"",
		'new'=>serialize(array("catetitle"=>$catetitle,"catesubid"=>$catesubid,"cateowner"=>$cateowner)),
	);
	logTransaction('ACTIVITY',$l,$sql);
    if($doc0->checkIntRef($cateid)) {
		$sql = "(null,$cateid,'".implode("','VIEW','Y'),(null,$cateid,'",$view)."','VIEW','Y')";
		$sql.= ",(null,$cateid,'".implode("','UP','Y'),(null,$cateid,'",$up)."','UP','Y')";
		$sql.= ",(null,$cateid,'".implode("','MAIN','Y'),(null,$cateid,'",$maint)."','MAIN','Y')";
		$sql = "INSERT INTO ".$dbref."_categories_users VALUES ".$sql;
		$doc0->db_insert($sql);
		$l = array(
			'section'=>"CATE",
			'ref'=>$cateid,
			'cateid'=>$cateid,
			'action'=>"C",
			'field'=>"",
			'transaction'=>"Set user access for new category '".ASSIST_HELPER::decode($catetitle)."' ($cateid).",
			'old'=>"",
			'new'=>"",
		);
		logTransaction('ACTIVITY',$l,$sql);
		$catetitle = ASSIST_HELPER::decode($catetitle);
		echo "<script type=text/javascript>document.location.href = 'admin_create.php?r[]=ok&r[]=Category ".$catetitle." has been successfully created.';</script>";
    } else {
        die("<p><span class=idelete>ERROR!</span> An error has occurred.  Please go back and try again.</p>");
    }
} else {
    die("<p><span class=idelete>ERROR!</span> Please complete all the required fields.</p>");
}
?>
</body>
</html>
