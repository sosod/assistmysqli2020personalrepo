	$(function() {
		$( "#dialog" ).dialog({
			autoOpen: false,
			width: "auto",
			modal: true
		});
		$( "#del_dialog" ).dialog({
			autoOpen: false,
			width: 400,
			modal: true
		});

		$( "#add" ).click(function() {
			$('#act').attr('value',"SAVE_COMM");
			$('#dialog').dialog('option','title','Add Comment');
			$('#dialog').dialog('open');
			$('#commtext').attr('innerText',"");
			$('#commid').attr('value',"");
			return false;
		});
		$("#del_no").click(function() {
			$("#del_dialog").dialog("close");
			return false;
		});
		$("#cancel").click(function() {
			$("#dialog").dialog("close");
			return false;
		});
	});
	function delComm(commid) {
		document.getElementById('del_commid').value = commid;
		var fld = "text_"+commid;
		document.getElementById('del_commtext').innerText = document.getElementById(fld).innerText;
		$("#del_dialog").dialog("open");
		return false;
	}
	function editComm(commid) {
		document.getElementById('act').value = "EDIT_COMM";
		document.getElementById('commid').value = commid;
		var fld = "text_"+commid;
		document.getElementById('commtext').innerText = document.getElementById(fld).innerText;
		$("#dialog").dialog("option","title",'Edit Comment');
		$("#dialog").dialog("open");
		return false;
	};