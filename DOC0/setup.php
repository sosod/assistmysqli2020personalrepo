<?php
    require 'inc_head.php';
    
//print_r($_POST);
$result = array();
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : false;

if(isset($_REQUEST['act'])) {
switch($_REQUEST['act']) {
case "setup":
	$mnadm = $_POST['mnadmn'];
	$a = $_POST['a'];
	if(strlen($mnadm) > 0 && $a == "modadmin")
	{
		$tref = strtoupper($tref);
		//GET SETUP ADMIN DATA
		$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = '".$tref."' AND refid = 0";
		//include("inc_db_con.php");
	//    $u = 0;
		$u = $doc0->db_get_num_rows($sql);
		if($u == 0)
		{
			//IF NO RESULT THEN CREATE RECORD
			$sql2 = "INSERT INTO assist_".$cmpcode."_setup SET ref = '".$tref."', refid = 0, value = '".$mnadm."', comment = 'Setup administrator', field = ''";
		}
		else
		{
			//IF RESULT THEN UPDATE RECORD
			$sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '".$mnadm."' WHERE ref = '".$tref."' AND refid = 0";
		}
		//mysql_close($con);
		//RUN SQL CREATED ABOVE
		$sql = $sql2;
		$doc0->db_query($sql);
		//include("inc_db_con.php");
			$tsql = $sql;
			//$tref = "MN";
			$trans = "Setup administrator updated.";
			//include("inc_transaction_log.php");
			logAct("Updated Setup Administrator to ".$mnadm,$sql2,"S_ADMIN",0);
		$sql = "";
		$result[1] = "Setup Administrator update complete.";
		$result[0] = "check";

	}
	break;
case "short":
	$value = isset($_REQUEST['v']) && $doc0->checkIntRef($_REQUEST['v']) ? $_REQUEST['v'] : 20;
	$refid = 1;
		$tref = strtoupper($tref);
		//GET SETUP ADMIN DATA
		$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = '".$tref."' AND refid = ".$refid;
		$u = $doc0->db_get_num_rows($sql);
		if($u == 0) {
			//IF NO RESULT THEN CREATE RECORD
			$sql2 = "INSERT INTO assist_".$cmpcode."_setup SET ref = '".$tref."', refid = ".$refid.", value = '".$value."', comment = 'Display limitation', field = ''";
		} else {
			//IF RESULT THEN UPDATE RECORD
			$sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '".$value."' WHERE ref = '".$tref."' AND refid = ".$refid;
		}
		//mysql_close();
		//RUN SQL CREATED ABOVE
		$sql = $sql2;
		$rs = $doc0->db_query($sql);
			$tsql = $sql;
			$trans = "Display limitation updated.";
			//include("inc_transaction_log.php");
			logAct("Updated Display Limitation to ".$value,$sql2,"S_LIMIT",1);
		$sql = "";
		$result[1] = "Display limitation updated to $value.";
		$result[0] = "check";
	break;
}
}

//echo("<P>: $mnadm :");
//echo("<P>: $a :");
//echo("<P>: $sql :");
//echo("<P>: $u :");
//echo("<P>: $sql2 :");
    include("inc_admin.php");
    

?>
<h1>Setup</h1>
<?php ASSIST_HELPER::displayResult($result); ?>
<form name=update method=post action=setup.php>
	<input type=hidden name=a value=modadmin /><input type=hidden name=act value=setup />
<table cellpadding=3 cellspacing=0 width=700>
    <tr height=30>
        <th style="text-align:left;">Module Admin</th>
        <td><select name=mnadmn>
<?php
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($modadmin == "0000")
{
?>
    <option selected value=0000>Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = '".$tref."' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
//include("inc_db_con.php");
$rs = $doc0->mysql_fetch_all($sql);
foreach($rs as $row) {
    $tid = $row['tkid'];
    if($tid == $modadmin)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
//mysql_close($con);

?>
</select><span class=float><input type=submit value=" Save "></span></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Display Limitation</th>
        <td>How many characters of a category title should be displayed in drop down lists:<span class=float><select id=short>
		<?php 
		for($i=5; $i<=15; $i++) { echo "<option ".($catedisplay==$i ? "selected" : "")." value=$i>$i</option>"; }
		for($i=20; $i<=50; $i+=5) { echo "<option ".($catedisplay==$i ? "selected" : "")." value=$i>$i</option>"; }
		?>
		</select>&nbsp;<input type=button value=Save id=btn_short /></span></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">User Access</th>
        <td>Configure user access.&nbsp;<span class=float><input type=button value=Configure id=2 onclick="document.location.href = 'setup_access.php';"></span></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document Type</th>
        <td>Configure Document Types.&nbsp;<span class=float><input type=button value=Configure id=2 onclick="document.location.href = 'setup_list_doctype.php';"></span></td>
    </tr>
	<tr height=30>
        <th style="text-align:left;">Data Report</th>
        <td>Generate a report of the current data storage of <?php echo($moduletitle); ?>.<span class=float><input type=button value=Generate id=8 onclick="setupGoTo('data_report');"></span></td>
    </tr>
</table>
</form>
<script type=text/javascript>
$(function() {
	$("input:button").button().css("font-size","75%");
	$("input:submit").button().css("font-size","75%");
	$("#btn_short").click(function() {
		var v = $("#short").val();
		document.location.href = "setup.php?act=short&v="+v;
	});
});
</script>
</body>

</html>
