<?php

class DOC0 extends DOC0_HELPER {
	/***
	 * Module Wide variables
	 */



	/**
	 * Module Wide Constants
	 * */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;

	const ASSIST_USER = 256;
	const NON_USER = 512;

	public $modref;
	public function __construct($modref="doc") {
		parent::__construct($modref);
		$this->modref = $modref;
	}


	function getModuleTitle($modref = "doc"){

		$tref = strtolower($modref);

		if(strlen($tref)>0) {
			$menu = array();

			$sql = "SELECT modtext, modlocation FROM assist_menu_modules WHERE modref = '$tref' AND modyn = 'Y'";
			$menu = $this->mysql_fetch_one($sql);
			if(count($menu)>0)
			{
				$moduletitle = $menu['modtext'];
			}
			else
			{
				$moduletitle = "Reports Assist";

			}
		} else {

		}
		return $moduletitle;
	}


}

?>