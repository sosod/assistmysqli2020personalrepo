<?php


	class DOC0_DISPLAY
	{
		function JSdisplayResultPrep($txt="") {
			/*	echo "<div id=display_result><p><span id=display_result_icon></span><span id=display_result_text>".$txt."</span></p></div></div>
						<script type=text/javascript>
						function JSdisplayResult(result,icon,txt) {
							if(result.length>0) {
								$(\"display_result\").show();
								$(\"display_result_text\").text('');
								//$(\"#display_result_text\").attr(\"innerText\",\"\");
								//$(\"#display_result\").addClass(\"ui-widget ui-state-\"+result);
								$(\"#display_result\").css({\"margin\":\"5px 0px 10px 0px\",\"padding\":\"0 .3em\"});
								$(\"#display_result_icon\").addClass(\"ui-icon\");
								switch(icon) {
									case \"ok\":		$(\"#display_result_icon\").addClass(\"ui-icon-check\"); break;
									case \"info\":	$(\"#display_result_icon\").addClass(\"ui-icon-info\"); break;
									case \"error\":	$(\"#display_result_icon\").addClass(\"ui-icon-closethick\"); break;
									default:		$(\"#display_result_icon\").addClass(\"ui-icon-\"+icon); break;
								}
								$(\"#display_result_icon\").css({\"float\":\"left\",\"margin-right\":\".3em\"});
								$(\"#display_result_text\").prop(\"innerText\",txt);
							} else {
								$(\"#display_result\").hide();
							}
						}
						</script>";*/
			echo "
			<div class=\"ui-widget\">
				<div id=display_result class=\"ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
					<p>
						<span id=display_result_icon class=\"\" style=\"float: left; margin-right: .3em;\"></span>
						<span id=display_result_text>".$txt."</span>
					</p>
				</div>
			</div>
			<script type=text/javascript>
			function JSdisplayResult(result,icon,txt) { 
				if(result.length>0) {
					$(\"display_result\").show();
					$(\"display_result_text\").text('');
					$(\"#display_result_icon\").removeClass().addClass(\"ui-icon\");
					$(\"#display_result\").removeClass();
					switch(icon) {
						case \"ok\":		$(\"#display_result_icon\").addClass(\"ui-icon-check\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-ok\"); break;
						case \"info\":	$(\"#display_result_icon\").addClass(\"ui-icon-info\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-info\"); break;
						case \"error\":	$(\"#display_result_icon\").addClass(\"ui-icon-closethick\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-fail\"); break;
						default:		$(\"#display_result_icon\").addClass(\"ui-icon-\"+icon); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-\"+result); break;
					}
					$(\"#display_result_text\").text(txt);
				} else {
					$(\"#display_result\").hide();
				}
			}
			</script>";
		}
	}