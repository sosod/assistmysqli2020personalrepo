function setupGoTo(loc) {
    if(loc.length > 0)
    {
        document.location.href = 'setup_'+loc+'.php';
    }
}

function adminGoTo(loc) {
    if(loc.length > 0)
    {
        document.location.href = 'admin_'+loc+'.php';
    }
}

function gotoUpload(sec,c) {
    var url = "";
    switch(sec)
    {
        case "admin":
            url = "admin.php";
            break;
        case "cate":
            if(!isNaN(parseInt(c)) && c > 0)
            {
                url = "admin_maintain_doc.php?c="+c;
            }
            else
            {
                url = "admin_maintain.php";
            }
            break;
        case "up":
            if(!isNaN(parseInt(c)) && c > 0)
            {
                url = "admin_upload.php?c="+c
            }
            else
            {
                url = "admin_upload.php";
            }
            break;
        default:
            url = "admin.php";
            break;
    }
    document.location.href = url;
}
