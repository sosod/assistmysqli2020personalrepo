<?php
				$mod = $menu_mod['amenu'];
				$modref = $_SESSION['MAIN']['modref'];
				$modloc = $_SESSION['MAIN']['modlocation'];
				$mod_object = isset($_SESSION['MAIN']['mod_object']) ? $_SESSION['MAIN']['mod_object'] : 0;
				$_SESSION['modlocation'] = $modloc;
				$my_assist->setModRef($modref);
				$tdb = new ASSIST_DB();

$new_button = false;
$new_link = "";

$file_name = strtoupper($modloc)."/login_table.php";
$actions = array();
$head = array();
$file_exists = false;
if(file_exists($file_name)) {
	$file_exists = true;
	include($file_name);
}

	echo "
	<table cellpadding=3 cellspacing=0  class=noborder width=100% id=list>
		<tr><td class=noborder  style=\"text-align: center\">";
		if($new_button===true) {
			echo "<button id=btn_action_new modloc='$modloc' modref='$modref' act=goto_mod action=view link='".$new_link."'>New</button>";
		}
		if(count($actionmenu)>1) {
			/*echo "<span style=\"float: right\"><select id=amenu>";
			foreach($actionmenu as $a) {
				echo "<option ";
					if($menu_mod['amenu'] == $a['modref']) { echo "selected "; }
				echo "value='".strtoupper($a['modref'])."'>".$a['modtext']."</option>";
			}
			echo "</select></span>";*/
			echo "<h2 style=\"margin-top: 0px; margin-bottom: 0px;\">".$_SESSION['MAIN']['modtext']."</h2>";
		} else {
			echo "<h2 style=\"margin-top: 0px;\"><span style=\"text-decoration: underline;\">".$actionmenu[$menu_mod['amenu']]['modtext']."</span> actions due within the next $next_due days</h2>";

		}
		echo "</td></tr>";
		echo "<tr>";
			echo "<td class=noborder>";
				//arrPrint($_SESSION);
				if($file_exists) {
					if(count($actions)>0) {
							echo "<table cellpadding=3 cellspacing=0  width='100%'>
									<tr>";
									foreach($head as $h) {
										echo "<th>".$h['text']."</th>";
									}
									echo "</tr>";
							foreach($actions as $act) {
								echo "<tr>";
								foreach($head as $fld => $h) {
									switch($fld) {
									case "ref":
										echo "<th width=50>".$act[$fld]."</th>";
										break;
									case "link":
										echo "<td width=50 class=center><input type=button value=Update modloc='$modloc' modref='$modref' act=goto_mod action=view link='".$act['link']."' /></td>";
										break;
									default:
										$v = $act[$fld];
										$class = "";
										$width = "";
										if($h['long']) {
											if(strlen($v)>100) { $v = "<label id=lbl_".$act['ref']." for=img_".$act['ref'].">".strFn("substr",$v,0,75)."...</label>&nbsp;<img src=\"/pics/plus.gif\" ref=".$act['ref']." id=img_".$act['ref']." class=more_text alt=\"".str_replace('"',"'",$v)."\">"; }
										}
										if($h['deadline']) {
											$class = "center";
											$width = "width=100";
										} elseif(isset($h['class']) && strlen($h['class'])>0) {
											$class = $h['class'];
										}
										echo "<td $width class=\"$class\">".$v."</td>";
										break;
									}
								}
								echo "</tr>";
							}
							echo "</table>";
					} else {
						echo "<p>No actions overdue, due today or due within the next $next_due days.</p>";
					}
					//arrPrint($head);
					//arrPrint($actions);
				} else {
					echo "No action listing found.";
				}
			
			echo "</td>";
		echo "</tr>";
	echo "</table>";
?>
