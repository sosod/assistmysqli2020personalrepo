<?php
//include_once("/library/class/assist_helper.php");

$logo_details = ASSIST_HELPER::getModuleLogoDetails();

$shortcut_details = array();
$shortcut_details['path'] = "../pics/module/";
$shortcut_details['logos'] = $logo_details['logos'];
/*$shortcut_details['logos'] = array(
	'CAPS'	=> "2013caps.png",
	'CAPS1B'	=> "2013caps.png",
//	'ACTION'=> "2013action.png",
	'ACTION'=> "2015action.png",
	'ACTION0'=> "2015action.png",
	'JOBS'=> "2013action.png",
	'SDBP4'	=> "sdbp4.png",
	'SDBP5'	=> "2013sdbp5.png",
	'SDBP5B'	=> "2013sdbp5.png",
	'SDBP5C'	=> "2017sdbp5c.png",
	'SDBP5PM'	=> "2016sdbp5pm.png",
	'SDBP5BCPM'	=> "2016sdbp5pm.png",
	'SDBP6'	=> "2017sdbp6.png",
	'PM3'	=> "2016sdbp5pm.png",
	'IND_KPI'	=> "2016sdbp5i.png",
	'PROJECT'	=> "2013sdbp5.png",
	'DOC'	=> "2015doc1.png",
	'DOC0'	=> "2015doc1.png",
//	'TD'	=> "td.png",
	'TD'	=> "2015tk.png",
	'DSHP'	=> "dshp.png",
	'USER_MANUALS'	=> "manuals.png",
//	'TK'	=> "td.png",
	'TK'	=> "2015tk.png",
	'PM'	=> "pm.png",
	'COMPL'=>"2013compl.png",
	'QUERY'=>"2013query.png",
	'CASE'=>"2013query.png",
	'RGSTR'=>"2013rgstr.png",
	'RGSTR_ALT'=>"2014rgstr_alt.png",
	'RAPS'=>"2013raps.png",
//	'S'=>"2013setup.png",
	'S'=>"2015setup.png",
	'HP'=>"hp.png",
	'NB'=>"noticeboard2014.png",
	'CONTRACT'	=>	"contract2014.png",
	'CNTRCT'	=>	"2015cntrct.png",
	'SCORECARD'	=>	"scorecard2014.png",
	'SMS'	=> "2014sms.png",
	'MASTER'	=> "2015master2.png",
	'MDOC'	=> "2015doc1.png",
	'DTM'=>"dtm.png",
	'SAGE'=>"sage.png",
	'reports'=>"2016reports.png",
	'IDP1'=>"2016idp1b.png",
	'IDP2'=>"2016idp1b.png",
	'JAL1'=>"2017jal1.png",
	'LDAL1'=>"2017ldal1.png",
	'AIT'=>"ait.png",
	'EMP1'=>"2017emp1.png",
	'PDP'=>"2017pdp.png",
	'helpdesk'=>"helpdesk.png",
);
*/
$shortcut_details['default'] = "circle.png";

$shortcut_details['new'] = "add_new.png";



function drawShortcuts($m=array()) {
	global $shortcut_details;
	$assist = new ASSIST();
		$cmpcode = $assist->getCmpCode();
		$tkid = $assist->getUserID();
	$db = new ASSIST_DB();
	$echo = "";
	$profile = isset($_SESSION['USER_PROFILE'][2]['field5']) ? $_SESSION['USER_PROFILE'][2]['field5'] : array();
	$menu = array();
	if(count($profile)>0 || $tkid=="0000") {
		if(count($m)==0) {
			$sql = "SELECT m.modtext, m.modref, m.modlocation FROM assist_menu_modules m ";
			if($tkid=="0000") {
				$sql.=" WHERE m.modadminyn = 'Y' ORDER BY m.modtext";
			} else {
				$sql.= "INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users u
					ON m.modref = u.usrmodref AND u.usrtkid = '".$_SESSION['tid']."'
					WHERE m.modyn = 'Y' AND m.modref IN ('".implode("','",$profile)."')";
			}
			$m = $db->mysql_fetch_all_fld($sql,"modref");
			/*$m['USER_MANUALS'] = array(
				'modref'=>"USER_MANUALS",
				'modtext'=>"User Manuals",
				'modlocation'=>"USER_MANUALS",
			);*/
		}
			/*echo "P";
			$assist->arrPrint($profile);
			echo "M";
			$assist->arrPrint($m);*/
		if($tkid=="0000") {
			foreach($m as $p => $r) {
				$menu[$p] = $r;
			}
			//$assist->arrPrint($m);
		} else {

			foreach($profile as $p) {
				if(isset($m[$p])) {
					$menu[$p] = $m[$p];
				}
			}
		}
	}
							$echo.= "<ul class=sortablefolders>";
							if(count($menu)>0) {
								foreach($menu as $ct) {
									//CHANGES TO SHORTCUTS MUST ALSO BE ADDED TO USER_PROFILE.PHP
									if($ct['modlocation']=="RGSTR" && strtoupper(substr($ct['modtext'],0,4))!="RISK") {
										$logo_ref = "RGSTR_ALT";
									} elseif(($ct['modlocation']=="SDBP5" || $ct['modlocation']=="SDBP5B") && !is_numeric(substr($ct['modref'],-1))) {
										$logo_ref = "IND_KPI";
									} elseif($ct['modlocation']=="EXTERNAL") {
										$logo_ref = $ct['modref'];
									} else {
										$logo_ref = $ct['modlocation'];
									}
									if(isset($shortcut_details['logos'][$logo_ref])) {
										$ct['icon'] = $shortcut_details['path'].$shortcut_details['logos'][$logo_ref];
									} else {
										$ct['icon'] = $shortcut_details['path'].$shortcut_details['default'];
									}
									$echo.= "<li id=".$ct['modref']." class=\"doclink no_hover\" action=view act=open_mod modtext='".$ct['modtext']."' modref=".$ct['modref']." modloc=".$ct['modlocation']." style=\"width: 85px;height: ".(strpos($ct['modlocation'],"PM")===false?"90":"105")."px;\">";
										$echo.= "<img src=".$ct['icon']." width=60px /><br />".$ct['modtext'];
									$echo.= "</li>";
								}
							} else {
								$ct['modref']="user_profile";
								$ct['modlocation']="user_profile";
								$ct['modtext']="Add Shortcut";
								$ct['icon'] = $shortcut_details['path'].$shortcut_details['new'];
									$echo.= "<li id=".$ct['modref']." class=\"doclink no_hover\" action=view act=open_mod modref=".$ct['modref']." modloc=".$ct['modlocation']." style=\"width: 85px;height: 90px;\">";
										$echo.= "<img src=".$ct['icon']." width=60px /><br />".$ct['modtext'];
									$echo.= "</li>";
							}
			$echo.= "				</ul>";

return $echo;

}






function drawShortLi($ref,$txt,$img) {
	$echo = "";
	global $shortcut_details;

return $echo;

}





?>