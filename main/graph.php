<?php
/**
 * @param $graph
 */
function drawMyActionDashboardGraph($graph) {
global $next_due_head;
	echo "<h2 class=center style=\"margin-top: 0px; margin-bottom: 2px\">".$_SESSION['MAIN']['modtext']."</h2>
			<div id=action_graph style=\"width: 200px; height: 180px; background-color: #ffffff;\"></div>";
	
	
	$result_settings = array(
		'past'=>array(
			'text'=>"past",
			'color'=>"#CC0001",
			'value'=>"Overdue",
			'obj'=>(isset($graph['past']) && ASSIST_HELPER::checkIntRef($graph['past']) ? $graph['past'] : 0)
		),
		'present'=>array(
			'text'=>"present",
			'color'=>"#009900",
			'value'=>"Due Today",
			'obj'=>(isset($graph['present']) && ASSIST_HELPER::checkIntRef($graph['present']) ? $graph['present'] : 0)
		),
		'future'=>array(
			'text'=>"future",
			'color'=>"#FE9900",
			'value'=>$next_due_head,
			'obj'=>(isset($graph['future']) && ASSIST_HELPER::checkIntRef($graph['future']) ? $graph['future'] : 0)
		)
	);
	
//function drawJavaGraph($result_settings,$values,$type = "view") { 
	$kr2 = array();
	foreach($result_settings as $ky => $k) {
		//$k['obj'] = isset($values[$k['text']]) ? $values[$k['text']] : 0;
		$val = array();
		foreach($k as $key => $s) {
			$val[] = $key.":\"".$s."\"";
		}
		$kr2[$ky] = implode(",",$val);
	}
	$chartData = "{".implode("},{",$kr2)."}";


	$echo = "<script type=\"text/javascript\">
        var chart;
		var legend;
        var chartData = [".$chartData."];
		
		$(document).ready(function() {
            main_chart = new AmCharts.AmPieChart();
			main_chart.color = \"#ffffff\";
            main_chart.dataProvider = chartData;
            main_chart.titleField = \"value\";
			main_chart.valueField = \"obj\";
			main_chart.colorField = \"color\";
			main_chart.pulledField=\"pull\";
			//main_chart.labelsEnabled = false;
			main_chart.labelText = \"[[title]]\";
			main_chart.labelRadius = -35;
			main_chart.outlineColor = \"#000000\";
			main_chart.outlineAlpha = 0.3;
			main_chart.outlineThickness = 1;
			main_chart.angle = 20;
			main_chart.depth3D = 15;
			main_chart.hideLabelsPercent = 5;
			main_chart.hoverAlpha = 0.7;
			main_chart.startDuration = 0;
			main_chart.radius = 90;
			main_chart.marginBottom = 20;
			main_chart.marginRight = 0;
			main_chart.balloonText = \"[[title]]: [[value]] ([[percents]]%)\";
			main_chart.write(\"action_graph\");
		});
		</script>";

	echo $echo;
}


?>