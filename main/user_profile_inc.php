<?php
/********************************
This page allows the user to create their own profile for Ignite Assist.

Table: assist_$cmpcode_timekeep_profile

Version 1.1
Created by: Janet Currie
Created on: 27 Dec 2011
Created Action List section (1) with options to set Default module, Number of actions and Sort order.

Version 1.2
Modified on: 11 March 2012
Modified by: Janet Currie
Added section 2 to allow user to define shortcuts for /main_login.

Version 1.3
Modified on: 2 July 2012
Modified By: Janet Currie
Added section 3 to allow user to decide whether or not to display folders for Documents modules in the Documents div on the front page.

********************************/


require '../inc_session.php';
require_once '../inc_assist.php';
require_once '../inc_codehelper.php';
require_once '../inc_db.php';
require_once '../inc_db_conn.php';


require_once "../library/autoloader.php";

$me = new ASSIST();
$db = new ASSIST_MODULE_HELPER();

$split_modules = $me->getSplitModules();


require_once 'shortcuts.php';

/* SECTION 1: $mods = action modules */
$sql = "SELECT m.modtext, m.modref, m.modlocation FROM assist_menu_modules m, assist_".$cmpcode."_menu_modules_users u WHERE modlocation IN ('".implode("','",$action_modules)."') AND usrtkid = '$tkid' AND modref = usrmodref AND modyn = 'Y' ORDER BY modtext";
$mods = $db->mysql_fetch_all_fld($sql,"modref");//mysql_fetch_all_fld($sql,"modref");

/* SECTION 2: $menu = all modules */
$sql = "SELECT m.modtext, m.modref, m.modlocation
		FROM assist_menu_modules m
		, assist_".$cmpcode."_menu_modules_users u
		WHERE u.usrtkid = '$tkid'
		AND m.modref = usrmodref
		AND m.modyn = 'Y'
		AND m.modref NOT IN ('".implode("','",$jean)."')
		ORDER BY m.modtext";
$menu = $db->mysql_fetch_all_fld($sql,"modref");
		/*$menu['USER_MANUALS'] = array(
			'modref'=>"USER_MANUALS",
			'modtext'=>"User Manuals",
			'modlocation'=>"USER_MANUALS",
		);*/


/* SECTION 3: $docs = documents modules */
$sql = "SELECT m.modtext, m.modref FROM assist_menu_modules m, assist_".$cmpcode."_menu_modules_users u WHERE modlocation IN ('DOC') AND usrtkid = '$tkid' AND modref = usrmodref AND modyn = 'Y' ORDER BY modtext";
$docs = $db->mysql_fetch_all_fld($sql,"modref");

if( !isset($_SESSION['USER_PROFILE'][3]['field1']) || !(strlen($_SESSION['USER_PROFILE'][3]['field1'])>0) ) {
	$_SESSION['USER_PROFILE'][3]['field1'] = $_SESSION['MAIN']['docmenu'];
}


$profile = $_SESSION['USER_PROFILE'];

$section1_field3 = array(
	'dead_asc' => "Deadline date: Oldest to Newest",
	'dead_desc' => "Deadline date: Newest to Oldest",
);

$field_headings = array(
	1 => array(
		'field1'=>"Default Module",
		'field2'=>"Maximum Number of Actions to List",
		'field3'=>"Display Order",
		'field4'=>"Days to Display",
	),
	3 => array(
		'field1'=>"Default Module",
		'field5'=>"Display Folders",
	),
);


$_REQUEST['jquery_version'] = "1.10.0";
$me->displayPageHeader();
?>
<h1>My Profile</h1>