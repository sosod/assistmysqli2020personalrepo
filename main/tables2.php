<?php



				$mod = $menu_mod['amenu'];
				$modref = $_SESSION['MAIN']['modref'];
				$modloc = $_SESSION['MAIN']['modlocation'];
				$mod_object = isset($_SESSION['MAIN']['mod_object']) ? $_SESSION['MAIN']['mod_object'] : 0;
				if(strlen($modref)>0 && strlen($modloc)>0) {
					$_SESSION['modlocation'] = $modloc;
					$my_assist->setModRef($modref);
					$tdb = new ASSIST_DB();

					$new_button = false;
					$new_link = "";

					$file_name = strtoupper($modloc)."/login_table.php";
					$actions = array();
					$head = array();
					$file_exists = false;
					if(file_exists($file_name)) {
						$file_exists = true;
						include($file_name);
					}


					if($file_exists) {
						if(count($actions) > 0) {
							echo "<table cellpadding=3 cellspacing=0  width='100%' id='action_tables'>
									<tr>";
							foreach($head as $key => $h) {
								if($key == "link") {
									echo "<th>";
									if($new_button === true) {
										echo "<button id=btn_action_new modloc='$modloc' modref='$modref' act=goto_mod action=view link='".$new_link."'>New</button>";
									}
									echo "</th>";
								} else {
									echo "<th>".$h['text']."</th>";
								}
							}
							echo "</tr>";
							foreach($actions as $act) {
								echo "<tr>";
								foreach($head as $fld => $h) {
									switch($fld) {
										case "ref":
											echo "<td class=td_ref width=50>".$act[$fld]."</td>";
											break;
										case "link":
											echo "<td width=50 class=center><button modloc='$modloc' modref='$modref' act=goto_mod action=view link='".$act['link']."'>Update</button></td>";
											break;
										default:
											$v = $act[$fld];
											$class = "";
											$width = "";
											if($h['long']) {
												if(strlen($v) > 100) {
													$v = "<label id=lbl_".$act['ref']." for=img_".$act['ref'].">".strFn("substr", $v, 0, 75)."...</label>&nbsp;<img src=\"/pics/plus.gif\" ref=".$act['ref']." id=img_".$act['ref']." class=more_text alt=\"".str_replace('"', "'", $v)."\">";
												}
											}
											if($h['deadline']) {
												$class = "center";
												$width = "width=100";
											} elseif(isset($h['class']) && strlen($h['class']) > 0) {
												$class = $h['class'];
											}
											echo "<td $width class=\"$class\">".$v."</td>";
											break;
									}
								}
								echo "</tr>";
							}
							echo "</table>";
						} else {
							echo "<p>No actions overdue, due today or due within the next $next_due days.</p>";
						}
						//arrPrint($head);
						//arrPrint($actions);
					} else {
						echo "<p>No actions found to display.</p>";
					}
				} else {
					echo "<P class='i'>No actions to display.";
				}
?>
