<?php
$me = new ASSIST();

//$sql = "SELECT * FROM assist_menu_modules WHERE modadminyn = 'Y' ORDER BY modtext";
//$menu = $db->mysql_fetch_all($sql);

//$me->arrPrint($menu);

function drawAdminUserModuleShortcuts() {

	echo "<DIV class=acc_display style='margin-top: -1px;' id=acc_user>
			<H3><A href=\"#\" class=accord>User Modules</A></H3>
			<DIV id=div_short_user style=\"padding: 5px;\" class=short_min>".drawShortcuts()."</DIV>
		</div>";

}


function drawAdminSystemShortcuts() {
	global $my_assist;
	$m = $my_assist->getAdminSystemMenu();
	$modules = array();
	foreach($m as $p => $r) {
		$modules[$p] = array(
				'modref'=>$p,
				'modtext'=>$r,
				'modlocation'=>$p,
			);
	}
	
	echo "<DIV class=acc_display>
			<H3><A href=\"#\" class=accord>System Modules</A></H3>
			<DIV id=div_short_system style=\"padding: 5px;\" class=short_min>".drawShortcuts($modules)."</DIV>
		</div>";

}

?>