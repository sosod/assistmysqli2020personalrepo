<?php
//    require '../inc_session.php';
//require_once '../inc_assist.php';
//require_once '../inc_codehelper.php';
//require_once '../inc_db.php';
//require_once '../inc_db_conn.php';

include_once("../module/autoloader.php");

$my_assist = new ASSIST_MODULE_HELPER();
$cmpcode = $my_assist->getCmpCode();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Action4u.co.za</title>
<base target="main">
	<link href="/library/jquery-ui-1.10.0/css/jquery-ui.css?1456142536" rel="stylesheet" type="text/css"/>

<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery.min.js"></script>
<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery-ui.min.js"></script>

<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
<script type ="text/javascript" src="/library/js/assiststring.js"></script>
<script type ="text/javascript" src="/library/js/assistform.js"></script>
<link rel="stylesheet" href="/assist.css" type="text/css">
</head>

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php


//ASSIST_HELPER::arrPrint($_REQUEST);

$modref = $_REQUEST['modref'];
$admin_modules = array("S", "TK", "reports", "module_management","dashboard_management","user_profile","USER_MANUALS","userman");

if(in_array($modref,$admin_modules)) {
$row = array('modyn'=>"Y");
} else {

$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$modref."'";
$row = $my_assist->mysql_fetch_one($sql);
}
if($row['modyn']!="Y") {
	if(!isset($_REQUEST['act'])) {

	echo "
	<script type=text/javascript>
var url = '/title_login.php?m=link&mr=".$modref."';
console.log(url);
parent.header.location.href = url;
	</script>
	";
	} else {
	ASSIST_HELPER::displayResult(array("error","The module you have tried to access is no longer available.  Please select another module."));

	}
	die();
} else {
	ASSIST_HELPER::displayResult(array("info","Redirecting..."));
}

$modloc = $_REQUEST['modloc'];

				$_SESSION['modref'] = strtoupper($modref);
				$_SESSION['modtext'] = isset($_REQUEST['modtext'])?$_REQUEST['modtext']:"";
				$_SESSION['modlocation'] = $modloc;
				$dbref = "assist_".$cmpcode."_".strtolower($modref);
				$_SESSION['dbref'] = $dbref;


switch($_REQUEST['act']) {
case "goto_mod":
	switch($modloc) {
	case "SDBP5":
	case "SDBP5B":
	case "SDBP5PM":
	case "SDBP6":
	case "PM4":
	case "PM6":
	case "IKPI1":
	case "IKPI2":
	case "ACTION":
	case "ACTION0":
	case "PDP":
	case "JOBS":
			case "CAPS":
			case "CAPS1B":
	case "SLA":
	case "RGSTR":
	case "RGSTR2":
	case "QUERY":
	case "CASE":
	case "COMPL":
	case "RAPS":
	case "SCORECARD":
	case "CNTRCT":
		switch($_REQUEST['action']) {
		case "view":
			$main_url = $modloc."/".str_replace("|","&",$_REQUEST['link']);
			break;
		}
		break;
	case "MDOC":
	case "DOC":
	case "DOC0":
		switch($_REQUEST['action']) {
		case "view":
			$main_url = $modloc."/view.php?c=".$_REQUEST['id'];
			break;
		case "admin":
			$main_url = $modloc."/admin_maintain.php";
			break;
		case "upload":
			$main_url = $modloc."/admin_upload.php";
			break;
		}
		break;
	case "DSHP":
		switch($_REQUEST['action']) {
		case "manage":
			$main_url = $modloc."/admin_edit.php";
			break;
		case "update":
			$main_url = $modloc."/admin_update_draw1.php?ref=".$_REQUEST['dashid'];
			break;
		case "view":	//view_dashboard.php?ref=1&timeid=1
			$sql = "SELECT * FROM ".$dbref."_dashboard WHERE dashid = ".$_REQUEST['dashid'];
			$dash = $my_assist->mysql_fetch_one($sql);
			$df = $dash['dashfrequency'];
			$sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ORDER BY sort";
			$t = $my_assist->mysql_fetch_all($sql);
			//if(in_array($df,array(4,5,6,7,8))) {
				switch($df) {
				case 1:
							case 4:
								$freq = 1;
								break;    //monthly
							case 5:
								$freq = 2;
								break;    //2-monthly
							case 6:
								$freq = 3;
								break;    //3-monthly
							case 7:
								$freq = 4;
								break;    //4-monthly
							case 8:
								$freq = 6;
								break;    //6-monthly
							case 9:
								$freq = 12;
								break;    //6-monthly
				}
				$time = array();
				$tc = 0;
				foreach($t as $a) {
					$tc++;
					if($tc%$freq==0) {
						$time[$tc] = $a;
					}
				}
			//} else {
			//}
			echo "
			<div id=dlg_dshp_view>
			<h1>".$_REQUEST['modtext']." >> View</h1>
			<h2 class=u>".$dash['dashname']."</h2>
			<form method=get action=/".$modloc."/view_dashboard.php>
			<input type=hidden name=ref value=".$dash['dashid']." />
			<table width=400>
				<tr>
					<th class=left>";
					echo "Period ending:</th>
					<td>";
				if($df==9) {
					$t = $time[0];
					echo "<input type=hidden name=timeid value=".$t['id']." />".date("d F Y",$t['eval']);
				} else {
					echo "<select name=timeid id=t>";
					foreach($time as $i => $t) {
						echo "<option ".($i==1 ? "selected" : "")." value=".$t['id'].">".date("d F Y",$t['eval'])."</option>";
					}
					echo "</select>";
				}
			if(!in_array($df,array(4,5,6,7,8,9))) {
					echo "</td></tr>
					<tr>
						<th class=left>Date:</th>
						<td><input type=text name=timedate class=jdate2012 />";
			}
				echo "</td>
				</tr>
				<tr>
					<th></th>
					<td><input type=submit value=Go class=isubmit /></td>
				</tr>
			</table>
			</form>
			</div>
			<script type=text/javascript>
			$(document).ready(function() {
			var time = new Array();
			";
			foreach($time as $t) {
				echo chr(10)." time[".$t['id']."] = new Array(); time[".$t['id']."]['min'] = new Date(".date("Y",$t['sval']).",".(date("m",$t['sval'])-1).",".date("d",$t['sval'])."); time[".$t['id']."]['max'] = new Date(".date("Y",$t['eval']).",".(date("m",$t['eval'])-1).",".date("d",$t['eval']).");";
			}
			echo "
				$('#dlg_dshp_view').dialog({
					modal: true,
					width: 500,
					height: 300,
					close: function(event, ui) { parent.header.location.href = '/title_login.php?m=action_dashboard'; }
				});
				$('.jdate2012').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:false,
                    changeYear:false
				});
				";
			if($df==1) {
				echo "
				$('#t').change(function() {
					var i = $(this).val();
					var minD = time[i]['min'];
					var maxD = time[i]['max'];
					$('.jdate2012').datepicker(\"option\",'minDate',minD);
					$('.jdate2012').datepicker(\"option\",'maxDate',maxD);
					var mydate = ''+(minD.getDate()<10 ? '0' : '')+minD.getDate();
					var mydate = mydate+'-'+((minD.getMonth()+1)<10 ? '0' : '')+(minD.getMonth()+1);
					var mydate = mydate+'-'+minD.getFullYear();
					$('.jdate2012').val(mydate);
				});
				$('#t').trigger('change');
				";
			}
			echo "
			});
			</script>
			";
		}
		break;
	}
	$title_url = !isset($title_url) || $title_url=="" ? "/title_login.php?src=main_login.php&act=no_route&m=".$modref."" : $title_url;
	break;
case "open_mod":
	if($modref == "USER_MANUALS") {
		$title_url = "/title_login.php?act=change&m=userman";
	} elseif($modloc=="EXTERNAL") {
		$ext_url = $modref."??";
		switch($modref) {
			case "DTM":
				$ext_url = "http://www.dtmsuite.com";
				break;
			case "SAGE":
				$ext_url = "https://accounting.sageone.co.za/Landing/Default.aspx";
				break;
		}
		$title_url = "/title_login.php?m=action_dashboard";
	} else {
		$title_url = "/title_login.php?act=change&m=".$modref;
	}
	break;
}

//echo $main_url;

	echo "
		<script type=text/javascript>
			".(isset($title_url) && $title_url != "" ? "parent.header.location.href = '".$title_url."';" : "")."
			".(isset($main_url) && $main_url != "" ? "parent.main.location.href = '/".$main_url."';" : "")."
			".(isset($ext_url) && $ext_url != "" ? "var AssistPartnerWin = window.open('".$ext_url."'); history.back();" : "")."
		</script>";

?>
</body>
</html>