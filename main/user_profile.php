<?php
require_once 'user_profile_inc.php';
/** $split_modules -> from inc file -> got from ASSIST class */
//$split_modules = array("SDBP6","IKPI2");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
ASSIST_HELPER::displayResult($result);
?>
<style type=text/css>
h2 { margin-top: 20px; }
	table tbody th { text-align: left; }
	table.tbl_section4 {
		border: 1px solid #ABABAB;
	}
	table.tbl_section4 td {
		border: 0px solid #ffffff;
		padding: 13px;
		font-weight: bold;
	}
	hr {
		border-color: #000099;
		width: 75%;
		margin-top: 25px;
	}
</style>


<!-- SECTION 4: MAIN_LOGIN option -->
<?php
if(!isset($profile[4])) {
	$profile[4] = array(
			'field1'=>"column"
	);
}
?>
<h2>Home >> Front Page Layout</h2>
<form name="section4" id="s4" action="user_profile_process.php" method="post">
	<input type=hidden name=section value=4 />
<p class="i">Please select the Front Page layout option you would like to use:

<table class="tbl_section4">
	<tr>
		<td><input type="radio" name="field1" value="grid" id="field1_grid" <?php if($profile[4]['field1']=="grid") { echo "checked=checked"; } ?> /></td>
		<td><label for="field1_grid" >Grid</label><br /><img src="/pics/profile_section4_grid.png" for="field1_grid" /></td>
		<td>&nbsp;&nbsp;&nbsp;</td>
		<td><input type="radio" name="field1" value="column" id="field1_col" <?php if($profile[4]['field1']!="grid") { echo "checked=checked"; } ?> /></td>
		<td><label for="field1_col" >Column</label><br /><img src="/pics/profile_section4_column.png" for="field1_col" /></td>
	</tr>
		<tr>
			<td colspan="5" class="center"><input type="button" value="Save Changes"  class=isubmit /></td>
		</tr>
</table>


	<script type=text/javascript>
		$(function() {
			$("form[name=section4] img").click(function() {
				var f = $(this).attr("for");
				$("#"+f).trigger("click");
			}).css("cursor","pointer");
			$("form[name=section4] .isubmit").click(function() {
				var form = "form[name=section4]";
				var err = false;
				if(!err) {
					$(form).submit();
				} else {
					alert("Please complete the required fields as highlighted in red.");
				}
			});
		});
	</script>

</form>


<hr  />
<!-- SECTION 1: ACTION LIST -->




<h2>Home >> Action List</h2>
<?php
/************************
This section allows the user to customise the home page / main page action list.
Section ID: 1
************************/
if(count($mods)>0) {

	if(!isset($profile[1]['field1'])) { $profile[1]['field1']=""; }
	if(!isset($profile[1]['field2'])) { $profile[1]['field2']=15; }
	if(!isset($profile[1]['field3'])) { $profile[1]['field3']="dead_asc"; }
	if(!isset($profile[1]['field4']) || !is_numeric($profile[1]['field4'])) { $profile[1]['field4']=7; }

		$lines = array();
		foreach($mods as $m) {
			if(in_array($m['modlocation'],$split_modules)) {
				$class = $m['modlocation']."_ASSIST";
				$moduleObject = new $class();
				$new_lines = $moduleObject->getActionDashboardUserProfileOptions($m['modref'],$m['modtext']);
				if($profile[1]['field1']==$m['modref'] && count($new_lines)>0 && strpos($profile[1]['field1'],"_")===false) {
					$keys = array_keys($new_lines);
					$new_lines[$keys[0]]['active']=true;
				}
				foreach($new_lines as $line) {
					if($profile[1]['field1']==$line['value']) {
						$line['active'] = true;
					}
					$lines[] = $line;
				}
			} else {
				$active = false;
				if($profile[1]['field1']==$m['modref'] || $profile[1]['field1']==$m['modref']."_0") {
					$active = true;
				}
				$lines[] = array(
							'value'=>$m['modref']."_0",
							'text'=>$m['modtext'],
							'active'=>$active,
				);
			}
		}
?>
<form name=section1 id=s1 action=user_profile_process.php method=post>
<input type=hidden name=section value=1 />
<table><tbody>
	<tr>
		<th>Default module:</th>
		<td><?php
			if(count($lines)>0) {
		?><select name=field1 id=f1><?php
				foreach($lines as $l) {
					$selected = "";
					if($l['active']===true) {
						$selected = "selected";
					}
					echo "<option $selected value=".$l['value'].">".$l['text']."</option>";
				}
		?></select>
		<?php
			} else {
				echo "No options available to select.";
			}
		?></td>
	</tr>
	<tr>
		<th>Number of Actions to display:</th>
		<td><select name=field2 id=f2><?php
			for($i=5;$i<=30;$i+=5) {
				if($i==$profile[1]['field2']) { $selected = "selected"; } else { $selected = ""; }
				echo "<option $selected value=$i>$i</option>";
			}
		?></select></td>
	</tr>
	<tr>
		<th>Sort order:</th>
		<td><select name=field3 id=f3>
		<?php
		foreach($section1_field3 as $key => $value) {
			if($profile[1]['field3']==$key) { $selected = "selected"; } else { $selected = ""; }
			echo "<option $selected value=".$key.">".$value."</option>";
		}
		?>
			<!-- <option value=add_asc>Date Added: Oldest to Newest</option>
			<option value=add_desc>Date Added: Newest to Oldest</option> -->
		</select></td>
	</tr>
	<tr>
		<th>Days to display:</th>
		<td><select name=field4 id=f4><?php
			for($i=1;$i<=30;$i++) {
				if($i==$profile[1]['field4']) { $selected = "selected"; } else { $selected = ""; }
				echo "<option $selected value=$i>$i</option>";
			}
			for($i=2;$i<=4;$i++) {
				$j = $i*30;
				echo "<option ".($j==$profile[1]['field4'] ? "selected" : "")." value=$j >$j</option>";
			}
		?></select></td>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td><input type=button value="Save Changes" class=isubmit /> <input type=reset /></td>
	</tr>
</tbody></table>
<script type=text/javascript>
	$(function() {
		$("form[name=section1] .isubmit").click(function() {
			var form = "form[name=section1]";
			var f1 = $(form+" #f1").val();
			var f2 = $(form+" #f2").val();
			var f3 = $(form+" #f3").val();
			var err = false;
			if(f1.length==0) {
				$(form+" #f1").addClass("required");
				err = true;
			}
			if(f2.length==0) {
				$(form+" #f2").addClass("required");
				err = true;
			}
			if(f3.length==0) {
				$(form+" #f3").addClass("required");
				err = true;
			}
			if(!err) {
				$(form).submit();
			} else {
				alert("Please complete the required fields as highlighted in red.");
			}
		});
	});
</script>
</form>
<?php
} else {
	echo "<P>You do not have access to any of the Assist action-based modules and therefore cannot set your Action List profile at this time.</p>";
}
?>

<!-- END SECTION 1 -->



<!-- SECTION 3: DOCUMENT MODULES -->
<?php
/************************
This section allows the user to customise the home page / main page documents div.
Section ID: 3
************************/
if(count($docs)>0) {

if(!isset($profile[3]['field5']) || !is_array($profile[3]['field5'])) {
	$profile[3]['field5'] = array();
}

?>

	<hr  />
<h2>Home >> Documents</h2>
<form name=section3 id=s3 action=user_profile_process.php method=post>
<input type=hidden name=section value=3 />
<table><thead>
	<tr>
		<th>Module</th>
		<th>Default<br />Module</th>
		<th>Display<br />Folders?</th>
	</tr>
	</thead><tbody>
<?php
foreach($docs as $m) {
	echo "<tr>
		<td class=b>".$m['modtext']."</td>
		<td class=center><input type=radio name=field1 value='".$m['modref']."' ".($profile[3]['field1']==$m['modref'] ? "checked" : "")." /></td>
		<td class=center><select name=field5[".$m['modref']."]><option ".((!isset($profile[3]['field5'][$m['modref']]) || $profile[3]['field5'][$m['modref']]==1) ? "selected" : "")." value=1>Yes</option><option ".((isset($profile[3]['field5'][$m['modref']]) && $profile[3]['field5'][$m['modref']]==0) ? "selected" : "")." value=0>No</option></select></td>
	</tr>";
}
?>
	<tr>
		<td colspan=3><input type=button value="Save Changes" class=isubmit /> <input type=reset /></td>
	</tr>
</tbody></table>
<script type=text/javascript>
	$(function() {
		$("form[name=section3] .isubmit").click(function() {
			var form = "form[name=section3]";
			$(form).submit();
		});
	});
</script>
</form>
<?php
}
?>
<!-- END SECTION 3 -->


<!-- SECTION 2: SHORTCUTS -->

<hr  />
<style type=text/css>
	.sortablefolders { list-style-type: none; margin: 0; padding: 0; list-style-image: url(); border: 1px solid #000099; height: 130px; padding-left: 5px;}
	.sortablefolders li {  margin: 3px 3px 3px 0; padding: 0px; float: left; width: 100px; height: 100px;  text-align: center; vertical-align: middle; font-size: 7pt; }
</style>
<h2>Home >> Shortcuts</h2>
<?php
/************************
This section allows the user to customise their home page / main page shortcut list.
Section ID: 2
************************/
if(count($menu)>0) {
?>
<ul>
<li>To add a shortcut to the front page, select a module in the "Available Shortcuts" box and drag it to the "My Shortcuts" box.  </li>
<li>Once you are done, click the "Save Changes" button to save your shortcuts.  </li>
<li>You can remove a shortcut by reversing the process.  </li>
<li>You can also choose the display order of your shortcuts by rearranging them in the "My Shortcuts" box.</li>
<li>The width of the "My Shortcuts" box mirrors the width of the "My Shortcuts" box on your front page.<br />The width changes depending on the size of your internet browser window.</li>
<li>If you tick the "Show all shortcuts" box then the height of your "My Shortcuts" box on the front page will change depending <br />on the number of shortcuts you have.  If you leave it unticked, then the box will only show the first row of shortcuts.</li>
</ul>
<div id=div_left style="background-color: #ffffff; ">
<?php
$echo = "";
echo "<h3>My Shortcuts</h3>";
/*$menu['USRMN'] = array(
						'modtext'=>"Manuals",
						'modref'=>"USRMN",
						'modlocation'=>"man",
					);*/
$myshorts = array();
foreach($profile[2]['field5'] as $p) {
	if(isset($menu[$p])) {
		$myshorts[$p] = array(
			'modref' => $p,
			'modtext'=> $menu[$p]['modtext'],
			'modlocation'=>$menu[$p]['modlocation']
		);
	}
}
//ASSIST_HELPER::arrPrint($shortcut_details);
$echo.= "<form name=section2 method=post action=user_profile_process.php>
	<input type=hidden name=section value=2 />";
$echo.= "<ul class=sortablefolders id=my_short>";
	foreach($myshorts as $m) {
if($m['modlocation']=="RGSTR" && ( strtoupper(substr($m['modtext'],0,4))!="RISK") ) {
	$logo_ref = "RGSTR_ALT";
} elseif($m['modlocation']=="SDBP5" && !is_numeric(substr($m['modref'],-1))) {
	$logo_ref = "IND_KPI";
} elseif($m['modlocation']=="EXTERNAL") {
	$logo_ref = $m['modref'];
} else {
	$logo_ref = $m['modlocation'];
}
		if(isset($shortcut_details['logos'][$logo_ref])) {
			$m['icon'] = $shortcut_details['path'].$shortcut_details['logos'][$logo_ref];
		} else {
			$m['icon'] = $shortcut_details['path'].$shortcut_details['default'];
		}
		$echo.= "<li id=".$m['modref']." style=\"width: 85px;height: 120px;\">";
			$echo.= "<img src=".$m['icon']." /><br />".$m['modtext']."<input type=hidden value=".$m['modref']." name=modref[] />";
		$echo.= "</li>";
	}
$echo.= "</ul>

<p class='center i'>
<span><input type=checkbox value=MIN name=field1 ".(!isset($profile[2]['field1']) || $profile[2]['field1']=="ALL" ? "" : "checked")." /> Limit Shortcuts box to only display 1 row of shortcuts at a time<br />(Only applicable to the \"Grid\" Front Page layout.)</span></p>
<p class=center><input type=submit value=\"Save Changes\" class=isubmit /></p>
</form>
<h3>Available Shortcuts</h3>
	<ul class=sortablefolders id=avail_short>";
//User manuals hard shortcut

	foreach($menu as $m) {
if($m['modlocation']=="RGSTR" && strtoupper(substr($m['modtext'],0,4))!="RISK") {
	$logo_ref = "RGSTR_ALT";
} elseif($m['modlocation']=="SDBP5" && !is_numeric(substr($m['modref'],-1))) {
	$logo_ref = "IND_KPI";
} elseif($m['modlocation']=="EXTERNAL") {
	$logo_ref = $m['modref'];
} else {
	$logo_ref = $m['modlocation'];
}

//echo " ".strtoupper(substr($ct['modtext'],0,4));
		if(!isset($myshorts[$m['modref']])) {
			if(isset($shortcut_details['logos'][$logo_ref])) {
				$m['icon'] = $shortcut_details['path'].$shortcut_details['logos'][$logo_ref];
			} else {
				$m['icon'] = $shortcut_details['path'].$shortcut_details['default'];
			}
			$echo.= "<li id=".$m['modref']." style=\"width: 85px;height: 120px;\">";
				$echo.= "<img src=".$m['icon']." /><br />".$m['modtext']."<input type=hidden value=".$m['modref']." name=modref[] />";
			$echo.= "</li>";
		}
	}
$echo.= "</ul>";
echo $echo;
?>
</div>
<script type=text/javascript>
$(function() {
	//alert($("#avail_short li").length);
	/*$( ".acc_display" ).accordion({
		collapsible: true,
		autoHeight: false
	});*/
	checkSize();
	$(window).resize(function() {
		checkSize();
	});
	$( "#my_short, #avail_short" ).sortable({connectWith: ".sortablefolders", placeholder: 'ui-state-highlight' }).mouseup(function() { checkSize(); });
	//$( ".sortablefolders" ).disableSelection();
	function checkSize() {
//		$(function() {
			var win = getWindowSize();
			var w = win['width'];
			w *=0.35;
			$("#div_left").css("width",w);
			var x = Math.floor(w/85);
			var l = $("#avail_short li").length;
			var h = 135;
			if(l>x) {
				h = Math.ceil(l/x) * 120; //alert(h);
			}
			$("#avail_short").css("height",h);
			h = 135;
			l = $("#my_short li").length;
			if(l>x) {
				h = Math.ceil(l/x) * 135; //alert(h);
			}
			$("#my_short").css("height",h);
		}
	//}
});
</script>
</form>
<?php
} else {
	echo "<P>You do not have access to any modules so you can\'t set up your shortcuts yet.</p>";
}
?>


<!-- END SECTION 2 -->


<hr  />


<?php
$log_sql = "SELECT l.date, l.tkname, l.transaction
			FROM assist_".$cmpcode."_timekeep_profile_log l
			WHERE l.active = true AND l.tkid = '".$tkid."'
			ORDER BY l.id DESC";
displayAuditLog2($log_sql,array('date'=>"date",'user'=>"tkname",'action'=>"transaction"));



?>
</body></html>