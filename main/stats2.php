<?php
//$split_modules = array("SDBP6","IKPI2");
echo "
<table class=noborder id=stats width='100%'>
	<tr>
		<th>Module</th>
		<th>Overdue</th>
		<th>Due<br />Today</th>
		<th>".$next_due_head."</th>
	</tr>";
$colwidth = 80;
if(count($actionmenu) > 0) {
	$lines = array();
	foreach($actionmenu as $am) {  //arrPrint($am);
		$count = array('past' => 0,
			'present' => 0,
			'future' => 0);
		$file_name = $am['modlocation'] . "/login_stats.php";
		$modref = strtolower($am['modref']);
		$_SESSION['modref'] = $modref;
		$modloc = $am['modlocation'];
		$_SESSION['modlocation'] = $modloc;
		if (in_array($modloc, $split_modules)) {
			$sdb = new ASSIST_DB();
			include($file_name);    //sets $line[sdbip_title] = $count
			if (count($line) == 0) {
				/*$line[$am['modtext']] = array(
					'count' => array(
						'past' => "N/A",
						'present' => "N/A",
						'future' => "N/A")
				, 'object_id' => 0
				,'modref'=>$modref,'modloc'=>$modloc);*/
			} else {
				foreach($line as $l => $ln) {
					$line[$l]['modref'] = $modref;
					$line[$l]['modloc'] = $modloc;
				}
			}
		} else {
			if (file_exists($file_name)) {
				$sdb = new ASSIST_DB();
				include($file_name);
			} else {
				$count = array('past' => "N/A",
					'present' => "N/A",
					'future' => "N/A");
			}
			$line = array(
				$am['modtext'] => array('count' => $count, 'object_id' => 0,'modref'=>$modref,'modloc'=>$modloc)
			);
		}
		if(strtoupper($modref) == $_SESSION['MAIN']['amenu']) {
			$graph = $count;
		}
		$lines = $lines+$line;
	}
		$style = array('past'    => "<span style=\"font-weight: bold; color: #CC0001; border: 1px solid #CC0001;\">",
					   'present' => "<span style=\"font-weight: bold; color: #009900; border: 1px solid #009900;\">",
					   'future'  => "<span style=\"font-weight: bold; color: #FE9900; border: 1px solid #FE9900;\">");
	if(count($lines)>0) {
		ksort($lines);

		foreach($lines as $title => $details) {
			$count = $details['count'];
			$mod_obj = $details['object_id'];
			$modref = $details['modref'];
			$modloc = $details['modloc'];


			if(in_array($modloc, $split_modules) && strtoupper($_SESSION['MAIN']['amenu']) == strtoupper($modref)) {
				if(strpos($_SESSION['MAIN']['modtext'], $title) === false && ($_SESSION['MAIN']['mod_object'] == $details['object_id'] || $_SESSION['MAIN']['mod_object'] == 0)) {
					$_SESSION['MAIN']['modtext'] .= ": ".$title;

					if($_SESSION['MAIN']['mod_object'] == 0) {
						$_SESSION['MAIN']['mod_object'] = $details['object_id'];
					}
				}
			}

			echo "
				<tr>";
			echo "<td class=b><a modref=".$modref." modloc=".$modloc." mod_obj='".$mod_obj."' act=open_mod style=\" cursor: pointer;\">".$title."</a>&nbsp;</td>";
			foreach($count as $key => $c) {
				echo "<td width=$colwidth class=\"center\">".(is_numeric($c) && $c > 0 ? $style[$key] : "")."&nbsp;".$c."&nbsp;</span></td>";
			}
			echo "
				</tr>";
		}
	} else {
		echo "
				<tr><td class=i colspan='4'>No Action Modules available to display.</td></tr>";
		$menu_mod['amenu'] = "";
		$_SESSION['MAIN']['modref'] = "";
		$_SESSION['MAIN']['modlocation'] = "";
	}
}

?>
</table>
<style type=text/css>
	.orange {
		color: #fe9900;
	}
</style>