<?php
require_once 'user_profile_inc.php';
?>
<p>Processing...</p>
<?php
$var = $_REQUEST;
$section = $var['section'];
//get values
	$f = array();
	for($i=1;$i<=5;$i++) {
		//echo $i;
		$f[$i] = isset($_REQUEST['field'.$i]) ? $_REQUEST['field'.$i] : "";
		if($i==5 && !isset($_REQUEST['field5'])) { $f[5] = array(); }
	}
	$f[5] = serialize($f[5]);


switch($var['section']) {
case 1:
	if(checkIntRef($profile[1]['id'])) {	//existing profile to be updated
		$old = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[1]['id']);
		$sql = "UPDATE assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '".$f[2]."', field3 = '".$f[3]."', field4 = '".$f[4]."' WHERE id = ".$profile[1]['id'];
		$mnr = $db->db_update($sql);
		$new = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[1]['id']);
		if($mnr > 0 || serialize($old)!=serialize($new)) {
				$txt = array();
				foreach($f as $k => $v) {
					$t = $field_headings[1]['field'.$k].": ";
					switch($k) {
						case 1: $t.=$mods[$f[$k]]['modtext']; break;
						case 3: $t.=$section1_field3[$f[$k]]; break;
						default: $t.=$f[$k]; break;
					}
					$txt[] = $t;
				}
					$c = array(
						'fld'=>serialize(array("field1","field2","field3","field4")),
						'old'=>serialize(array($profile[1]['field1'],$profile[1]['field2'],$profile[1]['field3'],$profile[1]['field4'])),
						'new'=>serialize($f),
						'txt'=>code("Edited User Profile for Action List as follows:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION1','".$profile[1]['id']."','E','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			foreach($f as $k => $v) {
				$_SESSION['USER_PROFILE'][1]['field'.$k] = $f[$k];
			}
			$result = array('ok','Update profile updated.');
		} else {
			$result = array('info','No changes found to be saved.');
		}
	} else {	//new profile
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '".$f[2]."', field3 = '".$f[3]."', field4 = '".$f[4]."', field5 = '', section = 1, active = 1, tkid = '$tkid'";
		$mnr = db_insert($sql);
		if($mnr > 0) {
				$txt = array();
				foreach($f as $k => $v) {
					$t = $field_headings[1]['field'.$k].": ";
					switch($k) {
						case 1: $t.=$mods[$f[$k]]['modtext']; break;
						case 3: $t.=$section1_field3[$f[$k]]; break;
						default: $t.=$f[$k]; break;
					}
					$txt[] = $t;
				}
					$c = array(
						'fld'=>serialize(array("field1","field2","field3","field4")),
						'old'=>serialize(array($profile[1]['field1'],$profile[1]['field2'],$profile[1]['field3'],$profile[1]['field4'])),
						'new'=>serialize($f),
						'txt'=>code("Created User Profile for Action List with the following settings:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION1','".$mnr."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			$_SESSION['USER_PROFILE'][1]['field1'] = $f[1];
			$_SESSION['USER_PROFILE'][1]['field2'] = $f[2];
			$_SESSION['USER_PROFILE'][1]['field3'] = $f[3];
			$_SESSION['USER_PROFILE'][1]['id'] = $mnr;
			$result = array('ok','User profile created.');
		} else {
			$result = array('error','An error occurred while trying to create your user profile.  Please try again.');
		}
	}
	echo "<script type=text/javascript>document.location.href = 'user_profile.php?r[]=".$result[0]."&r[]=".$result[1]."';</script>";
	break;
case 2:
	$f[1] = isset($_REQUEST['field1']) ? $_REQUEST['field1'] : "ALL";
	$f[5] = isset($_REQUEST['modref']) ? $_REQUEST['modref'] : array();
	if(checkIntRef($profile[2]['id'])) {	//existing profile to be updated
		$old = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[2]['id']);
		$sql = "UPDATE assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field5 = '".serialize($f[5])."' WHERE id = ".$profile[2]['id'];
		$mnr = $db->db_update($sql);
		$new = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[2]['id']);
		if($mnr > 0 || serialize($old)!=serialize($new)) {
				$txt = array();
				foreach($f[5] as $m) {
					$txt[] = $menu[$m]['modtext'];
				}
				$t = implode(", ",$txt);
				$txt = array();
				$txt[] = "Shortcuts: ".$t;
				$txt[] = "Show all shortcuts: ".($f[1]=="ALL" ? "Yes" : "No");
					$c = array(
						'fld'=>serialize(array("field1","field5")),
						'old'=>serialize(array($profile[2]['field1'],$profile[2]['field5'])),
						'new'=>serialize($f),
						'txt'=>code("Updated User Profile for Shortcuts to the following settings:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION2','".$mnr."','E','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			$_SESSION['USER_PROFILE'][2]['field5'] = $f[5];
			$_SESSION['USER_PROFILE'][2]['field1'] = $f[1];
			$result = array('ok','Update profile updated.');
		} else {
			$result = array('info','No changes found to be saved.');
		}
	} else {
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '', field3 = '', field4 = '', field5 = '".serialize($f[5])."', section = 2, active = 1, tkid = '$tkid'";
		$mnr = db_insert($sql);
		if($mnr > 0) {
				$txt = array();
				foreach($f[5] as $m) {
					$txt[] = $menu[$m]['modtext'];
				}
				$t = implode(", ",$txt);
				$txt = array();
				$txt[] = "Shortcuts: ".$t;
				$txt[] = "Show all shortcuts: ".($f[1]=="ALL" ? "Yes" : "No");
					$c = array(
						'fld'=>serialize(array("field1","field5")),
						'old'=>serialize(array($profile[2]['field1'],$profile[2]['field5'])),
						'new'=>serialize($f),
						'txt'=>code("Created User Profile for Shortcuts with the following settings:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION2','".$mnr."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			$_SESSION['USER_PROFILE'][2]['field5'] = $f[5];
			$_SESSION['USER_PROFILE'][2]['field1'] = $f[1];
			$_SESSION['USER_PROFILE'][2]['id'] = $mnr;
			$result = array('ok','Shortcuts created.');
		} else {
			$result = array('error','An error occurred while trying to create your shortcuts.  Please try again.');
		}
	}
	echo "<script type=text/javascript>document.location.href = 'user_profile.php?r[]=".$result[0]."&r[]=".$result[1]."';</script>";
	break;
case 3:
	if(checkIntRef($profile[$section]['id'])) {	//existing profile to be updated
		$old = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[$section]['id']);
		$sql = "UPDATE assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '".$f[2]."', field3 = '".$f[3]."', field4 = '".$f[4]."', field5 = '".$f[5]."' WHERE id = ".$profile[$section]['id'];
		$mnr = $db->db_update($sql);
		$new = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[$section]['id']);
		if($mnr > 0 || serialize($old)!=serialize($new)) {
				$txt = array();
				foreach($f as $k => $v) {
					if(isset($field_headings[$section]['field'.$k])) {
						$t = $field_headings[$section]['field'.$k].": ";
						switch($k) {
							case 1: $t.=$docs[$f[$k]]['modtext']; break;
							case 5:
								foreach($_REQUEST['field5'] as $mr => $mv) {
									$t.="<br />&nbsp;&nbsp;&nbsp;-&nbsp;".$docs[$mr]['modtext'].": ".($mv==0 ? "No" : "Yes");
								}
								break;
							default: $t.=$f[$k]; break;
						}
						$txt[] = $t;
					}
				}
					$c = array(
						'fld'=>serialize(array("field1","field2","field3","field4","field5")),
						'old'=>serialize(array($profile[$section]['field1'],$profile[$section]['field2'],$profile[$section]['field3'],$profile[$section]['field4'],$profile[$section]['field5'])),
						'new'=>serialize($f),
						'txt'=>code("Edited User Profile for Documents as follows:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION1','".$profile[$section]['id']."','E','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			foreach($f as $k => $v) {
				$_SESSION['USER_PROFILE'][$section]['field'.$k] = $f[$k];
			}
			$result = array('ok','User profile updated for Documents.');
		} else {
			$result = array('info','No changes found to be saved.');
		}
	} else {	//new profile
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '".$f[2]."', field3 = '".$f[3]."', field4 = '".$f[4]."', field5 = '".$f[5]."', section = $section, active = 1, tkid = '$tkid'";
		$mnr = db_insert($sql); echo ":".$mnr.":".$sql;
		if($mnr > 0) {
				$txt = array();
				foreach($f as $k => $v) {
					if(isset($field_headings[$section]['field'.$k])) {
						$t = $field_headings[$section]['field'.$k].": ";
						switch($k) {
							case 1: $t.=$docs[$f[$k]]['modtext']; break;
							case 5:
								foreach($_REQUEST['field5'] as $mr => $mv) {
									$t.="<br />&nbsp;&nbsp;&nbsp;-&nbsp;".$docs[$mr]['modtext'].": ".($mv==0 ? "No" : "Yes");
								}
								break;
							default: $t.=$f[$k]; break;
						}
						$txt[] = $t;
					}
				}
					$c = array(
						'fld'=>serialize(array("field1","field2","field3","field4","field5")),
						'old'=>serialize(array($profile[$section]['field1'],$profile[$section]['field2'],$profile[$section]['field3'],$profile[$section]['field4'],$profile[$section]['field5'])),
						'new'=>serialize($f),
						'txt'=>code("Created User Profile for Documents with the following settings:<br />&nbsp;&nbsp;&nbsp;".implode("<br />&nbsp;&nbsp;&nbsp;",$txt)),
					);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION".$section."','".$mnr."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
					db_insert($sql2);
			$_SESSION['USER_PROFILE'][$section]['field1'] = $f[1];
			$_SESSION['USER_PROFILE'][$section]['field2'] = $f[2];
			$_SESSION['USER_PROFILE'][$section]['field3'] = $f[3];
			$_SESSION['USER_PROFILE'][$section]['field4'] = $f[4];
			$_SESSION['USER_PROFILE'][$section]['field5'] = unserialize($f[5]);
			$_SESSION['USER_PROFILE'][$section]['id'] = $mnr;
			$result = array('ok','User profile created for Documents.');
		} else {
			$result = array('error','An error occurred while trying to create your user profile for Documents.  Please try again.');
		}
	}
	echo "<script type=text/javascript>document.location.href = 'user_profile.php?r[]=".$result[0]."&r[]=".$result[1]."';</script>";
	break;
	case 4:
		$f[1] = isset($_REQUEST['field1']) ? $_REQUEST['field1'] : "column";
		if(checkIntRef($profile[$section]['id'])) {	//existing profile to be updated
			$old = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[$section]['id']);
			$sql = "UPDATE assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."' WHERE id = ".$profile[$section]['id'];
			$mnr = $db->db_update($sql);
			$new = $db->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE id = ".$profile[$section]['id']);
			if($mnr > 0 || serialize($old)!=serialize($new)) {
				$c = array(
					'fld'=>serialize(array("field1")),
					'old'=>serialize(array($profile[$section]['field1'])),
					'new'=>serialize($f),
					'txt'=>code("Updated User Profile for Front Page to ".ucfirst($f[1])),
				);
				$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION".$section."','".$mnr."','E','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
				$db->db_insert($sql2);
				$_SESSION['USER_PROFILE'][$section]['field1'] = $f[1];
				$result = array('ok','Profile updated.');
			} else {
				$result = array('info','No changes found to be saved.');
			}
		} else {
			$sql = "INSERT INTO assist_".$cmpcode."_timekeep_profile SET field1 = '".$f[1]."', field2 = '', field3 = '', field4 = '', field5 = '', section = $section, active = 1, tkid = '$tkid'";
			$mnr = $db->db_insert($sql);
			if($mnr > 0) {
				$c = array(
					'fld'=>serialize(array("field1")),
					'old'=>serialize(array($profile[$section]['field1'])),
					'new'=>serialize($f),
					'txt'=>code("Created User Profile for Front Page: ".ucfirst($f[1])),
				);
				$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_profile_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'$tkid','".$_SESSION['tkn']."','SECTION".$section."','".$mnr."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql)."')";
				$db->db_insert($sql2);
				$_SESSION['USER_PROFILE'][$section]['field1'] = $f[1];
				$_SESSION['USER_PROFILE'][$section]['id'] = $mnr;
				$result = array('ok','Front Page setting saved.');
			} else {
				$result = array('error','An error occurred while trying to create your shortcuts.  Please try again.');
			}
		}
		echo "<script type=text/javascript>document.location.href = 'user_profile.php?r[]=".$result[0]."&r[]=".$result[1]."';</script>";
		break;

default:
	echo "<p><span class=required>Error:</span> An error occurred trying to determine your selected action.  Please go back and try again.</p>";
	displayGoBack();
	die();
}
?>