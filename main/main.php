<?php
/**
 * REQUIRED VARIABLES FROM CALLING PAGE (main_login or main_login_v1)
 * @var string $cmpcode - company code currently logged in
 * @var array $menu_mod - module info on current FPD module
 * @var string $tkid - user id currently logged in
 * @var array $action_modules - list of standard action modules that can show on the FPD
 * @var ASSIST_MODULE_HELPER $my_assist
 */
$action_profile = $_SESSION['USER_PROFILE'][1];

//$mod = isset($_REQUEST['mod']) ? $_REQUEST['mod'] : "";


//arrPrint($action_modules);

//GET MENU MODULES
if(!isset($_SESSION['actionmenu']) || !is_array($_SESSION['actionmenu'])) {
	$sql = "SELECT m.* FROM assist_menu_modules m, assist_".$cmpcode."_menu_modules_users u WHERE mod_dashboard = 1 AND modlocation IN ('".implode("','",$action_modules)."') AND usrtkid = '$tkid' AND modref = usrmodref AND modyn = 'Y' ORDER BY modtext";
	$rows = $my_assist->mysql_fetch_all($sql);
//	$rs = getRS($sql);
//	$mnr1 = mysql_num_rows($rs);
	$mnr1 = count($rows);
//echo $sql;
//	while($row = mysql_fetch_array($rs)) {
	foreach($rows as $row) {
		if(strtoupper($row['modtext'])=="TASK ASSIST" && strlen($menu_mod['amenu'])==0) { $menu_mod['amenu'] = $row['modref']; $_SESSION['MAIN']['amenu']= $row['modref']; }
		$actionmenu[$row['modref']] = $row;
	}
	unset($rs);
	$_SESSION['actionmenu'] = $actionmenu;
} else {
	$actionmenu = $_SESSION['actionmenu'];
	$mnr1 = count($actionmenu);
}
$a = 0;
if($mnr1*1 > 0) {
	if(strlen($menu_mod['amenu'])==0 || !isset($actionmenu[$menu_mod['amenu']])) { 
		foreach($actionmenu as $am) {
			$menu_mod['amenu'] = $am['modref']; 
			break;
		}
	}
	$dbref = "assist_".$cmpcode."_".strtolower($menu_mod['amenu']);
	$modref = strtolower($menu_mod['amenu']);
	$modloc = $actionmenu[$menu_mod['amenu']]['modlocation'];
	//echo $modloc;
	$_SESSION['MAIN']['ref'] = $modref;
	$_SESSION['MAIN']['modloc'] = $modloc;
	$_SESSION['MAIN']['modlocation'] = $modloc;
	$_SESSION['MAIN']['modtext'] = $actionmenu[$menu_mod['amenu']]['modtext'];
	$_SESSION['MAIN']['modref'] = $modref;
	$_SESSION['MAIN']['dbref'] = $dbref;
//arrPrint($_SESSION);
	
} else {

	$dbref = "";
	$modloc = "main";
	$modref = "main";
	$_SESSION['MAIN']['ref'] = $modloc;
	$_SESSION['MAIN']['modtext'] = "Ignite Assist";
	$_SESSION['MAIN']['modref'] = "main";

} //if action modules available ?>