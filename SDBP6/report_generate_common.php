<?php
/** @var STRING $page_part - from calling page */
/** @var STRING $report_class_object - from calling page */

if(!isset($_REQUEST['my_sdbip_id'])) {
	if(isset($_SESSION[$_SESSION['ref']]['REPORT_SDBIP_ID'])) {
		$_REQUEST['my_sdbip_id'] = $_SESSION[$_SESSION['ref']]['REPORT_SDBIP_ID'];
	} else {
		header("Location: report.php");
	}
}
require_once("inc_header.php");

ASSIST_HELPER::displayResult(array("info", "Redirecting..."));
?>
	<script>
		$(function () {
			document.location.href = '../module3/report.php?page=report_generate_<?php echo $page_part; ?>&class=MODULE3_SDBP6_REPORT_<?php echo $report_class_object; ?>&my_sdbip_id=<?php echo $_REQUEST['my_sdbip_id']; ?>';
		});
	</script>
<?php
?>