<?php
require_once("inc_header.php");

echo "<div style='width:500px;margin:0 auto;padding:10px'>";

ASSIST_HELPER::displayResult(array("warn", "Dear User<br /><br />We are aware of an issue on this page and it has been temporarily removed while this is resolved.<br /><br /><span class='b'>As a temporary workaround, please use the Admin > Departmental > Edit function to amend links between the Top Layer and Departmental KPIs.</span><br /><br />We apologise for any inconvenience.<br />Action Assist"));

echo "</div>";

?>