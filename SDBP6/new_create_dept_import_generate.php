<?php
require_once("../module/autoloader.php");


$myObject = new SDBP6_DEPTKPI();
$object_type = $myObject->getMyObjectType();
$results_object_type = $myObject->getMyChildObjectType();
$has_time = true;
$time_setting = $myObject->getTimeSetting();
//$sdbip_id = $myObject->getParentID(0); - not returning valid SDBIP ID.  Need to go directly to session to get current SDBIP ID  #AA-627 JC 9 June 2021
$sdbip_details = $myObject->getCurrentSDBIPFromSessionData("NEW");
$sdbip_id = $sdbip_details['id'];

$headingObject = new SDBP6_HEADINGS();
$headings = $headingObject->getMainObjectHeadings($object_type, "IMPORT", "NEW", "", true);
$results_headings = $headingObject->getMainObjectHeadings($results_object_type, "IMPORT", "NEW", "", true);


if(isset($_REQUEST['incl_top']) && $_REQUEST['incl_top'] == "YES") {
	$included_data = array();

	$topObject = new SDBP6_TOPKPI();
	$rows = $topObject->getRawObjectListByParent($sdbip_id);
	$top_field = $topObject->getTableField()."_";
	$dept_field = $myObject->getTableField()."_";

	//get list items
	$lists = array();
	$listObject = new SDBP6_LIST();
	$listObject->setSDBIPID($sdbip_id);
	foreach($headings['rows'] as $fld => $head) {
		$type = $head['type'];
		$list_table = $head['list_table'];
		if(!isset($lists[$list_table])) {
			switch($type) {
				case "LIST":
				case "MULTILIST":
					$listObject->changeListType($list_table);
					$lists[$list_table] = $listObject->getActiveListItemsFormattedForSelect();
					break;
				case "SEGMENT":
				case "MULTISEGMENT":
					$processObject = new SDBP6_SEGMENTS($list_table);
					$lists[$list_table] = $processObject->getActiveListItemsFormattedForImportProcessing();
					unset($processObject);
					break;
				case "OBJECT":
				case "MULTIOBJECT":
					if(strpos($list_table, "|") !== false) {
						$lon = explode("|", $list_table);
						$table = $lon[0];
						$extra_info = $lon[1];
					} else {
						$table = $list_table;
						$extra_info = "";
					}
					$processObject = new $table();
					echo $list_table;
					if($list_table == "SDBP6_PROJECT") {
						$lists[$list_table] = $processObject->getActiveListItemsFormattedForImport($sdbip_id);
					} else {
						$lists[$list_table] = $processObject->getActiveListItemsFormattedForSelect($extra_info);
					}
					unset($processObject);
					break;
			}
		}
	}

	//FIELD MAP - all fields match EXCEPT responsible dept (*_sub_id) AND top layer id (kpi_top_id)
	foreach($rows as $key => $r) {
		foreach($r as $fld => $d) {
			//convert top_*** to kpi_*** - str_replace didn't work as couldn't limit to only first occurrence
			$fld = substr_replace($fld, $dept_field, 0, strlen($top_field));
			//don't use responsible department as dept uses sub while top uses dir
			if($fld == $dept_field."sub_id") {
				$d = "";
			} else {
				//process lists, date & boolean to output
				if(isset($headings['rows'][$fld])) {
					$head = $headings['rows'][$fld];
					$type = $head['type'];
					$list_table = $head['list_table'];
					switch($type) {
						case "LIST":
						case "MULTILIST":
						case "SEGMENT":
						case "MULTISEGMENT":
						case "OBJECT":
						case "MULTIOBJECT":
							//leave blank if unspecified or can't be found in the list
							if((int)$d == 0 || !isset($lists[$list_table][$d])) {
								$d = "";
							} else {
								$d = $lists[$list_table][$d];
							}
							break;
						case "DATE":
							if($d == "0000-00-00") {
								$d = "";
							} else {
								//don't format - input looked for is Y-m-d
							}
							break;
						case "BOOL":
						case "BOOL_BUTTON":
							if((int)$d == 1) {
								$d = "Yes";
							} else {
								$d = "No";
							}
							break;
					}
				}
			}
			$d = ASSIST_HELPER::decode($d);
			$d = str_replace("\r\n", " ", $d);
			$d = str_replace(chr(10), " ", $d);
			$d = str_replace('"', "\"\"", $d);


			$included_data[$key][$fld] = $d;
		}
		//add link to top layer
		$included_data[$key][$dept_field."top_id"] = $r['top_ref'];
	}


}

//ASSIST_HELPER::arrPrint($included_data);

include("common/import_template_generator.php");

/*
$fdata = "\"Parent GUID\",\"Line Item GUID\",\"Line Item Name\",\"Description\",\"Has sub-levels?\",\"Can be assigned?\"\r\n\"\",\"Required & Unique\",\"Required\",\"\",\"Yes / No\",\"Yes / No\"\r\n";

$me = new MSCOA1_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$today = time();

        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi",$today).".csv";
        $newfilename = "template.csv";
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);

*/
?>