<?php

/**
 * Class SDBP6_REPORT_FIXED
 * To manage fixed reports #AA-162
 * Created: 10 Nov 2019
 * Created by: Janet Currie [JC]
 */

class SDBP6_REPORT_FIXED extends SDBP6 {

	private $table_name = "_report";
	private $field_prefix = "report";
	private $section_field_prefix = "section";
	private $custom_field_prefix = "custom";

	private $schedule_report_table = "sdbip_schedule";
	private $schedule_lines_table = "sdbip_schedule_lines";
	private $schedule_map_table = "sdbip_schedule_map";


	const MSCOA_ONLY_REPORT = 32;
	const FIN_SCHEDULE_REPORT = 128;
	const CUSTOM_REPORT = 512;


	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);
	}


	/****************************************************************************************
	 * Generic functions
	 */

	public function getTableName() {
		return $this->getDBRef().$this->table_name;
	}

	public function getSectionTableName() {
		return $this->getDBRef().$this->table_name."_section";
	}

	public function getCustomTableName() {
		return $this->getDBRef().$this->table_name."_custom";
	}

	public function getOutputTableName() {
		return $this->getDBRef().$this->table_name."_output_type";
	}

	public function getTableField() {
		return $this->field_prefix;
	}

	public function getSectionTableField() {
		return $this->section_field_prefix;
	}

	public function getCustomTableField() {
		return $this->custom_field_prefix;
	}


	public function processReportDetails($row) {
		$x = array('is_active' => true, 'is_fin_schedule' => false, 'is_mscoa_only' => false, 'is_custom' => false);
		foreach($row as $fld => $r) {
			$f = explode("_", $fld);
			unset($f[0]);
			$fld = implode("_", $f);
			if($fld == "section_id") {
				$section = $r;
			} else {
				$x[$fld] = $r;
				if($fld == "system_status") {
					$x['is_mscoa_only'] = (($r & self::MSCOA_ONLY_REPORT) == self::MSCOA_ONLY_REPORT);
					$x['is_fin_schedule'] = (($r & self::FIN_SCHEDULE_REPORT) == self::FIN_SCHEDULE_REPORT);
					$x['is_custom'] = (($r & self::CUSTOM_REPORT) == self::CUSTOM_REPORT);
				} elseif($fld == "user_status") {
					$x['is_active'] = (($r & self::ACTIVE) == self::ACTIVE);
				}
			}
		}
		$x = $this->replaceAllNames($x);
		return $x;
	}


	public function getReportSections($admin = false) {
		$sql = "SELECT section_id as id, section_name as name, section_code as code 
				FROM ".$this->getSectionTableName()." 
				WHERE (section_system_status & ".self::ACTIVE.") = ".self::ACTIVE." 
				".($admin == false ? " AND (section_user_status & ".self::ACTIVE.") = ".self::ACTIVE : "")." 
				ORDER BY section_sort, section_name ";
		$rows = $this->mysql_fetch_all_by_id($sql, "id");

		$rows = $this->replaceAllNames($rows);
		return $rows;
	}

	public function getReportsGroupedBySection($admin = false) {
		$sql = "SELECT R.*, OT.name as report_output_type 
				FROM ".$this->getTableName()." R
				INNER JOIN ".$this->getOutputTableName()." OT
				ON OT.id = R.report_output_type_id
				WHERE  (report_system_status & ".self::ACTIVE.") = ".self::ACTIVE." 
				".($admin == false ? " AND (report_user_status & ".self::ACTIVE.") = ".self::ACTIVE : "")." 
				";
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $row) {
			$section = $row['report_section_id'];
			$x = $this->processReportDetails($row);
			$data[$section][$x['id']] = $x;
		}
		$data = $this->replaceAllNames($data);
		return $data;
	}

	/*****************************************************************************************
	 * User Front end functions for Report > Fixed
	 */
	public function getReportDetails($object_id) {
		$sql = "SELECT R.*, OT.name as report_output_type 
				FROM ".$this->getTableName()." R
				INNER JOIN ".$this->getOutputTableName()." OT
				ON OT.id = R.report_output_type_id
				WHERE  report_id = ".$object_id." 
				";
		$row = $this->mysql_fetch_one($sql);
		$x = $this->processReportDetails($row);
		return $x;
	}

	/*****************************************************************************************
	 * Admin functions for Report > Admin
	 */
	public function getReportSectionsForAdmin() {
		return $this->getReportSections(true);
	}

	public function getReportsGroupedBySectionForAdmin() {
		return $this->getReportsGroupedBySection(true);
	}


	/*******************************************************************************************
	 * Financial Schedules
	 */
	/**
	 * getSDBIPSchedule : get Report Schedule details
	 * @param $code - code to match with schedule
	 */
	public function getSDBIPSchedule($code) {
		$masterDB = new ASSIST_DB("master");
		$sql = "SELECT * 
				FROM ".$this->schedule_report_table." 
				INNER JOIN ".$this->schedule_lines_table." 
				ON schedule_id = sl_schedule_id 
				WHERE schedule_remote_code = '$code' AND (sl_status & ".self::ACTIVE.") =  ".self::ACTIVE." ORDER BY sl_code";
		$rows = $masterDB->mysql_fetch_all_by_id($sql, "sl_id");
		$data = array(
			'schedule_details' => array(),
			'lines' => array(),
			'map' => array(),            //map for displaying report
			'fin_map' => array(),        //map for getting financials
		);
		//process schedule details
		$keys = array_keys($rows);
		$zero = $rows[$keys[0]];
		foreach($zero as $fld => $x) {
			$f = explode("_", $fld);
			if($f[0] == "schedule") {
				unset($f[0]);
				$fld = implode("_", $f);
				$data['schedule_details'][$fld] = $x;
			}
		}
		//process lines
		$lines = array();
		if(count($rows) > 0) {
			foreach($rows as $line_id => $row) {
				$lines[] = $line_id;
				$line_parent_id = $row['sl_parent_id'];
				foreach($row as $fld => $x) {
					$f = explode("_", $fld);
					if($f[0] == "sl") {
						unset($f[0]);
						$fld = implode("_", $f);
						if($fld != "schedule_id" && $fld != "status") {
							$data['lines'][$line_parent_id][$line_id][$fld] = $x;
						}
					}
				}
			}
		}
		//get map
		if(count($lines) > 0) {
			$sql = "SELECT * FROM ".$this->schedule_map_table." WHERE sm_sl_id IN (".implode(",", $lines).")";
			$rows = $masterDB->mysql_fetch_all($sql);
			foreach($rows as $row) {
				$line_id = $row['sm_sl_id'];
				$segment = $row['sm_segmentcode'];
				$nt_guid = $row['sm_nt_guid'];
				if(!isset($data['map'][$line_id][$segment]) || !in_array($nt_guid, $data['map'][$line_id][$segment])) {
					$data['map'][$line_id][$segment][] = $nt_guid;
				}
				if(!isset($data['fin_map'][$segment]) || !in_array($nt_guid, $data['fin_map'][$segment])) {
					$data['fin_map'][$segment][] = $nt_guid;
				}
			}
		}

		return $data;
	}

	/*******************************************************************************************
	 * End
	 */
	public function __destruct() {
		parent::__destruct(); // TODO: Change the autogenerated stub
	}
}


?>