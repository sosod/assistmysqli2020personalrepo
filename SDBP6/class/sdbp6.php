<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class SDBP6 extends SDBP6_HELPER {
	/***
	 * Module Wide variables
	 */
	protected $current_module_version = "SDBP6";
	protected $prior_module_version = "SDBP5B";

	protected $object_form_extra_js = "";

	protected $deliverable_types = array();

	protected $copy_function_protected_fields = array();
	protected $copy_function_protected_heading_types = array("ATTACH");

	protected $has_target = false;

	protected $has_secondary_parent = false;
	protected $has_tertiary_parent = false;

	protected $has_kpi_results = false;
	protected $has_finances = false;

	protected $module_sections = array("DEPTKPI", "TOPKPI", "PROJECT");
	protected $module_kpi_sections = array("DEPTKPI", "TOPKPI");

	protected $session_data = array();

	protected $local_modref = "";
	protected $local_cmpcode = "";

	protected $has_internal_ref_field = true;

	protected $actual_update_allowed = true;
	protected $has_approve_functionality = true;
	protected $has_assurance_functionality = true;

	protected $sdbip_id = 0;

	/**
	 * Import from external module variables
	 */
	private $external_import_record = array();
	private $external_import_phases = array(
		SDBP6_PROJECT::OBJECT_TYPE => array(
			1 => 0,
			2 => 0,
			3 => 0,
			'status' => 0
		),
		SDBP6_TOPKPI::OBJECT_TYPE => array(
			1 => 0,
			2 => 0,
			3 => 0,
			'status' => 0
		),
		SDBP6_DEPTKPI::OBJECT_TYPE => array(
			1 => 0,
			2 => 0,
			3 => 0,
			'status' => 0
		),
	);

	const EXTERNAL_IMPORT_ACTIVE = 2;
	const EXTERNAL_IMPORT_ERROR = 4;
	const EXTERNAL_IMPORT_PHASE1_BUSY = 128;
	const EXTERNAL_IMPORT_PHASE1_COMPLETE = 1024;
	const EXTERNAL_IMPORT_PHASE2_BUSY = 128 * 2;
	const EXTERNAL_IMPORT_PHASE2_COMPLETE = 1024 * 2;
	const EXTERNAL_IMPORT_PHASE3_BUSY = 128 * 2 * 2;
	const EXTERNAL_IMPORT_PHASE3_COMPLETE = 1024 * 2 * 2;
	/**
	 * Module Wide Constants
	 * */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;
	const EXTERNAL_IMPORT = 128;

	const UPDATE_NOT_STARTED = 0;
	const UPDATE_IN_PROGRESS = 16;
	const UPDATE_COMPLETE = 32;
	const TOP_AUTO_LOCKED = 64;    //updated manually = don't update by auto
	const TOP_AUTO_UPDATED = 128; //updated by auto process

	//Approve or Assurance
	const NOT_REVIEWED = 0;
	const REVIEW_OK = 16;
	const REVIEW_REJECT = 32;
	const REVIEW_UPDATED = 64;    //when user updates result in response to rejection
	const REVIEW_AUTO = 128;    //when approve/assurance is done by auto process
	//128

	const SDBIP_NEW = 256;
	const SDBIP_CONFIRMED = 512;
	const SDBIP_ACTIVATED = 1024;
	const SDBIP_RESET = 2048;

	//Converted objects - stage 1 or 2
	const CONVERT_IMPORT = 4096;    //applied when object is first imported from old module
	const CONVERT_SDBP6ED = 8192;    //applied when imported object has been updated with SDBP6 fields


	const MODULE_CODE = "SDBP6";

	const OBJECT_SRC_IDP = "IDP";    //copied from IDP
	const OBJECT_SRC_INTERNAL = "INTERNAL";    //copied from prior year in same module
	const OBJECT_SRC_IMPORT = "IMPORT";//import funtion
	const OBJECT_SRC_MANUAL = "MANUAL";//manual add
	const OBJECT_SRC_POST = "POST";//manual add post activation
	const OBJECT_SRC_PRIOR_VERSION = "PRIORVER";//copied from SDBP5B (previous version)

	const OBJECT_SRC_SDBIP = "SDBIP";//??
	const OBJECT_SRC_CONVERT = "CONVERT";//??

	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);

		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		$this->local_modref = $modref;
		$this->local_cmpcode = $cmpcode;

		//Assume prior module is SDBP5B if full module check passed otherwise assume IKPI1 (check done in SDBP6_HELPER)
		if($this->is_full_module !== true) {
			$this->prior_module_version = "IKPI1";
			$this->module_sections = array("DEPTKPI");
			$this->module_kpi_sections = array("DEPTKPI");
		}

	}


	public function getMySecondaryParentObjectType() {
		if($this->has_secondary_parent) {
			return self::SECONDARY_PARENT_OBJECT_TYPE;
		}
		return false;
	}

	public function hasParent() {
		if(!is_null($this->getMyParentObjectType())) {
			return true;
		}
		return false;
	}

	public function getModuleSections() {
		return $this->module_sections;
	}

	public function getModuleCode() {
		return self::MODULE_CODE;
	}

	public function getSessionData() {
		return $this->session_data;
	}

	public function setSessionDate() {
		if(strlen($this->local_modref) > 0 && isset($_SESSION[strtoupper($this->local_modref)])) {
			$this->session_data = $_SESSION[strtoupper($this->local_modref)];
		}
	}

	public function setSDBIPID($sdbip_id) {
		$this->sdbip_id = $sdbip_id;
	}

	public function getCurrentSDBIPIDFromSessionData() {
		$object_type = SDBP6_SDBIP::OBJECT_TYPE;
		if(isset($_SESSION[$this->getModRef()][$object_type]['MY_CURRENT_SDBIP']) && $_SESSION[$this->getModRef()][$object_type]['MY_CURRENT_SDBIP'] > 0 && $_SESSION[$this->getModRef()][$object_type]['MY_CURRENT_SDBIP'] !== false) {
			return $_SESSION[$this->getModRef()][$object_type]['MY_CURRENT_SDBIP'];
		} else {
			return false;
		}
	}

	public function getCurrentSDBIPFromSessionData($section = false) {
		//Added section option to force return of correct SDBIP if user is working in NEW but has an already active SDBIP available in MANAGE/ADMIN etc AA-554 JC 4 March 2021
		$object_type = SDBP6_SDBIP::OBJECT_TYPE;
		if($section != "NEW" && (isset($_SESSION[$this->getModRef()][$object_type]['OBJECT']['details'])) && count($_SESSION[$this->getModRef()][$object_type]['OBJECT']['details']) > 0) {
			return $_SESSION[$this->getModRef()][$object_type]['OBJECT']['details'];
		} elseif($section == "NEW" && isset($_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details']) && count($_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details']) > 0) {
			return $_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details'];
		} else {
			return array();
		}
	}

	public function getSDBIPFromSessionDataUsingID($sdbip_id) {
		$object_type = SDBP6_SDBIP::OBJECT_TYPE;
		if(isset($_SESSION[$this->getModRef()][$object_type]['OBJECT']['details']) && count($_SESSION[$this->getModRef()][$object_type]['OBJECT']['details']) > 0 && $_SESSION[$this->getModRef()][$object_type]['OBJECT']['details']['sdbip_id'] == $sdbip_id) {
			return $_SESSION[$this->getModRef()][$object_type]['OBJECT']['details'];
		} elseif(isset($_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details']) && count($_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details']) > 0 && $_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details']['sdbip_id'] == $sdbip_id) {
			return $_SESSION[$this->getModRef()][$object_type]['NEW_OBJECT']['details'];
		} else {
			return array();
		}
	}

	public function canUserUpdateActual($object_id = 0) {
		//if it's a Top Layer KPI - check for linking
		$const = SDBP6_TOPKPI::OBJECT_TYPE;
		$func = $this->getMyObjectType();
		if($func == $const) {
			$deptObject = new SDBP6_DEPTKPI();
			$linked_active_children = $deptObject->getNumberOfKPIsLinkedToTopLayer($object_id);
			return ($linked_active_children > 0 ? false : $this->actual_update_allowed);
		} else {
			return $this->actual_update_allowed;
		}
	}

	public function hasApproveFunctionality() {
		return $this->has_approve_functionality;
	}

	public function hasAssuranceFunctionality() {
		return $this->has_assurance_functionality;
	}

	/*********************
	 * MODULE OBJECT functions
	 * for CONTRACT, DELIVERABLE AND ACTION
	 */
	public function getObject($var) { //$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'], $options);
				if($var['type'] == "LIST" || $var['type'] == "ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data, $this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details) > 0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'], $var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'], $var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function isObjectActive($object_id) {
		if($this->checkIntRefForWholeIntegersOnly($object_id)) {
			$sql = "SELECT ".$this->getStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$status = $this->mysql_fetch_one_value($sql, $this->getStatusFieldName());
			if(($status & self::ACTIVE) == self::ACTIVE) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}

	public function hasKPIResults() {
		return $this->has_kpi_results;
	}

	public function hasFinances() {
		return $this->has_finances;
	}

	public function getTimeType() {
		return $this->time_type;
	}

	public function hasMonthlyTimePeriods() {
		return ($this->time_type == "MONTH");
	}

	public function hasQuarterlyTimePeriods() {
		return ($this->time_type == "QUARTER");
	}

	public function getResultsFieldName($field) {
		return $this->results_fields[$field];
	}

	public function getStatusFieldName() {
		return $this->status_field;
	}

	public function getResultsStatusFieldName() {
		return $this->getResultsFieldName("STATUS");
	}

	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}

	public function getProgressFieldName() {
		return $this->progress_field;
	}

	public function getIDFieldName() {
		return $this->id_field;
	}

	public function getResultsIDFieldName() {
		return $this->getResultsFieldName("ID");
	}

	public function getParentFieldName() {
		return $this->parent_field;
	}

	public function getResultsParentFieldName() {
		return $this->getResultsFieldName("PARENT");
	}

	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}

	public function getResultsSecondaryParentFieldName() {
		return $this->getResultsFieldName("SECONDARY_PARENT");
	}

	public function getResultsTimeFieldName() {
		return $this->getResultsFieldName("TIME");
	}

	public function getTertiaryParentFieldName() {
		return $this->tertiary_parent_field;
	}

	public function getNameFieldName() {
		return $this->name_field;
	}

	public function getSDBIPParentFieldName() {
		return $this->sdbip_parent_field;
	}

	public function getResultsNameFieldName() {
		return $this->getResultsFieldName("REVISED");
	}

	public function getResultsOriginalFieldName() {
		return $this->getResultsFieldName("ORIGINAL");
	}

	public function getResultsAdjustmentsFieldName() {
		return $this->getResultsFieldName("ADJUSTMENTS");
	}

	public function getResultsDescriptonFieldName() {
		return $this->getResultsFieldName("DESCRIPTION");
	}

	public function getResultsRevisedFieldName() {
		return $this->getResultsFieldName("REVISED");
	}

	public function getResultsActualFieldName() {
		return $this->getResultsFieldName("ACTUAL");
	}

	public function getResultsVarianceFieldName() {
		return $this->getResultsFieldName("VARIANCE");
	}

	public function getResultsResultsFieldName() {
		return $this->getResultsFieldName("RESULTS");
	}

	public function getResultsAttachmentFieldName() {
		return $this->getResultsFieldName("ATTACHMENT");
	}

	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}

	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}

	public function getInsertDateFieldName() {
		return $this->getTableField()."_insertdate";
	}

	public function getOwnerFieldName() {
		return $this->owner_field;
	}

	public function getDepartmentFieldName() {
		return $this->department_field;
	}

	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}

	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}

	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}

	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}

	public function getPageLimit() {
		$profileObject = new SDBP6_PROFILE();
		//return $profileObject->getProfileTableLength();
		return 1000000;
	}

	public function getDateOptions($fld) {
		if(stripos($fld, "action_on") !== FALSE) {
			$fld = "action_on";
		}
		switch($fld) {
			case "action_on":
				return array('maxDate' => "'+0D'");
				break;
			default:
				return array();
		}
	}

	public function getExtraObjectFormJS() {
		return $this->object_form_extra_js;
	}

	public function getAllDeliverableTypes() {
		return $this->deliverable_types;
	}

	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key => $t) {
			$arr[] = $t;
		}
		return $arr;
	}

	public function getDeliverableTypes($contract_id) {
		$contractObject = new SDBP6_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables() == FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types;
	}

	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt, $ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE", $options);
		//$this->arrPrint($data);
		$result = array(
			'past' => 0,
			'present' => 0,
			'future' => 0,
		);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z < $now) {
				$result['past']++; //echo "past";
			} elseif($z > $now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE", $options);
		return $data;
	}


	/*****************************
	 * Update status functions
	 */
	protected function getInProgressUpdateStatus() {
		return SDBP6::UPDATE_IN_PROGRESS;
	}

	protected function getNewUpdateStatus() {
		return SDBP6::UPDATE_NOT_STARTED;
	}

	protected function getCompletedUpdateStatus() {
		return SDBP6::UPDATE_COMPLETE;
	}

	public function isStatusCompleted($s) {
		return $this->checkUpdateStatus($s, SDBP6::UPDATE_COMPLETE);
	}

	public function isStatusNew($s) {
		return $this->checkUpdateStatus($s, SDBP6::UPDATE_NOT_STARTED);
	}

	public function isStatusInProgress($s) {
		return $this->checkUpdateStatus($s, SDBP6::UPDATE_IN_PROGRESS);
	}

	protected function checkUpdateStatus($sent_status, $status_to_validate_against) {
		//make sure it is numerical
		$sent_status = $sent_status * 1;
		if($sent_status == $status_to_validate_against) {
			return true;
		}
		return false;
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getActiveListItemsFormattedForSelect($options = array()) {
		if(isset($this->parent_id) && $this->parent_id !== false) {
			$sdbip_id = $this->parent_id;
		} elseif($this->sdbip_id > 0) {
			$sdbip_id = $this->sdbip_id;
		} else {
			$sdbip_id = 0;
		}
		return $this->getActiveObjectsFormattedForSelect($sdbip_id, $options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($sdbip_id = 0, $options = array()) { //$this->arrPrint($options);
		if(is_array($options)) {
			$name_type = isset($options['name_type']) ? $options['name_type'] : "name";
			unset($options['name_type']);
		} else {
			$name_type = "name";
		}
		switch($name_type) {
			case "ref":
				$name_sql = $this->getTableField()."_ref";
				break;
			default:
				$name_sql = "CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')')";
				break;
		}
		$parent_where = "";
		if(is_array($sdbip_id)) {
			if(isset($sdbip_id['parent_id'])) {
				if($this->checkIntRef($sdbip_id['parent_id'])) {
					$parent_where .= " AND ".$this->getParentFieldName()." = ".$sdbip_id['parent_id'];
				}
			} else {
				if(isset($this->parent_id) && $this->checkIntRef($this->parent_id)) {
					$parent_where .= " AND ".$this->getParentFieldName()." = ".$this->parent_id;
				}
			}
		} else {
			if(isset($sdbip_id) && $this->checkIntRef($sdbip_id)) {
				$parent_where .= " AND ".$this->getParentFieldName()." = ".$sdbip_id;
			} else {
				if(isset($this->parent_id) && $this->checkIntRef($this->parent_id)) {
					$parent_where .= " AND ".$this->getParentFieldName()." = ".$this->parent_id;
				}
			}
		}
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , $name_sql as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				".$parent_where;
		if(is_array($options) && count($options) > 0) {
			foreach($options as $key => $val) {
				switch($key) {
					case "id":
						$sql .= " AND ".$this->getIDFieldName()." = ".$val;
						break;
					case "parent":
						$sql .= " AND ".$this->getParentFieldName()." = ".$val;
						break;
					case "department": //Added department field to allow for "Used By" check in Setup > OrgStructure #AA-625 JC [3 June 2021]
						$sql .= " AND ".$this->getDepartmentFieldName()." = ".$val;
						break;
				}
			}
		} //echo $sql;
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getAllListItemsFormattedForSelect($options = array()) {
		if(isset($this->parent_id) && $this->parent_id !== false) {
			$sdbip_id = $this->parent_id;
		} elseif(isset($this->sdbip_id) && $this->sdbip_id > 0) {
			$sdbip_id = $this->sdbip_id;
		} else {
			$sdbip_id = 0;
		}
		return $this->getAllObjectsFormattedForSelect($options, $sdbip_id);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getAllObjectsFormattedForSelect($options = array(), $sdbip_id = 0) {
		if(!isset($options['include_system_ref'])) {
			$options['include_system_ref'] = $this->has_internal_ref_field;
		} else {
			if($this->has_internal_ref_field == false) {
				$options['include_system_ref'] = false;
			}
		}
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  ";
		if($options['include_system_ref'] === true) {
			$sql .= "
				  , IF(LENGTH(".$this->getTableField()."_ref)>0,CONCAT(".$this->getNameFieldName().",' [',".$this->getTableField()."_ref,']'),".$this->getNameFieldName().") ";
		} else {
			$sql .= " , ".$this->getNameFieldName()." ";
		}
		$sql .= " as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED."
				".($sdbip_id > 0 ? " AND ".$this->getParentFieldName()." = ".$sdbip_id : "");
		if(isset($options['id_filter'])) {
			if(is_array($options['id_filter'])) {
				$ids = ASSIST_HELPER::removeBlanksFromArray($options['id_filter']);
				if(count($ids) > 0) {
					$sql .= " AND ".$this->getIDFieldName()." IN (".implode(",", $ids).") ";
				}
			} elseif(strlen($options['id_filter']) > 0 && ASSIST_HELPER::checkIntRef($options['id_filter']) == true) {
				$ids = $options['id_filter'];
				$sql .= " AND ".$this->getIDFieldName()." = ".$ids;
			}
		}
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}

	/**
	 * Get selected list items depending on ID submitted - required for logging
	 */
	public function getSelectedListItemsFormattedForSelect($ids = 0) {
		$options = array('id_filter' => $ids);
		if(isset($this->parent_id) && $this->parent_id !== false) {
			$sdbip_id = $this->parent_id;
		} else {
			$sdbip_id = 0;
		}
		return $this->getAllObjectsFormattedForSelect($options, $sdbip_id);
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($options = array()) {
		if(isset($this->parent_id) && $this->parent_id !== false) {
			$sdbip_id = $this->parent_id;
		} else {
			$sdbip_id = 0;
		}
		return $this->getActiveObjectsFormattedForImportProcessing($sdbip_id, $options);
	}

	/**
	 * Get list of active objects ready to filter for import
	 */
	public function getActiveObjectsFormattedForImportProcessing($sdbip_id = 0, $options = array()) { //$this->arrPrint($options);
		$items = $this->getActiveObjectsFormattedForSelect($sdbip_id, $options);
		$data = array();
		foreach($items as $id => $name) {
			$name = trim(strtolower(str_replace(" ", "", $name)));
			$data[$name] = $id;
		}
		return $data;
	}


	public function formatListForImportProcessing($original_list = array()) {
		$formatted_array = array();
		foreach($original_list as $key => $value) {
			$value = str_replace(chr(10), "", $value);
			$value = trim(strtolower(str_replace(" ", "", $value)));
			$formatted_array[$value] = $key;
		}
		return $formatted_array;
	}


	public function getMyList($obj_type, $section, $options = array()) {
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		echo "<P>object: ".$obj_type;
//		echo "<P>section: ".$section;
//		echo "<P>options: "; $this->arrPrint($options);
		if((isset($options['filter']['when']) && $options['filter']['when'] == "X") || (isset($options['page']) && strpos($options['page'], "UPDATE") !== false && isset($options['update_when']) && count($options['update_when']) == 0)) {
			$do_not_get_results_due_to_time_period_filter = true;
//			echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
		} else {
//			echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
			$do_not_get_results_due_to_time_period_filter = false;
		}
		$has_results = $this->hasResults();
		$object_options = $options;
		$preferencesObject = new SDBP6_SETUP_PREFERENCES();
		$preference_for_sort_by = $preferencesObject->getAnswerToQuestion("ALL_sort");
		if(isset($options['page'])) {
			$page = strtoupper($options['page']);
			unset($options['page']);
		} else {
			$page = "DEFAULT";
		}
		$trigger_rows = true;
		if(isset($options['trigger'])) {
			$trigger_rows = $options['trigger'];
			unset($options['trigger']);
		}
		//echo $page;
		if($page == "LOGIN_STATS" || $page == "LOGIN_TABLE") {
			$future_deadline = $options['deadline'];
			unset($options['deadline']);
			if(!isset($options['limit'])) {
				$options['limit'] = false;
			}
		}
		$compact_details = ($page == "CONFIRM" || $page == "ALL");
		if(isset($options['limit']) && ($options['limit'] === false || $options['limit'] == "ALL")) {
			$pagelimit = false;
			$start = 0;
			$limit = false;
			unset($options['start']);
			unset($options['limit']);
		} else {
			$pagelimit = $this->getPageLimit();
			$start = isset($options['start']) ? $options['start'] : 0;
			unset($options['start']);
			$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;
			unset($options['limit']);
			if($start < 0) {
				$start = 0;
				$limit = false;
			}
		}
		//Filter settings
		if(isset($options['filter'])) {
			$paging_filter = $options['filter'];
			unset($options['filter']);
			if(!isset($paging_filter['who'])) {
				$paging_filter['who'] = "all";
			}
			if(!isset($paging_filter['what'])) {
				$paging_filter['what'] = "all";
			}
			if(!isset($paging_filter['when'])) {
				$paging_filter['when'] = "today";
			}
			if(!isset($paging_filter['display'])) {
				$paging_filter['display'] = "list";
			}
		} else {
			$paging_filter = array(
				'who' => "all",
				'what' => "all",
				'when' => "today",
				'display' => "list"
			);
		}
//This limits the SQL select statement to only show those items available based on the FILTERING options selected
		$filter_sql = $this->getFilterSQL($paging_filter);
		//AND moved to $where set [JC] AA-484 5 Oct 2020
		/*if(strlen($filter_sql)>0) {
			$filter_sql = " AND ".$filter_sql;
		}*/

		if(isset($options['sdbip_id'])) {
			$sdbip_sql = $this->getParentFieldName()." = ".$options['sdbip_id'];
		} elseif(isset($this->parent_id) && $this->checkIntRef($this->parent_id)) {
			$sdbip_sql = $this->getParentFieldName()." = ".$this->parent_id;
		} else {
			$sdbip_sql = "";
		}

		if(isset($options['sdbip_details'])) {
			$sdbip_details = $options['sdbip_details'];
			unset($options['sdbip_details']);
			$mscoa_version_id = $sdbip_details['mscoa_version_id'];
		} else {
			$mscoa_version_id = 1; //assume non-compliant (code:NA=id:1) if no sdbip_Details / mscoa_version given AA-554 JC
		}

		if(strlen($filter_sql) > 0) {
			$filter_sql .= (strlen($sdbip_sql) > 0 ? " AND " : "").$sdbip_sql;
		} else {
			//AND moved to $where set [JC] AA-484 5 Oct 2020
//			$filter_sql = (strlen($sdbip_sql)>0?" AND ":"").$sdbip_sql;
			$filter_sql = $sdbip_sql;
		}


		//filter options (needed for time periods)
		if(isset($options['by'])) {
			$filter_by = $options['by'];
			unset($options['by']);
		} else {
			$filter_by = false;
		}

		$order_by = ""; //still to be processed
		$left_joins = array();

		//comment out for development purposes to register changes in heading class
		if(!isset($_SESSION[$this->getModRef()]['headingObject'])) {
			$headObject = new SDBP6_HEADINGS();
		} else {
			$headObject = unserialize($_SESSION[$this->getModRef()]['headingObject']);
		}

		//check what type of page this is - are results fields required?
		$is_new_page = false;
		if($section == "NEW" || (isset($object_options['section']) && $object_options['section'] == "NEW")) {
			$is_new_page = true;
		}
		$is_update_page = false;
		if(isset($object_options['page']) && strpos($object_options['page'], "UPDATE") !== false) {
			$is_update_page = true;
		}
		$is_edit_page = false;
		if(isset($object_options['page']) && strpos($object_options['page'], "EDIT") !== false) {
			$is_edit_page = true;
		}
		$is_approve_page = false;
		if(strpos($object_options['page'], "APPROVE") !== false) {
			$is_approve_page = true;
		}
		$is_assurance_page = false;
		if(strpos($object_options['page'], "ASSURANCE") !== false) {
			$is_assurance_page = true;
		}

		if($has_results && !$is_new_page) {
			$final_data = array('head' => array(), 'results_head' => array('top' => array(), 'bottom' => array()), 'rows' => array(), 'results_rows' => array());
			$results_headings = $headObject->getListPageHeadings($this->getMyChildObjectType(), "RESULTS", $paging_filter['display']);
//ASSIST_HELPER::arrPrint($results_headings);
			$final_data['results_head']['bottom'] = $results_headings;

			$preferences = array(
				'past_results' => $is_update_page ? 0 : 1,
				'next_results' => $is_update_page ? 0 : 1,
			);
			if($is_update_page) {
				foreach($preferences as $key => $pref) {
					$preferences[$key] = $preferencesObject->getAnswerToQuestion($obj_type."_".$key);
				}
			}
			if(!$is_new_page && isset($paging_filter['when'])) { //echo "<P>paging_filter-when</p>"; ASSIST_HELPER::arrPrint($paging_filter['when']);
				if($paging_filter['when'] == "today" || !isset($filter_by['when'][$paging_filter['when']])) {
					if($is_update_page && count($filter_by['update_when']) > 0) {
						foreach($filter_by['update_when'] as $key => $time) {
							$paging_filter['when'] = $key;
							break;
						}
						/*} elseif($is_assurance_page || $is_approve_page) {
							foreach($filter_by['when'] as $key => $time) {
								$paging_filter['when'] = $key;
								break;
							}*/
					} else {
						foreach($filter_by['when'] as $key => $time) {
							if($time['is_current'] == true) {
								$paging_filter['when'] = $key;
							}
						}
					}
					//if filter when is still set to today then assume that current date is outside financial year and default to first period
					//this should be taken care of in the getting of the time periods for filtering but just in case let's check here too
					if($paging_filter['when'] == "today" || $paging_filter['when'] == "X") {
						$a = array_keys($filter_by['when']);
						$paging_filter['when'] = $a[0];
					}
				}
//ASSIST_HELPER::arrPrint($filter_by['when']);
				$main_time_id = $paging_filter['when'];
				$current_time = $filter_by['when'][$main_time_id];
//ASSIST_HELPER::arrPrint($current_time);
				//prev time
				if($preferences['past_results'] == true && $current_time['prev'] != false) {
					$prev_time = $filter_by['when'][$current_time['prev']];
					$final_data['results_head']['top'][$current_time['prev']] = $prev_time;
				} else {
					$prev_time = false;
				}
				//current time
				$final_data['results_head']['top'][$main_time_id] = $current_time;
				//next time
				if($preferences['next_results'] == true && $current_time['next'] != false) {
					$next_time = $filter_by['when'][$current_time['next']];
					$final_data['results_head']['top'][$current_time['next']] = $next_time;
				} else {
					$next_time = false;
				}
			} else {
				$main_time_id = false;
				$current_time = false;
				$prev_time = false;
				$next_time = false;
			}


		} else {
			$final_data = array('head' => array(), 'rows' => array());
		}

		//set up variables
		$idp_headings = $headObject->getListPageHeadings($obj_type, $section, (isset($paging_filter['display']) ? $paging_filter['display'] : "list"));

		$class_name = "SDBP6_".$obj_type;
		$idpObject = new $class_name();
		$idp_tblref = $idpObject->getMyObjectType();
		$idp_table = $idpObject->getTableName();
		$idp_id = $idp_tblref.".".$idpObject->getIDFieldName();
		$idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
		$idp_status_fld = false;//$idp_tblref.".".$idpObject->getProgressStatusFieldName();
		$idp_name = $idp_tblref.".".$idpObject->getNameFieldName();
		$idp_ref = $idp_tblref.".".$idpObject->getTableField()."_ref";
		$idp_deadline = false;//$idpObject->getDeadlineFieldName();
		$idp_field = $idp_tblref.".".$idpObject->getTableField();

		if($section == "ADMIN" && $is_edit_page) {
			$where = $filter_sql;
		} else {
			$where = $idpObject->getActiveStatusSQL($idp_tblref).(strlen($filter_sql) > 0 ? " AND " : "").$filter_sql;
		}
		$id_fld = $idpObject->getIDFieldName();
		$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
		//extra field in sql to get RAW TARGET TYPE ID - required for results formatting
		if($idpObject->getTargetTypeTableField() !== false) {
			$sql .= ", ".$idpObject->getTargetTypeTableField()." as target_type_id";
		}
		//extra field in sql to get RAW CALC TYPE ID - required for results calculations
		if($idpObject->getCalcTypeTableField() !== false) {
			$sql .= ", ".$idpObject->getCalcTypeTableField()." as calc_type_id";
		}
		if($idpObject->getBaselineTableField() !== false && $idpObject->getNewIndicatorTableField() !== false) {
			$sql .= ", ".$idpObject->getBaselineTableField()." as my_baseline";
			$sql .= ", ".$idpObject->getNewIndicatorTableField()." as new_indicator";
		} else {
//						$sql.= ", 0 as my_baseline";
//						$sql.= ", 0 as new_indicator";
		}
		$from = " $idp_table $idp_tblref
					";
		$final_data['head'] = $idp_headings;
		$where_tblref = $idp_tblref;
		$where_deadline = $idp_deadline;
		switch($preference_for_sort_by) {
			case "REFNUM":
				$where_order = "REGEXP_REPLACE(".$idp_ref.", '[^0-9.]+', '')*1";
				break;
			case "REFTEXT":
				$where_order = $idp_ref;
				break;
			case "NAME":
				$where_order = $idp_name.", ".$idp_id;
				break;
			default:
			case "ID":
				$where_order = $idp_id;
				break;
		}
		$ref_tag = $idpObject->getRefTag();
		if($page == "LOGIN_TABLE") {
			$order_by = $idp_name;
		}
		$where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
		$group_by = "";

//echo "<hr />".__LINE__." : ".$where;
//ASSIST_HELPER::arrPrint($final_data);
		if(count($options) > 0) {
			foreach($options as $key => $filter) {
				if(substr($key, 0, 3) != substr($this->getTableField(), 0, 3) && strrpos($key, ".") === FALSE) {
					$key = $this->getTableField()."_".$key;
				}
				$where .= " AND ".(strrpos($key, ".") === FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
			}
		}
//echo "<hr />".__LINE__." : ".$where;


		$all_headings = array_merge($idp_headings);

		$listObject = new SDBP6_LIST();

		foreach($all_headings as $fld => $head) {
			$lj_tblref = $head['section'];
			if($head['type'] == "MASTER") {
				$tbl = $head['list_table'];
				$masterObject = new SDBP6_MASTER($fld);
				$fy = $masterObject->getFields();
				$sql .= ", ".$fld.".".$fy['name']." as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
			} elseif($head['type'] == "USER") {
				$sql .= ", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
				$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
			}
		}
		$sql .= " FROM ".$from.implode(" ", $left_joins);
		$sql .= " WHERE ".$where; //echo "<h3>".$section." : $page : $where</h3>";
		switch($section) {
			case "ACTIVE":
				$sql .= " AND ".$idpObject->getActiveStatusSQL($idp_tblref);
				break;
			case "NEW":
				if($page == "ACTIVATE_WAITING") {
					$sql .= " AND ".$idpObject->getActivationStatusSQL($idp_tblref);
				} elseif($page == "ACTIVATE_DONE") {
					$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				} else {
					$sql .= " AND ".$idpObject->getNewStatusSQL($idp_tblref);
				}
				break;
			case "MANAGE":
				$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				if($page != "VIEW") {
					$adminObject = new SDBP6_SETUP_ADMINS();
					$admin_access = $adminObject->getMyAdminAccess();
					$p = explode("_", $page);
					$page_action = $p[0];
					$page_section = $p[1];
				} else {
					$page_action = $page;
					$page_section = $obj_type;
				}
				switch($page_action) {
					case "UPDATE":
					case "EDIT":
					case "ASSURANCE":
					case "APPROVE":
						$access_granted = false;
						$access_filter = array();
						$access = $admin_access['by_section'][$this->getAdminSectionIdentifier()][strtolower($page_action)];
						$department_field = $this->getDepartmentFieldName();
						$owner_field = $this->getOwnerFieldName();
						//ORG structure
						$org_access = ASSIST_HELPER::removeBlanksFromArray($access['ORG'], true, true);
						if(count($org_access) > 0) {
							$access_granted = true;
							//top layer only uses top level departments so can handle same as owner
							if($this->getAdminSectionIdentifier() == "top") {
								$access_filter[] = $department_field." IN (".implode(",", $org_access).")";
							} else {
								//otherwise get list of parents and convert to children
								$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
								$parent_departments = $orgObject->getListOfActiveParentsFormattedForSelect();
								//loop through each org_access item
								$check_list = $org_access;
								foreach($check_list as $ci) {
									//if it is set in the parent list
									if(isset($parent_departments[$ci])) {
										//get children ids and add into the org_access list
										$children = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => $ci));
										$org_access = array_merge($org_access, array_keys($children));
										//remove from org_access list
										//not required?  No object will have been assigned to the parent id so shouldn't impact the sql statement
										//= unnecessary overhead to process
									}
								}
								//double check the org_access count, just in case
								if(count($org_access) > 0) {
									$access_filter[] = $department_field." IN (".implode(",", $org_access).")";
								} else {
									$access_granted = false;
								}
							}
						}
						//Owner access only allowed on Update page
						if($page_action == "UPDATE" && isset($access['OWNER'])) {
							//Owner
							$owner_access = ASSIST_HELPER::removeBlanksFromArray($access['OWNER'], false, true);
							if(count($owner_access) > 0) {
								$access_granted = true;
								$access_filter[] = $owner_field." IN (".implode(",", $owner_access).")";
							}
						}
						//apply filter to remove all options if no access has been granted to any department or owner
						if($access_granted === false || count($access_filter) == 0) {
							$sql .= " AND ".$owner_field." < 0 AND ".$department_field."< 0";
						} else {
							$sql .= " AND (".implode(" OR ", $access_filter).")";
						}
						break;
					case "VIEW":
					case "DEFAULT":
					default:
						//do nothing for view page
				}
				break;
			case "ADMIN":
				if($is_edit_page) {
					$sql .= " AND (".$idpObject->getAdminEditStatusSQL($idp_tblref).")";
				} else {
					$sql .= " AND (".$idpObject->getReportingStatusSQL($idp_tblref).")";
				}
				break;
			case "REPORT":
			case "SEARCH":
				$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				break;
		}
		//$sql.= isset($parent_id) ? " AND ".$this->getParentFieldName()." = ".$parent_id : "";
		$sql .= isset($group_by) && strlen($group_by) > 0 ? $group_by : "";
		if(!$do_not_get_results_due_to_time_period_filter) {
			$mnr = 0;
		} else {
			$mnr = $this->db_get_num_rows($sql);
		}
		if($trigger_rows == true) {
			$start = ($start != false && is_numeric($start) ? $start : "0");
			$sql .= (strlen($order_by) > 0 ? " ORDER BY ".$order_by : " ORDER BY ".$where_order).($limit !== false ? " LIMIT ".$start." , $limit " : "");
//			echo $sql;			//return array($sql);
			//GET ROWS
			if(!$do_not_get_results_due_to_time_period_filter) {
				$rows = $this->mysql_fetch_all_by_id($sql, $id_fld);
			} else {
				$rows = array();
			}
			//REPLACE LIST, SEGMENT, OBJECT values with names
			$all_my_lists = array();
			//get replacement values
			foreach($all_headings as $fld => $head) {
				switch($head['type']) {
					case "MULTILIST":
					case "LIST":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$listObject = new SDBP6_LIST($head['list_table']);
							$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
							unset($listObject);
						}
						break;
					case "MULTIOBJECT":
					case "OBJECT":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$list_object_name = $head['list_table'];
							$extra_info = array();
							if(strpos($list_object_name, "|") !== false) {
								$lon = explode("|", $list_object_name);
								$list_object_name = $lon[0];
								$extra_info = $lon[1];
							}
							$listObject = new $list_object_name();
							$all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect($extra_info);
							unset($listObject);
						}
						break;
					case "MULTISEGMENT":
					case "SEGMENT":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$list_object_type = $head['list_table'];
							//Added passing of mSCOA version id to solve issue of segment data not displaying in list pages AA-554 JC 3 March 2021
							$listObject = new SDBP6_SEGMENTS($list_object_type, false, $mscoa_version_id);
							$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect(array());
							unset($listObject);
						}
						break;
				} //end switch by type
			}//end foreach heading
			//now loop through rows and replace values as needed
			foreach($rows as $key => $row) {
				foreach($row as $fld => $x) {
					//only treat as list if previously handled in check above && only act on field if required for list page i.e. set in headings list
					if(isset($all_my_lists[$fld]) && isset($all_headings[$fld])) {
						$head = $all_headings[$fld];
						//store display data in list_table element to preserve raw data in original fld element
						//formatting function (below) looks for display data in list_Table element for all list heading types (set in _HEADINGS->list_heading_types)
						$save_field = $head['list_table'];
						//if heading type contains MULTI - assume it is multi LIST SEGMENT or OBJECT
						if(strpos($head['type'], "MULTI") !== false) {
							if(strpos($head['type'], "OBJECT") !== false) {
								$blank_value = "";
							} else {
								$blank_value = $this->getUnspecified();
							}
							$x = explode(";", $x);
							$x = $this->removeBlanksFromArray($x);
							if(count($x) > 0) {
								$rows[$key][$save_field] = array();
								foreach($x as $y) {
									if(isset($all_my_lists[$fld][$y])) {
										$rows[$key][$save_field][] = $all_my_lists[$fld][$y];
									}
								}
								if(count($rows[$key][$save_field]) > 0) {
									$rows[$key][$save_field] = implode("; ", $rows[$key][$save_field]);
								} else {
									$rows[$key][$save_field] = $blank_value;
								}
							} else {
								$rows[$key][$save_field] = $blank_value;
							}
						} else {
							if($head['type'] == "OBJECT") {
								$blank_value = "";
							} else {
								$blank_value = $this->getUnspecified();
							}
							if(isset($all_my_lists[$fld][$x])) {
								$rows[$key][$save_field] = $all_my_lists[$fld][$x];
							} else {
								$rows[$key][$save_field] = $blank_value;
							}
						}//end if multi
					}//end if all_my_lists[fld] isset
				}//end foreach fld
				//test for baseline & new indicator fields - Set baseline = "N/A" if = 0 & new = yes [JC 25 August 2019 AA-171]
				if(isset($row['my_baseline']) && isset($row['new_indicator'])) {
					if($row['my_baseline'] * 1 == 0 && $row['new_indicator'] == 1) {
						$rows[$key][$idpObject->getBaselineTableField()] = $idpObject->replaceAllNames("N/A - New |kpi|");
					}
				}//endif baseline/new_indicator test
			}//end foreach rows
			$final_data = $this->formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, false, array('limit' => $limit, 'pagelimit' => $pagelimit, 'start' => $start));
		} else {
			$final_data['rows'] = array();
		}
		$final_data['head'] = $this->replaceObjectNames($final_data['head']);
		if($has_results && isset($final_data['results_head'])) {
			$final_data['results_head'] = $this->replaceObjectNames($final_data['results_head']);
		}

		return $final_data;
	}

	function formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, $status_id_fld, $paging) {
		$limit = $paging['limit'];
		$pagelimit = $paging['pagelimit'];
		$start = $paging['start'];
		//ASSIST_HELPER::arrPrint($rows);
		$keys = array_keys($rows);
		$displayObject = new SDBP6_DISPLAY();
		foreach($rows as $r) {
			$row = array();
			$i = $r[$id_fld];
			if(isset($r['target_type_id'])) {
				$row['target_type_id'] = $r['target_type_id'];
			}
			if(isset($r['calc_type_id'])) {
				$row['calc_type_id'] = $r['calc_type_id'];
			}
			if(isset($r['my_baseline'])) {
				$row['my_baseline'] = $r['my_baseline'];
			}
			if(isset($r['new_indicator'])) {
				$row['new_indicator'] = $r['new_indicator'];
			}
			$row['status'] = $r[$this->getStatusFieldName()];
			$row['is_active'] = ($row['status'] & self::ACTIVE) == self::ACTIVE;
			foreach($final_data['head'] as $fld => $head) {
				if($head['parent_id'] == 0 && isset($r[$fld])) {
					if($headObject->isListField($head['type'])) {
						//check for missing data done in getMyList - JC 6 Sep 2018
						//$row[$fld] = strlen($r[$head['list_table']])>0 ? $r[$head['list_table']] : $this->getUnspecified();
						$row[$fld] = $r[$head['list_table']];
						if($status_id_fld !== false && $fld == $status_id_fld) {
							if(($r['my_status'] & SDBP6::AWAITING_APPROVAL) == SDBP6::AWAITING_APPROVAL) {
								$row[$fld] .= " (Awaiting approval)";
							} elseif(($r['my_status'] & SDBP6::APPROVED) == SDBP6::APPROVED) {
								$row[$fld] .= " (Approved)";
							}
						}
					} elseif($this->isDateField($fld)) {
						$row[$fld] = $displayObject->getDataField("DATE", $r[$fld], array('include_time' => false));
					} elseif($head['type'] == "NUM" && $head['apply_formatting'] == true) {
						$row[$fld] = $displayObject->getDataField("TEXT", (isset($r[$fld]) ? $r[$fld] : $this->getUnspecified()), array('right' => true, 'html' => true, 'reftag' => $ref_tag));
					} else {
						$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld], array('right' => true, 'html' => true, 'reftag' => $ref_tag));
					}
				}
			}
			$final_data['rows'][$i] = $row;
		}

		if($mnr == 0 || $limit === false) {
			$totalpages = 1;
			$currentpage = 1;
		} else {
			$totalpages = round(($mnr / $pagelimit), 0);
			$totalpages += (($totalpages * $pagelimit) >= $mnr ? 0 : 1);
			if($start == 0) {
				$currentpage = 1;
			} else {
				$currentpage = round($start / $pagelimit, 0);
				$currentpage += (($currentpage * $pagelimit) > $start ? 0 : 1);
			}
		}
		$final_data['paging'] = array(
			'totalrows' => $mnr,
			'totalpages' => $totalpages,
			'currentpage' => $currentpage,
			'pagelimit' => $pagelimit,
			'first' => ($start == 0 ? false : 0),
			'prev' => ($start == 0 ? false : ($start - $pagelimit)),
			'next' => ($totalpages == $currentpage ? false : ($start + $pagelimit)),
			'last' => ($currentpage == $totalpages ? false : ($totalpages - 1) * $pagelimit),
		);
		return $final_data;
	}


	//public function getDetailedObject($obj_type,$id,$options=array()) {
	//die("getDetailedObject error");
	//}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}



	/***********************************
	 * GET functions
	 */

	/**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
	public function drawPageFooter($left = "", $log_table = "", $var = "") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
		echo "WRONG PAGE FOOTER";
		return "";
	}

	/**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
	public function getPageFooter($left = "", $log_table = "", $var = "") {
		/*    	$echo = "";
				$js = "";
				if(is_array($var)) {
					$x = $var;
					$d = array();
					unset($var);
					foreach($x as $f => $v) {
						$d[] = $f."=".$v;
					}
					$var = implode("&",$d);
				}
				$echo = "
				<table width=100% class=tbl-subcontainer><tr>
					<td width=50%>".$left."</td>
					<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
						<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
					</span>" : "")."</td>
				</tr></table><div id=div_audit_log></div>";
				if(strlen($log_table)>0){
					$js = "
					$(\"#disp_audit_log\").click(function() {
						var state = $(this).attr('state');
						if(state==\"show\"){
							$(this).find('img').prop('src','/pics/tri_down.gif');
							$(this).attr('state','hide');
							$(\"#div_audit_log\").html(\"\");
							$(\"#log_txt\").html('Display Activity Log');
						} else {
							$(this).find('img').prop('src','/pics/tri_up.gif');
							$(this).attr('state','show');
							var dta = '".$var."&log_table='+$(this).attr('table');
							var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
							$(\"#div_audit_log\").html(result[0]);
							$(\"#log_txt\").html('Hide Activity Log');
						}

					});
					";
				}
				$data = array('display'=>$echo,'js'=>$js);
				return $data;
		 **/
		return array('display' => "WRONG PAGE FOOTER", 'js' => "");
	}

	public function drawActivityLog() {

	}




	/*********************************************
	 * SET/UPDATE functions
	 */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new SDBP6_LOG($log_table);
	//	$logObject->addObject($var);
	//}


	public function notify($data, $type, $object) {
		$noteObj = new SDBP6_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
	}


	public function editMyObject($var, $extra_logs = array(), $section = false) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new SDBP6_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(), "FORM");

//Get Current SDBIP details in order to find out mSCOA Version AA-554 JC 4 March 2021
		$sdbip_details = $this->getCurrentSDBIPFromSessionData($section);
		if(isset($sdbip_details['mscoa_version_id'])) {
			$mscoa_version_id = $sdbip_details['mscoa_version_id'];
		} else {
			$mscoa_version_id = 1; //assume non-compliant / NA:1 if no version ID is available AA-554 JC 4 March 2021
		}

		$mar = 0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld => $v) {
			if(isset($headings['rows'][$fld])) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type'] == "DATE") {
					if(strlen($v) > 0) {
						$insert_data[$fld] = date("Y-m-d", strtotime($v));
					}
				} elseif(in_array($headings['rows'][$fld]['type'], array("TARGET", "COMPETENCY"))) {
					//unset($var[$fld]);
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}


		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
//$mar = 1;
		if($mar > 0 || count($extra_logs) > 0) {
			$changes = array(
				'user' => $this->getUserName(),
			);
			foreach($insert_data as $fld => $v) {
				$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type' => "TEXT");
				if(is_array($v)) {
					$v = implode(";", $v);
				}
				//need another way to run a comparative between numbers posing as strings
				//comparative after the decimal point is not trigger the usual $old!=$new test
				//and it can't be converted to a number in case it's a big float which PHP struggles with
				//AA-690 [JC] 3 Sep 2021
				$number_comparison_test = false;
				if($h['type']=="NUM") {
					$old_val = $old[$fld];
					$new_val = $v;
					if(strlen($old_val)!=strlen($new_val)) {
						$number_comparison_test = true;
					} else {
						for($i=0;$i<strlen($old_val);$i++) {
							$o = $old_val[$i];
							$n = $new_val[$i];
							if($o===$n || ($o=="." && $n==".") || ($o*1)===($n*1)) {
								$number_comparison_test = true;
								break;
							}
						}
					}
				}
				if(($fld != $this->getTableField()."_late_add" && $fld != $this->getTableField()."_late_edit") && ($number_comparison_test || $old[$fld] != $v || $h['type'] == "HEADING")) {
					if(in_array($h['type'], array("MULTILIST", "LIST", "MASTER", "USER", "SEGMENT", "MULTISEGMENT", "OBJECT", "MULTIOBJECT"))) {
						$list_items = array();
						$ids = array($v, $old[$fld]);
						switch($h['type']) {
							case "MULTILIST":
								$x = $ids;
								$ids = array();
								foreach($x as $a => $i) {
									$j = explode(";", $i);
									$j = ASSIST_HELPER::removeBlanksFromArray($j);
									if(count($j) > 0) {
										foreach($j as $k) {
											if(!in_array($k, $ids)) {
												$ids[] = $k;
											}
										}
									}
								}
							case "LIST":
								$listObject = new SDBP6_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new SDBP6_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new SDBP6_MASTER($h['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "MULTISEGMENT":
								$x = $ids;
								$ids = array();
								foreach($x as $a => $i) {
									$j = explode(";", $i);
									$j = ASSIST_HELPER::removeBlanksFromArray($j);
									if(count($j) > 0) {
										foreach($j as $k) {
											if(!in_array($k, $ids)) {
												$ids[] = $k;
											}
										}
									}
								}
							case "SEGMENT":
								//Send mscoa_version_id so that the MSCOA1 module knows which items to return instead of return the current blank array AA-554 JC 4 Mar 2021
								$listObject = new SDBP6_SEGMENTS($h['list_table'], false, $mscoa_version_id);
								$list_items = $listObject->getSelectedListItemsFormattedForSelect($ids);
								break;
							case "MULTIOBJECT":
								$x = $ids;
								$ids = array();
								foreach($x as $a => $i) {
									$j = explode(";", $i);
									$j = ASSIST_HELPER::removeBlanksFromArray($j);
									if(count($j) > 0) {
										foreach($j as $k) {
											if(!in_array($k, $ids)) {
												$ids[] = $k;
											}
										}
									}
								}
							case "OBJECT":
								$list_type = $h['list_table'];
								$list_object_name = $list_type;
								$extra_info = "";
								if(strpos($list_object_name, "|") !== false) {
									$lon = explode("|", $list_object_name);
									$list_object_name = $lon[0];
									$extra_info = $lon[1];
								}

								$listObject = new $list_object_name;
								$list_items = $listObject->getSelectedListItemsFormattedForSelect($ids);
								break;
						}
						unset($listObject);
						if($h['type'] == "MULTILIST" || $h['type'] == "MULTISEGMENT" || $h['type'] == "MULTIOBJECT") {
							$t = array();
							$u = ASSIST_HELPER::removeBlanksFromArray(explode(";", $v));
							if(count($u) == 0) {
								$to = "";
							} else {
								foreach($u as $z) {
									$t[] = $list_items[$z];
								}
								$to = implode("; ", $t);
							}
							$f = array();
							$g = ASSIST_HELPER::removeBlanksFromArray(explode(";", $old[$fld]));
							if(count($g) == 0) {
								$from = "";
							} else {
								foreach($g as $m) {
									$f[] = $list_items[$m];
								}
								$from = implode("; ", $f);
							}
						} else {
							$to = $v != 0 && isset($list_items[$v]) ? $list_items[$v] : "";
							$from = $old[$fld] != 0 && isset($list_items[$old[$fld]]) ? $list_items[$old[$fld]] : "";
						}
						$changes[$fld] = array('to' => $to, 'from' => $from, 'raw' => array('to' => $v, 'from' => $old[$fld]));
					} else {
						$changes[$fld] = array('to' => $v, 'from' => $old[$fld]);
					}
				}
			}
			$changes = array_merge($changes, $extra_logs);
			$log_var = array(
				'object_id' => $object_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been updated successfully.", array($log_var, $this->getMyObjectType()));
		}
		return array("info", "No changes were found to be saved.  Please try again.");
	}


	/***************************************************************************************************************************
	 * IMPORT FROM EXTERNAL MODULE functions
	 */


	public function getExternalImportRef($sdbip_details, $object_type) {
		//ref structure: FINYEAR|#_[localmodref]|#_[IDPmodref]|#_objectType
		$str = "FINYEAR|".$sdbip_details['fin_year_id']."_".$this->getModRef()."|".$sdbip_details['sdbip_id']."_".$sdbip_details['idp_module']."|".$sdbip_details['idp_id']."_".$object_type;
		return base64_encode($str);
	}


	public function getExternalImportRecord($ref) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_import_external_log 
				WHERE ref = '".$ref."' AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE." 
				ORDER BY id DESC LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		$this->external_import_record = $row;
		return $row;
	}

	public function setStatusOfExternalImportByRef($ref, $row = false, $object_type = false) {
		if($row === false) {
			$row = $this->getExternalImportRecord($ref);
		}
		if(is_array($row) && count($row) > 0) {
			$object_type = $row['object_type'];
			if(!isset($this->external_import_phases[$object_type])) {
				$this->external_import_phases[$object_type] = array(
					1 => 0,
					2 => 0,
					3 => 0,
					'status' => 0
				);
			}
			$this->external_import_phases[$object_type]['buttons'] = array(
				1 => false,
				2 => false,
				3 => false,
				'reset' => false,
				'map' => false,
			);
			$status = $row['status'];
			$this->external_import_phases[$object_type]['status'] = $status;
		} else {
			$this->external_import_phases[$object_type] = array(
				1 => 0,
				2 => 0,
				3 => 0,
				'status' => 0
			);
			$this->external_import_phases[$object_type]['buttons'] = array(
				1 => false,
				2 => false,
				3 => false,
				'reset' => false,
				'map' => false,
			);
			$status = 0;
		}
		if(($status & self::EXTERNAL_IMPORT_PHASE1_COMPLETE) == self::EXTERNAL_IMPORT_PHASE1_COMPLETE) {
			$this->external_import_phases[$object_type][1] = "done";
			if(($status & self::EXTERNAL_IMPORT_PHASE2_COMPLETE) == self::EXTERNAL_IMPORT_PHASE2_COMPLETE) {
				$this->external_import_phases[$object_type][2] = "done";
				if(($status & self::EXTERNAL_IMPORT_PHASE3_COMPLETE) == self::EXTERNAL_IMPORT_PHASE3_COMPLETE) {
					$this->external_import_phases[$object_type][3] = "done";
				} elseif(($status & self::EXTERNAL_IMPORT_PHASE3_BUSY) == self::EXTERNAL_IMPORT_PHASE3_BUSY) {
					$this->external_import_phases[$object_type][3] = "ip";
				} else {
					$this->external_import_phases[$object_type]['buttons'][3] = true;
				}
			} else {
				if(($status & self::EXTERNAL_IMPORT_PHASE2_BUSY) == self::EXTERNAL_IMPORT_PHASE2_BUSY) {
					$this->external_import_phases[$object_type][2] = "ip";
				} else {
					$this->external_import_phases[$object_type]['buttons'][2] = true;
					$this->external_import_phases[$object_type]['buttons']['reset'] = true;
				}
			}
		} else {
			if(($status & self::EXTERNAL_IMPORT_PHASE1_BUSY) == self::EXTERNAL_IMPORT_PHASE1_BUSY) {
				$this->external_import_phases[$object_type][1] = "ip";
			} else {
				$this->external_import_phases[$object_type]['buttons'][1] = true;
				$this->external_import_phases[$object_type]['buttons']['map'] = true;
			}
		}
	}

	public function getExternalImportButtonAvailability($object_type) {
		if(isset($this->external_import_phases[$object_type]['buttons'])) {
			return $this->external_import_phases[$object_type]['buttons'];
		} else {
			return array(
				1 => true,
				2 => false,
				3 => false,
				'reset' => false,
				'map' => true,
			);
		}
	}

	public function getStatusMessageForExternalImport($ref, $object_type) {
		$this->setStatusOfExternalImportByRef($ref, false, $object_type);
		if($this->external_import_phases[$object_type][1] == "0") {
			$message = "Please start the Import Process by clicking on Phase 1 button.";
		} elseif($this->external_import_phases[$object_type][1] == "ip") {
			$message = "Phase 1 is in progress.  Please check back again later.";
		} else {
			$message = "Phase 1 is complete.  ";
			if($this->external_import_phases[$object_type][2] == "0") {
				$message .= "Please continue with Phase 2.";
			} elseif($this->external_import_phases[$object_type][2] == "ip") {
				$message .= "Phase 2 is in progress.  Please check back again later.";
			} else {
				$message .= "Phase 2 is complete.  ";
				if($this->external_import_phases[$object_type][3] == "0") {
					$message .= "Please continue with Phase 3.";
				} elseif($this->external_import_phases[$object_type][3] == "ip") {
					$message .= "Phase 3 is in progress.  Please continue with Phase 3.";
				}
			}
		}
		return $message;
	}


	public function startExternalImportPhase1($sdbip_details, $object_type) {
		$ref = $this->getExternalImportRef($sdbip_details, $object_type);

		//insert record
		$insert_data = array(
			'sdbip_id' => $sdbip_details['id'],
			'object_type' => $object_type,
			'ref' => $ref,
			'status' => self::EXTERNAL_IMPORT_ACTIVE + self::EXTERNAL_IMPORT_PHASE1_BUSY,
			'insertuser' => $this->getUserID(),
			'insertusername' => $this->getUserName(),
			'insertdate' => date("Y-m-d H:i:s")
		);
		$sql = "INSERT INTO ".$this->getDBRef()."_import_external_log SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);

		return $ref;
	}

	public function endExternalImportPhase1($ref) {
		//update record: set status = status - 1_busy + 1_complete
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status - ".self::EXTERNAL_IMPORT_PHASE1_BUSY." + ".self::EXTERNAL_IMPORT_PHASE1_COMPLETE.") 
				WHERE ref = '$ref'
				AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE;
		$this->db_update($sql);
	}

	public function endExternalImportPhaseWithError($ref, $error) {
		//update record: set status = status + error
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status + ".self::EXTERNAL_IMPORT_ERROR."), notes = '".ASSIST_HELPER::code($error)."', ref = CONCAT('[',ref,']')
				WHERE ref = '$ref'";
		$this->db_update($sql);
	}

	public function resetExternalImportPhase($ref) {
		//update record: set status = status + error
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status - ".self::EXTERNAL_IMPORT_ACTIVE."), notes = 'RESET', ref = CONCAT('[',ref,']')
				WHERE ref = '$ref'";
		$this->db_update($sql);
	}

	public function startExternalImportPhase2($ref) {
		//update record: set status = status + 2_busy
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status + ".self::EXTERNAL_IMPORT_PHASE2_BUSY.") 
				WHERE ref = '$ref'
				AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE;
		$this->db_update($sql);
	}

	public function endExternalImportPhase2($ref) {
		//update record: set status = status - 2_busy + 2_complete
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status - ".self::EXTERNAL_IMPORT_PHASE2_BUSY." + ".self::EXTERNAL_IMPORT_PHASE2_COMPLETE.") 
				WHERE ref = '$ref'
				AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE;
		$this->db_update($sql);
	}

	public function startExternalImportPhase3($ref) {
		//update record: set status = status + 3_busy
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status + ".self::EXTERNAL_IMPORT_PHASE3_BUSY.") 
				WHERE ref = '$ref'
				AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE;
		$this->db_update($sql);
	}

	public function endExternalImportPhase3($ref) {
		//update record: set status = status - 3_busy + 3_complete
		$sql = "UPDATE ".$this->getDBRef()."_import_external_log 
				SET status = (status - ".self::EXTERNAL_IMPORT_PHASE3_BUSY." + ".self::EXTERNAL_IMPORT_PHASE3_COMPLETE.") 
				WHERE ref = '$ref'
				AND (status & ".self::EXTERNAL_IMPORT_ACTIVE.") = ".self::EXTERNAL_IMPORT_ACTIVE;
		$this->db_update($sql);
		//update all objects to make them accessible from create individual page?
	}

	public function isPhase2InProgress($var) {
		$ref = $var['import_ref'];
		$sql = "SELECT * FROM ".$this->getDBRef()."_import_external_log WHERE ref = '$ref'";
		$row = $this->mysql_fetch_one($sql);
		if(($row['status'] & self::EXTERNAL_IMPORT_PHASE2_BUSY) == self::EXTERNAL_IMPORT_PHASE2_BUSY) {
			return array("ip");
		} else {
			return array("done");
		}
	}


	/*****************************************************************************************************************************
	 * IMPORT / CONVERSION functions
	 */
	public function getFieldMap($modloc, $modref, $object_type) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_import_external_map 
				WHERE alt_modref = '$modref' 
				AND alt_modloc = '$modloc' 
				AND object_type = '".$object_type."' 
				AND status = ".self::ACTIVE."
				ORDER BY id LIMIT 1";
		$map = $this->mysql_fetch_one($sql);
		if(count($map) > 0 && isset($map['map']) && strlen($map['map']) > 0) {
			$map['map'] = unserialize(base64_decode($map['map']));
			return $map['map'];
		} else {
			return false;
		}
	}

	public function saveExternalMap($var) {
		$object_type = $var['object_type'];
		unset($var['object_type']);
		$modref = $var['modref'];
		unset($var['modref']);
		$modloc = $var['modloc'];
		unset($var['modloc']);
		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);
		$map = base64_encode(serialize($var));

		//disable existing maps (in case of edit)
		$sql = "UPDATE ".$this->getDBRef()."_import_external_map SET status = ".SDBP6::INACTIVE." WHERE alt_modref = '$modref' AND alt_modloc = '$modloc' AND object_type = '".$object_type."' AND sdbip_id = ".$sdbip_id;
		$this->db_update($sql);
		//save new map
		$sql = "INSERT INTO ".$this->getDBRef()."_import_external_map SET
				object_type = '$object_type'
				, sdbip_id = $sdbip_id
				, alt_modref = '$modref'
				, alt_modloc = '$modloc'
				, map = '$map'
				, status = ".SDBP6::ACTIVE."
				, insertuser = '".$this->getUserID()."'
				, insertdate = now()
				";
		$this->db_insert($sql);
		//return result
		return array("ok", "Map saved successfully.");
	}

	public function getListItemMapping($modref) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_import_external_links
				WHERE src_modref = '".$modref."' AND
				status = ".SDBP6::ACTIVE." ORDER BY local_list, local_id";
		$rows = $this->mysql_fetch_all($sql);

		$data = array();
		foreach($rows as $r) {
			if(!isset($data[$r['local_list']])) {
				$data[$r['local_list']] = array();
			}
			if(!isset($data[$r['local_list']][$r['src_list']][$r['src_id']])) {
				$data[$r['local_list']][$r['src_list']][$r['src_id']] = $r['local_id'];
			}
		}
		return $data;
	}

	public function createNewListItemMapping($old_modref, $new_list_table, $old_list_table, $original_value, $new_value) {
		$sql = "INSERT INTO ".$this->getDBRef()."_list_import_external_links
				SET local_id = $new_value
				, local_list = '$new_list_table'
				, src_modref = '$old_modref'
				, src_id = $original_value
				, src_list = '$old_list_table'
				, status = ".SDBP6::ACTIVE."
				, insertuser = '".$this->getUserID()."'
				, insertdate = now()
		";
		$this->db_insert($sql);
	}

	//get lists needed for mapping when converting from IDP / SDBP5B
	public function getListItemsForExternalMapping($field_map, $field_settings, $format_for_comparison = true, $mscoa_version = "NA") {
		$new_lists = array();
		$listObject = new SDBP6_LIST();
		$listObject->setSDBIPID($this->sdbip_id);
		foreach($field_map as $old_field => $new_field) { //echo "<h2 class=red>".$old_field."::".$new_field."::".$field_settings[$new_field]['type']."</h2>";
			if($new_field != "0" && $new_field != false && isset($field_settings[$new_field]['type'])) {
				$list = $field_settings[$new_field]['list'];
				switch($field_settings[$new_field]['type']) {
					case "LIST":
					case "MULTILIST":
						$new_lists[$list] = array();
						$listObject->changeListType($list);
						$data = $listObject->getAllListItemsFormattedForSelect();
						break;
					case "MULTISEGMENT":
					case "SEGMENT":
						$segmentObject = new SDBP6_SEGMENTS($list);
						$data = $segmentObject->getAllListItemsFormattedForSelect(array('version' => $mscoa_version));  //echo "<h1>".$list."</h1>"; ASSIST_HELPER::arrPrint($data);
						unset($segmentObject);
						break;
					case "OBJECT":
					case "MULTIOBJECT":
						if(strpos($list, "|") !== false) {
							$lon = explode("|", $list);
							$list = $lon[0];
							$extra_info = $lon[1];
						} else {
							$extra_info = "";
						}
						$objObject = new $list($this->local_modref);
						$objObject->setSDBIPID($this->sdbip_id);
						$data = $objObject->getAllListItemsFormattedForSelect($extra_info);
						unset($objObject);
						break;
				} //echo $list; ASSIST_HELPER::arrPrint($data);
				if(isset($data) && count($data) > 0) {
					foreach($data as $id => $value) {
						if($format_for_comparison === true) {
							$v = $this->formatTextForComparison($value);
						} else {
							$v = $value;
						}
						$new_lists[$list][$v] = $id;
					}
					unset($data);
				} else {
					$new_lists[$list] = array();
				}
			} else {
				//do nothing if field is not in use
			}
		}
		unset($listObject);

		return $new_lists;

	}


	/**
	 * Specific to each child object
	 *
	 * public function getAltModuleTable($modloc) {
	 *
	 * }
	 * public function getAltModuleIDField($modloc,$modref="") {
	 *
	 * }
	 */


	public function setExternalMappingVariables($extObject, $field_map, $old_field_settings, $new_field_settings, $list_map, $old_lists, $new_lists) {
		$this->external_mapping_variables = array(
			'map' => $field_map,
			'old_settings' => $old_field_settings,
			'new_settings' => $new_field_settings,
			'list_map' => $list_map,
			'old_lists' => $old_lists,
			'new_lists' => $new_lists,
		);
		$this->external_mapping_object = $extObject;
	}


	/***************************************************************************************************************************
	 * Shared Children functions
	 */


	public function getSimpleDetails($id = 0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'parent_id' => $obj[$this->getParentFieldName()],
			'id' => $id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"]) > 0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}


	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t = "") {
		//Contracts where
		//status = active and
		//status <> deleted
		return $this->getStatusSQL("ALL", $t, false);
	}

	public function getInactiveStatusSQL($t = "") {
		//Contracts where
		//status = active and
		//status <> deleted
		return $this->getInactiveStatusSQLForReport("ALL", $t, true);
	}

	public function getInactiveAndDeletedStatusSQL($t = "") {
		//Contracts where
		//status <> active and
		//status = deleted
		//OR
		//status = deleted
		return $this->getInactiveAndDeletedStatusSQLForReport("ALL", $t, true);
	}

	public function getReportingStatusSQL($t = "") {
		//Contracts where
		//activestatussql and
		//status = confirmed and
		//status = activated
		return $this->getStatusSQL("REPORT", $t, false, false);
	}

	public function getAdminEditStatusSQL($t = "") {
		//Contracts where
		//activestatussql and
		//status = confirmed and
		//status = activated
		return $this->getStatusSQL("ADMIN_EDIT", $t, false, false);
	}

	public function getResultsActiveStatusSQL($t = "", $tbl_field = "") {
		//Contracts where
		//status = active and
		//status <> deleted
		if(strlen($tbl_field) == 0) {
			$tbl_field = $this->getResultsTableField();
		}
		return $this->getStatusSQL("ALL", $t, false, false, $tbl_field);
	}

	public function getResultsReportingStatusSQL($t = "", $tbl_field = "") {
		//Contracts where
		//activestatussql and
		//status = confirmed and
		//status = activated
		return $this->getStatusSQL("REPORT", $t, false, false, $tbl_field);
	}


	public function getCountOfObjectsToBeImported($parent_id) {
		$sql = "SELECT count(".$this->getIDFieldName().") as count FROM ".$this->getTableName()." O WHERE O.".$this->getParentFieldName()." = ".$parent_id." AND ".$this->getActiveStatusSQL("O");
		$records = $this->mysql_fetch_one_value($sql, "count");
		return $records;
	}

	public function getRecordsForImportIntoNewSDBIP($parent_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." O WHERE O.".$this->getParentFieldName()." = ".$parent_id." AND ".$this->getActiveStatusSQL("O");
		$records = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());

		if(count($records) > 0) {
			$ids = array_keys($records);
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $ids).")";
			$results = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
			if(count($results) > 0) {
				foreach($results as $parent_id => $res) {
					$records[$parent_id]['targets'] = array();
					$t = 0;
					$o = 0;//used to detect period changes between targets & increment $t accordingly
					foreach($res as $time_id => $r) {
						//if o=0 then you're at the start of the loop so assume time period 1
						if($o == 0) {
							$t = 1;
							$o = $time_id;
							//else increment it by the difference between the current time_id & the previous one (to catch MONTH vs QUARTER)
						} else {
							$t += ($time_id - $o);
							$o = $time_id;
						}
						$records[$parent_id]['targets'][$t] = $r[$this->getResultsOriginalFieldName()];
					}
				}
			} else {
				foreach($records as $id => $record) {
					$records[$id]['targets'] = array();
				}
			}
		}


		return $records;
	}


	public function recordImportOfListItemFromLocalModule($new_list_id, $source_sdbip_id, $old_list_id, $local_list, $src_list) {
		$insert_data = array(
			'local_id' => $new_list_id,
			'local_list' => $local_list,
			'src_modref' => self::MODULE_CODE."_".$source_sdbip_id,
			'src_id' => $old_list_id,
			'src_list' => $src_list,
			'status' => self::ACTIVE,
			'insertuser' => $this->getUserID(),
			'insertdate' => date("Y-m-d H:i:s"),
		);
		$sql = "INSERT INTO ".$this->getDBRef()."_list_import_external_links SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);
	}

	public function getCopiedItemsFromSDBIPSetup($source_sdbip_id, $new_ids, $local_list) {
		$data = array();
		if(count($new_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_list_import_external_links 
				WHERE status = ".self::ACTIVE." 
				AND local_list = '$local_list' 
				AND src_modref = '".self::MODULE_CODE."_".$source_sdbip_id."'
				AND local_id IN (".implode(",", $new_ids).")";
			$data = $this->mysql_fetch_value_by_id($sql, "src_id", "local_id");
		}
		return $data;
	}

	/*******************************************
	 * PRIVATE functions
	 */


	/***********************
	 * WARNING!!!! HARD CODING MSCOA VERSION FOR EXPEDIENCY AA-327 JC 30 March 2020
	 */
	protected function convertMSCOAVersionCodeToID($mscoa_version_code = "NA") {
//		$listObject = new MSCOA1_LIST("version",$this->mscoa_mod_ref);
//				$version_row = $listObject->getRawObject("code = '".$mscoa_version_code."'");
//				$version = $version_row['id'];
		switch($mscoa_version_code) {
			case "6.2":
				return 2;
				break;
			case "6.3":
				return 3;
				break;
			case "6.4":
				return 4;
				break;
			case "6.5":
				return 5;
				break;
			case "6.6":
				return 6;
				break;
			case "NA":
			default:
				return 1;
				break;
		}
	}

	protected function convertMSCOAVersionIDToCode($mscoa_version_code = "NA") {
		switch($mscoa_version_code) {
			case 2:
				return "6.2";
				break;
			case 3:
				return "6.3";
				break;
			case 4:
				return "6.4";
				break;
			case 1:
			default:
				return "NA";
				break;
		}
	}


	public function __destruct() {
		parent::__destruct();
	}


}


?>