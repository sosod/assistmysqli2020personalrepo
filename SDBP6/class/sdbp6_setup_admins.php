<?php
/**
 * To manage the user access of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_ADMINS extends SDBP6 {

	private $table_name = "_setup_admins";

	private $organisational_top_level_only_sections = array("top");

	/**
	 * field_name section = OWNER for Responsible Person
	 **/
	private $field_names = array(
		'dept' => array(
			'name' => "|deptsdbip|",
			'access' => array(
				'update' => "|update|",
				'edit' => "|edit|",
				'create' => "|create|",
				'approve' => "|approve|",
				'assurance' => "|assurance|",
				'emails' => "Receive |assurance| Notifications",
			)
		),
		'top' => array(
			'name' => "|topsdbip|",
			'access' => array(
				'update' => "|update|",
				'edit' => "|edit|",
				'create' => "|create|",
				'approve' => "|approve|",
				'assurance' => "|assurance|",
				'emails' => "Receive |assurance| Notifications",
			)
		),
		'project' => array(
			'name' => "|projects|",
			'access' => array(
				'update' => "|update| Comments",
			)
		),
	);

	private $field_defaults = array(
		'dept' => array(
			'access' => array(
				'update' => 1,
				'edit' => 0,
				'create' => 0,
				'approve' => 0,
				'assurance' => 0,
				'emails' => 0,
			)
		),
		'top' => array(
			'access' => array(
				'update' => 1,
				'edit' => 0,
				'create' => 0,
				'approve' => 0,
				'assurance' => 0,
				'emails' => 0,
			)
		),
		'project' => array(
			'access' => array(
				'update' => 0,
			)
		),
	);

	private $field_glossary = array(
		'dept' => array(
			'access' => array(
				'update' => "User has access to |update| associated |deptkpis| in |manage_update|",
				'edit' => "User has access to |edit| associated |deptkpis| in |manage_edit|",
				'create' => "User has access to |create| associated |deptkpis| in |manage_create|",
				'approve' => "User has access to |approve| associated |deptkpis| in |manage_approve|",
				'assurance' => "User has access to perform |assurance| on associated |deptkpis| in |manage_assurance|",
				'emails' => "User will receive copies of emails generated due to |assurance| on associated |deptkpis|",
			)
		),
		'top' => array(
			'name' => "|topsdbip|",
			'access' => array(
				'update' => "User has access to |update| associated |topkpis| in |manage_update|",
				'edit' => "User has access to |edit| associated |topkpis| in |manage_edit|",
				'create' => "User has access to |create| associated |topkpis| in |manage_create|",
				'approve' => "User has access to |approve| associated |topkpis| in |manage_approve|",
				'assurance' => "User has access to perform |assurance| on associated |topkpis| in |manage_assurance|",
				'emails' => "User will receive copies of emails generated due to |assurance| on associated |topkpis|",
			)
		),
		'project' => array(
			'name' => "|projects|",
			'access' => array(
				'update' => "User has access to add performance comments to associated |projects| in |manage_update|",
			)
		),
	);

	private $contents_bar = array();

	private $applicable_sections = array('dept' => SDBP6_DEPTKPI::OBJECT_TYPE, 'top' => SDBP6_TOPKPI::OBJECT_TYPE, 'project' => SDBP6_PROJECT::OBJECT_TYPE);


	const ADMIN_YES = 32;
	const ADMIN_NO = 64;

	//differentiate between RESPONSIBLE_OWNER/ORGSTRUCTURE object_types and ADMINISTRATOR object_type
	const CLASS_OBJECT_TYPE = "ADMINOWNER";
	const CLASS_OBJECT_TYPE_PLURAL = "ADMINOWNERS";


	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);
		$this->field_names = $this->replaceAllNames($this->field_names);
		$this->fields_glossary = $this->replaceAllNames($this->field_glossary);
		$this->table_name = $this->getDBRef().$this->table_name;

		$active_sections = $this->active_sections;
		foreach($this->applicable_sections as $key => $section) {
			if(!in_array($section, $active_sections)) {
				unset($this->field_names[$key]);
				unset($this->fields_glossary[$key]);
				unset($this->field_defaults[$key]);
			}
		}
	}

	/**
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");
		$admin_tkid = $var['admin_tkid'];
		$admin_orgid = $var['parent_id'];
		$admin_type = $var['object_type'];
		$sections = $this->getFieldNames($admin_type);
		$sdbip_id = $var['sdbip_id'];

		if($admin_type == "OWNER") {
			$orgObject = new SDBP6_LIST("owner");
			$org_name = $orgObject->getAListItemName($admin_orgid);
			$object_type = "OWNER";
		} else {
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_name = $orgObject->getAObjectName($admin_orgid);
			$object_type = "ORGSTRUCTURE";
		}
		//$result[1].=$admin_tkid."=".$admin_orgid."-".$var['object_type']."=".serialize($sections);

		$c = 0;

		foreach($sections as $admin_section => $section) {
			foreach($section['access'] as $admin_access => $a_name) {
				if(isset($var[$admin_section][$admin_access]) && $var[$admin_section][$admin_access] == true) {
					$import_data = array(
						'admin_tkid' => $admin_tkid,
						'admin_type' => $admin_type,
						'admin_section' => $admin_section,
						'admin_access' => $admin_access,
						'admin_orgid' => $admin_orgid,
						'admin_status' => self::ACTIVE + self::ADMIN_YES,
					);
					$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($import_data);
					$this->db_insert($sql);
					$c++;
				}
			}
		}
		if($c == 0) {
			$result = array("info", "No access was found to be saved.  Please note that you do not need to complete this section for a user with no access, only if you need to grant access.");
		} else {
			/**
			 * LOG HERE
			 */

			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Granted ".$this->getAUserName($admin_tkid)." |".self::CLASS_OBJECT_TYPE."| access to ".$org_name.".",
			);
			$log_var = array(
				'sdbip_id' => $sdbip_id,
				'section' => "ADMIN_".$admin_type,
				'object_id' => 0,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);


			$result = array("ok", "New ".$this->getObjectName(self::CLASS_OBJECT_TYPE)." created successfully.");
		}

		return $result;
	}

	public function editObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.".serialize($var));
		$sdbip_id = $var['sdbip_id'];
		$admin_tkid = $var['object_id'];
		$admin_orgid = $var['parent_id'];
		$admin_type = $var['object_type'];
		$sections = $this->getFieldNames($admin_type);

		if($admin_type == "OWNER") {
			$orgObject = new SDBP6_LIST("owner");
			$org_name = $orgObject->getAListItemName($admin_orgid);
			$object_type = "OWNER";
		} else {
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_name = $orgObject->getAObjectName($admin_orgid);
			$object_type = "ORGSTRUCTURE";
		}

		$old_row = $this->getAdminDetails($admin_type, $admin_tkid, $admin_orgid);

		if(!isset($old_row[$admin_orgid][$admin_tkid])) {
			$result = array("error", "The ".$this->getObjectName(self::CLASS_OBJECT_TYPE)." settings could not be found to be edited.  Please reload the page and try again.");
		} else {

			$old_row = $old_row[$admin_orgid][$admin_tkid];

			$c_raw = array();
			$c_new = array();
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|".self::CLASS_OBJECT_TYPE."| access to $org_name for ".$old_row['name']." modified:",
			);
			$c_count = 0;
			$changes_found = false;
			$sql_data = array();
			foreach($sections as $admin_section => $section) {
				$s_name = $this->field_names[$admin_section]['name'];
				foreach($section['access'] as $admin_access => $a_name) {
					$a_name = $this->field_names[$admin_section]['access'][$admin_access];
					if(isset($var[$admin_section][$admin_access])) {
						$new = $var[$admin_section][$admin_access];
						if(isset($old_row[$admin_section][$admin_access])) {
							$old = $old_row[$admin_section][$admin_access];
							if($new != $old) {
								$changes_found = true;
								$c[$admin_section][$admin_access] = array(
									'to' => $new,
									'from' => $old,
								);
								$changes[$s_name." - ".$a_name] = array(
									'to' => $new == true ? "Yes" : "No",
									'from' => $old == true ? "Yes" : "No",
									'raw' => $c,
								);

								$status = self::ACTIVE + ($new == true ? self::ADMIN_YES : self::ADMIN_NO);
								$sql = "UPDATE ".$this->getTableName()." SET admin_status = $status WHERE admin_tkid = '$admin_tkid' AND admin_type = '$admin_type' AND admin_section = '$admin_section' AND admin_access = '$admin_access' AND admin_orgid = ".$admin_orgid;
								$this->db_update($sql);
								$c_count++;
							}
						} else {
							if($new != false) {
								$changes_found = true;
								$c[$admin_section][$admin_access] = array(
									'to' => $new,
									'from' => 0,
								);
								$changes[$s_name." - ".$a_name] = array(
									'to' => $new == true ? "Yes" : "No",
									'from' => "No",
									'raw' => $c,
								);
								$status = self::ACTIVE + ($new == true ? self::ADMIN_YES : self::ADMIN_NO);
								$sql = "INSERT INTO ".$this->getTableName()." SET admin_status = $status, admin_tkid = '$admin_tkid', admin_type = '$admin_type', admin_section = '$admin_section', admin_access = '$admin_access', admin_orgid = ".$admin_orgid;
								$this->db_insert($sql);
								$c_count++;
							}
						}
					}
				}
			}

			if($changes_found == true) {
				$log_var = array(
					'sdbip_id' => $sdbip_id,
					'section' => "ADMIN_".$admin_type,
					'object_id' => 0,
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
				$result = array("ok", $this->getObjectName(self::CLASS_OBJECT_TYPE)." modified successfully.");
			} else {
				$result = array("info", "No changes found to be saved. Please reload the page and try again.");
			}

		}

		return $result;
	}


	public function deactivateObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$sdbip_id = $var['sdbip_id'];
		$admin_tkid = $var['object_id'];
		$admin_orgid = $var['parent_id'];
		$admin_type = $var['object_type'];

		if($admin_type == "OWNER") {
			$orgObject = new SDBP6_LIST("owner");
			$org_name = $orgObject->getAListItemName($admin_orgid);
			$object_type = "OWNER";
		} else {
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_name = $orgObject->getAObjectName($admin_orgid);
			$object_type = "ORGSTRUCTURE";
		}

		$old = $this->getAdminDetails($admin_type, $admin_tkid, $admin_orgid);

		if($admin_type == "OWNER") {
			$sql = "UPDATE ".$this->getTableName()." SET admin_status = admin_status - ".self::ACTIVE." WHERE admin_tkid = '$admin_tkid' AND admin_type = '$admin_type' AND admin_orgid = $admin_orgid";
		} else {
			$sql = "UPDATE ".$this->getTableName()." SET admin_status = admin_status - ".self::ACTIVE." WHERE admin_tkid = '$admin_tkid' AND admin_type = '$admin_type' AND admin_orgid = $admin_orgid";
		}
		$this->db_update($sql);

		/**
		 * LOG
		 */
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "Removed |".self::CLASS_OBJECT_TYPE."| access to ".$org_name." for ".$this->getAUserName($admin_tkid).".",
			'raw' => $old,
		);
		$log_var = array(
			'sdbip_id' => $sdbip_id,
			'section' => "ADMIN_".$admin_type,
			'object_id' => 0,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DEACTIVATE,
		);
		$this->addActivityLog("setup", $log_var);


		$result = array("ok", $this->getObjectName(self::CLASS_OBJECT_TYPE)." ".$this->getActivityName("deactivated")." successfully.");


		return $result;
	}


	public function restoreObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$sdbip_id = $var['sdbip_id'];
		$admin_tkid = $var['object_id'];
		$admin_orgid = $var['parent_id'];
		$admin_type = $var['object_type'];

		if($admin_type == "OWNER") {
			$orgObject = new SDBP6_LIST("owner");
			$org_name = $orgObject->getAListItemName($admin_orgid);
			$object_type = "OWNER";
		} else {
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_name = $orgObject->getAObjectName($admin_orgid);
			$object_type = "ORGSTRUCTURE";
		}

		$old = $this->getAdminDetails($admin_type, $admin_tkid, $admin_orgid);

		if($admin_type == "OWNER") {
			$sql = "UPDATE ".$this->getTableName()." SET admin_status = admin_status + ".self::ACTIVE." WHERE admin_tkid = '$admin_tkid' AND admin_type = '$admin_type' AND admin_orgid = $admin_orgid";
		} else {
			$sql = "UPDATE ".$this->getTableName()." SET admin_status = admin_status + ".self::ACTIVE." WHERE admin_tkid = '$admin_tkid' AND admin_type = '$admin_type' AND admin_orgid = $admin_orgid";
		}
		$this->db_update($sql);

		/**
		 * LOG
		 */
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|restored| |".self::CLASS_OBJECT_TYPE."| access to ".$org_name." for ".$this->getAUserName($admin_tkid).".",
			'raw' => $old,
		);
		$log_var = array(
			'sdbip_id' => $sdbip_id,
			'section' => "ADMIN_".$admin_type,
			'object_id' => 0,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::RESTORE,
		);
		$this->addActivityLog("setup", $log_var);


		$result = array("ok", $this->getObjectName(self::CLASS_OBJECT_TYPE)." ".$this->getActivityName("restored")." successfully.");


		return $result;
	}


	/**
	 * GET functions
	 */
	public function getTableName() {
		return $this->table_name;
	}

	public function getFieldGlossary() {
		return $this->field_glossary;
	}

	public function getFieldNames($type = "ORG") {
		if($type == "ORG" || strlen($type) == 0) {
			return $this->field_names;
		} else {
			$names = $this->field_names;
			foreach($names as $sec => $section) {
				foreach($section['access'] as $access => $a) {
					if($access != "update") {
						unset($names[$sec]['access'][$access]);
					}
				}
			}
			return $names;
		}
	}

	public function getFieldDefaults() {
		return $this->field_defaults;
	}

	public function getTopLevelOnlySections() {
		return $this->organisational_top_level_only_sections;
	}

	/**
	 * Get the list of users with access
	 */
	public function getAdminsForSetup($object_type = "ORG", $admin_tkid = "", $org_id = 0) {
		$sql = "SELECT *, CONCAT(tkname, ' ',tksurname) as admin_name
				FROM ".$this->getTableName()."
				INNER JOIN ".$this->getUserTableName()."
				ON tkid = admin_tkid AND tkstatus = 1
				INNER JOIN assist_".strtolower($this->getCmpCode())."_menu_modules_users
				ON tkid = usrtkid AND usrmodref = '".strtoupper($this->getModRef())."'
				WHERE admin_type = '$object_type'";
		$filter = array();
		if(strlen($admin_tkid) > 0) {
			$filter[] = "admin_tkid = '$admin_tkid'";
		}
		if(ASSIST_HELPER::checkIntRef($org_id)) {
			$filter[] = "admin_orgid = ".$org_id;
		}
		$sql .= (count($filter) > 0 ? " AND " : "").implode(" AND ", $filter)."
				ORDER BY tkname, tksurname, tkid"; //echo $sql;
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $row) {
			$org_id = $row['admin_orgid']; //echo "<p>".$org_id;
			$tkid = $row['admin_tkid'];
			$name = $row['admin_name'];
			$section = $row['admin_section'];
			$access = $row['admin_access'];
			$status = $row['admin_status'];
			if(!isset($data[$org_id][$tkid][$section])) {
				$data[$org_id][$tkid][$section] = array($access => (($status & self::ADMIN_YES) == self::ADMIN_YES));
				$data[$org_id][$tkid]['name'] = $name;
				//not sure if I need total status but putting it in just in case
				$data[$org_id][$tkid]['status'] = (($status & self::ACTIVE) == self::ACTIVE);
			} else {
				$data[$org_id][$tkid][$section][$access] = (($status & self::ADMIN_YES) == self::ADMIN_YES);
				//not sure if I need total status but putting it in just in case - if one element is not active then all elements for that department are not active
				if((($status & self::ACTIVE) != self::ACTIVE)) {
					$data[$org_id][$tkid]['status'] = false;
				}
			}
		}
		return $data;
	}

	/**
	 * Get list of users who can be admins and who aren't currently admins to the given parent_id
	 */
	public function getAvailableAdminsForSetup($type, $parent_id) {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM ".$this->getUserTableName()."
				INNER JOIN assist_".strtolower($this->getCmpCode())."_menu_modules_users
				ON tkid = usrtkid AND usrmodref = '".strtoupper($this->getModRef())."'
				WHERE tkstatus = 1
				AND tkid NOT IN (SELECT admin_tkid FROM ".$this->getTableName()." WHERE admin_orgid = ".$parent_id." AND ".($type == "OWNER" ? "admin_type = '$type'" : "admin_type <> 'OWNER'").")
				ORDER BY tkname, tksurname, tkid";
		$rows = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $rows;
	}

	/**
	 * Get Admin details for edit in setup
	 */
	public function getAdminDetails($object_type, $admin_tkid, $org_id) {
		return $this->getAdminsForSetup($object_type, $admin_tkid, $org_id);
	}

	/*	public function getActiveUsers($ids="") {
			$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, UA.*
					FROM assist_".$this->getCmpCode()."_timekeep TK
					INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
					ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
					INNER JOIN ".$this->getTableName()." UA
					ON ua_tkid = tkid
					WHERE tkstatus = 1
					";
			if(is_array($ids)) {
				$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
			} elseif(strlen($ids)>0) {
				$sql.=" AND TK.tkid = '".$ids."' ";
			}
			$sql.= "
					ORDER BY tkname, tksurname";
			return $this->mysql_fetch_all_by_id($sql, "tkid");
				//return $sql;
		}
		public function getActiveUsersFormattedForSelect($ids="") {
			$rows = $this->getActiveUsers($ids);
			$data = $this->formatRowsForSelect($rows);
			return $data;
			//return $rows;
		}
		/**
		 * Get the current user's access
		 * Ignore SDBIP/financial year for now as it will slow down the process (inner joining tables) AA-540 JC 17 March 2021
		 */
	public function getMyAdminAccess($ui = "") {
		$ui = strlen($ui) == 0 ? $this->getUserID() : $ui;
		$sql = "SELECT * FROM ".$this->getTableName()." UA
				WHERE admin_tkid = '".$ui."'
				AND (admin_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (admin_status & ".self::ADMIN_YES.") = ".self::ADMIN_YES."
				"; //echo $sql;
		$rows = $this->mysql_fetch_all($sql); //ASSIST_HELPER::arrPrint($rows);
		$user_access = array(
			'by_type' => array(),
			'by_section' => array(),
			'by_page' => array(),
		);
		foreach($rows as $r) {
			$type = $r['admin_type'];
			$section = $r['admin_section'];
			$page = $r['admin_access'];
			$id = $r['admin_orgid'];
			$user_access['by_section'][$section][$page][$type][$id] = $id;
			if($type == "ORG") {
				$user_access['by_section'][$section][$page]['SUB'][$id] = $id;
			}
		}
		//ASSIST_HELPER::arrPrint($user_access);
		return $user_access;
	}


	/**
	 * Get all Users who have UPDATE access to a KPI - for Approve/Assurance
	 */
	public function getUpdateUsersForReview($section, $dept_id, $owner_id, $update_user) {
		//OWNER
		/* GET ALL USERS WHO:
		 *   admin_type = OWNER
		 *   admin_id = given owner id
		 *   admin_status is active & yes
		 *   admin access = update
		 *   admin section =
		 *   user status is active
		 *   user has menu access to the module
		 */
		$sql = "SELECT CONCAT(tkname,' ',tksurname,IF(tkid='".$update_user."','*','')) as name, tkid as id
				FROM ".$this->getTableName()."
				INNER JOIN assist_".$this->getCmpCode()."_timekeep
				  ON tkid = admin_tkid AND tkstatus = 1 AND tkemail <> ''
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				  ON tkid = usrtkid AND usrmodref = '".$this->getModRef()."'
				WHERE admin_type = 'OWNER'
				AND admin_orgid = $owner_id
				AND admin_access = 'update'
				AND admin_section = '$section'
				AND (admin_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (admin_status & ".self::ADMIN_YES.") = ".self::ADMIN_YES."
				ORDER BY tkname, tksurname";
		$owners = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//ORG
		/* FIRST check if given dept is a SUB in which case find it's parent who might also have an admin assigned */
		$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
		$parent_id = $orgObject->getMyParentID($dept_id);
		$dept_id = array($dept_id);
		if($parent_id != 0) {
			$dept_id[] = $parent_id;
		}
		/* THEN GET ALL USERS WHO:
		 *   admin_type = ORG
		 *   admin_id IN array(org_id)
		 *   admin_status is active & yes
		 *   admin access = update
		 *   admin section =
		 *   user status is active
		 *   user has menu access to the module
		 */
		$sql = "SELECT CONCAT(tkname,' ',tksurname,IF(tkid='".$update_user."','*','')) as name, tkid as id
				FROM ".$this->getTableName()."
				INNER JOIN assist_".$this->getCmpCode()."_timekeep
				  ON tkid = admin_tkid AND tkstatus = 1 AND tkemail <> ''
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				  ON tkid = usrtkid AND usrmodref = '".$this->getModRef()."'
				WHERE admin_type = 'ORG'
				AND admin_orgid IN (".implode(",", $dept_id).")
				AND admin_access = 'update'
				AND admin_section = '$section'
				AND (admin_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (admin_status & ".self::ADMIN_YES.") = ".self::ADMIN_YES."
				ORDER BY tkname, tksurname";
		$depts = $this->mysql_fetch_value_by_id($sql, "id", "name");
		$admins = $owners + $depts;
		asort($admins);
		return $admins;
	}


	/**
	 * Get all users who should receive additional notifications on Approve/Assurance
	 */
	public function getEmailUsersForReview($section, $dept_id) {
		//OWNER
		//Not applicable to Email notifications
		//ORG
		/* FIRST check if given dept is a SUB in which case find it's parent who might also have an admin assigned */
		$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
		$parent_id = $orgObject->getMyParentID($dept_id);
		$dept_id = array($dept_id);
		if($parent_id != 0) {
			$dept_id[] = $parent_id;
		}
		/* THEN GET ALL USERS WHO:
		 *   admin_type = ORG
		 *   admin_id IN array(org_id)
		 *   admin_status is active & yes
		 *   admin access = emails
		 *   admin section = ??
		 *   user status is active
		 *   user has menu access to the module
		 */
		$sql = "SELECT CONCAT(tkname,' ',tksurname) as name, tkid as id
				FROM ".$this->getTableName()."
				INNER JOIN assist_".$this->getCmpCode()."_timekeep
				  ON tkid = admin_tkid AND tkstatus = 1 AND tkemail <> ''
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				  ON tkid = usrtkid AND usrmodref = '".$this->getModRef()."'
				WHERE admin_type = 'ORG'
				AND admin_orgid IN (".implode(",", $dept_id).")
				AND admin_access = 'emails'
				AND admin_section = '$section'
				AND (admin_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (admin_status & ".self::ADMIN_YES.") = ".self::ADMIN_YES."
				ORDER BY tkname, tksurname";
		$admins = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $admins;
	}


	/**
	 * Get dept, top & project ORG & OWNER settings where user has UPDATE access and ORG is not PARENT (excl TOP)
	 */
	public function getMyUpdateAccessForFrontPageDashboard($sdbip_ids = array()) {
		//first get sdbip_id
		if(is_array($sdbip_ids) && count($sdbip_ids) == 0) {
			$sdbipObject = new SDBP6_SDBIP($this->local_modref);
			$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
			if(isset($sdbip['id'])) {
				$sdbip_ids[] = $sdbip['id'];
			}
		} elseif(!is_array($sdbip_ids)) {
			$sdbip_ids = array($sdbip_ids);
		}
		$top_sec = SDBP6_TOPKPI::ADMIN_SECTION;
		$my_access = array();
		$admin_access = $this->getMyAdminAccess($this->getUserID()); //No SDBIP_IDs as it will slow down the process - have to inner join tables

		$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
		$parents = $orgObject->getListOfActiveParentsFormattedForSelect($sdbip_ids);

		foreach($admin_access['by_section'] as $sec => $access) {
			$my_access[$sec] = array();
			if(isset($access['update'])) {
				foreach($access['update'] as $up_sec => $up_access) {
					if($up_sec == "OWNER") {
						$my_access[$sec][$up_sec] = $up_access;
					} elseif($up_sec == "ORG") {
						if($sec != $top_sec) {
							foreach($up_access as $i => $a) {
								if(isset($parents[$i])) {
									$my_access[$sec]['PARENT_ORG'][$i] = $a;    //need to keep track of top level org structure admin access as KPIs can be Assurance/Approve rejected to a user & it won't show on the FPD without it (FPD only shows SUB level) - AA-540 JC 17 March 2021
									unset($up_access[$i]);
								}
							}
						}
						$my_access[$sec][$up_sec] = $up_access;
					}
				}
			}
		}
		return $my_access;
	}


	/**
	 * Get list of users who have update access and might need to receive reminder emails - user status doesn't matter as UserAccess check will deal with that
	 */
	public function getUsersWithUpdateAccessForReminderEmails() {
		$sql = "SELECT DISTINCT admin_tkid as tkid
				FROM ".$this->getTableName()."
				WHERE admin_access = 'update'
				AND (admin_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (admin_status & ".self::ADMIN_YES.") = ".self::ADMIN_YES."
				";
		$admins = $this->mysql_fetch_value_by_id($sql, "tkid", "tkid");
		return $admins;
	}


	public function copySDBIPSpecificSetup($org_ids, $owner_ids) {
		//ORG STRUCTURE
		if(count($org_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE admin_type = 'ORG' AND admin_orgid IN (".implode(",", array_keys($org_ids)).") AND (admin_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." ";
			$source_rows = $this->mysql_fetch_all($sql);
			if(count($source_rows) > 0) {
				foreach($source_rows as $row) {
					unset($row['admin_id']);
					//replace source id with destination id
					$row['admin_orgid'] = $org_ids[$row['admin_orgid']];
					$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($row);
					$this->db_insert($sql);
				}
			}
		}
		//OWNERS
		if(count($owner_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE admin_type <> 'ORG' AND admin_orgid IN (".implode(",", array_keys($owner_ids)).") AND (admin_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." ";
			$source_rows = $this->mysql_fetch_all($sql);
			if(count($source_rows) > 0) {
				foreach($source_rows as $row) {
					unset($row['admin_id']);
					//replace source id with destination id
					$row['admin_orgid'] = $owner_ids[$row['admin_orgid']];
					$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($row);
					$this->db_insert($sql);
				}
			}
		}
		return true;
	}


}


?>