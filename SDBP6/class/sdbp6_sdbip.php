<?php
/**
 * To manage the functions of the core SDBIP object of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SDBIP extends SDBP6 {

//which generic idp modules, by MODLOC, that can be linked to this module
	private $idp_module_versions_that_can_be_linked = array("IDP3");
//which idp modules, by MODREF, exist on the client database which can be linked to this module
	private $idp_modules_that_can_be_linked = array();
//which idp module, by MODREF, has been linked to this module
	private $linked_idp_module = "";
//which specific idp has been linked to this specific SDBIP
	private $linked_idp = 0;
//which generic sdbip modules, by MODLOC, that can be linked to this module
	private $sdbip_module_versions_that_can_be_linked = array("SDBP5B");
//which idp modules, by MODREF, exist on the client database which can be linked to this module
	private $sdbip_modules_that_can_be_linked = array();
//available sdbip objects SDBP6 - AA-327
	private $sdbp6_sdbips = array();
//which sdbp5b module is permitted to be converted (all objects, updates & logs converted)
	//private $sdbip_module_that_can_be_converted = "SDP18"; - removed JC 2018-10-29
//which SDBIP module statuses can be imported/converted
	private $sdbip_module_status_that_can_be_used = array("T", "Y");
//which sections / pages can operate even when there is no activated SDBIP
	private $no_activated_sdbip_pages = array("new", "setup", "form");


//object details
	protected $object_id = 0;
	protected $object_details = array();

//table settings
	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = false;
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $has_attachment = false;
	protected $ref_tag = "SDP";


//Module constants
	const OBJECT_TYPE = "SDBIP";
	const OBJECT_NAME = "SDBIP";

	const TABLE = "sdbip";
	const TABLE_FLD = "sdbip";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "sdbip";


	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
//Added -5 to time() because the call to the doesActivatedSDBIPExist function happened too soon after this and didn't trigger the db call with $force turned off - JC 3 April 2020 #AA-371
		if(!isset($_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT'])) {
			$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT'] = array(
				'details' => array(),
				'timestamp' => (time() - 5),
				'status' => false,
			);
		}
		if(!isset($_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT'])) {
			$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT'] = array(
				'details' => array(),
				'timestamp' => (time() - 5),
				'status' => false,
			);
		}
	}


	/**
	 * SDBP6_SDBIP->addObject = Function to create a new SDBIP in the database for the given financial year
	 * (1) Get Financial Year details from ASSIST_MASTER_FINANCIALYEARS for passed $var[fin_year_id]
	 * (2) Create Time Periods depending on the start/end date of the financial year
	 * (3) Create SDBIP database record with status = NEW
	 * CANCELLED - User must first decide which to import on Import from IDP pages (4) If linked to IDP then call on IDP3 module to import Projects and Top Layer KPIs
	 * (5) Determine redirect url: If IDP then go to New > Create > Projects > Import from IDP else go to New > Create > Projects > Manual Upload
	 */
	public function addObject($var) {
		$result = array("info", "Sorry, I got lost.  Please try again. [Error code: ".base64_encode("SDBP6_SDBIP.addObject->".serialize($var))."]");
		$final_step = $var['step'];                //what was the final step?
		$step_history = $var['step_history'];    //what process did the user follow when creating this SDBIP
		$fin_year_id = $var['fin_year_id'];        //which financial year is this SDBIP linked to?
		$name = $var['sdbip_name'];                //what is the SDBIP name - kept in place in case multiple SDBIPS in same module is implemented
		$idp_module = $var['idp_module'];        //is this SDBIP linked to an IDP module?
		$idp_id = $var['idp_id'];                //if the SDBIP is linked to an IDP module, which IDP within that module is it linked to?
		$sdbip_alt_module = $var['sdbp5b_allowed'] == true ? $var['sdbp5b_module'] : $var['sdbp6_sdbip'];            //is this SDBIP linked to a prior SDBIP module or current SDBIP module?
		$sdbip_alt_action = "IMPORT";            //if the SDBIP is linked, what is the link action?  CONVERT or IMPORT?
		$mscoa_compliant = $var['mscoa_yesno'];
		$mscoa_version = $var['mscoa_version_code'];
		$copy_setup = $var['copy_setup'];

		//new SDBIP settings
		$save_details = array(
			$this->getTableField().'_fin_year_id' => $fin_year_id,
			$this->getTableField().'_name' => $name,
			$this->getTableField().'_idp_module' => $idp_module,
			$this->getTableField().'_idp_id' => $idp_id,
			$this->getTableField().'_alt_module' => $sdbip_alt_module,
			$this->getTableField().'_alt_action' => $sdbip_alt_action,
			$this->getTableField().'_mscoa_compliant' => $mscoa_compliant,
			$this->getTableField().'_mscoa_version' => $mscoa_version,
			$this->getTableField().'_finteg' => "MANUAL",
			$this->getTableField().'_copy_setup' => $copy_setup,
			$this->getTableField().'_creation_step_history' => $step_history,
			$this->getTableField().'_status' => SDBP6::ACTIVE + SDBP6::SDBIP_NEW,
			$this->getTableField().'_insertuser' => $this->getUserID(),
			$this->getTableField().'_insertdate' => date("Y-m-d H:i:s"),
		);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($save_details);
		$sdbip_id = $this->db_insert($sql);

		//get Financial Year details
		$fyObject = new ASSIST_MASTER_FINANCIALYEARS();
		$fin_year_details = $fyObject->fetch("", "", $fin_year_id);

		//create time periods for selected financial year
		$timeObject = new SDBP6_SETUP_TIME();
		$time_var = $fin_year_details[$fin_year_id];
		$time_var['type'] = "year";
		$time_var['sdbip_id'] = $sdbip_id;
		$time_result = $timeObject->addObject($time_var);

		if($time_result[0] == "error") {
			$result = $time_result;
			$this->cancelSDBIPCreationOnStep1($sdbip_id);
		} else {
			//if chosen to link to prior module for CONVERT
			if($sdbip_alt_module != "FALSE_LINK" && $sdbip_alt_action == "CONVERT") {
				$this->changeSDBIPmodStatusToConverted($sdbip_alt_module);
			}
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$sdbip_id." |created|.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $sdbip_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			$result = array("ok", $this->getObjectName("SDBIP")." '".$name."' [Ref: ".$this->getRefTag().$sdbip_id."] has been created successfully.");
			/*if($idp_module=="FALSE_LINK" && $sdbip_alt_module=="FALSE_LINK") {
				$result[2] = "new_create_projects_import.php?sdbip_id=".$sdbip_id;
			} elseif($idp_module=="FALSE_LINK") {
				$result[2] = "new_create_projects_sdbip.php?sdbip_id=".$sdbip_id;
			} else {
				$result[2] = "new_create_projects_idp.php?sdbip_id=".$sdbip_id;
			}*/
			//$result[2] = "new_create_projects_create.php?sdbip_id=".$sdbip_id;
			$result[2] = $this->whatIsMyNextStepInCreatingMySDBIP(array(), "", $sdbip_alt_module, $sdbip_id);
		}
		$result[3] = $sdbip_id;
		//REMOVED - Handled by external page to speed up process
		/*if($copy_setup==true) {
			$copy_result = $this->copySetup($sdbip_id);
		} else {
			$copy_result = "Blank |sdbip| setup was created.  Please go to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_setup_sdbip"),"|"))." to do the necessary setups before continuing.";
		}
		$result[1].=" ".$copy_result;
		*/


		return $result;
	}


	private function changeSDBIPmodStatusToConverted($sdbip_modref) {
		$my_modref = $this->getModRef();
		$my_cmpcode = $this->getCmpCode();
		//get old record for loggin
		$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$sdbip_modref."'";
		$row = $this->mysql_fetch_one($sql);
		//if the prior module selected is still active then close as "converTed"
		if($row['modyn'] != "T") {
			//convert status code to T = Convert
			$sql1 = "UPDATE assist_menu_modules SET modyn = 'T', modadminyn = 'T' WHERE modref = '".$sdbip_modref."'";
			$this->db_update($sql1);
			//log in central module_activity_log
			$mDB = new ASSIST_DB("master");
			$sql = "INSERT INTO assist_client_module_activity_log VALUES (null, '$my_cmpcode','$sdbip_modref','".$row['modloc']."','$my_cmpcode','".$this->getUserID()."','".$this->getUserName()."','T',now(),2)";
			$mDB->db_insert($sql);
			unset($mDB);
			//log in old SDBIP activity log
			$sql = "INSERT INTO ".strtolower("assist_".$my_cmpcode."_".$sdbip_modref."_log")." VALUES (null,'".$this->getUserID()."',".time().",'','CONVERT','Closed module for conversion to new module version',0,'".base64_encode($sql1)."')";
			$this->db_insert($sql);
		}
	}


	private function whatIsMyNextStepInCreatingMySDBIP($details = array(), $idp_module = "", $sdbip_module = "", $sdbip_id = 0) {
		/*	//get idp_module to set path
			if(strlen($idp_module)>0) {
				//if passed directly, do nothing
			} elseif(isset($details['sdbip_idp_module'])) {
				$idp_module = $details['sdbip_idp_module'];
			} elseif(isset($details['idp_module'])) {
				$idp_module = $details['idp_module'];
			} else {
				$idp_module = "FALSE_LINK";
			}
			//get SDBIP module to set path if IDP is FALSE_LINK / 0
			if($idp_module=="FALSE_LINK" || $idp_module==0) {
				if(strlen($sdbip_module)>0) {
					//if passed directly, do nothing
				} elseif(isset($details['sdbip_alt_module'])) {
					$sdbip_module = $details['sdbip_alt_module'];
				} elseif(isset($details['alt_module'])) {
					$sdbip_module = $details['alt_module'];
				} else {
					$sdbip_module = "FALSE_LINK";
				}
			}
			//get sdbip_id to select sdbip - needed for later variants which might allow for multiple sdbips in same module
			if(ASSIST_HELPER::checkIntRef($sdbip_id)) {
				//if passed directly, do nothing
			} elseif(isset($details['sdbip_id'])) {
				$sdbip_id = $details['sdbip_id'];
			} elseif(isset($details['id'])) {
				$sdbip_id = $details['id'];
			} else {
				$sdbip_id = 0;
			}
			//set redirect path
			if($idp_module=="FALSE_LINK" && $sdbip_module=="FALSE_LINK") {
				return "new_create_projects_import.php".($sdbip_id>0?"?sdbip_id=".$sdbip_id:"");
			} elseif($sdbip_module=="FALSE_LINK") {
				return "new_create_projects_idp.php".($sdbip_id>0?"?sdbip_id=".$sdbip_id:"");
			} else {
				return "new_create_projects_sdbip.php".($sdbip_id>0?"?sdbip_id=".$sdbip_id:"");
			}*/

		if(strlen($sdbip_module) > 0) {
			//if passed directly, do nothing
		} elseif(isset($details['sdbip_alt_module'])) {
			$sdbip_module = $details['sdbip_alt_module'];
		} elseif(isset($details['alt_module'])) {
			$sdbip_module = $details['alt_module'];
		} else {
			$sdbip_module = "FALSE_LINK";
		}
		if($sdbip_module == "FALSE_LINK") {
			if($this->is_full_module !== true) {
				return "new_create_dept_create.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			} else {
				return "new_create_projects_create.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			}
		} elseif($this->checkIntRef($sdbip_module)) {
			if($this->is_full_module !== true) {
				return "new_create_dept_sdbp6.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			} else {
				return "new_create_projects_sdbp6.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			}
		} else {
			if($this->is_full_module !== true) {
				return "new_create_dept_sdbip.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			} else {
				return "new_create_projects_sdbip.php".($sdbip_id > 0 ? "?sdbip_id=".$sdbip_id : "");
			}
		}


	}


	/**
	 * Confirm SDBIP
	 */
	public function confirmObject($var) {
		$object_id = $var['object_id'];
		$id = $object_id;
		$status = self::SDBIP_CONFIRMED + self::ACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET
				".$this->getStatusFieldName()." = $status
				, ".$this->getTableField()."_confirmuser = '".$this->getUserID()."'
				, ".$this->getTableField()."_confirmdate = now()
				WHERE ".$this->getIDFieldName()." = $object_id ";
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$object_id." |confirmed|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Confirmed",
				'from' => "New",
				'raw' => array(
					'to' => self::SDBIP_CONFIRMED,
					'from' => self::SDBIP_NEW,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::CONFIRM,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		$this->hasSDBIPBeenCreatedAndNotYetActivated(true);
		return array("ok", $this->getObjectName(self::OBJECT_TYPE)." ".$this->getActivityName("confirmed")." successfully.");

	}


	/**
	 * Activate SDBIP
	 */
	public function activateObject($var) {
		$object_id = $var['object_id'];
		$id = $object_id;
		$status = self::SDBIP_ACTIVATED + self::ACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET
				".$this->getStatusFieldName()." = $status
				, ".$this->getTableField()."_activationuser = '".$this->getUserID()."'
				, ".$this->getTableField()."_activationdate = now()
				WHERE ".$this->getIDFieldName()." = $object_id ";
		$this->db_update($sql);

		//Log update
		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |activated|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Activated",
				'from' => "Confirmed",
				'raw' => array(
					'to' => self::SDBIP_ACTIVATED,
					'from' => self::SDBIP_CONFIRMED,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::ACTIVATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		/** save copy of SDBIP into history **/
		//insert activation_log record
		$sql = "INSERT INTO ".$this->getTableName()."_activation_log VALUES (null, $id, '".$this->getUserID()."',now())";
		$activation_log_id = $this->db_insert($sql);
		//create files name
		$file_name = "_parent_".$id."_activation_".$activation_log_id."_".date("YmdHis");
		//check that folder path exists
		$path = $this->getModRef()."/history";
		$this->checkFolder($path);

		$active_sections = $this->getActiveSections();
		/**  deptkpi **/
		if(in_array(SDBP6_DEPTKPI::OBJECT_TYPE,$active_sections)) {
			//save data
			$local_filename = SDBP6_DEPTKPI::OBJECT_TYPE.$file_name;
			$childObject = new SDBP6_DEPTKPI($this->getModRef(), 0, true);    //send external call=true to speed up process
			$files = $childObject->saveActivatedObjectForHistory($id, $local_filename, $path);
			unset($childObject);
			//insert activation_files record
			$insert_data = array();
			foreach($files as $fname) {
				$insert_data[] = "(null, $activation_log_id, '".SDBP6_DEPTKPI::OBJECT_TYPE."','".$path."/".$fname."')";
			}
			if(count($insert_data) > 0) {
				$sql = "INSERT INTO ".$this->getTableName()."_activation_files VALUES ".implode(",", $insert_data);
				$this->db_insert($sql);
			}
		}
		/**  topkpi */
		if(in_array(SDBP6_TOPKPI::OBJECT_TYPE,$active_sections)) {
			//save data
			$local_filename = SDBP6_TOPKPI::OBJECT_TYPE.$file_name;
			$childObject = new SDBP6_TOPKPI($this->getModRef(), 0, true);    //send external call=true to speed up process
			$files = $childObject->saveActivatedObjectForHistory($id, $local_filename, $path);
			unset($childObject);
			//insert activation_files record
			$insert_data = array();
			foreach($files as $fname) {
				$insert_data[] = "(null, $activation_log_id, '".SDBP6_TOPKPI::OBJECT_TYPE."','".$path."/".$fname."')";
			}
			if(count($insert_data) > 0) {
				$sql = "INSERT INTO ".$this->getTableName()."_activation_files VALUES ".implode(",", $insert_data);
				$this->db_insert($sql);
			}
		}
		/** project */
		if(in_array(SDBP6_PROJECT::OBJECT_TYPE,$active_sections)) {
			//save data
			$local_filename = SDBP6_PROJECT::OBJECT_TYPE.$file_name;
			$childObject = new SDBP6_PROJECT($this->getModRef(), 0, true);    //send external call=true to speed up process
			$files = $childObject->saveActivatedObjectForHistory($id, $local_filename, $path);
			unset($childObject);
			//insert activation_files record
			$insert_data = array();
			foreach($files as $fname) {
				$insert_data[] = "(null, $activation_log_id, '".SDBP6_PROJECT::OBJECT_TYPE."','".$path."/".$fname."')";
			}
			if(count($insert_data) > 0) {
				$sql = "INSERT INTO ".$this->getTableName()."_activation_files VALUES ".implode(",", $insert_data);
				$this->db_insert($sql);
			}
		}
		$this->hasSDBIPBeenCreatedAndNotYetActivated(true);
		$this->doesActivatedSDBIPExist(true);
		$this->getYearFilterOptions(true);//force update of filter_by drop down


		return array("ok", $this->getObjectName(self::OBJECT_TYPE)." ".$this->getActivityName("activated")." successfully.  Please note that it can take up to 30 minutes for the new ".$this->getObjectName(self::OBJECT_TYPE)." to become available to all users who are currently logged in.");

	}

	/**
	 * Activate SDBIP
	 */
	public function undoActivateObject($var) {
		$object_id = $var['object_id'];
		$id = $object_id;
		$status = self::SDBIP_NEW + self::ACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET
				".$this->getStatusFieldName()." = $status
				, ".$this->getTableField()."_confirmuser = ''
				, ".$this->getTableField()."_confirmdate = '0000-00-00 00:00:00'
				, ".$this->getTableField()."_activationuser = ''
				, ".$this->getTableField()."_activationdate = '0000-00-00 00:00:00'
				WHERE ".$this->getIDFieldName()." = $object_id ";
		$this->db_update($sql);

		$changes = array(
			'response' => "Prior |activation| of |".$this->getMyObjectName()."| ".$this->getRefTag().$id." reversed.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "New",
				'from' => "|activated|",
				'raw' => array(
					'to' => self::SDBIP_NEW,
					'from' => self::SDBIP_ACTIVATED,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::UNDOACTIVATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);


		$this->hasSDBIPBeenCreatedAndNotYetActivated(true);
		$this->doesActivatedSDBIPExist(true);
		return array("ok", "Action completed successfully.");

	}


	/**
	 * RESET all objects relating to specific SDBIP: Called from New > Create > RESET
	 */
	public function resetObject($var) {
		$object_id = is_array($var) ? $var['object_id'] : $var;
		//get current status
		$sql = "SELECT ".$this->getStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$old_status = $this->mysql_fetch_one_value($sql, $this->getStatusFieldName());
		if(($old_status & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED) {
			$old_status = self::SDBIP_ACTIVATED;
			$status_text = "|activated|";
		} elseif(($old_status & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED) {
			$old_status = self::SDBIP_CONFIRMED;
			$status_text = "|confirmed|";
		} elseif(($old_status & self::SDBIP_NEW) == self::SDBIP_NEW) {
			$old_status = self::SDBIP_NEW;
			$status_text = "|new|";
		} else {
			$old_status = $old_status; //dunno what this status is so record as is
			$status_text = "|active|";
		}
		//reset projects
		$myObject = new SDBP6_PROJECT();
		$projects = $myObject->resetObject($object_id, true);
		unset($myObject);
		//reset top
		$myObject = new SDBP6_TOPKPI();
		$tops = $myObject->resetObject($object_id, true);
		unset($myObject);
		//reset dept
		$myObject = new SDBP6_DEPTKPI();
		$depts = $myObject->resetObject($object_id, true);
		unset($myObject);
		//reset self
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE." + ".self::SDBIP_RESET.") WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." AND ".$this->getIDFieldName()." = ".$object_id;
		$active = $this->db_update($sql);
		if($active > 0) {

			$id = $object_id;
			$changes = array(
				'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |deleted|.",
				'user' => $this->getUserName(),
				$this->getStatusFieldName() => array(
					'to' => "|deleted|",
					'from' => $status_text,
					'raw' => array(
						'to' => self::SDBIP_RESET,
						'from' => $old_status,
					),
				),
			);
			$log_var = array(
				'object_id' => $id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);


			//force reset of session
			$this->doesActivatedSDBIPExist(true);
			$this->hasSDBIPBeenCreatedAndNotYetActivated(true);
			//notify users
			$response = "Reset completed successfully.  ".$projects.", ".$tops.", ".$depts." removed.";
			return array("ok", $response);
		} else {
			return array("error", "An error occurred while trying to ".strtolower($this->getActivityName("RESET"))." this ".$this->getObjectName(self::OBJECT_TYPE)." Please wait for the page to reload and try again.");
		}
	}


	/*************************************************************************
	 * General functions
	 */
	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_TYPE;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}


	public function getSummaryOfChildren($object_id) {
		$data = array(
			SDBP6_PROJECT::OBJECT_TYPE => array('count' => 0, 'updates_done' => false, 'converted' => 0, 'sdbp6ed' => 0),
			SDBP6_TOPKPI::OBJECT_TYPE => array('count' => 0, 'updates_done' => false, 'converted' => 0, 'sdbp6ed' => 0),
			SDBP6_DEPTKPI::OBJECT_TYPE => array('count' => 0, 'updates_done' => false, 'converted' => 0, 'sdbp6ed' => 0),
		);
		$myObject = new SDBP6_PROJECT();
		$field = SDBP6_PROJECT::OBJECT_TYPE;
		$data[$field] = $myObject->getStats($object_id);
		unset($myObject);
		$myObject = new SDBP6_TOPKPI();
		$field = SDBP6_TOPKPI::OBJECT_TYPE;
		$data[$field] = $myObject->getStats($object_id);
		unset($myObject);
		$myObject = new SDBP6_DEPTKPI();
		$field = SDBP6_DEPTKPI::OBJECT_TYPE;
		$data[$field] = $myObject->getStats($object_id);
		unset($myObject);

		return $data;
	}


	/****************************************************
	 * Functions to manage status of SDBIP
	 */

	public function getSDBIPByID($sdbip_id = false, $external_call = false) {
		$sql = "SELECT * FROM ".$this->getTableName()." ".($sdbip_id !== false ? " WHERE ".$this->getIDFieldName()."=".$sdbip_id."" : "");
		$row = $this->mysql_fetch_one($sql);
		//add mscoa fields, just in case working in an outdated database that doesn't have A-147 update from 11 Feb 2020
		if(!isset($row['sdbip_mscoa_compliant'])) {
			$row['sdbip_mscoa_compliant'] = 0;
		}
		if(!isset($row['sdbip_mscoa_version'])) {
			$row['sdbip_mscoa_version'] = "NA";
		}
		if(!isset($row['sdbip_finteg'])) {
			$row['sdbip_finteg'] = "MANUAL";
		}
		//otherwise assume that something exists then update details and set status = true
		foreach($row as $fld => $val) {
			$f = explode("_", $fld);
			unset($f[0]);
			$key = implode("_", $f);
			$row[$key] = $val;
		}

		$row['ref'] = $this->getRefTag().$row['id'];
		$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
		$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
		$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);
		$row['is_confirmed'] = (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED) || $row['is_activated'];

		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$fin = $masterObject->getAListItem($row['fin_year_id']);
		$row['fin_year_name'] = isset($fin['name']) ? $fin['name'] : $fin['value'];
		$row['fin_year_ref'] = $fin['fin_ref'];

		if(!$external_call) {
			$row['summary_details'] = $this->getSummaryOfChildren($row['id']);
		}

		if($row['alt_module'] != "FALSE_LINK") {
			$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['alt_module']."'";
			$alt = $this->mysql_fetch_one($sql);
			$row['alt_module_name'] = $alt['modtext'];
			$row['alt_module_settings'] = $alt;
		}
		$row['mscoa_version_id'] = $this->convertMSCOAVersionCodeToID($row['mscoa_version']);
		return $row;
	}

	public function doesActivatedSDBIPExist($force = false, $external_call = false, $sdbip_id = false) { //$force=true;
		//session variable is set in __construct
		//if OBJECT.timestamp < now then recheck the database && update details
		if($force || $sdbip_id !== false || !isset($_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['timestamp']) || $_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['timestamp'] < time()) {
			$sql = "SELECT * FROM ".$this->getTableName()." 
					WHERE  ".($sdbip_id !== false ? $this->getIDFieldName()."=".$sdbip_id." AND " : "")." ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." 
					AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." ORDER BY ".$this->getIDFieldName()." ASC LIMIT 1";
			$row = $this->mysql_fetch_one($sql);
			//if nothing received from DB then reset details to nothing (in case previously activated SDBIP has been cancelled)
			if(!(is_array($row) && count($row) > 0)) {
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['details'] = array();
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['status'] = false;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['timestamp'] = time() + (30 * 60);    //30 minutes - increased check from 5 min to 30 min AA-447
				return false;

			} else {
				//add mscoa fields, just in case working in an outdated database that doesn't have A-147 update from 11 Feb 2020
				if(!isset($row['sdbip_mscoa_compliant'])) {
					$row['sdbip_mscoa_compliant'] = 0;
				}
				if(!isset($row['sdbip_mscoa_version'])) {
					$row['sdbip_mscoa_version'] = "NA";
				}
				if(!isset($row['sdbip_finteg'])) {
					$row['sdbip_finteg'] = "MANUAL";
				}
				//otherwise assume that something exists then update details and set status = true
				foreach($row as $fld => $val) {
					$f = explode("_", $fld);
					unset($f[0]);
					$key = implode("_", $f);
					$row[$key] = $val;
				}

				$row['ref'] = $this->getRefTag().$row['id'];
				$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
				$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
				$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);
				$row['is_confirmed'] = (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED) || $row['is_activated'];

				$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
				$fin = $masterObject->getAListItem($row['fin_year_id']);
				$row['fin_year_name'] = isset($fin['name']) ? $fin['name'] : $fin['value'];
				$row['fin_year_ref'] = $fin['fin_ref'];

				if(!$external_call) {
					$row['summary_details'] = $this->getSummaryOfChildren($row['id']);
				}

				if($row['alt_module'] != "FALSE_LINK") {
					$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['alt_module']."'";
					$alt = $this->mysql_fetch_one($sql);
					$row['alt_module_name'] = $alt['modtext'];
					$row['alt_module_settings'] = $alt;
				}
				$row['mscoa_version_id'] = $this->convertMSCOAVersionCodeToID($row['mscoa_version']);
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['details'] = $row;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['status'] = true;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['timestamp'] = time() + (5 * 60);    //5 minutes
				return true;
			}
		} else {
			return $_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['status'];
		}
	}

	public function getCurrentActivatedSDBIPDetails($external_call = false, $sdbip_id = false) {
		$is_activated_exist = $this->doesActivatedSDBIPExist(false, $external_call, $sdbip_id);
		if($is_activated_exist) {
			return $_SESSION[$this->getModRef()][$this->getMyObjectType()]['OBJECT']['details'];
		} else {
			return array();
		}
	}

	public function getAllActivatedSDBIPDetails($external_call = false) {
		return $this->getFormattedActivatedSDBIPDetails($external_call, false, false);
	}

	public function getFormattedActivatedSDBIPDetails($external_call = false, $sdbip_id = false, $limit = 1) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE  ".($sdbip_id !== false ? $this->getIDFieldName()."=".$sdbip_id." AND " : "")." ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." 
				AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." ORDER BY ".$this->getIDFieldName()." ASC ".($limit !== false ? " LIMIT ".$limit : "");
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		if(count($rows) > 0) {
			foreach($rows as $i => $row) { //echo "<hr /><h1 class='green'>Helllloooo $i from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

				//add mscoa fields, just in case working in an outdated database that doesn't have A-147 update from 11 Feb 2020
				if(!isset($row['sdbip_mscoa_compliant'])) {
					$row['sdbip_mscoa_compliant'] = 0;
				}
				if(!isset($row['sdbip_mscoa_version'])) {
					$row['sdbip_mscoa_version'] = "NA";
				}
				if(!isset($row['sdbip_finteg'])) {
					$row['sdbip_finteg'] = "MANUAL";
				}
				//otherwise assume that something exists then update details and set status = true
				foreach($row as $fld => $val) {
					$f = explode("_", $fld);
					unset($f[0]);
					$key = implode("_", $f);
					$row[$key] = $val;
				}

				$row['ref'] = $this->getRefTag().$row['id'];
				$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
				$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
				$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);
				$row['is_confirmed'] = (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED) || $row['is_activated'];

				$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
				$fin = $masterObject->getAListItem($row['fin_year_id']);
				$row['fin_year_name'] = isset($fin['name']) ? $fin['name'] : $fin['value'];
				$row['fin_year_ref'] = $fin['fin_ref'];

				if(!$external_call) {
					$row['summary_details'] = $this->getSummaryOfChildren($row['id']);
				}

				if($row['alt_module'] != "FALSE_LINK") {
					$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['alt_module']."'";
					$alt = $this->mysql_fetch_one($sql);
					$row['alt_module_name'] = $alt['modtext'];
					$row['alt_module_settings'] = $alt;
				}
				$row['mscoa_version_id'] = $this->convertMSCOAVersionCodeToID($row['mscoa_version']);
				$rows[$i] = $row;
			}
			if($sdbip_id !== false) {
				$rows = $rows[$sdbip_id];
			} elseif($limit !== false) {
				$ids = array_keys($rows);
				$o = $rows;
				$rows = array();
				if($limit > count($ids)) {
					$limit = count($ids);
				}
				for($i = 0; $i < $limit; $i++) {
					$rows[$ids[$i]] = $o[$ids[$i]];
				}
			}
		}
		return $rows;
	}

	public function getPagesWhereNoActivatedSDBIPisAllowed() {
		return $this->no_activated_sdbip_pages;
	}

	public function getRawObject($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$row = $this->mysql_fetch_one($sql);
		return $row;
	}

	public function hasSDBIPBeenCreatedAndNotYetActivated($force = false) {
		$force = true;
		//session variable is set in __construct
		//if NEW_OBJECT.timestamp < now then recheck the database && update details
		if($force || $_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['timestamp'] < time()) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." AND ( ( ".$this->getStatusFieldName()." & ".self::SDBIP_NEW." ) = ".self::SDBIP_NEW." OR ( ".$this->getStatusFieldName()." & ".self::SDBIP_CONFIRMED." ) = ".self::SDBIP_CONFIRMED." )ORDER BY ".$this->getIDFieldName()." DESC LIMIT 1";
			$row = $this->mysql_fetch_one($sql);
			//if something exists then update details and set status = true
			if(is_array($row) && count($row) > 0) {
				foreach($row as $fld => $val) {
					$f = explode("_", $fld);
					unset($f[0]);
					$key = implode("_", $f);
					$row[$key] = $val;
				}

				$row['ref'] = $this->getRefTag().$row['id'];
				$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
				$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
				$row['is_confirmed'] = (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED);
				$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);

				$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
				$row['fin_year_name'] = $masterObject->getAListItemName($row['fin_year_id']);
				$row['summary_details'] = $this->getSummaryOfChildren($row['id']);

				if($row['idp_module'] != "FALSE_LINK") {
					$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['idp_module']."'";
					$alt = $this->mysql_fetch_one($sql);
					$row['idp_module_name'] = $alt['modtext'];
					$row['idp_module_settings'] = $alt;
				}
				if($row['alt_module'] != "FALSE_LINK") {
					if($this->checkIntRef($row['alt_module'])) {
						$alt = $this->getRawObject($row['alt_module']);
						$alt['modtext'] = $alt['sdbip_name'];
						$alt['modref'] = $this->getModRef();
						$alt['modlocation'] = "SDBP6";
						$alt['modid'] = $row['alt_module'];
					} else {
						$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['alt_module']."'";
						$alt = $this->mysql_fetch_one($sql);
					}
					$row['alt_module_name'] = $alt['modtext'];
					$row['alt_module_settings'] = $alt;
				}
				$row['mscoa_version_id'] = $this->convertMSCOAVersionCodeToID($row['mscoa_version']);
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['details'] = $row;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['status'] = true;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['timestamp'] = time() + (5 * 60);    //5 minutes
				return true;
			} else {
				//otherwise reset details to nothing (in case previously activated SDBIP has been cancelled)
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['details'] = array();
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['status'] = false;
				$_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['timestamp'] = time() + (5 * 60);    //5 minutes
				return false;
			}
		} else {
			return $_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['status'];
		}
		return false;

	}

	public function getCurrentNewSDBIPDetails() {
		$is_new_exist = $this->hasSDBIPBeenCreatedAndNotYetActivated();
		if($is_new_exist) {
			$details = $_SESSION[$this->getModRef()][$this->getMyObjectType()]['NEW_OBJECT']['details'];
			$details['next_step'] = $this->whatIsMyNextStepInCreatingMySDBIP($details);
			return $details;
		} else {
			return array();
		}
	}


//Cancel SDBIP Creation if an error occurred during initial setup (step 1)
	public function cancelSDBIPCreationOnStep1($sdbip_id) {
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".(self::INACTIVE + SDBP6::SDBIP_NEW)." WHERE ".$this->getIDFieldName()." = ".$sdbip_id;
		$this->db_update($sql);
	}


	public function formatSDBIPRecord($row) {
		foreach($row as $fld => $val) {
			$f = explode("_", $fld);
			unset($f[0]);
			$key = implode("_", $f);
			$row[$key] = $val;
		}

		$row['ref'] = $this->getRefTag().$row['id'];
		$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
		$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
		$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);
		// #AA-659 - Confirmed status not reflecting correctly on Undo Activation page - it is returning false when it should be true because confirmation status is removed when activating
		$row['is_confirmed'] = $row['is_activated'] || (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED);

		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$row['fin_year_name'] = $masterObject->getAListItemName($row['fin_year_id']);
		$row['summary_details'] = $this->getSummaryOfChildren($row['id']);

		if($row['idp_module'] != "FALSE_LINK") {
			$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['idp_module']."'";
			$alt = $this->mysql_fetch_one($sql);
			$row['idp_module_name'] = $alt['modtext'];
			$row['idp_module_settings'] = $alt;
		}
		if($row['alt_module'] != "FALSE_LINK") {
			if($this->checkIntRef($row['alt_module'])) {
				$alt = $this->getRawObject($row['alt_module']);
				$alt['modtext'] = $alt['sdbip_name'];
				$alt['modref'] = $this->getModRef();
				$alt['modlocation'] = "SDBP6";
				$alt['modid'] = $row['alt_module'];
			} else {
				$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$row['alt_module']."'";
				$alt = $this->mysql_fetch_one($sql);
			}
			$row['alt_module_name'] = $alt['modtext'];
			$row['alt_module_settings'] = $alt;
		}
		$row['mscoa_version_id'] = $this->convertMSCOAVersionCodeToID($row['mscoa_version']);

		return $row;
	}


	public function getFirstSDBIPID() {
		$sql = "SELECT * FROM ".$this->getTableName()." O 
				WHERE ".$this->getActiveStatusSQL("O")." 
				AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." 
				ORDER BY ".$this->getIDFieldName()." ASC LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		return $row[$this->getIDFieldName()];
	}


	/********************************************************
	 * Functions related to link between SDBIP and IDP or prior SDBP5B modules
	 */


	public function doesLinkedIDPModuleExistOnDatabase() {
		$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','", $this->idp_module_versions_that_can_be_linked)."') AND modyn = 'Y'";
		$this->idp_modules_that_can_be_linked = $this->mysql_fetch_all_by_id($sql, "modref");
		return (count($this->idp_modules_that_can_be_linked) > 0 ? true : false);
	}

	public function getAvailableIDPModules($include_names = false) {
		if($include_names == false) {
			return array_keys($this->idp_modules_that_can_be_linked);
		} elseif(count($this->idp_modules_that_can_be_linked) > 0) {
			$rows = array();
			foreach($this->idp_modules_that_can_be_linked as $r => $t) {
				$rows[$r] = $t['modtext'];
			}
			return $rows;
		} else {
//			echo "<h4 class=red>no available modules found</h4>";
			return array();
		}
	}

	public function getLinkedIDPModule() {
		return $this->linked_idp_module;
	}

	public function getLinkedIDP($idp_modref = "", $idp_id = 0) {
		//if(strlen($this->linked_idp)==0 && strlen($idp_modref)>0 && ASSIST_HELPER::checkIntRef($idp_id)) {
		if(strlen($idp_modref) > 0 && ASSIST_HELPER::checkIntRef($idp_id)) {
			$row = $this->getActivatedIDPsFromModule($idp_modref, $idp_id);
			return $row['name'];
		} else {
			return "";//$this->linked_idp;
		}
	}

	public function getActivatedIDPsFromModule($idp_modref, $idp_id = 0, $include_mscoa_version_in_name = false) {
		//get local modref
		$my_modref = $this->getModRef();
		//communicate with external module = changes _SESSION modref
		$idpObject = new IDP3_EXTERNAL($idp_modref);
		$rows = $idpObject->getActivatedIDPs($idp_id, $include_mscoa_version_in_name);
		if($idp_id == 0) {
			foreach($rows as $key => $row) {
				$rows[$key]['mscoa_version_code'] = $this->convertMSCOAVersionIDToCode($row['mscoa_version']);
				if($include_mscoa_version_in_name) {
					if($row['mscoa_version'] == 1) {
						$rows[$key]['name'] .= " (Not mSCOA Compliant)";
					} else {
						$rows[$key]['name'] .= " (mSCOA Version: ".$this->convertMSCOAVersionIDToCode($row['mscoa_version']).")";
					}
				}
			}
		} else {
			$rows['mscoa_version_code'] = $this->convertMSCOAVersionIDToCode($rows['mscoa_version']);
			if($include_mscoa_version_in_name) {
				if($rows['mscoa_version'] == 1) {
					$rows['name'] .= " (Not mSCOA Compliant)";
				} else {
					$rows['name'] .= " (mSCOA Version: ".$this->convertMSCOAVersionIDToCode($rows['mscoa_version']).")";
				}
			}
		}
		//reset _SESSION modref to local modref
		$this->setDBRef($my_modref);
		//return results
		return $rows;
	}

	public function getLinkedSDBIP($sdbip_modref) {
		$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '".$sdbip_modref."'";
		$name = $this->mysql_fetch_one_value($sql, "modtext");
		return $name;
	}


	/*************************************************************************************
	 * Functions linked to creating an SDBIP from SDBP5B module - only used for the 2019/2020 financial year & disabled thereafter - AA-327 JC 28 March 2020
	 */
	public function doesLinkedSDBP5BModuleExistOnDatabase() {
		$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','", $this->sdbip_module_versions_that_can_be_linked)."') AND modyn IN ('".implode("','", $this->sdbip_module_status_that_can_be_used)."') AND modref <> '".$this->getModRef()."'";
		$this->sdbip_modules_that_can_be_linked = $this->mysql_fetch_all_by_id($sql, "modref");
		return (count($this->sdbip_modules_that_can_be_linked) > 0 ? true : false);
	}

	public function getAvailableSDBP5BModules($include_names = false) {
		if($include_names == false) {
			return array_keys($this->sdbip_modules_that_can_be_linked);
		} elseif(count($this->sdbip_modules_that_can_be_linked) > 0) {
			$rows = array();
			foreach($this->sdbip_modules_that_can_be_linked as $r => $t) {
				$rows[$r] = $t['modtext'];
			}
			return $rows;
		} else {
//			echo "<h4 class=red>no available modules found</h4>";
			return array();
		}
	}


	/****************************************************************************************
	 * END SDBP5B functions
	 */

	public function canLinkedSDBP5BModuleBeConverted() {
		if(count($this->sdbip_modules_that_can_be_linked) > 0 && in_array($this->sdbip_module_that_can_be_converted, $this->sdbip_modules_that_can_be_linked)) {
			return true;
		}
		return false;
	}

	//removed - jc 2018-10-29
	public function getSDBP5BModuleThatCanBeConverted() {
		//return $this->sdbip_module_that_can_be_converted;
	}


	/*******************************************************************
	 * Functions related to linking to internal SDBIP
	 */

	public function getAvailableSDBP6Objects($include_names = false) {
		if($include_names == false) {
			return array_keys($this->sdbp6_sdbips);
		} elseif(count($this->sdbp6_sdbips) > 0) {
			return $this->sdbp6_sdbips;
		} else {
			return array();
		}
	}

	public function getListOfSDBIPObjects() {
		$sql = "SELECT * FROM ".$this->getTableName()." S WHERE ".$this->getActiveStatusSQL("S");
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$data[$r['sdbip_id']] = $r['sdbip_name'];
		}
		return $data;
	}


	public function getAvailableSDBIPsForChooser($status_filter = "ACTIVE", $include_status = false, $include_colour = false) {
		$finObject = new ASSIST_MASTER_FINANCIALYEARS();
		$years = $finObject->getActiveItemsFormattedForSelect();
		//ACTIVATED = ACTIVE & ACTIVATED
		//ALL = ACTIVE || RESET - DO NOT USE!!!
		//ACTIVE = ACTIVE & (NEW || CONFIRMED || ACTIVATED)
		//NEW = ACTIVE & NEW
		//CONFIRMED = ACTIVE & CONFIRMED
		switch($status_filter) {
			case "ACTIVE":
				$sql = "SELECT * 
				FROM ".$this->getTableName()." S 
				INNER JOIN ".$finObject->getTable()." FY
				ON S.".$this->getTableField()."_fin_year_id = FY.id
				WHERE ".$this->getActiveStatusSQL("S")."
				".$finObject->getSortBy("FY", false, "DESC");
				break;
			case "CONFIRMED":
				$sql = "SELECT * 
				FROM ".$this->getTableName()." S 
				INNER JOIN ".$finObject->getTable()." FY
				ON S.".$this->getTableField()."_fin_year_id = FY.id
				WHERE ".$this->getActiveStatusSQL("S")."
				AND (".$this->getStatusFieldName()." & ".SDBP6::SDBIP_CONFIRMED.") = ".SDBP6::SDBIP_CONFIRMED."
				AND (".$this->getStatusFieldName()." & ".SDBP6::SDBIP_ACTIVATED.") <> ".SDBP6::SDBIP_ACTIVATED."
				".$finObject->getSortBy("FY", false, "DESC");
				break;
			case "ACTIVATED":
				$sql = "SELECT * 
				FROM ".$this->getTableName()." S 
				INNER JOIN ".$finObject->getTable()." FY
				ON S.".$this->getTableField()."_fin_year_id = FY.id
				WHERE ".$this->getActiveStatusSQL("S")."
				AND (".$this->getStatusFieldName()." & ".SDBP6::SDBIP_ACTIVATED.") = ".SDBP6::SDBIP_ACTIVATED."
				".$finObject->getSortBy("FY", false, "DESC");
				break;
		}
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$extra_info = "Financial Year: ".(isset($years[$r['sdbip_fin_year_id']]) ? $years[$r['sdbip_fin_year_id']] : "Unknown (ID: ".$r['sdbip_fin_year_id'].")")."
							<br />mSCOA Compliant: ".($r['sdbip_mscoa_compliant'] == 1 ? "Yes" : "No")."
							<br />mSCOA Version: ".$r['sdbip_mscoa_version'];
			$data[$r['sdbip_id']] = array(
				'name' => $r['sdbip_name'],
				'extra_info' => $extra_info,
			);
			if($include_status || $include_colour) {
				if(($r['sdbip_status'] & SDBP6::SDBIP_ACTIVATED) == SDBP6::SDBIP_ACTIVATED) {
					$status = "Activated";
					$colour = "green";
				} elseif(($r['sdbip_status'] & SDBP6::SDBIP_CONFIRMED) == SDBP6::SDBIP_CONFIRMED) {
					$status = "Confirmed";
					$colour = "orange";
				} elseif(($r['sdbip_status'] & SDBP6::SDBIP_NEW) == SDBP6::SDBIP_NEW) {
					$status = "New";
					$colour = "red";
				}
				if($include_status) {
					$data[$r['sdbip_id']]['status'] = $status;
				}
				if($include_colour) {
					$data[$r['sdbip_id']]['colour'] = $colour;
				}
			}
		}
		return $data;

	}

	/** Used by Setup > SDBIP */
	//TODO: Add extra info to the return to make Setup screen more informative but for expedience keep it simple for now - JC #AA-408 2 June 2020
	public function getAllAvailableSDBIPsForSetup($include_status = false, $include_colour = false) {
		$finObject = new ASSIST_MASTER_FINANCIALYEARS();
		$years = $finObject->getActiveItemsFormattedForSelect();

		$sql = "SELECT * 
				FROM ".$this->getTableName()." S 
				INNER JOIN ".$finObject->getTable()." FY
				ON S.".$this->getTableField()."_fin_year_id = FY.id
				WHERE ".$this->getActiveStatusSQL("S")."
				".$finObject->getSortBy("FY", false, "DESC");
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$extra_info = "Financial Year: ".(isset($years[$r['sdbip_fin_year_id']]) ? $years[$r['sdbip_fin_year_id']] : "Unknown (ID: ".$r['sdbip_fin_year_id'].")")."
							<br />mSCOA Compliant: ".($r['sdbip_mscoa_compliant'] == 1 ? "Yes" : "No")."
							<br />mSCOA Version: ".$r['sdbip_mscoa_version'];
			$data[$r['sdbip_id']] = array(
				'name' => $r['sdbip_name'],
				'extra_info' => $extra_info,
			);
			if($include_status || $include_colour) {
				if(($r['sdbip_status'] & SDBP6::SDBIP_ACTIVATED) == SDBP6::SDBIP_ACTIVATED) {
					$status = "Activated";
					$colour = "green";
				} elseif(($r['sdbip_status'] & SDBP6::SDBIP_CONFIRMED) == SDBP6::SDBIP_CONFIRMED) {
					$status = "Confirmed";
					$colour = "orange";
				} elseif(($r['sdbip_status'] & SDBP6::SDBIP_NEW) == SDBP6::SDBIP_NEW) {
					$status = "New";
					$colour = "red";
				}
				if($include_status) {
					$data[$r['sdbip_id']]['status'] = $status;
				}
				if($include_colour) {
					$data[$r['sdbip_id']]['colour'] = $colour;
				}
			}
		}
		return $data;
	}

	/********************************************************
	 * Functions related to Financial Years [ASSIST_MASTER_FINANCIALYEARS]
	 */

	public function getAvailableFinancialYears() {
		//get complete list of financial years
		$finObject = new ASSIST_MASTER_FINANCIALYEARS();
		$years = $finObject->getActiveItemsFormattedForSelect();
		//check for those in use already
		$sql = "SELECT DISTINCT ".$this->getTableField()."_fin_year_id as fin_year_id, sdbip_id as id, sdbip_name as name FROM ".$this->getTableName()." WHERE ".$this->getActiveStatusSQL();
		$rows = $this->mysql_fetch_all($sql);
		$fin_years_used = array();
		foreach($rows as $r) {
			$fin_years_used[] = $r['fin_year_id'];
			$this->sdbp6_sdbips[$r['id']] = $r['name'];
		}
		//filter out those already in use
		foreach($years as $key => $value) {
			if(in_array($key, $fin_years_used)) {
				unset($years[$key]);
			}
		}
		//return result
		return $years;
	}


	/**
	 * getAllActivitedObjects
	 *  - called from SDBP6_INTERNAL for PM5
	 * @param bool - when creating the list, do the financial year details also need to be included
	 * @return bool|mixed
	 */
	public function getAllActivatedObjects($include_financial_year_details = false, $include_summary_details = false, $limit_by_finyear = false, $sort_by = "DESC") {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." 
				AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." 
				".($limit_by_finyear !== false ? " AND ".$this->getTableField()."_fin_year_id = ".$limit_by_finyear : "")."
				ORDER BY ".$this->getIDFieldName()." ".$sort_by;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$data = array();
		foreach($rows as $row_key => $row) {
			foreach($row as $fld => $val) {
				$f = explode("_", $fld);
				unset($f[0]);
				$key = implode("_", $f);
				$row[$key] = $val;
			}

			$row['ref'] = $this->getRefTag().$row['id'];
			$row['is_active'] = (($row['status'] & self::ACTIVE) == self::ACTIVE) && !((($row['status'] & self::SDBIP_RESET) == self::SDBIP_RESET));
			$row['is_new'] = (($row['status'] & self::SDBIP_NEW) == self::SDBIP_NEW);
			$row['is_activated'] = (($row['status'] & self::SDBIP_ACTIVATED) == self::SDBIP_ACTIVATED);
			$row['is_confirmed'] = (($row['status'] & self::SDBIP_CONFIRMED) == self::SDBIP_CONFIRMED) || $row['is_activated'];
			if($include_financial_year_details === true) {
				$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
				$row['fin_year_name'] = $masterObject->getAListItemName($row['fin_year_id']);
			}
			if($include_summary_details === true) {
				$row['summary_details'] = $this->getSummaryOfChildren($row['id']);
			}
			$data[$row_key] = $row;
		}
		return $data;
	}

	public function getASDBIPDetails($sdbip_id) {
		$all = $this->getAllActivatedObjects(false, false, false);
		if(isset($all[$sdbip_id])) {
			return $all[$sdbip_id];
		}
		return false;
	}

	public function getASDBIPName($sdbip_id, $include_ref = false) {
		$sdbip = $this->getASDBIPDetails($sdbip_id);
		$name = "";
		if($sdbip !== false) {
			$name = $sdbip['name'];
			if($include_ref === true) {
				$name .= " (".$sdbip['ref'].")";
			}
		}
		return $name;
	}


	public function getSourceSDBIPForSetupCopy() {
		$sql = "SELECT sdbip_id as id FROM ".$this->getTableName()." 
				WHERE  ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." 
				AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." 
				ORDER BY ".$this->getIDFieldName()." DESC LIMIT 1";
		$source_sdbip_id = $this->mysql_fetch_one_value($sql, "id");
		return $source_sdbip_id;
	}

	//NOT NEEDED BUT KEPT FOR INFO PURPOSES
	private function copySetup($new_sdbip_id) {
		$sql = "SELECT sdbip_id as id FROM ".$this->getTableName()." 
				WHERE  ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE." 
				AND ( ".$this->getStatusFieldName()." & ".self::SDBIP_ACTIVATED." ) = ".self::SDBIP_ACTIVATED." 
				ORDER BY ".$this->getIDFieldName()." DESC LIMIT 1";
		$source_sdbip_id = $this->mysql_fetch_one_value($sql, "id");

		$result = $this->replaceAllNames("|sdbip| setup was copied.");
		if(is_null($source_sdbip_id) || !$this->checkIntRef($source_sdbip_id)) {
			$result = $this->replaceAllNames("|sdbip| setup could NOT be copied - Activated |sdbip| could not be found.");
		} else {
			//ORG STRUCTURE
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_ids = $orgObject->copySDBIPSpecificSetup($source_sdbip_id, $new_sdbip_id);
			unset($orgObject);
			//LISTS
			$headingObject = new SDBP6_HEADINGS();
			$list_names = $headingObject->getStandardListHeadings(true);
			$listObject = new SDBP6_LIST();
			$owner_ids = array();
			foreach($list_names as $list) {
				$listObject->changeListType($list['id']);
				$ids = $listObject->copySDBIPSpecificSetup($source_sdbip_id, $new_sdbip_id);
				if($list['id'] == "owner") {
					$owner_ids = $ids;
				}
			}
			unset($listObject);

			//ADMINS - need to process LISTS first to get owner_ids
			$adminObject = new SDBP6_SETUP_ADMINS();
			$adminObject->copySDBIPSpecificSetup($org_ids, $owner_ids);
			unset($adminObject);
		}

		return $result;
	}


	public function __destruct() {
		parent::__destruct();
	}

}

?>