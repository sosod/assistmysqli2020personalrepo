<?php

class SDBP6_SDBP5B extends SDBP6 {

	private $my_modref;
	private $my_dbref;
	private $my_cmpcode;
	private $sdbp6_object;
	private $my_object;
	private $ext_object;


	private $field_type_map;

	private $allowed_map_matches;

	public function __construct($modref = "", $sdbp6_object = "", $cmpcode = "", $create_source_object = false) {
		parent::__construct();
		$this->my_modref = strtolower($modref);
		$this->my_cmpcode = strlen($cmpcode) > 0 ? strtolower($cmpcode) : strtolower($this->getCmpCode());
		$this->my_dbref = "assist_".$this->my_cmpcode."_".$this->my_modref;
		$this->sdbp6_object = $sdbp6_object;
		$this->my_object = $this->getExternalObjectType($sdbp6_object);
		$this->ext_object = new SDBP5B_EXTERNAL($modref, $this->my_object, $create_source_object);
		$this->field_type_map = array(
			'REF' => "System Reference",
			'MEDVC' => "Medium Text",
			'VC' => "Medium Text",
			'LRGVC' => "Large Text",
			'TEXT' => "Unlimited Text",
			'BOOL' => "Yes/No",
			'ATTACH' => "Attachment",
			'PERC' => "Percentage",
			'DATE' => "Date",
			'LIST' => "List",
			'NUM' => "Number",
			'MULTILIST' => "Multi-select List",
			'TEXTLIST' => "Multi-select List",
			'AREA' => "Multi-select List",
			'WARDS' => "Multi-select List",
			'SEGMENT' => "mSCOA Segment List",
			'MULTISEGMENT' => "Multi-select mSCOA Segment List",
			'OBJECT' => "List of Module Objects",
			'PROJECT' => "List of Module Objects",
			'MULTIOBJECT' => "Multi-select List of Module Objects",
			'TOP' => "List of Top Layer KPIs",
			'CAP' => "List of Capital Projects",
		);

		$this->allowed_map_matches = array(
			'DATE' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'BOOL' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'LIST' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => true,
				'MULTIOBJECT' => true,
				'SEGMENT' => true,
				'MULTISEGMENT' => true,
				'NUM' => false,
			),
			'SEGMENT' => array(
				'REF' => false,
				'MEDVC' => false,
				'LRGVC' => false,
				'TEXT' => false,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => false,
				'MULTILIST' => false,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => true,
				'MULTISEGMENT' => true,
				'NUM' => false,
			),
			'OBJECT' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => true,
				'MULTIOBJECT' => true,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'TEXT' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => true,
			),
			'NUM' => array(
				'REF' => false,
				'BOOL' => false,
				'ATTACH' => false,
				'DATE' => false,
				'LIST' => false,
				'MULTILIST' => false,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'PERC' => true,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'NUM' => true,
			),
			'TOP' => array(
				'REF' => false,
				'BOOL' => false,
				'ATTACH' => false,
				'DATE' => false,
				'LIST' => false,
				'MULTILIST' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'PERC' => false,
				'MEDVC' => false,
				'LRGVC' => false,
				'NUM' => false,
				'TEXT' => true,
				'OBJECT' => true,
				'MULTIOBJECT' => true,
			),
		);
		$this->allowed_map_matches['VC'] = $this->allowed_map_matches['TEXT'];
		$this->allowed_map_matches['MEDVC'] = $this->allowed_map_matches['TEXT'];
		$this->allowed_map_matches['LRGVC'] = $this->allowed_map_matches['TEXT'];
		$this->allowed_map_matches['MULTILIST'] = $this->allowed_map_matches['LIST'];
		$this->allowed_map_matches['TEXTILIST'] = $this->allowed_map_matches['LIST'];
		$this->allowed_map_matches['AREA'] = $this->allowed_map_matches['LIST'];
		$this->allowed_map_matches['WARDS'] = $this->allowed_map_matches['LIST'];
		$this->allowed_map_matches['MULTIOBJECT'] = $this->allowed_map_matches['OBJECT'];
		$this->allowed_map_matches['MULTISEGMENT'] = $this->allowed_map_matches['SEGMENT'];

	}

	private function getExternalObjectType($sdbp6_object) {
		switch($sdbp6_object) {
			case "PROJECT":
				return "PROJECT";
				break;
			case "TOPKPI":
				return "TOP";
				break;
			case "DEPTKPI":
				return "KPI";
				break;
		}
	}

	public function getDefaultFieldMap() {
		$project_type = SDBP6_PROJECT::OBJECT_TYPE;
		$deptkpi_type = SDBP6_DEPTKPI::OBJECT_TYPE;
		$topkpi_type = SDBP6_TOPKPI::OBJECT_TYPE;
		/***
		 * Field_map: ext_mod_field => local_mod_field
		 * 'proj_mscoa_ref'=>"proj_mscoa_ref",
		 * 'proj_id'=>"proj_id",
		 */
		switch($this->sdbp6_object) {
			case $project_type:
				$field_map = array(
					'cap_subid' => "proj_sub_id",
					'cap_gfsid' => "proj_function_id",
					'cap_cpref' => "0",
					'cap_idpref' => "proj_ref",
					'cap_vote' => "proj_finref",
					'cap_name' => "proj_name",
					'cap_descrip' => "proj_description",
					'cap_planstart' => "proj_startdate",
					'cap_planend' => "proj_enddate",
					'cap_actualstart' => "proj_actualstartdate",
					'cap_actualend' => "proj_actualenddate",
					'capital_area' => "proj_area",
					'capital_wards' => "proj_ward",
				);
				break;
			case $topkpi_type:
				$field_map = array('top_dirid' => "top_sub_id",
					'top_riskratingid' => "top_riskrating_id",
					'top_repkpi' => "0",
					'top_pmsref' => "top_ref",
					'top_gfsid' => "top_function_id",
					'top_natoutcomeid' => "top_natoutcome_id",
					'top_natkpaid' => "top_natkpa_id",
					'top_pdoid' => "top_pdo_id",
					'top_ndpid' => "top_ndp_id",
					'top_idp' => "top_expresult_id",
					'top_munkpaid' => "top_munkpa_id",
					'top_value' => "top_name",
					'top_unit' => "top_description",
					'top_risk' => "top_risk",
					'top_wards' => "top_ward",
					'top_area' => "top_area",
					'top_ownerid' => "top_owner_id",
					'top_baseline' => "top_baseline",
					'top_poe' => "top_soe",
					'top_pyp' => "top_pyp",
					'top_mtas' => "0",
					'top_repcate' => "0",
					'top_text1' => "0",
					'top_annual' => "0",
					'top_text2' => "0",
					'top_revised' => "0",
					'top_text3' => "0",
					'top_calctype' => "top_calctype_id",
					'top_targettype' => "top_unit_id",
				);
				break;
			case $deptkpi_type:
				$field_map = array(
					'kpi_subid' => "kpi_sub_id",
					'kpi_topid' => "kpi_top_id",
					'kpi_gfsid' => "kpi_function_id",
					'kpi_idpref' => "kpi_ref",
					'kpi_natoutcomeid' => "kpi_natoutcome_id",
					'kpi_idpid' => "kpi_expresult_id",
					'kpi_natkpaid' => "kpi_natkpa_id",
					'kpi_munkpaid' => "kpi_munkpa_id",
					'kpi_pdoid' => "kpi_pdo_id",
					'kpi_ndpid' => "kpi_ndp_id",
					'kpi_capitalid' => "kpi_proj_id",
					'kpi_value' => "kpi_name",
					'kpi_unit' => "kpi_description",
					'kpi_conceptid' => "kpi_concept_id",
					'kpi_typeid' => "kpi_type_id",
					'kpi_riskref' => "kpi_riskref",
					'kpi_risk' => "kpi_risk",
					'kpi_riskratingid' => "kpi_riskrating_id",
					'kpi_wards' => "kpi_ward",
					'kpi_area' => "kpi_area",
					'kpi_ownerid' => "kpi_owner_id",
					'kpi_baseline' => "kpi_baseline",
					'kpi_pyp' => "kpi_pyp",
					'kpi_perfstd' => "kpi_perfstd",
					'kpi_poe' => "kpi_soe",
					'kpi_mtas' => "0",
					'kpi_repcate' => "0",
					'kpi_annual' => "0",
					'kpi_text1' => "0",
					'kpi_revised' => "0",
					'kpi_text2' => "0",
					'kpi_text3' => "0",
					'kpi_calctype' => "kpi_calctype_id",
					'kpi_targettype' => "kpi_unit_id",
				);
				break;
			default:
				$field_map = array();
				break;
		}
		return $field_map;
	}

	public function getHeadingsForMapping($field_map = array(), $include_type_in_name = true, $include_details = false) {
		$dbref = $this->my_dbref;
		$fields = array();
		if(!is_array($field_map) || count($field_map) == 0) {
			$field_map = $this->getDefaultFieldMap();
		}
		if(is_array($field_map) && count($field_map) > 0) {
			$sql = "SELECT SH.h_ignite, SH.h_client, SH.h_type, SHS.field, SH.h_table FROM  ".$dbref."_setup_headings SH
				INNER JOIN  ".$dbref."_setup_headings_setup SHS
				ON SH.h_id = SHS.head_id AND SHS.field IN ('".implode("','", array_keys($field_map))."')";
			$rows = $this->mysql_fetch_all_by_id($sql, "field");

			foreach($field_map as $fld => $f) {
				if(isset($rows[$fld])) {
					if($include_type_in_name === true) {
						if(isset($rows[$fld])) {
							$name = "<span class=b>".(strlen($rows[$fld]['h_client']) > 0 ? $rows[$fld]['h_client'] : $rows[$fld]['h_ignite'])."</span>";
							$name .= isset($this->field_type_map[$rows[$fld]['h_type']]) ? "<br /><span class='float normal i'>[".$this->field_type_map[$rows[$fld]['h_type']]."]</span>" : "";
						} else {
							$name = $fld;
						}
					} elseif(isset($rows[$fld])) {
						$name = (strlen($rows[$fld]['h_client']) > 0 ? $rows[$fld]['h_client'] : $rows[$fld]['h_ignite']);
					} else {
						$name = $fld;
					}
					if($include_details === true) {
						$fields[$fld] = array('name' => $name,
							'type' => "TEXT",
							'list' => "",);
						if(isset($rows[$fld])) {
							$fields[$fld]['type'] = $rows[$fld]['h_type'];
							$fields[$fld]['list'] = $rows[$fld]['h_table'];
							if($fields[$fld]['type'] == "TOP") {
								$fields[$fld]['list'] = "TOP";
							} elseif($fields[$fld]['type'] == "CAP") {
								$fields[$fld]['list'] = "CAP";
							}
						}
					} else {
						$fields[$fld] = $name;
					}
				} else {
					switch($fld) {
						default:
							$fields[$fld] = $fld;
					}
				}
			}
			if(count($fields) > 0) {
				$fields = $this->ext_object->replaceExternalNames($fields);
			}
		}
		return $fields;
	}


	/**
	 * Get list items - customise specific to SDBP5B module
	 */
	public function getListItems($field_map, $old_field_settings, $modref = "") {
		$old_lists = array('raw' => array(), 'comparison' => array());
		foreach($field_map as $old_field => $new_field) {
			if($new_field != "0" && $new_field != false && isset($old_field_settings[$old_field]['type']) && isset($old_field_settings[$old_field]['list']) && strlen($old_field_settings[$old_field]['list']) > 0) {
				$list = $old_field_settings[$old_field]['list'];
				$send_id = false;
				switch($old_field_settings[$old_field]['type']) {
					case "SEGMENT":
					case "MULTISEGMENT":
						//$extObject = new IDP3_SEGMENTS($list,$modref);
						//$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForSelect();
						//SEGMENT Objects don't exist in SDBP5B
						break;
					case "OBJECT":
					case "MULTIOBJECT":
//						$send_id = true;
//						$extObject = new $list(0,$modref,true);
//						$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForExternalModule($idp_id);
					case "CAP":
						$extObject = new SDBP5B_EXTERNAL($this->my_modref, $this->getExternalObjectType(SDBP6_PROJECT::OBJECT_TYPE), true);
						$old_lists['raw'][$list] = $extObject->getListItemsForSelect();
						break;
					case "TOP":
						$extObject = new SDBP5B_EXTERNAL($this->my_modref, $this->getExternalObjectType(SDBP6_TOPKPI::OBJECT_TYPE), true);
						$old_lists['raw'][$list] = $extObject->getListItemsForSelect();
						break;
					case "LIST":
					case "MULTILIST":
					case "TEXTLIST":
					case "AREA":
					case "WARDS":
						$old_lists['raw'][$list] = $this->getList($list);
//						$extObject = new IDP3_LIST($list,$modref);
//						$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForSelect();
						break;
					case "WARDS":

					case "AREA":

						break;
				}
				/*if(!isset($extObject) ){
					//do nothing - list type not accounted for!
					echo $list.":".$old_field_settings[$old_field]['type'];
				}*/
				unset($extObject);
			} else {
				//do nothing if field is not in use
			}
		}
		if(count($old_lists['raw']) > 0) {
			foreach($old_lists['raw'] as $list => $l) {
				foreach($l as $key => $v) {
					$old_lists['comparison'][$list][$key] = $this->formatTextForComparison($v);
				}
			}
		}
		return $old_lists;
	}


	public function getList($list) {
		$data = array();
		if($list == "subdir") {
			$sql = "SELECT subdir.*, dir.value as dir FROM ".$this->my_dbref."_subdir subdir INNER JOIN ".$this->my_dbref."_dir dir ON dir.id = subdir.dirid";
			$rows = $this->mysql_fetch_all($sql);
			foreach($rows as $r) {
				//both colon & dash version needed depending on where the object is mapped to
				$display = $r['dir'].": ".$r['value'];
				$data[$r['id']] = $display;
				$display = $r['dir']." - ".$r['value'];
				$data[$r['id']] = $display;
			}
		} elseif($list == "dir") {
			$sql = "SELECT * FROM ".$this->my_dbref."_dir";
			$rows = $this->mysql_fetch_all($sql);
			foreach($rows as $r) {
				//both colon & dash version needed depending on where the object is mapped to
				$display = $r['value'];
				$data[$r['id']] = $display;
			}
		} elseif($list == "area") {
			$data = $this->getArea();
		} elseif($list == "wards") {
			$data = $this->getWards();
		} else {
			$sql = "SELECT * FROM ".$this->my_dbref."_list_".$list;
			$rows = $this->mysql_fetch_all($sql);
			foreach($rows as $r) {
				if($list == "gfs") {
					//convert "Function [Core Function] - Sub function" to "Function: Core Function: Sub function"
					//first catch extra spaces
					while(strpos($r['value'], "  ") !== false) {
						$r['value'] = str_replace("  ", " ", $r['value']);    //catch extra spaces
					}
					$r['value'] = str_ireplace("Function] - ", "Function: ", $r['value']);
					$r['value'] = str_ireplace(" [Core", ": Core", $r['value']);
					$r['value'] = str_ireplace(" [Non-core", ": Non-core", $r['value']);
				}
				if($list == "calctype") {
					$i = $r['code'];
				} else {
					$i = $r['id'];
				}
				$data[$i] = $r['value'];
			}
		}
		return $data;
	}


	public function getArea() {
		$sql = "SELECT * FROM ".$this->my_dbref."_list_area";
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$data[$r['id']] = $r['value'];
		}
		return $data;
	}

	public function getWards() {
		$sql = "SELECT * FROM ".$this->my_dbref."_list_wards";
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$data[$r['id']] = $r['value'];
		}
		return $data;
	}


	public function getAllowedMapMatches() {
		return $this->allowed_map_matches;
	}


	public function getSourceRecords($idp_id = 0) {
		$records = $this->ext_object->getSourceRecords($this->my_object);
		return $records;
	}


	public function getRefTag() {
		return $this->ext_object->getRefTag();
	}


	public function __destruct() {
		parent::__destruct();
	}

}


?>