<?php
/**
 * To manage the NAMES of items
 *
 *
 * This class cannot be a child of the SDBP6 class because that class instantiates this class which in turn creates in infinite loop.
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_NAMES extends ASSIST_MODULE_HELPER {

	private $additional_headings = array(
		'admins' => 'contract_owner_id',
		'menu' => "Menu",
		'headings' => "Headings",
		'lists' => "Lists",
	);

	private $menu_setup_defaults = array(
		'menu' => "Menu",
		'names' => "Naming",
		'headings' => "Headings",
		'lists' => "Lists",
		'orgstructure' => "Organisational Structure",
		'results' => "Result Settings",
		'columns' => "List Columns",
		'admins' => "Administrators",
		'calctype' => "Calculation Types",
	);


	const TABLE = "setup_names";
	const TABLE_FIELD = "name";
	const MENU_TABLE = "setup_menu";
	/*************
	 * CONSTANTS
	 */
	const ACTIVE = 2;
	/**
	 * Can a heading be renamed by the client
	 */
	const CAN_RENAME = 16;
	/**
	 * Is a heading the name of an object
	 */
	const OBJECT_HEADING = 32;
	/**
	 * Is a heading the name of an activity
	 */
	const ACTIVITY_HEADING = 64;


	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct("client", $cmpcode, $autojob, $modref);
	}


	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editNames($section, $var);
		if($mar > 0) {
			$helper = new SDBP6();
			$helper->forceNameUpdate();
			if($section == "object_names") {
				return array("ok", "Object names updated successfully.");
			} else {
				return array("ok", "Activity names updated successfully.");
			}
		} else {
			return array("info", "No changes were found to be saved.");
		}
		return array("error", $mar);
	}


	/*******************
	 * GET functions
	 * */
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getMenuTableName() {
		return $this->getDBRef()."_".SDBP6_MENU::TABLE;
	}

	protected function getFormattedNames($type, $get_default = false) {
		$names = array();
		$sql = "SELECT name_section as section,
				".($get_default ? "name_default as name" : "IF(LENGTH(name_client)>0,name_client,name_default) as name")."
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")
				AND (name_status & ".self::ACTIVE." = ".self::ACTIVE.") ";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$s = explode("_", $r['section']);
			if(count($s) > 1) {
				unset($s[0]);
			}
			$s = implode("_", $s);
			$names[$s] = $r['name'];
		}
		return $names;
	}

	/**
	 * Get the object names from the table in easy readable format
	 */
	public function fetchObjectNames() {
		return $this->getFormattedNames(SDBP6_NAMES::OBJECT_HEADING);
	}

	/**
	 * Get the default object names from the table in easy readable format
	 */
	public function fetchDefaultObjectNames() {
		return $this->getFormattedNames(SDBP6_NAMES::OBJECT_HEADING, true);
	}

	/**
	 * Get the object names from the table in easy readable format
	 */
	public function fetchActivityNames() {
		return $this->getFormattedNames(SDBP6_NAMES::ACTIVITY_HEADING);
	}

	/**
	 * Get the default object names from the table in easy readable format
	 */
	public function fetchDefaultActivityNames() {
		return $this->getFormattedNames(SDBP6_NAMES::ACTIVITY_HEADING, true);
	}

	public function fetchDefaultMenuNames() {
		return $this->fetchMenuForNaming();
	}

	/**
	 * Required for getting menu names to be used as object names
	 */
	public function fetchMenuForNaming($modref = "", $for_replacement = false) {
		$sql = "SELECT CONCAT('menu_',menu_link) as field, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name
				FROM ".$this->getMenuTableName()."
				WHERE (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
				ORDER BY menu_link";// echo $sql;
//				".(!$for_replacement?"AND (menu_status & ".self::CAN_RENAME." = ".self::CAN_RENAME.")":"")." ORDER BY menu_link";
		$rows = $this->mysql_fetch_value_by_id($sql, "field", "name");
		foreach($this->menu_setup_defaults as $fld => $name) {
			$rows['menu_setup_defaults_'.$fld] = $name;
			$rows['menu_setup_sdbip_defaults_'.$fld] = $name;
		}
		if(isset($rows['menu_setup_sdbip'])) {
			$rows['menu_setup_sdbip_defaults'] = "[Selected |sdbip|]";
		}
//		echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		ASSIST_HELPER::arrPrint($rows);
//		echo "<hr />";
		return $rows;
	}

	/**
	 * Required for getting MSCOA names to be used as object names
	 */
	public function fetchMSCOAForNaming($local_modref = "") {
		$modref = $this->getMSCOAModRefForIDP3AndSDBP6();
		$modcode = $this->getMSCOAModCodeForIDP3AndSDBP6();
		$class = $modcode."_NAMES";
		$mscoaObject = new $class($modref);
		$rows = $mscoaObject->fetchObjectNames();
		unset($mscoaObject);
		$this->setDBRef($local_modref);
		$names = array();
		foreach($rows as $key => $name) {
			$names['mscoa_'.$key] = $name;
		}
		return $names;
	}


	protected function getNamesFromDB($type, $sections = array(), $can_rename = false) {
		$sql = "SELECT name_id as id, name_section as section, name_default as mdefault, name_client as client
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")
				AND (name_status & ".self::ACTIVE." = ".self::ACTIVE.") "
			.($can_rename == true ? "AND (name_status & ".self::CAN_RENAME." = ".self::CAN_RENAME.") " : "")
			.(count($sections) > 0 ? " AND name_section IN ('".implode("','", $sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql, "section");
		return $rows;
		//return array($sql);
	}

	/**
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections = array()) {
		return $this->getNamesFromDB(SDBP6_NAMES::OBJECT_HEADING, $sections);
	}

	/**
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDB($sections = array()) {
		return $this->getNamesFromDB(SDBP6_NAMES::ACTIVITY_HEADING, $sections);
	}


	/**
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDBForEditing($sections = array()) {
		return $this->getNamesFromDB(SDBP6_NAMES::OBJECT_HEADING, $sections, $can_rename = true);
	}

	/**
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDBForEditing($sections = array()) {
		return $this->getNamesFromDB(SDBP6_NAMES::ACTIVITY_HEADING, $sections, $can_rename = true);
	}






	/****************************************
	 * SET / UPDATE Functions
	 */


	/**
	 * Function to edit object name / menu heading
	 */
	private function editNames($section, $var) {
		if($section == "object_names") {
			$original_names = $this->getObjectNamesFromDB(array_keys($var));
			$log_section = "OBJECT";
			$field = "name_section";
		} else {
			$original_names = $this->getActivityNamesFromDB(array_keys($var));
			$log_section = "ACTIVITY";
			$field = "name_section";
		}
		$mar = 0;
		$sql_start = "UPDATE ".$this->getTableName()." SET name_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
		foreach($var as $section => $val) {
			$m = 0;
			$to = (strlen($val) > 0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to != $from) {
				$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m = $this->db_update($sql);
				if($m > 0) {
					$s = explode("_", $section);
					if(count($s) > 1) {
						unset($s[0]);
					}
					$key = "|".implode("_", $s)."|";
					$c[$key] = array('to' => $to, 'from' => $from);
					$mar += $m;
				}
			}
		}
		if($mar > 0 && count($c) > 0) {
			$changes = array_merge(array('user' => $this->getUserName()), $c);
			$log_var = array(
				'section' => $log_section,
				'object_id' => $original_names[$section]['id'],
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$me = new SDBP6();
			$me->addActivityLog("setup", $log_var);
		}

		return $mar;
	}








	/**********************************************
	 * PROTECTED functions: functions available for use in class heirarchy
	 */


	/***********************
	 * PRIVATE functions: functions only for use within the class
	 */


	public function __destruct() {
		parent::__destruct();
	}


}


?>