<?php
/**
 * To manage the Top Layer KPI objects of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_ORGSTRUCTURE extends SDBP6 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_parent_id";//"";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $sdbip_parent_field = "_sdbip_id";

	protected $has_attachment = false;

	protected $ref_tag = "OS";


	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "ORGSTRUCTURE";
	const OBJECT_NAME = "ORGSTRUCTURE";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "setup_orgstructure";
	const TABLE_FLD = "org";
	const REFTAG = "OS";

	const LOG_TABLE = "setup";
	const LOG_SECTION = "org";


	public function __construct($modref = "", $obj_id = 0) {
		parent::__construct($modref);
		$this->has_internal_ref_field = false;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->sdbip_parent_field = self::TABLE_FLD.$this->sdbip_parent_field;
		if($obj_id > 0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";


	}


	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");

		if(isset($var['add_type']) && $var['add_type'] == "TOP") {
		}

		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);

		//first save the main object
		$import_data = array(
			'org_name' => $var['name'],
			'org_parent_id' => (isset($var['parent_id']) ? $var['parent_id'] : 0),
			'org_hod' => isset($var['hod']) ? $var['hod'] : 0,
			'org_status' => self::ACTIVE,
			'org_order' => isset($var['order']) ? $var['order'] : 99,
			'org_sdbip_id' => $sdbip_id,
		);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($import_data);
		$id = $this->db_insert($sql);

		if(ASSIST_HELPER::checkIntRef($id)) {
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|created| |".self::OBJECT_TYPE."| ".$this->getRefTag().$id.": ".$import_data[$this->getNameFieldName()],
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'sdbip_id' => $sdbip_id,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);


			//then check for children & pass to the addObject function to save
			if(isset($var['sub']) && is_array($var['sub']) && count($var['sub']) > 0) {
				foreach($var['sub'] as $order => $name) {
					if(strlen($name) > 0) {
						$send_data = array(
							'name' => $name,
							'parent_id' => $id,
							'order' => $order,
							'hod' => ($order == 0 ? 1 : 0),
							'sdbip_id' => $sdbip_id,
						);
						$this->addObject($send_data);
					}
				}
			}
			$result = array("ok", "All items saved successfully.", $id);
		} else {
			$result = array("error", "An error occurred while trying to save.  Please try again.");
		}

		return $result;
	}


	public function editObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$object_name = $this->getObjectName(self::OBJECT_TYPE);
		$action = "edit";
		$action_name = $this->getActivityName($action);

		if(!isset($var['object_id']) || !ASSIST_HELPER::checkIntRef($var['object_id'])) {
			$result = array("error", "An error occurred while trying to identify which item you are trying to ".$action_name.". Please reload the page and try again.");
		} else {
			$object_id = $var['object_id'];
			unset($var['object_id']);
			$old = $this->getRawObjectDetails($object_id);
			$sdbip_id = $old['sdbip_id'];

			$c = array();
			$import_data = array();
			foreach($var as $fld => $new) {
				switch($fld) {
					case "name":
						if($new != $old[$fld]) {
							$c[$fld] = array(
								'to' => $new,
								'from' => $old[$fld]
							);
							$import_data[$this->getNameFieldName()] = $new;
						}
						break;
					case "parent_id":
						if($new != $old[$fld]) {
							$names = $this->getAObjectName(array($new, $old[$fld]));
							$c['parent'] = array(
								'to' => isset($names[$new]) ? $names[$new] : $this->getUnspecified(),
								'from' => isset($names[$old[$fld]]) ? $names[$old[$fld]] : $this->getUnspecified(),
								'raw' => array(
									'to' => $new,
									'from' => $old[$fld]
								)
							);
							$import_data[$this->getParentFieldName()] = $new;
						}
						break;
				}
			}

			if(count($c) == 0) {
				$result = array("info", "No changes were found.");
			} else {
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($import_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
				$this->db_update($sql);

				$changes = array(
						'user' => $this->getUserName(),
						'response' => "|".self::OBJECT_TYPE."| ".$this->getRefTag().$object_id." (".$old['name'].") modified:",
					) + $c;
				$log_var = array(
					'section' => self::LOG_SECTION,
					'sdbip_id' => $sdbip_id,
					'object_id' => $object_id,
					'changes' => $changes,
					'log_type' => SDBP6_LOG::RESTORE,
				);
				$this->addActivityLog("setup", $log_var);
				$result = array("ok", "Item successfully modified.");
			}

		}
		return $result;
	}


	public function deactivateObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$action = "deactivate";
		$action_name = $this->getActivityName($action);
		$actioned = "deactivated";
		$actioned_name = $this->getActivityName($actioned);
		$object_name = $this->getObjectName(self::OBJECT_TYPE);

		if(!isset($var['object_id']) || !ASSIST_HELPER::checkIntRef($var['object_id'])) {
			$result = array("error", "An error occurred while trying to identify which item you are trying to ".$action_name.". Please reload the page and try again.");
		} else {
			$object_id = $var['object_id'];
			$old = $this->getRawObjectDetails($object_id);
			$sdbip_id = $old['sdbip_id'];
			$new_status = self::INACTIVE;
			$old_status = $old['status'];
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$this->db_update($sql);
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|".$actioned."| |".self::OBJECT_TYPE."| ".$this->getRefTag().$object_id.": ".$old['name'],
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $object_id,
				'sdbip_id' => $sdbip_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::DEACTIVATE,
			);
			$this->addActivityLog("setup", $log_var);
			$result = array("ok", $object_name." ".$this->getRefTag().$object_id.": ".$old['name']." successfully ".$actioned_name);
		}
		return $result;
	}

	/**
	 * Delete OrgStructure Item #AA-625 JC 3 June 2021
	 * @param $var
	 * @return string[]
	 */
	public function deleteObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$action = "delete";
		$action_name = $this->getActivityName($action);
		$actioned = "deleted";
		$actioned_name = $this->getActivityName($actioned);
		$object_name = $this->getObjectName(self::OBJECT_TYPE);

		if(!isset($var['object_id']) || !ASSIST_HELPER::checkIntRef($var['object_id'])) {
			$result = array("error", "An error occurred while trying to identify which item you are trying to ".$action_name.". Please reload the page and try again.");
		} else {
			$object_id = $var['object_id'];
			$old = $this->getRawObjectDetails($object_id);
			$sdbip_id = $old['sdbip_id'];
			$new_status = self::DELETED;
			$old_status = $old['status'];
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$this->db_update($sql);
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|".$actioned."| |".self::OBJECT_TYPE."| ".$this->getRefTag().$object_id.": ".$old['name'],
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $object_id,
				'sdbip_id' => $sdbip_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::DELETE,
			);
			$this->addActivityLog("setup", $log_var);
			$result = array("ok", $object_name." ".$this->getRefTag().$object_id.": ".$old['name']." successfully ".$actioned_name);
		}
		return $result;
	}

	public function restoreObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");

		$action = "restore";
		$action_name = $this->getActivityName($action);
		$actioned = "restored";
		$actioned_name = $this->getActivityName($actioned);
		$object_name = $this->getObjectName(self::OBJECT_TYPE);

		if(!isset($var['object_id']) || !ASSIST_HELPER::checkIntRef($var['object_id'])) {
			$result = array("error", "An error occurred while trying to identify which item you are trying to ".$action_name.". Please reload the page and try again.");
		} else {
			$object_id = $var['object_id'];
			$old = $this->getRawObjectDetails($object_id);
			$sdbip_id = $old['sdbip_id'];
			$new_status = self::ACTIVE;
			$old_status = $old['status'];
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$this->db_update($sql);
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|".$actioned."| |".self::OBJECT_TYPE."| ".$this->getRefTag().$object_id.": ".$old['name'],
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $object_id,
				'sdbip_id' => $sdbip_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::RESTORE,
			);
			$this->addActivityLog("setup", $log_var);
			$result = array("ok", $object_name." ".$this->getRefTag().$object_id.": ".$old['name']." successfully ".$actioned_name);
		}
		return $result;
	}


	public function sortObject($var) {
		$parent_id = $var['parent_id'];
		$raw_order = $var['order'];
		$new_order = $raw_order;


		//fetch any deactivated items to be added to the end
		$sql = "SELECT org_id as id FROM ".$this->getTableName()." WHERE org_parent_id = ".$parent_id." AND (org_status & ".self::ACTIVE.")<>".self::ACTIVE." ORDER BY org_order";
		$inactives = $this->mysql_fetch_all_by_id($sql, "id");
		foreach($inactives as $key => $x) {
			$new_order[] = $key;
		}

		if($parent_id > 0) {
			$old = $this->getRawObjectDetails($parent_id);
		} else {
			if(count($new_order) > 0) {
				$my_key = $new_order[0];
				$old = $this->getRawObjectDetails($my_key);
			} else {
				$old = array('sdbip_id' => $_SESSION[$this->getModRef()]['MY_CURRENT_SDBIP']);
			}
		}
		$sdbip_id = $old['sdbip_id'];

		//convert order from array[position]=>id to array[id]=>position
		$order = array_flip($new_order);

		//update order
		foreach($order as $id => $position) {
			$sql = "UPDATE ".$this->getTableName()." SET org_order = ".$position." WHERE org_id = ".$id;
			$this->db_update($sql);
		}


		$changes = array(
			'user' => $this->getUserName(),
			'response' => "Display order changed (".self::REFTAG.implode("; ".self::REFTAG, $raw_order).")",
		);
		$log_var = array(
			'section' => self::LOG_SECTION,
			'object_id' => $parent_id,
			'sdbip_id' => $sdbip_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::SORT,
		);
		$this->addActivityLog("setup", $log_var);
		return array("ok", "Display order changed successfully.");

	}


	/*****************************************************************************************************************************
	 * GET functions
	 */

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getMyLogSection() {
		return self::LOG_SECTION;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}


	public function getAObjectName($object_id) {
		$sql = "SELECT
					org.org_id as id,
					org.org_name as name,
					parent.org_name as parent_name,
					org.org_parent_id as parent_id
				FROM ".$this->getTableName()." org
				LEFT OUTER JOIN ".$this->getTableName()." parent
				ON parent.org_id = org.org_parent_id
				WHERE ";
		if(is_array($object_id)) {
			$object_id = ASSIST_HELPER::removeBlanksFromArray($object_id);
			if(count($object_id) > 0) {
				$sql .= " org.org_id IN (".implode(",", $object_id).") ";
				$rows = $this->mysql_fetch_all_by_id($sql, "id");
				$data = array();
				foreach($rows as $id => $row) {
					if($row['parent_id'] == 0) {
						$data[$id] = $row['name'];
					} else {
						$data[$id] = $row['parent_name']." - ".$row['name'];
					}
				}
				return $data;
			} else {
				return array();
			}
		} else {
			$sql .= " org.org_id = ".$object_id."";
			$row = $this->mysql_fetch_one($sql);
			if($row['parent_id'] == 0) {
				return $row['name'];
			} else {
				return $row['parent_name']." - ".$row['name'];
			}
		}
	}

	public function getOrgStructureForFixedReport() {
		return $this->getOrgStructureFromDBForSetup(true, $this->sdbip_id);
	}

	public function getOrgStructureFromDBForSetup($active_only = true, $sdbip_id = 0) {
		$sql = "SELECT
					org_id as id,
					CONCAT('".self::REFTAG."',org_id) as ref,
					org_parent_id as parent_id,
					org_order as display_order,
					org_status as status,
					org_name as name,
					IF(org_parent_id=0,1,0) as has_children
				FROM ".$this->getTableName()."
				WHERE (org_status & ".self::DELETED.") <> ".self::DELETED."
				".($active_only ? " AND (org_status & ".self::ACTIVE.") = ".self::ACTIVE : "")."
				AND ".$this->getSDBIPParentFieldName()." = ".$sdbip_id."
				ORDER BY org_parent_id, org_order";
		$rows = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id", true);
		return $rows;
	}


	public function getRawObjectDetails($object_id, $include_used_by = false) {
		$sql = "SELECT ME.org_id as id
					, ME.org_name as name
					, ME.org_status as status
					, ME.org_parent_id as parent_id
					, ME.org_sdbip_id as sdbip_id
					, PARENT.org_name as parent_name
					, IF(ISNULL(PARENT.org_name),ME.org_name,CONCAT_WS(' - ',PARENT.org_name,ME.org_name)) as full_name
					FROM ".$this->getTableName()." ME
					LEFT OUTER JOIN ".$this->getTableName()." PARENT
					  ON ME.org_parent_id = PARENT.org_id
					WHERE ME.org_id = ".$object_id;
		$row = $this->mysql_fetch_one($sql);
		if($include_used_by) {
			$sdbip_id = $row['sdbip_id'];
			//if top of the food chain then check for children + TL KPIs
			$results = array();
			if($row['parent_id'] == 0) {
				$children = $this->getChildren($object_id, $sdbip_id);
				$results['children'] = count($children);
				$topObject = new SDBP6_TOPKPI();
				$tl_kpis = $topObject->getActiveObjectsFormattedForSelect($sdbip_id, array('department' => $object_id));
				$results['TL'] = count($tl_kpis);
				if($results['children'] == 0 && $results['TL'] == 0) {
					$results_string = false;
				} else {
					$results_string = array();
					if($results['children'] > 0) {
						$results_string[] = $results['children']." ".($results['children'] > 1 ? "children" : "child");
					}
					if($results['TL'] > 0) {
						$results_string[] = $results['TL']." ".($results['TL'] > 1 ? $topObject->getObjectName($topObject->getMyObjectName(true)) : $topObject->getObjectName($topObject->getMyObjectName()));
					}
					$results_string = implode(" and ", $results_string);
				}
				$row['used_by'] = $results_string;
			} //else check for Dept.KPIs & Projects
			else {
				$kpiObject = new SDBP6_DEPTKPI();
				$kpis = $kpiObject->getActiveObjectsFormattedForSelect($sdbip_id, array('department' => $object_id));
				$results['D'] = count($kpis);
				$projectObject = new SDBP6_PROJECT();
				$projs = $projectObject->getActiveObjectsFormattedForSelect($sdbip_id, array('department' => $object_id));
				$results['P'] = count($projs);
				if($results['D'] == 0 && $results['P'] == 0) {
					$results_string = false;
				} else {
					$results_string = array();
					if($results['D'] > 0) {
						$results_string[] = $results['D']." ".($results['D'] > 1 ? $kpiObject->getObjectName($kpiObject->getMyObjectName(true)) : $kpiObject->getObjectName($kpiObject->getMyObjectName()));
					}
					if($results['P'] > 0) {
						$results_string[] = $results['P']." ".($results['P'] > 1 ? $projectObject->getObjectName($projectObject->getMyObjectName(true)) : $projectObject->getObjectName($projectObject->getMyObjectName()));
					}
					$results_string = implode(" and ", $results_string);
				}
				$row['used_by'] = $results_string;
			}
		}
		return $row;
	}


	public function getChildren($parent_id, $sdbip_id) {
		$sql = "SELECT org_id as id, org_name as name 
				FROM ".$this->getTableName()."
				WHERE org_parent_id = ".$parent_id."
				AND org_sdbip_id = ".$sdbip_id."
				AND (org_status = ".self::ACTIVE." OR org_status = ".self::INACTIVE.")";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}


	public function getMyParentID($child_id) {
		$row = $this->getRawObjectDetails($child_id);
		return $row['parent_id'];
	}


	public function getListOfActiveParentsFormattedForSelect($sdbip_id = false) {
		//Updated to take SDBIP_ID into account - AA-540 JC 17 March 2021
		$sql = "SELECT org_id as id, org_name as name 
				FROM ".$this->getTableName()."
				WHERE org_parent_id = 0 ";
		if($sdbip_id == false) {
			$sql .= "
				".($this->sdbip_id > 0 ? " AND org_sdbip_id = ".$this->sdbip_id : "")."";
		} else {
			if(is_array($sdbip_id) && count(ASSIST_HELPER::removeBlanksFromArray($sdbip_id)) > 0) {
				$sql .= "
					 AND org_sdbip_id IN (".implode(",", $sdbip_id).")";
			} elseif(!is_array($sdbip_id) && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($sdbip_id)) {
				$sql .= "
					 AND org_sdbip_id = ".$sdbip_id."";
			}
		}
		$sql .= "
				AND org_status = ".self::ACTIVE." 
				ORDER BY org_order";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
	}


	public function getParentOfChildren() {
		$sql = "SELECT ME.org_id as id
					, ME.org_name as name
					, ME.org_status as status
					, ME.org_parent_id as parent_id
					, PARENT.org_name as parent_name
					, IF(ISNULL(PARENT.org_name),ME.org_name,CONCAT_WS(' - ',PARENT.org_name,ME.org_name)) as full_name
					FROM ".$this->getTableName()." ME
					LEFT OUTER JOIN ".$this->getTableName()." PARENT
					  ON ME.org_parent_id = PARENT.org_id
					WHERE ".$this->getActiveStatusSQL("ME");
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$data[$r['id']] = $r['parent_id'];
		}
		return $data;
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getAllListItemsFormattedForSelect($options = array()) {
		return $this->getActiveObjectsFormattedForSelect($options, false);
	}

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getActiveListItemsFormattedForSelect($options = array()) {
		return $this->getActiveObjectsFormattedForSelect($options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($options = array(), $active_only = true) { //$this->arrPrint($options);
		if(!is_array($options)) {
			if($options == "TOP") {
				$options = array('parent_id' => 0);
			} else {
				$options = array();
			}
		}
		if(!isset($options['include_parent_name'])) {
			$options['include_parent_name'] = true;
		}
		$sql = "SELECT
					org_id as id,
					CONCAT('".self::REFTAG."',org_id) as ref,
					org_parent_id as parent_id,
					org_order as display_order,
					org_status as status,
					org_name as name
				FROM ".$this->getTableName()."
				WHERE ".($active_only === false ? ("(org_status & ".self::DELETED.") <> ".self::DELETED) : ("(org_status & ".self::ACTIVE.") = ".self::ACTIVE))."
				";
		if(isset($options['parent_id'])) {
			$sql .= " AND (org_parent_id = ".$options['parent_id']." OR org_id = ".$options['parent_id'].")";
		}
		if(isset($options['sdbip_id'])) {
			$sql .= " AND (org_sdbip_id = ".$options['sdbip_id'].")";
		} elseif($this->sdbip_id > 0) {
			$sql .= " AND (org_sdbip_id = ".$this->sdbip_id.")";
		}
		$sql .= "
				ORDER BY org_parent_id, org_order";
		$rows = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id", true);
		$items = array();
		if(isset($rows[0]) && count($rows[0]) > 0) {
			foreach($rows[0] as $key => $row) {
				if(!isset($options['parent_id']) || $options['parent_id'] != 0) {
					$name1 = $row['name'];
					if(isset($rows[$key])) {//} && (is_null($filter) || $filter==$key)) {
						foreach($rows[$key] as $k => $r) {
							$items[$k] = ($options['include_parent_name'] == true ? $name1." - " : "").$r['name'];
						}
					}
				} else {
					$items[$key] = $row['name'];
				}
			}
		}
		return $items;
	}

	public function getActiveListItemForGraphReports($options = array()) {
		return $this->getActiveObjectsFormattedForSelect($options);
	}

	public function getActiveObjectsFormattedForFiltering($include_parent_name = false) {
		$parents = $this->getListOfActiveParentsFormattedForSelect();
		$data = array();
		foreach($parents as $p => $n) {
			$subs = $this->getActiveObjectsFormattedForSelect(array('parent_id' => $p, 'include_parent_name' => $include_parent_name));
			if(count($subs) > 0) {
				$data['DIR_'.$p] = $n;
				foreach($subs as $s => $i) {
					$data['SUB_'.$s] = ($include_parent_name == false ? " - " : "").$i;
				}
			}
		}
		return $data;
	}

	public function getActiveParentObjectsFormattedForFiltering() {
		$parents = $this->getListOfActiveParentsFormattedForSelect();
		$data = array();
		if(count($parents) > 0) {
			foreach($parents as $p => $n) {
				$data['DIR_'.$p] = $n;
			}
		}
		return $data;
	}




	/*****************************************************************************************************************************
	 * SET functions
	 */


	/**
	 * @param $source_sdbip_id
	 * @param $destination_sdbip_id
	 * @return array of org_ids (src=>dest) - needed for copying admins
	 */
	public function copySDBIPSpecificSetup($source_sdbip_id, $destination_sdbip_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE org_sdbip_id = ".$source_sdbip_id." AND org_parent_id = 0 AND org_status = 2 ORDER BY org_order, org_name";
		$parents = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$org_ids = array();
		$order = 1;
		foreach($parents as $id => $row) {
			$sql = $sql = "SELECT * FROM ".$this->getTableName()." WHERE org_parent_id = ".$id." AND org_status = 2 ORDER BY org_order, org_name";
			$children = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
			$insert_data = array(
				'name' => $row['org_name'],
				'parent_id' => 0,
				'order' => $order,
				'sdbip_id' => $destination_sdbip_id,
			);
			$new_parent_add_result = $this->addObject($insert_data);
			$new_parent_id = $new_parent_add_result[2];
			$this->recordImportOfListItemFromLocalModule($new_parent_id, $source_sdbip_id, $id, self::OBJECT_TYPE, "OBJECT|".__CLASS__);    //needed for import from SDBP6 [JC] AA-419 8 June 2020
			$org_ids[$id] = $new_parent_id;
			if(count($children) > 0) {
				$child_order = 1;
				foreach($children as $child) {
					$name = $child['org_name'];
					$insert_data = array(
						'name' => $name,
						'parent_id' => $new_parent_id,
						'order' => $child_order,
						'sdbip_id' => $destination_sdbip_id,
					);
					$child_order++;
					$new_child_add_result = $this->addObject($insert_data);
					$new_child_id = $new_child_add_result[2];
					$this->recordImportOfListItemFromLocalModule($new_child_id, $source_sdbip_id, $child['org_id'], self::OBJECT_TYPE, "OBJECT|".__CLASS__);    //needed for import from SDBP6 [JC] AA-419 8 June 2020
					$org_ids[$child['org_id']] = $new_child_id;
				}
			}
			$order++;
		}

		return $org_ids;
	}


	public function getCopiedListItems($source_sdbip_id, $my_sdbip_id) {
		$local_list = self::OBJECT_TYPE;
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE org_sdbip_id = ".$my_sdbip_id;
		$new_items = $this->mysql_fetch_all_by_id($sql, "org_id");
		$new_ids = array_keys($new_items);
		return $this->getCopiedItemsFromSDBIPSetup($source_sdbip_id, $new_ids, $local_list);
	}


	public function __destruct() {
		parent::__destruct();
	}

}

?>