<?php
/**
 * To manage the functions relating to the TIME PERIODS of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_TIME extends SDBP6 {
	private $time_users = array(
		'primary' => "Primary",
		'secondary' => "Secondary",
		'tertiary' => "Tertiary",
		'finance' => "Finance"
	);

	private $headings = array(
		'rem' => "Reminder",
		'close' => "Closure"
	);

	protected $autojob = false;

//object details
	protected $object_id = 0;
	protected $object_details = array();

//table settings
	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = "sdbip_id";
	protected $name_field = false;
	protected $attachment_field = "attachment";
	protected $has_attachment = false;
	protected $ref_tag = "TP";

//Module constants
	const OBJECT_TYPE = "TIME";
	const PARENT_OBJECT_TYPE = "SDBIP";

	const TABLE = "setup_time";
	const TABLE_FLD = "tp";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "setup";
	const LOG_SECTION = "TIME";

//Time management codes
	const OPEN = 1;
	const CLOSED = 2; //or = 0
	const FORCE_CLOSED = 4;

	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		$this->autojob = $autojob;
//		$this->local_cmpcode = $cmpcode;
		parent::__construct($modref, $autojob, $cmpcode);
		$this->local_modref = $modref;
		foreach($this->time_users as $tu => $tun) {
			$this->headings['rem_'.$tu] = $tun." - ".$this->headings['rem'];
			$this->headings['close_'.$tu] = $tun." - ".$this->headings['close'];
		}
		if($this->is_full_module !== true) {
			unset($this->time_users['finance']);
		}
	}


	public function addObject($var) {
		$result = array("info", "Sorry, this functionality still needs to be programmed.".serialize($var));
		$error = false;
		$type = $var['type'];
		$sdbip_id = $var['sdbip_id'];
		if($type == "year") {
			//check that time periods haven't already been created for the given SDBIP
			$test_periods = $this->getActiveTimePeriods($sdbip_id);
			if(count($test_periods) == 0) {
				$year_start_date = strtotime($var['start_date']." 00:00:00");
				$year_end_date = strtotime($var['end_date']." 23:59:59");
				//check for valid start and end dates - does the end date fall after the start date?
				if(($year_end_date) < ($year_start_date)) {
					$error = true;
					$result = array("error", "Invalid ".$this->getObjectName("financialyear")." start and end dates provided.  Unable to continue.  Error: End date falls before start date.");
				} else {    //else for if check for valid start and end dates - does the end date fall after the start date?
					//if start date is not the first day of the month
					if(date("d", $year_start_date) * 1 != 1) {
						//check if the end date has a day that is 1 day less than the given start date (i.e. 15 July 2018 to 14 July 2019)
						//if yes then use those days to create the time periods
						if(date("d", $year_start_date) * 1 == (date("d", $year_end_date) * 1 + 1)) {
							$day = date("d", $year_start_date) * 1;
							$month = date("m", $year_start_date) * 1;
							$year = date("Y", $year_start_date) * 1;
							$tp_start = mktime(0, 0, 0, $month, $day, $year);

							$eday = date("d", $year_end_date) * 1;
							$emonth = date("m", $year_end_date) * 1;
							$eyear = date("Y", $year_end_date) * 1;
							$tp_end = mktime(23, 59, 59, $emonth, $eday, $eyear);
							//loop through the months and add each as time periods individually
							while($tp_start < $year_end_date && $tp_end <= $year_end_date) {
								$add_var = array(
									'type' => "single",
									'start' => $tp_start,
									'end' => $tp_end
								);
								$result = $this->addObject($add_var);
								//prep for next period
								$year = ($month == 12 ? $year + 1 : $year);
								$month = ($month == 12 ? 1 : $month + 1);
								$tp_start = mktime(00, 00, 00, $month, $day, $year);
								$eyear = ($emonth == 12 ? $eyear + 1 : $eyear);
								$emonth = ($emonth == 12 ? 1 : $emonth + 1);
								$tp_end = mktime(23, 59, 59, $emonth, $eday, $eyear);
							}
						} else {
							$day = date("d", $year_start_date) * 1;
							$month = date("m", $year_start_date) * 1;
							$year = date("Y", $year_start_date) * 1;
							$tp_start = mktime(0, 0, 0, $month, $day, $year);
							$tp_end = mktime(23, 59, 59, $month, date("t", $year_start_date) * 1, $year);
							//create filler time period to get start date to first day
							$add_var = array(
								'type' => "single",
								'start' => $tp_start,
								'end' => $tp_end
							);
							$result = $this->addObject($add_var);
							//reset start date to 1 of next month
							$day = 1;
							$year = ($month == 12 ? $year + 1 : $year);
							$month = ($month == 12 ? 1 : $month + 1);
							$tp_start = mktime(00, 00, 00, $month, $day, $year);
							$tp_end = mktime(23, 59, 59, $month, date("t", $tp_start), $year);

							//loop through the months and add each as time periods individually
							while($tp_start < $year_end_date && $tp_end <= $year_end_date) {
								$add_var = array(
									'type' => "single",
									'start' => $tp_start,
									'end' => $tp_end
								);
								$result = $this->addObject($add_var);
								//prep for next period
								$year = ($month == 12 ? $year + 1 : $year);
								$month = ($month == 12 ? 1 : $month + 1);
								$tp_start = mktime(00, 00, 00, $month, $day, $year);
								$tp_end = mktime(23, 59, 59, $month, date("t", $tp_start), $year);
							}
						}
						//otherwise create 1 shortened time period for the first month and then default to monthly after that
					} else {    //else to if start date is not first day of the month
						$day = date("d", $year_start_date) * 1;
						$month = date("m", $year_start_date) * 1;
						$year = date("Y", $year_start_date) * 1;
						$tp_start = mktime(0, 0, 0, $month, $day, $year);
						$tp_end = mktime(23, 59, 59, $month, date("t", $tp_start), $year);
						//loop through the months and add each as time periods individually
						while($tp_start < $year_end_date) {
							$add_var = array(
								'type' => "single",
								'sdbip_id' => $sdbip_id,
								'start' => $tp_start,
								'end' => $tp_end
							);
							$result = $this->addObject($add_var);
							//if insert wasn't successful then roll back changes and exit
							if($result !== true) {
								$this->cancelTimePeriods($sdbip_id);
								$result = array("error", "An error occurred while trying to create the ".$this->getObjectName("TIMES").". All changes have been rolled back.  Please try again.");
								$tp_start = $year_end_date + 1;
							} else {
								//prep for next period
								$year = ($month == 12 ? $year + 1 : $year);
								$month = ($month == 12 ? 1 : $month + 1);
								$tp_start = mktime(00, 00, 00, $month, $day, $year);
								$tp_end = mktime(23, 59, 59, $month, date("t", $tp_start), $year);
							}
						}
					}    //endif start date is not the first date of the month
					//if result[0] is still "info" then assume all successful and adjust result message
					if($result[0] == "info") {
						$result = array("ok", "All ".$this->getObjectName("TIMES")." successfully created.");
					}
				} //endif check for valid start and end dates - does the end date fall after the start date?
			} else { //else for test if sdbip_id doesn't already have time periods
				$result = array("error", $this->getObjectName("TIMES")." have already been created for this ".$this->getObjectName("SDBIP"));
			} //endif test if sdbip_id doesn't already have time periods
		} else {
			$sql = "INSERT INTO ".$this->getTableName()." SET
					sdbip_id = ".$var['sdbip_id']."
					, start_date = '".date("Y-m-d H:i:s", $var['start'])."'
					, end_date = '".date("Y-m-d H:i:s", $var['end'])."'
					, status = ".self::ACTIVE;
			$id = $this->db_insert($sql);
			if($this->checkIntRef($id)) {
				return true;
			} else {
				return false;
			}
		}


		return $result;
	}


	public function editObject($var) {
		$result = array("info", "Sorry, I couldn't edit the object.".serialize($var));

		//what time is being edited & remove from input variable
		$time_id = $var['time_id'];
		unset($var['time_id']);
		//get existing record to check for changes for logging purposes
		$old_record = $this->getRawObject($time_id);
		//get naming for  log
		$x = $this->getTimePeriodNames(array(0 => $old_record), "MONTH");
		$name = $x[0];
		//set insert_data
		$insert_data = array();
		$changes = array();
		$changes['user'] = $this->getUserName();
		$changes['response'] = "|".self::OBJECT_TYPE."| ".$this->getRefTag().$time_id." ($name) edited:";
		//check for changes
		foreach($var as $key => $v) {
			$old = $old_record[$key];
			if($key == "code") {
				$new = ASSIST_HELPER::code($v);
				if($new <> $old) {
					$insert_data[$key] = $new;
					$changes[$key] = array(
						'to' => $new,
						'from' => $old
					);
				}
			} else {
				$new = $v;
				if(strlen($new) > 0) {
					if(substr($key, 0, 3) == "rem") {
						$new = strtotime($v." 00:00:01");
					} else {
						$new = strtotime($v." 23:59:59");
					}
					if($new <> $old) {
						$insert_data[$key] = $new;
						if($old == 0) {
							$old = $this->getUnspecified();
						} else {
							$old = strtotime($old);
						}
						$changes[$key] = array(
							'to' => date("d-M-Y", $new),
							'from' => is_numeric($old) ? date("d-M-Y", $old) : $old,
						);
					}
				} else {
					$new = 0;
					if($new <> $old) {
						$insert_data[$key] = $new;
						$changes[$key] = array(
							'to' => $this->getUnspecified(),
							'old' => is_numeric($old) ? date("d-M-Y", $old) : $old
						);
					}

				}
			}
		}
		if(count($changes) > 2 && count($insert_data) > 0) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$time_id;
			$this->db_update($sql);


			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $time_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);

			$result = array("ok", "Changes saved successfully.");

		} else {
			$result = array("info", "No changes were found to be saved.");
		}


		return $result;
	}


	/**
	 * Function for user to manually close a time period (a.k.a. Force close)
	 */
	public function manualCloseTime($var) {
		$result = array("info", "Sorry, I couldn't close the time period.");

		$time_id = $var['time_id'];
		$tu = $var['tu'];
		$fld = "active_".$tu;

		$old = $this->getRawObject($time_id);

		if($old[$fld] == SDBP6_SETUP_TIME::OPEN) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$fld." = ".SDBP6_SETUP_TIME::FORCE_CLOSED.", close_".$tu." = 0 WHERE ".$this->getIDFieldName()." = ".$time_id;
			$this->db_update($sql);

			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|".$this->getMyObjectType()."| ".$this->getRefTag().$time_id." manually closed for ".$this->time_users[$tu]." users.",
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $time_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);

			$result = array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." closed successfully.");

		} else {
			$result = array("info", "Sorry, ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." could not be closed as it is not open.");
		}

		return $result;
	}


	/**
	 * Time period reminders sent by the SYSTEM only
	 */
	public function updateTimePeriodForReminderSent($var) {
		$result = array("error", "Sorry, I couldn't record reminders for the time period.");

		$time_id = $var['time_id'];
		$tu = $var['tu'];
		$fld = "rem_".$tu;


		$sql = "UPDATE ".$this->getTableName()." SET ".$fld." = 0 WHERE ".$this->getIDFieldName()." = ".$time_id;
		$this->db_update($sql);

		$changes = array(
			'user' => "Assist Auto Updates",
			'response' => "|".$this->getMyObjectType()."| ".$this->getRefTag().$time_id." reminders sent for ".$this->time_users[$tu]." users.",
		);
		$log_var = array(
			'section' => self::LOG_SECTION,
			'object_id' => $time_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::EDIT,
		);
		$this->addActivityLog("setup", $log_var, $this->local_modref, $this->autojob, $this->local_cmpcode);

		$result = array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." reminders sent successfully.");


		return $result;
	}

	/**
	 * Time period closure by the SYSTEM only
	 */
	public function closeTime($var) {
		$result = array("error", "Sorry, I couldn't close the time period.");

		$time_id = $var['time_id'];
		$tu = $var['tu'];
		$fld = "active_".$tu;
		//used to correct log date when auto closures fail #AA-349
		$force_log_date = isset($var['insertdate']) ? $var['insertdate'] : false;

		$old = $this->getRawObject($time_id);

		if($old[$fld] == SDBP6_SETUP_TIME::OPEN) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$fld." = ".SDBP6_SETUP_TIME::CLOSED.", close_".$tu." = 0 WHERE ".$this->getIDFieldName()." = ".$time_id;
			$this->db_update($sql);

			$changes = array(
				'user' => "Assist Auto Updates",
				'response' => "|".$this->getMyObjectType()."| ".$this->getRefTag().$time_id." closed for ".$this->time_users[$tu]." users.",
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $time_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			//used to correct log date when auto closures fail #AA-349
			if($force_log_date !== false) {
				$log_var['insertdate'] = $force_log_date;
			}
			$this->addActivityLog("setup", $log_var, $this->local_modref, $this->autojob, $this->local_cmpcode);

			$result = array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." closed successfully.");

		} else {
			$changes = array(
				'user' => "Assist Auto Updates",
				'response' => "Attempted to close |".$this->getMyObjectType()."| ".$this->getRefTag().$time_id." for ".$this->time_users[$tu]." users unsuccessfully.  |".$this->getMyObjectType()."| not open.",
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $time_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var, $this->local_modref, $this->autojob, $this->local_cmpcode);
			$result = array("error", "Sorry, ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." could not be closed as it is not open.");
		}
		return $result;
	}

	/**
	 * Function to reopen closed time period - only available via Admin section
	 */
	public function openTime($var) {
		$result = array("info", "Sorry, I couldn't open the time period.");

		$time_id = $var['time_id'];
		$tu = $var['tu'];
		$fld = "active_".$tu;

		$old = $this->getRawObject($time_id);

		if($old[$fld] != SDBP6_SETUP_TIME::OPEN) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$fld." = ".SDBP6_SETUP_TIME::OPEN." WHERE ".$this->getIDFieldName()." = ".$time_id;
			$this->db_update($sql);

			$changes = array('user' => $this->getUserName(),
				'response' => "|".$this->getMyObjectType()."| ".$this->getRefTag().$time_id." reopened for ".$this->time_users[$tu]." users.",);
			$log_var = array('section' => self::LOG_SECTION,
				'object_id' => $time_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,);
			$this->addActivityLog("setup", $log_var);
			//set new display_status
			$now = $this->module_time;
			$start_date = strtotime($old['start_date']);
			$new_result = "";
			if($now < $start_date) {
				$new_result = ASSIST_HELPER::getDisplayIconAsDiv("ui-icon-radio-on", "", "orange")."&nbsp;Not yet started";
			} else {
				$new_result = ASSIST_HELPER::getDisplayIconAsDiv("ok")."&nbsp;Open";
			}
			$result = array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." opened successfully.", $new_result);
		} elseif($old['close_'.$tu] < $this->module_time) {
			//error trap to catch time periods which were closed by setting past closure date
			$var2 = array(
				'time_id' => $time_id,
				'close_'.$tu => 0
			);
			$result = $this->editObject($var2);
			if($result[0] == "ok") {
				$start_date = strtotime($old['start_date']);
				if($this->module_time < $start_date) {
					$new_result = ASSIST_HELPER::getDisplayIconAsDiv("ui-icon-radio-on", "", "orange")."&nbsp;Not yet started";
				} else {
					$new_result = ASSIST_HELPER::getDisplayIconAsDiv("ok")."&nbsp;Open";
				}
				$result = array("ok", "Opened ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." by removing historial closure date.", $new_result);
			} else {
				$result = array("info", "Sorry, ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." could not be opened as it is already open.");
			}
		} else {
			$result = array("info", "Sorry, ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$time_id." could not be opened as it is already open.");
		}

		return $result;
	}


	/*************************************************************************
	 * General functions
	 */
	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getTimeUsers() {
		return $this->time_users;
	}

	public function getTimeHeadings() {
		return $this->headings;
	}


	/**
	 * get specific record from the database unformatted
	 */
	public function getRawObject($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		return $this->mysql_fetch_one($sql);
	}

	/**
	 * getActiveTimePeriods = get all time periods that have not been DELETED/DEACTIVATED for the given SDBIP
	 * For OPEN time periods: call getOpenTimePeriods
	 */
	public function getActiveTimePeriods($sdbip_id = false, $time_id = 0) {
		$sql = "SELECT *
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				".($sdbip_id !== false ? " AND ".$this->getParentFieldName()." = ".$sdbip_id." " : "")."
				".($time_id > 0 ? " AND ".$this->getIDFieldName()." = ".$time_id : "")."
				ORDER BY start_date, end_date";
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		return $rows;
	}


	/**
	 * Get list of active objects ready to populate a SELECT element
	 *
	 */

	public function getActiveTimeObjectsFormattedForSelect($sdbip_id, $time_type = "MONTH") {
		$rows = $this->getActiveTimePeriods($sdbip_id);
		$rows = $this->getTimePeriodNames($rows, $time_type);
		return $rows;
	}

	public function getAllTimePeriodsRegardlessOfYear($time_type = "MONTH") {
		$rows = $this->getActiveTimePeriods(false);
		$rows = $this->getTimePeriodNames($rows, $time_type);
		return $rows;
	}

	public function getTimePeriodsWithCodeForFinances($sdbip_id, $time_type = "MONTH") {
		$rows = $this->getActiveTimePeriods($sdbip_id);
		$names = $this->getTimePeriodNames($rows, $time_type);
		$data = array();
		foreach($rows as $key => $r) {
			$data[$key] = array('name' => $names[$key], 'code' => $r['code']);
		}
		return $data;
	}

	/**
	 * Get list of active objects ready to populate the FILTER element
	 *
	 */
	public function getActiveTimeObjectsFormattedForFiltering($sdbip_id, $time_type = "MONTH", $time_id = 0, $include_deadline = false, $limit = "ALL") { //echo $time_type;
		$rows = $this->getActiveTimePeriods($sdbip_id);
		$names = $this->getTimePeriodNames($rows, $time_type);
		$included_time_periods = array();//use to note which time periods are included in quarters etc
		//if time_type != month then reformat rows
		if($time_type != "MONTH") {
			$original = $rows;
			$rows = array();
			$keys = array_keys($original);
			$start = array();
			$inc = array();
			foreach($keys as $t_id) {
				//if start array is blank then set it so that you know what your start date is
				$inc[] = $t_id;
				if(count($start) == 0) {
					$start = $original[$t_id];
				} elseif(isset($names[$t_id])) {
					//otherwise if the time_id exists in names array then assume that you're at the end of a period
					$end = $original[$t_id];
					//set the start date of the period
					$end['start_date'] = $start['start_date'];
					//save the result
					$rows[$t_id] = $end;
					$included_time_periods[$t_id] = $inc;
					//when you're done, then clear start so that the next iteration can set it again
					$start = array();
					$inc = array();
				} else {
					//otherwise do nothing and iterate again
				}
			}
			//reset variables
			$start = array();
			$end = array();
			$keys = array();
			$inc = array();
		}
		//prep variables
		$data = array();
		//needed to determine which time period to allocate as current if TODAY's date falls outside of the financial year of the SDBIP
		if(count($rows) > 0) {
			$keys = array_keys($rows);
			//$codes = array_flip($keys);
			$first_key = $keys[0];
			$last_key = $keys[count($keys) - 1];
		} else {
			$keys = array();
			$first_key = false;
			$last_key = false;
		}
		$has_current = false;
		//needed to make it easier to work out which is the previous / next tp for list pages
		$previous_tp = false;
		$next_tp = false;
		//loop through the rows and process
		//foreach($rows as $key => $row) {
		foreach($keys as $i => $key) {
			//get the row
			$row = $rows[$key];
			//determine previous and next time periods
			$prev_i = $i - 1;
			if($prev_i < 0 || !isset($keys[$prev_i])) {
				$prev_key = false;
			} else {
				$prev_key = $keys[$prev_i];
			}
			$next_i = $i + 1;
			if($next_i >= count($keys) || !isset($keys[$next_i])) {
				$next_key = false;
			} else {
				$next_key = $keys[$next_i];
			}
			//prep row specific variables
			$is_current = false;
			$has_started = false;
			$has_ended = false;
			//check if this is the last time period and a previous current hasn't been found
			//if end date falls before today then use this as the current
			//i.e. user is looking at the sdbip after the end of the financial year
			if(!$has_current && $key == $last_key && strtotime($row['end_date']) < $this->module_time) {
				$is_current = true;
				$has_current = true;
			}
			//check if the time period has already started (NOT if it is open, but just if the start date has passed)
			if(strtotime($row['start_date']) < $this->module_time) {
				$has_started = true;
			}
			//check if the time period has already ended
			if(strtotime($row['end_date']) < $this->module_time) {
				$has_ended = true;
			}
			//if current has not already been found, then check if this tp is the current
			//i.e. today falls after the start and before the end
			if(!$has_current && $has_started && !$has_ended) {
				$is_current = true;
				$has_current = true;
			}
			if($limit != "YTD" || $has_started == true) {
				if(!is_array($limit) || ($key >= $limit['start'] && $key <= $limit['end'])) {
					//set variables
					$inc = $time_type == "MONTH" ? array($key) : $included_time_periods[$key];
					$data[$key] = array('name' => $names[$key],
						'prev' => $prev_key,
						'next' => $next_key,
						'is_current' => $is_current,
						'has_started' => $has_started,
						'has_ended' => $has_ended,
						'included_time_periods' => $inc,);
					//check for open time periods per time user
					//only allow time periods which have started to be deemed open, otherwise they are closed! - strict rule - TPs can only be updated after the first day of the months has passed
					foreach($this->time_users as $tu => $tuh) {
						$data[$key]['is_open'][$tu] = false;
						if($has_started == true) {
							if(($row['active_'.$tu] == 1 || $row['active_'.$tu] == SDBP6_SETUP_TIME::OPEN) && ($row['close_'.$tu] == 0 || $this->module_time < $row['close_'.$tu])) {
								$data[$key]['is_open'][$tu] = true;
							}
						}
					}
					if($include_deadline) {
						foreach($this->time_users as $tu => $tuh) {
							$data[$key]['deadline'][$tu] = $row['close_'.$tu];
						}
					}
				}
			}
		}
		//if no current time period has been found, then assume that you are looking at it before the start of hte financial year
		//therefore first tp is current
		//BUT only do this if there is a record to mark as current - needed to handle situations where this is called without valid sdbip_id
		if($has_current == false && $first_key !== false && $this->checkIntRef($first_key) && count($data) > 0 && count($rows) > 0) {
			$data[$first_key]['is_current'] = true;
		}
		if(ASSIST_HELPER::checkIntRef($time_id) && $time_id > 0) {
			return $data[$time_id];
		} else {
			return $data;
		}
	}


	/**
	 * Get a time period name
	 */
	public function getATimePeriodName($sdbip_id, $time_id, $time_type = "MONTH") {
		$time_details = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, $time_type, $time_id);
		return $time_details['name'];
	}

	/**
	 * Determine name for time period
	 */
	public function getTimePeriodNames($rows, $time_type) {
		$items = array();
		$c = 1;
		foreach($rows as $id => $row) {
			if($time_type == "MONTH" || ($c == 1 && $time_type == "QUARTER")) {
				$start = strtotime($row['start_date']);
			}
			if($time_type == "MONTH" || ($c == 3 && $time_type == "QUARTER")) {
				$end = strtotime($row['end_date']);
				if(date("F Y", $start) == date("F Y", $end)) {
					$name = date("F Y", $end);
				} elseif((date("m", $end) == (date("m", $start) + 2)) || (date("Y", $end) == (date("Y", $start) + 1) && (date("m", $end) + 12 == (date("m", $start) + 2)))) {
					$name = "Quarter ending ".date("F Y", $end);
				} else {
					$name = date("d M Y", $start)." - ".date("d M Y", $end);
				}
				$items[$id] = $name;
				$c = 1;
			} else {
				$c++;
			}
		}
		return $items;
	}


	/**
	 * getActiveTimePeriods = get all time periods that have not been DELETED/DEACTIVATED for the given SDBIP
	 * Formatted for display in list table in Manage / Admin > Time
	 */
	public function getActiveTimePeriodsForSetup($sdbip_id, $section = "MANAGE") {
		$rows = $this->getActiveTimePeriods($sdbip_id);
		$rows = $this->formatTimePeriodForSetup($rows, $section);
		return $rows;
	}


	/**
	 * Get details of a single time period formatted for the dialog edit form
	 */
	public function getDetailsForEdit($var) {
		$time_id = $var['time_id'];
		$section = isset($var['section']) ? $var['section'] : "MANAGE";

		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$time_id;
		$data = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());

		$data = $this->formatTimePeriodForSetup($data, $section);
//		$data['ref'] = $this->getRefTag().$time_id;
//		$data['display_start'] = date("d F Y",strtotime($data['start_date']));
//		$data['display_end'] = date("d F Y",strtotime($data['end_date']));

		$data = $data[$time_id];

		return $data;
	}


	/**
	 * Function to read and format time periods for the admin / manage > Time pages
	 * Called by: getActiveTimePeriodsForSetup & getDetailsForEdit
	 */
	private function formatTimePeriodForSetup($rows, $section = "MANAGE") {
		$time_users = $this->getTimeUsers();
		$time_users = array_reverse($time_users, true);
		$now = $this->module_time;
		foreach($rows as $time_id => $r) {
			$r['ref'] = $this->getRefTag().$time_id;
			$start_date = strtotime($r['start_date']);
			$end_date = strtotime($r['end_date']);
			//format start/end dates
			$r['display_start'] = date("d M Y", $start_date);
			$r['display_end'] = date("d M Y", $end_date);
			//format for time users
			foreach($time_users as $tu => $tu_name) {
				$r[$tu] = array();
				$r[$tu]['status'] = 0;
				$r[$tu]['display_status'] = "";
				$r[$tu]['is_open'] = false;
				//check statuses - ONLY ACT ON LOCAL STATUS = ignore parent status for this module - contrary to SDBP5B [JC 2018-08-29]
				if($r['active_'.$tu] == SDBP6_SETUP_TIME::FORCE_CLOSED) {
					$r[$tu]['display_status'] = ASSIST_HELPER::getDisplayIconAsDiv("error")."&nbsp;Manually Closed";
					$r[$tu]['status'] = SDBP6_SETUP_TIME::FORCE_CLOSED;
				} elseif($now < $start_date) {
					$r[$tu]['display_status'] = ASSIST_HELPER::getDisplayIconAsDiv("ui-icon-radio-on", "", "orange")."&nbsp;Not yet started";
					$r[$tu]['status'] = SDBP6_SETUP_TIME::OPEN;
					$r[$tu]['is_open'] = true;
				} elseif(($r['active_'.$tu] == 1 || $r['active_'.$tu] == SDBP6_SETUP_TIME::OPEN) && ($r['close_'.$tu] == 0 || $now < $r['close_'.$tu])) {
					$r[$tu]['display_status'] = ASSIST_HELPER::getDisplayIconAsDiv("ok")."&nbsp;Open";
					$r[$tu]['status'] = SDBP6_SETUP_TIME::OPEN;
					$r[$tu]['is_open'] = true;
				} else {
					$r[$tu]['display_status'] = ASSIST_HELPER::getDisplayIconAsDiv("error")."&nbsp;Closed";
					$r[$tu]['status'] = SDBP6_SETUP_TIME::CLOSED;
				}
				//format dates
				$r['can_edit'] = true;
				if($r[$tu]['status'] == SDBP6_SETUP_TIME::CLOSED || $r[$tu]['status'] == SDBP6_SETUP_TIME::FORCE_CLOSED) {
					$r[$tu]['rem'] = "-";
					$r[$tu]['close'] = "-";
				} else {
					if($r['rem_'.$tu] == 0) {
						$r[$tu]['rem'] = "-";
					} else {
						$r[$tu]['rem'] = date("d M Y", $r['rem_'.$tu]);
					}
					if($start_date < $now) {
						$r[$tu]['rem_min'] = "+1D";
					} else {
						$earlier = new DateTime(date("Y-m-d", $now));
						$later = new DateTime(date("Y-m-d", $start_date));
						$diff = $later->diff($earlier)->format("%a");
						$r[$tu]['rem_min'] = "+".$diff."D";
					}
					if($r['close_'.$tu] == 0) {
						$r[$tu]['close'] = "-";
					} else {
						$r[$tu]['close'] = date("d M Y", $r['close_'.$tu]);
					}
					if($end_date < $now) {
						$r[$tu]['close_min'] = "+1D";
					} else {
						//$earlier = new DateTime(date("Y-m-d",$now));
						//$later = new DateTime(date("Y-m-d",$end_date));
						//$diff = $later->diff($earlier)->format("%a");
						//$r[$tu]['close_min'] = "+".($diff+1)."D";
						$r[$tu]['close_min'] = date("Y-m-d", $end_date + (12 * 60 * 60));

					}
				}

			}
			//can edit?
			$rows[$time_id] = $r;
		}

		return $rows;
	}


	/*************************************
	 * Functions needed by FRONTPAGE DASHBOARD
	 */
	public function getAvailableTimePeriodsForFrontPageDashboard($user_access, $future, $sdbip_ids = array()) {
		//first get sdbip_id
		if(is_array($sdbip_ids) && count($sdbip_ids) == 0) {
			$sdbipObject = new SDBP6_SDBIP($this->local_modref);
			$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
			if(isset($sdbip['id'])) {
				$sdbip_ids[] = $sdbip['id'];
			}
		}
		$final_time_periods = array();
		if(count($sdbip_ids) > 0) {
			foreach($sdbip_ids as $sdbip_id) {
				$data = array();
				//then look for time periods in that sdbip where user_access closure date is set to <= $future & >time()
				$months = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "MONTH", 0, true);
				foreach($months as $time_id => $time) {
					if(!($time['has_started'] == true && $time['is_open'][$user_access] == true && $time['deadline'][$user_access] > $this->module_time && $time['deadline'][$user_access] <= $future)) {
						unset($months[$time_id]);
					} else {
						if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
							$months[$time_id]['deadline_type'] = "present";
						} elseif($time['deadline'][$user_access] <= $future) {
							$months[$time_id]['deadline_type'] = "future";
						}
						$months[$time_id]['my_deadline'] = $time['deadline'][$user_access];
					}
				}
				$data['MONTH'] = $months;
				$quarters = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "QUARTER", 0, true);
				foreach($quarters as $time_id => $time) {
					if(!($time['has_started'] == true && $time['is_open'][$user_access] == true && $time['deadline'][$user_access] > $this->module_time && $time['deadline'][$user_access] <= $future)) {
						unset($quarters[$time_id]);
					} else {
						if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
							$quarters[$time_id]['deadline_type'] = "present";
						} elseif($time['deadline'][$user_access] <= $future) {
							$quarters[$time_id]['deadline_type'] = "future";
						}
						$quarters[$time_id]['my_deadline'] = $time['deadline'][$user_access];
					}
				}
				$data['QUARTER'] = $quarters;
				$final_time_periods[$sdbip_id] = $data;
			}
		}
		return $final_time_periods;
	}

	public function getAllTimePeriodsForFrontPageDashboard($user_access, $future) {
		$data = array();
		//first get sdbip_id
		$sdbipObject = new SDBP6_SDBIP($this->local_modref);
		$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
		if(isset($sdbip['id'])) {
			$sdbip_id = $sdbip['id'];
			//then look for time periods in that sdbip where user_access closure date is set to <= $future & >time()
			$months = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "MONTH", 0, true);
			foreach($months as $time_id => $time) {
				if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
					$months[$time_id]['deadline_type'] = "present";
				} elseif($time['deadline'][$user_access] <= $future) {
					$months[$time_id]['deadline_type'] = "future";
				}
				$months[$time_id]['my_deadline'] = $time['deadline'][$user_access];
			}
			$data['MONTH'] = $months;
			$quarters = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "QUARTER", 0, true);
			foreach($quarters as $time_id => $time) {
				if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
					$quarters[$time_id]['deadline_type'] = "present";
				} elseif($time['deadline'][$user_access] <= $future) {
					$quarters[$time_id]['deadline_type'] = "future";
				}
				$quarters[$time_id]['my_deadline'] = $time['deadline'][$user_access];
			}
			$data['QUARTER'] = $quarters;
		}
		return $data;
	}


	public function getTimePeriodsForFrontPageDashboard($user_access, $future, $sdbip_id = 0) {
		$data = array('all' => array(), 'available' => array());
		if($sdbip_id == 0) {
			//first get sdbip_id
			$sdbipObject = new SDBP6_SDBIP($this->local_modref);
			$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
			if(isset($sdbip['id'])) {
				$sdbip_id = $sdbip['id'];
			}
		}
		if($sdbip_id > 0) {
			//then look for time periods in that sdbip where user_access closure date is set to <= $future & >time()
			$months = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "MONTH", 0, true);
			$available_months = $months;
			foreach($months as $time_id => $time) {
				if(!($time['has_started'] == true && $time['is_open'][$user_access] == true && $time['deadline'][$user_access] > $this->module_time && $time['deadline'][$user_access] <= $future)) {
					unset($available_months[$time_id]);
				}

				if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
					$months[$time_id]['deadline_type'] = "present";
				} elseif($time['deadline'][$user_access] <= $future) {
					$months[$time_id]['deadline_type'] = "future";
				}
				$months[$time_id]['my_deadline'] = $time['deadline'][$user_access];
				if(isset($available_months[$time_id])) {
					$available_months[$time_id]['deadline_type'] = $months[$time_id]['deadline_type'];
					$available_months[$time_id]['my_deadline'] = $months[$time_id]['my_deadline'];
				}
			}
			$data['all']['MONTH'] = $months;
			$data['available']['MONTH'] = $available_months;
			$quarters = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "QUARTER", 0, true);
			$available_quarters = $quarters;
			foreach($quarters as $time_id => $time) {
				if(!($time['has_started'] == true && $time['is_open'][$user_access] == true && $time['deadline'][$user_access] > $this->module_time && $time['deadline'][$user_access] <= $future)) {
					unset($available_quarters[$time_id]);
				}
				if(date("d F Y", $time['deadline'][$user_access]) == date("d F Y")) {
					$quarters[$time_id]['deadline_type'] = "present";
				} elseif($time['deadline'][$user_access] <= $future) {
					$quarters[$time_id]['deadline_type'] = "future";
				}
				$quarters[$time_id]['my_deadline'] = $time['deadline'][$user_access];
				if(isset($available_quarters[$time_id])) {
					$available_quarters[$time_id]['deadline_type'] = $quarters[$time_id]['deadline_type'];
					$available_quarters[$time_id]['my_deadline'] = $quarters[$time_id]['my_deadline'];
				}
			}
			$data['all']['QUARTER'] = $quarters;
			$data['available']['QUARTER'] = $available_quarters;
		}
		return $data;
	}


	public function getTimeForReportFiltering($sdbip_id, $time_type = "MONTH") {
		$limit = "ALL";
		$rows = $this->getActiveTimeObjectsFormattedForFiltering($sdbip_id, $time_type, 0, false, $limit);
		return $rows;
	}


	/***********************************
	 * Functions for AUTOMATED TASKS (stasks)
	 */

	/**
	 * Function to manage AUTOMATED CLOSURES by stasks
	 * @param $today = UNIX timestamp for the current time
	 */
	public function runAutomatedClosures($today = false, $date_of_last_successful_run = false) {
		if($today === false) {
			$today = strtotime(date("d F Y H:i:s"));
		}
		if($date_of_last_successful_run === false) {
			$date_of_last_successful_run = 0;
		}
		$result = array();
		$time_users = $this->getTimeUsers();
		//get time periods - regardless of SDBIP
		$sql = "SELECT * FROM ".$this->getTableName();
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		//for each time period, check for closure date < today
		foreach($rows as $time_id => $time) {
			foreach($time_users as $tu => $tun) {
				$closure_date = $time['close_'.$tu];
				if($closure_date > $date_of_last_successful_run && $closure_date < $today) {
					//call closeTime($var = array('time_id'=>X,'tu'=>time_user))
					$var = array('time_id' => $time_id, 'tu' => $tu, 'insertdate' => $today);
					$this->closeTime($var);
					//prep data for return to be displayed in stasks/Results log
					$var['start_date'] = $time['start_date'];
					$var['end_date'] = $time['end_date'];
					$var['tu'] = $this->time_users[$var['tu']];
					$time_name = $this->getTimePeriodNames(array($time), "MONTH");
					$var['time_name'] = $time_name[0];
					$result[] = $var;
				}
			}
		}

		if(count($result) == 0) {
			return false;
		} else {
			return $result;
		}


	}


	/**
	 * Function to manage AUTOMATED REMINDERS by stasks
	 * @param $today = UNIX timestamp for the current time
	 */
	public function generateAutomatedReminders($today = false, $module_title, $helpdesk_emails = array(), $date_of_last_successful_run = false) {
		if($today === false) {
			$today = strtotime(date("d F Y H:i:s"));
		}
		if($date_of_last_successful_run === false) {
			$date_of_last_successful_run = 0;
		}
		$result = array();
		$time_users = $this->getTimeUsers();
		$emailObject = new ASSIST_EMAIL();
		$emailObject->setSenderUserID("AUTO");
		$emailObject->setSenderCmpCode($this->local_cmpcode);
		//get user lists per user type
		$userAccessObject = new SDBP6_USERACCESS($this->local_modref, $this->autojob, $this->local_cmpcode);
		$users_by_time_type = $userAccessObject->getUsersByTimePeriodType();
		$users_by_time_type['tertiary'] = $users_by_time_type['third'];
		$users_by_time_type['secondary'] = $users_by_time_type['second'];
//		ASSIST_HELPER::arrPrint($users_by_time_type);
		$userAdminObject = new SDBP6_SETUP_ADMINS($this->local_modref, $this->autojob, $this->local_cmpcode);
		$users_with_update_access = $userAdminObject->getUsersWithUpdateAccessForReminderEmails();
//		ASSIST_HELPER::arrPrint($users_with_update_access);
		$sdbipObject = new SDBP6_SDBIP($this->local_modref, $this->autojob, $this->local_cmpcode);
		//get time periods
		$sql = "SELECT * 
				FROM ".$this->getTableName()." TP
				INNER JOIN ".$sdbipObject->getTableName()." S 
				ON TP.".$this->getParentFieldName()." = S.".$sdbipObject->getIDFieldName()." 
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND (".$sdbipObject->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
//		ASSIST_HELPER::arrPrint($rows);
		//for each time period, check for closure date < today
		foreach($rows as $time_id => $time) { //echo "<hr />";ASSIST_HELPER::arrPrint($time);
			foreach($time_users as $tu => $tun) {
				$reminder_date = $time['rem_'.$tu];
				$closure_date = $time['close_'.$tu];
				if($reminder_date > $date_of_last_successful_run && $reminder_date < $today && $closure_date > 0 && $closure_date > $today) {
					//check for users for that tu
					if(isset($users_by_time_type[$tu]) && count($users_by_time_type[$tu]) > 0) {
						//generate emails
						$modtitle = $time['sdbip_name'];
						$subject = $module_title.": ".$modtitle." - Closure - ".date("M Y", strtotime($time['end_date']))."";
						$message = "<font face=arial>Kindly note that <span style=\"font-weight: bold;text-decoration: underline;\">".$module_title.": ".$modtitle." - ".date("F Y", strtotime($time['end_date']))."</span> will close for ".$tun." users on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y", $closure_date)." at 23h59</span>.";
						$message .= "<br>Please ensure that you have completed all necessary updates before this time.";
						$message .= "<br>&nbsp;<br>Kind regards<br>Assist";
						$message .= "<br>&nbsp;<br><span class='i'>Please do not reply to this email. Replies to this message will be sent to an unmonitored mailbox.</span></font>";
						$emailObject->setSubject($subject);
						$emailObject->setBody($message);
						$emailObject->setContentType("HTML");
						//send emails
						$count = 0;
						foreach($users_by_time_type[$tu] as $i => $u) {
							if(in_array($i, $users_with_update_access)) {
								$emailObject->setRecipient(array('name' => $u['name'], 'email' => $u['email']));
								$emailObject->setRecipientUserID($i);
								if(in_array($u['email'], $helpdesk_emails)) {
									$message2 = $message."<font face=arial><br>&nbsp;<br>Organisation: ".strtoupper($this->local_cmpcode)."</font>";
									$emailObject->setBody($message2);
									$emailObject->setBCC("helpdesk_cc@actionassist.co.za");
									$emailObject->sendEmail();
								} else {
									$emailObject->setBCC("");
									$emailObject->setBody($message); //do this extra here in case you've changed the body to send to a helpdesk email and are now going back to mun users [JC - 2019-08-08]
									$emailObject->sendEmail();
								}
								$count++;
							}
						}
						//remove reminder date
						$var = array('time_id' => $time_id, 'tu' => $tu);
						$this->updateTimePeriodForReminderSent($var);
						//prep data for return to be displayed in stasks/Results log
						$var['start_date'] = $time['start_date'];
						$var['end_date'] = $time['end_date'];
						$var['tu'] = $this->time_users[$var['tu']];
						$time_name = $this->getTimePeriodNames(array($time), "MONTH");
						$var['time_name'] = $time_name[0];
						$var['count'] = $count;
						$result[] = $var;
					}
				}
			}
		}


		if(count($result) == 0) {
			return false;
		} else {
			return $result;
		}


	}


	public function __destruct() {
		parent::__destruct();
	}

}

?>