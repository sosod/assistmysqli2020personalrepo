<?php
/**
 * To manage the Financial information in the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_FINANCE extends SDBP6 {

	private $finteg_modref = "FINTEG";

	private $import_log_table = "_financials_import";
	private $import_temp_table = "_financials_lines_import";
	private $import_lines_table = "_financials_lines";
	private $import_results_table = "_financials_lines_results";

	const OBJECT_TYPE = "FINANCE";

	public function __construct($modref = "") {
		parent::__construct($modref);

		$this->import_log_table = $this->getDBRef()."".$this->import_log_table;
		$this->import_temp_table = $this->getDBRef()."".$this->import_temp_table;
		$this->import_lines_table = $this->getDBRef()."".$this->import_lines_table;
		$this->import_results_table = $this->getDBRef()."".$this->import_results_table;

	}


	public function getFinancialsForScheduleReport($fin_year_id, $map) {
		$data = array();
		foreach($map as $segment_code => $guids) {
			$sql = "SELECT ".$segment_code."_nt_guid as guid, fin_time_id as time_id, SUM(fin_budget) as budget, SUM(fin_actual) as actual 
					FROM ".$this->getDBRef()."_financials_lines FL 
					INNER JOIN ".$this->getDBRef()."_financials_lines_results ON fin_line_id = line_id
					WHERE ".$segment_code."_nt_guid IN ('".implode("','", $guids)."')
					AND line_fin_year = '".$fin_year_id."'
					GROUP BY ".$segment_code."_nt_guid, fin_time_id";
			$data[$segment_code] = $this->mysql_fetch_all_by_id2($sql, "guid", "time_id");
		}
		return $data;
	}


	public function uploadFilesToCompanyPendingFolder($files, $var) {
		$company_code = strtolower($_SESSION['cc']);
		$finteg_object = new FINTEG_INTERNAL('');
		//physically upload provided file to the FINTEG_FILES folder for the company
		$result = $finteg_object->uploadFilesToCompanyPendingFolder($company_code, $files, true,isset($var['data_type'])?$var['data_type']:"");
		$error_code = base64_encode(__CLASS__."=>".__FUNCTION__." @ ".__LINE__);    //generate error code just in case it's needed - must do here for line number

		//if no errors then carry on
		if($result[0] != "error" && $result[1] !== false) {

			//Add import record to database to manage temporary storage
			$file_name = $result[1]; //file_name is needed to only process that file and ignore any others that might exist from broken processes
			$original_file_name = $result[2]; //file_name is needed to only process that file and ignore any others that might exist from broken processes
			$import_record = $this->addImportRecord($var, $file_name, $original_file_name);
			$error_code = base64_encode(__CLASS__."=>".__FUNCTION__." @ ".__LINE__);    //generate error code just in case it's needed - must do here for line number
			$import_id = $import_record['import_id'];
			$fin_year_id = $import_record['import_fin_year_id'];

			if(isset($import_record['import_id']) && $import_record['import_id'] !== false) {
				//Process the contents into the |#|_financials_lines/_results tables
				$result = $finteg_object->processUploadedFileDataIntoTemporaryStorage($company_code, $file_name, $import_record);
				$error_code = base64_encode(__CLASS__."=>".__FUNCTION__." @ ".__LINE__);    //generate error code just in case it's needed - must do here for line number
			} else {
				$result = array("error", "An error occurred while trying to upload your file [Error code: ".$error_code."].  Please try again.");

			}

			if($result[0] != "error") {
				$result = $this->processUploadedFileDataIntoPermanentStorage($import_record);
				$error_code = base64_encode(__CLASS__."=>".__FUNCTION__." @ ".__LINE__);    //generate error code just in case it's needed - must do here for line number
				if($result[0] != "error") {
					//Now get the uploaded data into SBDP6 Projects
					//$result = $this->updateFinancialUploads2();
					$dbref = $this->getDBRef();
					$result = $this->updateProjects($dbref, $fin_year_id);
					$error_code = base64_encode(__CLASS__."=>".__FUNCTION__." @ ".__LINE__);    //generate error code just in case it's needed - must do here for line number
				} else {
					$result = array("error", "An error occurred while trying to process the financials [Error code: ".$error_code."].  Please try again.");
				}
			} else {
				$result = array("error", "An error occurred while trying to process the financials [Error code: ".$error_code."].  Please try again.");
			}
		} else {
			$result = array("error", "An error occurred while trying to upload your file [Error code: ".$error_code."].  Please try again.");
		}

		if($result[0] == "error" && isset($import_id) && $this->checkIntRef($import_id)) {
			$this->recordImportError($import_id, $error_code);
		} elseif(isset($import_id) && $this->checkIntRef($import_id)) {
			$this->finishImport($import_id);
		}

		return $result;
	}


	public function addImportRecord($var, $file_name, $original_file_name) {
		$insert_data = array(
			'import_original_filename' => $file_name,
			'import_system_filename' => $original_file_name,
			'import_sdbip_id' => $var['sdbip_id'],
			'import_fin_year_id' => $var['fin_year_id'],
			'import_time_id' => $var['time_id'],
			'import_fin_type' => $var['data_type'],
			'import_status' => SDBP6::ACTIVE + SDBP6::UPDATE_IN_PROGRESS,
			'import_date' => date("Y-m-d H:i:s"),
			'import_user' => $this->getUserID(),
			'import_result' => "",
		);
		$sql = "INSERT INTO ".$this->import_log_table." SET ".$this->convertArrayToSQLForSave($insert_data);
		$id = $this->db_insert($sql);
		if($this->checkIntRef($id)) {
			$insert_data['import_id'] = $id;
			$insert_data['sdbip_id'] = $insert_data['import_sdbip_id'];
			$insert_data['time_id'] = $insert_data['import_time_id'];
			$insert_data['fin_year_id'] = $insert_data['import_fin_year_id'];
			$insert_data['fin_type'] = $insert_data['import_fin_type'];
		} else {
			$insert_data['import_id'] = false;
		}
		return $insert_data;
	}

	public function recordImportError($id, $error_code) {
		if($this->checkIntRef($id)) {
			$sql = "UPDATE ".$this->import_log_table." SET import_result = '".$error_code."' WHERE import_id = ".$id;
			$this->db_update($sql);
		}
	}

	public function finishImport($id) {
		if($this->checkIntRef($id)) {
			$sql = "UPDATE ".$this->import_log_table." SET import_result = 'OK', import_status = ".(SDBP6::ACTIVE + SDBP6::UPDATE_COMPLETE)." WHERE import_id = ".$id;
			$this->db_update($sql);
		}
	}


	public function processUploadedFileDataIntoPermanentStorage($import_record) {
		$time_id = $import_record['time_id'];
		$fin_year_id = $import_record['fin_year_id'];
		$import_id = $import_record['import_id'];
		$fin_type = $import_record['fin_type'];
		if(strtoupper($fin_type) == "B" || strtoupper($fin_type) == "BUDGET") {
			$fld = "budget";
			$alt_fld = "actual";
		} else {
			$fld = "actual";
			$alt_fld = "budget";
		}

		/************************************************************************************************************
		 * get data from database
		 */


		//get temp records - group by client guids to deal with any duplicate rows, use client_guids as array key
		$sql = "SELECT CONCAT(pc_client_guid,'|',item_client_guid,'|',fund_client_guid,'|',fx_client_guid,'|',cost_client_guid,'|',reg_client_guid,'|',msc_guid) as master_key, temp.*, SUM(value) as sum_value 
				FROM ".$this->import_temp_table." temp 
				WHERE temp.import_id = ".$import_id." 
				GROUP BY pc_client_guid,item_client_guid,fund_client_guid,fx_client_guid,cost_client_guid,reg_client_guid,msc_guid";
		$new_records = $this->mysql_fetch_all_by_id($sql, "master_key");

		//get current records for time period & fin_year
		//get all lines for current financial_year first with master key => line_id
		$sql = "SELECT CONCAT(pc_client_guid,'|',item_client_guid,'|',fund_client_guid,'|',fx_client_guid,'|',cost_client_guid,'|',reg_client_guid,'|',msc_guid) as master_key, line_id 
				FROM ".$this->import_lines_table." 
				WHERE line_fin_year = ".$fin_year_id."
				ORDER BY line_id ASC";
		$line_rows = $this->mysql_fetch_value_by_id2($sql, "master_key", "line_id", "line_id");
		//now get any results - include line details as well to help with checking for duplicates
		$sql = "SELECT CONCAT(pc_client_guid,'|',item_client_guid,'|',fund_client_guid,'|',fx_client_guid,'|',cost_client_guid,'|',reg_client_guid,'|',msc_guid) as master_key, results.*, flines.* 
				FROM ".$this->import_results_table." results 
				INNER JOIN ".$this->import_lines_table." flines 
				  ON results.fin_line_id = flines.line_id
				WHERE flines.line_fin_year = ".$fin_year_id." AND results.fin_time_id = ".$time_id;
		$results_rows = $this->mysql_fetch_all_by_id2($sql, "master_key", "fin_id");

		/****************************************************************************************************************
		 * process data
		 */
		$fin_inserts = array();
		$kill_lines = array();    //duplicate lines to be removed
		$kill_fin = array();    //financials related to duplicate lines to be removed
		foreach($line_rows as $master_key => $lines) {
			//check for duplicates - if count(lines)>1 then kill extra lines
			if(count($lines) > 1) {
				//kill newer rows = merge values & add duplicate line to kill_rows list
				$c = 0;
				$keep_line_id = 0; //reset in case loop fails to reset it
				$keep_fin_id = 0;
				foreach($lines as $line_id) { //no need to get key - set the same in the SQL call
					if($c == 0) {
						//keep the first record encountered so set the keep_line to this line_id - needed for processing any existing financials
						$keep_line_id = $line_id;
					} else {
						//add id to kill_lines for deletion in the later process
						$kill_lines[] = $line_id;
						//remove line from array for later loop
						unset($line_rows[$master_key][$line_id]);
						//check for financials and process as required
						if(isset($results_rows[$master_key])) {
							//if duplicate rows exist then merge
							if(count($results_rows[$master_key]) > 1) {
								//first get the fin_id of the financial row you are going to keep - there could be a bizzare reason why it is not the first record, so just in case check separately
								foreach($results_rows[$master_key] as $fin_id => $fin_row) {
									if($fin_row['fin_line_id'] == $keep_line_id) {
										$keep_fin_id = $fin_id;
										break;//break from loop
									}
								}//foreach fin_rows first pass
								//second pass to merge records
								foreach($results_rows[$master_key] as $fin_id => $fin_row) {
									if($fin_id == $keep_fin_id) {
										//do nothing
									} else {
										//merge records
										$results_rows[$master_key][$keep_fin_id]['fin_budget'] += $fin_row['fin_budget'];
										$results_rows[$master_key][$keep_fin_id]['fin_actual'] += $fin_row['fin_actual'];
										//add to kill_fin
										$kill_fin[] = $fin_id;
										//remove from array for next pass
										unset($results_rows[$master_key][$fin_id]);
									}
								}//foreach fin_rows second pass
							} else {
								$f = array_keys($results_rows[$master_key]);
								$keep_fin_id = $f[0];
							}
							//remove sub array to make it easier to access in later code
							$results_rows[$master_key] = $results_rows[$master_key][$keep_fin_id];
						}
					}
					//increment counter so loop knows we're past the first record
					$c++;
				}
			} else {
				//set keep_line_id to only record
				$l = array_keys($lines);
				$keep_line_id = $l[0];
			}
			//change master_key array to value
			$line_rows[$master_key] = $keep_line_id;

			//process new records
			if(isset($new_records[$master_key]) && isset($results_rows[$master_key])) {
				//(1) if in both temp & results rows then merge - get all details from results then replace fld with temp - unset both fields
				$new_row = $results_rows[$master_key];
				//Added to catch issues with the $results_rows formatting [JC] 25 Aug 2021
				if(count(array_keys($new_row))==1) {
					$x = array_keys($new_row);
					$new_row = $new_row[$x[0]];
				}
				unset($new_row['fin_id']);
				$new_row['fin_'.$fld] = $new_records[$master_key]['sum_value'];
//				$new_row['test'] = __LINE__;
				$fin_inserts[] = $new_row;
				unset($new_records[$master_key]);
				unset($results_rows[$master_key]);
			} elseif(isset($new_records[$master_key])) {
				//(2) if only in temp then create blank record and add temp data - unset data
				$new_row = $this->createNewFinRecord($keep_line_id, $time_id, $fld, $new_records[$master_key]['sum_value'], $alt_fld);
//				$new_row['test'] = __LINE__;
				$fin_inserts[] = $new_row;
				unset($new_records[$master_key]);
			} elseif(isset($results_rows[$master_key])) {
				//(3) if only in results then replace fld = 0 - unset data
				$new_row = $results_rows[$master_key];
				//Added to catch issues with the $results_rows formatting [JC] 25 Aug 2021
				if(count(array_keys($new_row))==1) {
					$x = array_keys($new_row);
					$new_row = $new_row[$x[0]];
				}
				unset($new_row['fin_id']);    //remove fin_id as new record will be created
				$new_row['fin_'.$fld] = 0;
//				$new_row['test'] = __LINE__;
				$fin_inserts[] = $new_row;
				unset($results_rows[$master_key]);
			} else {
				//(4) do nothing - line not used
			}//end process new records

		}//end foreach masterkey loop
		//check for any data left in temp data - if yes, then create new line
		if(count($new_records) > 0) {
			foreach($new_records as $master_key => $row) {
				//(1) create new line record
				$line_insert_data = $row;
				unset($line_insert_data['line_id']);
				unset($line_insert_data['import_id']);
				unset($line_insert_data['value']);
				unset($line_insert_data['sum_value']);
				unset($line_insert_data['master_key']);
				$line_insert_data['line_fin_year'] = $fin_year_id;

				$sql = "INSERT INTO ".$this->import_lines_table." SET ".$this->convertArrayToSQLForSave($line_insert_data);
				$i = $this->db_insert($sql);

				//(2) get line id => sorry, don't trust returned id field, rather do this the long way
				$sql = "SELECT * FROM ".$this->import_lines_table." WHERE ".$this->convertArrayToSQLForSave($line_insert_data, " AND ")." LIMIT 1";
				$line_id = $this->mysql_fetch_one_value($sql, "line_id");
				//(3) create new fin record for insert as above
				$new_row = $this->createNewFinRecord($line_id, $time_id, $fld, $row['sum_value'], $alt_fld);
//				$new_row['test'] = __LINE__;
				$fin_inserts[] = $new_row;
				unset($new_records[$master_key]);
			}
		}


		/***************************************************************************************
		 * Save data back to database
		 */

		//TODO: DUPLICATE LINES: (1) check for any financials not in this time_id that are using the duplicate lines given & process as above then (2) delete duplicate lines

		//update current records
		//update (1) delete from results table where time_id & line_id matches
		if(count($line_rows) > 0) {
			$sql = "DELETE FROM ".$this->import_results_table." WHERE fin_line_id IN (".implode(",", $line_rows).") AND fin_time_id = ".$time_id;
			$this->db_update($sql);
		}
//		echo "<hr /><h1 class='orange'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		ASSIST_HELPER::arrPrint($fin_inserts);
//		echo "<hr />";
//die();
		//update (2) insert given fin rows
		$sql = "INSERT INTO ".$this->import_results_table." (fin_line_id,fin_time_id,fin_budget,fin_actual) VALUES ";
		$total_rows = count($fin_inserts);
		$rows_processed = 0;
		while($rows_processed < $total_rows) {
//			echo "<hr /><h1 class='green'>Helllloooo $rows_processed : $total_rows from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

			$insert_data = array();
			for($i = 0; $i < 1000; $i++) {
//			echo "<hr /><h1 class='red'>Helllloooo i $i : RP $rows_processed : TR $total_rows from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
				$d = $fin_inserts[$rows_processed]; //ASSIST_HELPER::arrPrint($d);
				$insert_data[] = "('".$d['fin_line_id']."','".$d['fin_time_id']."','".$d['fin_budget']."','".$d['fin_actual']."')";
				$rows_processed++;
				//check if reached end of rows
				if(!($rows_processed < $total_rows)) {
					$i = 1001;//break for loop
				}
			}
			$this->db_insert($sql.implode(",", $insert_data));
		}

	} //end function

	private function createNewFinRecord($line_id, $time_id, $fld, $value, $alt_fld) {
		$new_row = array(
			'fin_line_id' => $line_id,
			'fin_time_id' => $time_id,
			'fin_'.$fld => $value,
			'fin_'.$alt_fld => 0
		);
		return $new_row;
	}


	public function updateFinancialUploads() {
		$company_code = strtolower($_SESSION['cc']);

		$sdbipObject = new SDBP6_SDBIP();
		$does_activated_exist = $sdbipObject->doesActivatedSDBIPExist(true);

		if($does_activated_exist) {
			$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
			if(isset($sdbip['fin_year_id'])) {
				$fin_year_id = (int)$sdbip['fin_year_id'];
				$sdbip_id = $sdbip['id'];
				$projectObject = new SDBP6_PROJECT();
				$fintegObject = new FINTEG_INTERNAL();
				$fin_types = array("EXPENSES");
				foreach($fin_types as $type) {
					$financials = $fintegObject->getLatestLocallyUploadedFinances($company_code, $type, $fin_year_id);

					if(count($financials) > 0) {
						//get projects
						$project_list = $projectObject->getListOfProjectsByGUID($sdbip_id);

						//remove old financials
						$dbref = $this->getDBRef();
						$sql = "UPDATE ".$dbref."_financials SET fin_status = 0";
						$this->db_update($sql);
						//process financials
						foreach($financials as $project_guid => $records) {
							foreach($records as $row) {
								$project_id = isset($project_list[$project_guid]) ? $project_list[$project_guid] : 0;
								$sql = "INSERT INTO ".$dbref."_financials SET
										fin_type = '$type'
										, fin_finyear_id = $fin_year_id
										, fin_project_guid = '$project_guid'
										, fin_project_id = $project_id
										, fin_function_id = ".$row['function']."
										, fin_msc_id = ".$row['msc']."
										, fin_item_id = ".($type == "INCOME" ? $row['funding'] : $row['item'])."
										, fin_region_id = ".$row['region']."
										, fin_status = ".SDBP6::ACTIVE."
										, fin_insertdate = now()";
								$fin_id = $this->db_insert($sql);
								foreach($row['results'] as $time_id => $results) {
									foreach($results as $res_type => $amount) {
										$res_type = strtolower($res_type);
										$sql = "INSERT INTO ".$dbref."_financials_results SET
												fr_fin_id = $fin_id
												, fr_type = '$res_type'
												, fr_time_id = $time_id
												, fr_amount = ".$amount;
										$this->db_insert($sql);
									}
								}
							}
						}
						//update projects
						$timeObject = new SDBP6_SETUP_TIME();
						$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, "MONTH");

						foreach($project_list as $project_guid => $project_id) {
							$sql = "SELECT SUM(fr_amount) as amnt, fr_type as res_type, fr_time_id as time_id
									FROM ".$dbref."_financials_results
									INNER JOIN ".$dbref."_financials
									ON fr_fin_id = fin_id
									WHERE fin_project_id = $project_id
									AND fin_type = 'EXPENSES'
									AND fin_status = 2
									GROUP BY fr_time_id, fr_type";
							$data = $projectObject->mysql_fetch_all_by_id2($sql, "time_id", "res_type");
							$t_count = 0;
							foreach($time_periods as $time_id => $time) {
								$t_count++;
								if(isset($data[$t_count])) {
									$t_data = $data[$t_count];
									$budget = $t_data['budget']['amnt'];
									$adjustments = $t_data['adjustment']['amnt'];
									$actual = $t_data['actual']['amnt'];
									$revised = $budget + $adjustments;
									$variance = $revised - $actual;
									if($revised > 0) {
										$perc_spent = $actual / $revised * 100;
									} else {
										$perc_spent = 0;
									}
									$sql = "UPDATE ".$dbref."_project_finances SET
											pf_original = $budget
											, pf_adjustments = $adjustments
											, pf_actual = $actual
											, pf_revised = $revised
											, pf_variance = $variance
											, pf_perc_spent = $perc_spent
											WHERE pf_proj_id = $project_id
											AND pf_time_id = $time_id
									";
									$this->db_update($sql);
								}
							}
						}
						$result = array('ok', 'Financial records have been successfully updated.');
					} else {
						$result = array('info', 'No financials received.');
					}
				}
			} else {
				$result = array('info', 'No activated SDBIP found.');
			}
		} else {
			$result = array('info', 'No activated SDBIP found.');
		}

		return $result;
	}


	public function updateFinancialUploads2() {
		$company_code = strtolower($_SESSION['cc']);

		$sdbipObject = new SDBP6_SDBIP();
		$does_activated_exist = $sdbipObject->doesActivatedSDBIPExist(true);

		if($does_activated_exist) {
			$sdbip = $sdbipObject->getCurrentActivatedSDBIPDetails();
			if(isset($sdbip['fin_year_id'])) {
				$fin_year_id = (int)$sdbip['fin_year_id'];

				//return array('ok', 'DB REF is 111: ' . $this->getDBRef());

				//$sdbp6_fin_obj = new SDBP6_FINANCE_PROJECTS($company_code, $this->getModRef());

				$result = $this->updateProjects($this->getDBRef(), $fin_year_id);
			} else {
				$result = array('info', 'No activated SDBIP found.');
			}
		} else {
			$result = array('info', 'No activated SDBIP found.');
		}

		return $result;
	}

	public function updateProjects($dbref, $fin_year_id) {
		$fin_ref = $fin_year_id;//which field to use for search
		$dbref = strlen($dbref) == 0 ? $this->getDBRef() : $dbref;
		$result2 = "";
		$local_lines_table = $this->import_lines_table;//$this->getDBRef()."_financials_lines";
		$local_fin_table = $this->import_results_table;//$this->getDBRef()."_financials_lines_results";

//        $company_code = strtolower($_SESSION['cc']);
//        $company_db_object = new ASSIST_MODULE_HELPER('client', $company_code);

		//get list of project_id & fin_ref
		$sql = "SELECT proj_id as id, proj_finref as fin_ref FROM ".$dbref."_project WHERE (proj_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE;
		$projects = $this->mysql_fetch_value_by_id($sql, "fin_ref", "id");

		//get sum of financials per month per project - excluding revenue, assets & liabilities (but including asset acquisitions)
		$sql = "SELECT pc_client_guid as project , fin_time_id as time_id , SUM(fin_budget) as budget , SUM(fin_actual) as actual
                FROM $local_fin_table
                INNER JOIN $local_lines_table
                ON line_id = fin_line_id
                WHERE line_fin_year = '".$fin_ref."'
                AND item_prefix NOT IN ('IR','IA','IL')
                GROUP BY pc_client_guid, fin_time_id";
		$financials = $this->mysql_fetch_all_by_id2($sql, "project", "time_id");
//        ASSIST_HELPER::arrPrint($projects);
//        ASSIST_HELPER::arrPrint($financials);
		//update projects
		$projects_processed = 0;
		$projects_found = count($financials);
		foreach($projects as $fin_ref => $project_id) {
			if(isset($financials[$fin_ref])) {
				$project_financials = $financials[$fin_ref];
				foreach($project_financials as $time_id => $fin) {
					$variance = $fin['budget'] - $fin['actual'];
					$perc_spent = $fin['budget'] > 0 ? ($fin['actual'] / $fin['budget']) * 100 : 100;
					$sql = "UPDATE ".$dbref."_project_finances 
                        SET pf_original = '".$fin['budget']."', pf_revised = '".$fin['budget']."'
                            , pf_actual = '".$fin['actual']."'
                            , pf_variance = ".$variance."
                            , pf_perc_spent = ".$perc_spent."
                        WHERE pf_proj_id = ".$project_id." AND pf_time_id = ".$time_id;
					$this->db_update($sql);
				}
				unset($financials[$fin_ref]);
				$projects_processed++;
			}
		}

		$result2 .= $projects_found." projects found in financials; ".count($projects)." projects found in SDBIP; ".$projects_processed." projects updated;";

//        return array('ok', $result2);

		//record "unspecified" data
		$info_or_ok = 'ok';
		if(count($financials) > 0) {
			$result2 .= " ";
			$result2 .= count($financials)." projects not found in local SDBIP - recorded as Unspecified Project;";
			$sql = "DELETE FROM ".$dbref."_project_finances WHERE pf_proj_id = 0";
			$this->db_update($sql);
			//sum
			$insert_data = array();
			foreach($financials as $fin) {
				foreach($fin as $time_id => $f) {
					if(!isset($insert_data[$time_id]['budget'])) {
						$insert_data[$time_id]['budget'] = 0;
					}
					$insert_data[$time_id]['budget'] += $f['budget'];
					if(!isset($insert_data[$time_id]['actual'])) {
						$insert_data[$time_id]['actual'] = 0;
					}
					$insert_data[$time_id]['actual'] += $f['actual'];
				}
			}

//            return array($info_or_ok, $result2);

			//insert
			foreach($insert_data as $time_id => $f) {
				$b = $f['budget'];
				$a = $f['actual'];
				$v = $b - $a;
				$ps = $b > 0 ? ($a / $b) * 100 : 100;
				$sql = "INSERT INTO ".$dbref."_project_finances 
                    SET pf_proj_id = 0, pf_time_id = $time_id 
                    , pf_original = $b , pf_revised = $b , pf_actual = $a 
                    , pf_variance = $v , pf_perc_spent = $ps 
                    , pf_status = ".SDBP6::ACTIVE." , pf_insertuser = 'AUTO' , pf_insertdate = now()";
				$this->db_update($sql);
			}

			$info_or_ok = 'ok';
		}

		$result = array($info_or_ok, $result2);
		return $result;
	}


	public function __destruct() {
		parent::__destruct();
	}

}

?>