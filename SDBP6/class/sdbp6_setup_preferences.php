<?php

class SDBP6_SETUP_PREFERENCES extends SDBP6 {

	private $questions = array();
	private $answers_by_code = array();
	private $answers_by_id = array();

	private $sections = array(
		'USER' => "User Interface Settings",
		'MODULE' => "Module Settings",
	);

	private $table_name = "_setup_module";
	private $field_prefix = "";

	private $fields = array(
		'Ref' => "id",
		'Section' => "section",
		'Setting' => "display_text",
		'Type' => "type",
		'Options' => "options",
		'Value' => "value",
		'Sort' => "sort",
		'Status' => "status",
		'Additional Information' => "comment",
		'Short Code' => "code",
	);
	private $field_names = array();

	const LOG_TABLE = "setup";

	public function __construct($modref = "", $autojob = false) {
		parent::__construct($modref, $autojob);
		$this->questions = $this->replaceAllNames($this->getQuestionsFromDB());
		$this->prepQuestionsToGiveAnswers();
		$this->field_names = array_flip($this->fields);
	}


	/********************
	 * CONTROLLER Functions
	 */
	public function updateObject($var) {
		$result = array("info", "Sorry, I don't know what you want me to do.");
		$questions = $this->getQuestions();
		$sections = $this->getSections();

		$changes = array();
		foreach($sections as $section => $section_name) {
			foreach($questions[$section] as $q_id => $q) {
				$new = $var['Q_'.$q_id];
				$old = $q['value'];
				if($new != $old) {
					$sql = "UPDATE ".$this->getTableName()." SET value = '$new' WHERE id = ".$q_id;
					$this->db_update($sql);
					if($q['type'] == "BOOL") {
						$n = $new == true ? "Yes" : "No";
						$o = $old == true ? "Yes" : "No";
					} elseif($q['type'] == "LIST") {
						if(isset($q['options']['OBJECT'])) {                //AA-361 - Add option for object based module preference [JC]
							$class_name = "SDBP6_".$q['options']['SDBP6'];
							$objObject = new $class_name();
							unset($q['options']['OBJECT']);
							unset($q['options']['SDBP6']);
							$my_keys = array_keys($q['options']);
							$function_name = $q['options'][$my_keys[0]];
							$q['options'] = $objObject->$function_name();
						}
						$n = isset($q['options'][$new]) ? $q['options'][$new] : $this->getUnspecified();
						$o = isset($q['options'][$old]) ? $q['options'][$old] : $this->getUnspecified();
					} else {
						$n = $new;
						$o = $old;
					}
					$changes[$q['name']] = array(
						'to' => $n,
						'from' => $o,
						'raw' => array(
							'to' => $new,
							'from' => $old,
						),
					);
				}
			}
		}
		if(count($changes) > 0) {
			$changes['user'] = $this->getUserName();

			$log_var = array(
				'section' => "PREF",
				'object_id' => "",
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			$result = array("ok", "Changes saved successfully.");
			$this->questions = $this->getQuestionsFromDB();
			$this->prepQuestionsToGiveAnswers();
		}
		return $result;
		/*
		$mar = 0;
		$r_err = array();
		$r_ok = array();
		$err = false;
		$changes = array();
		foreach($questions as $key => $q) {
			if(isset($var['Q_'.$key])) {
				$new = $var['Q_'.$key];

				$old = $q['value'];
				if($new != $old) {
					//update database
					$sql = "UPDATE ".$this->getTableName()." SET ".$this->field_names['value']." = '".$new."' WHERE ".$this->field_names['id']." = ".$key;
					$mar = $this->db_update($sql);
					if($mar>0) {
						$options = explode("|",$q['options']);
						$changes[$key] = array('to'=>"",'from'=>"");
						foreach($options as $opt) {
							$opts = explode("_",$opt);
							if($new==$opts[0]) {
								$changes[$key]['to'] = $opts[1];
							} elseif($old==$opts[0]) {
								$changes[$key]['from'] = $opts[1];
							}
							if($changes[$key]['to']!="" && $changes[$key]['from']!="") { break; }
						}
						$r_ok[] = $key;
					} else {
						$r_err[] = $key;
						$err = true;
					}
				}
			}
		}
		if(count($changes)>0) {
			$changes['user']=$this->getUserName();

			$log_var = array(
				'section'	=> "PREF",
				'object_id'	=> "",
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			if(!$err) {
				return array("ok","Changes saved successfully.");
			} elseif(count($r_ok)==0) {
				return array("error","An error occurred while trying to save changes.");
			} elseif(count($r_err)>0) {
				$return = array("okerror",count($r_ok)." changes saved successfully, but the following changes did not save: PREF".implode(", PREF",$r_err).".");
				return $return;
			} else {
				return $array("okerror","An unknown error occurred.  Please review the page and confirm that your changes have been saved.");
			}
		} elseif($err) {
			return array("error","An error occurred while trying to save changes.");
		}
		return array("info","No change found to be saved.");
		*/
	}


	private function prepQuestionsToGiveAnswers() {
		$all_questions = $this->questions;
		foreach($all_questions as $section => $questions) {
			foreach($questions as $q_id => $q) {
				$this->answers_by_id[$q_id] = $q['value'];
				$this->answers_by_code[strtoupper($q['code'])] = $q['value'];
			}
		}
	}


	/***************************
	 * GET functions
	 */
	public function getTableName() {
		return $this->getDBRef().$this->table_name;
	}

	public function getTableFieldPrefix() {
		return $this->field_prefix;
	}

	/**
	 * Returns the list of questions to be displayed on the Setup > Preferences page
	 */
	public function getQuestions() {
		return $this->questions;
	}

	public function getAnswerToQuestion($needle) {
		if($this->checkIntRef($needle)) {
			return $this->getAnswerToQuestionByID($needle);
		} else {
			return $this->getAnswerToQuestionByCode($needle);
		}
	}

	public function getAnswerToQuestionByCode($code) {
		if(isset($this->answers_by_code[strtoupper($code)])) {
			return $this->answers_by_code[strtoupper($code)];
		} else {
			return 1;
		}
	}

	public function getAnswerToQuestionByID($id) {
		return $this->answers_by_id[$id];
	}

	public function getSections() {
		return $this->sections;
	}


	/**
	 * Returns the questions from the database
	 */
	public function getQuestionsFromDB() {
		$sql = "SELECT ".implode(", ", $this->fields).", display_text as name, comment as description FROM ".$this->getTableName()." WHERE status & ".SDBP6::ACTIVE." = ".SDBP6::ACTIVE." ORDER BY sort";
		$rows = $this->mysql_fetch_all_by_id2($sql, "section", "id");
		foreach($rows as $section => $questions) {
			foreach($questions as $q_id => $q) {
				if($q['type'] == "LIST") {
					$options = explode("|", $q['options']);
					$rows[$section][$q_id]['options'] = array();
					foreach($options as $opt) {
						$opt2 = explode("_", $opt);
						if(count($opt2) == 1) {
							$rows[$section][$q_id]['options'][$opt2[0]] = $opt2[0];
						} elseif(count($opt2) == 2) {
							$rows[$section][$q_id]['options'][$opt2[0]] = $opt2[1];
						}
					}
				} elseif($q['type'] == "NUMLIST") {
					$options = explode(";", $q['options']);
					$rows[$section][$q_id]['options'] = array();
					$start = $options[0];
					$end = $options[1];
					$inc = $options[2];
					for($i = $start; $i <= $end; $i += $inc) {
						$rows[$section][$q_id]['options'][$i] = $i;
					}
					$rows[$section][$q_id]['type'] = "LIST";
				}
			}
		}

		return $rows;
	}

	public function getQuestionForLogs() {
		$data = array();
		foreach($this->questions as $key => $q) {
			$data[$key] = $q['name'];
		}
		return $data;
	}


	/******************
	 * SET functions
	 */
	private function updateQuestionsWithAnswersFromDB() {
		$questions = $this->getQuestions();
		foreach($questions as $key => $q) {

		}
	}


	public function __destruct() {
		parent::__destruct();
	}
}


?>