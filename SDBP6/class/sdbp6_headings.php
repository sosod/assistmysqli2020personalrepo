<?php
/**
 * To manage the various object headings within the module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 *
 * HEADING FIELD TYPES:
 * TEXT = unlimited textarea
 * DATE = datepicker
 * LIST = select - get contents from associated h_table
 * SMLVC = small varchar, 10-20 char e.g. short codes
 * MEDVC = medium varchar, 50-100 char
 * LRGVC = large varchar, up to 255 - display as character limited textareas
 * MASTER = select - get contents from associated MASTER h_table
 * CURRENCY = numeric with R in front, formatted with 2 decimal places
 * NUM = numeric
 * USER = select - get contents from timekeep table
 * BOOL = true/false, display as select with yes/no
 * PERC = numeric with %
 * ATTACH = attachment function
 * HEADING = no field but with sub-headings
 *
 * --Module specific--
 * TEMPLATE = associated with template table
 * OWNER = associated with admins table which is associated with MASTER directorate table
 * ASSESSMENT = associated with deliverable statuses
 * SUPPLIER = associated with contract_supplier table which is associated with supplier list
 *
 */


class SDBP6_HEADINGS extends SDBP6 {

	private $list_heading_types = array("LIST", "USER", "MASTER", "OWNER", "MULTILIST", "SEGMENT", "MULTISEGMENT", "OBJECT", "MULTIOBJECT");
	private $text_heading_types = array("TEXT", "SMLVC", "MEDVC", "LRGVC");
	private $num_heading_types = array("NUM", "CURRENCY", "PERC");
	private $bool_heading_types = array("BOOL", "BOOL_BUTTON");

	private $heading_names_for_log = array(
		'name' => "Name",
		'new' => "New List Page",
		'manage' => "Manage List Page",
		'admin' => "Admin List Page",
		'dashboard' => "Action Dashboard",
		'internal' => "Other Modules",
		'required' => "Required"
	);

	private $heading_type_names = array(
		'REF' => "System Reference",
		'MEDVC' => "Medium Text",
		'LRGVC' => "Large Text",
		'TEXT' => "Unlimited Text",
		'BOOL' => "Yes/No",
		'ATTACH' => "Attachment",
		'PERC' => "Percentage",
		'DATE' => "Date",
		'LIST' => "List",
		'NUM' => "Number",
		'MULTILIST' => "Multi-select List",
		'SEGMENT' => "mSCOA Segment List",
		'MULTISEGMENT' => "Multi-select mSCOA Segment List",
		'OBJECT' => "List of Module Objects",
		'MULTIOBJECT' => "Multi-select List of Module Objects",
	);

	const TABLE = "setup_headings";
	const LOG_TABLE = "setup";
	const LOG_SECTION = "HEAD";

	/**
	 * STATUS: Can a heading be renamed by the client
	 */
	const CAN_RENAME = 16;
	/**
	 * STATUS: Is field allowed to be imported?
	 */
	const IMPORT_FIELD = 32; //duped to list_display field for update fields
	/**
	 * STATUS: Can a heading be displayed in the list view?
	 * also used in h_list_display for UPDATE fields
	 */
	const CAN_LIST_VIEW = 64;
	/**
	 * STATUS Can client disable this field
	 */
	const CAN_DISABLE = 128;
	/**
	 * STATUS Can client disable this field
	 */
	const IS_HIDDEN = 256;
	/**
	 * STATUS Must this field have target type formatting applied?
	 */
	const APPLY_FORMATTING = 512;
	/**
	 * STATUS Is a normal list field
	 */
	const IS_STANDARD_LIST = 2048;
	/**
	 * STATUS System managed field? i.e. Do not display in Add/Edit form (Used to disable Date Completed field which is updated automatically by system on update where status_id = 3)
	 */
	const SYSTEM_MANAGED = 4096;
	/**
	 * STATUS Codes for displaying results fields
	 */
	const RESULTS_TARGET = 32768;//(2**15);		//Target/Budget fields = allow input on ADD/EDIT form otherwise display only
	const RESULTS_ACTUAL = 65536;//(2**16);		//Actual fields = allow input on EDIT/UPDATE form and do not display on ADD
	const RESULTS_CALC_TARGET = 131072;//(2**17);	//Target Calculation fields (Revised Target) = display on all forms
	const RESULTS_CALC_ACTUAL = 262144;//(2**18);	//Actual Calculation fields (Variance, results) = display on update/edit forms
	const RESULTS_TRIGGER_NEXT_CALC = 524288;//(2**19);	//Changes to Revised Target must trigger the Results/Variance calculation
	const RESULTS_UPDATE_COMMENTS = 1048576;//2**20;//Text fields to be updated (excl target description)
	const RESULTS_PERFORMANCE_COMMENT = 1024;
	const RESULTS_CORRECTIVE_COMMENT = 8192;
	const RESULTS_POE_COMMENT = 16384;


	/**
	 * LIST_DISPLAY
	 * Display in New, Manage or Admin list pages or on action dashboard
	 * Stored in the h_list_display field, separate from h_status field so bitwaise can be duplicated
	 */
	const DISPLAY_DETAILS_VIEW = 16; //display this column on the detailed view page - duped - can_rename
	const DISPLAY_UPDATE_FORM = 32; //display on the update form - duped - import_field
	const DISPLAY_NEW = 128;    //duped - can_disable
	const DISPLAY_MANAGE = 256;    //duped - is_hidden
	const DISPLAY_DASHBOARD = 512;    //duped - apply_formatting
	const DISPLAY_ADMIN = 1024;
	const DISPLAY_ALL_RESULTS = 2048;    //identifies results columnst to be displayed in list pages - duped - is_std_list
	const DISPLAY_REPORT_RESULTS = 4096;    //identifies results columnst to be displayed in report generator - duped - sys_managed
	const COMPACT_VIEW = 8192; //compact/summary view for update
	const INTERNAL = 16384; //display when sending lines to other modules e.g. Individual Performance


	/********************************
	 * CONSTRUCTOR
	 */
	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);
	}


	/****************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$h_section = $var['section'];
		unset($var['section']);
		$old_data = $this->getRenameHeadings("", array_keys($var['ref']), true);
		$result = 0;
		foreach($var['ref'] as $field => $id) {
			$data = array();
			$c = array('response' => $old_data[$field]['mdefault']." updated:");
			if(isset($var['client'])) {
				$client = $var['client'][$field];
				if($client != $old_data[$field]['client']) {
					$data['h_client'] = $client;
					$c['name'] = array('to' => $client, 'from' => $old_data[$field]['client']);
				}
			}
			$list_check_fields = array("list_new", "list_manage", "list_admin", "list_dashboard", "list_internal", "list_compact");
			if(isset($var['list_new'][$field]) || isset($var['list_manage'][$field]) || isset($var['list_admin'][$field]) || isset($var['list_dashboard'][$field]) || isset($var['list_internal'][$field])) {
				$n = array();
				$lcf = "list_new";
				$list_new = isset($var['list_new'][$field]) ? $var['list_new'][$field] : 0;
				$n[$lcf] = $list_new;
				$lcf = "list_manage";
				$list_manage = isset($var['list_manage'][$field]) ? $var['list_manage'][$field] : 0;
				$n[$lcf] = $list_manage;
				$lcf = "list_admin";
				$list_admin = isset($var['list_admin'][$field]) ? $var['list_admin'][$field] : 0;
				$n[$lcf] = $list_admin;
				$lcf = "list_dashboard";
				$list_dashboard = isset($var['list_dashboard'][$field]) ? $var['list_dashboard'][$field] : 0;
				$n[$lcf] = $list_dashboard;
				$lcf = "list_internal";
				$list_internal = isset($var['list_internal'][$field]) ? $var['list_internal'][$field] : 0;
				$n[$lcf] = $list_internal;
				$lcf = "list_compact";
				$list_compact = isset($var[$lcf][$field]) ? $var[$lcf][$field] : 0;
				$n[$lcf] = $list_compact;

				$list_display = ($list_new == 1 ? self::DISPLAY_NEW : 0) + ($list_manage == 1 ? self::DISPLAY_MANAGE : 0) + ($list_admin == 1 ? self::DISPLAY_ADMIN : 0) + ($list_dashboard == 1 ? self::DISPLAY_DASHBOARD : 0) + ($list_internal == 1 ? self::INTERNAL : 0) + ($list_compact == 1 ? self::COMPACT_VIEW : 0);
				if($list_display * 1 != $old_data[$field]['list'] * 1) {
					$data['h_list_display'] = $list_display;
					foreach($list_check_fields as $lcf) {
						if(isset($var[$lcf][$field]) && $n[$lcf] != $old_data[$field][$lcf]) {
							$lcf2 = explode("_", $lcf);
							unset($lcf2[0]);
							$lcf2 = implode("_", $lcf2);
							$c[$lcf2] = array(
								'to' => ($n[$lcf] == 1 ? "Yes" : "No")
							, 'from' => ($old_data[$field][$lcf] == 1 ? "Yes" : "No"));
						}
					}
				}
			}
			//if(isset($var['required'][$field])) {
			$required = (!isset($var['required'][$field]) || $var['required'][$field] == 1) ? true : FALSE;
			$old_required = $old_data[$field]['required'] == 1 ? true : false;
			if(($required == true && $old_required == false) || ($required == false && $old_required == true)) {
				$data['h_client_required'] = ($required === true ? 1 : 0);
				//only log changes for fields that user could have changed - if required is not set then required by system = don't log
				if(isset($var['required'][$field])) {
					$c['required'] = array('to' => ($required == true ? "Yes" : "No"), 'from' => ($old_required == true ? "Yes" : "No"));
				}
			}
			//}

			if(count($data) > 0) {
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($data)." WHERE h_id = ".$id." AND h_field = '$field'";
				$mar = $this->db_update($sql);
				//if($mar>0) {
				//	$result+=$mar;
				$changes = array_merge(array('user' => $this->getUserName()), $c);
				$log_var = array(
					'section' => "HEAD_".$h_section,
					'object_id' => $old_data[$field]['id'],
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
//				}
				return array("ok", "Changes saved successfully.");
			}
		}
		//	if($result>0) {
		//}
		return array("info", "No change found to be saved.");
	}


	public function deactivateObject($var) {
		$section = $var['section'];
		$field = $var['field'];

		$sql = "SELECT * FROM ".$this->getTableName()." WHERE h_field = '$field' and h_section = '$section'";
		$old_row = $this->mysql_fetch_one($sql);

		if(!is_array($old_row) || count($old_row) == 0) {
			return array("error", "Sorry, couldn't identify which field you are trying to hidden.");
		} else {
			$status = $old_row['h_status'];
			if(($status & self::ACTIVE) == self::ACTIVE) {
				if(($status & self::IS_HIDDEN) == self::IS_HIDDEN) {
					$new_status = $status - self::ACTIVE;
				} else {
					$new_status = $status - self::ACTIVE + self::IS_HIDDEN;
				}
				$sql = "UPDATE ".$this->getTableName()." SET h_status = $new_status WHERE h_section = '$section' AND  h_field = '$field'";
				$mar = $this->db_update($sql);
				$changes = array(
					'user' => $this->getUserName(),
					'response' => $old_row['h_default']." hidden.",
					'status' => array(
						'to' => "Hidden",
						'from' => "Active",
						'raw' => array(
							'to' => $new_status,
							'from' => $status,
						),
					)
				);
				$log_var = array(
					'section' => "HEAD",
					'object_id' => $old_row['h_id'],
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);

				return array("ok", "Field successfully hidden.");
			} else {
				return array("error", "The field you are trying to hide appears to already be hidden.");
			}
		}
	}


	public function restoreObject($var) {
		$section = $var['section'];
		$field = $var['field'];

		$sql = "SELECT * FROM ".$this->getTableName()." WHERE h_field = '$field' and h_section = '$section'";
		$old_row = $this->mysql_fetch_one($sql);

		if(!is_array($old_row) || count($old_row) == 0) {
			return array("error", "Sorry, couldn't identify which field you are trying to restore.");
		} else {
			$status = $old_row['h_status'];
			if(($status & self::ACTIVE) != self::ACTIVE) {
				if(($status & self::IS_HIDDEN) != self::IS_HIDDEN) {
					$new_status = $status + self::ACTIVE - self::IS_HIDDEN;
				} else {
					$new_status = $status + self::ACTIVE;
				}
				$sql = "UPDATE ".$this->getTableName()." SET h_status = $new_status WHERE h_section = '$section' AND  h_field = '$field'";
				$mar = $this->db_update($sql);
				$changes = array(
					'user' => $this->getUserName(),
					'response' => $old_row['h_default']." restored.",
					'status' => array(
						'to' => "Active",
						'from' => "Hidden",
						'raw' => array(
							'to' => $new_status,
							'from' => $status,
						),
					)
				);
				$log_var = array(
					'section' => "HEAD",
					'object_id' => $old_row['h_id'],
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);

				return array("ok", "Field successfully restored.");
			} else {
				return array("error", "The field you are trying to restore appears to already be available.");
			}
		}
	}


	public function sortObject($var) {
		$section = $var['section'];
		$page_type = $var['page_type'];
		$raw_order = $var['order'];
		$new_order = $raw_order;

		switch($page_type) {
			case "LIST":
				$field = "h_list_order";
				$page_name = "List Pages";
				break;
			default:
				$field = "h_details_order";
				$page_name = "Detailed View / Form Pages";
				break;
		}

		//fetch any deactivated items to be added to the end
		$sql = "SELECT h_field as id FROM ".$this->getTableName()." WHERE h_section = '".$section."' AND (h_status & ".self::IS_HIDDEN.")=".self::IS_HIDDEN." ORDER BY $field ";
		$inactives = $this->mysql_fetch_all_by_id($sql, "id");
		foreach($inactives as $key => $x) {
			$new_order[] = $key;
		}

		//convert order from array[position]=>id to array[id]=>position
		$order = array_flip($new_order);

		//update order
		foreach($order as $id => $position) {
			$sql = "UPDATE ".$this->getTableName()." SET $field = ".$position." WHERE h_field = '".$id."' AND h_section = '".$section."'";
			$this->db_update($sql);
		}


		$changes = array(
			'user' => $this->getUserName(),
			'response' => "Display order of |$section| changed for ".$page_name,
		);
		$log_var = array(
			'section' => self::LOG_SECTION,
			'object_id' => 0,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::SORT,
		);
		$this->addActivityLog("setup", $log_var);
		return array("ok", "Display order changed successfully.");

	}


	/****************************
	 * CHECK functions
	 */
	public function isListField($type) {
		return in_array($type, $this->list_heading_types);
	}

	public function getListFieldTypes() {
		return $this->list_heading_types;
	}

	public function isTextField($type) {
		return in_array($type, $this->text_heading_types);
	}

	public function isNumField($type) {
		return in_array($type, $this->num_heading_types);
	}

	public function isBooleanField($type) {
		return in_array($type, $this->bool_heading_types);
	}

	/***
	 * GET Functions
	 */
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	/**
	 * Get the custom heading for a specific drop down list associated with a _list_table
	 * @param (String) tbl = Table name to search for in the headings table
	 * @return (String)
	 */
	public function getAListHeading($tbl) {
		$sql = "SELECT IF(LENGTH(h_client)>0,h_client,h_default) as name FROM ".$this->getTableName()." WHERE h_table = '".$tbl."'";
		return $this->replaceAllNames($this->mysql_fetch_one_value($sql, "name"));
		//return "MAKE THE TABLE!!";
	}

	/**
	 * Get the headings for all list fields that can be edited in Setup
	 *
	 * @return (Array) rows[h_table] = name
	 */
	public function getStandardListHeadings($include_object_impacted = false) {
		$sql = "SELECT h_table as id
        		, IF(LENGTH(h_client)>0,h_client,h_default) as name
        		, h_status as status
        		, h_section as objects
        		FROM ".$this->getTableName()."
        		WHERE h_type IN ('LIST','MULTILIST')
        		AND (h_status & ".SDBP6::ACTIVE." = ".SDBP6::ACTIVE.")
        		AND (h_status & ".self::IS_STANDARD_LIST." = ".self::IS_STANDARD_LIST.")
        		ORDER BY IF(LENGTH(h_client)>0,h_client,h_default), h_table";
		//echo $sql;
		if(!$include_object_impacted) {
			$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		} else {
			$rows = $this->mysql_fetch_all($sql);
			$data = array();
			foreach($rows as $row) {
				if(!isset($data[$row['id']])) {
					$data[$row['id']] = $row;
					unset($data[$row['id']]['status']);
					$data[$row['id']]['objects'] = array($row['objects']."S");
				} elseif(!in_array($row['objects'], $data[$row['id']]['objects'])) {
					$data[$row['id']]['objects'][] = $row['objects']."S";
				}
			}
			foreach($data as $fld => $row) {
				$usage = "|".implode("|, |", $row['objects'])."|";
				$pos = strrpos($usage, "|, |");
				if($pos !== false) {
					$usage = substr_replace($usage, "| & |", $pos, 4);
				}
				$data[$fld]['usage'] = $usage;
			}
		}
		//ASSIST_HELPER::arrPrint($data);
		//unset($data['deliverable_type']);
		//unset($data['deliverable_quality_weight']);
		//unset($data['deliverable_quantity_weight']);
		//unset($data['deliverable_other_weight']);
		$data = $this->replaceAllNames($data);
		$sort_data = array();
		foreach($data as $id => $d) {
			$sort_data[$this->stripStringForComparison($d['name'])] = $d;
		}
		ksort($sort_data);
		return $sort_data;
	}

	/**
	 * Get the headings for all list fields
	 *
	 * @return (Array) rows[h_table] = name
	 */
	public function getAllListHeadings() {
		$sql = "SELECT h_table as id, IF(LENGTH(h_client)>0,h_client,h_default) as name
        		FROM ".$this->getTableName()." WHERE h_type = 'LIST'
        		AND (h_status & ".self::IS_STANDARD_LIST." = ".self::IS_STANDARD_LIST.")";
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		unset($data['deliverable_type']);
		return $data;
	}

	/**
	 * Get a single heading name based on field
	 *
	 * @param (String) field = the field name of the heading to be found.
	 */
	public function getAHeadingNameByField($field) {
		$err = false;
		$return_one = true;
		if(is_array($field)) {
			if(count($field) == 0) {
				$err = true;
			} else {
				$return_one = false;
				$where = "h_field IN ('".implode("','", $field)."')";
			}
		} else {
			$where = "h_field = '$field'";
		}
		$sql = "SELECT IF(LENGTH(h_client)>0,h_client,h_default) as name, h_field as field FROM ".$this->getTableName()." WHERE ".$where;
		if($return_one) {
			return $this->mysql_fetch_one_value($sql, "name");
		} else {
			return $this->mysql_fetch_value_by_id($sql, "field", "name");
		}
	}

	/**
	 * Get a single heading based on field
	 *
	 * @param (String) field = the field name of the heading to be found.
	 */
	public function getAHeadingRecordsByField($field, $object_type) {
		$sql = "SELECT *, IF(LENGTH(h_client)>0,h_client,h_default) as name FROM ".$this->getTableName()." WHERE h_field = '".$field."' AND h_section = '".$object_type."'";
		return $this->mysql_fetch_one($sql);
	}

	/**
	 * Get headings for list pages based on filter display settings
	 */
	public function getListPageHeadings($obj_type, $section, $filter_display_type = "list") {
		$fn = "LIST";
		return $this->getMainObjectHeadings($obj_type, $fn, $section, "", false, array(), $filter_display_type);
	}

	/**
	 * Get all the headings used by a particular object for a specific use
	 * @param (String) $object name = contract, deliverable, action
	 * @param (String) $fn = LIST, VIEW (DETAILS), FORM, REPORT
	 * @param (String) $section = NEW / MANAGE / ADMIN / REPORT
	 * @param (String) $fld_prefix
	 * @param (Bool) $replace_names
	 * @param (Array) $specific_fields (to get names for)
	 * @param (String) $filter_display_type = list/normal
	 * @param (Array) $exclusion_fields (ignore)
	 */
	public function getMainObjectHeadings($object, $fn = "DETAILS", $section = "", $fld_prefix = "", $replace_names = false, $specific_fields = array(), $filter_display_type = "list", $exclusion_fields = array()) {
		if($filter_display_type == "list" || strlen($filter_display_type) == 0) {
			$filter_display_type = "normal";
		}
		//echo "<P>".$object.":".$fn.":",$section."<p>";
		$sql = "SELECT h_id as id
				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required
				, h_system_glossary as help
				, h_status as status
				, IF( ((h_status & ".self::APPLY_FORMATTING.") = ".self::APPLY_FORMATTING."),1,0) as apply_formatting
				, IF( ((h_status & ".self::RESULTS_TARGET.") = ".self::RESULTS_TARGET."),1,0) as is_target
				, IF( ((h_status & ".self::RESULTS_CALC_TARGET.") = ".self::RESULTS_CALC_TARGET."),1,0) as is_calc_target
				, IF( ((h_status & ".self::RESULTS_ACTUAL.") = ".self::RESULTS_ACTUAL."),1,0) as is_actual
				, IF( ((h_status & ".self::RESULTS_CALC_ACTUAL.") = ".self::RESULTS_CALC_ACTUAL."),1,0) as is_result
				, IF( ((h_status & ".self::RESULTS_PERFORMANCE_COMMENT.") = ".self::RESULTS_PERFORMANCE_COMMENT."),1,0) as is_performance_comment
				, IF( ((h_status & ".self::RESULTS_CORRECTIVE_COMMENT.") = ".self::RESULTS_CORRECTIVE_COMMENT."),1,0) as is_corrective_measures
				, IF( ((h_status & ".self::RESULTS_POE_COMMENT.") = ".self::RESULTS_POE_COMMENT."),1,0) as is_poe
				, IF( ((h_status & ".self::RESULTS_UPDATE_COMMENTS.") = ".self::RESULTS_UPDATE_COMMENTS."),1,0) as update_comment
				FROM ".$this->getTableName()."
				WHERE (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				";
		if(count($specific_fields) > 0) {
			$order_by = "h_details_order";
			$sql .= " AND h_field IN ('".implode("','", $specific_fields)."') ";
		} else {
			if(count($exclusion_fields) > 0) {
				$sql .= " AND h_field NOT IN ('".implode("','", $exclusion_fields)."') ";
			}
			$sql .= "
							".($fn == "FORM" ? "AND h_section NOT LIKE '".strtoupper($object."_UPDATE")."%' AND ( (h_status & ".self::SYSTEM_MANAGED.") <> ".self::SYSTEM_MANAGED.")" : "")."
							".($section != "RESULTS" && ($fn == "LIST" || $fn == "FULL") ? "AND ( (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW.")" : "")."
							".($fn == "COMPACT" || $fn == "SUMMARY" ? "AND ( (h_list_display & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW.")" : "")."
							".($fn == "IMPORT" ? "AND ( (h_status & ".self::IMPORT_FIELD.") = ".self::IMPORT_FIELD.")" : "")."
							".($fn == "REPORT" ? "AND  (h_type <> 'COMMENT')" : "")."
							".($fn == "GRAPH" ? "AND  (h_type IN ('BOOL','LIST','MULTILIST','SEGMENT','MULTISEGMENT','OBJECT','MULTIOBJECT'))" : "")."
							".($section != "DASHBOARD" ? "AND h_section LIKE '".strtoupper($object)."' " : "AND h_section LIKE '".strtoupper($object)."%' ");
			if($fn == "LIST" || $fn == "FULL" || $fn == "LOGS") {
				if($filter_display_type == "all") {
					$order_by = "h_details_order";
				} else {
					$order_by = "h_list_order";
				}
				if($fn == "LIST") {
					if($section == "RESULTS") {
						if($filter_display_type == "all") {
							//do nothing for all columns
							$sql .= " AND ( (h_list_display & ".self::DISPLAY_ALL_RESULTS.") = ".self::DISPLAY_ALL_RESULTS.") ";
						} elseif($filter_display_type == "compact") {
							//force to "summary view" columns
							$sql .= " AND ( (h_list_display & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW.") ";
						} else { //filter_display_type==normal||limited||(blank)
							$sql .= " AND ( (h_list_display & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW.") ";
						}
					} else {
						if($filter_display_type == "all") {
							//do nothing for all columns
						} elseif($filter_display_type == "compact") {
							//force to "summary view" columns
							$sql .= " AND ( (h_list_display & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW.") ";
						} else { //filter_display_type==normal||limited||(blank)
							switch($section) {
								case "NEW":
									$sql .= " AND ( (h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW.") ";
									break;
								case "MANAGE":
									$sql .= " AND ( (h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE.") ";
									break;
								case "ADMIN":
									$sql .= " AND ( (h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN.") ";
									break;
								case "DASHBOARD":
									$sql .= " AND ( (h_list_display & ".self::DISPLAY_DASHBOARD.") = ".self::DISPLAY_DASHBOARD.") ";
									break;
								default:
									$sql .= " AND ( h_list_display > 0) ";
									break;
							}
						}
					}
				}
			} elseif($fn == "GRAPH") {
				$order_by = "IF(LENGTH(h_client)>0,h_client,h_default), h_details_order";
			} else {
				$order_by = "h_details_order";
				if($fn != "FORM") {
					//$sql.= " AND ( (h_section NOT LIKE '%".strtoupper($object."_UPDATE")."%') OR ( h_section LIKE '%".strtoupper($object."_UPDATE")."%' AND (h_list_display > 0 && (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."  ) ) ) ";
				}
			}
		}
		$sql .= " ORDER BY ".$order_by; //echo "<P>".$sql;
		$rows = $this->mysql_fetch_all_by_id($sql, ($fn == "REPORT" ? "id" : "field"));
		if($replace_names === true) {
			$rows = $this->replaceAllNames($rows);
		}
		if($object == "ASSURANCE") {
			$original = $rows;
			$rows = array();
			foreach($original as $fld => $r) {
				$r['field'] = $fld_prefix.$r['field'];
				$rows[$fld_prefix.$fld] = $r;
			}
		}
		if($fn == "LIST" || $fn == "FULL" || $fn == "LOGS" || $fn == "REPORT" || $fn == "GRAPH") {
			return $rows;
		} else {
			$data = array('rows' => $rows, 'sub' => array());
			foreach($rows as $fld => $r) {
				if($r['parent_id'] > 0) {
					$data['sub'][$r['parent_id']][] = $r;
					unset($data['rows'][$r['field']]);
				}
			}
			return $data;
		}
	}


	public function getUpdateCommentHeadings($object) {
		return $this->getSpecificFieldsForReporting($object, true);
	}

	public function getUpdateNonCommentHeadings($object) {
		return $this->getSpecificFieldsForReporting($object, false);
	}

	public function getSpecificFieldsForReporting($object, $comment_fields_only = true) {
		$all = $this->getMainObjectHeadings($object); //ASSIST_HELPER::arrPrint($all);
		$result = array();
		foreach($all['rows'] as $key => $head) {
			if(stripos($key, "display_status") === false && stripos($key, "display_comment") === false && stripos($key, "original") === false && stripos($key, "adjustments") === false) {
				if($comment_fields_only && ($head['is_performance_comment'] || $head['is_corrective_measures'] || $head['is_poe'] || $head['update_comment'])) {
					$result[$key] = $head;
				} elseif(!($head['is_performance_comment'] || $head['is_corrective_measures'] || $head['is_poe'] || $head['update_comment'])) {
					//assuming we want the remaining fields which should be financial / target, actual etc results
					$result[$key] = $head;
				}
			} else {
				//do nothing -= we don't want that column
			}
		}
		return $result;
	}

	public function getUpdateObjectHeadings($object, $fn = "DETAILS", $section = "", $fields = array()) {
		$sql = "SELECT h_id as id
				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required
				, h_system_glossary as help
				, IF( ((h_status & ".self::APPLY_FORMATTING.") = ".self::APPLY_FORMATTING."),1,0) as apply_formatting
				, IF( ((h_status & ".self::RESULTS_TARGET.") = ".self::RESULTS_TARGET."),1,0) as is_target
				, IF( ((h_status & ".self::RESULTS_CALC_TARGET.") = ".self::RESULTS_CALC_TARGET."),1,0) as is_calc_target
				, IF( ((h_status & ".self::RESULTS_ACTUAL.") = ".self::RESULTS_ACTUAL."),1,0) as is_actual
				, IF( ((h_status & ".self::RESULTS_CALC_ACTUAL.") = ".self::RESULTS_CALC_ACTUAL."),1,0) as is_result
				, IF( ((h_status & ".self::RESULTS_PERFORMANCE_COMMENT.") = ".self::RESULTS_PERFORMANCE_COMMENT."),1,0) as is_performance_comment
				, IF( ((h_status & ".self::RESULTS_CORRECTIVE_COMMENT.") = ".self::RESULTS_CORRECTIVE_COMMENT."),1,0) as is_corrective_measures
				, IF( ((h_status & ".self::RESULTS_POE_COMMENT.") = ".self::RESULTS_POE_COMMENT."),1,0) as is_poe
				, IF( ((h_status & ".self::RESULTS_UPDATE_COMMENTS.") = ".self::RESULTS_UPDATE_COMMENTS."),1,0) as update_comment
				, IF( ((h_status & ".self::RESULTS_UPDATE_COMMENTS.") = ".self::RESULTS_UPDATE_COMMENTS."),1,0) as is_update_comments
				FROM ".$this->getTableName()."
				WHERE (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				 ".(count($fields) == 0 ? "AND (h_list_display & ".self::DISPLAY_UPDATE_FORM.") = ".self::DISPLAY_UPDATE_FORM."" : "AND h_field IN ('".implode(",", $fields)."')")." 
				AND h_section = '".strtoupper($object)."' ";
		if($fn == "LIST") {
			$order_by = "h_list_order";
			switch($section) {
				case "NEW":
					$sql .= " AND ( (h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW.") ";
					break;
				case "MANAGE":
					$sql .= " AND ( (h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE.") ";
					break;
				case "ADMIN":
					$sql .= " AND ( (h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN.") ";
					break;
				default:
					$sql .= " AND ( h_list_display > 0) ";
					break;
			}
		} else {
			$order_by = "h_details_order";
		}
		$sql .= " ORDER BY ".$order_by; //echo $sql;
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		if($fn == "LIST" || $fn == "FORM") {
			return $rows;
		} else {
			$data = array('rows' => $rows, 'sub' => array());
			foreach($rows as $fld => $r) {
				if($r['parent_id'] > 0) {
					$data['sub'][$r['parent_id']][] = $r;
				}
			}
			return $data;
		}

	}


	public function getReviewObjectHeadings($object, $fn = "DETAILS", $section = "") {
		$sql = "SELECT h_id as id
				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required
				, h_system_glossary as help
				, IF( ((h_status & ".self::APPLY_FORMATTING.") = ".self::APPLY_FORMATTING."),1,0) as apply_formatting
				FROM ".$this->getTableName()."
				WHERE (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				AND h_section = '".strtoupper($object)."' ";
		$order_by = "h_details_order";
		$sql .= " ORDER BY ".$order_by; //echo $sql;
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		return $rows;
	}


	public function getHeadingsForInternalMapping($object_type, $include_type_in_name = true, $include_type_separately = false) {
		$sql = "SELECT C.h_id as id
				, C.h_field as field
				, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default) as head_name
				, C.h_section as section
				, C.h_type as type
				, C.h_table as list_table
				, C.h_client_length as max
				, C.h_parent_id as parent_id
				, P.h_field as parent_field
				, IF(LENGTH(P.h_client)>0,P.h_client,P.h_default) as parent_name
				, IF(P.h_field IS NULL
					, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default)
					, CONCAT(IF(LENGTH(P.h_client)>0,P.h_client,P.h_default),': ',IF(LENGTH(C.h_client)>0,C.h_client,C.h_default))
				  ) as name
				, C.h_parent_link as parent_link
				, C.h_default_value as default_value
				, IF(C.h_assist_required>0,1,C.h_client_required) as required
				FROM ".$this->getTableName()." C
				LEFT OUTER JOIN ".$this->getTableName()." P
				  ON P.h_id = C.h_parent_id
				WHERE
				(C.h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				AND (C.h_status & ".self::SYSTEM_MANAGED.") <> ".self::SYSTEM_MANAGED."
				AND C.h_type NOT IN ('REF','CALC')
				AND C.h_section = '".$object_type."'
				ORDER BY C.h_details_order, C.h_field";
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		$fields = array();
		foreach($rows as $fld => $r) {
			if($include_type_in_name === true) {
				if(isset($this->heading_type_names[$r['type']])) {
					$r['head_name'] .= " [".$this->heading_type_names[$r['type']]."]";
				} else {
					$r['head_name'] .= " {".$r['type']."}";
				}
			}

			if($include_type_separately === true) {
				$fields[$fld] = array(
					'name' => $r['head_name'],
					'type' => $r['type'],
					'list' => $r['list_table'],
				);
			} else {
				$fields[$fld] = $r['head_name'];
			}
		}
		$fields = $this->replaceAllNames($fields);
		return $fields;
	}

	public function getHeadingsForExternalMapping($object_type, $include_type_in_name = true, $include_type_separately = false) {
		$sql = "SELECT C.h_id as id
				, C.h_field as field
				, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default) as head_name
				, C.h_section as section
				, C.h_type as type
				, C.h_table as list_table
				, C.h_client_length as max
				, C.h_parent_id as parent_id
				, P.h_field as parent_field
				, IF(LENGTH(P.h_client)>0,P.h_client,P.h_default) as parent_name
				, IF(P.h_field IS NULL
					, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default)
					, CONCAT(IF(LENGTH(P.h_client)>0,P.h_client,P.h_default),': ',IF(LENGTH(C.h_client)>0,C.h_client,C.h_default))
				  ) as name
				, C.h_parent_link as parent_link
				, C.h_default_value as default_value
				, IF(C.h_assist_required>0,1,C.h_client_required) as required
				FROM ".$this->getTableName()." C
				LEFT OUTER JOIN ".$this->getTableName()." P
				  ON P.h_id = C.h_parent_id
				WHERE
				(C.h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				AND (C.h_status & ".self::IMPORT_FIELD.") = ".self::IMPORT_FIELD."
				AND C.h_section = '".$object_type."'
				ORDER BY C.h_details_order, C.h_field";
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		$fields = array();
		foreach($rows as $fld => $r) {
			if($include_type_in_name === true) {
				if(isset($this->heading_type_names[$r['type']])) {
					$r['head_name'] .= " [".$this->heading_type_names[$r['type']]."]";
				} else {
					$r['head_name'] .= " {".$r['type']."}";
				}
			}

			if($include_type_separately === true) {
				$fields[$fld] = array(
					'name' => $r['head_name'],
					'type' => $r['type'],
					'list' => $r['list_table'],
				);
			} else {
				$fields[$fld] = $r['head_name'];
			}
		}
		$fields = $this->replaceAllNames($fields);
		return $fields;
	}

	public function getHeadingsForLog() {
		$sql = "SELECT C.h_id as id
				, C.h_field as field
				, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default) as head_name
				, C.h_section as section
				, C.h_type as type
				, C.h_table as list_table
				, C.h_client_length as max
				, C.h_parent_id as parent_id
				, P.h_field as parent_field
				, IF(LENGTH(P.h_client)>0,P.h_client,P.h_default) as parent_name
				, IF(P.h_field IS NULL
					, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default)
					, CONCAT(IF(LENGTH(P.h_client)>0,P.h_client,P.h_default),': ',IF(LENGTH(C.h_client)>0,C.h_client,C.h_default))
				  ) as name
				, C.h_parent_link as parent_link
				, C.h_default_value as default_value
				, IF(C.h_assist_required>0,1,C.h_client_required) as required
				FROM ".$this->getTableName()." C
				LEFT OUTER JOIN ".$this->getTableName()." P
				  ON P.h_id = C.h_parent_id
				WHERE (C.h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				ORDER BY C.h_field";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}

	public function getReportObjectHeadings($object_type) {
		$headings = $this->replaceAllNames($this->getMainObjectHeadings($object_type, "REPORT"));
		return $headings;
	}

	public function getHeadingsAvailableForGraphing($object_type, $filter_fields_not_allowed = array()) {
		$headings = $this->replaceAllNames($this->getMainObjectHeadings($object_type, "GRAPH", "REPORT", "", false, array(), "all", $filter_fields_not_allowed));
		return $headings;
	}

	public function getHeadingsForInternal() {
		$sql = "SELECT C.h_field as field
				, IF(P.h_field IS NULL
					, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default)
					, CONCAT(IF(LENGTH(P.h_client)>0,P.h_client,P.h_default),': ',IF(LENGTH(C.h_client)>0,C.h_client,C.h_default))
				  ) as name
				FROM ".$this->getTableName()." C
				LEFT OUTER JOIN ".$this->getTableName()." P
				  ON P.h_id = C.h_parent_id
				WHERE (C.h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				AND  (C.h_status & ".self::INTERNAL.") = ".self::INTERNAL."
				ORDER BY C.h_section DESC, C.h_list_display";
		return $this->replaceAllNames($this->mysql_fetch_all_by_id($sql, "field"));
	}


	/**
	 * Get Headings for renaming in Setup
	 */
	public function getRenameHeadings($hs = "", $fields = array(), $for_saving = false) {
		$sql = "SELECT h_id as id
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, IF( ((h_list_display & ".self::DISPLAY_DASHBOARD.") = ".self::DISPLAY_DASHBOARD."),1,0) as list_dashboard
				, IF( ((h_list_display & ".self::INTERNAL.") = ".self::INTERNAL."),1,0) as list_internal
				, IF( ((h_list_display & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW."),1,0) as list_compact
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				, IF( ((h_status & ".self::CAN_DISABLE.") = ".self::CAN_DISABLE."),1,0) as can_disable
				, IF( ((h_status & ".self::IS_HIDDEN.") = ".self::IS_HIDDEN."),1,0) as is_hidden
				, IF( ((h_status & ".self::APPLY_FORMATTING.") = ".self::APPLY_FORMATTING."),1,0) as apply_formatting
				FROM ".$this->getTableName()."
				WHERE (
				    (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				    OR
				    (h_status & ".self::IS_HIDDEN.") = ".self::IS_HIDDEN."
				  ) ".($for_saving != true ? " AND (h_status & ".self::CAN_RENAME.") = ".self::CAN_RENAME."" : "")."
				  ".(strlen($hs) > 0 ? "AND h_section = '".$hs."'" : "")."
				  ".(count($fields) > 0 ? " AND h_field IN ('".implode("','", $fields)."') " : "")."
				ORDER BY h_details_order";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}

	/**
	 * Get Headings for renaming in Setup
	 */
	public function getAllHeadingsForRenaming($hs = "", $fields = array()) {
		$sql = "SELECT h_id as id
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, IF( ((h_list_display & ".self::DISPLAY_DASHBOARD.") = ".self::DISPLAY_DASHBOARD."),1,0) as list_dashboard
				, IF( ((h_list_display & ".self::INTERNAL.") = ".self::INTERNAL."),1,0) as list_internal
				, IF( ((h_list_display & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW."),1,0) as list_compact
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				, IF( ((h_status & ".self::CAN_DISABLE.") = ".self::CAN_DISABLE."),1,0) as can_disable
				, IF( ((h_status & ".self::IS_HIDDEN.") = ".self::IS_HIDDEN."),1,0) as is_hidden
				, IF( ((h_status & ".self::APPLY_FORMATTING.") = ".self::APPLY_FORMATTING."),1,0) as apply_formatting
				FROM ".$this->getTableName()."
				WHERE (
				    (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				    OR
				    (h_status & ".self::IS_HIDDEN.") = ".self::IS_HIDDEN."
				  )
				  AND h_section = '".$hs."'
				  ".(count($fields) > 0 ? " AND h_field IN ('".implode("','", $fields)."') " : "")."
				ORDER BY h_details_order";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}

	/**
	 * Get Headings for ordering list columns in Setup
	 */
	public function getListColumnHeadings($hs = "", $fields = array()) {
		$sql = "SELECT h_id as id
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				FROM ".$this->getTableName()."
				WHERE (h_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
				  AND (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."
				  AND h_list_display > 0
				  ".(strlen($hs) > 0 ? "AND h_section LIKE '".$hs."%'" : "")."
				  ".(count($fields) > 0 ? " AND h_field IN ('".implode("','", $fields)."') " : "")."
				ORDER BY h_list_order"; //echo "<P>".$sql;
		return $this->mysql_fetch_all_by_id($sql, "field");
	}


	/**
	 * Save list column changes
	 */
	public function saveListColumns($var) {
		$sections = $var['section'];
		$mar = 0;
		foreach($sections as $sec) {
			$m = 0;
			$order = $var[strtolower($sec)];
			foreach($order as $i => $id) {
				$sql = "UPDATE ".$this->getTableName()." SET h_list_order = ".($i + 2)." WHERE h_id = '".$id."'";
				$m += $this->db_update($sql);
			}
			if($m > 0) {
				$s = explode("_", $sec);
				$sec = $s[0];
				$changes = array(
					'response' => "|".strtolower($sec)."| list column order updated.",
					'user' => $this->getUserName(),
				);
				$log_var = array(
					'section' => "COL",
					'object_id' => $this->getObjectNameMenuID($sec),
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
			}
			$mar += $m;
		}
		if($mar > 0) {
			return array("ok", "Changes to list columns saved successfully.");
		} else {
			return array("info", "No list column changes found to be saved.");
		}
	}


	/**
	 * Get heading types to be displayed for the client to understand
	 */
	public function getHeadingTypeFormattedForClient($type) {
		if($this->isListField($type)) {
			return "List";
		} elseif(isset($this->heading_type_names[$type])) {
			return $this->heading_type_names[$type];
		} else {
			return ucwords(strtolower($type));
		}
	}

	/**
	 * Get heading names for display in log
	 */
	public function getDefaultHeadingNamesForLog() {
		return $this->heading_names_for_log;
	}


	/**
	 * @param $generic_fields - list of fields, less prefix (TABLE_FLD), from performance module - only return matching fields which are in use in module
	 * @param $external_module - is it being called from an external module
	 * @param $must_include_field - field to be included always
	 * @return array
	 */
	public function getFieldsForExternalModule($generic_fields = array(), $external_module = false, $must_include_field = false) {
		$kpi_modules = $this->module_kpi_sections;
		//get all records where
		//section = kpi section
		//status = active
		//status = !hidden
		//field in generic field list
		$sql = "SELECT h_section as section, h_field as fld, h_type as field_type, h_client as name, h_table, h_client, h_type, h_field as field FROM ".$this->getTableName()." H
		 		WHERE h_section IN ('".implode("','", $kpi_modules)."') 
		 		AND ((h_status & ".self::ACTIVE.") = ".self::ACTIVE.") 
		 		AND ((h_status & ".self::IS_HIDDEN.") <> ".self::IS_HIDDEN.") 
		 		";
		if(count($generic_fields) > 0) {
			$sql_filter = array();
			foreach($generic_fields as $f) {
				$sql_filter[] = "h_field LIKE '%_".$f."' ";
			}
			if($must_include_field !== false && !in_array($must_include_field, $generic_fields)) {
				$sql_filter[] = "h_field LIKE '%_".$must_include_field."' ";
			}
			$sql .= " AND (".implode(" OR ", $sql_filter).")
		 		ORDER BY h_section, h_field
		 		";
		} elseif($external_module !== false) {
			$sql .= " AND h_section = '$external_module' AND ((h_list_display & ".self::INTERNAL.") = ".self::INTERNAL;
			if($must_include_field !== false) {
				$sql .= " OR h_field LIKE '%_".$must_include_field."' ";
			}
			$sql .= ")";
		}

		$rows = $this->mysql_fetch_all($sql);

		//filter for specific fields
		if(count($generic_fields) > 0) {
			//process rows to check for active headings in all sections
			$processed_data = array();
			$data = array();
			foreach($rows as $row) {
				$f = explode("_", $row['fld']);
				unset($f[0]);
				$fld = implode("_", $f);
				if(!isset($processed_data[$fld][$row['section']])) {
					$processed_data[$fld][$row['section']] = $row['name'];
				}
				if(!isset($data[$fld])) {
					$data[$fld] = $row['name'];
				}
			}
			//validate that all fields are active in all kpi sections - only keep those that match the number of kpi sections
			foreach($processed_data as $fld => $pd) {
				if(count($pd) != count($this->module_kpi_sections)) {
					unset($data[$fld]);
				}
			}
		} elseif($external_module !== false) {
			$data = $rows;
		}
		//get object_names
		$data = $this->replaceAllNames($data);
		return $data;
	}

	/**
	 * @param array $generic_fields
	 * @param bool $external_module
	 * @param bool $must_include_field
	 * @return array|false|mixed|string
	 */
	public function getResultsFieldsForExternalModule($generic_fields = array(), $external_module = false, $must_include_field = false) {
		$kpi_modules = $this->module_kpi_sections;
		//get all records where
		//section = kpi section
		//status = active
		//status = !hidden
		//field in generic field list
		$sql = "SELECT h_section as section, h_field as fld, h_type as field_type, h_client as name, h_table, h_client, h_type, h_field as field FROM ".$this->getTableName()." H
		 		WHERE h_section IN ('".implode("','", $kpi_modules)."') 
		 		AND ((h_status & ".self::ACTIVE.") = ".self::ACTIVE.") 
		 		AND ((h_status & ".self::IS_HIDDEN.") <> ".self::IS_HIDDEN.") 
		 		";
		if(count($generic_fields) > 0) {
			$sql_filter = array();
			foreach($generic_fields as $f) {
				$sql_filter[] = "h_field LIKE '%_".$f."' ";
			}
			$sql .= " AND (".implode(" OR ", $sql_filter).")
		 		ORDER BY h_section, h_field
		 		";
		} elseif($external_module !== false) {
			$sql .= " AND h_section = '".$external_module."_RESULTS' ";
		}

		$rows = $this->mysql_fetch_all($sql);

		//filter for specific fields
		if(count($generic_fields) > 0) {
			//process rows to check for active headings in all sections
			$processed_data = array();
			$data = array();
			foreach($rows as $row) {
				$f = explode("_", $row['fld']);
				unset($f[0]);
				$fld = implode("_", $f);
				if(!isset($processed_data[$fld][$row['section']])) {
					$processed_data[$fld][$row['section']] = $row['name'];
				}
				if(!isset($data[$fld])) {
					$data[$fld] = $row['name'];
				}
			}
			//validate that all fields are active in all kpi sections - only keep those that match the number of kpi sections
			foreach($processed_data as $fld => $pd) {
				if(count($pd) != count($this->module_kpi_sections)) {
					unset($data[$fld]);
				}
			}
		} elseif($external_module !== false) {
			$data = $rows;
		}
		//get object_names
		$data = $this->replaceAllNames($data);
		return $data;
	}



	/***
	 * SET / UPDATE Functions
	 */


	/****
	 * PROTECTED functions: functions available for use in class heirarchy
	 */
	/****
	 * PRIVATE functions: functions only for use within the class
	 */

}

?>