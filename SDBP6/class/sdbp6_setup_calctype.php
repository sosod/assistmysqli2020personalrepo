<?php
/**
 * To manage the Calculation Types of the SDBP6 module + calculation of results
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_CALCTYPE extends SDBP6 {

	protected $object_id = 0;
	protected $object_details = array();
	protected $all_object_details = array();

	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = null;//"";
	protected $name_field = "name";
	protected $attachment_field = null;

	protected $has_attachment = false;

	protected $ref_tag = "CT";


	protected $result_type_options = array(
		'ABOVE' => "Greater than or equal to",
		'BELOW' => "Less than or equal to",
	);
	protected $calculation_options = array(
		'SUM' => "Sum / Addition",
		'AVE_T' => "Average over number of targets",            //counts revised targets
		'AVE_E' => "Average over the number of entries",        //counts revised targets && actuals if updated
		'MAX' => "Max / Highest entry",
		'RECENT' => "Most recent entry",                            //actuals only included if updated
		'NA' => "No Calculation",
	);

	protected $calculation_type_headings = array(
		'id' => array(
			'field' => "id",
			'name' => "Ref",
			'type' => "REF",
			'table' => "",
			'finance_setting' => "0",
		),
		'code' => array(
			'field' => "code",
			'name' => "Code",
			'type' => "SMLVC",
			'table' => "",
			'finance_setting' => "",
		),
		'name' => array(
			'field' => "name",
			'name' => "Name",
			'type' => "MEDVC",
			'table' => "",
			'finance_setting' => "",
		),
		'result_type' => array(
			'field' => "result_type",
			'name' => "Result determination:|Actual must be [?] the Target?",
			'type' => "LIST",
			'table' => "result_type",
			'finance_setting' => "BELOW",
		),
		'calculation' => array(
			'field' => "calculation",
			'name' => "Overall / YTD / PTD|Target/Actual|Calculation Formula",
			'type' => "LIST",
			'table' => "calculation",
			'finance_setting' => "SUM",
		),
		'include_zero_targets' => array(
			'field' => "include_zero_targets",
			'name' => "Consider|'0' Targets|in Calculations",
			'type' => "BOOL_BUTTON",
			'table' => "",
			'finance_setting' => 0,
		),
		'include_zero_actuals' => array(
			'field' => "include_zero_actuals",
			'name' => "Consider|'0' Actual|in Calculations",
			'type' => "BOOL_BUTTON",
			'table' => "",
			'finance_setting' => 0,
		),
	);

	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "CALCULATIONTYPE";
	const OBJECT_NAME = "CALCULATIONTYPE";
	const PLURAL_OBJECT_NAME = "CALCULATIONTYPES";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "setup_calculation_types";
	const TABLE_FLD = "ct";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "setup";
	const LOG_SECTION = "calctype";


	public function __construct($modref = "", $autojob = false) {
		parent::__construct($modref, $autojob);
		$this->has_internal_ref_field = false;

	}


	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");

		$var['status'] = self::ACTIVE;
		$var['insertuser'] = $this->getUserID();
		$var['insertdate'] = date("Y-m-d H:i:s");

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);

		if(ASSIST_HELPER::checkIntRef($id)) {
			$result = array("ok", "New ".$this->getObjectName(self::OBJECT_TYPE)." ".$this->getRefTag().$id." added successfully.");

			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|created| a new |".self::OBJECT_TYPE."| ".$this->getRefTag().$id.": ".$var[$this->getNameFieldName()],
			);
			$log_var = array(
				'section' => "calctype",
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error", "An error occurred while trying to add the new ".$this->getObjectName(self::OBJECT_TYPE).".  Please try again.");
		}

		return $result;
	}

	public function editObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");

		$object_id = $var['object_id'];
		unset($var['object_id']);
		//get current details
		$old_row = $this->getRawObjectDetails($object_id);
		//scan submitted data for changes
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|calculationtype| ".$this->getRefTag().$object_id." edited.",
		);
		$list_items = $this->getListsForSetup();
		$headings = $this->getCalculationTypeHeadings(false);
		foreach($var as $fld => $new) {
			$old = $old_row[$fld];
			if($new != $old) {
				$head = $headings[$fld];
				switch($head['type']) {
					case "BOOL_BUTTON":
						$n = $new == 1 ? "Yes" : "No";
						$o = $old == 1 ? "Yes" : "No";
						break;
					case "LIST":
						$t = $head['table'];
						$n = isset($list_items[$t][$new]) ? $list_items[$t][$new] : $this->getUnspecified();
						$o = isset($list_items[$t][$old]) ? $list_items[$t][$old] : $this->getUnspecified();
						break;
					default:
						$n = $new;
						$o = $old;
						break;
				}
				$changes[$head['name']] = array(
					'to' => $n,
					'from' => $o,
					'raw' => array(
						'to' => $new,
						'from' => $old,
					),
				);
			}
		}
		//if changes found then save to database and log
		if(count($changes) > 0) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var)." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$mar = $this->db_update($sql);
			$log_var = array(
				'section' => "calctype",
				'object_id' => $object_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			//set ok result
			$result = array("ok", "Changes saved successfully.");
		} else {
			//otherwise return error message that no changes were found
		}


		return $result;
	}

	public function deactivateObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");
		$object_id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|calculationtype| ".$this->getRefTag().$object_id." |deactivated|.",
		);
		$log_var = array(
			'section' => "calctype",
			'object_id' => $object_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DEACTIVATE,
		);
		$this->addActivityLog("setup", $log_var);
		//set ok result
		$result = array("ok", "Changes saved successfully.");

		return $result;
	}

	public function restoreObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");
		$object_id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|calculationtype| ".$this->getRefTag().$object_id." |restored|.",
		);
		$log_var = array(
			'section' => "calctype",
			'object_id' => $object_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::RESTORE,
		);
		$this->addActivityLog("setup", $log_var);
		//set ok result
		$result = array("ok", "Changes saved successfully.");


		return $result;
	}


	/*****************************************************************************************************************************
	 * GET functions
	 */

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName($plural = false) {
		return self::OBJECT_NAME;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getCalcTypeForFinances() {
		$row = array();
		foreach($this->calculation_type_headings as $fld => $ct) {
			$row[$fld] = $ct['finance_setting'];
		}
		return $row;
	}

	/**
	 * Get list of Calculation Types for editing in Setup > Defaults > Calculation Types
	 * Also called by KPI > getSummaryResults to get all calc type options for calculating overall results
	 */
	public function getObjectsForSetup() {
		$sql = "SELECT * FROM ".$this->getTableName()." ORDER BY name, code, id";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		return $data;
	}


	/**
	 * Get the headings needed for Setup > Defaults > Calculation Types
	 */
	public function getCalculationTypeHeadings($for_setup = true) {
		if($for_setup) {
			//can't replace | here with <br /> as character numbers are different so unserialize / json_decode doesn't work
			$a = $this->calculation_type_headings;
		} else {
			$x = serialize($this->calculation_type_headings);
			$x = str_replace("|", " ", $x);
			$a = unserialize($x);
		}
		return $a;
	}

	/**
	 * Get any list items needed for drop downs to add / edit in Setup > Defaults > Calculation Types
	 */
	public function getListsForSetup() {
		$lists = array(
			'result_type' => $this->getResultTypeOptions(),
			'calculation' => $this->getCalculationOptions(),
		);
		return $lists;
	}

	public function getResultTypeOptions() {
		return $this->result_type_options;
	}

	public function getCalculationOptions() {
		return $this->calculation_options;
	}

	/**
	 * Get an explanation of the various options to help user in Setup > Defaults > Calculation Types
	 */
	public function getExplanatoryGlossary() {
	}


	public function getAObjectName($object_id) {
		/*		$sql = "SELECT
							org.org_name as name,
							parent.org_name as parent_name,
							org.org_parent_id as parent_id
						FROM ".$this->getTableName()." org
						LEFT OUTER JOIN ".$this->getTableName()." parent
						ON parent.org_id = org.org_parent_id
						WHERE org.org_id = ".$object_id."";
				$row = $this->mysql_fetch_one($sql);
				if($row['parent_id']==0) {
					return $row['name'];
				} else {
					return $row['parent_name']." - ".$row['name'];
				}*/
	}


	public function getRawObjectDetails($object_id) {
		if(is_array($object_id) && isset($object_id['object_id'])) {
			$object_id = $object_id['object_id'];
		}

		if($object_id == 0) {
			return $this->getCalcTypeForFinances();
		} elseif($this->object_id == $object_id && count($this->object_details) > 0) {
			return $this->object_details;
		} elseif(isset($this->all_object_details[$object_id]) && count($this->all_object_details[$object_id]) > 0) {
			return $this->all_object_details[$object_id];
		} else {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
			return $this->mysql_fetch_one($sql);
		}
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getActiveListItemsFormattedForSelect($options = array()) {
		return $this->getActiveObjectsFormattedForSelect($options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($sdbip_id = 0, $options = array()) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , ".$this->getNameFieldName()." as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." ";
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $items;
	}

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getAllListItemsFormattedForSelect($options = array()) {
		return $this->getAllObjectsFormattedForSelect($options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getAllObjectsFormattedForSelect($options = array(), $sdbip_id = 0) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , ".$this->getNameFieldName()." as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED." ";
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $items;
	}




























	/*******************************************************************************************************************
	 * PERFORM CALCULATIONS
	 */


	/**
	 * Calculation of ANNUAL OVERALL original_total, revised_total, actual_total
	 */
	public function calculateSummaryResult($raw_data, $calc_type_id, $time_periods, $calc_type_settings = array()) {
//ASSIST_HELPER::arrPrint($time_periods);
		if(count($calc_type_settings) == 0) {
			$calc_type_settings = $this->getRawObjectDetails($calc_type_id);
		}
		$data = array(
			'original_total' => 0,
			'revised_total' => 0,
			'actual_total' => 0,
		);
		$original_total = 0;
		$revised_total = 0;
		$actual_total = 0;
//echo $calc_type_settings['calculation'];
		switch($calc_type_settings['calculation']) {
			case "SUM":
				foreach($time_periods as $t_id => $time) {
					if(isset($raw_data[$t_id])) {
						$original_total += $raw_data[$t_id]['original_target'];
						$revised_total += $raw_data[$t_id]['revised_target'];
						$actual_total += $raw_data[$t_id]['actual'];
					}
				}
				$data['original_total'] = $original_total;
				$data['revised_total'] = $revised_total;
				$data['actual_total'] = $actual_total;
				break;
			case "AVE_T":
				$count = 0;
				foreach($time_periods as $t_id => $time) {
					if(isset($raw_data[$t_id])) {
						$original_total += $raw_data[$t_id]['original_target'];
						$revised_total += $raw_data[$t_id]['revised_target'];
						$actual_total += $raw_data[$t_id]['actual'];
					}
					if($calc_type_settings['include_zero_targets'] == true) {
						$count++;
					} elseif(isset($raw_data[$t_id]['revised_target']) && $raw_data[$t_id]['revised_target'] > 0) {
						$count++;
					}
				}
				if($count > 0) {
					$data['original_total'] = $original_total / $count;
					$data['revised_total'] = $revised_total / $count;
					$data['actual_total'] = $actual_total / $count;
				} else {
					$data['original_total'] = $original_total;
					$data['revised_total'] = $revised_total;
					$data['actual_total'] = $actual_total;
				}
				break;
			case "AVE_E":
				$count_t = 0;
				$count_a = 0;
				foreach($time_periods as $t_id => $time) {
					if(isset($raw_data[$t_id])) {
						$original_total += $raw_data[$t_id]['original_target'];
						$revised_total += $raw_data[$t_id]['revised_target'];
						$actual_total += $raw_data[$t_id]['actual'];
					}
					if($calc_type_settings['include_zero_targets'] == true) {
						$count_t++;
					} elseif($raw_data[$t_id]['revised_target'] > 0) {
						$count_t++;
					}
					if($raw_data[$t_id]['has_been_updated'] == true) {
						if($calc_type_settings['include_zero_actuals'] == true) {
							$count_a++;
						} elseif($raw_data[$t_id]['actual'] > 0) {
							$count_a++;
						}
					}
				}
				$data['original_total'] = $original_total / $count_t;
				$data['revised_total'] = $revised_total / $count_t;
				$data['actual_total'] = $actual_total / $count_a;
				break;
			case "MAX":
				foreach($time_periods as $t_id => $time) {
					if(isset($raw_data[$t_id])) {
						$data['original_total'] = ($raw_data[$t_id]['original_target'] > $data['original_total']) ? $raw_data[$t_id]['original_target'] : $data['original_total'];
						$data['revised_total'] = ($raw_data[$t_id]['revised_target'] > $data['revised_total']) ? $raw_data[$t_id]['revised_target'] : $data['revised_total'];
						$data['actual_total'] = ($raw_data[$t_id]['actual'] > $data['actual_total']) ? $raw_data[$t_id]['actual'] : $data['actual_total'];
					}
				}
				break;
			case "RECENT":
				foreach($time_periods as $t_id => $time) {
					if(isset($raw_data[$t_id])) {
						if($calc_type_settings['include_zero_targets'] == true) {
							$data['original_total'] = $raw_data[$t_id]['original_target'];
							$data['revised_total'] = $raw_data[$t_id]['revised_target'];
						} else {
							if($raw_data[$t_id]['original_target'] > 0) {
								$data['original_total'] = $raw_data[$t_id]['original_target'];
							}
							if($raw_data[$t_id]['revised_target'] > 0) {
								$data['revised_total'] = $raw_data[$t_id]['revised_target'];
							}
						}
						if($raw_data[$t_id]['has_been_updated'] == true) {
							if($calc_type_settings['include_zero_actuals'] == true) {
								$data['actual_total'] = $raw_data[$t_id]['actual'];
							} else {
								if($raw_data[$t_id]['original_target'] > 0) {
									$data['actual_total'] = $raw_data[$t_id]['actual'];
								}
							}
						}
					}
				}
				break;
			case "NA":
			default:
				//do nothing
				break;
		}
//ASSIST_HELPER::arrPrint($data);
		return $data;
	}


	public function getCalcTypeOptions($calc_type_id) {
		//get settings for specific calculation type
		//if(isset($_SESSION[$this->getModRef()]['CALCULATION_TYPES'][$calc_type_id]['timestamp']) && $_SESSION[$this->getModRef()]['CALCULATION_TYPES'][$calc_type_id]<time()) {
		//	$calc_type_settings = $_SESSION[$this->getModRef()]['CALCULATION_TYPES'][$calc_type_id];
		//} else {
		if($calc_type_id > 0) {
			$calc_type_settings = $this->getRawObjectDetails($calc_type_id);
		} else {
			$calc_type_settings = $this->getCalcTypeForFinances();
		}
		$_SESSION[$this->getModRef()]['CALCULATION_TYPES'][$calc_type_id] = $calc_type_settings;
		$_SESSION[$this->getModRef()]['CALCULATION_TYPES'][$calc_type_id]['timestamp'] = time() + 30 * 60;    //remain valid for 30 minutes
		//}
		return $calc_type_settings;
	}

	public function getAllCalcTypeOptions() {
		$list = $this->getActiveListItemsFormattedForSelect();
		foreach($list as $i => $l) {
			$list[$i] = $this->getCalcTypeOptions($i);
		}
		return $list;
	}


	/**
	 * Function to determine result for given target & actual
	 */
	public function calculatePeriodResult($calc_type_id, $target, $actual, $result_options) {
		$calc_type_settings = $this->getCalcTypeOptions($calc_type_id);
		$result = array();
		$r = 0;
		//first check that calculation type is not set to Not Applicable - only then do you act on it
		if($calc_type_settings['calculation'] != "NA") {
			$result_type = $calc_type_settings['result_type'];
			//if target&actual == 0 and ignore zero targets is set then set to 0
			if($calc_type_settings['include_zero_targets'] == false && $target == 0 && $actual == 0) {
				$r = 0;
				//if actual must be >= target, then calculate %
			} elseif($result_type == "ABOVE") {
				if($actual == $target) {
					$r = 3;    //if actual=target then met
				} else {
					if($target != 0) {
						$perc = $actual / $target;
					} else {
						$perc = $result_options[5]['limit_decimal'] + 1;
					}
					if($actual < $target) {
						if($perc < $result_options[2]['limit_decimal']) {
							$r = 1; //if actual < 75% of target then NOT MET
						} else {
							$r = 2; //else ALMOST MET (75%-99/99%)
						}
					} else {
						if($perc < $result_options[5]['limit_decimal']) {
							$r = 4; //if actual < 150% of target then WELL MET
						} else {
							$r = 5; //else EXTREMEMLY WELL MET (actual >=150% of target)
						}
					}
				}
			} else {
				//else assume actual must be <= target
				if($actual > $target) {
					$r = 1;    //if actual > target then NOT MET
				} elseif($actual == $target) {
					$r = 3; //if actual = target then MET
				} else {
					$r = 5;    //if actual < target then WELL MET
				}
			}
		}
		return $r;

	}


	public function checkTargetMetForUpdateRequiredFieldsValidation($calc_type_id, $target, $actual) {
		$result_options = $this->getResultOptions();
		$result = $this->calculatePeriodResult($calc_type_id, $target, $actual, $result_options);
		if($result < 3 && $result > 0) {
			$answer = false;
		} else {
			$answer = true;
		}
		return $answer;
	}

	/**
	 * Called by common/update form to check if comment field must be enforced due to target not met
	 * @param $var Array(calc_type_id, target, actual)
	 * @return array(answer=>BOOL)
	 */
	public function checkTargetMetForUpdateRequiredFieldsValidationReturnForAjax($var) {
		$calc_type_id = $var['calc_type_id'];
		$target = $var['target'];
		$actual = $var['actual'];
		$answer = $this->checkTargetMetForUpdateRequiredFieldsValidation($calc_type_id, $target, $actual);
		return array('answer' => $answer);
	}


	/**
	 * Function to determine target & actual for YTD / PTD results
	 */
	public function calculatePTDValues($calc_type_id, $targets, $actuals, $return_for_pm = false) {
		$calc_type_settings = $this->getCalcTypeOptions($calc_type_id);

		$target = 0;
		$actual = 0;

		switch($calc_type_settings['calculation']) {
			case "SUM":
				$target = array_sum($targets);
				$actual = array_sum($actuals);
				break;
			case "AVE_T":
				$target_count = 0;
				foreach($targets as $t) {
					if($t > 0 || $this->processZeroTargets($calc_type_settings) == true) {
						$target_count++;
					}
				}
				if($target_count == 0) {
					$target_count = 1;
				}
				$target = array_sum($targets) / $target_count;
				$actual = array_sum($actuals) / $target_count;
				break;
			case "AVE_E":
				$target_count = 0;
				foreach($targets as $t) {
					if($t > 0 || $this->processZeroTargets($calc_type_settings) == true) {
						$target_count++;
					}
				}
				$actual_count = 0;
				foreach($actuals as $a) {
					if($a > 0 || $this->processZeroActuals($calc_type_settings) == true) {
						$actual_count++;
					}
				}
				if($target_count == 0) {
					$target_count = 1;
				}
				if($actual_count == 0) {
					$actual_count = 1;
				}
				$target = array_sum($targets) / $target_count;
				$actual = array_sum($actuals) / $actual_count;
				break;
			case "MAX":
				foreach($targets as $t) {
					if($t > $target) {
						$target = $t;
					}
				}
				foreach($actuals as $a) {
					if($a > $actual) {
						$actual = $a;
					}
				}
				break;
			case "RECENT":
				foreach($targets as $t) {
					if($t > 0 || $this->processZeroTargets($calc_type_settings) == true) {
						$target = $t;
					}
				}
				foreach($actuals as $a) {
					if($a > 0 || $this->processZeroActuals($calc_type_settings) == true) {
						$actual = $a;
					}
				}
				break;
			case "NA":
				//do nothing
				$target = "N/A";
				$actual = "N/A";
				break;
		}
		$return = array('target' => $target, 'actual' => $actual);
		if($return_for_pm) {

		}
		return $return;
	}


	public function processZeroTargets($calc_type_settings) {
		if($calc_type_settings['include_zero_targets'] == 1) {
			return true;
		}
		return false;
	}


	public function processZeroActuals($calc_type_settings) {
		if($calc_type_settings['include_zero_actuals'] == 1) {
			return true;
		}
		return false;
	}


	public function getZeroResultCalculationTypesForDashboard() {
		$data = array();

		$raw = $this->getObjectsForSetup();
		foreach($raw as $row) {
			$id = $row[$this->getIDFieldName()];
			if($this->processZeroActuals($row) || $this->processZeroTargets($row)) {
				$data[$id] = $row['name'];
			}
		}


		return $data;
	}


	/*****************************************************************************************************************************
	 * SET functions
	 */


	public function __destruct() {
		parent::__destruct();
	}

}

?>