<?php
/**
 * To manage the centralised KPI functions relating to both Top Layer and Departmental KPI objects of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_KPI extends SDBP6 {

	protected $external_call = false;
	private $sdbp6_kpi_modref = "";

	public function __construct($modref = "", $autojob = false) {
		parent::__construct($modref, $autojob);
		$this->sdbp6_kpi_modref = $modref;
	}


	/**
	 * Get the annual targets & actuals
	 * @param int $object_id
	 * @return array
	 */
	public function getSummaryResults($object_id = 0, $sdbip_id = false) {
		/* PROCESS
		 *  get all results
		 *  get calc types for parents
		 *  foreach parent
		 *     send results to calc type object for processing
		 *  return data
		 */

		$final_data = array();
		$raw_data = array();
		//convert single object_id to array to standardise return format
		if(!is_array($object_id)) {
			$object_id = array($object_id);
		}
		//get all results for given object_id(s)
		$raw_data = $this->getRawResults($object_id, true, true, true, $sdbip_id);
		$time_periods = $raw_data['time'];
//ASSIST_HELPER::arrPrint($time_periods);
		$raw_data = $raw_data['data'];
		$calctypeObject = new SDBP6_SETUP_CALCTYPE($this->sdbp6_kpi_modref);
		$all_calctypes = $calctypeObject->getObjectsForSetup();
//ASSIST_HELPER::arrPrint($all_calctypes);
		foreach($object_id as $obj_id) {
//echo "<hr /><h3>".$obj_id."</h3>";
			if(!isset($raw_data[$obj_id])) {
				$final_data[$obj_id] = array(
					'original_total' => "N/A",
					'revised_total' => "N/A",
					'actual_total' => "N/A",
				);
			} else {
				$raw = $raw_data[$obj_id];
//ASSIST_HELPER::arrPrint($raw);
				$raw_keys = array_keys($raw);
				$last_result_key = end($raw_keys);
				$calc_type = $raw[$last_result_key]['calc_type'];
//echo "<H3>".$obj_id." = ".$calc_type."</h3>";
				$summary = $calctypeObject->calculateSummaryResult($raw, $calc_type, $time_periods, (isset($all_calctypes[$calc_type]) ? $all_calctypes[$calc_type] : array()));
//ASSIST_HELPER::arrPrint($summary);
				$final_data[$obj_id] = $summary;
			}
		}
		/*		$sql = "SELECT ".$this->getResultsParentFieldName()." as parent
						, ".$this->getTableField()."_calctype_id as calc_type
						, ".$this->getResultsOriginalFieldName()." as original_target
						, ".$this->getResultsRevisedFieldName().") as revised_target
						, ".$this->getResultsActualFieldName().") as actual
						FROM ".$this->getResultsTableName()." TR WHERE ".$this->getResultsActiveStatusSQL("TR",$this->getResultsTableField());
				if(is_array($object_id)) {
					//if the project_id variable is an array: (1) Remove any blanks then (2) implode to a string
					$object_id = $this->removeBlanksFromArray($object_id);
					if(count($object_id)>0) {
						$sql.=" AND ".$this->getResultsParentFieldName()." IN (".implode(",",$object_id).")";
					} else {
						//if the array of project ids is blank then default to checking for project_id = 0 so as not to break the sql statement
						$sql.=" AND ".$this->getResultsParentFieldName()." = 0";
						$object_id = 0;
					}
				} else {
					//if the project_id variable is not an array, assume it is a single id to be searched for
					//if the project_id variable is not a valid number (id) then default to 0 so as not to break the sql statement
					if(!$this->checkIntRef($object_id)) { $object_id = 0; }
					$sql.=" AND ".$this->getResultsParentFieldName()." = ".$object_id;
				}
				if(is_array($object_id)) {
					$raw_data = $this->mysql_fetch_all_by_id($sql, "parent");
				} else {
					$raw_data = $this->mysql_fetch_one($sql);
				}*/
//ASSIST_HELPER::arrPrint($final_data);
		return $final_data;
	}


	/**
	 * Get the complete results records per KPI
	 */
	public function getRawResults($object_id = 0, $include_calctype = false, $rename_fields = false, $return_time_periods = false, $sdbip_id = false, $page_section = "MANAGE") {
		//select fields
		$sql = "SELECT ".$this->getResultsParentFieldName()." as parent ";
		$sql .= ", TR.* ";
		//if generic names have been asked for
		if($rename_fields == true) {
			$sql .= "
				, ".$this->getResultsOriginalFieldName()." as original_target
				, ".$this->getResultsRevisedFieldName()." as revised_target
				, ".$this->getResultsActualFieldName()." as actual
				, ".$this->getResultsTableField()."_update as has_been_updated
				";
		}
		//validate against time periods
		$timeObject = new SDBP6_SETUP_TIME($this->sdbp6_kpi_modref);
		$extra_joins = "
		INNER JOIN ".$timeObject->getTableName()." T ON ".$this->getResultsTimeFieldName()." = ".$timeObject->getIDFieldName()." AND (".$timeObject->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
		";
		//if parent calculation type id is required
		if($include_calctype == true) {
			$sql .= "
			, ".$this->getTableField()."_calctype_id as calc_type
			";
			$extra_joins .= "
			INNER JOIN ".$this->getTableName()." KPI ON ".$this->getIDFieldName()." = ".$this->getResultsParentFieldName()."
			";
		}
		//set from & where
		$sql .= "
		FROM ".$this->getResultsTableName()." TR ".$extra_joins."
		 WHERE ".$this->getResultsActiveStatusSQL("TR", $this->getResultsTableField());
		//check object_id fed into function
		if(is_array($object_id)) {
			//if the project_id variable is an array: (1) Remove any blanks then (2) implode to a string
			$object_id = $this->removeBlanksFromArray($object_id);
			if(count($object_id) > 0) {
				$sql .= " AND ".$this->getResultsParentFieldName()." IN (".implode(",", $object_id).")";
			} else {
				//if the array of project ids is blank then default to checking for project_id = 0 so as not to break the sql statement
				$sql .= " AND ".$this->getResultsParentFieldName()." = 0";
			}
			//if multiple objects then fetch 3D array [object_id][time_id] = row
			$data = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
		} else {
			//if the project_id variable is not an array, assume it is a single id to be searched for
			//if the project_id variable is not a valid number (id) then default to 0 so as not to break the sql statement
			if(!$this->checkIntRef($object_id)) {
				$object_id = 0;
			}
			$sql .= " AND ".$this->getResultsParentFieldName()." = ".$object_id;
			//if single object then fetch 2D array [time_id] = row
			$data = $this->mysql_fetch_all_by_id($sql, $this->getResultsTimeFieldName());
		}

		if($return_time_periods == true) {
			if($sdbip_id === false) {
				//get sdbip_id from session data
				$sdbip_details = $this->getCurrentSDBIPFromSessionData($page_section);
				if(count($sdbip_details) == 0) {
					$sdbipObject = new SDBP6_SDBIP($this->sdbp6_kpi_modref);
					$sdbip_details = $sdbipObject->getCurrentActivatedSDBIPDetails($this->external_call);
				}
				$sdbip_id = $sdbip_details['sdbip_id'];
			}
			//get time periods and rearrange data
			$data = array('data' => $data, 'time' => $timeObject->getActiveTimePeriods($sdbip_id));
		}

		return $data;
	}


	public function __destruct() {
		parent::__destruct();
	}

}

?>