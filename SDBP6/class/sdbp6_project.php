<?php
/**
 * To manage the Project objects of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_PROJECT extends SDBP6 {

	protected $object_id = 0;
	protected $object_details = array();
	protected $parent_id = false;

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_sdbip_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $owner_field = "_owner_id";
	protected $department_field = "_sub_id";
	protected $secondary_parent_field = false;
	protected $tertiary_parent_field = false;

	protected $results_id_field = "_id";
	protected $results_parent_field = "_proj_id";
	protected $results_secondary_parent_field = "_time_id";
	protected $results_status_field = "_status";
	protected $results_attachment_field = "_attachment";

	protected $results_fields = array(
		'ORIGINAL' => "_original",
		'ADJUSTMENTS' => "_adjustments",
		'REVISED' => "_revised",
		'DESCRIPTION' => "_target_description",
		'ACTUAL' => "_actual",
		'VARIANCE' => "_variance",
		'RESULT' => null,
	);

	protected $time_type = "MONTH";

	protected $sdbip_id = 0;

	protected $list_page_filter_options = array(
		'year' => array(),
		'who' => array('SUB' => "proj_sub_id", 'OWNER' => "proj_owner_id"),
		'what' => array(),
		'when' => array(),
		'display' => array(),
	);

	protected $external_mapping_variables;

	/*protected $results_original_field = "_original";
	protected $results_adjustments_field = "_adjustments";
	protected $results_revised_field = "_revised";
	protected $results_actual_field = "_actual";
	protected $results_variance_field = "_variance";
	protected $results_results_field = null;*/

	protected $has_attachment = false;
	protected $has_results = true;

	protected $ref_tag = "P";


	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "PROJECT";
	const OBJECT_TYPE_PLURAL = "PROJECTS";
	const OBJECT_NAME = "PROJECT";
	const OBJECT_NAME_PLURAL = "PROJECTS";
	const PARENT_OBJECT_TYPE = "SDBIP";
	const CHILD_OBJECT_TYPE = "PROJECT_FINANCES";
	const ADMIN_SECTION = "project";

	const REFTAG = "ERROR/CONST/REFTAG";

	const TABLE = "project";
	const TABLE_FLD = "proj";

	const RESULTS_TABLE = "project_finances";
	const RESULTS_TABLE_FLD = "pf";

	const LOG_TABLE = "project";


	public function __construct($modref = "", $obj_id = 0, $external_call = false, $sdbip_id = 0) {
		parent::__construct($modref);


		if($sdbip_id > 0) {
			$this->parent_id = $sdbip_id;
		}

		$this->has_finances = true;
		$this->actual_update_allowed = false;
		$this->has_approve_functionality = false;
		$this->has_assurance_functionality = false;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		$this->department_field = self::TABLE_FLD.$this->department_field;

		$this->list_page_filter_options['who']['SUB'] = $this->department_field;
		$this->list_page_filter_options['who']['OWNER'] = $this->owner_field;

		$this->results_id_field = self::RESULTS_TABLE_FLD.$this->results_id_field;
		$this->results_parent_field = self::RESULTS_TABLE_FLD.$this->results_parent_field;
		$this->results_secondary_parent_field = self::RESULTS_TABLE_FLD.$this->results_secondary_parent_field;
		$this->results_status_field = self::RESULTS_TABLE_FLD.$this->results_status_field;
		$this->results_attachment_field = self::RESULTS_TABLE_FLD.$this->results_attachment_field;

		$results_keys = array_keys($this->results_fields);
		foreach($results_keys as $fld) {
			if($this->results_fields[$fld] != null) {
				$this->results_fields[$fld] = self::RESULTS_TABLE_FLD.$this->results_fields[$fld];
			}
		}
		$this->results_fields['ID'] = $this->results_id_field;
		$this->results_fields['PARENT'] = $this->results_parent_field;
		$this->results_fields['SECONDARY_PARENT'] = $this->results_secondary_parent_field;
		$this->results_fields['TIME'] = $this->results_secondary_parent_field;
		$this->results_fields['STATUS'] = $this->results_status_field;
		$this->results_fields['ATTACHMENT'] = $this->results_attachment_field;

		if($obj_id > 0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";

		/*		//filter page options - WHO section will be generated as required
				$this->list_page_filter_options['what'] = $this->getWhatFilterOptions();
				$this->list_page_filter_options['display'] = $this->getDisplayFilterOptions();
				$this->list_page_filter_options['filter_names'] = $this->getFilterNames();

				//get time type from parent in SESSION
				$current_sdbip = $this->getCurrentSDBIPFromSessionData();
				if(isset($current_sdbip['project_time_type'])) {
					$this->time_type = $current_sdbip['project_time_type'];
				}
				if(isset($current_sdbip['id'])) {
					$this->sdbip_id = $current_sdbip['id'];
				}
		*/


		//get time type from parent in SESSION
		$current_sdbip = $this->getCurrentSDBIPFromSessionData();
		if(isset($current_sdbip['project_time_type'])) {
			$this->time_type = $current_sdbip['project_time_type'];
		}
		if(isset($current_sdbip['id'])) {
			$this->sdbip_id = $current_sdbip['id'];
		} else {
			$this->sdbip_id = 0;
		}


		//filter page options - WHO section will be generated as required
		$this->list_page_filter_options['year'] = $this->getYearFilterOptions();
		$this->list_page_filter_options['what'] = $this->getWhatFilterOptions();
		unset($this->list_page_filter_options['what']['top']);
		unset($this->list_page_filter_options['what']['!top']);
		$this->list_page_filter_options['when'] = $this->getWhenFilterOptions($this->sdbip_id, $this->time_type);
		$this->list_page_filter_options['display'] = $this->getDisplayFilterOptions();
		$this->list_page_filter_options['filter_names'] = $this->getFilterNames();


	}


	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key) && $v != "0" && $v != "0000-00-00") {
				$var[$key] = date("Y-m-d", strtotime($v));
			} else {
				//check for unnecessary auto-complete fields
				if($this->isThisAnExtraAutoCompleteFieldName($key) == true) {
					unset($var[$key]);
				}
			}
		}
		$import_targets = isset($var['import_targets']) ? $var['import_targets'] : false;
		unset($var['import_targets']);
		if($import_targets === false) {
			$budgets = isset($var[$this->getResultsOriginalFieldName()]) ? $var[$this->getResultsOriginalFieldName()] : array();
			$adjustment_budgets = isset($var[$this->getResultsAdjustmentsFieldName()]) ? $var[$this->getResultsAdjustmentsFieldName()] : array();
			unset($var[$this->getResultsOriginalFieldName()]);
			unset($var[$this->getResultsAdjustmentsFieldName()]);
			$description = isset($var[$this->getResultsTableField()."_target_description"]) ? $var[$this->getResultsTableField()."_target_description"] : array();
			unset($var[$this->getResultsTableField()."_target_description"]);
		} else {
			//projects don't import budgets so no processing required.
			$budgets = array();
			$adjustment_budgets = array();
			$description = array();
		}

		if(!isset($var['proj_src_type'])) {
			$var['proj_src_type'] = self::OBJECT_SRC_MANUAL;
		}
		$import_status = isset($var['import_status']) ? $var['import_status'] : 0;
		unset($var['import_status']);
		$var[$this->getTableField().'_status'] = SDBP6::ACTIVE + $import_status;
		/*		switch($var['proj_src_type']) {
					case self::OBJECT_SRC_IDP:
					case self::OBJECT_SRC_SDBIP:
						$var[$this->getTableField().'_status']+=self::CONVERT_IMPORT;
						$var[$this->getTableField().'_status']+=self::EXTERNAL_IMPORT;
						break;
				}*/
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);
		if($id > 0) {
			//Add budgets
			if(count($budgets) == 0) {
				//create blank records
				$timeObject = new SDBP6_SETUP_TIME();
				$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($var['proj_sdbip_id'], $this->getTimeType());
				foreach($time_periods as $time_id => $t) {
					$this->addBudgetObject($id, $time_id, 0, 0, "");
				}
			} else {
				foreach($budgets as $time_id => $value) {
					$a = isset($adjustment_budgets[$time_id]) ? $adjustment_budgets[$time_id] : 0;
					$d = isset($description[$time_id]) ? $description[$time_id] : "";
					$this->addBudgetObject($id, $time_id, $value, $a, $d);
				}
			}
			//don't add log if converting from previous version
			if($var['proj_src_type'] != SDBP6::OBJECT_SRC_CONVERT) {
				$changes = array(
					'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$id." |created|.",
					'user' => $this->getUserName(),
				);
				$log_var = array(
					'object_id' => $id,
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => SDBP6_LOG::CREATE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			}
			$result = array(
				0 => "ok",
				1 => "".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully ".$this->getActivityName("created").".",
				'object_id' => $id,
				'log_var' => $log_var
			);
			return $result;
		}
		return array("error", "An error occurred while trying to save the ".$this->getObjectName($this->getMyObjectName()).".  Please try again or contact your Assist Administrator for assistance.");
	}


	public function addObjectFromExternalConversion($new_record) {
		$link_details = $new_record['link_details'];
		unset($new_record['link_details']);
		$new_record['proj_src_type'] = $link_details['src_type'];
		unset($link_details['src_type']);
		$new_record[$this->getParentFieldName()] = $link_details['sdbip_id'];
		unset($link_details['sdbip_id']);

		$result = $this->addObject($new_record);
		$object_id = $result['object_id'];

		$link_details['proj_id'] = $object_id;

		$insert_data = array();
		foreach($link_details as $fld => $value) {
			$insert_data['ps_'.$fld] = $value;
		}
		$insert_data['ps_status'] = SDBP6::ACTIVE;

		$sql = "INSERT INTO ".$this->getTableName()."_source SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);

		return $object_id;

	}


	public function editObject($var, $attach_logs = array()) {
		$extra_logs = array();

		/**
		 * first process extra items first and generate any necessary additional logs
		 */
		//get field names
		$original_field_name = $this->getResultsOriginalFieldName();
		$adjustments_field_name = $this->getResultsAdjustmentsFieldName();
		$revised_field_name = $this->getResultsRevisedFieldName();
		$actual_field_name = $this->getResultsActualFieldName();
		$variance_field_name = $this->getResultsVarianceFieldName();
		//get values from input
		$original_budgets = $var[$original_field_name];
		$adjustment_budgets = $var[$adjustments_field_name];
		$object_id = $var['object_id'];
		$sdbip_id = $var[$this->getParentFieldName()];
		//remove results values from input so that not processed in central edit function
		unset($var[$this->getParentFieldName()]);
		unset($var[$original_field_name]);
		unset($var[$adjustments_field_name]);
		//get time settings to have names handy for logs
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id);
		//get results headings to have names handy for logs
		$headingObject = new SDBP6_HEADINGS();
		$results_headings = $headingObject->getMainObjectHeadings(self::CHILD_OBJECT_TYPE, "LOGS");
		//fetch existing results
		$old_results = $this->getRawResults($object_id);

		//loop through budgets to get valid time_ids that are open and can be amended
		foreach($original_budgets as $time_id => $new_original) {
			if(strlen($new_original) == 0) {
				$new_original = 0;
			}
			$new_adjustment = isset($adjustment_budgets[$time_id]) ? $adjustment_budgets[$time_id] : 0;
			if(strlen($new_adjustment) == 0) {
				$new_adjustment = 0;
			}
			//if old budget exists for given time then process as edit otherwise process as add
			if(isset($old_results[$time_id])) {
				//check for changes to edit
				$old = $old_results[$time_id];
				$old_original = $old[$original_field_name];
				$old_adjustments = $old[$adjustments_field_name];
				//if old != new then record changes
				if($old_original != $new_original || $old_adjustments != $new_adjustment) {
					//save to DB
					$calculations = $this->editBudgetObject($object_id, $time_id, $new_original, $new_adjustment, $old[$actual_field_name]);
					//add to log
					$n = $new_original;
					$o = $old_original;
					$f = $original_field_name;
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$n = $new_adjustment;
					$o = $old_adjustments;
					$f = $adjustments_field_name;
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$f = $revised_field_name;
					$n = $calculations[$f];
					$o = $old[$f];
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$f = $variance_field_name;
					$n = $calculations[$f];
					$o = $old[$f];
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
				}
			} else {
				//process as add
				$calculations = $this->addBudgetObject($object_id, $time_id, $new_original, $new_adjustment);
				//add to log
				$n = $new_original;
				$o = 0;
				$f = $original_field_name;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$n = $new_adjustment;
				$o = 0;
				$f = $adjustments_field_name;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$f = $revised_field_name;
				$n = $calculations[$f];
				$o = 0;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$f = $variance_field_name;
				$n = $calculations[$f];
				$o = 0;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
			}
		}


		$result = $this->editMyObject($var, $extra_logs);


		return $result;
	}


	public function saveActivatedObjectForHistory($parent_id, $filename, $path) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = ".$parent_id;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$keys = array_keys($rows);
		$results = $this->getRawResults($keys);
		$files = array($filename);
		$final_path = $this->getFilesLocation($path, $this->getCmpCode());

		//save as serialize TXT file
		$fdata = serialize($rows);
		$f = $filename."_serialize_rows.txt";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$rdata = serialize($results);
		$f = $filename."_serialize_results.txt";
		$this->saveDataToFile($final_path, $f, $rdata);
		$files[] = $f;

		//save as json_encode TXT file
		$fdata = json_encode($rows);
		$f = $filename."_json_rows.txt";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$rdata = json_encode($results);
		$f = $filename."_json_results.txt";
		$this->saveDataToFile($final_path, $f, $rdata);

		//save as CSV file
		$header = array_keys($rows[$keys[0]]);
		$data = array(
			0 => "\"".implode("\",\"", $header)."\"",
		);
		foreach($rows as $row) {
			$data[] = "\"".implode("\",\"", $row)."\"";
		}
		$fdata = implode("\r\n", $data);
		$f = $filename."_rows.csv";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$r_keys = array_keys($results);
		$t_keys = array_keys($results[$r_keys[0]]);
		$header = array_keys($results[$r_keys[0]][$t_keys[0]]);
		$data = array(
			0 => "\"".implode("\",\"", $header)."\"",
		);
		foreach($results as $ri => $row) {
			foreach($row as $ti => $time) {
				$time['parent_object_id'] = $ri;
				$time['parent_time_id'] = $ti;
				$data[] = "\"".implode("\",\"", $time)."\"";
			}
		}
		$rdata = implode("\r\n", $data);
		$f = $filename."_results.csv";
		$this->saveDataToFile($final_path, $f, $rdata);
		$files[] = $f;

		return $files;
	}


	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |deactivated|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Inactive",
				'from' => "Active",
				'raw' => array(
					'to' => self::INACTIVE,
					'from' => self::ACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DEACTIVATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".$this->getActivityName("deactivated").".",
			'object_id' => $id,
		);
		return $result;
	}


	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |deleted|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Deleted",
				'from' => "Active",
				'raw' => array(
					'to' => self::DELETED,
					'from' => self::ACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DELETE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".strtolower($this->getActivityName("deleted")).".",
			'object_id' => $id,
		);
		return $result;
	}


	public function restoreObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |restored|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Active",
				'from' => "Inactive",
				'raw' => array(
					'to' => self::ACTIVE,
					'from' => self::INACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::RESTORE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".strtolower($this->getActivityName("restored")).".",
			'object_id' => $id,
		);
		return $result;
	}


	/**
	 * RESET all objects relating to specific SDBIP: Called from New > Create > RESET
	 */
	public function resetObject($var, $return_response = false) {
		$parent_id = is_array($var) ? $var['parent_id'] : $var;
		//change all active objects to SDBIP_RESET - use bitwise to catch all active items that are individually created, manually imported or are in process of external import
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::SDBIP_RESET." WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_active = $this->db_update($sql);
		//change all deleted objects to DELETED + SDBIP_RESET
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".(self::SDBIP_RESET + self::DELETED)." WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") = ".self::DELETED." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_deleted = $this->db_update($sql);
		//change all deactivated objects to INACTIVE + SDBIP_RESET
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".(self::SDBIP_RESET + self::INACTIVE)." WHERE (".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_inactive = $this->db_update($sql);
		//remove links if imported
		$sql = "UPDATE ".$this->getDBRef()."_import_external_links SET status = 0 WHERE sdbip_id = ".$parent_id." AND status = ".self::ACTIVE." AND local_type = '".self::OBJECT_TYPE."'";
		$this->db_update($sql);

		if($return_response) {
			$response = $mar_active." active ".strtolower($this->getObjectName($this->getMyObjectName($mar_active == 1 ? false : true)));
			return $response;
		} else {
			$response = "Reset completed successfully.  ".$mar_active." active ".strtolower($this->getObjectName($this->getMyObjectName($mar_active == 1 ? false : true)))." removed.";
			return array("ok", $response);
		}
	}


	/**
	 * Save changes in phase 3 of import
	 */
	public function importPhase3($var) {
		$i = $var['settings'];
		$val = $var['my_val'];
		$type = $var['type'];


		if($type == "RESULT") {

			$val = ASSIST_HELPER::code($val);

			$x = explode("_", $i);
			$time_id = $x[count($x) - 1];
			unset($x[count($x) - 1]);
			$id = $x[count($x) - 1];
			unset($x[count($x) - 1]);
			$field = implode("_", $x);

			$sql = "UPDATE ".$this->getResultsTableName()." SET $field = '".$val."' WHERE ".$this->getResultsParentFieldName()." = ".$id." AND ".$this->getResultsSecondaryParentFieldName()." = ".$time_id;
			$this->db_update($sql);

			$required_result = "";

		} else {
			if($type != "STATUS_CHECK" && $val !== false) {

				switch($type) {
					case "DATE":
						$v = strtotime($val);
						$val = date("Y-m-d", $v);
						break;
					case "TEXT":
					default:
						$val = ASSIST_HELPER::code($val);
						break;
				}


				$x = explode("_", $i);
				$id = $x[count($x) - 1];
				unset($x[count($x) - 1]);
				$field = implode("_", $x);

				$sql = "UPDATE ".$this->getTableName()." SET $field = '".$val."' WHERE ".$this->getIDFieldName()." = ".$id;
				$this->db_update($sql);


				//update required field status
				if(isset($_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'][$field])) {
					$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'][$field]['value'] = $val;
				}
			} else {
				$id = $i;
			}
			//check required field status
			$required = $_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'];
			if(count($required) > 0) {
				$can_signoff = true;
				foreach($required as $fld => $r) {
					switch($r['type']) {
						case "MULTILIST":
						case "MULTIOBJECT":
						case "MULTISEGMENT":
							if(strlen($r['value']) == 0) {
								$can_signoff = false;
							}
							break;
						case "LIST":
						case "OBJECT":
						case "SEGMENT":
							if(strlen($r['value']) == 0 || $r['value'] == "X" || $r['value'] == "0" || $r['value'] == 0) {
								$can_signoff = false;
							}
							break;
						default:
							if(strlen($r['value']) == 0) {
								$can_signoff = false;
							}
							break;
					}
				}
			} else {
				$can_signoff = true;
			}
			$status = $_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'];
			if((($status & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) && $can_signoff === true) {
				$new_status = ($status - SDBP6::CONVERT_IMPORT + SDBP6::CONVERT_SDBP6ED);
				//set status to complete
				$sql = "UPDATE ".$this->getTableName()." 
						SET ".$this->getStatusFieldName()." = ".$new_status." 
						WHERE ".$this->getIDFieldName()." = ".$id." 
						AND (".$this->getStatusFieldName()." & ".SDBP6::CONVERT_IMPORT.") = ".SDBP6::CONVERT_IMPORT;
				$this->db_update($sql);
				$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'] = $new_status;
				$required_result = ASSIST_HELPER::getDisplayResult(array("ok", "Complete"));
			} elseif((($status & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) && $can_signoff === false) {
				$new_status = ($status + SDBP6::CONVERT_IMPORT - SDBP6::CONVERT_SDBP6ED);
				//set status to incomplete
				$sql = "UPDATE ".$this->getTableName()." 
						SET ".$this->getStatusFieldName()." = ".$new_status." 
						WHERE ".$this->getIDFieldName()." = ".$id." 
						AND (".$this->getStatusFieldName()." & ".SDBP6::CONVERT_SDBP6ED.") = ".SDBP6::CONVERT_SDBP6ED;
				$this->db_update($sql);
				$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'] = $new_status;
				$required_result = ASSIST_HELPER::getDisplayResult(array("info", "In Progress"));
			} elseif((($status & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) && $can_signoff === true) {
				$required_result = ASSIST_HELPER::getDisplayResult(array("ok", "Complete"));
			} else {
				$required_result = ASSIST_HELPER::getDisplayResult(array("info", "In Progress"));
			}
		}
		return array("ok", ASSIST_HELPER::getDisplayIconAsDiv("ok"), $required_result);

	}


	/**
	 * Update KPI Results
	 */
	public function updateObject($var, $attach) {
		$result = array("info", "I'm sorry but I don't know what you want me to do.", $var);

		/* set variables and tidy up $var so that what is left is the form fields */
		unset($var['action']);
		unset($var['page_direct']);
		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);
		$kpi_id = $var['object_id'];
		unset($var['object_id']);
		$time_id = $var['time_id'];
		unset($var['time_id']);
		$target_type_id = isset($var['target_type_id']) ? $var['target_type_id'] : 0;
		unset($var['target_type_id']);
		$update_status = $var[$this->getResultsTableField().'_display_status'];    //convert the STATUS heading from display_status to update field
		$var[$this->getResultsTableField().'_update'] = $update_status;
		unset($var[$this->getResultsTableField().'_display_status']);
		$revised_target = $var[$this->getResultsRevisedFieldName()];
		unset($var[$this->getResultsRevisedFieldName()]);
		$new_attachments = $attach['new']; //$new attachments (for logging)
		$save_attachments = $attach['save']; //attachments for DB savings
		//get headings so that field type is known for logging purposes
		$headingObject = new SDBP6_HEADINGS();
		$result_headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($this->getMyChildObjectType(), "FORM"));
		//Set variables for processing
		$update_data = array();
		$log_changes = array();
		//Get time information - for logging
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $this->getTimeType());
		$time_name = $time_periods[$time_id];
		//Get target type information - for logging
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("", $target_type_id);


		/* GET PREVIOUS UPDATE RECORD FOR LOG COMPARISON */
		$old_var = $this->getRawUpdateObject($kpi_id, $time_id);
		/* PROCESS $var FOR CHANGES */
		foreach($var as $fld => $new_val) {
			if($fld == $this->getResultsTableField()."_update") {
				$heading_field = $this->getResultsTableField()."_display_status";
			} else {
				$heading_field = $fld;
			}
			//check that the field is valid and in the database by making sure that it exists in the old_var record and in the headings
			if(isset($old_var[$fld]) && isset($result_headings[$heading_field])) {
				//get previous value
				$old_val = $old_var[$fld];
				//get headings settings
				$head = $result_headings[$heading_field];
				//check if the value has changed & process changes according to field type - WARNING: TEXT FIELDS ARE NOT CODED AS THE SDBP6Helper(js).processObjectFormWithAttachments DOES NOT CALL ASSISTFORM.serialize
				switch($head['type']) {
					case "NUM":
						if($old_val != $new_val) {
							$update_data[$fld] = $new_val;
							$log_changes[$fld] = array(
								'to' => ($head['apply_formatting'] == true ? $targetTypeObject->formatNumberBasedOnTargetType($new_val) : $new_val),
								'from' => ($head['apply_formatting'] == true ? $targetTypeObject->formatNumberBasedOnTargetType($old_val) : $old_val),
								'raw' => array('to' => $new_val, 'from' => $old_val),
							);
						}
						break;
					case "TEXT":
						$new_val = ASSIST_HELPER::code($new_val);
						if($old_val != $new_val) {
							$update_data[$fld] = $new_val;
							$log_changes[$fld] = array(
								'to' => $new_val,
								'from' => $old_val,
							);
						}
						break;
					case "STATUS":
						$log_field = "Update Status";
						//Convert Bool Y/N "true" to Completed
						if($new_val == true) {
							$new_val = $this->getCompletedUpdateStatus();
						}

						//if new = (completed)
						//if old (completed) then do nothing
						//else if old = (in progress) then to Completed from In Progress
						//else to Completed from New
						//else
						//if old = (new) then to In Progress (2) from New
						//else if old = (completed) then to In Progress (2) from Completed
						//else - do nothing (no change)
						if($this->isStatusCompleted($new_val)) {
							if($this->isStatusCompleted($old_val)) {
								//do nothing - no change
							} elseif($this->isStatusInProgress($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "Completed",
									'from' => "In Progress",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} else {
								//assume old = new
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "Completed",
									'from' => "New",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							}
						} else {
							//assume new is now In Progress
							$new_val = $this->getInProgressUpdateStatus();
							if($this->isStatusCompleted($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "In Progress",
									'from' => "Completed",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} elseif($this->isStatusNew($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "In Progress",
									'from' => "New",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} else {
								//do nothing - both = In Progress
							}
						}
						break;
				}
//test for variance
				if($head['is_actual'] == true) {
					$next_fld = $this->getResultsVarianceFieldName();
					$next_new = $revised_target - $var[$this->getResultsActualFieldName()];
					$next_old = $old_var[$next_fld];
					if($next_old != $next_new) {
						$update_data[$next_fld] = $next_new;
						$log_changes[$next_fld] = array(
							'to' => $targetTypeObject->formatNumberBasedOnTargetType($next_new),
							'from' => $targetTypeObject->formatNumberBasedOnTargetType($next_old),
							'raw' => array('to' => $next_new, 'from' => $next_old)
						);
					}
				}

			} //end if isset (old)
		}

		/* Check if new attachments are present and need to be updated & logged */
		if(count($new_attachments) > 0) {
			$update_data[$this->getResultsAttachmentFieldName()] = $this->codeAttachmentDataForDatabase($save_attachments);
			$new_attach = array();
			foreach($new_attachments as $i => $na) {
				$new_attach[] = $na['original_filename'];
			}
			$log_changes[$this->getResultsAttachmentFieldName()] = "Added new |".(count($new_attach) == 1 ? "attachment" : "attachments")."|: ".implode("; ", $new_attach);
		}

		/* only save update if there is something to update */
		if(count($log_changes) > 0 || count($update_data) > 0) {
			$update_data[$this->getResultsTableField().'_updateuser'] = $this->getUserID();
			$update_data[$this->getResultsTableField().'_updatedate'] = date("Y-m-d H:i:s");

			/* Update database */
			$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$this->db_update($sql);

			/* Log changes */
			$changes = array(
					'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$kpi_id." |updated| for |time|: $time_name.",
					'user' => $this->getUserName()
				) + $log_changes;
			$log_var = array(
				'object_id' => $kpi_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::UPDATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			/* Send emails if required for Approve/Assurance */


			/* set result & return */
			$result = array("ok", "Update saved successfully.", $update_data);
		} else {
			$result = array("info", "No changes found to be saved.");
		}
		return $result;
	}


	public function deleteAttachment($raw_id, $i, $page_activity = "UPDATE") {
		//TODO fix function - undefined variables
		$result = array("info", "Sorry, I couldn't work out what you wanted me to do.");
		//process object_id to extract time_id
		$o = explode("-", $raw_id);
		$kpi_id = $o[0];
		$time_id = $o[1];
		//Get time information - for logging
		$raw_object = $this->getRawObject($kpi_id);
		$sdbip_id = $raw_object[$this->getParentFieldName()];
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $this->getTimeType());
		$time_name = $time_periods[$time_id];
		//get exisitng attachment details
		$old_attach = $this->getAttachmentDetails($raw_id, $i, $page_activity);

		if(isset($old_attach[$i])) {
			//get the deleted item for logging
			$deleted_attach = $old_attach[$i];
			//get the updated attachment settings for saving to the database
			$new_attach = $old_attach;
			unset($new_attach[$i]);

			//save to db
			$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->getResultsAttachmentFieldName()." = '".$this->codeAttachmentDataForDatabase($new_attach)."' WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$this->db_update($sql);

			//set logs
			$changes = array(
				'user' => $this->getUserName(),
				$this->getResultsAttachmentFieldName() => "|deleted| |attachment| ".$deleted_attach['original_filename']." from |".$this->getMyObjectType()."| ".$this->getRefTag().$kpi_id." for |time|: $time_name.",
			);
			$log_var = array(
				'object_id' => $kpi_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::UPDATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			//return result
			$result = array("ok", $this->getObjectName("ATTACHMENT")." successfully ".$this->getActivityName("DELETED"));
		} else {
			$result = array("error", "Delete was unsuccessful - attachment $i could not be found.");
		}
		return $result;
	}


	/*****************************************************************************************************************************
	 * GET Info functions
	 */

	public function getMyObjectType($plural = false) {
		return $plural ? self::OBJECT_TYPE_PLURAL : self::OBJECT_TYPE;
	}

	public function getMyObjectName($plural = false) {
		return $plural ? self::OBJECT_NAME_PLURAL : self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyChildObjectType() {
		return self::CHILD_OBJECT_TYPE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getResultsTableField() {
		return self::RESULTS_TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getResultsTableName() {
		return $this->getDBRef()."_".self::RESULTS_TABLE;
	}

	public function hasResults() {
		return $this->has_results;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function hasDeadline() {
		return false;
	}

	public function getDeadlineField() {
		return false;
	}

	public function getTimeSetting() {
		return $this->time_type;
	}

	public function getCalcTypeTableField() {
		return false;
	}

	public function getTargetTypeTableField() {
		return false;
	}

	public function getBaselineTableField() {
		return false;
	}

	public function getNewIndicatorTableField() {
		return false;
	}

	public function getAdminSectionIdentifier() {
		return self::ADMIN_SECTION;
	}

	public function getFilterByOptions($page_section = "MANAGE", $page_action = "UPDATE", $sdbip_id = 0) {
		if($sdbip_id == 0 || $sdbip_id === false) {
			if($page_section != "NEW") {
				$sdbip_id = $_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['OBJECT']['details']['id'];
			} else {
				$sdbip_id = $_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['NEW_OBJECT']['details']['id'];
			}
		}
		$filter_by = $this->list_page_filter_options;
		$who = $filter_by['who'];
		$filter_by['who'] = array();
		//if the settings aren't available in the session - get from db
//DEVELOPMENT NOTE: REMEMBER TO COMMENT OUT THE IF STATEMENT IF TESTING USER ACCESS OR ELSE WAIT FOR 15 MINUTES!!!
		if(!isset($_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]) || $_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['timestamp'] < time()) {
			$who_settings = array();
			//if not view or admin then check for admin access
			//echo "<h3>".$page_section." :: ".$page_action."</h3>";
			if($page_section == "MANAGE" && $page_action != "VIEW") {
				$adminObject = new SDBP6_SETUP_ADMINS();
				$admin_access = $adminObject->getMyAdminAccess();
				$include_parent_names = true;
			} else {
				$admin_access = array();
				$include_parent_names = false;
			} //ASSIST_HELPER::arrPrint($admin_access);
			$headObject = new SDBP6_HEADINGS();
			$listObject = new SDBP6_LIST();
			$listObject->setSDBIPID($sdbip_id);
			foreach($who as $k => $f) { //echo "<hr /><h3>".$k."</h3>";
				//get heading details per field
				$head = $headObject->getAHeadingRecordsByField($f, self::OBJECT_TYPE);
				$who_settings[$f] = array(
					'name' => $this->replaceAllNames($head['name']),
					'table' => $head['h_table'],
					'type' => $head['h_type'],
					'items' => array()
				);
				//get list items per field
				if($head['h_type'] == "LIST") {
					$listObject->changeListType($head['h_table']);
					$items = $listObject->getActiveListItemsFormattedForSelect();
				} else {
					//assume OBJECT
					$class = $head['h_table'];
					$objObject = new $class();
					$objObject->setSDBIPID($sdbip_id);
					$items = $objObject->getActiveObjectsFormattedForFiltering($include_parent_names);
				}
				if($page_section == "MANAGE" && $page_action != "VIEW" && count($admin_access) > 0 && isset($admin_access['by_section'][self::ADMIN_SECTION][strtolower($page_action)][$k])) {
					$access = $admin_access['by_section'][self::ADMIN_SECTION][strtolower($page_action)][$k]; //ASSIST_HELPER::ArrPrint($access);
					$item_keys = array_keys($items);
					$process_items = array();
					//foreach($items as $key => $item) {
					for($x = 0; $x < count($item_keys); $x++) {
						$key = $item_keys[$x];
						if($k == "OWNER") {
							if(!in_array($key, $access)) {
								unset($items[$key]);
							}
						} else {
							$key2 = explode("_", $key);
							$item_type = $key2[0];
							$item_id = $key2[1];
							if(in_array($item_id, $access)) {
								//check that item hasn't already been processed (if both DIR and SUB are applicable)
								if(isset($process_items[$key])) {
									$process_items[$key] .= "*";
								} else {
									$process_items[$key] = $items[$key];
									if($item_type != "SUB" && $x != (count($item_keys) - 1)) {
										$move_on = true;
										while($move_on) {
											$x++;
											$key = $item_keys[$x];
											$key2 = explode("_", $key);
											if($key2[0] == "SUB") {
												$process_items[$key] = $items[$key];
											} else {
												$move_on = false;
												$x--;
											}
										}
									}
								}
							}
						}
						/*						if(!in_array($key2[1],$access)) {
													unset($items[$key]);
												} else {
													if($key2[0]=="DIR" && $x!=(count($item_keys)-1)) {
														$move_on = true;
														while($move_on) {
															$x++;
															$key = $item_keys[$x];
															$key2 = explode("_",$key);
															if($key2[0]=="SUB") {

															} else {
																$move_on=false;
																$x--;
															}
														}
													}
												}*/
					}
					if($k != "OWNER") {
						$raw_items = $items;
						$items = $process_items;
					}
				}
				$who_settings[$f]['items'] = $items;
			}
			//populate session
			$_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['WHO'] = $who_settings;
			$_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['timestamp'] = time() + 15 * 60;
		} else {
			$who_settings = $_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['WHO'];
		}

		//process who_settings
		foreach($who as $k => $f) {
			if(count($who_settings[$f]['items']) > 0) {
				if($k == "SUB") {
					//special code to be added for parent directorate here
					$filter_by['who'][$k] = " ---".$who_settings[$f]['name']."--- ";
					foreach($who_settings[$f]['items'] as $i => $l) {
						$filter_by['who'][$i] = $l;
					}
				} else {
					$filter_by['who'][$k] = " ---".$who_settings[$f]['name']."--- ";
					foreach($who_settings[$f]['items'] as $i => $l) {
						$filter_by['who'][$k."_".$i] = $l;
					}
				}
			}
		}


		if($page_action == "UPDATE") {
			$filter_by['update_when'] = array();
			/**
			 * Limit time periods on the update page to those which the user can actually access to update
			 *  = TP must have started
			 *  = TP must be open for the user's access level
			 **/
			//get user access level
			$useraccessObject = new SDBP6_USERACCESS();
			$my_useraccess = $useraccessObject->getMyTimePeriodUserAccess();
			if($my_useraccess['time_third'] == true) {
				$time_access = "tertiary";
			} else {
				if($my_useraccess['time_second'] == true) {
					$time_access = "secondary";
				} else {
					$time_access = "primary";
				}
			}
			//loop through TPs and find those that have started and which are open for the user
			foreach($filter_by['when'] as $time_id => $time) {
				if($time['has_started'] == true && $time['is_open'][$time_access] == true) {
					$filter_by['update_when'][$time_id] = $time;
				}
			}
		}

		return $filter_by;
	}


	public function getParentID($object_id = 0) {
		if($object_id == 0) {
			return $this->sdbip_id;
		} else {
			$row = $this->getRawObject($object_id);
			return $row[$this->getParentFieldName()];
		}
	}

	/** Function to get the SDBIP ID for a given object - used by Individual Performance series of modules (PM*)  */
	public function getMyParentSDBIPID($object_id = 0) {
		return $this->getParentID($object_id);
	}


	public function getFilterSQL($filter) {
		$where = array();
//		'who'=>"all",
		if($filter['who'] != "all") {
			$who = explode("_", $filter['who']);
			switch($who[0]) {
				case "SUB":
					$where['who'] = $this->getDepartmentFieldName()." = ".$who[1];
					break;
				case "OWNER":
					$where['who'] = $this->getOwnerFieldName()." = ".$who[1];
					break;
				case "DIR":
					$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
					$list = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => $who[1]));
					if(count($list) > 0) {
						$where['who'] = $this->getDepartmentFieldName()." IN (".implode(",", array_keys($list)).")";
					} else {
						//use impossible condition to deal with issue arising from a directorate with no children
						$where['who'] = $this->getDepartmentFieldName()." < 0 ";
					}
					break;
			}
		}
//		'what'=>"all",
		switch($filter['what']) {
			case "op":
				$where['what'] = $this->getTableField()."_cap_op = 0";
				break;
			case "cap":
				$where['what'] = $this->getTableField()."_cap_op > 0";
				break;
			/*			case "top":
							$where['what'] = $this->getTableField()."_top_id > 0";
							break;
						case "!top":
							$where['what'] = $this->getTableField()."_top_id = 0";
							break;*/
			case "all":
			default:
				//do nothing - all is default;
		}
//		'when'=>false,
		//to be programmed?
//		'display'=>"limited" - IGNORE - Handled by HEADINGS class

		if(count($where) > 0) {
			return "(".implode(") AND (", $where).")";
		} else {
			return "";
		}
	}


	/**
	 * Does the Project have TopKPIs or DeptKPIs linked to it for display on a view page?
	 * Used by common/form_object.php & *_DISPLAY->getObjectForm()
	 * JC #AA-508 5 Jan 2021
	 * @param $object Array returned from getRawUpdateObject
	 * @return Array [display_linked=>true/false, linked=>array of linked objects id => type
	 */
	public function getLinkedObjectDetailsForDisplay($object) {
		$data = array('display_linked_objects' => false, 'linked_objects' => array());


		//SDBP6_TOPKPI
		//not linked directly - must get from TOPKPI class
		$proj_id = $object[$this->getIDFieldName()];
		$linked_object = new SDBP6_TOPKPI();
		$linked_object_ids = $linked_object->getKPIsLinkedToProject($proj_id);
		if(count($linked_object_ids) > 0) {
			$data['display_linked_objects'] = true;
			$data['linked_objects'][SDBP6_TOPKPI::OBJECT_TYPE] = array();
			foreach($linked_object_ids as $row) {
				$data['linked_objects'][SDBP6_TOPKPI::OBJECT_TYPE][] = $row['id'];
			}
		}


		//tertiary = SDBP6_DEPTKPI
		//not linked directly - must get from DEPTKPI class
		$proj_id = $object[$this->getIDFieldName()];
		$linked_object = new SDBP6_DEPTKPI();
		$linked_object_ids = $linked_object->getKPIsLinkedToProject($proj_id);
		if(count($linked_object_ids) > 0) {
			$data['display_linked_objects'] = true;
			$data['linked_objects'][SDBP6_DEPTKPI::OBJECT_TYPE] = array();
			foreach($linked_object_ids as $row) {
				$data['linked_objects'][SDBP6_DEPTKPI::OBJECT_TYPE][] = $row['id'];
			}
		}


		return $data;
	}


	/*****************************************************************************************************************************
	 * GET Object functions
	 */

	public function getListOfProjectsByGUID($sdbip_id) {
		$data = array();
		$sql = "SELECT proj_ref, proj_id FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = $sdbip_id";
		$rows = $this->mysql_fetch_value_by_id($sql, "proj_id", "proj_ref");
		foreach($rows as $id => $guid) {
			$data[trim($guid)] = $id;
		}
		return $data;
	}

	public function getListOfProjectsByFinancialGUID($sdbip_id) {
		$data = array();
		$sql = "SELECT proj_finref, proj_id FROM ".$this->getTableName()." P WHERE ".$this->getParentFieldName()." = ".$sdbip_id." AND ".$this->getActiveStatusSQL("P");
		$rows = $this->mysql_fetch_value_by_id($sql, "proj_id", "proj_finref");
		foreach($rows as $id => $guid) {
			$guid = $this->cleanUpText($guid);
			$data[$guid] = $id;
		}
		return $data;
	}

	public function getList($section, $options) {
		$sdbip_id = $options['sdbip_id'];
		$list = $this->getMyList($this->getMyObjectType(), $section, $options);

		if(isset($list['head']['original_total']) || isset($list['head']['revised_total']) || isset($list['head']['actual_total'])) {
			$project_ids = array_keys($list['rows']);
			$project_ids = $this->removeBlanksFromArray($project_ids);
			if(count($project_ids) > 0) {
				//if required to display, get results for specific time periods
				if(isset($list['results_head']['bottom']) && isset($list['results_head']['top']) && count($list['results_head']['top']) > 0 && count($list['results_head']['top']) > 0 && count($list['results_head']['bottom']) > 0) {
					$list['results_rows'] = $this->getResultsForListPages($list['rows'], ($list['results_head']['top']), $list['results_head']['bottom']);
				}
				//set and format summary details
				$summary_values = $this->getSummaryResults($project_ids, $sdbip_id);
				//prep formatting
				$pre = "";//$target_types[$tt]['display_type']!="post" ? $target_types[$tt]['code']."&nbsp;" : "";
				$post = "";//$target_types[$tt]['display_type']=="post" ? (substr($target_types[$tt]['code'],0,1)!=":"?"&nbsp;":"").$target_types[$tt]['code'] : "";

				foreach($list['rows'] as $proj_id => $row) {
					if(!isset($summary_values[$proj_id])) {
						$summary_values[$proj_id]['original_total'] = "0.00";
						$summary_values[$proj_id]['revised_total'] = "0.00";
						$summary_values[$proj_id]['actual_total'] = "0.00";
					}
					if(isset($list['head']['original_total'])) {
						$list['rows'][$proj_id]['original_total']['display'] = str_replace(" ", "&nbsp;", ASSIST_HELPER::format_number($summary_values[$proj_id]['original_total'], 2));
					}
					if(isset($list['head']['revised_total'])) {
						$list['rows'][$proj_id]['revised_total']['display'] = str_replace(" ", "&nbsp;", ASSIST_HELPER::format_number($summary_values[$proj_id]['revised_total'], 2));
					}
					if(isset($list['head']['actual_total'])) {
						$list['rows'][$proj_id]['actual_total']['display'] = str_replace(" ", "&nbsp;", ASSIST_HELPER::format_number($summary_values[$proj_id]['actual_total'], 2));
					}
					$obj_id = $proj_id;
					if(isset($list['results_rows'])) {
						foreach($list['results_rows'][$obj_id] as $time_id => $result) {
							foreach($result as $r_fld => $r) {
								if(isset($list['results_head']['bottom'][$r_fld]['apply_formatting']) && $list['results_head']['bottom'][$r_fld]['apply_formatting'] == true) {
									$list['results_rows'][$obj_id][$time_id]['raw'][$r_fld] = $r;
									//always format as float because currency
									$r = ASSIST_HELPER::format_number($r, "FLOAT");
									$list['results_rows'][$obj_id][$time_id][$r_fld] = $pre.str_replace(" ", "&nbsp;", $r).$post;
								}
							}
						}
					}
				}
			}
		}
		return $list;
	}

	public function getAObject($id = 0, $options = array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id, $options);
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		//check for a valid id and return a blank array if not
		if(!$this->checkIntRef($obj_id)) {
			return array();
		} else {
			//get raw data direct from table
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
			$data = $this->mysql_fetch_one($sql);
			//add totals & results
			$summary_values = $this->getSummaryResults(array($obj_id));
			foreach($summary_values[$obj_id] as $fld => $v) {
				$data[$fld] = $v;
			}
			$data['results'] = $this->getRawResults($obj_id);
			return $data;
		}
	}

	/**
	 * Returns an unformatted array of a Results Object for given object_id & time_id
	 */
	public function getRawUpdateObject($obj_id, $time_id) {
		//check for a valid id and return a blank array if not
		if(!$this->checkIntRef($obj_id) || !$this->checkIntRef($time_id)) {
			return array();
		} else {
			//get raw data direct from table
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." = ".$obj_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$data = $this->mysql_fetch_one($sql);
			return $data;
		}
	}


	public function getStats($parent_id) {
		$data = array(
			'count' => 0,
			'updates_done' => 0,
			'converted' => 0,
			'sdbp6ed' => 0
		);
		//count number of objects
		$sql = "SELECT
					count(O.".$this->getIDFieldName().") as count
					, O.".$this->getStatusFieldName()." as status
				FROM ".$this->getTableName()." O
				WHERE ".$this->getActiveStatusSQL("O")."
				AND O.".$this->getParentFieldName()." = ".$parent_id
			." GROUP BY O.".$this->getStatusFieldName();
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$data['count'] += $r['count'];
			if(($r['status'] & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) {
				$data['converted'] += $r['count'];
			}
			if(($r['status'] & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) {
				$data['sdbp6ed'] += $r['count'];
			}
		}
		//check if updates have been done
		$sql = "SELECT count(R.".$this->getResultsTableField()."_update) as updates_done
				FROM ".$this->getResultsTableName()." R
				INNER JOIN ".$this->getTableName()." O ON R.".$this->getResultsParentFieldName()." = O.".$this->getParentFieldName()."
				WHERE ".$this->getResultsActiveStatusSQL("R")."
				AND R.".$this->getResultsTableField()."_update = 1
				AND O.".$this->getParentFieldName()." = ".$parent_id;
		$row = $this->mysql_fetch_one($sql);
		$data['updates_done'] += $row['updates_done'];
		return $data;
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but returns _ref field insteadl of name
	 */
	public function getActiveListItemsFormattedForImport($sdbip_id = 0, $options = array()) {
		$options = $options + array('name_type' => "ref", 'parent' => $sdbip_id);
		$items = $this->getActiveListItemsFormattedForSelect($options);
		return $items;
	}

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($options = array()) {
		return $this->getActiveObjectsFormattedForImportProcessing($options);
	}

	/**
	 * Get list of active objects ready to filter for import
	 */
	public function getActiveObjectsFormattedForImportProcessing($sdbip_id = 0, $options = array()) {
		$options['name_type'] = "ref";
		$items = $this->getActiveObjectsFormattedForSelect($sdbip_id, $options);
		$data = array();
		foreach($items as $id => $name) {
			$name = trim(strtolower(str_replace(" ", "", $name)));
			$data[$name] = $id;
		}
		return $data;
	}


	public function getImportedRecords($sdbip_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND ".$this->getParentFieldName()." = ".$sdbip_id." 
				AND (".$this->getStatusFieldName()." & ".self::CONVERT_IMPORT.") = ".self::CONVERT_IMPORT;
		$in_progress = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND ".$this->getParentFieldName()." = ".$sdbip_id." 
				AND (".$this->getStatusFieldName()." & ".self::CONVERT_SDBP6ED.") = ".self::CONVERT_SDBP6ED;
		$completed = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		return ($in_progress + $completed);
	}


	public function getImportedResultsRecords($keys) {
		if(count($keys) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." 
					WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $keys).")";
			return $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsSecondaryParentFieldName());
		} else {
			return array();
		}
	}






	/*********************************************************************************************
	 * Results/Finances functions
	 */


	/**
	 * Add Budget per project per time period - called by $this->addObject()
	 */
	private function addBudgetObject($project_id, $time_id, $original, $adjustment, $description) {
		$revised = $original + $adjustment;
		$var = array(
			$this->getResultsOriginalFieldName() => $original,
			$this->getResultsAdjustmentsFieldName() => $adjustment,
			$this->getResultsRevisedFieldName() => $revised,
			$this->getResultsTableField().'_target_description' => $description,
			$this->getResultsActualFieldName() => 0,
			$this->getResultsFieldName("VARIANCE") => $revised,
			$this->getResultsParentFieldName() => $project_id,
			$this->getResultsTimeFieldName() => $time_id,
			$this->getResultsStatusFieldName() => self::ACTIVE,
			self::RESULTS_TABLE_FLD.'_insertuser' => $this->getUserID(),
			self::RESULTS_TABLE_FLD.'_insertdate' => $this->getDateForDB(),
		);
		$sql = "INSERT INTO ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$this->db_insert($sql);
		return array($this->getResultsRevisedFieldName() => $revised, $this->getResultsFieldName("VARIANCE") => $revised);
	}

	/**
	 * Edit Budget per project per time period - called by $this->editObject()
	 */
	private function editBudgetObject($project_id, $time_id, $original, $adjustment, $actual) {
		$revised = $original + $adjustment;
		$variance = $revised - $actual;
		$var = array(
			$this->getResultsOriginalFieldName() => $original,
			$this->getResultsAdjustmentsFieldName() => $adjustment,
			$this->getResultsRevisedFieldName() => $revised,
			$this->getResultsFieldName("VARIANCE") => $variance,
		);
		$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($var)." WHERE ".$this->getResultsParentFieldName()." = $project_id AND ".$this->getResultsTimeFieldName()." = $time_id";
		$this->db_update($sql);
		return array($this->getResultsRevisedFieldName() => $revised, $this->getResultsFieldName("VARIANCE") => $variance);
	}


	/**
	 * Get the total budget per project (single or multiple)
	 * @return Array (id=>budget)
	 */
	private function getSummaryResults($object_id = 0, $sdbip_id = false) {
		$sql = "SELECT ".$this->getResultsParentFieldName()." as parent
				, SUM(".$this->getResultsOriginalFieldName().") as original_total
				, SUM(".$this->getResultsRevisedFieldName().") as revised_total
				, SUM(".$this->getResultsActualFieldName().") as actual_total
				FROM ".$this->getResultsTableName()." PF WHERE ".$this->getResultsActiveStatusSQL("PF", $this->getResultsTableField());
		if(is_array($object_id)) {
			//if the project_id variable is an array: (1) Remove any blanks then (2) implode to a string
			$object_id = $this->removeBlanksFromArray($object_id);
			if(count($object_id) > 0) {
				$sql .= " AND ".$this->getResultsParentFieldName()." IN (".implode(",", $object_id).")";
			} else {
				//if the array of project ids is blank then default to checking for project_id = 0 so as not to break the sql statement
				$sql .= " AND ".$this->getResultsParentFieldName()." = 0";
				$object_id = 0;
			}
		} else {
			//if the project_id variable is not an array, assume it is a single id to be searched for
			//if the project_id variable is not a valid number (id) then default to 0 so as not to break the sql statement
			if(!$this->checkIntRef($object_id)) {
				$object_id = 0;
			}
			$sql .= " AND ".$this->getResultsParentFieldName()." = ".$object_id;
		}
		$sql .= " GROUP BY ".$this->getResultsParentFieldName();
		if(is_array($object_id)) {
			$data = $this->mysql_fetch_all_by_id($sql, "parent");
		} else {
			$data = $this->mysql_fetch_one($sql);
		}
		return $data;
	}


	/**
	 * Get the complete financials per project (single or multiple)
	 */
	public function getRawResults($object_id = 0) {
		$sql = "SELECT * FROM ".$this->getResultsTableName()." PF WHERE ".$this->getResultsActiveStatusSQL("PF", $this->getResultsTableField());
		if(is_array($object_id)) {
			//if the project_id variable is an array: (1) Remove any blanks then (2) implode to a string
			$object_id = $this->removeBlanksFromArray($object_id);
			if(count($object_id) > 0) {
				$sql .= " AND ".$this->getResultsParentFieldName()." IN (".implode(",", $object_id).")";
			} else {
				//if the array of project ids is blank then default to checking for project_id = 0 so as not to break the sql statement
				$sql .= " AND ".$this->getResultsParentFieldName()." = 0";
			}
			$data = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
		} else {
			//if the project_id variable is not an array, assume it is a single id to be searched for
			//if the project_id variable is not a valid number (id) then default to 0 so as not to break the sql statement
			if(!$this->checkIntRef($object_id)) {
				$object_id = 0;
			}
			$sql .= " AND ".$this->getResultsParentFieldName()." = ".$object_id;
			$data = $this->mysql_fetch_all_by_id($sql, $this->getResultsTimeFieldName());
		}
		return $data;
	}


	/**
	 * get results for specific time periods for specific object ids for specific columns for list pages
	 */
	public function getResultsForListPages($objects, $time, $headings, $value_type = "", $format_result_code = true, $limit = "ALL") {
		$displayObject = new SDBP6_DISPLAY();
		if(!is_array($time) || count($time) == 0) {
			$timeObject = new SDBP6_SETUP_TIME();
			$sdbip = $this->getCurrentSDBIPFromSessionData();
			$sdbip_id = $sdbip['id'];
			$time = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, "MONTH");
		}
		$time_ids = array_keys($time);
		$object_ids = array_keys($objects);
		$calctypeObject = new SDBP6_SETUP_CALCTYPE();
		$calc_type_settings = $calctypeObject->getCalcTypeForFinances();

		//get records
		if(count($time_ids) > 0 && count($object_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $object_ids).") AND ".$this->getResultsTimeFieldName()." IN (".implode(",", $time_ids).")";
			$rows = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
		} else {
			$rows = array();
		}
//ASSIST_HELPER::arrPrint($objects);
		foreach($rows as $obj_id => $row) {

			if(isset($target_array)) {
				unset($target_array);
			}
			if(isset($actual_array)) {
				unset($actual_array);
			}
			if(isset($original_target_array)) {
				unset($original_target_array);
			}
			if(isset($target_adjustment_array)) {
				unset($target_adjustment_array);
			}

			$object = $objects[$obj_id];
			$target_type_id = isset($object['target_type_id']) ? $object['target_type_id'] : 0;    //added with hardcoded field name in SDBP6.getMyList()
			$calc_type_id = 0;//isset($object['calc_type_id']) ? $object['calc_type_id'] : 0;	//added with hardcoded field name in SDBP6.getMyList()
			//get calculation type settings - if already called before then get from array otherwise query calc type object
			/*			if(isset($calc_type_settings[$calc_type_id])) {
							$calc_type = $calc_type_settings[$calc_type_id];
						} else {
							$calc_type = $calctypeObject->getCalcTypeOptions($calc_type_id);
							$calc_type_settings[$calc_type_id] = $calc_type;
						}*/
			$calc_type = $calc_type_settings;
			$result_options = $this->getResultOptions();
			foreach($row as $time_id => $result) {
				$t_period = $time[$time_id];
				$target = $result[$this->getResultsRevisedFieldName()];
				$actual = $result[$this->getResultsActualFieldName()];

				$original_target = $result[$this->getResultsOriginalFieldName()];
				$target_adjustment = $result[$this->getResultsAdjustmentsFieldName()];
				foreach($headings as $fld => $head) {
					$h_type = $head['type'];
//					$val = $result[$fld];
					switch($h_type) {
						//generate the display indicating the result
						case "RESULT":
							if($t_period['has_started'] != true) {// || $result[$this->getTableField().'_update']==SDBP6::UPDATE_NOT_STARTED) { - JC decided not to default non-updated KPIs to N/A result
								$kr_result = 0;
							} else {
								$kr_result = $calctypeObject->calculatePeriodResult($calc_type_id, $target, $actual, $result_options);
							}
							$kr_display = $this->createResultDisplay($kr_result, $format_result_code);
							$rows[$obj_id][$time_id][$fld] = $kr_display;
							break;
						//generate a display indicating the status of the UPDATE APPROVE ASSURANCE
						case "STATUS":
							$rows[$obj_id][$time_id][$fld] = $this->generateStatusDisplay($result, $calctypeObject->processZeroTargets($calc_type), $t_period['has_started']);
							break;
						case "COMMENT":
							$rows[$obj_id][$time_id][$fld] = "comment";
							break;
						case "ATTACH":
							$object_type = $this->getMyObjectName();//isset($options['object_type']) ? $options['object_type'] : "X";
							$object_id = $obj_id;//isset($options['object_id']) ? $options['object_id'] : "0";
							$attach_detail = $result[$fld];
							if(strlen($attach_detail) > 0) {
								$attach_detail = $this->decodeAttachmentDataFromDatabase($attach_detail, false);
								$disp = $displayObject->getAttachForDisplayInList($attach_detail, $object_type, $object_id);
							} else {
								$disp = "";
							}
							$rows[$obj_id][$time_id][$fld] = $disp;
							break;
					}
				}
				if(isset($value_type) && is_string($value_type) && strlen($value_type) > 0 && $value_type != 'value_as_captured' && array_key_exists($time_id, $time)) {
					$target_array[$time_id] = $target;
					$actual_array[$time_id] = $actual;

					$original_target_array[$time_id] = $original_target;
					$target_adjustment_array[$time_id] = $target_adjustment;

					$values = $calctypeObject->calculatePTDValues($calc_type_id, $target_array, $actual_array, $result_options);

					$kr_result = $calctypeObject->calculatePeriodResult($calc_type_id, $values['target'], $values['actual'], $result_options);

					$kr_display = $this->createResultDisplay($kr_result, $format_result_code);
					$rows[$obj_id][$time_id][$this->getResultsResultsFieldName()] = $kr_display;

					$rows[$obj_id][$time_id][$this->getResultsRevisedFieldName()] = $values['target'];
					$rows[$obj_id][$time_id][$this->getResultsActualFieldName()] = $values['actual'];

					$values = $calctypeObject->calculatePTDValues($calc_type_id, $original_target_array, $target_adjustment_array);

					$rows[$obj_id][$time_id][$this->getResultsOriginalFieldName()] = $values['target'];
					$rows[$obj_id][$time_id][$this->getResultsAdjustmentsFieldName()] = $values['actual'];
				}

			}
		}

		return $rows;
	}


	/**************************************
	 * FRONT PAGE DASHBOARD FUNCTIONS
	 */

	public function getLoginStats($time, $org, $owner, $future) {
		$data = array('present' => 0, 'future' => 0);
//check for normal kpis - where not assurance or approve
		$sql = "SELECT count(".$this->getIDFieldName().") as count, ".$this->getResultsTimeFieldName()." as time_id
				FROM ".$this->getTableName()." K
				INNER JOIN ".$this->getResultsTableName()."
				  ON ".$this->getIDFieldName()." = ".$this->getResultsIDFieldName()."
				WHERE ".$this->getActiveStatusSQL("K")."
				AND ".$this->getResultsTimeFieldName()." IN (".implode(",", array_keys($time)).")
				AND ".$this->getResultsTableField()."_update = ".SDBP6::UPDATE_NOT_STARTED."
				GROUP BY time_id";
		//AND ".$this->getResultsTableField()."_approve = 0
		//AND ".$this->getResultsTableField()."_assurance = 0
		$rows = $this->mysql_fetch_value_by_id($sql, "time_id", "count");

		foreach($rows as $time_id => $count) {
			if(isset($time[$time_id]['deadline_type']) && strlen($time[$time_id]['deadline_type']) > 0 && isset($data[$time[$time_id]['deadline_type']])) {
				$data[$time[$time_id]['deadline_type']] += $count;
			}
		}
		/*
		//check for assurance
				$fld = "assurance";
				$sql = "SELECT count(".$this->getIDFieldName().") as count, ".$this->getResultsTableField()."_".$fld."_updatedeadline as deadline
						FROM ".$this->getTableName()." K
						INNER JOIN ".$this->getResultsTableName()."
						  ON ".$this->getIDFieldName()." = ".$this->getResultsIDFieldName()."
						WHERE ".$this->getActiveStatusSQL("K")."
						AND (".$this->getResultsTableField()."_".$fld." = ".SDBP6::REVIEW_REJECT." OR ".$this->getResultsTableField()."_".$fld." = ".SDBP6::REVIEW_UPDATED.")
						AND ".$this->getResultsTableField()."_".$fld."_updateuser = '".$this->getUserID()."'
						AND ".$this->getResultsTableField()."_".$fld."_updatedeadline <= DATE('".date("Y-m-d",$future)."')
						AND ".$this->getResultsTableField()."_".$fld."_updatedeadline >= DATE('".date("Y-m-d")."')
						GROUP BY deadline";
				$rows = $this->mysql_fetch_value_by_id($sql, "deadline", "count");

				foreach($rows as $deadline => $count) {
					if(strtotime($deadline)==strtotime(date("Y-m-d"))) {
						$data['present']+=$count;
					} else {
						$data['future']+=$count;
					}
				}


		//check for approve - where not assurance
				$fld = "approve";
				$fld2 = "assurance";
				$sql = "SELECT count(".$this->getIDFieldName().") as count, ".$this->getResultsTableField()."_".$fld."_updatedeadline as deadline
						FROM ".$this->getTableName()." K
						INNER JOIN ".$this->getResultsTableName()."
						  ON ".$this->getIDFieldName()." = ".$this->getResultsIDFieldName()."
						WHERE ".$this->getActiveStatusSQL("K")."
						AND ".$this->getResultsTableField()."_".$fld2." = ".SDBP6::NOT_REVIEWED."
						AND (".$this->getResultsTableField()."_".$fld." = ".SDBP6::REVIEW_REJECT." OR ".$this->getResultsTableField()."_".$fld." = ".SDBP6::REVIEW_UPDATED.")
						AND ".$this->getResultsTableField()."_".$fld."_updateuser = '".$this->getUserID()."'
						AND ".$this->getResultsTableField()."_".$fld."_updatedeadline <= DATE('".date("Y-m-d",$future)."')
						AND ".$this->getResultsTableField()."_".$fld."_updatedeadline >= DATE('".date("Y-m-d")."')
						GROUP BY deadline";
				$rows = $this->mysql_fetch_value_by_id($sql, "deadline", "count");

				foreach($rows as $deadline => $count) {
					if(strtotime($deadline)==strtotime(date("Y-m-d"))) {
						$data['present']+=$count;
					} else {
						$data['future']+=$count;
					}
				}
		*/
		return $data;
	}


	public function generateStatusDisplay($result, $process_zero_targets = false, $time_has_started = false) {
		$status = array();
		$kr_fld = $this->getResultsTableField()."_update";
		if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
			if($result[$this->getResultsRevisedFieldName()] == 0 && !$process_zero_targets) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Update Not Applicable\"", "default");
			} elseif($time_has_started != true) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Update not yet due\"", "light-grey");
			} else {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done", "title=\"Update due but not yet started\"", "red");
			}
		} elseif($result[$kr_fld] == SDBP6::UPDATE_IN_PROGRESS) {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("info", "title=\"Update in progress\"", "orange");
		} else {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done", "title=\"Update Completed\"", "green");
		}
		/*
				$kr_fld = $this->getResultsTableField()."_approve";
				if($result[$kr_fld]==SDBP6::NOT_REVIEWED) {
					if($result[$kr_fld]==SDBP6::UPDATE_NOT_STARTED) {
						$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry","title=\"Approve - No update to review\"","default");
					} else {
						$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry","title=\"Approve - Not yet reviewed\"","orange");
					}
				} elseif($result[$kr_fld]==SDBP6::REVIEW_OK) {
					$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done","title=\"Approve - Update signed off\"","green");
				} else {
					$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done","title=\"Approve - Update rejected\"","red");
				}
				$kr_fld = $this->getResultsTableField()."_assurance";
				if($result[$kr_fld]==SDBP6::NOT_REVIEWED) {
					if($result[$kr_fld]==SDBP6::UPDATE_NOT_STARTED) {
						$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry","title=\"Assurance - No update to review\"","default");
					} else {
						$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry","title=\"Assurance - Not yet reviewed\"","orange");
					}
				} elseif($result[$kr_fld]==SDBP6::REVIEW_OK) {
					$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done","title=\"Assurance - Update signed off\"","green");
				} else {
					$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done","title=\"Assurance - Update rejected\"","red");
				}
		 */
		return "<div style='width:100%; padding-bottom:3px;' class=center>".implode("</div><div style='width:100%; padding-bottom:3px;' class=center>", $status)."</div>";
	}


	/*****************************************************************************************************************************
	 * IMPORT / CONVERSION functions
	 */

	public function getAltModuleTable($modloc) {
		switch($modloc) {
			case "SDBP5B":
				$table = "capital";
				break;
			case "SDBP6":
				$table = SDBP6_PROJECT::TABLE;
				break;
			case "IDP3":
				$table = IDP3_PROJECT::TABLE;
				break;
			default:
				$table = false;
				break;
		}
		return $table;
	}

	public function getAltModuleIDField($modloc, $modref = "") {
		switch($modloc) {
			case "SDBP5B":
				$table = "cap_id";
				break;
			case "SDBP6":
				$table = $this->getIDFieldName();
				break;
			case "IDP3":
				$altObject = new IDP3_PROJECT(0, $modref, true);
				$table = $altObject->getIDFieldName();
				unset($altObject);
				break;
			default:
				$table = false;
				break;
		}
		return $table;
	}

	public function importGetResult($var) {
		$obj_id = $var['obj_id'];
		$sql = "SELECT *, ROUND(".$this->getResultsOriginalFieldName().",2) as ".$this->getResultsOriginalFieldName()." 
				FROM ".$this->getResultsTableName()." 
				WHERE ".$this->getResultsParentFieldName()." = ".$obj_id;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getResultsSecondaryParentFieldName());
		return $rows;
	}

	public function importSaveResult($var) {
		$obj_id = $var['obj_id'];
		//no original target unlike KPIs - all project financials to be managed via financial integration
		$description = $var[$this->getResultsDescriptonFieldName()];
		foreach($description as $time_id => $d) {
			$sql = "UPDATE ".$this->getResultsTableName()." 
					SET ".$this->getResultsDescriptonFieldName()." = '".ASSIST_HELPER::code($d)."'
					WHERE ".$this->getResultsParentFieldName()." = $obj_id
					AND ".$this->getResultsSecondaryParentFieldName()." = $time_id ";
			$this->db_update($sql);
		}
		$result = array("ok", "Results saved successfully.");
		return $result;
	}




	/*
	 *
	 * FUNCTIONS BELONGING TO ABORTED MID-YEAR  CONVERT FUNCTION
		public function getOldRecordsSDBP5B($dbref,$table,$start,$limit) {
			$modloc = "SDBP5B";
			$id_field = $this->getAltModuleIDField($modloc);
			$tblref = $dbref."_".$table;
			$records = array();
			//get object details
			$sql = "SELECT * FROM ".$tblref." LIMIT ".$start.", ".$limit;
			$records = $this->mysql_fetch_all_by_id($sql,$id_field);
			if(count($records)>0) {
				$object_ids = array_keys($records);
				//get areas
				$parent_field = "ca_".str_replace("_","",$id_field);
				$item_field = "ca_listid";
				$sql = "SELECT $item_field, $parent_field FROM ".$tblref."_area  WHERE $parent_field IN (".implode(",",$object_ids).")";
				$area = $this->mysql_fetch_value_by_id($sql,$parent_field,$item_field);
				//get wards
				$parent_field = "cw_".str_replace("_","",$id_field);
				$item_field = "cw_listid";
				$sql = "SELECT $item_field, $parent_field FROM ".$tblref."_wards  WHERE $parent_field IN (".implode(",",$object_ids).")";
				$wards = $this->mysql_fetch_value_by_id($sql,$parent_field,$item_field);
				//get logs
				//get results
					//not required - will come from first update from finteg
				foreach($records as $id => $rec) {
					$records[$id]['capital_area'] = isset($area[$id]) ? $area[$id] : array();
					$records[$id]['capital_wards'] = isset($wards[$id]) ? $wards[$id] : array();
				}
			}
			return $records;
		}












		public function convertRecord($record,$map,$old_id) {
			//convert according to map
				//if type = LIST
					//check list_import_external_links if list item exists
					//if not, create
			//add extra fields required by new table
				//proj_src_type = CONVERT
				//proj_status = ACTIVE + CONVERT1
				//proj_insertuser
				//proj_insertdate
			//save to db
		}

	*/


	/*****************************************************************************************************************************
	 * REPORTING functions [TM]
	 */


	public function getProjectListForGroupByMSCFixedReport($sdbip_id) {
		$fld = $this->getTableField()."_msc_id";
		return $this->getProjectListForGroupByFieldForFixedReport($sdbip_id, $fld);
	}


	public function getProjectListForGroupByDepartmentFixedReport($sdbip_id) {
		$fld = $this->getTableField()."_sub_id";
		return $this->getProjectListForGroupByFieldForFixedReport($sdbip_id, $fld);
	}

	public function getProjectListForGroupByFieldForFixedReport($sdbip_id, $fld) {
		$data = array(
			'lines' => array(),
			'results' => array(),
		);

		$sql = "SELECT * 
				FROM ".$this->getTableName()." P
				WHERE ".$this->getParentFieldName()." = $sdbip_id AND ".$this->getActiveStatusSQL("P");
		$rows = $this->mysql_fetch_all_by_id2($sql, $fld, $this->getIDFieldName());
		$data['lines'] = $rows;
		$ids = array();
		foreach($data['lines'] as $d => $r) {
			foreach($r as $id => $p) {
				$ids[] = $id;
			}
		}
		if(count($ids) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $ids).")";
			$data['results'] = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsSecondaryParentFieldName());

		}


		return $data;
	}


	public function getCopiedListItems($source_sdbip_id, $my_sdbip_id) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_import_external_links 
				WHERE sdbip_id = $my_sdbip_id 
				AND local_type = '".self::OBJECT_TYPE."' 
				AND src_modref = '".$this->getModRef()."_".$source_sdbip_id."'
				AND status = ".SDBP6::ACTIVE;
		return $this->mysql_fetch_value_by_id($sql, "src_id", "local_id");
	}


	/*********************************************************
	 * END
	 */


	public function __destruct() {
		parent::__destruct();
	}

}

?>