<?php
/**
 * To manage the user access of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_USERACCESS extends SDBP6 {

	private $table_name = "_useraccess";

	private $field_names = array(
		'ua_time_second' => "Second |time| Closure",
		'ua_time_third' => "Third |time| Closure",
		'ua_new_create' => "Create |sdbip|",
		'ua_new_activate' => "Activate |sdbip|",
		'ua_manage_time' => "|time| Manager",
		'ua_admin_time' => "|time| Super User",
		'ua_admin_assurance_dept' => "|deptsdbip| |assurance| Super User",
		'ua_admin_assurance_top' => "|topsdbip| |assurance| Super User",
		'ua_admin_auditlog' => "|auditlog| |reports|",
		'ua_admin_dept' => "|deptsdbip| Super User",
		'ua_admin_top' => "|topsdbip| Super User",
		'ua_admin_project' => "|project| Super User",
		'ua_admin_finance' => "|finance| Super User",
		'ua_report_admin' => "|report| Manager",
		'ua_setup' => "Setup",
	);

	private $field_defaults = array(
		'ua_time_second' => 0,
		'ua_time_third' => 0,
		'ua_new_create' => 0,
		'ua_new_activate' => 0,
		'ua_manage_time' => 0,
		'ua_admin_time' => 0,
		'ua_admin_assurance_dept' => 0,
		'ua_admin_assurance_top' => 0,
		'ua_admin_auditlog' => 0,
		'ua_admin_dept' => 0,
		'ua_admin_top' => 0,
		'ua_admin_project' => 0,
		'ua_admin_finance' => 0,
		'ua_report_admin' => 0,
		'ua_setup' => 0,
	);

	private $fields_glossary = array();

	private $contents_bar = array(
		'ua_new' => array("ua_new_create", "ua_new_activate"),
		'ua_manage' => true,
		'ua_admin' => array("ua_admin_time", "ua_admin_assurance_dept", "ua_admin_assurance_top", "ua_admin_auditlog", "ua_admin_dept", "ua_admin_top", "ua_admin_project", "ua_admin_finance"),
		'ua_report' => true,
		'ua_setup' => array("ua_setup"),
	);

	private $useraccess_per_section = array(
		'DEPTKPI' => array('ua_admin_assurance_dept', 'ua_admin_dept'),
		'TOPKPI' => array('ua_admin_assurance_top', 'ua_admin_top'),
		'PROJECT' => array('ua_admin_project'),
		'FINANCE' => array('ua_admin_finance'),
	);

	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct($modref, $autojob, $cmpcode);

		$this->field_names = $this->replaceAllNames($this->field_names);
		$this->fields_glossary = array(
			'ua_time_second' => "User has access to |update| their |kpis| / |projects| until the second |time| closure deadline.",
			'ua_time_third' => "User has access to |update| their |kpis| / |projects| until the third |time| closure deadline.  Enabling this option automatically overrides the second |time| closure option for this user.",
			'ua_new_create' => "User has access to |create| and |confirm| the new |sdbip| within the ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_new"), "|"))." section.  Once the |sdbip| has been |activated| this user access is no longer applicable.",
			'ua_new_activate' => "User has access to |activate| the new |sdbip| within the ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_new"), "|"))." section or to undo previous |activation| of an existing |sdbip| (to allow for the |sdbip| to be rebuilt).",
			'ua_manage_time' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_manage_time"), "|"))." to set reminder and closure dates for the various |times|.",
			'ua_admin_time' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_time"), "|"))." to set reminder and closure dates for the various |times| as well as to reopen previously closed |times|.",
			'ua_admin_assurance_dept' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_assurance_dept"), "|"))." to perform |assurance| on any |deptkpi| regardless of |dir| as well as access the |deptsdbip| |auditlog| |report|.",
			'ua_admin_assurance_top' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_assurance_top"), "|"))." to performance |assurance| on any |topkpi| regardless of |dir| as well as access the |topsdbip| |auditlog| |report|.",
			'ua_admin_auditlog' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_auditlog"), "|"))." to draw |auditlog| |reports|. Users who have |deptsdbip| |admin| or |topsdbip| |admin| or |assurance| access are automatically granted access to the |auditlog| |reports| for their relevant section.",
			'ua_admin_dept' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_dept"), "|"))." to |update|, |edit|, |create|, |delete| and |restore| |deptkpis| regardless of |dir|.  Users are also able to amend |targets|, |targettypes| and |calculationtypes| within this section.",
			'ua_admin_top' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_top"), "|"))." to |update|, |edit|, |create|, |delete| and |restore| |topkpis|.  Users are also able to amend |targets|, |targettypes| and |calculationtypes| within this section.",
			'ua_admin_project' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_projects"), "|"))." to |update|, |edit|, |create|, |delete| and |restore| |projects|.",
			'ua_admin_finance' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_admin_finance"), "|"))." upload finances (if automatic integration has not been implemented).",
			'ua_report_admin' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_report_admin"), "|"))." to manage the |menu_report_fixed| settings.",
			'ua_setup' => "User has access to ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_setup"), "|"))." to manage the module settings.",
		);
		$this->fields_glossary = $this->replaceAllNames($this->fields_glossary);
		$this->table_name = $this->getDBRef().$this->table_name;

		//check for active sections to remove unnecessary items
		$active_sections = $this->active_sections;//from SDBP6_HELPER
		if(is_array($this->useraccess_per_section) && count($this->useraccess_per_section)) {
			foreach($this->useraccess_per_section as $u_key => $u_sections) {
				if(!in_array($u_key, $active_sections)) {
					foreach($u_sections as $fld) {
						unset($this->field_names[$fld]);
						unset($this->fields_glossary[$fld]);
						unset($this->field_defaults[$fld]);
					}
				}
			}
		}

	}

	/**
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		if(!isset($var['ua_status'])) {
			$var['ua_status'] = self::ACTIVE;
		}
		if(!isset($var['ua_insertuser'])) {
			$var['ua_insertuser'] = $this->getUserID();
		}
		if(!isset($var['ua_insertdate'])) {
			$var['ua_insertdate'] = date("Y-m-d H:i:s");
		}
		return $this->addUser($var);
		//return array("info","No changes were found to be saved.");
	}

	public function editObject($var) {
		$id = $var['ua_id'];
		unset($var['ua_id']);
		return $this->editUser($id, $var);
	}


	/**
	 * GET functions
	 */
	public function getUserAccessFields() {
		return $this->field_names;
	}

	public function getUserAccessDefaults() {
		return $this->field_defaults;
	}

	public function getTableName() {
		return $this->table_name;
	}

	public function getFieldGlossary() {
		return $this->fields_glossary;
	}

	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccess() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");

	}

	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccessWithEmails() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "id");

	}

	/**
	 * Get the list of users with access
	 */
	public function getActiveUsers($ids = "") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, tkemail as email, UA.*
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql .= " AND TK.tkid IN ('".implode("','", $ids)."') ";
		} elseif(strlen($ids) > 0) {
			$sql .= " AND TK.tkid = '".$ids."' ";
		}
		$sql .= "
				ORDER BY tkname, tksurname"; //echo $sql;
		return $this->mysql_fetch_all_by_id($sql, "tkid");
		//return $sql;
	}

	public function getActiveUsersFormattedForSelect($ids = "") {
		$rows = $this->getActiveUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}

	/**
	 * Get the current user's access
	 */
	public function getMyUserAccess($ui = "") {
		$ui = strlen($ui) == 0 ? $this->getUserID() : $ui;
		$sql = "SELECT UA.* FROM ".$this->getTableName()." UA WHERE ua_tkid = '".$ui."'";
		$row = $this->mysql_fetch_one($sql);
		//if user access hasn't been set then set defaults
		if(!isset($row['ua_status'])) {
			$row = $this->field_defaults;
			$row['ua_status'] = 0;
			$row['ua_tkid'] = $ui;
		}
		//check contents bar settings against user access
		foreach($this->contents_bar as $fld => $access) {
			if($access === true) {
				$row[$fld] = 1;
			} else {
				foreach($access as $f) {
					if(isset($row[$f]) && $row[$f] == true) {
						$row[$fld] = 1;
						break;
					}
				}
			}
		}
		$user_access = array();
		foreach($row as $key => $val) {
			$user_access[substr($key, 3)] = $val;
		}
		return $user_access;
	}

	public function getMyTimePeriodUserAccess() {
		if(isset($_SESSION[$this->getModRef()]['MY_USERACCESS']) && isset($_SESSION[$this->getModRef()]['MY_USERACCESS']['timestamp']) && $_SESSION[$this->getModRef()]['MY_USERACCESS']['timestamp'] > time()) {
			$ua = $_SESSION[$this->getModRef()]['MY_USERACCESS'];
		} else {
			$ua = $this->getMyUserAccess();
			$_SESSION[$this->getModRef()]['MY_USERACCESS'] = $ua;
			$_SESSION[$this->getModRef()]['MY_USERACCESS']['timstamp'] = time() + 5 * 60;
		}
		return $ua;
	}

	function getUsersByTimePeriodType() {
		$users = $this->getActiveUsers();
		$other_users = $this->getUsersWithNoAccessWithEmails();
		$result = array();
		foreach($users as $tkid => $u) {
			if($u['ua_time_second'] == true) {
				$result['second'][$tkid] = $u;
			} elseif($u['ua_time_third'] == true) {
				$result['third'][$tkid] = $u;
			} else {
				$result['primary'][$tkid] = $u;
			}
			if($u['ua_finance'] == true) {
				$result['finance'][$tkid] = $u;
			}
		}
		foreach($other_users as $i => $u) {
			if(!isset($users[$i])) {
				$result['primary'][$i] = $u;
			}
		}
		return $result;
	}

	/**
	 * SET / UPDATE functions
	 */
	/**
	 * Adds a new user
	 */
	private function addUser($var) {
		$result = array("info", "No change was found to be saved.");
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id > 0) {
			$result = array("ok", "User added successfully.");
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Added user access for user: ".$this->getAUserName($var['ua_tkid']),
			);
			$log_var = array(
				'section' => "USER",
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);
		} else {
			/*
			 * */
			$result = array("error", "Sorry, something went wrong while trying to add the user.  Please try again.");
		}
		return $result;
	}


	private function editUser($id, $var) {
		if(ASSIST_HELPER::checkIntRef($id)) {
			$old = $this->mysql_fetch_one("SELECT * FROM ".$this->getTableName()." WHERE ua_id = ".$id);
			//$edits = array();
			//foreach($var as $fld=>$v){
			//	$edits[substr($fld,5)] = $v;
			//}
			$edits = $var;
			$update_data = $this->convertArrayToSQL($edits);
			$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ua_id = ".$id;
			$mar = $this->db_update($sql);
			if($mar > 0) {
				$un = $this->getAUserName($old['ua_tkid']);
				//$user_access_fields = $this->getUserAccessFields();
				$changes = array(
					'user' => $this->getUserName(),
					'response' => "Edited user access for user: ".$un,
				);
				foreach($old as $key => $value) {
					if(isset($edits[$key]) && $value != $edits[$key]) {
						//$changes[$user_access_fields[$key]] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						$name = (str_replace(" ", "_", $this->field_names[$key]));
						$changes[$name] = array('to' => ($edits[$key] == 1 ? "Yes" : "No"), 'from' => ($value == 1 ? "Yes" : "No"));
					}
				}

				$log_var = array(
					'section' => "USER",
					'object_id' => $id,
					'changes' => $changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
				return array("ok", "Changes to ".$un." have been saved successfully.");
			} else {
				return array("info", "No change was found to be saved.");
			}
		}
		return array("error", "An error occurred while trying to save the changes. Please try again.");
	}


	/**
	 * Functions relating to import of prior year SDBIP users
	 */
	public function doesPriorSDBIPModuleExist() {
		if(isset($_SESSION[$this->getModRef()]['PRIOR_VERSION'])) {
			if(is_array($_SESSION[$this->getModRef()]['PRIOR_VERSION']) || $_SESSION[$this->getModRef()]['PRIOR_VERSION'] !== false) {
				return true;
			} else {
				return false;
			}
		} else {
			$sql = "SELECT modtext as name, modlocation as version, modref FROM assist_menu_modules WHERE modlocation = '".$this->prior_module_version."' AND modyn = 'Y' ORDER BY modref DESC LIMIT 1";
			$row = $this->mysql_fetch_one($sql);
			if(count($row) > 0) {
				$_SESSION[$this->getModRef()]['PRIOR_VERSION'] = $row;
				return true;
			} else {
				$_SESSION[$this->getModRef()]['PRIOR_VERSION'] = false;
				return false;
			}
		}

	}

	public function getPriorSDBIPModuleDetails() {
		if($this->doesPriorSDBIPModuleExist() == true) {
			return $_SESSION[$this->getModRef()]['PRIOR_VERSION'];
		} else {
			return false;
		}
	}

	public function getMapOfPriorSDBIPUserAccess() {
		//create map of known SDBP5B user access settings
		$map = array(
			'ua_time_second' => "second",
			'ua_time_third' => "tertiary",
			'ua_admin_assurance_dept' => "assurance",
			'ua_admin_auditlog' => "audit_log",
			'ua_admin_dept' => "kpi",
			'ua_admin_top' => "toplayer",
			'ua_admin_project' => "finance",
			'ua_setup' => "setup",
		);
		//check for any new settings that don't exist in old and set to false
		foreach($this->field_names as $fld => $name) {
			if(!isset($map[$fld])) {
				$map[$fld] = false;
			}
		}
		return $map;
	}

	public function getPriorSDBIPUserAccess($modref) {
		$cmpcode = strtolower($this->getCmpCode());
		$sql = "SELECT TK.tkid as user_id, CONCAT(TK.tkname, ' ', TK.tksurname) as user_name, UA.*
				FROM assist_".$cmpcode."_menu_modules_users mmu
				INNER JOIN ".$this->getUserTableName()." TK
				    ON mmu.usrtkid = TK.tkid
				INNER JOIN assist_".$cmpcode."_".strtolower($modref)."_user_access UA
				    ON UA.tkid = TK.tkid
				WHERE mmu.usrmodref = '$modref' AND TK.tkstatus = 1 AND UA.active = 1
				ORDER BY TK.tkname, TK.tksurname, TK.tkid";
		$rows = $this->mysql_fetch_all_by_id($sql, "user_id");
		return $rows;
	}

	public function saveMultiplePriorModuleUsers($var) {
		//get the current module's menu details - for adding users to the module if needed
		$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".strtoupper($this->getModRef())."'";
		$menu = $this->mysql_fetch_one($sql);
		//get all users who already have menu access
		$sql = "SELECT usrtkid FROM assist_".strtolower($this->getCmpcode())."_menu_modules_users WHERE usrmodref = '".strtoupper($this->getModRef())."'";
		$menu_users = $this->mysql_fetch_all_by_value($sql, "usrtkid");
		//process user data
		$users_added = 0;
		$users_failed = 0;
		$changes = array();
		$user_ids = $var['tkid'];
		foreach($user_ids as $user_id) {
			//check that user hasn't been previously saved through "save one" button
			if(strlen($user_id) > 0) {
				//convert data into format expected by addObject function - this function will handle the addition of users to the module
				$insert_data = array('ua_tkid' => $user_id);
				foreach($this->field_names as $fld => $name) {
					//if third time = yes then set second time to no
					if($fld == "ua_time_second" && isset($var['ui_time_third'][$user_id]) && $var['ui_time_third'][$user_id] == 1) {
						$insert_data[$fld] = 0;
					} else {
						$insert_data[$fld] = $var[$fld][$user_id];
					}
				}
				$r = $this->addObject($insert_data);
				//once added, now grant the user menu access to the module if they don't have it already
				if($r[0] == "ok") {
					$users_added++;
					if(!in_array($user_id, $menu_users)) {
						$sql = "INSERT INTO assist_".strtolower($this->getCmpcode())."_menu_modules_users SET usrmodid = '".$menu['modid']."', usrtkid = '".$user_id."', usrmodref = '".strtoupper($this->getModRef())."'";
						$this->db_insert($sql);
						//LOG - code copied from TK/inc_access.php
						$changes[] = array(
							'user_id' => $user_id,
							'type' => "G",
							'fld' => "mmu",
							'new' => $this->getModRef(),
							'old' => "",
							'txt' => "Granted access to ".$menu['modtext'].".",
							'sql' => $sql
						);
					}
				} else {
					$users_failed++;
				}
			}
		}
		if(count($changes) > 0) {
			foreach($changes as $c) {
				$sql2 = "INSERT INTO assist_".strtolower($this->getCmpcode())."_timekeep_log
						(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
						VALUES
						(now(),'0000','Assist Administrator','MENU','".$c['user_id']."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($c['sql'])."')";
				$this->db_insert($sql2);
			}
		}
		if($users_added > 0) {
			return array("ok", $users_added." users were added successfully.".($users_failed > 0 ? " However, ".$users_failed." users were NOT added successfully." : ""), $users_failed);
		} else {
			return array("error", "An error appears to have occurred while trying to add the users and no users were addedd successfully.  Please try again.");
		}
	}


	public function saveSinglePriorModuleUser($var) {        //get the current module's menu details - for adding users to the module if needed
		$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".strtoupper($this->getModRef())."'";
		$menu = $this->mysql_fetch_one($sql);
		//get all users who already have menu access
		$sql = "SELECT usrtkid FROM assist_".strtolower($this->getCmpcode())."_menu_modules_users WHERE usrmodref = '".strtoupper($this->getModRef())."'";
		$menu_users = $this->mysql_fetch_all_by_value($sql, "usrtkid");
		//process user data
		$users_added = 0;
		$users_failed = 0;
		$changes = array();
		$user_id = $var['tkid'];

		//convert data into format expected by addObject function - this function will handle the addition of users to the module
		$insert_data = array('ua_tkid' => $user_id);
		foreach($this->field_names as $fld => $name) {
			//if third time = yes then set second time to no
			if($fld == "ua_time_second" && isset($var['ui_time_third_'.$user_id]) && $var['ui_time_third_'.$user_id] == 1) {
				$insert_data[$fld] = 0;
			} else {
				$insert_data[$fld] = $var[$fld."_".$user_id];
			}
		}
		$r = $this->addObject($insert_data);
		//once added, now grant the user menu access to the module if they don't have it already
		if($r[0] == "ok") {
			$users_added++;
			if(!in_array($user_id, $menu_users)) {
				$sql = "INSERT INTO assist_".strtolower($this->getCmpcode())."_menu_modules_users SET usrmodid = '".$menu['modid']."', usrtkid = '".$user_id."', usrmodref = '".strtoupper($this->getModRef())."'";
				$this->db_insert($sql);
				//LOG - code copied from TK/inc_access.php
				$changes[] = array(
					'type' => "G",
					'fld' => "mmu",
					'new' => $this->getModRef(),
					'old' => "",
					'txt' => "Granted access to ".$menu['modtext'].".",
					'sql' => $sql
				);
			}
		} else {
			$users_failed++;
		}

		if(count($changes) > 0) {
			foreach($changes as $c) {
				$sql2 = "INSERT INTO assist_".strtolower($this->getCmpcode())."_timekeep_log
						(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
						VALUES
						(now(),'0000','Assist Administrator','MENU','".$user_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($c['sql'])."')";
				$this->db_insert($sql2);
			}
		}
		if($users_added > 0) {
			return array("ok", "User added successfully.", $users_failed);
		} else {
			return array("error", "An error appears to have occurred while trying to add the users and the user was not addedd successfully.  Please try again.");
		}


	}


}


?>