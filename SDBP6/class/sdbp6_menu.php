<?php
/**
 * To manage the Menu items
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_MENU extends SDBP6 {

	private $additional_headings = array(
		'menu' => "Menu",
		'headings' => "Headings",
		'lists' => "Lists",
		'import' => "|import|",
		'orgstructure' => "|orgstructure|",
		'results' => "|resultssetting|",
		'columns' => "List Columns",
		'admins' => "|".SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL."|",
		'targettypes' => "|targettypes|",
		'calctype' => "|calculationtypes|",
		'preferences' => "Module Preferences",
		'link' => "Link",
		'exception' => "Exception Report",
		'force' => "Force Update",
		'history' => "History"
	);
	private $modref = "";
	const MODLOCATION = "SDBP6";

	const TABLE = "setup_menu";
	/*************
	 * CONSTANTS
	 */

	/**
	 * Can a heading be renamed by the client
	 */
	const CAN_RENAME = 16;
	/**
	 * Is a heading the name of an object
	 */
	const OBJECT_HEADING = 32;


	public function __construct($modref = "") {
		parent::__construct($modref);
		$this->modref = $modref;
	}


	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editNames($section, $var);
		if($mar > 0) {
			if($section == "object_names") {
				return array("ok", "Object names updated successfully.");
			} else {
				return array("ok", "Menu headings updated successfully.");
			}
		} else {
			return array("info", "No changes were found to be saved.");
		}
		return array("error", $mar);
	}


	/*******************
	 * GET functions
	 * */
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	/**
	 * Get the options for the Contents bar
	 */
	public function getContentsBar() {
		$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_link as section, CONCAT('".self::MODLOCATION."/',menu_link,'.php') as link
                 FROM ".$this->getTableName()."
                 WHERE menu_parent_id = 0
                 AND (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                 AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                 ORDER BY menu_order";
		return $this->mysql_fetch_all_by_id($sql, "section");
	}

	/**
	 * Get the navigation buttons & breadcrumbs based on the current page
	 *
	 * @param (String) page = file name excluding extention and folder hierarchy e.g. manage_update_action
	 */
	public function getPageMenu($page, $path = "") {
		$userObject = new SDBP6_USERACCESS($this->modref);
		$useraccess = $userObject->getMyUserAccess();
		$arr = explode("_", $page);
		$menu = array('buttons' => array(), 'breadcrumbs' => array());
		$check = array($arr[0]);
		$c = $arr[0];
		unset($arr[0]);
		foreach($arr as $a) {
			$c .= "_".$a;
			$check[] = $c;
		}
//ASSIST_HELPER::arrPrint($check);
		if(count($check) == 1 && in_array($check[0], array("new", "admin"))) {
			$user_checks = array();
			foreach($useraccess as $key => $u) {
				if(substr($key, 0, (strlen($check[0]) + 1)) == $check[0]."_") {
					if($u != true) {
						$user_checks[] = "AND menu_link <> '$key' ";
					}
				}
			}
			if($check[0] == "new" && $useraccess['new_create'] != true) {
				$user_checks[] = "AND menu_link <> 'new_create' ";
				$user_checks[] = "AND menu_link <> 'new_top' ";
				$user_checks[] = "AND menu_link <> 'new_dept' ";
				$user_checks[] = "AND menu_link <> 'new_confirm' ";
			}
			$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link, IF(menu_link='".$check[1]."',1,0) as active, menu_help as help
                     FROM ".$this->getTableName()."
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[0]."'
                     ".implode("", $user_checks)."
                     ORDER BY menu_order";
			$menu['buttons'][0] = $this->mysql_fetch_all($sql);
		}

		//buttons 1
		if(isset($check[1])) {
			$user_checks = array();
			if($check[0] == "admin") {
				foreach($useraccess as $key => $u) {
					if(substr($key, 0, (strlen($check[0]) + 1)) == $check[0]."_") {
						if($u != true) {
							$user_checks[] = "AND menu_link <> '$key' ";
						}
					}
				}
			} elseif($check[0] == "new") {
				foreach($useraccess as $key => $u) {
					if(substr($key, 0, (strlen($check[0]) + 1)) == $check[0]."_") {
						if($u != true) {
							$user_checks[] = "AND menu_link <> '$key' ";
						}
					}
				}
				if($useraccess['new_create'] != true) {
					$user_checks[] = "AND menu_link <> 'new_create' ";
					$user_checks[] = "AND menu_link <> 'new_top' ";
					$user_checks[] = "AND menu_link <> 'new_dept' ";
					$user_checks[] = "AND menu_link <> 'new_confirm' ";
				}
			} elseif($check[0] == "manage" && $useraccess['manage_time'] != true) {
				$user_checks[] = "AND menu_link <> 'manage_time'";
			}

			$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link, IF(menu_link='".$check[1]."',1,0) as active, menu_help as help
                     FROM ".$this->getTableName()."
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[0]."'
					 ".implode("", $user_checks)."
                     ORDER BY menu_order";
			$menu['buttons'][1] = $this->mysql_fetch_all($sql);
		}
		//buttons 2
		if(isset($check[2])) {
//AA-148 - hide Projects & Finance menu items temporarily [JC]
			$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link , IF(menu_link='".$check[2]."',1,0) as active, menu_help as help
                     FROM ".$this->getTableName()."
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[1]."'
                     AND menu_link NOT IN ('report_graphs_projects','report_graphs_finances','admin_dept_exception','admin_top_exception','admin_top_link','report_generate_finances')
                     ORDER BY menu_order";
			$menu['buttons'][2] = $this->mysql_fetch_all($sql);
			if($check[2] == "setup_defaults_headings") {
//Hide Admins > Report page temporarily [JC]
				$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link , IF(menu_link='".$check[3]."',1,0) as active, menu_help as help
	                     FROM ".$this->getTableName()."
	                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
	                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
	                     AND menu_section = '".$check[2]."'
 AND menu_link <> 'setup_defaults_admins_report'
	                     ORDER BY menu_order";
				$menu['buttons'][2] = $this->mysql_fetch_all($sql);
			}
		}
		if(isset($check[3])) {
			//check for special buttons for Setup > Defaults > Admins
			if($check[3] == "setup_sdbip_defaults_admins") {
//Hide Admins > Report page temporarily [JC]
				$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link , IF(menu_link='".$check[4]."',1,0) as active, menu_help as help
	                     FROM ".$this->getTableName()."
	                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
	                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
	                     AND menu_section = '".$check[3]."'
 AND menu_link <> 'setup_sdbip_defaults_admins_report'
	                     ORDER BY menu_order";
				$menu['buttons'][2] = $this->mysql_fetch_all($sql);
			}
		}
		//buttons 3
		if((isset($check[4]) && $check[3] != "setup_defaults_admins") || (isset($check[3]) && $check[2] != "setup_defaults_headings" && $check[2] !== "admin_top_auto")) {
			$user_checks = array();
			if($check[1] == "new_create" && isset($_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['NEW_OBJECT']['details'])) {
				$sdbip_details = $_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['NEW_OBJECT']['details'];
				if(!isset($sdbip_details['idp_module']) || $sdbip_details['idp_module'] == "FALSE_LINK") {
					$user_checks[] = " AND menu_link <> 'new_create_projects_idp' ";
					$user_checks[] = " AND menu_link <> 'new_create_top_idp' ";
				}
				if(!isset($sdbip_details['alt_module']) || $sdbip_details['alt_module'] == "FALSE_LINK" || $this->checkIntRef($sdbip_details['alt_module'])) {
					$user_checks[] = " AND menu_link <> 'new_create_projects_sdbip' ";
					$user_checks[] = " AND menu_link <> 'new_create_top_sdbip' ";
					$user_checks[] = " AND menu_link <> 'new_create_dept_sdbip' ";
				}
				if(!isset($sdbip_details['alt_module']) || $sdbip_details['alt_module'] == "FALSE_LINK" || !$this->checkIntRef($sdbip_details['alt_module'])) {
					$user_checks[] = " AND menu_link <> 'new_create_projects_sdbp6' ";
					$user_checks[] = " AND menu_link <> 'new_create_top_sdbp6' ";
					$user_checks[] = " AND menu_link <> 'new_create_dept_sdbp6' ";
				}
			}
			$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link , IF(menu_link='".$check[3]."',1,0) as active, menu_help as help
                     FROM ".$this->getTableName()."
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[2]."'
                     ".implode("", $user_checks)."
                     ORDER BY menu_order";
			$menu['buttons'][3] = $this->mysql_fetch_all($sql);
		} elseif(isset($check[3]) && $check[2] == "admin_top_auto") {
			//AA-264 - manually set menu items to save the DB changes so that it can go live as a hotfix
			$menu['buttons'][3] = array(
				array('id' => 0, 'name' => "Link", 'section' => "admin_top_auto", 'link' => "admin_top_auto_link.php", 'active' => ($check[3] == "admin_top_auto_link" ? 1 : 0), 'help' => ""),
				array('id' => 1, 'name' => "Exception Report", 'section' => "admin_top_auto", 'link' => "admin_top_auto_exception.php", 'active' => ($check[3] == "admin_top_auto_exception" ? 1 : 0), 'help' => ""),
				array('id' => 2, 'name' => "Force Update", 'section' => "admin_top_auto", 'link' => "admin_top_auto_force.php", 'active' => ($check[3] == "admin_top_auto_force" ? 1 : 0), 'help' => ""),
				array('id' => 3, 'name' => "Auto Update History", 'section' => "admin_top_auto", 'link' => "admin_top_auto_history.php", 'active' => ($check[3] == "admin_top_auto_history" ? 1 : 0), 'help' => ""),
			);
		}
		//breadcrumbs
		if(count($check) > 0) {
			$sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path) > 0 ? "'".$path."'," : "")."menu_link,'.php') as link
                     FROM ".$this->getTableName()."
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_link IN ('".implode("','", $check)."')
                     ORDER BY menu_link, menu_order";
			$menu['breadcrumbs'] = $this->mysql_fetch_value_by_id($sql, "link", "name");
		}

		if(count($check) > count($menu['breadcrumbs'])) {
			foreach($check as $link) {
				if(!isset($menu['breadcrumbs'][$link.'.php'])) {
					$x = explode("_", $link);
					$l = end($x);
					$b = $this->checkAdditionalBreadcrumbs($l);
					if($b !== false) {
						//Hack to accommodate special case of Setup > Defaults > Admins / Headings
						if(isset($check[2]) && ($check[2] == "setup_defaults_admins" || $check[2] == "setup_defaults_headings")) {
							$k = array_keys($menu['breadcrumbs']);
							$z = $menu['breadcrumbs'][end($k)];
							unset($menu['breadcrumbs'][end($k)]);
							$menu['breadcrumbs'][$link.'.php'] = $b;
							$menu['breadcrumbs'][end($k)] = $z;
						} elseif(isset($check[2]) && $check[2] == "setup_defaults_lists" && isset($check[3]) && $check[3] == "setup_defaults_lists_import" && isset($_REQUEST['list'])) {
							$menu['breadcrumbs'][$link.'.php?l='.$_REQUEST['list']] = $b;
						} else {
							$menu['breadcrumbs'][] = $b;
						}
					} else {
						//echo "<p>".$l;
					}
				}
			}
		}
		//Process menu array to replace any |object| placeholders with valid object_names
		$menu = $this->replaceAllNames($menu);
		return $menu;
	}

	/**
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections = array()) {
		$sql = "SELECT menu_id as id, menu_section as section, menu_default as mdefault, menu_client as client
				FROM ".$this->getTableName()."
				WHERE (menu_status & ".SDBP6_MENU::OBJECT_HEADING." = ".SDBP6_MENU::OBJECT_HEADING.")"
			.(count($sections) > 0 ? " AND menu_section IN ('".implode("','", $sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql, "section");
		return $rows;
		//return array($sql);
	}

	/**
	 * Check for any additional sub headings
	 */
	public function checkAdditionalBreadcrumbs($b) {
		if(isset($this->additional_headings[$b])) {
			$a = $this->additional_headings[$b];
			if(substr_count($a, "_")) {
				$headingObject = new SDBP6_HEADINGS();
				return $headingObject->getAHeadingNameByField($a);
			}
			return $a;
		}
		return false;
	}

	/**
	 * Get a single menu heading based on field - NOT FOR OBJECT NAMES
	 *
	 * @param (String) field = the field name of the menu heading to be found.
	 */
	public function getAMenuNameByField($field) {
		$sql = "SELECT IF(LENGTH(menu_client)>0,menu_client,menu_default) as name FROM ".$this->getTableName()." WHERE menu_link = '".$field."'";
		return $this->mysql_fetch_one_value($sql, "name");
	}

	/**
	 * Get the menu names from the table for saving logs changes in Setup
	 */
	public function getOriginalMenuNamesFromDB($sections = array()) {
		$sql = "SELECT menu_id as id, menu_link as section, menu_default as mdefault, menu_client as client
				FROM ".$this->getTableName()."
				WHERE (menu_status & ".SDBP6_MENU::OBJECT_HEADING." <> ".SDBP6_MENU::OBJECT_HEADING.")"
			.(count($sections) > 0 ? " AND menu_link IN ('".implode("','", $sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql, "section");
		return $rows;
	}

	/**
	 * Get the menu names from the table for displaying logs changes
	 */
	public function getDefaultMenuNamesForLog($sections = array()) {
		$sql = "SELECT menu_section as section, menu_default as mdefault
				FROM ".$this->getTableName()."
				WHERE (menu_status & ".SDBP6_MENU::OBJECT_HEADING." <> ".SDBP6_MENU::OBJECT_HEADING.")"
			.(count($sections) > 0 ? " AND menu_section IN ('".implode("','", $sections)."')" : "");
		$rows = $this->mysql_fetch_value_by_id($sql, "section", "mdefault");
		return $rows;
	}

	/**
	 * Get all renameable menu items for editing in Setup
	 */
	public function getRenameableMenuFromDB() {
		$sql = "SELECT
				menu.menu_id as id,
				menu.menu_link as section,
				menu.menu_default as mdefault,
				menu.menu_client as client ,
				IF(LENGTH(menu.menu_client)=0,menu.menu_default,menu.menu_client) as display_text,
				menu.menu_parent_id as parent_id,
				menu.menu_order,
				sub_parent.menu_id as sub_parent_menu_id,
				sub_parent.menu_order as sub_parent_menu_order,
				sub_parent.menu_default as sub_parent_mdefault,
				parent.menu_id as parent_menu_id,
				parent.menu_order as parent_menu_order,
				parent.menu_default as parent_mdefault,
				IF((menu.menu_status & ".self::CAN_RENAME.") = ".self::CAN_RENAME.",1,0) as can_rename
				FROM ".$this->getTableName()." as menu
				LEFT JOIN ".$this->getTableName()." as sub_parent
				  ON sub_parent.menu_id = menu.menu_parent_id
				LEFT JOIN ".$this->getTableName()." as parent
				  ON sub_parent.menu_parent_id = parent.menu_id
				WHERE ( (menu.menu_status & ".self::OBJECT_HEADING.") <> ".self::OBJECT_HEADING.")
				  AND ( (menu.menu_status & ".self::ACTIVE.") = ".self::ACTIVE.")
				ORDER BY parent.menu_order, sub_parent.menu_order, menu.menu_order";
		$rows = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id", true);
		$data = $this->replaceAllNames($rows);
		return $data;
	}






	/****************************************
	 * SET / UPDATE Functions
	 */


	/**
	 * Function to edit object name / menu heading
	 */
	private function editNames($menu_section, $var) {
		if($menu_section == "object_names") {
			$original_names = $this->getObjectNamesFromDB(array_keys($var));
			$log_section = "OBJECT";
			$field = "menu_section";
		} else {
			$original_names = $this->getOriginalMenuNamesFromDB(array_keys($var));
			$log_section = "MENU";
			$field = "menu_link";
		}
		$mar = 0;
		$sql_start = "UPDATE ".$this->getTableName()." SET menu_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
		foreach($var as $section => $val) {
			$m = 0;
			$to = (strlen($val) > 0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to != $from) {
				$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m = $this->db_update($sql);
				if($m > 0) {
					$key = "|menu_".$section."|";
					$c[$key] = array('to' => $to, 'from' => $from);
					$mar += $m;
				}
			}
		}
		if($mar > 0 && count($c) > 0) {
			$changes = array_merge(array('user' => $this->getUserName()), $c);
			/*foreach($var as $section => $val) {
				if($mar[$section]>0) {
					if($menu_section=="object_name") {
						$s = explode("_",$section);
						$changes['|'.end($s).'|'] = array('to'=>$val,'from'=>$original_names[$section]['client']);
					} else {
						$changes[$section] = array('to'=>$val,'from'=>$original_names[$section]['client']);
					}
				}
			}*/
			$log_var = array(
				'section' => $log_section,
				'object_id' => $original_names[$section]['id'],
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
		}

		return $mar;
		//return serialize(array_keys($var))." => ".serialize($original_names);
	}


	/**
	 * Function to echo the navigation buttons and breadcrumbs onscreen
	 */
	public function drawPageTop($menu, $display_navigation_buttons = true, $display_sdbip_name = false, $sdbip_name = "") {
		/**********
		 * Navigation buttons
		 */
		$active_button_label = "";
		if(!isset($display_navigation_buttons) || $display_navigation_buttons !== false) {
			foreach($menu['buttons'] as $level => $buttons) {
				//$helper->arrPrint($buttons);
				echo $this->generateNavigationButtons($buttons, $level);
				if($level == 1) {
					foreach($buttons as $b) {
						if($b['active'] == true) {
							$active_button_label = $b['name'];
							break;
						}
					}
				}
			}
		}
		//marktime("navigation buttons");
		/**********
		 * Breadcrumbs
		 */
		$breadcrumbs = array();
		if($display_sdbip_name && strlen($sdbip_name) > 0) {
			$breadcrumbs[] = $sdbip_name;
		}
		foreach($menu['breadcrumbs'] as $link => $text) {
			if(substr_count($link, ".php") > 0) {
				$breadcrumbs[] = "<a href='".$link."' class=breadcrumb>".$text."</a>";
			} else {
				$breadcrumbs[] = "".$text."";
			}
		}
		echo "<h1 id=h1_page_heading>".implode(" ".self::BREADCRUMB_DIVIDER." ", $this->replaceAllNames($breadcrumbs))."</h1>";
		return $active_button_label;
	}








	/**********************************************
	 * PROTECTED functions: functions available for use in class heirarchy
	 */


	/***********************
	 * PRIVATE functions: functions only for use within the class
	 */


	public function __destruct() {
		parent::__destruct();
	}


}


?>