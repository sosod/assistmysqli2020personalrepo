<?php

class SDBP6_FINANCE_PROJECTS extends ASSIST_MODULE_HELPER {


	public function __construct($cc = "", $mr = "") {
		parent::__construct("client", $cc, false, $mr);
	}

	public function updateProjects($dbref = "", $fin_ref = "") {
		$dbref = strlen($dbref) == 0 ? $this->getDBRef() : $dbref;
		$result2 = "";
//get list of project_id & fin_ref
		$sql = "SELECT proj_id as id, proj_finref as fin_ref FROM ".$dbref."_project WHERE (proj_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE;
		$projects = $this->mysql_fetch_value_by_id($sql, "fin_ref", "id");
		//get sum of financials per month per project - excluding revenue, assets & liabilities (but including asset acquisitions)
		$sql = "SELECT pc_client_guid as project , fin_time_id as time_id , SUM(fin_budget) as budget , SUM(fin_actual) as actual
								FROM ".$dbref."_financials_lines 
								INNER JOIN ".$dbref."_financials_lines_results
								ON line_id = fin_line_id
								WHERE line_fin_year = '".$fin_ref."'
								AND item_prefix NOT IN ('IR','IA','IL')
								GROUP BY pc_client_guid, fin_time_id";
		$financials = $this->mysql_fetch_all_by_id2($sql, "project", "time_id");
//					ASSIST_HELPER::arrPrint($projects);
//					ASSIST_HELPER::arrPrint($financials);
		//update projects
		$projects_processed = 0;
		foreach($projects as $fin_ref => $project_id) {
			if(isset($financials[$fin_ref])) {
				$project_financials = $financials[$fin_ref];
				foreach($project_financials as $time_id => $fin) {
					$variance = $fin['budget'] - $fin['actual'];
					$perc_spent = $fin['budget'] > 0 ? ($fin['actual'] / $fin['budget']) * 100 : 100;
					$sql = "UPDATE ".$dbref."_project_finances 
										SET pf_revised = '".$fin['budget']."'
											, pf_actual = '".$fin['actual']."'
											, pf_variance = ".$variance."
											, pf_perc_spent = ".$perc_spent."
										WHERE pf_proj_id = ".$project_id." AND pf_time_id = ".$time_id;
					$this->db_update($sql);
				}
				unset($financials[$fin_ref]);
				$projects_processed++;
			}
		}
		$result2 .= "<p class=green>".count($financials)." projects found in financials; ".count($projects)." projects found in SDBIP; ".$projects_processed." projects updated;</p>";
		//record "unspecified" data
		if(count($financials) > 0) {
			$result2 .= "<p class=red>".count($financials)." projects not found in local SDBIP - recorded as Unspecified Project;</p>";
			$sql = "DELETE FROM ".$dbref."_project_finances WHERE pf_proj_id = 0";
			$this->db_update($sql);
			//sum
			$insert_data = array();
			foreach($financials as $fin) {
				foreach($fin as $time_id => $f) {
					if(!isset($insert_data[$time_id]['budget'])) {
						$insert_data[$time_id]['budget'] = 0;
					}
					$insert_data[$time_id]['budget'] += $f['budget'];
					if(!isset($insert_data[$time_id]['actual'])) {
						$insert_data[$time_id]['actual'] = 0;
					}
					$insert_data[$time_id]['actual'] += $f['actual'];
				}
			}
			//insert
			foreach($insert_data as $time_id => $f) {
				$b = $f['budget'];
				$a = $f['actual'];
				$v = $b - $a;
				$ps = $b > 0 ? ($a / $b) * 100 : 100;
				$sql = "INSERT INTO ".$dbref."_project_finances 
									SET pf_proj_id = 0, pf_time_id = $time_id 
									, pf_original = $b , pf_revised = $b , pf_actual = $a 
									, pf_variance = $v , pf_perc_spent = $ps 
									, pf_status = ".SDBP6::ACTIVE." , pf_insertuser = 'AUTO' , pf_insertdate = now()";
				$this->db_update($sql);
			}
		}
		return $result2;
	}


}

?>