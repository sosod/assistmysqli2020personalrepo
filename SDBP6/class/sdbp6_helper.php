<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_HELPER extends ASSIST_MODULE_HELPER {

	private $default_object_type = "DEPTKPI";


	protected $mscoa_mod_ref;
	protected $mscoa_mod_code;


	private $my_attachment_download_link = "inc_attachment_controller.php";
	private $my_attachment_download_options = "action=GET_ATTACH";
	private $my_attachment_delete_link = "inc_attachment_controller.php";
	private $my_attachment_delete_options = "action=DELETE_ATTACH";
	private $my_attachment_delete_ajax = true;
	private $my_attachment_delete_function = "doDeleteAttachment";


	protected $object_names;
	protected $default_object_names = array();
	protected $object_name_menu_id = array();
	protected $default_menu_names = array();


	protected $filter_what = array(
		'all' => "All",
		'op' => "Operational Project",
		'cap' => "Capital Project",
		'top' => "Strategic",
		'!top' => "Operational"
	);
	protected $filter_display = array(
		'compact' => "Summary Columns",
		'list' => "List Columns",
		'all' => "All Columns"
	);
	protected $filter_names = array(
		'year' => "SDBIP",
		'who' => "Who",
		'what' => "What",
		'when' => "When",
		'display' => "Display",
	);

	protected $active_sections;
	protected $is_full_module = true;

	protected $module_time;

	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		parent::__construct("client", $cmpcode, $autojob, $modref);
		//Added to facilitate testing of different dates for different scenarios [JC] AA-362 18 June 2020
		$this->module_time = time(); //strtotime("15 August 2020 15:32:11");//time();
		/**
		 * SET ACTIVE SECTIONS - to allow IKPI2 & SDBP6 module to operate off SDBP6 code [AA-338] JC 19 Feb 2020
		 */
		if($autojob == false) {
			if(strlen($modref) == 0) {
				$modref = $this->getModRef();
			}
			$local_modref = $modref;
			if(isset($_SESSION[$local_modref]['active_sections']) && count($_SESSION[$local_modref]['active_sections']) > 0) {
				$active_sections = $_SESSION[$local_modref]['active_sections'];
				$is_full_module = $_SESSION[$local_modref]['is_full_module'];
			} else {
				if(substr($local_modref, 0, 1) == "S") {
					//IF modref START WITH s THEN ASSUME FULL MODULE WITH ALL SECTIONS
					$active_sections = array(SDBP6_DEPTKPI::OBJECT_TYPE, SDBP6_TOPKPI::OBJECT_TYPE, SDBP6_PROJECT::OBJECT_TYPE, SDBP6_FINANCE::OBJECT_TYPE);
					$is_full_module = true;
				} else {
					//ELSE ASSUME LIMITED MODULE WITH ONLY THE DEPTKPI SECTION
					$active_sections = array(SDBP6_DEPTKPI::OBJECT_TYPE);
					$is_full_module = false;
				}
				$_SESSION[$local_modref]['active_sections'] = $active_sections;
				$_SESSION[$local_modref]['is_full_module'] = $is_full_module;
			}
			$this->active_sections = $active_sections;
			$this->is_full_module = $is_full_module;
			if(!$this->is_full_module) {
				unset($this->filter_what['op']);
				unset($this->filter_what['cap']);
				unset($this->filter_what['top']);
			}
		} else {
			if(strlen($modref) == 0) {
				$modref = $this->getModRef();
			}
			$local_modref = $modref;
			if(substr($local_modref, 0, 1) == "S") {
				//IF modref START WITH s THEN ASSUME FULL MODULE WITH ALL SECTIONS
				$active_sections = array(SDBP6_DEPTKPI::OBJECT_TYPE, SDBP6_TOPKPI::OBJECT_TYPE, SDBP6_PROJECT::OBJECT_TYPE, SDBP6_FINANCE::OBJECT_TYPE);
				$is_full_module = true;
			} else {
				//ELSE ASSUME LIMITED MODULE WITH ONLY THE DEPTKPI SECTION
				$active_sections = array(SDBP6_DEPTKPI::OBJECT_TYPE);
				$is_full_module = false;
			}
			$this->active_sections = $active_sections;
			$this->is_full_module = $is_full_module;
		}

		$this->checkDefaultNames(true, true, true, $modref, $autojob, $cmpcode);
//		$this->checkDefaultObjectNames($modref,$autojob,$cmpcode);
//		$this->checkDefaultActivityNames($modref,$autojob,$cmpcode);
//		$this->checkDefaultMenuNames($modref,$autojob,$cmpcode);

		$this->checkObjectNames($modref, false, $autojob, $cmpcode);
		$this->checkActivityNames($modref, false, $autojob, $cmpcode);
		$this->checkMenuNames($modref, false, $autojob, $cmpcode);
		if($autojob !== true) {
			$this->checkMSCOANames($modref, false, $autojob, $cmpcode);
		}


		$this->setAttachmentDownloadOptions();
		$this->setAttachmentDeleteOptions();
		$this->setHelperAttachmentDownloadLink($this->getAttachmentDownloadLink());
		$this->setHelperAttachmentDeleteLink($this->getAttachmentDeleteLink());
		$this->setHelperAttachmentDeleteByAjax($this->getAttachmentDeleteByAjax());
		$this->setHelperAttachmentDeleteFunction($this->getAttachmentDeleteFunction());

		//SET MSCOA DB REF  - assume mod_Ref = "MSCOA" [JC decision taken on 19 March 2018]
		$this->mscoa_mod_ref = $this->getMSCOAModRefForIDP3AndSDBP6();
		$this->mscoa_mod_code = $this->getMSCOAModCodeForIDP3AndSDBP6();


	}

	//Because ASSIST_HELPER->saveEcho didn't want to work #AA-166
	public function saveDataToFile($full_path, $file_name, $str_data) {
		$file = fopen($full_path.$file_name, "w");
		fwrite($file, $str_data."\n");
		fclose($file);
	}

	public function getActiveSections() {
		return $this->active_sections;
	}

	public function isFullModule() {
		return $this->is_full_module;
	}


	function getFileDate($fn) {
		$fdate = $this->strFn("substr", $fn, 0, 8);
		$d = $this->strFn("substr", $fdate, 6, 2);
		$m = $this->strFn("substr", $fdate, 4, 2);
		$y = $this->strFn("substr", $fdate, 0, 4);
		$ftime = $this->strFn("substr", $fn, 9, 6);
		$H = $this->strFn("substr", $ftime, 0, 2);
		$M = $this->strFn("substr", $ftime, 2, 2);
		$s = $this->strFn("substr", $ftime, 4, 2);
		return mktime($H, $M, $s, $m, $d, $y);
	}


	function strFn($fn, $str, $var1, $var2) {
//    $ret = "strFn-";
		switch($fn) {
			case "explode":
				return explode($var1, $str);
				break;
			case "implode":
				return implode($var1, $str);
				break;
			case "strpos":
				return strpos($str, $var1);
				break;
			case "substr":
				return substr($str, $var1, $var2);
				break;
			case "str_replace":
				return str_replace($var1, $var2, $str);
				break;
		}
	}


	/* MOVED TO CENTRAL ASSIST_MODULE_HELPER AS REQUIRED BY IDP MODULE - JC AA-407 1 June 2020
	 * protected function isThisAnExtraAutoCompleteFieldName($fld) {
		$f = explode("_", $fld);
		if(is_array($f) && count($f) > 0 && $f[0] == "auto") {
			return true;
		}
		return false;
	}*/

	protected function checkDefaultNames($check_object = true, $check_activity = true, $check_menu = true, $modref = "", $autojob = false, $cmpcode = "") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		//OBJECT NAMES
		if($check_object) {
			if(isset($_SESSION[$modref]['DEFAULT_NAMES_TIMESTAMP']) && $_SESSION[$modref]['DEFAULT_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['DEFAULT_NAMES'])) {
				$this->default_object_names = $_SESSION[$modref]['DEFAULT_NAMES'];
			} else {
				$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
				$this->default_object_names = $nameObject->fetchDefaultObjectNames();
				$_SESSION[$modref]['DEFAULT_NAMES'] = $this->default_object_names;
				$_SESSION[$modref]['DEFAULT_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
				$this->setDefaultObjectNames($this->default_object_names);
			}
		}
		//ACTIVITY NAMES
		if($check_activity) {
			if(isset($_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES_TIMESTAMP']) && $_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES'])) {
				$this->default_object_names = $_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES'];
			} else {
				if(!isset($nameObject)) {
					$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
				}
				$this->default_activity_names = $nameObject->fetchDefaultActivityNames();
				$_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES'] = $this->default_activity_names;
				$_SESSION[$modref]['DEFAULT_ACTIVITY_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
				$this->setDefaultActivityNames($this->default_activity_names);
			}
		}
		//MENU NAMES
		if($check_menu) {
			if(isset($_SESSION[$modref]['DEFAULT_MENU_NAMES_TIMESTAMP']) && $_SESSION[$modref]['DEFAULT_MENU_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['DEFAULT_MENU_NAMES'])) {
				$this->default_object_names = $_SESSION[$modref]['DEFAULT_MENU_NAMES'];
			} else {
				if(!isset($nameObject)) {
					$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
				}
				$this->default_menu_names = $nameObject->fetchDefaultMenuNames();
				$_SESSION[$modref]['DEFAULT_MENU_NAMES'] = $this->default_menu_names;
				$_SESSION[$modref]['DEFAULT_MENU_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
				$this->setDefaultObjectNames($this->default_menu_names);
			}
		}
	}

	protected function checkDefaultObjectNames($modref = "", $autojob = false, $cmpcode = "") {
		$this->checkDefaultNames(true, false, false, $modref, $autojob, $cmpcode);
	}

	protected function checkDefaultActivityNames($modref = "", $autojob = false, $cmpcode = "") {
		$this->checkDefaultNames(false, true, false, $modref, $autojob, $cmpcode);
	}

	protected function checkDefaultMenuNames($modref = "", $autojob = false, $cmpcode = "") {
		$this->checkDefaultNames(false, false, true, $modref, $autojob, $cmpcode);
	}


	/*********************************
	 * ATTACHMENT FUNCTIONS
	 */


	protected function setAttachmentDownloadOptions($action = "GET_ATTACH", $add_to_default = true) {
		if($action == "") {    //reset to default
			$this->my_attachment_download_options = "action=GET_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_download_options .= "&action=".$action;
		} else {
			$this->my_attachment_download_options = $action;
		}
		$this->setHelperAttachmentDownloadOptions($this->my_attachment_download_options);
	}

	protected function setAttachmentDeleteOptions($action = "", $add_to_default = true) {
		if($action == "") {    //reset to default
			$this->my_attachment_delete_options = "action=DELETE_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_delete_options .= "&action=".$action;
		} else {
			$this->my_attachment_delete_options = $action;
		}
		$this->setHelperAttachmentDeleteOptions($this->my_attachment_delete_options);
	}

	public function getAttachmentDeleteOptions() {
		return $this->my_attachment_delete_options;
	}

	public function getAttachmentDeleteLink() {
		return $this->my_attachment_delete_link;
	}

	public function getAttachmentDownloadOptions() {
		return $this->my_attachment_download_options;
	}

	public function getAttachmentDownloadLink() {
		return $this->my_attachment_download_link;
	}

	public function getAttachmentDeleteFolder($full = false) {
		return $this->getModRef()."/deleted";
	}

	public function getAttachmentImportFolder($full = false) {
		return $this->getModRef()."/import";
	}

	public function getAttachmentDeleteByAjax() {
		return $this->my_attachment_delete_ajax;
	}

	public function getAttachmentDeleteFunction() {
		return $this->my_attachment_delete_function;
	}

	public function getAttachmentFieldName() {
		return $this->attachment_field;
	}


	public function getStorageFolder() {
		return strtoupper($this->getModRef())."/".strtolower($this->getMyObjectType());
	}

	public function getFullFolderPath() {
		$folder = $this->getParentFolderPath()."/".$this->getStorageFolder();
		return $folder;
	}


	public function getDeletedFolder() {
		$folder = strtoupper($this->getModRef())."/deleted";
		return $folder;
	}


	public function getParentFolderPath() {
		$folder = "";
		$location = explode("/", $_SERVER["REQUEST_URI"]);
		$l = count($location) - 2;
		for($f = 0; $f < $l; $f++) {
			$folder .= "../";
		}
		$folder .= "files/".$this->getCmpCode();
		return $folder;
	}

	public function getDeletedFileName($object_type, $sys) {
		return $object_type."_".date("YmdHis")."_".$sys;
	}


	/********************************
	 * Attachment processing functions for objects
	 */


	public function codeAttachmentDataForDatabase($a) {
		return str_replace('"', '|', serialize($a));
	}

	public function decodeAttachmentDataFromDatabase($a, $unserial = true) { //echo "<hr /><h1 class='green'>".$a."</h1>";
		if($unserial) {
			$data = unserialize(str_replace('|', '"', $a));
//		ASSIST_HELPER::arrPrint($data);
		} else {
			$data = str_replace('|', '"', $a);
//			ASSIST_HELPER::arrPrint(unserialize($data));
		}
		if($unserial == true && is_array($data) && count($data) > 0) {
			$c = 0;
			$result = array();
			foreach($data as $i => $d) {
				if(is_null($i) || strlen($i) == 0) {
					while(isset($data[$c])) {
						$c++;
					}
					$i = $c;
				}
				$result[$i] = $d;
				$c++;
			}
			return $result;
		} else {
			return $data;
		}
	}

	/**
	 * Function to save attachments in the database
	 */
	public function saveAttachments($activity, $object_id, $attach) {
		$attach = serialize($attach);
		//$attach = base64_encode($attach);
		switch($activity) {
			case "ADD":
			case "EDIT":
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getAttachmentFieldName()." = '".($attach)."' WHERE ".$this->getIDFieldName()." = ".$object_id;
				break;
			case "UPDATE":
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getUpdateAttachmentFieldName()." = '".($attach)."' WHERE ".$this->getIDFieldName()." = ".$object_id;
				break;
		}
		$mnr = $this->db_update($sql);
		return $mnr;
	}

	/**
	 * Function to get attachments from a specific object or object's update
	 */
	public function getAttachmentDetails($object_id, $i = "all", $activity = "") { //echo $activity;
		//$data = array("object_id"=>$object_id,"i"=>$i,"activity"=>$activity);
		if($activity == "UPDATE" || $activity == "CHILD" || $activity == "RESULTS") {
			$o = explode("-", $object_id);
			if(is_array($o) && count($o) == 2) {
				$object_id = $o[0];
				$time_id = $o[1];
				if(ASSIST_HELPER::checkIntRef($time_id)) {
					return $this->getAttachmentDetailsFromResults($object_id, $time_id, $i);
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return $this->getAttachmentDetailsFromObject($object_id, $i);
		}
	}

	/**
	 * Function to get attachments from a specific object
	 */
	public function getAttachmentDetailsFromObject($object_id, $i = "all") {
		$attach_field = $this->getAttachmentFieldName();
		$sql = "SELECT ".$attach_field." as a FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$a = $this->mysql_fetch_one_value($sql, "a");
		$attach = $this->processAttachmentsForDetails($a, $i);
		return $attach;
	}

	/**
	 * Function to get attachments from a specific object
	 */
	public function getAttachmentDetailsFromResults($object_id, $time_id, $i = "all") {
		$attach_field = $this->getResultsAttachmentFieldName();
		$sql = "SELECT ".$attach_field." as a FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." = ".$object_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
		$a = $this->mysql_fetch_one_value($sql, "a");
		$attach = $this->processAttachmentsForDetails($a, $i);
		return $attach;
	}

	/**
	 * function to convert raw attachments details from db into expected layout for ui
	 */
	protected function processAttachmentsForDetails($a, $i = "all") {
		if($a !== false) {
			if(strlen($a) == 0) {
				$attach = array();
			} else {
				if(substr($a, 0, 2) != "a:") {
					$attach = $this->decodeAttachmentDataFromDatabase(base64_decode($a));
					//$attach=array();
				} else {
//				$attach = unserialize($a);
					//$attach=array();
					$attach = $this->decodeAttachmentDataFromDatabase($a);
				}
			}
			if($i == "all") {
				return $attach;
			} else {
				return $attach[$i];
			}
		} else {
			return false;
		}
	}


	public function deleteAttachment($object_id, $i, $activity = "") {
		//get original attachment details
		$attach = $this->getAttachmentDetails($object_id, "all", $activity);
		$obj = $this->getRawObject($object_id);
		//update $i attachment status to deleted
		$old = $attach[$i];
		$attach[$i]['status'] = SDBP6::DELETED;
		$attach[$i]['deleted_location'] = $this->getDeletedFolder();
		$attach[$i]['deleted_filename'] = $this->getDeletedFileName($this->getMyObjectType(), $old['system_filename']);
		//update table
		$mnr = $this->saveAttachments($activity, $object_id, $attach);
		if($mnr > 0) {
			//activity log
			$changes = array(
				'user' => $this->getUserName(),
				'contract_attachment' => "|attachment| ".$old['original_filename']." was removed.",
			);
			$log_var = array(
				'object_id' => $object_id,
				'changes' => $changes,
				'log_type' => ($activity == "UPDATE" ? SDBP6_LOG::UPDATE : SDBP6_LOG::EDIT),
				'progress' => $obj[$this->getProgressFieldName()],
				'status_id' => $obj[$this->getProgressStatusFieldName()],
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok", $this->getObjectName("attachment")." ".$old['original_filename']." successfully removed.");
		} else {
			return array("error", "An error occurred while trying to ".$this->getActivityName("delete")." ".$this->getObjectName("attachment").".  Please reload the page and try again.");
		}
	}





































	/*************************************
	 * NAMING functions
	 */
	//Function to force name update after changes made in Setup
	public function forceNameUpdate() {
		$this->checkObjectNames("", true);
		$this->checkActivityNames("", true);
		$this->checkMenuNames("", true);
		$this->checkMSCOANames("", true);
	}

	protected function checkObjectNames($modref = "", $force = false, $autojob = false, $cmpcode = "") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(!$force && isset($_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP']) && $_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['OBJECT_NAMES'])) {
			$on = $_SESSION[$modref]['OBJECT_NAMES'];
		} else {
			$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
			$on = $nameObject->fetchObjectNames();
			$_SESSION[$modref]['OBJECT_NAMES'] = $on;
			$_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setObjectNames($on);
	}

	protected function checkActivityNames($modref = "", $force = false, $autojob = false, $cmpcode = "") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(!$force && isset($_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP']) && $_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['ACTIVITY_NAMES'])) {
			$an = $_SESSION[$modref]['ACTIVITY_NAMES'];
		} else {
			$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
			$an = $nameObject->fetchActivityNames();
			$_SESSION[$modref]['ACTIVITY_NAMES'] = $an;
			$_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setActivityNames($an);
	}

	protected function checkMenuNames($modref = "", $force = false, $autojob = false, $cmpcode = "") { //$force=true;
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(!$force && isset($_SESSION[$modref]['MENU_NAMES_TIMESTAMP']) && $_SESSION[$modref]['MENU_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['MENU_NAMES'])) {
			$an = $_SESSION[$modref]['MENU_NAMES'];
		} else {
			$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
			$an = $nameObject->fetchMenuForNaming($modref, true);
			$_SESSION[$modref]['MENU_NAMES'] = $an;
			$_SESSION[$modref]['MENU_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setAdditionalObjectNames($an);
	}

	protected function checkMSCOANames($modref = "", $force = false, $autojob = false, $cmpcode = "") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(!$force && isset($_SESSION[$modref]['MSCOA_NAMES_TIMESTAMP']) && $_SESSION[$modref]['MSCOA_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['MSCOA_NAMES'])) {
			$an = $_SESSION[$modref]['MSCOA_NAMES'];
		} else {
			$nameObject = new SDBP6_NAMES($modref, $autojob, $cmpcode);
			$an = $nameObject->fetchMSCOAForNaming();
			$_SESSION[$modref]['MSCOA_NAMES'] = $an;
			$_SESSION[$modref]['MSCOA_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setAdditionalObjectNames($an);
	}

	protected function formatRowsForSelect($rows, $name = "name", $strip_line_breaks = false) {
		$data = array();
		foreach($rows as $key => $r) {
			if($strip_line_breaks === true) {
				$data[$key] = $this->cleanUpText($r[$name]);
			} else {
				$data[$key] = $r[$name];
			}
		}
		return $data;
	}





	/******************************************
	 * FUNCTIONS HERE ARE TO PREVENT INFINITE LOOPS!!!!!!
	 */


	/**
	 * Get the object names from the table in easy readable format
	 */
	protected function fetchObjectNames() {
		$names = array();
		$sql = "SELECT menu_section as section, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name
				FROM ".$this->getDBRef()."_setup_menu
				WHERE (menu_status & ".SDBP6_MENU::OBJECT_HEADING." = ".SDBP6_MENU::OBJECT_HEADING.")";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$s = explode("_", $r['section']);
			$names[end($s)] = $r['name'];
		}
		return $names;
	}


	public function getObjectNameMenuID($o) {
		$x = explode("_", $o);
		if(count($x) > 1 && $x[0] == "TEMPLATE") {
			$o = $x[1];
		}
		return $this->object_name_menu_id[strtolower($o)];
	}

	public function getContractObjectName($plural = false) {
		return $this->getObjectName("contract").($plural ? "s" : "");
	}

	public function getDeliverableObjectName($plural = false) {
		return $this->getObjectName("deliverable").($plural ? "s" : "");
	}

	public function getActionObjectName($plural = false) {
		return $this->getObjectName("action").($plural ? "s" : "");
	}

	public function getTemplateObjectName($plural = false) {
		return $this->getObjectName("template").($plural ? "s" : "");
	}

	/**
	 * Function to replace both |object| and |activity| with the current usage
	 *//*
	public function replaceAllNames($arr) {
		$arr = $this->replaceObjectNames($arr);
		$arr = $this->replaceActivityNames($arr);
		return $arr;
	}
    /**
	 * Function to replace |object| with object_name
	 *//*
    public function replaceObjectNames($arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($this->object_names as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($this->object_names as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }

    /**
	 * Function to replace |field| with head[name]
	 *//*
    public function replaceHeadingNames($head,$arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($head as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($head as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }
    */


	public function isDateField($fld) {
		if(strrpos($fld, "date") !== FALSE || strrpos($fld, "reminder") !== FALSE || strrpos($fld, "deadline") !== FALSE || strrpos($fld, "action_on") !== FALSE) {
			return true;
		}
		return false;
	}

	protected function getDateForDB() {
		return date("Y-m-d H:i:s");
	}


	/**
	 * FILTERING
	 */

	public function getYearFilterOptions($force = false) {
		//hard code to bypass creation of sdbip class & speed process up - AA-327 JC 30 March 2020
		if(!$force && isset($_SESSION[$this->getModRef()]['filter_by']['year']['timestamp']) && $_SESSION[$this->getModRef()]['filter_by']['year']['timestamp'] < time()) {
			return $_SESSION[$this->getModRef()]['filter_by']['year']['options'];
		} else {
			$sql = "SELECT sdbip_id, sdbip_name FROM ".$this->getDBRef()."_".SDBP6_SDBIP::TABLE."
			 		WHERE (sdbip_status & ".SDBP6::SDBIP_ACTIVATED.") = ".SDBP6::SDBIP_ACTIVATED." 
			 		AND (sdbip_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." 
			 		ORDER BY sdbip_name";
			$rows = $this->mysql_fetch_value_by_id($sql, "sdbip_id", "sdbip_name");
			$_SESSION[$this->getModRef()]['filter_by']['year']['options'] = $rows;
			$_SESSION[$this->getModRef()]['filter_by']['year']['timestamp'] = time() + 1800;
			return $rows;
		}
	}

	public function getWhatFilterOptions() {
		return $this->filter_what;
	}

	public function getDisplayFilterOptions() {
		return $this->filter_display;
	}

	public function getFilterNames() {
		return $this->filter_names;
	}

	public function setListPageFilter($filter) {
		$_SESSION[$this->getModRef()]['LIST_PAGE_FILTER'] = $filter;
	}

	public function getListPageFilter() {
		if(isset($_SESSION[$this->getModRef()]['LIST_PAGE_FILTER'])) {

			return $_SESSION[$this->getModRef()]['LIST_PAGE_FILTER'];
		} else {
			return array('year' => 0, 'who' => "ALL", 'what' => "ALL");
		}
	}

	public function getWhenFilterOptions($sdbip_id, $time_type = "MONTH") {
		//ASSIST_HELPER::arrPrint($_SESSION[$this->getModRef()]['TIME'][$time_type]);
//		if(isset($_SESSION[$this->getModRef()]['TIME'][$time_type]) && isset($_SESSION[$this->getModRef()]['TIME'][$time_type.'_timestamp']) && ($_SESSION[$this->getModRef()]['TIME'][$time_type.'_timestamp']<time()) && is_array($_SESSION[$this->getModRef()]['TIME'][$time_type]) && count($_SESSION[$this->getModRef()]['TIME'][$time_type])>0) {
		//echo "<h1>session isset</h1>";
//			return $_SESSION[$this->getModRef()]['TIME'][$time_type];
//		} else {
		//echo "<h2 class=red>No session set - going to time for sdbip: ".$sdbip_id."</h2>";
		$timeObject = new SDBP6_SETUP_TIME();
		$time = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, $time_type, 0, false, "ALL");
		$_SESSION[$this->getModRef()]['TIME'][$time_type] = $time;
		$_SESSION[$this->getModRef()]['TIME'][$time_type.'_timestamp'] = time() + 5 * 60;    //make valid for 5 minutes
		return $time;
//		}
	}


	/**
	 * GET RESULT OPTIONS
	 *
	 * Save time - don't create the object if you don't have to
	 * only creates object & checks db every 5 minutes
	 */
	public function getResultOptions($modref = "", $force = false) { //$force=true;
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(!$force && isset($_SESSION[$modref]['RESULT_OPTIONS']) && count($_SESSION[$modref]['RESULT_OPTIONS']) > 1 && isset($_SESSION[$modref]['RESULT_OPTIONS']['timestamp']) && $_SESSION[$modref]['RESULT_OPTIONS']['timestamp'] < time()) {
			$result_options = $_SESSION[$modref]['RESULT_OPTIONS'];
		} else {
			$resultObject = new SDBP6_SETUP_RESULTS($modref);
			$result_options = $resultObject->getMyActiveResultSettings(true);
			$_SESSION[$modref]['RESULT_OPTIONS'] = $result_options;
			$_SESSION[$modref]['RESULT_OPTIONS']['timestamp'] = time() + 5 * 60;
		}
//		ASSIST_HELPER::arrPrint($result_options);
		return $result_options;
	}

	public function createResultDisplay($result_id, $format_result = true) {
		$result_options = $this->getResultOptions();
		if($format_result) {
			$display = "<div class='result result".$result_id."'>".$result_options[$result_id]['code']."</div>";
		} else {
			$display = $result_options[$result_id]['code'];
		}
		return $display;
	}


	/*************************
	 * Module generic functions
	 */


	public function addActivityLog($log_table, $var, $modref = "", $autojob = false, $cmpcode = "") {
		$logObject = new SDBP6_LOG($log_table, $modref, $autojob, $cmpcode);
		$logObject->addObject($var);
	}


	public function logUserPageMovement() {
		$tkid = $this->getUserID();
		$tkname = $this->getUserName();

		$req = serialize($_REQUEST);
		$p = explode("/", $_SERVER['PHP_SELF']);
		unset($p[0]);
		unset($p[1]);
		$page = implode("/", $p);
		//$page = substr($_SERVER['PHP_SELF'],7,strlen($_SERVER['PHP_SELF']));
		$val = "";
		/*if(count($req)>0) {
			$vals = array();
			$keys = array_keys($req);
			foreach($keys as $k) {
				if($k!="PHPSESSID") {
					if(is_array($req[$k])) {
						$v = "array(";
						$v2 = array();
							foreach($req[$k] as $a => $b) {
								$v2[] = $a."=".$b;
							}
						$v.=implode(", ",$v2).")";
					} else {
						$v = $req[$k];
					}
					$vals[] = $k."=".$v;
				}
			}
			$val = code(implode(", ",$vals));
		}*/
		$import_data = array(
			'tkid' => $tkid,
			'tkname' => $tkname,
			'dttoday' => date("Y-m-d H:i:s"),
			'page' => $page,
			'request' => $req
		);
		//$sql = "INSERT INTO ".$this->getDBRef()."_user_log (id,tkid,tkname,dttoday,page,request) VALUES (null,'$tkid','$tkname',now(),'$page','$req')";
		$sql = "INSERT INTO ".$this->getDBRef()."_user_log SET ".$this->convertArrayToSQLForSave($import_data);
		$id = $this->db_insert($sql);
		//echo "<h1>".$id."</h1>";
	}


	/**
	 * Function to generate SQL statement for bitwise status check
	 */
	protected function generateStatusSQL($status, $compare, $t = "", $tbl_field_prefix = "") {
		if(strlen($tbl_field_prefix) == 0) {
			$tbl_field_prefix = $this->getTableField();
		}
		return "( ".(strlen($t) > 0 ? $t."." : "")."".$tbl_field_prefix."_status & ".$status.") ".$compare." ".$status;
	}

	/**
	 * Generate Status SQL statement dependent on page
	 */
	protected function getStatusSQL($section = "REPORT", $t = "", $deleted = false, $is_contract = true, $tbl_field_prefix = "") {
		//AA-484 [JC] - Added status option 2 to allow for OR option
		//0 => statuses to be excluded AND 1 => statuses to be included
		//OR
		//2 => statuses to be included AND 3 => statuses to be excluded
		$statuses = array(0 => array(), 1 => array(), 2 => array());
		if($deleted) {
			$statuses[1][] = SDBP6::DELETED;
			$statuses[0][] = SDBP6::ACTIVE;
		} else {
			$statuses[0][] = SDBP6::DELETED;
			$statuses[1][] = SDBP6::ACTIVE;
		}
		switch($section) {
			case "CONFIRM":
			case "NEW":
			case "ACTIVATE":
			case "ALL":
			case "DELETED":
			case "REPORT":
				break;
			case "ADMIN_EDIT":            //ADDED FOR AA-484 [JC] 5 Oct 2020
				$statuses[1] = array();
				$statuses[0] = array();
				$statuses[2][] = SDBP6::ACTIVE;
				$statuses[2][] = SDBP6::INACTIVE;
				//Removed Deleted status [AA-525] - Delete in NEW = DELETED & never show again; Delete in ADMIN = INACTIVE & make available for restore
//				$statuses[2][] = SDBP6::DELETED;
				break;
			default:
				break;
		}
		$final_sql = array();
		$and_sql = array();  //print_r($statuses);
		foreach($statuses[0] as $not) {
			$and_sql[] = $this->generateStatusSQL($not, "<>", $t, $tbl_field_prefix);
		}
		foreach($statuses[1] as $yes) {
			$and_sql[] = $this->generateStatusSQL($yes, "=", $t, $tbl_field_prefix);
		}
		$or_sql = array();
		foreach($statuses[2] as $yes) {
			$or_sql[] = $this->generateStatusSQL($yes, "=", $t, $tbl_field_prefix);
		}
		//ASSIST_HELPER::arrPrint($sql);
		if(count($and_sql) > 0) {
			$final_sql[] = " (".implode(" AND ", $and_sql).") ";
		}
		if(count($or_sql)) {
			$final_sql[] = " (".implode(" OR ", $or_sql).") ";
		}
		if(count($final_sql) > 0) {
			return " (".implode(" AND ", $final_sql).") ";
		} else {
			return "";
		}
	}

	protected function getInactiveStatusSQLForReport($section = "REPORT", $t = "", $deleted = true, $is_contract = true, $tbl_field_prefix = "") {
		$statuses = array(0 => array(), 1 => array());
		if($deleted) {
			$statuses[1][] = SDBP6::INACTIVE;
			$statuses[0][] = SDBP6::ACTIVE;
		} else {
			$statuses[0][] = SDBP6::INACTIVE;
			$statuses[1][] = SDBP6::ACTIVE;
		}
		switch($section) {
			case "CONFIRM":
			case "NEW":
				break;
			case "ACTIVATE":
				break;
			case "ALL":
			case "DELETED":
				break;
			case "REPORT":
			default:
				break;
		}
		$sql = array();  //print_r($statuses);
		foreach($statuses[0] as $not) {
			$sql[] = $this->generateStatusSQL($not, "<>", $t, $tbl_field_prefix);
		}
		foreach($statuses[1] as $yes) {
			$sql[] = $this->generateStatusSQL($yes, "=", $t, $tbl_field_prefix);
		}
		//ASSIST_HELPER::arrPrint($sql);
		return " (".implode(" AND ", $sql).") ";
	}

	protected function getInactiveAndDeletedStatusSQLForReport($section = "REPORT", $t = "", $deleted = true, $is_contract = true, $tbl_field_prefix = "") {
		$statuses = array(0 => array(), 1 => array());
		if($deleted) {
			$statuses[1][] = SDBP6::INACTIVE;
			$statuses[1][] = SDBP6::DELETED;
			$statuses[0][] = SDBP6::ACTIVE;
		} else {
			$statuses[0][] = SDBP6::INACTIVE;
			$statuses[0][] = SDBP6::DELETED;
			$statuses[1][] = SDBP6::ACTIVE;
		}
		switch($section) {
			case "CONFIRM":
			case "NEW":
				break;
			case "ACTIVATE":
				break;
			case "ALL":
			case "DELETED":
				break;
			case "REPORT":
			default:
				break;
		}
		$not_equal_to_sql = array();  //print_r($statuses);
		foreach($statuses[0] as $not) {
			$not_equal_to_sql[] = $this->generateStatusSQL($not, "<>", $t, $tbl_field_prefix);
		}
		$equal_to_sql = array();  //print_r($statuses);
		foreach($statuses[1] as $yes) {
			$equal_to_sql[] = $this->generateStatusSQL($yes, "=", $t, $tbl_field_prefix);
		}
		//ASSIST_HELPER::arrPrint($sql);
		return " (".$not_equal_to_sql[0]." AND (".implode(" OR ", $equal_to_sql).")) ";
	}


	public function getNewStatusSQL($t = "") {
		//Contracts where
		//activestatussql and
		//status <> confirmed and
		//status <> activated
		return $this->getStatusSQL("NEW", $t, false);
	}


}

?>