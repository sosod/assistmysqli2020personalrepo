<?php

/**
 * Class SDBP6_ASSIST
 * Used to manage any SDBP6 functions from Action Dashboard or main User Profile
 */
class SDBP6_ASSIST extends ASSIST {
	public function __construct() {
		parent::__construct();
	}


	/**
	 * @param $modref
	 * @param $module_name
	 * @return array
	 */
	public function getActionDashboardUserProfileOptions($modref, $module_name) {

		$sdbipObject = new SDBP6_SDBIP($modref, true);
		$available_sdbips = $sdbipObject->getAllActivatedObjects(false, false, false, "ASC");

		$lines = array();
		foreach($available_sdbips as $sdbip_id => $sdbip) {
			$lines[$sdbip_id] = array(
				'value' => $modref."_".$sdbip_id,
				'text' => $module_name.": ".$sdbip['name'],
				'active' => false,
			);
		}
		return $lines;

	}

}