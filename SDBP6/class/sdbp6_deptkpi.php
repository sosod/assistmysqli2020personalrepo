<?php
/**
 * To manage the Departmental KPI objects of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_DEPTKPI extends SDBP6_KPI {

	protected $object_id = 0;
	protected $object_details = array();
	protected $parent_id = false;

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_sdbip_id";
	protected $secondary_parent_field = "_proj_id";
	protected $tertiary_parent_field = "_top_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $target_type_field = "_unit_id";
	protected $calc_type_field = "_calctype_id";
	protected $owner_field = "_owner_id";
	protected $department_field = "_sub_id";

	protected $results_id_field = "_id";
	protected $results_parent_field = "_kpi_id";
	protected $results_secondary_parent_field = "_time_id";
	protected $results_status_field = "_status";
	protected $results_attachment_field = "_attachment";

	protected $results_fields = array(
		'ORIGINAL' => "_original",
		'ADJUSTMENTS' => "_adjustments",
		'REVISED' => "_revised",
		'ACTUAL' => "_actual",
		'RESULTS' => "_result",
		'DESCRIPTION' => "_target_description",
		'VARIANCE' => null,
	);

	protected $time_type = "MONTH";
	protected $sdbip_id = 0;

	protected $has_attachment = false;
	protected $has_results = true;

	protected $ref_tag = "D";

	protected $list_page_filter_options = array(
		'year' => array(),
		'who' => array('SUB' => "kpi_sub_id", 'OWNER' => "kpi_owner_id"),
		'what' => array(),
		'when' => array(),
		'display' => array(),
	);

	protected $graph_report_filter_fields = array();
	protected $graph_filter_fields_not_allowed = array(
		"_top_id",
		"_proj_id",
		"_ref",
		"_calctype_id",
		"_unit_id",
	);
	protected $extra_graph_filter_fields = array();
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "DEPTKPI";
	const OBJECT_TYPE_PLURAL = "DEPTKPIS";
	const OBJECT_NAME = "DEPTKPI";
	const OBJECT_NAME_PLURAL = "DEPTKPIS";
	const PARENT_OBJECT_TYPE = "SDBIP";
	const CHILD_OBJECT_TYPE = "DEPTKPI_RESULTS";
	const ADMIN_SECTION = "dept";

	const REFTAG = "ERROR/CONST/REFTAG";

	const TABLE = "kpi";
	const TABLE_FLD = "kpi";

	const RESULTS_TABLE = "kpi_results";
	const RESULTS_TABLE_FLD = "kpir";

	const APPROVE_TABLE = "kpi_approve";
	const APPROVE_TABLE_FLD = "ka";

	const ASSURANCE_TABLE = "kpi_assurance";
	const ASSURANCE_TABLE_FLD = "ka";

	const LOG_TABLE = "kpi";


	public function __construct($modref = "", $obj_id = 0, $external_call = false, $sdbip_id = 0) {
		parent::__construct($modref, $external_call);

		if($sdbip_id > 0) {
			$this->parent_id = $sdbip_id;
		}

		$this->external_call = $external_call;
		$this->has_kpi_results = true;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->has_secondary_parent = true;
		$this->tertiary_parent_field = self::TABLE_FLD.$this->tertiary_parent_field;
		$this->has_tertiary_parent = true;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->target_type_field = self::TABLE_FLD.$this->target_type_field;
		$this->calc_type_field = self::TABLE_FLD.$this->calc_type_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		$this->department_field = self::TABLE_FLD.$this->department_field;

		$this->results_id_field = self::RESULTS_TABLE_FLD.$this->results_id_field;
		$this->results_parent_field = self::RESULTS_TABLE_FLD.$this->results_parent_field;
		$this->results_secondary_parent_field = self::RESULTS_TABLE_FLD.$this->results_secondary_parent_field;
		$this->results_status_field = self::RESULTS_TABLE_FLD.$this->results_status_field;
		$this->results_attachment_field = self::RESULTS_TABLE_FLD.$this->results_attachment_field;

		$results_keys = array_keys($this->results_fields);
		foreach($results_keys as $fld) {
			if($this->results_fields[$fld] != null) {
				$this->results_fields[$fld] = self::RESULTS_TABLE_FLD.$this->results_fields[$fld];
			}
		}
		$this->results_fields['ID'] = $this->results_id_field;
		$this->results_fields['PARENT'] = $this->results_parent_field;
		$this->results_fields['SECONDARY_PARENT'] = $this->results_secondary_parent_field;
		$this->results_fields['TIME'] = $this->results_secondary_parent_field;
		$this->results_fields['STATUS'] = $this->results_status_field;
		$this->results_fields['ATTACHMENT'] = $this->results_attachment_field;

		if($obj_id > 0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";

		if(!$external_call) {


			//get time type from parent in SESSION
			$current_sdbip = $this->getCurrentSDBIPFromSessionData();
			if(isset($current_sdbip['deptkpi_time_type'])) {
				$this->time_type = $current_sdbip['deptkpi_time_type'];
			}
			if(isset($current_sdbip['id'])) {
				$this->sdbip_id = $current_sdbip['id'];
			} else {
				$this->sdbip_id = 0;
			}


			//filter page options - WHO section will be generated as required
			$this->list_page_filter_options['year'] = $this->getYearFilterOptions();
			$this->list_page_filter_options['what'] = $this->getWhatFilterOptions();
			$this->list_page_filter_options['when'] = $this->getWhenFilterOptions($this->sdbip_id, $this->time_type);
			$this->list_page_filter_options['display'] = $this->getDisplayFilterOptions();
			$this->list_page_filter_options['filter_names'] = $this->getFilterNames();


			//filter fields for grfaph reporting  $this->getParentFieldName()          => "LIST", (return sdbip_id - for later release in AA-156)
			$this->graph_report_filter_fields = array(
				$this->getDepartmentFieldName() => "LIST",
				$this->getTableField()."_cap_op" => "TRI_BOOL",
				$this->getTableField()."_repcate" => "TRI_BOOL",
				$this->getTertiaryParentFieldName() => "BOOL",);
			if(count($this->graph_filter_fields_not_allowed) > 0) {
				$x = $this->graph_filter_fields_not_allowed;
				$this->graph_filter_fields_not_allowed = array();
				foreach($x as $f) {
					$this->graph_filter_fields_not_allowed[] = $this->getTableField().$f;
				}
			}
//$this->getSecondaryParentFieldName() => "BOOL",
		}

	}


	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d", strtotime($v));
			} else {
				//check for unnecessary auto-complete fields
				if($this->isThisAnExtraAutoCompleteFieldName($key) == true) {
					unset($var[$key]);
				}
			}
		}
		$import_targets = isset($var['import_targets']) ? $var['import_targets'] : false;
		unset($var['import_targets']);
		if(isset($var[$this->getResultsOriginalFieldName()]) && $import_targets === false) {
			$original = $var[$this->getResultsOriginalFieldName()];
			unset($var[$this->getResultsOriginalFieldName()]);
		} else {
			$original = array();
		}
		if(isset($var[$this->getResultsAdjustmentsFieldName()]) && $import_targets === false) {
			$adjustments = $var[$this->getResultsAdjustmentsFieldName()];
			unset($var[$this->getResultsAdjustmentsFieldName()]);
		} else {
			$adjustments = array();
		}
		if(isset($var[$this->getResultsTableField()."_target_description"]) && $import_targets === false) {
			$description = $var[$this->getResultsTableField()."_target_description"];
			unset($var[$this->getResultsTableField()."_target_description"]);
		} else {
			$description = array();
		}

		if(!isset($var[$this->getTableField().'_src_type'])) {
			$var[$this->getTableField().'_src_type'] = self::OBJECT_SRC_MANUAL;
		}
		$import_status = isset($var['import_status']) ? $var['import_status'] : 0;
		unset($var['import_status']);
		$var[$this->getTableField().'_status'] = SDBP6::ACTIVE + $import_status;
		/*
				$var[$this->getTableField().'_status'] = SDBP6::ACTIVE;
				switch($var[$this->getTableField().'_src_type']) {
					case self::OBJECT_SRC_IDP:
					case self::OBJECT_SRC_SDBIP:
						$var[$this->getTableField().'_status'] += self::CONVERT_IMPORT;
						$var[$this->getTableField().'_status']+=self::EXTERNAL_IMPORT;
						break;
				}*/
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		//IF top layer kpi selected then set the "auto update" to true
		if(isset($var[$this->getTableField()."_top_id"]) && ASSIST_HELPER::checkIntRef($var[$this->getTableField()."_top_id"])) {
			$var[$this->getTableField().'_auto_update_top'] = 1;
		}

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);
		if($id > 0) {
			//Add Targets
			if(count($original) == 0) {
				//create blank records
				$timeObject = new SDBP6_SETUP_TIME();
				$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($var[$this->getTableField().'_sdbip_id'], $this->getTimeType());
				$x = ($this->time_type != "MONTH" ? 3 : 1);
				foreach($time_periods as $time_id => $t) {
					if($import_targets !== false && isset($import_targets[$x])) {
						$o = $import_targets[$x];
					} else {
						$o = 0;
					}
					$this->addTargetObject($id, $time_id, $o, 0, "");
					$x += ($this->time_type != "MONTH" ? 3 : 1);
				}
			} else {
				foreach($original as $time_id => $value) {
					$a = isset($adjustments[$time_id]) ? $adjustments[$time_id] : 0;
					$d = isset($description[$time_id]) ? $description[$time_id] : "";
					$this->addTargetObject($id, $time_id, $value, $a, $d);
				}
			}
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$id." |created|.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			$result = array(
				0 => "ok",
				1 => "".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully ".$this->getActivityName("created").".",
				'object_id' => $id,
				'log_var' => $log_var
			);
			return $result;
		}
		return array("error", "An error occurred while trying to save the ".$this->getObjectName($this->getMyObjectName()).".  Please try again or contact your Assist Administrator for assistance.");
	}


	public function editObject($var, $attach_logs = array()) {
		$extra_logs = array();
		/**
		 * first process extra items first and generate any necessary additional logs
		 */
		//get field names
		$original_field_name = $this->getResultsOriginalFieldName();
		$adjustments_field_name = $this->getResultsAdjustmentsFieldName();
		$revised_field_name = $this->getResultsRevisedFieldName();
		$actual_field_name = $this->getResultsActualFieldName();
		$results_field_name = $this->getResultsResultsFieldName();
		$target_description_field_name = $this->getResultsTableField()."_target_description";
		//get values from input
		$originals = $var[$original_field_name];
		$adjustments = $var[$adjustments_field_name];
		$object_id = $var['object_id'];
		$sdbip_id = $var[$this->getParentFieldName()];
		$descriptions = $var[$target_description_field_name];
		//remove results values from input so that not processed in central edit function
		unset($var[$this->getParentFieldName()]);
		unset($var[$original_field_name]);
		unset($var[$adjustments_field_name]);
		unset($var[$target_description_field_name]);
		//get time settings to have names handy for logs
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id);
		//get results headings to have names handy for logs
		$headingObject = new SDBP6_HEADINGS();
		$results_headings = $headingObject->getMainObjectHeadings(self::CHILD_OBJECT_TYPE, "LOGS");
		//fetch existing results
		$old_results = $this->getRawResults($object_id);

		//loop through budgets to get valid time_ids that are open and can be amended
		foreach($originals as $time_id => $new_original) {
			if(strlen($new_original) == 0) {
				$new_original = 0;
			}
			$new_adjustment = isset($adjustments[$time_id]) ? $adjustments[$time_id] : 0;
			if(strlen($new_adjustment) == 0) {
				$new_adjustment = 0;
			}
			$new_description = $descriptions[$time_id];
			//if old budget exists for given time then process as edit otherwise process as add
			if(isset($old_results[$time_id])) {
				//check for changes to edit
				$old = $old_results[$time_id];
				$old_original = $old[$original_field_name];
				$old_adjustments = $old[$adjustments_field_name];
				$old_description = $old[$target_description_field_name];
				//if old != new then record changes
				if($old_original != $new_original || $old_adjustments != $new_adjustment || $old_description != $new_description) {
					//save to DB
					$calculations = $this->editTargetObject($object_id, $time_id, $new_original, $new_adjustment, $old[$actual_field_name], $new_description);
					//add to log
					$n = $new_original;
					$o = $old_original;
					$f = $original_field_name;
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$n = $new_adjustment;
					$o = $old_adjustments;
					$f = $adjustments_field_name;
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$f = $revised_field_name;
					$n = $calculations[$f];
					$o = $old[$f];
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$f = $results_field_name;
					$n = $calculations[$f];
					$o = $old[$f];
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
					$n = $new_description;
					$o = $old_description;
					$f = $target_description_field_name;
					if($o != $n) {
						$extra_logs[$f."_".$time_id] = array('response' => $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id]);
					}
				}
			} else {
				//process as add
				$calculations = $this->addTargetObject($object_id, $time_id, $new_original, $new_adjustment);
				//add to log
				$n = $new_original;
				$o = 0;
				$f = $original_field_name;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$n = $new_adjustment;
				$o = 0;
				$f = $adjustments_field_name;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$f = $revised_field_name;
				$n = $calculations[$f];
				$o = 0;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$f = $results_field_name;
				$n = $calculations[$f];
				$o = 0;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
				$n = $new_description;
				$o = "";
				$f = $target_description_field_name;
				if($o != $n) {
					$extra_logs[$f."_".$time_id] = $results_headings[$f]['name']." changed to '".$n."' from '".$o."' for ".$time_periods[$time_id];
				}
			}
		}


		$result = $this->editMyObject($var, $extra_logs);


		return $result;
	}

	public function saveActivatedObjectForHistory($parent_id, $filename, $path) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = ".$parent_id;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$keys = array_keys($rows);
		$results = $this->getRawResults($keys);
		$files = array($filename);
		$final_path = $this->getFilesLocation($path, $this->getCmpCode());

		//save as serialize TXT file
		$fdata = serialize($rows);
		$f = $filename."_serialize_rows.txt";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$rdata = serialize($results);
		$f = $filename."_serialize_results.txt";
		$this->saveDataToFile($final_path, $f, $rdata);
		$files[] = $f;

		//save as json_encode TXT file
		$fdata = json_encode($rows);
		$f = $filename."_json_rows.txt";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$rdata = json_encode($results);
		$f = $filename."_json_results.txt";
		$this->saveDataToFile($final_path, $f, $rdata);

		//save as CSV file
		$header = array_keys($rows[$keys[0]]);
		$data = array(
			0 => "\"".implode("\",\"", $header)."\"",
		);
		foreach($rows as $row) {
			$data[] = "\"".implode("\",\"", $row)."\"";
		}
		$fdata = implode("\r\n", $data);
		$f = $filename."_rows.csv";
		$this->saveDataToFile($final_path, $f, $fdata);
		$files[] = $f;

		$r_keys = array_keys($results);
		$t_keys = array_keys($results[$r_keys[0]]);
		$header = array_keys($results[$r_keys[0]][$t_keys[0]]);
		$data = array(
			0 => "\"".implode("\",\"", $header)."\"",
		);
		foreach($results as $ri => $row) {
			foreach($row as $ti => $time) {
				$time['parent_object_id'] = $ri;
				$time['parent_time_id'] = $ti;
				$data[] = "\"".implode("\",\"", $time)."\"";
			}
		}
		$rdata = implode("\r\n", $data);
		$f = $filename."_results.csv";
		$this->saveDataToFile($final_path, $f, $rdata);
		$files[] = $f;

		return $files;
	}

	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |deactivated|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Inactive",
				'from' => "Active",
				'raw' => array(
					'to' => self::INACTIVE,
					'from' => self::ACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DEACTIVATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".$this->getActivityName("deactivated").".",
			'object_id' => $id,
		);
		return $result;
	}


	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |deleted|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Deleted",
				'from' => "Active",
				'raw' => array(
					'to' => self::DELETED,
					'from' => self::ACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DELETE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".strtolower($this->getActivityName("deleted")).".",
			'object_id' => $id,
		);
		return $result;
	}


	public function restoreObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$id." |restored|.",
			'user' => $this->getUserName(),
			$this->getStatusFieldName() => array(
				'to' => "Active",
				'from' => "Inactive",
				'raw' => array(
					'to' => self::ACTIVE,
					'from' => self::INACTIVE,
				),
			),
		);
		$log_var = array(
			'object_id' => $id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::RESTORE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array(
			0 => "ok",
			1 => "".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully ".strtolower($this->getActivityName("restored")).".",
			'object_id' => $id,
		);
		return $result;
	}


	/**
	 * RESET all objects relating to specific SDBIP: Called from New > Create > RESET
	 */
	public function resetObject($var, $return_response = false) {
		$parent_id = is_array($var) ? $var['parent_id'] : $var;
		//change all active objects to SDBIP_RESET - use bitwise to catch all active items that are individually created, manually imported or are in process of external import
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::SDBIP_RESET." WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_active = $this->db_update($sql);
		//change all deleted objects to DELETED + SDBIP_RESET
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".(self::SDBIP_RESET + self::DELETED)." WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") = ".self::DELETED." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_deleted = $this->db_update($sql);
		//change all deactivated objects to INACTIVE + SDBIP_RESET
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".(self::SDBIP_RESET + self::INACTIVE)." WHERE (".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE." AND (".$this->getStatusFieldName()." & ".self::SDBIP_RESET.") <> ".self::SDBIP_RESET." AND ".$this->getParentFieldName()." = ".$parent_id;
		$mar_inactive = $this->db_update($sql);
		//remove links if imported
		$sql = "UPDATE ".$this->getDBRef()."_import_external_links SET status = 0 WHERE sdbip_id = ".$parent_id." AND status = ".self::ACTIVE." AND local_type = '".self::OBJECT_TYPE."'";
		$this->db_update($sql);

		if($return_response) {
			$response = $mar_active." active ".strtolower($this->getObjectName($this->getMyObjectName($mar_active == 1 ? false : true)));
			return $response;
		} else {
			$response = "Reset completed successfully.  ".$mar_active." active ".strtolower($this->getObjectName($this->getMyObjectName($mar_active == 1 ? false : true)))." removed.";
			return array("ok", $response);
		}
	}


	/**
	 * Save changes in phase 3 of import
	 */
	public function importPhase3($var) {
		$i = $var['settings'];
		$val = $var['my_val'];
		$type = $var['type'];


		if($type == "RESULT") {

			$val = ASSIST_HELPER::code($val);

			$x = explode("_", $i);
			$time_id = $x[count($x) - 1];
			unset($x[count($x) - 1]);
			$id = $x[count($x) - 1];
			unset($x[count($x) - 1]);
			$field = implode("_", $x);

			if($field == $this->getResultsOriginalFieldName()) {
				$sql = "UPDATE ".$this->getResultsTableName()." SET $field = '".$val."', ".$this->getResultsRevisedFieldName()." = '".$val."' WHERE ".$this->getResultsParentFieldName()." = ".$id." AND ".$this->getResultsSecondaryParentFieldName()." = ".$time_id;

			} else {
				$sql = "UPDATE ".$this->getResultsTableName()." SET $field = '".$val."' WHERE ".$this->getResultsParentFieldName()." = ".$id." AND ".$this->getResultsSecondaryParentFieldName()." = ".$time_id;
			}

			$this->db_update($sql);

			$required_result = "";

		} else {

			if($type != "STATUS_CHECK" && $val !== false) {
				switch($type) {
					case "DATE":
						$v = strtotime($val);
						$val = date("Y-m-d", $v);
						break;
					case "LIST":
						$val = str_replace(",", ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $val);
						break;
					case "TEXT":
					default:
						$val = ASSIST_HELPER::code($val);
						break;
				}

				$x = explode("_", $i);
				$id = $x[count($x) - 1];
				unset($x[count($x) - 1]);
				$field = implode("_", $x);

				$sql = "UPDATE ".$this->getTableName()." SET $field = '".$val."' WHERE ".$this->getIDFieldName()." = ".$id;
				$this->db_update($sql);


				//update required field status
				if(isset($_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'][$field])) {
					$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'][$field]['value'] = $val;
				}
			} else {
				$id = $i;
			}
			//check required field status
			$required = $_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['fields'];
			if(count($required) > 0) {
				$can_signoff = true;
				foreach($required as $fld => $r) {
					switch($r['type']) {
						case "MULTILIST":
						case "MULTIOBJECT":
						case "MULTISEGMENT":
							if(strlen($r['value']) == 0) {
								$can_signoff = false;
							}
							break;
						case "LIST":
						case "OBJECT":
						case "SEGMENT":
							if(strlen($r['value']) == 0 || $r['value'] == "X" || (int)$r['value'] == 0) {
								$can_signoff = false;
							}
							break;
						default:
							if(strlen($r['value']) == 0) {
								$can_signoff = false;
							}
							break;
					}
				}
			} else {
				$can_signoff = true;
			}
			$status = $_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'];
			if((($status & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) && $can_signoff === true) {
				$new_status = ($status - SDBP6::CONVERT_IMPORT + SDBP6::CONVERT_SDBP6ED);
				//set status to complete
				$sql = "UPDATE ".$this->getTableName()." 
						SET ".$this->getStatusFieldName()." = ".$new_status." 
						WHERE ".$this->getIDFieldName()." = ".$id." 
						AND (".$this->getStatusFieldName()." & ".SDBP6::CONVERT_IMPORT.") = ".SDBP6::CONVERT_IMPORT;
				$this->db_update($sql);
				$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'] = $new_status;
				$required_result = ASSIST_HELPER::getDisplayResult(array("ok", "Complete"));
			} elseif((($status & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) && $can_signoff === false) {
				$new_status = ($status + SDBP6::CONVERT_IMPORT - SDBP6::CONVERT_SDBP6ED);
				//set status to incomplete
				$sql = "UPDATE ".$this->getTableName()." 
						SET ".$this->getStatusFieldName()." = ".$new_status." 
						WHERE ".$this->getIDFieldName()." = ".$id." 
						AND (".$this->getStatusFieldName()." & ".SDBP6::CONVERT_SDBP6ED.") = ".SDBP6::CONVERT_SDBP6ED;
				$this->db_update($sql);
				$_SESSION[$this->getModRef()]['IMPORT'][$this->getMyObjectType()][$id]['status'] = $new_status;
				$required_result = ASSIST_HELPER::getDisplayResult(array("info", "In Progress"));
			} elseif((($status & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) && $can_signoff === true) {
				$required_result = ASSIST_HELPER::getDisplayResult(array("ok", "Complete"));
			} else {
				$required_result = ASSIST_HELPER::getDisplayResult(array("info", "In Progress"));
			}

		}
		return array("ok", ASSIST_HELPER::getDisplayIconAsDiv("ok"), $required_result);

	}


	/**
	 * Update Results
	 */
	public function updateObject($var, $attach) {
		$result = array("info", "I'm sorry but I don't know what you want me to do.", $var);

		/* set variables and tidy up $var so that what is left is the form fields */
		unset($var['action']);
		unset($var['page_direct']);
		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);
		$kpi_id = $var['object_id'];
		unset($var['object_id']);
		$time_id = $var['time_id'];
		unset($var['time_id']);
		$target_type_id = isset($var['target_type_id']) ? $var['target_type_id'] : 0;
		unset($var['target_type_id']);
		$update_status = $var[$this->getResultsTableField().'_display_status'];    //convert the STATUS heading from display_status to update field
		$var[$this->getResultsTableField().'_update'] = $update_status;
		unset($var[$this->getResultsTableField().'_display_status']);
		$revised_target = $var[$this->getResultsRevisedFieldName()];
		unset($var[$this->getResultsRevisedFieldName()]);
		$new_attachments = $attach['new']; //$new attachments (for logging)
		$save_attachments = $attach['save']; //attachments for DB savings
		//get headings so that field type is known for logging purposes
		$headingObject = new SDBP6_HEADINGS();
		$result_headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($this->getMyChildObjectType(), "FORM"));
		//Set variables for processing
		$update_data = array();
		$log_changes = array();
		//Get time information - for logging
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $this->getTimeType());
		$time_name = $time_periods[$time_id];
		//Get target type information - for logging
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("", $target_type_id);


		/* GET PREVIOUS UPDATE RECORD FOR LOG COMPARISON */
		$old_var = $this->getRawUpdateObject($kpi_id, $time_id);
		$old_kpi_var = $this->getRawObject($kpi_id, false, false);
		/* PROCESS $var FOR CHANGES */
		foreach($var as $fld => $new_val) {
			if($fld == $this->getResultsTableField()."_update") {
				$heading_field = $this->getResultsTableField()."_display_status";
			} else {
				$heading_field = $fld;
			}
			//check that the field is valid and in the database by making sure that it exists in the old_var record and in the headings
			if(isset($old_var[$fld]) && isset($result_headings[$heading_field])) {
				//get previous value
				$old_val = $old_var[$fld];
				//get headings settings
				$head = $result_headings[$heading_field];
				//check if the value has changed & process changes according to field type - WARNING: TEXT FIELDS ARE NOT CODED AS THE SDBP6Helper(js).processObjectFormWithAttachments DOES NOT CALL ASSISTFORM.serialize
				switch($head['type']) {
					case "NUM":
						if($old_val != $new_val) {
							$update_data[$fld] = $new_val;
							$log_changes[$fld] = array(
								'to' => ($head['apply_formatting'] == true ? $targetTypeObject->formatNumberBasedOnTargetType($new_val) : $new_val),
								'from' => ($head['apply_formatting'] == true ? $targetTypeObject->formatNumberBasedOnTargetType($old_val) : $old_val),
								'raw' => array('to' => $new_val, 'from' => $old_val),
							);
						}
						break;
					case "TEXT":
						$new_val = ASSIST_HELPER::code($new_val);
						if($old_val != $new_val) {
							$update_data[$fld] = $new_val;
							$log_changes[$fld] = array(
								'to' => $new_val,
								'from' => $old_val,
							);
						}
						break;
					case "STATUS":
						$log_field = "Update Status";
						//Convert Bool Y/N "true" to Completed
						if($new_val == true) {
							$new_val = $this->getCompletedUpdateStatus();
						}
						//if new = (completed)
						//if old (completed) then do nothing
						//else if old = (in progress) then to Completed from In Progress
						//else to Completed from New
						//else
						//if old = (new) then to In Progress (2) from New
						//else if old = (completed) then to In Progress (2) from Completed
						//else - do nothing (no change)
						if($this->isStatusCompleted($new_val)) {
							if($this->isStatusCompleted($old_val)) {
								//do nothing - no change
							} elseif($this->isStatusInProgress($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "Completed",
									'from' => "In Progress",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} else {
								//assume old value = New
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "Completed",
									'from' => "New",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							}
							//AA-540 - JC - 17 March 2021
							//Because update has been marked as completed, check if it must be returned to Approve/Assurance reviewer if previously rejected
							//Check if approve status is rejected & if so, change it to returned
							$review_result['approve'] = $this->returnRejectedObject("approve", $kpi_id, $time_id, $old_var, $old_kpi_var);
							//Check if assurance status is rejected & if so, change it to returned
							$review_result['assurance'] = $this->returnRejectedObject("assurance", $kpi_id, $time_id, $old_var, $old_kpi_var);

						} else {
							//assume new is now In Progress
							$new_val = $this->getInProgressUpdateStatus();
							if($this->isStatusCompleted($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "In Progress",
									'from' => "Completed",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} elseif($this->isStatusNew($old_val)) {
								$update_data[$fld] = $new_val;
								$log_changes[$log_field] = array(
									'to' => "In Progress",
									'from' => "New",
									'raw' => array('to' => $new_val, 'from' => $old_val)
								);
							} else {
								//do nothing - both = In Progress
							}
						}
						break;
				}
			}
		}

		/* Check if new attachments are present and need to be updated & logged */
		if(count($new_attachments) > 0) {
			$update_data[$this->getResultsAttachmentFieldName()] = $this->codeAttachmentDataForDatabase($save_attachments);
			$new_attach = array();
			foreach($new_attachments as $i => $na) {
				$new_attach[] = $na['original_filename'];
			}
			$log_changes[$this->getResultsAttachmentFieldName()] = "Added new |".(count($new_attach) == 1 ? "attachment" : "attachments")."|: ".implode("; ", $new_attach);
		}

		/* only save update if there is something to update */
		if(count($log_changes) > 0 || count($update_data) > 0) {
			$update_data[$this->getResultsTableField().'_updateuser'] = $this->getUserID();
			$update_data[$this->getResultsTableField().'_updatedate'] = date("Y-m-d H:i:s");
			/* check for approve/assurance add-ons */
			if(isset($review_result) && is_array($review_result)) {
				foreach($review_result as $review_type => $review) {
					if(isset($review['result']) && $review['result'] === true) {
						foreach($review['update_data'] as $fld => $datum) {
							$update_data[$fld] = $datum;
						}
						foreach($review['log_data'] as $fld => $datum) {
							$log_changes[$fld] = $datum;
						}
					}
				}
			}

			/* Update database */
			$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$this->db_update($sql);

			/* Log changes */
			$changes = array(
					'response' => "|".$this->getMyObjectName()."| ".$this->getRefTag().$kpi_id." |updated| for |time|: $time_name.",
					'user' => $this->getUserName()
				) + $log_changes;
			$log_var = array(
				'object_id' => $kpi_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::UPDATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			/* Approve/Assurance comments here moved to the isStatusCompleted check section as they should only trigger in that situation - AA-540 JC 17 March 2021 */
			//Check if any approve/assurance actions need to be taken
			if(isset($review_result) && is_array($review_result)) {
				foreach($review_result as $review_type => $review) {
					if(isset($review['result']) && $review['result'] === true) {
						//save log
						if(isset($review['review_log']) && strlen($review['review_log']) > 0) {
							$sql = $review['review_log'];
							$this->db_insert($sql);
						}
						//send emails
						if(isset($review['emails']) && is_array($review['emails']) && count($review['emails']) > 0) {
							$to = $review['emails']['to'];
							$subject = $review['emails']['subject'];
							$message = $review['emails']['message'];
							$content_type = $review['emails']['content_type'];
							$cc = $review['emails']['cc'];
							$reply_to = $review['emails']['reply_to'];
							$emailObject = new ASSIST_EMAIL($to, $subject, $message, $content_type, $cc, "", $reply_to);
							$emailObject->sendEmail();
							unset($emailObject);
						}
					}
				}
			}


			/* set result & return */
			$result = array("ok", "Update saved successfully.");//,$update_data);

		} else {
			$result = array("info", "No changes found to be saved.");
		}
		return $result;
	}


	public function deleteAttachment($raw_id, $i, $page_activity = "UPDATE") {
		$result = array("info", "Sorry, I couldn't work out what you wanted me to do.");
		//process object_id to extract time_id
		$o = explode("-", $raw_id);
		$kpi_id = $o[0];
		$time_id = $o[1];
		//Get time information - for logging
		$raw_object = $this->getRawObject($kpi_id);
		$sdbip_id = $raw_object[$this->getParentFieldName()];
		$timeObject = new SDBP6_SETUP_TIME();
		$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $this->getTimeType());
		$time_name = $time_periods[$time_id];
		//get exisitng attachment details
		$old_attach = $this->getAttachmentDetails($raw_id, "all", $page_activity);
//return array("error",serialize($old_attach),$old_attach);
		if(isset($old_attach[$i])) {
			//get the deleted item for logging
			$deleted_attach = $old_attach[$i];
			//get the updated attachment settings for saving to the database
			$new_attach = $old_attach;
			unset($new_attach[$i]);

			//save to db
			$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->getResultsAttachmentFieldName()." = '".$this->codeAttachmentDataForDatabase($new_attach)."' WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$this->db_update($sql);

			//set logs
			$changes = array(
				'user' => $this->getUserName(),
				$this->getResultsAttachmentFieldName() => "|deleted| |attachment| ".$deleted_attach['original_filename']." from |".$this->getMyObjectType()."| ".$this->getRefTag().$kpi_id." for |time|: $time_name.",
			);
			$log_var = array(
				'object_id' => $kpi_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => SDBP6_LOG::UPDATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			//return result
			$result = array("ok", $this->getObjectName("ATTACHMENT")." successfully ".$this->getActivityName("DELETED"));
		} else {
			$result = array("error", "Delete was unsuccessful - attachment $i could not be found.");
		}
		return $result;
	}


	/**
	 * Sign-off / Reject updates to KPIs
	 */
	public function approveObject($var) {
		//get variables
		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);
		$kpi_id = $var['object_id'];
		unset($var['object_id']);
		$time_id = $var['time_id'];
		unset($var['time_id']);
		$emails = isset($var['emails']) && is_array($var['emails']) ? $var['emails'] : array();
		unset($var['emails']);
		$returnuser = isset($var['returnuser']) ? $var['returnuser'] : "";    //needed separately to generate email if required
		$deadline = isset($var['deadline']) ? $var['deadline'] : "";    //needed separately to generate email if required
		$signoff = $var['signoff'];
		$comment = $var['comment'];
		if($signoff == 1) {
			$var['signoff'] = self::REVIEW_OK;
		} else {
			$var['signoff'] = self::REVIEW_REJECT;
		}
		$table_name = $this->getApproveTableName();
		$table_field = $this->getApproveTableField()."_";
		$results_table_name = $this->getResultsTableName();
		$results_table_field = $this->getResultsTableField()."_";
		$object_name = $this->getObjectName($this->getMyObjectType());
		$object_ref = $this->getRefTag().$kpi_id;
		//get time period name
		$timeObject = new SDBP6_SETUP_TIME();
		$time_name = $timeObject->getATimePeriodName($sdbip_id, $time_id, $this->getTimeType());

		//process data for _approve table
		$insert_data = array(
			$table_field.'kpi_id' => $kpi_id,
			$table_field.'time_id' => $time_id,
			$table_field.'insertuser' => $this->getUserID(),
			$table_field.'insertdate' => date("Y-m-d H:i:s"),
		);
//return array("info","insertdata :: ".serialize($var));
		foreach($var as $fld => $val) {
			if($fld == "deadline") {
				$val = date("Y-m-d", strtotime($val));
			}
			$insert_data[$table_field.$fld] = ASSIST_HELPER::code($val);
		}
		//insert into db
		$sql = "INSERT INTO ".$table_name." SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);

		//process changes to results table
		$results_data = array();
		$results_data[$results_table_field.'approve'] = $var['signoff'];
		$results_data[$results_table_field.'approveuser'] = $this->getUserID();
		$results_data[$results_table_field.'approvedate'] = date("Y-m-d H:i:s");
		if($signoff == 0) {
			$results_data[$results_table_field.'approve_updateuser'] = $returnuser;
			$results_data[$results_table_field.'approve_updatedeadline'] = date("Y-m-d", strtotime($deadline));
		}
		//update db
		$sql = "UPDATE ".$results_table_name." SET ".$this->convertArrayToSQLForSave($results_data)." WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
		$this->db_update($sql);

		//add to log
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "Added new |approve| review for |time| ".$time_name.". The update was ".($signoff == 1 ? "" : "not ")."signed-off.",
		);
		$log_var = array(
			'object_id' => $kpi_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::APPROVE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		//send emails if the update was rejected
		if($signoff == 0) {
			$send_email = true;
			$subject = $this->getModTitle()." ".$this->getActivityName("approve")." Review for ".$object_name." ".$object_ref." [$time_name]";
			//get sender information
			$from = $this->getUserNameWithEmail($this->getUserID());
			//cc notify emails - get these first so that if no return user is set these emails can be used as the to instead of CC
			if(count($emails) > 0) {
				//get usernames and emails
				$cc = $this->getUserNameWithEmail($emails);
			}
			//set the wording dependent on whether a return user was selected or not.
			if(strlen($returnuser) > 3) {
				//get user name & email
				$return_user_details = $this->getUserNameWithEmail($returnuser);
				if(count($return_user_details) > 0 && isset($return_user_details['email']) && strlen($return_user_details['email']) > 0) {
					$to = $return_user_details;
				} else {
					$send_email = false;
				}
				//set message to indicate that changes are requested
				$message = "
				<p>".$from['name']." has reviewed your update to ".$this->object_title." ".$object_ref." for time period ".$time_name.".</p>
				<p>The update has <span class='b u'>not</span> been signed-off with the following reason given:<br /><span class=i>".str_replace(chr(10), "<br />", $comment)."</span></p>
				<p>You have until <span class=overdue>".date("d F Y", strtotime($deadline))."</span> to make the required changes.</p>
				<p>&nbsp;</p>
				<p class=i>Reminder: ".$this->getObjectName("KPIS")." which have been returned can only be updated from the Action List on the Frontpage Dashboard.
				<br />If you are unable to find the ".$this->getObjectName("KPI")." on your Action List check your 'Days to Display' setting under your 'My Profile' (Main Menu) to confirm if the deadline falls within the given time frame.</p>
				";
			} else {
				//set cc as recipient
				$to = $cc;
				$cc = "";
				//set message to indicate that no further updates have been requested
				$message = "
				<p>".$from['name']." has reviewed the update to ".$this->object_title." ".$object_ref." for time period ".$time_name.".</p>
				<p>The update has <span class='b u'>not</span> been signed-off with the following reason given:<br /><span class=i>".str_replace(chr(10), "<br />", $comment)."</span></p>
				<p>".$from['name']." has not returned the ".$this->getObjectName("KPI")." update to a user for further updating.";
			}
			//generate email
			$emailObject = new ASSIST_EMAIL($to, $subject, $message, "HTML", $cc, "", $from);
			$emailObject->sendEmail();
		}

		//return
		$result = array("ok", "Review saved successfully.");

		return $result;
	}


	/**
	 * Sign-off / Reject updates to KPIs
	 */
	public function assuranceObject($var) {
		//get variables
		$sdbip_id = $var['sdbip_id'];
		unset($var['sdbip_id']);
		$kpi_id = $var['object_id'];
		unset($var['object_id']);
		$time_id = $var['time_id'];
		unset($var['time_id']);
		$emails = isset($var['emails']) && is_array($var['emails']) ? $var['emails'] : array();
		unset($var['emails']);
		$returnuser = isset($var['returnuser']) ? $var['returnuser'] : "";    //needed separately to generate email if required
		$deadline = isset($var['deadline']) ? $var['deadline'] : "";    //needed separately to generate email if required
		$signoff = $var['signoff'];
		$comment = $var['comment'];
		if($signoff == 1) {
			$var['signoff'] = self::REVIEW_OK;
		} else {
			$var['signoff'] = self::REVIEW_REJECT;
		}
		$table_name = $this->getAssuranceTableName();
		$table_field = $this->getAssuranceTableField()."_";
		$results_table_name = $this->getResultsTableName();
		$results_table_field = $this->getResultsTableField()."_";
		$object_name = $this->getObjectName($this->getMyObjectType());
		$object_ref = $this->getRefTag().$kpi_id;
		//get time period name
		$timeObject = new SDBP6_SETUP_TIME();
		$time_name = $timeObject->getATimePeriodName($sdbip_id, $time_id, $this->getTimeType());

		//process data for _approve table
		$insert_data = array(
			$table_field.'kpi_id' => $kpi_id,
			$table_field.'time_id' => $time_id,
			$table_field.'insertuser' => $this->getUserID(),
			$table_field.'insertdate' => date("Y-m-d H:i:s"),
		);
//return array("info","insertdata :: ".serialize($var));
		foreach($var as $fld => $val) {
			if($fld == "deadline") {
				$val = date("Y-m-d", strtotime($val));
			}
			$insert_data[$table_field.$fld] = ASSIST_HELPER::code($val);
		}
		//insert into db
		$sql = "INSERT INTO ".$table_name." SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);

		//process changes to results table
		$results_data = array();
		$results_data[$results_table_field.'assurance'] = $var['signoff'];
		$results_data[$results_table_field.'assuranceuser'] = $this->getUserID();
		$results_data[$results_table_field.'assurancedate'] = date("Y-m-d H:i:s");
		if($signoff == 0) {
			$results_data[$results_table_field.'assurance_updateuser'] = $returnuser;
			$results_data[$results_table_field.'assurance_updatedeadline'] = date("Y-m-d", strtotime($deadline));
		}
		//update db
		$sql = "UPDATE ".$results_table_name." SET ".$this->convertArrayToSQLForSave($results_data)." WHERE ".$this->getResultsParentFieldName()." = ".$kpi_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
		$this->db_update($sql);

		//add to log
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "Added new |assurance| review for |time| ".$time_name.". The update was ".($signoff == 1 ? "" : "not ")."signed-off.",
		);
		$log_var = array(
			'object_id' => $kpi_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => SDBP6_LOG::APPROVE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		//send emails if the update was rejected
		if($signoff == 0) {
			$send_email = true;
			$subject = $this->getModTitle()." ".$this->getActivityName("assurance")." Review for ".$object_name." ".$object_ref." [$time_name]";
			//get sender information
			$from = $this->getUserNameWithEmail($this->getUserID());
			//cc notify emails - get these first so that if no return user is set these emails can be used as the to instead of CC
			if(count($emails) > 0) {
				//get usernames and emails
				$cc = $this->getUserNameWithEmail($emails);
			}
			//set the wording dependent on whether a return user was selected or not.
			if(strlen($returnuser) > 3) {
				//get user name & email
				$return_user_details = $this->getUserNameWithEmail($returnuser);
				if(count($return_user_details) > 0 && isset($return_user_details['email']) && strlen($return_user_details['email']) > 0) {
					$to = $return_user_details;
				} else {
					$send_email = false;
				}
				//set message to indicate that changes are requested
				$message = "
				<p>".$from['name']." has reviewed your update to ".$this->object_title." ".$object_ref." for time period ".$time_name.".</p>
				<p>The update has <span class='b u'>not</span> been signed-off with the following reason given:<br /><span class=i>".str_replace(chr(10), "<br />", $comment)."</span></p>
				<p>You have until <span class=overdue>".date("d F Y", strtotime($deadline))."</span> to make the required changes.</p>
				<p>&nbsp;</p>
				<p class=i>Reminder: ".$this->getObjectName("KPIS")." which have been returned can only be updated from the Action List on the Frontpage Dashboard.
				<br />If you are unable to find the ".$this->getObjectName("KPI")." on your Action List check your 'Days to Display' setting under your 'My Profile' (Main Menu) to confirm if the deadline falls within the given time frame.</p>
				";
			} else {
				//set cc as recipient
				$to = $cc;
				$cc = "";
				//set message to indicate that no further updates have been requested
				$message = "
				<p>".$from['name']." has reviewed the update to ".$this->object_title." ".$object_ref." for time period ".$time_name.".</p>
				<p>The update has <span class='b u'>not</span> been signed-off with the following reason given:<br /><span class=i>".str_replace(chr(10), "<br />", $comment)."</span></p>
				<p>".$from['name']." has not returned the ".$this->getObjectName("KPI")." update to a user for further updating.";
			}
			//generate email
			if($send_email !== false) {
				$emailObject = new ASSIST_EMAIL($to, $subject, $message, "HTML", $cc, "", $from);
				$emailObject->sendEmail();
			}
		}

		//return
		$result = array("ok", "Review saved successfully.");

		return $result;
	}


	/**
	 * Return updated KPIs which were previously rejected and have been updated and marked as complete
	 * @param $review_type - "assurance" or "approve"
	 * @param $kpi_id - which KPI is being worked on
	 * @param $time_id - which time period is being worked on
	 * @param $old_kpi_details - original details of the KPI
	 * @param $kpi_details - full details of the KPI
	 * @return array $result - result=>bool/need to act, 'update_data'=>array_of_fields_to_be_included_in_save,'emails'=>array_of_email_data_to_be_sent
	 */
	private function returnRejectedObject($review_type, $kpi_id, $time_id, $old_kpi_details, $kpi_details) {
		$result = array('result' => false, 'update_data' => array(), 'emails' => array(), 'log_data' => array(), 'review_log' => "");
		//1. check if the KPI was rejected and needs to be returned - is status SDBP6::REVIEW_REJECTED
		$current_review_status = $old_kpi_details[$this->getResultsTableField()."_".strtolower($review_type)];
		if(($current_review_status & SDBP6::REVIEW_REJECT) == SDBP6::REVIEW_REJECT) {
			//2. Change the assurance status to SDBP6::REVIEW_UPDATED
			$result['result'] = true;
			$result['update_data'] = array(strtolower($this->getResultsTableField()."_".$review_type) => SDBP6::REVIEW_UPDATED);
			$result['log_data'][strtolower($this->getResultsTableField()."_".$review_type)] = array(
				'to' => "Updated & Returned For Review",
				'from' => "Rejected",
				'raw' => array('to' => SDBP6::REVIEW_REJECT, 'from' => SDBP6::REVIEW_UPDATED)
			);
			//3. Get latest approve/assurance record
			$review_data = $this->getReviewHistory(strtoupper($review_type), $kpi_id, $time_id, false, true);
			//4. Check if reviewer wanted to be notified on updates
			if(isset($review_data['raw_notify']) && $review_data['raw_notify'] == true) {
				//5. If yes, send email
				$recipient_email = $this->getUserEmail($old_kpi_details[$this->getResultsTableField()."_".$review_type.'user']);
				$adminObject = new SDBP6_SETUP_ADMINS();
				$admin_section = $this->getAdminSectionIdentifier();
				$email_admins = $adminObject->getEmailUsersForReview($admin_section, $kpi_details[$this->getDepartmentFieldName()]);
				if(count($email_admins) > 0) {
					$cc = $this->getUserEmail(array_keys($email_admins));
				} else {
					$cc = "";
				}
				unset($adminObject);
				$object_name = $this->getObjectName($this->getMyObjectType());
				$object_ref = $this->getRefTag().$kpi_id;
				$sdbip_id = $kpi_details[$this->getParentFieldName()];
				//get time period name
				$timeObject = new SDBP6_SETUP_TIME();
				$time_name = $timeObject->getATimePeriodName($sdbip_id, $time_id, $this->getTimeType());
				unset($timeObject);
				//get sender information
				$from = $this->getUserDetailsForEmail($this->getUserID());
				$to = $this->getUserDetailsForEmail($old_kpi_details[$this->getResultsTableField()."_".$review_type.'user']);
				//set email contents
				$subject = $this->getModTitle()." ".$this->getActivityName(strtolower($review_type))." Rejected ".$object_name." ".$object_ref." [$time_name] returned for further review";
				$message = "<p>".$from['name']." has updated ".$object_name." ".$object_ref." for time period ".$time_name." in response to your review.</p><p>Please log onto Assist to review this update.</p>";
				$result['emails']['to'] = $to;
				$result['emails']['subject'] = $subject;
				$result['emails']['message'] = $message;
				$result['emails']['content_type'] = "HTML";
				$result['emails']['cc'] = $cc;
				$result['emails']['reply_to'] = $from;
			}
			//6. Add review record to show that object was updated & returned
			if(strtoupper($review_type) == "ASSURANCE") {
				$object_type = "ASSURANCE";
				$table_name = $this->getAssuranceTableName();
				$table_field = $this->getAssuranceTableField();
			} else {
				$object_type = "APPROVE";
				$table_name = $this->getApproveTableName();
				$table_field = $this->getApproveTableField();
			}
			//process data for _**** table
			$insert_data = array(
				$table_field.'_kpi_id' => $kpi_id,
				$table_field.'_time_id' => $time_id,
				$table_field.'_insertuser' => $this->getUserID(),
				$table_field.'_insertdate' => date("Y-m-d H:i:s"),
				$table_field.'_signoff' => SDBP6::REVIEW_UPDATED,
				$table_field.'_comment' => "Updated and returned for further review",
				$table_field.'_returnuser' => "",
				$table_field.'_deadline' => "0000-00-00",
				$table_field.'_attachment' => "",
				$table_field.'_notify' => "",
			);
			//insert into db
			$sql = "INSERT INTO ".$table_name." SET ".$this->convertArrayToSQLForSave($insert_data);
			$result['review_log'] = $sql;

		}

		return $result;
	}


	/*****************************************************************************************************************************
	 * GET Info functions
	 */

	public function getMyObjectType($plural = false) {
		return $plural ? self::OBJECT_TYPE_PLURAL : self::OBJECT_TYPE;
	}

	public function getMyObjectName($plural = false) {
		return $plural ? self::OBJECT_NAME_PLURAL : self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyChildObjectType() {
		return self::CHILD_OBJECT_TYPE;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getResultsTableName() {
		return $this->getDBRef()."_".self::RESULTS_TABLE;
	}

	public function getResultsTableField() {
		return self::RESULTS_TABLE_FLD;
	}

	public function getApproveTableName() {
		return $this->getDBRef()."_".self::APPROVE_TABLE;
	}

	public function getApproveTableField() {
		return self::APPROVE_TABLE_FLD;
	}

	public function getAssuranceTableName() {
		return $this->getDBRef()."_".self::ASSURANCE_TABLE;
	}

	public function getAssuranceTableField() {
		return self::ASSURANCE_TABLE_FLD;
	}

	public function hasResults() {
		return $this->has_results;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	//Added for PM6 integration #AA-568 JC
	public function getFullLogTableName() {
		return $this->getDBRef()."_".self::LOG_TABLE."_log";
	}

	public function hasDeadline() {
		return false;
	}

	public function getDeadlineField() {
		return false;
	}

	public function getTimeSetting() {
		return $this->time_type;
	}

	public function getCalcTypeTableField() {
		return $this->getTableField()."_calctype_id";
	}

	public function getTargetTypeTableField() {
		return $this->getTableField()."_unit_id";
	}

	public function getBaselineTableField() {
		return $this->getTableField()."_baseline";
	}

	public function getNewIndicatorTableField() {
		return $this->getTableField()."_new";
	}

	public function getAdminSectionIdentifier() {
		return self::ADMIN_SECTION;
	}

	public function getGraphReportFilterFields() {
		return $this->graph_report_filter_fields;
	}

	public function getGraphFilterFieldsNotAllowed() {
		return $this->graph_filter_fields_not_allowed;
	}

	public function getExtraFilterFieldsForGraphs() {
		return $this->extra_graph_filter_fields;
	}

	public function getFilterByOptions($page_section = "MANAGE", $page_action = "UPDATE", $sdbip_id = 0) {
		if($sdbip_id == 0 || $sdbip_id === false) {
			if($page_section == "REPORT") {
				$sdbip_id = $_SESSION[$this->getModRef()]['REPORT_SDBIP_ID'];//$_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['OBJECT']['details']['id'];
			} elseif($page_section != "NEW") {
				$sdbip_id = $_SESSION[$this->getModRef()]['MY_CURRENT_SDBIP'];//$_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['OBJECT']['details']['id'];
			} else {
				$sdbip_id = $_SESSION[$this->getModRef()][SDBP6_SDBIP::OBJECT_TYPE]['NEW_OBJECT']['details']['id'];
			}
		}
		$filter_by = $this->list_page_filter_options;
		$who = $filter_by['who'];
		$filter_by['who'] = array();
		//if the settings aren't available in the session - get from db
		/* DEVELOPMENT NOTE: REMEMBER TO COMMENT OUT THE IF STATEMENT IF TESTING USER ACCESS OR ELSE WAIT FOR 5 MINUTES!!! */
		if(!isset($_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]) || $_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['timestamp'] < (time())) {
			$who_settings = array();
			//if not view or admin then check for admin access
			if($page_section == "MANAGE" && $page_action != "VIEW") {
				$adminObject = new SDBP6_SETUP_ADMINS();
				$adminObject->setSDBIPID($sdbip_id);
				$admin_access = $adminObject->getMyAdminAccess();
//ASSIST_HELPER::arrPrint($admin_access);
				$include_parent_names = true;
				$display_all_who = false;
			} else {
				$admin_access = array();
				$display_all_who = true;
				$include_parent_names = false;
			}
			$headObject = new SDBP6_HEADINGS();
			$listObject = new SDBP6_LIST();
			$listObject->setSDBIPID($sdbip_id);
			foreach($who as $k => $f) { //echo "<hr /><h3>".$k."</h3>";
				//get heading details per field
				$head = $headObject->getAHeadingRecordsByField($f, self::OBJECT_TYPE);
				$who_settings[$f] = array(
					'name' => $this->replaceAllNames($head['name']),
					'table' => $head['h_table'],
					'type' => $head['h_type'],
					'items' => array()
				);
				//get list items per field
				if($head['h_type'] == "LIST") {
					$listObject->changeListType($head['h_table']);
					$items = $listObject->getActiveListItemsFormattedForSelect();
				} else {
					//assume OBJECT
					$class = $head['h_table'];
					$objObject = new $class();
					$objObject->setSDBIPID($sdbip_id);
					$items = $objObject->getActiveObjectsFormattedForFiltering($include_parent_names);
					$process_items = array();
				}
				if($page_section == "MANAGE" && $page_action != "VIEW" && count($admin_access) > 0 && isset($admin_access['by_section'][self::ADMIN_SECTION][strtolower($page_action)][$k])) {
					$access = $admin_access['by_section'][self::ADMIN_SECTION][strtolower($page_action)][$k];
					$item_keys = array_keys($items);
					//foreach($items as $key => $item) {
					for($x = 0; $x < count($item_keys); $x++) {
						$key = $item_keys[$x];
						if($k == "OWNER") {
							if(!in_array($key, $access)) {
								unset($items[$key]);
							}
						} else {
							$key2 = explode("_", $key);
							$item_type = $key2[0];
							$item_id = $key2[1];
							if(in_array($item_id, $access)) {
								//check that item hasn't already been processed (if both DIR and SUB are applicable)
								if(isset($process_items[$key])) {
									$process_items[$key] .= "*";
								} else {
									$process_items[$key] = $items[$key];
									if($item_type != "SUB" && $x != (count($item_keys) - 1)) {
										$move_on = true;
										while($move_on) {
											$x++;
											if(isset($item_keys[$x])) {    //check added due to "undefined offset 85" error received on manage_create_dept [JC 2019-08-02 YT AA-180]
												$key = $item_keys[$x];
												$key2 = explode("_", $key);
											} else {
												$key = false;
												$key2 = array("END");
											}
											if($key2[0] == "SUB") {
												$process_items[$key] = $items[$key];
											} else {
												$move_on = false;
												$x--;
											}
										}
									}
								}
							}
						}
					}
					if($k != "OWNER") {
						$raw_items = $items;
						$items = $process_items;
					}
				} elseif(!$display_all_who) {
					$items = array();
				}
				$who_settings[$f]['items'] = $items;
			}
			//populate session
			$_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['WHO'] = $who_settings;
			$_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['timestamp'] = time() + 5 * 60;
		} else {
			$who_settings = $_SESSION[$this->getModRef()][self::OBJECT_TYPE]['LIST_PAGE_FILTER_OPTIONS'][$page_section][$page_action][$sdbip_id]['WHO'];
		}

		//process who_settings
		foreach($who as $k => $f) {
			if(count($who_settings[$f]['items']) > 0) {
				if($k == "SUB") {
					//special code to be added for parent directorate here
					$filter_by['who'][$k] = " ---".$who_settings[$f]['name']."--- ";
					foreach($who_settings[$f]['items'] as $i => $l) {
						$filter_by['who'][$i] = $l;
					}
				} else {
					$filter_by['who'][$k] = " ---".$who_settings[$f]['name']."--- ";
					foreach($who_settings[$f]['items'] as $i => $l) {
						$filter_by['who'][$k."_".$i] = $l;
					}
				}
			}
		}

		if($page_action == "UPDATE") {
			$filter_by['update_when'] = array();
			/**
			 * Limit time periods on the update page to those which the user can actually access to update
			 *  = TP must have started
			 *  = TP must be open for the user's access level
			 **/
			//get user access level
			$useraccessObject = new SDBP6_USERACCESS();
			$my_useraccess = $useraccessObject->getMyTimePeriodUserAccess();
			if($my_useraccess['time_third'] == true) {
				$time_access = "tertiary";
			} else {
				if($my_useraccess['time_second'] == true) {
					$time_access = "secondary";
				} else {
					$time_access = "primary";
				}
			}
			//loop through TPs and find those that have started and which are open for the user
			foreach($filter_by['when'] as $time_id => $time) {
				if($time['has_started'] == true && $time['is_open'][$time_access] == true) {
					$filter_by['update_when'][$time_id] = $time;
				}
			}
		}

		return $filter_by;
	}

	public function getParentID($object_id = 0) {
		if($object_id == 0) {
			return $this->sdbip_id;
		} else {
			$row = $this->getRawObject($object_id);
			return $row[$this->getParentFieldName()];
		}
	}

	/** Function to get the SDBIP ID for a given object - used by Individual Performance series of modules (PM*)  */
	public function getMyParentSDBIPID($object_id = 0) {
		return $this->getParentID($object_id);
	}

	public function getFilterSQL($filter) {
		$where = array();
//		'who'=>"all",
		if($filter['who'] != "all") {
			$who = explode("_", $filter['who']);
			switch($who[0]) {
				case "SUB":
					$where['who'] = $this->getDepartmentFieldName()." = ".$who[1];
					break;
				case "OWNER":
					$where['who'] = $this->getOwnerFieldName()." = ".$who[1];
					break;
				case "DIR":
					$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
					$list = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => $who[1]));
					if(count($list) > 0) {
						$where['who'] = $this->getDepartmentFieldName()." IN (".implode(",", array_keys($list)).")";
					} else {
						//use impossible condition to deal with issue arising from a directorate with no children
						$where['who'] = $this->getDepartmentFieldName()." < 0 ";
					}
					break;
			}
		}
//		'what'=>"all",
		switch($filter['what']) {
			case "op":
				$where['what'] = $this->getTableField()."_cap_op = 0";
				break;
			case "cap":
				$where['what'] = $this->getTableField()."_cap_op > 0";
				break;
			case "top":
				$where['what'] = $this->getTableField()."_top_id > 0";
				break;
			case "!top":
				$where['what'] = $this->getTableField()."_top_id = 0";
				break;
			case "all":
			default:
				//do nothing - all is default;
		}
//		'when'=>"today",
		//WHEN is only applicable to the results - doesn't impact main object sql
//		'display'=>"limited" - IGNORE - Handled by HEADINGS class

		if(count($where) > 0) {
			return "(".implode(") AND (", $where).")";
		} else {
			return "";
		}
	}











	/*****************************************************************************************************************************
	 * GET Object functions
	 */

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($options = array()) {
		return $this->getActiveObjectsFormattedForImportProcessing($options);
	}

	/**
	 * Get list of active objects ready to filter for import
	 */
	public function getActiveObjectsFormattedForImportProcessing($sdbip_id = 0, $options = array()) {
		$options['name_type'] = "ref";
		$items = $this->getActiveObjectsFormattedForSelect($sdbip_id, $options);
		$data = array();
		foreach($items as $id => $name) {
			$name = trim(strtolower(str_replace(" ", "", $name)));
			$data[$name] = $id;
		}
		return $data;
	}


	public function getImportedRecords($sdbip_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND ".$this->getParentFieldName()." = ".$sdbip_id." 
				AND (".$this->getStatusFieldName()." & ".self::CONVERT_IMPORT.") = ".self::CONVERT_IMPORT;
		$in_progress = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND ".$this->getParentFieldName()." = ".$sdbip_id." 
				AND (".$this->getStatusFieldName()." & ".self::CONVERT_SDBP6ED.") = ".self::CONVERT_SDBP6ED;
		$completed = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		return ($in_progress + $completed);
	}


	public function getImportedResultsRecords($keys) {
		if(count($keys) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." 
					WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $keys).")";
			return $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsSecondaryParentFieldName());
		} else {
			return array();
		}
	}


	public function getList($section, $options) {
//			echo "<hr /><h1>GET LIST OPTIONS :: ".$section."</h1>";
//			ASSIST_HELPER::arrPrint($options);
		$sdbip_id = $options['sdbip_id'];
		$list = $this->getMyList($this->getMyObjectType(), $section, $options);
//ASSIST_HELPER::arrPrint($list['rows']);
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE();
		$target_types = $targetTypeObject->getObjectsForSetup();
		$results_field = array("original_total",
			"revised_total",
			"actual_total");

		$object_ids = array_keys($list['rows']);
		$object_ids = $this->removeBlanksFromArray($object_ids);
		if(count($object_ids) > 0) {
			//if required to display, get results for specific time periods
			if(isset($list['results_head']['bottom']) && isset($list['results_head']['top']) && count($list['results_head']['top']) > 0 && count($list['results_head']['bottom']) > 0) {
				$list['results_rows'] = $this->getResultsForListPages($list['rows'], ($list['results_head']['top']), $list['results_head']['bottom']);
			}

			//if required to display, get totals of results
			if(isset($list['head']['original_total']) || isset($list['head']['revised_total']) || isset($list['head']['actual_total'])) {
				$summary_values = $this->getSummaryResults($object_ids, $sdbip_id);
			}

			//process additional results
			foreach($list['rows'] as $obj_id => $row) {
				//set target type formatting
				$tt = $row['target_type_id'];
				if(!isset($target_types[$tt])) {
					$tt = 2;
				}
				$this_target_type = $target_types[$tt];
				//remove custom TT & CT fields so that list page can display properly
				unset($list['rows'][$obj_id]['target_type_id']);
				unset($list['rows'][$obj_id]['calc_type_id']);
				//prep formatting
				$pre = $target_types[$tt]['display_type'] != "post" ? $target_types[$tt]['code']."&nbsp;" : "";
				$post = $target_types[$tt]['display_type'] == "post" ? (substr($target_types[$tt]['code'], 0, 1) != ":" ? "&nbsp;" : "").$target_types[$tt]['code'] : "";
				//foreach summary field, check if required and then format
				foreach($results_field as $field) {
					if(isset($list['head'][$field])) {
						if(!isset($summary_values[$obj_id][$field]) || !is_numeric($summary_values[$obj_id][$field])) {
							$list['rows'][$obj_id][$field]['display'] = $this->getUnspecified();
						} else {
							$this_number = $summary_values[$obj_id][$field];
							if(round($this_number, 0) == $this_number && $this_target_type['format'] != true) {
								$this_number = ASSIST_HELPER::format_number($this_number, "INT");
							} else {
								$this_number = ASSIST_HELPER::format_number($this_number, "FLOAT");
							}
							$list['rows'][$obj_id][$field]['display'] = $pre.str_replace(" ", "&nbsp;", $this_number).$post;
						}
					}
				}
				//loop through all fields (excl summary fields) to check for any that require formatting
				foreach($row as $fld => $r) {
					if(isset($list['head'][$fld]['apply_formatting']) && $list['head'][$fld]['apply_formatting'] == true && !in_array($fld, $results_field)) {
						if(is_numeric($r['display'])) {
							$this_number = $r['display'];
							if(round($this_number, 0) == $this_number && $this_target_type['format'] != true) {
								$this_number = ASSIST_HELPER::format_number($this_number, "INT");
							} else {
								$this_number = ASSIST_HELPER::format_number($this_number, "FLOAT");
							}
							$list['rows'][$obj_id][$fld]['display'] = $pre.str_replace(" ", "&nbsp;", $this_number).$post;
						} else {
							$list['rows'][$obj_id][$fld]['format_me'] = false;
						}
					}
				}
				if(isset($list['results_rows'])) {
					foreach($list['results_rows'][$obj_id] as $time_id => $result) {
						foreach($result as $r_fld => $r) {
							if(isset($list['results_head']['bottom'][$r_fld]['apply_formatting']) && $list['results_head']['bottom'][$r_fld]['apply_formatting'] == true) {
								$list['results_rows'][$obj_id][$time_id]['raw'][$r_fld] = $r;
								if(round($r, 0) == $r && $this_target_type['format'] != true) {
									$r = ASSIST_HELPER::format_number($r, "INT");
								} else {
									$r = ASSIST_HELPER::format_number($r, "FLOAT");
								}
								$list['results_rows'][$obj_id][$time_id][$r_fld] = $pre.str_replace(" ", "&nbsp;", $r).$post;
							}
						}
					}
				}
			}
		}

		return $list;
	}


	public function getAObject($id = 0, $options = array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id, $options);
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id, $include_summary = true, $include_result = true) {
		//check for a valid id and return a blank array if not
		if(!$this->checkIntRef($obj_id)) {
			return array();
		} else {
			//get raw data direct from table
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
			$data = $this->mysql_fetch_one($sql);
			$sdbip_id = $data[$this->getParentFieldName()];

			//add totals & results
			if($include_summary) {
				$summary_values = $this->getSummaryResults(array($obj_id), $sdbip_id);
				foreach($summary_values[$obj_id] as $fld => $v) {
					$data[$fld] = $v;
				}
			}

			if($include_result) {
				$data['results'] = $this->getRawResults($obj_id, false, false, false, $sdbip_id);
			}
			return $data;
		}
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObjectListByParent($sdbip_id) {
		//check for a valid id and return a blank array if not
		if(!$this->checkIntRef($sdbip_id)) {
			return array();
		} else {
			//get raw data direct from table
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = ".$sdbip_id;
			$data = $this->mysql_fetch_all($sql);
			return $data;
		}
	}

	/**
	 * Returns an unformatted array of a Results Object for given object_id & time_id
	 */
	public function getRawUpdateObject($obj_id, $time_id) {
		//check for a valid id and return a blank array if not
		if(!$this->checkIntRef($obj_id) || !$this->checkIntRef($time_id)) {
			return array();
		} else {
			//get raw data direct from table
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." = ".$obj_id." AND ".$this->getResultsTimeFieldName()." = ".$time_id;
			$data = $this->mysql_fetch_one($sql);
			return $data;
		}
	}


	public function getStats($parent_id) {
		$data = array(
			'count' => 0,
			'updates_done' => 0,
			'converted' => 0,
			'sdbp6ed' => 0
		);
		//count number of objects
		$sql = "SELECT
					count(O.".$this->getIDFieldName().") as count
					, O.".$this->getStatusFieldName()." as status
				FROM ".$this->getTableName()." O
				WHERE ".$this->getActiveStatusSQL("O")."
				AND O.".$this->getParentFieldName()." = ".$parent_id
			." GROUP BY O.".$this->getStatusFieldName();
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$data['count'] += $r['count'];
			if(($r['status'] & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) {
				$data['converted'] += $r['count'];
			}
			if(($r['status'] & SDBP6::CONVERT_SDBP6ED) == SDBP6::CONVERT_SDBP6ED) {
				$data['sdbp6ed'] += $r['count'];
			}
		}
		//check if updates have been done
		$sql = "SELECT count(R.".$this->getResultsTableField()."_update) as updates_done
				FROM ".$this->getResultsTableName()." R
				INNER JOIN ".$this->getTableName()." O ON R.".$this->getResultsParentFieldName()." = O.".$this->getParentFieldName()."
				WHERE ".$this->getResultsActiveStatusSQL("R")."
				AND R.".$this->getResultsTableField()."_update = 1
				AND O.".$this->getParentFieldName()." = ".$parent_id;
		$row = $this->mysql_fetch_one($sql);
		$data['updates_done'] += $row['updates_done'];
		return $data;
	}










	/************************************************
	 * DEPT KPI linked to TOP functions
	 */

	/**
	 * How many Dept KPIs are linked to the given Top KPI - ACTIVE KPIs
	 * Needed to know whether user can update the Top KPI actual or not
	 */
	public function getNumberOfKPIsLinkedToTopLayer($object_id) {
		$sql = "SELECT count(K.".$this->getIDFieldName().") as count FROM ".$this->getTableName()." K WHERE K.".$this->getTertiaryParentFieldName()." = ".$object_id." AND ".$this->getActiveStatusSQL("K");
		$row = $this->mysql_fetch_one($sql);
		return $row['count'];
	}

	/**
	 * Get list of active DEPT KPIs linked to given TL KPI
	 * Used by *_TOPKPI
	 * JC #AA-508 5 Jan 2021
	 */
	public function getKPIsLinkedToTopLayer($object_id) {
		$sql = "SELECT K.".$this->getIDFieldName()." as id FROM ".$this->getTableName()." K WHERE K.".$this->getTertiaryParentFieldName()." = ".$object_id." AND ".$this->getActiveStatusSQL("K");
		$row = $this->mysql_fetch_all($sql);
		return $row;
	}

	/**
	 * Get list of active DEPT KPIs linked to given PROJECT
	 * Used by *_PROJECT
	 * JC #AA-508 5 Jan 2021
	 */
	public function getKPIsLinkedToProject($object_id) {
		$sql = "SELECT K.".$this->getIDFieldName()." as id FROM ".$this->getTableName()." K WHERE K.".$this->getSecondaryParentFieldName()." = ".$object_id." AND ".$this->getActiveStatusSQL("K");
		$row = $this->mysql_fetch_all($sql);
		return $row;
	}

	/**
	 * Does the KPI have a TL or Proj linked to it for display on a view page?
	 * Used by common/form_object.php
	 * JC #AA-508 5 Jan 2021
	 * @param $object Array returned from getRawUpdateObject
	 * @return Array [display_linked=>true/false, linked=>array of linked objects id => type
	 */
	public function getLinkedObjectDetailsForDisplay($object) {
		$data = array('display_linked_objects' => false, 'linked_objects' => array());

		//secondary = SDBP6_PROJECT
		//if item is linked then test linked object - is it still active?
		if($this->getSecondaryParentFieldName() !== false && $object[$this->getSecondaryParentFieldName()] != false) {
			$project_id = $object[$this->getSecondaryParentFieldName()];
			$linked_object = new SDBP6_PROJECT();
			if($linked_object->isObjectActive($project_id)) {
				$data['display_linked_objects'] = true;
				$data['linked_objects'][SDBP6_PROJECT::OBJECT_TYPE] = array($project_id);
			}
		}


		//tertiary = SDBP6_TOPKPI
		//if item is linked then test linked object - is it still active?
		if($this->getTertiaryParentFieldName() !== false && $object[$this->getTertiaryParentFieldName()] != false) {
			$top_id = $object[$this->getTertiaryParentFieldName()];
			$linked_object = new SDBP6_TOPKPI();
			if($linked_object->isObjectActive($top_id)) {
				$data['display_linked_objects'] = true;
				$data['linked_objects'][SDBP6_TOPKPI::OBJECT_TYPE] = array($top_id);
			}
		}


		return $data;
	}


















	/*********************************************************************************************
	 * Results/Finances functions
	 */


	/**
	 * Add Budget per project per time period - called by $this->addObject()
	 */
	private function addTargetObject($object_id, $time_id, $original, $adjustment, $description) {
		$revised = $original + $adjustment;
		$result = 0;
		$var = array(
			$this->getResultsOriginalFieldName() => $original,
			$this->getResultsAdjustmentsFieldName() => $adjustment,
			$this->getResultsRevisedFieldName() => $revised,
			$this->getResultsTableField().'_target_description' => $description,
			$this->getResultsActualFieldName() => 0,
			$this->getResultsResultsFieldName() => $result,
			$this->getResultsParentFieldName() => $object_id,
			$this->getResultsTimeFieldName() => $time_id,
			$this->getResultsStatusFieldName() => self::ACTIVE,
			self::RESULTS_TABLE_FLD.'_insertuser' => $this->getUserID(),
			self::RESULTS_TABLE_FLD.'_insertdate' => $this->getDateForDB(),
		);
		$sql = "INSERT INTO ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$this->db_insert($sql);
		return array($this->getResultsRevisedFieldName() => $revised, $this->getResultsResultsFieldName() => $result);
	}

	/**
	 * Edit Budget per project per time period - called by $this->editObject()
	 */
	private function editTargetObject($project_id, $time_id, $original, $adjustment, $actual, $description) {
		$revised = $original + $adjustment;
		$result = 0;
		$var = array(
			$this->getResultsOriginalFieldName() => $original,
			$this->getResultsAdjustmentsFieldName() => $adjustment,
			$this->getResultsRevisedFieldName() => $revised,
			$this->getResultsTableField()."_target_description" => $description,
			$this->getResultsResultsFieldName() => $result,
		);
		$sql = "UPDATE ".$this->getResultsTableName()." SET ".$this->convertArrayToSQLForSave($var)." WHERE ".$this->getResultsParentFieldName()." = $project_id AND ".$this->getResultsTimeFieldName()." = $time_id";
		$this->db_update($sql);
		return array($this->getResultsRevisedFieldName() => $revised, $this->getResultsResultsFieldName() => $result);
	}


	/**
	 * get results for specific time periods for specific object ids for specific columns for list pages
	 * @param $objects Array : of id=>(calc_type_id=>##) - any more data is unneeded and uses up memory unnecessarily [JC] #AA-380 26 April 2020
	 * @param $time Array of time periods for calculation
	 * @param $headings Array of headings for processing of data
	 * @param $value_type String
	 * @param $format_result_code Bool
	 * @param $limit String - how much time to process
	 * @param $include_time_in_return Bool - return $time with final dataset - ignored if save_to_file_for_reporting is TRUE
	 * @param $save_to_file_for_reporting Bool - save data in file and return filename instead of data (to free up memory)
	 * @param $process_for_formatting Bool - Process non-target/actual fields for display?
	 * @param $save_memory Bool - remove unnecessary data from final data set to free memory
	 * @return Array or String - depends on save_to_file_for_reporting setting
	 */
	public function getResultsForListPages($objects, $time, $headings, $value_type = "", $format_result_code = true, $limit = "ALL", $include_time_in_return = false, $save_to_file_for_reporting = false, $process_for_formatting = true, $save_memory = false) {
		if($save_to_file_for_reporting) {
			$file_name = "../files/temp/".$this->getCmpCode()."_".$this->getModRef()."_".$this->getUserID()."_report_results_".mt_rand()."_".date("Ymd_His").".txt";
			$file_handler = fopen($file_name, "w");
		} else {
			$file_name = false;
			$file_handler = null;
		}

		if(!is_array($time) || count($time) == 0) {
			$timeObject = new SDBP6_SETUP_TIME();
			$sdbip = $this->getCurrentSDBIPFromSessionData();
			$sdbip_id = $sdbip['id'];
			$time = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "MONTH", 0, false, $limit);
			unset($timeObject);
		}
		$time_ids = array_keys($time);

		$object_ids = array_keys($objects);
		$calctypeObject = new SDBP6_SETUP_CALCTYPE();
		$calc_type_settings = array();

		//get result options - test against >1 for timestamp setting
		//uses additional session setting, even tho object class uses one to, just to save time creating the object if not needed
		$result_options = $this->getResultOptions();

		//get records
		if(count($time_ids) > 0 && count($object_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $object_ids).") AND ".$this->getResultsTimeFieldName()." IN (".implode(",", $time_ids).")";
			$rows = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
		} else {
			$rows = array();
		}

		foreach($rows as $obj_id => $row) {

			//changed if(isset()){unset();} to simple $ = array() to speed up processing [JC] #AA-380
			$target_array = array();
			$actual_array = array();
			$original_target_array = array();
			$target_adjustment_array = array();

			//$object = $objects[$obj_id]; - removed to reduce memory load [JC] #AA-380 26 April 2020
			//$target_type_id = $object['target_type_id'];	//added with hardcoded field name in SDBP6.getMyList() - not used so removed to reduce memory load [JC] #AA-380 26 April 2020
			$calc_type_id = $objects[$obj_id]['calc_type_id'];    //added with hardcoded field name in SDBP6.getMyList()
			//get calculation type settings - if already called before then get from array otherwise query calc type object
			if(isset($calc_type_settings[$calc_type_id])) {
				$calc_type = $calc_type_settings[$calc_type_id];
			} else {
				$calc_type = $calctypeObject->getCalcTypeOptions($calc_type_id);
				$calc_type_settings[$calc_type_id] = $calc_type;
			}
//			$this->markTime("start of processing"); ASSIST_HELPER::arrPrint($headings);
			foreach($row as $time_id => $result) {
				$t_period = $time[$time_id];
				$target = $result[$this->getResultsRevisedFieldName()];
				$actual = $result[$this->getResultsActualFieldName()];

				$original_target = $result[$this->getResultsOriginalFieldName()];
				$target_adjustment = $result[$this->getResultsAdjustmentsFieldName()];
				foreach($headings as $fld => $head) {
					$h_type = $head['type'];
					if($process_for_formatting) {
						//$val = $result[$fld];
						switch($h_type) {
							//generate the display indicating the result
							case "RESULT":
								if($t_period['has_started'] != true) {// || $result['kr_update']==SDBP6::UPDATE_NOT_STARTED) { - JC decided not to default non-updated KPIs to N/A result
									$kr_result = 0;
								} else {
									$kr_result = $calctypeObject->calculatePeriodResult($calc_type_id, $target, $actual, $result_options);
								}
								$kr_display = $this->createResultDisplay($kr_result, $format_result_code);
								$rows[$obj_id][$time_id][$fld] = $kr_display;
								break;
							//generate a display indicating the status of the UPDATE APPROVE ASSURANCE
							case "STATUS":
								$rows[$obj_id][$time_id][$fld] = $this->generateStatusDisplay($result, $calctypeObject->processZeroTargets($calc_type), $t_period['has_started']);
								break;
							case "COMMENT":
								$rows[$obj_id][$time_id][$fld] = "comment";
								break;
							case "ATTACH":
								$object_type = $this->getMyObjectName();//isset($options['object_type']) ? $options['object_type'] : "X";
								$object_id = $obj_id;//isset($options['object_id']) ? $options['object_id'] : "0";
								$attach_detail = $result[$fld];
								if(strlen($attach_detail) > 0) {
									$attach_detail = $this->decodeAttachmentDataFromDatabase($attach_detail, false);
									$attach_detail = unserialize($attach_detail);
									if(!isset($displayObject)) {
										$displayObject = new SDBP6_DISPLAY();
									}
									$save_folder = $this->getSaveFolder();
									$disp = $displayObject->getAttachForDisplayInList($attach_detail, $object_type, $object_id, $save_folder);
								} else {
									$disp = "";
								}
								$rows[$obj_id][$time_id][$fld] = $disp;
								break;
						}
					} elseif($save_memory) {
						//remove excess data to save memory
						if(in_array($h_type, array("TEXT", "ATTACH", "COMMENT"))) {
							unset($rows[$obj_id][$time_id][$fld]);
						}
					}
				}

				$rows[$obj_id][$time_id]['assurance_status'] = $this->generateAssuranceStatusForReport($result);
				$rows[$obj_id][$time_id]['approve_status'] = $this->generateApproveStatusForReport($result);
				if(isset($value_type) && is_string($value_type) && strlen($value_type) > 0 && $value_type != 'value_as_captured' && array_key_exists($time_id, $time)) {
					$target_array[$time_id] = $target;
					$actual_array[$time_id] = $actual;

					$original_target_array[$time_id] = $original_target;
					$target_adjustment_array[$time_id] = $target_adjustment;

					$values = $calctypeObject->calculatePTDValues($calc_type_id, $target_array, $actual_array, $result_options);

					$kr_result = $calctypeObject->calculatePeriodResult($calc_type_id, $values['target'], $values['actual'], $result_options);

					$kr_display = $this->createResultDisplay($kr_result, $format_result_code);
					$rows[$obj_id][$time_id][$this->getResultsResultsFieldName()] = $kr_display;

					$rows[$obj_id][$time_id][$this->getResultsRevisedFieldName()] = $values['target'];
					$rows[$obj_id][$time_id][$this->getResultsActualFieldName()] = $values['actual'];

					$values = $calctypeObject->calculatePTDValues($calc_type_id, $original_target_array, $target_adjustment_array);

					$rows[$obj_id][$time_id][$this->getResultsOriginalFieldName()] = $values['target'];
					$rows[$obj_id][$time_id][$this->getResultsAdjustmentsFieldName()] = $values['actual'];
				}
			}

			//save record to file and then remove to reduce memory load [JC] #AA-380
			/*if($save_to_file_for_reporting) {
				$line = json_encode($rows[$obj_id]);
				$line = str_replace(chr(10),"||",$line);
				fwrite($file_handler,$line.chr(10).chr(10));
				unset($rows[$obj_id]);
			}*/
		}
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->print_mem();
//echo("<hr />End of processing results");

		if($save_to_file_for_reporting) {
			$alt_rows = array();
			foreach($rows as $i => $time_row) {
				foreach($time_row as $ti => $row) {
					foreach($row as $f => $r) {
						$alt_rows[$i][$ti][$f] = mb_convert_encoding($r, 'UTF-8');
					}
				}
			}
			$line = base64_encode(json_encode($alt_rows));
			fwrite($file_handler, $line);
			return $file_name;
		} else {
			if($include_time_in_return === true) {
				$data = array('time' => $time, 'rows' => $rows);
			} else {
				$data = $rows;
			}
			return $data;
		}
	}

	public function getResultsForLinkedTopLayerKPIs($object_ids, $headings, $raw_time) {
		//AA-594 - approve_status/logs fields breaking the TL audit log report because fields were not being correctly pulled out of the headings sent here - fixed the send but added an extra unset here just in case [JC] 15 April 2021
		if(isset($headings['approve_status'])) {
			unset($headings['approve_status']);
		}
		if(isset($headings['assurance_status'])) {
			unset($headings['assurance_status']);
		}
		if(isset($headings['approve_logs'])) {
			unset($headings['approve_logs']);
		}
		if(isset($headings['assurance_logs'])) {
			unset($headings['assurance_logs']);
		}

		$data = array();
		if(count($object_ids) > 0) {
			$time_conversion_to_quarters = array();
			foreach($raw_time as $t_id => $t) {
				foreach($t['included_time_periods'] as $x_t) {
					$time_conversion_to_quarters[$x_t] = $t_id;
				}
			}
			$fields = array_keys($headings);
			$listObject = new SDBP6_LIST("owner");
			$owners = $listObject->getAllListItemsFormattedForSelect();
			$timeObject = new SDBP6_SETUP_TIME();
			$time_names = $timeObject->getAllTimePeriodsRegardlessOfYear();
			$sql = "SELECT ".$this->getIDFieldName().", ".$this->getTertiaryParentFieldName()." as top_id, ".$this->getTableField()."_ref as ref, ".$this->getTableField()."_owner_id as owner FROM ".$this->getTableName()." O WHERE ".$this->getTertiaryParentFieldName()." IN (".implode(",", $object_ids).") AND ".$this->getActiveStatusSQL("O");
			$kpis = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
			$kpi_keys = array_keys($kpis);
			if(count($kpi_keys) > 0) {
				$sql = "SELECT ".implode(",", $fields).", ".$this->getResultsTimeFieldName()." as time_id, ".$this->getResultsParentFieldName()." as kpi_id, ".$this->getResultsTableField()."_updateuser as updateuser FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $kpi_keys).")";
				$rows = $this->mysql_fetch_all_by_id2($sql, "kpi_id", "time_id");
				foreach($kpis as $kpi_id => $kpi) {
					$top_id = $kpi['top_id'];
					if(!isset($data[$top_id])) {
						$data[$top_id] = array();
					}
					foreach($rows[$kpi_id] as $time_id => $row) {
						if(isset($time_conversion_to_quarters[$time_id])) {    //Added IF statement to catch errors generated if limited time periods are called.  Seen while investigating AA-554 [JC] 2 Mar 2021
							$my_time_id = $time_conversion_to_quarters[$time_id];
							foreach($fields as $fld) {
								if(strlen($row[$fld]) > 0) {
									$ref = isset($kpi['ref']) && strlen($kpi['ref']) > 0 ? $kpi['ref'] : $this->getRefTag().$kpi_id;
									$owner = isset($owners[$kpi['owner']]) ? $owners[$kpi['owner']] : "(".$this->getUserName($row['updateuser']).")";
									if($headings[$fld]['type'] == "ATTACH") {
										$x = $this->processAttachmentsForDetails($row[$fld]);
										$v = array();
										foreach($x as $y) {
											$v[] = $y['original_filename'];
										}
										$v = implode("; ", $v);
									} else {
										$v = $row[$fld];
									}
									$data[$top_id][$my_time_id][$fld][] = "[".$ref."] ".$owner.": ".$v." (".$time_names[$time_id].")";
								}
							}
						}
					}
				}


			}

		}
		return $data;
	}

	public function getResultsForGraphs($objects, $limit_time_periods) {
//		echo "<hr />";
// $this->markTime("start of function");
		//get time & sdbip details
		$timeObject = new SDBP6_SETUP_TIME();
		$sdbip = $this->getCurrentSDBIPFromSessionData();
		$sdbip_id = $sdbip['id'];
		$time = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, $this->getTimeType(), 0, false, $limit_time_periods);
		$time_ids = array_keys($time);
		if($limit_time_periods == "YTD") {
			$limit_time_periods = array('start' => $time_ids[0], 'end' => $time_ids[count($time_ids) - 1]);
		}

		$object_ids = array_keys($objects);
		$calctypeObject = new SDBP6_SETUP_CALCTYPE();
		$calc_type_settings = array();
		//get result options - test against >1 for timestamp setting
		//uses additional session setting, even tho object class uses one to, just to save time creating the object if not needed
		$result_options = $this->getResultOptions();
		$this->markTime("start of sql");
		//get records
		if(count($time_ids) > 0 && count($object_ids) > 0) {
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $object_ids).") AND ".$this->getResultsTimeFieldName()." IN (".implode(",", $time_ids).")";
			$rows = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsTimeFieldName());
		} else {
			$rows = array();
		}
		$this->markTime("end of sql");

		$this->markTime("start of processing");
		foreach($rows as $obj_id => $row) {

			if(isset($target_array)) {
				unset($target_array);
			}
			if(isset($actual_array)) {
				unset($actual_array);
			}
			if(isset($original_target_array)) {
				unset($original_target_array);
			}
			if(isset($target_adjustment_array)) {
				unset($target_adjustment_array);
			}

			$object = $objects[$obj_id];
			$calc_type_id = $object['calc_type_id'];    //added with hardcoded field name in SDBP6.getMyList()
			//get calculation type settings - if already called before then get from array otherwise query calc type object
			if(isset($calc_type_settings[$calc_type_id])) {
				$calc_type = $calc_type_settings[$calc_type_id];
			} else {
				$calc_type = $calctypeObject->getCalcTypeOptions($calc_type_id);
				$calc_type_settings[$calc_type_id] = $calc_type;
			}
			foreach($row as $time_id => $result) {
				$target = $result[$this->getResultsRevisedFieldName()];
				$actual = $result[$this->getResultsActualFieldName()];
				$target_array[$time_id] = $target;
				$actual_array[$time_id] = $actual;

				if($time_id == $limit_time_periods['end']) {

					$values = $calctypeObject->calculatePTDValues($calc_type_id, $target_array, $actual_array, $result_options);
					//get result id - 0 -> 5
					$kr_result = $calctypeObject->calculatePeriodResult($calc_type_id, $values['target'], $values['actual'], $result_options);
					//convert to code
					$kr_code = $result_options[$kr_result]['code'];
					$rows[$obj_id][$time_id] = $kr_code;

				}
			}
		}
		$data = array('time' => $time, 'rows' => $rows);

		$this->markTime("end of function"); //echo "<hr />";
		return $data;
	}







	/*****************************************************************
	 * REVIEW (APPROVE / ASSURANCE) FUNCTIONS
	 */
	/**
	 * Get Approve History function
	 * Triggers standardised getReviewHistory function
	 * @param $kpi_id - object id being worked on
	 * @param $time_id - time period being worked on
	 * @param bool $include_html
	 * @param false $get_latest_record_only - false = get all records (for main history display), true = get last record (for returning update purposes)
	 * @return array
	 */
	public function getApproveHistory($kpi_id, $time_id, $include_html = true, $get_latest_record_only = false) {
		return $this->getReviewHistory("APPROVE", $kpi_id, $time_id, $include_html, $get_latest_record_only);
	}

	/**
	 * Triggers getReviewHistory function
	 * @param $kpi_id - object id being worked on
	 * @param $time_id - time period being worked on
	 * @param bool $include_html
	 * @param false $get_latest_record_only - false = get all records (for main history display), true = get last record (for returning update purposes)
	 * @return array
	 */
	public function getAssuranceHistory($kpi_id, $time_id, $include_html = true, $get_latest_record_only = false) {
		return $this->getReviewHistory("ASSURANCE", $kpi_id, $time_id, $include_html, $get_latest_record_only);
	}


	/**
	 * @param $review_type - ASSURANCE or APPROVE
	 * @param $kpi_id - object id being worked on
	 * @param $time_id - time period being worked on
	 * @param bool $include_html
	 * @param false $get_latest_record_only - false = get all records (for main history display), true = get last record (for returning update purposes)
	 * @return array
	 */
	private function getReviewHistory($review_type, $kpi_id, $time_id, $include_html = true, $get_latest_reject_record_only = false) {
		//set variables
		if($review_type == "ASSURANCE") {
			$object_type = "ASSURANCE";
			$table_name = $this->getAssuranceTableName();
			$table_field = $this->getAssuranceTableField();
		} else {
			$object_type = "APPROVE";
			$table_name = $this->getApproveTableName();
			$table_field = $this->getApproveTableField();
		}
		$id_field = $table_field."_id";
		$parent_field = $table_field."_kpi_id";
		$time_field = $table_field."_time_id";
		$datetime_field = "insertdate";
		$deadline_field = "deadline";
		$data = array(
			'head' => array(
				'insertdate' => "Date",
				'insertuser' => "Reviewer"
			),
			'rows' => array(),
		);
		$select = array(
			"CONCAT(TK.tkname,' ',TK.tksurname) as insertuser",
			$table_field."_insertdate as insertdate"
		);

		//get table headings
		$headingObject = new SDBP6_HEADINGS();
		$headings = $headingObject->getReviewObjectHeadings($object_type);
		foreach($headings as $fld => $head) {
			if($fld != "emails") {
				$data['head'][$fld] = $head['name'];
				if($fld == "returnuser") {
					$select[] = "CONCAT(R.tkname,' ',R.tksurname) as ".$fld;
				} elseif($fld == "signoff") {
					$select[] = "IF(( ".$table_field."_".$fld." & ".self::REVIEW_OK.")=".self::REVIEW_OK.",1,0) as $fld ";
				} else {
					$select[] = $table_field."_".$fld." as ".$fld;
				}
				if($get_latest_reject_record_only) {
					$select[] = $table_field."_".$fld." as raw_".$fld;
				}
			}
		}

		//get info from db
		$sql = "SELECT ".implode(",", $select)."
				FROM ".$table_name."
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep R ON R.tkid = ".$table_field."_returnuser
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = ".$table_field."_insertuser
				WHERE $parent_field = $kpi_id AND $time_field = $time_id
				".($get_latest_reject_record_only ? " AND (".$table_field."_signoff & ".SDBP6::REVIEW_REJECT." ) = ".SDBP6::REVIEW_REJECT : "")."
				ORDER BY $id_field ";
		if($get_latest_reject_record_only) {
			$sql .= " DESC LIMIT 1"; //order by id from newest to oldest & limit to only 1 record
		}
		$data['rows'] = $this->mysql_fetch_all($sql);
		if($get_latest_reject_record_only) {
			return isset($data['rows'][0]) ? $data['rows'][0] : array();
		} else {
			foreach($data['rows'] as $i => $row) {
				foreach($row as $fld => $val) {
					if($fld == $deadline_field) {
						if(!is_null($row['returnuser']) && ($val == "0000-00-00" || date("d M Y", strtotime($val)) == "01 Jan 1970")) {
							$data['rows'][$i][$fld] = $this->getUnspecified();
						} elseif(is_null($row['returnuser'])) {
							$data['rows'][$i][$fld] = $include_html ? "<div class=center>-</span>" : "N/A";
						} else {
							$data['rows'][$i][$fld] = date("d M Y", strtotime($val));
						}
					} elseif($fld == $datetime_field) {
						$data['rows'][$i][$fld] = date("d M Y H:i", strtotime($val));
					} elseif($fld == "returnuser" && is_null($val)) {
						$data['rows'][$i][$fld] = $include_html ? "<div class=center>-</span>" : "N/A";
					} elseif($fld == "notify") {
						if(is_null($row['returnuser'])) {
							$data['rows'][$i][$fld] = $include_html ? "<div class=center>-</span>" : "N/A";
						} else {
							$data['rows'][$i][$fld] = $include_html ? "<div class=center>".ASSIST_HELPER::getDisplayStatusAsDiv($val != 0 ? "ok" : "error").($val != 0 ? "Yes" : "No")."</div>" : ($val != 0 ? "Yes" : "No");
						}
					} elseif($fld == "signoff") {
						$data['rows'][$i][$fld] = $include_html ? "<div class=center>".ASSIST_HELPER::getDisplayStatusAsDiv($val != 0 ? "ok" : "error").($val != 0 ? "Yes" : "No")."</div>" : ($val != 0 ? "Yes" : "No");
					}
				}
			}
			//return
			return $data;
		}
	}

	public function getAssuranceHistoryForReport($sdbp_id, $kpi_id, $time_id) {
		$include_html = false;
		$assurance_history = $this->getAssuranceHistory($kpi_id, $time_id, $include_html);
		$assurance_history_rows = $assurance_history['rows'];

		$assurance_logs = "";
		if(count($assurance_history_rows) > 0) {
			$assurance_history_rows_reversed = array_reverse($assurance_history_rows);
			$val = $assurance_history_rows_reversed[0];

			$assurance_logs .= "<span style=\"font-weight:bold\">Current Assurance Status:</span><br/>";
			$assurance_logs .= $val['comment'].";<br/>";
			$assurance_logs .= "Update user: ".$val['insertuser'].";<br/>";
			$assurance_logs .= (isset($val['returnuser']) && $val['returnuser'] != "N/A" ? $assurance_history['head']['returnuser'].": ".$val['returnuser'].";<br/>" : "");
			$assurance_logs .= (isset($val['deadline']) && $val['deadline'] != "N/A" ? $assurance_history['head']['deadline'].": ".$val['deadline'].";<br/>" : "");

			/**********************************************************************************************************/
			$time_odject = new SDBP6_SETUP_TIME();
			$time_name = $time_odject->getATimePeriodName($sdbp_id, $time_id, "MONTH");
			$var['log_object_id'] = $kpi_id;
			$sdbp_log_object = new SDBP6_LOG($this->getMyLogTable());
			$assurance_change_logs = $sdbp_log_object->getAssuranceAuditLogHTMLFormattedForReport($var, $time_name);

			$assurance_logs .= "<span style=\"font-weight:bold\">Assurance History:</span><br/>";
			$assurance_logs .= $assurance_change_logs;

		} else {
			$assurance_logs .= "No Assurance History found.";
		}

		return $assurance_logs;
	}

	public function getApproveHistoryForReport($sdbp_id, $kpi_id, $time_id) {
		$include_html = false;
		$assurance_history = $this->getApproveHistory($kpi_id, $time_id, $include_html);
		$assurance_history_rows = $assurance_history['rows'];

		$assurance_logs = "";
		if(count($assurance_history_rows) > 0) {
			$assurance_history_rows_reversed = array_reverse($assurance_history_rows);
			$val = $assurance_history_rows_reversed[0];

			$assurance_logs .= "<span style=\"font-weight:bold\">Current Approve Status:</span><br/>";
			$assurance_logs .= $val['comment'].";<br/>";
			$assurance_logs .= "Update user: ".$val['insertuser'].";<br/>";
			$assurance_logs .= (isset($val['returnuser']) && $val['returnuser'] != "N/A" ? $assurance_history['head']['returnuser'].": ".$val['returnuser'].";<br/>" : "");
			$assurance_logs .= (isset($val['deadline']) && $val['deadline'] != "N/A" ? $assurance_history['head']['deadline'].": ".$val['deadline'].";<br/>" : "");

			/**********************************************************************************************************/
			$time_odject = new SDBP6_SETUP_TIME();
			$time_name = $time_odject->getATimePeriodName($sdbp_id, $time_id, "MONTH");
			$var['log_object_id'] = $kpi_id;
			$sdbp_log_object = new SDBP6_LOG($this->getMyLogTable());
			$assurance_change_logs = $sdbp_log_object->getApproveAuditLogHTMLFormattedForReport($var, $time_name);

			$assurance_logs .= "<span style=\"font-weight:bold\">Approve History:</span><br/>";
			$assurance_logs .= $assurance_change_logs;

		} else {
			$assurance_logs .= "No Approve History found.";
		}

		return $assurance_logs;
	}


	/**************************************
	 * FRONT PAGE DASHBOARD FUNCTIONS
	 */

	public function getLoginRecords($return_records, $time, $org, $owner, $future, $zero_calc_types, $headings, $sdbip_id = 0) {
		//if(count($time) > 0) {
		if($return_records === true) {
			$select_sql = " K.*, kr.*, ".$this->getResultsTimeFieldName()." as time_id 
								, ".$this->getResultsTableField()."_approve as approve_status
								, ".$this->getResultsTableField()."_approve_updatedeadline as approve_deadline
								, ".$this->getResultsTableField()."_assurance as assurance_status
								, ".$this->getResultsTableField()."_assurance_updatedeadline as assurance_deadline
								";
			$group_by = "";
		} else {
			$select_sql = " count(".$this->getIDFieldName().") as count
								, ".$this->getResultsTimeFieldName()." as time_id 
								, ".$this->getResultsTableField()."_approve as approve_status
								, ".$this->getResultsTableField()."_approve_updatedeadline as approve_deadline
								, ".$this->getResultsTableField()."_assurance as assurance_status
								, ".$this->getResultsTableField()."_assurance_updatedeadline as assurance_deadline
								 ";
			$group_by = " GROUP BY 
								".$this->getResultsTableField()."_approve 
								, ".$this->getResultsTableField()."_approve_updatedeadline
								, ".$this->getResultsTableField()."_assurance 
								, ".$this->getResultsTableField()."_assurance_updatedeadline 
								, ".$this->getResultsTimeFieldName()."";
		}

		$filter_sql = array();
		if(count($org) > 0) {
			$filter_sql[] = " ".$this->getDepartmentFieldName()." IN (".implode(",", $org).") ";
		}
		if(count($owner) > 0) {
			$filter_sql[] = " ".$this->getOwnerFieldName()." IN (".implode(",", $owner).") ";
		}
		$target_sql = array(" kr.".$this->getResultsRevisedFieldName()." > 0");
		if(count($zero_calc_types) > 0) {
			$target_sql[] = "K.".$this->getCalcTypeTableField()." IN (".implode(",", array_keys($zero_calc_types)).") ";
		}
		/* FETCH where:
		user has owner access & update not complete for open time periods with deadline
		OR user has been assigned approve rejected (status > REVIEW_OK, updateuser = this user, update deadline hasn't passed & update deadline has been set)
		OR user has been assigned assurance rejected
		*/
		$sql = "SELECT $select_sql
				FROM ".$this->getTableName()." K
				INNER JOIN ".$this->getResultsTableName()." kr
				  ON ".$this->getIDFieldName()." = ".$this->getResultsParentFieldName()."
				WHERE ".$this->getActiveStatusSQL("K")."
				AND ( ";
		if(count($time) > 0) {
//Don't need to filter by SDBIP_id = time ids do that automatically
			$sql .= "		 
					(
						".$this->getResultsTimeFieldName()." IN (".implode(",", array_keys($time)).")
				AND ".$this->getResultsTableField()."_update < ".SDBP6::UPDATE_COMPLETE."
				AND ".$this->getResultsTableField()."_approve = 0
				AND ".$this->getResultsTableField()."_assurance = 0
				".(count($filter_sql) > 0 ? " AND (".implode(" OR ", $filter_sql).") " : "")."
				".(count($target_sql) > 0 ? " AND (".implode(" OR ", $target_sql).") " : "")."
					) OR";
		}
//add SDBIP_id to only pull up approve/assurance KPIs related to the given SDBIP
		$sql .= " (
						".$this->getResultsTableField()."_approve = ".self::REVIEW_REJECT."
						AND ".$this->getResultsTableField()."_approve_updateuser = '".$this->getUserID()."'
						AND ".$this->getResultsTableField()."_approve_updatedeadline >= '".date("Y-m-d")."'
						AND ".$this->getResultsTableField()."_approve_updatedeadline <> '0000-00-00'
						AND ".$this->getParentFieldName()." = ".$sdbip_id." 
					) OR (
						".$this->getResultsTableField()."_assurance = ".self::REVIEW_REJECT."
						AND ".$this->getResultsTableField()."_assurance_updateuser = '".$this->getUserID()."'
						AND ".$this->getResultsTableField()."_assurance_updatedeadline >= '".date("Y-m-d")."'
						AND ".$this->getResultsTableField()."_assurance_updatedeadline <> '0000-00-00'
						AND ".$this->getParentFieldName()." = ".$sdbip_id." 
					)
				)
				$group_by ";
		if($return_records === true) {
			$rows = $this->mysql_fetch_all($sql);
		} else {
			//AA-254
			//$rows = $this->mysql_fetch_value_by_id($sql,"time_id","count");
			$rows = $this->mysql_fetch_all($sql);
		}
		//} else {
		//	$rows = array();
		//}

		return $rows;

	}


	public function getLoginStats($time, $org, $owner, $future, $zero_calc_types, $sdbip_id = 0) {
		$data = array('present' => 0, 'future' => 0);

		$rows = $this->getLoginRecords(false, $time, $org, $owner, $future, $zero_calc_types, array(), $sdbip_id);
		foreach($rows as $r) {
			if($r['assurance_status'] == self::REVIEW_REJECT) {
				if($r['assurance_deadline'] == date("Y-m-d")) {
					$data['present'] += $r['count'];
				} else {
					$data['future'] += $r['count'];
				}
			} elseif($r['approve_status'] == self::REVIEW_REJECT) {
				if($r['approve_deadline'] == date("Y-m-d")) {
					$data['present'] += $r['count'];
				} else {
					$data['future'] += $r['count'];
				}
			} else {
				$time_id = $r['time_id'];
				if(isset($time[$time_id]['deadline_type']) && strlen($time[$time_id]['deadline_type']) > 0 && isset($data[$time[$time_id]['deadline_type']])) {
					$data[$time[$time_id]['deadline_type']] += $r['count'];
				}
			}
		}
		//AA-254
//		foreach($rows as $time_id => $count) {
//			if(isset($time[$time_id]['deadline_type']) && strlen($time[$time_id]['deadline_type'])>0 && isset($data[$time[$time_id]['deadline_type']])) {
		//		$data[$time[$time_id]['deadline_type']]+=$count;
		//	}
		//}
		return $data;
	}


	public function getLoginTableObjects($time, $org, $owner, $future, $headings, $all_time, $sdbip_id) {
		$actions = array();
		//target types
		$ttObject = new SDBP6_SETUP_TARGETTYPE($this->local_modref);
		$target_types = $ttObject->getObjectsForSetup();
		//check headings for objects/lists
		$calcTypeObj = new SDBP6_SETUP_CALCTYPE();
		$zero_calc_types = $calcTypeObj->getZeroResultCalculationTypesForDashboard();
		//get lists
		$all_my_lists = array();
		foreach($headings as $fld => $head) {
			switch($head['type']) {
				case "MULTILIST":
				case "LIST":
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$listObject = new SDBP6_LIST($head['list_table']);
						$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
						unset($listObject);
					}
					break;
				case "MULTIOBJECT":
				case "OBJECT":
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_name = $head['list_table'];
						$extra_info = array();
						if(strpos($list_object_name, "|") !== false) {
							$lon = explode("|", $list_object_name);
							$list_object_name = $lon[0];
							$extra_info = $lon[1];
						}
						$listObject = new $list_object_name();
						$all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect($extra_info);
						unset($listObject);
					}
					break;
				case "MULTISEGMENT":
				case "SEGMENT":
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_type = $head['list_table'];
						$listObject = new SDBP6_SEGMENTS($list_object_type);
						$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect(false);
						unset($listObject);
					}
					break;
			}
		}
		$rows = $this->getLoginRecords(true, $time, $org, $owner, $future, $zero_calc_types, $headings, $sdbip_id);
		foreach($rows as $i => $row) {
			$time_id = $row['time_id'];
			$obj_id = $row[$this->getIDFieldName()];
			$tt_id = $row[$this->getTargetTypeTableField()];
			//if(isset($time[$time_id]['my_deadline']) && $time[$time_id]['my_deadline']>0) {
			$row['time'] = $all_time[$time_id]['name'];
			//AA-254 if assurance || approve then use that deadline else time period deadline
//				$row['deadline'] = date("d-M-Y",$time[$time_id]['my_deadline']);
//				$row['sort_by'] = $time[$time_id]['my_deadline']."_".$time_id."_".$obj_id;
			if($row['assurance_status'] == self::REVIEW_REJECT) {
				$row['deadline'] = date("d-M-Y", strtotime($row['assurance_deadline']));
				//sort assurance updates first
				$row['sort_by'] = "1_".strtotime($row['assurance_deadline'])."_".$time_id."_".$obj_id;
			} elseif($row['approve_status'] == self::REVIEW_REJECT) {
				$row['deadline'] = date("d-M-Y", strtotime($row['approve_deadline']));
				//sort approve updates after assurance
				$row['sort_by'] = "5_".strtotime($row['approve_deadline'])."_".$time_id."_".$obj_id;
			} else {
				$row['deadline'] = date("d-M-Y", $all_time[$time_id]['my_deadline']);
				//sort normal updates last
				$row['sort_by'] = "9_".$time[$time_id]['my_deadline']."_".$time_id."_".$obj_id;
			}
			foreach($headings as $fld => $head) {
				if($fld == $this->getIDFieldName()) {
					$row[$fld] = $this->getRefTag().$obj_id;
				} elseif($head['apply_formatting'] == true) {
					//apply target type formatting
					$row[$fld] = $ttObject->formatNumberBasedOnTargetType($row[$fld], $tt_id, $target_types[$tt_id]);
				}
				//replace lists/objects with values
			}
			$row['link'] = "manage_update_dept_object.php?object_id=".$obj_id."|time_id=".$time_id."|page_src=dashboard|sdbip_id=".$sdbip_id;
			foreach($headings as $fld => $head) {
				if(isset($all_my_lists[$fld])) {
					$row[$fld] = isset($all_my_lists[$fld][$row[$fld]]) ? $all_my_lists[$fld][$row[$fld]] : $this->getUnspecified();
				}
			}
			$actions[$obj_id.'_'.$time_id] = $row;
			//}
		}

		uasort($actions, function($a, $b) {
			return $a['sort_by'] > $b['sort_by'];
		});
		return $actions;
	}


	public function generateStatusDisplay($result, $process_zero_targets = false, $time_has_started = false) {
		$status = array();
		$kr_fld = $this->getResultsTableField()."_update";
		if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
			if($result[$this->getResultsRevisedFieldName()] == 0 && !$process_zero_targets) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Update Not Applicable\"", "default");
			} elseif($time_has_started != true) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Update not yet due\"", "light-grey");
			} else {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done", "title=\"Update due but not yet started\"", "red");
			}
		} elseif($result[$kr_fld] == SDBP6::UPDATE_IN_PROGRESS) {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("info", "title=\"Update in progress\"", "orange");
		} else {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done", "title=\"Update Completed\"", "green");
		}
		$kr_fld = $this->getResultsTableField()."_approve";
		if($result[$kr_fld] == SDBP6::NOT_REVIEWED) {
			if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Approve - No update to review\"", "default");
			} else {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Approve - Not yet reviewed\"", "orange");
			}
		} elseif($result[$kr_fld] == SDBP6::REVIEW_OK) {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done", "title=\"Approve - Update signed off\"", "green");
		} else {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done", "title=\"Approve - Update rejected\"", "red");
		}
		$kr_fld = $this->getResultsTableField()."_assurance";
		if($result[$kr_fld] == SDBP6::NOT_REVIEWED) {
			if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Assurance - No update to review\"", "default");
			} else {
				$status[] = ASSIST_HELPER::getDisplayIconAsDiv("no-entry", "title=\"Assurance - Not yet reviewed\"", "orange");
			}
		} elseif($result[$kr_fld] == SDBP6::REVIEW_OK) {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("done", "title=\"Assurance - Update signed off\"", "green");
		} else {
			$status[] = ASSIST_HELPER::getDisplayIconAsDiv("not-done", "title=\"Assurance - Update rejected\"", "red");
		}
		return "<div style='width:100%; padding-bottom:3px;' class=center>".implode("</div><div style='width:100%; padding-bottom:3px;' class=center>", $status)."</div>";
	}

	public function generateAssuranceStatusForReport($result) {
		$status = "";
		$kr_fld = $this->getResultsTableField()."_assurance";
		if($result[$kr_fld] == SDBP6::NOT_REVIEWED) {
			if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
				$status .= "Assurance - No update to review";
			} else {
				$status .= "Assurance - Not yet reviewed";
			}
		} elseif($result[$kr_fld] == SDBP6::REVIEW_OK) {
			$status .= "Assurance - Update signed off";
		} else {
			$status .= "Assurance - Update rejected";
		}
		return $status;
	}

	public function generateApproveStatusForReport($result) {
		$status = "";
		$kr_fld = $this->getResultsTableField()."_approve";
		if($result[$kr_fld] == SDBP6::NOT_REVIEWED) {
			if($result[$kr_fld] == SDBP6::UPDATE_NOT_STARTED) {
				$status .= "Approve - No update to review";
			} else {
				$status .= "Approve - Not yet reviewed";
			}
		} elseif($result[$kr_fld] == SDBP6::REVIEW_OK) {
			$status .= "Approve - Update signed off";
		} else {
			$status .= "Approve - Update rejected";
		}
		return $status;
	}


	/*****************************************************************************************************************************
	 * IMPORT / CONVERSION functions
	 */

	public function getAltModuleTable($modloc) {
		switch($modloc) {
			case "SDBP5B":
				$table = "kpi";
				break;
			case "SDBP6":
				$table = SDBP6_DEPTKPI::TABLE;
				break;
			case "IDP3":
			default:
				$table = false;
				break;
		}
		return $table;
	}

	public function getAltModuleIDField($modloc, $modref = "") {
		switch($modloc) {
			case "SDBP5B":
				$table = "kpi_id";
				break;
			case "SDBP6":
				$table = $this->getIDFieldName();
				break;
			case "IDP3":
			default:
				$table = false;
				break;
		}
		return $table;
	}

	public function importGetResult($var) {
		$obj_id = $var['obj_id'];
		$sql = "SELECT *, ROUND(".$this->getResultsOriginalFieldName().",2) as ".$this->getResultsOriginalFieldName()." 
				FROM ".$this->getResultsTableName()." 
				WHERE ".$this->getResultsParentFieldName()." = ".$obj_id;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getResultsSecondaryParentFieldName());
		return $rows;
	}

	public function importSaveResult($var) {
		$obj_id = $var['obj_id'];
		$original = $var[$this->getResultsOriginalFieldName()];
		$description = $var[$this->getResultsDescriptonFieldName()];
		foreach($original as $time_id => $o) {
			if(strlen($o) == 0) {
				$o = 0;
			}
			$d = $description[$time_id];
			$sql = "UPDATE ".$this->getResultsTableName()." 
					SET ".$this->getResultsOriginalFieldName()." = '".$o."',
					".$this->getResultsRevisedFieldName()." = '".$o."',
					".$this->getResultsDescriptonFieldName()." = '".ASSIST_HELPER::code($d)."'
					WHERE ".$this->getResultsParentFieldName()." = $obj_id
					AND ".$this->getResultsSecondaryParentFieldName()." = $time_id ";
			$this->db_update($sql);
		}
		$result = array("ok", "Results saved successfully.");
		return $result;
	}


	/*********************************************************************************************************************************************************
	 * Reporting functions
	 */
	public function getSummaryOfResultsByOrgTopID($sdbip_id, $dir_id, $limit = "YTD", $is_sub = false) {
		$data = $this->getSummaryOfResultsForGraphReporting($sdbip_id, $dir_id, array('type' => "OBJECT", 'table' => "SDBP6_SETUP_ORGSTRUCTURE", 'field' => $this->getDepartmentFieldName()), $limit, $is_sub);
		/*//echo "<hr />"; $this->markTime("start of function");
		$data = array();
		$is_entire_mun = $dir_id==false || $dir_id=="ALL";
		//get list of (sub-)directorates by top id
		$this->markTime("start of get org data");
		$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
		if(!$is_entire_mun) {
			if($is_sub) {
				$org_list = array($dir_id => $dir_id);
			} else {
				$org_list = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => $dir_id, 'include_parent_name' => false));
			}
			//get name of top level
			$org_name = $orgObject->getAObjectName($dir_id);
		} else {
			$org_list = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => 0, 'include_parent_name' => false));
			$org_name = $this->getCmpName();
		}
		$data['org_list'] = $org_list;
		$data['org_name'] = $org_name;
		$this->markTime("end get org data");
		$data['results'] = array();

		if(count($org_list)>0){
			//get kpis in the dir
			$this->markTime("start of sql");
			$sql = "SELECT *, ".$this->getTargetTypeTableField()." as target_type_id, ".$this->getCalcTypeTableField()." as calc_type_id FROM ".$this->getTableName()." O WHERE ".$this->getStatusSQL("O")." AND ".$this->getParentFieldName()." = ".$sdbip_id;
			if(!$is_entire_mun) {
				$sql.= " AND ".$this->getDepartmentFieldName()." IN (".implode(",",array_keys($org_list)).")";
			}
			$objects = $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
			$this->markTime("end of sql");
			//get results
			$headings = array(
				$this->getResultsFieldName("RESULTS")=>array(
					'type'=>"RESULT"
				)
			);
			$this->markTime("start of get results for list pages");
			$data_results = $this->getResultsForListPages($objects,array(),$headings, "YTD", false,$limit,true);
			$results = $data_results['rows'];
			$time = $data_results['time'];
			$data['time'] = $time;
			$this->markTime("end of get results for list pages - start of processing");
			$count = array();
			$keys = array_keys($objects);
			$first = $keys[0];
			$time_keys = array_keys($results[$first]);
			$ytd_time = $time_keys[count($time_keys)-1];
			foreach($objects as $i => $obj) {
				$res = $results[$i][$ytd_time];
				$r = $res[$this->getResultsFieldName("RESULTS")];
				if(!isset($count['ALL'][$r])) {
					$count['ALL'][$r]=1;
				} else {
					$count['ALL'][$r]++;
				}
				if(!isset($count[$obj[$this->getDepartmentFieldName()]][$r])) {
					$count[$obj[$this->getDepartmentFieldName()]][$r]=1;
				} else {
					$count[$obj[$this->getDepartmentFieldName()]][$r]++;
				}
			}
			if(!$is_entire_mun) {
				$data['results'] = $count;
			} else {
				$count2 = array();
				$count2['ALL'] = $count['ALL'];
				unset($count['ALL']);
				$org_parents = $orgObject->getParentOfChildren();
				if(isset($count[0])) {
					$org_parents = array(0=>0)+$org_parents;
					$data['org_list'] += array(0=>$this->getUnspecified());
				}
				foreach($count as $c => $r) {
					$p = $org_parents[$c];
					foreach($r as $k => $v) {
						if(!isset($count2[$p][$k])) {
							$count2[$p][$k] = 0;
						}
						$count2[$p][$k]+=$v;
					}
				}
				$data['results'] = $count2;
			}
		}//ASSIST_HELPER::arrPrint($data);
		$this->markTime("end of function");*/
		return $data;
	}


	/**
	 * @param $sdbip_id
	 * @param $filter_id
	 * @param $group_by
	 * @param array $limit
	 * @param bool $is_sub
	 * @return array
	 * in use on report_graphs_kpi_process.php
	 */
	public function getSummaryOfResultsForGraphReporting($sdbip_id, $filter, $group_by, $limit, $is_sub = false,$mscoa_version_id=1) {
		//echo "<hr />"; $this->markTime("start of function");
		$data = array();
		if(is_array($filter) && isset($filter['dir_filter'])) {
			$filter_id = $filter['dir_filter'];
			unset($filter['dir_filter']);
		} elseif(is_array($filter)) {
			$filter_id = 0;
		} else {
			$filter_id = $filter;
		}
		$dir_id = $filter_id;//shortcut until other filters added
		$group_by_field = $group_by !== false ? $group_by['field'] : false;
		$is_entire_mun = $dir_id == false || $dir_id == "ALL";
		if(!$is_entire_mun) {
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$orgObject->setSDBIPID($sdbip_id);
			$org_name = $orgObject->getAObjectName($dir_id);
			$department_list = $orgObject->getActiveListItemForGraphReports(array('parent_id' => $dir_id, 'include_parent_name' => false));
			//unset the object if grouping by something other than org structure. DO NOT UNSET IF DISPLAYING LOWEST LEVEL (i.e. Grouping=false)!!!
			if($group_by !== false && $group_by['table'] != "SDBP6_SETUP_ORGSTRUCTURE") {
				unset($orgObject);
			}
		} else {
			$org_name = $this->getCmpName();
		}
		$data['org_name'] = $org_name;
		//get list of (sub-)directorates by top id
		$this->markTime("start of get group data");
		$group_by_segment = false;
		if($group_by !== false && !isset($orgObject)) {
			switch($group_by['type']) {
				case "OBJECT":
				case "MULTIOBJECT":
					$class = $group_by['table'];
					//if($class != "SDBP6_SETUP_ORGSTRUCTURE" || !isset($orgObject)) {
					$orgObject = new $class();
					//}
					break;
				case "SEGMENT":
				case "MULTISEGMENT":
					$orgObject = new SDBP6_SEGMENTS($group_by['table'],false,$mscoa_version_id);
					$group_by_segment = true;
					break;
				case "LIST":
				case "MULTILIST":
					$orgObject = new SDBP6_LIST($group_by['table']);
					break;
			}
		}
		$orgObject->setSDBIPID($sdbip_id);
		if($group_by===false || ($group_by['type']=="OBJECT" && $group_by['table']=="SDBP6_SETUP_ORGSTRUCTURE")) {
			if(!$is_entire_mun) {
				if($is_sub) {
					$org_list = array($dir_id => $dir_id);
				} else {
					$org_list = $orgObject->getActiveListItemForGraphReports(array('parent_id' => $dir_id, 'include_parent_name' => false));
				}
			} else {
				$org_list = $orgObject->getActiveListItemForGraphReports(array('parent_id' => 0, 'include_parent_name' => false))+array(0=>$this->getUnspecified());
			}
		} else {
			if($group_by_segment) {
				/** @var SDBP6_SEGMENTS $orgObject */
				$org_list = $orgObject->getActiveListItemForGraphReports(array('version' => $mscoa_version_id, 'include_parent_name' => false))+array(0=>$this->getUnspecified());
			} else {
				$org_list = $orgObject->getActiveListItemForGraphReports(array('parent_id' => 0, 'include_parent_name' => false))+array(0=>$this->getUnspecified());
			}
		}
		$data['org_list'] = $org_list;
		$this->markTime("end get org data");
		$data['results'] = array();
		if($group_by['field'] == "kpi_sub_id") {
			$org_parents = $orgObject->getParentOfChildren();
		} else {
			$org_parents = array();
			foreach($org_list as $key => $o) {
				$org_parents[$key] = $key;
			}
		}
		if(count($org_list) > 0) {
			//get kpis in the dir
			$this->markTime("start of sql");
			$sql = "SELECT *, ".$this->getTargetTypeTableField()." as target_type_id, ".$this->getCalcTypeTableField()." as calc_type_id FROM ".$this->getTableName()." O WHERE ".$this->getStatusSQL("O")." AND ".$this->getParentFieldName()." = ".$sdbip_id;
			if($is_sub) {
				$sql .= " AND ".$this->getDepartmentFieldName()." = $dir_id ";
			} elseif(!$is_entire_mun) {
				$sql .= " AND ".$this->getDepartmentFieldName()." IN (".implode(",",array_keys($department_list)).") ";
			}
			if(is_array($filter) && count($filter) > 0) {
				foreach($filter as $filter_fld => $filter_key) {
					$sql .= " AND ".$filter_fld." = ".$filter_key;
				}
			}
			$objects = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
			$this->markTime("end of sql");
			//get results
			$this->markTime("start of get results for list pages");
			$data_results = $this->getResultsForGraphs($objects, $limit);
			$results = $data_results['rows'];
			$time = $data_results['time'];
			if($limit == "YTD") {
				$ti = array_keys($time);
				$limit = array('start' => $ti[0], 'end' => $ti[count($ti) - 1]);
			}
			$data['time'] = $time;
			$this->markTime("end of get results for list pages - start of processing");
			$count = array();
			$ytd_time = $limit['end'];
			foreach($objects as $i => $obj) {
				$r = $results[$i][$ytd_time];
				if(!isset($count['ALL'][$r])) {
					$count['ALL'][$r] = 1;
				} else {
					$count['ALL'][$r]++;
				}
				if($group_by_field !== false) {
					if(!isset($count[$obj[$group_by_field]][$r])) {
						$count[$obj[$group_by_field]][$r] = 1;
					} else {
						$count[$obj[$group_by_field]][$r]++;
					}
				}
			}
			if(!$is_entire_mun) {
				$data['results'] = $count;
			} else {
				$count2 = array();
				$count2['ALL'] = isset($count['ALL']) ? $count['ALL'] : 0;
				unset($count['ALL']);
				if(isset($count[0])) {
					$org_parents = array(0 => 0) + $org_parents;
					$data['org_list'] += array(0 => $this->getUnspecified());
				}
				foreach($count as $c => $r) {
					$p = $org_parents[$c];
					foreach($r as $k => $v) {
						if(!isset($count2[$p][$k])) {
							$count2[$p][$k] = 0;
						}
						$count2[$p][$k] += $v;
					}
				}
				$data['results'] = $count2;
			}
		}
		$this->markTime("end of function");
		return $data;
	}


	/***********************************************************************************
	 * LINKED TO TOP LAYER
	 */
	public function getCompleteListOfKPIsLinkedToTL($sdbip_id) {
		$data = array();
		//get all active KPIs linked to top layer KPIs
		$sql = "SELECT * FROM ".$this->getTableName()." O 
				WHERE ".$this->getParentFieldName()." = ".$sdbip_id." 
				AND ".$this->getTertiaryParentFieldName()." > 0 
				AND ".$this->getActiveStatusSQL("O");
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		//if rows returned in previous SQL, then get results for those rows
		if(count($rows)) {
			$keys = array_keys($rows);
			$sql = "SELECT * FROM ".$this->getResultsTableName()." WHERE ".$this->getResultsParentFieldName()." IN (".implode(",", $keys).")";
			$results = $this->mysql_fetch_all_by_id2($sql, $this->getResultsParentFieldName(), $this->getResultsSecondaryParentFieldName());
			foreach($rows as $i => $r) {
				$r['results'] = $results[$i];
				$data[$r[$this->getTertiaryParentFieldName()]][$i] = $r;
			}
		}
		return $data;
	}


	/*********************************************************
	 * END
	 */


	public function __destruct() {
		parent::__destruct();
	}

}

?>