<?php
/**
 * To manage the Target Types of the SDBP6 module for formatting of KPI results
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_TARGETTYPE extends SDBP6 {

	protected $object_id = 0;
	protected $object_details = array();
	protected $all_object_details = array();

	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = null;//"";
	protected $name_field = "name";
	protected $attachment_field = null;

	protected $has_attachment = false;

	protected $ref_tag = "TT";


	protected $display_type_options = array(
		'pre' => "Before Number",
		'post' => "After Number",
	);
	/*	protected $calculation_options = array(
			'SUM'		=> "Sum / Addition",
			'AVE_T'		=> "Average over number of targets",			//counts revised targets
			'AVE_E'		=> "Average over the number of entries",		//counts revised targets && actuals if updated
			'MAX'		=> "Max / Highest entry",
			'RECENT'	=> "Most recent entry",							//actuals only included if updated
			'NA'		=> "No Calculation",
		);
	*/
	protected $target_type_headings = array(
		'id' => array(
			'field' => "id",
			'name' => "Ref",
			'type' => "REF",
			'table' => "",
			'finance_option' => "0",
		),
		'name' => array(
			'field' => "name",
			'name' => "Name",
			'type' => "MEDVC",
			'table' => "",
			'finance_option' => "",
		),
		'code' => array(
			'field' => "code",
			'name' => "Symbol",
			'type' => "SMLVC",
			'table' => "",
			'finance_option' => "",
		),
		'display_type' => array(
			'field' => "display_type",
			'name' => "Symbol Display Position",
			'type' => "LIST",
			'table' => "display_type",
			'finance_option' => "pre",
		),
		'format' => array(
			'field' => "format",
			'name' => "Force Decimals",
			'type' => "BOOL",
			'table' => "",
			'finance_option' => true,
		),
	);

	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "TARGETTYPE";
	const OBJECT_NAME = "TARGETTYPE";
	const PLURAL_OBJECT_NAME = "TARGETTYPES";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "setup_target_types";
	const TABLE_FLD = "tt";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "setup";
	const LOG_SECTION = "targettype";


	public function __construct($modref = "", $object_id = -1, $autojob = false) {
		parent::__construct($modref, $autojob);
		$this->has_internal_ref_field = false;
		if($object_id >= 0) {
			$this->object_id = $object_id;
			$this->object_details = $this->getRawObjectDetails($object_id);
		}
	}


	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");

		$var['status'] = self::ACTIVE;
		$var['insertuser'] = $this->getUserID();
		$var['insertdate'] = date("Y-m-d H:i:s");

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);

		if(ASSIST_HELPER::checkIntRef($id)) {
			$result = array("ok", "New ".$this->getObjectName(self::OBJECT_TYPE)." ".$this->getRefTag().$id." added successfully.");

			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|created| a new |".self::OBJECT_TYPE."| ".$this->getRefTag().$id.": ".$var[$this->getNameFieldName()],
			);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error", "An error occurred while trying to add the new ".$this->getObjectName(self::OBJECT_TYPE).".  Please try again.");
		}

		return $result;
	}

	public function editObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");

		$object_id = $var['object_id'];
		unset($var['object_id']);
		//get current details
		$old_row = $this->getRawObjectDetails($object_id);
		//scan submitted data for changes
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|TARGETTYPE| ".$this->getRefTag().$object_id." edited.",
		);
		$list_items = $this->getListsForSetup();
		$headings = $this->getTargetTypeHeadings(false);
		foreach($var as $fld => $new) {
			$old = $old_row[$fld];
			if($new != $old) {
				$head = $headings[$fld];
				switch($head['type']) {
					case "BOOL_BUTTON":
						$n = $new == 1 ? "Yes" : "No";
						$o = $old == 1 ? "Yes" : "No";
						break;
					case "LIST":
						$t = $head['table'];
						$n = isset($list_items[$t][$new]) ? $list_items[$t][$new] : $this->getUnspecified();
						$o = isset($list_items[$t][$old]) ? $list_items[$t][$old] : $this->getUnspecified();
						break;
					default:
						$n = $new;
						$o = $old;
						break;
				}
				$changes[$head['name']] = array(
					'to' => $n,
					'from' => $o,
					'raw' => array(
						'to' => $new,
						'from' => $old,
					),
				);
			}
		}
		//if changes found then save to database and log
		if(count($changes) > 0) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var)." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$mar = $this->db_update($sql);
			$log_var = array(
				'section' => self::LOG_SECTION,
				'object_id' => $object_id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			//set ok result
			$result = array("ok", "Changes saved successfully.");
		} else {
			//otherwise return error message that no changes were found
		}


		return $result;
	}

	public function deactivateObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");
		$object_id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|TARGETTYPE| ".$this->getRefTag().$object_id." |deactivated|.",
		);
		$log_var = array(
			'section' => self::LOG_SECTION,
			'object_id' => $object_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::DEACTIVATE,
		);
		$this->addActivityLog("setup", $log_var);
		//set ok result
		$result = array("ok", "Changes saved successfully.");

		return $result;
	}

	public function restoreObject($var) {
		$result = array("info", "Sorry, Nothing found to be saved.");
		$object_id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		$changes = array(
			'user' => $this->getUserName(),
			'response' => "|TARGETTYPE| ".$this->getRefTag().$object_id." |restored|.",
		);
		$log_var = array(
			'section' => self::LOG_SECTION,
			'object_id' => $object_id,
			'changes' => $changes,
			'log_type' => SDBP6_LOG::RESTORE,
		);
		$this->addActivityLog("setup", $log_var);
		//set ok result
		$result = array("ok", "Changes saved successfully.");


		return $result;
	}


	/*****************************************************************************************************************************
	 * GET functions
	 */

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName($plural = false) {
		return self::OBJECT_NAME;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getTargetTypeForFinances() {
		$row = array();
		foreach($this->target_type_headings as $fld => $ct) {
			$row[$fld] = $ct['finance_option'];
		}
		return $row;
	}


	/**
	 * Get list of Calculation Types for editing in Setup > Defaults > Calculation Types
	 * Also called by KPI > getSummaryResults to get all calc type options for calculating overall results
	 */
	public function getObjectsForSetup() {
		$sql = "SELECT * FROM ".$this->getTableName()." ORDER BY name, code, id";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		return $data;
	}


	/**
	 * Get the headings needed for Setup > Defaults > Calculation Types
	 */
	public function getTargetTypeHeadings($for_setup = true) {
		if($for_setup) {
			//can't replace | here with <br /> as character numbers are different so unserialize / json_decode doesn't work
			$a = $this->target_type_headings;
		} else {
			$x = serialize($this->target_type_headings);
			$x = str_replace("|", " ", $x);
			$a = unserialize($x);
		}
		return $a;
	}

	/**
	 * Get any list items needed for drop downs to add / edit in Setup > Defaults > Calculation Types
	 */
	public function getListsForSetup() {
		$lists = array(
			'display_type' => $this->getDisplayTypeOptions(),
		);
		//'calculation' => $this->getCalculationOptions(),
		return $lists;
	}

	public function getDisplayTypeOptions() {
		return $this->display_type_options;
	}
	//public function getCalculationOptions()  { return $this->calculation_options; }

	/**
	 * Get an explanation of the various options to help user in Setup > Defaults > Calculation Types
	 */
	public function getExplanatoryGlossary() {
	}


	public function getAObjectName($object_id) {
		/*		$sql = "SELECT
							org.org_name as name,
							parent.org_name as parent_name,
							org.org_parent_id as parent_id
						FROM ".$this->getTableName()." org
						LEFT OUTER JOIN ".$this->getTableName()." parent
						ON parent.org_id = org.org_parent_id
						WHERE org.org_id = ".$object_id."";
				$row = $this->mysql_fetch_one($sql);
				if($row['parent_id']==0) {
					return $row['name'];
				} else {
					return $row['parent_name']." - ".$row['name'];
				}*/
	}


	public function getRawObjectDetails($object_id) {
		if(is_array($object_id) && isset($object_id['object_id'])) {
			$object_id = $object_id['object_id'];
		}
		if($object_id == 0) {
			return $this->getTargetTypeForFinances();
		} elseif($object_id == $this->object_id && count($this->object_details) > 0) {
			return $this->object_details;
		} elseif(isset($this->all_object_details[$object_id]) && count($this->all_object_details[$object_id]) > 0) {
			return $this->all_object_details[$object_id];
		} else {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$row = $this->mysql_fetch_one($sql);
			$this->all_object_details[$object_id] = $row;
			return $row;
		}
	}


	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getActiveListItemsFormattedForSelect($options = array()) {
		return $this->getActiveObjectsFormattedForSelect($options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($sdbip_id = 0, $options = array()) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , ".$this->getNameFieldName()." as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." ";
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $items;
	}

	/**
	 * Same as getActiveObjectsFormattedForSelect but needed for centralised call in _DISPLAY->getObjectForm();
	 */
	public function getAllListItemsFormattedForSelect($options = array()) {
		return $this->getAllObjectsFormattedForSelect($options);
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getAllObjectsFormattedForSelect($options = array(), $sdbip_id = 0) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , ".$this->getNameFieldName()." as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED." ";
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $items;
	}




























	/*******************************************************************************************************************
	 * PERFORM CALCULATIONS
	 */


	/**
	 * Calculation of ANNUAL OVERALL original_total, revised_total, actual_total
	 */
	public function calculateSummaryResult($raw_data, $calc_type_id, $time_periods, $calc_type_settings = array()) {
//ASSIST_HELPER::arrPrint($time_periods);
		if(count($calc_type_settings) == 0) {
			$calc_type_settings = $this->getRawObjectDetails($calc_type_id);
		}
		$data = array(
			'original_total' => 0,
			'revised_total' => 0,
			'actual_total' => 0,
		);
		$original_total = 0;
		$revised_total = 0;
		$actual_total = 0;
//echo $calc_type_settings['calculation'];
		switch($calc_type_settings['calculation']) {
			case "SUM":
				foreach($time_periods as $t_id => $time) {
//echo "<Br />".$t_id;
					$original_total += $raw_data[$t_id]['original_target'];
					$revised_total += $raw_data[$t_id]['revised_target'];
					$actual_total += $raw_data[$t_id]['actual'];
				}
				$data['original_total'] = $original_total;
				$data['revised_total'] = $revised_total;
				$data['actual_total'] = $actual_total;
				break;
			case "AVE_T":
				$count = 0;
				foreach($time_periods as $t_id => $time) {
					$original_total += $raw_data[$t_id]['original_target'];
					$revised_total += $raw_data[$t_id]['revised_target'];
					$actual_total += $raw_data[$t_id]['actual'];
					if($calc_type_settings['include_zero_targets'] == true) {
						$count++;
					} elseif($raw_data[$t_id]['revised_target'] > 0) {
						$count++;
					}
				}
				$data['original_total'] = $original_total / $count;
				$data['revised_total'] = $revised_total / $count;
				$data['actual_total'] = $actual_total / $count;
				break;
			case "AVE_E":
				$count_t = 0;
				$count_a = 0;
				foreach($time_periods as $t_id => $time) {
					$original_total += $raw_data[$t_id]['original_target'];
					$revised_total += $raw_data[$t_id]['revised_target'];
					$actual_total += $raw_data[$t_id]['actual'];
					if($calc_type_settings['include_zero_targets'] == true) {
						$count_t++;
					} elseif($raw_data[$t_id]['revised_target'] > 0) {
						$count_t++;
					}
					if($raw_data[$t_id]['has_been_updated'] == true) {
						if($calc_type_settings['include_zero_actuals'] == true) {
							$count_a++;
						} elseif($raw_data[$t_id]['actual'] > 0) {
							$count_a++;
						}
					}
				}
				$data['original_total'] = $original_total / $count_t;
				$data['revised_total'] = $revised_total / $count_t;
				$data['actual_total'] = $actual_total / $count_a;
				break;
			case "MAX":
				foreach($time_periods as $t_id => $time) {
					$data['original_total'] = ($raw_data[$t_id]['original_target'] > $data['original_total']) ? $raw_data[$t_id]['original_target'] : $data['original_total'];
					$data['revised_total'] = ($raw_data[$t_id]['revised_target'] > $data['revised_total']) ? $raw_data[$t_id]['revised_target'] : $data['revised_total'];
					$data['actual_total'] = ($raw_data[$t_id]['actual'] > $data['actual_total']) ? $raw_data[$t_id]['actual'] : $data['actual_total'];
				}
				break;
			case "RECENT":
				foreach($time_periods as $t_id => $time) {
					if($calc_type_settings['include_zero_targets'] == true) {
						$data['original_total'] = $raw_data[$t_id]['original_target'];
						$data['revised_total'] = $raw_data[$t_id]['revised_target'];
					} else {
						if($raw_data[$t_id]['original_target'] > 0) {
							$data['original_total'] = $raw_data[$t_id]['original_target'];
						}
						if($raw_data[$t_id]['revised_target'] > 0) {
							$data['revised_total'] = $raw_data[$t_id]['revised_target'];
						}
					}
					if($raw_data[$t_id]['has_been_updated'] == true) {
						if($calc_type_settings['include_zero_actuals'] == true) {
							$data['actual_total'] = $raw_data[$t_id]['actual'];
						} else {
							if($raw_data[$t_id]['original_target'] > 0) {
								$data['actual_total'] = $raw_data[$t_id]['actual'];
							}
						}
					}
				}
				break;
			case "NA":
			default:
				//do nothing
				break;
		}
//ASSIST_HELPER::arrPrint($data);
		return $data;
	}


	/**
	 * Apply format to a given number based on target type settings
	 * NOTE: TARGET TYPE SETTINGS MUST BE SET INTO $this->object_details AHEAD OF TIME
	 */
	public function formatNumberBasedOnTargetType($this_number, $object_id = -1, $target_type = array(), $html_output = true) {
		if($object_id >= 0 && count($target_type) == 0) {
			//get raw details must come before object_id set as get will validate the given object_id against the class object_id
			$this->object_details = $this->getRawObjectDetails($object_id);
			$this->object_id = $object_id;
			$target_type = $this->object_details;
		} elseif(count($target_type) == 0) {
			$target_type = $this->object_details;
		}
		$symbols = $this->getSymbolsForFormatting($object_id, $target_type, $html_output);
		$pre = $symbols['pre'];
		$post = $symbols['post'];

		if(round($this_number, 0) == $this_number && $target_type['format'] != true) {
			$this_number = ASSIST_HELPER::format_number($this_number, "INT");
		} else {
			$this_number = ASSIST_HELPER::format_number($this_number, "FLOAT");
		}
		if($html_output) {
			return $pre.str_replace(" ", "&nbsp;", $this_number).$post;
		} else {
			return trim($pre).$this_number.trim($post);
		}
	}

	public function formatNumberBasedOnTargetTypeForReporting($this_number, $html_output = true, $object_id = -1) {
		$val = $this->formatNumberBasedOnTargetType($this_number, $object_id, array(), $html_output);
		if($html_output) {
			return $val;
		} else {
			return str_replace("&nbsp;", "", $val);
		}
	}


	/**
	 * @param int $object_id
	 * @param array $target_type
	 * @return array
	 */


	public function getSymbolsForFormatting($object_id = -1, $target_type = array(), $html_output = true) {
		if($object_id >= 0 && count($target_type) == 0) {
			//get raw details must come before object_id set as get will validate the given object_id against the class object_id
			$this->object_details = $this->getRawObjectDetails($object_id);
			$this->object_id = $object_id;
			$target_type = $this->object_details;
		} elseif(count($target_type) == 0) {
			$target_type = $this->object_details;
		}

		$pre = isset($target_type['display_type']) && $target_type['display_type'] != "post" ? $target_type['code'].($html_output ? "&nbsp;" : "") : "";
		$post = isset($target_type['display_type']) && $target_type['display_type'] == "post" ? (substr($target_type['code'], 0, 1) != ":" || $html_output ? "&nbsp;" : "").$target_type['code'] : "";

		return array('pre' => $pre, 'post' => $post);

	}


	/*****************************************************************************************************************************
	 * SET functions
	 */


	public function __destruct() {
		parent::__destruct();
	}

}

?>