<?php

/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */
class SDBP6_DISPLAY extends ASSIST_MODULE_DISPLAY {

	//protected $mod_ref;
	protected $default_attach_buttons = true;
	protected $use_secondary_modref = false;
	protected $secondary_modref = "";

	public function __construct($modref = "", $autojob = false, $cmpcode = "") {
		$me = new SDBP6($modref, $autojob, $cmpcode);
		$an = $me->getAllActivityNames();
		$on = $me->getAllObjectNames();
		//$this->mod_ref = $me->getModRef();
		unset($me);
//echo $modref;
		if(strlen($modref) > 0) {
			$this->use_secondary_modref = true;
			$this->secondary_modref = $modref;
		}
//echo "<br />Use secondary? ".($this->use_secondary_modref?"yes":"no")." of ".$this->secondary_modref;
		parent::__construct($an, $on);
	}

	public function disableAttachButtons() {
		$this->default_attach_buttons = false;
	}

	public function enableAttachButtons() {
		$this->default_attach_buttons = true;
	}


	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */


	public function getDataFieldNoJS($type, $val, $options = array()) {
		$d = $this->getDataField($type, $val, $options);
		return $d['display'];
	}

	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 *
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type, $val, $options = array()) {
		$d = $this->getDataField($type, $val, $options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}

	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 *     *
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type, $val, $options = array()) {
		$data = array('display' => "", 'js' => "");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val);
				break;;
			case "NUM":
			case "CALC":
				$data['display'] = $this->getNumberForDisplay($val, 2);
				break;;
			case "CURRENCY":
				if(strlen($val) == 0 || is_null($val)) {
					$val = 0;
				}
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val, $options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right'] == true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val, $options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val, false);
				}
				break;
			case "BOOL":
			case "BOOL_BUTTON":
				$data['display'] = $this->getBoolForDisplay($val, (isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val, $can_edit, $object_type, $object_id, $buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html'] == true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display' => $val, 'js' => "");
			//$data = array('display'=>$type);
		}
		return $data;
	}


	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type, $options = array(), $val = "") {
		//echo "dFF VAL:-".$val."- ARR: "; print_r($options);
		$ff = $this->createFormField($type, $options, $val);
		//if(is_array($ff['display'])) {
		//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}

	/**
	 * Returns string of selected form field
	 *
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type, $options = array(), $val = "") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type) {
			case "REF":
				$data = array('display' => $val, 'js' => "");
				break;
			case "LABEL":
				$data = $this->getLabel($val, $options);
				break;
			case "SMLVC":
				$data = $this->getSmallInputText($val, $options);
				break;
			case "MEDVC":
				$data = $this->getMediumInputText($val, $options);
				break;
			case "LRGVC":
				$data = $this->getLimitedTextArea($val, $options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows'];
					unset($options['rows']);
					$cols = $options['cols'];
					unset($options['cols']);
					$data = $this->getTextArea($val, $options, $rows, $cols);
				} else {
					$data = $this->getTextArea($val, $options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data = $this->getSelect($val, $options, $items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val) > 0) {
						$val2[] = $val;
					}
				} else {
					$val2 = $val;
				}
				$data = $this->getMultipleSelect($val2, $options, $items);
				break;
			case "AUTO":
				$items = $options['options'];
				unset($options['options']);
				$data = $this->getAutoComplete($val, $options, $items);
				break;
			case "DATE": //echo "date!";
				$extra = $options['options'];
				unset($options['options']);
				$options['onSelect'] = true;
				$data = $this->getDatePicker($val, $options, $extra);
				break;
			case "COLOUR":
				$data = $this->getColour($val, $options);
				break;
			case "RATING":
				$data = $this->getRating($val, $options);
				break;
			case "CURRENCY":
				$size = 15;
			case "PERC":
			case "PERCENTAGE":
				$size = !isset($size) ? 5 : $size;
			case "NUM":
				$class = "right";
				if(!isset($size)) {
					if(isset($options['size'])) {
						$size = $options['size'];
						unset($options['size']);
					} else {
						$size = 0;
					}
				}
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
				if(isset($class)) {
					$options['class'] = (isset($options['class']) ? $options['class'] : "")." ".$class;
				}
				$data = $this->getNumInputText($val, $options, 0, $size);
				if($type == "CURRENCY") {
					if(!$has_sym) {
						$data['display'] = "R&nbsp;".$data['display'];
					} elseif(strlen($symbol) > 0) {
						if($symbol_postfix == true) {
							$data['display'] = $data['display']."&nbsp;".$symbol;
						} else {
							$data['display'] = $symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
				} elseif($type == "PERC" || $type == "PERCENTAGE") {
					$data['display'] .= "&nbsp;%";
				}
				break;
			case "BOOL":
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val, $options);
				break;
			case "ATTACH":
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInputSDBP6($val, $options);
				break;
			case "CALC" :
				$options['extra'] = explode("|", $options['extra']);
				$data = $this->getCalculationForm($val, $options);
				break;
			default:
				$data = array('display' => $val, 'js' => "");
				break;
		}
		return $data;
	}


	/**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
	public function drawPageFooter($left = "", $log_table = "", $var = "", $log_id = "") {
		$data = $this->getPageFooter($left, $log_table, $var, $log_id);
		echo $data['display'];
		return $data['js'];
	}

	/**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
	public function getPageFooter($left = "", $log_table = "", $var = "", $log_id = "") {
		$echo = "";
		$js = "";
		if(is_array($var)) {
			$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".(is_array($v) ? implode("|", $v) : $v);
			}
			$var = implode("&", $d);
		}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table) > 0 ? "<span id=".(strlen($log_id) > 0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table>".(strlen($log_table) > 0 ? "<div id=".(strlen($log_id) > 0 ? $log_id."_" : "")."div_audit_log></div>" : "");
		if(strlen($log_table) > 0) {
			$js = "
			$(\"#".(strlen($log_id) > 0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id) > 0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id) > 0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}

			});
			";
		}
		$data = array('display' => $echo, 'js' => $js);
		return $data;
	}









	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */


	/*****
	 * returns the details table for the object it is fed.
	 *
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 *
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new SDBP6_ACTION();
				break;
			case "CONTRACT":
				if($child_type == "ACTION") {
					$myObject = new SDBP6_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new SDBP6_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new SDBP6_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id, false, false, false, array(), false, false);
	}

	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type, $object_id, $include_parent_button = false, $sub_table = false, $view_all_button = false, $button = array(), $compact_view = false, $attachment_buttons = "X") {
		if($attachment_buttons == "X") {
			$attachment_buttons = $this->default_attach_buttons;
		}
		$me = $this->getDetailedView($object_type, $object_id, $include_parent_button, $sub_table, $view_all_button, $button, $compact_view, $attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}

	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type, $object_id, $include_parent_button = false, $sub_table = false, $view_all_button = false, $button = array(), $compact_view = false, $attachment_buttons = true) {
		if($this->use_secondary_modref) {
			$local_modref = $this->secondary_modref;
		} else {
			$local_modref = "";
		}
		//echo "<br />getDetailedView modref: ".$local_modref;
		if(is_array($object_type)) {
			$var = $object_type;
			$object_id = $var['object_id'];
			$object_type = $var['object_type'];
		}
		//echo $object_type." :: ".$object_id;
		$js = "";
		$echo = "";
		//$echo = "abc :: ".$object_type." :: ".$object_id;
		//add code to get js from assist_module_display to trigger on attachment click for download
		if($attachment_buttons) {
			$js .= $this->getAttachmentDownloadJS3($object_type, $object_id);
		}
		$parent_buttons = "";
		/*		switch(strtoupper($object_type)) {
					case "IDP":
						$myObject = new SDBP6_ACTION();
						if($include_parent_button) {
							$parent_buttons = "<input type=button value='".$myObject->getObjectName("deliverable")."' class='float btn_parent' id=DELIVERABLE /> <input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
						}
						break;
					case "SUB-DELIVERABLE":
					case "DELIVERABLE":
						$myObject = new SDBP6_DELIVERABLE();
						if($include_parent_button) {
							$parent_buttons = "<input type=button value='".$myObject->getObjectName("contract")."' class='float btn_parent' id=CONTRACT />";
						}
						break;
					case "CONTRACT":
						$myObject = new SDBP6_CONTRACT();
						if($include_parent_button) {
							$parent_buttons = "<input type=button value='".$myObject->getObjectName("FINANCE")."' class='float btn_parent' id=FINANCE />";
						}
						break;
					case "FINANCE":
						$myObject = new SDBP6_FINANCE();
						break;
					case "TEMPCON":
						$myObject = new SDBP6_TEMPLATE();
						break;
					default:
						$myObject = new SDBP6();
						break;
				}
		*/
		$class_name = "SDBP6_".$object_type;
		$myObject = new $class_name(0, $local_modref);

		if($view_all_button === true) {
			$parent_buttons .= "<input class='float btn_view_all' type=button id='view_all' value='View All' />";
		}
		if($object_type == "FINANCE") {
			$result = $myObject->getDetailedObjectForDisplay($object_id);
			$js = $result['js'];
			$echo = $result['display'];
		} else {
			if($include_parent_button || $view_all_button) {
				$js .= "
						$('input:button.btn_view_all').button();
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type != "CONTRACT" ? "0.5" : "0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
			} //echo "cv: ".$compact_view.":";
			if($object_type != "TEMPCON") {
				$get_object_options = array('type' => "DETAILS", 'id' => $object_id, 'attachment_buttons' => $attachment_buttons, 'compact_view' => ($compact_view));
//				$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons),($compact_view===true?array('page'=>"COMPACT"):array()));
				$object = $myObject->getObject($get_object_options);
				//ASSIST_HELPER::arrPrint($object);
			} else {
				$object = $myObject->getListObject($object_id, "CON");
			}

			$echo .= "
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head" : "")."'>".($object_type == "DELIVERABLE" && $sub_table ? "Sub-" : "").$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2" : "")."' width=100%>";
			foreach($object['head'] as $fld => $head) {
				if(isset($object['rows'][$fld])) {
					$val = $object['rows'][$fld];
					if(is_array($val)) {
						if(isset($val['display'])) {
							$js .= $val['js'];
							$val = $val['display'];
						} else {
							$v = "<table class=th2 width=100%>";
							foreach($val as $a) {
								foreach($a as $b => $c)
									$v .= "<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
							}
							$v .= "</table>";
							$val = $v;
						}
					}
				} else {
					$val = $fld;
				}
				$echo .= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val == $fld ? "class=idelete" : "").">".$val."</td>
						</tr>";
			}
			//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button) > 0) {
				$echo .= "
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}
			$echo .= "</table>";
		}

		return array('display' => $echo, 'js' => $js, 'title' => $myObject->getObjectName($object_type).($object_type != "FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
		//return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type));
	}


















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */


	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}

	public function getPaging($i, $options, $button, $object_type = "", $object_options = array(), $add_button = array(false, "", ""), $filter_by = false) {
		/**************
		 * $options = array(
		 * 'totalrows'=> mysql_num_rows(),
		 * 'totalpages'=> totalrows / pagelimit,
		 * 'currentpage'=> start / pagelimit,
		 * 'first'=> start==0 ? false : 0,
		 * 'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
		 * 'prev'=> start==0 ? false : (start - pagelimit),
		 * 'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
		 * 'pagelimit'=>pagelimit,
		 * );
		 * $object_options = array(
		 * 'type'=>$page_type,
		 * 'section'=>$page_section,
		 * 'page'=>$page_action,
		 * 'filter'=>array(
		 * 'year'=>0,
		 * 'who'=>"all",
		 * 'what'=>"all",
		 * 'when'=>"today",
		 * 'display'=>"limited",
		 * )
		 * );
		 **********************************/
		if(isset($object_options['section'])) {
			$page_section = $object_options['section'];
		}
		$is_update_page = false;
		$is_approve_page = false;
		$is_assurance_page = false;
		$is_view_page = false;
		$is_edit_page = false;
		$is_new_page = false;
		$display_earliest_time_period = false;
		if(isset($object_options['page'])) {
			$page_action = $object_options['page'];
			if(strpos($page_action, "UPDATE") !== false) {
				$is_update_page = true;
				$display_earliest_time_period = true;
			} elseif(strpos($page_action, "APPROVE") !== false) {
				$is_approve_page = true;
			} elseif(strpos($page_action, "ASSURANCE") !== false) {
				$is_assurance_page = true;
			} elseif(strpos($page_action, "VIEW") !== false) {
				$is_view_page = true;
			} elseif(strpos($page_action, "EDIT") !== false) {
				$is_edit_page = true;
			}
		} //echo $page_action;
		/*if(strpos($page_action,"VIEW")!==false || strpos( $page_action,"EDIT")!==false) {
			$multiple_when_filter = true;
			$display_earliest_time_period = false;
		} else {
			$multiple_when_filter = false;
			$display_earliest_time_period = true;
		}*/
		$data = array('display' => "", 'js' => "");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier center'><span id='$i'>
					<span style='float: left' id=spn_paging_buttons>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
		for($p = 1; $p <= $options['totalpages']; $p++) {
			$data['display'] .= "<option ".($p == $options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
		}
		$data['display'] .= "<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
					</span>";
		if(isset($add_button [0]) && $add_button[0] == true) {
			$ab_label = $this->replaceAllNames(isset($add_button[1]) && strlen($add_button[1]) > 0 ? $add_button[1] : "|add|");
			//$data['display'].="<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['display'] .= "<span style='float:right' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['js'] .= "
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).click(function(e) {
				e.preventDefault();
				".$add_button[2]."
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\").children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"})
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); });
			";
		}
		$main_time_id = false;
		if($filter_by !== false) {
			$filter_names = $filter_by['filter_names'];
			unset($filter_by['filter_names']);
			if(!isset($object_options['filter']['display'])) {
				$object_options['filter']['display'] = "compact";
			}
			if(!isset($object_options['filter']['when'])) {
				$object_options['filter']['when'] = "today";
			}
			$filter_applied = $object_options['filter'];
//			ASSIST_HELPER::arrPrint($filter_applied);
//			echo "<hr />";ASSIST_HELPER::arrPrint($filter_by);
			$data['display'] .= "<span class=float id=spn_paging_filter>
							<span class=b>Filter:</span> ";
			foreach($filter_by as $flt => $opts) {
				//ignore the update_when filter option - only needed for choosing which time periods to display on UPDATE list pages
				if($flt != "update_when") {
					$data['display'] .= "<span class=i>".$filter_names[$flt].":</span>&nbsp;<select id=filter_".$flt." name=filter[$flt]>";
					if($flt != "who" && $flt != "when") {
						foreach($opts as $key => $opt) {
							$data['display'] .= "<option ".(strtolower($key) == strtolower($filter_applied[$flt]) ? "selected" : "")." value=$key>$opt</option>";
						}
					} elseif($flt == "when") {
						//if update_when exists then assume that this is an update page and only offer those time periods for filtering
						if($is_update_page) {//isset($filter_by['update_when']) && $filter_by['update_when']!=false) {
							$opts = $filter_by['update_when'];
							if(count($opts) == 0) {
								$opts = array("X" => array('id' => "X", 'name' => "No open periods available"));
							}
						}
						foreach($opts as $key => $opt) {
//							echo "<p>".$key." : ";
							if($display_earliest_time_period) {
//								echo " display earliest ";
								if(isset($filter_applied[$flt]) && (strtolower($filter_applied[$flt]) == "today" || (strtoupper($filter_applied[$flt]) == "X" && !$is_update_page))) {// && $opt['is_current'] == true) { - default to first time period
									$filter_applied[$flt] = $key;
//									echo "reset key";
								}
							} else {
//								echo " not display earliest : ";
								if(isset($filter_applied[$flt]) && (strtolower($filter_applied[$flt]) == "today" || (strtoupper($filter_applied[$flt]) == "X" && !$is_update_page)) && $opt['is_current'] == true) {
									$filter_applied[$flt] = $key;
//									echo "reset key";
								}
							}
						}
//						ASSIST_HELPER::arrPrint($opts);
//						echo "<h3 class=red>".$filter_applied[$flt]."</h3>";
						foreach($opts as $key => $opt) {
							if(!isset($filter_applied[$flt])
								|| !isset($opts[$filter_applied[$flt]])
								|| strlen($filter_applied[$flt]) == 0
							) {
								$filter_applied[$flt] = $key;
							}
							$data['display'] .= "<option ".(strtolower($key) == strtolower($filter_applied[$flt]) ? "selected" : "")." value=$key>".$opt['name']."</option>";
						}
						$main_time_id = isset($filter_applied[$flt]) && ASSIST_HELPER::checkIntRef($filter_applied[$flt]) ? $filter_applied[$flt] : false;
					} else {//filter by who
						//remove "ALL" option - [JC] 28 July 2019 #AA-152
//						$key = "all";
//						$opt = "All";
//						$data['display'].="<option ".(strtolower($key)==strtolower($filter_applied[$flt])?"selected":"")." value=$key>$opt</option>";
						foreach($opts as $key => $opt) {
							if(strpos($key, "_") == false) {
								$key = "all";
								$selected = "";
							} else {
								$selected = (strtolower($key) == strtolower($filter_applied[$flt]) ? "selected" : "");
							}
							$data['display'] .= "<option ".$selected." value=$key>$opt</option>";
						}
					}
					$data['display'] .= "</select>&nbsp;";
					//needed so that list page know which time period has been selected for UPDATE process
					if($flt == "when") {
						$data['display'] .= "<input type=hidden name=page_time_id id=page_time_id value=".$filter_applied[$flt]." />";
					}
				}
			}
//			ASSIST_HELPER::arrPrint($filter_applied);
			$data['display'] .= " <button id=btn_filter_go>Apply Filters</button>
						</span>
					</span>";

		}
		$data['displayed_time_id'] = $main_time_id;
		$data['display'] .= "</td>
				</tr>
			</table>";
		$op = "options[set]=true";
		$fop = "options[set]=true";
		foreach($object_options as $key => $v) {
			if(is_array($v)) {
				foreach($v as $k => $x) {
					$op .= "&options[$key][$k]=$x";
					if($key != "filter") {
						$fop .= "&options[$key][$k]=$x";
					}
				}
			} else {
				$op .= "&options[$key]=$v";
				if($key != "filter") {
					$fop .= "&options[$key]=$v";
				}
			}
		}
		$data['js'] .= "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var filter_paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$fop."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first'] !== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev'] !== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next'] !== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last'] !== false ? $options['last'] : "false")."';

		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i."').children('#spn_paging_buttons').find('button').click(function() {
			$('.tbl-container:first').css('width','');
			$('#".$i."').paging('buttonClick',$(this));
			$(window).trigger('resize');
		});
		$('#".$i." select.page_picker').change(function() {
			$('.tbl-container:first').css('width','');
			$('#".$i."').paging('changePage',$(this).val());
			$(window).trigger('resize');
		});
		$('#spn_paging_filter').css({
		});
		$('#btn_filter_go').button({
			icons:{primary:'ui-icon-check'}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = filter_paging_page_options+'&'+AssistForm.nonFormSerialize($('#spn_paging_filter'));
			//alert(dta);
			var url = '".$_SERVER['PHP_SELF']."?'+dta;
			document.location.href = url;
		}).children(\".ui-button-text\").css({\"font-size\":\"75%\",\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"});
		";
		return $data;
	}


	/*****
	 * List View
	 */
	/**
	 * Function to draw the list table onscreen
	 * @param (Array) $child_objects = array of objects to display in table
	 * @param (Array) $button = details of button to display in last column
	 * @param (String) $object_type = the name of the object
	 * @param (Array) $object_options = getPaging options
	 * @param (Bool) $sub_table = is this table a child of another object true/false
	 * @param (Array) $add_button = array(0=>true/false, 1=>label if true)
	 */
	public function drawListTable($child_objects, $button = array(), $object_type = "", $object_options = array(), $sub_table = false, $add_button = array(false, "", ""), $filter_by = false) {
		$me = $this->getListTable($child_objects, $button, $object_type, $object_options, $sub_table, $add_button, $filter_by);
		echo $me['display'];
		return $me['js'];
	}

	public function getListTable($child_objects, $button, $object_type = "", $object_options = array(), $sub_table = false, $add_button = array(false, "", ""), $filter_by = false) {
//		echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		ASSIST_HELPER::arrPrint($filter_by);
//		ASSIST_HELPER::arrPrint($object_options);
		//echo $object_type;

		//check if is list page in NEW section
		//if yes then remove filter[when]
		//and later don't add results columns - no results to display in NEW
		$is_new_page = false;
		if($object_options['section'] == "NEW") {
			$is_new_page = true;
			if($filter_by !== false) {
				unset($filter_by['when']);
			}
			if(isset($object_options['filter']['year'])) {
				unset($object_options['filter']['year']);
			}
			if(isset($object_options['filter']['when'])) {
				unset($object_options['filter']['when']);
			}
			if(isset($child_objects['results_head'])) {
				unset($child_objects['results_head']);
			}
			if(isset($child_objects['results_rows'])) {
				unset($child_objects['results_rows']);
			}
		}


		$is_update_page = false;
		if(strpos($object_options['page'], "UPDATE") !== false) {
			$is_update_page = true;
		}
		$is_approve_page = false;
		if(strpos($object_options['page'], "APPROVE") !== false) {
			$is_approve_page = true;
		}
		$is_assurance_page = false;
		if(strpos($object_options['page'], "ASSURANCE") !== false) {
			$is_assurance_page = true;
		}


		$is_dept_page = false;
		if(strpos($object_options['page'], "DEPTKPI") !== false) {
			$is_dept_page = true;
		}
		$is_top_page = false;
		if(strpos($object_options['page'], "TOPKPI") !== false) {
			$is_top_page = true;
		}
		$is_project_page = false;
		if(strpos($object_options['page'], "PROJ") !== false) {
			$is_project_page = true;
		}

		//get preference settings
		//$object_type.'_past_comments'=>0, //removed by JC on 14 Sep - Can be displayed by user choice using the Filter_by[display] option
		//if not on update page then automatically display the time periods on either side of the current one
		$preferences = array(
			'past_results' => $is_update_page ? 0 : 1,
			'next_results' => $is_update_page ? 0 : 1,
		);
		if($is_update_page) {
			$preferencesObject = new SDBP6_SETUP_PREFERENCES();
			foreach($preferences as $key => $pref) {
				$preferences[$key] = $preferencesObject->getAnswerToQuestion($object_type."_".$key);
			}
		}

		if(!$is_new_page && isset($object_options['filter']['when'])) {
			$time_ids = array_keys($child_objects['results_head']['top']);
			$prev_time = $child_objects['results_head']['top'][$time_ids[0]];
			$current_time = $child_objects['results_head']['top'][$time_ids[1]];
			//$next_time = $child_objects['results_head']['top'][$time_ids[2]];
			$main_time_id = $object_options['filter']['when'];
			if($main_time_id == "today" || !in_array($main_time_id, $time_ids)) {
				if(isset($prev_time['is_current']) && $prev_time['is_current'] == true) {
					$main_time_id = $time_ids[0];
				} elseif($current_time['is_current'] == true || !isset($time_ids[2])) {
					$main_time_id = $time_ids[1];
				} else {
					$main_time_id = $time_ids[2];
				}
			}
			/*
			if($object_options['filter']['when']=="today") {
				foreach($filter_by['when'] as $key => $time) {
					if($time['is_current']==true) {
						$object_options['filter']['when'] = $key;
					}
				}
				//if filter when is still set to today then assume that current date is outside financial year and default to first period
					//this should be taken care of in the getting of the time periods for filtering but just in case let's check here too
				if($object_options['filter']['when']=="today") {
					$a = array_keys($filter_by['when']);
					$object_options['filter']['when'] = $a[0];
				}
			}
			$main_time_id = $object_options['filter']['when'];
			$current_time = $filter_by['when'][$main_time_id];
			//prev time
			if($preferences['past_results']==true && $current_time['prev']!==false) {
				$prev_time = $filter_by['when'][$current_time['prev']];
				$child_objects['results_head']['top'][$current_time['prev']] = $prev_time;
			} else {
				$prev_time = false;
			}
			//current time
			$child_objects['results_head']['top'][$main_time_id] = $current_time;
			//next time
			if($preferences['next_results']==true && $current_time['next']!==false) {
				$next_time = $filter_by['when'][$current_time['next']];
				$child_objects['results_head']['top'][$current_time['next']] = $next_time;
			} else {
				$next_time = false;
			}
			*/
		} else {
			$main_time_id = false;
			$current_time = false;
			$prev_time = false;
			$next_time = false;
		}


		$data = array('display' => "<table class=tbl-container><tr><td>", 'js' => "");
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button === false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager'] === false) {
			$display_paging = false;
		}
		if($display_paging) {
			$paging = $this->getPaging($page_id, $page_options, $button, $object_type, $object_options, $add_button, $filter_by);
			$data['display'] .= $paging['display'];
			$data['js'] .= $paging['js'];
			$paging_displayed_time_id = $paging['displayed_time_id'];
		}
		unset($button['pager']);
		if(!$is_new_page && isset($child_objects['results_head']['bottom']) && count($child_objects['results_head']['bottom']) > 0) {
			$rowspan = 2;
			$has_results = true;
		} else {
			$rowspan = 1;
			$has_results = false;
		}

		$data['display'] .= "
			<table class='list ".($sub_table !== false ? "th2" : "")."' id='".$page_id."_list_view'>
				<tr id='head_row'>
					";
		foreach($child_objects['head'] as $fld => $head) {
			$data['display'] .= "<th rowspan=".$rowspan.">".$head['name']."</th>";
		}
		if($has_results) {
			$t_count = 1;
			/*						//if past_Results = true & current_time has prev then display
									if($prev_time!==false) {
										$data['display'].="<th class='heading".$t_count."' colspan=".count($child_objects['results_head']['bottom']).">".$prev_time['name']."</th>";
										$t_count++;
									}
									//display current
									$data['display'].="<th class='heading".$t_count."' colspan=".count($child_objects['results_head']['bottom']).">".$current_time['name']."</th>";
									$t_count++;
									//if next_results = true & current_time has next then display
									if($next_time!==false) {
										$data['display'].="<th class='heading".$t_count."' colspan=".count($child_objects['results_head']['bottom']).">".$next_time['name']."</th>";
									}*/
//ASSIST_HELPER::arrPrint($child_objects['results_head']['top']);
//echo "<h3>:".$main_time_id.":</h3>";
//echo "<h3>:".$paging_displayed_time_id.":</h3>";
			if($main_time_id === false && isset($paging_displayed_time_id) && $paging_displayed_time_id !== false) {
				if(isset($child_objects['results_head']['top'][$paging_displayed_time_id])) {
					$main_time_id = $paging_displayed_time_id;
					$green_time = $main_time_id;
					$valid_time_ids = array($green_time);
					$x = $child_objects['results_head']['top'][$paging_displayed_time_id];
					$nx = $x['next'];
					$pr = $x['prev'];
					if($nx != false && isset($child_objects['results_head']['top'][$nx])) {
						$orange_time = $nx;
						$valid_time_ids[] = $nx;
					} else {
						$orange_time = false;
					}
					$child_objects['results_head']['top'][$main_time_id]['prev'] = $orange_time;
					if($pr != false && isset($child_objects['results_head']['top'][$paging_displayed_time_id + 1])) {
						$red_time = $pr;
						$valid_time_ids[] = $pr;
					} else {
						$red_time = false;
					}
					$child_objects['results_head']['top'][$main_time_id]['next'] = $red_time;
					$use_t_count = false;
					foreach($child_objects['results_head']['top'] as $t => $c) {
						if(!in_array($t, $valid_time_ids)) {
							unset($child_objects['results_head']['top'][$t]);
						}
					}
				} else {
					$use_t_count = true;
					$orange_time = 1;
					$green_time = 2;
					$red_time = 3;
				}

			} else {
				if($main_time_id !== false && isset($child_objects['results_head']['top'][$main_time_id])) {
					$list_page_current_time = $child_objects['results_head']['top'][$main_time_id];
					$orange_time = $list_page_current_time['prev'];
					$green_time = $main_time_id;
					$red_time = $list_page_current_time['next'];
					$use_t_count = false;
				} else {
					$use_t_count = true;
					$orange_time = 1;
					$green_time = 2;
					$red_time = 3;
				}
			}
			foreach($child_objects['results_head']['top'] as $time_id => $t_head) { //ASSIST_HELPER::arrPrint($t_head);
				$background_class = "";
				if($use_t_count) {
					if((count($child_objects['results_head']['top']) - 2) == ($t_count)) {
						$background_class = "orange-back";
					} elseif((count($child_objects['results_head']['top']) - 1) == ($t_count)) {
						$background_class = "green-back";
					} elseif(count($child_objects['results_head']['top']) == $t_count) {
						$background_class = "red-back";
					}
				} else {
					if($time_id == $orange_time) {
						$background_class = "orange-back";
					} elseif($time_id == $green_time) {
						$background_class = "green-back";
					} elseif($time_id == $red_time) {
						$background_class = "red-back";
					}
				}
				$data['display'] .= "<th class='heading".$t_count." $background_class' colspan=".count($child_objects['results_head']['bottom']).">".$t_head['name'].($time_id == $main_time_id && $main_time_id != false ? "*" : "")."</th>";
				$t_count++;
			}
		}
		$data['display'] .= (($button !== false && is_array($button) && count($button) > 0) ? "<th rowspan=".$rowspan."></th>" : "")."
				</tr>";
		if($has_results) {
			$t_count = 1;
			$data['display'] .= "<tr>";
			foreach($child_objects['results_head']['top'] as $time_id => $t_head) {
				$background_class = "";
				if($use_t_count) {
					if($t_count == 1) {
						$background_class = "orange-back";
					} elseif($t_count == 2) {
						$background_class = "green-back";
					} elseif($t_count == 3) {
						$background_class = "red-back";
					}
				} else {
					if($time_id == $orange_time) {
						$background_class = "orange-back";
					} elseif($time_id == $green_time) {
						$background_class = "green-back";
					} elseif($time_id == $red_time) {
						$background_class = "red-back";
					}
				}
				foreach($child_objects['results_head']['bottom'] as $r_fld => $r_head) {
					$data['display'] .= "<th class='heading".$t_count." $background_class'>".$r_head['name']."</th>";
				}
				$t_count++;
			}

			/*						//prev time
									if($prev_time!==false) {
										foreach($child_objects['results_head']['bottom'] as $r_fld => $r_head) {
											$data['display'].="<th class='heading".$t_count."'>".$r_head['name']."</th>";
										}
										$t_count++;
									}
									//curent time
									foreach($child_objects['results_head']['bottom'] as $r_fld => $r_head) {
										$data['display'].="<th class='heading".$t_count."'>".$r_head['name']."</th>";
									}
									$t_count++;
									//next time
									foreach($child_objects['results_head']['bottom'] as $r_fld => $r_head) {
										$data['display'].="<th class='heading".$t_count."'>".$r_head['name']."</th>";
									}*/
			$data['display'] .= "</tr>";
		}
		$rows = $this->getListTableRows($child_objects, $button, $main_time_id);
		$data['display'] .= $rows['display']."</table>";
		$data['display'] .= "</td></tr></table>";
		$data['js'] .= $rows['js']."

				//console.log($('#".$page_id."_list_view').css('width'));
			$(window).resize(function() {
				//console.log($('#".$page_id."_list_view').css('width'));
				//console.log($('.tbl-container:first').css('width'));

				var tbl_w = parseInt(AssistString.substr($('#".$page_id."_list_view').css('width'),0,-2));
				var btn_w = parseInt(AssistString.substr($('#".$page_id."_list_view tr:first th:last').css('width'),0,-2));
				var ref_w = parseInt(AssistString.substr($('#".$page_id."_list_view tr:first th:first').css('width'),0,-2));
				var con_w = parseInt(AssistString.substr($('.tbl-container:first').css('width'),0,-2));

				if(tbl_w < (con_w/2)) {
					tbl_w = Math.round(con_w/2);
					$('#".$page_id."_list_view').css('width',tbl_w+'px');
					$('#".$page_id."_list_view').css('margin','0 auto');
					$('#".$page_id."_list_view tr:first th:last').css('width',btn_w+'px');
					$('#".$page_id."_list_view tr:first th:first').css('width',ref_w+'px');
				}
				if(tbl_w < (con_w-30)) {
					con_w = tbl_w + 20;
					$('.tbl-container:first').css('width',con_w+'px');
				}

			});
			$(window).trigger('resize');
			";
		return $data;
	}

	public function getListTableRows($child_objects, $button, $main_time_id) {
		$headings = $child_objects['head'];
		$button_js = false;
		$data = array('display' => "", 'js' => "");
		if(count($child_objects['rows']) == 0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head']) + 1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				if(isset($child_objects['results_rows'][$id])) {
					$results = $child_objects['results_rows'][$id];
				} else {
					$results = false;
				}
				$data['display'] .= "<tr ref=".$id." ".((isset($obj['is_active']) && $obj['is_active'] !== true) ? "class=inactive" : "")." >";
				foreach($child_objects['head'] as $fld => $head) {
					$val = isset($obj[$fld]) ? $obj[$fld] : "";
					$class = "";
					if(isset($headings[$fld]) && $headings[$fld]['type'] == "NUM" && (!isset($val['format_me']) || $val['format_me'] !== false)) {
						$class = "class='right'";
					}
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'] .= "<td $class >".$val['display']."</td>";
							$data['js'] .= isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'] .= "<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'] .= "<td>".$val."</td>";
					}
				}
				if(isset($child_objects['results_head']['bottom']) && count($child_objects['results_head']['bottom']) > 0) {
					$t_count = 0;
					foreach($child_objects['results_head']['top'] as $time_key => $time) {
						$t_count++;
						$result = $results[$time_key];
						if($main_time_id !== false && isset($child_objects['results_head']['top'])) {
							$list_page_current_time = $child_objects['results_head']['top'][$main_time_id];
							$orange_time = $list_page_current_time['prev'];
							$green_time = $main_time_id;
							$red_time = $list_page_current_time['next'];
							$use_t_count = false;
						} else {
							$use_t_count = true;
							$orange_time = 1;
							$green_time = 2;
							$red_time = 3;
						}
						foreach($child_objects['results_head']['bottom'] as $r_fld => $r_head) {
							if($use_t_count) {
								if($t_count == 1) {
									$background_class = "light-orange-back";
								} elseif($t_count == 2) {
									$background_class = "light-green-back";
								} elseif($t_count == 3) {
									$background_class = "light-red-back";
								}
							} else {
								if($time_key == $orange_time) {
									$background_class = "light-orange-back";
								} elseif($time_key == $green_time) {
									$background_class = "light-green-back";
								} elseif($time_key == $red_time) {
									$background_class = "light-red-back";
								}
							}
							$data['display'] .= "<td class='cell".$t_count." ".($r_head['apply_formatting'] == true ? "right" : "")." ".($background_class)."'>".$result[$r_fld]."</td>";
						}
					}
				}

				if(isset($button['type']) && $button['type'] == "button") {
					$data['display'] .= (($button !== false && is_array($button) && count($button) > 0) ? "<td class=center><button ref=".$id." class='".$button['class']." ui_btn' />".$button['value']."</button></td>" : "");
					if($button_js == false) {
						$button_js = true;
						$data['js'] .= "
						$('button.ui_btn').button({
							icons: {primary: \"ui-icon-".(isset($button['icon']) ? $button['icon'] : "pencil")."\"},
						}).children(\".ui-button-text\").css({\"font-size\":\"80%\"}).click(function(e) {
							e.preventDefault();

						}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")
						";
					}
				} else {
					$data['display'] .= (($button !== false && is_array($button) && count($button) > 0) ? "<td class=center><button ref=".$id." class='".$button['class']."'>".$button['value']."</button></td>" : "");
				}
				$data['display'] .= "</tr>";
			}
		}
		return $data;
	}


	/************************************************************
	 * Function to generate the form for UPDATE
	 * @param string $page_action - what are you doing?
	 * @param string $form_object_type - what are you working on?
	 * @param SDBP6_DEPTKPI $formObject - object of type that you are working on? - can be SDBP6_TOPKPI or SDBP6_PROJECT
	 * @param int $sdbip_id - parent SDBIP
	 * @param int $object_id - id of object you are working on
	 * @param int $time_id - id of time period you are working in
	 * @param int $targettype_id - id of the format to be applied to the target/actual
	 * @param string $page_redirect_path - where to go when you're done
	 * @param int $calc_type_id - id of the calculation type to be applied to the target/actual
	 * @return array
	 */
	public function getUpdateForm($page_action, $form_object_type, $formObject, $sdbip_id, $object_id, $time_id, $targettype_id, $page_redirect_path, $calc_type_id = 0) {
		//Set Variables
		$form_object_id = $object_id.".".$time_id;
		//$form_object = $formObject->getRawUpdateObject($object_id,$time_id);
		$page_section = "MANAGE";//update form doesn't change between MANAGE and ADMIN
		$data = array('display' => "", 'js' => "");
		$display = "";
		$js = "";
		$js_page_action = ucwords($form_object_type).".".$page_action;
		$js_page_redirect = $page_redirect_path;
		//allow user to input an actual?  Not allowed for PROJECTS or TOPKPIs linked to DEPTKPIs
		$can_update_actual_field = $formObject->canUserUpdateActual($object_id);

		//target type settings
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("", $targettype_id);

		//preferences / required fields
		$preferencesObject = new SDBP6_SETUP_PREFERENCES();
		$default_update_status = $preferencesObject->getAnswerToQuestionByCode("ALL_update_status");
		$require_performance_comment_regardless_of_update_status = $preferencesObject->getAnswerToQuestionByCode("ALL_update_perfcommalways");
		if($require_performance_comment_regardless_of_update_status == false) {
			$require_performance_comment_if_target_not_met = $preferencesObject->getAnswerToQuestionByCode("ALL_update_perfcommtarget");
		} else {
			$require_performance_comment_if_target_not_met = false;
		}
		$require_corrective_comment_regardless_of_update_status = $preferencesObject->getAnswerToQuestionByCode("ALL_update_correctivecommalways");
		if($require_corrective_comment_regardless_of_update_status == false) {
			$require_corrective_comment_if_target_not_met = $preferencesObject->getAnswerToQuestionByCode("ALL_update_correctivecommtarget");
		} else {
			$require_corrective_comment_if_target_not_met = false;
		}
		$require_poe_comment_or_attachment = $preferencesObject->getAnswerToQuestionByCode("ALL_update_poerequired");
		if($require_performance_comment_regardless_of_update_status == true || $require_performance_comment_if_target_not_met == true || $require_corrective_comment_regardless_of_update_status == true || $require_corrective_comment_if_target_not_met == true) {
			$comment_length = $preferencesObject->getAnswerToQuestionByCode("ALL_comments_min");
		} else {
			$comment_length = false;
		}

//		echo "<h1 class='orange'>".($require_comment_if_target_not_met==true ? "capture comments if target not met" : "not to worry if you don't meet your targets")."</h1>";
//		echo "<h1 class='green'>".($require_comment_regardless_of_update_status==true ? "you better capture your comments" : "not to worry with comments")."</h1>";
//		echo "<h1 class='red'>".$comment_length." characters required at least</h1>";

		//Get Headings
		$headingObject = new SDBP6_HEADINGS();
		switch($page_action) {
			case "UPDATE":
			case "VIEW":
				$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($formObject->getMyChildObjectType(), "FORM"));
				break;
			case "APPROVE":
			case "ASSURANCE":
				//TO BE POPULATED WHEN I GET THERE - JC
				//Possibly no longer required - replaced by getReviewForm(); - JC 20 March 2021 AA-540
				break;
		}

		//Get Raw Object (only for UPDATE)
		if($page_action == "UPDATE" || $page_action == "VIEW") {
			$form_object = $formObject->getRawUpdateObject($object_id, $time_id);
		} else {
			$form_object = array();
		}
		//form element names
		$tbl_id = $form_object_type."_".$page_action."_".$object_id."_".$time_id;
		$form_name = "frm_object_".$form_object_type."_".str_replace(".", "_", $form_object_id)."_".strtolower(str_replace(".", "", $page_action));
		$save_button = "btn_update_save";

		//test for approve/assurance messages
		$results_table_field_name = $formObject->getResultsTableField();
		$extra_text_message_for_the_status_bool_buttons = "";
		if((isset($form_object[$results_table_field_name.'_approve']) && $form_object[$results_table_field_name.'_approve'] == SDBP6::REVIEW_REJECT) || (isset($form_object[$results_table_field_name.'_assurance']) && $form_object[$results_table_field_name.'_assurance'] == SDBP6::REVIEW_REJECT)) {
			$extra_text_message_for_the_status_bool_buttons = "To return a rejected update for further review, mark the update as complete.";
		}

		//Generate Form JS
		$js .= "
				var ".$form_name."_page_action = '".$js_page_action."';
				var ".$form_name."_page_direct = '".$js_page_redirect."';

		".$this->getAttachmentDownloadJS3($form_object_type, str_replace('.', '-', $form_object_id), $page_action)."

		$('#".$save_button."').button({
			icons:{primary:\"ui-icon-disk\"}
		}).removeClass('ui-state-default').addClass('ui-button-bold-green')
		.hover(function() {
			$(this).removeClass('ui-button-bold-green').addClass('ui-button-bold-orange');
		},function() {
			$(this).removeClass('ui-button-bold-orange').addClass('ui-button-bold-green');
		}).click(function(e) {
			e.preventDefault();
			";
		if($page_action == "UPDATE" && ($require_performance_comment_regardless_of_update_status || $require_performance_comment_if_target_not_met || $require_corrective_comment_regardless_of_update_status || $require_corrective_comment_if_target_not_met || $require_poe_comment_or_attachment)) {
			$js .= "	
			//before sending for save, check required fields - custom function needed as status must be checked
			AssistHelper.processing();
			$('textarea').removeClass('required');
			//get status
			var has_status = false;
			var is_complete = false;
			$('.update_status').each(function() {
				has_status = true;
				if($(this).val()*1==1 || $(this).val()==true) {
					is_complete = true;
				}
			});
			//if complete then trigger required field check
			var error = false;
			var err_fields = Array();
			if(is_complete==true) {
//			console.log('is complete');
				var require_performance_comment_regardless_of_update_status = ".($require_performance_comment_regardless_of_update_status ? "true" : "false").";
				var require_performance_comment_if_target_not_met = ".($require_performance_comment_if_target_not_met ? "true" : "false").";
				var require_corrective_comment_regardless_of_update_status = ".($require_corrective_comment_regardless_of_update_status ? "true" : "false").";
				var require_corrective_comment_if_target_not_met = ".($require_corrective_comment_if_target_not_met ? "true" : "false").";
				var require_poe_comment_or_attachment = ".($require_poe_comment_or_attachment ? "true" : "false").";
				var comment_min_length = ".($comment_length === false ? 0 : $comment_length).";
				var perf_comm = '';
				var perf_comm_id = '';
				var correct_measures = '';
				var correct_measures_id = '';
				var poe_comment = '';
				var poe_comment_id = '';
				var comm_type = '';
				var i = '';
				var v = '';
				$('textarea').each(function() {
//					console.log($(this).prop('id')+' = '+$(this).attr('comment_type'));
					i = $(this).prop('id');
					comm_type = $(this).attr('comment_type');
					v = $(this).val();
					if(comm_type=='performance_comment') {
						perf_comm = v;
						perf_comm_id = i;
					} else if(comm_type=='corrective_comment') {
						correct_measures = v;
						correct_measures_id = i;
					} else if(comm_type=='poe_comment') {
						poe_comment = v;
						poe_comment_id = i;
					}
				});
				if(require_performance_comment_if_target_not_met || require_corrective_comment_if_target_not_met) {
					var calc_type_id = ".(isset($calc_type_id) && ASSIST_HELPER::checkIntRef($calc_type_id) ? $calc_type_id : 0).";
					var target = $('#".$formObject->getResultsRevisedFieldName()."').val();
					var actual = AssistString.calcPureNumberString($('input.is_actual:first').val(),true);
					var dta = 'calc_type_id='+calc_type_id+'&target='+target+'&actual='+actual; //console.log(dta);
					var result = AssistHelper.doAjax('inc_controller.php?action=CALCTYPE.checkTargetMetForUpdateRequiredFieldsValidationReturnForAjax',dta); //console.log(result);
					var answer = result.answer;
				}
//				console.log(perf_comm_id+' = '+perf_comm);
//				console.log(correct_measures_id+' = '+correct_measures);
//				console.log(poe_comment_id+' = '+poe_comment);
				if(require_performance_comment_regardless_of_update_status || (require_performance_comment_if_target_not_met && (answer==false || answer == 0 || answer=='false'))) {
					if(perf_comm.length < comment_min_length) {
						error = true;
						err_fields.push($('#spn_'+perf_comm_id).html());
						$('#'+perf_comm_id).addClass('required');
					}
				}
				if(require_corrective_comment_regardless_of_update_status || (require_corrective_comment_if_target_not_met && (answer==false || answer == 0 || answer=='false'))) {
					if(correct_measures.length < comment_min_length) {
						error = true;
						err_fields.push($('#spn_'+correct_measures_id).html());
						$('#'+correct_measures_id).addClass('required');
					}
				}
				if(require_poe_comment_or_attachment) {
				//get attachment field info
					var attachment_loaded = false;
					$('input:file').each(function() {
						if($(this).val().length>0) {
							attachment_loaded = true;
						}
					});
					if(poe_comment.length < comment_min_length && attachment_loaded == false) {
							error = true;
							err_fields.push($('#spn_'+poe_comment_id).html()+' [Comment or attachment required]');
							$('#'+poe_comment_id).addClass('required');
					}
				}
			} else {
//			console.log('not complete so carry on');
			}
			if(error) {
				AssistHelper.finishedProcessing('error','Please complete the following required fields:<ul class=black><li> '+AssistString.implode('</li><li>',err_fields)+'</li></ul>');
			} else {
				AssistHelper.closeProcessing();
//				alert('ready to continue with saving update form');
				SDBP6Helper.processObjectFormWithAttachment($('form[name=".$form_name."]'));
			}
		});
			";
		} else {

			$js .= "
//			alert('not enforcing required fields');
				SDBP6Helper.processObjectFormWithAttachment($('form[name=".$form_name."]'));
			});
			";

		}

		//<input type=hidden name=action value='".$js_page_action."' id=action />
		//<input type=hidden name=page_action value='".$js_page_redirect."' id=page_direct />
		//Generate Form DISPLAY
		$display .= "
			<div id=div_error class=div_frm_error>

			</div>
				<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">
				<input type=hidden name=sdbip_id value='".$sdbip_id."' id=sdbip_id />
				<input type=hidden name=object_id value='".$object_id."' id=object_id />
				<input type=hidden name=time_id value='".$time_id."' id=time_id />
				<input type=hidden name=target_type_id value='".$targettype_id."' id=target_type_id />
				<input type=hidden name=calc_type_id value='".$calc_type_id."' id=calc_type_id />
				<table class='form' id=tbl_object_form_".$tbl_id.">";

		foreach($headings as $fld => $head) {
			$h_type = $head['type'];
			$options = array('id' => $fld, 'name' => $fld, 'req' => $head['required']);
			if($h_type == "STATUS") {
				$head['name'] = "Is this update complete?";
			}
			$extra_heading_info = "";
			if($head['is_update_comments'] == true) {
				if($head['is_performance_comment'] == true) {
					if($require_performance_comment_if_target_not_met) {
						$extra_heading_info = "Required if target not met";
					} elseif($require_performance_comment_regardless_of_update_status) {
						$extra_heading_info = "Required";
					}
				} elseif($head['is_corrective_measures'] == true) {
					if($require_corrective_comment_if_target_not_met) {
						$extra_heading_info = "Required if target not met";
					} elseif($require_corrective_comment_regardless_of_update_status) {
						$extra_heading_info = "Required";
					}
				} elseif($head['is_poe'] == true) {
					if($require_poe_comment_or_attachment) {
						$extra_heading_info = "Comment or attachment required";
					}
				}
				if(isset($extra_heading_info) && strlen($extra_heading_info) > 0) {
					$extra_heading_info = "<br /><span class=i style='font-size:75%; font-weight: normal'>[".$extra_heading_info."]</span>";
				}
			} else {
				$extra_heading_info = "";
			}
			$display .= "
			<tr>
				<th width=40%><span id='spn_".$fld."'>".$head['name']."</span>:".$extra_heading_info."</th>
				<td>";
			switch($head['type']) {
				case "CALC":
					$val = isset($form_object[$fld]) ? $form_object[$fld] : 0;
					if($head['apply_formatting'] == true) {
						$display .= $targetTypeObject->formatNumberBasedOnTargetType($val);
//							if($fld==$formObject->getResultsRevisedFieldName()) {
//								$display.="<input type=hidden name=".$formObject->getResultsRevisedFieldName()." value='".$val."' />";
//							}
					} else {
						$display .= $val;
					}
					break;
				case "NUM":
					$val = isset($form_object[$fld]) ? $form_object[$fld] : 0;
					if($head['is_actual'] == true && $page_action == "UPDATE" && $can_update_actual_field) {
						$options['required'] = true;
						$options['class'] = (isset($options['class']) ? $options['class'] : "")." right is_actual";
						$echo = $this->createFormField($h_type, $options, ASSIST_HELPER::format_number($val, "FLOAT", 2, "", ""));
						$symbols = $targetTypeObject->getSymbolsForFormatting($targettype_id);
						$display .= $symbols['pre'].$echo['display'].$symbols['post'];
						$js .= $echo['js'];
					} else {
						if($head['apply_formatting'] == true) {
							$display .= $targetTypeObject->formatNumberBasedOnTargetType($val);
						} else {
							$display .= $val;
						}
					}
					break;
				case "STATUS":
					$h_type = "BOOL_BUTTON";
					if($page_action == "UPDATE") {
						$options['yes'] = "1";
						$options['no'] = "0";
						$options['extra'] = "boolButtonClickExtra";
						$options['small_size'] = true;
						$options['class'] = "update_status";
						$form_field = $formObject->getResultsTableField()."_update";
//						$form_field = $fld; echo $fld." = ".$form_object[$form_field];
						if(!isset($form_object[$form_field]) || $formObject->isStatusNew($form_object[$form_field])) {
							$val = $default_update_status;
						} else {
							$val = $formObject->isStatusCompleted($form_object[$form_field]) ? 1 : 0;
						}
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$val = isset($form_object[$fld]) ? $form_object[$fld] : "0";
						$echo = $this->getDataField($h_type, $val);
					}
					$display .= (isset($extra_text_message_for_the_status_bool_buttons) && strlen($extra_text_message_for_the_status_bool_buttons) > 0 ? "<div class=float style='width:280px; margin-top:-8px;margin-right:-3px'>".ASSIST_HELPER::getDisplayResult(array("info", $extra_text_message_for_the_status_bool_buttons))."</div>" : "");
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				case "TEXT":
					$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
					if($head['is_update_comments'] == true) {
						if($head['is_performance_comment'] == true) {
							$options['comment_type'] = "performance_comment";
						} elseif($head['is_corrective_measures'] == true) {
							$options['comment_type'] = "corrective_comment";
						} elseif($head['is_poe'] == true) {
							$options['comment_type'] = "poe_comment";
						} else {
							$options['comment_type'] = "unknown_comment";
						}
						if($page_action == "UPDATE") {
							$echo = $this->createFormField($h_type, $options, $val);
						} else {
							$echo = $this->getDataField($h_type, str_replace(chr(10), "<br />", $val));
						}
						$display .= $echo['display'];
						$js .= $echo['js'];
					} else {
						$display .= str_replace(chr(10), "<br />", $val);
					}
					break;
				case "ATTACH":
					$val = $formObject->decodeAttachmentDataFromDatabase($form_object[$fld], false);
					if($page_action == "UPDATE") {
						$options['action'] = $js_page_action;
						$options['page_direct'] = $js_page_redirect;
						$options['page_activity'] = $page_action;
						$options['can_edit'] = true;
						$options['object_type'] = $form_object_type;
						$options['object_id'] = str_replace('.', '-', $form_object_id);
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$options['object_type'] = $formObject->getMyObjectType();
						$options['object_id'] = $form_object[$formObject->getResultsParentFieldName()]."-".$time_id;
						$options['page_activity'] = "VIEW";
						$echo = $this->getAttachmentViewNoButton($val, $options);
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				default:
					$display .= $h_type;
					break;
			}
			if($fld == $formObject->getResultsRevisedFieldName()) {
				$display .= "<input type=hidden name=".$formObject->getResultsRevisedFieldName()."  id=".$formObject->getResultsRevisedFieldName()." value='".$val."' class='is_target' />";
			}
			$display .= "</td></tr>";
		}

		if($page_action == "UPDATE") {
			$display .= "
			<tr>
				<td class=center colspan=2><button id=".$save_button.">".$formObject->replaceAllNames("|save| |".$page_action."|")."</button></td>
			</tr>";
		}
		$display .= "
		</table></form>";


		//Return Data
		$data['display'] = $display;
		$data['js'] = $js;
		return $data;
	}


	/************************************************************
	 * Function to generate the form for APPROVE / ASSURANCE
	 */
	public function getReviewForm($page_action, $form_object_type, $formObject, $sdbip_id, $object_id, $time_id, $object_details, $page_redirect_path) {
//ASSIST_HELPER::arrPrint($object_details);
		//Set Variables
		$adminObject = new SDBP6_SETUP_ADMINS();
		$department_field = $formObject->getDepartmentFieldName();
		$owner_field = $formObject->getOwnerFieldName();
		$admin_section = $formObject->getAdminSectionIdentifier();
		$update_field = $formObject->getResultsTableField()."_updateuser";
		$update_user = $object_details['results'][$time_id][$update_field];
		$admins = $adminObject->getUpdateUsersForReview($admin_section, $object_details[$department_field], $object_details[$owner_field], $update_user);
		$email_admins = $adminObject->getEmailUsersForReview($admin_section, $object_details[$department_field], $object_details[$owner_field], $update_user);
		$form_object_id = $object_id.".".$time_id;
		$form_object = $formObject->getRawUpdateObject($object_id, $time_id);
		$page_section = "MANAGE";//update form doesn't change between MANAGE and ADMIN
		$data = array('display' => "", 'js' => "");
		$display = "";
		$js = "";
		$js_page_action = ucwords($form_object_type).".".$page_action;
		$js_page_redirect = $page_redirect_path;


		//Get Headings
		$headingObject = new SDBP6_HEADINGS();
		$headings = $headingObject->replaceObjectNames($headingObject->getReviewObjectHeadings($page_action, "FORM"));
//ASSIST_HELPER::arrPrint($headings);

		if($page_action == "VIEW") {
			$form_object = $formObject->getRawUpdateObject($object_id, $time_id);
		} else {
			$form_object = array();
		}

		//form element names
		$tbl_id = $form_object_type."_".$page_action."_".$object_id."_".$time_id;
		$form_name = "frm_object_".$form_object_type."_".str_replace(".", "_", $form_object_id)."_".strtolower(str_replace(".", "", $page_action));
		$save_button = "btn_review_save";


		//Generate Form JS
		$js .= "  //console.log('".$form_name."');
				var ".$form_name."_page_action = '".$js_page_action."';
				var ".$form_name."_page_direct = '".$js_page_redirect."';

		".$this->getAttachmentDownloadJS3($form_object_type, str_replace('.', '-', $form_object_id), $page_action)."

		$('#".$save_button."').button({
			icons:{primary:\"ui-icon-disk\"}
		}).removeClass('ui-state-default').addClass('ui-button-bold-green')
		.hover(function() {
			$(this).removeClass('ui-button-bold-green').addClass('ui-button-bold-orange');
		},function() {
			$(this).removeClass('ui-button-bold-orange').addClass('ui-button-bold-green');
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var valid = true;
			var errmsg = 'Please correct the following errors:<ul>';

			var signoff = $('#signoff').val();
			if(signoff==false) {
				if($('#comment').val().length==0) {
					valid = false;
					errmsg+='<li class=black>A message is required, explaining why you are not accepting the update;</li>';
				}
				if($('#returnuser').val()=='X') {
					$('#returnuser').val(0);
				}
				if($('#returnuser').val()!='0') {
					if($('#deadline').val().length==0) {
						valid = false;
						errmsg+='<li class=black>A deadline date is required if you are rejecting & returning to a user for further updating;</li>';
					}
				}
			}

			if(valid) {
				SDBP6Helper.processObjectFormWithAttachment($('form[name=".$form_name."]'));
			} else {
				AssistHelper.finishedProcessing('error',errmsg);
			}
		});
		$('#returnuser').change(function() {
			if($(this).val()=='X') {
				$(this).val('0');
			}
			if($(this).val()=='0') {
				$('#deadline').datepicker('option',{disabled:true}).css('cursor','not-allowed');
				$('#notify_no').trigger('click').prop('disabled',true).css('cursor','not-allowed');
				$('#notify_yes').prop('disabled',true).css('cursor','not-allowed');
			} else {
				$('#deadline').datepicker('option',{disabled:false}).css('cursor','pointer');
				$('#notify_no').prop('disabled',false).css('cursor','pointer');
				$('#notify_yes').prop('disabled',false).css('cursor','pointer');
			}
		});

		$('#signoff_yes').click(function() {
			$('#returnuser').prop('disabled',true).css('cursor','not-allowed').css('background-color','#dedede');
			$('#deadline').datepicker('option',{disabled:true}).css('cursor','not-allowed');
			$('#notify_no').trigger('click').prop('disabled',true).css('cursor','not-allowed');
			$('#notify_yes').prop('disabled',true).css('cursor','not-allowed');
		});
		$('#signoff_no').click(function() {
			$('#returnuser').prop('disabled',false).css('cursor','pointer').css('background-color','#ffffff');
			$('#deadline').datepicker('option',{disabled:false}).css('cursor','pointer');
			$('#notify_no').trigger('click').prop('disabled',false).css('cursor','pointer');
			$('#notify_yes').prop('disabled',false).css('cursor','pointer');
		});

		";


		//<input type=hidden name=action value='".$js_page_action."' id=action />
		//<input type=hidden name=page_action value='".$js_page_redirect."' id=page_direct />
		//Generate Form DISPLAY
		$display .= "
			<div id=div_error class=div_frm_error>

			</div>
				<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">
				<input type=hidden name=sdbip_id value='".$sdbip_id."' id=sdbip_id />
				<input type=hidden name=object_id value='".$object_id."' id=object_id />
				<input type=hidden name=time_id value='".$time_id."' id=time_id />
				<table class='form' id=tbl_object_form_".$tbl_id.">";
		$has_attachment_field = false;
		foreach($headings as $fld => $head) {
			$h_type = $head['type'];
			$options = array('id' => $fld, 'name' => $fld);
			if($h_type == "STATUS") {
				$head['name'] = "Is this update complete?";
			}
			$display .= "<tr><th width=40%>".$head['name'].":</th><td>";
			switch($head['type']) {
				case "BOOL":
					$h_type = "BOOL_BUTTON";
					if($page_action != "VIEW") {
						$options['yes'] = "1";
						$options['no'] = "0";
						$options['extra'] = "boolButtonClickExtra";
						$options['small_size'] = true;
						if($fld == "signoff") {
							$val = 0;
						} else {
							$val = 1;
						}
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$echo = $this->getDataField($h_type, (isset($form_object[$fld]) ? $form_object[$fld] : "0"));
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				case "TEXT":
					$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
					if($page_action != "VIEW") {
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$display .= str_replace(chr(10), "<br />", $val);
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				case "DATE":
					$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
					if($page_action != "VIEW") {
						$options['options']['minDate'] = "'+0D'";
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$echo = $this->getDataField($h_type, $val);
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				case "UPDATE_USER":
					$h_type = "LIST";
					if($page_action != "VIEW") {
						$items = array(0 => "Do not return for Updating") + $admins;
						$options['options'] = $items;
						$val = $update_user;
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$echo = array('display' => "", 'js' => "");
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				case "ADMINS":
					if(count($email_admins) > 0) {
						$display .= "<ul>";
						foreach($email_admins as $id => $name) {
							$display .= "<li>".$name."<input type=hidden name=".$fld."[] value='$id' /></li>";
						}
						$display .= "</ul>";
					} else {
						$display .= $formObject->getUnspecified();
					}
					break;
				case "ATTACH":
					$val = $formObject->decodeAttachmentDataFromDatabase($form_object[$fld], true);
					if($page_action != "VIEW") {
						$has_attachment_field = true;
						$options['action'] = $js_page_action;
						$options['page_direct'] = $js_page_redirect;
						$options['page_activity'] = $page_action;
						$options['can_edit'] = true;
						$options['object_type'] = $form_object_type;
						$options['object_id'] = str_replace('.', '-', $form_object_id);
						$echo = $this->createFormField($h_type, $options, $val);
					} else {
						$options['object_type'] = $formObject->getMyObjectType();
						$options['object_id'] = $object_details['results'][$time_id][$formObject->getResultsParentFieldName()]."-".$time_id;
						$options['page_activity'] = "VIEW";
						$echo = $this->getAttachmentViewNoButton($val, $options);
					}
					$display .= $echo['display'];
					$js .= $echo['js'];
					break;
				default:
					$display .= $h_type;
					break;
			}
			$display .= "</td></tr>";
		}

		if($page_action != "VIEW") {
			$display .= "
			<tr>
				<td class=center colspan=2><button id=".$save_button.">".$formObject->replaceAllNames("|save|")."</button>";
			if(!$has_attachment_field) {
				//Even tho this form doesn't actually have attachments, it still needs to convince the inc_controller that it does so that it can use the same redirect process
				$display .= "
					<iframe id='file_upload_target' name='file_upload_target' src='' style='width:0px;height:0px;border:0px solid #000000;color:#000000;'></iframe>
					<input type=hidden name=has_attachments id=has_attachments value=1 />
					<input type=hidden name=action value='".$form_object_type.".".$page_action."' />
					<input type=hidden name=page_direct value='".$page_redirect_path."' />
				";
			}
			$display .= "
				</td>
			</tr>";
		}
		$display .= "
		</table></form>";


		//Return Data
		$data['display'] = $display;
		$data['js'] = $js;
		return $data;
	}


	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag)
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page)
	 */
	public function getObjectForm($form_object_type, $formObject, $form_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path, $display_form = true, $tbl_id = 0, $form_object = array(), $filter_by = array(), $external_call = false, $linked_object_call = false) {
//echo $formObject->getMyObjectType().":".$form_object_type;
		global $sdbip_details;
		global $mscoa_version_id;

		if($this->use_secondary_modref) {
			$local_modref = $this->secondary_modref;
		} else {
			$local_modref = "";
		}
		if(is_array($form_object_id) && isset($form_object_id['object'])) {
			$raw_object_id = $form_object_id;
			$form_object_id = $raw_object_id['object'];
			$time_id = $raw_object_id['time'];
		} else {
			$time_id = false;
		}

//echo "<br />getObjectForm modref: ".$local_modref;
		$last_deliverable_status = 0;
		if(is_array($form_object_type)) {
			$var = $form_object_type;
			$form_object_type = $var['object_type'];
			$form_object_id = $var['object_id'];
			$page_action = $var['page_action'];
			$page_redirect_path = $var['page_redirect_path'];
			$parent_object_id = $var['parent_id'];
			switch($form_object_type) {
				case "PARENT":
					$parent_object_type = "";
					$formObject = new SDBP6_PARENT($form_object_id, $local_modref);
					$parentObject = null;
					break;
			}
		}
		$sdbip_id = $parent_object_id;
//echo "|".$formObject->getMyObjectName();
		if(stripos($page_action, ".") !== false) {
			$pa = explode(".", $page_action);
			$page_action = $pa[1];
			$page_section = strtoupper($pa[0]);
		} else {
			$page_section = "MANAGE";
		}
		//target type settings
		$target_type_field = $formObject->getTargetTypeTableField();
		$has_target_type = $target_type_field !== false;
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE();
		//Calculation Type Settings
		$calc_type_field = $formObject->getCalcTypeTableField();

		$headingObject = new SDBP6_HEADINGS($local_modref);

		$th_class = "";
		$tbl_class = "";

		$attachment_form = false;

		$data = array('display' => "", 'js' => "", 'linked_objects' => array());
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		$pa = ucwords($form_object_type).".".$page_action;
		$pd = $page_redirect_path;

		$is_view_page = (strrpos(strtoupper($page_action), "VIEW") !== FALSE) || (strrpos(strtoupper($page_action), "RESTORE") !== FALSE);
		$is_restore_page = (strrpos(strtoupper($page_action), "RESTORE") !== FALSE);
		$is_edit_page = (strrpos(strtoupper($page_action), "EDIT") !== FALSE) || (strrpos(strtoupper($page_action), "COPY") !== FALSE);
		$is_update_page = (strrpos(strtoupper($page_action), "UPDATE") !== FALSE);
		$is_add_page = (strrpos(strtoupper($page_action), "ADD") !== FALSE) || (strrpos(strtoupper($page_action), "CREATE") !== FALSE);
		$is_copy_page = (strrpos(strtoupper($page_action), "COPY") !== FALSE);
		$is_manage_edit_page = $is_edit_page && $page_section == "MANAGE";    //needed to know whether or not to allow changes to the target type & calc type fields
		$manage_edit_fields_not_allowed_to_change = array($target_type_field, $calc_type_field); //needed for above
		$is_new_add_page = $is_add_page && ($page_section == "NEW");
		$is_new_edit_page = $is_edit_page && ($page_section == "NEW");
		$record_post_activation_activity = in_array($form_object_type, array(SDBP6_DEPTKPI::OBJECT_TYPE, SDBP6_TOPKPI::OBJECT_TYPE, SDBP6_PROJECT::OBJECT_TYPE));

		$extra_form_inputs = "";
		if($record_post_activation_activity) {
			if($is_add_page) {
				$extra_form_inputs = "<input type=hidden name=".$formObject->getTableField()."_late_add value='".($is_new_add_page ? 0 : 1)."' />";
			}
			if($is_edit_page) {
				$extra_form_inputs = "<input type=hidden name=".$formObject->getTableField()."_late_edit value='".($is_new_edit_page ? 0 : 1)."' />";
			}
		}

		if($page_action == "UPDATE") {
			$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($formObject->getMyChildObjectType(), "FORM"));
		} elseif($page_section == "SUMMARY") {
			$fld_prefix = "";//[JC 2019-08-06] inserted to addressed unknown variable in next line - need to identify why the field is there [HDR213]
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($form_object_type, "SUMMARY", ((strrpos(strtoupper($page_action), "ADD") !== FALSE) ? "NEW" : ""), $fld_prefix));
		} else {
			if(stripos($form_object_type, "ASSURANCE") === false) {
				$head_object_type = $form_object_type;
			} else {
				$head_object_type = "ASSURANCE";
			}
			$fld_prefix = $formObject->getTableField()."_";
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type, ($is_view_page ? "DETAILS" : "FORM"), ((strrpos(strtoupper($page_action), "ADD") !== FALSE) ? "NEW" : ""), $fld_prefix));
		}
		//ASSIST_HELPER::arrPrint($headings);
		//ASSIST_HELPER::arrPrint($headingObject->getHeadingsForLog());


		$copy_protected_fields = array();
		$copy_protected_heading_types = array();

		if($is_edit_page || $is_view_page) {
			if(count($form_object) == 0) {
				$form_object = $formObject->getRawObject($form_object_id);
			}
			$form_activity = "EDIT";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
			}
			//Prevent linked objects from displaying on Edit pages when restoring a deleted object to fix bug in AA-604 [JC] 5 May 2021
			if($is_view_page && !$is_restore_page && $linked_object_call == false) {
				$linked_objects = $formObject->getLinkedObjectDetailsForDisplay($form_object);
				$data['linked_objects'] = $linked_objects;
			}
		} elseif($is_update_page) {
			if(count($form_object) == 0) {
				if($time_id !== false) {
					$form_object = $formObject->getRawUpdateObject($form_object_id, $time_id);
				} else {
					$form_object = $formObject->getRawUpdateObject($form_object_id);
				}
			}
			$form_activity = "UPDATE";
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}

		//ASSIST_HELPER::arrPrint($form_object);
		if($is_view_page && $has_target_type && isset($form_object[$target_type_field]) && $form_object[$target_type_field] > 0) {
			$target_type = $targetTypeObject->getRawObjectDetails($form_object[$target_type_field]);
			$pre = $target_type['display_type'] != "post" ? $target_type['code']."&nbsp;" : "";
			$post = $target_type['display_type'] == "post" ? (substr($target_type['code'], 0, 1) != ":" ? "&nbsp;" : "").$target_type['code'] : "";
		} else {
			$target_type = array();
			$pre = "";
			$post = "";
		}

		$form_name = "frm_object_".$form_object_type."_".$form_object_id."_".strtolower(str_replace(".", "", $page_action));
//echo ":form_name:".$form_name.":";
		$js .= "  console.log('".$form_name."');
				var ".$form_name."_page_action = '".$pa."';
				var ".$form_name."_page_direct = '".$pd."';

		".$this->getAttachmentDownloadJS3($form_object_type, $form_object_id, $form_activity);


		/***************************************************************
		 * FORM SECTION STARTS HERE
		 * ".($page_section=="SUMMARY"?"width=100%":"")."
		 */

		$echo .= "
			<div id=div_error class=div_frm_error>

			</div>
			".($display_form ? "
				<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">".$extra_form_inputs : "")."
				<table class='form $tbl_class'  id=tbl_object_form_".$tbl_id.">";
		$form_valid8 = true;
		$form_error = array();
//ASSIST_HELPER::arrPrint($form_object);
		foreach($headings['rows'] as $fld => $head) {
			//echo "<P>".$fld;ASSIST_HELPER::arrPrint($head);
			if($head['parent_id'] == 0) {
				$val = "";
				$h_type = $head['type']; //echo "<hr /><h2>$h_type</h2>";
				if($h_type != "HEADING" && !in_array($h_type, $copy_protected_heading_types) && !in_array($fld, $copy_protected_fields)) {
					$display_me = true;
					$display_my_row = false;
					$options = array('id' => $fld, 'name' => $fld, 'req' => $head['required']);
					if($head['required'] == 1) {
						$options['title'] = "This is a required field.";
					}
					if(in_array($h_type, array("MASTER", "USER"))) {
						$list_items = array();
						switch($h_type) {
							case "USER":
								$listObject = new SDBP6_USERACCESS($local_modref);
								$list_items = $listObject->getActiveUsersFormattedForSelect();
								break;
							case "MASTER":
								$listObject = new SDBP6_MASTER($head['list_table'], $local_modref);
								$list_items = $listObject->getActiveItemsFormattedForSelect();
								break;
							default:
								echo $h_type;
								break;
						}
						$options['options'] = $list_items;
						$h_type = "LIST";
						if(count($list_items) == 0 && $head['required'] == 1) {
							$form_valid8 = false;
							$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
							$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
						}
						if($is_edit_page || $is_update_page) {
							$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && ($form_object[$fld] > 0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
						} elseif($is_view_page) {
							$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : $formObject->getUnspecified();
							if($formObject->checkIntRef($val)) {
								$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
							}
						} else {
							$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : "X";
						}


					} elseif($h_type == "LIST" || $h_type == "MULTILIST"
						|| $h_type == "SEGMENT" || $h_type == "MULTISEGMENT"
						|| $h_type == "OBJECT" || $h_type == "MULTIOBJECT"
					) {
						$allowed_to_display_form_field = !($is_view_page || ($is_manage_edit_page && in_array($fld, $manage_edit_fields_not_allowed_to_change)));
						$is_multi = false;
						$list_items = array();
						//get list items by list type
						$list_object_type = $head['list_table'];
						$extra_info = array();
						switch($h_type) {
							/* normal lists */
							case "MULTILIST":
								$is_multi = true;
							case "LIST":
								$listObject = new SDBP6_LIST($list_object_type, $local_modref);
								$listObject->setSDBIPID($sdbip_id);
								break;
							/* local objects within module */
							case "MULTIOBJECT":
								$is_multi = true;
							case "OBJECT":
								if(strpos($list_object_type, "|") !== false) {
									$lon = explode("|", $list_object_type);
									$list_object_type = $lon[0];
									$extra_info = $lon[1];
								}
								//echo "<p>".$fld." : ".$list_object_type;
								if($page_section == "MANAGE" && $page_action == "CREATE" && $fld == $formObject->getDepartmentFieldName()) {
									$filter_by = count($filter_by) > 0 ? $filter_by : $formObject->getFilterByOptions($page_section, $page_action);
									$list_items = array();
									foreach($filter_by['who'] as $aai => $aav) {
										if($aai == "SUB") {
											//ignore the ---responsible department--- header
										} else {
											$aai2 = explode("_", $aai);
											$list_items[$aai2[1]] = $aav;
										}
									}
								}
								if($list_object_type == "SDBP6_PROJECT" || $list_object_type == "SDBP6_TOPKPI") {
									//filter for items from given SDBIP
									$listObject = new $list_object_type($local_modref, 0, false, $sdbip_details['sdbip_id']);
								} else {
									$listObject = new $list_object_type($local_modref);
								}
								$listObject->setSDBIPID($sdbip_id);
								break;
							/* For mSCOA Segment Objects */
							case "MULTISEGMENT":
								$is_multi = true;
							case "SEGMENT":
								$options['alternative_display'] = true;
								//don't send local_modref like above as MSCOA has it's own fixed modref
								$listObject = new SDBP6_SEGMENTS($list_object_type, false, $mscoa_version_id);
								$extra_info['include_nickname'] = true;
								$extra_info['include_guid'] = true;
								break;
						}
						//if view page then get all items so that deactivated ones will still display with associated objects
						if(!$allowed_to_display_form_field) {
							$list_items = $listObject->getAllListItemsFormattedForSelect($extra_info);
						} else {
							//if not view page then only get those items that can be selected
							//if manage > create & fld = department then assume items variable set above in the admin access check section
							if(!($page_section == "MANAGE" && $page_action == "CREATE" && $fld == $formObject->getDepartmentFieldName())) {
								$list_items = $listObject->getActiveListItemsFormattedForSelect($extra_info);
							}
						}
						//not sure if this is required - possibly needed to handle lists linked to other lists? [JC - 6 July 2018]
						/*							if(isset($list_items['list_num'])) {
														$list_parent_association = $list_items['list_num'];
														$list_items = $list_items['options'];
														$options['list_num'] = $list_parent_association;
													}*/
						unset($listObject);
						//set generic options
						$options['options'] = $list_items;
						if(count($list_items) == 0 && $head['required'] == 1) {
							$form_valid8 = false;
							$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
							$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
						}
						//process form values depending on whether multi list or not
						if($is_multi) {
							//force field type to MULTILIST so that display knows what type of form field to draw
							$h_type = "MULTILIST";
							//set extra options needed for multi lists
							$options['unspecified'] = false;
							$options['name'] .= "[]";
							//get values
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : array();
							} else {
								//if not view/edit/update then assume add page and default to blank
								$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : array();
							}
							//check that value has been set to array type
							if(!is_array($val)) {
								//convert string to array & remove any blanks
								if(strlen($val) > 0) {
									$val = explode(";", $val);
									$val = ASSIST_HELPER::removeBlanksFromArray($val);
								} else {
									//else default to blank array
									$val = array();
								}
							}
							//check for detailed view page & convert values to list items
							if(!$allowed_to_display_form_field) {
								if(is_array($val) && count($val) > 0) {
									foreach($val as $key => $x) {
										if(isset($options['options'][$x])) {
											$val[$key] = $options['options'][$x];
										} else {
											unset($val[$key]);
										}
									}
									if(count($val) == 0) {
										$val = $formObject->getUnspecified();
									} else {
										$val = implode(";<br />", $val);
									}
								} else {
									$val = $formObject->getUnspecified();
								}
							} else {
								//do nothing for active forms
							}

						} else {    //else if is_multi
							//force field type to LIST so that display knows what type of form field to draw
							$h_type = ($h_type == "SEGMENT" ? "AUTO" : "LIST");
							//process values - MUST DO !allowed_to_display_form_field check first to pick up EDIT page with non-editable fields
							if(!$allowed_to_display_form_field) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : $formObject->getUnspecified();
								if($h_type != "AUTO" && $formObject->checkIntRef($val)) {
									$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
								} else {
									$options['raw_id'] = ($formObject->checkIntRef($val) && isset($options['options'][$val])) ? $val : "";
									$val = isset($options['options'][$val]) ? $options['options'][$val] : "";
								}
							} elseif($is_edit_page || $is_update_page) {
								if($h_type !== "AUTO") {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : 0;
									$options['raw_id'] = ($formObject->checkIntRef($val) && isset($options['options'][$val])) ? $val : "";
									$val = isset($options['options'][$val]) ? $options['options'][$val] : "";
								}
							} else {
								if($h_type !== "AUTO") {
									$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : "X";
								} else {
									$options['raw_id'] = "";
									$val = "";
								}
							}
						} //end if is_multi


					} elseif($h_type == "DATE") {
						$options['options'] = array();
						$options['class'] = "";
						if($is_edit_page || $is_update_page || $is_view_page) {
							$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 && strtotime($form_object[$fld]) > 0 ? date("d-M-Y", strtotime($form_object[$fld])) : "";
							if($is_update_page) {
								$options['options']['maxDate'] = "'+0D'";
							}
						} else {
							$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : "";
						}


					} elseif($h_type == "CURRENCY" || $h_type == "PERC") {
						$options['extra'] = "processCurrency";
						if($is_edit_page || $is_update_page || $is_view_page) {
							$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : "";
						} else {
							$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : "";
						}


					} elseif($h_type == "BOOL") {
						$h_type = "BOOL_BUTTON";
						$options['yes'] = "1";
						$options['no'] = "0";
						$options['extra'] = "boolButtonClickExtra";
						if($is_edit_page || $is_update_page || $is_view_page) {
							$val = isset($form_object[$fld]) && strlen($form_object[$fld]) > 0 && $form_object[$fld] > 0 ? $form_object[$fld] : "0";
						} else {
							$val = isset($head['default_value']) && strlen($head['default_value']) > 0 ? $head['default_value'] : "1";
						}
					} elseif($h_type == "REF") {
						if($page_action == "Add") {
							$val = "System Generated";
						} else {
							$val = $formObject->getRefTag().$form_object_id;
						}
					} elseif($h_type == "ATTACH") {
						$attachment_form = true;
						$options['action'] = $pa;
						$options['page_direct'] = $pd;
						$options['can_edit'] = ($is_edit_page || $is_update_page);
						$options['object_type'] = $form_object_type;
						$options['object_id'] = $form_object_id;
						$options['page_activity'] = $form_activity;
						$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
						if(substr($val, 0, 2) != "a:") {
							$val = base64_decode($val);
						}
//echo "<h1>::::".$val."</h1>";


					} elseif($h_type == "NUM" || ($h_type == "CALC" && $page_action == "UPDATE")) {
						$val = isset($form_object[$fld]) ? $form_object[$fld] : 0; //echo "<hr /><h3>".$val."</h3><hr />";
						//$val = ASSIST_HELPER::format_number($val);echo "<hr /><h3>".$val."</h3><hr />";
						$options['size']=20;

					} elseif($h_type == "LRGVC" || $h_type == "MEDVC") {
						$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
						$options['max'] = $head['max'];

					} else {
						if($is_edit_page || $is_update_page || $is_view_page) {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$val = isset($head['default_value']) ? $head['default_value'] : "";
						}
					}

					if($display_me) {
						if($is_view_page || ($h_type == "CALC" && $is_update_page) || ($is_manage_edit_page && in_array($fld, $manage_edit_fields_not_allowed_to_change))) {
							$display = $this->getDataField($h_type, $val); //ASSIST_HELPER::arrPrint($display);
						} else {
							$display = $this->createFormField($h_type, $options, $val);
						}
						$js .= $display['js'];
						if($formObject->hasTarget() == true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
							if($fld == $formObject->getTargetFieldName() || $fld == $formObject->getActualFieldName()) {
								$display['display'] .= " ".$form_object[$formObject->getTargetUnitFieldName()];
							}
						}

					}
				} elseif(in_array($fld, $copy_protected_fields) || in_array($h_type, $copy_protected_heading_types)) {
					$val = "";
					if($h_type == "ATTACH") {
						$val = "ATTACH"; //echo "<h1>".$val."</h1>";
					} else {
						$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

						if($headingObject->isListField($head['type']) && $head['section'] != "DELIVERABLE_ASSESS" && !in_array($head['type'], array("DEL_TYPE", "DELIVERABLE"))) {
							$val = ((!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
						} else {
							$v = $this->getDataField($head['type'], $val);
							$val = $v['display'];
						}
					}
					$display = array('display' => $val);
				} else {
					if($is_edit_page || $is_update_page) {
						$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
					} else {
						$pval = isset($head['default_value']) ? $head['default_value'] : "";
					}
					$sub_head = $headings['sub'][$head['id']];
					if(isset($form_object[$fld]) && is_array($form_object[$fld])) {
						$sub_form_object = $form_object[$fld];
					} else {
						$sub_form_object = isset($form_object[$fld]) ? array($form_object[$fld]) : array();
					}
					if(count($sub_form_object) == 0) {
						foreach($sub_head as $shead) {
							$sub_form_object[0][$shead['field']] = "";
						}
					}
					$td = "
						<div class=".$fld."><span class=spn_".$fld.">";
					$add_another[$fld] = false;
					foreach($sub_form_object as $sfo) {
						$td .= "<span>";
						$td .= "<table class=sub_form width=100%>";
						foreach($sub_head as $shead) {
							$sh_type = $shead['type'];
							$sfld = $shead['field'];
							$options = array('id' => $sfld, 'name' => $sfld, 'req' => $head['required']);
							$val = "";
							if($sh_type == "LIST") {
								$list_items = array();
								$listObject = new SDBP6_LIST($shead['list_table'], $local_modref);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								$options['options'] = $list_items;
								if(count($list_items) == 0 && $shead['required'] == 1) {
									$form_valid8 = false;
									$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if(count($list_items) > 1) {
									$add_another[$fld] = true;
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
								} else {
									$val = isset($shead['default_value']) && strlen($shead['default_value']) > 0 ? $shead['default_value'] : 0;
								}
								unset($listObject);
							} elseif($sh_type == "BOOL") {
								$sh_type = "BOOL_BUTTON";
								$options['yes'] = 1;
								$options['no'] = 0;
								$options['extra'] = "boolButtonClickExtra";
								if($is_edit_page || $is_update_page) {
									$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
								}
								if(strlen($val) == 0) {
									$val = (isset($shead['default_value']) && strlen($shead['default_value']) > 0) ? $shead['default_value'] : 0;
								}
							} elseif($sh_type == "CURRENCY") {
								$options['extra'] = "processCurrency";
								if($is_edit_page || $is_update_page) {
									$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
								} else {
									$val = isset($shead['default_value']) && strlen($shead['default_value']) > 0 ? $shead['default_value'] : "";
								}
							} else {
								if($is_edit_page || $is_update_page) {
									$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
								} else {
									$val = isset($shead['default_value']) && strlen($shead['default_value']) > 0 ? $shead['default_value'] : "";
								}
							}
							$sdisplay = $this->createFormField($sh_type, $options, $val);
							$js .= $sdisplay['js'];
							$td .= "
								<tr ".(strlen($shead['parent_link']) > 0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".($shead['required'] == 1 ? "*" : "")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
						}
						$td .= "
							</table></span>
							";
					}
					$td .= "
							</span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
					$td_blank = "";
					if($add_another[$fld]) {
						$td_blank = "<div id=".$fld."_blank><span>";
						$td_blank .= "<table class=sub_form width=100%>";
						foreach($sub_head as $shead) {
							$sh_type = $shead['type'];
							$sfld = $shead['field'];
							if($fld == "contract_supplier") {
								$options = array('name' => $sfld."[]", 'req' => $head['required']);
							} else {
								$options = array('id' => $sfld, 'name' => $sfld, 'req' => $head['required']);
							}
							$val = "";
							if($sh_type == "LIST") {
								$list_items = array();
								$listObject = new SDBP6_LIST($shead['list_table'], $local_modref);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								$options['options'] = $list_items;
								$val = isset($shead['default_value']) && strlen($shead['default_value']) > 0 ? $shead['default_value'] : 0;
								unset($listObject);
							} elseif($sh_type == "BOOL") {
								$sh_type = "BOOL_BUTTON";
								$options['yes'] = 1;
								$options['no'] = 0;
								$options['extra'] = "boolButtonClickExtra";
								$val = (isset($shead['default_value']) && strlen($shead['default_value']) > 0) ? $shead['default_value'] : 0;
							} elseif($sh_type == "CURRENCY") {
								$options['extra'] = "processCurrency";
								$val = isset($shead['default_value']) && strlen($shead['default_value']) > 0 ? $shead['default_value'] : "";
							}
							$sdisplay = $this->createFormField($sh_type, $options, $val);
							$td_blank .= "
												<tr ".(strlen($shead['parent_link']) > 0 ? "class=\"tr_".$shead['parent_link']."\"" : "").">
													<th width=40% class=th2>".$shead['name'].":".($shead['required'] == 1 ? "*" : "")."</th>
													<td>".$sdisplay['display']."</td>
												</tr>";
						}
						$td_blank .= "
											</table></span>
										</div>
											";
					}
					$display = array('display' => $td.$td_blank);
				}

				if($display_me || $display_my_row) {
					if($head['apply_formatting'] == true) {
						$display['display'] = $pre.$display['display'].$post;
					}
					$echo .= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link']) > 0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".(!$is_view_page && $head['required'] == 1 ? "*" : "")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
				}
			}
		}
		if(!$form_valid8) {
			$js .= "
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
		}
		$button_echo = "";
		if($is_restore_page) {
			$button_echo .= "
							<button id=btn_restore class='restore-btn'>".$formObject->getActivityName("RESTORE")." ".$formObject->getObjectName($formObject->getMyObjectName())."</button>
				";
		} else {
			//IF In NEW section then offer DELETE otherwise only DEACTIVATE (assumed post activation if not in NEW) [AA-525] JC
			if($is_new_add_page || $is_new_edit_page) {
				$delete_action = "DELETE";
			} else {
				$delete_action = "DEACTIVATE";
			}
			$button_echo .= "
						".($is_edit_page && !$is_manage_edit_page ? "<button class='float delete-btn' id=del_btn>".$formObject->getActivityName($delete_action)."</button>" : "")."
							<button id=btn_save class='save-btn'>".$formObject->getActivityName("SAVE")." ".$formObject->getObjectName($formObject->getMyObjectName())."</button>
						".($page_section == "NEW" && $is_add_page ? "<button id=btn_save_another class='save-btn'>".$formObject->getActivityName("SAVE")." & ".$formObject->getActivityName("ADD")." Another</button>" : "")."
						";
		}
		$button_echo .= "<input type=hidden name=object_id value=".$form_object_id." />
							".(($formObject->hasParent() !== false && $parent_object_id != 0) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" style='background-color:#FE9900;' />" : "")."
					";
		if(!$is_copy_page && !$is_view_page && !($formObject->hasKPIResults() === true || $formObject->hasFinances() === true)) {
			$echo .= "
					<tr>
						<th></th>
						<td>".$button_echo."</td>
					</tr>";
		} elseif($is_restore_page) {
			$echo .= "
					<tr>
						<th></th>
						<td>".$button_echo."</td>
					</tr>";
		}
		$echo .= "
			</table>";
		/************************************************************
		 * RESULTS SECTION STARTS HERE
		 */
		//KPI RESULTS SECTION
		if($formObject->hasKPIResults() === true && $page_section != "SUMMARY" && $page_action != "UPDATE" && !$is_restore_page) {


			$child_object_type = $formObject->getMyChildObjectType();
			$child_headings = $headingObject->getMainObjectHeadings($child_object_type, "DETAILS", $page_section);
			$time_type = $formObject->getTimeSetting();
//ASSIST_HELPER::arrPrint($child_headings);
			$object_details = $form_object;
//ASSIST_HELPER::arrPrint($object_details['results']);
			$time_periods = array();
			$child_form = $this->getChildObjectForm(strtoupper($page_action), $object_details, $child_headings, $time_periods, $parent_object_id, $form_name, $time_type, $page_section, array('pre' => $pre, 'post' => $post), $formObject, $external_call);
			$echo .= "
			<h3>".$formObject->getObjectName("RESULTS")."</h3>
			".$child_form['display'];//.ASSIST_HELPER::code($child_form['js']);
			$js .= $child_form['js'];


		}
		//PROJECT FINANCES SECTION
		if($formObject->hasFinances() === true && $page_section != "SUMMARY" && $page_action != "UPDATE") {
			$child_object_type = $formObject->getMyChildObjectType();
			$time_type = $formObject->getTimeSetting();
			$child_headings = $headingObject->getMainObjectHeadings($child_object_type, "DETAILS", $page_section);
			//$formObject->arrPrint($child_headings);
			$object_details = $form_object;
			$time_periods = array();
			//public function getChildObjectForm($page_action,          $object_details,$child_headings,$time_periods,$grand_parent_id=0,$form_name,$time_type,$page_section="MANAGE",$formatting=array(),$formObject=null) {
			$child_form = $this->getChildObjectForm(strtoupper($page_action), $object_details, $child_headings, $time_periods, $parent_object_id, $form_name, $time_type, $page_section, array('pre' => $pre, 'post' => $post), $formObject, $external_call);
			$echo .= "
			<h3>".$formObject->getObjectName("FINANCES")."</h3>
			".$child_form['display'];//.ASSIST_HELPER::code($child_form['js']);
			$js .= $child_form['js'];


		}


		//Show Save button if results table included
		if(!$is_copy_page && !$is_view_page && ($formObject->hasKPIResults() === true || $formObject->hasFinances() === true)) {
			$echo .= "<table width=100% style='margin-top:20px'><tr><td class=center>".$button_echo."</td></tr></table>";
		}


		/***********************************************************
		 * FORM SECTION ENDS HERE
		 */
		//IF In NEW section then offer DELETE otherwise only DEACTIVATE (assumed post activation if not in NEW) [AA-525] JC
		$delete_action = "DELETE";
		if($is_new_add_page || $is_new_edit_page) {
		} else {
			$delete_action = "DEACTIVATE";
		}
		$echo .= ($display_form ? "</form><div id=dlg_confirm></div>" : "");
		$data['js'] .= "
		//AssistHelper.processing();
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($formObject->getMyObjectName(true))." can be added:</p> <ul><li>".implode('</li><li>', $form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"

	//development code to SHOW HIDDEN FIELDS
	//$('input:hidden').prop('type','text');


				".$js."
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});


		function confirmSubmission(display_text,option,next_step) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>'+display_text+'</p><label id=lbl_hide></label>');
			if(option=='ok') {
				var buttons = [ { text: \"Ok\", click: function() { $( this ).dialog( \"close\" ); } } ];
			} else if(option=='continue') {
				var buttons = [
					{ text: \"Confirm & Continue\", click: function() {
						$( this ).dialog( \"close\" );
						nextStep(next_step);
					}, class: 'ui-state-ok' },
					{ text: \"Cancel\", click: function() {
						$(this).dialog(\"close\");
					}, class: 'ui-state-error' }
				];
			} else {
				var buttons = [
					{ text: \"Yes\", click: function() {
						nextStep(next_step);
						$( this ).dialog( \"close\" );
					}, class: 'ui-state-ok' },
					{ text: \"No\", click: function() {
						$( this ).dialog( \"close\" );
					}, class: 'ui-state-error' }
				];
			}
			$('#dlg_confirm').dialog('option','buttons',buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id','dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}
		function nextStep(step) {
		//IF In NEW section then offer DELETE otherwise only DEACTIVATE (assumed post activation if not in NEW) [AA-525] JC
			switch(step) {
				case 'DELETE':
					$"."form = $(\"form[name=".$form_name."]\");
					SDBP6Helper.processObjectForm($"."form,'".$form_object_type.".DELETE',".$form_name."_page_direct);
					break;
				case 'DEACTIVATE':
					$"."form = $(\"form[name=".$form_name."]\");
					SDBP6Helper.processObjectForm($"."form,'".$form_object_type.".DEACTIVATE',".$form_name."_page_direct);
					break;
				case 'RESTORE':
					$"."form = $(\"form[name=".$form_name."]\");
					SDBP6Helper.processObjectForm($"."form,'".$form_object_type.".RESTORE',".$form_name."_page_direct);
					break;
			}
		}


				";

		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then = strtotime($parent_deadline);
			$diff = ($then - $now) / (3600 * 24);
			$data['js'] .= " $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'] .= "


				$(\"form[name=".$form_name."] select\").each(function() {
					$(this).trigger('change');

					//if select only has 1 option + unspecified then auto select the second option
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});

				$(\"form[name=".$form_name."] button.delete-btn\").button({
					icons:{primary:'ui-icon-trash'}
				}).click(function(e) {
					e.preventDefault();
					//IF In NEW section then offer DELETE otherwise only DEACTIVATE (assumed post activation if not in NEW) [AA-525] JC
					confirmSubmission('Are you sure you wish to ".ASSIST_HELPER::code($formObject->getActivityName(strtolower($delete_action)))." this item? ".($delete_action == "DELETE" ? "Please note: This action cannot be undone." : "")."','continue','".$delete_action."');
					//$"."form = $(\"form[name=".$form_name."]\");
					//if(confirm('Are you sure you wish to delete this item?')) {
					//	SDBP6Helper.processObjectForm($"."form,'".$form_object_type.".DELETE',".$form_name."_page_direct);
					//}
				}).removeClass('ui-state-default').addClass('ui-button-minor-grey')
				.hover(function() {
					$(this).removeClass('ui-button-minor-grey').addClass('ui-button-minor-red');
				},function() {
					$(this).addClass('ui-button-minor-grey').removeClass('ui-button-minor-red');
				});

				$(\"form[name=".$form_name."] button.restore-btn\").button({
					icons:{primary:'ui-icon-check'}
				}).click(function(e) {
					e.preventDefault();
					confirmSubmission('Are you sure you wish to ".ASSIST_HELPER::code($formObject->getActivityName("restore"))." this item?','continue','RESTORE');
				}).removeClass('ui-state-default').addClass('ui-button-bold-grey')
				.hover(function() {
					$(this).removeClass('ui-button-bold-grey').addClass('ui-button-bold-green');
				},function() {
					$(this).addClass('ui-button-bold-grey').removeClass('ui-button-bold-green');
				});


				$(\"form[name=".$form_name."] button.save-btn\").button({
					icons:{primary:'ui-icon-disk'}
				}).removeClass('ui-state-default').addClass('ui-button-bold-green')
				.hover(function() {
					$(this).removeClass('ui-button-bold-green').addClass('ui-button-bold-orange');
				},function() {
					$(this).removeClass('ui-button-bold-orange').addClass('ui-button-bold-green');
				}).click(function(e) {
					e.preventDefault();
					AssistHelper.showProcessing();

					$"."form = $(\"form[name=".$form_name."]\");
					";
		if($attachment_form) {
			$data['js'] .= "
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//alert(f);
					if(f>0) {
						SDBP6Helper.processObjectFormWithAttachment($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					} else {
						$('#has_attachments').val(0);
						//alert(AssistForm.serialize($"."form));
						SDBP6Helper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					}
					";
		} else {
			$data['js'] .= "
			//FOR DEVELOPMENT TESTING - redirect to form_process.php (WARNING no validation is done as validation is handled by SDBP6Helper)
			//$"."form.prop('method','post');
			//$"."form.prop('action','form_process.php');
			//$"."form.submit();
if($(this).prop('id')=='btn_save_another') {
	var next_page = document.location.href;
} else {
	var next_page = ".$form_name."_page_direct;
}
					SDBP6Helper.processObjectForm($"."form,".$form_name."_page_action,next_page);
					";
		}
		$data['js'] .= "
					return false;
				});

				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}

				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");
				}
				";
		if(isset($add_another) && count($add_another) > 0) {
			foreach($add_another as $key => $aa) {
				if($aa == true) {
					$data['js'] .= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
				}
			}
		}

		$data['js'] .= "

				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});

AssistHelper.hideProcessing();

			";


		$data['display'] = $echo;
		return $data;


	}


	/*******************************************************************************************
	 * Function for creating the results/finance table
	 */
	public function getChildObjectForm($page_action, $object_details, $child_headings, $time_periods, $grand_parent_id = 0, $form_name = "", $time_type = "MONTH", $page_section = "MANAGE", $formatting = array(), $formObject = null, $external_call = false) {
		$pre = isset($formatting['pre']) ? $formatting['pre'] : "";
		$post = isset($formatting['post']) ? $formatting['post'] : "";
		//In Manage > Edit, no changing of targets are allowed so change the page_action to VIEW
		if($page_action == "EDIT" && $page_section == "MANAGE") {
			$page_action = "VIEW";
		}

		//determine what type of page this is - influences how the table is displayed
		//Added RESTORE page_action to VIEW page_action to address faulty results table on the Admin > Edit page of a deleted object AA-605 [JC] 5 May 2021
		$is_view_page = (strrpos(strtoupper($page_action), "VIEW") !== FALSE) || (strrpos(strtoupper($page_action), "RESTORE") !== FALSE);
		$is_edit_page = (strrpos(strtoupper($page_action), "EDIT") !== FALSE) || (strrpos(strtoupper($page_action), "COPY") !== FALSE);
		$is_update_page = (strrpos(strtoupper($page_action), "UPDATE") !== FALSE);
		$is_add_page = (strrpos(strtoupper($page_action), "ADD") !== FALSE) || (strrpos(strtoupper($page_action), "CREATE") !== FALSE);
		$is_copy_page = (strrpos(strtoupper($page_action), "COPY") !== FALSE);
		$is_formatted_results_page = $is_view_page || $is_edit_page;

		//create Calculation type Object - for calculating YTD results
		$calcTypeObject = new SDBP6_SETUP_CALCTYPE("");
		$calc_type_id = $formObject->getCalcTypeTableField() === false || $is_add_page ? 0 : $object_details[$formObject->getCalcTypeTableField()];
		$result_options = $formObject->getResultOptions();

		//Start prepping the data to be echoed onscreen
		$echo = "";
		$js = "
		$('.target-fields').addClass('right');
		var col_width = 0;
		$('#tbl_child_results th.get_width').each(function() {
			if($(this).width()>col_width) {
				col_width = $(this).width();
			}
		});
		$('#tbl_child_results th.get_width').width(col_width);
		$('#tbl_child_results th.result_width').width(30);
		$('#tbl_child_results td.td_ytd').css('background-color','#efefef');
		";

		if(!is_array($time_periods) || count($time_periods) == 0) {
			$timeObject = new SDBP6_SETUP_TIME();
			$time_periods = $timeObject->getActiveTimeObjectsFormattedForFiltering($grand_parent_id, $time_type);
		}
//Check columns to determine which must display and which belong to period/ytd performance columns
		$period_count = 0;
		$top_row = array();
		$ytd_headings = array();
		foreach($child_headings['rows'] as $fld => $head) { //echo "<hr />".$fld; ASSIST_HELPER::arrPrint($head);//.": ".$head['status'].": ".$head['is_calc_target'];
			if($is_add_page || $page_section == "NEW") {
				$display_column = false;
				if(($head['status'] & SDBP6_HEADINGS::RESULTS_TARGET) == SDBP6_HEADINGS::RESULTS_TARGET) {
					$display_column = true;
				} elseif(($head['status'] & SDBP6_HEADINGS::RESULTS_CALC_TARGET) == SDBP6_HEADINGS::RESULTS_CALC_TARGET) {
					$display_column = true;
				}
			} elseif($is_formatted_results_page && $head['update_comment'] == true && $head['type'] != "ATTACH") {
				$display_column = false;
			} else {
				$display_column = true;
			}
			$child_headings['rows'][$fld]['display_column'] = $display_column;
			if($display_column == true) {
				if($head['is_target'] || $head['is_actual'] || $head['is_result'] || $head['is_calc_target'] || ($head['type'] == "TEXT" && !$head['update_comment'])) {
					$period_count++;
				} else {
					$top_row[] = $fld;
				}
				if($head['is_calc_target'] || $head['is_actual'] || $head['is_result']) {
					$ytd_headings[$fld] = $head;
				}
			}
		}

		$echo .= "
		<table id=tbl_child_results>
			<tr>
				<th rowspan=2 id=th_time></th>
				<th colspan=".$period_count.">Period Performance</th>
				<th colspan=3>Year to Date Performance</th>";
		foreach($top_row as $fld) {
			$head = $child_headings['rows'][$fld];
			if($head['display_column']) {
				$class = "";
				$echo .= "
							<th rowspan=2 id=th_".$fld." $class >".str_replace(" ", "<br />", $head['name'])."</th>
						";

			}
		}
		$echo .= "
			</tr>
			<tr>
";
		$displayed_count = 0;
		foreach($child_headings['rows'] as $fld => $head) {
			if(!in_array($fld, $top_row)) {
				$display_column = $head['display_column'];
				if($display_column) {
					if($head['type'] == "TEXT") {
						$class = "";
					} elseif($head['is_result']) {
						$class = "class=result_width";
					} else {
						$class = "class=get_width";
					}
					$echo .= "
							<th id=th_".$fld." $class >".str_replace(" ", "<br />", $head['name'])."</th>
						";
					$displayed_count++;
					if($displayed_count == $period_count) {
						foreach($ytd_headings as $fld2 => $head2) {
							if($head2['type'] == "TEXT") {
								$class = "";
							} elseif($head2['is_result']) {
								$class = "class=result_width";
							} else {
								$class = "class=get_width";
							}
							$echo .= "
									<th id=th_ytd_".$fld." $class >".str_replace(" ", "<br />", $head2['name'])."</th>
								";
						}
					}
				}
			}
		}
		$echo .= "
			</tr>";
		//set variables to store results as loop through time periods - used for calculating YTD results
		$ytd_targets = array();
		$ytd_actuals = array();
		foreach($time_periods as $time_id => $time) { //echo "<hr /><h3>".$time['name']."</h3>";
			$displayed_count = 0;
			$target = 0;
			$actual = 0;
			$result = 0;
			$time_name = $time['name'];
			$time_has_started = $time['has_started'];
			$class = "";
			$echo .= "
			<tr>
				<td class=b>".$time_name."</td>";
			$display_comment = array();
			foreach($child_headings['rows'] as $fld => $head) {
				if($is_add_page) {
					$target = 0;
					$actual = 0;
					$ytd_targets[$time_id] = 0;
					$ytd_actuals[$time_id] = 0;
				} else {
					if($head['is_calc_target']) {
						$target = $object_details['results'][$time_id][$fld];
						$ytd_targets[$time_id] = $target;
					} elseif($head['is_actual']) {
						$actual = $object_details['results'][$time_id][$fld];
						$ytd_actuals[$time_id] = $actual;
					}
				}
				$display_column = $head['display_column'];
				if($fld == "kpir_attachment") {
//ASSIST_HELPER::arrPrint($head);
//				echo "<h1>".$fld."</h1>".$object_details['results'][$time_id][$fld];
				}
				if($head['update_comment'] == true && $head['type'] != "ATTACH") {
					if($head['type'] == "ATTACH") { //echo "How'd I end up here?";
						$val = $formObject->decodeAttachmentDataFromDatabase($object_details['results'][$time_id][$fld], false);
						$options['object_type'] = $formObject->getMyObjectType();
						$options['object_id'] = $object_details['results'][$time_id][$formObject->getResultsParentFieldName()]."-".$time_id;
						$options['page_activity'] = "VIEW";
						$x = $this->getAttachmentViewNoButton($val, $options);
						$display_comment[] = "<p class=b>".$head['name']."</p>".$x['display'];
						$js .= $x['js'];
						$class = "";
					} else {
						$val = isset($object_details['results'][$time_id][$fld]) ? $object_details['results'][$time_id][$fld] : "";
						if(strlen($val) > 0) {
							$display_comment[] = "<p class=b style='margin-bottom:3px;'>".$head['name'].":</p><p style='margin:0px 5px 0px 5px;'>".str_replace(chr(10), "<br />", $val)."</p>";
						}
					}
				} elseif($display_column) {
					$displayed_count++;
					if($head['type'] == "STATUS") {
						if(!is_null($formObject)) {
							$val = $formObject->generateStatusDisplay($object_details['results'][$time_id], false, $time_has_started);
						} else {
							$val = "Unknown Status";
						}
					} elseif($head['type'] == "RESULT") {
						$val = $calcTypeObject->calculatePeriodResult($calc_type_id, $target, $actual, $result_options);
					} else {
						$val = isset($object_details['results'][$time_id][$fld]) ? $object_details['results'][$time_id][$fld] : 0;
					}
					if($head['type'] == "TEXT" && $val == "0") {
						$val = "";
					}
					//Basic can_input calculation assuming all time periods are open (i.e. add page)
					$can_input = false;
					if($head['type'] == "NUM") {
						$class = "right";
						if(($is_add_page || $is_edit_page) && ($head['status'] & SDBP6_HEADINGS::RESULTS_TARGET) == SDBP6_HEADINGS::RESULTS_TARGET) {
							$can_input = true;
						} elseif($is_update_page && (($head['status'] & SDBP6_HEADINGS::RESULTS_ACTUAL) == SDBP6_HEADINGS::RESULTS_ACTUAL)) {
							$can_input = true;
						}
					} elseif($head['type'] == "CALC") {
						$can_input = true;
						$class = "right";
					} elseif($head['type'] == "TEXT") {
						if($is_view_page) {
							$can_input = false;
						} elseif($is_update_page && ($head['status'] * 1 & SDBP6_HEADINGS::RESULTS_UPDATE_COMMENTS) == SDBP6_HEADINGS::RESULTS_UPDATE_COMMENTS) {
							$can_input = true;
						} elseif(!$is_update_page && ($head['status'] * 1 & SDBP6_HEADINGS::RESULTS_UPDATE_COMMENTS) != SDBP6_HEADINGS::RESULTS_UPDATE_COMMENTS) {
							$can_input = true;
						}
					}
					//Now to do some more indepth can_input determinations for edit pages once time periods have started closing


					//end can_input determinations
					$options = array(
						'name' => $fld."[".$time_id."]",
						'id' => $fld."_".$time_id,
					);
					if($is_view_page || $head['is_target'] == false) {
						if($head['type'] == "ATTACH") {
							$val = $formObject->decodeAttachmentDataFromDatabase($object_details['results'][$time_id][$fld], false);
//							echo "<hr />"; ASSIST_HELPER::arrPrint($val);
							$options['object_type'] = $formObject->getMyObjectType();
							$options['object_id'] = $object_details['results'][$time_id][$formObject->getResultsParentFieldName()]."-".$time_id;
							$options['page_activity'] = "VIEW".($external_call ? "_EXTERNAL" : "");
							$options['buttons'] = true;//!$external_call;
							$x = $this->getAttachmentViewNoButton($val, $options);
							$class = "";
						} elseif($head['type'] == "RESULT") {
							$x = array('display' => $formObject->createResultDisplay($val));
							$class = "center";
						} elseif($head['type'] == "STATUS") {
							//$x = "STATUS";
							$x = $val;
							$class = "center";
						} elseif($head['type'] == "COMMENT") {
							$head['type'] = "TEXT";
							$val = implode("", $display_comment);
							$class = "";
							$x = array();
							$x['display'] = $val;
							$x['js'] = "";
						} else {
							$type = $head['type'] == "TEXT" ? "TEXT" : "NUM";
							if($type == "TEXT") {
								$class = "";
								$val = str_replace(chr(10), "<br />", $val);
							}
							$x = $this->getDataField($type, $val);
							if($type == "NUM") {
								$class = "right";
								$x['display'] = $pre.str_replace(" ", "&nbsp;", $x['display']).$post;
							}
						}
					} elseif(!$can_input) {
						if($head['type'] == "TEXT") {
							$class = "";
							$x = str_replace(chr(10), "<br />", $val);
						} else {
							$class = "right";
							$x = $this->getLabel(ASSIST_HELPER::format_number($val), $options);
						}
					} else {
						if($head['type'] == "NUM") {
							$class = "right";
							$options = $options + array(
									'required' => "required",
									'warn' => false,
									'class' => "target-fields",
								);
							$val = ASSIST_HELPER::format_number($val);
						} elseif($head['type'] == "CALC") {
							$class = "right";
							//add time id to each field for identification purposes
							$ex = explode("|", $head['list_table']);
							$ey = array();
							$count = count($ex) - 1;
							foreach($ex as $d => $v) {
								if($d == 0 || $d == $count) {
									$ey[$d] = $v;
								} else {
									$ey[$d] = $v."_".$time_id;
								}
							}
							$options['extra'] = implode("|", $ey);
							$options['form_name'] = $form_name;
							if(($head['status'] & SDBP6_HEADINGS::RESULTS_TRIGGER_NEXT_CALC) == SDBP6_HEADINGS::RESULTS_TRIGGER_NEXT_CALC) {
								$options['class'] = "calculate_next";
							}
						}
						$x = $this->createFormField($head['type'], $options, $val);
					}
					$echo .= "
						<td class='".$class."'>".(is_array($x) ? $x['display'] : $x)."</td>
					";
					if(isset($x['js'])) {
						$js .= $x['js'];
					}
					if($displayed_count == $period_count) {
						$values = $calcTypeObject->calculatePTDValues($calc_type_id, $ytd_targets, $ytd_actuals);
						foreach($ytd_headings as $ytd_fld => $ytd_head) {
							if($ytd_head['is_result']) {
								if($formObject->getMyObjectType() == "PROJECT") {
									$display = $pre.ASSIST_HELPER::format_number($values['target'] - $values['actual']).$post;
									$display = str_replace(" ", "&nbsp;", $display);
								} else {
									$class = "center";
									$display = $formObject->createResultDisplay($calcTypeObject->calculatePeriodResult($calc_type_id, $values['target'], $values['actual'], $result_options));
								}
							} else {
								$class = "right";
								if($ytd_head['is_actual']) {
									$display = $pre.ASSIST_HELPER::format_number($values['actual']).$post;
								} else {
									$display = $pre.ASSIST_HELPER::format_number($values['target']).$post;
								}
								$display = str_replace(" ", "&nbsp;", $display);
							}
							$echo .= "
								<td class='".$class." td_ytd'>".$display."</td>
							";
						}
					}
				} //end if display column
			}
			$echo .= "
			</tr>";
		}
		$echo .= "
		</table>

		";

		return array('display' => $echo, 'js' => $js);
	}


	/*******************
	 * JS code for handling forms displayed inside iframe inside dialogs
	 * Added close_parent_processing to accommodate the "are you sure you want to delete" pop-up in Setup > OrgStructure #AA-625 JC 3 June 2021
	 *
	 * @param (String) object_type
	 * @param (String) Dialog ID
	 * @param (Array) additional variables
	 * @param (Bool) Close the parent AssistHelper.processing() pop-up
	 * @return (String) JQuery
	 */
	public function getIframeDialogJS($object_type, $parent_dlg_id, $var = array(), $close_parent_processing = false) {
		$echo = "";

		$echo .= "
		var dlg_size_buffer = 70;
		var ifr_size_buffer = 20;
		window.parent.$('#".$parent_dlg_id."').dialog('open');
		".($close_parent_processing ? "window.parent.AssistHelper.closeProcessing();" : "")."
		";
		$echo .= "
		var my_height = $('table.tbl-container').css('height');
		if(AssistString.stripos(my_height,'px')>0) {
			my_height = parseInt(AssistString.substr(my_height,0,-2))+ifr_size_buffer;
			
			window.parent.$('#".$parent_dlg_id."').find('iframe').prop('height',(my_height)+'px');
			var dlg_height = window.parent.$('#".$parent_dlg_id."').dialog('option','height');
			var test_height = my_height+dlg_size_buffer;
			if(dlg_height > test_height) {
				window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
				var check = !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')));
				while(!check) {
					window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
					test_height+=dlg_size_buffer;
					if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')))) {
						check = true;
					}
				}
			}
		}
		";
		return $echo;
	}


	/**
	 * Returns filter for selecting parent objects according to section it is fed
	 *
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
		$data = array('display' => "", 'js' => "");
		$data['display'] = "
			<div id='filter'>";

		switch(strtoupper($section)) {
			case 'CONTRACT':
				$data['display'] .= "
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index => $key) {
					$data['display'] .= "<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js'] = "
					fyear = 0;

					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;

			case 'DELIVERABLE':
				$dsp = "contracts get";

				break;

			case 'ACTION':
				$dsp = "deliverables get";

				break;

			default:
				$dsp = "Invalid arguments supplied";
				break;
		}

		$data['display'] .= "
							</select>
						</td>
					</form>
				</table>
			</div>";

		return $data;
	}


	/**
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 *
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from SDBP6_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from SDBP6_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
	public function getListView($section, $fyears = array(), $headings = array()) {
		$data = array('display' => "", 'js' => "");
		switch(strtoupper($section)) {
			case "CONTRACT":
				$filter = $this->getFilter($section, $fyears);
				//For the financial year filter
				$data['display'] = $filter['display'];
				//For the list view table
				$data['display'] .=
					'<table class=tbl-container><tr><td>
										<table class=list id=master_list>
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';

				foreach($headings as $key => $val) {
					$string = "<th id='".$val['field']."' >".$val['name']."</th>";
					$data['display'] .= $string;
				}
				$data['display'] .=
					"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
				//For the javascript

				break;
			default:
				$data['display'] = "Lol, not for you";
				break;
		}

		return $data;
	}


	public function drawTreeTier($object_type, $level, $obj, $page_action, $extra_btn_class = "", $can_i_view = true, $can_i_edit = true, $text = "") {
		switch($page_action) {
			case "CONFIRM":
			case "VIEW":
				$this->drawTreeTierForView($object_type, $level, $obj, $extra_btn_class, $can_i_view, $text);
				break;
			default:
				$this->drawTreeTierForEdit($object_type, $level, $obj, $extra_btn_class, $can_i_view, $can_i_edit);
				break;
		}
	}

	public function drawTreeTierForEdit($object_type, $level, $obj, $extra_btn_class = "", $can_i_view = true, $can_i_edit = true) {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%; margin-left: 20px;'>[".$obj['reftag']."]</span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>" : "")."
				".($can_i_edit ? "<button class='action-button $extra_btn_class edit-btn' parent_id=".$obj['id'].">Edit</button>" : "")."
			</td>
		</tr>
		";

	}

	public function drawTreeTierForView($object_type, $level, $obj, $extra_btn_class = "", $can_i_view = true, $text = "") {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%;margin-left: 20px;'>$text".(strlen($text) > 0 ? "&nbsp;\&nbsp;" : "")."[".$obj['reftag']."]</span></span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>" : "")."
			</td>
		</tr>
		";

	}


	public function drawVisibleBlock($title, $lot, $text, $button_name, $color = "blue") {
		$data = $this->getVisibleBlock($title, $lot, $text, $button_name, $color);
		echo $data['display'];
		return $data['js'];
	}

	public function getVisibleBlock($title, $lot, $text, $button_name, $color = "blue") {
		$data = array(
			'display' => "",
			'js' => "",
		);

		$echo = "
		<div style='width:500px;' id=div_".$lot." class=div_segment page=".$lot." my_btn=btn_".$lot."_open>
		<h2 class='object_title $color' colorhistory='$color'>".$title."</h2>
				<Table class='form tbl-segment-description' width=100%>
			<tr>
				<td class='i'>".str_replace(chr(10), "<br />", $text)."</td>
			</tr>
		</Table>
		<p class=right><button class=btn_open id=btn_".$lot."_open>".$button_name."</button></p>
	</div>";


		$data['display'] = $echo;
		return $data;

	}


}

?>