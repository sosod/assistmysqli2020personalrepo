<?php
/**
 * To manage the Top Layer KPI objects of the SDBP6 module
 *
 * Created on: 8 May 2018
 * Authors: Janet Currie
 *
 */

class SDBP6_SETUP_RESULTS extends SDBP6 {


	protected $result_settings = array(
		0 => array('id' => 0, 'r' => 0, 'value' => "KPI Not Yet Measured", 'code' => "N/A", 'style' => "result0", 'color' => "#999999", 'glossary' => "KPIs with no targets or actuals in the selected period.", 'limit_int' => false, 'limit_link' => false, 'limit_decimal' => false),
		1 => array('id' => 1, 'r' => 1, 'value' => "KPI Not Met", 'code' => "R", 'style' => "result1", 'color' => "#CC0001", 'glossary' => "0% <= Actual/Target <= 74.99%", 'limit_int' => false, 'limit_link' => 2, 'limit_decimal' => 0.75),
		2 => array('id' => 2, 'r' => 2, 'value' => "KPI Almost Met", 'code' => "O", 'style' => "result2", 'color' => "#FE9900", 'glossary' => "75.000% <= Actual/Target <= 99.999%", 'limit_int' => 75, 'limit_link' => false, 'limit_decimal' => 0.75),
		3 => array('id' => 3, 'r' => 3, 'value' => "KPI Met", 'code' => "G", 'style' => "result3", 'color' => "#009900", 'glossary' => "Actual meets Target (Actual/Target = 100%)", 'limit_int' => 100, 'limit_link' => false, 'limit_decimal' => 1),
		4 => array('id' => 4, 'r' => 4, 'value' => "KPI Well Met", 'code' => "G2", 'style' => "result4", 'color' => "#005500", 'glossary' => "100.001% <= Actual/Target <= 149.999%", 'limit_int' => false, 'limit_link' => 5, 'limit_decimal' => 1.5),
		5 => array('id' => 5, 'r' => 5, 'value' => "KPI Extremely Well Met", 'code' => "B", 'style' => "result5", 'color' => "#000077", 'glossary' => "150.000 <= Actual/Target", 'limit_int' => 150, 'limit_link' => false, 'limit_decimal' => 1.5),
	);

	protected $default_result_settings = array(
		0 => array('id' => 0, 'r' => 0, 'value' => "KPI Not Yet Measured", 'code' => "N/A", 'style' => "result0", 'color' => "#999999", 'glossary' => "KPIs with no targets or actuals in the selected period.", 'limit_int' => false, 'limit_link' => false, 'limit_decimal' => false),
		1 => array('id' => 1, 'r' => 1, 'value' => "KPI Not Met", 'code' => "R", 'style' => "result1", 'color' => "#CC0001", 'glossary' => "0% <= Actual/Target <= 74.999%", 'limit_int' => false, 'limit_link' => 2, 'limit_decimal' => 0.75),
		2 => array('id' => 2, 'r' => 2, 'value' => "KPI Almost Met", 'code' => "O", 'style' => "result2", 'color' => "#FE9900", 'glossary' => "75%.000 <= Actual/Target <= 99.999%", 'limit_int' => 75, 'limit_link' => false, 'limit_decimal' => 0.75),
		3 => array('id' => 3, 'r' => 3, 'value' => "KPI Met", 'code' => "G", 'style' => "result3", 'color' => "#009900", 'glossary' => "Actual meets Target (Actual/Target = 100%)", 'limit_int' => 100, 'limit_link' => false, 'limit_decimal' => 1),
		4 => array('id' => 4, 'r' => 4, 'value' => "KPI Well Met", 'code' => "G2", 'style' => "result4", 'color' => "#005500", 'glossary' => "100.001% <= Actual/Target <= 149.999%", 'limit_int' => false, 'limit_link' => 5, 'limit_decimal' => 1.5),
		5 => array('id' => 5, 'r' => 5, 'value' => "KPI Extremely Well Met", 'code' => "B", 'style' => "result5", 'color' => "#000077", 'glossary' => "150.000% <= Actual/Target", 'limit_int' => 150, 'limit_link' => false, 'limit_decimal' => 1.5),
	);

	protected $result_settings_headings = array(
		'value' => "Name",
		'code' => "Short Code",
		'color' => "Colour",
		'limit_int' => "Limit",
		'glossary' => "Explanation",
	);

	protected $result_settings_management_properties = array(
		'settings_with_limit_int' => array(2, 5),
		'settings_with_linked_limit_int' => array(1, 4),
		'setting_with_fixed_limit' => 3,
		'setting_with_no_limit' => 0,
	);

	protected $ref_tag = "RS";


	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "RESULTS";
	const OBJECT_NAME = "RESULTS";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "setup_results";
	const TABLE_FLD = "results";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "setup";
	const LOG_SECTION = "results";


	public function __construct($modref = "") {
		parent::__construct($modref);
		$this->has_internal_ref_field = false;
		$this->getMyActiveResultSettings(false);

	}





	/*****************************************************************************************************************************
	 * General CONTROLLER functions
	 */


	/*******
	 * Functions to manage Result Settings - TAKEN FROM SDBP5B
	 * Added by JC in Dec 2017
	 */

	public function getResultSettingsManagementProperties() {
		return $this->result_settings_management_properties;
	}

	public function getDefaultResultSettings() {
		return $this->default_result_settings;
	}

	public function getResultSettingsHeadings() {
		return $this->result_settings_headings;
	}

	public function canICustomResultSettings() {
		$sql = "SHOW TABLES LIKE '".$this->getDBRef()."_setup_result_settings'";
		$check_for_result_settings_table = $this->mysql_fetch_all($sql);
		return (count($check_for_result_settings_table) > 0 ? true : false);
	}

	public function getCustomResultSettingsDirectlyFromDatabase($format_glossary = true) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_result_settings ORDER BY id";
		$data = $this->mysql_fetch_all_by_id($sql, "id");

		//fix bug in saving function to prevent having to do a db change (JC - AA-247 - 2019-10-20)
		$data[2]['limit_decimal'] = $data[2]['limit_int'] / 100;
		$data[5]['limit_decimal'] = $data[5]['limit_int'] / 100;


		//replace codes from glossary text with actual limits
		if($format_glossary) {
			$data = $this->formatGlossary($data);
		}
		return $data;
	}


	public function formatGlossary($data) {
		foreach($data as $i => $setting) {
			$x = $data[$i]['value'];
			$data[$i]['value'] = ASSIST_HELPER::decode($x);
			$x = $data[$i]['glossary'];
			$x = ASSIST_HELPER::decode($x);
			$x = str_replace("|limit2-|", ($data[2]['limit_int'] - 0.001), $x);
			$x = str_replace("|limit2|", $data[2]['limit_int'].".000", $x);
			$x = str_replace("|limit5-|", ($data[5]['limit_int'] - 0.001), $x);
			$x = str_replace("|limit5|", $data[5]['limit_int'].".000", $x);
			$data[$i]['glossary'] = $x;
		}
		return $data;
	}


	public function saveCustomResultSettings($var) {
		$result = array("error", "An error occured while trying to save your update.  Please try again.");
		$management_properties = $this->getResultSettingsManagementProperties();

		$old = $this->getCustomResultSettingsDirectlyFromDatabase(false);
		$result_settings = $old;
		$changes = array();
		$update_data = array();

		foreach($result_settings as $i => $setting) {
			foreach($setting as $key => $value) {
				if($key == "limit_int") {
					if(in_array($i, $management_properties['settings_with_limit_int'])) {
						if($var[$key][$i] != $result_settings[$i][$key]) {
							$changes[$i]['limit'] = array('from' => $result_settings[$i][$key]."%", 'to' => $var[$key][$i]."%");
							$result_settings[$i][$key] = $var[$key][$i];
							$update_data[$i][$key] = $var[$key][$i];
							$result_settings[$i]['limit_decimal'] = $var[$key][$i] / 100;
						}
					} elseif(in_array($i, $management_properties['settings_with_linked_limit_int'])) {
						$result_settings[$i]['limit_decimal'] = $var[$key][$setting['limit_link']] / 100;
						$update_data[$i]['limit_decimal'] = $result_settings[$i]['limit_decimal'];
					}
				} elseif(isset($var[$key][$i])) {
					if($var[$key][$i] != $result_settings[$i][$key]) {
						if($key == "value") {
							$log_name = "name";
						} else {
							$log_name = $key;
						}
						$changes[$i][$log_name] = array('from' => $result_settings[$i][$key], 'to' => $var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
						$update_data[$i][$key] = $var[$key][$i];
					}
				}
			}
		}


		foreach($update_data as $i => $setting) {
			$id = $i;
			foreach($setting as $key => $value) {
				$setting[$key] = ASSIST_HELPER::code($value);
			}
			$sql = "UPDATE ".$this->getDBRef()."_setup_result_settings SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = ".$id;
			$this->db_update($sql);

			if(isset($changes[$i]) && count($changes[$i]) > 0) {
				$log_changes = array(
						'user' => $this->getUserName(),
						'response' => "|resultssetting| ".$this->ref_tag.$i." modified:",
					) + $changes[$i];
				$log_var = array(
					'section' => self::LOG_SECTION,
					'object_id' => $i,
					'changes' => $log_changes,
					'log_type' => SDBP6_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
			}
		}
		//update session
		$result_settings = $this->formatGlossary($result_settings);
		$this->updateMyActiveResultSettings($result_settings);

		$result = array("ok", "Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message' => $result, 'result_settings' => $result_settings);
	}

	public function updateMyActiveResultSettings($result_settings = array()) {
		if(count($result_settings) > 0) {
			$_SESSION[$this->local_modref]['result_settings']['data'] = $result_settings;
		} else {
			$_SESSION[$this->local_modref]['result_settings']['data'] = $this->default_result_settings;
		}
		$_SESSION[$this->local_modref]['result_settings']['expiration'] = time() + 30 * 60;
	}

	public function getMyActiveResultSettings($return = false, $force = false) { //$force=true;
		//if the session has been set & the expiration time hasn't expired
		if(!$force && isset($_SESSION[$this->local_modref]['result_settings']['expiration']) && $_SESSION[$this->local_modref]['result_settings']['expiration'] > time() && isset($_SESSION[$this->local_modref]['result_settings']['data'])) {
			//use the current settings
			$this->result_settings = $_SESSION[$this->local_modref]['result_settings']['data'];
		} else {
			//check if table exists & is populated
			$check_for_result_settings_table = $this->canICustomResultSettings();
			if($check_for_result_settings_table) {
				//get from database
				$this->result_settings = $this->getCustomResultSettingsDirectlyFromDatabase();
				//update session
				$this->updateMyActiveResultSettings($this->result_settings);
			} else {
				//else use default
				$this->result_settings = $this->default_result_settings;
				//update session to prevent multiple database checks
				$this->updateMyActiveResultSettings($this->result_settings);
			}
		}
		if($return) {
			return $this->result_settings;
		}
	}


	public function getMyActiveResultSettingsForReporting() {
		return $this->getMyActiveResultSettings(true);
	}


	/*****************************************************************************************************************************
	 * GET functions
	 */

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getMyLogSection() {
		return self::LOG_SECTION;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}


	/*****************************************************************************************************************************
	 * SET functions
	 */


	public function __destruct() {
		parent::__destruct();
	}

}

?>