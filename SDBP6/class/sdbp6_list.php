<?php
/**
 * To manage the various list items
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class SDBP6_LIST extends SDBP6 {

	private $my_list = "";
	private $list_table = "";
	private $fields = array();
	private $field_names = array();
	private $field_types = array();
	private $associatedObject;
	private $object_table = "";
	private $object_field = "";
	private $parent_field = "";
	private $sort_by = "sort, name";
	private $sort_by_array = array("sort", "name");
	private $is_sort_function_available = false;
	private $my_name = "name";
	private $sql_name = "|X|name";
	private $is_multi_list = false;

	private $lists = array();
	private $default_field_names = array(
		'id' => "Ref",
		'sdbip_id' => "Parent |sdbip|",
		'name' => "Name",
		'default_name' => "Default Name",
		'client_name' => "Your Name",
		'shortcode' => "Short Code",
		'description' => "Description",
		'status' => "Status",
		'colour' => "Colour",
		'rating' => "Score",
		'list_num' => "Supplier Category",
		'sort' => "Display Order",
	);
	private $default_field_types = array(
		'id' => "REF",
		'sdbip_id' => "OBJECT",
		'default_name' => "LRGVC",
		'client_name' => "LRGVC",
		'name' => "LRGVC",
		'shortcode' => "SMLVC",
		'description' => "TEXT",
		'status' => "STATUS",
		'colour' => "COLOUR",
		'rating' => "RATING",
		'list_num' => "LIST",
		'sort' => "ORDER",
	);
	private $required_fields = array(
		'id' => false,
		'sdbip_id' => false,
		'name' => true,
		'default_name' => false,
		'client_name' => true,
		'shortcode' => true,
		'description' => false,
		'status' => false,
		'colour' => true,
		'rating' => true,
		'list_num' => true,
		'sort' => false,
	);


	/**
	 * @param {String} list     The list identifier
	 */
	public function __construct($list = "", $modref = "") {
		parent::__construct($modref);
		if(strlen($list) > 0) {
			//AA-666 - catching faulty inputs that add a / to the table name
			$list = preg_replace("/[^a-zA-Z0-9_]\w+/","",$list);
			$this->my_list = $list;
			$this->setListDetails();
		}
	}

	public function changeListType($list = "") {
		if(strlen($list) > 0) {
			//AA-666 - catching faulty inputs that add a / to the table name
			$list = preg_replace("/[^a-zA-Z0-9_]\w+/","",$list);
			$this->my_list = $list;
			$this->setListDetails();
		}
	}

	/**
	 * Setup / Defaults / Lists page - check if Sorting function must be available
	 */
	public function canISortThisList() {
		return $this->is_sort_function_available;
	}

	/**
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		if(!isset($var['status'])) {
			$var['status'] = self::ACTIVE;
		}
		return $this->addListItem($var);
	}

	public function editObject($var) {
		$id = $var['id'];
		return $this->editListItem($id, $var);
	}

	public function deleteObject($var) {
		$id = $var['id'];
		return $this->deleteListItem($id);
	}

	public function deactivateObject($var) {
		$id = $var['id'];
		return $this->deactivateListItem($id);
	}

	public function restoreObject($var) {
		$id = $var['id'];
		return $this->restoreListItem($id);
	}


	/***************************
	 * GET functions
	 **************************/
	public function getMyListName() {
		return $this->my_list;
	}

	public function getMyName() {
		return $this->my_name;
	}

	public function getSQLName($t = "") {
		return str_ireplace("|X|", (strlen($t) > 0 ? $t."." : ""), $this->sql_name);
	}

	public function getListTable() {
		//AA-666 - catching faulty inputs that add a / to the table name
		$list = preg_replace("/[^a-zA-Z0-9_]\w+/","",$this->list_table);
		if(strpos($list,"/")!==false) {
			$list = substr($list,0,strlen($list)-1);
		}
		return $list;
	}

	public function getSortBy($as_array = true) {
		if($as_array) {
			return $this->sort_by_array;
		} else {
			return $this->sort_by;
		}
	}

	public function getAllListTables($lists = array()) {
		$data = array();
		foreach($lists as $l) {
			$this->my_list = $l;
			$this->setListDetails();
			$data[$l] = $this->getListTable();
		}
		return $data;
	}

	/**
	 * Returns the field names for the given list
	 */
	public function getFieldNames() {
		return $this->field_names;
	}

	/**
	 * Returns the required fields for the given list
	 */
	public function getRequredFields() {
		return $this->required_fields;
	}

	/**
	 * Returns the field names for the given list
	 */
	public function getFieldTypes() {
		return $this->field_types;
	}

	/**
	 * Returns the field names and type for a given list, formatted as required by the centralised import function
	 */
	public function getHeadingsForImport() {
		$names = $this->getFieldNames();
		unset($names['id']);
		unset($names['status']);
		unset($names['sdbip_id']);
		$types = $this->getFieldTypes();
		$req = $this->getRequredFields();
		$rows = array();

		foreach($names as $fld => $name) {
			$max = 200;
			if($types[$fld] == "SMLVC") {
				$max = 15;
			} elseif($types[$fld] == "TEXT") {
				$max = 65000;
			}
			$rows[$fld] = array(
				'id' => $fld,
				'field' => $fld,
				'name' => $name,
				'section' => $this->getMyListName(),
				'type' => $types[$fld],
				'list_table' => $this->getMyListName(),
				'max' => $max,
				'parent_id' => 0,
				'parent_link' => "",
				'default_value' => "",
				'required' => $req[$fld],
				'help' => "",
			);
		}

		$data = array('rows' => $rows, 'subs' => array());
		return $data;
	}

	/**
	 * Returns list items in an array with the id as key depending on the input options
	 * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
	 * @return (Array of Records)
	 */
	public function getListItems($options = "") {
		$sql = "SELECT * ";
		if(in_array("client_name", $this->fields)) {
			$sql .= ", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
		}
		$sql .= " FROM ".$this->getListTable()." 
        WHERE (status & ".SDBP6::DELETED.") <> ".SDBP6::DELETED.(strlen($options) > 0 ? " AND ".$options : "")." 
        ORDER BY ".$this->sort_by;
//		echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//        echo "<P>".$sql;
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	/**
	 * Returns all list items which have been used formatted for reporting purposes
	 * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
	 * @return (Array of Records)
	 */
	public function getItemsForReport($options = "") {
		$options = (strlen($options) > 0 ? " (".$options.") AND " : "")
			."(status & ".SDBP6::DELETED." <> ".SDBP6::DELETED." ) "
			.($this->object_table != null ? "AND (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table."))" : "")
			.($this->sdbip_id > 0 ? " AND sdbip_id = ".$this->sdbip_id : "");
		$items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
	}

	public function getItemsInUse($parent_id = 0) {
		$options = "(status & ".SDBP6::DELETED." <> ".SDBP6::DELETED." )"
			.($this->object_table != null ? "AND (id IN (SELECT DISTINCT ".$this->object_field."
        			FROM ".$this->object_table
				.($parent_id > 0 ? " WHERE ".$this->parent_field." = ".$parent_id : "")
				."))" : "");
		//echo $options;
		$items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
	}

	/**
	 * Returns all active list items in an array with the id as key
	 * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
	 * @return (Array of Records)
	 */
	public function getActiveListItems($options = "") {
		$opt = "((status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." )";
		if(is_array($options)) {
			if(count($options) > 0) {
				$opt .= " AND ".$this->convertArrayToSQL($options, " AND ");
			}
		} elseif(strlen($options) > 0) {
			$opt .= " AND (".$options.")";
		}
		return $this->getListItems($opt);
	}

	/**
	 * gets list of available items for display in Setup > Defaults
	 */
	public function getCountOfActiveItems($sdbip_id = 0) {
		$options = $sdbip_id > 0 ? "sdbip_id = ".$sdbip_id : "";
		$list_items = $this->getActiveListItems($options);
		return count($list_items);
	}

	/**
	 * Returns all active list items formatted for display in SELECT => array with the id as key and value as element
	 * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
	 * @return (Array of Records)
	 */
	public function getActiveListItemsFormattedForSelect($options = array()) {
		$options['sdbip_id'] = $this->sdbip_id;
		$rows = $this->getActiveListItems($options);
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num", $this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
	}

	public function getActiveListItemForGraphReports($ignore_these_options = array()) {
		$options = array();
		$options['sdbip_id'] = $this->sdbip_id;
		$rows = $this->getActiveListItems($options);
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num", $this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
	}

	/**
	 * needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($options = array()) {
		$items = $this->getActiveListItemsFormattedForSelect();
		$data = array();
		foreach($items as $id => $name) {
			$name = trim(strtolower(str_replace(" ", "", $name)));
			$data[$name] = $id;
		}
		return $data;
	}


	public function getActiveListItemsNotInUseFormattedForSelect($parent_id, $is_edit_page = false, $edit_id = "") {
		$items_in_use = $this->getItemsInUse($parent_id);  //echo $parent_id; $this->arrPrint($items_in_use);
		$active_items = $this->getActiveListItemsFormattedForSelect(); //$this->arrPrint($active_items);
		$data = $active_items['options'];  //$this->arrPrint($data);
		//array_diff($data, $items_in_use);
		foreach($items_in_use as $i => $v) {
			if(isset($data[$i]) && (!$is_edit_page || $i != $edit_id)) {
				unset($data[$i]);
			}
		}
		if($is_edit_page && strlen($edit_id) > 0 && !isset($data[$edit_id])) {
			$data[$edit_id] = $items_in_use[$edit_id];
		}
		//$this->arrPrint($data);
		return $data;
		//return $active_items;
		//return $items_in_use;
	}


	/**
	 * Returns all list items not deleted
	 * @return (Array of Records)
	 */
	public function getAllListItems() {
		return $this->getListItems();
	}

	/**
	 * Returns all list items not deleted formatted for easy access
	 * @return (Array of Records)
	 */
	public function getAllListItemsFormattedForSelect($options = array()) {
		$opt = "";
		if(is_array($options)) {
			if(!isset($options['sdbip_id']) && $this->sdbip_id > 0) {
				$options['sdbip_id'] = $this->sdbip_id;
			}
			if(count($options) > 0) {
				$opt = $this->convertArrayToSQL($options, " AND ");
			}
		} elseif(strlen($options) > 0) {
			$opt = "(".$options.")";
		}

		$rows = $this->getListItems($opt);
		$dta = $this->formatRowsForSelect($rows, "name", true);
		if(in_array("list_num", $this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
	}

	/**
	 * Returns a specific list item determined by the input id
	 * @param (INT) id = The ID of the list item
	 * @param (Array) id = array of IDs to be found
	 * @return (One Record)
	 * OR
	 * @return (Array of Arrays) records found
	 */
	public function getAListItem($id) {
		if(is_array($id)) {
			if(count($id) > 0) {
				$data = $this->getListItems("id IN (".implode(",", $id).")");
				return $data;
			} else {
				return $this->getUnspecified();
			}
		} else {
			if($this->checkIntRef($id)) {
				$data = $this->getListItems("id = ".$id);
				return $data[$id];
			} else {
				return $this->getUnspecified();
			}
		}
	}

	/**
	 * returns the names of specific list items determined by the input id(s)
	 * @param (int) id = id of the list item to be found
	 * @param (array) id = array of ids to be found
	 * @return (String) name of list item
	 * OR
	 * @return (Array of Strings) names of the list items with the ID as index
	 */
	public function getAListItemName($id) {
		$data = $this->getAListItem($id);
		if(is_array($id)) {
			$items = array();
			foreach($data as $i => $d) {
				$items[$i] = $d['name'];
			}
			return $items;
		} else {
			return $data['name'];
		}
	}

	/**
	 * Returns a list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
	 * @param (INT) id = The ID of the list item
	 * @return (One record)
	 */
	public function getAListItemForSetup($id) {
		$data = $this->getAListItem($id);
		/*****
		 * TO BE PROGRAMMED:
		 *     $sql = SELECT count(O.$this->object_field) as in_use
		 *      FROM $this->object_table O
		 *      ON L.id = O.$this->object_field
		 *      WHERE O.$this->object_field = $id
		 *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
		 * $row = $this->mysql_fetch_one_value($sql,"in_use")
		 * parse results: if in_use > 0 then $data['can_delete'] = false else = true
		 *
		 */
		return $data;
	}

	/**
	 * Returns  list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
	 * @return (records)
	 */
	public function getListItemsForSetup($sdbip_id = 0) {
		$data = $this->getListItems("sdbip_id = ".$sdbip_id);
		/*****
		 * TO BE PROGRAMMED:
		 *     $sql = SELECT count(O.$this->object_field) as in_use
		 *      FROM $this->object_table O
		 *      ON L.id = O.$this->object_field
		 *      WHERE O.$this->object_field = $id
		 *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
		 * $row = $this->mysql_fetch_one_value($sql,"in_use")
		 * parse results: if in_use > 0 then $data['can_delete'] = false else = true
		 *
		 */
		if(count($data) > 0) {
			$keys = array_keys($data);
			if(is_array($this->object_field)) {
				$u = array();
				$used = array();
				foreach($this->object_field as $k => $of) {
					$ot = $this->object_table[$k];
					if($ot !== null) {
						$sql = "SELECT ".$of." as fld, count(".$of.") as in_use FROM ".$ot." WHERE ".$of." IN (".implode(",", $keys).") GROUP BY ".$of;
						$u[] = $this->mysql_fetch_all_by_id($sql, "fld");
						foreach($u as $f => $s) {
							$s['in_use'] = isset($s['in_use']) ? $s['in_use'] : 0;
							if(isset($used[$f])) {
								$used[$f]['in_use'] += $s['in_use'];
							} else {
								$used[$f] = $s;
							}
						}
					}
				}
			} elseif($this->is_multi_list && $this->object_table !== null) {
				$sql = "SELECT ".$this->object_field." as fld FROM ".$this->object_table." WHERE CHAR_LENGTH(".$this->object_field.")>0";
				$rows = $this->mysql_fetch_all_by_value($sql, "fld");
				$used = array();
				foreach($rows as $f) {
					$v = explode(";", $f);
					foreach($v as $x) {
						if(!isset($used[$x])) {
							$used[$x] = array('fld' => $x, 'is_use' => 1);
						} else {
							$used[$x]['in_use']++;
						}
					}
				}
			} elseif($this->object_table !== null) {
				$sql = "SELECT ".$this->object_field." as fld, count(".$this->object_field.") as in_use FROM ".$this->object_table." WHERE ".$this->object_field." IN (".implode(",", $keys).") GROUP BY ".$this->object_field;
				$used = $this->mysql_fetch_all_by_id($sql, "fld");
			}
			foreach($data as $key => $item) {
				if(!isset($used[$key]) || $used[$key]['in_use'] == 0) {
					$data[$key]['can_delete'] = true;
				} else {
					$data[$key]['can_delete'] = false;
				}
			}
		}
		return $data;
	}




	/****************************
	 * SET/UPDATE functions
	 *****************************/
	/**
	 * Create a new list item
	 * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
	 * @return TBD (INT) id of new value? or true/false to indicate success or failure
	 */
	public function addListItem($var) {
		$insert_data = $this->convertArrayToSQLForSave($var);
		$sql = "INSERT INTO ".$this->getListTable()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id > 0) {
			$result = array("ok", "List item added successfully.", $id);
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "|created| a new item ".$id.": ".$var[$this->getMyName()],
			);
			$log_var = array(
				'sdbip_id' => $var['sdbip_id'],
				'section' => $this->my_list,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);
		} else {
			$result = array("error", "Sorry, something went wrong while trying to add the list item. Please try again.", -1);
		}
		return $result;
	}

	public function addListItemFromExternalConversion($original_details) {
		$fields = $this->fields;
		$var = array();
		foreach($fields as $fld) {
			$var[$fld] = "";
		}
		$var['name'] = $original_details['value'];
		if(isset($var['code']) && isset($original_details['code'])) {
			$var['code'] = $original_details['code'];
		}
		$var['status'] = $original_details['status'];
		$var['sdbip_id'] = $this->sdbip_id;
		$result = $this->addListItem($var);
		return $result[2];
	}

	/**
	 * Add list item from external module import function
	 */
	public function addListItemFromExternalImport($name, $link) {
		$fields = $this->fields;
		$var = array();
		foreach($fields as $fld) {
			if($fld != "id" && $fld != "name") {
				$var[$fld] = "";
			}
		}
		$var['name'] = $name;
		$var['status'] = self::ACTIVE + self::EXTERNAL_IMPORT;
		$var['sdbip_id'] = $this->sdbip_id;
		$result = $this->addListItem($var);
		$id = $result[2];

		//add external link
		$link['local_id'] = $id;
		$link['status'] = self::ACTIVE;
		$link['insertuser'] = $this->getUserID();
		$link['insertdate'] = date("Y-m-d H:i:s");
		$sql = "INSERT INTO ".$this->getDBRef()."_list_import_external_links SET ".$this->convertArrayToSQLForSave($link);
		$this->db_insert($sql);
		return $id;
	}

	/**
	 * Edit a list item details
	 * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
	 * @return TBD (INT) id of new value? or true/false to indicate success or failure
	 */
	public function editListItem($id, $var) {
		$old_sql = "SELECT * FROM ".$this->getListTable()." WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "UPDATE ".$this->getListTable()." SET ".$insert_data." WHERE id = $id";
		$mar = $this->db_update($sql);
		if($mar > 0) {
			$result = array("ok", "List item modified successfully.");
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Item ".$id." modified.",
			);
			foreach($old_result as $key => $value) {
				if(isset($var[$key]) && $value != $var[$key]) {
					if($key == "list_num") {
						$listObject2 = new SDBP6_LIST("contract_supplier_category");
						$item_names = $listObject2->getAListItemName(array($var[$key], $value));
						$item_names[0] = $this->getUnspecified();
						$changes[$key] = array('to' => $item_names[$var[$key]], 'from' => $item_names[$value]);
					} else {
						$changes[$key] = array('to' => $var[$key], 'from' => $value);
					}
				}
			}
			$log_var = array(
				'sdbip_id' => $old_result['sdbip_id'],
				'section' => $this->my_list,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("info", "No changes were found to be saved. Please try again.");
		}
		return $result;
	}

	/**
	 * Delete a list item by removing the ACTIVE status and adding a DELETED status - the item should no longer show anywhere within the module
	 * @param (INT) id = id of list item to be deleted
	 * @return (BOOL) status of success as true/false
	 */
	public function deleteListItem($id) {
		$old_sql = "SELECT * FROM ".$this->getListTable()." WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);

		$sql = "UPDATE ".$this->getListTable()." SET status = (status - ".SDBP6::ACTIVE." + ".SDBP6::DELETED.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar > 0) {
			$result = array("ok", "List item ".$this->getActivityName("deleted")." successfully.");
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Item ".$id." |deleted|.",
			);
			$log_var = array(
				'sdbip_id' => $old_result['sdbip_id'],
				'section' => $this->my_list,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::DELETE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error", "Sorry, something went wrong while trying to remove the list item. Please try again.");
		}

		return $result;
	}

	/**
	 * Deactivate a list item by removing the ACTIVE status and adding INACTIVE status - the item should no longer be availavble in new or edit
	 * @param (INT) id = id of list item to be deactivated
	 * @return (BOOL) status of success as true/false
	 */
	public function deactivateListItem($id) {
		$old_sql = "SELECT * FROM ".$this->getListTable()." WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);

		$sql = "UPDATE ".$this->getListTable()." SET status = (status - ".SDBP6::ACTIVE." + ".SDBP6::INACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar > 0) {
			$result = array("ok", "List item ".$this->getActivityName("deactivated")." successfully.");
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Item ".$id." |deactivated|.",
			);
			$log_var = array(
				'sdbip_id' => $old_result['sdbip_id'],
				'section' => $this->my_list,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::DEACTIVATE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error", "Sorry, something went wrong while trying to deactivate the list item. Please try again.");
		}

		return $result;
	}

	/**
	 * Restore a deactivated list item by removing the INACTIVE status and adding ACTIVE - the list item should be available for use throughout the module
	 * @param (INT) id = id of list item to be restored
	 * @return (BOOL) status of success as true/false
	 */
	public function restoreListItem($id) {
		$old_sql = "SELECT * FROM ".$this->getListTable()." WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);

		$sql = "UPDATE ".$this->getListTable()." SET status = (status - ".SDBP6::INACTIVE." + ".SDBP6::ACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar > 0) {
			$result = array("ok", "List item ".$this->getActivityName("restored")." successfully.");
			$changes = array(
				'user' => $this->getUserName(),
				'response' => "Item ".$id." |restored|.",
			);
			$log_var = array(
				'sdbip_id' => $old_result['sdbip_id'],
				'section' => $this->my_list,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => SDBP6_LOG::RESTORE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error", "Sorry, something went wrong while trying to restore the list item. Please try again.");
		}

		return $result;

	}







	/********************************
	 * Protected functions: functions available for use in class heirarchy
	 ********************************/


	/****************************
	 * Private functions: functions only for use within the class
	 *********************/

	/**
	 * Sets the various variables needed by the class depending on the list name provided
	 */
	private function setListDetails() {
		$this->is_multi_list = false;
		$this->sort_by = "sort, name, shortcode";
		$this->sort_by_array = array("sort", "name", "shortcode");
		$this->my_name = "name";
		$this->sql_name = "|X|name";
		$this->list_table = $this->getDBRef()."_list_".$this->my_list;
		//AA-666 - catching faulty inputs that add a / to the table name
		$list = preg_replace("/[^a-zA-Z0-9_]\w+/","",$this->list_table);
		$this->list_table = $list;
		$this->is_sort_function_available = false;
		//"sort",
		$this->fields = array(
			"id",
			"sdbip_id",
			"shortcode",
			"name",
			"description",
			"status",
		);
		switch($this->my_list) {
			case "strategic_objective":
				$this->field_types['name'] = "TEXT";
				$this->associatedObject = null;
				$this->object_table = null;
				$this->object_field = null;
				break;
			case "owner":
				$this->fields = array(
					"id",
					"sdbip_id",
					"name",
					"description",
					"status",
				);
				$this->sort_by = "name";
				$this->sort_by_array = array("name");
				$this->associatedObject = null;
				$this->object_table = null;
				$this->object_field = null;
				/*$this->associatedObject = new IDP3_PMKPI();
				$this->object_table = $this->associatedObject->getTableName();
				$this->object_field = $this->associatedObject->getTableField()."_***_id";
				*/
				break;
			case "kpi_natoutcome":
				$this->fields = array(
					"id",
					"sdbip_id",
					"name",
					"description",
					"status",
				);
				$this->associatedObject = null;
				$this->object_table = null;
				$this->object_field = null;
				/*$this->associatedObject = new IDP3_PMKPI();
				$this->object_table = $this->associatedObject->getTableName();
				$this->object_field = $this->associatedObject->getTableField()."_***_id";
				*/
				break;
			case "kpi_pdo":
				$this->field_types['name'] = "TEXT";
				$this->associatedObject = null;
				$this->object_table = null;
				$this->object_field = null;
				/*$this->associatedObject = new IDP3_PMKPI();
				$this->object_table = $this->associatedObject->getTableName();
				$this->object_field = $this->associatedObject->getTableField()."_***_id";
				*/
				break;
			case "kpi_munkpa":
				$this->field_types['name'] = "TEXT";
			case "project_type":
			case "measurement_unit":
			case "kpi_riskrating":
			case "town":
			case "area":
			case "kpi_natkpa":
			case "report_cycle":
			case "kpi_concept":
			case "kpi_type":
			case "kpi_pso":
			case "kpi_ndp":
			case "kpi_mdg":
			case "kpi_iudf":
			default:
				$this->associatedObject = null;
				$this->object_table = null;
				$this->object_field = null;
				/*$this->associatedObject = new IDP3_PMKPI();
				$this->object_table = $this->associatedObject->getTableName();
				$this->object_field = $this->associatedObject->getTableField()."_***_id";
				*/
				break;


		}
		if($this->my_name != "name") {
			$this->sql_name = " if(length(|X|client_name) > 0, |X|client_name, |X|default_name) ";
		} else {
		}
		foreach($this->fields as $key) {
			if(!isset($this->field_names[$key])) {
				$this->field_names[$key] = $this->default_field_names[$key];
			}
			if(!isset($this->field_types[$key])) {
				$this->field_types[$key] = $this->default_field_types[$key];
			}
		}
	}


	/**
	 * @param $source_sdbip_id
	 * @param $destination_sdbip_id
	 * @return array
	 */
	public function copySDBIPSpecificSetup($source_sdbip_id, $destination_sdbip_id) {
		$sql = "SELECT * FROM ".$this->getListTable()." WHERE sdbip_id = ".$source_sdbip_id." AND status & ".SDBP6::ACTIVE." = ".SDBP6::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		$object_ids = array();

		if(count($rows) > 0) {
			foreach($rows as $src_id => $row) {
				unset($row['id']);
				$row['sdbip_id'] = $destination_sdbip_id;
				$new_result = $this->addListItem($row);
				$new_id = $new_result[2];
				$object_ids[$src_id] = $new_id;
				$this->recordImportOfListItemFromLocalModule($new_id, $source_sdbip_id, $src_id, $this->my_list, "LIST|".$this->my_list);    //needed for import from SDBP6 [JC] AA-419 8 June 2020
			}
		}

		return $object_ids;
	}

	public function getCopiedListItems($source_sdbip_id, $my_sdbip_id) {
		$local_list = $this->my_list;
		$this->setSDBIPID($my_sdbip_id);
		$new_items = $this->getAllListItemsFormattedForSelect();
		$new_ids = array_keys($new_items);
		return $this->getCopiedItemsFromSDBIPSetup($source_sdbip_id, $new_ids, $local_list);
	}


}

?>