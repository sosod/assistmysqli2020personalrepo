<?php

class SDBP6_IDP3 extends SDBP6 {

	private $my_modref;
	private $my_dbref;
	private $my_cmpcode;
	private $sdbp6_object;
	private $my_object;
	private $ext_object;


	private $field_type_map;

	private $allowed_map_matches;

	public function __construct($modref = "", $sdbp6_object = "", $cmpcode = "", $create_source_object = false) {
		parent::__construct();
		$this->my_modref = strtolower($modref);
		$this->my_cmpcode = strlen($cmpcode) > 0 ? strtolower($cmpcode) : strtolower($this->getCmpCode());
		$this->my_dbref = "assist_".$this->my_cmpcode."_".$this->my_modref;
		$this->sdbp6_object = $sdbp6_object;
		switch($sdbp6_object) {
			case "PROJECT":
				$this->my_object = "PROJECT";
				break;
			case "TOPKPI":
				$this->my_object = "PMKPI";
				break;
		}
		$this->ext_object = new IDP3_EXTERNAL($modref, $this->my_object, $create_source_object);
		$this->field_type_map = array(
			'REF' => "System Reference",
			'MEDVC' => "Medium Text",
			'LRGVC' => "Large Text",
			'TEXT' => "Unlimited Text",
			'BOOL' => "Yes/No",
			'ATTACH' => "Attachment",
			'PERC' => "Percentage",
			'DATE' => "Date",
			'LIST' => "List",
			'NUM' => "Number",
			'MULTILIST' => "Multi-select List",
			'SEGMENT' => "mSCOA Segment List",
			'MULTISEGMENT' => "Multi-select mSCOA Segment List",
			'OBJECT' => "List of Module Objects",
			'PROJECT' => "List of Module Objects",
			'MULTIOBJECT' => "Multi-select List of Module Objects",
		);

		$this->allowed_map_matches = array(
			'DATE' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'BOOL' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'LIST' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'SEGMENT' => array(
				'REF' => false,
				'MEDVC' => false,
				'LRGVC' => false,
				'TEXT' => false,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => false,
				'MULTILIST' => false,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => true,
				'MULTISEGMENT' => true,
				'NUM' => false,
			),
			'OBJECT' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => true,
				'MULTIOBJECT' => true,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => false,
			),
			'TEXT' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => true,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => true,
				'LIST' => true,
				'MULTILIST' => true,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => true,
			),
			'NUM' => array(
				'REF' => false,
				'MEDVC' => true,
				'LRGVC' => true,
				'TEXT' => true,
				'BOOL' => false,
				'ATTACH' => false,
				'PERC' => false,
				'DATE' => false,
				'LIST' => false,
				'MULTILIST' => false,
				'OBJECT' => false,
				'MULTIOBJECT' => false,
				'SEGMENT' => false,
				'MULTISEGMENT' => false,
				'NUM' => true,
			),
		);
		$this->allowed_map_matches['MEDVC'] = $this->allowed_map_matches['TEXT'];
		$this->allowed_map_matches['LRGVC'] = $this->allowed_map_matches['TEXT'];
		$this->allowed_map_matches['MULTILIST'] = $this->allowed_map_matches['LIST'];
		$this->allowed_map_matches['MULTIOBJECT'] = $this->allowed_map_matches['OBJECT'];
		$this->allowed_map_matches['MULTISEGMENT'] = $this->allowed_map_matches['SEGMENT'];
	}

	public function getDefaultFieldMap() {
		$project_type = SDBP6_PROJECT::OBJECT_TYPE;
		$deptkpi_type = SDBP6_DEPTKPI::OBJECT_TYPE;
		$topkpi_type = SDBP6_TOPKPI::OBJECT_TYPE;
		/***
		 * Field_map: ext_mod_field => local_mod_field
		 * 'proj_mscoa_ref'=>"proj_mscoa_ref",
		 * 'proj_id'=>"proj_id",
		 */
		switch($this->sdbp6_object) {
			case $project_type:
				$field_map = array(
					'proj_ref' => "proj_ref",
					'proj_name' => "proj_name",
					'proj_description' => "proj_description",
					'proj_expresult_id' => "proj_expresult_id",
					'proj_type_id' => "proj_type_id",
					'proj_cate_id' => "proj_cate_id",
					'proj_subdir_id' => "proj_msc_id",
					'proj_function_id' => "proj_function_id",
					'proj_town' => "proj_town",
					'proj_area' => "proj_area",
					'proj_ward' => "proj_ward",
					'proj_gps' => "proj_gps",
					'proj_riskref' => "proj_riskref",
					'proj_risk' => "proj_risk",
					'proj_startdate' => "proj_startdate",
					'proj_enddate' => "proj_enddate",
					'proj_actions' => "proj_actions",
					'proj_cap_op' => "proj_cap_op",
				);
				break;
			case $topkpi_type:
				$field_map = array('kpi_ref' => 'top_ref',
					'kpi_name' => 'top_name',
					'kpi_description' => 'top_description',
					'kpi_unit_id' => 'top_unit_id',
					'kpi_baseline' => 'top_baseline',
					'kpi_soe' => 'top_soe',
					'kpi_concept_id' => 'top_concept_id',
					'kpi_type_id' => 'top_type_id',
					'kpi_perfstd' => 'top_perfstd',
					'kpi_comment' => 'top_comment',
					'kpi_expresult_id' => 'top_expresult_id',
					'kp_proj_id' => 'top_proj_id',
					'kpi_owner_id' => 'top_owner_id',
					'kpi_subdir_id' => 'top_subdir_id',
					'kpi_function_id' => 'top_function_id',
					'kpi_natkpa_id' => 'top_natkpa_id',
					'kpi_pso_id' => 'top_pso_id',
					'kpi_ndp_id' => 'top_ndp_id',
					'kpi_natoutcome_id' => 'top_natoutcome_id',
					'kpi_pdo_id' => 'top_pdo_id',
					'kpi_riskref' => 'top_riskref',
					'kpi_risk' => 'top_risk',
					'kpi_riskrating_id' => 'top_riskrating_id',
					'kpi_new' => 'top_new',
					'kpi_limits' => 'top_limits',
					'kpi_reportcycle_id' => 'top_reportcycle_id',
					'kpi_ward' => 'top_ward',
					'kpi_cap_op' => 'top_cap_op',
					'kpi_mdg_id' => 'top_mdg_id',
					'kpi_iudf_id' => 'top_iudf_id',
					'kpi_sopia' => 'top_sopia',
					'kpi_sopptc' => 'top_sopptc',
					'kpi_sopsys' => 'top_sopsys',
					'kpi_sopreporting' => 'top_sopreporting',
					'kpi_sopoutput' => 'top_sopoutput',
					'kpi_prog_id' => 'top_munkpa_id',
				);
				break;
			case $deptkpi_type:
				break;
			default:
				$field_map = array();
				break;
		}
		return $field_map;
	}

	public function getHeadingsForMapping($field_map = array(), $include_type_in_name = true, $include_details = false) {
		$dbref = $this->my_dbref;
		$fields = array();
		if(!is_array($field_map) || count($field_map) == 0) {
			$field_map = $this->getDefaultFieldMap();
		}
		if(is_array($field_map) && count($field_map) > 0) {
			$sql = "SELECT h_default as h_ignite, h_client, h_type, h_field as field, h_table FROM  ".$dbref."_setup_headings 
			 		WHERE h_field IN ('".implode("','", array_keys($field_map))."') AND (h_status & ".self::ACTIVE.") = ".self::ACTIVE;
			$rows = $this->mysql_fetch_all_by_id($sql, "field");
			foreach($field_map as $fld => $f) {
				if(isset($rows[$fld])) {
					if($include_type_in_name === true) {
						if(isset($rows[$fld])) {
							$name = "<span class=b>".(strlen($rows[$fld]['h_client']) > 0 ? $rows[$fld]['h_client'] : $rows[$fld]['h_ignite'])."</span>";
							$name .= isset($this->field_type_map[$rows[$fld]['h_type']]) ? "<br /><span class='float normal i'>[".$this->field_type_map[$rows[$fld]['h_type']]."]</span>" : "";
						} else {
							$name = $fld;
						}
					} elseif(isset($rows[$fld])) {
						$name = (strlen($rows[$fld]['h_client']) > 0 ? $rows[$fld]['h_client'] : $rows[$fld]['h_ignite']);
					} else {
						$name = $fld;
					}
					if($include_details === true) {
						$fields[$fld] = array('name' => $name,
							'type' => "TEXT",
							'list' => "",);
						if(isset($rows[$fld])) {
							$fields[$fld]['type'] = $rows[$fld]['h_type'];
							$fields[$fld]['list'] = $rows[$fld]['h_table'];
						}
					} else {
						$fields[$fld] = $name;
					}
				} else {
					switch($fld) {
						case "kpi_prog_id":
							$name = "|".IDP3_PMPROG::OBJECT_NAME."|";
							if($include_type_in_name) {
								$name = "<span class=b>".$name."</span><br /><span class='float normal i'>[".$this->field_type_map["OBJECT"]."]</span>";
							}
							if($include_details) {
								$fields[$fld] = array('name' => $name,
									'type' => "OBJECT",
									'list' => "IDP3_PMPROG",);
							} else {
								$fields[$fld] = $name;
							}
							break;
						default:
							$fields[$fld] = $fld;
					}
				}
			}
			if(count($fields) > 0) {
				$fields = $this->ext_object->replaceExternalNames($fields);
			}
		}
		return $fields;
	}


	/**
	 * Get list items - customise specific to IDP3 module
	 */
	public function getListItems($field_map, $old_field_settings, $modref = "", $idp_id = 0) {
		$old_lists = array('raw' => array(), 'comparison' => array());
		foreach($field_map as $old_field => $new_field) {
			if($new_field != "0" && $new_field != false && isset($old_field_settings[$old_field]['type']) && isset($old_field_settings[$old_field]['list']) && strlen($old_field_settings[$old_field]['list']) > 0) {
				$list = $old_field_settings[$old_field]['list'];
				$send_id = false;
				switch($old_field_settings[$old_field]['type']) {
					case "SEGMENT":
					case "MULTISEGMENT":
						$extObject = new IDP3_SEGMENTS($list, $modref);
						$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForSelect();
						break;
					case "OBJECT":
					case "MULTIOBJECT":
						$send_id = true;
						$extObject = new $list(0, $modref, true);
						$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForExternalModule($idp_id);
						break;
					case "LIST":
					case "MULTILIST":
						$extObject = new IDP3_LIST($list, $modref);
						$old_lists['raw'][$list] = $extObject->getActiveListItemsFormattedForSelect();
						break;
				}
				/*if(!isset($extObject) ){
					//do nothing - list type not accounted for!
					echo $list.":".$old_field_settings[$old_field]['type'];
				}*/
				unset($extObject);
			} else {
				//do nothing if field is not in use
			}
		}
		if(count($old_lists['raw']) > 0) {
			foreach($old_lists['raw'] as $list => $l) {
				foreach($l as $key => $v) {
					$old_lists['comparison'][$list][$key] = $this->formatTextForComparison($v);
				}
			}
		}
		return $old_lists;
	}


	public function getAllowedMapMatches() {
		return $this->allowed_map_matches;
	}


	public function getSourceRecords($idp_id) {
		/*$modloc = "IDP3";
		$id_field = $this->getAltModuleIDField($modloc);
		$tblref = $dbref."_".$table;
		$records = array();
		//get object details
		$sql = "SELECT * FROM ".$tblref." LIMIT ".$start.", ".$limit;
		$records = $this->mysql_fetch_all_by_id($sql,$id_field);*/
		$records = $this->ext_object->getSourceRecords($this->my_object, $idp_id);
		return $records;
	}


	public function getRefTag() {
		return $this->ext_object->getRefTag();
	}


	public function __destruct() {
		parent::__destruct();
	}

}


?>