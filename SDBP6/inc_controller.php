<?php //error_reporting(-1);
require_once("../module/autoloader.php");
$result = array("info", "Sorry, I could not figure out what you wanted me to do.".serialize($_REQUEST));

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */

$my_action = explode(".", $_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);

$page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : false;
unset($_REQUEST['page_direct']);
$has_attachments = isset($_REQUEST['has_attachments']) ? $_REQUEST['has_attachments'] : false;
unset($_REQUEST['has_attachments']);
$attach = array();

$activity_log_handled_by_class = array("USERACCESS", "LISTS", "MENU", "NAMES", "SETUP_ORGSTRUCTURE", "SETUP_RESULTS", "SETUP_ADMINS",
	"OBJECT", "SETUP_CALCTYPE"
);

switch($class) {
	case "DISPLAY":
		$object = new SDBP6_DISPLAY();
		break;
	/**
	 * Miscellaneous objects
	 */
	case "LOG":
		$log_table = $_REQUEST['log_table'];
		unset($_REQUEST['log_table']);
		$object = new SDBP6_LOG($log_table);
		break;
	/**
	 * Setup objects
	 */
	case "USERACCESS":
		$add_log = false;
		$object = new SDBP6_USERACCESS();
		break;
	case "LISTS":
		$list_id = $_REQUEST['list_id'];
		unset($_REQUEST['list_id']);
		$object = new SDBP6_LIST($list_id);
		break;
	case "MENU":
		$object = new SDBP6_MENU();
		break;
	case "NAMES":
		$object = new SDBP6_NAMES();
		break;
	case "GLOSSARY":
		$object = new SDBP6_GLOSSARY();
		break;
	case "HEADINGS":
		$object = new SDBP6_HEADINGS();
		break;
	case "PROFILE":
		$object = new SDBP6_PROFILE();
		break;
	case "SETUP_ADMINS":
	case "SETUP_ADMIN":
		$object = new SDBP6_SETUP_ADMINS();
		break;
	case "SETUP_ORGSTRUCTURE":
		$object = new SDBP6_SETUP_ORGSTRUCTURE();
		break;
	case "SETUPPREFERENCES":
	case "SETUP_PREFERENCES":
		$object = new SDBP6_SETUP_PREFERENCES();
		break;
	case "SETUP_RESULTS":
		$object = new SDBP6_SETUP_RESULTS();
		break;
	case "SETUP_CALCTYPE":
	case "CALCTYPE":
		$object = new SDBP6_SETUP_CALCTYPE();
		break;
	case "SETUP_TARGETTYPE":
	case "TARGETTYPE":
		$object = new SDBP6_SETUP_TARGETTYPE();
		break;
	case "SETUP_TIME":
	case "TIME":
		$object = new SDBP6_SETUP_TIME();
		break;
	/**
	 * Module objects
	 */
	case "SDBIP":
		$object = new SDBP6_SDBIP();
		break;
	case "PROJECT":
	case "PROJECTS":
		$object = new SDBP6_PROJECT();
		break;
	case "TOPKPI":
	case "TOP":
		$object = new SDBP6_TOPKPI();
		break;
	case "DEPTKPI":
	case "DEPT":
		$object = new SDBP6_DEPTKPI();
		break;
	case "FINANCE":
		$object = new SDBP6_FINANCE();
		break;
}
$result[2] = ":".implode(".", $my_action).":";
$result[3] = array($class, $activity);


if(isset($object)) {
	switch($activity) {
		case "DELETE_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id, $i, $page_activity);
			if($attach !== false) {
				$folder = $object->getParentFolderPath();
				$old = $folder."/".$attach['location']."/".$attach['system_filename'];
				$dest_folder = $object->getDeletedFolder();
				$object->checkFolder($dest_folder);
				$dest_folder = $object->getParentFolderPath()."/".$dest_folder;
				if(file_exists($old)) {
					$result = $object->deleteAttachment($object_id, $i, $page_activity);
					if($result[0] == "ok") {
						$new_path = $dest_folder."/".$object->getDeletedFileName($class, $attach['system_filename']);
						if(copy($old, $new_path)) {
							unlink($old);
						}
					} else {
						//result from object->deleteAttachment
					}
				} else {
					$result = array("error", "File could not be found.  Please reload the page and try again.", $old);
				}
			} else {
				$result = array("error", "An error occurred while trying to find the file you want to delete.  Please reload the page and try again", $_REQUEST);
			}
			break;
		case "GETLISTTABLEHTML":
			unset($_REQUEST['options']['set']);
			$button = $_REQUEST['button'];
			$data = $object->getObject($_REQUEST['options']);
			$displayObj = new SDBP6_DISPLAY();
			$display = $displayObj->getListTableRows($data, $button);
			$result = array('display' => $display['display'], 'paging' => $data['paging'], 'spage' => serialize($data['paging']));
			break;
		case "GET":
			$result = $object->getObject($_REQUEST);
			break;
		case "ADD":
		case "CREATE":
			$data = $object->addObject($_REQUEST);
			if($data[0] == "ok") {
				if($has_attachments) {
					$attach = processAttachments($activity, $object, $data);
				}
				$result = $data;
				/* all activity logs handled by class - JC [8 July 2018] */
				//if(!in_array($class,$activity_log_handled_by_class) && (!isset($data['add_log']) || $data['add_log']===true)) {
				//	$object->addActivityLog(strtolower($object->getMyLogTable()),$data['log_var']);
				//}
			} else {
				$result = $data;
			}
			break;
		case "SIMPLEADD":
			$result = $object->addObject($_REQUEST);
			break;
		case "SIMPLEEDIT":
			$result = $object->editObject($_REQUEST, $attach);
			break;
		case "EDIT":
			if($has_attachments) {
				$res = processAttachments($activity, $object, array('object_id' => $_REQUEST['object_id']));
				$attach = $res['attachments'];
			}
			$result = $object->editObject($_REQUEST, $attach);
			break;
		case "UPDATE":
			if($has_attachments) {
				$res = "has_attachments";
				$options = array('object_id' => $_REQUEST['object_id'], 'object_type', $object->getMyObjectType(), 'time_id' => $_REQUEST['time_id']);
				$res = processAttachments($activity, $object, $options);
//				$result = array("error",serialize($res));
			} else {
//				$res = "";
//$res = array('attachments'=>"");
			}

			if(isset($res) && $res === false) { //Added AA-361 - just a bit of cleaner code
				$result = array("error", "An error occurred while trying to upload your file.  Please reload the page and try again.");
			} else {
				$attach = isset($res['attachments']) ? $res['attachments'] : "";
				$result = $object->updateObject($_REQUEST, $attach);
			}
			break;
		case "UPLOADFILESTOPENDINGFOLDER":
			if($has_attachments && isset($_FILES['attachments'])) {
				/** @var SDBP6_FINANCE $object */
				$result = $object->uploadFilesToCompanyPendingFolder($_FILES, $_REQUEST);
			}
			break;
		case "APPROVE":
			$result = $object->approveObject($_REQUEST);
			//$result = array("info","found approve :: ".serialize($_REQUEST));
			break;
		case "ASSURANCE":
			$result = $object->assuranceObject($_REQUEST);
			break;
		case "DECLINE":
			$result = $object->declineObject($_REQUEST);
			break;
		case "UNLOCK_APPROVE":
			$result = $object->unlockApprovedObject($_REQUEST);
			break;
		case "CONFIRM":
			$result = $object->confirmObject($_REQUEST);
			break;
		case "UNDOCONFIRM":
			$result = $object->undoConfirmObject($_REQUEST);
			break;
		case "ACTIVATE":
			$result = $object->activateObject($_REQUEST);
			break;
		case "UNDOACTIVATE":
			$result = $object->undoActivateObject($_REQUEST);
			break;
		case "UNLOCK":
			$result = $object->unlockObject($_REQUEST);
			break;
		case "DELETE":
			$result = $object->deleteObject($_REQUEST);
			break;
		case "DEACTIVATE":
			$result = $object->deactivateObject($_REQUEST);
			break;
		case "RESET":
			$result = $object->resetObject($_REQUEST);
			break;
		case "RESTORE":
			$result = $object->restoreObject($_REQUEST);
			break;
		case "SORT":
		case "ORDER":
			$result = $object->sortObject($_REQUEST);
			break;
		case "RECIPIENTS":
			$result = $object->getObjectRecipients($_REQUEST);
			break;
		case "GETPROJECTDETAILCHILDROW":
			$tier = $_REQUEST['tier'];
			$object_type = $_REQUEST['object_type'];
			$project_id = $_REQUEST['project_id'];
			$result = $object->getProjectDetailChildRow($tier, $object_type, $project_id, $_REQUEST);
			break;
		default:
			$result = call_user_func(array($object, $activity), $_REQUEST);
			break;
	}
} else {
	$result[4] = "no object set";
}
if($has_attachments == false) {
	echo json_encode($result);
} else {
	if($page_direct == "dialog" && $result[0] == "ok") {
		echo "<script type=text/javascript>window.parent.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
	} else {
//			$page_direct.="r[]=".$result[0]."&r[]=".$result[1];
		echo "<script type=text/javascript>
			//alert('".ASSIST_HELPER::code($result[1])."');
			//console.log('".ASSIST_HELPER::code(serialize($result))."');
			window.parent.saveFinished('".$result[0]."','".ASSIST_HELPER::code($result[1])."');
			</script>";
//			echo "<script type=text/javascript>window.parent.location.href = '".$page_direct."';</script>";
	}
}


/*************** Attachment processing ******************/
function processAttachments($activity, $object, $data) {
	$object_id = $data['object_id'];
	$object_type = $object->getMyObjectType();
	$time_id = $data['time_id'];
	$attachment_ref = $object_id."-".$time_id;
//	$data['ref'] = $attachment_ref;
	$default_system_filename = $object_type.$object_id."_".($activity == "UPDATE" ? "UPDATE".$time_id."_" : "").date("YmdHis")."_";
	$result = array();
	$result[] = $default_system_filename;
	$original_filename = "";
	$system_filename = "";
	if(isset($_FILES) && isset($_FILES['attachments']) && count($_FILES['attachments']) > 0) {
		$result[] = "files are set";
		$save_folder = $object->getStorageFolder();
		$object->checkFolder($save_folder);
		$result[] = $save_folder." folder check complete";
		$folder = $object->getFullFolderPath();
		$result[] = $folder;
		//Check for existing attachments
		switch($activity) {
			case "ADD":
				$i = 0;
				$current_attach = array();
				break;
			case "UPDATE":
			case "EDIT":
				$current_attach = array();
				$current_attach = $object->getAttachmentDetails($attachment_ref, "all", $activity);
				if($current_attach === false) {
					return false;
				} else {
					if(count($current_attach) > 0) {
						$keys = array_keys($current_attach);
						$i = max($keys);
						$i++;
					} else {
						$i = 0;
					}
				}
				break;
		}
		$new_attach = array();
		$result[] = $i;
		foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
			if(strlen($tmp_name) > 0) {
				$x = array('id' => $key, 'tmp_name' => $tmp_name);
				$x['error'] = $_FILES['attachments']['error'][$key];
				if($_FILES['attachments']['error'][$key] == 0) {
					//trim all spaces from front & back and then replace any spaces in filenames with _
					$original_filename = str_replace(" ", "_", trim($_FILES['attachments']['name'][$key]));
					//remove all illegal characters from file name -> ALLOWED CHAR: A-Z(case insensitive) 0-9 . - _ ( ) [No spaces allowed]
					$original_filename = preg_replace("/[^a-zA-Z0-9.\-]/", "", $original_filename);
					$parts = explode(".", $original_filename);
					if(count($parts) > 1) {
						$ext = ".".$parts[count($parts) - 1];
						unset($parts[count($parts) - 1]);
					} else {
						$ext = "";
					}
					$file = implode(".", $parts);
					$system_filename = $default_system_filename.$i.$ext;
					$x['sys'] = $system_filename;
					$full_path = $folder."/".$system_filename;
					$x['path'] = $full_path;
					$new_attach[$i] = array('status' => SDBP6::ACTIVE, 'location' => $save_folder, 'system_filename' => $system_filename, 'original_filename' => $original_filename, 'ext' => substr($ext, 1, strlen($ext)));
					$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
					$result[] = $x;
					$i++;
				}
			}
		}
		$result['attachments'] = array('new' => $new_attach, 'save' => array_merge($current_attach, $new_attach));
		//$object->saveAttachments($activity,$object_id,array_merge($current_attach,$new_attach));
	}
	return $result;
}


function getSaveFolder($object) {
	return $object->getStorageFolder();
}

function getFullFolderPath($object) {
	$folder = $object->getFullFolderPath();
	return $folder;
}


function getDeletedFolder($object) {
	$folder = $object->getDeletedFolder();
}


function getParentFolderPath($object) {
	$folder = $object->getParentFolderPath();
	return $folder;
}

?>