<?php

if(!isset($_REQUEST['my_sdbip_id'])) {

	$url = "manage_time.php?my_sdbip_id=";
	$include_status = false;
	$include_colour = false;
	$sdbip_status_filter = "ACTIVATED";

	require_once "common/sdbip_chooser.php";

} else {

	if(isset($_REQUEST['my_sdbip_id'])) {
		$_REQUEST['sdbip_id'] = $_REQUEST['my_sdbip_id'];
	}
	$src_url = "manage_time.php";
	$page_section = "MANAGE";
	include("common/time.php");

}
?>