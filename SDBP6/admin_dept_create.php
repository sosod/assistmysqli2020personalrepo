<?php
require_once("inc_header.php");


$object_id = 0;//$_REQUEST['object_id']; no object_id available because we're about to create!
$myObject = new SDBP6_DEPTKPI();

$page_section = "ADMIN";
$page_action = "CREATE";

$page_redirect_path = "admin_dept_create.php?";

//don't set filter_by because this is admin section where user has access to everything.

include("common/form_object.php");


?>
<?php markTime("end of page"); ?>