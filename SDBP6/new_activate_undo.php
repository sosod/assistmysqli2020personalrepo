<?php
/**
 * @var array $sdbip_details - array of sdbip currently going through NEW process - from inc_new_statuscheck
 * @var SDBP6_SDBIP $sdbipObject - from inc_header
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var string $js - placeholder for all Javascript/jquery - from inc_header
 * @var string $sdbip_object_name - from inc_header
 */
if(!isset($_REQUEST['okay_to_undo_activation'])) {

	require_once("inc_header.php");
	$is_new_sdbip_in_progress = $sdbipObject->hasSDBIPBeenCreatedAndNotYetActivated();

	if($is_new_sdbip_in_progress) {
		ASSIST_HELPER::displayResult(array("error", "Reversing activation is not possible while a new ".$sdbip_object_name." is in progress."));
		die();
	} else {
		$_REQUEST['okay_to_undo_activation'] = true;
		$header_already_included = true;
	}

}


$_REQUEST['step'] = "activate";
//$is_reset_page = true;
$url = "new_activate_undo_step2.php?my_sdbip_id=";
$include_status = false;
$include_colour = false;
$sdbip_status_filter = "ACTIVATED";


require_once "common/sdbip_chooser.php";

die();
?>