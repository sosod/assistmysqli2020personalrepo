<?php
require_once("inc_header.php");

/*****
 * inc_new_statuscheck will validate that an existing SDBIP has not yet been activated and whether the start process has been completed
 * NOTE: ALL NEW_* PAGES MUST CALL THIS PAGE EXCEPT new_activate_undo AND new_create_reset
 * available variables:
 * @var SDBP6_SDBIP $sdbipObject = new SDBP6_SDBIP();
 * @var string $sdbip_object_name = object_name setting for SDBIP object
 * @var string $mscoa_object_name = object_name setting for mSCOA
 * @var string $step = "start" || url of next step if process has been completed
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var BOOL $is_sdbip_activated - from inc_new_statuscheck
 */
$is_create_step1_page = true;
require_once("inc_new_statuscheck.php");


if($step == "start") {
	//variable setup
	$table_class = "ok";
	$content = "";
	$content = "<h1>".$sdbipObject->getObjectName("SDBIP")." ".$sdbipObject->getActivityName("CREATE")." Process</h1>";
	$financial_years_object_name = $sdbipObject->getObjectName("financialyears");
	$financial_year_object_name = $sdbipObject->getObjectName("financialyear");
	$idp_object_name = $sdbipObject->getObjectName("IDP");
	$projects_object_name = $sdbipObject->getObjectName("PROJECTS");
	$kpis_object_name = $sdbipObject->getObjectName("KPIS");
	//what steps are needed during the creation process
	$steps = array(
		'choose_fin_year' => false,
		//'choose_idp_module'=>false,
		'link_to_idp_module' => false,
		'link_to_idp' => false,
		'link_to_sdbp5b_module' => false,
		'link_to_sdbp6_sdbip' => false,
		'mscoa_compliant' => true,
		'mscoa_version' => true,
		//'choose_sdbp5b_module'=>false,
//		'sdbp5b_convert'=>false,
		//'no_activated_idps'=>false,
		'name' => true,
		'setup' => true,
	);
	//where to go after each step - DEFAULT OPTIONS - can be adjusted by system depending on whether IDPs or SDBIPs are found
	$next_steps = array(
		'choose_fin_year' => "mscoa_compliant",
		'link_to_idp_module' => array(1 => "link_to_idp", 0 => "link_to_sdbp5b_module"),
		//'choose_idp_module'=>"link_to_idp",
		'link_to_idp' => "name",
		'link_to_sdbp5b_module' => "name",
		'link_to_sdbp6_sdbip' => "mscoa_compliant",
		'mscoa_compliant' => "mscoa_version",
		'mscoa_version' => "name",
		//'choose_sdbp5b_module'=>"sdbp5b_convert",
		'sdbp5b_convert' => "name",
		//'no_activated_idps'=>array(1=>false,0=>"link_to_sdbp5b_module"),
		'name' => "setup",
		'setup' => true,
	);
	$no_idp_warning = "Warning: Please note that if you choose not to link this $sdbip_object_name to a(n) $idp_object_name or prior $sdbip_object_name module, all $projects_object_name & $kpis_object_name will have to be created manually and cannot be linked to past or future $projects_object_name or $kpis_object_name for year-on-year performance analysis within Assist.";
	$sdbp5b_convert_warning = "Warning: Please note that, if you continue, the $sdbip_object_name module you have chosen to convert will be closed and users will no longer be able to access it.  This is done to prevent data loss and corruption due to users accessing the old module while you are in the process of converting it.";
	$steps_text = array(
		'choose_fin_year' => "Please select the relevant $financial_year_object_name to which this $sdbip_object_name belongs: ",
		'link_to_idp_module' => "Do you wish to link this $sdbip_object_name to an $idp_object_name module? ",
		//'choose_idp_module'=>"Please select which $idp_object_name module you wish to link to: ",
		'link_to_idp' => "Please select which $idp_object_name you wish to link this $sdbip_object_name to:",
		//'no_activated_idps'=>"$idp_object_name modules exist to be linked to, however no activated $idp_object_name is available to be selected.  If you wish to link to an $idp_object_name then please ensure that the $idp_object_name module has an activated $idp_object_name before continuing here.  Otherwise you can continue here by skipping the linking process.  </p><p>".str_replace("Warning:","<span class=b>Warning:</span>",$no_idp_warning),
		'link_to_sdbp5b_module' => "Do you wish to import this $sdbip_object_name from an alternative $sdbip_object_name module? ",
		'link_to_sdbp6_sdbip' => "Do you wish to import this $sdbip_object_name from an existing $sdbip_object_name?<br /><span class='i'>This will include the SDBIP-specific Setup.</span>",
		'mscoa_compliant' => "Is this $sdbip_object_name $mscoa_object_name compliant?",
		'mscoa_version' => "Which $mscoa_object_name version is applicable to this ".$sdbip_object_name."?",
		//'choose_sdbp5b_module'=>"Please select which $sdbip_object_name module you wish to link to: ",
//		'sdbp5b_convert'=>"Do you want to convert from the older module version to this newer version (i.e. import all objects, updates and logs and remove the older module) or only import the objects and start updating from scratch?",
		'name' => "Please provide a name for the new ".$sdbip_object_name.": ",
		'setup' => "Copy ".$sdbip_object_name." specific setup from the most recently activated ".$sdbip_object_name."?",
	);
	if($is_sdbip_activated == true) {
		$allow_link_to_sdbp5b = false;
		$allow_link_to_sdbp6 = true;
	} else {
		//$allow_link_to_sdbp5b = true;
		$allow_link_to_sdbp5b = false;
		$allow_link_to_sdbp6 = false;
		//no activated SDBIP then leave Setup blank & don't ask user
		$next_steps['name'] = true;
	}
	//SYSTEM CHECK: Have Financial Years been created in MASTER SETUP [S] > Financial Years - Don't allow duplicates
	$financial_years = $sdbipObject->getAvailableFinancialYears();
	//mark choose_fin_year as available regardless of anything else and it can be called on from various places depending on user responses
	$steps['choose_fin_year'] = true;
	//If no financial years then stop process
	if(count($financial_years) == 0) {
		$table_class = "error";
		$starting_step = 0;
		$content = "<h1>Error</h1><p>No $financial_years_object_name are available.  $financial_years_object_name are required before the $sdbip_object_name can be created.</p><p>Please contact your Assist Administrator and ask them to update the Master Setups > Financial Years section before you can continue here.</p>";
	} else {    //else to if count(financial years)>0
		//SYSTEM CHECK: Is IDP module available? And if so, get the list of modules
		$are_idp_modules_available = $sdbipObject->doesLinkedIDPModuleExistOnDatabase();
		if($are_idp_modules_available === true) {
			$available_idp_modules = $sdbipObject->getAvailableIDPModules(true);
			//SYSTEM CHECK: Get list of available IDPs from the IDP modules
			if(count($available_idp_modules) > 0) {
				//foreach idp module get the list of activated idps
				$activated_idps = array();
				foreach($available_idp_modules as $idp_modref => $module_name) {
					//get list of activate idps
					$my_idps = $sdbipObject->getActivatedIDPsFromModule($idp_modref, 0, true);
					//if no activated idps exist then remove that idp from the list of available modules
					if(count($my_idps) > 0) {
						$activated_idps[$idp_modref] = $my_idps;
					} else {
						unset($available_idp_modules[$idp_modref]);
					}
				}
				//if one or more idp modules have activated idps then include the option to link to an idp module
				if(count($available_idp_modules) > 0) {
					//redirect process here after choosing fin_year
					$next_steps['choose_fin_year'] = "link_to_idp_module";
					//activate IDP steps
					$steps['link_to_idp_module'] = true;
					$steps['link_to_idp'] = true;
				} else {    //else to if 1 or more idp modules have activated idps
					//do nothing - default options kick in
				}            //endif 1 or more idp modules have activated idps
			} else {
				//do nothing - default options kick in
			}
		} else {
			$available_idp_modules = array();
			//do nothing more - default options kick in
		}
		//SYSTEM CHECK2: Are any SDBP5B modules available? And if so, get the list of modules
		$are_sdbp5b_modules_available = false;//NO LONGER ALLOWED $sdbipObject->doesLinkedSDBP5BModuleExistOnDatabase();
		if($are_sdbp5b_modules_available === true && $allow_link_to_sdbp5b === true) {
			$available_sdbp5b_modules = $sdbipObject->getAvailableSDBP5BModules(true);
			if(count($available_sdbp5b_modules) > 0) {
				//activate sdbip steps
				$steps['link_to_sdbp5b_module'] = true;
//				$steps['sdbp5b_convert'] = false;//true; - conversion functionality dead [JC 2019-03-21]
				//if choose_fin_year is still set to default, redirect here
				if($next_steps['choose_fin_year'] == "name" || $next_steps['choose_fin_year'] == "mscoa_compliant") {
					$next_steps['choose_fin_year'] = "link_to_sdbp5b_module";
				}
			}
		} else {
			$available_sdbp5b_modules = array();
			$next_steps['link_to_idp_module'][0] = "link_to_sdbp6_sdbip";    //if no sdbp5b then redirect to sdbp6
			//do nothing more - default options kick in
		}
		//SYSTEM CHECK3: Are any SDBP6 SDBIPs available? And if so, get the list of sdbips
		if($is_sdbip_activated === true && $allow_link_to_sdbp5b !== true && $allow_link_to_sdbp6 === true) {
			$available_sdbp6_sdbips = $sdbipObject->getAvailableSDBP6Objects(true);
			if(count($available_sdbp6_sdbips) > 0) {
				//activate sdbip steps
				$steps['link_to_sdbp6_sdbip'] = true;
				//if choose_fin_year is still set to default, redirect here
				if($next_steps['choose_fin_year'] == "name" || $next_steps['choose_fin_year'] == "mscoa_compliant") {
					$next_steps['choose_fin_year'] = "link_to_sdbp6_sdbip";
				}
			}
		} else {
			//do nothing more - default options kick in
			$available_sdbp6_sdbips = array();
			$next_steps['link_to_idp_module'][0] = "mscoa_compliant";
		}

	}

	//ASSIST_HELPER::arrPrint($steps);
	//ASSIST_HELPER::arrPrint($next_steps);
	//ASSIST_HELPER::arrPrint($steps_text);
	//ASSIST_HELPER::arrPrint($available_idp_modules);
	//ASSIST_HELPER::arrPrint($available_sdbp5b_modules);


	if($table_class != "error") {
		foreach($steps as $key => $s) {
			if($s == true) {
				$content .= "




				<div id=step_".$key.">
					<p>".$steps_text[$key];
				switch($key) {
					case "setup":
						$options = array('1' => "Yes, copy SDBIP Setup", '0' => "No, I'll do SDBIP Setup manually");
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false), 1);
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "name":
						$display = $displayObject->createFormField("MEDVC", array('id' => $key, 'name' => $key, 'required' => true));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "link_to_idp_module":
						$options = array("FALSE_LINK" => "No - Don't link");
						foreach($available_idp_modules as $mod_ref => $mod_name) {
							$options[$mod_ref] = "Link to: ".$mod_name;
						}
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "link_to_idp":
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'required' => true, 'unspecified' => false, 'options' => array(0 => "Don't link.  The ".$idp_object_name." I want is not in this list.")));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						$js .= "
						var idp_list = new Array();
						";
						foreach($available_idp_modules as $modref => $mod_name) {
							if(count($activated_idps[$modref]) > 0) {
								$js .= "
								idp_list['$modref'] = new Array();";
								foreach($activated_idps[$modref] as $idp_id => $idp) {
									if(isset($financial_years[$idp['financial_year']])) {
										$js .= "
										idp_list['$modref'][$idp_id] = new Array('".$idp['name']." [Financial Year: ".$financial_years[$idp['financial_year']]."]',".$idp['financial_year'].",'".$idp['mscoa_version_code']."');
										";
									}
								}
							}
						}
						break;
					case "link_to_sdbp5b_module":
						$options = array("FALSE_LINK" => "No - Don't link");
						foreach($available_sdbp5b_modules as $mod_ref => $mod_name) {
							$options[$mod_ref] = "Import from: ".$mod_name;
						}
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
//						break;
//					case "sdbp5b_convert":
//						$display = $displayObject->createFormField("LIST",array('id'=>$key,'name'=>$key,'required'=>true,'unspecified'=>false,'options'=>array('CONVERT'=>"Convert everything and close the old module.",'IMPORT'=>"Import objects only, I want to start a new year.")));
//						$js.=$display['js'];
//						$content.="</p><p class=center>".$display['display']."</p>";
						/*$js.="
						var idp_list = new Array();
						";
						foreach($available_idp_modules as $modref => $mod_name) {
							if(count($activated_idps[$modref])>0) {
								$js.="
								idp_list['$modref'] = new Array();";
								foreach($activated_idps[$modref] as $idp_id => $idp) {
									$js.="
									idp_list['$modref'][$idp_id] = new Array('".$idp['name']." [Financial Year: ".$financial_years[$idp['financial_year']]."]',".$idp['financial_year'].");
									";
								}
							}
						}*/
						break;
					case "link_to_sdbp6_sdbip":
						$options = array("FALSE_LINK" => "No - Don't link");
						foreach($available_sdbp6_sdbips as $id => $name) {
							$options[$id] = "Import from: ".$name;
						}
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "mscoa_compliant":
						$options = array(0 => "No", 1 => "Yes");
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false), 1);
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "mscoa_version":
						$options = array();
						$listObject = new SDBP6_SEGMENTS("version", true);
						$options = $listObject->getListItemsFromMSCOAFormattedForSelect();
						$most_recent = $listObject->getMostRecentVersion(true);
						unset($listObject);
						if(isset($options[$most_recent])) {
							$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false), $most_recent);
						} else {
							$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'options' => $options, 'required' => true, 'unspecified' => false));
						}
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					case "choose_fin_year":
						$options = $financial_years;
						$display = $displayObject->createFormField("LIST", array('id' => $key, 'name' => $key, 'required' => true, 'unspecified' => false, 'options' => $options));
						$js .= $display['js'];
						$content .= "</p><p class=center>".$display['display']."</p>";
						break;
					/*					case "no_activated_idps":
											$display = $displayObject->createFormField("BOOL",array('id'=>$key,'name'=>$key,'extra_act'=>"fixButtonColours"),0);
											$js.=$display['js']."
											$(\"button.btn_yes\").removeClass('ui-state-default').addClass('ui-button-bold-grey');
											$(\"button.btn_no\").removeClass('ui-state-default').addClass('ui-button-bold-grey');
											$(\"button.btn_no\").trigger('click');
											function fixButtonColours($"."me,act) {
												if(act=='yes') {
													$('#no_activated_idps_yes').removeClass('ui-button-bold-grey');
													$('#no_activated_idps_no').addClass('ui-button-bold-grey');
												} else {
													$('#no_activated_idps_no').removeClass('ui-button-bold-grey').addClass('ui-button-bold-red');
													$('#no_activated_idps_yes').addClass('ui-button-bold-grey');
												}
											}
											";
											$content.="</p>
											<div style='width:650px;padding-bottom: 10px;margin:0 auto;border:1px solid #999999; background-color:#FFFFFF; border-radius:5px;'>
											<p class='b center'>Do you wish to skip the linking process for this ".$sdbip_object_name."?</p>
											<div class=center>".$display['display']."</div>
											</div>
											";
											break;*/
					default:
						$content .= "Oops, no step action available.";
				}
				$content .= "
				</p></div>";
				$js .= "
				$('#step_".$key."').hide();
				";
			}
		}
		if($steps['choose_fin_year']) {
			$key = "choose_fin_year";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
		} elseif($steps['link_to_idp_module']) {
			$key = "link_to_idp_module";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
			/*}elseif($steps['no_activated_idps']) {
				$key = "no_activated_idps";
				$starting_step = $key;
				$js.="$('#step_".$key."').show()";*/
		} elseif($steps['link_to_idp']) {
			$key = "link_to_idp";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
		} elseif($steps['link_to_sdbp5b_module']) {
			$key = "link_to_sdbp5b_module";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
		} elseif($steps['link_to_sdbp6_sdbip']) {
			$key = "link_to_sdbp6_sdbip";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
		} elseif($steps['mscoa_compliant']) {
			$key = "mscoa_compliant";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
		} elseif($steps['mscoa_version']) {
			$key = "mscoa_version";
			$starting_step = $key;
			$js .= "$('#step_".$key."').show()";
//		}elseif($steps['sdbp5b_convert']) {
//			$key = "sdbp5b_convert";
//			$starting_step = $key;
//			$js.="$('#step_".$key."').show()";
		}
		$content .= "<p class=center><button id=btn_back>Start Over</button>&nbsp;<button id=btn_next>Next</button></p>";
		$js .= "
		$('#btn_back').button({
			icons:{ primary: \"ui-icon-arrowthick-1-w\" }
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = document.location.href;
		}).removeClass('ui-state-default').addClass('ui-button-bold-grey').css('background','#ffffff').children(\".ui-button-text\").css({\"font-size\":\"75%\",\"padding-top\":\"3px\",\"padding-bottom\":\"3px\"})
		$('#btn_back').hide();


		$('#btn_next').button({
			icons:{ secondary: \"ui-icon-arrowthick-1-e\" }
		}).click(function(e) {
			e.preventDefault();
			var step = $('#step').val();
			switch(step) {
				case 'setup':
					var answer = $('#setup').val();
					$('#copy_setup').val(answer);
					submitForm();
					err = true;
					break;
				case 'name':
					var answer = $('#name').val();
					var temp_next_step = '".($next_steps['name'] === true ? "submit" : $next_steps['name'])."';
					if(answer.length==0) {
						alert('Please enter a name for this new ".$sdbip_object_name.".');
						err = true;
					} else {
						$('#sdbip_name').val(answer);
						//if copy SDBIP setup was not allowed (no activated exists) OR import from existing SDBIP was chosen then skip copy_setup question and submit
						if(temp_next_step=='submit' || $('#copy_setup').val()==1) {
							submitForm();
							err = true;
						} else {
							var next_step = temp_next_step;
						}
					}
					break;
				/* case 'no_activated_idps':
					var answer = $('#no_activated_idps').val();
					if(parseInt(answer)==0) {
						//alert('Please come back once you\'ve activated an IDP');
						confirmSubmission('Please come back and continue once you\'ve activated a(n) ".$idp_object_name.".','ok','');
						err = true;
					} else {
						var next_step = 'choose_fin_year';
					}
					break; */
				case 'choose_fin_year':
					var fin_year = $('#choose_fin_year').val();
					var fin_year_name = $('#choose_fin_year option:selected').text();
					var err = false;
					if(fin_year=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#fin_year_id').val(fin_year);
						var next_step = '".$next_steps['choose_fin_year']."';
						//IF next step is IDP step then verify that IDPs for the financial year exist
						var found_idp = false;
						if(next_step=='link_to_idp_module') {
							for(m in idp_list) {
								for(i in idp_list[m]) {
									if(idp_list[m][i][1]==fin_year) {
										found_idp = true;
										module_found = true;
									}
								}
							}
							//if no idps were found - move on
							if(!found_idp) {
								next_step = '".$next_steps['link_to_idp_module'][0]."';
							}
						}
						confirmSubmission('Please confirm that this $sdbip_object_name is applicable to the \''+fin_year_name+'\' $financial_year_object_name.',next_step,'')
						err = true;
					}
					break;
				case 'link_to_idp':
					var idp_id = $('#link_to_idp').val();
					var idp_module = $('#idp_module').val();
					var err = false;
					if(idp_id=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#idp_id').val(idp_id);
						if(parseInt(idp_id)==0) {
							var next_step = '".$next_steps['link_to_idp_module'][0]."';
							confirmSubmission('Please confirm that you are choosing not to link this ".$sdbip_object_name." to a(n) ".$idp_object_name.".</p><p class=i>".str_replace("Warning:", "<span class=b>Warning:</span>", $no_idp_warning)."',next_step,step);
							err = true;
						} else {
							//$('#fin_year_id').val(idp_list[idp_module][idp_id][1]);
							var next_step = '".$next_steps['link_to_idp']."';
							var mscoa_code = idp_list[$('#idp_module').val()][idp_id][2];
							$('#mscoa_version_code').val(mscoa_code);
							if(mscoa_code=='NA') {
								$('#mscoa_yesno').val(0);
							} else {
								$('#mscoa_yesno').val(1);
							}
							confirmSubmission('Please confirm that you are choosing to link this ".$sdbip_object_name." to ".$idp_object_name." \''+idp_list[idp_module][i][0]+'\'.',next_step,step);
							err = true;
						}
					}
					break;
				case 'link_to_idp_module':
					var idp_module = $('#link_to_idp_module').val();
					var err = false;
					if(idp_module=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#idp_module').val(idp_module);
						if(idp_module=='FALSE_LINK') {
							var next_step = '".$next_steps['link_to_idp_module'][0]."';
							confirmSubmission('Please confirm that you are choosing not to link this ".$sdbip_object_name." to a(n) ".$idp_object_name." module.</p><p class=i>".str_replace("Warning:", "<span class=b>Warning:</span>", $no_idp_warning)."',next_step,step);
							err = true;
						} else {
							var next_step = '".$next_steps['link_to_idp_module'][1]."';
							$('#link_to_idp option:gt(1)').remove();
							var fin_year = $('#fin_year_id').val();
							for(i in idp_list[idp_module]) {
								if(idp_list[idp_module][i][1]==fin_year) {
									$('#link_to_idp').append('<option value='+i+'>'+idp_list[idp_module][i][0]+'</option>');
								}
							}
							$('#link_to_idp').val('X');
						}
					}
					break;
				case 'link_to_sdbp5b_module':
					var sdbp5b_module = $('#link_to_sdbp5b_module').val();
					var err = false;
					if(sdbp5b_module=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#sdbp5b_module').val(sdbp5b_module);
						var next_step = '".$next_steps['link_to_sdbp5b_module']."';
						if(sdbp5b_module=='FALSE_LINK') {
							confirmSubmission('Please confirm that you are choosing not to link this ".$sdbip_object_name." to a prior ".$sdbip_object_name." module.</p>',next_step,step);
							err = true;
						} else {
							confirmSubmission('Please confirm that you are choosing to link this ".$sdbip_object_name." to \''+$('#link_to_sdbp5b_module option:selected').text().substring(13)+'\'.',next_step,step);
							err = true;
						}
					}
					break;
//				case 'sdbp5b_convert':
//					var sdbp5b_convert = $('#sdbp5b_convert').val(); //alert(sdbp5b_convert);
//					var err = false;
//					if(sdbp5b_convert=='X') {
//						alert('Please select an option from the drop down list.');
//						err = true;
//					} else {
//						$('#sdbp5b_action').val(sdbp5b_convert);
//						var next_step = '".$next_steps['sdbp5b_convert']."';
//						if(sdbp5b_convert=='CONVERT') {
//							confirmSubmission('Please confirm that you are choosing to CONVERT from the prior SDBIP to the current SDBIP</p><p class=i>".str_replace("Warning:", "<span class=b>Warning:</span>", $sdbp5b_convert_warning)."</p>',next_step,step);
//							err = true;
//						} else {
//							confirmSubmission('Please confirm that you are choosing to IMPORT from a prior SDBIP to the current SDBIP</p><p class=i>This will not impact the status of the prior $sdbip_object_name module as no updates or logs will be imported.</p>',next_step,step);
//							err = true;
//						}
//					}
//					break;
				case 'link_to_sdbp6_sdbip':
					var sdbp6_module = $('#link_to_sdbp6_sdbip').val();
					var err = false;
					if(sdbp6_module=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#sdbp6_sdbip').val(sdbp6_module);
						var next_step = '".$next_steps['link_to_sdbp6_sdbip']."';
						if(sdbp6_module=='FALSE_LINK') {
							$('#copy_setup').val(0);
							confirmSubmission('Please confirm that you are choosing not to link this ".$sdbip_object_name." to an existing ".$sdbip_object_name.".</p>',next_step,step);
							err = true;
						} else {
							$('#copy_setup').val(1);
							confirmSubmission('Please confirm that you are choosing to link this ".$sdbip_object_name." to \''+$('#link_to_sdbp6_sdbip option:selected').text().substring(13)+'\'.',next_step,step);
							err = true;
						}
					}
					break;
				case 'mscoa_compliant':
					var mscoa_compliant = $('#mscoa_compliant').val();
					var err = false;
					if(mscoa_compliant=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#mscoa_yesno').val(mscoa_compliant);
						err = true;
						if(mscoa_compliant*1==0) {
							var next_step = 'name';
							$('#mscoa_version_code').val('NA');
							confirmSubmission('Please confirm that this ".$sdbip_object_name." is NOT $mscoa_object_name compliant and will not be linked to a specific $mscoa_object_name version as provided by National Treasury.',next_step,step);
						} else {
							var next_step = '".$next_steps['mscoa_compliant']."';
							confirmSubmission('Please confirm that this ".$sdbip_object_name." is $mscoa_object_name compliant and will be linked to a specific $mscoa_object_name version as provided by National Treasury in the next step.',next_step,step);
						}
					}
					break;
				case 'mscoa_version':
					var mscoa_version = $('#mscoa_version').val();
					var err = false;
					if(mscoa_version=='X') {
						alert('Please select an option from the drop down list.');
						err = true;
					} else {
						$('#mscoa_version_code').val(mscoa_version);
						err = true;
						var next_step = '".$next_steps['mscoa_version']."';
						confirmSubmission('Please confirm that this ".$sdbip_object_name." is linked to $mscoa_object_name version \''+$('#mscoa_version option:selected').text()+'\'.',next_step,step);
					}
					break;

			}
			if(!err) {
				nextStep(next_step,$('#step').val());
			}
		}).removeClass('ui-state-default').addClass('ui-button-bold-green').css('background','#ffffff');

		function nextStep(next_step,prev_step) {//alert(next_step);
				$('#step_history').val($('#step_history').val()+'|'+prev_step);
				$('#step').val(next_step);
				$('#step_'+prev_step).hide(200);
				$('#step_'+next_step).show(200);
				$('#btn_back').show();
				console.log('---------------'); 
				console.log($('#step').val());
				console.log($('#step_history').val());
				console.log(prev_step);
				console.log(next_step);
		}

		function submitForm() {
			AssistHelper.processing();
			$('#step_history').val($('#step_history').val()+'|'+$('#step').val()+'|submitForm');
			var dta = AssistForm.serialize($('form[name=frm_step1]'));	//THIS WILL ENCODE STRINGS - NO NEED TO ENCODE ON DB INSERT
			//console.log(dta);
			//result = ['info',dta];
			if($('#copy_setup').val()==1) {
				$('#p_processing').html('Processing.  Please be patient as this can take some time when copying an existing setup.');
				$('#div_questions').hide();
				$('#div_answers').show();
				$('#ifr_answers').prop('src','new_create_step1_process.php?action=SDBIP.ADD&'+dta);
			} else {
				var result = AssistHelper.doAjax('inc_controller.php?action=SDBIP.ADD',dta);
				if(result[0]=='ok') {
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],result[2]);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		}
		
		$('#div_answers').hide();
		
		function confirmSubmission(display_text,next_step,prev_step,show_buttons) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>'+display_text+'</p><label id=lbl_hide></label>');
			if(next_step=='ok') {
				var buttons = [ { text: \"Ok\", click: function() { $( this ).dialog( \"close\" ); } } ];
			} else if(next_step=='name') {
				var buttons = [
					{ text: \"Confirm & Continue\", click: function() {
						$( this ).dialog( \"close\" );
						nextStep(next_step,$('#step').val());
						//submitForm();
					}, class: 'ui-state-ok' },
					{ text: \"Cancel & Start Over\", click: function() {
						$('#btn_back').trigger('click');
					}, class: 'ui-state-error' }
				];
			} else {
				var buttons = [
					{ text: \"Yes\", click: function() {
						nextStep(next_step,$('#step').val());
						$( this ).dialog( \"close\" );
					}, class: 'ui-state-ok' },
					{ text: \"No\", click: function() {
						$( this ).dialog( \"close\" );
					}, class: 'ui-state-error' }
				];
			}
			$('#dlg_confirm').dialog('option','buttons',buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id','dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}
		";
	} else {
		$starting_step = "choose_fin_year";
		$steps = array();
	} //end if table_Class != error
} else {
//	$table_class = "info";
//	$content = "<h3>REDIRECT TO NEXT STEP: ".$sdbip_details['next_step']."</h3>";
	//$js = "document.location.href = '".$sdbip_details['next_step']."';"
}

echo "
	<form name=frm_step1>
	<input type=hidden name=step id=step value=".$starting_step." />
	<input type=hidden name=step_history id=step_history value='' />
	<input type=hidden name=fin_year_id id=fin_year_id value='0' />
	<input type=hidden name=idp_module id=idp_module value='FALSE_LINK' />
	<input type=hidden name=idp_id id=idp_id value='0' />
	<input type=hidden name=sdbp5b_allowed id=sdbp5b_allowed value=".($allow_link_to_sdbp5b ? 1 : 0)." />
	<input type=hidden name=sdbp5b_module id=sdbp5b_module value='FALSE_LINK' />
	<input type=hidden name=sdbp5b_action id=sdbp5b_action value='' />
	<input type=hidden name=sdbp6_allowed id=sdbp6_allowed value=".($allow_link_to_sdbp6 ? 1 : 0)." />
	<input type=hidden name=sdbp6_sdbip id=sdbp6_sdbip value='FALSE_LINK' />
	<input type=hidden name=mscoa_yesno id=mscoa_yesno value='0' />
	<input type=hidden name=mscoa_version_code id=mscoa_version_code value='NA' />
	<input type=hidden name=sdbip_name id=sdbip_name value='' />
	<input type=hidden name=copy_setup id=copy_setup value='0' />
	</form>
	<div id=div_questions width=750px class=tbl-".$table_class." style='width:750px'>
		".$content."
	</div>
	<div id=dlg_confirm></div>";
if($table_class!="error" && count($steps)>0) {
	echo "
	<div id='div_answers' style='width:750px;margin:0 auto;' class=tbl-".$table_class.">
		<h1 class='green'>$sdbip_object_name Create Process</h1>
		<p id='p_processing'>Processing...</p>
		<iframe id='ifr_answers' width='740px' height='500px' style='width:740px;height:500px;border:0px solid #009900;margin:0 auto;'>
			
		</iframe>
	</div>";
}
echo "
	<script type=text/javascript>
	$(function() {
		".$js."
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});
	});
	function helpMyProcessingIsDone(icon,msg,url) {
		$(function() {
			$('#p_processing').hide();
			if(icon=='ok') {
				AssistHelper.finishedProcessingWithRedirect(icon,msg,url);
			} else {
				AssistHelper.finishedProcessing(icon,msg);
			}
		});
	}
	</script>
	";


?>
<!--
0 System Check: Are Financial Years available?
IF YES
	1 System Check: Is an IDP module available?
	IF YES
		IF count(IDP modules) > 0
			11 User: Which IDP Module do you wish to link to? SELECT
			ON SELECT
				SYSTEM: 2
				REDIRECT to 4
		ELSE
			2 System: Get list of activated IDPs with Financial Years (highlight any already linked to SDBIPs)
			IF count (activated IDP) > 0
				3 USER: Do you wish to link this SDBIP to the available IDP module? ANSWER = BOOL
				IF YES
					4 USER: Which IDP do you wish to link to? SELECT || BUTTON (IDP I want is not listed)
					IF SELECTED
						5 System - User: Please Confirm that you have selected IDP X related to Financial Year Y.  This will create the following TIME PERIODS for use within the SDBIP.
						ANSWER = BOOL
						IF YES
							6 System: Save: Link = Y, IDP = ##, Financial Year = ##
							System: Create time periods
							System: Import KPAs & Strategic Objectives
							System: Lock above lists (can't be edited by users within module)
							System: Import Projects & Top Layer KPIs
							System - redirect user to New > Create > Projects > Import from IDP > Step 2 (allocate budget)
						ELSE
							7 System: redirect back to 4
						ENDIF
					ELSE
						8 System: redirect back to 3
					ENDIF
				ELSE
					9 User: Which Financial Year is this SDBIP applicable to? SELECT
				ENDIF
			ELSE
				ERROR: No activated IDPs exist in the module.  If you wish to link to an IDP, please ensure that the IDP module has an activated IDP before continuing here.  Otherwise, continue here by skipping the link process WARNING - You will not be able to rollback the SDBIP results to the IDP nor link KPIs for year-on-year performance analysis
				IF choice = skip
					REDIRECT to 9
				ELSE
					System: Please come back to New > Create > Step 1 once an IDP has been activated in IDP_MODULE_NAME
				END IF
			END IF
		END IF
	ELSE
		Redirect to 9
	END IF
ELSE
	10 ERROR: Please create Financail Years before continuing.  To do this, please contact your Assist Administrator and ask them to update the Master Setups > Financial Years section.
END IF
-->