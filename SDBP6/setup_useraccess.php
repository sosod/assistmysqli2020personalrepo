<?php
/**
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var SDBP6 $helper - from inc_header
 */
require_once("inc_header.php");

$userObject = new SDBP6_USERACCESS();
$user_access_fields = $userObject->getUserAccessFields();
$user_access_defaults = $userObject->getUserAccessDefaults();
$users_not_set = $userObject->getUsersWithNoAccess();
$active_users = $userObject->getActiveUsers();
$js = "";

$fields_glossary = $userObject->getFieldGlossary();

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$can_import = $userObject->doesPriorSDBIPModuleExist();
?>
<table class='tbl-container not-max'>
	<?php if($can_import) { ?>
		<tr>
			<td class=right>
				<button id=btn_import>Import from Prior Module (ver. SDBP5B)</button>
			</td>
		</tr> <?php } ?>
	<tr>
		<td>
			<form name=frm_add>
				<table class=list id=tbl_useraccess>
					<tr>
						<th></th>
						<th>User</th>
						<?php
						foreach($user_access_fields as $fld => $name) {
							echo "<th title='".$fields_glossary[$fld]."'>".str_replace(" ", "<br />", $name)."</th>";
						}
						?>
						<th></th>
					</tr>
					<?php //if(count($users_not_set)>0) { ?>
					<tr>
						<td></td>
						<td><select name=ua_tkid>
								<option value=X selected>--- SELECT ---</option><?php
								foreach($users_not_set as $id => $name) {
									echo "<option value='".$id."'>".$name."</option>";
								}
								?></select></td>
						<?php
						foreach($user_access_fields as $fld => $name) {
							echo "<td>";
							$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld, 'yes' => 1, 'no' => 0, 'form' => "vertical"), $user_access_defaults[$fld]);
							echo "</td>";
						}
						?>
						<td><input type=submit value=Add id='btn_add' /></td>
					</tr>
					<?php
					//}
					foreach($active_users as $ua) {
						echo "
		<tr>
			<td>".$ua['tkid']."</td>
			<td>".$ua['name']."</td>";
						foreach($user_access_fields as $fld => $name) {
							echo "<td style='text-align: center'>".$helper->getDisplayIcon(($ua[$fld] == 1 ? "ok" : "error"))."</td>";
						}
						echo "
			<td><input type=button value=Edit class=btn_edit id='".$ua['ua_id']."' /></td>
		</tr>";
					}
					?>
				</table>
			</form>
		</td>
	</tr>
	<tr>
		<td><?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'), "user", array('section' => "USER")); ?></td>
	</tr>
</table>
<h2>Explanation of User Access Fields</h2>
<table class=form id=tbl_fields_glossary>
	<?php
	foreach($fields_glossary as $fld => $explain) {
		echo "
	<tr>
		<th>".$user_access_fields[$fld]."</th>
		<td>".$explain."</td>
	</tr>";
	}

	?>
</table>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		<?php if(count($users_not_set) > 0) { ?>
		<?php } else { ?>
		$("#tbl_useraccess tr:eq(1)").hide();
		<?php } ?>
		$("#tbl_useraccess tr").not("tr:first").next().find("span.ui-icon").css("margin", "0 auto");
		//$("span.ui-icon").css("margin","0 auto");
		$("#btn_add").click(function () {
			if ($("select[name=ua_tkid]").val() == "X") {
				alert("Please select a user.");
			} else {
				AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=frm_add]"));
				//console.log(dta);
				//alert(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Add", dta);
				//console.log(result);
				if (result[0] == "ok") {
					document.location.href = "setup_useraccess.php?r[]=" + result[0] + "&r[]=" + result[1];
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
			return false;
		});
		$("#tbl_fields_glossary").css("width", $("#tbl_useraccess").css("width"));

		$("#btn_import").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).removeClass("ui-button-state-grey").addClass("ui-button-state-orange");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = "setup_useraccess_import.php";
		});
	});
</script>
<div id=dlg_edit title="Edit User">
	<h1>Edit: <span id=spn_edit></span></h1>
	<form name=frm_edit>
		<input type=hidden value="" id='ua_id' name='ua_id' />
		<table class=form id=tbl_edit>
			<!--			<tr>
							<th width=40%>User:</th>
							<td width=60% id=td_user></td>
			</tr> -->
			<?php
			foreach($user_access_fields as $fld => $name) {
				echo "<tr><th>".str_ireplace(" ", "&nbsp;", $name).":</th>";
				echo "<td>";
				$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => "edit_".$fld, 'name' => $fld, 'yes' => 1, 'no' => 0, 'form' => "horizontal"), $user_access_defaults[$fld]);
				echo "</td>
				</tr>";
			}
			?>
		</table>
</div>
<script type="text/javascript">
	$(function () {
		$("#dlg_edit").dialog({
			modal: true,
			autoOpen: false,
			width: "1024px",
			buttons: [{
				text: "<?php echo $helper->getActivityName("save"); ?>",
				click: function () {
					AssistHelper.processing();
					var dta = AssistForm.serialize($("form[name=frm_edit]"));
					var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Edit", dta);
					if (result[0] == "ok") {
						document.location.href = "setup_useraccess.php?r[]=" + result[0] + "&r[]=" + result[1];
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				}
			}, {
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "dlg_edit");
		AssistHelper.formatDialogButtons($("#dlg_edit"), 0, AssistHelper.getDialogSaveCSS());
		AssistHelper.formatDialogButtons($("#dlg_edit"), 1, AssistHelper.getCloseCSS());
		$(".btn_edit").click(function (e) {
			e.preventDefault();
			var i = $(this).prop("id");
			$("#dlg_edit #ua_id").val(i);
			var tk = $(this).parent().parent().find("td:eq(1)").html();
			//$("#dlg_edit #td_user").html(tk);
			$("#dlg_edit #spn_edit").html(tk);
			$("#dlg_edit").dialog("open");
			var h = 0;
			$(this).parent().parent().find("td:gt(1)").each(function () {
				if ($(this).find("span").hasClass("ui-icon-check")) {
					$("#tbl_edit tr:eq(" + h + ") td button.btn_yes").trigger("click");
				} else {
					$("#tbl_edit tr:eq(" + h + ") td button.btn_no").trigger("click");
				}
				h++;
			});
			AssistHelper.focusDialogButtons($("#dlg_edit"), 1);
			AssistHelper.blurDialogButtons($("#dlg_edit"), 1);
			//resize the dialog windows to accommodate customised names
			var w = parseInt($("#tbl_edit").css("width")) + 25;
			$("#dlg_edit").dialog("option", "width", w + "px");
		});
		//detect click "yes" for second time and default third time to "no"
		$("#ua_time_second_yes").click(function (e) {
			$("#ua_time_third_no").trigger("click");
		});
		$("#edit_ua_time_second_yes").click(function (e) {
			$("#edit_ua_time_third_no").trigger("click");
		});
		//detect click "yes" for third time and default second time to "no"
		$("#ua_time_third_yes").click(function (e) {
			$("#ua_time_second_no").trigger("click");
		});
		$("#edit_ua_time_third_yes").click(function (e) {
			$("#edit_ua_time_second_no").trigger("click");
		});
	});
</script>