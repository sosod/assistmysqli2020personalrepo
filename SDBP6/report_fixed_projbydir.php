<?php


$projObject = new SDBP6_PROJECT();


$fin_year_ref = $sdbip_details['fin_year_ref'];
//get report info
if(strpos($code, "msc") === false) {
	$listObject = new SDBP6_SETUP_ORGSTRUCTURE();
	$listObject->setSDBIPID($sdbip_id);
	$report_data = $projObject->getProjectListForGroupByDepartmentFixedReport($sdbip_id);
} else {
	$listObject = new SDBP6_SEGMENTS("MSC");
	$report_data = $projObject->getProjectListForGroupByMSCFixedReport($sdbip_id);
}
$lines = $report_data['lines'];
$results = $report_data['results'];
//get grouping info
$grouping = $listObject->getOrgStructureForFixedReport();


//echo "<hr /><h1 class='green'>$code</h1><hr />";
//ASSIST_HELPER::arrPrint($results);
//die();

//get MAIN headings
$project_table_field = $projObject->getTableField();
$main_fields = array(
	$project_table_field."_sub_id",
	$project_table_field."_finref",
	$project_table_field."_ref",
	$project_table_field."_name",
);
/** @var SDBP6_HEADINGS $headingObject - from inc_header */
$main_headings = $headingObject->getMainObjectHeadings($projObject->getMyObjectType(), "LIST", "REPORT", $project_table_field, true, $main_fields);
//get Results headings
$project_results_table_field = $projObject->getResultsTableField();
switch($code) {
	case "projbydir_pref":
	case "projbymsc_pref":
		$result_headings = $headingObject->getUpdateCommentHeadings($projObject->getMyChildObjectType());
		break;
	case "projbydir_fin":
	case "projbymsc_fin":
	default:
		$result_headings = $headingObject->getUpdateNonCommentHeadings($projObject->getMyChildObjectType());
		break;
}
//	ASSIST_HELPER::arrPrint($grouping);

//ASSIST_HELPER::arrPrint($result_headings);
//
//	ASSIST_HELPER::arrPrint($results);
//	ASSIST_HELPER::arrPrint($report_data);


//display onscreen


?>
<style type="text/css">
	.grouping-td {
		background-color: #eeeeee;
		font-weight: bold;
	}

	.results, .total-results {
		text-align: right;
	}
</style>
<h1 class="center"><?php echo $myObject->getCmpName(); ?></h1>
<h2 class="center"><?php echo $report_settings['user_name']; ?></h2>
<table>
	<thead>
	<tr>
		<?php
		$column_count = 0;
		foreach($main_fields as $fld) {
			$head = $main_headings[$fld];
			echo "<th rowspan=2>".$head['name']."</th>";
			$column_count++;
		}
		$colspan = count($result_headings);
		foreach($time_periods as $time_id => $time) {
			echo "<th colspan=".$colspan.">".$time['name']."</th>";
			$column_count += $colspan;
		}
		$column_count += $colspan;    //add to column count for total column
		?>
		<th colspan="<?php echo $colspan ?>">Total</th>
	</tr>
	<tr>
		<?php
		foreach($time_periods as $time_id => $time) {
			foreach($result_headings as $rfld => $rhead) {
				echo "<th>".$rhead['name']."</th>";
			}
		}
		//row total - no text column
		foreach($result_headings as $rfld => $rhead) {
			if($rhead['type'] != "TEXT") {
				echo "<th>".$rhead['name']."</th>";
			}
		}
		?>
	</tr>
	</thead>
	<tbody>
	<?php
	$grand_totals = array();
	foreach($grouping[0] as $group_id => $group) {
		echo "<tr><td class='grouping-td group-title' colspan='".$column_count."'>".$group['name']."</td></tr>";
		$count_projects = 0;
		$sub_totals = array();
		if(isset($grouping[$group_id]) && count($grouping[$group_id]) > 0) {
			foreach($grouping[$group_id] as $sub_id => $sub) {
				if(is_array($sub)) {
					$s_name = $sub['name'];
				} else {
					$s_name = $sub;
				}
				if(isset($report_data['lines'][$sub_id]) && count($report_data['lines'][$sub_id]) > 0) {
					foreach($report_data['lines'][$sub_id] as $proj_id => $proj) {
						$row_totals = array();
						$count_projects++;
						if(isset($results[$proj_id])) {
							$my_results = $results[$proj_id];
						} else {
							$my_results = array();
						}
						echo "<tr>";
						foreach($main_fields as $fld) {
							echo "<td>".($fld == $project_table_field."_sub_id" ? $s_name : $proj[$fld])."</td>";
						}
						foreach($time_periods as $time_id => $time) {
							if(isset($my_results[$time_id])) {
								$time_results = $my_results[$time_id];
							} else {
								$time_results = array();
							}
							foreach($result_headings as $rfld => $rhead) {
								$v = isset($time_results[$rfld]) ? $time_results[$rfld] : 0;
								if($rhead['type'] == "TEXT") {
									echo "<td class='text-results row-budget'>".$v."</td>";
								} else {
									$class = "results";
									if(strpos($rfld, "var") !== false) {
										$class .= " row-variance";
									} elseif(strpos($rfld, "revised") !== false) {
										$class .= " row-budget";
									}
									echo "<td class='$class'>".str_replace(",", "&nbsp;", number_format($v, 0))."</td>";
									if(!isset($row_totals[$rfld])) {
										$row_totals[$rfld] = $v;
									} else {
										$row_totals[$rfld] += $v;
									}
									if(!isset($sub_totals[$time_id][$rfld])) {
										$sub_totals[$time_id][$rfld] = $v;
									} else {
										$sub_totals[$time_id][$rfld] += $v;
									}
									if(!isset($grand_totals[$time_id][$rfld])) {
										$grand_totals[$time_id][$rfld] = $v;
									} else {
										$grand_totals[$time_id][$rfld] += $v;
									}
								}
							}
						}
						foreach($result_headings as $rfld => $rhead) {
							if($rhead['type'] != "TEXT") {
								echo "<td class='right row-total'>".str_replace(",", "&nbsp;", number_format((isset($row_totals[$rfld]) ? $row_totals[$rfld] : 0), 0))."</td>";
							}
						}
						echo "</tr>";
					}
				}
			}
		}
		if($count_projects == 0) {
			echo "<tr><td class='td-no-items'  colspan='".$column_count."'>".$projObject->replaceAllNames("No |PROJECTS| found for this section.")."</td></tr>";
		} else {
			echo "<tr><td colspan=".count($main_fields)." class='right group-total'>Sub-Total for ".$group['name'].":</td>";
			$row_totals = array();
			foreach($time_periods as $time_id => $time) {
				foreach($result_headings as $rfld => $rhead) {
					$v = (isset($sub_totals[$time_id][$rfld]) ? $sub_totals[$time_id][$rfld] : 0);
					if(!isset($row_totals[$rfld])) {
						$row_totals[$rfld] = $v;
					} else {
						$row_totals[$rfld] += $v;
					}
					if($rhead['type'] == "TEXT") {
						echo "<td class='group-total'></td>";
					} else {
						echo "<td class='right group-total'>".str_replace(",", "&nbsp;", number_format($v, 0))."</td>";
					}
				}
			}
			foreach($result_headings as $rfld => $rhead) {
				if($rhead['type'] != "TEXT") {
					echo "<td class='right group-total-total'>".str_replace(",", "&nbsp;", number_format((isset($row_totals[$rfld]) ? $row_totals[$rfld] : 0), 0))."</td>";
				}
			}
			echo "</tr>";
		}
	}
	?>
	</tbody>
	<tfoot>
	<?php
	echo "<tr><td colspan=".count($main_fields)." class='right grand-total'>Total:</td>";
	$row_totals = array();
	foreach($time_periods as $time_id => $time) {
		foreach($result_headings as $rfld => $rhead) {
			$v = (isset($grand_totals[$time_id][$rfld]) ? $grand_totals[$time_id][$rfld] : 0);
			if(!isset($row_totals[$rfld])) {
				$row_totals[$rfld] = $v;
			} else {
				$row_totals[$rfld] += $v;
			}
			if($rhead['type'] == "TEXT") {
				echo "<td class='grand-total'></td>";
			} else {
				echo "<td class='right grand-total'>".str_replace(",", "&nbsp;", number_format($v, 0))."</td>";
			}
		}
	}
	foreach($result_headings as $rfld => $rhead) {
		if($rhead['type'] != "TEXT") {
			echo "<td class='right grand-total-total'>".str_replace(",", "&nbsp;", number_format((isset($row_totals[$rfld]) ? $row_totals[$rfld] : 0), 0))."</td>";
		}
	}
	echo "</tr>";

	?>
	</tfoot>
</table>
<p class="i"><small>Generated: <?php echo date("d F Y H:i:s"); ?></small></p>
<script type="text/javascript">
	$(function () {
		$("td").not(".grouping-td").css("font-size", "90%");
	})
</script>