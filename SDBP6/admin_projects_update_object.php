<?php
if(isset($_REQUEST['page_src']) && $_REQUEST['page_src'] == "dialog") {
	$no_page_heading = true;
} else {
	$_REQUEST['page_src'] = "page";
}

require_once("inc_header.php");


$object_id = $_REQUEST['object_id'];
$myObject = new SDBP6_PROJECT();

$page_section = "ADMIN";
$page_action = "UPDATE";

$page_redirect_path = "admin_projects_update.php?";


include("common/update_object.php");


?>

<?php markTime("end of page"); ?>