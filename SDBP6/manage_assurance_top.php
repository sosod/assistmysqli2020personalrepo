<?php
require_once("inc_header.php");
$header_already_included = true;


$myObject = new SDBP6_TOPKPI();
$object_type = $myObject->getMyObjectType();
$parent_object_type = $myObject->getMyParentObjectType();
$class_name = "SDBP6_".$parent_object_type;
$parentObject = new $class_name();
$parent_object_id = $sdbip_details['id'];

$page_section = "MANAGE";
$page_action = "ASSURANCE";
//$filter_by = $myObject->getFilterByOptions($page_section,$page_action); //ASSIST_HELPER::arrPrint($filter_by);
$filter_by = true;


$add_button = false;
//$add_button_label = "|add| |".$object_type."|"; //echo $add_button_label;
//$add_button_function = "showAddDialog();";

$page_direct = "manage_assurance_top_object.php";
$button_label = "|review|";

include("common/generic_list_page.php");
markTime("end generic list page");
?>
	<script type="text/javascript">
		$(function () {
			<?php echo $data['js']; ?>

		});	//end start of jquery


	</script>
<?php markTime("end of page"); ?>