<?php


/**************
 * CODE TO MEASURE PAGE LOAD TIME
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

$now = time();

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	//echo '<p>'.$s.' => '.$total_time.' seconds.';

}

//error_reporting(-1);
require_once("../module/autoloader.php");
marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/", $self);
$self = explode(".", end($self));
$page = $self[0];
if(isset($my_page)) {
	$page .= "_".$my_page;
}
$navigation_path = explode("_", $page);


marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */

$redirect = array(
	'new_create' => "new_create_step1",
	'new_activate' => "new_activate_do",
	'new_create_top' => "new_create_top_create",
	'new_create_dept' => "new_create_dept_create",
	'new_create_projects' => "new_create_projects_create",
	'manage' => "manage_view_dept",
	'manage_view' => "manage_view_dept",
	'manage_update' => "manage_update_dept",
	'manage_edit' => "manage_edit_dept",
	'manage_create' => "manage_create_dept",
	'manage_approve' => "manage_approve_dept",
	'manage_assurance' => "manage_assurance_dept",
	'setup' => "setup_sdbip",
	'report_generate' => "report_generate_dept",
	'report_graphs' => "report_graphs_dept",
	'admin_dept' => "admin_dept_update",
	'admin_dept_exception' => "admin_top_auto_exception",
	'admin_top' => "admin_top_update",
	'admin_top_auto' => "admin_top_auto_link",
	'admin_top_link' => "admin_top_auto_link",
	'admin_projects' => "admin_projects_update",
	'admin_finance' => "admin_finance_manual",
	'admin_auditlog' => "admin_auditlog_dept",
	'admin_assurance' => "admin_assurance_dept_assurance",
	'admin_assurance_dept' => "admin_assurance_dept_assurance",
	'admin_assurance_top' => "admin_assurance_top_assurance",
);
//	'report'			=> "report_generate_dept",
if(isset($redirect[$page])) {
	header("Location:".$redirect[$page].".php");
}
marktime("redirect");

/*********
 * create base helper object
 */
if(!isset($local_modref)) {
	$helper = new SDBP6();
	$local_modref = $helper->getModRef();
} else {
	$helper = new SDBP6($local_modref);
}


//echo "<hr /><h1 class='green'>Helllloooo from TOP OF INC HEADER ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($_REQUEST['filter']);
//echo "<hr />";

//Save a history of the filters so that they can be switched between for different years #AA-447
if(isset($_REQUEST['filter']) && isset($_REQUEST['filter']['year'])) {
	saveListPageFilterHistory();
	//if filter submitted & year changed then other filter settings won't be applicable to the current year, so default back to the last history item
	if(isset($_SESSION[$local_modref]['LIST_PAGE_FILTER']) && $_REQUEST['filter']['year'] != $_SESSION[$local_modref]['LIST_PAGE_FILTER']['year'] && isset($_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$_REQUEST['filter']['year']])) {
		$keys = array_keys($_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$_REQUEST['filter']['year']]);
		$key = $keys[count($keys) - 1];
		$_REQUEST['filter'] = $_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$_REQUEST['filter']['year']][$key];
	} elseif(!isset($_SESSION[$local_modref]['LIST_PAGE_FILTER']) || $_REQUEST['filter']['year'] == $_SESSION[$local_modref]['LIST_PAGE_FILTER']['year']) {
		saveListPageFilterHistory($_REQUEST['filter']);
	}
	$_SESSION[$local_modref]['LIST_PAGE_FILTER'] = $_REQUEST['filter'];
}

function saveListPageFilterHistory($filter_to_save = false) {
	global $local_modref;
//	echo "<hr /><h1 class='red'>Helllloooo SaveListPageFilterHistory from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

	$save_filter = true;
	if($filter_to_save === false && isset($_SESSION[$local_modref]['LIST_PAGE_FILTER'])) {
		$current_filter = $_SESSION[$local_modref]['LIST_PAGE_FILTER'];
	} elseif($filter_to_save !== false) {
		$current_filter = $filter_to_save;
	} else {
		$current_filter = array();
		$save_filter = false;
	}
	if($save_filter !== false) {
		if(isset($_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$current_filter['year']])) {
			$keys = array_keys($_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$current_filter['year']]);
			$key = $keys[count($keys) - 1];
			$test_1 = implode("|", $current_filter);
			$test_2 = implode("|", $_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$current_filter['year']][$key]);
			if($test_1 == $test_2) {
				$save_filter = false;
			}
		}
		if($save_filter) {
			$_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY'][$current_filter['year']][] = $current_filter;
		}
	}
}

//echo "<hr /><h1 class='green'>Helllloooo AFTER REQUEST FILTER from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($_REQUEST['filter']);
//ASSIST_HELPER::arrPrint($_SESSION[$local_modref]['LIST_PAGE_FILTER']);
//ASSIST_HELPER::arrPrint($_SESSION[$local_modref]['LIST_PAGE_FILTER_HISTORY']);
//echo "<hr />";


if($navigation_path[0] == "new") {
	$is_new_section = true;
} else {
	$is_new_section = false;
}
if($navigation_path[0] == "report") {
	$is_report_section = true;
} else {
	$is_report_section = false;
}
$is_setup_sdbip_section = false;
if($navigation_path[0] == "setup") {
	$is_setup_section = true;
	if($navigation_path[1] == "sdbip") {
		$is_setup_sdbip_section = true;
		if(!isset($_REQUEST['sdbip_id'])) {
			if(isset($_SESSION[$local_modref]['SETUP']['sdbip_defaults']['id'])) {
				$sdbip_id = $_SESSION[$local_modref]['SETUP']['sdbip_defaults']['id'];
				$_REQUEST['sdbip_id'] = $sdbip_id;
			}
		} else {
			$sdbip_id = $_REQUEST['sdbip_id'];
			$_SESSION[$local_modref]['SETUP']['sdbip_defaults']['id'] = $sdbip_id;
		}
	}
} else {
	$is_setup_section = false;
}

$head_dir = $_SESSION['cmp_terminology']['DEPT'];
$head_sub = $_SESSION['cmp_terminology']['SUB'];


$display_sdbip_name = false;
if(in_array($navigation_path[0], array("new", "manage", "admin", "report")) || ($is_setup_sdbip_section && isset($navigation_path[2]))) {
	$display_sdbip_name = true;
}


//FOR DEVELOPMENT PURPOSES ONLY TO FORCE CHECK OF DB FOR NAMES DUE TO CONSTANT ADJUSTMENTS
//ONLY OPERATES ON LOCALHOST SITES
$force_me_to_update = false;
if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
//	$helper->forceNameUpdate();
	$force_me_to_update = true;
}


marktime("helper ".__LINE__);


//ASSIST_HELPER::arrPrint($menu);
/************
 * Section specific redirects - IF REQUIRED
 */
//if($navigation_path[0]=="new" && $navigation_path[1]=="create" && $menu['buttons'][1][0]['link']!="new_create.php") {
//	header("Location:".$menu['buttons'][1][0]['link']);
//}
marktime("menu".__LINE__);

/***********
 * Create default objects used throughout the module
 * 1/2 second time lost here
 * Session storage of objects used temporarily for DEVELOPMENT - consider removing on live version to limit risk of server overload due to large size of objects - cost to DB is less than cost to unserialize
 */
$session_object_name = "headingObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$headingObject = new SDBP6_HEADINGS($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($headingObject);
} else {
	$headingObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}
$session_object_name = "displayObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$displayObject = new SDBP6_DISPLAY($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($displayObject);
} else {
	$displayObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}
$session_object_name = "nameObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$nameObject = new SDBP6_NAMES($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($nameObject);
} else {
	$nameObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}

$session_object_name = "orgObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$orgObject = new SDBP6_SETUP_ORGSTRUCTURE($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($orgObject);
} else {
	$orgObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}
$session_object_name = "adminObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$adminObject = new SDBP6_SETUP_ADMINS($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($adminObject);
} else {
	$adminObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}
$session_object_name = "sdbipObject";
if($force_me_to_update || !isset($_SESSION[$local_modref][$session_object_name])) {
	$sdbipObject = new SDBP6_SDBIP($local_modref);
	$_SESSION[$local_modref][$session_object_name] = serialize($sdbipObject);
} else {
	$sdbipObject = unserialize($_SESSION[$local_modref][$session_object_name]);
}
marktime("objects".__LINE__);
$sdbip_object_name = $sdbipObject->getObjectName("SDBIP");
$sdbip_name = "";


//Added MY_CURRENT_SDBIP to remember which SDBIP is being worked on - activatedSDBIP check timeout happening too quickly when adding KPIs #AA-447 JC 15 July 2020
if(isset($_REQUEST['sdbip_id'])) {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$sdbip_id = $_REQUEST['sdbip_id'];
} elseif(isset($_REQUEST['filter']['year'])) {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$sdbip_id = $_REQUEST['filter']['year'];
} elseif($is_report_section && isset($_REQUEST['my_sdbip_id'])) {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$sdbip_id = $_REQUEST['my_sdbip_id'];
} elseif($is_report_section && isset($_SESSION[$sdbipObject->getModRef()]['REPORT_SDBIP_ID'])) {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$sdbip_id = $_SESSION[$sdbipObject->getModRef()]['REPORT_SDBIP_ID'];
} elseif(isset($external_call) && $external_call === true && isset($_REQUEST['object_id']) && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($_REQUEST['object_id']) && isset($class_name_for_external_call) && strlen($class_name_for_external_call) > 0) {
	$anObject = new $class_name_for_external_call();
	$sdbip_id = $anObject->getMyParentSDBIPID($_REQUEST['object_id']);
	unset($anObject);
} elseif(isset($_SESSION[$sdbipObject->getModRef()]['MY_CURRENT_SDBIP'])) {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$sdbip_id = $_SESSION[$sdbipObject->getModRef()]['MY_CURRENT_SDBIP'];
} else {
	//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	//AA-361 - If no SDBIP has been selected then check if the default has been set in Setup > Global > Module Preferance else set to false to default to oldest SDBIP
	$preferencesObject = new SDBP6_SETUP_PREFERENCES();
	$default_sdbip = $preferencesObject->getAnswerToQuestionByCode("default_sdbip");
	if($default_sdbip == 0) {
		$sdbip_id = false;
	} else {
		$sdbip_id = $default_sdbip;
	}
}

//Update to manage remembering SDBIP IDs better #AA-447
if($sdbip_id !== false) {
	//Update current SDBIP in case it is different from what was previously
	$_SESSION[$sdbipObject->getModRef()]['MY_CURRENT_SDBIP'] = $sdbip_id;
	//check if list page filter history exists for new sdbip so that list pages display correctly
	if(isset($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER'])) {
		$current_filter = $_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER'];
		if($sdbip_id != $current_filter['year']) {
			//update history for previous sdbip
			saveListPageFilterHistory();
			//if history for new SDBIP exists then use that
			if(isset($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER_HISTORY'][$sdbip_id]) && count($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER_HISTORY'][$sdbip_id]) > 0) {
				$_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER'] = $_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER_HISTORY'][$sdbip_id][count($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER_HISTORY'][$sdbip_id]) - 1];
				$_REQUEST['filter'] = $_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER'];
			}
		}
	}
}

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint(array_keys($_SESSION[$sdbipObject->getModRef()]));
//ASSIST_HELPER::arrPrint($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER']);
//ASSIST_HELPER::arrPrint($_SESSION[$sdbipObject->getModRef()]['LIST_PAGE_FILTER_HISTORY']);
//echo "<hr />";

if($is_setup_sdbip_section && $sdbip_id !== false) {
	$sdbip_details = $sdbipObject->getSDBIPByID($sdbip_id);
	$is_sdbip_activated = $sdbip_details['is_activated'];
	$sdbip_name = $sdbip_details['name'];
} else {
	if($is_new_section && isset($_REQUEST['step']) && $_REQUEST['step'] == "reset") {
		$sdbip_details = $sdbipObject->getSDBIPByID($sdbip_id);
		$is_sdbip_activated = false;
	} else {
		$is_sdbip_activated = $sdbipObject->doesActivatedSDBIPExist($is_new_section, false, $sdbip_id);
		//catch where user has worked on new SDBIP in New and is now back in Manage or Admin looking for the current SDBIP
		if(!$is_sdbip_activated && !$is_new_section) {
//			echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
			$is_sdbip_activated = $sdbipObject->doesActivatedSDBIPExist(true, false, false);
			if($is_sdbip_activated) {
				$sdbip_id = false;
			}
		}
		if($is_sdbip_activated && !$is_new_section) {
			$display_sdbip_name = $display_sdbip_name && $is_sdbip_activated;
			$sdbip_details = $sdbipObject->getCurrentActivatedSDBIPDetails(false, $sdbip_id);
			$sdbip_id = $sdbip_details['sdbip_id'];
			$_SESSION[$sdbipObject->getModRef()]['MY_CURRENT_SDBIP'] = $sdbip_id;
			$sdbip_name = $sdbip_details['name'];
			$timeObject = new SDBP6_SETUP_TIME($local_modref);
		} else {
			//test for new SDBIP
			$is_new_sdbip_in_progress = $sdbipObject->hasSDBIPBeenCreatedAndNotYetActivated();
			if($is_new_sdbip_in_progress) {
				$display_sdbip_name = $display_sdbip_name && $is_new_sdbip_in_progress;
				$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
				$sdbip_id = $sdbip_details['sdbip_id'];
				$_SESSION[$sdbipObject->getModRef()]['MY_CURRENT_SDBIP'] = $sdbip_id;
				$sdbip_name = $sdbip_details['name'];
			}
		}
	}
}

if(isset($sdbip_details['mscoa_version'])) {
	$mscoa_version_code = $sdbip_details['mscoa_version'];
	$mscoa_version_id = $sdbip_details['mscoa_version_id'];
} else {
	$mscoa_version_code = "NA";
	$mscoa_version_id = 1;
}
$no_activated_sdbip_sections = $sdbipObject->getPagesWhereNoActivatedSDBIPisAllowed();
marktime("variables".__LINE__);


/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new SDBP6_MENU($local_modref);
$menu = $menuObject->getPageMenu($page);
/****
 * secondary redirect for New and Admin which is useraccess limited
 */
if(isset($menu['buttons'][0])) {
	if(count($menu['buttons'][0]) > 0) {
		header("Location:".$menu['buttons'][0][0]['link']);
	} else {
		die(ASSIST_HELPER::displayResult(array("info", "You do not have access to this section.  Please try again or contact your Assist Administrator if you believe this to be an error.")));
	}
}


/***********
 * Start general header
 */

if(!isset($no_page_output) || !$no_page_output) {
	$s = array("/library/js/assist.ui.paging.js?".time(),
		"/library/jquery-plugins/jscolor/jscolor.js",
		"/".$helper->getModLocation()."/common/sdbp6helper.js?".time(),
		"/".$helper->getModLocation()."/common/sdbp6.js",
		"/".$helper->getModLocation()."/common/hoverIntent.js");
	if(isset($scripts)) {
		$scripts = array_merge($s, $scripts);
	} else {
		$scripts = $s;
	}


	ASSIST_HELPER::echoPageHeader("1.10.0", $scripts, array("/assist_jquery.css?".time(), "/assist3.css?".time()));

	$js = ""; //to catch any js put out by the various display classes

	marktime("general".__LINE__);

	/*********
	 * Module-wide Javascript
	 */

	?>
	<script type="text/javascript">
		$(document).ready(function () {
			//format container and sub-container tables to hide borders
			//$("table.tbl-container").addClass("noborder");
			$("table.tbl-container:not(.not-max)").css("width", "100%");
			//$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
			$("table.th2").find("th").addClass("th2");
			$("table tr.th2").find("th").addClass("th2");
			$("table.tbl_audit_log").css("width", "100%").find("td").removeClass("noborder");
			$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");

			//$("select").css("padding","1px");

			//jquery ui button formatting
			$("button.abutton").children(".ui-button-text").css({"padding-top": "0px", "padding-bottom": "0px"});


			$("div.tbl-error").addClass("ui-state-error").find("h1").addClass("red");
			$("div.tbl-ok").addClass("ui-state-ok").find("h1").addClass("green");
			$("div.tbl-info").addClass("ui-state-info").find("h1").addClass("orange");
		});

	</script>

	<style type="text/css">
		/* jquery ui button styling */
		/* used by  setup > useraccess */
		#tbl_useraccess .button_class, #tbl_useraccess .ui-widget.button_class {
			font-size: 75%;
			padding: 1px;
		}

		table.tbl-container, table.tbl-container > tbody > tr > td {
			border: 0px solid #ffffff;
		}

		.small-button {
			font-size: 75%;
			padding: 1px;
		}


		/* tbl-ok/error styling - used by Create for user notifications */
		/* generic settings */
		table.tbl-ok, table.tbl-error, div.tbl-ok, div.tbl-error, div.tbl-info {
			-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			margin: 50px auto;
			border-collapse: inherit;
		}

		table.tbl-ok td, table.tbl-error td {
			border: 0px solid #ffffff;
		}

		div.tbl-ok, div.tbl-error, div.tbl-info {
			padding: 0px 15px 10px 15px;
		}

		div.tbl-ok p, div.tbl-error p, div.tbl-info p {
			font-size: 150%;
			line-height: 165%;
		}


		/* green = ok */
		table.tbl-ok {
			border: 2px solid #009900;
		}

		div.tbl-ok {
		}

		/* red = error */
		table.tbl-error {
			border: 2px solid #009900;
		}

		div.tbl-error {
		}

		/* orange = info */
		table.tbl-info {
			border: 2px solid #fe9900;
		}

		div.tbl-info {
		}


		button.cancel_attach {
			font-size: 75%;
			padding: 1px;
		}

		table.attach {
			border: 0px solid #ffffff;
		}

		table.attach td {
			border: 0px solid #ffffff;
			vertical-align: middle;
			background: url();
		}

		/* Results colours */
		.result {
			font-weight: bold;
			color: #ffffff;
			padding: 2px 4px 2px 4px;
			text-align: center;
			margin: 0px;
		}

		.result0 {
			background-color: #777777;
		}

		.result1 {
			background-color: #cc0001;
		}

		.result2 {
			background-color: #fe9900;
		}

		.result3 {
			background-color: #009900;
		}

		.result4 {
			background-color: #005500;
		}

		.result5 {
			background-color: #000077;
		}

		/* Enable this to limit size of autocomplete select list - currently disabled after discussion with Louw/HES0001 on 8 Jan 2020 and agreed that full list easier to work with [JC] */
		/
		/
		.ui-autocomplete {
		/ / max-height: 200 px;
		/ / overflow-y: auto;
			/* prevent horizontal scrollbar */
		/ / overflow-x: hidden;
		/ /
		}

	</style>
	<?php
	marktime("module wide".__LINE__);
	/**********
	 * Navigation buttons
	 */
	if(!isset($no_page_heading) || !$no_page_heading) {
		if(isset($do_not_include_sdbip_name) && $do_not_include_sdbip_name == true && isset($sdbip_name)) {
			unset($sdbip_name);
		}
		$active_button_label = $menuObject->drawPageTop($menu, (isset($display_navigation_buttons) ? $display_navigation_buttons : true), ($display_sdbip_name), ((isset($sdbip_name)) ? $sdbip_name : ""));
	}
}
$helper->logUserPageMovement();
if(!$is_sdbip_activated && (!in_array($navigation_path[0], $no_activated_sdbip_sections))) {
	ASSIST_HELPER::displayResult(array("info", "No activated $sdbip_object_name is available.  Please come back once a(n) $sdbip_object_name has been activated."));
	marktime("end inc_header".__LINE__);
	die();
} else {

	marktime("end inc_header".__LINE__);
}
?>