<?php
/**
 * Var from calling page
 * @var array $sdbip_details
 * @var SDBP6_FINANCE $myObject
 * @var array $report_settings
 * @var array $time_periods
 */

function drawLine($line_id, $line) {
	global $line_map, $line_financials, $total_financials, $time_periods, $financials;
	echo "<tr><td class='center b'>".$line['code']."</td><td class='b'>".$line['name']."</td>";
	if(isset($line_map[$line_id])) {
		$lines = $line_map[$line_id];
		$line_financials = array('total' => array('budget' => 0, 'actual' => 0));
		foreach($lines as $segmentcode => $guids) {
			foreach($guids as $guid) {
				if(isset($financials[$segmentcode][$guid])) {
					$f = $financials[$segmentcode][$guid];
					foreach($f as $time_id => $v) {
						if(!isset($line_financials[$time_id])) {
							$line_financials[$time_id] = array('budget' => 0, 'actual' => 0);
						}
						$line_financials[$time_id]['budget'] += $v['budget'];
						$line_financials[$time_id]['actual'] += $v['actual'];
						if(!isset($line_financials['total'])) {
							$line_financials['total'] = array('budget' => 0, 'actual' => 0);
						}
						$line_financials['total']['budget'] += $v['budget'];
						$line_financials['total']['actual'] += $v['actual'];
						if(!isset($total_financials[$time_id])) {
							$total_financials[$time_id] = array('budget' => 0, 'actual' => 0);
						}
						$total_financials[$time_id]['budget'] += $v['budget'];
						$total_financials[$time_id]['actual'] += $v['actual'];
						if(!isset($total_financials['total'])) {
							$total_financials['total'] = array('budget' => 0, 'actual' => 0);
						}
						$total_financials['total']['budget'] += $v['budget'];
						$total_financials['total']['actual'] += $v['actual'];
						if(!isset($total_financials['sub'][$time_id])) {
							$total_financials['sub'][$time_id] = array('budget' => 0, 'actual' => 0);
						}
						$total_financials['sub'][$time_id]['budget'] += $v['budget'];
						$total_financials['sub'][$time_id]['actual'] += $v['actual'];
						if(!isset($total_financials['sub']['total'])) {
							$total_financials['sub']['total'] = array('budget' => 0, 'actual' => 0);
						}
						$total_financials['sub']['total']['budget'] += $v['budget'];
						$total_financials['sub']['total']['actual'] += $v['actual'];
					}
				} else {

				}
			}
		}
	}
	foreach($time_periods as $time_id => $time) {
		if(!isset($line_financials[$time_id])) {
			$line_financials[$time_id] = array('budget' => 0, 'actual' => 0);
		}
		echo "
			<td class='right row-budget'>".str_replace(",", "&nbsp;", number_format($line_financials[$time_id]['budget'], 0))."</td>
			<td class='right'>".str_replace(",", "&nbsp;", number_format($line_financials[$time_id]['actual'], 0))."</td>";
	}
	if(!isset($line_financials['total'])) {
		$line_financials['total'] = array('budget' => 0, 'actual' => 0);
	}
	echo "
			<td class='right row-total'>".str_replace(",", "&nbsp;", number_format($line_financials['total']['budget'], 0))."</td>
			<td class='right row-total'>".str_replace(",", "&nbsp;", number_format($line_financials['total']['actual'], 0))."</td>";
	echo "
		</tr>";
}


$fin_year_ref = $sdbip_details['fin_year_id'];
//get schedule details
$schedule_settings = $myObject->getSDBIPSchedule($report_settings['code']);
$schedule_map = $schedule_settings['fin_map'];
$line_map = $schedule_settings['map'];
//get financials for schedule map
$finObject = new SDBP6_FINANCE();
$financials = $finObject->getFinancialsForScheduleReport($fin_year_ref, $schedule_map);
//display onscreen

//if($myObject->isSupportUser()) { echo $fin_year_ref."<hr />"; ASSIST_HELPER::arrPrint($financials); }

?>
<h1 class="center"><?php echo $myObject->getCmpName(); ?></h1>
<h2 class="center"><?php echo $report_settings['user_name']; ?></h2>
<table>
	<thead>
	<tr>
		<th rowspan='2'></th>
		<th rowspan='2'>
			<div width="350px" style="width:350px">&nbsp;</div>
		</th>
		<?php
		foreach($time_periods as $time_id => $time) {
			echo "<th colspan=2>".$time['name']."</th>";
		}
		?>
		<th colspan="2">Total</th>
	</tr>
	<tr>
		<?php
		foreach($time_periods as $time_id => $time) {
			echo "<th>Budget</th><th>Actual</th>";
		}
		echo "<th>Budget</th><th>Actual</th>";
		?>
	</tr>
	</thead>
	<tbody>
	<?php
	$total_financials = array('total' => array('budget' => 0, 'actual' => 0), 'sub' => array());
	foreach($schedule_settings['lines'][0] as $line_id => $line) {
		if(isset($schedule_settings['lines'][$line_id]) && count($schedule_settings['lines'][$line_id]) > 0) {
			echo "<tr><td class='grouping-td group-title' colspan='".(2 + (count($time_periods) * 2) + 2)."'>".$line['name']."</td></tr>";
			foreach($schedule_settings['lines'][$line_id] as $l_id => $l) {
				drawLine($l_id, $l);
			}
			?>
			<tr>
				<td colspan="2" class="right group-total">Sub-Total for <?php echo $line['name']; ?>:</td>
				<?php
				foreach($time_periods as $time_id => $time) {
					if(!isset($total_financials['sub'][$time_id])) {
						$total_financials['sub'][$time_id] = array('budget'=>0,'actual'=>0);
					}
					echo "
			<td class='right group-total'>".str_replace(",", "&nbsp;", number_format($total_financials['sub'][$time_id]['budget'], 0))."</td>
			<td class='right group-total'>".str_replace(",", "&nbsp;", number_format($total_financials['sub'][$time_id]['actual'], 0))."</td>";
				}
				if(!isset($total_financials['sub']['total'])) {
					$total_financials['sub']['total'] = array('budget'=>0,'actual'=>0);
				}
				echo "
			<td class='right group-total-total'>".str_replace(",", "&nbsp;", number_format($total_financials['sub']['total']['budget'], 0))."</td>
			<td class='right group-total-total'>".str_replace(",", "&nbsp;", number_format($total_financials['sub']['total']['actual'], 0))."</td>";
				?>
			</tr>
			<?php
			//reset sub total
			$total_financials['sub'] = array();
		} else {
			drawLine($line_id, $line);
		}
	}
	?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2" class="right grand-total">Total:</td>
		<?php
		foreach($time_periods as $time_id => $time) {
			if(!isset($total_financials[$time_id])) {
				$total_financials[$time_id] = array('budget'=>0,'actual'=>0);
			}
			echo "
			<td class='right grand-total'>".str_replace(",", "&nbsp;", number_format($total_financials[$time_id]['budget'], 0))."</td>
			<td class='right grand-total'>".str_replace(",", "&nbsp;", number_format($total_financials[$time_id]['actual'], 0))."</td>";
		}
		echo "
			<td class='right grand-total-total'>".str_replace(",", "&nbsp;", number_format($total_financials['total']['budget'], 0))."</td>
			<td class='right grand-total-total'>".str_replace(",", "&nbsp;", number_format($total_financials['total']['actual'], 0))."</td>";
		?>
	</tr>
	</tfoot>
</table>
<p class="i"><small>Generated: <?php echo date("d F Y H:i:s"); ?></small></p>
<script type="text/javascript">
	$(function () {
		$("td").not(".grouping-td").css("font-size", "90%");
	})
</script>