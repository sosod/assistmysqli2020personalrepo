<?php
/**
 * @var array $sdbip_details - array of sdbip currently going through NEW process - from inc_new_statuscheck
 * @var SDBP6_SDBIP $sdbipObject - from inc_header
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var string $js - placeholder for all Javascript/jquery - from inc_header
 * @var string $sdbip_object_name - from inc_header
 */
//Don't use SDBIP Chooser as only 1 SDBIP can be new at any time.

$_REQUEST['step'] = "activate";
require_once("inc_header.php");
require_once("inc_new_statuscheck.php");


$available_sdbips = $sdbipObject->getAvailableSDBIPsForChooser("CONFIRMED", false, false);

if(count($available_sdbips) == 0) {
	ASSIST_HELPER::displayResult(array("info", "No available ".$sdbipObject->replaceAllNames(SDBP6_SDBIP::OBJECT_TYPE)." to display."));
	die();
}


$sdbip_id = $sdbip_details['id'];
$fin_year_name = $sdbip_details['fin_year_name'];
$summary_details = $sdbip_details['summary_details'];
$active_module_sections = $sdbipObject->getActiveSections();

$is_confirmed = $sdbip_details['is_confirmed'];
$is_activated = $sdbip_details['is_activated'];

if($sdbip_details['idp_module'] == "FALSE_LINK") {
	$echo = $displayObject->getDataField("BOOL_BUTTON", 0);
	$idp_module = $echo['display'];
	$js .= $echo['js'];
} else {
	$idp_name = $sdbipObject->getLinkedIDP($sdbip_details['idp_module'], $sdbip_details['idp_id']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$idp_module = $echo['display']." - ".$idp_name;
	$js .= $echo['js'];
}

if($sdbip_details['alt_module'] == "FALSE_LINK") {
	$echo = $displayObject->getDataField("BOOL_BUTTON", 0);
	$alt_module = $echo['display'];
	$js .= $echo['js'];
} elseif($sdbipObject->checkIntRef($sdbip_details['alt_module'])) {
	$alt_name = $sdbip_details['alt_module_name'];//$sdbipObject->getSDBIPName($sdbip_details['alt_module']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$alt_module = $echo['display']." - ".$alt_name." [".ucwords($sdbip_details['alt_action'])."]";
	$js .= $echo['js'];
} else {
	$alt_name = $sdbipObject->getLinkedSDBIP($sdbip_details['alt_module']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$alt_module = $echo['display']." - ".$alt_name." [".ucwords($sdbip_details['alt_action'])."]";
	$js .= $echo['js'];
}
$sdbip_status = "Active";
if($sdbip_details['is_activated']) {
	$sdbip_status = "Activated";
} elseif($sdbip_details['is_confirmed']) {
	$sdbip_status = "Confirmed";
} elseif($sdbip_details['is_new']) {
	$sdbip_status = "New";
} else {
	$sdbip_status = "Unknown";
}

$echo = $displayObject->getDataField("BOOL_BUTTON", $sdbip_details['mscoa_compliant']);
$mscoa_compliant = $echo['display'];
$js .= $echo['js'];

$mscoa_version = $sdbip_details['mscoa_version'];

?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<h2><?php echo $sdbip_object_name; ?> Details</h2>
			<table class=form width=600px id=tbl_details style='margin: 0 auto;'>
				<tr>
					<th width=150px>Ref:</th>
					<td><?php echo $sdbip_details['ref']; ?></td>
				</tr>
				<tr>
					<th><?php echo $sdbipObject->getObjectName("financialyear"); ?>:</th>
					<td><?php echo $fin_year_name; ?></td>
				</tr>
				<tr>
					<th>Name:</th>
					<td><?php echo $sdbip_details['name']; ?></td>
				</tr>
				<tr>
					<th>Linked to IDP Module:</th>
					<td><?php echo $idp_module; ?></td>
				</tr>
				<tr>
					<th>Linked to alternative <?php echo $sdbip_object_name; ?>:</th>
					<td><?php echo $alt_module; ?></td>
				</tr>
				<tr>
					<th>mSCOA Compliant</th>
					<td><?php echo $mscoa_compliant; ?></td>
				</tr>
				<tr>
					<th>mSCOA Version</th>
					<td><?php echo $mscoa_version; ?></td>
				</tr>
				<tr>
					<th>Created On:</th>
					<td><?php echo date("d F Y H:i", strtotime($sdbip_details['insertdate'])); ?></td>
				</tr>
				<tr>
					<th>Created By:</th>
					<td><?php echo $sdbipObject->getUserName($sdbip_details['insertuser']); ?></td>
				</tr>
				<tr>
					<th>Confirmed On:</th>
					<td><?php echo($is_confirmed && strlen($sdbip_details['confirmdate']) > 0 ? date("d F Y H:i", strtotime($sdbip_details['confirmdate'])) : "N/A"); ?></td>
				</tr>
				<tr>
					<th>Confirmed By:</th>
					<td><?php echo($is_confirmed && strlen($sdbip_details['confirmuser']) > 0 ? $sdbipObject->getUserName($sdbip_details['confirmuser']) : "N/A"); ?></td>
				</tr>
				<tr>
					<th>Activated On:</th>
					<td><?php echo($is_activated && strlen($sdbip_details['activationdate']) > 0 ? date("d F Y H:i", strtotime($sdbip_details['activationdate'])) : "N/A"); ?></td>
				</tr>
				<tr>
					<th>Activated By:</th>
					<td><?php echo($is_activated && strlen($sdbip_details['activationuser']) > 0 ? $sdbipObject->getUserName($sdbip_details['activationuser']) : "N/A"); ?></td>
				</tr>
				<tr>
					<th>Status:</th>
					<td><?php echo $sdbip_status; ?></td>
				</tr>
			</table>
			<h2>Child Object Details</h2>
			<table width=100% id=tbl_child>
				<tr>
					<td class=center>
						<div style='width:350px;margin:0 auto;'>
							<?php
							$fld = SDBP6_PROJECT::OBJECT_TYPE;
							if(in_array($fld, $active_module_sections)) {
								?>
								<div id=div_projects class='tbl-child-info'>
									<div class=inner>
										<h3><?php echo $sdbipObject->getObjectName("PROJECTS"); ?></h3>
										<p><?php echo $summary_details[SDBP6_PROJECT::OBJECT_TYPE]['count']." ".$sdbipObject->getObjectName("PROJECTS"); ?></p>
									</div>
								</div>
								<?php
							}
							$fld = SDBP6_TOPKPI::OBJECT_TYPE;
							if(in_array($fld, $active_module_sections)) {
								?>
								<div id=div_top class='tbl-child-info'>
									<div class=inner>
										<h3><?php echo $sdbipObject->getObjectName("TOPKPIS"); ?></h3>
										<p><?php echo $summary_details[SDBP6_TOPKPI::OBJECT_TYPE]['count']." ".$sdbipObject->getObjectName("KPIS"); ?></p>
									</div>
								</div>
								<?php
							}
							$fld = SDBP6_DEPTKPI::OBJECT_TYPE;
							if(in_array($fld, $active_module_sections)) {
								?>
								<div id=div_dept class='tbl-child-info'>
									<div class=inner>
										<h3><?php echo $sdbipObject->getObjectName("DEPTKPIS"); ?></h3>
										<p><?php echo $summary_details[SDBP6_DEPTKPI::OBJECT_TYPE]['count']." ".$sdbipObject->getObjectName("KPIS"); ?></p>
									</div>
								</div>
							<?php } ?>
						</div>
					</td>
				</tr>
			</table>
			<?php
			echo "
			<div style='width:500px;margin:0 auto; padding:10px; border-radius: 8px;border-style:solid; border-width:1px' class='blue-border'>
				<h2>Activation</h2>
				<p>I, ".$sdbipObject->getUserName().", hereby confirm that this ".$sdbipObject->getObjectName($sdbipObject->getMyObjectName())." is complete and correct and that I have authority to ".strtolower($sdbipObject->getActivityName("activate"))." this ".$sdbipObject->getObjectName($sdbipObject->getMyObjectName()).".</p>
				<p class=center>
					<button id=btn_confirm class='green-button abutton' >".$sdbipObject->getActivityName("activate")." ".$sdbipObject->getObjectName($sdbipObject->getMyObjectName())."</button>
				</p>
			</div>";

			?>
		</td>
	</tr>
</table>
<script type=text/javascript>
	$(function () {
		<?php echo $js; ?>
		$("table.tbl-container").css("margin", "0 auto");
		$("div.tbl-child-info").css({
			"position": "relative",
			"border-radius": "10px",
			"width": "290px",
			"height": "170px",
			"text-align": "center",
			"vertical-align": "middle",
			"display": "table",
			"margin": "20px"
		}).addClass("orange-border");
		$("div.tbl-child-info").find("h3").css("font-family", "Tahoma").css("color", "orange");//.css("font-size","220%").css("line-height","115%");
		//$("div.tbl-child-info").find("p").css("font-size","150%");
		$("div.inner").css({
			"width": "100%",
			"vertical-align": "middle",
			"position": "relative",
			"text-align": "center",
			"display": "table-cell"
		});
		$("#tbl_child").css("border-width", "0px").find("td").css("border-width", "0px");
		$("#btn_confirm").button({
			icons: {primary: "ui-icon-check"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=SDBIP.ACTIVATE", "object_id=<?php echo $sdbip_id; ?>");
			if (r[0] == "ok") {
				AssistHelper.finishedProcessingWithRedirect(r[0], r[1], "manage_view.php");
			} else {
				AssistHelper.finishedProcessing(r[0], r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-green");

	});
</script>