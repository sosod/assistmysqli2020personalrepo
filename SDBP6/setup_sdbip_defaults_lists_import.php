<?php
require_once("inc_header.php");

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "STEP0";

$object_type = $_REQUEST['list'];
$list_id = $object_type;
$list_name = $headingObject->getAListHeading($list_id);

$my_url = "setup_sdbip_defaults_lists_import.php?list=".$list_id;
$my_parent_url = "setup_sdbip_defaults_lists.php?list=".$list_id;
$generate_url = "setup_sdbip_defaults_lists_import_generate.php?list=".$list_id;
$step3_url = "setup_sdbip_defaults_lists_import_step3.php";
$step4_url = "setup_sdbip_defaults_lists_import_step4.php";

$listObject = new SDBP6_LIST($list_id);
$listObject->setSDBIPID($sdbip_id);

$my_object_name = "List item";
$mys_object_name = "List items";

$max_rows_to_import = 500;

//needed by all steps
$headings = $listObject->getHeadingsForImport();

?>
<h2><span id=list_title><?php echo $helper->replaceAllNames($headingObject->getAListHeading($list_id)); ?></span></h2>
<?php


switch($action) {
	case "STEP3":
		//import file for review
		include($step3_url);
		break;
	case "STEP4":
		//accept import
		include($step4_url);
		break;
	case "STEP0":
	default:


		?>
		<h2>Process</h2>
		<table class='tbl-container not-max'>
			<tr>
				<td>
					<form name=frm_import method=post action=<?php echo $my_url; ?> enctype="multipart/form-data">
						<input type=hidden name=action value='STEP3' />
						<input type=hidden name=list value='<?php echo $list_id; ?>' />
						<table width=500px class=form>
							<tr>
								<th width=50px>Step 1:</th>
								<td>Generate template (if not done already).
									<span class=float><button id=btn_step0_generate class=btn-step0><?php echo $sdbipObject->replaceActivityNames("|generate|"); ?></button></span></td>
							</tr>
							<tr>
								<th>Step 2:</th>
								<td>Populate the template with the <?php echo $list_name; ?> list items.</td>
							</tr>
							<tr>
								<th>Step 3:</th>
								<td>Import the template with the <?php echo $list_name; ?> list items.<br />&nbsp;
									<span class=float style='padding-top:10px;'><input type=file name=import_file value='' /> <button id=btn_step0_import class=btn-step0><?php echo $sdbipObject->replaceActivityNames("|import|"); ?></button></span></td>
							</tr>
							<tr>
								<th>Step 4:</th>
								<td>Finalise and Accept the Import. The <?php echo $mys_object_name; ?> will not be imported until the "Accept" button is clicked.</td>
							</tr>

						</table>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<?php $js .= $displayObject->drawPageFooter($helper->getGoBack($my_parent_url)); ?>
				</td>
			</tr>
		</table>
		<h3>Guidelines on using the template</h3>
		<ul>
			<li>The template is in comma-separated values (CSV) format. If using Windows, please ensure that your computer's Regional Settings (also known as Region) has the following settings:
				<ul>
					<li>Decimal: . (period / full stop)</li>
					<li>List separator: , (comma)</li>
					<li>Thousand separator: " " (space)</li>
				</ul>
			</li>
			</li>
			<li>The columns of the template must be in the order given below.</li>
			<li>The first 2 rows will be ignored (assumed to be heading rows).</li>
			<li>It is advisable to import no more than <?php echo $max_rows_to_import; ?> rows at a time, to reduce the risk of the process taking too long and logging you out before it is complete.</li>
		</ul>
		<h4>Template Columns</h4>
		<?php
//ASSIST_HELPER::arrPrint($headings);
		$str = "A";
		?>
		<table class=form>
			<?php
			foreach($headings['rows'] as $fld => $head) {
				$help = $head['help']."  ";
				if($head['required'] == true) {
					$help .= "<span class=red>This field is required.</span>";
				}
				echo "
	<tr>
		<th width=80px>Column ".$str.":</th>
		<td class=b>".$head['name']."</td>
		<td>".$help."</td>
	</tr>
	";
				$str++;
			}
			?>
		</table>


	<?php

} //end switch over which step to do now

?>


<p>&nbsp;</p>
<p>&nbsp;</p>
<script type="text/javascript">
	$(function () {
		var breadcrumb = "<?php echo $helper->getBreadcrumbDivider(); ?>";
		var list_name = $("#list_title").html();
		$("#h1_page_heading").append(" " + breadcrumb + " " + list_name);
		$("#list_title").parent("h2").hide();

//STEP 0 CODE
		$(".btn-step0").button()
			.removeClass("ui-state-default")
			.addClass("ui-button-bold-grey")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
				},
				function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
				}
			);
		$("#btn_step0_import").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			$("form[name=frm_import]").submit();
		});
		$("#btn_step0_generate").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-s"}
		}).click(function (e) {
			e.preventDefault();
			document.location.href = "<?php echo $generate_url; ?>";
		});
	});
</script>