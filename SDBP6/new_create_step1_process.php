<?php
/**
 * @var string $sdbip_object_name
 * @var SDBP6_SDBIP $sdbipObject
 * @var SDBP6_HEADINGS $headingObjet
 */
$no_page_heading = true;
require_once("inc_header.php");


//ASSIST_HELPER::arrPrint(explode("_",$_SERVER['PHP_SELF']));
//
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($_REQUEST);
//echo "<hr />";


$var = $_REQUEST;
unset($var['action']);

echo "<ol>";

echo "<li>Copying ".$sdbip_object_name."...";

$result = $sdbipObject->addObject($var);
if($result[0] == "ok") {
	$new_sdbip_id = $result[3];
	echo " Completed Successfully.  New ".$sdbip_object_name." ID is ".$sdbipObject->getRefTag().$new_sdbip_id.".</li>";

	if($var['copy_setup'] == 1) {
		echo "<li>Copying Setup...<ul>";
		echo "<li>Getting Source $sdbip_object_name...";
		//If not importing from SDBIP then get most recent SDBIP else use the import source SDBIP
		if($var['sdbp6_sdbip'] == "FALSE_LINK" || strlen($var['sdbp6_sdbip']) == 0 || !$sdbipObject->checkIntRefForWholeIntegersOnly($var['sdbp6_sdbip'])) {
			$source_sdbip_id = $sdbipObject->getSourceSDBIPForSetupCopy();
		} else {
			$source_sdbip_id = $var['sdbp6_sdbip'];
		}
		if(!$sdbipObject->checkIntRefForWholeIntegersOnly($source_sdbip_id)) {
			$err = true;
			echo " <span class='red b'>ERROR! Source could not be identified.  Copying stopped.</span></li>";
			$return_message = $sdbip_object_name." was created successfully, however the copy was unsuccessful as a source $sdbip_object_name could not be found.";
		} else {
			$err = false;
			echo " Source identified as ".$sdbipObject->getRefTag().$source_sdbip_id."</li>";
		}
		if(!$err) {
			echo "<li>Copying Organisational Structure...";
			//ORG STRUCTURE
			$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
			$org_ids = $orgObject->copySDBIPSpecificSetup($source_sdbip_id, $new_sdbip_id);
			unset($orgObject);
			echo "done!</li><li>Copying Lists:<ul>";
			//LISTS
			$list_names = $headingObject->getStandardListHeadings(true);
			$listObject = new SDBP6_LIST();
			$owner_ids = array();
			foreach($list_names as $list) {
				echo "<li>".$list['name']."...";
				$listObject->changeListType($list['id']);
				$ids = $listObject->copySDBIPSpecificSetup($source_sdbip_id, $new_sdbip_id);
				if($list['id'] == "owner") {
					$owner_ids = $ids;
				}
				echo " done!</li>";
			}
			unset($listObject);
			echo "</ul></li><li>Copying Owner Access...";
			//ADMINS - need to process LISTS first to get owner_ids
			$adminObject = new SDBP6_SETUP_ADMINS();
			$adminObject->copySDBIPSpecificSetup($org_ids, $owner_ids);
			unset($adminObject);
			echo " done!</li></ul>
 		Setup copy Completed!</li></ol>
 		<p class='b'>Process Completed.</p>";
			$return_message = "Process completed successfully.";
		}
	}

	echo "
	<script type='text/javascript'>
	$(function() {
		parent.helpMyProcessingIsDone('ok','".$return_message."','".$result[2]."');
	});
	</script>";


} else {
	echo " <span class=red>ERROR.  Oops!  Something went wrong.  Please try again.</span></li>";
	echo "
	<script type='text/javascript'>
	$(function() {
		parent.helpMyProcessingIsDone('error','Oops! Something went wrong.  Please try again.','');
	});
	</script>";
}

echo "</ol>";


?>
