<?php

if(!isset($_REQUEST['my_sdbip_id'])) {
	$_REQUEST['my_sdbip_id'] = $_SESSION[$_SESSION['ref']]['REPORT_SDBIP_ID'];
}

require_once("inc_header.php");


$myObject = new SDBP6_REPORT_FIXED();

$report_sections = $myObject->getReportSections();
$reports_per_section = $myObject->getReportsGroupedBySection();

//ASSIST_HELPER::arrPrint($reports_per_section);


?>
<div id="div_container">
	<?php
	foreach($report_sections as $sec_id => $section) {
		$section_code = $section['code'];
		?>
		<h2><?php echo $section['name']; ?></h2>
		<table id="tbl_section_<?php echo $sec_id; ?>" class="tbl-section">
			<thead>
			<tr>
				<th rowspan="1">Ref</th>
				<th width=200px>Report Name</th>
				<th width=350px>Description</th>
				<th rowspan="1">Output Type</th>
				<th rowspan="1">&nbsp;</th>
			</tr>
			</thead>
			<tbody><?php
			if(isset($reports_per_section[$sec_id]) && count($reports_per_section[$sec_id]) > 0) {
				foreach($reports_per_section[$sec_id] as $report_id => $report) {
					echo "
				<tr id='tr_".$report_id."'>
					<td class='b center'>".$section_code.$report_id."</td>
					<td>".$report['user_name']."</td>
					<td>".nl2br($report['user_description'])."</td>
					<td class='center'>".$report['output_type']."</td>
					<td class='center'>
						<button class='btn-generate' report_id='".$report_id."' sdbip_id='".$sdbip_id."'>Generate</button>
					</td>
				</tr>";
				}
			} else {
				echo "<tr><td colspan='5'>No reports available for this section.</td></tr>";
			}
			?></tbody>
		</table>
		<?php
	}
	?>
</div>

<script type="text/javascript">
	$(function () {
		//$("#div_container").css({"border":"2px dashed #fe9900"});
		$("button.btn-generate")
			.button({icons: {primary: "ui-icon-disk"}})
			.removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function () {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
			}, function () {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
			})
			.click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var i = $(this).attr("report_id");
				var s = $(this).attr("sdbip_id");
				document.location.href = 'report_fixed_process.php?my_sdbip_id=' + s + '&i=' + i;
			});
		var tbl_width = 0;
		var th_width = [0, 0, 0, 0, 0];
		var th_count = 0;
		$(".tbl-section").each(function () {
			th_count = 0;
			$(this).find("tr:first").find("th").each(function () {
				if ($(this).width() > th_width[th_count]) {
					th_width[th_count] = $(this).width();
				}
				th_count++;
			});
		});
		$(".tbl-section").each(function () {
			for (t in th_width) {
				$(this).find("tr:first").find("th:eq(" + t + ")").width(th_width[t] + 15);
			}
		});
		/*$(".tbl-section").each(function() {
			if($(this).width()>tbl_width) {
				tbl_width = $(this).width();
			}
		});
		$(".tbl-section").width(tbl_width);*/
	});
</script>