<?php
require_once("../../module/autoloader.php");
$me = new SDBP6_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$page_type = isset($_REQUEST['page']) ? $_REQUEST['page'] : "onscreen";

$table = $_REQUEST['t'];
$name = $_REQUEST['n'];
$type = $_REQUEST['y'];
$sdbip_id = isset($_REQUEST['sdbip_id']) ? $_REQUEST['sdbip_id'] : 0;
$mscoa_version = isset($_REQUEST['mscoa_version']) ? $_REQUEST['mscoa_version'] : 0;
$extra_info = array();
switch($type) {
	case "MULTILIST":
	case "LIST":
		$processObject = new SDBP6_LIST($table);
		$processObject->setSDBIPID($sdbip_id);
		break;
	case "MULTISEGMENT":
	case "SEGMENT":
		$processObject = new SDBP6_SEGMENTS($table, false, $mscoa_version);
		break;
	case "MULTIOBJECT":
	case "OBJECT":
		if(strpos($table, "|") !== false) {
			$lon = explode("|", $table);
			$table = $lon[0];
			$extra_info = $lon[1];
		}
		if($table == "SDBP6_PROJECT" || $table == "SDBP6_TOPKPI") {
			$extra_info = array('name_type' => "ref");
			if(ASSIST_HELPER::checkIntRef($sdbip_id)) {
				$extra_info['parent'] = $sdbip_id;
			}
		}
		$processObject = new $table();
		$processObject->setSDBIPID($sdbip_id);
		break;
}
$valid_items = $processObject->getActiveListItemsFormattedForSelect($extra_info);


if($page_type == "onscreen") {
	ASSIST_HELPER::echoPageHeader();
	echo "<h1>".$name."</h1>";
	if(count($valid_items) > 0) {
		echo "<table>";
		foreach($valid_items as $i => $item) {
			echo "<tr><td>$item</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<p>No valid list items available.</p>";
	}
} else {
	$fdata = "\"".$name."\"\r\n\"\"\r\n";
	foreach($valid_items as $i => $item) {
		$fdata .= "\"".ASSIST_HELPER::decode($item)."\"\r\n";
	}

	//WRITE DATA TO FILE
	$filename = "../../files/".$cmpcode."/".$modref."_".strtolower($table)."_list_".date("Ymd_Hi", $today).".csv";
	$newfilename = strtolower($table)."_list_".date("YmdHis", $today).".csv";
	$file = fopen($filename, "w");
	fwrite($file, $fdata."\n");
	fclose($file);
	//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
	header('Content-type: text/plain');
	header('Content-Disposition: attachment; filename="'.$newfilename.'"');
	readfile($filename);


}
?>