<?php
$result = array("error", "An error occurred while trying to reset the import process.  Please try again.");

$import_ref = $_REQUEST['import_ref'];


$myObject->logUserPageMovement();
$page_object = $myObject->getMyObjectType();


$sql = "SELECT * FROM ".$myObject->getDBRef()."_import_external_log WHERE ref = '$import_ref' AND (status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."";
$row = $myObject->mysql_fetch_one($sql);

//remove temp objects
$sql = "DELETE FROM ".$myObject->getTableName()."_temp WHERE import_ref = '$import_ref'";
$myObject->db_update($sql);

//remove temp list items
$sql = "DELETE FROM ".$myObject->getDBRef()."_list_import_external_temp WHERE import_ref = '$import_ref'";
$myObject->db_update($sql);

//update log record
$myObject->resetExternalImportPhase($import_ref);

$result = array("ok", "Reset completed successfully.");

echo json_encode($result);
?>