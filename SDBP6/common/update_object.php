<?php
/**
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var int $sdbip_id - from inc_header
 * @var string $js - from inc_header & calling page - used to collate javascript/jquery commands to display once at the end of the document load
 * @var SDBP6_DEPTKPI $myObject - from calling page - what object are you working on?  Can also be SDBP6_TOPKPI or SDBP6_PROJECT
 * @var string $page_section - from calling page - where are you working?
 * @var string $page_redirect_path - from calling page - url where to go when you're done
 */
//ASSIST_HELPER::varPrint(get_defined_vars());

/* SET VARIABLES */
//$myObject = new SDBP6_? //set in calling page
//$page_section = NEW / MANAGE / ADMIN //set in calling page
//$sdbip_id = ? //set in header
$object_type = $myObject->getMyObjectType();
$object_id = $_REQUEST['object_id'];
$time_id = $_REQUEST['time_id'];
$page_src = $_REQUEST['page_src']; //Where is the page being called from?  Pop-up/dialog / normal page / front page dashboard - dictates formatting and post-update action

$update_activity_name = $myObject->getActivityName("UPDATE");
$object_name = $myObject->getObjectName($object_type);
$reftag = $myObject->getRefTag();
$time_type = $myObject->getTimeType();
$form_object = $myObject->getRawObject($object_id);
$targettype_id = $form_object[$myObject->getTargetTypeTableField()];
$calc_type_id = $form_object[$myObject->getCalcTypeTableField()]; //echo $calc_type_id;
$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("", $targettype_id);
$has_approve = $myObject->hasApproveFunctionality();
$has_assurance = $myObject->hasAssuranceFunctionality();

if($page_src == "dialog") {
	$page_redirect_path = "dialog";
} elseif($page_src == "dashboard") {
	$page_redirect_path = "dashboard";
} else {
	//use page_redirect_path set in calling page
}

/* Get details to draw page */
//Get Time Period information
$timeObject = new SDBP6_SETUP_TIME();
$time_details = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, $time_type, $time_id);
//Get object summary information
$summary_form = $displayObject->getObjectForm($object_type, $myObject, $object_id, null, null, 0, "SUMMARY.VIEW", $page_redirect_path, true, 0, $form_object);
//Get update form information
//getUpdateForm($page_action,$form_object_type,$formObject,$object_id,$time_id,$page_redirect_path)
$update_form = $displayObject->getUpdateForm("UPDATE", $object_type, $myObject, $sdbip_id, $object_id, $time_id, $targettype_id, $page_redirect_path, $calc_type_id);
//get Complete results table (same as View page)
//if($page_src!="dialog") {

$object_details = $myObject->getRawObject($object_id);//array('results'=>$myObject->getRawResults($object_id));
$headingObject = new SDBP6_HEADINGS();
$child_headings = $headingObject->getMainObjectHeadings($myObject->getMyChildObjectType(), "DETAILS", $page_section);
$time_periods = array();//getChildObjectForm will populate this list if empty array is sent
$current_results = $displayObject->getChildObjectForm("VIEW", $object_details, $child_headings, $time_periods, $sdbip_id, "frm_update_".$object_type."_current_results", $time_type, $page_section, $targetTypeObject->getSymbolsForFormatting(), $myObject);
//} else {
//	$current_results = array('display'=>"",'js'=>"");
//}
if($has_approve) {
	//get approve history
	$approve_history = $myObject->getApproveHistory($object_id, $time_id);
} else {
	$approve_history = array();
}
if($has_assurance) {
	//get assurance history
	$assurance_history = $myObject->getAssuranceHistory($object_id, $time_id);
} else {
	$assurance_history = array();
}
?>
<div id=update_form_div>
	<h2><?php echo $update_activity_name." ".$object_name." ".$reftag.$object_id." for ".$time_details['name']; ?></h2>

	<div id=div_update_form class=update-div>
		<h3>Update form</h3>
		<?php
		echo $update_form['display'];
		$js .= $update_form['js'];
		?>
	</div>

	<div id=div_object_summary class=update-div>
		<h3>Summary Details</h3>
		<?php
		echo $summary_form['display'];
		$js .= $summary_form['js'];

		$js .= $displayObject->drawPageFooter($myObject->getGoBack());

		?>
	</div>

	<div id=div_full_results class='update-div results-div' style='clear:both'>
		<h2>Current Results</h2>
		<?php
		echo $current_results['display'];
		//echo str_replace(chr(10),"<br />",$current_results['js']);
		$js .= $current_results['js'];

		?>
	</div>

	<div id=div_approve_history class="update-div history-div left-div">
		<?php
		if($has_approve) {
			?>
			<h2><?php echo $myObject->getActivityName("APPROVE"); ?> Review History</h2>
			<table id=tbl_review_history>
				<tr><?php
					foreach($approve_history['head'] as $fld => $name) {
						echo "<th>".$name."</th>";
					}
					?></tr>
				<?php
				if(count($approve_history['rows']) > 0) {
					foreach($approve_history['rows'] as $i => $row) {
						echo "<tr>";
						foreach($approve_history['head'] as $fld => $name) {
							$val = $row[$fld];
							echo "<td>".$val."</td>";
						}
						echo "</tr>";
					}
				} else {
					echo "<tr><td colspan=".count($approve_history['head']).">No reviews available</td></tr>";
				}
				?>
			</table>
			<?php
		}
		?>
	</div>

	<div id=div_assurance_history class="update-div history-div right-div">
		<?php
		if($has_assurance) {
			?>
			<h2><?php echo $myObject->getActivityName("ASSURANCE"); ?> Review History</h2>
			<table id=tbl_assurance_history>
				<tr><?php
					foreach($assurance_history['head'] as $fld => $name) {
						echo "<th>".$name."</th>";
					}
					?></tr>
				<?php
				if(count($assurance_history['rows']) > 0) {
					foreach($assurance_history['rows'] as $i => $row) {
						echo "<tr>";
						foreach($assurance_history['head'] as $fld => $name) {
							$val = $row[$fld];
							echo "<td>".$val."</td>";
						}
						echo "</tr>";
					}
				} else {
					echo "<tr><td colspan=".count($assurance_history['head']).">No reviews available</td></tr>";
				}
				?>
			</table>
			<?php
		}
		$js .= $displayObject->drawPageFooter($myObject->getGoBack(), $myObject->getMyLogTable(), array("object_id" => $object_id));
		?>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		/* DEV CODE */
		//$("#update_form_div").css({"border":"1px dashed #cc0001","padding":"3px"});
		//code needed after development to keep floated divs inline with partners
		$("#update_form_div").find("div.update-div").css({
			'border': '1px solid #ffffff',
			'background-color': '#ffffff'
		});

		/* FORM CODE */
		<?php echo $js; ?>




		/* PAGE CODE */
		$(window).resize(function () {
			var windowSize = AssistHelper.getWindowSize();
			var window_width = windowSize['width'];
			//1150px tested as smallest width capable of managing approve form & update form side-by-side
			var $review_form = $("#div_update_form");
			if (window_width < 1150) {
				$("div.update-div").width("");
				//$("#div_update_form").after($review_form);
				$review_form.removeClass("float");
			} else {
				//$("#div_update_form").before($review_form);
				$review_form.addClass("float");
				var half_width = (window_width - 50) * 0.5;
				$("div.update-div").find("table:first").each(function () {
					// var w = $(this).width()+30;
					if ($(this).parents("div:first").hasClass("results-div")) {
						//results-div = full size so do nothing
					} else {
						$(this).parents("div:first").width(half_width);
						//$(this).parents("div:first").width(w).css("padding-left","10px").css("padding-bottom","10px");
					}
				});
			}

		});
		$(window).trigger("resize");




		<?php
		if($page_src == "dialog") {
		?>
		window.parent.$('#dlg_update').dialog('open');
		<?php
		}
		?>

		var focus_me = true;
		$("input:text, textarea").each(function () {
			if (focus_me) {
				$(this).focus();
				focus_me = false;
			}
		});


	});

	function saveFinished(icon, message) {
		<?php
		if($page_src == "dashboard") {
		?>
		//alert(message);
		if (icon == "ok") {
			AssistHelper.finishedProcessingWithFollowonFunction(icon, message, goHome);
		} else {
			AssistHelper.finishedProcessing(icon, message);
		}
	}
	<?php
	} else {
	?>
	//alert(message);
	if (icon == "ok") {
		AssistHelper.finishedProcessingWithRedirect(icon, message, "<?php echo $page_redirect_path; ?>");
	} else {
		AssistHelper.finishedProcessing(icon, message);
	}
	}
	<?php
	}
	?>
	function goHome() {
		AssistHelper.processing();
		$('#backHome', window.parent.header.document).click();
	}
</script>


<?php

//ASSIST_HELPER::arrPrint($_REQUEST);
//echo $myObject->getMyObjectType();

?>
