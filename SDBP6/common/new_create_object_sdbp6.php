<?php
$is_import_from_sdbip_page = true;
/** @var SDBP6_SDBIP $sdbipObject - from parent page */
$page_object_name = $sdbipObject->replaceObjectNames("|".$page_object."|");

$js = "";

//1 Check that SDBIP is linked to idp module
require_once("inc_new_statuscheck.php");
/** @var ARRAY $sdbip_details - from parent page */
$sdbip_id = $sdbip_details['id'];
$mscoa_version_code = $sdbip_details['mscoa_version'];
$myObject->setSDBIPID($sdbip_id);


//ASSIST_HELPER::arrPrint($sdbip_details);

$import_ref = $sdbipObject->getExternalImportRef($sdbip_details, $page_object); //echo $import_ref;
$display_message = $sdbipObject->getStatusMessageForExternalImport($import_ref, $page_object);
$buttons = $sdbipObject->getExternalImportButtonAvailability($page_object);

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "DEFAULT";
$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module_settings']['modref'];
$alt_module_name = $sdbip_details['alt_module_name'];

$external_class = "SDBP6_".$old_modloc;
$extObject = new $external_class($old_modref, $page_object, $myObject->getCmpCode());

//$all_allowed_map_matched = $extObject->getAllowedMapMatches();
$headings_for_map = $headingObject->getHeadingsForInternalMapping($page_object, true, true);
//ASSIST_HELPER::arrPrint($headings_for_map);
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//echo "<P>".$external_class." :: ".$old_modloc." :: ".$old_modref."</P>";
//ASSIST_HELPER::arrPrint($headings_for_map);
//echo "<hr />";

/*FIRST CHECK IF FIELD MAP HAS BEEN DONE - ONLY IF REQUIRED - not applicable to SDBP6 imports*/
if($old_modloc != SDBP6::MODULE_CODE) {
	$field_map = $myObject->getFieldMap($old_modloc, $old_modref, $page_object);
} else {
//	$field_map = true;
	$field_map = $extObject->getDefaultFieldMap($headings_for_map);
}
//ASSIST_HELPER::arrPrint($field_map); echo ":".$field_map.":";

/*If no field map then display mapping form, else display next step with edit form at bottom of page*/
$show_other_sections = false;
//$default_field_map = $extObject->getDefaultFieldMap();
//
//$old_headings = $extObject->getHeadingsForMapping(($field_map===true?$default_field_map:$field_map),true,true);
$old_headings = $headings_for_map;
//ASSIST_HELPER::arrPrint($default_field_map);
//ASSIST_HELPER::arrPrint($old_headings);

if($field_map === false || $action == "EDIT_MAP") {
	if($action == "EDIT_MAP") {
		$active_map = $field_map;
	} else {
		$active_map = $default_field_map;
	} //ASSIST_HELPER::arrPrint($active_map);
	echo "
	<div id=div_map class=tbl-ok width=50% style='margin:0 auto;width:50%'>
		<h1>Field Map</h1>
		<p>Before you can begin with the import, please confirm the mapping of fields between the modules:</p>
		<form name=frm_map>
			<input type=hidden name=object_type value='$page_object' />
			<input type=hidden name=modref value='$old_modref' />
			<input type=hidden name=modloc value='$old_modloc' />
			<input type=hidden name=sdbip_id value='$sdbip_id' />
			<table id=tbl_map class=format_me style='margin:0 auto'>
				<tr>
					<th>Source Module</th>
					<th>This Module</th>
				</tr>";
	foreach($active_map as $old_field => $new_field) {
		$fld = "0";
		$old_type = $old_headings[$old_field]['type'];
		if(!isset($all_allowed_map_matched[$old_type])) {
			$old_type = "OBJECT";
		}
		$allowed_map_matches = $all_allowed_map_matched[$old_type];
		echo "
				<tr>
					<td >".str_replace("</span><br />", ":</span><br />", $old_headings[$old_field]['name'])."</td>
					<td><select id='".$old_field."' name=".$old_field." class='field_mapping_select'><option value='0' canmap='yes' allow_multiple_map='yes'".($new_field == $fld ? "selected" : "").">Do not use</option>";
		foreach($headings_for_map as $fld => $f) {
			$name = $f['name'];
			$new_type = $f['type'];
			echo "<option value='".$fld."' ".($new_field == $fld ? "selected" : "")."  allow_multiple_map='no' ".((!isset($allowed_map_matches[$new_type]) || $allowed_map_matches[$new_type] !== true) ? "disabled='disabled' canmap='no'" : "canmap='yes'").">".$name."</option>";
		}
		echo "</select></td>
				</tr>";
	}
	echo "
			</table>
			<p class=center><button id=btn_save_map>Save Mapping</button>".($action == "EDIT_MAP" ? "&nbsp;<button id='btn_cancel'>Cancel</button>" : "")."</p>
		</form>";

	echo "
	</div>
	<div id=div_map_warning style='width:50%; margin:0 auto'>";
	ASSIST_HELPER::displayResult(array("info", "Please note: 
<br />- The text inside the square brackets [] indicates the field type (e.g. Date, Text etc).  
<br />- Certain field types cannot be mapped (e.g. a Yes/No field cannot be mapped to a Date field) while others might result in data loss (e.g. mapping an Unlimited Text field to Medium Text field will result in excess text being cut off).
<br />- mSCOA Segment field will not be converted, the system will assume that the same Segment has been mapped. 
<br />- If Source Module Lists are mapped to Local Module Lists, any missing list items will be copied from the Source Module to the Local Module.
<br />- If Source Module Lists are mapped to mSCOA Segments or Local Module Objects, the items will not be copied.  The items must exist in the mSCOA Library / Local Module Object List before importing. "));
	echo "</div>";

	$js = "
$('select.field_mapping_select').change(function() {
	var in_use = new Array();
	$('select.field_mapping_select').each(function() {
		in_use.push($(this).val());
	});
	$('select.field_mapping_select').each(function() {
		var my_val = $(this).val(); 
		$(this).children('option').each(function() {
			if($(this).attr('allow_multiple_map')=='no' && $.inArray($(this).val(),in_use)>=0 && $(this).val()!=my_val && ($(this).attr('canmap')!='no')) {
				$(this).attr('disabled','disabled');
			} else if($(this).attr('canmap')!='no') {
				$(this).removeAttr('disabled');
			}
		});
	});
});
$('select.field_mapping_select:first').trigger('change');
$(window).resize(function() {
	var scr = AssistHelper.getWindowSize();
	var dw = scr.width*0.5;
	var tw = $('#tbl_map').width();
	if(dw<(tw+20)) {
		$('#div_map').width(tw+20);
		$('#div_map_warning').width(tw);
	} else {
		$('#div_map').width(dw);
		$('#div_map_warning').width(dw-20);
	}
});
$(window).trigger('resize');
";

//die(ASSIST_HELPER::displayResult(array("error","dev test end here ".__LINE__)));


	$show_field_map_edit_form = false;

} else {
	$show_other_sections = true;
	$show_field_map_edit_form = true;


	//2. Check if link is CONVERT or IMPORT
	$is_convert = false;
	$is_import = false;
	$alt_module_action = $sdbip_details['alt_action'];
	if($alt_module_action == "CONVERT") {
		$is_convert = true;
	} else {
		$is_import = true;
	}

	//3. if IMPORT then .... TO BE PROGRAMMED
	if($is_import) {
		$show_field_map_edit_form = true; //temp until programmed
		$object_count = $sdbip_details['summary_details'][$page_object];

		//check if phase 1 report is available
		if(isset($buttons[1]) && $buttons[1] == false) {
			$button1_message = "<a href='".$page_link."_step1_report.php?ref=".$import_ref."'>View Results</a>";
		} else {
			$button1_message = "";
		}
		//check for number of items to import if phase 2 is ready
		if(isset($buttons[2]) && $buttons[2] == true) {
			$sql = "SELECT count(import_id) as c FROM ".$myObject->getTableName()."_temp WHERE import_ref = '$import_ref'";
			$count = $myObject->mysql_fetch_one_fld($sql, "c");
			$button2_message = $count." ".$myObject->getObjectName($page_object)."(s) ready to ".$myObject->getActivityName("import");
		} else {
			$button2_message = "";
		}

		echo "
			<div class=tbl-ok width=50% style='margin:0 auto;width:50%'>
				<h1>Import</h1>
				<p class='center'>".$display_message."</p>
				<table width='100%' id='tbl_import_buttons'>
					<tr>
						<td class='center' width='33%'><button id=btn_import1>Phase 1. Prepare Import</button></td>
						<td class='center' width='34%'><button id=btn_import2>Phase 2. Accept Import</button></td>
						<td class='center' width='33%'><button id=btn_import3>Phase 3. Finalise Import</button></td>
					</tr><tr>
						<td class='center'>".$button1_message."</td>
						<td class='center i'>".$button2_message."</td>
						<td class='center'></td>
					</tr>
					<tr><td class='right' colspan='3'>&nbsp;<button id='btn_reset' >Reset Import Process</button></td> </tr>
				</table>
			</div>";


	} else {
		//ELSE give error message
		$show_field_map_edit_form = false;
		echo "
		<div width=50% style='margin:0 auto;width:50%'>
		";
		ASSIST_HELPER::displayResult(array("error", "Oops, this section is still under development.  Please contact Action iT at <a href=mailto:helpdesk@actionassist.co.za>helpdesk@actionassist.co.za</a> with error code: SDBP6/NewCreateProjectsSDBIP/Unspecified."));
		echo "
		</div>";

	}


	if($show_field_map_edit_form === true) {
		$active_map = $field_map;

//			<button class=float id=btn_edit style='margin-top: 10px'>Edit</button>
		echo "
		<div class=tbl-info  style='margin:25px auto; width:50%'>
			<h1>Field Map </h1>
			<p>As this $sdbip_object_name is linked to another $sdbip_object_name within the same module, all fields automatically map to themselves.  No Field Map required.</p>
			<!-- <table id=tbl_map class=format_me style='margin:0 auto'>
				<tr>
					<th>Source Module</th>
					<th>This Module</th>
				</tr>";
		foreach($active_map as $old_field => $new_field) {
			$fld = "0";
			echo "
				<tr>
					<td >".str_replace("</span><br />", ":</span><br />", $old_headings[$old_field]['name'])."</td>
					<td>".(isset($headings_for_map[$new_field]) ? $headings_for_map[$new_field]['name'] : "<span class=red>Do not use</span>")."</td>
				</tr>";
		}
		echo "
			</table> -->
		</div>";
	}


} //end check for field_map


//ASSIST_HELPER::arrPrint($sdbip_details);


if(strpos($_SERVER['HTTP_HOST'], "localhost") === false) {
	$style = "border:0px solid #FFFFFF; width:0px; height:0px";
} else {
	$style = "border:2px dashed #009900; width:750px; height: 750px";
}

echo "<iframe id='ifr_import' style='".$style."'></iframe>";
?>
<div id=dlg_confirm></div>

<script type=text/javascript>
	$(function () {
		var windowSize = AssistHelper.getWindowSize();
		var dlgWidth = windowSize['width'] * 0.75;
		var dlgHeight = windowSize['height'] * 0.9;
		var ifrWidth = dlgWidth - 30;
		var ifrHeight = dlgHeight - 25;
//DIALOG SETTINGS
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});


		$("#tbl_import_buttons, #tbl_import_buttons td").css("border", "0px solid #FFFFFF");

//IMPORT BUTTON SETTINGS
		$("#btn_import1").button({}).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF")
			.hover(function () {
				$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var url = "<?php echo $page_link; ?>_step1.php?action=PREP&sdbip_id=<?php echo $sdbip_id; ?>";
			// AssistHelper.finishedProcessing("ok",url);
			// document.location.href = url;
			$("#ifr_import").prop("src", url);
		});
		<?php
		if(isset($buttons[1]) && $buttons[1] == false) {
			echo "
				$(\"#btn_import1\").removeClass(\"ui-button-bold-green\").addClass(\"ui-button-bold-grey\").prop(\"disabled\",\"disabled\").css(\"cursor\",\"not-allowed\");
				";
		}
		?>
		$("#btn_import2").button({}).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF")
			.hover(function () {
				$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
			}).click(function (e) {
			e.preventDefault();
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>Phase 2 will import the <?php echo $page_object_name; ?> into the <?php echo $sdbip_object_name; ?>.  You will no longer be able to reset the import.  Do you wish to continue?</p><label id=lbl_hide></label>');
			var buttons = [
				{
					text: "Confirm & Continue", click: function () {
						$(this).dialog("close");
						AssistHelper.processing();
						var url = "<?php echo $page_link; ?>_step2.php?import_ref=<?php echo $import_ref; ?>&sdbip_id=<?php echo $sdbip_id; ?>";
						$("#ifr_import").prop("src", url);
					}, class: 'ui-state-ok'
				},
				{
					text: "Cancel", click: function () {
						$(this).dialog("close");
					}, class: 'ui-state-error'
				}
			];
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		});
		<?php
		if(isset($buttons[2]) && $buttons[2] == false) {
			echo "
				$(\"#btn_import2\").removeClass(\"ui-button-bold-green\").addClass(\"ui-button-bold-grey\").prop(\"disabled\",\"disabled\").css(\"cursor\",\"not-allowed\");
				";
		}
		?>

		$("#btn_import3").button({}).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF")
			.hover(function () {
				$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var url = "<?php echo $page_link; ?>_step3.php?import_ref=<?php echo $import_ref; ?>&sdbip_id=<?php echo $sdbip_id; ?>";
			document.location.href = url;
		});
		<?php
		if(isset($buttons[3]) && $buttons[3] == false) {
			echo "
				$(\"#btn_import3\").removeClass(\"ui-button-bold-green\").addClass(\"ui-button-bold-grey\").prop(\"disabled\",\"disabled\").css(\"cursor\",\"not-allowed\");
				";
		}
		?>

		$("#btn_reset").button({
			icons: {primary: "ui-icon-trash"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-red")
			.hover(function () {
				$(this).removeClass("ui-button-minor-red").addClass("ui-button-minor-orange");
			}, function () {
				$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-red");
			}).click(function (e) {
			e.preventDefault();
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>You have asked to reset the Import Process.  This will undo the import preparations done so far.  Do you wish to continue?</p><label id=lbl_hide></label>');
			var buttons = [
				{
					text: "Confirm & Continue", click: function () {
						$(this).dialog("close");
						AssistHelper.processing();
						var r = AssistHelper.doAjax("<?php echo $page_link; ?>_reset.php", "import_ref=<?php echo $import_ref; ?>");
						AssistHelper.finishedProcessingWithRedirect(r[0], r[1], document.location.href);
					}, class: 'ui-state-ok'
				},
				{
					text: "Cancel", click: function () {
						$(this).dialog("close");
					}, class: 'ui-state-error'
				}
			];
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		});
		<?php
		if(isset($buttons['reset']) && $buttons['reset'] == false) {
			echo "
				$(\"#btn_reset\").removeClass(\"ui-button-minor-red\").addClass(\"ui-button-minor-grey\").prop(\"disabled\",\"disabled\").css(\"cursor\",\"not-allowed\");
				";
		}
		?>

//MAPPING
		var redirect_url = "<?php echo $page; ?>.php";
		$("#btn_save_map").button({
			icons: {primary: "ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF")
			.hover(function () {
				$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
			}, function () {
				$(this).addClass("ui-button-bold-green").removeClass("ui-button-bold-orange");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_map]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=<?php echo $page_object; ?>.saveExternalMap", dta);
			AssistHelper.finishedProcessingWithRedirect(result[0], result[1], redirect_url);
		});

		$("#btn_edit").button({
			icons: {primary: "ui-icon-pencil"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function () {
				$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
			}, function () {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = '<?php echo $page_link; ?>.php?action=EDIT_MAP';
		});
		<?php
		if(isset($buttons['map']) && $buttons['map'] == false) {
			echo "
				$(\"#btn_edit\").prop(\"disabled\",\"disabled\").css(\"cursor\",\"not-allowed\").hide();
				";
		}
		?>

		$("#btn_cancel").button({
			icons: {primary: "ui-icon-closethick"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function () {
				$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
			}, function () {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = '<?php echo $page_link; ?>.php';
		});



		<?php echo $js; ?>


		$("table.format_me").find("th").css("background-color", "#777777");
	});


	function finishDialog(icon, msg) {
		$(function () {
			if (icon == "info") {
				AssistHelper.finishedProcessing(icon, msg);
			} else {
				AssistHelper.finishedProcessingWithRedirect(icon, msg, document.location.href);
			}
		});
	}


</script>

