<?php
$import_ref = $_REQUEST['import_ref'];
$sdbip_id = $_REQUEST['sdbip_id'];
$myObject->setSDBIPID($sdbip_id);
$is_import_from_sdbip_page = true;
$page_query_string = $_SERVER['QUERY_STRING'];

//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
$mscoa_version_code = $sdbip_details['mscoa_version'];

//use echo variable to output to screen for development purposes - using variable to make it easier to turn off for production
$echo = "";

//get external module settings
$old_modloc = $sdbip_details['idp_module_settings']['modlocation'];
$old_modref = $sdbip_details['idp_module'];
$linked_idp = $sdbip_details['idp_id'];
$old_dbref = strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);

//create objects
$my_modref = $myObject->getModRef();
$field_prefix = $myObject->getTableField();
$ref_field = $field_prefix."_ref";
$name_field = $myObject->getNameFieldName();
$id_field = $myObject->getIDFieldName();
$status_field = $myObject->getStatusFieldName();
$ref_tag = $myObject->getRefTag();
$result_prefix = $myObject->getResultsTableField();
$target_fields = array();
$description_field = $result_prefix."_target_description";
if($page_object == SDBP6_TOPKPI::OBJECT_TYPE) {
	$original_field = $myObject->getResultsOriginalFieldName();
	$target_fields[] = $original_field;
}
$target_fields[] = $description_field;
$result_object = $myObject->getMyChildObjectType();
$records = $myObject->getImportedRecords();
$results_records = $myObject->getImportedResultsRecords(array_keys($records));

$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object] = array();

//get mapping
$field_map = $myObject->getFieldMap($old_modloc, $old_modref, $page_object);    //array('old_field'=>"new_field")
$all_headings = $headingObject->getMainObjectHeadings($page_object, "DETAILS", "", "", true);
$all_headings = $all_headings['rows'];
$new_field_settings = $headingObject->getHeadingsForExternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
$id_heading = $headingObject->getAHeadingRecordsByField($id_field, $page_object);
$target_heading = array();
foreach($target_fields as $fld) {
	$target_heading[$fld] = $headingObject->getAHeadingRecordsByField($fld, $result_object);
}
$fixed_headings = array($ref_field, $name_field);

//ASSIST_HELPER::arrPrint($all_headings);

//time_periods
$timeObject = new SDBP6_SETUP_TIME();
$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $myObject->getTimeType());
//ASSIST_HELPER::arrPrint($time_periods);
$lists = array();

$headings = array();
foreach($fixed_headings as $fld) {
	$headings[$fld] = $all_headings[$fld];
}
foreach($new_field_settings as $fld => $head) {
	if(!in_array($fld, $fixed_headings) && !in_array($fld, $field_map)) {
		$headings[$fld] = $all_headings[$fld];
		$headings[$fld]['list'] = $head['list'];
		if($head['type'] == "LIST" || $head['type'] == "MULTILIST") {
			$listObject = new SDBP6_LIST($head['list']);
			$listObject->setSDBIPID($sdbip_id);
			$lists[$head['list']] = $listObject->getActiveListItemsFormattedForSelect();
			unset($listObject);
		} elseif($head['type'] == "OBJECT" || $head['type'] == "MULTIOBJECT") {
			$list = $head['list'];
			$list_name = $list;
			if(strpos($list, "|") !== false) {
				$lon = explode("|", $list);
				$list = $lon[0];
				$extra_info = $lon[1];
			} else {
				$extra_info = "";
			}
			$listObject = new $list();
			$listObject->setSDBIPID($sdbip_id);
			$lists[$list_name] = $listObject->getActiveListItemsFormattedForSelect($extra_info);
			unset($listObject);
		} elseif($head['type'] == "SEGMENT" || $head['type'] == "MULTISEGMENT") {
			$listObject = new SDBP6_SEGMENTS($head['list']);
			$lists[$head['list']] = $listObject->getActiveListItemsFormattedForSelect(array('version' => $mscoa_version_code));
			unset($listObject);
		}
	}
}
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($lists);
//echo "<hr />";
//die();
ASSIST_HELPER::displayResult(array("info", "Capture the missing information.  Assist will automatically save as you go.<br />All required fields must be captured before this ".$sdbip_object_name." can be confirmed and activated."));
?>
	<table id="tbl_records">
		<thead>
		<tr><?php
			if(count($target_fields) > 1) {
				$rowspan = 2;
			} else {
				$rowspan = 1;
			}
			echo "<th rowspan='".$rowspan."'>".$id_heading['name']."</th>";
			foreach($headings as $fld => $head) {
				echo "<th  rowspan='".$rowspan."'>".$head['name'].(!in_array($fld, $field_map) && $head['required'] ? "*" : "")."</th>";
			}
			foreach($time_periods as $time_id => $time) {
				echo "<th colspan='".count($target_fields)."'>".$time.($rowspan == 1 && isset($target_heading[$description_field]) ? " - ".$target_heading[$description_field]['name'] : "")."</th>";
			}
			echo "<th rowspan='".$rowspan."'>Status</th>"
			?></tr>
		<?php
		if($rowspan == 2) {
			echo "<tr>";
			foreach($time_periods as $time_id => $time) {
				foreach($target_heading as $fld => $head) {
					echo "<th>".$head['name']."</th>";
				}
			}
			echo "</tr>";
		}
		?>
		</thead>
		<tbody>
		<?php
		foreach($records as $row) {
			$i = $row[$id_field];
			$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i] = array('status' => $row[$status_field], 'fields' => array());
			echo "<tr><td class='b'>".$ref_tag.$row[$id_field]."</td>";
			foreach($headings as $fld => $head) {
				echo "<td id='td_".$fld."_".$i."'>";
				if(!in_array($fld, $field_map)) {
					$type = $head['type'];
					if($head['required']) {
						$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i]['fields'][$fld] = array('type' => $type, 'value' => $row[$fld]);
					}
					switch($type) {
						case "DATE":
							$val = $row[$fld] == "0000-00-00" ? "" : date("d-M-Y", strtotime($row[$fld]));
							if($head['required']) {
								$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i]['fields'][$fld]['value'] = $val;
							}
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i, 'options' => array('buttonImage' => false), 'class' => "auto_save"), $val);
							break;
						case "MULTILIST":
							$row[$fld] = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $row[$fld]);
						case "LIST":
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i, 'options' => $lists[$head['list']], 'class' => "auto_save", 'req' => true, 'unspecified' => false), $row[$fld]);
							break;
						case "MULTIOBJECT":
							$row[$fld] = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $row[$fld]);
						case "OBJECT":
							$js .= $displayObject->drawFormField("LIST", array('id' => $fld."_".$i, 'options' => $lists[$head['list']], 'class' => "auto_save", 'req' => true, 'unspecified' => false), $row[$fld]);
							break;
						case "TEXT":
						default:
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i, 'class' => "auto_save"), $row[$fld]);
					}
				} else {
					echo $row[$fld];
				}
				echo "</td>";
			}
			$fld = $description_field;
			foreach($time_periods as $time_id => $time) {
				foreach($target_heading as $fld => $head) {
					$type = $head['h_type'];
					echo "<td id='td_".$fld."_".$i."_".$time_id."'>";
					$v = ASSIST_HELPER::decode($results_records[$i][$time_id][$fld]);
					switch($type) {
						case "NUM":
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i."_".$time_id,
								'class' => "result_auto_save",
								'warn' => false), $v);
							break;
						case "TEXT":
						default:
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i."_".$time_id,
								'class' => "result_auto_save"), $v);
							break;
					}
					echo "</td>";
				}
			}

			echo "<td class='td_status'>";
			if(($row[$status_field] & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) {
				ASSIST_HELPER::displayResult(array("info", "In Progress"));
			} else {
				ASSIST_HELPER::displayResult(array("ok", "Complete"));
			}
			echo "</td></tr>";
		}
		?>
		</tbody>
	</table>

	<script type="text/javascript">
		$(function () {
			<?php echo $js; ?>

			$(".datepicker").datepicker("option", "onSelect", function (my_val) {
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "DATE", $(this));
			});

			$("input.auto_save").blur(function () {
				if ($(this).hasClass("datepicker")) {
					//console.log($(this).datepicker("getDate"));
				} else {
					var my_val = $(this).val();
					var i = $(this).prop("id");
					saveUpdate(i, my_val, "TEXT", $(this));
				}
			});
			$("select.auto_save").change(function () {
				var my_val = $(this).val();
				if (my_val != "X") {
					var i = $(this).prop("id");
					saveUpdate(i, my_val, "LIST", $(this));
				}
			});
			$("textarea.auto_save").change(function () {
				var my_val = $(this).text();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "TEXT", $(this));
			});
			$("textarea.result_auto_save").change(function () {
				var my_val = $(this).val();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "RESULT", $(this));
			});
			$("input.result_auto_save").change(function () {
				var my_val = $(this).val();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "RESULT", $(this));
			});

			function saveUpdate(i, my_val, typ, $me) {
				var dta = "settings=" + i + "&my_val=" + my_val + "&type=" + typ;
				var r = AssistHelper.doAjax("inc_controller.php?action=<?php echo $page_object.".importPhase3"; ?>", dta);
				if (r[0] == "ok") {
					$("#td_" + i).addClass("ui-state-ok");
					$("#" + i).addClass("ui-state-ok").before(r[1]);
				}
				$me.parent().parent().find("td.td_status").html(r[2]);
			}

		});
	</script>
<?php
//ASSIST_HELPER::arrPrint($_SESSION[$myObject->getModRef()]['IMPORT']);
?>