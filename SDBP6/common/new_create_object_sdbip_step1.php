<?php
//JC NOTE: doing this old school to handle multiple modrefs at once and to assist with displaying progress onscreen
$new_list_item_count = 0;
function processListItems($new_type, $old_value, $new_list_table, $original_id = 0, $old_field = "") {
	global $new_lists;
	global $myObject;
	global $import_ref;
	global $old_modref;

	$old_value = trim($old_value);
	$old_value = str_replace("\t", ' ', $old_value); // remove tabs
	$old_value = str_replace("\n", '', $old_value); // remove new lines
	$old_value = str_replace("\r", '', $old_value); // remove carriage returns
	$old_value = str_replace(chr(10), '', $old_value); // remove ascii enter key

	//convert input value to standard format for comparison
	$compare_value = $myObject->formatTextForComparison($old_value);

	if(substr($old_field, -6) == "_wards") {
//		echo "<hr /><p>".$new_type.":".$new_list_table.":".$old_value.":".$compare_value."<br />";
		if(!isset($list_values[$compare_value]) && strtoupper($old_value) == "ALL") {
			$old_value = "Whole of the Municipality";
			$compare_value = $myObject->formatTextForComparison($old_value);
		}
	} elseif(substr($old_field, -11) == "_targettype") {
		if(!isset($list_values[$compare_value]) && strtoupper($old_value) == "PERCENTAGE") {
			$old_value = "Percent";
			$compare_value = $myObject->formatTextForComparison($old_value);
		}
	}


	//get list items specific to this list
	$list_values = $new_lists[$new_list_table];
	//check if input value exists - if yes, use that value, if no, then add new value
	if(isset($list_values[$compare_value])) {
		if(is_array($list_values[$compare_value])) {
			$new_value = $list_values[$compare_value];
		} else {
			$new_value = array("EXISTING", $list_values[$compare_value]);
		}
//		if($new_list_table=="strategic_objective") {echo "FOUND</p>";}
	} else {
		if($new_type != "OBJECT" && $new_type != "SEGMENT" && $new_type != "MULTIOBJECT" && $new_type != "MULTISEGMENT") {
			//add new item here
			$sql = "INSERT INTO ".$myObject->getDBRef()."_list_import_external_temp 
					SET value = '$old_value'
					, import_ref = '$import_ref'
					, local_list = '$new_list_table'
					, src_modref = '$old_modref'
					, src_id = '$original_id'
					, src_list = '$old_field'
					, status = ".SDBP6::ACTIVE."
					, insertuser = '".$myObject->getUserID()."'
					, insertdate = now()";
			$new_value = array("NEW", $myObject->db_insert($sql));
			$new_lists[$new_list_table][$compare_value] = $new_value;
		} else {
			$new_value = array("NOT_FOUND", 0);
		}
	}
	return $new_value;
}

$dev_new_list_items = array();

$is_import_from_sdbip_page = true;
$page_query_string = $_SERVER['QUERY_STRING'];
$page_action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "SAVE";

//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
$sdbip_id = $sdbip_details['id'];
//record start of phase 1 of import
$import_ref = $sdbipObject->startExternalImportPhase1($sdbip_details, $page_object);
//$import_ref = $sdbipObject->getExternalImportRef($sdbip_details,$page_object);

//use echo variable to output to screen for development purposes - using variable to make it easier to turn off for production
$echo = "";

//get external module settings
$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module'];
$linked_idp = 0;
$old_dbref = strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);

//create objects
$my_modref = $myObject->getModRef();
$field_prefix = $myObject->getTableField();
$external_class = "SDBP6_".$old_modloc;
$extObject = new $external_class($old_modref, $page_object, $myObject->getCmpCode(), true);

//get table
$alt_table = $myObject->getAltModuleTable($old_modloc);
if($alt_table === false) {
	//stop phase process and record error
	$myObject->endExternalImportPhaseWithError($import_ref, "Couldn't identify the source module's table.");
	die("ERROR!  Couldn't identify the source module's table.");
}
//get id field
$alt_id_field = $myObject->getAltModuleIDField($old_modloc, $old_modref);
if($alt_id_field === false) {
	//stop phase process and record error
	$myObject->endExternalImportPhaseWithError($import_ref, "Couldn't identify the source module's System Ref field.");
	die("ERROR!  Couldn't identify the source module's System Ref field.");
}

//get mapping
$field_map = $myObject->getFieldMap($old_modloc, $old_modref, $page_object);    //array('old_field'=>"new_field")
$field_map_by_new_field = array_flip($field_map);
$new_field_settings = $headingObject->getHeadingsForExternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
$old_field_settings = $extObject->getHeadingsForMapping($field_map, false, true);


//ASSIST_HELPER::arrPrint($field_map);
//ASSIST_HELPER::arrPrint($old_field_settings);
//ASSIST_HELPER::arrPrint($new_field_settings);


//get lists from external module
$old_lists = $extObject->getListItems($field_map, $old_field_settings, $old_modref, $linked_idp);

//ASSIST_HELPER::arrPrint($old_lists);
//get local lists
//$list_map = $myObject->getListItemMapping($old_modref);
$new_lists = $myObject->getListItemsForExternalMapping($field_map, $new_field_settings);

//ASSIST_HELPER::arrPrint($new_lists);
//echo "<h3>".__LINE__;die();
//get list of previously imported items
$sql = "SELECT * FROM ".$myObject->getDBRef()."_import_external_links WHERE sdbip_id = ".$sdbip_id." AND src_modref = '".$old_modref."' AND (status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE;
$prev_objects = $myObject->mysql_fetch_all_by_id2($sql, "local_type", "src_id");

//get objects from external module
$records = $extObject->getSourceRecords($linked_idp);
//process objects based on mapping
//ASSIST_HELPER::arrPrint($prev_objects);
//ASSIST_HELPER::arrPrint($records);
$valid_number_options = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "-");

$insert_data = array();
foreach($records as $project_id => $project_data) {
	$new_record = array(
		$field_prefix.'_sdbip_id' => $sdbip_details['id'],
		$field_prefix.'_src_type' => "IMPORT",
		'results' => array(
			'src_modloc' => $old_modloc,
			'src_modref' => $old_modref,
			'src_id' => $project_id,
			'src_ref' => $extObject->getRefTag().$project_id,
			'list_results' => array(),
			'targets' => $project_data['targets'],
		),
		'import_key' => $project_id,
		'import_ref' => $import_ref,
		'import_status' => (SDBP6::CONVERT_IMPORT + SDBP6::ACTIVE),
		'import_user' => $myObject->getUserID(),
		'import_date' => date("Y-m-d H:i:s")
	);
	foreach($field_map as $old_field => $new_field) {
		/**
		 * Process record data:
		 * CHECK FIELD MAP:
		 *    IF FIELD IS MAPPED THEN CHECK DATA
		 *        DATE:DATE = leave as is - IDP3 handles it the same as SDBP6
		 *        DATE:TEXT = convert to string format (dd mmmm yyyy)
		 *        DATE:(MULTI)LIST = convert to string format (dd mmmm yyyy) & follow list check
		 *        DATE:BOOL = error
		 *        LIST:LIST = list process
		 *        MULTILIST:MULTILIST = list process
		 *        LIST:MULTILIST = list process
		 *        MULTILIST:LIST = take first item and then list process
		 *        LIST:DATE = convert list text to date (YYYY-MM-DD)
		 *        MULTILIST:DATE = get first item, convert to date
		 *        LIST:TEXT/VC = get list item and store as string
		 *        MULTILIST:TEXT/VC = get each list item and convert to string (;chr(10))
		 *        LIST:BOOL = get list item, test for yes/no in list item and convert to true/false - if no bool value then default to no
		 *        MULTILIST:BOOL = get first list item then follow LIST:BOOL function
		 *        BOOL:BOOL = leave as is
		 *        BOOL:TEXT/VC = Convert to string (Yes/No)
		 *        BOOL:DATE = not permitted
		 *        BOOL:LIST = Convert to string (Yes/No) then list process
		 *        BOOL:MULTILIST = Convert to string (Yes/No) then list process
		 *        TEXT/VC:TEXT/VC = leave as is
		 *        TEXT/VC:DATE = convert string to date (YYYY-MM-DD)
		 *        TEXT/VC:BOOL = test for yes/no and convert - if no yes/no then default to no
		 *        TEXT/VC:LIST = list process
		 *        TEXT/VC:MULTILIST = list process
		 *        OBJECT:OBJECT = list process
		 *        OBJECT:LIST = list process
		 *        OBJECT:anything else = not allowed
		 *        LIST:OBJECT = list process
		 *        anything else:OBJECT = not allowed
		 *
		 *        SEGMENT || MULTISEGMENT = leave as is - IDP3 & SDBP6 both source SEGMENT from MSCOA1 so refs remain the same - segment fields must match segment fields
		 *
		 *        List processing:
		 *            1. Get existing list item
		 *            2. Convert to comparison style
		 *            3. Check if exists in matching SDBP6 list
		 *                a. if exists get current ID
		 *                b. if not, add list item
		 */
		if($new_field != "0" && $new_field != false && isset($new_field_settings[$new_field]['type'])) {
			$old_setting = $old_field_settings[$old_field];
			$old_list_table = $old_setting['list'];
			$old_type = $old_setting['type'];

			$new_setting = $new_field_settings[$new_field];
			$new_list_table = $new_setting['list'];
			$new_type = $new_setting['type'];

			$original_value = $project_data[$old_field];
			$map_allowed = true;
			switch($old_type) {
				case "BOOL":
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
						case "DATE":
						case "SEGMENT":
						case "MULTISEGMENT":
						case "OBJECT":
						case "MULTIOBJECT":
							$map_allowed = false;
							break;
						case "BOOL":
							$new_value = $original_value;
							break;
						case "LIST":
						case "MULTILIST":
							if($original_value * 1 == 0) {
								$old_value = "No";
							} elseif($original_value * 1 == 1) {
								$old_value = "Yes";
							} else {
								$old_value = "No";
							}
							$new_value = processListItems($new_type, $old_value, $new_list_table, $original_value, $old_field);
							$new_record['results']['list_results'][$new_field] = $new_value;
							$new_value = $new_value[1];
							break;
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:
							if((int)$original_value == 0) {
								$new_value = "No";
							} elseif((int)$original_value == 1) {
								$new_value = "Yes";
							} else {
								$new_value = "No";
							}
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to "No"
					if($map_allowed === false) {
						$new_value = 0;
					}
					break;
				case "DATE":
					//format date from timestamp to YYYY-mm-dd format
					if($original_value > 0) {
						$original_value = date("Y-m-d", $original_value);
					} else {
						$original_value = "0000-00-00";
					}
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
						case "BOOL":
						case "OBJECT":
						case "MULTIOBJECT":
						case "SEGMENT":
						case "MULTISEGMENT":
							$map_allowed = false;
							break;
						case "DATE":
							$new_value = $original_value;
							break;
						case "LIST":
						case "MULTILIST":
							if(strlen($original_value) > 0 && $original_value != "0000-00-00" && strtotime($original_value) !== false && $original_value == date("Y-m-d", strtotime($original_value))) {
								$old_value = date("d F Y", strtotime($original_value));
								$new_value = processListItems($new_type, $old_value, $new_list_table, 0, $old_field);
								$new_record['results']['list_results'][$new_field] = $new_value;
								$new_value = $new_value[1];
							} else {
								$new_value = "";
							}
							break;
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:
							$new_value = data("d F Y", strtotime($original_value));
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to blank date
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
				case "MULTILIST":
				case "TEXTLIST":
				case "LIST":
				case "AREA":
				case "WARDS":
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
							$map_allowed = false;
							break;
						case "BOOL":    //LIST->BOOL (output = 0/1)
							$err = false;
							//if was multilist then get first value
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$o = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
								$o = ASSIST_HELPER::removeBlanksFromArray($o);
								$original_value = $o[0];
							}
							if($original_value * 1 > 0 && isset($old_lists['raw'][$old_list_table][$original_value])) {
								$old_value = $old_lists['raw'][$old_list_table][$original_value];
							} else {
								$err = true;
							}
							if($err === false && strtoupper(trim($old_value)) == "YES") {
								$new_value = 1;
							} else {
								$new_value = 0;
							}
							break;
						case "DATE":    //LIST->DATE (output = YYYY-MM-DD)
							$err = false;
							//if multilist then get first value
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$o = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
								$o = ASSIST_HELPER::removeBlanksFromArray($o);
								$original_value = $o[0];
							}
							if($original_value > 0 && isset($old_lists['raw'][$old_list_table][$original_value])) {
								$old_value = $old_lists['raw'][$old_list_table][$original_value];
							} else {
								$err = true;
							}
							if($err === false && strlen($old_value) > 0 && $old_value != "0000-00-00" && strtotime($old_value) !== false && $old_value == date("Y-m-d", strtotime($old_value))) {
								$new_value = date("Y-m-d", strtotime($old_value));
							} else {
								$new_value = "";
							}
							break;
						case "MULTIOBJECT":
						case "MULTISEGMENT":
						case "MULTILIST":    //LIST/Multi -> Multi (output = int;int)
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$original_value = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
								$original_value = ASSIST_HELPER::removeBlanksFromArray($original_value);
							} else {
								$original_value = array($original_value);
							}
							$new = array();
							foreach($original_value as $old) {
								if(ASSIST_HELPER::checkIntRef($old) && isset($old_lists['raw'][$old_list_table][$old])) {
									$old_value = $old_lists['raw'][$old_list_table][$old];
									$n = processListItems($new_type, $old_value, $new_list_table, $old, $old_field);
									$new_record['results']['list_results'][$new_field][$n[1]] = $n;
									$n = $n[1];
									if(!in_array($n, $new)) {
										$new[] = $n;
									}
								}
							}
							$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $new);
							break;
						case "OBJECT":
						case "SEGMENT":
						case "LIST":    //LIST->LIST (output - int)
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$o = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
								$o = ASSIST_HELPER::removeBlanksFromArray($o);
								$original_value = $o[0];
							}
							if(isset($old_lists['raw'][$old_list_table][$original_value]) !== false) {
								$old_value = $old_lists['raw'][$old_list_table][$original_value];
								$new_value = processListItems($new_type, $old_value, $new_list_table, $original_value, $old_field);
								$new_record['results']['list_results'][$new_field] = $new_value;
								$new_value = $new_value[1];
							} else {
								$new_value = 0;
							}
							break;
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:    //LIST->TEXT (output = text)
							if($old_type == "LIST") {
								if(isset($old_lists['raw'][$old_list_table][$original_value]) !== false) {
									$new_value = $old_lists['raw'][$old_list_table][$original_value];
								} else {
									$new_value = "";
								}
							} else { //assume source is multi list
								if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
									$original_value = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
									$original_value = ASSIST_HELPER::removeBlanksFromArray($original_value);
								}
								$new = array();
								foreach($original_value as $o) {
									if(isset($old_lists['raw'][$old_list_table][$o])) {
										$new[] = $old_lists['raw'][$old_list_table][$o];
									}
								}
								$new_value = implode("; ", $new);
							}
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to Unspecified
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
				case "MULTIOBJECT":
				case "OBJECT":
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
						case "SEGMENT":
						case "MULTISEGMENT":
						case "BOOL":
						case "DATE":
							$map_allowed = false;
							break;
						case "MULTILIST":
						case "MULTIOBJECT":
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$original_value = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
							} else {
								$original_value = array($original_value);
							}
							$new = array();
							foreach($original_value as $old) {
								if(isset($old_lists['raw'][$old_list_table][$old])) {
									$old_value = $old_lists['raw'][$old_list_table][$old];
									$n = processListItems($new_type, $old_value, $new_list_table, $old, $old_field);
									$new_record['results']['list_results'][$new_field][$n[1]] = $n;
									$n = $n[1];
									if(!in_array($n, $new)) {
										$new[] = $n;
									}
								}
							}
							$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $new);
							break;
						case "LIST":
						case "OBJECT":        //OBJECT->LIST/OBJECT (output = int)
							if(strpos($original_value, ASSIST_HELPER::JOIN_FOR_MULTI_LISTS) !== false) {
								$o = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
								$original_value = $o[0];
							}
							if(isset($old_lists['raw'][$old_list_table][$original_value])) {
								$old_value = $old_lists['raw'][$old_list_table][$original_value];
								$new_value = processListItems($new_type, $old_value, $new_list_table, $original_value, $old_field);
								$new_record['results']['list_results'][$new_field] = $new_value;
								$new_value = $new_value[1];
							} else {
								$new_value = 0;
							}
							break;
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:        //OBJECT->TEXT (output = text)
							$new_value = $old_lists['raw'][$old_list_table][$original_value];
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to Unspecified
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
				case "MULTISEGMENT":
				case "SEGMENT":
					//Segment - assume that the fields are mapped to the same segment.
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
						case "BOOL":
						case "DATE":
						case "LIST":
						case "MULTILIST":
						case "OBJECT":
						case "MULTIOBJECT":
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
							$map_allowed = false;
							break;
						case "SEGMENT":
						case "MULTISEGMENT":    //SEGMENT->SEGMENT (output = int)
							$new_value = $original_value;
							break;
						default:
							$map_allowed = false;
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to Unspecified
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
				case "PROJECT":
					$o = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $original_value);
					$o = $myObject->removeBlanksFromArray($o);
					if(count($o) > 0) {
						$new = array();
						foreach($o as $i) {
							if(isset($prev_objects[SDBP6_PROJECT::OBJECT_TYPE][$i])) {
								$new[] = $prev_objects[SDBP6_PROJECT::OBJECT_TYPE][$i]['local_id'];
							}
						}
						$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $new);
					} else {
						$new_value = "";
					}
					break;
				case "TOP":
					switch($new_type) {
						case 'REF':
						case 'BOOL':
						case 'ATTACH':
						case 'DATE':
						case 'LIST':
						case 'MULTILIST':
						case 'SEGMENT':
						case 'MULTISEGMENT':
						case 'PERC':
						case 'MEDVC':
						case 'LRGVC':
						case 'NUM':
							$map_allowed = false;
							break;
						case "TEXT":
							//get top layer name
							if(isset($old_lists['raw'][$old_list_table][$original_value])) {
								$new_value = $old_lists['raw'][$old_list_table][$original_value];
							} else {
								$new_value = "";
							}
							break;
						case "OBJECT":
						case "MULTIOBJECT":
							//if object is top then replace with id else get name and treat like list item
							if($new_field == $myObject->getTertiaryParentFieldName()) {
								if($original_value == 0 || !isset($prev_objects[SDBP6_TOPKPI::OBJECT_TYPE][$original_value])) {
									$new_value = 0;
								} else {
									$new_value = $prev_objects[SDBP6_TOPKPI::OBJECT_TYPE][$original_value]['local_id'];
									$new_record['results']['list_results'][$new_field] = array("EXISTING", $new_value);
								}
							} else {
								if(isset($old_lists['raw'][$old_list_table][$original_value])) {
									$old_value = $old_lists['raw'][$old_list_table][$original_value];
									$new_value = processListItems($new_type, $old_value, $new_list_table, $original_value, $old_field);
									$new_record['results']['list_results'][$new_field] = $new_value;
									$new_value = $new_value[1];
								} else {
									$new_value = 0;
								}
							}
							break;
					}
					break;
				case "CAP":
					switch($new_type) {
						case 'REF':
						case 'BOOL':
						case 'ATTACH':
						case 'DATE':
						case 'LIST':
						case 'MULTILIST':
						case 'SEGMENT':
						case 'MULTISEGMENT':
						case 'PERC':
						case 'MEDVC':
						case 'LRGVC':
						case 'NUM':
							$map_allowed = false;
							break;
						case "TEXT":
							//get top layer name
							if(isset($old_lists['raw'][$old_list_table][$original_value])) {
								$new_value = $old_lists['raw'][$old_list_table][$original_value];
							} else {
								$new_value = "";
							}
							break;
						case "OBJECT":
						case "MULTIOBJECT":
							//if object is top then replace with id else get name and treat like list item
							if($new_field == $myObject->getSecondaryParentFieldName()) {
								if($original_value == 0 || !isset($prev_objects[SDBP6_PROJECT::OBJECT_TYPE][$original_value])) {
									$new_value = 0;
								} else {
									$new_value = $prev_objects[SDBP6_PROJECT::OBJECT_TYPE][$original_value]['local_id'];
									if($new_type == "MULTIOBJECT") {
										$new_record['results']['list_results'][$new_field] = array(array("EXISTING", $new_value));
									} else {
										$new_record['results']['list_results'][$new_field] = array("EXISTING", $new_value);
									}
								}
							} else {
								if(isset($old_lists['raw'][$old_list_table][$original_value])) {
									$old_value = $old_lists['raw'][$old_list_table][$original_value];
									$new_value = processListItems($new_type, $old_value, $new_list_table, $original_value, $old_field);
									$new_record['results']['list_results'][$new_field] = $new_value;
									$new_value = $new_value[1];
								} else {
									$new_value = 0;
								}
							}
							break;
					}
					break;
				case "NUM":
					switch($new_type) {
						case "ATTACH":
						case "REF":
						case "SEGMENT":
						case "MULTISEGMENT":
						case "BOOL":
						case "DATE":
						case "LIST":
						case "MULTILIST":    //TEXT->LIST (output = int)
						case "OBJECT":
						case "MULTIOBJECT":    //TEXT->OBJECT (output = int)
							$map_allowed = false;
							break;
						case "PERC":
						case "NUM":
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:
							$new_value = $original_value;
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to blank
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
				case "TEXT":
				case "MEDVC":
				case "LRGVC":
				default:    //assume text field
					switch($new_type) {
						case "ATTACH":
						case "PERC":
						case "REF":
						case "SEGMENT":
						case "MULTISEGMENT":
							$map_allowed = false;
							break;
						case "NUM":        //TEXT->NUM = strip all alpha characters & keep any numbers found that are no more than 2 alpha characters apart e.g. "1 200" = 1200 but "1 and 2" = 1
							$v = str_replace(" ", "", strtolower($original_value));
							$v = str_replace("\t", ' ', $v); // remove tabs
							$v = str_replace("\n", '', $v); // remove new lines
							$v = str_replace("\r", '', $v); // remove carriage returns
							$v = str_replace(chr(10), '', $v); // remove ascii enter key
							$x = str_split($v);
							$c = 0;
							$num_found = false;
							$new = "";
							foreach($x as $char) {
								if(!$num_found) {
									if(in_array($char, $valid_number_options)) {
										$num_found = true;
										$new .= $char;
									}
								} else {
									if(in_array($char, $valid_number_options)) {
										$c = 0;
										$new .= $char;
									} elseif($c > 1) {
										break;
									} else {
										$c++;
									}
								}
							}
							if($num_found) {
								$new_value = $new * 1;
							} else {
								$new_value = 0;
							}
							break;
						case "BOOL":    //TEXT->BOOL (output = 0/1)
							if($original_value == 1) {
								$new_value = "Yes";
							} else {
								$new_value = "No";
							}
							break;
						case "DATE":    //TEXT->DATE (output = YYYY-MM-DD)
							if(strtotime($original_value) === false) {
								$new_value = "";
							} else {
								$new_value = date("Y-m-d H:i:s", strtotime($original_value));
							}
							break;
						case "LIST":
						case "MULTILIST":    //TEXT->LIST (output = int)
						case "OBJECT":
						case "MULTIOBJECT":    //TEXT->OBJECT (output = int)
							//function   processListItems($new_type,$old_value,$new_list_table,$original_id=0,$old_field="")
							$new_value = processListItems($new_type, $original_value, $new_list_table, 0, $old_field);
							$new_record['results']['list_results'][$new_field] = $new_value;
							$new_value = $new_value[1];
							break;
						case "TEXT":
						case "MEDVC":
						case "LRGVC":
						default:        //TEXT->TEXT
							$new_value = $original_value;
							break;
					}
					//if mapping is not allowed but user somehow got around restrictions, default to blank
					if($map_allowed === false) {
						$new_value = "";
					}
					break;
			}
			$new_record[$new_field] = $new_value;
		}
	}
	$insert_data[] = $new_record;
}
//ASSIST_HELPER::arrPrint($insert_data);


//echo "<table><tr>";
//foreach($field_map as $old_field => $new_field) {
//	echo "<th>".$old_field." : ".$new_field."</th>";
//}
//echo "</tr>";

foreach($insert_data as $row) {
	$row['results'] = serialize($row['results']);
	//bypass automatic capital project setting in table structure and set to operational
	if(!isset($row[$myObject->getTableField()."_cap_op"]) && ($page_object == SDBP6_TOPKPI::OBJECT_TYPE || $page_object == SDBP6_DEPTKPI::OBJECT_TYPE)) {
		$row[$myObject->getTableField()."_cap_op"] = 0;
	}
	$sql = "INSERT INTO ".$myObject->getTableName()."_temp SET ".$myObject->convertArrayToSQLForSave($row);
	$myObject->db_insert($sql);
//	echo "<tr>";
//	foreach($field_map as $old_field => $new_field) {
//		echo "<td>".$row[$new_field]."</td>";
//	}
//	echo "</tr>";
}

//echo "</table>";


//prep report?

//record end of phase 1 of import
$myObject->endExternalImportPhase1($import_ref);

//notify user
//$to = $myObject->getAnEmail($myObject->getUserID());
$to = "faranath@gmail.com";
if(strlen($to) > 0) {
	$subject = $myObject->getModTitle()." ".$myObject->replaceObjectNames("|".$page_object."|")." Import Phase 1 Complete";
	$message = "Dear ".$myObject->getUserName()."
	
	Phase 1 of the Import from IDP process for ".$myObject->getModTitle()." > ".$myObject->replaceObjectNames("|".$page_object."|")." is complete.  You can review the results and continue with Phase 2.
	";
	$emailObject = new ASSIST_EMAIL($to, $subject, $message);//,"TEXT","",$bcc);
	//$emailObject->sendEmail();
}
//echo $message;
?>
<?php
//echo json_encode(array("ok","Phase 1 completed successfully"));
?>
<script type="text/javascript">
	$(function () {
		parent.finishDialog("ok", "Phase 1 completed successfully.");
	});
</script>
