<?php
/**
 * REQUIRED FIELDS:
 * $section = module section
 */

require_once("inc_header.php");

ASSIST_HELPER::displayResult(array("info", "Any blank headings will be replaced with the default terminology."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$module_code = $helper->getModuleCode();

$sections = array($section);

$has_list_view = $sections;

$head_section = array();
foreach($sections as $s) {
	$head_section[] = $s;
	$cn = "SDBP6_".$s;
	$head_section[] = $cn::CHILD_OBJECT_TYPE;
}

?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<?php
			foreach($head_section as $h_section) {
				$rename_headings = $headingObject->getRenameHeadings($h_section);  //ASSIST_HELPER::arrPrint($rename_headings);
				$headings = $headingObject->getAllHeadingsForRenaming($h_section);  //ASSIST_HELPER::arrPrint($headings);
				$o = explode("_", $h_section);
				$object_type = $o[0];

				$class_name = $module_code."_".$object_type; //echo "<P>".$class_name;
				$objObject = new $class_name();
				$object_name = $objObject->getMyObjectName();
				$section_heading = $headingObject->getObjectName($object_name).(isset($o[1]) ? " ".$helper->getActivityName("update") : "");
				?>
				<h2><?php echo $section_heading; ?> Headings</h2>
				<?php if(in_array($h_section, $has_list_view)) {
					echo "<p><button class='btn_sort list' section='".$h_section."' />Sort for List Pages</button>&nbsp;<button class='btn_sort detail' section='".$h_section."' />Sort for Detailed View / Form Pages</button></p>";
				} ?>
				<table class='tbl-container not-max'>
					<tr>
						<td>
							<form name='frm_<?php echo $h_section; ?>'>
								<input type=hidden name=section value='<?php echo $h_section; ?>' />
								<table class=tbl_headings id="<?php echo "tbl_".$h_section; ?>" sec='<?php echo $h_section; ?>'>
									<tr>
										<th width=30px <?php if(in_array($h_section, $has_list_view)) {
											echo "rowspan=2";
										} ?>>Ref
										</th>
										<th width=50px <?php if(in_array($h_section, $has_list_view)) {
											echo "rowspan=2";
										} ?>>Status
										</th>
										<th width=200px <?php if(in_array($h_section, $has_list_view)) {
											echo "rowspan=2";
										} ?> >Default<br />Terminology<br />[Field Type]
										</th>
										<th width=200px <?php if(in_array($h_section, $has_list_view)) {
											echo "rowspan=2";
										} ?> >Your<Br />Terminology
										</th>
										<?php if(in_array($h_section, $has_list_view)) { ?>
											<th colspan=5>Include In List View</th><?php } ?>
										<?php if(in_array($h_section, $has_list_view)) { ?>
											<th rowspan=2 width=50px>Include in <br /><?php echo $helper->getActivityName("UPDATE"); ?> Summary View</th>
											<th rowspan=2 width=50px>Required<br />Field</th>
										<?php } ?>
										<th <?php if(in_array($h_section, $has_list_view)) {
											echo "rowspan=2";
										} ?> width=50px></th>
									</tr>
									<?php if(in_array($h_section, $has_list_view)) { ?>
										<tr class=th2>
											<th width=50px><?php echo $menuObject->getAMenuNameByField("new"); ?></th>
											<th width=50px><?php echo $menuObject->getAMenuNameByField("manage"); ?></th>
											<th width=50px><?php echo $menuObject->getAMenuNameByField("admin"); ?></th>
											<th width=50px>Action Dashboard</th>
											<th width=50px>Performance Module</th>
										</tr>
									<?php } ?>
									<?php
									foreach($headings as $fld => $h) {
										if($h['can_disable']) {
											$status = "";
											if($h['is_hidden']) {
												$status .= "Hidden<br /><button class='btn-restore small-button' id=restore_".$fld." field=".$fld.">Restore</button>";
											} else {
												$status .= "Active<br /><button class='btn-hide small-button' id=hide_".$fld." field=".$fld.">Hide</button>";
											}
										} else {
											$status = "<span class=i style='font-size:75%'>System<br />Required</span>";
										}
										echo "
	<tr class='".($h['is_hidden'] ? "inact" : "")."'>
		<td class=center>".$h['id']."</td>
		<td class=center>".$status."</td>
		<td style='position:relative'>".$helper->replaceObjectNames($h['mdefault'])." <br /><div style='margin-top: 10px;'>[".$headingObject->getHeadingTypeFormattedForClient($h['type'])."]</div></td>
		<td>";
										if(isset($rename_headings[$fld])) {
											$js .= $displayObject->drawFormField("LRGVC", array('id' => $fld, 'name' => "client[$fld]", 'class' => "heading_textarea", 'orig' => $h['client']), $h['client']);
										} else {
											echo "Rename field in Setup ".$helper->getBreadcrumbDivider()." Defaults ".$helper->getBreadcrumbDivider()." Naming";
										}
										echo "</td>";
										if(in_array($h_section, $has_list_view)) {
											if(($h['status'] & SDBP6_HEADINGS::SYSTEM_DEFAULT) == SDBP6_HEADINGS::SYSTEM_DEFAULT) {
												echo "
				<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("ok")."</td>
			";
											} else {
												if($h['can_list'] == 1) {
													//NEW
													$list_fld = "list_new";
													echo "
					<td>";
													$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
													echo "</td>";
													//MANAGE
													$list_fld = "list_manage";
													echo "
					<td>";
													$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
													echo "</td>";
													//ADMIN
													$list_fld = "list_admin";
													echo "
					<td>";
													$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
													echo "</td>";
													//ACTION DASHBOARD
													$list_fld = "list_dashboard";
													echo "
					<td>";
													$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
													echo "</td>";
													//INDIVIDUAL PERFORMANCE
													$list_fld = "list_internal";
													echo "
					<td>";
													$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
													echo "</td>";
												} else {
													echo "
					<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("error")."</td>
				";
												}
											}
											//COMPACT_VIEW
											$list_fld = "list_compact";
											echo "
			<td>";
											$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_".$list_fld, 'name' => $list_fld."[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h[$list_fld]), $h[$list_fld]);
											echo "</td>";
											//REQUIRED
											if($h['assist_required'] == 1 || ($h['status'] & SDBP6_HEADINGS::SYSTEM_MANAGED) == SDBP6_HEADINGS::SYSTEM_MANAGED) {
												echo "<td style='text-align: center; vertical-align: middle;'>";
												echo $helper->getDisplayIcon("ok");
											} else {
												echo "<td style=''>";
												$js .= $displayObject->drawFormField("BOOL_BUTTON", array('id' => $fld."_required", 'name' => "required[$fld]", 'yes' => 1, 'no' => 0, 'form' => "vertical", 'extra' => "boolButtonClick", 'small_size' => true, 'other' => "orig=".$h['required']), $h['required']);
											}
										}
										echo "</td>
		<td class='middle center'>
			<input type=hidden name=changes_".$fld." value='0' />
			<input type=hidden name=ref[$fld] value='".$h['id']."' />
			<button class=btn_save>".$headingObject->getActivityName("save")."</button>
		</td>
	</tr>";
									}
									?>
								</table>
							</form>
						</td>
					</tr>
				</table>
				<?php
			}
			?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'), "setup", array('section' => "HEAD_".$section), $section); ?></td>
	</tr>
</table>
<div id=dlg_confirm></div>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});

		$(".tbl_headings tr").not("tr:first").find("span.ui-icon").each(function () {
			if (!($(this).parents("button").length > 0)) {
				$(this).css("margin", "0 auto");
			}
		});
		$(".heading_textarea").prop("rows", 3);
		$(".heading_textarea").prop("cols", 40);

		$(".btn-hide").button({
			icons: {primary: "ui-icon-closethick"}
		}).click(function (e) {
			e.preventDefault();
			//var action = "DEACTIVATE";
			//var field = $(this).attr("field");
			//var dta = "section="+$(this).closest("table").attr("sec")+"&field="+field;
			//var display_text = "Do you wish to hide this field from all areas of the module?<br /><Br />Any data, if already captured, won't be lost and you can restore the field again at any stage.";
			//confirmSubmission(display_text,action,dta,field,$(this));
			deactivateButton($(this));
		}).closest("td").css("vertical-align", "middle");

		$(".btn-restore").button({
			icons: {primary: "ui-icon-check"}
		}).click(function (e) {
			e.preventDefault();
			restoreButton($(this));
		}).closest("td").css("vertical-align", "middle");

		$("tr.inact").find("button, textarea").each(function () {
			if ($(this).hasClass("btn-restore")) {
				$(this).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF");
			} else {
				$(this).removeClass("ui-state-ok").removeClass("ui-state-error").prop("disabled", true);
			}
		});

		$("textarea.heading_textarea").keyup(function () {
			if (AssistString.code($(this).val()) != $(this).attr('orig') && $(this).val() != $(this).attr("orig")) {
				$(this).addClass("orange-border");
			} else {
				$(this).removeClass("orange-border");
			}
		});

		$(".btn_save_all, .btn_save").removeClass("ui-state-default").addClass("ui-button-bold-grey")
			.hover(function () {
				$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-grey");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var sec = $(this).closest("table").attr("sec");
			if ($(this).hasClass("btn_save")) {
				var dta = "section=" + sec;
				dta += "&" + AssistForm.nonFormSerialize($(this).parent().parent());
			} else {
				$form = $("form[name=frm_" + sec + "]");
				var dta = AssistForm.serialize($form);
			}
			//$(this).closest("form").prop("method","post").prop("action","form_process.php").submit();
			console.log(dta);
			var result = AssistHelper.doAjax("inc_controller.php?action=Headings.Edit", dta);
			console.log(result);
			if (result[0] == "ok" && !$(this).hasClass("btn_save")) {
				document.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>?r[]=' + result[0] + '&r[]=' + result[1];
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
				var r = $(this).attr("ref");
				var v = $("#" + r).val();
				$("#" + r).attr("orig", v);
				$("input:text.valid8me").trigger("keyup");
			}
		});

		$(".tbl_headings").find("textarea.heading_textarea").each(function () {
			$(this).trigger("keyup");
		});


		$(".btn_sort").button({
			icons: {primary: "ui-icon-arrowthick-2-n-s"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).removeClass("ui-button-state-grey").addClass("ui-button-state-orange");
			}, function () {
				$(this).addClass("ui-button-state-grey").removeClass("ui-button-state-orange");
			}).click(function (e) {
			e.preventDefault();
			var section = $(this).attr("section");
			if ($(this).hasClass("list")) {
				var type = "list";
			} else {
				var type = "detail";
			}
			document.location.href = 'setup_defaults_headings_sort.php?section=' + section + '&type=' + type;
		});


		function confirmSubmission(display_text, action, dta, field, $btn) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>' + display_text + '</p><label id=lbl_hide></label>');
			var buttons = [
				{
					text: "Confirm & Continue", click: function () {
						$(this).dialog("close");
						nextStep(action, dta, field, $btn);
					}, class: 'ui-state-ok'
				},
				{
					text: "Cancel", click: function () {
						$(this).dialog("close");
					}, class: 'ui-state-error'
				}
			];
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}

		function nextStep(action, dta, field, $btn) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("inc_controller.php?action=HEADINGS." + action, dta);
			if (result[0] == "ok") {
				switch (action) {
					case "DEACTIVATE":
						$btn.closest("tr").addClass("inact").find("textarea").prop("disabled", true);
						$btn.closest("tr").find("button").each(function () {
							if ($(this).hasClass("btn-hide")) {
								$(this).closest("td")
									.html("Hidden<br />")
									.append($("<button />", {class: "btn-restore", html: "Restore"})
										.button({icons: {primary: "ui-icon-check"}})
										.attr("field", field)
										.removeClass("ui-state-default").addClass("ui-button-bold-green").addClass("small-button")
										.css("background-color", "#FFFFFF")
										.click(function (e) {
											e.preventDefault();
											restoreButton($(this));
										})
									);
							} else if ($(this).hasClass("btn-restore")) {
								$(this).closest("td").html("Active<br />").append($("<button />", {class: "btn-hide", html: "Hide"}).button({icons: {primary: "ui-icon-check"}}).attr("field", field).addClass("small-button"));
							} else {
								$(this).removeClass("ui-state-ok").removeClass("ui-state-error").prop("disabled", true);
							}
						});
						break;
					case "RESTORE":
						$btn.closest("tr").removeClass("inact").find("textarea").prop("disabled", false);
						$btn.closest("tr").find("button").each(function () {
							if ($(this).hasClass("btn-restore")) {
								$(this).closest("td")
									.html("Active<br />")
									.append($("<button />", {class: "btn-hide", html: "Hide"})
										.button({icons: {primary: "ui-icon-closethick"}})
										.attr("field", field)
										.addClass("small-button")
										.click(function (e) {
											e.preventDefault();
											deactivateButton($(this));
										})
									);
							} else if ($(this).hasClass("btn_yes")) {
								$(this).addClass("ui-state-ok").prop("disabled", false);
							} else if ($(this).hasClass("btn_no")) {
								$(this).addClass("ui-state-error").prop("disabled", false);
							} else {
								$(this).prop("disabled", false);
							}
						});
						break;
				}

				AssistHelper.finishedProcessing(result[0], result[1]);
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		}

		function restoreButton($me) {
			var action = "RESTORE";
			var field = $me.attr("field");
			var dta = "section=" + $me.closest("table").attr("sec") + "&field=" + field;
			var display_text = "Do you wish to restore this field to all areas of the module?<br /><Br />Any data, if already captured before, will also be restored.";
			confirmSubmission(display_text, action, dta, field, $me);
		}

		function deactivateButton($me) {
			var action = "DEACTIVATE";
			var field = $me.attr("field");
			var dta = "section=" + $me.closest("table").attr("sec") + "&field=" + field;
			var display_text = "Do you wish to hide this field from all areas of the module?<br /><Br />Any data, if already captured, won't be lost and you can restore the field again at any stage.";
			confirmSubmission(display_text, action, dta, field, $me);
		}

	});

	function boolButtonClick($me) {
		//alert($me.prop("id"));
		//$me.parent().addClass("orange-border");
//	$input = $me.parent().find("input:hidden");
//	if($input.val()!=$input.attr("orig")) {
//		$me.parent().parent().parent().find("input:hidden").val($me.parent().parent().parent().find("input:hidden").val()+1);
//	}
//	$me.parent().parent().parent().find("input:button").prop("disabled",false);
	}
</script>