<?php
/*******
 * Pre-requisites:
 * @var SDBP6_DEPTKPI $myObject - from calling page, which SDBP6_* object are you working on
 * @var string $page_section = MANAGE || ADMIN
 * @var string $page_action = UPDATE || EDIT
 * @var array $menu - from inc_header
 * @var array $sdbip_details - from inc_header
 * @var string $js - from inc_header
 * @var SDBP6_HELPER $helper - from inc_header
 * @var int $sdbip_id - from inc_header
 */

if(!isset($_REQUEST['object_type'])) {
	if(isset($object_type)) {
		$_REQUEST['object_type'] = $object_type;
	} else {
		ASSIST_HELPER::displayResult(array("error", "Assist has encountered an error while trying to determine which object you are working on.  Please try again."));
		die();
	}
} else {
	$object_type = $_REQUEST['object_type'];
}
$my_page = strtolower($_REQUEST['object_type']);

if(!isset($header_already_included) || $header_already_included == false) {
	require_once("inc_header.php");
}

if(isset($filter_by) && $filter_by == true) { //echo "<h3>FILTER BY</h3>";
//	echo "<hr /><h1 class='green'>Helllloooo $sdbip_id from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
	$filter_by = $myObject->getFilterByOptions($page_section, $page_action, (isset($sdbip_id) ? $sdbip_id : false));//echo $page_action."::".$page_section;ASSIST_HELPER::arrPrint($filter_by);

//	ASSIST_HELPER::arrPrint($filter_by);
//	echo "<hr />";
}

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
////ASSIST_HELPER::arrPrint($_SESSION[$myObject->getModRef()]);
//ASSIST_HELPER::arrPrint($_REQUEST);
//echo "<hr />";

if(isset($_REQUEST['filter']) && count($_REQUEST['filter']) > 0) {
	$req_filter = $_REQUEST['filter'];
	$options['filter'] = $req_filter;
	$myObject->setListPageFilter($req_filter);
} else {
	$old_filter = $myObject->getListPageFilter();
	if(count($old_filter) > 0) {
		$options['filter'] = $old_filter;
	}
	if(isset($options['filter']['year']) && $options['filter']['year'] == 0) {
		$options['filter']['year'] = $sdbip_id;
	}
}

//ASSIST_HELPER::arrPrint($options);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


if(isset($add_button) && $add_button == true && isset($add_button_label)) {
	$add_button_label = $helper->replaceAllNames($add_button_label);
}


$page_type = "LIST";
if(!isset($button_label) || strlen($button_label) == 0) {
	$button_label = $helper->getActivityName("open");
} elseif($button_label == "breadcrumbs") {
	$button_label = implode(" ", $menu['breadcrumbs']);
} elseif(substr($button_label, 0, 1) == "|" && substr($button_label, -1) == "|") {
	$button_label2 = $helper->getActivityName(substr($button_label, 1, -1));
	if(strlen($button_label2) == 0 || $button_label2 == false) {
		$button_label = $helper->getObjectName(substr($button_label, 1, -1));
	} else {
		$button_label = $helper->replaceAllNames($button_label2);
	}
}

//if($page_section=="NEW") {
//$page_action.= ($page_action!="CONFIRM" && substr($page_action,0,8)!="ACTIVATE" ? "_".$child_object_type : "");
//} else
$is_update_page = false;
$is_review_page = false;
$is_view_page = false;
$is_edit_page = false;
$is_add_page = false; //this page should never be an add page as it is a list page and create pages, after new, never include list pages
$original_page_action = $page_action;
if($page_action != "VIEW") {
	if($page_action == "UPDATE") {
		$is_update_page = true;
	} elseif($page_action == "APPROVE" || $page_action == "ASSURANCE") {
		$is_review_page = true;
	} else {
		$is_edit_page = true;
	}
	$page_action .= "_".$object_type;
} else {
	$is_view_page = true;
}
if(isset($page_direct)) {
	$page_direct = strtolower($page_direct).(substr($page_direct, -1) != "&" ? "?" : "")."object_id=";
} elseif($page_section != "NEW") {
	$page_direct = strtolower($page_section)."_".strtolower($page_action)."_object.php?object_id=";
} else {
	$pa = substr($page_action, 0, strpos($page_action, "_"));
	$page_direct = strtolower($page_section)."_".strtolower($object_type)."_".strtolower($pa)."_object.php?object_id=";
}

$js .= " var page_direct = '".$page_direct."';
";
$class_name = "SDBP6_".$object_type;
$myObject = new $class_name();
if(isset($options)) {
	$options = array(
			'type' => $page_type,
			'section' => $page_section,
			'page' => $page_action,
		) + $options;
} else {
	$options = array(
		'type' => $page_type,
		'section' => $page_section,
		'page' => $page_action,
		'filter' => array(
			'year' => $sdbip_id,
			'who' => "all",
			'what' => "all",
			'when' => "today",
			'display' => "list",
		)
	);
}

$button = array('value' => $helper->replaceAllNames($button_label), 'class' => "btn_list", 'type' => "button");
if(isset($button_icon)) {
	$button['icon'] = $button_icon;
}

if(!isset($filter_by)) {
	$filter_by = false;
} else {
	if(isset($options['filter']['who']) && (strtoupper($options['filter']['who']) == "X" || strtoupper($options['filter']['who']) == "ALL") && count($filter_by['who']) > 0) {
		$who_keys = array_keys($filter_by['who']);
//		ASSIST_HELPER::arrPrint($who_keys);
		$found = false;
		$c = 0;
		while(strpos($who_keys[$c], "_") === false && $c < count($who_keys)) {
//			echo "<br />".$c." :: ".$who_keys[$c];
			$c++;
			if(strpos($who_keys[$c], "_") !== false) {
				$found = true;
			}
		}
		if($found === false) {
			$options['filter']['who'] = "all";
		} else {

//			echo "<br />FOUND @ ".$c." :: ".$who_keys[$c];
			if(substr($who_keys[$c], 0, 3) == "DIR" && substr($who_keys[$c + 1], 0, 3) == "SUB") {
				$c++;
			}
//			echo "<br />OOPS!  SUB FOUND @ ".$c." :: ".$who_keys[$c];
			$options['filter']['who'] = $who_keys[$c];
		}

	}
}

if($page_section !== "NEW" && (!isset($filter_by['who']) || count($filter_by['who']) == 0)) {
	echo "<P>You do not have access to ".$myObject->replaceAllNames("|".$original_page_action."|")." any ".$myObject->replaceAllNames("|".$object_type."S|").".</P>";
} else {


	$get_list_options = $options;
	$get_list_options['by'] = $filter_by;

	if($page_action == "VIEW" && isset($get_list_options['filter']['when']) && $get_list_options['filter']['when'] == "X") {
		$ti = array_keys($get_list_options['by']['when']);
		$get_list_options['filter']['when'] = $ti[0];
	}

//	echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($filter_by);
//ASSIST_HELPER::arrPrint($options);
//	echo "<hr />";

	$get_list_options['sdbip_id'] = $sdbip_id;
	$get_list_options['sdbip_details'] = $sdbip_details;
	/*if(in_array($original_page_action,array("VIEW","EDIT"))) {
		$get_list_options['first_open_time_period'] = true;
	} else {
		$get_list_options['first_open_time_period'] = false;
	}*/
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($options);
//ASSIST_HELPER::arrPrint($get_list_options);
//ASSIST_HELPER::arrPrint($filter_by);
//echo "<hr />";

	/** @var SDBP6_DISPLAY $displayObject */
	/** @var SDBP6_DEPTKPI $myObject */
	$js .= $displayObject->drawListTable($myObject->getObject($get_list_options), $button, $object_type, $options, false, array((isset($add_button) ? $add_button : false),
		(isset($add_button_label) ? $add_button_label : ""),
		(isset($add_button_function) ? $add_button_function : "")), $filter_by);


	$js .= "
		$(\"button.btn_list\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";


//if the page is an update page, when a table cell is clicked (not last cell with button in it) then open update pop-up
	if($is_update_page || $is_review_page) {
		$activity_name = $myObject->getActivityName($original_page_action);
		echo "
	<div id=dlg_update title='$activity_name'>
		<iframe id=ifr_update>
		</iframe>
	</div>";
		$js .= "
	var windowSize = AssistHelper.getWindowSize();
	var dlgWidth = windowSize.width<800?windowSize.width*0.9:windowSize.width*0.75;  console.log(windowSize); console.log(dlgWidth);
	var ifrWidth = dlgWidth-2;
	var dlgHeight = windowSize.height*0.9;
	var ifrHeight = dlgHeight-37;
	$('#dlg_update').dialog({
		modal:true,
		autoOpen: false,
		width:dlgWidth,
		height:dlgHeight,
		close:function(e) {
			AssistHelper.closeProcessing();
		}
	}).css('padding','0px');
	$('#ifr_update').prop('width',ifrWidth).prop('height',ifrHeight).css({'border':'0px solid #ababab','margin':'0px'});
	$('table.tbl-container').find('table.list:first').find('tr:gt(0)').find('td:not(:last)').click(function() {
		AssistHelper.processing();
		var i = $(this).parent('tr').attr('ref');
		var time_id = $('#page_time_id').val();
		$('#ifr_update').prop('src',url+i+'&time_id='+time_id+'&page_src=dialog');
		//$('#dlg_update').dialog('open');
	});
	";
	}
}
//ASSIST_HELPER::arrPrint($_SESSION['SDP18']);
?>
<script type=text/javascript>
	var url = "<?php echo $page_direct; ?>";
	var section = "<?php echo $object_type; ?>";
	$(function () {

		<?php echo $js; ?>

	});

	function tableButtonClick($me) {
		<?php
		if(isset($alternative_button_click_function) && $alternative_button_click_function !== false && strlen($alternative_button_click_function) > 0) {
			echo $alternative_button_click_function."($"."me);";
		} elseif($is_edit_page && $page_section == "ADMIN") {
			echo "
			AssistHelper.processing();
			if($"."me.closest('tr').hasClass('inactive')) {
				var r = $"."me.attr(\"ref\");
				document.location.href = url+r+'&inactive_object=1&time_id='+time_id;	
			} else {
				var r = $"."me.attr(\"ref\");
				var time_id = $('#page_time_id').val();
				document.location.href = url+r+'&time_id='+time_id;			
			}
			";
		} else {
			echo "
			AssistHelper.processing();
				var r = $"."me.attr(\"ref\");
				var time_id = $('#page_time_id').val();
				document.location.href = url+r+'&time_id='+time_id;
				";
		}
		?>

	}

	function formatTableButton($me) {
		$me.button({
			icons: {primary: "ui-icon-<?php echo isset($button['icon']) ? $button['icon'] : "newwin"; ?>"},
		}).children(".ui-button-text").css({"font-size": "80%"}).click(function (e) {
			e.preventDefault();
		});
	}

	function dialogFinished(icon, message) {
		$("#dlg_update").dialog("close");
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon, message);
	}


</script>