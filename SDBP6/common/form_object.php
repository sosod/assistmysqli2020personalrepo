<?php
/*************
 * REQUIRES:
 *
 * @var int $object_id ; //0 if ADD else current object id
 * @var SDBP6_DEPTKPI $myObject = new SDBP6_*****();
 * @var array $sdbip_details - from inc_header;
 *
 * @var string $page_section = "NEW || MANAGE || ADMIN etc";
 * @var string $page_action = "ADD || EDIT || UPDATE etc";
 *
 * @var string $page_redirect_path = ""; //go back home
 *
 * @var string $js - from inc_header
 */
$object_type = $myObject->getMyObjectType();
$parent_object_type = $myObject->getMyParentObjectType();
if(!is_null($parent_object_type)) {
	$class_name = "SDBP6_".$parent_object_type;
	$parentObject = new $class_name();
	$parent_object_id = $sdbip_details['id'];
} else {
	$parentObject = null;
	$parent_object_id = 0;
}

$filter_by = isset($filter_by) ? $filter_by : array();

$form_page_action = $page_section.".".$page_action;
//	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0,$form_object=array(),$filter_by=array()) {
/** @var SDBP6_DISPLAY $displayObject */
$data = $displayObject->getObjectForm($object_type, $myObject, $object_id, $parent_object_type, $parentObject, $parent_object_id, $form_page_action, $page_redirect_path, true, 0, array(), $filter_by, (isset($external_call) ? $external_call : false));
$js .= $data['js'];
echo "
	<h2>".$myObject->getActivityName($page_action)." ".$myObject->getObjectName($object_type)."</h2>
	<table class='not-max tbl-container'>
		<tr><td>".$data['display'];


if(isset($data['linked_objects']['display_linked_objects']) && $data['linked_objects']['display_linked_objects'] == true) {
	$object_types = array(SDBP6_PROJECT::OBJECT_TYPE => "SDBP6_PROJECT", SDBP6_TOPKPI::OBJECT_TYPE => "SDBP6_TOPKPI", SDBP6_DEPTKPI::OBJECT_TYPE => "SDBP6_DEPTKPI");
	//trap to catch any faulty "display_linked_objects" coming from $data - linked objects should only display on VIEW page but the linked object page_action must be forced to VIEW, just in case they display by mistake on another page [extra bug fix for AA-604 - JC 5 May 2021]
	$linked_page_action = "VIEW";
	$linked_form_page_action = $page_section.".".$linked_page_action;
	foreach($object_types as $linked_object_type => $linked_class) {
		if(isset($data['linked_objects']['linked_objects'][$linked_object_type]) && count($data['linked_objects']['linked_objects'][$linked_object_type]) > 0) {
			echo "
		<h2>".$myObject->getActivityName($linked_page_action)." Linked ".$myObject->getObjectName($linked_object_type)."</h2>";
			foreach($data['linked_objects']['linked_objects'][$linked_object_type] as $linked_id) {
				$linkObject = new $linked_class();
				$linked_data = $displayObject->getObjectForm($linked_object_type, $linkObject, $linked_id, "", null, $parent_object_id, $linked_form_page_action, $page_redirect_path, true, 0, array(), $filter_by, (isset($external_call) ? $external_call : false), true);
				$js .= $linked_data['js'];
				echo $linked_data['display'];
			}
		}
	}
}


//display activity log for development purposes
if(!isset($external_call) || $external_call != true) {
	if($page_section != "NEW") {
		if($page_section != "SETUP") {
			$js .= $displayObject->drawPageFooter($myObject->getGoBack($page_redirect_path, "", true), $myObject->getMyLogTable(), "min_date=".strtotime($sdbip_details['activationdate'])."&object_id=".$object_id, $object_id);
		} else {
			$js .= $displayObject->drawPageFooter($myObject->getGoBack($page_redirect_path, "", true), $myObject->getMyLogTable(), "object_id=".$object_id, $object_id);
		}
	} else {
		/** @var SDBP6_DISPLAY $displayObject */
		$js .= $displayObject->drawPageFooter($myObject->getGoBack($page_redirect_path, "", true));
	}
} else {
	$logObject = new SDBP6_LOG($myObject->getMyLogTable());
	$var = array(
		'min_date' => strtotime($sdbip_details['activationdate']),
		'object_id' => $object_id,
	);
	echo "<h4>Activity Log</h4>";
	$logs = $logObject->getObject($var);
	echo $logs[0];
}


echo "<p>&nbsp;</p></td></tr>
	</table>";

?>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>

		$('form:first').find('input[type=text],textarea,select').filter(':visible:first').focus();


	});	//end start of jquery
</script>