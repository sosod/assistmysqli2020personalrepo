<?php
//Complete rewrite using separate code from the IDP/SDBP5B copy to accommodate the split of SDBIP specific setups
//record start of phase 1 of import
$import_ref = $sdbipObject->startExternalImportPhase1($sdbip_details, $page_object);
//Basic details
$my_sdbip_id = $sdbip_details['sdbip_id'];
$my_mscoa_version_code = $sdbip_details['mscoa_version'];
$source_sdbip_id = $sdbip_details['alt_module'];
$source_mscoa_version_code = $sdbip_details['alt_module_settings']['sdbip_mscoa_version'];
$source_modloc = "SDBP6";
$source_modref = $sdbipObject->getModRef();
$field_prefix = $myObject->getTableField();

$same_mscoa_version = true;
if($my_mscoa_version_code !== $source_mscoa_version_code) {
	$same_mscoa_version = false;
}
$headings = $headingObject->getHeadingsForInternalMapping($page_object, false, true);

$all_lists = array();
$all_segments = array();
$listObject = new SDBP6_LIST();
if($same_mscoa_version !== true) {
	$segmentObject = new SDBP6_SEGMENTS();
}

//foreach headings - get lists & segments
foreach($headings as $fld => $head) {
	$list = $head['list'];
	switch($head['type']) {
		case "LIST":
		case "MULTILIST":
			$listObject->changeListType($list);
			$all_lists[$fld] = $listObject->getCopiedListItems($source_sdbip_id, $my_sdbip_id);
			break;
		case "SEGMENT":
		case "MULTISEGMENT":
			if($same_mscoa_version !== true) {
				$segmentObject->setSection($list);
				$all_segments[$fld] = $segmentObject->getIDsForNewYearImport($source_mscoa_version_code, $my_mscoa_version_code);
			}
			break;
		case "OBJECT":
		case "MULTIOBJECT":
			//get copied items for orgstructure - everything else is... TODO
			if(substr($list, 0, strlen("SDBP6_SETUP_ORGSTRUCTURE")) == "SDBP6_SETUP_ORGSTRUCTURE") {
				$l = explode("|", $list);
				$class_name = $l[0];
				$extra_info = isset($l[1]) ? $l[1] : "";
				$objObject = new $class_name();
				$all_lists[$fld] = $objObject->getCopiedListItems($source_sdbip_id, $my_sdbip_id);
			} elseif($list == "SDBP6_PROJECT" || $list == "SDBP6_TOPKPI") {
				$objObject = new $list();
				$all_lists[$fld] = $objObject->getCopiedListItems($source_sdbip_id, $my_sdbip_id);
			} else {
				$objObject = new $list();
				$all_lists[$fld] = "ERROR - ".$list;//TODO !!!!! [JC] AA-419
			}
			break;
	}
}

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($all_lists);
//echo "<hr />";

//get objects from external module
$extObject = new SDBP6_SDBP6($source_modref, $page_object, $myObject->getCmpCode(), true);
$field_map = $extObject->getDefaultFieldMap($headings);
$records = $extObject->getSourceRecords($source_sdbip_id);//,$count_of_records_processed,$records_to_be_processed);

//process objects based on mapping - just copy data straight across
$insert_data = array();
foreach($records as $object_id => $object_data) {
	$new_record = array($field_prefix.'_sdbip_id' => $my_sdbip_id,
		$field_prefix.'_src_type' => "IMPORT",
		'results' => array('src_modloc' => $source_modloc,
			'src_modref' => $source_modref,
			'src_id' => $object_id,
			'src_ref' => $extObject->getRefTag().$object_id,
			'list_results' => array(),
			'targets' => isset($object_data['targets']) ? $object_data['targets'] : array(),
		),
		'import_key' => $object_id,
		'import_ref' => $import_ref,
		'import_status' => (SDBP6::CONVERT_SDBP6ED + SDBP6::ACTIVE),    //auto mark as done as the copy is happening internally
		'import_user' => $myObject->getUserID(),
		'import_date' => date("Y-m-d H:i:s"));
	foreach($field_map as $old_field => $new_field) {
		$old_value = $object_data[$old_field];
		$type = $headings[$old_field]['type'];
		$is_multi = false;
		switch($type) {
			case "MULTILIST":
				$is_multi = true;
			case"LIST":
				if(!$is_multi) {
					if(isset($all_lists[$new_field][$old_value])) {
						$new_value = $all_lists[$new_field][$old_value];
					} else {
						$new_value = 0;
					}
				} else {
					$old_array = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $old_value);
					$old_array = ASSIST_HELPER::removeBlanksFromArray($old_array);
					if(count($old_array) > 0) {
						$new_value = array();
						foreach($old_array as $old_value) {
							if(isset($all_lists[$new_field][$old_value])) {
								$new_value[] = $all_lists[$new_field][$old_value];
							}
						}
						if(count($new_value) > 0) {
							$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $new_value);
						} else {
							$new_value = "";
						}
					} else {
						$new_value = "";
					}
				}
				break;
			case "MULTISEGMENT":
				$is_multi = true;
			case "SEGMENT":
				if($is_multi) {
				}
				if(isset($all_segments[$new_field][$old_value])) {
					$new_value = $all_segments[$new_field][$old_value];
				} else {
					$new_value = 0;
				}
				break;
			case "MULTIOBJECT":
				$is_multi = true;
			case "OBJECT":
//	echo "<hr /><h1 class='green'> $old_field  ".$headings[$old_field]['list']." Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
				if(substr($headings[$old_field]['list'], 0, strlen("SDBP6_SETUP_ORGSTRUCTURE")) == "SDBP6_SETUP_ORGSTRUCTURE") {
//	echo "<hr /><h1 class='green'> ORG Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//	echo "<hr />";
					if(isset($all_lists[$new_field][$old_value])) {
						$new_value = $all_lists[$new_field][$old_value];
					} else {
						$new_value = 0;
					}
				} elseif($headings[$old_field]['list'] == "SDBP6_TOPKPI" || $headings[$old_field]['list'] == "SDBP6_PROJECT") {
//	echo "<hr /><h1 class='green'> PROJECT Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
					if($is_multi) {
						$old_array = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $old_value);
						$old_array = ASSIST_HELPER::removeBlanksFromArray($old_array);
//							echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//							ASSIST_HELPER::arrPrint($old_array);
//							echo "<hr />";
						if(count($old_array) > 0) {
							$new_value = array();
							foreach($old_array as $old_value) {
								if(isset($all_lists[$new_field][$old_value])) {
									$new_value[] = $all_lists[$new_field][$old_value];
								}
							}
							if(count($new_value) > 0) {
								$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $new_value);
							} else {
								$new_value = "";
							}
						} else {
							$new_value = "";
						}
					} else {
						if(isset($all_lists[$new_field][$old_value])) {
							$new_value = $all_lists[$new_field][$old_value];
						} else {
							$new_value = 0;
						}
					}
				} else {
					$new_value = $old_value;
				}
				break;
			default:
				$new_value = $old_value;
				break;
		}
		$new_record[$new_field] = $new_value;
	}
	$insert_data[] = $new_record;
}

//insert data into database
foreach($insert_data as $row) {
	$row['results'] = serialize($row['results']);
//		echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		ASSIST_HELPER::arrPrint($row);
//		echo "<hr />";
	$sql = "INSERT INTO ".$myObject->getTableName()."_temp SET ".$myObject->convertArrayToSQLForSave($row);
	$myObject->db_insert($sql);
}


//record end of phase 1 of import
$myObject->endExternalImportPhase1($import_ref);

//notify user
//$to = $myObject->getAnEmail($myObject->getUserID());
$to = "faranath@gmail.com";
if(strlen($to) > 0) {
	$subject = $myObject->getModTitle()." ".$myObject->replaceObjectNames("|".$page_object."|")." Import Phase 1 Complete";
	$message = "Dear ".$myObject->getUserName()."
	
	Phase 1 of the Import from SDBP6 process for ".$myObject->getModTitle()." > ".$myObject->replaceObjectNames("|".$page_object."|")." is complete.  You can review the results and continue with Phase 2.
	";
	$emailObject = new ASSIST_EMAIL($to, $subject, $message);//,"TEXT","",$bcc);
	$emailObject->sendEmail();
}
echo $message;

?>
<script type="text/javascript">
	$(function () {
		parent.finishDialog("ok", "Phase 1 completed successfully.");
	});
</script>
