<?php
require_once("inc_header.php");


$object_id = 0;//$_REQUEST['object_id'];
$myObject = new SDBP6_TOPKPI();

$page_section = "MANAGE";
$page_action = "CREATE";

$page_redirect_path = "manage_create_top.php?";

$filter_by = $myObject->getFilterByOptions($page_section, $page_action);

if(count($filter_by['who']) > 0) {
	include("common/form_object.php");
} else {
	ASSIST_HELPER::displayResult(array("info", "You do not have the necessary ".$myObject->getObjectName("ADMIN")." access to ".$myObject->getActivityName($page_action)." ".$myObject->getObjectName($myObject->getMyObjectName(true)).".  Please contact your ".$myObject->getObjectName("SDBIP")." ".$myObject->getObjectName("ADMIN")." if you believe this to be an error."));
}


?>
<?php markTime("end of page"); ?>