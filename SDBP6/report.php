<?php

if(!isset($_REQUEST['my_sdbip_id']) || !isset($_REQUEST['mr'])) {

	$header_already_included = true;
	$include_status = false;
	$include_colour = false;
	$sdbip_status_filter = "ACTIVATED";
	$do_not_include_sdbip_name = true;
	require_once("inc_header.php");
	$url = "report.php?mr=".$sdbipObject->getModRef()."&my_sdbip_id=";


	require_once "common/sdbip_chooser.php";

} else {

	$url = "report_generate_dept.php?my_sdbip_id=".$_REQUEST['my_sdbip_id'];
	$_SESSION[$_REQUEST['mr']]['REPORT_SDBIP_ID'] = $_REQUEST['my_sdbip_id'];
	header("Location: $url ");
}
?>