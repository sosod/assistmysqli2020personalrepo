<?php


require_once("inc_header.php");
ASSIST_HELPER::displayResult(array("info", "This section allows you to manage settings which affect a specific ".$helper->replaceAllNames("SDBIP").".  To manage global settings which affect the entire module please go to ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_setup_defaults"), "|"))."."));


/** @var SDBP6_SDBIP $sdbipObject - defined in inc_header */
$available_sdbips = $sdbipObject->getAllAvailableSDBIPsForSetup();


?>
<table style="width:1100px;margin: 0 auto" id=tbl_container>
	<tr>
		<td style='text-align: center' class=container>
			<?php

			$url = "setup_sdbip_defaults.php?sdbip_id=";


			if(isset($button_name)) {
				$button_name = $sdbipObject->replaceActivityNames($button_name);
			} else {
				$button_name = $sdbipObject->replaceActivityNames("|open|");
			}


			foreach($available_sdbips as $key => $s) {
				$js .= $displayObject->drawVisibleBlock($s['name'], $key, $s['extra_info'], $button_name);
			}


			?>
		</td>
	</tr>
</table>

<script type="text/javascript">

	$(function () {
		<?php echo $js; ?>



		var scr_dimensions = AssistHelper.getWindowSize();
		var div_h = 0;
		var tbl_h = 0;
		$("div.div_segment").button().click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).attr("page");
			var url = '<?php echo $url; ?>' + page.toLowerCase();
			document.location.href = url;

		});
		$("div.div_segment").hover(
			function () {
				$me = $(this).find("#btn_" + $(this).attr("page") + "_open");
				$me.addClass("ui-state-hover");
			},
			function () {
				$me = $(this).find("#btn_" + $(this).attr("page") + "_open");
				$me.addClass("ui-state-default").removeClass("ui-state-hover");
			}
		).css({"margin": "10px", "background": "url()", "padding-bottom": "30px"});//,"border-color":"#0099FF"});

		$("button.btn_open").button({
			icons: {primary: "ui-icon-newwin"}
		}).css({
			"position": "absolute", "bottom": "5px", "right": "5px"
		});
		$("button.btn_open").click(function (e) {
			e.preventDefault();
			$(this).parent("div.div_segment").trigger("click");
		});

		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size": "80%", "padding": "4px", "padding-left": "24px"});
			$("button.xbutton").css({"margin": "2px"});
		}

		formatButtons();
		<?php
		if(isset($_REQUEST['section'])) {
			echo "
			$('#btn_".$_REQUEST['section']."_open').trigger('click');
			";
		}
		?>

		var description_height = 0;
		$(".tbl-segment-description").find("td").css({"vertical-align": "middle", "text-align": "center", "padding": "5px"});
		$(".tbl-segment-description").each(function () {
			var h = parseInt(AssistString.substr($(this).css("height"), 0, -2));
			if (h > description_height) {
				description_height = h;
			}
		});
		$(".tbl-segment-description").css("height", (description_height + 5) + "px");
		$("#tbl_container, #tbl_container td").css("border", "1px solid #ffffff");
		$(".div_segment").css("border", "1px solid #9999ff");
	});
</script>
