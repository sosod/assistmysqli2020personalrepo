<?php
//$my_tab = "stratperfmeasures";
$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "manage_edit_object.php?object_id=".$parent_object_id."&";
}

include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$page_action = $_REQUEST['page_action'];
if($page_action=="add") {
	$parent_object_id = $_REQUEST['object_id'];
	$child_object_id = 0;
} else {
	$child_object_id = $_REQUEST['object_id'];
	$parent_object_id = 0;
}
//echo $object_type;
switch($object_type) {
	case "PARENT":
		$parent_object_type = null;
		$childObject = new LDAL1_PARENT();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = null;
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
	case "CHILD":
		$parent_object_type = "PARENT";
		$childObject = new LDAL1_CHILD();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = new LDAL1_PARENT();
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
	case "ACTIVITY":
		$parent_object_type = "CHILD";
		$childObject = new LDAL1_ACTIVITY();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = new LDAL1_CHILD();
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
}

$child_redirect = "manage_edit_object.php?object_type=".$object_type."&object_id=";
$child_name = $helper->getObjectName($childObject->getMyObjectName());



//ASSIST_HELPER::arrPrint($_REQUEST);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php 
	if($display_type=="default") {
		$td2_width = "48%"; 
	?>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="IDP"));
			//$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>><h2>New <?php echo $child_name; ?></h2>
	<?php 
	} else {
		echo "<td>";
	}
	//echo $childObject->getMyObjectType();
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<?php if(isset($child_objects) && count($child_objects)>0 && $display_type=="default") { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	?>
	$("input:button.btn_edit").click(function() {
		var i = $(this).attr("ref");
		document.location.href = '<?php echo $child_redirect; ?>'+i;
	});
	
	
	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>
	
});
</script>