<?php
require_once("inc_header.php");
//ASSIST_HELPER::arrPrint($_REQUEST);

/*****************
 * Plan:
 *
 * + Get object id from REQUEST
 * + Draw tree view
 *
 * object/contract = FUNCTION
 * parent = NULL
 * child/deliverable = SUB / SUBFUNCTION
 * child's child's child/action = ACTIVITY
 *
 */



function getTierLevel($object_type) {
	switch($object_type) {
		case "PARENT": return "tier1"; break;
		case "CHILD": return "tier2"; break;
		//case "": return "tier4"; break;
		case "ACTIVITY":
		default:
			return "tier3"; break;
	}
}

function validateTierLevel($tier) {
	if($tier=="tier4" || (is_numeric($tier) && $tier>3)) {
		return false;
	}
	return true;
}


$object_id = $_REQUEST['object_id'];

$section = "MANAGE";
$page_redirect_path = "manage_view_object.php?object_id=".$object_id."&tab_act=tree&";
$page_action = "VIEW";
$dialog_url =  "manage_view_dialog.php";


require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$can_confirm = true;

/**
 * Set Tier objects
 */
//TIER1 = STRATOBJ
$t1Object = new LDAL1_PARENT();
$object_type = $t1Object->getMyObjectType();
$object_name = $t1Object->getMyObjectName();
$t1_object_type = $object_type;
$t1_object_name = $object_name;
$t1_id = $object_id;
$t1_object_id = $object_id;
$t1_object_details = $t1Object->getSimpleDetails($t1_object_id);
//$idp_object_id = $t1_object_details['parent_id'];
//$idpObject = new IDP1_IDP();
//$second_heading = " ".IDP1_HELPER::BREADCRUMB_DIVIDER." <a href='new_create_idp.php?object_id=".$idp_object_id."' class=breadcrumb>".$idpObject->getHeading($idp_object_id)."</a> ".IDP1_HELPER::BREADCRUMB_DIVIDER." ".$helper->getObjectName("STRATPLAN");

//$helper->arrPrint($t1_object_details);

//TIER2 = STRATFOCUS
$t2Object = new LDAL1_CHILD();
$t2_object_type = $t2Object->getMyObjectType();
$t2_object_name = $t2Object->getMyObjectName();
$t2_objects = $t2Object->getOrderedObjects($t1_object_id);   //ASSIST_HELPER::arrPrint($t2_objects);
$t2_objects = isset($t2_objects[$t1_object_id]) ? $t2_objects[$t1_object_id] : array();

//TIER3 = STRATGOAL
$t3Object = new LDAL1_ACTIVITY();
$t3_object_type = $t3Object->getMyObjectType();
$t3_object_name = $t3Object->getMyObjectName();
$t3_objects = $t3Object->getOrderedObjects($t1_object_id);
//ASSIST_HELPER::arrPrint($t3_objects);

//TIER4 = STRATFOCUS
/*$t4Object = new IDP1_STRATRESULT();
$t4_object_type = $t4Object->getMyObjectType();
$t4_object_name = $t4Object->getMyObjectName();
$t4_objects = $t4Object->getOrderedObjects(0,$t1_object_id);
 */

 //HEADER

echo "
<table class='tbl-container not-max'><tr><td>
<table class=tbl-tree>
	<tr>
		<td class=uni-pos></td>
		<td></td>
	</tr>";
//CONTRACT DETAILS
//$count = (isset($del_objects['main']) ? count($del_objects['main'])." ".$helper->getObjectName("deliverable",true) : "<span class=required>0 ".$helper->getObjectName("deliverable",true)."</span>");
$count = count($t2_objects);
$can_confirm = $count>0 ? $can_confirm : false;
$count = ($count>0 ? $count." ".$helper->getObjectName($t2_object_name.($count!=1 ? "s" : "")) : "<span class=required>0 ".$helper->getObjectName($t2_object_name."s")."</span>");
$tier = 1;
$displayObject->drawTreeTier("tier1",1,$t1_object_details,$page_action);
if(validateTierLevel($tier+1)) {

	echo "
		<tr>
			<td class=uni-pos object_id=0 object_type=".$t2_object_type.">

				&nbsp;<span class='count float'>".$count."</span></td>
			<td></td>
		</tr>
	";
}
		//TIER2
		foreach($t2_objects as $t2_id => $t2_obj) {
			$tier = 2;
				$count = (isset($t3_objects[$t2_id]) ? count($t3_objects[$t2_id]) : 0);
				$actual_count = $count;
				$can_confirm = $count>0 ? $can_confirm : false;
				$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName($t3_object_name.($count!=1?"s":"")).($count==0 ? "</span>" : "");
			$displayObject->drawTreeTier("tier2",2,$t2_obj,$page_action);
			if(validateTierLevel($tier+1)) {
				if($actual_count==0) {
					echo "
					<tr>
						<td></td>
						<td class=bi-pos object_id=0 object_type=".$t3_object_type.">
							&nbsp;<span class='float count'>$count</span></td>
						<td></td>
					</tr>
					";
				} else {

				}
			}
				if(validateTierLevel($tier) && isset($t3_objects[$t2_id])) {
					$tier = 3;
					foreach($t3_objects[$t2_id] as $t3_id => $t3_obj) {
						$count = (isset($t4_objects[$t3_id]) ? count($t4_objects[$t3_id]) : 0);
						$can_confirm = $count>0 ? $can_confirm : false;
						if(!validateTierLevel($tier+1)){ $draw_level = $tier+1; } else { $draw_level = $tier; }
						$displayObject->drawTreeTier("tier".$draw_level,3,$t3_obj,$page_action);
						if(validateTierLevel($tier+1)) {
							$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName($t4_object_name,$count!=1).($count==0 ? "</span>" : "");
							echo "
							<tr>
								<td></td>
								<td></td>
								<td class=tri-pos object_id=0 object_type=".$t4_object_type.">
									&nbsp;<span class='float count'>$count</span>
								</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							";
						}
						$tier++;
						if(validateTierLevel($tier) && isset($t4_objects[$t3_id])) {
							foreach($t4_objects[$t3_id] as $t4_id => $t4_obj){
								$displayObject->drawTreeTier("tier4",4,$t4_obj,$page_action);
							}
						}
						$tier--;
					}
				}
			/*} else {
				echo "
				<tr>
					<td></td>
					<td class=bi-pos object_id=0 object_type=action><button class='action-button add-btn' parent_id=".$di.">Add ".$helper->getObjectName("action")."</button> <span class='float count'>$count</span></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				";
				if(isset($act_objects[$di])) {
					foreach($act_objects[$di] as $ai => $act){
						drawTreeTier("action",3,$act);
					}
				}
			}*/
		}
	//}//endif count del_objects per cate
//}//endif category foreach loop
?> <!--
	<tr>
		<td class='tree-icon uni-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='parent bi-pos'>Main Deliverable / Parent Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td class='tree-icon bi-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='sub-parent tri-pos'>Sub Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td class='tree-icon tri-icon'><button class='ibutton sub-btn'>X</button></td>
		<td class='child quad-pos'>Action</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td class='tree-icon uni-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='parent bi-pos'>Main Deliverable / Parent Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td class='tree-icon bi-icon'><button class='ibutton sub-btn'>X</button></td>
		<td class='child tri-pos'>Action</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td>C</td>
		<td>PD</td>
		<td>SD</td>
		<td>A</td>
		<td>V/E</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	-->
</table>
</td></tr><tr><td>
	<div id=confirmation_status>

</div>

</td></tr>
<tr><td>

<?php
//$footer = $displayObject->getPageFooter($helper->getGoBack("new_contract_edit.php"));
//echo $footer['display'];
//$js.=$footer['js'];
?>

</td></tr>
</table>

<?php

/*******************
 * PARENT
 */
$view_object_type = $t1_object_type;
$view_object_id = $t1_object_details['id'];
$view_object = $t1_object_details;
$viewObject = $t1Object;
$parentObject = null;
$parent_object_id = 0;
$parent_object_type = "";
?>
<div id=dlg_edit_parent class=dlg-child title="Edit <?php echo $helper->getObjectName($t1_object_name); ?>">
	<span class=float><button class=close-btn id=edit_parent>Close</button></span>
	<h2><?php echo $helper->getObjectName($view_object_type); ?> Details</h2><?php
	$data = $displayObject->getObjectForm($view_object_type, $viewObject, $view_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
	echo $data['display'];
	$js.=$data['js'];
	?>
</div>






<div id=dlg_child class=dlg-child title="">
		<iframe id=ifr_form_display style="border:0px solid #000000;" src="">

		</iframe>
</div>


<script type=text/javascript>
$(function() {
	<?php 	echo $js;

	?>



	<?php echo "
	var object_names = [];
	".(validateTierLevel(1) ? "
	object_names['tier1'] = '".$helper->getObjectName($t1_object_name)."';
	object_names['".$t1_object_type."'] = '".$helper->getObjectName($t1_object_name)."';
	":"")."
	".(validateTierLevel(2) ? "
	object_names['tier2'] = '".$helper->getObjectName($t2_object_name)."';
	object_names['".$t2_object_type."'] = '".$helper->getObjectName($t2_object_name)."';
	":"")."
	".(validateTierLevel(3) ? "
	object_names['tier3'] = '".$helper->getObjectName($t3_object_name)."';
	object_names['".$t3_object_type."'] = '".$helper->getObjectName($t3_object_name)."';
	object_names['tier4'] = '".$helper->getObjectName($t3_object_name)."';
	":"")."
	".(validateTierLevel(4) ? "
	object_names['".$t4_object_type."'] = '".$helper->getObjectName($t4_object_name)."';
	":"")."

	var object_types = [];
	".(validateTierLevel(1) ? "
	object_types['tier1'] = '".$t1_object_type."';
	":"")."
	".(validateTierLevel(2) ? "
	object_types['tier2'] = '".$t2_object_type."';
	":"")."
	".(validateTierLevel(3) ? "
	object_types['tier3'] = '".$t3_object_type."';
	object_types['tier4'] = '".$t3_object_type."';
	":"")."
	".(validateTierLevel(4) ? "
	":"")."

	";
	?>
	var my_window = AssistHelper.getWindowSize();
	if(my_window['width']>800) {
		var my_width = 850;
	} else {
		var my_width = 800;
	}
	var my_height = my_window['height']-50;


	$("table.tbl-tree").find("td.tree-icon").addClass("right").prop("width","20px");
	//Set the column spanning for each object description cell based on their position in the hierarchy but excluding the last two columns
	$("table.tbl-tree").find("td.cate-pos, td.td-line-break").prop("colspan","7");
	$("table.tbl-tree").find("td.uni-pos").not("td.center").prop("colspan","4");
	$("table.tbl-tree").find("td.bi-pos").not("td.center").prop("colspan","3");
	$("table.tbl-tree").find("td.tri-pos").not("td.center").prop("colspan","2");
	$("table.tbl-tree").find("td.quad-pos").not("td.center").prop("colspan","1");

	//Action positional buttons
	$("button.sub-btn").button({
		icons: {primary: "ui-icon-carat-1-sw"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
	//Parent positional buttons
	$("button.expand-btn").button({
		icons: {primary: "ui-icon-triangle-1-se"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});

	//Edit button
	$("button.edit-btn").button({
		icons: {primary: "ui-icon-pencil"},
	}).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
*/
	//View button
	$("button.view-btn").button({
		icons: {primary: "ui-icon-extlink"},
	}).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Add button
	$("button.add-btn").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css("color","#009900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"25px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Button styling and positioning
	$("button.ibutton:not(.sub-btn)").hover(
		function() {
			$(this).css({"border-width":"0px","background":"url()"})
		},
		function() {}
	);
	$("button.ibutton").each(function() {
		if($(this).prev().hasClass("ibutton")) {
			$(this).css("margin-left","-5px");
		}
		if($(this).next().hasClass("ibutton")) {
			$(this).css("margin-right","-5px");
		}
	});

	//Styling based on object type
	$("table.tbl-tree tr.grand-parent").css({"font-size":"125%","line-height":"150%","background-color":"#FFFFFF"}).find("button").css("font-size","65%");
	$("table.tbl-tree tr.sub_detail_row").css({"vertical-align":"bottom"}).find("button").css("font-size","90%");
	$("table.tbl-tree tr.parent").css("background-color","#DEDEDE").find("td.td-button").css("background-color","#FFFFFF");

	//General styling
	$("span.count").css({"font-size":"80%","margin-top":"-3px"});
	$("table.tbl-tree th").css({"padding":"10px"});


	$("button.action-button").click(function() {
		AssistHelper.processing();
		var i = $(this).parent("td").attr("object_id"); //console.log(i);
		var t = $(this).parent("td").attr("object_type"); //console.log(t);
		if(!$(this).hasClass("add-btn")) {
			t = object_types[t];
		} //console.log(t);
		var dta = "object_type="+t+"&object_id="+i;
//		AssistHelper.hideProcessing();
		if(t==object_types['tier1'] && !$(this).hasClass("view-btn")) {
			$("div#dlg_edit_contract").dialog("open");
			$("div#dlg_edit_contract button.close-btn").blur();
		} else {
			$dlg = $("div#dlg_child");
			var act = "view";
			if($(this).hasClass("edit-btn")) {
				act = "edit";
			} else if($(this).hasClass("add-btn")) {
				act = "add";
			}
			var obj = t.toLowerCase();
			//var obj = "deliverable";
			//if(t=="contract" || t=="action") { obj = t; }
			//var obj = (t=="action" ? "action" : "deliverable");
			var heading = AssistString.ucwords(act)+" "+object_names[t];
			var parent_id = $(this).attr("parent_id");
			var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&object_type="+t+"&object_id="+parent_id;

			$dlg.dialog("option","title",heading);
			//$dlg.find("iframe").prop("src",url);
			$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
			//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
			//$dlg.dialog("open");
		}
	});


	$("div button.close-btn").button({
		icons: { primary: "ui-icon-closethick" },
		text: false
	}).click(function(e) {
		e.preventDefault();
		$(this).parent("span").parent("div").dialog("close");
	}).removeClass("ui-state-default").addClass("ui-button-state-default");


	//AssistHelper.hideDialogTitlebar("class","dlg-view");

	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			$(this).dialog('option','width',my_width);
			$(this).dialog('option','height',my_height);
		}
	});





});


function dialogFinished(icon,result) {
	if(icon=="ok") {
		document.location.href = '<?php echo $page_redirect_path."object_id=".$t1_id; ?>&r[]=ok&r[]='+result;
	} else {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon,result);
	}
}

</script>




