<?php
/**
 * To manage the user profile of the IDP module
 * 
 * Created on: October 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * Profile is stored as an array in: $_SESSION[$this->getModRef()]['profile']
 * 
 * Profile array is structured:
 * 	array(
 *  	'financial_year'=>default_int_value,
 * 		'table_rows'=>int value
 *  );
 * 
 */

class LDAL1_PROFILE extends LDAL1 {
	
	
	protected $sections = array("Function","Sub-Function","Activity");
		
	protected $note_whats = array(
		array(
		//Actions
			1=>"All incomplete Actions",          
			2=>"Actions due this week",         
			3=>"Actions overdue or due this week",
			4=>"Actions due today",
			5=>"Actions overdue or due today",
		),array(
		//Deliverables
			1=>"All incomplete deliverables",          
			2=>"Deliverables due this week",         
			3=>"Deliverables overdue or due this week",
			4=>"Deliverables due today",
			5=>"Deliverables overdue or due today",    
			6=>"Incomplete Actions under your deliverable",
			7=>"Actions under your deliverable due this week",
			8=>"Incomplete actions under your deliverable due this week",
		),array(
		//Contracts
			1=>"Incomplete Deliverables under your contract",
			2=>"Deliverables under your contract due this week",
			3=>"Deliverables with incompletable Actions",
		)
	);
	
	
	protected $missed_whats = array(
		1=>"Child owner has missed a deadline"
	);
	
	protected $note_whens = array(
		"DAILY","WEEKLY","FORTNIGHTLY","MONTHLY","QUARTERLY (by calender year)","BY_FIN_YEAR","YEARLY"
	);
	
	protected $note_everies = array(
		array("Daily"),
		array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"),
		array("First Sunday","First Monday","First Tuesday","First Wednesday","First Thursday","First Friday","First Saturday","Second Sunday","Second Monday","Second Tuesday","Second Wednesday","Second Thursday","Second Friday","Second Saturday"),
		array("First Day","Second Day","25th","Second Last Day","Last Day"),
		array("First Day","Second Day","Second Last Day","Last Day"),
		array("First day of Financial Year","Last day of Financial Year","A week before last day of Financial Year"),
		array("First day of year","Start of last week of year","Last day of year","Custom"),
	);
		
	protected $mode;
	
	private $table = "userprofile";
	private $table_fld = "up";
	const MONDAY = 1;
	const TUESDAY = 2;
	const WEDNESDAY = 3;
	const THURSDAY = 4;
	const FRIDAY = 5;
	
	/*
	 * When using this thing, you should look in the $_SESSION's modRef ['profile'] to find the relevant info.
	 * It should contain:
	 * financial_year (id - int);
	 * table_rows (preferred list length - int);
	 * reminders (yes/no - does the user want to receive reminder notifications?)  
	 * remind_freq (daily/weekly)
	 * remind_day (int weekday from 0 - only not-empty if remind_freq is weekly)
	 * remind_what (id - int describing when notifications must happen. Defined herein as $note_whats)
	 * 
	 * ^the above are repeated with the prefix 'missed_' for missed deadline emails. The remind_what is different
	*/
	public function __construct($sections=array(),$note_whats=array(),$missed_whats=array(),$note_whens=array(),$note_everies=array(),$autojob=false,$mode="") {
		parent::__construct();
		$this->getMyProfile($autojob);
		if(count($sections)>0){
			$this->sections = $sections;
		}
		if(count($note_whats)>0){
			$this->note_whats = $note_whats;
		}
		if(count($missed_whats)>0){
			$this->missed_whats = $missed_whats;
		}
		if(count($note_whens)>0){
			$this->note_whens = $note_whens;
		}
		if(count($note_everies)>0){
			$this->note_everies = $note_everies;
		}
		if(strlen($mode)>0){
			$this->mode = $mode;
			$this->table = "usernotes";	
			$this->table_fld = "un";	
		}
	}	
	public function getMode(){
		return $this->mode;
	}	
	public function getSections($head,$i,$opt="") {
		$std = new IDP1_NAMES();	
		$new_names = $std->fetchObjectNames();
		switch($head){
			case "un_section":
			$res = $this->sections[$i];
			$replaced = str_ireplace(array("Action","Deliverable","Contract"),array($new_names['action'],$new_names['deliverable'],$new_names['contract']),$res);
			return $replaced;	
				break;
			case "un_what":
			$res = $this->note_whats[$opt][$i];
			$replaced = str_ireplace(array("Action","Actions","Deliverable","Deliverables","Contract"),array($new_names['action'],$new_names['action']."s",$new_names['deliverable'],$new_names['deliverable']."s",$new_names['contract']),$res);
			return $replaced;	
				break;
			case "un_when":
			return $this->note_whens[$i];
				break;
			case "un_every":
			return $this->note_everies[$opt][$i];
				break;
		}
	}
	public function getTableField() { return $this->table_fld; }
	public function getTableName() { return $this->getDBRef()."_".$this->table; }
	public function getNoteTableName() { return $this->getDBRef()."_usernotes"; }
	public function addObject($data){
		//return $this->setProfile($data);
		return $this->setProfileRule($data);
	}
	public function editObject($data){
		return $this->editProfileRule($data);
	}
	public function deactivateObject($id){
		return $this->deactivateProfileRule($id);
	}
	public function restoreObject($id){
		return $this->restoreProfileRule($id);
	}
	public function deleteObject($id){
		return $this->deleteProfileRule($id);
	}
	/**
	 * This function is called by the controller to save profile rules, as opposed to profile notifications. Ignore the 'approve' in the name
	 */
	public function approveObject($data){
		return $this->setBasicProfile($data);
	}
	/*
	 *	Feed in a number from 0 to 4, in upward hierarchy (0=action, 4=contract) and the 'whats' will be fed out
	 * @param section(int) 
	 *
	*/
	public function getNoteWhats(){
		$std = new IDP1_NAMES();	
		$new_names = $std->fetchObjectNames();
		$res=$this->note_whats;
		$res2 = json_encode($res);
		$res2=str_ireplace(array("Action","Actions","Deliverable","Deliverables","Contract"),array($new_names['action'],$new_names['action']."s",$new_names['deliverable'],$new_names['deliverable']."s",$new_names['contract']),$res2);
		$res = json_decode($res2);
		return $res;
	}
	
	/*
	 *	Feed in a number from 0 to 5 (0=daily, 4=yearly) and the 'whens' will be fed out
	 * @param when = (int) 
	 *
	*/
	public function getNoteWhens($when=""){
		return strlen($when)>0?$this->note_whens[$when]:$this->note_whens;
	}
	
	/*
	 *	Feed in a number from 0 to 5 (0=daily, 4=yearly) and the 'specific whens' will be fed out ()
	 * @param when = (int) 
	 *
	*/
	public function getNoteEveries($when=""){
		$std = new IDP1_NAMES();	
		$new_names = $std->fetchObjectNames();
		return strlen($when)>0?$this->note_everies[$when]:$this->note_everies;
	}
	
	
	/**
	 * Function to read user's latest profile from the database and store in Session variable for later access
	 */	
	public function getMyProfile($autojob = false,$tkid="") {
		if(strlen($tkid)==0) { $up_tkid = $this->getUserID(); } else { $up_tkid = $tkid; }
		$sql = "SELECT yr_strt, financial_year, table_rows FROM ".$this->getTableName()." WHERE up_tkid = '".$up_tkid."' AND up_status = ".IDP1::ACTIVE." ORDER BY up_id DESC LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		//ASSIST_HELPER::arrPrint($_SESSION[$this->getModRef()]);
		//ASSIST_HELPER::arrPrint($row);
		//echo $row;
		if(isset($row) && count($row)>0 && $row != 0) {
			if($autojob == false){
				foreach($row as $key=>$val){
					$_SESSION[$this->getModRef()]['profile'][$key] = $val;
				}
			}
		} else {
			if($autojob == false){$_SESSION[$this->getModRef()]['profile'] = array();}
		}
		if(strlen($tkid)>0) {
			return $row;
		}
	} 
		
	/** 
	 * Function to check if a profile has been set and to update if necessary
	 */
	public function checkMyProfile() {
		if( count($_SESSION[$this->getModRef()]['profile']) == 0) {
			$this->getMyProfile();
		}
	}

	/**
	 * Function to read user profile for the number of rows to display in a table
	 */
	public function getProfileTableLength() {
		$this->checkMyProfile();
		$p = $_SESSION[$this->getModRef()]['profile'];
		if(!isset($p['table_rows']) || !$this->checkIntRef($p['table_rows'])) {
			return $this->mainProfileActionsToDisplay();
		} else {
			return $p['table_rows'];
		}
	}
	
	/**
	 * Function to read user profile for the default financial year
	 */
	public function getProfileFinancialYear() {
		$this->checkMyProfile();
		$p = $_SESSION[$this->getModRef()]['profile'];
		if(!isset($p['financial_year']) || !$this->checkIntRef($p['financial_year'])) {
			return "X";
		} else {
			return $p['financial_year'];
		}
	}
	
	/**
	 * Function to read user profile for the financial year's start day
	 */
	public function getProfileFnYrStartDay() {
		$this->checkMyProfile();
		$p = $_SESSION[$this->getModRef()]['profile'];
		if(!isset($p['yr_strt'])) {
			return "X";
		} else {
			return $p['yr_strt'];
		}
	}
	/**
	 * Function returning all of the user profiles on the database in form [tkid]=>array([profile item]=>value)
	 */
	public function getAllProfiles(){
		$sql = "SELECT * FROM ".$this->getTableName()." ORDER BY up_tkid";
		$res = $this->mysql_fetch_all($sql);
		/*
		foreach($res as $pro){
			$data[$pro['tkid']]=unserialize($pro['profile']);
		}
		*/
		return $data;
	}
	
	/*
	 * 
	 * ________Usernotes functions___________________________
	 * 
	 */

	 public function getNotes($tkid=""){
	 	$mode = strlen($tkid)>0?" un_tkid = $tkid AND ":" un_tkid = ".$this->getUserID()." AND ";
	 	$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$mode." un_status <> ".IDP1::DELETED." ORDER BY un_id ASC";
		$res = $this->mysql_fetch_all($sql);
		return $res;
	/*
		foreach($res as $index=>$thing){
			$res[$thing['un_id']]=$thing;
			unset($res[$index]);
		}
		return $res;
	 
	 */
	 }
	 	
	/*
	 * 
	 * ________SET functions___________________________
	 * 
	 */
	
	/*
	 * Function to set current user's profile in the database
	 */
	public function setProfile($data, $tkid=""){
		if(strlen($tkid)==0) { $up_tkid = $this->getUserID(); } else { $up_tkid = $tkid; }
		foreach($data as $index=>$thing){
			if($index == "reminders" || $index == "missed_reminders"){
				if($thing == 1){
					$data[$index] = "yes";
				}else{
					$data[$index] = "no";
				}
			}else if($index == "remind_freq"){
				if($thing == 1){
					$data[$index] = "weekly";
				}else{
					$data[$index] = "daily";
					$data['remind_day']="";
				}
			}else if($index == "missed_remind_freq"){
				if($thing == 1){
					$data[$index] = "weekly";
				}else{
					$data[$index] = "daily";
					$data['missed_remind_day']="";
				}				
			}
		}
		if(isset($_SESSION[$this->getModRef()]['profile']) && count($_SESSION[$this->getModRef()]['profile']) > 0){
			//Use the force, Luke
				//Make an array with all new profile data in form key=>value (with the up_profile data as serialized array), and use convertArrayToSql()
			$old_pro = "SELECT up_profile FROM ".$this->getTableName()." WHERE up_tkid = '$up_tkid'";
			$old_pro_dta = $this->mysql_fetch_one_value($old_pro, "up_profile");
			$o_data = unserialize($old_pro_dta);
			foreach($data as $key=>$val){
				$o_data[$key]=$val;
			}
			$sql_data = serialize($o_data);
			$sql = "UPDATE ".$this->getTableName()." SET up_profile = '$sql_data', up_tkid = '$up_tkid', up_status = ".IDP1::ACTIVE.", up_insertdate = NOW(), up_insertuser = '".$this->getUserID()."' WHERE up_tkid = '$up_tkid'";
			$res = $this->db_update($sql);
			return $res;
		}else{
			$sql_data = serialize($data);
			$sql = "INSERT INTO ".$this->getTableName()." SET up_profile = '$sql_data', up_tkid = '$up_tkid', up_status = ".IDP1::ACTIVE.", up_insertdate = NOW(), up_insertuser = '".$this->getUserID()."'";
			$res = $this->db_insert($sql);
			return $res;
		}
		$this->getMyProfile(false,"");
	}	
	/**
	 * Function for setting up basic profile rules
	 */
	public function setBasicProfile($data){
		$data2 = $this->convertArrayToSQL($data);
		$osql = "SELECT * FROM ".$this->getTableName()." WHERE up_tkid = '".$this->getUserID()."' AND up_status = ".IDP1::ACTIVE;
		$odata = $this->mysql_fetch_all($osql);
		$vodata = $this->mysql_fetch_all($osql);
		foreach($odata[0] as $key=>$val){
			if($key == "yr_strt" || $key == "financial_year" || $key == "table_rows"){
				if(isset($data[$key])){
					$odata[0][$key]=$data[$key];
				}else{
					$raw[$key]=$data[$key];
					unset($odata[0][$key]);
				}
			}else{
				unset($odata[0][$key]);
			}
		}
		$data3 = $this->convertArrayToSQL($odata[0]);
		$sql = "UPDATE ".$this->getTableName()." SET $data3 , up_status = ".IDP1::ACTIVE.", up_insertdate = NOW(), up_insertuser = '".$this->getUserID()."' WHERE up_tkid= '".$this->getUserID()."' ";
		$sql2 = "INSERT INTO ".$this->getTableName()." SET $data2 , up_status = ".IDP1::ACTIVE.", up_insertdate = NOW(), up_tkid= '".$this->getUserID()."', up_insertuser = '".$this->getUserID()."'";
		$Xsql = count($odata)>0?$sql:$sql2;
		$usrnm=$this->getAUserName($this->getUserID());
		$change = array("user"=>$usrnm);
		if(count($odata)>0){
			foreach($data as $key=>$val){
				switch ($key) {
					case 'yr_strt':
						$change['financial_year_start_day']=array("to"=>$data[$key],"from"=>$vodata[0][$key]);
						//$txt[] = "Financial Year start day changed to '".$data[$key]."' from '".$vodata[0][$key]."'";
						break;
					case 'financial_year':
						$change['financial_year']=array("to"=>$data[$key],"from"=>$vodata[0][$key]);
						//$txt[] = "Default Financial Year changed to '".$data[$key]."' from '".$vodata[0][$key]."'";
						break;
					case 'table_rows':
						$change['table_rows']=array("to"=>$data[$key],"from"=>$vodata[0][$key]);
						//$txt[] = "List length changed to '".$data[$key]."' from '".$vodata[0][$key]."'";
						break;
				}
			}
		}else{
			foreach($data as $key=>$val){
				switch ($key) {
					case 'yr_strt':
						$change['financial_year_start_day']=array("to"=>$data[$key],"from"=>"");
						//$txt[] = "Financial Year start day set to '".$data[$key]."'";
						break;
					case 'financial_year':
						$change['financial_year']=array("to"=>$data[$key],"from"=>"");
						//$txt[] = "Default Financial Year set to '".$data[$key]."'";
						break;
					case 'table_rows':
						$change['table_rows']=array("to"=>$data[$key],"from"=>"");
						//$txt[] = "List length set to '".$data[$key]."'";
						break;
				}
			}
		}
		$changestr = "".implode(", ",$txt).".";
//		return $Xsql;
		$res = $this->db_query($Xsql);
		if($res && $res >0){
			$log_var = array(
						'object_id'	=> $res,
						//'changes'	=> array("user"=>$usrnm,"response"=>$changestr),
						'changes'	=> $change,
						'log_type'	=> IDP1_LOG::EDIT,
						'section'	=> 'basic',
					);
			$this->addActivityLog("userprofile", $log_var);
			return array("ok","Profile succesfully updated");
		}else{
			return array("error",$res);
		}
	}
	
	/**
	 * Function to create a rule for personal notifications. Takes an array (in query string format) containing id,name,section(A,D,C),what,when,and every. See this class for details, and manage_profile_notifications.php
	 */
	public function setProfileRule($data){
		$custom = false;
		if(isset($data['add_date']) && count($data['add_date'])>0 && $data['when_sel'] == '6'){
			$custom = true;
			$data['un_custom']=$data['add_date'];
			unset($data['add_date']);
			unset($data['every_sel']);
		}
		foreach($data as $key=>$val){
			switch($key){
				case "id":
					$data['un_id']=$val;
					break;
				case "name":
					$data['un_name']=mysql_real_escape_string($val);
					break;
				case "sect_sel":
					$data['un_section']=$val;
					break;
				case "what_sel":
					$data['un_what']=$val;
					break;
				case "when_sel":
					$data['un_when']=$val;
					break;
				case "every_sel":
					$data['un_every']=$val;
					break;
			}
			if($key != "un_custom"){unset($data[$key]);}
			if($custom){
				$data['un_every']='3';
			}
		}
		$data['un_tkid']= $this->getUserID();
		$data['un_insertuser']= $this->getUserID();
		$data['un_status']= IDP1::ACTIVE;
		$data_sql = $this->convertArrayToSQL($data);
		$sql = "INSERT INTO ".$this->getNoteTableName()." SET $data_sql, un_insertdate = NOW()";
		$res = $this->db_insert($sql);
		$tkid = $this->getUserID();
		if(strlen($res)>0){
			$log_var = array(
						'object_id'	=> $res,
						'changes'	=> array("user"=>$this->getAUserName($tkid),"changes"=>"New user notification '".$data['un_name']."' set up."),
						'log_type'	=> IDP1_LOG::CREATE,
						'section'	=> 'note',
					);
			$this->addActivityLog("userprofile", $log_var);
		}
		return strlen($res)>0?array("ok","Notification created successfully",'add_log'=>false):array("error","Error creating notification. Please re-check your data");
	}

	/**
	 * Function to modify a rule for personal notifications. Takes an array (in query string format) containing id,name,section(A,D,C),what,when,and every. See this class for details, and manage_profile_notifications.php
	 */
	public function editProfileRule($data){
		$custom = false;
		$id = $data['id'];
		if(isset($data['edit_date']) && strlen($data['edit_date'])>0 && $data['when_sel'] == '6'){
			$data['un_custom']=$data['edit_date'];
			$data['un_every']='3';
			$custom = true;
			unset($data['edit_date']);
			unset($data['every_sel']);
		}
		foreach($data as $key=>$val){
			switch($key){
				case "name":
					$data['un_name']=mysql_real_escape_string($val);
					break;
				case "sect_sel":
					$data['un_section']=$val;
					break;
				case "what_sel":
					$data['un_what']=$val;
					break;
				case "when_sel":
					$data['un_when']=$val;
					break;
				case "every_sel":
					$data['un_every']=$val;
					break;
			}
			if($key != "un_custom"){unset($data[$key]);}
			if($custom){
				$data['un_every']='3';
			}
		}
		$data['un_tkid']= $data['un_insertuser']= $this->getUserID();
		$data['un_status']= IDP1::ACTIVE;
		$data_sql = $this->convertArrayToSQL($data);
		$osql = "SELECT * FROM ".$this->getNoteTableName()." WHERE un_id = $id";
		$ores = $this->mysql_fetch_all($osql);
		$sql = "UPDATE ".$this->getNoteTableName()." SET $data_sql, un_insertdate = NOW() WHERE un_id = $id";
		$res = $this->db_update($sql);
		foreach($data as $key=>$val){
			if($key == "un_what" && $data['un_section'] != $ores[0]['un_section']){
				$changes[$key]['to']=$val;
				$changes[$key]['from']=$ores[0][$key];
			}else if(($key == "un_every" || $key == "un_custom") && $data['un_when'] != $ores[0]['un_when']){
				$changes[$key]['to']=$val;
				$changes[$key]['from']=$ores[0][$key];
			}else if($val != $ores[0][$key]){
				$changes[$key]['to']=$val;
				$changes[$key]['from']=$ores[0][$key];
			}
		}
		//return array("info",$changes);
		$schange = false;
		$wchange = false;
		foreach($changes as $key=>$val){
			switch ($key) {
				case 'un_name':
					$txt['notification_name']=array("to"=>$val['to'],"from"=>$val['from']);
					//$txt['notification_name']="Name changed to '".$val['to']."' from '".$val['from']."'";
					break;
				case 'un_section':
					$txt['notification_section']=array("to"=>$this->getSections($key,$val['to']),"from"=>$this->getSections($key,$val['from']));
					//$txt['notification_section']="Section changed to '".$this->getSections($key,$val['to'])."' from '".$this->getSections($key,$val['from'])."'";
					$schange = true;
					break;
				case 'un_what':
					if($schange){
						$to = $this->getSections($key,$val['to'],$changes['un_section']['to']);
					}else{
						$to = $this->getSections($key,$val['to'],$ores[0]['un_section']);
					}
					$txt['notification_trigger']=array("to"=>$to,"from"=>$this->getSections($key,$val['from'],$ores[0]['un_section']));
					//$txt['notification_trigger']="'What' trigger changed to '".$to."' from '".$this->getSections($key,$val['from'],$ores[0]['un_section'])."'";
					break;
				case 'un_when':
					$wchange = true;
					$txt['notification_time_trigger']=array("to"=>$this->getSections($key,$val['to']),"from"=>$this->getSections($key,$val['from']));
					//$txt['notification_time_trigger']="'When' trigger changed to '".$this->getSections($key,$val['to'])."' from '".$this->getSections($key,$val['from'])."'";
					break;
				case 'un_every':
					if($wchange){
						$to = $this->getSections($key,$val['to'],$changes['un_when']['to']);
					}else{
						$to = $this->getSections($key,$val['to'],$ores[0]['un_when']);
					}
					$txt['notification_frequency']=array("to"=>$to,"from"=>$this->getSections($key,$val['from'],$ores[0]['un_when']));
					//$txt['notification_frequency']="'On every' trigger changed to '".$to."' from '".$this->getSections($key,$val['from'],$ores[0]['un_when'])."'";
					break;
				case 'un_custom':
					$txt['notification_custom_time']=array("to"=>$val['to'],"from"=>$val['from']);
					//$txt['notification_custom_time']="Custom date changed to '".$val['to']."' from '".$val['from']."'";
					break;
				default:
					
					break;
			}
		}
		$tkid = $this->getUserID();
		//$txt = implode(", ",$txt);
		$usrnm = $this->getAUserName($tkid);
		$txt['user']=$usrnm;
		if(strlen($res)>0){
			$changestr = "Notification '".$data['un_name']."' modified: ".$txt.".";
			$log_var = array(
						'object_id'	=> $res,
						//'changes'	=> array("user"=>$usrnm,"changes"=>$changestr),
						'changes'	=> $txt,
						'log_type'	=> IDP1_LOG::EDIT,
						'section'	=> 'note',
					);
		//	return array("info",$txt,$changestr);
			$this->addActivityLog("userprofile", $log_var);
		}
		return strlen($res)>0?array("ok",$this->getObjectName("notification")." '".$name."' modified successfully"):array("error","Error modifying notification. Please re-check your data");
		//return $sql;
	}
	
	public function deactivateProfileRule($id){
		$id = $id['id'];
		$sql = "UPDATE ".$this->getNoteTableName()." SET un_status = ".IDP1::INACTIVE.", un_insertdate = NOW() WHERE un_id = $id";
		$res = $this->db_update($sql);
		$sql2 = "SELECT un_name FROM ".$this->getNoteTableName()." WHERE un_id = $id";
		$name = $this->mysql_fetch_one_value($sql2, "un_name");
		if(strlen($res)>0){
			$tkid = $this->getUserID();
			$log_var = array(
						'object_id'	=> $id,
						'changes'	=> array("user"=>$this->getAUserName($tkid),"response"=>"|notification| '".$name."' |deactivated|."),
						'log_type'	=> IDP1_LOG::DEACTIVATE,
						'section'	=> 'note',
					);
			$logres = $this->addActivityLog("userprofile", $log_var);
		}
		if($res && $res >0){
			return array("ok",$this->getObjectName("notification")." succesfully deactivated");
		}else{
			return array("error",array($res,$logres));
		}
	}

	public function restoreProfileRule($id){
		$id = $id['id'];
		$sql = "UPDATE ".$this->getNoteTableName()." SET un_status = ".IDP1::ACTIVE.", un_insertdate = NOW() WHERE un_id = $id";
		$res = $this->db_update($sql);
		$sql2 = "SELECT un_name FROM ".$this->getNoteTableName()." WHERE un_id = $id";
		$name = $this->mysql_fetch_one_value($sql2, "un_name");
		if(strlen($res)>0){
			$tkid = $this->getUserID();
			$log_var = array(
						'object_id'	=> $id,
						'changes'	=> array("user"=>$this->getAUserName($tkid),"response"=>"|notification| '".$name."' |restored|."),
						'log_type'	=> IDP1_LOG::RESTORE,
						'section'	=> 'note',
					);
			$logres = $this->addActivityLog("userprofile", $log_var);
		}
		if($res && $res >0){
			return array("ok",$this->getObjectName("notification")." succesfully restored");
		}else{
			return array("error",$res);
		}
	}
	

	public function deleteProfileRule($id){
		$id = $id['id'];
		$sql = "UPDATE ".$this->getNoteTableName()." SET un_status = ".IDP1::DELETED.", un_insertdate = NOW() WHERE un_id = $id";
		$res = $this->db_update($sql); 
		$sql2 = "SELECT un_name FROM ".$this->getNoteTableName()." WHERE un_id = $id";
		$name = $this->mysql_fetch_one_value($sql2, "un_name");
		if(strlen($res)>0){
			$tkid = $this->getUserID();
			$log_var = array(
						'object_id'	=> $id,
						'changes'	=> array("user"=>$this->getAUserName($tkid),"response"=>"|notification| '".$name."' |deleted|."),
						'log_type'	=> IDP1_LOG::DELETE,
						'section'	=> 'note',
					);
			$logres = $this->addActivityLog("userprofile", $log_var);
		}
		if($res && $res >0){
			return array("ok",$this->getObjectName("notification")." succesfully removed");
		}else{
			return array("error",$res);
		}
	}
	
	public function __destruct() {
		parent::__destruct();
	}
		
}	