<?php
/**
 * To manage any MASTER FILE classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class LDAL1_MASTER extends LDAL1 {

    private $master_list = "";

    public function __construct($ml="",$modref="") {
        parent::__construct($modref);
		$this->master_list = $ml;
		$this->fields = $this->getFields();
    }


/**************
 * FINANCIAL YEARS
 */
	public function getActiveItems($id="") {
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$rows = $masterObject->getActive($id);
		return $rows;
	}
    public function getActiveItemsFormattedForSelect($ids="") {
    	return $this->formatRowsForSelect($this->getActiveItems($ids),"value");
    }
	public function getAllItems($id="") {
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$rows = $masterObject->getNotDeleted($id);
		return $rows;
	}
    public function getAllItemsFormattedForSelect($ids="") {
    	return $this->formatRowsForSelect($this->getAllItems($ids),"value");
    }
	public function getFields(){
		$fields = array();
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$fields['id'] = $masterObject->getFld("ID");
		$fields['name'] = $masterObject->getFld("NAME");
		$fields['sort'] = $masterObject->getFld("SORT");
		$fields['table'] = "assist_".$this->getCmpCode()."_".$masterObject->getFld("TABLE");
		unset($masterObject);
		return $fields;
	}
    public function getItemsForReport() {
    	$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$contractObject = new LDAL1_CONTRACT();
    	return $masterObject->getLinkedForReport($contractObject->getTableName(),$contractObject->getTableField()."_financial_year_id");
    }
	public function getSortBy() {
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		return $masterObject->getSortBy("",true);
	}
}


?>