<?php
include("inc_ignite.php");



class Query
{
    private $where;
    private $tasktkid;
    private $taskadduser;
    private $taskurgencyid;
    private $tasktopicid;
    private $taskstatusid;
    private $datecreatedFrom;
    private $datecreatedTo;
    private $datecreatedopt;
    private $deadlinedateopt;
    private $deadlineFrom;
    private $deadlineTo;
    private $graphtype;
    private $groupby;
    private $field_filter;
    private $from;
    private $dbref;
    private $cmpcode;
    private $statuses = array();
    private $data;
    private $result;
    private $udf;
    private $invalidXML;
    private $connection;
    private $drawpie;
    private $statusTitle;
    private $allowedStatus;
    private $currentPage;
    private $offset;
    private $rowsPerPage;
    private $maxPages;
    private $numrows;
    public function __construct()
    {
        $this->tasktkid = $_POST['graphtasktkid'];
        $this->taskadduser = $_POST['graphtaskadduser'];
        $this->taskurgencyid = $_POST['graphtaskurgencyid'];
        $this->tasktopicid = $_POST['graphtasktopicid'];
        $this->taskstatusid = $_POST['graphtaskstatusid'];
        $this->datecreatedFrom = $_POST['graphdatecreatedFrom'];
        $this->datecreatedTo = $_POST['graphdatecreatedTo'];
        $this->datecreatedopt = $_POST['graphdatecreatedopt'];
        $this->deadlinedateopt = $_POST['graphdeadlinedateopt'];
        $this->deadlineFrom = $_POST['graphdeadlineFrom'];
        $this->deadlineTo = $_POST['graphdeadlineTo'];
        $this->graphtype = $_POST['graphtype'];
        $this->groupby = $_POST['groupby'];
        $this->field_filter = $_POST['field_filter'];
        $this->udf = $_POST['udfs'];
        $this->udfvalue = $_POST['udfvalue'];
        $this->currentPage = $_POST['currentPage'];
        $this->rowsPerPage = $_POST['rowsPerPage'];
        $this->maxPages = $_POST['maxPages'];
        $this->offset =  $_POST['offset'];
        $this->dbref=strtolower($_SESSION['dbref']);
        $this->cmpcode = $_SESSION['cc'];
        $this->setWhereClause();
        $this->setFromClause();
        $this->invalidXML = false;
        $this->setStatusTitle();
        $this->setLimitVars();
        $this->setCurrentPage();

    }
    public function getNumRows()
    {
        return $this->numrows;
    }
    public function getMaxPages()
    {
        return $this->maxPages;
    }
    public function setMaxPages()
    {
        $this->maxPages = ceil($this->numrows/$this->rowsPerPage);
        if($this->maxPages == 0)
            $this->maxPages = 1;
    }
    public function getOffSet()
    {
        return $this->offset;
    }
    public function getRowsPerPage()
    {
        return $this->rowsPerPage;
    }
    public function getDeadlineFrom()
    {
        return $this->deadlineFrom;
    }
    public function getDeadlineTo()
    {
        return $this->deadlineTo;
    }
    public function getGraphType()
    {
        return $this->graphtype;
    }
    public function getGroupBy()
    {
        return $this->groupby;
    }
    public function getfieldFilter()
    {
        return $this->field_filter;
    }
    public function getUDF()
    {
        return $this->udf;
    }
    public function getUDFValue()
    {
        return $this->udfvalue;
    }
    public function getDateCreatedTo()
    {
        return $this->datecreatedTo;
    }
    public function getDateCreatedOpt()
    {
        return $this->datecreatedopt;
    }
    public function getDeadlineDateOpt()
    {
        return $this->deadlinedateopt;
    }
    public function getTaskTkId()
    {
        return $this->tasktkid;
    }
    public function getTaskAddUser()
    {
        return $this->taskadduser;
    }
    public function getTaskUrgencyId()
    {
        return $this->taskurgencyid;
    }
    public function getTaskTopicId()
    {
        return $this->tasktopicid;
    }
    public function getTaskStatusId()
    {
        return $this->taskstatusid;
    }
    public function getDateCreatedFrom()
    {
        return $this->datecreatedFrom;
    }

    public function setLimitVars($offset = 0,$rowsPerPage = 10)
    {
        if(!isset($this->offset))
            $this->offset = $offset;
        if(!isset($this->rowsPerPage))
            $this->rowsPerPage = $rowsPerPage;
    }
    public function setStatusTitle()
    {
        if($this->taskstatusid != 'X')
        {
            $sql = "SELECT value FROM ".$this->dbref."_list_status WHERE pkey=".$this->taskstatusid;
            $this->execQuery($sql);
            $row = mysql_fetch_array($this->result);
            $this->statusTitle = $row['value'];
            mysql_close($this->connection);
        }
    }
    public function getStatusTitle()
    {
        return $this->statusTitle;
    }
    public function getTaskStatus()
    {
        return $this->taskstatusid;
    }
    public function getData()
    {
        return $this->data;
    }
    public function setCurrentPage()
    {
        if(isset($_POST['nextBtn']))
        {
            $this->currentPage = $_POST['currentPage'];
            $this->maxPages = $_POST['maxPages'];
            if($this->currentPage + 1 > $this->maxPages)
                $this->currentPage = $this->maxPages;
            else
                $this->currentPage++;
        }
        else if(isset($_POST['prevBtn']))
        {
            $this->currentPage = $_POST['currentPage'];
            $this->maxPages = $_POST['maxPages'];
            if($this->currentPage - 1 < 1)
                $this->currentPage = 1;
            else
                $this->currentPage--;
        }
        else if(isset($_POST['lastPage']))
        {
            $this->currentPage = $_POST['maxPages'];
        }
        else if(isset($_POST['firstPage']))
        {
            $this->currentPage = 1;
        }
        else
        {
            $this->currentPage = 1;
        }
        $this->offset = ($this->currentPage - 1) * $this->rowsPerPage;
    }
    public function getCurrentPage()
    {
        return $this->currentPage;
    }
    public function getStatuses()
    {
        $sql = "SELECT s.value, s.pkey FROM ".$this->dbref."_list_status s ORDER BY s.sort";
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->statuses[$row['pkey']] = str_replace("&#039;", "", $row['value']);
        }
        mysql_close($this->connection);
        return $this->statuses;
    }
    public function setFromClause()
    {
        if(isset ($this->tasktkid) AND $this->tasktkid != 'X')
        {
            $this->from  .= " INNER JOIN ".$this->dbref."_task_recipients r2 ON (t.taskid=r2.taskid) ";
        }
        if((isset($this->udf) AND $this->udf != 'X') OR (is_numeric($this->groupby)))
        {
            $this->from .= " INNER JOIN assist_".$this->cmpcode."_udf udf ON(t.taskid=udf.udfnum) ";
        }
    }
    public function getFromClause()
    {
        return $this->from;
    }
    public function setWhereClause()
    {
        if(isset ($this->tasktkid) AND $this->tasktkid != 'X')
        {
            $this->where .= " AND r2.tasktkid = '".$this->tasktkid."' ";
        }
        if(isset($this->taskadduser) AND $this->taskadduser != 'X')
        {
            $this->where .= " AND t.taskadduser = '".$this->taskadduser."' ";
        }
        if(isset($this->taskurgencyid) AND $this->taskurgencyid != 'X')
        {
            $this->where .= " AND t.taskurgencyid = $this->taskurgencyid ";
        }
        if(isset($this->tasktopicid) AND $this->tasktopicid != 'X')
        {
            $this->where .= " AND t.tasktopicid = $this->tasktopicid ";
        }

        if(isset($this->datecreatedopt) AND $this->datecreatedopt == 'Exact')
        {
            $fromTimeStamp = strtotime($this->datecreatedFrom);
            $toTimeStamp = strtotime($this->datecreatedTo);
            $begin = mktime(0,0,0,date("n",$fromTimeStamp),date("j",$fromTimeStamp),date("y",$fromTimeStamp));
            $end = mktime(23,59,59,date("n",$toTimeStamp),date("j",$toTimeStamp),date("y",$toTimeStamp));
            $this->where .=" AND t.taskadddate >='".$begin."' AND t.taskadddate <='".$end."' ";
        }
        if(isset($this->deadlinedateopt) AND $this->deadlinedateopt == 'Exact')
        {
            $fromTimeStamp = strtotime($this->deadlineFrom);
            $toTimeStamp = strtotime($this->deadlineTo);
            $begin = mktime(0,0,0,date("n",$fromTimeStamp),date("j",$fromTimeStamp),date("y",$fromTimeStamp));
            $end = mktime(23,59,59,date("n",$toTimeStamp),date("j",$toTimeStamp),date("y",$toTimeStamp));
            $this->where .= " AND t.taskdeadline >='".$begin."' AND t.taskdeadline <= '".$end."' ";
        }
        if(isset($this->udf) && $this->udf != 'X' )
        {
            $sql = "SELECT * FROM assist_".$this->cmpcode."_udfindex x WHERE x.udfiid=".$this->udf;
            $this->execQuery($sql);
            $row = mysql_fetch_array($this->result);
            switch ($row['udfilist'])
            {
                case 'T':
                case 'M':
                    $this->where .= " AND udf.udfvalue LIKE '%".$this->udfvalue."%'";
                    break;
                case 'Y':
                    $this->where .= " AND udf.udfvalue = $this->udfvalue";
                    break;
            }
        }
        if(isset($this->taskstatusid) AND $this->taskstatusid != 'X')
        {
            if($this->taskstatusid == -1)
            {
                $this->where .= " AND (t.taskstatusid != 1 OR t.taskstatusid != 2) ";
            }else
            {
                $this->where .= " AND t.taskstatusid=$this->taskstatusid";
            }

        }
    }
    public function getStatus()
    {
        return $this->taskstatusid;
    }
    public function getWhereClause()
    {
        return $this->where;
    }

    public function execQuery($sql)
    {
        $cmpcode = $this->cmpcode;
        include("inc_db_con.php");
        $this->result = $rs;
        $this->connection = $con;
    }

    public function checkNumRecords()
    {
        if(mysql_num_rows($this->result) > 0)
        {
            return true;
        }
        return false;
    }
    private function getPieData($cond = "")
    {
        $where = $this->getWhereClause();
        $this->data = $this->getStatuses();
        $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".$this->getFromClause()."
                        WHERE 1 ".$where." GROUP BY s.pkey";
        //echo $sql;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            if(array_key_exists($row['pkey'], $this->data))
            {
                if($row['cnt'] > 0)
                    $this->data[$row['pkey']] = array("title"=>$row['value'],"count"=>$row['cnt']);
            }
        }
    }
    private function getActionTopicData($cond = "")
    {
        $sql = "SELECT * FROM ".$this->dbref."_list_topic WHERE ".(empty($cond) ? "" : " (1=1) $cond AND")." yn='Y'";
        $this->execQuery($sql);
        $this->numrows = mysql_num_rows($this->result);
        $sql .= "  LIMIT ".$this->offset.",".$this->rowsPerPage;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['id']] = array('name'=>$row['value']);
        }

        $where = $this->getWhereClause();
        $from = $this->getFromClause();
        foreach($this->data as $tasktopicid => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".(empty($from) ? "":$from)."
                        WHERE t.tasktopicid=$tasktopicid AND ".(empty($where) ? " (1=1) ": " (1=1) $where ")." GROUP BY s.pkey";
            $this->execQuery($sql);
            if($this->taskstatusid != 'X')
            {
                $row = mysql_fetch_array($this->result);
                if($this->graphtype == 'pie')
                {
                    // if($row['cnt'] > 0)
                    //{
                    $this->data[$tasktopicid]['title'] = htmlentities($this->data[$tasktopicid]['name'],ENT_QUOTES,"ISO-8859-1");
                    $this->data[$tasktopicid]['count'] = ($row['cnt'] > 0 ? $row['cnt'] : 0);
                    $this->data['name'] = $row['value'];
                    //}
                }
                else
                {

                    if($row['cnt'] > 0)
                    {
                        $this->data[$tasktopicid][$row['pkey']]['title'] = htmlentities($this->data[$tasktopicid]['name'],ENT_QUOTES,"ISO-8859-1");
                        $this->data[$tasktopicid][$row['pkey']]['count'] = $row['cnt'];
                        //$this->data[$taskadduser][$row['pkey']] = array('title'=> str_replace("&#039;", "'", $row['value']),'count'=>$row['cnt']);
                    }
                    //$this->data['name'] = $row['value'];
                }
            }
            else
            {
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$tasktopicid][$row['pkey']] = array('title'=> htmlentities($row['value'],ENT_QUOTES,"ISO-8859-1"),'count'=>$row['cnt']);
                    }
                }
            }
        }
        if($this->taskstatusid != 'X' AND $this->graphtype != 'pie')
        {
            $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
        }
    }

    private function getActionOwnerData($cond = "")
    {
        $where = $this->getWhereClause();
        $from = $this->getFromClause();
        $sql = "SELECT DISTINCT(t.taskadduser),CONCAT_WS(' ',k.tkname,k.tksurname) AS name
                    FROM ".$this->dbref."_task t ".$from." INNER JOIN assist_".$this->cmpcode."_timekeep k ON (t.taskadduser=k.tkid)
                    WHERE  1 ".($cond == "" ? "" : $cond)." ".$where;
        $this->execQuery($sql);
        $this->numrows = mysql_num_rows($this->result);
        $sql .= "  LIMIT ".$this->offset.",".$this->rowsPerPage;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['taskadduser']] = array('name'=>$row['name']);
        }


        foreach($this->data as $taskadduser => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid )                        
                            ".$from."
                        WHERE t.taskadduser ='".$taskadduser."' ".$where." GROUP BY s.pkey";
            $this->execQuery($sql);
            if($this->taskstatusid != 'X')
            {
                $row = mysql_fetch_array($this->result);
                if($this->graphtype == 'pie')
                {
                    // if($row['cnt'] > 0)
                    // {
                    $this->data[$taskadduser]['title'] = $this->data[$taskadduser]['name'];
                    $this->data[$taskadduser]['count'] = ($row['cnt'] > 0 ? $row['cnt'] : 0);
                    // }
                }
                else
                {
                    //$this->data[$tasktkid] = $this->statuses;
                    $this->data[$taskadduser]['name'] = $cntData['name'];
                    if($row['cnt'] > 0)
                    {
                        $this->data[$taskadduser][$row['pkey']] = array('title'=> htmlentities($row['value'],ENT_QUOTES,"ISO-8859-1"),'count'=>$row['cnt']);
                    }
                }
            }
            else
            {
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$taskadduser][$row['pkey']] = array('title'=> htmlentities($row['value'],ENT_QUOTES,"ISO-8859-1"),'count'=>$row['cnt']);
                    }
                }
            }
        }
        if($this->graphtype != 'pie' AND $this->taskstatusid != 'X')
        {
            $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
        }
        // echo '<pre>';
        //  print_r($this->data);die;
    }

    private function getPersonActionedData($cond = "")
    {
        $where = $this->getWhereClause();
        $from = $this->getFromClause();
        $sql = "SELECT DISTINCT(r.tasktkid),CONCAT_WS(' ',k.tkname,k.tksurname) AS name
                    FROM ".$this->dbref."_task t INNER JOIN ".$this->dbref."_task_recipients r ON(t.taskid=r.taskid)
                        INNER JOIN assist_".$this->cmpcode."_timekeep k ON(r.tasktkid=k.tkid) ".$from." 
                    WHERE 1 ".($cond == "" ? "" : $cond).$where;
        $this->execQuery($sql);
        $this->numrows = mysql_num_rows($this->result);
        $sql .= "  LIMIT ".$this->offset.",".$this->rowsPerPage;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['tasktkid']] = array('name'=>$row['name']);
        }
        $this->statuses = $this->getStatuses();
        foreach($this->data as $tasktkid => $value)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid )
                        INNER JOIN  ".$this->dbref."_task_recipients r ON ( t.taskid = r.taskid ) ".(empty($from) ? "" : $from)."
                        WHERE r.tasktkid ='".$tasktkid."' ".$where." GROUP BY s.pkey";
            $this->execQuery($sql);
            if($this->taskstatusid != 'X')
            {
                $row = mysql_fetch_array($this->result);
                if($this->graphtype == 'pie')
                {
                    // if($row['cnt'] > 0)
                    // {
                    $this->data[$tasktkid]['title'] = htmlentities($this->data[$tasktkid]['name'],ENT_QUOTES,"ISO-8859-1");
                    $this->data[$tasktkid]['count'] = ($row['cnt'] > 0 ? $row['cnt'] : 0);
                    // }
                }else
                {
                    $this->data[$tasktkid] = $this->statuses;
                    $this->data[$tasktkid]['name'] = $value['name'];
                    if($row['cnt'] > 0)
                    {
                        $this->data[$tasktkid][$row['pkey']] = array('title'=> htmlentities($row['value'],ENT_QUOTES,"ISO-8859-1"),'count'=>$row['cnt']);
                    }
                }
            }
            else
            {
                //$this->data[$tasktkid] = $this->statuses;

                $this->data[$tasktkid]['name'] = $value['name'];
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                        $this->data[$tasktkid][$row['pkey']] = array('title'=> htmlentities($row['value'],ENT_QUOTES,"ISO-8859-1"),'count'=>$row['cnt']);
                }
            }
        }

        if($this->taskstatusid != 'X' AND $this->graphtype != 'pie')
        {
            $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
            // $this->removeUnnessaryData();
        }else if($this->taskstatusid != 'X' AND $this->graphtype == 'pie')
        {
            $this->data['name'] = $this->getStatusTitle();
        }

    }
    public function getActionUrgencyData($cond = "")
    {
        $sql = "SELECT * FROM ".$this->dbref."_list_urgency WHERE ".(empty($cond) ? "" : " (1=1) $cond AND")." yn='Y' ORDER BY sort";
        $this->execQuery($sql);
        $this->numrows = mysql_num_rows($this->result);
        $sql .= "  LIMIT ".$this->offset.",".$this->rowsPerPage;
        $this->execQuery($sql);

        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['id']] = array('name'=>$row['value']);
        }
        foreach($this->data as $taskurgencyid => $value)
        {
            $this->data[$taskurgencyid]['name'] = $value['name'];
        }
        $this->statuses = $this->getStatuses();
        $where = $this->getWhereClause();
        $from = $this->getFromClause();
        foreach($this->data as $taskurgencyid => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".(empty($from) ? "":$from)."
                        WHERE t.taskurgencyid=$taskurgencyid AND ".(empty($where) ? " (1=1) ": " (1=1) $where ")." GROUP BY s.pkey";
            $this->execQuery($sql);
            if($this->taskstatusid != 'X')
            {
                $row = mysql_fetch_array($this->result);
                if($this->graphtype == 'pie')
                {
                    //if($row['cnt'] > 0)
                    // {
                    $this->data[$taskurgencyid]['title'] = $this->data[$taskurgencyid]['name'];
                    $this->data[$taskurgencyid]['count'] = ($row['cnt'] > 0 ? $row['cnt'] : 0);
                    // }
                }else
                {
                    $this->data[$taskurgencyid]['name'] = $cntData['name'];
                    if($row['cnt'] > 0)
                    {
                        $this->data[$taskurgencyid][$row['pkey']] = array('title'=> str_replace("&#039;", '', $row['value']),'count'=>$row['cnt']);

                    }
                }
            }
            else
            {
                //$this->data[$taskurgencyid] = $this->statuses;
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$taskurgencyid][$row['pkey']] = array('title'=> str_replace("&#039;", '', $row['value']),'count'=>$row['cnt']);
                        $this->data[$taskurgencyid]['name'] = $this->data[$taskurgencyid]['name'];
                    }
                }
            }
        }
        if($this->taskstatusid != 'X' AND $this->graphtype != 'pie')
        {
            $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
        }
        //  echo '<pre>';
        //   print_r($this->data);die;
    }
    public function getDrawPie()
    {
        return $this->drawpie;
    }
    public function setDrawPie($value = true)
    {
        $this->drawpie = $value;
    }
    public function getUDFTitle($field = null)
    {
        if(!isset($field))
        {
            $field_value = $this->field_filter;
        }else
        {
            $field_value = $field;
        }
        $sql = "SELECT udfvvalue FROM assist_".$this->cmpcode."_udfvalue WHERE udfvid='".$field_value."'";
        $this->execQuery($sql);
        $row = mysql_fetch_array($this->result);
        mysql_close($this->connection);
        return  $row['udfvvalue'];
    }
    public function getUDFData($cond = "")
    {
        $where = $this->getWhereClause().($cond == "" ? "" : $cond);

        $from = $this->getFromClause();

        $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".(empty($from) ? "":$from)."
                        WHERE  ".(empty($where) ? " (1=1) ": " (1=1) $where ")." GROUP BY s.pkey";
        $this->execQuery($sql);

        if($this->taskstatusid != 'X')
        {
            if($this->graphtype == 'pie')
            {

                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$row['pkey']] = array('title'=> $row['value'],'count'=>$row['cnt']);
                    }
                }
                if(is_numeric($this->field_filter))
                    $this->data['name'] = $this->getUDFTitle();
                else
                    $this->data['name'] = $this->field_filter;
                $this->setDrawPie(true);
            }
            else
            {
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$this->field_filter][$row['pkey']] = array('title'=> $row['value'],'count'=>$row['cnt']);
                    }
                }
                if(is_numeric($this->field_filter))
                    $this->data[$this->field_filter]['name'] = $this->getUDFTitle();
                else
                    $this->data[$this->field_filter]['name'] = $this->field_filter;
                $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
            }
        }
        else
        {
            if($this->graphtype == 'pie')
            {
                $this->data = $this->statuses;

                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$row['pkey']] = array('title'=> $row['value'],'count'=>$row['cnt']);
                    }
                }
                if(is_numeric($this->field_filter))
                {
                    $this->data['name'] = $this->getUDFTitle();

                }else
                {
                    $this->data['name'] = $this->field_filter;
                }
                $this->setDrawPie(true);
            }
            else
            {
                $this->data[$this->field_filter] = $this->statuses;
                while($row = mysql_fetch_array($this->result))
                {
                    if($row['cnt'] > 0)
                    {
                        $this->data[$this->field_filter][$row['pkey']] = array('title'=> str_replace("&#039;", '', $row['value']),'count'=>$row['cnt']);
                    }
                }

                if(is_numeric($this->field_filter))
                {
                    $this->data[$this->field_filter]['name'] = $this->getUDFTitle($this->field_filter);
                }
                else
                {
                    $this->data[$this->field_filter]['name'] = $this->field_filter;
                }
                //$this->data[$this->field_filter]['name'] = $this->field_filter;
            }

        }
        // echo '<pre>';
        //   print_r($this->data);die;
    }
    public function getUDFDataAll($cond = "")
    {
        $sql = "SELECT DISTINCT(udf.udfvalue) AS udfvalue,x.udfilist
                FROM assist_".$this->cmpcode."_udf udf INNER JOIN assist_".$this->cmpcode."_udfindex x ON(udf.udfindex=x.udfiid)
            WHERE udf.udfindex=".$this->groupby." AND udf.udfvalue != '' AND udf.udfvalue !='X'";
        $this->execQuery($sql);
        $this->numrows = mysql_num_rows($this->result);
        $sql .= "  LIMIT ".$this->offset.",".$this->rowsPerPage;

        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['udfvalue']] = $this->statuses;
            $this->data[$row['udfvalue']]['udfilist'] = $row['udfilist'];

        }

        $from = $this->getFromClause();
        $where = $this->getWhereClause();
        if($this->taskstatusid != 'X')
        {
            $taskStatusTitle = $this->getStatusTitle();
        }
        // echo '<pre>';
        //print_r($this->data);
        if(!empty($this->data))
        {
            foreach($this->data as $udfvalue => $data)
            {
                $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".(empty($from) ? "" : $from);

                if($data['udfilist'] != 'Y')
                {
                    $udf = " 1 AND udf.udfvalue LIKE '%".$udfvalue."%' ";
                }
                else
                {
                    if($udfvalue != 'X')
                        $udf = " 1 AND udf.udfvalue=".$udfvalue;
                }
                $sql .= " WHERE  $udf ".$where." GROUP BY s.pkey ";

                $this->execQuery($sql);
                if($this->taskstatusid != 'X')
                {

                    $row = mysql_fetch_array($this->result);
                    if($row)
                    {
                        if($row['cnt'] > 0)
                        {
                            if($this->graphtype == 'pie')
                            {
                                if(is_numeric($udfvalue))
                                {
                                    $title = $this->getUDFTitle($udfvalue);
                                }
                                else
                                {
                                    $title = $udfvalue;
                                }
                                $this->data[$udfvalue] = array('title'=>$title,'count'=>$row['cnt']);
                                // $this->data['name'] = ;
                            }
                            else
                            {
                                $this->data[$udfvalue][$row['pkey']] = array('title'=>$udfvalue,'count'=>$row['cnt']);
                                if(is_numeric($udfvalue))
                                {
                                    $this->data[$udfvalue]['name'] = $this->getUDFTitle($udfvalue);

                                }
                                else
                                {
                                    $this->data[$udfvalue]['name'] = $udfvalue;
                                }
                            }
                        }
                    }
                }
                else
                {
                    while($row = mysql_fetch_array($this->result))
                    {
                        $this->data[$udfvalue][$row['pkey']] = array('title'=>$row['value'],'count'=>$row['cnt']);
                    }
                    if(is_numeric($udfvalue))
                    {
                        $this->data[$udfvalue]['name'] = $this->getUDFTitle($udfvalue);
                    }
                    else
                    {
                        $this->data[$udfvalue]['name'] = $udfvalue;
                    }
                    $this->removeInvalidData();
                }

            }

            if($this->taskstatusid != 'X' AND $this->graphtype != 'pie')
            {
                $this->allowedStatus = array($this->taskstatusid=>$this->getStatusTitle());
            }
            else if($this->taskstatusid != 'X' AND $this->graphtype == 'pie')
            {
                $this->removeRedundancyPieData();
            }
        }
        //  echo '<pre>';
        ///  print_r($this->data);
        //  die;
    }
    public function getAllowedStatus()
    {
        return $this->allowedStatus;
    }
    public function removeRedundancyPieData()
    {
        foreach($this->data as $key => $data)
        {
            if(is_array($data) AND !array_key_exists('title', $data))
            {
                unset($this->data[$key]);
            }
        }
    }
    public function removeUnnessaryData()
    {
        foreach($this->data as $key => $data)
        {

            $valid = false;
            foreach($data as $pkey => $cntData)
            {
                if($pkey != 'name' AND !is_array($cntData))
                {
                    unset($this->data[$key][$pkey]);
                }
            }
        }
    }
    public function removeInvalidData()
    {
        foreach($this->data as $key => $data)
        {
            $valid = false;
            foreach($data as $pkey => $cntData)
            {
                if(is_array($cntData))
                {
                    $valid = true;
                }else
                {
                    if($pkey != 'name')
                        unset ($this->data[$key][$pkey]);
                }
            }
            if(!$valid)
            {
                unset($this->data[$key]);
            }
        }
    }

    public function getDataBy()
    {
        switch($this->groupby)
        {
            case 'tasktkid':
                if($this->graphtype == 'pie')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        //generate pie data for only the specified tasktkid
                        $this->getPersonActionedData(" AND r.tasktkid='$this->field_filter' ");
                    }
                    else
                    {
                        //generate pie data for each task/action/activity recipient
                        $this->getPersonActionedData();
                    }
                }
                else if($this->graphtype == '3dcolumn' OR $this->graphtype == '3dbarchart')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getPersonActionedData(" AND r.tasktkid='$this->field_filter' ");
                    }
                    else
                    {
                        $this->getPersonActionedData();
                    }
                }
                break;
            case 'taskadduser':
                if($this->graphtype == 'pie')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        //generate pie data for only the specified taskadduser
                        $this->getActionOwnerData(" AND t.taskadduser='$this->field_filter'");
                    }else
                    {
                        //generate pie data for each task/action/activity owner.
                        $this->getActionOwnerData();
                    }
                    //$this->getPieData();
                }
                else if($this->graphtype == '3dcolumn' OR $this->graphtype == '3dbarchart')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getActionOwnerData(" AND t.taskadduser='$this->field_filter'");
                    }else
                    {
                        $this->getActionOwnerData();
                    }
                }
                break;
            case 'taskurgencyid':
                if($this->graphtype == 'pie')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getActionUrgencyData(" AND id=$this->field_filter");
                    }else
                    {
                        $this->getActionUrgencyData();
                    }
                }
                else if($this->graphtype == '3dcolumn' OR $this->graphtype == '3dbarchart')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getActionUrgencyData(" AND id=$this->field_filter");
                    }
                    else
                    {
                        $this->getActionUrgencyData();
                    }
                }
                break;
            case 'tasktopicid':
                if($this->graphtype == 'pie')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getActionTopicData(" AND id=$this->field_filter");
                    }
                    else
                    {
                        $this->getActionTopicData();
                    }
                }
                else if($this->graphtype == '3dcolumn' OR $this->graphtype == '3dbarchart')
                {
                    if(isset($this->field_filter) AND $this->field_filter != 'X')
                    {
                        $this->getActionTopicData(" AND id=$this->field_filter");
                    }
                    else
                    {
                        $this->getActionTopicData();
                    }
                }
                break;
            default:
                if($this->groupby == "X")
                {
                    if($this->graphtype == 'pie')
                    {
                        $this->getPieData();
                        $this->setDrawPie(true);
                    }else
                    {
                    }
                }
                elseif(is_numeric($this->groupby))
                {
                    if($this->graphtype == 'pie')
                    {
                        if($this->field_filter != 'X' AND !empty($this->field_filter))
                        {
                            if(is_numeric($this->field_filter))
                            {
                                $this->getUDFData(" AND udf.udfvalue=$this->field_filter");
                            }
                            else
                            {
                                $this->getUDFData(" AND udf.udfvalue LIKE '%".$this->field_filter."%'");
                            }
                        }
                        else
                        {

                            $this->getUDFDataAll();
                        }
                    }else if($this->graphtype == '3dcolumn' OR $this->graphtype == '3dbarchart')
                    {
                        if($this->field_filter != 'X' AND !empty($this->field_filter))
                        {
                            if(is_numeric($this->field_filter))
                            {
                                $this->getUDFData(" AND udf.udfvalue=$this->field_filter");
                            }
                            else
                            {
                                $this->getUDFData(" AND udf.udfvalue LIKE '%".$this->field_filter."%'");
                            }
                        }
                        else
                        {
                            $this->getUDFDataAll();
                        }
                    }
                }
                break;
        }
    }

}
class Graph
{
    const AMCHARTS_DIR ="../lib/amcharts-php-0.2.1/amcharts";
    const AMCHART_SETTINGS = "../lib/amcharts-php-0.2.1/amcharts/chart_settings";
    private $type;
    private $query;
    private $data;
    private $statuses;
    private $graph_settings;

    public function  __construct()
    {
        /*
         * graph configuration variable
        */
        $this->graph_settings = array
                (
                '3dcolumn'=>array
                (
                        'name_path'=>self::AMCHARTS_DIR."/amcolumn_1.6.4.2/amcolumn",
                        'settings_file'=>self::AMCHART_SETTINGS.'/vert_amcolumn_settings.xml',
                        'swf_file'=>'amcolumn.swf'
                ),
                '3dbarchart'=>array
                (
                        'name_path'=>self::AMCHARTS_DIR."/amcolumn_1.6.4.2/amcolumn",
                        'settings_file'=>self::AMCHART_SETTINGS.'/hor_amcolumn_settings.xml',
                        'swf_file'=>'amcolumn.swf'
                ),
                'pie'=>array
                (
                        'name_path'=>self::AMCHARTS_DIR."/ampie_1.6.4.1/ampie",
                        'settings_file'=>self::AMCHART_SETTINGS.'/ampie_settings.xml',
                        'swf_file'=>'ampie.swf'
                )
        );

        $this->query = new Query();
        $this->type = $this->query->getGraphType();
        $this->statuses = $this->query->getStatuses();
        $this->query->getDataBy();
        $this->data = $this->query->getData();
        $this->query->setMaxPages();

        $this->getGraph();
    }
    public function isEmpty($data = array())
    {
        foreach($data as $k => $val)
        {
            if(is_array($val))
            {
                return false;

            }
        }
        return  true;
    }
    public function getNumPages()
    {
        return ceil($this->resultsCount/$this->limit);
    }
    public function getPieInXML($data = array())
    {
        if($this->isEmpty($data))
        {
            return "";
        }
        else
        {
            $xml = "<pie>";
            foreach($data as $id => $cntData)
            {
                if($id != 'name')
                {
                    if(is_array($cntData) AND !empty($cntData))
                    {
                        $xml .= "<slice title=\"".$cntData['title']."\">".$cntData['count']."</slice>";
                    }
                    else
                    {
                        $xml .= "<slice title=\"".$cntData."\">0</slice>";
                    }
                }
            }
            $xml .= "<labels>
            <label lid=\"0\">
              <x></x>
              <y></y>
              <rotate>false</rotate>
              <width></width>
              <align>center</align>
              <text_color></text_color>
              <text_size>12</text_size>
              <text>        
                <![CDATA[<b>".$data['name']."</b>]]>
              </text>
            </label>
          </labels>";
            $xml .= "</pie>";
            $xml = str_replace(array("\r\n","\n"), "", $xml);
            //   header("Content-type:text/xml");
            //  echo $xml;die;
            return $xml;
        }
    }
    public function getColumnInXML($data = array())
    {
        if($this->query->getMaxPages() > 1){
                $this->getNavForm();
            }
        $xml = "<chart>";
        $xml .= "<series>";
        foreach($this->data as $id => $value)
        {
            $xml .= "<value xid=\"".$id."\">".$value['name']."</value>";
        }
        $xml .= "</series>";
        $xml .= "<graphs>";
        if($this->query->getTaskStatus() > 0)
        {
            $allowedStatus = $this->query->getAllowedStatus();
            foreach($this->statuses as $statusId => $heading)
            {
                if(array_key_exists($statusId, $allowedStatus))
                {
                    $xml .= "<graph gid=\"".$statusId."\" title=\"".$heading."\">";
                    foreach($this->data as $id => $countData)
                    {
                        if(is_array($countData[$statusId]))
                            $xml .= "<value xid=\"".$id."\">".$countData[$statusId]['count']."</value>";
                        else
                            $xml .= "<value xid=\"".$id."\">0</value>";
                    }
                    $xml .= "</graph>";
                }
            }
        }else
        {
            foreach($this->statuses as $statusId => $heading)
            {
                $xml .= "<graph gid=\"".$statusId."\" title=\"".$heading."\">";
                foreach($this->data as $id => $countData)
                {
                    if(is_array($countData[$statusId]))
                        $xml .= "<value xid=\"".$id."\">".$countData[$statusId]['count']."</value>";
                    else
                        $xml .= "<value xid=\"".$id."\">0</value>";
                }
                $xml .= "</graph>";
            }
        }
        $xml .= "</graphs>";
        $xml .= "</chart>";
        $xml = str_replace(array("\r\n","\n"), "", $xml);
        // file_put_contents("data.xml", $xml);
        // header("Content-type: text/xml");
        //  echo $xml;
        //  die;
        return $xml;
    }
    /*Examines the nature of the pssed in data- whether it has data for more than 1 pie charts or just one.
     * Calls getPieInXML and passes the XML string to getStript().
     * @data - the array that holds data to be converted in XML pie
    */
    public function makePie($data = array())
    {
        $draw = false;
        $graphs = 0;
        $lowerBound=0;
        $upperBound=0;
        $status = $this->query->getStatus();
        $drawPie = $this->query->getDrawPie();

        //single pie chart
        if(isset($status) AND $status != 'X' OR $drawPie)
        {
            $xmlDdata = $this->getPieInXML($data);
            //if(!$this->invalidXML)
            $this->getScript($xmlDdata);
        }
        else
        {
            /*
             * for multiple pie charts per page.
            */
            //display the navigation form
            if($this->query->getMaxPages() > 1){
                $this->getNavForm();
            }
            //echo '<pre>';
            // print_r($data);
            if(!empty($data))
            {
                foreach($data as $id => $data)
                {
                    foreach($data as $kId => $cntData)
                    {
                        if(is_array($cntData))
                        {
                            $draw = true;
                            break;
                        }
                    }

                    if($draw)
                    {
                        //put diagram in XML format
                        $xmlDdata = $this->getPieInXML($data);
                        //pass XML string to javascript
                        $this->getScript($xmlDdata);

                    }
                }
            }else
            {
                echo "<p>Your selection criteria did not return any data.</p>";
            }
        }
    }
    public function makeColumnGraph($data = array())
    {
        $this->getScript($this->getColumnInXML());
    }
    public  function countdim($array)
    {
        if (is_array(reset($array)))
            $return = $this->countdim(reset($array)) + 1;
        else
            $return = 1;

        return $return;
    }

    public function getGraph()
    {
        switch($this->type)
        {
            case '3dcolumn':
            case '3dbarchart':
                $this->makeColumnGraph($this->data);
                break;
            case 'pie':
                $this->makePie($this->data);
                break;
            default :
                break;
        }
    }
    public function getScript($data = "")
    {

        if(empty($data))
        {
            echo "<p>Your selection criteria did not return any data.</p>";
        }
        else
        {
            $graphId = time().rand(0,100000);
            echo "<script type='text/javascript' src='".$this->graph_settings[$this->type]['name_path']."/swfobject.js'></script>
                        <!-- this id must be unique! -->
                        <div id='flashcontent$graphId'>
                                <strong>You need to upgrade your Flash Player</strong>
                        </div>
                        <script type='text/javascript'>
                                // <![CDATA[
                                var so = new SWFObject('".$this->graph_settings[$this->type]['name_path']."/".$this->graph_settings[$this->type]['swf_file']."', '".$this->type."', '100%', '100%', '8', '#FFFFFF');
                                so.addVariable('path', '".$this->graph_settings[$this->type]['name_path']."/');
                                so.addVariable('settings_file', encodeURIComponent('".$this->graph_settings[$this->type]['settings_file']."'));
                                so.addVariable('chart_data', encodeURIComponent('$data'));
                                so.write('flashcontent$graphId');   // this id must match the div id above
                                // ]]>
                        </script>";
        }

    }
    public function getNavForm()
    {
        $html = "<style type='text/css'>
               td,table,form {
                    padding:0;
                    margin:0;
                }
                 table.search-paginate {
                width:100%;
            }
            table.search-paginate td{
            border:none;
            padding: 5px;
            vertical-align: top;
            }
            table {border:1px solid #EDEDED;}
            td {
                color:#000000;
                font-family:Verdana,Arial,Helvetica, sans-serif;
                font-size:8.5pt;
                line-height:12pt;
            }
            
            </style>
 <table class='search-paginate'><tr><td><form method='post' action='".$_SERVER['PHP_SELF']."' name='paginateForm'>
        <input type='submit' id='firstPage' name='firstPage' ".($this->query->getCurrentPage() == 1 ? 'disabled=disabled' : '')." value='|<' />
        <input type='submit' id='prevBtn' name='prevBtn' ".($this->query->getCurrentPage() == 1 ? 'disabled=disabled' : '')." value='<' />
        Page ".$this->query->getCurrentPage()."/".$this->query->getMaxPages()."
        <input type='submit' id='nextBtn' name='nextBtn' ".($this->query->getCurrentPage() == $this->query->getMaxPages() ? 'disabled=disabled' : '')." value='>' />
        <input type='submit' id='lastPage' name='lastPage' ".($this->query->getCurrentPage() == $this->query->getMaxPages() ? 'disabled=disabled' : '')." value='>|' />
        <input type='hidden' id='maxPages' name='maxPages' value='".$this->query->getMaxPages()."' />
        <input type='hidden' id='currentPage' name='currentPage' value='".$this->query->getCurrentPage()."' />
        <input type='hidden' id='offset' name='offset' value='".$this->query->getOffSet()."' />
        <input type='hidden' id='rowsPerPage' name='rowsPerPage' value='".$this->query->getRowsPerPage()."' />
        <input type='hidden' id='graphtasktkid' name='graphtasktkid' value='".$this->query->getTaskTkId()."' />
        <input type='hidden' id='graphtaskadduser' name='graphtaskadduser' value='".$this->query->getTaskAddUser()."' />
        <input type='hidden' id='graphtaskurgencyid' name='graphtaskurgencyid' value='".$this->query->getTaskUrgencyId()."' />
        <input type='hidden' id='graphtasktopicid' name='graphtasktopicid' value='".$this->query->getTaskTopicId()."' />
        <input type='hidden' id='graphtaskstatusid' name='graphtaskstatusid' value='".$this->query->getTaskStatus()."' />
        <input type='hidden' id='graphdatecreatedFrom' name='graphdatecreatedFrom' value='".$this->query->getDateCreatedFrom()."' />
        <input type='hidden' id='graphdatecreatedTo' name='graphdatecreatedTo' value='".$this->query->getDateCreatedTo()."' />
        <input type='hidden' id='graphdatecreatedopt' name='graphdatecreatedopt' value='".$this->query->getDateCreatedOpt()."' />
        <input type='hidden' id='graphdeadlinedateopt' name='graphdeadlinedateopt' value='".$this->query->getDeadlineDateOpt()."' />
        <input type='hidden' id='graphdeadlineFrom' name='graphdeadlineFrom' value='".$this->query->getDeadlineFrom()."' />
        <input type='hidden' id='graphdeadlineTo' name='graphdeadlineTo' value='".$this->query->getDeadlineTo()."' />
        <input type='hidden' id='graphtype' name='graphtype' value='".$this->query->getGraphType()."' />
        <input type='hidden' id='groupby' name='groupby' value='".$this->query->getGroupBy()."' />
        <input type='hidden' id='field_filter' name='field_filter' value='".$this->query->getfieldFilter()."' />
        <input type='hidden' id='udfs' name='udfs' value='".$this->query->getUDF()."' />
        <input type='hidden' id='udfvalue' name='udfvalue' value='".$this->query->getUDFValue()."' />
        </form></td></tr></table>";
        echo $html;
    }
    public function getData()
    {
        return $this->data;
    }

}
new Graph();
?>

