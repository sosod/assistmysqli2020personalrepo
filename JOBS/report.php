<?php
$title = array(
	array('txt'=>"Report Generator"),
);
$page = array("report");
$get_udf_link_headings = false;
require_once("inc_header.php");


$tact = getUserAccess();

$view_type = $tact==20 ? "MY" : "ALL";
$headings = array();
$headings['action'] = array(
	'taskadddate'	=> "Created On",
	'taskadduser'	=> "Assigned By",
	'tasktkid'		=> "Assigned To",
	'tasktopicid'	=> "Topic",
	'taskurgencyid'	=> "Priority",
	'taskstatusid'	=> "Status",
	'taskdeadline'	=> "Deadline",
	'taskattach'	=> "Attachment Details"
);
//	'taskaction'	=> ucfirst($actname)." Instructions",
//	'taskdeliver'	=> ucfirst($actname)." Deliverables",
$headings['update'] = array(
	'logdate'		=> "Date logged",
	'logtkid'		=> "Logged By",
	'logupdate'		=> "Message",
	'logstatusid'	=> "Status",
	'logactdate'	=> "Date of Activity",
	'logattach'		=> "Attachment details",
);

?>
<form name=search method=post action=report_process.php>
<h2>1. Select the fields to display</h2>
<table id=tbl_fields>
	<tr>
		<td><table id=tbl_fields_task>
			<tr>
				<th colspan=2>Action Details</th>
			</tr>
		<?php
			foreach($headings['action'] as $fld => $h) {
				echo "	<tr>
							<td><input type=checkbox checked name='field[]' value='$fld' /></td>
							<td>$h</td>
						</tr>";
			}
			foreach($udf_index['action']['index'] as $fld => $u) {
				$h = $u['udfivalue'];
				echo "	<tr>
							<td><input type=checkbox checked name='field[]' value='$fld' /></td>
							<td>$h</td>
						</tr>";
			}
		?></table></td>
		<td width='20'>&nbsp;</td>
		<td><table id=tbl_fields_update>
			<tr>
				<th colspan=2>Update Details</th>
			</tr>
		<?php
			foreach($headings['update'] as $fld => $h) {
				echo "	<tr>
							<td><input type=checkbox checked name='field[]' value='$fld' /></td>
							<td>$h</td>
						</tr>";
			}
			foreach($udf_index['update']['index'] as $fld => $u) {
				$h = $u['udfivalue'];
				echo "	<tr>
							<td><input type=checkbox checked name='field[]' value='$fld' /></td>
							<td>$h</td>
						</tr>";
			}
		?></table></td>
	</tr>
	<tr>
		<td colspan=3 class=center style='font-size: 6.5pt;'><a href=javascript:void(0) id=select>Select All</a> | <a href=javascript:void(0) id=deselect>Deselect All</a> | <a href=javascript:void(0) id=invert>Invert Selection</a></td>
	</tr>
</table>
<h2>2. Select the filters to apply</h2>
            <table id=tbl_filter>
                <tr>
                    <th>Created On:</th>
                    <td><input type=text size=10 class=jdate2012 name="taskadddate[from]" readonly="readonly" /> - <input type=text size=10 class=jdate2012 name="taskadddate[to]" readonly="readonly" /></td>
                </tr>
               <!-- <tr>
                    <th>Assigned By:</th>
                    <td><?php 
						$sql = "SELECT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as value
								FROM assist_".$cmpcode."_timekeep tk
								INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
								  ON tk.tkid = mmu.usrtkid
								  AND mmu.usrmodref = '".$_SESSION['modref']."'
								INNER JOIN ".$dbref."_list_access a
								  ON a.tkid = tk.tkid
								  AND (a.act > 20 
									OR a.tkid = '".$_SESSION['tid']."'
								  )
								WHERE tk.tkstatus = 1";
						$taskaddusers = mysql_fetch_all($sql);
						echo "<select name=taskadduser[] multiple size=5>";
							foreach($taskaddusers as $ta) {
								echo chr(10)."<option value=".$ta['id'].">".$ta['value']."</option>";
							}
						echo "</select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>";
					?></td>
                </tr>  -->
                <tr>
                    <th>Assign To:</th>
                    <td><?php 
					switch($view_type) {
					case "OWN":
					case "ALL":
						$sql = "SELECT DISTINCT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as value
								FROM assist_".$cmpcode."_timekeep tk
								INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
								  ON tk.tkid = mmu.usrtkid
								  AND mmu.usrmodref = '".$_SESSION['modref']."'
								INNER JOIN ".$dbref."_task_recipients tr
								  ON tk.tkid = tr.tasktkid
								WHERE tk.tkstatus = 1";
						$tasktkid = mysql_fetch_all($sql);
						echo "<select name=tasktkid[] multiple size=5>";
							foreach($tasktkid as $ta) {
								echo chr(10)."<option value=".$ta['id'].">".$ta['value']."</option>";
							}
						echo "</select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>";
						break;
					case "SUB":
					case "MY":
						echo $tkname."<input type=hidden name=tasktkid[] value=".$tkid." />";
						break;
					}
					?></td>
                </tr>
                <tr>
                    <th>Topic:</th>
                    <td>
                        <select name="tasktopicid[]" id=tasktopicid multiple size=5>
<?php
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                            $rs = getRS($sql);
                            while($row = mysql_fetch_array($rs)) {
                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                            }
                            unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Priority:</th>
                    <td>
                        <select name="taskurgencyid[]" multiple size=3><?php
							$sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
                            $rs = getRS($sql);
                            while($row = mysql_fetch_array($rs)) {
                                echo "<option value=".$row['id'].">".$row['value']."</option>";
                            }
                            unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td>
                        <select name="taskstatusid[]" multiple size=5><?php
							$sql = "SELECT * FROM ".$dbref."_list_status WHERE id <> 'CN' ORDER BY sort";
                            $rs = getRS($sql);
                            while($row = mysql_fetch_array($rs)) {
                                echo "<option value=".$row['pkey'].">".$row['value']."</option>";
                            }
                            unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Deadline:</th>
                    <td><input type=text size=10 class=jdate2012 name="taskdeadline[from]" readonly="readonly" /> - <input type=text size=10 class=jdate2012 name="taskdeadline[to]" readonly="readonly" /></td>
                </tr>
<!--                <tr>
                    <th><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><input type=text name='taskaction[search]' value='' /> <select name='taskaction[type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><input type=text name='taskdeliver[search]' value='' /> <select name='taskdeliver[type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select></td>
                </tr>
-->
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') ORDER BY udfisort, udfivalue";
$rs = getRS($sql);
while($row = mysql_fetch_array($rs)) {
	$class = $row['udfiobject']." ".(strlen($row['udfilinkfield'])>0 ? $row['udfilinkfield']." ".$row['udfilinkref'] : "");
	echo "<tr class='udf $class'>
		<th>".$row['udfivalue'].":</th>
		<td>";
	switch($row['udfilist']) {
	case "Y":
		$sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
		$rs2 = getRS($sql2);
		echo "<select name='udf[".$row['udfiid']."][]' size=5 multiple><option value=0>[Unspecified]</option>";
		while($row2 = mysql_fetch_array($rs2)) {
			echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
		}
		unset($rs2);
		echo "</select><br /><span class=note>Ctrl + Click to select multiple options</span>";
		break;
	case "T":
	case "M":
		echo "<input type=text name='udf[".$row['udfiid']."][search]' /> <select name='udf[".$row['udfiid']."][type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select>";
		break;
	case "D":
		echo "<input class='jdate2012'  type='text' name='udf[".$row['udfiid']."][from]' size='10' readonly='readonly' /> - <input class='jdate2012'  type='text' name='udf[".$row['udfiid']."][to]' size='10' readonly='readonly' />";
		break;
	case "N":
		 echo "<input type='text' name='udf[".$row['udfiid']."][from]' size='15' class=numb /> - <input type='text' name='udf[".$row['udfiid']."][to]' size='15' class=numb />&nbsp;<br /><span class=note>Only numeric values are allowed.</span>";
		break;
	default:
		echo "<input type=text name='udf[".$row['udfiid']."]' size='50'>";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th><?php echo $actname; ?> Attachments:</th>
                    <td><select name=attach>
						<option selected value=X>Doesn't matter</option>
						<option value=1>Must have attachments</option>
						<option value=0>Must not have attachments</option>
					</select></td>
                </tr>
            </table>

<h2>3. Select the format of the report</h2>
<table id=tbl_output>
	<tr>
		<td><input type=radio name=output value=onscreen checked id=out_onscreen /></td>
		<td class=out format=onscreen>Onscreen</td>
	</tr>
	<tr>
		<td><input type=radio name=output value=xls id=out_xls /></td>
		<td class=out format=xls>Microsoft Excel (formatted)</td>
	</tr>
	<!-- <tr>
		<td><input type=radio name=output value=csv id=out_csv /></td>
		<td class=out format=csv>Microsoft Excel (plain text)</td>
	</tr> -->
</table>			
<h2>4. Generate the report</h2>
<p id=p_generate><input type=submit value="Generate" class=isubmit /> <input type=reset /></p>

</form>
<script type=text/javascript>
$(function() {
	$("th").addClass("left").addClass("top");
	$("#tbl_fields th").removeClass("left").css("border-color","#ababab");
	$("td").addClass("top");
	$("tr").unbind("mouseenter mouseleave");
	$(".note").css("color","#fe9900").css("font-style","italic").css("font-size","6.5pt");
	$("#tbl_fields, #tbl_fields td, #tbl_output, #tbl_output td").addClass("noborder");
	$("#tbl_fields, #tbl_filter, #tbl_output, #p_generate").css("margin-left","25px");
	$("#tbl_fields_task tr td:last-child, #tbl_fields_update tr td:last-child").css("padding-right","30px");
	$(".out").css("cursor","hand");
	
	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});
	
	$("#tbl_fields a").click(function() {
		var i = $(this).attr("id");
		switch(i) {
		case "select":
			$("#tbl_fields input:checkbox").attr("checked","checked");
			break;
		case "deselect":
			$("#tbl_fields input:checkbox").attr("checked","");
			break;
		case "invert":
			$("#tbl_fields input:checkbox").each(function() {
				$(this).attr("checked",!$(this).attr("checked"));
			});
			break;
		}
	});
	$("#tbl_output .out").click(function() {
		var i = $(this).attr("format");
		$("#tbl_output #out_"+i).attr("checked","checked");
	});
});
</script>
    </body>
</html>

