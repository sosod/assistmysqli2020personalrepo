<?php

class JOBS_USER extends ASSIST_MODULE_HELPER {

	private $user_access;
	private $juser_id;
	private $me;

	const VIEW_OWN = 4;
	const VIEW_SUB = 64;
	const VIEW_ALL = 128;
	const SETUP = 256;

	public function __construct($id="ME") {
		parent::__construct();
		$this->user_access = JOBS_USER::VIEW_OWN;
		$this->me = new ASSIST();
		if($id=="ME") {
			$id = $this->getUserID();
		}
		$this->setJUserID($id);
		if(strlen($id) > 0) {
			$this->user_access = $this->getUserAccess($id);
		}
	}

	
	private function getUA() { return $this->user_access; }
	private function getJUserID() { 
		if(strlen($this->juser_id)==0) {
			$this->juser_id = $this->me->getUserID();
		}
		return $this->juser_id;
	}
	
	public function getUserAccess($id = "") {
		if(strlen($id)==0) {
			$id = $this->getUserID();
		}
		$db = new ASSIST_DB();
		$sql = "SELECT access FROM ".$db->getDBRef()."_list_access WHERE tkid = '".$id."' AND yn = 'Y' ORDER BY id DESC LIMIT 1";
		$ua = $db->mysql_fetch_one_value($sql,"access");
		if(strlen($ua)==0 || !is_numeric($ua)) {
			$this->setUA(JOBS_USER::VIEW_OWN);
		} else {
			$this->setUA($ua);
		}
		return $this->getUA();
	}
	
	
	public function getSubs() {
		$rec = array();
		$db = new ASSIST_DB();
		$sql = "SELECT DISTINCT tkid FROM assist_".$this->getCmpCode()."_timekeep WHERE tkmanager = '".$this->getUserID()."' AND tkstatus > 0";
		$first = $db->mysql_fetch_all_by_value($sql,"tkid");
		if(count($first)>0) {
			$sql = "SELECT DISTINCT tkid FROM assist_".$this->getCmpCode()."_timekeep WHERE tkmanager IN ('".implode("",$first)."','".$this->getUserID()."') AND tkstatus > 0";
			$rec = $db->mysql_fetch_all_by_value($sql,"tkid");
		}
		return $rec;
	}
	
	
	
	
	private function setJUserID($id) {	$this->juser_id = $id;	}
	private function setUA($i) 		{	$this->user_access = $i;	}
	
	//private function setUserAccess($
	
	
/**************
	USER ACCESS FUNCTIONS 
	*******************/
	
	static function canViewOwn($a) {
		return ( ($a & JOBS_USER::VIEW_OWN) == JOBS_USER::VIEW_OWN);
	}
	
	static function canViewSub($a) {
		return ( ($a & JOBS_USER::VIEW_SUB) == JOBS_USER::VIEW_SUB);
	}

	static function canViewAll($a) {
		return ( ($a & JOBS_USER::VIEW_ALL) == JOBS_USER::VIEW_ALL);
	}

	static function canAccessSetup($a) {
		return ( ($a & JOBS_USER::SETUP) == JOBS_USER::SETUP);
	}
	
	function viewOwn() {		return $this->canViewOwn($this->getUA());	}
	function viewSub() {		return $this->canViewSub($this->getUA());	}
	function viewAll() {		return $this->canViewAll($this->getUA());	}
	function accessSetup() {	return $this->canAccessSetup($this->getUA());	}
	
	
}



?>