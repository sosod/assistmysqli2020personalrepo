<?php

class JOBS_ACTIONS extends ASSIST_MODULE_HELPER {


	public function __construct($id="") {
		parent::__construct();
	}

	
	function getActionCounts($type="MY") {
		$db = new ASSIST_DB();
		$mu = new JOBS_USER();
		$dbref = $db->getDBRef();
		
		$rec = array();
		$where = array();
		$err = false;
		switch($type) {
			case "MY":	
				$rec[] = $this->getUserID(); 
				break;
			case "OWN":
				$where[] = "t.taskadduser = '".$this->getUserID()."'";
				break;
			case "SUB":
				$rec = $mu->getSubs();
				if(count($rec)==0) {
					$err = true;
				}
				break;
			case "ALL":
				break;
		}

		if($err) {
			return array();
		} else {
			$sql = "SELECT s.pkey as status, count(t.taskid) as actcount
					FROM ".$dbref."_task t
					INNER JOIN ".$dbref."_list_status s
					  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
					INNER JOIN ".$dbref."_task_recipients tr
					  ON tr.taskid = t.taskid 
					  ".(count($rec)>0 ? " AND tr.tasktkid IN ('".implode("','",$rec)."') " : "")."
					".(count($where)>0 ? implode(" AND ",$where) : "")."
					GROUP BY s.pkey";
			return $db->mysql_fetch_value_by_id($sql,"status","actcount");
		}
		
	}
	








	
	
	
	public function __destruct() {
		parent::__destruct();
	}
	
	
}



?>