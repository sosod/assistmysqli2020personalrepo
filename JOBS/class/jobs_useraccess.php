<?php

class JOBS_USERACCESS extends ASSIST_MODULE_HELPER {


	const VIEW_OWN = 4;
	const VIEW_SUB = 64;
	const VIEW_ALL = 128;
	const SETUP = 256;

	public function __construct() {
		parent::__construct();
	}

	
	function viewOwn($a) {
		return ( ($a & self::VIEW_OWN) == self::VIEW_OWN);
	}
	
	function viewSub($a) {
		return ( ($a & self::VIEW_SUB) == self::VIEW_SUB);
	}

	function viewAll($a) {
		return ( ($a & self::VIEW_ALL) == self::VIEW_ALL);
	}

	function accessSetup($a) {
		return ( ($a & self::SETUP) == self::SETUP);
	}
}



?>