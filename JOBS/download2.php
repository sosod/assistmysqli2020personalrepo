<?php
include("../inc_codehelper.php");

$cmpcode = $_SESSION['cc'];
$modref = $_SESSION['modref'];
$dbref = "assist_".$cmpcode."_".strtolower($modref);
$docid = $_REQUEST['i'];

include "../inc_db.php";
include "../inc_db_conn.php";

if(checkIntRef($docid)) {
	$sql = "SELECT * FROM ".$dbref."_task_attachments WHERE id = ".$docid;
	$doc = mysql_fetch_one($sql);
	$filename = "../files/".$cmpcode."/".$_SESSION['modref']."/".(strlen($doc['file_location'])>0 ? $doc['file_location']."/" : "").$doc['system_filename'];

	header("Content-type: application/octet-stream");
	header("Content-disposition: attachment; filename=".$doc['original_filename']);
	readfile($filename);
} else {
	die("<P>An error occurred while trying to download the selected document.  Please try again.");
}
?>