<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$get_udf_link_headings = false;
require_once 'inc_header.php';


function drawHistoryRow($log) {
	global $update_udfs;
	global $history_attachments;
	global $history_udfs;
	global $update_udf_lists;
	$cmpcode = $_SESSION['cc'];
	$modref = $_SESSION['modref'];
	
	$logid = $log['logid'];
	$log_udf = isset($history_udfs[$logid]) ? $history_udfs[$logid] : array();
	$log_attach = isset($history_attachments[$logid]) ? $history_attachments[$logid] : array();
	$file_location = "../files/".$cmpcode."/".$modref."/";
	foreach($log_attach as $i => $l) {
		if(!file_exists($file_location.$l['system_filename'])) {
			unset($log_attach[$i]);
		}
	}
	
		echo "<tr>
				<td colspan=2 style='padding: 0 0 0 0;'>
					<table id=log_".$logid." width=99% class=noborder>
						<tr>
							<th width=50 rowspan=2>".date("d M Y H:i",$log['logactdate'])."</th>
							<td >
								<p style='margin-top: 0px;'>".str_replace(chr(10),"<br />",$log['logupdate'])."</p>";
		if(count($log_attach)>0) {
			echo "
							<p><span class=b>Attachments:</span><br />+ <a href=''>link here</a><br />+ <a href=''>another file</a></p>";
		}
		echo "				</td>
							<td  class='right'>
								<p style='margin-top: 0px;'><span class=b>Status:</span> ".$log['status']."<br />(".$log['logstate']."% completed)</p>
								<p><span class=b>Date Logged:</span><br />".date("d M Y H:i",$log['logdate'])."</p>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table class=noborder>";
								foreach($update_udfs as $u) {
									$lv = isset($log_udf[$u['udfiid']]['udfvalue']) ? $log_udf[$u['udfiid']]['udfvalue'] : "";
									echo "
										<tr>
											<td class='b right' >".$u['udfivalue'].":</td>
											<td>";
											switch($u['udfilist']) {
											case "Y":
												echo (isset($lv) && checkIntRef($lv) && isset($update_udf_lists[$u['udfiid']][$lv]['udfvvalue'])) ? $update_udf_lists[$u['udfiid']][$lv]['udfvvalue'] : "N/A" ;
												break;
											case "M":
												echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
												break;
											case "D":
												echo strlen($lv)>0 && is_numeric($lv) ? date("d M Y",$lv) : "N/A";
												break;
											case "T":
												echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
												break;
											case "N":
												echo strlen($lv)>0 && is_numeric($lv) ? number_format($lv,2) : "N/A";
												break;
											}
										echo "</td>
										</tr>";
								}
						echo	"</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
}


//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."'";
$row = mysql_fetch_one($sql);
$tact = $row['act'];

//GET TASK ID
$taskid = $_REQUEST['i'];
if(checkIntRef($taskid)) {
	//GET TASK DETAILS INTO ARRAY TASK
//	$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
	$sql = "SELECT t.*
			, top.value as topic
			, urg.value as urgency
			, stat.value as status
			, CONCAT_WS(' ',tk.tkname,tk.tksurname) as adduser
			FROM ".$dbref."_task t
			LEFT OUTER JOIN ".$dbref."_list_topic top
			  ON top.id = t.tasktopicid
			LEFT OUTER JOIN ".$dbref."_list_urgency urg
			  ON urg.id = t.taskurgencyid
			LEFT OUTER JOIN ".$dbref."_list_status stat
			  ON stat.pkey = t.taskstatusid
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			  ON tk.tkid = t.taskadduser
			WHERE taskid = ".$taskid;
	$task = mysql_fetch_one($sql);
	
	
	//HISTORY INFORMATION
		$sql = "SELECT l.*, stat.value as status 
				FROM ".$dbref."_log l
				INNER JOIN ".$dbref."_list_status stat
				  ON l.logstatusid = stat.pkey
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U','E','D') 
				ORDER BY logactdate DESC, logdate DESC";
		$history = mysql_fetch_all($sql);
		$sql = "SELECT a.* 
				FROM ".$dbref."_task_attachments a
				INNER JOIN ".$dbref."_log l
				  ON a.logid = l.logid
				  AND l.logtaskid = ".$taskid;
		$history_attachments = mysql_fetch_all_fld2($sql,"logid", "id");
		$sql = "SELECT u.*, l.logid 
				FROM assist_".$cmpcode."_udf u
				INNER JOIN assist_".$cmpcode."_udfindex i
				  ON u.udfindex = i.udfiid
				  AND i.udfiobject = 'update'
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = '".$modref."'
				INNER JOIN ".$dbref."_log l
				  ON l.logid = u.udfnum
				  AND l.logtaskid = ".$taskid."
				WHERE u.udfref = '".$modref."'";
		$history_udfs = mysql_fetch_all_fld2($sql,"logid", "udfindex");
		
	//UPDATE INFORMATION	
		$sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND id NOT IN ('CN','NW')ORDER BY sort";
		$status = mysql_fetch_all_fld($sql,"pkey");
		
		$sql = "SELECT * 
				FROM assist_".$cmpcode."_udfindex 
				WHERE udfiref = '".$modref."' 
				AND udfiyn = 'Y' 
				AND udfiobject = 'update' 
				AND (
						udfilinkref = 0 
					OR (
						udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid']."
					) OR (
						udfilinkfield = 'taskstatusid'
					)
				)
				ORDER BY udfisort, udfivalue";
		$update_udfs = mysql_fetch_all($sql);
		$sql = "SELECT v.* 
				FROM assist_janet12_udfvalue v
				INNER JOIN assist_janet12_udfindex i
				  ON i.udfiid = v.udfvindex
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = 'TA'
				  AND i.udfiobject = 'update'
				WHERE udfvyn = 'Y' 
				ORDER BY udfvindex, udfvvalue";
		$update_udf_lists = mysql_fetch_all_fld2($sql,"udfvindex","udfvid");

	
} else {
	die("<p>An error occurred while trying to access the requested ".$actname.".  Please go back and try again.</p>");
}
/*
//SET REMINDER
$tremind = $_GET['remind'];
if(strlen($tremind) > 0) {
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE ".$dbref."_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
    $tsql = $sql;
    $tref = "TA";
    $trans = "Set reminder ".$tr." for task ".$taskid.".";
    include("inc_transaction_log.php");
}
*/

?>
<style type=text/css>
[type=file] { margin-bottom: 5px; }
</style>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function statusPerc() {
            var stat = document.taskupdate.logstatusid.value;
            stat = parseInt(stat);
            if(stat == 3 || stat > 5)
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
                if(stat > 5 && lstat[stat] > 0)
                {
                    document.taskupdate.logstate.value = lstat[stat];
                }
                else
                {
                    document.taskupdate.logstate.value = lstat[0];
                }
            }
            else
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>
<table width=100% id=tbl_container>
	<tr><td class=td_container style='padding-right: 30px'>
        <h2 class=fc><?php echo ucfirst($actname);?> Status</h2>
        <table width=95% id=tbl_object>
            <tr>
                <th width=140>Created By:</b></th>
				<td><?php echo $task['adduser']; ?></td>
            </tr>
			<tr>
				<th>Created On:</th>
				<td><?php echo $task['taskadddate']; ?></td>
			</tr>
            <tr>
                <th>Assigned To:</th>
                <td><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN ".$dbref."_task_recipients r ON (u.tkid=r.tasktkid)
                WHERE r.taskid= '".$task['taskid']."' ";
                    // echo $sql;
                    $rs = getRS($sql);
                    $assignees = "";
                    while($row = mysql_fetch_array($rs)) {
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    unset($rs);
                ?></td>
            </tr>
            <tr>
                <th>Topic:</td>
				<td><?php echo $task['topic']; ?></td>
            </tr>
            <tr>
                <th>Priority:</th>
				<td><?php echo $task['urgency']; ?></td>
            </tr>
            <tr>
                <th>Status:</th>
				<td><?php echo $task['status']; ?></td>
            </tr>
            <tr>
                <th>Deadline:</th>
                <td><?php echo date("d-M-Y",$task['taskdeadline']); ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Instructions:</b></td>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskaction']);
                ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Deliverables:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskdeliver']);
                ?></td>
            </tr>
<?php
if($cmpcode == "fin0001" || $cmpcode == "janet12") {
	$sql = "SELECT * FROM ".$dbref."_task_preset WHERE preset_taskid = $taskid AND active = true"; 
	$rs = getRS($sql);
		if(mysql_num_rows($rs)>0) { ?>
				<tr>
					<th>Source <?php echo ucfirst($actname);?>:</th>
					<td><ul>
						<?php
						while($row = mysql_fetch_array($rs)) {
							echo "<li><a href=view_all_update.php?i=".$row['source_taskid']." target=_blank>$actname #".$row['source_taskid']."</a></li>";
						}
					?></ul></td>
				</tr>
	<?php }
	unset($rs);
}
?>
<!--<tr id="firstdoc">

    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
</tr>
<tr>
    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
</tr>-->


            <?php
            if($task['topic'] == 'Travel Requests' && $topicyn = 'N') {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                $udfi = mysql_fetch_one($sql);
                if(strlen($udfi['udfiid'])>0) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                    $rs2 = getRS($sql2);
                    $udf = mysql_num_rows($rs2);
                    $row2 = mysql_fetch_array($rs2);
                    ?>
            <tr>
                <th>Travel Request Ref.:</th>
                <td><?php
					if($udf>0) {
						echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
					}
					else {
						echo("N/A");
					}
				?>&nbsp;</td>
            </tr>
                    <?php
                }
            }
            $sql = "SELECT * 
					FROM assist_".$cmpcode."_udfindex 
					WHERE udfiref = '".$modref."' 
					AND udfiyn = 'Y' 
					AND udfiobject = 'action' 
					AND (udfilinkref = 0 OR udfilinkref = ".$task['tasktopicid'].")
					ORDER BY udfisort, udfivalue";
            $rs = getRS($sql);
            while($row = mysql_fetch_array($rs)) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                $rs2 = getRS($sql2);
					$udf = mysql_num_rows($rs2);
					$row2 = mysql_fetch_array($rs2);
                ?>
            <tr>
                <th><?php echo($row['udfivalue']); ?>:</th>
                <td><?php
                        switch($row['udfilist']) {
                            case "Y":
                                if(checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    $rs3 = getRS($sql2);
										$row3 = mysql_fetch_array($rs3);
										echo $row3['udfvvalue'];
                                    unset($rs3);
                                } else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo $row2['udfvalue'];
                                else
                                    echo "N/A";
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                break;
                        }
                ?>&nbsp;</td>
            </tr>
                <?php
            }
			unset($rs2);

            $sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE att.taskid = ".$taskid;
            $rs = getRS($sql);
            if(mysql_num_rows($rs) > 0) {
                echo "<tr>
				<th>Attachment(s):</th>
				<td><table  style='border:none;'>";
                while($row = mysql_fetch_assoc($rs)) {
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
                    $file = "../files/$cmpcode/$modref/".$row['system_filename'];
                    if(file_exists($file)) {
						echo "<tr><td style='border:none;'><a href='view_dl.php?d=".$row['id']."'>".$row['original_filename']."</a></td>";
					}
                }
                echo "</table></td></tr>";
            }

            if($tkid == $task['taskadduser'] && $task['topic'] != 'Travel Requests') { ?>
                <tr><th></th><td class=right><input type="button" value="Edit"  onclick="redirect(<?php echo($taskid);?>);"/></td></tr>
           <?php } ?>
        </table>
        <?php 
		displayGoBack(); 
?>
	<table id=tbl_history width=95%>
	<?php
	echo "	<tr>
				<th colspan=2>History</th>
			</tr>";
	foreach($history as $h) {
		drawHistoryRow($h);
	}
	
	?>
	</table>
</td><td class=td_container><div class=float>
<?php
		
		echo "<h2>".ucfirst($actname)." Updates</h2>
		<table id=tbl_updates >
			<tr>
				<th colspan=2>Update</th>
			</tr>
			<tr class=no-highlight>
				<td class=center colspan=2>
					<textarea rows=6 cols=70 name=logupdate></textarea>
				</td>
			</tr>
			<tr>
				<td class=left style='border-right: 0px solid #ababab;'>
					<p><span class=b>Status:</span> <select name=logstatusid>";
					foreach($status as $s) {
						echo "<option value=".$s['pkey']." state='".$s['state']."'>".$s['value']."</option>";
					}
		echo "
					</select></p>
					<p><input type=text name=logstate size=5 value=0 style='text-align: right; margin-left: 45px' />% complete </p>
					<p><span class=b>Date of Activity:</span> <input type=text value='".date("d M Y H:i")."' class=datetime /></p>
				</td>
				<td class=right  style='border-left: 0px solid #ababab;'>
					<span id=spn_attach_update><input type=file name=attachments[] /></span>
					<br /><a href=javascript:void(0) id=p_attach>Attach another</a>
				</td>
			</tr>";
			if(count($update_udfs)>0) {
				echo "
					<tr><td colspan=2>
						<table id=tbl_update_udf>";
				foreach($update_udfs as $u) {
					$class = $u['udfiobject']." ".(strlen($u['udfilinkfield'])>0 ? $u['udfilinkfield']." ".$u['udfilinkref'] : "");
					echo "
						<tr class='udf $class'>
							<td class='b right'>".$u['udfivalue'].":</td>
							<td>";
						switch($u['udfilist']) {
						case "Y":
							echo "<select name=".$u['udfiid']."><option value=0 selected>N/A</option>";
							foreach($update_udf_lists[$u['udfiid']] as $v) {
								echo "<option value=".$v['udfvid'].">".$v['udfvvalue']."</option>";
							}
							echo "</select>";
							break;
						case "M":
							echo "<textarea rows=5 cols=40></textarea>";
							break;
						case "D":
							echo "<input type=text size=10 class=jdate2012 />";
							break;
						case "T":
							echo "<input type=text size=30 />";
							break;
						case "N":
							echo "<input type=text size=20 class=number />";
							break;
						}
					echo "</td>
						</tr>";
				}
				echo "	</table>
					</td></tr>";
			}
			echo "
			<tr>
				<td class=center colspan=2><input type=button value='Save Update' class=isubmit /><input type=reset /></td>
			</tr>
		</table>";
		displayGoBack();
		?></div>
</td></tr>
</table>
<script type=text/javascript>
$(function() {
	$("tr").unbind('mouseenter mouseleave');
	$("td").addClass("top");
	$("table.noborder td").addClass("noborder");
	$("table#tbl_container, table#tbl_container td.td_container").addClass("noborder");
	$("#tbl_object th").addClass("left");
	$("#tbl_update_udf, #tbl_update_udf td").addClass("noborder");
	$("#tbl_updates select[name=logstatusid]").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_updates select[name=logstatusid]").change(function() {
		var s = $(this).attr("state");
		if(!isNaN(parseInt(s)) && s > 0) {
			$("#tbl_updates input:text[name=logstate]").val(s);
		}
		var v = $(this).val();
		$("#tbl_update_udf tr.taskstatusid").each(function() {
			if($(this).hasClass(v)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	$("#tbl_updates #p_attach").click(function() {
		$("#tbl_updates #spn_attach_update").append("<br /><input type=file name=attachments[] />");
	});
});
</script>		
		
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		
		
		
    </body>

</html>
