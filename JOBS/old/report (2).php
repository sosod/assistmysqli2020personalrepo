<?php //include("inc_ignite.php");
$title = array(
	array('url'=>"report.php",'txt'=>"Report"),
);
$page = array("report");
$get_udf_link_headings = false;
require_once("inc_header.php");



?>
        <style type='text/css'>
            div#graphs {
                margin-left: 50%;
            }
        </style>
        <script type="text/javascript">
            $(function(){

                //Start
                $('#datepickerFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

                //End
                $('#datepickerTo').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#datepickerDeadlineTo').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#datepickerDeadlineFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdatecreatedFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdatecreatedTo').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdeadlineFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdeadlineTo').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                 $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                /*
                $('#taskreport').submit(function(){
                    alert($("#simplepie").val());
                   if($("#simplepie").val() == 'pie' && $('#tasktkid').val() == 'X'){
                    alert('Please select Person Tasked.');
                    $('#tasktkid').focus();
                    return false;
                   }
                });

                $('#btnGraph').click(function(){
                  $('#taskreport').attr('action','report_process_graphs.php');
                  $('#taskreport').submit();
                });*/
                //  $('input:radio[name="datecreated"]').filter('[value="Exact"]').attr('checked',true);
                // $('input:radio[name="deadlinedate"]').filter('[value="Exact"]').attr('checked',true);

			$("#report-output table, #report-output table td").addClass("noborder");
			$("#report-columns table, #report-columns table td").addClass("noborder");
			$("#report-filters table th").addClass("left").addClass("top");
			$("#report-output tr, #report-columns tr").unbind("mouseenter mouseleave");
		});
        function Validate(me) {
            return true;
        }

    </script>
        <form name='taskreport' id="taskreport" method='post' action='report_process.php' >

            <div id="report-columns">
                <h2>1. Select the details you want in your report:</h2>
                <table style='margin-left: 17px;'>
                    <tr>
                        <td class="center"><input type="checkbox" checked name="field[]" value="adddate"></td>
                        <td>Date <?php echo $actname;?>ed</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="tkid"></td>
                        <td>User <?php echo $actname;?>ed</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="adduser"></td>
                        <td><?php echo $actname;?> owner</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="topicid"></td>
                        <td><?php echo $actname;?> topic</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="action"></td>
                        <td><?php echo $actname;?> instructions</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="deliver"></td>
                        <td><?php echo $actname;?> deliverables</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="deadline"></td>
                        <td><?php echo $actname;?> deadline</td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="update"></td>
                        <td>Updates: <select name=upfilter><option selected value=AGE>Most recent update</option><option value=ALL>All updates</option></select></td>
                    </tr>
                    <tr>
                        <td class=center><input type="checkbox" checked name="field[]" value="duration"></td>
                        <td><?php echo $actname;?> Duration</td>
                    </tr>
                    <?php
					foreach($udf_index['action']['index'] as $row) {
                        ?>
                    <tr>
                        <td class=center><input type="checkbox" checked name="udf[]" value="<?php echo($row['udfiid']); ?>"></td>
                        <td><?php echo($row['udfivalue']); ?></td>
                    </tr>
                        <?php
                    }
                    ?>
                </table>

            </div>
            <h2>2. Select the filter you wish to apply:</h2>
            <div id=report-filters>
                <table  style="margin-left: 17px">
                    <tr>
                        <th><?php echo $actname;?> assigned to:</th>
                        <td>
                            <?php
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'
                                ORDER BY t.tkname, t.tksurname";
                            //echo $sql;die;
                            include("inc_db_con.php");
                            $size = mysql_num_rows($rs);
                            ;?>
                            <i>Ctrl + left click to select multiple users</i><br />
                            <select  id="tasktkidfilter" name="tasktkidfilter[]" multiple="multiple" size="<?php echo($size > 10 ? 10 : $size);?>">
                                <!--<option value=X>--- SELECT ---</option>-->
                                <?php
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                mysql_close();
                                ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $actname;?> owner:</th>
                        <td><select size="1" name="taskaddfilter">
                                <option selected value=ALL>All users</option>
                                <?php
                                $sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                mysql_close();
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <th><?php echo $actname;?> topic:</th>
                        <td><select size="1" name="tasktopicfilter">
                                <option selected value=ALL>All topics</option>
                                <?php
                                $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option value=".$row['id'].">".$row['value']."</option>");
                                }
                                mysql_close();
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <th><?php echo $actname;?> instructions:</th>
                        <td><input type=text name=taskactfilter size=40> <select size="1" name="taskactfiltertype">
                                <option selected value=ALL>Match all words</option>
                                <option value=ANY>Match any word</option>
                                <option value=EXACT>Match exact phrase</option>
                            </select></td>
                    </tr>
                    <tr>
                        <th><?php echo $actname;?> deliverables:</th>
                        <td><input type=text name=taskdelfilter size=40> <select size="1" name="taskdelfiltertype">
                                <option selected value=ALL>Match all words</option>
                                <option value=ANY>Match any word</option>
                                <option value=EXACT>Match exact phrase</option>
                            </select></td>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td><select size="1" name="statusfilter">
                                <option selected value=ALL>All <?php echo $actname;?>s</option>
                                <option value=INC>All incomplete <?php echo $actname;?>s</option>
                                <?php
                                $sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                }
                                mysql_close();
                                ?>

                            </select></td>
                    </tr>
                    <tr>
                        <th>Date created:</th>
                        <td><input type=radio name=taskadddatefilter value=ANY checked id=tadf1> <label for=tadf1>Any date</label><br />
                            <input type=radio name=taskadddatefilter value=EXACT id=tadf2> <label for=tadf2>From </label>
                            <input type=text id="datepickerFrom" name="datepickerFrom"   readonly="readonly"/>
                            <label for=tadf2>&nbsp;to&nbsp;</label>
                            <input type=text id="datepickerTo" name="datepickerTo"   readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Deadline:</th>
                        <td><input type=radio name=taskdlfilter value=ANY checked id=tdl1> <label for=tdl1>Any date</label><br>
                            <input type=radio name=taskdlfilter value=EXACT id=tdl2> <label for=tdl2>From </label>
                            <input type=text id="datepickerDeadlineFrom" name="datepickerDeadlineFrom"   readonly="readonly"/>
                            <!--<input type=text name=tdl1[] size=4 value=<?php //echo(date("Y")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
                            <label for=tdl2>&nbsp;to&nbsp;</label>
                            <!--<input type=text name=tdl2[] size=2 value=<?php //echo(date("d")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
                            <input type=text id="datepickerDeadlineTo" name="datepickerDeadlineTo"   readonly="readonly"/>
                            <!--<input type=text name=tdl2[] size=4 value=<?php //echo(date("Y")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
                        </td>
                    </tr>
                    <?php
                    //$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '$modref' ORDER BY udfisort";
                    //include("inc_db_con.php");
                    //while($row = mysql_fetch_array($rs)) {
					foreach($udf_index['action']['index'] as $row) {
                        ?>
                    <tr>
                        <th><?php echo($row['udfivalue']); ?>:</td>
                        <td>
                                <?php
                                switch($row['udfilist']) {
                                    case "Y":
                                        echo("<select size=1 name=\"udf".$row['udfiid']."filter\">");
                                        echo("<option selected value=ALL>All</option>");
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                                        include("inc_db_con2.php");
                                        while($row2 = mysql_fetch_array($rs2)) {
                                            echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                        }
                                        mysql_close($rs2);
                                        echo("</select>");
                                        break;
                                     case "D":
                                          echo "<input class='date-type' type='text' name='".$row['udfiid']."'  size='15' readonly='readonly'/>";
                                         break;
                                     case "N":
                                         echo "<input type='text' name='".$row['udfiid']."'  size='15' />&nbsp;<small><i>Only numeric values are allowed.</i></small>";
                                         break;
                                    default:
                                        echo("<input type=text name=\"udf".$row['udfiid']."filter\">");
                                        break;
                                }
                                ?>
                        </td>
                    </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <h2>3. Select the report output method:</h2>
            <div id=report-output>
                <table style="margin-left: 17px">
                    <tr>
                        <td class=center><input type="radio" name="csvfile" value="N" checked id=t1></td>
                        <td><label for=t1>Onscreen display</label></td>
                    </tr>
                    <tr>
                        <td class=center><input type="radio" name="csvfile" value="Y" id=t2></td>
                        <td><label for=t2>Save to file (Microsoft Excel file)</label></td>
                    </tr>
                </table>
            </div>
            <h2>4. Click the button:</h2>
            <p style="margin-left: 17px"><input type="submit" value="Generate Report" class=isubmit />
                <input type="reset" value="Reset" name="B2" /></p>
        </form>
    </body>
</html>
