<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$get_udf_link_headings = false;
require_once 'inc_header.php';


//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."'";
$row = mysql_fetch_one($sql);
$tact = $row['act'];

//GET TASK ID
$taskid = $_REQUEST['i'];
if(checkIntRef($taskid)) {
	//GET TASK DETAILS INTO ARRAY TASK
//	$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
	$sql = "SELECT t.*
			, top.value as topic
			, urg.value as urgency
			, stat.value as status
			, CONCAT_WS(' ',tk.tkname,tk.tksurname) as adduser
			FROM ".$dbref."_task t
			LEFT OUTER JOIN ".$dbref."_list_topic top
			  ON top.id = t.tasktopicid
			LEFT OUTER JOIN ".$dbref."_list_urgency urg
			  ON urg.id = t.taskurgencyid
			LEFT OUTER JOIN ".$dbref."_list_status stat
			  ON stat.pkey = t.taskstatusid
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			  ON tk.tkid = t.taskadduser
			WHERE taskid = ".$taskid;
	$task = mysql_fetch_one($sql);
} else {
	die("<p>An error occurred while trying to access the requested ".$actname.".  Please go back and try again.</p>");
}
/*
//SET REMINDER
$tremind = $_GET['remind'];
if(strlen($tremind) > 0) {
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE ".$dbref."_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
    $tsql = $sql;
    $tref = "TA";
    $trans = "Set reminder ".$tr." for task ".$taskid.".";
    include("inc_transaction_log.php");
}
*/

?>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function statusPerc() {
            var stat = document.taskupdate.logstatusid.value;
            stat = parseInt(stat);
            if(stat == 3 || stat > 5)
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
                if(stat > 5 && lstat[stat] > 0)
                {
                    document.taskupdate.logstate.value = lstat[stat];
                }
                else
                {
                    document.taskupdate.logstate.value = lstat[0];
                }
            }
            else
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>
        <h2 class=fc><?php echo ucfirst($actname);?> Status</h2>
        <table width=600 id=tbl_object>
            <tr>
                <th width=140>Created By:</b></th>
				<td><?php echo $task['adduser']; ?></td>
            </tr>
			<tr>
				<th>Created On:</th>
				<td><?php echo $task['taskadddate']; ?></td>
			</tr>
            <tr>
                <th>Assigned To:</th>
                <td><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN ".$dbref."_task_recipients r ON (u.tkid=r.tasktkid)
                WHERE r.taskid= '".$task['taskid']."' ";
                    // echo $sql;
                    $rs = getRS($sql);
                    $assignees = "";
                    while($row = mysql_fetch_array($rs)) {
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    unset($rs);
                ?></td>
            </tr>
            <tr>
                <th>Topic:</td>
				<td><?php echo $task['topic']; ?></td>
            </tr>
            <tr>
                <th>Priority:</th>
				<td><?php echo $task['urgency']; ?></td>
            </tr>
            <tr>
                <th>Status:</th>
				<td><?php echo $task['status']; ?></td>
            </tr>
            <tr>
                <th>Deadline:</th>
                <td><?php echo date("d-M-Y",$task['taskdeadline']); ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Instructions:</b></td>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskaction']);
                ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Deliverables:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskdeliver']);
                ?></td>
            </tr>
<?php
if($cmpcode == "fin0001" || $cmpcode == "janet12") {
	$sql = "SELECT * FROM ".$dbref."_task_preset WHERE preset_taskid = $taskid AND active = true"; 
	$rs = getRS($sql);
		if(mysql_num_rows($rs)>0) { ?>
				<tr>
					<th>Source <?php echo ucfirst($actname);?>:</th>
					<td><ul>
						<?php
						while($row = mysql_fetch_array($rs)) {
							echo "<li><a href=view_all_update.php?i=".$row['source_taskid']." target=_blank>$actname #".$row['source_taskid']."</a></li>";
						}
					?></ul></td>
				</tr>
	<?php }
	unset($rs);
}
?>
<!--<tr id="firstdoc">

    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
</tr>
<tr>
    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
</tr>-->


            <?php
            if($task['topic'] == 'Travel Requests' && $topicyn = 'N') {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                $udfi = mysql_fetch_one($sql);
                if(strlen($udfi['udfiid'])>0) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                    $rs2 = getRS($sql2);
                    $udf = mysql_num_rows($rs2);
                    $row2 = mysql_fetch_array($rs2);
                    ?>
            <tr>
                <th>Travel Request Ref.:</th>
                <td><?php
					if($udf>0) {
						echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
					}
					else {
						echo("N/A");
					}
				?>&nbsp;</td>
            </tr>
                    <?php
                }
            }
            $sql = "SELECT * 
					FROM assist_".$cmpcode."_udfindex 
					WHERE udfiref = '".$modref."' 
					AND udfiyn = 'Y' 
					AND udfiobject = 'action' 
					AND (udfilinkref = 0 OR udfilinkref = ".$task['tasktopicid'].")
					ORDER BY udfisort, udfivalue";
            $rs = getRS($sql);
            while($row = mysql_fetch_array($rs)) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                $rs2 = getRS($sql2);
					$udf = mysql_num_rows($rs2);
					$row2 = mysql_fetch_array($rs2);
                ?>
            <tr>
                <th><?php echo($row['udfivalue']); ?>:</th>
                <td><?php
                        switch($row['udfilist']) {
                            case "Y":
                                if(checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    $rs3 = getRS($sql2);
										$row3 = mysql_fetch_array($rs3);
										echo $row3['udfvvalue'];
                                    unset($rs3);
                                } else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo $row2['udfvalue'];
                                else
                                    echo "N/A";
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                break;
                        }
                ?>&nbsp;</td>
            </tr>
                <?php
            }
			unset($rs2);

            $sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE att.taskid = ".$taskid;
            $rs = getRS($sql);
            if(mysql_num_rows($rs) > 0) {
                echo "<tr>
				<th>Attachment(s):</th>
				<td><table  style='border:none;'>";
                while($row = mysql_fetch_assoc($rs)) {
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
                    $file = "../files/$cmpcode/$modref/".$row['system_filename'];
                    if(file_exists($file)) {
						echo "<tr><td style='border:none;'><a href='view_dl.php?d=".$row['id']."'>".$row['original_filename']."</a></td>";
					}
                }
                echo "</table></td></tr>";
            }

            if($tkid == $task['taskadduser'] && $task['topic'] != 'Travel Requests') { ?>
                <tr><th></th><td class=right><input type="button" value="Edit"  onclick="redirect(<?php echo($taskid);?>);"/></td></tr>
           <?php } ?>
        </table>
        <?php 
		displayGoBack(); 
		
		$sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND id NOT IN ('CN','NW')ORDER BY sort";
		$status = mysql_fetch_all_fld($sql,"pkey");
		
		echo "<h2>".ucfirst($actname)." Updates</h2>
		<table id=tbl_updates width=600>
			<tr>
				<th colspan=2>Update</th>
			</tr>
			<tr>
				<td class=center colspan=2>
					<textarea rows=6 cols=70 name=logupdate></textarea>
				</td>
			</tr>
			<tr>
				<td class=left style='border-right: 0px solid #ababab;'>
					<span class=b>Status:</span> <select name=logstatusid>";
					foreach($status as $s) {
						echo "<option value=".$s['pkey']." state='".$s['state']."'>".$s['value']."</option>";
					}
		echo "
					</select>
					<p><input type=text name=logstate size=5 value=0 style='text-align: right' />% complete </p>
				</td>
				<td class=right  style='border-left: 0px solid #ababab;'>
					<span id=spn_attach_update><input type=file name=attach[] /></span>
					<p>Attach another</p>
				</td>
			</tr>
			<tr>
				<td class=center colspan=2><input type=button value='Save Update' class=isubmit /><input type=reset /></td>
			</tr>
		</table>";
		
		
		
		?>
<script type=text/javascript>
$(function() {
	$("#tbl_object th").addClass("left");
	$("#tbl_updates select[name=logstatusid]").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_updates select[name=logstatusid]").change(function() {
		var s = $(this).attr("state");
		if(!isNaN(parseInt(s)) && s > 0) {
			$("#tbl_updates input:text[name=logstate]").val(s);
		}
	});
});
</script>		
		
		
		
		
		
		
		
        <h2><?php echo ucfirst($actname);?> Updates</h2>
        <form name=taskupdate method=post action=view_task_update_process.php onsubmit="return validateUpdates(this);" language=jscript enctype="multipart/form-data">
            <input type=hidden name=taskid value=<?php echo($taskid);?>>
            <table width=600 >
                <?php
//IF THE TASK IS NOT CLOSED OFFER THE OPTION TO UPDATE
                if($task['taskstatusid'] != "1") {
                    ?>
				<tr>
					<th>Update</th>
				</tr>
                <tr>
                    <td><?php echo(date("d-M-Y H:i",$today)); ?></td>
                    <td><textarea rows="5" name="logupdate" cols="35"></textarea></td>
                    <td><select size="1" name="logstatusid" onchange=statusPerc()><?php
                                $lstat = "lstat[0] = ".$task['taskstate'].";";
                                $sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND pkey NOT IN (2,4) ORDER BY sort";
                                $rs = getRS($sql);
                                while($row = mysql_fetch_array($rs)) {
                                    if( ($row['pkey']==3 && $task['taskstatusid']==4 ) || ($task['taskstatusid']==$row['pkey']))  {
                                        echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                    } else {
                                        echo("<option  value=".$row['pkey'].">".$row['value']."</option>");
                                    }
                                    $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                }
                                unset($rs);
                                ?>
                        </select><br>&nbsp;
                        <script language=JavaScript>
                            var lstat = new Array();
    <?php                   echo($lstat); ?>
                        </script>
                        <br>
                        <input type="text" name="logstate" size="4" value=<?php echo($task['taskstate']); ?>>% complete</td>
                </tr>
                <tr id="firstdoc">
                    <td ><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td ><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
                </tr>
                <tr>
                    <td >
                        <input type="submit" value="Submit" name="B1">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
                    <?php
                }
                else {
                    echo("<input type=hidden name=logstatusid value=CL>");
                }
//DISPLAY PAST UPDATES
                $sql = "SELECT * FROM ".$dbref."_log l, ".$dbref."_list_status s WHERE l.logstatusid = s.pkey AND l.logtaskid = ".$taskid." ORDER BY logid DESC";
                $rs = getRS($sql);
                $l = mysql_num_rows($rs);
				if($l>0) {
					echo "
						<tr>
							<th>Date</th>
							<th>Update</th>
							<th>Details</th>
						</tr>";
				}
                $l2 = 0;
                while($row = mysql_fetch_array($rs)) {
                    $l2++;
                    if($l > $l2) {
                        ?>
                <tr>
                    <td><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
                    <td><?php
                                $logid = $row['logid'];
                                $logarr = explode(chr(10),$row['logupdate']);
                                $logupdate = html_entity_decode(implode("<br>",$logarr));
                                $sql = "SELECT * FROM ".$dbref."_task_attachments WHERE logid=".$row['logid'];
                                $rs2 = mysql_query($sql);
                                $attachments = "";
                                while($row3 = mysql_fetch_array($rs2)) {
                                    $file = "../files/$cmpcode/$modref/".$row3['system_filename'];
                                    if(!file_exists($file))
                                        continue;
                                    //$attachments .= "<a href='javascript:download(\"".$file."\")'>".$row3['original_filename']."</a>&nbsp;&nbsp;";
						$attachments.= "<a href='view_dl.php?d=".$row3['id']."'>".$row3['original_filename']."</a>&nbsp;&nbsp;";
                                    if($task['tasktkid'] == $tkid) {
                                        $button = "<input type='button' onclick='delAttachment(\"$logid\",\"".$row3['id']."\")' value='Delete' id='deleteAttachment' name='deleteAttachment'><br />";
                                    }
                                    $attachments .= $button;
                                }
                                echo($logupdate);
                                if(!empty($attachments))
                                    echo "<br />Attachments:<br />".$attachments;

                                ?>&nbsp;</td>
                    <td><?php
                                echo($row['value']);
                                if($row['pkey'] == "3" || $row['pkey'] > 5) {
                                    echo("<br>(".$row['logstate']."%)");
                                }
                                ?>&nbsp;</td>
                </tr>
                        <?php
                    }
                }
                ?>
            </table>

        </form>
        <script language=JavaScript>
            var statu = document.taskupdate.logstatusid.value;
            if(statu == "5")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        </script>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </body>

</html>
