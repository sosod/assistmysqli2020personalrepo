<?php
$tkname = "User Name Here";
$cmpcode = "ign0001";
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: New Task</b></h1>
<p><i>Please note that all fields are required unless otherwise indicated.</p>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class=tdgeneral valign="top"><b>Assigned to:</b></td>
		<td class=tdgeneral valign="top">
            <select size="1" name="tasktkid">
                <option value=X>--- SELECT ---</option>
            </select>
        </td>
		<td class=tdgeneral valign="top"><b>Assigned by:</b><br><i>(Task owner)</i></td>
		<td class=tdgeneral valign="top"><?php echo($tkname); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral valign="top"><b>Topic:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="tasktopicid">
                <option value=X>--- SELECT ---</option>
            </select>
        </td>
		<td class=tdgeneral valign="top"><b>Priority:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="taskurgencyid">
                <option value=X>--- SELECT ---</option>
            </select>
        </td>
	</tr>
	<tr>
		<td class=tdgeneral valign="top"><b>Status:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="taskstatusid" onchange="deadLine()">
                <option value=X>--- SELECT ---</option>
            </select>
        </td>
		<td class=tdgeneral valign="top"><b>Deadline:</td>
		<td class=tdgeneral valign="top"><input type="text" name="dday" size="3"> <select size="1" name="dmon">
            <?php
                $nmon = date("n");
                $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                $m = 0;
                $m2 = 1;
                while($m<=11)
                {
                    if($m2 == $nmon)
                    {
                        echo("<option selected value=".$m2.">".$months[$m]."</option>");
                    }
                    else
                    {
                        echo("<option value=".$m2.">".$months[$m]."</option>");
                    }
                    $m++;
                    $m2++;
                }
            ?>
            </select> <input type="text" name="dyear" size="6" value=<?php echo(date("Y")); ?>>
        </td>
	</tr>
	<tr>
		<td class=tdgeneral colspan=2 valign="top"><b>Task Instructions:</b><br>
        <textarea rows="5" name="taskaction" cols="35"></textarea></td>
		<td class=tdgeneral colspan=4 valign="top"><b>Task Deliverables:</b><br>
        <textarea rows="5" name="taskdeliver" cols="35"></textarea></td>
	</tr>
	<tr>
		<td class=tdgeneral colspan="4" valign="top">
		<input type="submit" value="Submit" name="B1">
		<input type="reset" value="Reset" name="B2"></td>
	</tr>
</table>
</form>
</body>

</html>
