<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET USER ACCESS - IF 20 THEN NEW TASKS ONLY TO SELF ELSE CAN ASSIGN TO OTHERS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function deadLine(){
    var statusid = document.newtask.taskstatusid.value;
    if(statusid == "ON")
    {
        document.newtask.dday.disabled = true;
        document.newtask.dmon.disabled = true;
        document.newtask.dyear.disabled = true;
    }
    else
    {
        document.newtask.dday.disabled = false;
        document.newtask.dmon.disabled = false;
        document.newtask.dyear.disabled = false;
    }
}

function Validate(tform){
    var tasktkid = "";
    var taskadduser = tform.taskadduser.value;
    var taskurgencyid = tform.taskurgencyid.value;
    var taskstatusid = tform.taskstatusid.value;
    var dday = tform.dday.value;
    var tasktopicid = tform.tasktopicid.value;
    var taskaction = tform.taskaction.value;
    var taskdeliver = tform.taskdeliver.value;
    
    var tkc = document.getElementById('tkc').value;
    var tkc2 = parseInt(tkc)+1;
    var t=1;
    var target;
    var targ;
//    alert(tkc);
//    alert(tkc2);
    for(t=1;t<tkc2;t++)
    {
        targ = "tk"+t;
//        alert(targ);
        target = document.getElementById(targ).value;
//        alert(target)
        if(target == "X")
        {
            tasktkid = "X";
        }
    }
    
//    alert(tasktkid);
    
    if(tasktkid == "X" || taskurgencyid == "X" || taskstatusid == "X" || tasktopicid == "X" || taskaction.length == 0 || taskdeliver.length == 0)
    {
        alert("Please complete all required fields.");
        return false;
    }
    else
    {
        if(taskstatusid == "NW" && dday.length == 0)
        {
            alert("Please complete all required fields.");
            return false;
        }
        else
        {
            return false;
        }
    }
    return false;
}

function newTkid() {

    var tkform1 = document.getElementById('tkform1').value;
    var tkform2 = document.getElementById('tkform2').value;
    var tkc = document.getElementById('tkc').value;
    tkc++;
    var tkform = tkform1+tkc+tkform2;
    document.getElementById('tkc').value = tkc;
    var newtkid = document.getElementById('newtasktkid').innerHTML;
    if(newtkid.length>0)
    {
        document.getElementById('newtasktkid').innerHTML = newtkid+"<br>"+tkform;
    }
    else
    {
        document.getElementById('newtasktkid').innerHTML = newtkid+tkform;
    }

}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo $modtext;?>: New <?php echo ucfirst($actname);?></b></h1>
<p><i>Please note that all fields are required unless otherwise indicated.</p>
<form name=newtask method=post action=new_process.php onsubmit="return Validate(this);" language=jscript>
<table border="1" id="table1" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border: 1px solid" width=600>
	<tr>
		<td class=tdheaderl valign="top"><b>Assigned to:</b></td>
		<td class=tdgeneral valign="top">
		<?php
        if($tact == 20) //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
        {
            echo($tkname."<input type=hidden size=5 name=tasktkid value=".$tkid.">");
        }
        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
        {

            $form = "<select size=\"1\" name=\"tasktkid[]\" id=tk";
            $form2 = ">";
                $form2.= "<option value=X>--- SELECT ---</option>";

                    $sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $form2.= "<option value=".$row['tkid'].">".$row['name']."</option>";
                    }
                    mysql_close();

            $form2.= "</select>";
            echo($form."1".$form2."&nbsp;<input type=button value=\" + \" onclick=\"newTkid();\" class=button style=\"padding: 0 0 0 0;\">");
        }
        ?><div id=newtasktkid></div>
        </td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Assigned by:</b></td>
		<td class=tdgeneral valign="top"><?php echo($tkname); ?><input type=hidden size=5 name=taskadduser value=<?php echo($tkid); ?>></td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Topic:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="tasktopicid">
                <option value=X>--- SELECT ---</option>
                <?php
                    $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                    }
                    mysql_close();
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Priority:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="taskurgencyid">
                <option value=X>--- SELECT ---</option>
                <?php
                    $sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                    }
                    mysql_close();
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Status:</td>
		<td class=tdgeneral valign="top">
            <select size="1" name="taskstatusid" onchange="deadLine()">
                <option value=X>--- SELECT ---</option>
                <?php
                    $sql = "SELECT * FROM ".$dbref."_list_status WHERE state < 1 AND state > -100 AND yn = 'Y' ORDER BY sort";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        if($row['id'] == "NW")
                        {
                            echo("<option selected value=".$row['id'].">".$row['value']."</option>");
                        }
                        else
                        {
                            echo("<option value=".$row['id'].">".$row['value']."</option>");
                        }
                    }
                    mysql_close();
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Deadline:</td>
		<td class=tdgeneral valign="top"><input type="text" name="dday" size="3"> <select size="1" name="dmon">
            <?php
                $nmon = date("n");
                $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                $m = 0;
                $m2 = 1;
                while($m<=11)
                {
                    if($m2 == $nmon)
                    {
                        echo("<option selected value=".$m2.">".$months[$m]."</option>");
                    }
                    else
                    {
                        echo("<option value=".$m2.">".$months[$m]."</option>");
                    }
                    $m++;
                    $m2++;
                }
            ?>
            </select> <input type="text" name="dyear" size="6" value=<?php echo(date("Y")); ?>>
        </td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><?php echo ucfirst($actname);?> Instructions:</td>
        <td class=tdgeneral valign="top"><textarea rows="5" name="taskaction" cols="50"></textarea></td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><?php echo ucfirst($actname);?> Deliverables:</td>
        <td class=tdgeneral valign="top"><textarea rows="5" name="taskdeliver" cols="50"></textarea></td>
	</tr>
	<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    ?>
	<tr>
		<td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
		<td class=tdgeneral valign="top">
        <?php
            switch($row['udfilist'])
            {
                case "Y":
                    echo("<select name=".$row['udfiid']."><option value=X>---SELECT---</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                        }
                    mysql_close($rs2);
                    echo("</select>");
                    break;
                case "T":
                    echo("<input type=text name=".$row['udfiid']." size=50>");
                    break;
                case "M":
                    echo("<textarea name=".$row['udfiid']." rows=5 cols=40></textarea>");
                    break;
                default:
                    echo("<input type=text name=".$row['udfiid']." size=50>");
                    break;
            }
        ?>
        </td>
	</tr>
<?php
}
mysql_close();
?>
	<tr>
<!--		<td class=tdheaderl valign="top">&nbsp;</td> -->
		<td class=tdgeneral valign="top" colspan=2>
		<input type="submit" value="Submit" name="B1" style="margin-left:142px">
		<input type="reset" value="Reset" name="B2"><span id=extra><textarea name=tkf id=tkform1 rows=1><?php echo($form); ?></textarea><textarea name=tkf id=tkform2 rows=1><?php echo($form2); ?></textarea></span><input type=hidden name=tkc id=tkc value=1></td>
	</tr>
</table>
<script language=JavaScript>
    document.getElementById('extra').style.visibility = "hidden";
</script>
</form>
</body>

</html>
