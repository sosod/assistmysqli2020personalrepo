<?php include("inc_ignite.php");

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
    </head>
    <link rel="stylesheet" href="/lib/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b><?php echo $modtext;?>: <?php echo ucfirst($actname);?> Update</b></h1>
        <?php
//GET DATA FROM FORM

        $logdate = $today;
        $logupdate = $_POST['logupdate'];
        $logstatusid = $_POST['logstatusid'];
        $logstate = $_POST['logstate'];
        $taskid = $_POST['taskid'];

//REMOVE ' FROM UPDATE
        //$apos = explode("'",$logupdate);
        //$logupdate = implode("&#39",$apos);
        //$apos = "";
		$logupdate = code($logupdate);


//GET STATE FOR GIVEN STATUS IF NOT IP
        /*if($logstatusid != "IP")
{
    $sql = "SELECT state FROM ".$dbref."_list_status WHERE id = '".$logstatusid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $logstate = $row['state'];
    mysql_close();
}
        */
//GET OLD STATUSID - USED FOR REDIRECT AFTER UPDATE

        //$sql = "SELECT * FROM ".$dbref."_task t INNER JOIN ".$dbref."_task_recipients r ON (t.taskid=r.taskid) WHERE t.taskid=$taskid AND r.tasktkid='$tkid'";
        $sql = "SELECT * FROM ".$dbref."_task t WHERE t.taskid=$taskid";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $oldstatus = $row['taskstatusid'];
        $tasktkid = $row['tasktkid'];
        $taskadduser = $row['taskadduser'];
        $tasktopicid = $row['tasktopicid'];
        $taskaction = $row['taskaction'];
        $taskdeliver = $row['taskdeliver'];
        $taskdeadline = $row['taskdeadline'];
        $taskstate = $row['taskstate'];
        mysql_close();
        $row = "";

//GET STATE FOR GIVEN STATUS IF NOT IP
        if($logstatusid == "3" || $logstatusid > 5) {
            if(strlen($logstate)==0 || !is_numeric($logstate)) {
                $logstate = $taskstate;
            }
            if(strlen($logstate)==0 || !is_numeric($logstate)) {
                $logstate = 0;
            }
        }
        else {
            $sql = "SELECT state FROM ".$dbref."_list_status WHERE pkey = '".$logstatusid."'";
            include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $logstate = $row['state'];
            mysql_close();
        }


//UPDATE TA_TASK RECORD
        $sql = "UPDATE ".$dbref."_task SET ";
        $sql .= "taskstatusid = '".$logstatusid."', ";
        $sql .= "taskstate = ".$logstate." WHERE ";
        $sql .= "taskid = ".$taskid;
        $sqltask = $sql;
        include("inc_db_con.php");


//GET TASK DETAILS
        $sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
        include("inc_db_con.php");
        $task = mysql_fetch_array($rs);
        mysql_close();


//ADD RECORD TO TA_LOG
        $sql = "INSERT INTO ".$dbref."_log SET ";
        $sql .= "logdate = '".$today."', ";
        $sql .= "logtkid = '".$tkid."', ";
        $sql .= "logupdate = '".$logupdate."', ";
        $sql .= "logstatusid = '".$logstatusid."', ";
        $sql .= "logstate = ".$logstate.", ";
        $sql .= "logemail = 'N', ";
        $sql .= "logsubmittkid = '".$tkid."', ";
        $sql .= "logtaskid = '".$taskid."', ";
        $sql .= "logtasktkid = '".$task['tasktkid']."'";
        $sqllog = $sql;
        include("inc_db_con.php");

//GET LOG ID
        $sql = "SELECT logid FROM ".$dbref."_log WHERE logtaskid = ".$taskid." ORDER BY logid DESC";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $logid = $row['logid'];
        mysql_close();

//CMP LOG UPDATE TA_TASK
        $trans = "Updated $modref task record ".$taskid.".";
        $tsql = str_replace("'","|",$sqltask);
        $tref = $modref;
        include("inc_transaction_log.php");

//CMP LOG NEW TA_LOG
        $trans = "Added $modref log record ".$logid.".";
        $tsql = str_replace("'","|",$sqllog);
        $tref = $modref;
        include("inc_transaction_log.php");

//NOTIFY TASK-ADDUSER
        //Get tasktkid details
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tasktkid."'";
        include("inc_db_con.php");
        $rowtkid = mysql_fetch_array($rs);
        mysql_close();
        //Get taskadduser details
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$taskadduser."'";
        include("inc_db_con.php");
        $rowadduser = mysql_fetch_array($rs);
        mysql_close();
        //Get topic details
        $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id = ".$tasktopicid;
        include("inc_db_con.php");
        $rowtopic = mysql_fetch_array($rs);
        mysql_close();
        //Get status details
        $sql = "SELECT * FROM ".$dbref."_list_status WHERE id = '".$logstatusid."'";
        include("inc_db_con.php");
        $rowstatus = mysql_fetch_array($rs);
        mysql_close();
        //Set email variables
        $subject = "";
        $message = "";
        $userFrom = $rowadduser['tkname']." ".$rowadduser['tksurname'];
        $to = $rowadduser['tkemail'];
        $from = $rowtkid['tkemail'];
        $subject = "Update to ".ucfirst($actname)." ".$taskid." on Ignite Assist";
        
        $message = $rowtkid['tkname']." ".$rowtkid['tksurname']." has updated a ".strtolower($actname)." assigned to them by you:\n";
        $message .= ucfirst($actname)." details:\n";
        $message .= "Topic:".$rowtopic['value']."\n";
        $taskaction = preg_replace('/(&#39)/', "'", stripslashes($taskaction));
         $taskdeliver = preg_replace('/(&#39)/', "'", stripslashes($taskdeliver));

        $message .= ucfirst($actname)." Instructions:\n".$taskaction."\n";
        $message .= ucfirst($actname)." Deliverables:\n".$taskdeliver."\n";
        
        $message .= "Deadline: ".date("d F Y",$taskdeadline)."\n";
        $message .= "Update details:\n";
        $message .= ucfirst($actname)." Update:\n".$logupdate."\n";
        $message .= ucfirst($actname)." Status: ".$rowstatus['value']."\n";
        if($logstatusid == "IP") {
            $message .= " (".$logstate."%)\n";
        } else {
            $message .= "\n";
        }
        $message .= "Please log onto Ignite Assist in order to view all the updates to this ".strtolower($actname).".\n";
        $message = nl2br($message);
        //Send email
if(strtoupper($cmpcode)!="IASSIST") {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=us-ascii" . "\r\n";
        $headers .= "From: $userFrom <".$from.">" . "\r\n";
        $headers .= "Reply-to: $userFrom <".$from.">" . "\r\n";
} else {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=us-ascii" . "\r\n";
        $headers .= "From: no-reply@ignite4u.co.za" . "\r\n";
        $headers .= "Reply-to: $userFrom <".$from.">" . "\r\n";
}
        //ini_set("sendmail_from",$from);
        if(strtolower($cmpcode) != 'wcc0001'){
            mail($to,$subject,$message,$headers);
        }
        //Update transaction log
        $trans = "Sent email to ".$rowtkid['tkname']." ".$rowtkid['tksurname']." for new ".strtolower($actname)." ".$taskid.".";
        $tsql = str_replace("'","|",$message);
        $tref = $modref;
        include("inc_transaction_log.php");
        $header = "";
        $message = "";


		
		
if(strtolower($cmpcode) == "fin0001") {
		$preset_status_id = $logstatusid;
		$preset_task_id = $taskid;
		$preset_task_owner = $taskadduser;
		//$preset_folder	=> set above
		//$preset_filename['o'=>'$original_filename','s'=>$system_filename]	=> set above
		include("inc_create_preset.php");
}
		
		
		
		
//CREATE FORM TO REDIRECT BACK TO TASK LIST
        echo("<form name=nw><input type=hidden name=tsk value=".$oldstatus."></form>");
        ?>
        <script language=JavaScript>
            var ntask = document.nw.tsk.value;
            if(ntask.length > 0)
            {
                document.location.href = "view_own.php?s="+ntask;
            }
        </script>
    </body>

</html>
