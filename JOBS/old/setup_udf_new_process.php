<?php
include("inc_ignite.php");
?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
    </head>

    <link rel="stylesheet" href="/lib/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc style="margin-bottom: -10px"><b><?php echo $modtext;?>: Setup - Create UDF</b></h1>
        <p>&nbsp;</p>
        <?php

        if(isset($_POST['required']) && $_POST['required'] == 'on')
        {
            $required = 1;
        }else
        {
            $required = 0;
        }
//GET DETAILS
        $value = $_POST['udfivalue'];
        $list = $_POST['udfilist'];
        $default = $_POST['default'];
        $ref = "$modref";
        $custom = "N";
        $sort = 0;
        $yn = "Y";

//format
        $value = htmlentities($value,ENT_QUOTES,"ISO-8859-1");

//GET SORT
        $sql = "SELECT max(udfisort) as isort FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref'";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        mysql_close();
        $sort = $row['isort']++;

        if($sort==0 || strlen($sort)==0)
        {
            $sort = 1;
        }

//ADD TO UDFINDEX
        $sql = "INSERT INTO assist_".$cmpcode."_udfindex SET ";
        $sql.= "  udfivalue = '".$value."'";
        $sql.= ", udfilist = '".$list."'";
        $sql.= ", udfiref = '".$ref."'";
        $sql.= ", udficustom = '".$custom."'";
        $sql.= ", udfisort = ".$sort."";
        $sql.= ", udfiyn = '".$yn."'";
        $sql.= ", udfirequired = $required";
        $sql.= ", udfidefault = '$default'";
        include("inc_db_con.php");
        $udfiid = mysql_insert_id();
        $tref = "$modref";
        $trans = "Created new UDF ".$udfiid;
        $tsql = $sql;
        include("inc_transaction_log.php");
        $sql = "INSERT INTO ".$dbref."_list_display SET ";
        $sql.= "  headingfull = '".$value."'";
        $sql.= ",  headingshort = '".$value."'";
        $sql.= ",  type = 'U'";
        $sql.= ",  field = '".$udfiid."'";
        $sql.= ",  mydisp = 'N'";
        $sql.= ",  myyn = 'Y'";
        $sql.= ",  owndisp = 'N'";
        $sql.= ",  ownyn = 'Y'";
        $sql.= ",  alldisp = 'N'";
        $sql.= ",  allyn = 'Y'";
        $sql.= ",  allsort = '0'";
        $sql.= ",  ownsort = '0'";
        $sql.= ",  mysort = '0'";
        $sql.= ", yn = '".$yn."'";
        include("inc_db_con.php");
        $udfiid = mysql_insert_id();
        $tref = "$modref";
        $trans = "Created new UDF ".$udfiid;
        $tsql = $sql;
        include("inc_transaction_log.php");

echo("<p>UDF '".$value."' has been created.</p>");
echo("<p>&nbsp;</p><p><a href=# onclick=\"document.location.href = 'setup.php'\" class=grey><img src=/pics/tri_left.gif align=absmiddle border=0></a> <a href=# onclick=\"document.location.href = 'setup.php'\" class=grey>Go Back to Setup</a></p>");

?>

    </body>

</html>
