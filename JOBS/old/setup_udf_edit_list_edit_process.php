<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b><?php echo $modtext;?>: Setup - Edit List Item</b></h1>
<p>&nbsp;</p>
<?php
//GET DETAILS
$id = $_POST['id'];
$udf = $_POST['udf'];
$val = $_POST['val'];

//FORMAT
$val = htmlentities($val,ENT_QUOTES,"ISO-8859-1");


if($id > 0 && strlen($val)>0)       //CHECK FOR DETAILS
{
    //CHECK THAT RECORD EXISTS
    $chk = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$id;
    include("inc_db_con.php");
        $chk = mysql_num_rows($rs);
        $old = mysql_fetch_array($rs);
    mysql_close();
    if($chk > 0)    //IF CHECK POSITIVE
    {
        ?>
        <p>Processing...</p>
        <?php
        //UPDATE RECORD
        $sql = "UPDATE assist_".$cmpcode."_udfvalue SET ";
        $sql.= "udfvvalue = '".$val."'";
        $sql.= " WHERE udfvid = ".$id;
        include("inc_db_con.php");
            $tref = "$modref";
            $trans = "Updated UDF List Item ".$id." from ".$old['udfvvalue']." to ".$val;
            $tsql = $sql;
            $told = "UPDATE assist_".$cmpcode."_udfvalue SET ";
            $told.= "udfvvalue = '".$old['udfvvalue']."'";
            $told.= " WHERE udfvid = ".$id;
            include("inc_transaction_log.php");
        //SEND BACK
        echo("<script language=JavaScript>");
        echo("    document.location.href = \"setup_udf_edit_list.php?id=".$udf."\";");
        echo("</script>");
    }
    else    //IF CHECK NEGATIVE
    {
        ?>
        <p>An error has occurred (chk).  Please go back and try again.</p>
        <p>&nbsp;</p>
        <p><a href=# onclick="document.location.href = 'setup_udf_edit.php'" class=grey><img src=/pics/tri_left.gif align=absmiddle border=0></a> <a href=# onclick="document.location.href = 'setup_udf_edit.php'" class=grey>Go Back</a></p>
        <?php
    }
}
else    //IF DETAILS CHECK NEGATIVE
{
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <p>&nbsp;</p>
        <p><a href=# onclick="document.location.href = 'setup_udf_edit.php'" class=grey><img src=/pics/tri_left.gif align=absmiddle border=0></a> <a href=# onclick="document.location.href = 'setup_udf_edit.php'" class=grey>Go Back</a></p>
        <?php
}
?>

</body>

</html>
