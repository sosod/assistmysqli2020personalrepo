<?php
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('url'=>"setup_udf.php",'txt'=>"User Defined Fields (UDFs)"),
	array('txt'=>"Edit List"),
);
$page = array("setup","udf");
$get_udf_link_headings = true;
require_once 'inc_header.php';
?>
<script type=text/javascript>
function delList(i,val,u) {
    if(confirm("Are you sure you wish to delete '"+val+"'?")==true)
    {
        document.location.href = "setup_udf_edit_list_del.php?id="+i+"&udf="+u;
    }
}
function editList(i,u) {
    if(i > 0 && u > 0)
    {
        document.location.href = "setup_udf_edit_list_edit.php?id="+i+"&udf="+u;
    }
    else
    {
        alert("An error has occurred.  Please try again.");
    }
}
function Validate(me) {
    var val = me.udfvvalue.value;
    if(val.length==0)
    {
        document.getElementById('uv').className = 'req';
        alert("Please enter a List Item before clicking the 'Add' button.");
        document.getElementById('uv').focus();
        return false;
    }
    else
    {
        return true;
    }
}
</script>
<?php
$id = $_REQUEST['id'];
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiid = ".$id;
$uindex = mysql_fetch_one($sql);
?>
<h2 class=fc>UDF: <u><?php echo($uindex['udfivalue']); ?></u></h2>
<form name=addlist action=setup_udf_edit_list_process.php method=POST>
<table>
    <tr>
        <th>Ref</th>
        <th>List Item</th>
        <th></th>
    </tr>
	<tr>
        <th>&nbsp;</th>
        <td><input type=text size=50 maxlength=50 name=udfvvalue /></td>
        <td class=center><input type=button value=Add class=isubmit /></td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$id." AND udfvyn = 'Y' ORDER BY udfvvalue";
$items = mysql_fetch_all($sql);
foreach($items as $row) {
        ?>
	<tr>
        <th><?php echo($row['udfvid']); ?></th>
        <td><?php echo($row['udfvvalue']); ?></td>
        <td class=center><input type=button value=Edit /></td>
    </tr>
        <?php
}
?>
</table>
</form>
<script type=text/javascript>
$(function() {
	$("form[name=addlist] input:text[name=udfvvalue]").focus();
});
</script>
</body>

</html>
