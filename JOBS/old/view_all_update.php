<?php
include("inc_ignite.php");



//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();

//GET TASKS DETAILS
$taskid = $_GET['i'];
$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
    </head>
    <script language=JavaScript>
        function statusPerc() {
            var stat = document.taskupdate.logstatusid.value;
            if(stat != "3" || stat > 5)
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
            else
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;

            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
            function editTask(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }
</script>
    <link rel="stylesheet" href="/lib/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b><?php echo $modtext;?>: Update <?php echo ucfirst($actname);?></b></h1>
        <h2 class=fc><?php echo ucfirst($actname);?> Status</h2>
        <table border="1" id="table1" cellspacing="0" cellpadding="4" width=600>
            <tr>
                <td class=tdheaderl width=140 valign="top"><b>Assigned to:</b></td>
                <td class=tdgeneral width=460 valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN ".$dbref."_task_recipients r ON (u.tkid=r.tasktkid) WHERE r.taskid= '".$task['taskid']."'";
                    // echo $sql;
                    include("inc_db_con.php");
                    $assignees = "";
                    while($row = mysql_fetch_array($rs)) {
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Assigned by:</b></td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['taskadduser']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['tkname']." ".$row['tksurname']);
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Topic:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id = '".$task['tasktopicid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
$topicval = $row['value'];
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Priority:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE id = '".$task['taskurgencyid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Status:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey = '".$task['taskstatusid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    if($task['taskstatusid'] == "3") {
                        echo(" (".$task['taskstate']."%) ");
                    }
                    else {
                        if($task['taskstatusid'] != "5") {
                            echo(" (".$row['state']."%) ");
                        }
                    }
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Deadline:</td>
                <td class=tdgeneral valign="top"><?php echo(date("d-M-Y",$task['taskdeadline'])); ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b><?php echo ucfirst($actname);?> Instructions:</b></td>
                <td class=tdgeneral valign=top>
<?php
$action = explode(chr(10),$task['taskaction']);
$taskaction = implode("<br>",$action);
                    echo($taskaction);
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b><?php echo ucfirst($actname);?> Deliverables:</b></td>
                <td class=tdgeneral valign=top>
<?php
$deliver = explode(chr(10),$task['taskdeliver']);
$taskdeliver = implode("<br>",$deliver);
                    echo($taskdeliver);
                    ?></td>
            </tr>
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
include("inc_db_con.php");
            while($row = mysql_fetch_array($rs)) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                include("inc_db_con2.php");
                $udf = mysql_num_rows($rs2);
                $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                ?>
            <tr>
                <td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
                <td class=tdgeneral valign="top">
                <?php
    if($udf>0) {
        switch($row['udfilist']) {
            case "Y":
                                    if(checkIntRef($row2['udfvalue'])) {
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                        include("inc_db_con2.php");
                                        $row3 = mysql_fetch_array($rs2);
                                        echo($row3['udfvvalue']);
                                        mysql_close($con2);
                                    }else {
                                        echo ("N/A");
                                    }
                                    break;
                                case "T":
                                //$sql = "SELECT * FROM assist_".$cmpcode."_".
                                    if($row2['udfvalue'])
                                        echo $row2['udfvalue'];
                                    else
                                        echo "N/A";
                                    break;
                                default:
                                    echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                    break;
                            }
                        }
                        else {
                            echo("N/A");
                        }
                        ?>
                    &nbsp;</td>
            </tr>
                        <?php
                    }
//mysql_close();
                    ?>
                    <?php
$sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE att.taskid = ".$taskid;
include("inc_db_con.php");
            if(mysql_num_rows($rs) > 0) {
                echo ("<tr><td  class=tdheaderl valign='top'>Attachment(s):</td><td><table style='border:none;'>");
                while($row = mysql_fetch_assoc($rs)) {
                    $filename = "../files/$cmpcode/$modref/".$row['system_filename'];
                    if(!file_exists($filename))
                        continue;
                    $taskattach_id = $row['id'];
                    echo ("<tr><td  style='border:none;'>");
//<a href='javascript:redirect(\"$filename\")' >");
			echo "<a href='view_dl.php?d=".$row['id']."'>";
                    echo($row['original_filename']."<br />");
                    echo ("</a></td>");
                    //echo("<td  style='border:none;'><input type='button' name='deleteAttachment' id='deleteAttachment' value='Delete' onclick='deleteAttachment($taskid,$taskattach_id)' /></td></tr>");
                }

                echo ("</table></td></tr>");
            }
            ?>
            <?php
            if($task['taskstatusid']!="CL" && $topicval != 'Travel Requests') {
                ?>
            <tr>

                <td class=tdgeneral colspan=4>
                    <input type="button" value="Edit"  onclick="editTask(<?php echo($taskid);?>);"/>
                    
            </tr>
                <?php
            }
            ?>
        </table>
            <?php
//IF THE TASK IS NOT NEW = THERE MUST BE UPDATES TO VIEW
//OR IF THE TASK TKID = CURRENT USER THEN MUST ALLOW ADDING OF UPDATE
//THEREFORE DISPLAY TABLE
            if($task['taskstatusid'] != "NW" || $task['tasktkid'] == $tkid) {
                ?>
        <h2 class=fc><?php echo ucfirst($actname);?> Updates</h2>
        <form name=taskupdate method=post action=view_all_update_process.php onsubmit="return Validate(this);" language=jscript>
            <input type=hidden name=taskid value=<?php echo($taskid);?>>
            <table border="1" id="table1" cellspacing="0" cellpadding="5" width=600>
                <tr>
                    <td valign="top" class=tdheader>Date</td>
                    <td valign="top" class=tdheader><?php echo ucfirst($actname);?> Update</td>
                    <td valign="top" class=tdheader><?php echo ucfirst($actname);?> Status</td>
                </tr>
    <?php
//IF THE TASK ISN'T CLOSED AND THE TASK TKID = CURRENT USER THEN DISPLAY THE UPDATE BOX
    if($task['taskstatusid'] != "CL" && $task['tasktkid'] == $tkid && $tkid == "0000") {
        ?>
                <tr>
                    <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$today)); ?></td>
                    <td valign="top" class=tdgeneral><textarea rows="5" name="logupdate" cols="35"></textarea></td>
                    <td valign="top" class=tdgeneral><select size="1" name="logstatusid" onchange=statusPerc()>
                        <?php
                        $sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND state > -100 ORDER BY sort";
                        include("inc_db_con.php");
                        while($row = mysql_fetch_array($rs)) {
                            if($task['taskstatusid'] != "ON") {
                if($row['id'] == "IP") {
                    echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                }
                else {
                                                if($row['id'] != "NW") {
                                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                }
                                            }
                                        }
                                        else {
                                            if($row['id'] == "ON") {
                                                echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                            }
                                            else {
                                                if($row['id'] != "NW") {
                                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                }
                                            }
                                        }
                                    }
                                    ?>
                        </select><br>&nbsp;<br>
                        <input type="text" name="logstate" size="4" value=<?php echo($task['taskstate']); ?>>% complete</td>
                </tr>
                <tr>
                    <td valign="top" colspan="3" class=tdgeneral>
                        <input type="submit" value="Submit" name="B1">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
                                    <?php
                                }
                                else {
                                    echo("<input type=hidden name=logstatusid value=1>");
                                }

//DISPLAY THE UPDATES ON THE CURRENT TASK
                                $sql = "SELECT * FROM ".$dbref."_log l, ".$dbref."_list_status s WHERE l.logstatusid = s.pkey AND l.logtaskid = ".$taskid." ORDER BY logid DESC";
                                include("inc_db_con.php");
                                $l = mysql_num_rows($rs);
    $l2 = 0;
    while($row = mysql_fetch_array($rs)) {
        $l2++;
        if($l > $l2) {
            ?>
            <?php include("inc_tr.php"); ?>
                <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
                <td valign="top" class=tdgeneral ><?php
                            $logarr = explode(chr(10),$row['logupdate']);
                            $logupdate = implode("<br>",$logarr);
                            $logupdate = html_entity_decode($logupdate);
                            $sql = "SELECT * FROM ".$dbref."_task_attachments WHERE logid=".$row['logid'];
                            $rs2 = mysql_query($sql);
                            $attachments = "";
                            while($row3 = mysql_fetch_array($rs2)) {
                                $file = "../files/$cmpcode/$modref/".$row3['system_filename'];
                                if(!file_exists($file))
                                    continue;
//                                $attachments .= "<a href='javascript:download(\"".$file."\")'>".$row3['original_filename']."</a><br />";
						$attachments.= "<a href='view_dl.php?d=".$row3['id']."'>".$row3['original_filename']."</a>&nbsp;&nbsp;";
                            }
                            echo($logupdate);
                            if(!empty($attachments))
                                echo "<br />Attachments:<br />".$attachments;

                            ?>&nbsp;</td>
                <td valign="top" class=tdgeneral><?php
                            echo($row['value']);
            if($row['logstatusid'] == "3") {
                echo("<br>(".$row['logstate']."%)");
                                }
                                ?>&nbsp;</td>
                </tr>
                                <?php
                            }
                        }
                        ?>
            </table>

        </form>
        <script language=JavaScript>
            var statu = document.taskupdate.logstatusid.value;
            if(statu == "5")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        </script>
                        <?php
                    }
                    ?>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

    </body>

</html>
