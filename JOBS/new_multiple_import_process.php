<?php include("inc_ignite.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.yes-th { background-color: #009900; color: #ffffff; font-weight: bold; }
.yes-td { background-color: #FFFFFF; color: #000000; font-weight: normal; }
.no-th { background-color: #CC0001; color: #FFFFFF; }
.no-td { background-color: #FFFFFF; color: #CC0001; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
	echo "<h1 class=fc>".$modtext.": New ".$actname." - Import</h1>";
//USERS
$users = array();
$usersid = array();
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users u";
$sql.= " WHERE t.tkstatus = 1 AND t.tkid = a.tkid AND a.yn = 'Y' AND t.tkid = u.usrtkid AND u.usrmodref = '".strtoupper($modref)."'";
$sql.= " ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$id = $row['tkid'];
	$n = decode($row['tkname']."&nbsp;".$row['tksurname']);
	$users[$id] = array('id'=>$id,'value'=>$n);
}
mysql_close($con);
//PRIORITY
$lists = array();
$sql = "SELECT * FROM ".$dbref."_list_urgency WHERE yn = 'Y'";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$lists['taskurgencyid'][$row['id']] = $row;
}
mysql_close($con);
//STATUS
//TOPIC
$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y'  ORDER BY value";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$lists['tasktopicid'][$row['id']] = $row;
}
mysql_close($con);
/*** UDFs ***/
$sql = "SELECT v.* FROM assist_".$cmpcode."_udfvalue v, assist_".$cmpcode."_udfindex u WHERE u.udfiref = '".strtoupper($modref)."' AND ( udfiobject = '' OR udfiobject = 'action' ) AND u.udfiyn = 'Y' AND u.udfiid = v.udfvindex AND v.udfvyn = 'Y'";
//$sql = "SELECT v.* FROM assist_".$cmpcode."_udfvalue v, assist_".$cmpcode."_udfindex u WHERE u.udfiref = '".strtoupper($modref)."' AND u.udfiyn = 'Y' AND u.udfiid = v.udfvindex AND v.udfvyn = 'Y'";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$lists[$row['udfvindex']][$row['udfvid']] = $row;
}
mysql_close($con);
/*** HEADINGS ***/
$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND type = 'S'";
include("inc_db_con.php");
while($row = mysql_fetch_assoc($rs)) {
	$heading[$row['field']] = $row;
}
mysql_close($con);
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".strtoupper($modref)."' AND udfiyn = 'Y' AND (udfiobject = '' OR udfiobject = 'action') ORDER BY udfisort, udfivalue";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$row['type'] = "U";
	$heading[$row['udfiid']] = $row;
	$udfs[] = $row['udfiid'];
}
mysql_close($con);

/****************** GET DATA FROM FORM ***********************/
	$variables = $_REQUEST;
	$count = $variables['count'];
	//NEW TOPICS
	$newtop = $variables['newtopic'];
	$newtopics = array();
	foreach($newtop as $nt) {
		$n = strFn("explode",$nt,"|_|","");
		if(count($n)==2 && strlen($n[1])>0) {
			$sql = "INSERT INTO ".$dbref."_list_topic (value,yn) VALUES ('".code($n[1])."','Y')";
			include("inc_db_con.php");
				$id = mysql_insert_id($con);
			$newtopics[$n[0]] = $id;
		}
	}

/****************** DISPLAY DATA ****************************/
echo "<table cellpadding=3 cellspacing=0>";
//HEADINGS
	echo "<tr><th>Ref</th>";
	foreach($heading as $h) {
		echo "<th>";
		switch($h['type']) {
			case "U": echo $h['udfivalue']; break;
			default: echo $h['headingfull'];
		}
		echo "</th>";
	}
	echo "</tr>";
//DATA
for($c=0;$c<$count;$c++) {
	$err = "N";
	$var = array();
	//get data
	foreach($heading as $h) {
		switch($h['type']) {
			case "U": 
				$hf = $h['udfiid'];
				$var[$hf] = $variables[$hf][$c];
				break;
			default: 
				$hf = $h['field'];
				switch($hf) {
					case "tasktkid":
						$t = $c+1;
						$var[$hf] = $variables['tasktkid'.$t];
						if(count($var[$hf])==0) $err = "Recipient";
						break;
					case "taskadddate":
						$var[$hf] = $today;
						break;
					case "tasktopicid":
						$topic = $variables[$hf][$c];
						if(!checkIntRef($topic))
							$topic = $newtopics[$topic];
						if(checkIntRef($topic))
							$var[$hf] = $topic;
						else
							$err = "Topic";
						break;
					case "taskdeadline":
						$d = $variables[$hf][$c];
						if(strlen($d)>0) {
							$d = strFn("explode",$d,"-","");
							$d = mktime(23,59,59,$d[1],$d[0],$d[2]);
						} elseif($variables['taskstatusid']==5) {
							$d = "";
						} else {
							$d = ""; $err = "Deadline";
						}
						$var[$hf] = $d;	
						break;
					case "taskurgencyid":
						$var[$hf] = $variables[$hf][$c];
						if(!checkIntRef($var[$hf])) $err = "Priority";
						break;
					default: 
						$var[$hf] = $variables[$hf][$c];
						if($hf == "taskaction" && strlen($var[$hf])==0) $err = "Instruction";
						if($hf == "taskadduser" && ($var[$hf]=="X" || strlen($var[$hf])==0)) $err = "Owner";
				}
				break;
		}
	}
	if($var['taskstatusid']==5) { $var['taskstate'] = -10; } else { $var['taskstate'] = 0; }
	if(!isset($var['taskurgencyid'])) { $var['taskurgencyid'] = 2; }
	//run sql
	if($err == "N") {
		//CREATE TASK
		$sql = "INSERT INTO ".$dbref."_task (tasktkid, taskurgencyid, tasktopicid, taskaction, taskdeliver, taskstatusid, taskstate, taskdeadline, taskadddate, taskadduser) VALUES ";
		$sql.= "('',".$var['taskurgencyid'].",".$var['tasktopicid'].",'".code($var['taskaction'])."','".code($var['taskdeliver'])."','".$var['taskstatusid']."',".$var['taskstate'].",'".$var['taskdeadline']."','".$var['taskadddate']."','".$var['taskadduser']."')";
		//include("inc_db_con.php");
			$taskid = db_insert($sql);
		if(checkIntRef($taskid)) {
			//TASK RECIPIENTS
			foreach($var['tasktkid'] as $tk) {
				$sql = "INSERT INTO ".$dbref."_task_recipients (taskid, tasktkid) VALUES ($taskid,'$tk')";
				include("inc_db_con.php");
			}
			//CREATE LOG
			$logupdate = "New ".strtolower($actname)." added.".chr(10)."$actname instructions: ".$var['taskaction'].chr(10)."$actname deliverables: ".$var['taskdeliver'];
			$sql = "INSERT INTO ".$dbref."_log SET ";
			$sql .= "logdate = '".$today."', ";
			$sql .= "logactdate = '".$today."', ";
			$sql .= "logtkid = '', ";
			$sql .= "logupdate = '".$logupdate."', ";
			$sql .= "logstatusid = '".$var['taskstatusid']."', ";
			$sql .= "logstate = ".$var['taskstate'].", ";
			$sql .= "logemail = 'N', ";
			$sql .= "logsubmittkid = '".$var['taskadduser']."', ";
			$sql .= "logtaskid = '".$taskid."', ";
			$sql .= "logtasktkid = '', ";
			$sql .= "logtype = 'C'";
			include("inc_db_con.php");
			//UDFS
			foreach($udfs as $u) {
				$uv = $var[$u];
				$ul = $heading[$u]['udfilist'];
				if($ul == "D" && strlen($uv)>0) {
					$uv = strFn("explode",$uv,"-","");
					$uv = date("d M Y",mktime(12,0,0,$uv[1],$uv[0],$uv[2]));
					$var[$u] = $uv;
				}
				if(strlen($uv)>0 && !($uv=="X" && $ul == "Y")) {
					$sql = "INSERT INTO assist_".$cmpcode."_udf (udfindex, udfvalue, udfnum, udfref) VALUES ($u,'".code($uv)."',$taskid,'".strtoupper($modref)."')";
					include("inc_db_con.php");
				}
			}
			$classth = "yes-th"; $classtd = "yes-td";
		} else {
			$classth ="no-th"; $classtd = "no-td"; $taskid = "ERR";
		}	
	} else {
		$classth ="no-th"; $classtd = "no-td"; $taskid = "ERR - ".$err;
	}
	//display result
	echo "<tr><th class=".$classth." >$taskid</th>";
	foreach($heading as $h) {
		switch($h['type']) {
			case "U": 
				echo "<td class=".$classtd.">";
				$hf = $h['udfiid'];
				$u = $var[$hf];
				switch($h['udfilist']) {
					case "Y":
						if(strlen($u)>0 && strlen($u)!="X") {
							echo $lists[$hf][$u]['udfvvalue'];
						}
						break;
					default:
						echo $u;
						break;
				}
				break;
			default: 
				$hf = $h['field'];
				$class = $classtd;
				switch($hf) {
					case "taskurgencyid":
					case "tasktopicid":
						$echo = $lists[$hf][$var[$hf]]['value'];
						if(strlen($echo)==0) { $class = "no-th"; $echo = "Required"; }
						break;
					case "tasktkid":
						$echo = "";
						if(count($var[$hf])>0) {
						foreach($var[$hf] as $t) {
							if(strlen($echo)>0) { $echo.="<br />"; }
							$echo.=$users[$t]['value'].";";
						}
						} else {
							$class = "no-th";
							$echo = "Required";
						}
						break;
					case "taskdeadline":
					case "taskadddate":
						$echo = date("d M Y",$var[$hf]);
						if(strlen($echo)==0) { $class = "no-th"; $echo = "Required"; }
						break;
					case "taskstatusid":
						if($var[$hf]==5) 
							$echo = "On-going";
						else
							$echo = "New";
						break;
					case "taskadduser":
						$echo = $users[$var[$hf]]['value'];
						if(strlen($echo)==0) { $class = "no-th"; $echo = "Required"; }
						break;
					default:
						$echo = $var[$hf];
						break;
				}
				echo "<td class=".$class.">";
				echo $echo;
				break;
		}
		echo "</td>";
	}
	echo "</tr>";
	//reset
	$sql = "";
	$taskid = "";
}	
echo "</table>";	
$urlback = "new_multiple.php";
include("inc_goback.php");
?>
</body>
</html>