<?php
//include("inc_ignite.php");
$page = array("setup","access");
require_once "inc_header.php";



include("inc_ta.php");
//GET EDIT VARIABLES
$result = "";
$aid = $_GET['id'];
$type = $_GET['t'];
$access = $_GET['a'];


$user_access = array(
	20 => "own ".ucfirst($actname)."s only",
	50 => "own & sub-ordinate ".ucfirst($actname)."s",
	90 => "own, sub-ordinate & all ".ucfirst($actname)."s",
);


if($type == "e")    //IF EDIT TYPE = EDIT
{
    //GET TRANSACTION LOG INFO
    $sql = "SELECT * FROM ".$dbref."_list_access WHERE id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ttkid = $row['tkid'];
        $taccess = $row['view'];
		$tacc = $row['access'];
    mysql_close();
    //PERFORM EDIT UPDATE
			$acc = 0;
			if($access>=20) { $acc+=JOBS_USER::VIEW_OWN; }
			if($access>=50) { $acc+=JOBS_USER::VIEW_SUB; }
			if($access>=90) { $acc+=JOBS_USER::VIEW_ALL; }
    $sql = "UPDATE ".$dbref."_list_access SET view = ".$access.", act = ".$access.", yn = 'Y', access = ".$acc." WHERE id = ".$aid;
    include("inc_db_con.php");
    //SET TRANSACTION LOG VALUES
    $tref = "$modref";
    $tsql = $sql;
    $trans = "Updated user ".$ttkid." access to ".$user_access[$access]." from ".$user_access[$taccess].".";
    //PERFORM TRANSACTION LOG UPDATE
    include("inc_transaction_log.php");
    $result = "done";
}
else
{
    if($type == "d")    //IF EDIT TYPE = DELETE
    {
        //UPDATE LIST-ACCESS AND SET USER TO N
        $sql = "UPDATE ".$dbref."_list_access SET yn = 'N' WHERE id = ".$aid;
        include("inc_db_con.php");
            //Set transaction log sql value for delete
            $transsql[0] = $sql;
        //GET DETAILS FOR TRANSFERRAL OF TASKS BACK TO TASK OWNER
        $sql = "SELECT tkid FROM ".$dbref."_list_access WHERE id = ".$aid;
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $tid = $row['tkid'];
        mysql_close();
            //Set transaction log transaction for delete
            $transaction[0] = "Removed user ".$tid." from $modref user access.";
        //GET TASK IDs FOR ALL INCOMPLETE OR ONGOING TASKS
        $sql = "SELECT taskid, taskadduser FROM ".$dbref."_task WHERE tasktkid = '".$tid."' AND taskstatusid <> 'CL' AND taskstatusid <> 'CN'";
        include("inc_db_con.php");
            $t = 0;
            while($row = mysql_fetch_array($rs))
            {
                $taskid[$t] = $row['taskid'];
                $taskown[$t] = $row['taskadduser'];
                $t++;
            }
        mysql_close();
        $tr = 1;    //Used for transaction log values array
        //LOOP THROUGH TASK IDs OBTAINS FROM PREVIOUS SQL STATEMENT AND UPDATE TASKTKID
        for($t2=0;$t2<$t;$t2++)
        {
            //UPDATE TASKTKID
            $sql = "UPDATE ".$dbref."_task SET tasktkid = '".$taskown[$t2]."' WHERE taskid = ".$taskid[$t2];
            include("inc_db_con.php");
                //Set transaction log details
                $transaction[$tr] = "Reverted ".strtolower($actname)." ".$taskid[$t2]." back to ".strtolower($actname)." owner ".$taskown[$t2]." due to removal of ".$tid." access to $modref.";
                $transsql[$tr] = $sql;
                $tr++;
            //GET TASK DETAILS FOR LOG UPDATE
            $sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid[$t2];
            include("inc_db_con.php");
                $rowtask = mysql_fetch_array($rs);
            mysql_close();
            //UPDATE TASK LOG TO INDICATE CHANGE OF OWNER
            $logupdate = "Reverted ".strtolower($actname)." back to task owner as assigned user was removed from ".ucfirst($actname)." Assist.";
            $sql = "INSERT INTO ".$dbref."_log SET ";
            $sql .= "logdate = '".$today."', ";
            $sql .= "logtkid = '".$taskown[$t2]."', ";
            $sql .= "logupdate = '".$logupdate."', ";
            $sql .= "logstatusid = '".$rowtask['taskstatusid']."', ";
            $sql .= "logstate = ".$rowtask['taskstate'].", ";
            $sql .= "logemail = 'N', ";
            $sql .= "logsubmittkid = '".$tkid."', ";
            $sql .= "logtaskid = '".$taskid[$t2]."', ";
            $sql .= "logtasktkid = '".$taskown[$t2]."'";
            include("inc_db_con.php");
                //Set transaction log details
                $transaction[$tr] = "Reverted ".strtolower($actname)." ".$taskid[$t2]." back to ".strtolower($actname)." owner ".$taskown[$t2]." due to removal of ".$tid." access to $modref.";
                $transsql[$tr] = $sql;
                $tr++;
        }
        //LOOP THROUGH TRANSACTION AND TRANSSQL ARRAY AND EXECURE SQL
        for($tr2=0;$tr2<$tr;$tr2++)
        {
            $tref = "$modref";
            $tsql = $transsql[$tr2];
            $trans = $transaction[$tr2];
            include("inc_transaction_log.php");
        }
        $result = "done";
    }
}

?>
<script language=JavaScript>
function editUser(id) {
    var acc = document.edit.access.value;
    document.location.href = "setup_access_edit.php?id="+id+"&t=e&a="+acc;
}

function delUser(id,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to delete "+tkn+".\n\nAny incomplete or ongoing tasks will be reassigned to their task owner."))
    {
        document.location.href = "setup_access_edit.php?id="+id+"&t=d";
    }
}
</script>
<h1 ><a href=setup.php class=breadcrumb>Setup</a> >> <a href=setup_access.php class=breadcrumb>User Access</a> >> Edit User</h1>
<?php
echo($transaction[1]);
//print_r($taskid);
if($result != "done")   //IF NO ACTION WAS TAKEN THEN DISPLAY EDIT FORM
{
?>
<table>
	<tr>
		<th>ID</td>
		<th>User</td>
		<th>Access</td>
	</tr>
<?php

    //GET USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.act, a.id, r.ruletext ";
    $sql .= "FROM ".$dbref."_list_access a, ".$dbref."_list_access_rules r, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE r.ruleint = a.act AND r.rulefn = 'act' AND yn = 'Y' AND t.tkid = a.tkid AND ruleint < 100 AND a.id = ".$aid;
	$users = mysql_fetch_all($sql);
    foreach($users as $row) {
        $tkn = $row['tkname']." ".$row['tksurname'];
?><form name=edit>
        <tr>
            <td class="tdgeneral"><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($tkn); ?></td>
		<td class="tdgeneral">Add/Update/View&nbsp;
            <select name=access>
				<?php
				foreach($user_access as $key=>$u) {
					echo "<option ".($row['act']==$key ? "selected" : "")." value=".$key.">".$u."</option>";
				}
				?>
            </select>
        </td>
        </tr></form>
<?php
    }
    
?>
        <tr>
            <td colspan=3 class="tdgeneral"><input type=button value="Save Changes" class=isubmit onclick="editUser(<?php echo($aid) ;?>)"> <span class=float><input type=button value=Delete onclick="delUser(<?php echo($aid) ;?>,'<?php echo(str_replace(" ","_",$tkn)) ;?>')" class=idelete /></span></td>
        </tr>
</table>
<?php
}
?>
<form name=result>
<input type=hidden name=res value=<?php echo($result); ?>>
</form>
<script language=JavaScript>
var res = document.result.res.value;
//IF ACTION WAS TAKEN (result = done) THEN REDIRECT
if(res == "done")
{
    document.location.href="setup_access.php";
}
</script>
</body>

</html>
