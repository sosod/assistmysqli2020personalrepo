<?php
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"Defaults"),
);
require_once 'inc_header.php';



//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(isset($_REQUEST['TA0']) && strlen($_REQUEST['TA0']) > 0) {
	$ta0 = $_REQUEST['TA0'];
    //GET CURRENT SETUP ADMIN TKID
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '$modref' AND refid = 0";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ta0z = $row['value'];
    mysql_close();
    $sql = "";
    //UPDATE ASSIST-SETUP TABLE
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0."' WHERE ref = '$modref' AND refid = 0";
    include("inc_db_con.php");
        //Set transaction log values for this update
        $transsql[0] = str_replace("'","|",$sql);
        $transaction[0] = "Updated $modref Setup Administrator to ".$ta0;
    $sql = "";
    //WRITE DOWN PREVIOUS SETUP ADMIN'S ACCESS
    $sql = "UPDATE ".$dbref."_list_access SET act = 90, view = 90 WHERE tkid = '".$ta0z."' AND yn = 'Y'";
    include("inc_db_con.php");
        //set transaction log values
        $transsql[1] = str_replace("'","|",$sql);
        $transaction[1] = "Updated $modref user access for the previous setup admin ".$ta0z." down to 90.";

    if($ta0 != "0000")  //If the setup admin is not the IA admin then change user access to 100
    {
        //Get access details from assist-ta-list-access
        $sql = "SELECT id FROM ".$dbref."_list_access WHERE tkid = '".$ta0."' AND yn='Y'";
        include("inc_db_con.php");
        $ta0c = mysql_num_rows($rs);
        if($ta0c > 0)   //If there is a result then set the sql variable to update the access value
        {
            $err = "N";
            $row = mysql_fetch_array($rs);
            $taccid = $row['id'];
            $sql0 = "UPDATE ".$dbref."_list_access SET act = 100, view = 100 WHERE id = ".$taccid;
                //set transaction log values
                $transsql[2] = str_replace("'","|",$sql);
                $transaction[2] = "Updated $modref user access for ".$ta0." to 100.";
        }
        else    //If there was no result then set the sql variable to insert a new record for the setup admin
        {
            $err = "Y";
            
            $sql0 = "INSERT INTO ".$dbref."_list_access SET tkid = '".$ta0."', act = 100, view = 100, yn = 'Y'";
                //Set the transaction log values
                $transsql[2] = str_replace("'","|",$sql);
                $transaction[2] = "Added $modref user access for ".$ta0." as 100.";
        }
        mysql_close();
        $sql = "";
        //RUN THE UPDATE SET IN PREVIOUS IF STATEMENT
        $sql = $sql0;
        include("inc_db_con.php");
        $sql = "";
    }
    else
    {
        //Set transaction log values if setup admin = IA admin
        $transsql[2] = "No work was done.";
        $transaction[2] = "Did not add $modref user access for ".$ta0." as tkid = 0000 (Administrator).";
    }

    //PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUSLY
    for($t=0;$t<3;$t++)
    {
        $trans = $transaction[$t];
        $tsql = $transsql[$t];
        $tref = $modref;
        include("inc_transaction_log.php");
    }

    $result = array("ok","Administrator update complete.");
}
else
{
    $result = array();
}



?>
<style type=text/css>
table th { text-align: left; vertical-align: top; }
table td { vertical-align: top; border-left-width: 0px; border-right-width: 0px;}
.tdbutton { text-align: right; padding-left: 15px; border-right-width: 1px;}
</style>
<script type=text/javascript>
function goTo(src,page) {
	switch(src) {
		case "L":
			document.location.href = page;
			break;
		case "U":
			//var loc = document.getElementById(page).value;
			document.location.href = "setup_"+page+".php";
			break;
		case "S":
			var loc = document.getElementById(page).value;
			if(page=='columns' && loc == 'HOME') {
				page = page + "_home";
			}
			document.location.href = "setup_"+page+".php?section="+loc;
			break;
	}
}
</script>
<?php displayResult($result); ?>
<form name=update method=post action=setup.php>
<table cellpadding=3 cellspacing=0>
	<tr>
		<th width=150>Administrator:</th>
		<td><select name=TA0>
<?php
//GET SETUP ADMIN TKID FROM ASSIST-SETUP
$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '$modref' AND refid = 0";
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0) {
    //IF RESULT THEN GET TKID
    $row = mysql_fetch_array($rs);
    $taadmin = $row['value'];
} else {
    //IF NO RESULT THEN SET SQL VARIABLE TO ADD DEFAULT 0000
    $sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '$modref', refid = 0, value = '0000', comment = 'Setup administrator', field = ''";
    include("inc_db_con.php");
    $taadmin = "0000";
}
mysql_close($con);
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($taadmin == "0000") {
	echo "<option selected value=0000>Ignite Assist Administrator</option>";
} else {
	echo "<option value=0000>Ignite Assist Administrator</option>";
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = '$modref'";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $taadmin)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close($con);
?>
		</select></td>
		<td class=tdbutton><input type=submit value="  Save  "></td>
	</tr>
	<!-- <tr>
		<th>User Access:</th>
		<td>Configure user access.</td>
		<td class=tdbutton><input type=button value=Configure onclick="goTo('L','setup_access.php');"></td>
	</tr> -->
	<tr>
		<th>Topics:</th>
		<td>Configure list of Topics.</td>
		<td class=tdbutton><input type=button value=Configure onclick="goTo('L','setup_topic.php');"></td>
	</tr>
	<tr>
		<th>Status:</th>
		<td>Configure Status list.</td>
		<td class=tdbutton><input type=button value=Configure onclick="goTo('L','setup_status.php');"></td>
	</tr>
	<tr>
		<th>User Defined Fields<br />(UDFs):</th>
		<td>Configure additional fields.</td>
	<td class=tdbutton><!--<select id=udf>
		<option selected value=new>New UDF</option>
		<option value=edit>Edit UDF</option>
		<option value=order>Sort UDFs</option>
		</select><br />--><input type=button value=Configure onclick="goTo('U','udf');">
	</td>
	</tr>
	<tr>
		<th>List Columns:</th>
		<td>Configure which columns display on List pages.</td>
	<td class=tdbutton><select id=columns>
		<option selected value=my>My <?php echo ucfirst($actname);?>s</option>
		<option value=own><?php echo ucfirst($actname);?>s Owned</option>
		<option value=all>All <?php echo ucfirst($actname);?>s</option>
		<option value=HOME>Home Page List</option>
		</select><br /><input type=button value=Configure onclick="goTo('S','columns');"></td>
	</tr>
	<tr>
		<th>Data Usage Report</th>
		<td>View the data storage in use in this module.</td><td class=tdbutton><input type=button value=View id=setup_datareport /></td>
	</tr>
</table>

</form>
<script type=text/javascript>
$(function() {
	$("#setup_datareport").click(function() {
		document.location.href = 'setup_datareport.php';
	});
});
</script>
</body>

</html>
