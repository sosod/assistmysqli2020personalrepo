<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	
);
$page = array("update");
$get_udf_link_headings = false;
require_once 'inc_header.php';

$actname.="s";


$me_user = new JOBS_USER();
$me_actions = new JOBS_ACTIONS();

$actions = array();
$sections = array();
$display = array();

//My actions
$sections['MY'] = "My $actname "; 
$actions['MY'] = $me_actions->getActionCounts("MY");
$display['MY'] = true;
if($me_user->viewSub()) { 
	$sections['SUB'] = "$actname Assigned to Sub-Ordinates"; 
	$actions['SUB'] = $me_actions->getActionCounts("SUB");
	$display['SUB'] = true;
} else {
	$display['SUB'] = false;
}
if($me_user->viewAll()) { 
	$sections['ALL'] = "All $actname "; 
	$actions['ALL'] = $me_actions->getActionCounts("ALL");
	$display['ALL'] = true;
} else {
	$display['ALL'] = false;
}

$tact = getUserAccess();

//GET LIST OF STATUSES
$status = mysql_fetch_all_fld("SELECT * FROM ".$dbref."_list_status WHERE id <> 'CN' AND yn <> 'N' ORDER BY sort ","pkey");

//$table_width = 400;
//GET MY ACTIONS
/*$sql = "SELECT s.pkey, count(t.taskid) as tcount
		FROM ".$dbref."_task t
		INNER JOIN ".$dbref."_list_status s
		  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
		INNER JOIN ".$dbref."_task_recipients tr
		  ON tr.taskid = t.taskid 
		  AND tr.tasktkid = '".$tkid."'
		GROUP BY s.pkey";
$actions['MY'] = mysql_fetch_fld2_one($sql,"pkey","tcount");
//GET MY ASSIGNED ACTIONS
$sql = "SELECT s.pkey, count(t.taskid) as tcount
		FROM ".$dbref."_task t
		INNER JOIN ".$dbref."_list_status s
		  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
		WHERE t.taskadduser = '".$tkid."'
		GROUP BY s.pkey";
$actions['OWN'] = mysql_fetch_fld2_one($sql,"pkey","tcount");
//IF TACT > 20 GET ALL ACTIONS
if($tact>20) {
	$table_width+=200;
	$sql = "SELECT s.pkey, count(t.taskid) as tcount
			FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_list_status s
			  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
			GROUP BY s.pkey";
	$actions['ALL'] = mysql_fetch_fld2_one($sql,"pkey","tcount");
}*/
//ASSIST_HELPER::arrPrint($actions);
function displayList($act,$count) {
	global $status;
	$echo = "<ul>";
		foreach($status as $key => $row) {
			$echo.= "<li>".(isset($count[$key]) ? "<a href=view_list.php?act=".$act."&s=".$key.">" : "").$row['value']." (".(isset($count[$key]) ? $count[$key] : "0").")</a></li>";
		}
	$echo.= "	</ul>";
	return $echo;
}




echo "
<table>
	<tr>
		".($display['MY']  ? "<th width=200>".$sections['MY']."</th>" : "")."
		".($display['SUB'] ? "<th width=200>".$sections['SUB']."</th>" : "")."
		".($display['ALL'] ? "<th width=200>".$sections['ALL']."</th>" : "")."
	</tr>
	<tr>
		".($display['MY']  ? "<td class=top>".displayList("MY",$actions['MY'])."</td>" : "")."
		".($display['SUB'] ? "<td class=top>".displayList("SUB",$actions['SUB'])."</td>" : "")."
		".($display['ALL'] ? "<td class=top>".displayList("ALL",$actions['ALL'])."</td>" : "")."
	</tr>
</table>";

?>
<script type=text/javascript>
	$(function() {
		$("tr").unbind("mouseenter mouseleave");
		$("table").css("margin-left","20px");
	});
</script>



</body>

</html>
