<?php include("inc_ignite.php");


 ?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var addtxt = me.edittxt.value;
    var astate = me.editstate.value;
    var err = "";
    var valid8 = "true";

    if(addtxt.length > 0)
    {
        var target2 = document.getElementById('s2');
        target2.className = "blank";
    }
    else
    {
        err = err + "- Please enter a status description.";
        var target2 = document.getElementById('s2');
        target2.className = "req";
        target2.focus();
        valid8 = "false";
    }


    if(astate.length == 0)
    {
        var target1 = document.getElementById('s1');
        target1.className = "blank";

        if(err.length==0)
        {
            me.editstate.value = 0;
        }
    }
    else
    {
        if(astate != escape(astate))
        {
            var target1 = document.getElementById('s1');
            target1.className = "req";
            if(err.length > 0)
            {
                err = err+"\n";
            }
            else
            {
                target1.focus();
            }
            err = err+"- Please enter only numbers as the 'Default %'.";
            valid8 = "false";
        }
        else
        {
            if(astate > 99 || astate < 0)
            {
                var target1 = document.getElementById('s1');
                target1.className = "req";
                if(err.length > 0)
                {
                    err = err+"\n";
                }
                else
                {
                    target1.focus();
                }
                err = err+"- 'Default %' must be a number between 0 and 99 (inclusive).";
                valid8 = "false";
            }
        }
    }

    if(err.length>0 || valid8 == "false")
    {
        alert("Please complete all the fields as indicated below:\n"+err);
        return false;
    }
    else
    {
        if(valid8 == "true")
        {
            return true;
        }
    }
return false;
}

</script>
<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo $modtext;?>: Setup - Update Status</b></h1>
<p>&nbsp;</p>
<?php
$id = $_GET['i'];
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' and custom = 'Y' AND pkey = ".$id;
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
mysql_close();
?>

<form name=edit method=post action=setup_status_edit_process.php onsubmit="return Validate(this);" language=jscript>
<table border=1 width=500 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid #ededed">
    <tr>
        <td class=tdheaderl width=140 height=27>Ref:</td>
        <td class=tdgeneral width=360><?php echo($id); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Old Status:</td>
        <td class=tdgeneral><?php echo($row['value']); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>New Status:</td>
        <td class=tdgeneral><input type=text name=edittxt maxlength=50 size=50 class=blank id=s2></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Old Default %:</td>
        <td class=tdgeneral><?php if($row['state']>0) { echo($row['state']."%"); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>New Default %:</td>
        <td class=tdgeneral><input type=text size=5 name=editstate class=blank id=s1>%</td>
    </tr>
    <tr>
        <td class=tdgeneral height=27 colspan=2><input type=hidden name=pkey value=<?php echo($id); ?>><input type=submit value=Update style="margin-left: 138px;">&nbsp;<input type=reset></td>
    </tr>
</form>
<script language=JavaScript>
    var targ = document.getElementById('s2');
    targ.focus();
</script>
</table>
</body>

</html>
