<?php
/***** DRAW NAVIGATION BUTTONS *****/
function drawNav($nav,$id) { 
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?><script type=text/javascript>
		  $(function()
		  {
			$("#<?php echo $id ?>").buttonset();
			$("#<?php echo $id ?> input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo "<div id=$id style=\"font-weight: bold;\">";
	foreach($nav as $key => $m) {
		if($key == "my") { $m['display'] = "&nbsp;&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;&nbsp;"; }
		echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="checked") { echo "checked=checked"; } echo " />";
		echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
	}
	echo "</div>";
}	//end drawNav($nav)
function drawNavSub($nav,$id,$pad) { 
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?>
	<script type=text/javascript>
		  $(function()
		  {
			$("#<?php echo $id ?>").buttonset();
			$("#<?php echo $id ?> input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo "<div id=$id style=\"padding-top: 5px; padding-left: ".$pad."px;\">";
	foreach($nav as $key => $m) {
		echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="checked") { echo "checked=checked"; } echo " />";
		echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
	}
	echo "</div>";
}	//end drawNav($nav)
function drawNavSubSelect($nav,$id,$pad) { 
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?>
	<script type=text/javascript>
		$(function()
		{
			$("#<?php echo $id ?>").buttonset();
			$("#<?php echo $id ?> input[type='radio']").click( function()
			{
				if($(this).val()!="na") {
					document.location.href = $(this).val();
				}
			});
		});
		function pageChange(me) {
			document.location.href = me.value;
		}
		function divCSS() {
			document.getElementById('S_div').className = 'ui-state-hover ui-widget-content ui-button';
		}
		function divCSS2(cls) {
			document.getElementById('S_div').className = cls;
		}
	</script>
	<?php
	echo "<div id=$id style=\"padding-top: 5px; padding-left: ".$pad."px;\">";
	if($nav['inc']['active']!="checked" && $nav['S1']['active']!="checked") { $class = "ui-state-active ui-widget-content ui-button"; } else { $class = "ui-state-default ui-widget-content ui-button"; }
	echo chr(10)."<div id=S_div class=\"$class\" style=\"padding: 0.4em 5px 4px 5px; margin-right: -1px;\" onmouseover=\"divCSS();\" onmouseout=\"divCSS2('$class');\"><input type=\"radio\" id=\"S\" name=\"radio".$id."\" value=\"na\" />";
	echo chr(10)."<label for=\"$key\"><select onchange=\"pageChange(this);\"><option value=X>--- SELECT ---</option>";
	foreach($nav as $key => $m) {
		if($key!="S1" && $key !="inc") {
			echo chr(10)."<option ";
			if($m['active']=="checked") { echo "selected "; }
			echo "value=\"".$m['url']."\">".$m['display']."</option>";
		}
	}
	echo chr(10)."</select>&nbsp;&nbsp;</label></div>";
	$key = "inc"; $m = $nav[$key];
	echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="checked") { echo "checked=checked"; } echo " />";
	echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
	$key = "S1"; $m = $nav[$key];
	echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="checked") { echo "checked=checked"; } echo " />";
	echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
	echo "</div>";
}	//end drawNav($nav)


function drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form) {
/*** variables:
		$url = $_SERVER['REQUEST_URI']
		$search   $search_label
		$page / $pages
		$start
		$limit
***/
	echo "<form name=page action=$url method=get>".$form;
		echo "<table cellpadding=5 cellspacing=0 width=100% style=\"margin-bottom: 10px;\">";
			echo "<tr>";
				echo "<td width=45% style=\"border-right: 0px;\">";
					echo "<input type=button id=b1 value=\"|<\" onclick=\"goNext('$url',0,'$search');\">";
						echo "&nbsp;";
					echo "<input type=button id=b2 value=\"<\" onclick=\"goNext('$url',".($start-$limit).",'$search');\">";
				if($page<2) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b1').disabled = true;";
					echo "document.getElementById('b2').disabled = true;";
					echo "</script>";
				}
						echo " Page $page / $pages ";
					echo "<input type=button value=\">\" id=b3 onclick=\"goNext('$url',".($start+$limit).",'$search');\">";
						echo "&nbsp;";
					echo "<input type=button value=\">|\" id=b4 onclick=\"goNext('$url','e','$search');\">";
				if($page==$pages) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b3').disabled = true;";
					echo "document.getElementById('b4').disabled = true;";
					echo "</script>";
				}
				echo "</td>";
				echo "<td width=10% style=\"border-right: 0px; border-left: 0px;\" align=center>";
					if(strlen($search)>0) { 
						echo "<input type=button value=\"Clear Search\" onclick=\"document.location.href = '$url';\" style=\"float: center\">";
					} 
				echo "</td>";
				echo "<td width=45% align=right style=\"border-left: 0px;\">$search_label:&nbsp;<input type=hidden name=s value=0><input type=text width=10 name=f>&nbsp;<input type=submit value=\" Go \"></td>";
			echo "</tr>";
		echo "</table>";
	echo "</form>";
}	//end drawPage()


/******* DETERMINE FONT COLOUR BY STATUS COLOUR *****/
function textColour($color) {
	$text = "000000";
	$w = 0;
	$b = 0;
//	echo "<p>";
	for($i=0;$i<strlen($color);$i+=1) {
		$sub = substr($color,$i,1);
//		echo "-------".$sub;
		if(checkIntRef($sub) || $sub == "0") {
			$b++;
			if($i/2==ceil($i/2)) $b++;
//			echo "-b";
		} else {
			$w++;
			if($i/2==ceil($i/2)) $w++;
//			echo "-w";
		}
	}
	if($b>=$w) { $text = "ffffff"; } else { $text="000000"; }
//	echo "------ $b -- $w ------   =>   $text ";
	return $text;
}



/****** DRAW PIE GRAPH ******/
function drawPie($required,$graph_ref,$chart_title,$slices,$values,$graph_layout,$legend_layout) {
	$total = array_sum($values);
	global $color_array;  $c = 0;
	/*** REQUIRED ***/
	if($required=="Y") {
		require("../lib/amcharts2-php/AmPieChart.php");
		AmChart::$swfObjectPath = "../lib/amcharts2/swfobject.js";
		AmChart::$libraryPath = "../lib/amcharts2/ampie/";
		AmChart::$jsPath = "../lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "../lib/amcharts2/ampie/jquery.js";
		AmChart::$loadJQuery = true;
	}
	/*** LAYOUT ***/
	$cradius = 120;
	$dlabel = "-20%";
	$tdl = 3;
	$layout = "ONS";
	$cwidth = 30;
	$cwidth+= $cradius * 2 + 20;
	$cheight = $cwidth;
	
	/*** CHART ***/
	$chart = new AmPieChart("pie_".$graph_ref);
	$chart->setTitle("<p class=charttitle >".$chart_title."</p>"); 
	foreach($slices as $s) {
		if(isset($values[$s['id']])) {
			if($s['color']=="ffffff" || strlen($s['color'])==0) {
				/*$color = $color_array[$c]; $c++;
				$slices[$s['id']]['color'] = $color;*/
				$color = "";
			} else {
				$color = $s['color'];
			}
			if(strlen($color)>0) {
				$chart->addSlice($s['id'], code($s['value'])." (".$values[$s['id']]." of $total)", $values[$s['id']], array("color" => $color));
			} else {
				$chart->addSlice($s['id'], code($s['value'])." (".$values[$s['id']]." of $total)", $values[$s['id']]);
			}
		}
	}
	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" =>$cheight,
		"font" => "Tahoma",
		"decimals_separator" => ".",
		"pie.y" => "50%",
		"pie.radius" => $cradius,
		"pie.height" => 5,
		"pie.inner_radius" => 0,
		"animation.pull_out_on_click" => true,
		"background.border_alpha" => 0,
		"data_labels.radius" => $dlabel,
		"data_labels.text_color" => "0xFFFFFF",
		"data_labels.show" => "<![CDATA[{percents}%]]>",
		"legend.enabled" => "false",
		"balloon.enabled" => "true"
	));
	echo html_entity_decode($chart->getCode());

	/*** LEGEND ***/
	$krsetup2 = $slices;
		$kc = count($values);
	echo "<div align=center>";
	echo "<table cellpadding=2 cellspacing=0 style=\"border: 1px solid #ababab; margin: 0 0 0 0;\">";
		echo "<tr><td class=legend><table cellpadding=3 cellspacing=6 style=\"border-width: px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				$k++;
				if(isset($values[$krs['id']])) {
					echo "<tr>";
					echo "<td class=legend width=10 style=\"background-color: #".$krs['color']."\">&nbsp;</td>";
					echo "<td class=legend>".$krs['value']." (".$values[$krs['id']]." of ".$total.")</td>";
					echo "</tr>";
				}
			}
		echo "</table></td></tr>";
	echo "</table></div>";
	
}

function rgb2html($r, $g=-1, $b=-1)
{
	if (is_array($r) && sizeof($r) == 3)
		list($r, $g, $b) = $r;
	$r = intval($r); $g = intval($g);
	$b = intval($b);
	$r = dechex($r<0?0:($r>255?255:$r));
	$g = dechex($g<0?0:($g>255?255:$g));
	$b = dechex($b<0?0:($b>255?255:$b));
	$color = (strlen($r) < 2?'0':'').$r;
	$color .= (strlen($g) < 2?'0':'').$g;
	$color .= (strlen($b) < 2?'0':'').$b;
	return $color;
}


//GET USER'S ACCESS PERMISSIONS
function getUserAccess() {
	global $dbref;
	global $cmpcode;
	$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$_SESSION['tid']."' AND yn = 'Y'";
	$row = mysql_fetch_one($sql);
	$tact = $row['act'];
	unset($rs);
	return $tact;
}




//make sure that the display list is up to date with UDFs
function updateUDFDisplayList() {
	global $dbref;
	global $cmpcode;
	
	$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$_SESSION['modref']."' AND udfiid NOT IN (SELECT field FROM ".$dbref."_list_display WHERE type = 'U')";
	$newudfs = mysql_fetch_all($sql);
	if(count($newudfs)>0) {
		$sql = array();
		$start = "INSERT INTO ".$dbref."_list_display 
				(headingfull,headingshort,yn,type,mydisp,myyn,mysort,owndisp,ownyn,ownsort,alldisp,allyn,allsort,field)";
		foreach($newudfs as $u) {
			$v = "('".$u['udfivalue']."','".$u['udfivalue']."','".$u['udfiyn']."','U','N','Y','9999','N','Y','9999','N','Y','9999','".$u['udfiid']."')";
			$sql[] = $v;
		}
		if(count($sql)>0) {
			$sql2 = $start." VALUES ".implode(",",$sql);
			db_insert($sql2);
		}
	}

}



















/** TASK/ACTION DETAIL FUNCTIONS **/
function getTask($taskid) {
	global $dbref;
	$cmpcode = strtolower($_SESSION['cc']);
	$sql = "SELECT t.*
			, top.value as topic
			, urg.value as urgency
			, stat.value as status
			, CONCAT_WS(' ',tk.tkname,tk.tksurname) as adduser
			FROM ".$dbref."_task t
			LEFT OUTER JOIN ".$dbref."_list_topic top
			  ON top.id = t.tasktopicid
			LEFT OUTER JOIN ".$dbref."_list_urgency urg
			  ON urg.id = t.taskurgencyid
			LEFT OUTER JOIN ".$dbref."_list_status stat
			  ON stat.pkey = t.taskstatusid
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			  ON tk.tkid = t.taskadduser
			WHERE taskid = ".$taskid;
	$task = mysql_fetch_one($sql);
	return $task;
}

function getUDFListItems($type="") {
	$cmpcode = strtolower($_SESSION['cc']);
		$sql = "SELECT v.* 
				FROM assist_".$cmpcode."_udfvalue v
				INNER JOIN assist_".$cmpcode."_udfindex i
				  ON i.udfiid = v.udfvindex
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = '".$_SESSION['modref']."'
				  ".(strlen($type)>0 ? "AND i.udfiobject = '$type'" : "")."
				WHERE udfvyn = 'Y' 
				ORDER BY udfvindex, udfvvalue";
		$items = mysql_fetch_all_fld2($sql,"udfvindex","udfvid");

	return $items;
}
?>