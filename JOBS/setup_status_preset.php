<?php
require_once("inc_header.php");

function resetForm(&$varP) {
	$varP['title'] = "";
	$varP['link'] = 1;
	$varP['owner'] = "S";
	$varP['recipients'] = array();
	$varP['topic'] = 0;
	$varP['urgency'] = 0;
	$varP['deadline'] = "1";
	$varP['action'] = "";
	$varP['deliver'] = "";
	$varP['udf'] = "S";
	$varP['attachments'] = "N";
}

$srcid = $_REQUEST['i'];
if(!checkIntRef($srcid)) 
	die("<h1>ERROR</h1><p>An error has occurred.  Please go back and try again.</p>");

$var = array();
$var = $_REQUEST;
$act = $var['act'];

if($act=="SAVE") {
	$title = code($var['title']);
	$src = "S";
	$active = true;
	$link = $var['link'];
	$owner = $var['owner'];
	$recipients = implode("_",$var['recipients']);
	$topic = $var['topic'];
	$urgency = $var['urgency'];
	$deadline = (isset($var['deadline']) && strlen($var['deadline'])>0 && is_numeric($var['deadline'])) ? $var['deadline'] : 1;
	$action = code($var['action']);
	$deliver = code($var['deliver']);
	$udf = $var['udf'];
	$attachments = $var['attachments'];
	$sql = "INSERT INTO ".$dbref."_list_preset 
		(value, src, srcid, active, link, owner, recipients, urgency, topic, action, 
		deliver, deadline, attachments, udf, created_on, created_by, last_modified_by )
		VALUES
		('$title','$src',$srcid,$active,$link,'$owner','$recipients',$urgency,$topic,'$action',
		'$deliver',$deadline,'$attachments','$udf',now(),'$tkid','')";
	include("inc_db_con.php");
	if(mysql_affected_rows() > 0) {
		$result[0] = "check";
		$result[1] = "Preset $actname '".$title."' successfully created.";
	}
	resetForm($var);
} elseif($act=="VIEW") {
	$sql = "SELECT * FROM ".$dbref."_list_preset WHERE id = ".$var['p'];
	include("inc_db_con.php");
	if(mysql_num_rows($rs)==0) {
		resetForm($var);
	} else {
		$row = mysql_fetch_array($rs);
		$var['recipients'] = explode("_",$row['recipients']);
		$var['action'] = decode($row['action']);
		$var['deliver'] = decode($row['deliver']);
		$var['title'] = decode($row['value']);
		foreach($row as $key => $v) {
			if(!isset($var[$key])) {
				$var[$key] = $v;
			}
		}
		$sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['created_by']."'";
		include("inc_db_con2.php");
			$row2 = mysql_fetch_array($rs2);
			$var['created'] = $row['created_on']." by ".$row2['tkname']." ".$row2['tksurname'];
		mysql_close($con2);
		if(strlen($row['last_modified_by']) > 0) {
			$sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['last_modified_by']."'";
			include("inc_db_con2.php");
				$row2 = mysql_fetch_array($rs2);
				$var['last_modified'] = $row['last_modified_on']." by ".$row2['tkname']." ".$row2['tksurname'];
			mysql_close($con2);
		}
	}
	mysql_close($con);
} elseif($act=="EDIT") {
	$id = $var['p'];
	$title = code($var['title']);
	$link = $var['link'];
	$owner = $var['owner'];
	$recipients = implode("_",$var['recipients']);
	$topic = $var['topic'];
	$urgency = $var['urgency'];
	$deadline = (isset($var['deadline']) && strlen($var['deadline'])>0 && is_numeric($var['deadline'])) ? $var['deadline'] : 1;
//$deadline = $var['deadline'];
	$action = code($var['action']);
	$deliver = code($var['deliver']);
	$udf = $var['udf'];
	$attachments = $var['attachments'];
	$sql = "UPDATE ".$dbref."_list_preset SET 
				value = '$title', 
				link = $link, 
				owner = '$owner', 
				recipients = '$recipients', 
				urgency = $urgency, 
				topic = $topic, 
				action = '$action', 
				deliver = '$deliver', 
				deadline = $deadline, 
				attachments = '$attachments', 
				udf = '$udf', 
				last_modified_by = '$tkid',
				last_modified_on = now()
			WHERE 
				id = $id
			";
	include("inc_db_con.php");
	if(mysql_affected_rows() > 0) {
		$result[0] = "check";
		$result[1] = "Preset $actname '".$title."' successfully edited.";
	}
	resetForm($var);
} elseif($act=="DEL") {
	$id = $var['p'];
	$sql = "UPDATE ".$dbref."_list_preset SET active = false WHERE id = $id ";
	include("inc_db_con.php");
	resetForm($var);
} else {
	resetForm($var);
}
?>
        <script type ="text/javascript">
            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true		
                });
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                })
            });
			
			function editPreset() {
				var p = document.getElementById('p').value;
				var i = document.getElementById('i').value;
				document.location.href = "setup_status_preset.php?i="+i+"&p="+p+"&act=VIEW";
			}
    </script>
<?php 
//TITLE
echo "<h1>$modtext >> Setup >> Status >> Preset $actname </h1>"; 
displayResColor($result);
$sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey = $srcid ";
include("inc_db_con.php");
if(mysql_num_rows($rs==0)) { die("<h2>ERROR</h2><p>An error has occurred.  Please go back and try again.</p>"); }
	$row = mysql_fetch_array($rs);
mysql_close($con);
echo "<h2>Status: ".$row['value']."</h2>";
?>
<p>The Preset function allows you to instruct the system to automatically create a certain <?php echo $actname ?>(s) when a specific status is selected by a user when updating a(n) <?php echo $actname; ?>.*</p>
<?php
//LIST OF EXISTING PRESETS
$sql = "SELECT * FROM ".$dbref."_list_preset WHERE src = 'S' AND srcid = $srcid AND active = true";
include("inc_db_con.php");
if(mysql_num_rows($rs)>0) {
	echo "<p><b>Existing Presets:</b> <select name=preset id=p><option selected value=X>--- SELECT A PRESET TO EDIT ---</option>";
	while($row = mysql_fetch_array($rs)) {
		echo "<option value=".$row['id'].">".$row['value']."</option>";
	}
	echo "</select> <input type=button value=Edit onclick=\"editPreset()\" /></p>";
}
mysql_close($con);

//NEW FORM
?>
        <form name=newpreset method=post action=setup_status_preset.php onsubmit="return ValidatePresetStatus(this);">
<?php
		if(isset($var['p']) && checkIntRef($var['p']) && $act=="VIEW") {
			echo "<input type=hidden name=act value=EDIT><input type=hidden name=i id=i value=\"$srcid\"><input type=hidden name=p id=p value=\"".$var['p']."\">";
		} else {
			echo "<input type=hidden name=act value=SAVE><input type=hidden name=i id=i value=\"$srcid\">";
		}
?>
            <table cellspacing="0" cellpadding="4">
                <tr>
                    <th class="left top" width=140>Preset Title:</th>
                    <td class="top"><input type=text size=40 name=title value="<?php echo $var['title']; ?>" /></td>
				</tr>
<?php if($act == "VIEW" && isset($var['created'])) { ?>
                <tr>
                    <th class="left top" width=140>Created:</th>
                    <td class="top"><?php echo $var['created']; ?></td>
				</tr>
				<?php if(isset($var['last_modified'])) { ?>
                <tr>
                    <th class="left top" width=140>Last Modified:</th>
                    <td class="top"><?php echo $var['last_modified']; ?></td>
				</tr>
				<?php } ?>
<?php } ?>
                <tr>
                    <th class="left top" width=140>Owner:</th>
                    <td class="top"><select name=owner><?php
						echo "<option ".(($var['owner']=="S" || !isset($var['owner']) || strlen($var['owner'])==0) ? "selected " : "")."value=S>Same as source $actname </option>";
						echo "<option ".(($var['owner']=="U") ? "selected " : "")."value=U>User who updated the source $actname </option>";
						$sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name 
							FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
							WHERE t.tkid = a.tkid AND a.act > 20 AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($modref)."'
							ORDER BY t.tkname, t.tksurname";
						include("inc_db_con.php");
							while($row = mysql_fetch_array($rs)) {
								echo "<option ".(($var['owner']==$row['tkid']) ? "selected " : "")."value=".$row['tkid'].">".$row['name']."</option>";
							} 
						mysql_close($con);
					?></select></td>
                </tr>
                <tr>
                    <th class="left top">Assign To:</th>
                    <td class="top"><?php
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
								FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($modref)."'
                                ORDER BY t.tkname, t.tksurname";
                            include("inc_db_con.php");
                            $size = mysql_num_rows($rs);
							echo "<select name=\"recipients[]\" multiple=\"multiple\" size=".(($size > 10 || $size==0) ? 10 : $size).">";
                                while($row = mysql_fetch_array($rs)) {
                                    echo "<option ".(in_array($row['tkid'],$var['recipients']) ? "selected " : "")."name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
                                }
                                mysql_close($con);
						?></select>
                        <br /><i>Ctrl + left click to select multiple users</i>
                    </td>
                </tr>
                <tr>
                    <th class="left top">Topic:</th>
                    <td class="top">
                        <select size="1" name="topic">
						<?php
							echo "<option ".(($var['topic']==0 || !isset($var['topic']) || strlen($var['topic'])==0) ? "selected " : "")."value=0>Same as source <?php echo $actname; ?></option>";
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
								echo "<option ".(($var['topic']==$row['id']) ? "selected " : "")."value=".$row['id'].">".$row['value']."</option>";
                            }
                            mysql_close($con);
						?></select>
                    </td>
                </tr>
                <tr>
                    <th class="left top">Priority:</th>
                    <td class="top">
                        <select size="1" name="urgency">
						<?php
							echo "<option ".(($var['urgency']==0 || !isset($var['urgency']) || strlen($var['urgency'])==0) ? "selected " : "")."value=0>Same as source <?php echo $actname; ?></option>";
                            $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE yn = 'Y'";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
								echo "<option ".(($var['urgency']==$row['id']) ? "selected " : "")."value=".$row['id'].">".$row['value']."</option>";
                            }
                            mysql_close($con);
						?></select>
                    </td>
                </tr>
                <tr>
                    <th  class="left top">Deadline:</th>
                    <td class="top"><?php
						echo "<input type=text size=5 class=right name=deadline value=\"".$var['deadline']."\"> calendar day(s) after the creation date";
					?></td>
                </tr>
                <tr>
                    <th class="left top"><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><textarea rows="5" name="action" cols="35"><?php echo $var['action']; ?></textarea></td>
                </tr>
                <tr>
                    <th class="left top"><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><textarea rows="5" name="deliver" cols="35"><?php echo $var['deliver']; ?></textarea></td>
                </tr>
                <tr>
                    <th class="left top">UDFs:</th>
                    <td><select name=udf><?php
						echo "<option ".(($var['udf']==0 || !isset($var['udf']) || strlen($var['udf'])==0) ? "selected " : "")." value=S>Same as source </option>";
						echo "<option value=N>Blank</option>";
					?></select></td>
                </tr>
                <tr>
                    <th class="left top">Attachments:</th>
                    <td><select name=attachments><?php
						echo "<option ".(($var['attachments']=="N" || !isset($var['attachments']) || strlen($var['attachments'])==0) ? "selected " : "")." value=N>None</option>";
						echo "<option ".(($var['attachments']=="Y") ? "selected " : "")." value=Y>Attach any documents included in source update</option>";
					?></select></td>
                </tr>
                <tr>
                    <th class="left top">Provide a link to the source <?php echo $actname; ?>:</th>
                    <td><select name=link><?php
						echo "<option ".(($var['link']==1 || !isset($var['link']) || strlen($var['link'])==0) ? "selected " : "")." value=1>Yes</option>";
						echo "<option ".(($var['link']==0) ? "selected " : "")." value=0>No</option>";
					?></select></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top"><?php
						if($act == "VIEW") {
							echo "<input type=\"submit\" value=\"Save Changes\" name=\"B1\" style=\"margin-left: 147px;\">";
							echo " <input type=\"button\" value=\"Delete\" name=\"B3\" style=\"float: right\" onclick=\"if(confirm('Are you sure you wish to delete this Preset?')) { document.location.href = 'setup_status_preset.php?i=".$srcid."&p=".$var['p']."&act=DEL'; }\">";
						} else {
							echo "<input type=\"submit\" value=\"Save Preset $actname\" name=\"B1\" style=\"margin-left: 147px;\">";
						}
                        echo "<input type=\"reset\" value=\"Reset\" name=\"B2\">";
					?></td>
                </tr>
            </table>
        </form>
<p>*Please note the following:<ol>
<li>When a Preset <?php echo $actname; ?> is created an email will be sent notifing the recipient(s) automatically, even if the owner, updater and recipient are the same person.</li>
<li>An Update Record is automatically added to the source <?php echo $actname; ?> to indicate that a preset <?php echo $actname; ?> was generated.</li>
<li>A Preset <?php echo $actname; ?> is created whenever a(n) <?php echo $actname; ?> is updated and the status is changed to the specific status.<br />If the <?php echo $actname; ?> is updated after a Preset has been generated and the status is not changed, a second Preset <u>will not</u> be generated.<br />However, if a(n) <?php echo $actname; ?> is updated and the status is changed to a different status and then is updated again and the status is changed back to the specific status, then a second Preset <u>will</u> be created.</li>
</ol></p>
<p>&nbsp;</p>
	</body>
</html>

