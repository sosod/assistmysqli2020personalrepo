<?php require_once('inc_header.php'); ?>
<?php
/*
 * These are the company details
 * Including:
 * Company information
 * Admin users, if any
 * Preferred way of handling the helpdesk request (settings)
*/

$sql = 'SELECT * FROM helpdesk_companies ';

// Company Type
$sql .= 'INNER JOIN helpdesk_company_types ';
$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

// Company helpdesk settings
$sql .= 'INNER JOIN helpdesk_company_settings ';
$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

$sql .= 'INNER JOIN helpdesk_admin_management ';
$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

$sql .= 'INNER JOIN helpdesk_request_settings ';
$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

// Clauses
$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
$company_info = $helper->mysql_fetch_one($sql);
?>

<?php if(isset($company_info) && is_array($company_info) && count($company_info) > 0){ ?>
    <h1>Company Name: <?php echo $company_info['hdc_name']; ?></h1>

    <?php
        ASSIST_HELPER::displayResult(
            isset($_REQUEST['r']) ? $_REQUEST['r'] : array()
        );
    ?>

    <p>Admin Management: <?php echo $company_info['hdam_value']; ?></p>

    <p>Helpdesk Setting: <?php echo $company_info['hdrs_value']; ?></p>

    <?php if($company_info['hdam_id'] != 1 && $company_info['hdrs_id'] != 1){//If there is an admin and the the settings aren't ?>
        <?php
        //Get the admins of this company to display them in the table
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        //Admin users
        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_user_admin_types ';
        $sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

        $sql .= 'INNER JOIN helpdesk_admin_types ';
        $sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$company_info['hdc_cc'] . '\'';
        $company_admins = $helper->mysql_fetch_all($sql);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINTHERE</p>';
//        print_r($company_admins);
//        echo '</pre>';
        ?>
        <?php if(count($company_admins) > 0){ ?>
        <table class="list " id="paging_obj_list_view" width="100%">
            <tbody>
            <tr id="head_row">
                <th>Name</th>
                <th></th>
            </tr>

            <?php foreach($company_admins as $key => $val){ ?>
                <tr>
                    <td class=""><?php echo $val['user_name']; ?></td>
                    <td class="center"><input type="button" value="Delete" ref="3" data-userid="<?php echo $val['user_id']; ?>" class="btn_admin_del"></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    <?php } ?>

    <br/>
    <br/>


    <?php
    $test_array = array('0002', '0003', '0004', '0005', '0007');

    $sql = 'SELECT tkid, tkname, tksurname FROM assist_'.$company_info['hdc_cc'].'_timekeep ';
    $sql .= 'WHERE tkstatus = \'1\' ';

    if(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0){


        $blank_array = array();

        for($i = 0; $i <  count($company_admins); $i++){
            $blank_array[] = $company_admins[$i]['user_tkid'];

        }
        $sql .= 'AND tkid NOT IN (\'' . implode('\',\'', $blank_array) . '\') ';
    }


    $users = $client_db->mysql_fetch_all($sql);

//    echo '<pre style="font-size: 18px">';
//    echo '<p>PRINTHERE</p>';
//    print_r($users);
//    echo '</pre>';

    $user_list = array();

    foreach($users as $key => $val){
        $user_list[$val['tkid']] = $val['tkname'] . ' ' . $val['tksurname'];
    }
    ?>
    <form name="add_admin" method="post" enctype="multipart/form-data">
        <table id="tbl_action" class="form">
            <tbody>
            <!-- Decide On An Action To Take - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Users:</th>
                <td>
                    <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'helpdesk_user', 'name' => 'helpdesk_user', 'required' => true, 'unspecified' => false,  'options' => $user_list));?>
                </td>
            </tr>
            <!-- Decide On An Action To Take - END -->
            <tr>
                <th></th>
                <td>
                    <input id="btn_submit" type="button" value="Add Admin" data-hdrid="user_id" class="isubmit i-am-the-submit-button">
                </td>
            </tr>
            </tbody>
        </table>
    </form>

    <?php
//    echo '<pre style="font-size: 18px">';
//    echo '<p>COMPANY INFORMATION</p>';
//    print_r($company_info);
//    echo '</pre>';
//
//    $contact_details = $helper->getAnEmail($_SESSION['tid'], $_SESSION['cc']);
//
//    echo '<pre style="font-size: 18px">';
//    echo '<p>CONTACT INFO</p>';
//    print_r($contact_details);
//    echo '</pre>';
    ?>
<?php }else{ ?>
    <?php
    /*
     * This company doesn't exist in the helpdesk db,
     * so here's a button to initialise it,
     * so that we have initial admin management and request settings
     * */
//    echo '<pre style="font-size: 18px">';
//    echo '<p>PRINTHERE</p>';
//    print_r($_SESSION);
//    echo '</pre>';

    ?>
<form name="create_company" method="post" enctype="multipart/form-data">
    <input id="btn_submit" type="button" value="Register Company" class="btn_list">
</form>
<?php } ?>


<?php
////Test email softwarer from locahost
//$emailer = new ASSIST_EMAIL();
//$to = 'mosnubian@gmail.com';
//$emailer->setRecipient($to);
//$sub = 'Localhost Email Test';
//$emailer->setSubject($sub);
//$message = 'This is the message you should be getting';
//$emailer->setBody($message);
//$emailer->sendEmail();
?>

<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>

        //Create the current company because it does not exist in the helpdesk db
        $('form[name="create_company"] #btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="create_company"]'), 'COMPANY.ADD', 'manage.php?')
        });

        $('form[name="add_admin"] #btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="add_admin"]'), 'USERS.ADD', 'manage.php?')
        });

        $('.btn_admin_del').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var value = [];
            value["user_id"] = $(this).data('userid');
            var dta = {user_id : value["user_id"]};
            console.log(value);
            console.log(dta);

            var page_action = 'USERS.DEMOTE';
            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'manage.php');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });
    });
</script>

<?php require_once('inc_footer.php'); ?>

