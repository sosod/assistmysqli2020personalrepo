<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME 
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>'.$s.' => '.$total_time.' seconds.';
	
}

require ("../module/autoloader.php");
//marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/",$self);
$self = explode(".",end($self));
$page = $self[0];
if(isset($my_page)) {
	$page.="_".$my_page;
}
$navigation_path = explode("_",$page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
	//'new'				=> "new_create",
$redirect = array(
	'report'			=> "report_generate",
	'setup'				=> "setup_defaults",
);
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}
/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new HELPDESK_MENU();
$menu = $menuObject->getPageMenu($page);
/***********
 * Start general header
 */

$headingObject = new HELPDESK_HEADINGS();
$displayObject = new HELPDESK_DISPLAY();
$nameObject = new HELPDESK_NAMES();
$helper = new HELPDESK();

$client_db = new ASSIST_MODULE_HELPER();

$s = array(
	"/library/js/assist.ui.paging.js",
	"/".$helper->getModLocation()."/common/helpdeskhelper.js",
	"/".$helper->getModLocation()."/common/HELPDESK.js",
	"/".$helper->getModLocation()."/common/hoverIntent.js"
);
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

//ASSIST_HELPER::echoPageHeader("1.11.4",$scripts);
//$helper->echoPageHeader2016();
//$helper->arrPrint($_REQUEST);

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);


$js = ""; //to catch any js put out by the various display classes

//marktime("general");

/*********
 * Module-wide Javascript
 */
?>
<script type="text/javascript" >
$(document).ready(function() {
    

//DEVELOPER NOTES
$("#div_developers_notes").css({"border":"1px solid #fe9900","margin":"10px","padding":"10px","width":"400px"}).find("h3, h4, p, li").addClass("orange");
$("#div_page_description").css({"border":"1px solid #009900","margin":"10px","padding":"10px","width":"400px"}).find("h3, h4, p, li").addClass("green");
//$("#div_developers_notes").hide();


	//$("table.tbl-container, table.tbl-container td:first").addClass("noborder");
	//$("table.tbl-container tr").find("td:first").addClass("noborder");
	//$("table.tbl-subcontainer").parent("td").addClass("noborder");
	//$("table.sub_table").addClass("noborder").find("td").addClass("noborder");
	$("table.tbl-container").addClass("noborder");
	$("table.tbl-container:not(.not-max)").css("width","100%");
	$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
	$("table.th2").find("th").addClass("th2");
	$("table tr.th2").find("th").addClass("th2");
	$("table.tbl_audit_log").css("width","100%").find("td").removeClass("noborder");
	//$("h2").css("margin-top","0px");
	$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");




	$("button.format_btn")
		.css({"background":"url()","margin":"0px","padding":"0px","border-width":"1px",})
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","margin":"0px"});

	$("button.btn_green").removeClass("ui-state-default").addClass("ui-button-state-green");
	$("button.btn_green").hover(function() {
		$(this).removeClass("ui-button-state-green").addClass("ui-button-state-orange");
	},function() {
		$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
	});

	$("button.btn_red").removeClass("ui-state-default").addClass("ui-button-state-red");
	$("button.btn_red").hover(function() {
		$(this).removeClass("ui-button-state-red").addClass("ui-button-state-orange");
	},function() {
		$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-red");
	});


	$("table.zeroborder, table.zeroborder td, table.zeroborder th").addClass("zeroborder").css("border","0px solid #ffffff");

});

</script>
<style type="text/css">
table.middle td { vertical-align: middle; }
/* table td { font-size: 100%; }
table th { font-size: 120%; line-height: 150%; }
*/
button.format_btn { font-size: 8.5pt; line-height: 9pt; }

.blue-border { border: 1px solid #374ea2; }

table.zeroborder {
	border: 0px solid #fffffff;
}

	.noborder { border: 1px dashed #ffffff; }
	table.tbl_audit_log { width: 100%; }
	table.attach { border: 0px dashed #009900; }
	table.attach td { 
		border: 0px dashed #009900;
		vertical-align: middle;
		padding: 1px; 
	}
	.button_class, .ui-widget.button_class {
		font-size: 75%;
		padding: 1px;
	}
	tr.total th {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	.abutton {
		font-size: 90%;
		padding: 0px;
		font-weight: bold;
	}
	.white-border {
		border-color: #FFFFFF;
	}
	
	button:disabled {
		cursor: not-allowed; 
	}
	
	.disabled-button {
		color: #777777;
		border: 1px solid #777777;
		background-color: #efefef;
	}
 /*
.ui-button-state-ok, .ui-widget-content .ui-button-state-ok, .ui-widget-header .ui-button-state-ok  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #009933;
	cursor: pointer; 
}
.ui-button-state-ok a, .ui-widget-content .ui-button-state-ok a,.ui-widget-header .ui-button-state-ok a { 
	color: #363636; 
	padding: 0px;
	cursor: pointer; 
}
*/
.ui-button-state-green, .ui-widget-content .ui-button-state-green, .ui-widget-header .ui-button-state-green  {border: 1px solid #009933; background: url(); background-color: #FFFFFF; color: #009933; }
.ui-button-state-green a, .ui-widget-content .ui-button-state-green a,.ui-widget-header .ui-button-state-green a { color: #009933; }
.ui-button-state-green .ui-icon {
	background-image: url(/library/images/ui-icons_009933_256x240.png);
}

.ui-button-state-red, .ui-widget-content .ui-button-state-red, .ui-widget-header .ui-button-state-red  {border: 1px solid #DD1921; background: url(); background-color: #FFFFFF; color: #DD1921; }
.ui-button-state-red a, .ui-widget-content .ui-button-state-red a,.ui-widget-header .ui-button-state-red a { color: #DD1921; }
.ui-button-state-red .ui-icon {
	background-image: url(/library/images/ui-icons_dd1921_256x240.png);
}

.ui-button-state-disabled, .ui-widget-content .ui-button-state-disabled, .ui-widget-header .ui-button-state-disabled {
	border: 1px solid #d3d3d3; 
	background: #ffffff url(/library/images/ui-bg_highlight-soft_50_ededed_1x100.png) 50% 50% repeat-x;
	color: #ababab; 
}
.ui-button-state-disabled a, .ui-widget-content .ui-button-state-disabled a,.ui-widget-header .ui-button-state-disabled a {  }
.ui-button-state-disabled .ui-icon {
	background-image: url(/library/images/ui-icons_d3d3d3_256x240.png);
}



.ui-button-state-orange, .ui-widget-content .ui-button-state-orange, .ui-widget-header .ui-button-state-orange  {border: 1px solid #faa74a; background: url(); background-color: #FFFFFF; color: #faa74a; }
.ui-button-state-orange a, .ui-widget-content .ui-button-state-orange a,.ui-widget-header .ui-button-state-orange a { color: #faa74a; }
.ui-button-state-orange .ui-icon {
	background-image: url(/library/images/ui-icons_faa74a_256x240.png);
}


.ui-button-state-blue, .ui-widget-content .ui-button-state-blue, .ui-widget-header .ui-button-state-blue  {border: 1px solid #374ea2; background: url(); background-color: #FFFFFF; color: #374ea2; }
.ui-button-state-blue a, .ui-widget-content .ui-button-state-blue a,.ui-widget-header .ui-button-state-blue a { color: #374ea2; }
.ui-button-state-blue .ui-icon {
	background-image: url(/library/images/ui-icons_374ea2_256x240.png);
}


.ui-button-state-grey, .ui-widget-content .ui-button-state-grey, .ui-widget-header .ui-button-state-grey  {border: 1px solid #ababab; background: url(); background-color: #FFFFFF; color: #ababab; }
.ui-button-state-grey a, .ui-widget-content .ui-button-state-grey a,.ui-widget-header .ui-button-state-grey a { color: #ababab; }
.ui-button-state-grey .ui-icon {
	background-image: url(/library/images/ui-icons_ababab_256x240.png);
}



.ui-button-state-darkgrey, .ui-widget-content .ui-button-state-darkgrey, .ui-widget-header .ui-button-state-darkgrey  {border: 1px solid #555555; background: url(); background-color: #FFFFFF; color: #555555; }
.ui-button-state-darkgrey a, .ui-widget-content .ui-button-state-darkgrey a,.ui-widget-header .ui-button-state-darkgrey a { color: #555555; }
.ui-button-state-darkgrey .ui-icon {
	background-image: url(/library/images/ui-icons_555555_256x240.png);
}


/*
.ui-button-state-default, .ui-widget-content .ui-button-state-default, .ui-widget-header .ui-button-state-default  {border: 1px solid #888888; background: url() ; color: #888888; }
.ui-ui-button-state-default a, .ui-widget-content .ui-button-state-default a,.ui-widget-header .ui-button-state-default a { color: #363636; }

.ui-state-error-button, .ui-widget-content .ui-state-error-button, .ui-widget-header .ui-state-error-button  {border: 0px solid #cc0001; background: url() ; color: #cd0a0a; }
.ui-state-error-button a, .ui-widget-content .ui-state-error-button a,.ui-widget-header .ui-state-error-button a { color: #363636; }


.ui-state-icon-button, .ui-widget-content .ui-state-icon-button, .ui-widget-header .ui-state-icon-button  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #363636;
	padding: 0px;
	margin: 0px;  
}
.ui-state-icon-button a, .ui-widget-content .ui-state-icon-button a,.ui-widget-header .ui-state-icon-button a { color: #363636; }

.ui-state-focus-icon-button {
	border: 0px solid #ffffff; 
	background: #ffffff; 
	color: #FE9900;
	padding: 0px; 
	margin: 0px;
}

.ui-button-state-error .ui-icon {
	background-image: url(/library/images/ui-icons_cc0001_256x240.png);
}
.ui-button-state-ok .ui-icon {
	background-image: url(/library/images/ui-icons_009900_256x240.png);
}
.ui-button-state-blue .ui-icon {
	background-image: url(/library/images/ui-icons_000099_256x240.png);
}
.ui-button-state-black .ui-icon {
	background-image: url(/library/images/ui-icons_222222_256x240.png);
}


.ui-button-state-default .ui-icon {
	background-image: url(/library/images/ui-icons_888888_256x240.png);
}
*/
	
</style>
<?php
//marktime("module wide");
/**********
 * Navigation buttons
 */
if(!isset($no_page_heading) || !$no_page_heading) {
	$active_button_label = $menuObject->drawPageTop($menu,(isset($display_navigation_buttons) ? $display_navigation_buttons : true));
}
?>

<!------------Hint Box ------------>
<!-- <div id="hint_box" style="padding:8px; width:170px; box-shadow: 2px 2px 5px #333333; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div> -->
