<?php

class HELPDESK_USERS extends HELPDESK
{

    private $table_name = "helpdesk_users";
    private $table_field = "hdr_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "users";
    const TABLE_FLD = "user_";
    const REFTAG = "HDR";
    const LOG_TABLE = "object";

    public function __construct(){
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix().$this->id_field;
        $this->status_field = $this->getTableFieldPrefix().$this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getApproverFieldName() {
        return $this->approver_field;
    }
    public function getTableFieldPrefix() { return $this->table_field; }


    //Tshegofatso Mosidi
    public function addObject($var) {

//        return $var;

        //Check if user exists in the helpdesk db
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_tkid = '.$var['helpdesk_user'] . ' ';
        $sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
        $user = $this->mysql_fetch_one($sql);

        //Does this user exist in the helpdesk db
        if(isset($user) && ((is_array($user) && count($user) == 0) || $user === false)) {//No: Create user and make them an admin
            //Get user information from the company db

            // 1. Get information on whether this is a  client or a reseller
            $sql = 'SELECT * FROM helpdesk_companies ';
            // Company Type
            $sql .= 'INNER JOIN helpdesk_company_types ';
            $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

            $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
            $company = $this->mysql_fetch_one($sql);

            if($company['hdct_value'] == 'client'){
                $user_type = 2;//client_user
            }elseif($company['hdct_value'] == 'reseller'){
                $user_type = 3;//reseller_user
            }elseif($company['hdct_value'] == 'iassist'){
                $user_type = 5;//reseller_user
            }

            $sql = 'SELECT tkid, tkname, tksurname FROM assist_'.$company['hdc_cc'].'_timekeep ';
            $sql .='WHERE tkid = \''.$var['helpdesk_user'].'\'';

            $client_db = new ASSIST_MODULE_HELPER();
            $user_details = $client_db->mysql_fetch_one($sql);

            if(count($user_details) > 0){
                $user_id = $var['helpdesk_user'];
                $user_name = $user_details['tkname'] . ' ' . $user_details['tksurname'];
                $user_company = $company['hdc_id'];

                //Enter information in the helpdesk user table
                unset($var);
                $var = array();
                $var[$this->getTableField() . 'tkid'] = $user_id;
                $var[$this->getTableField() . 'name'] = $user_name;
                $var[$this->getTableField() . 'type'] = $user_type;
                $var[$this->getTableField() . 'company'] = $user_company;

                $sql = "INSERT INTO helpdesk_users SET " . $this->convertArrayToSQL($var);
                $id = $this->db_insert($sql);
                if ($id > 0) {
                    //Make them a regular admin
                    $admin_type = 1; // Regular Admin
                    $sql = 'INSERT INTO helpdesk_user_admin_types ';
                    $sql .= '(hduat_userid, hduat_admin_type_id)';
                    $sql .= 'VALUES (' . $id . ', ' . $admin_type . ')';
                    $id = $this->db_insert($sql);
                }
            }

            $event = 'created';
        }else{//Yes: Make This user an admin user
            // 1. Get information on whether this is a  client or a reseller
            $sql = 'SELECT * FROM helpdesk_companies ';
            // Company Type
            $sql .= 'INNER JOIN helpdesk_company_types ';
            $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

            $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'] . '\'';
            $company = $this->mysql_fetch_one($sql);

            if($company['hdct_value'] == 'client'){
                $user_type = 2;//client_user
            }elseif($company['hdct_value'] == 'reseller'){
                $user_type = 3;//reseller_user
            }elseif($company['hdct_value'] == 'iassist'){
                $user_type = 5;//reseller_user
            }

            $sql = 'UPDATE helpdesk_users ';
            $sql .= 'SET user_type = ' . $user_type . ' ';
            $sql .= 'WHERE user_id = ' . $user['user_id'];
            $id = $this->db_update($sql);

            if ($id > 0) {
                //Make them a regular admin
                $admin_type = 1; // Regular Admin
                $sql = 'INSERT INTO helpdesk_user_admin_types ';
                $sql .= '(hduat_userid, hduat_admin_type_id)';
                $sql .= 'VALUES (' . $user['user_id'] . ', ' . $admin_type . ')';
                $id = $this->db_insert($sql);
            }

            $event = 'updated';
        }


        //Update the admin management and the settings
        if($id){
            $company = new HELPDESK_COMPANY();
            $id = $company->updateSettings();
            if($id > 0){
                $result = array(
                    0 => "ok",
                    1 => "Admin User: " . (isset($user_name) ? $user_name : $user['user_name'] ) . " has been successfully assigned the role of admin user",
                    'object_id' => $id,
                );
                return $result;
            }
        }else{
            return array("error", "Testing: " . $sql);
        }

    }

    public function deleteObject($var) {
        //This is more of a demotion than a deletion

        //Check if user is from client or reseller
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_id = '.$var['user_id'];

        $user = $this->mysql_fetch_one($sql);

        if($user['hdct_value'] == 'client'){
            $user_type = 1;//client_user
        }elseif($user['hdct_value'] == 'reseller'){
            $user_type = 4;//reseller_user
        }elseif($user['hdct_value'] == 'iassist'){
            $user_type = 5;//reseller_user
        }

        $sql = 'UPDATE helpdesk_users ';
        $sql .= 'SET user_type = ' . $user_type . ' ';
        $sql .= 'WHERE user_id = ' . $user['user_id'];
        $id = $this->db_update($sql);

        if ($id > 0) {
            //Make them a regular admin
            $sql = 'DELETE FROM helpdesk_user_admin_types ';
            $sql .= 'WHERE hduat_userid = ' . $user['user_id'];
            $id = $this->db_update($sql);
            $diditwork = $id;
        }

        //Update the admin management and the settings
        if($diditwork > 0){
            $company = new HELPDESK_COMPANY();
            $id = $company->updateSettings();
            if($id > 0){
                $result = array(
                    0 => "ok",
                    1 => "Admin User: " . $user['user_name'] . " has been successfully deleted as an admin user",
                    'object_id' => $id,
                );
                return $result;
            }
        }else{
            return array("error", "Testing: " . $sql);
        }
    }

}