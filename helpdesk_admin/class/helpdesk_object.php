<?php
/**
 * To manage the TRAVEL REQUEST object
 * 
 * Created on: 29 June 2016
 * Authors: Janet Currie
 * 
 */
 
class HELPDESK_OBJECT extends HELPDESK {
    
	protected $object_id = 0;
	protected $object_details = array();
    
    protected $progress_status_field = "_status_id";
	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $deadline_field = "_deadline";
	protected $owner_field = "_owner_id";
	protected $approver_field = "_approver";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "object";
    const TABLE_FLD = "object";
    const REFTAG = "TVLREQ";
	const LOG_TABLE = "object";
	/**
	 * STATUS CONSTANTS
	 */
	const CONFIRMED = 32;
	const ACTIVATED = 64;

    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->approver_field = self::TABLE_FLD.$this->approver_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		if($obj_id>0) {
	    	$this->object_id = $obj_id;
			$this->object_details = $this->getRawObject($obj_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "
		//variable to remember the previous progress value when defaulting to 100
		var old_progress = $('#".$this->getProgressFieldName()."').val();
		 
		//on change of the status drop down
		$('#".$this->getProgressStatusFieldName()."').change(function() {
		                //get the selected option value
		                var v = ($(this).val())*1;
		                //if it is set to \"Completed\" (always id=3 except deliverable which is 5)
		                if(v==3) {
		                                //if the progress field is not already set to 100, then remember the current value
		                                if(($('#".$this->getProgressFieldName()."').val())*1!=100) {
		                                                old_progress = $('#".$this->getProgressFieldName()."').val();
		                                }
		                                //update progress field to 100 and disable
		                                $('#".$this->getProgressFieldName()."').val('100').prop('disabled',true);
		                //else if the status field is not Completed, check if the progress field was previously disabled
		                } else if($('#".$this->getProgressFieldName()."').prop('disabled')==true) {
		                                //cancel the disable and reset the value back to the last remembered value
		                                $('#".$this->getProgressFieldName()."').val(old_progress).prop('disabled',false);
		                }
		});";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		//unset($var['contract_id']);	//remove incorrect contract_id from generic form
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_progress'] = 0;
		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = HELPDESK::ACTIVE;
		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|object| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> HELPDESK_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$result = array(
				0=>"ok",
				1=>"New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		return array("error","Not yet programmed.");
	}
	
	
	public function activateObject($var) {
		return array("error","Not yet programmed.");
	}
		
	public function deactivateObject($var) {
		return array("error","Not yet programmed.");
	}
	
	
	public function updateObject($var) {
		return array("error","Not yet programmed.");
	}
	
	public function editObject($var,$attach=array()) {
		return array("error","Not yet programmed.");
	}	
	
	

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getApproverFieldName() {
		return $this->approver_field;
	}

	public function getActiveSQLScript($tn = "C") {
		//return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
		return "";
	}
    
	public function checkIfIsAuthoriser() {
		return array("error","Not yet programmed.");
	}
    public function checkIfIsAApprover() {
		return array("error","Not yet programmed.");
    }
    
	public function getList($section,$options=array()) {
		return $this->getMyList("OBJECT", $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject("OBJECT", $id,$options);
	}
	
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject("OBJECT", $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}	
	
	
	
	
	
	
	
	public function getSummary($obj_id=0) {
		return array("error","Not yet programmed.");
	}
    
	 
     
	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a contract
	 */ 
	public function getDeadlineDate() {
		return $this->object_details[$this->getDeadlineField()];
	}
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>