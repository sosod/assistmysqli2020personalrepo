<?php
/**
 * 
 * 
 * Created on: 2 July 2016
 * Authors: Janet Currie
 * 
 */
 
class HELPDESK_REQUEST extends HELPDESK {
	
	
	private $table_name = "helpdesk_request";
	private $table_field = "req_";
	protected $id_field = "id";
	protected $status_field = "status";
	
	
	const LOG_TABLE = "request";
	
	const AUTHORISATION_REQUIRED = 16;		//STEP 1
	const ACTIVATED = 32;					//STEP 1
	const AUTHORISE_REJECTED = 64;			//STEP 2
	const AUTHORISE_ACCEPTED = 128;			//STEP 2
	const QUOTED = 256;						//STEP 3
	const REVIEW_ACCEPTED = 512;			//STEP 4
	const REVIEW_REJECTED = 1024;			//STEP 4
	const APPROVE_ACCEPTED = 2048;			//STEP 5
	const APPROVE_REJECTED = 4096;			//STEP 5
	const PROCESSED = 8192;					//STEP 6
	
	
	public function __construct(){
		parent::__construct();
		$this->id_field = $this->getTableFieldPrefix().$this->id_field;
		$this->status_field = $this->getTableFieldPrefix().$this->status_field;
	}

	public function getTableName() { return $this->table_name; }
	public function getTableFieldPrefix() { return $this->table_field; }





	public function createObject($var) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$i_data = array(
			'booker',
			'policy_id',
			'benefits_id',
			'project',
			'travel_type',
			'travel_insurance',
			'travel_agent',
			'authoriser',
			'approver',
		);
		$insert_data = array(
			'req_ref' => isset($var['ref']) && strlen($var['ref'])>0 ? $var['ref'] : ""
		);
		foreach($i_data as $k) {
			$k1 = explode("_",$k); $k2 = implode("",$k1);
		}
		$insert_data[$this->getTableFieldPrefix()."status"] = self::ACTIVE;
		$insert_data[$this->getTableFieldPrefix()."insertdate"] = date("Y-m-d H:i:s");
		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data);
		$id = $this->db_insert($sql);
		
		if(!isset($var['ref']) || strlen($var['ref'])==0) {
			$ref = date("Ym")."/".$id;
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getTableFieldPrefix()."ref = '".$ref."' WHERE ".$this->getIDFieldName()." = ".$id;
			$this->db_update($sql);
		} else {
			$ref = $var['ref'];
		}

		//LOGGING
			$changes = array(
				'user'=>$this->getUserName(),
				'response'	=> "Created ".$this->getObjectName("request")." ".$ref
			);
				
				$log_var = array(
					'request_id'	=> $id,
					'object_id'	=> $id,
					'object_type'	=> "REQUEST",
					'changes'	=> $changes,
					'log_type'	=> HELPDESK_LOG::CREATE,
				);
				$this->addActivityLog(self::LOG_TABLE, $log_var);
		$_SESSION[$this->getModRef()]['REQUEST'][$id] = array(
			'policy'=>$_SESSION[$this->getModRef()]['policy'],
			'benefits'=>$_SESSION[$this->getModRef()]['benefits'],
		);
		return array("ok",$this->getObjectName("request")." ".$ref." created successfully.",$id);
	}

	public function updateObject($var) {
		return array("ok","PROGRAM ME",$var['object_id']);
	}


	public function finaliseRequest($var) {
		$req_id = $var['id'];
		
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$req_id;
		$old = $this->mysql_fetch_one($sql);
		
		$status = self::ACTIVE + self::ACTIVATED;
		$response = $this->getObjectName("request")." $req_id has been finalised. ";
		$objObject = new HELPDESK_REQUEST_OBJECTS();
		$auth_required = $objObject->checkAuthorisationRequired($req_id);
		if($auth_required) {
			$status+=self::AUTHORISATION_REQUIRED;
			$response.=" It has been sent for authorisation.";
		} else {
			$response.= " It has been sent to the ".$this->getObjectName("agent").".";
		}
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".($status)." WHERE ".$this->getIDFieldName()." = $req_id ";
		$this->db_update($sql);
		
		/**
		 * NOTIFY RELEVANT PARTY
		 * if(auth_required) {
		 *   check $old for authorisation setting
		 * } else {
		 *   check travel agent db for linked user 
		 * }
		 */
		
		
		//LOGGING
		
		return array("ok",$response."   <br /><br /><span style='font-weight:bold; font-size: 150%; color:#CC0001'>QUESTION???? Who must be notified IF Request requires Authorisation????</span>");
	}





	/**
	 * Function to record authorisation of request and handle notifying booker and sending to travel agent
	 */
	public function authoriseAccepted($req_id) {
		//update request status
		$old = $this->getRawObject($req_id);
		$status = $old['req_status'] - self::AUTHORISATION_REQUIRED + self::AUTHORISE_ACCEPTED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$status." WHERE ".$this->getIDFieldName()." = ".$req_id;
		$this->db_update($sql);
		//logging
		
		//notify booker
		
		//send to TVLA
	}
	/**
	 * Function to record rejection / non-authorisation and handle notifying the booker and sending the request back to NEW process
	 */
	public function authoriseRejected($req_id) {
		//update request status
		$old = $this->getRawObject($req_id);
		$status = $old['req_status'] - self::ACTIVATED - self::AUTHORISATION_REQUIRED + self::AUTHORISE_REJECTED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$status." WHERE ".$this->getIDFieldName()." = ".$req_id;
		$this->db_update($sql);
		//logging
		//notify booker
	}












	/**
	 * function to get a list of requests waiting on authorisation - ADMIN view
	 */
	public function getListOfRequestsAwaitingAuthorisationForAdmin() {
		return $this->getListOfRequests("ADMIN", "AUTHORISE");
	}
	/**
	 * function to get a list of requests waiting on authorisation - MANAGE view
	 */
	public function getListOfRequestsAwaitingAuthorisationForManage() {
		return $this->getListOfRequests("MANAGE", "AUTHORISE");
	}
	/** 
	 * Function to get a list of incomplete requests for NEW
	 */
	public function getListOfIncompleteRequests() {
		return $this->getListOfRequests("NEW","NEW");
	}
	/** 
	 * Function to get a list of activated requests for VIEW
	 */
	public function getListOfActivatedRequests() {
		return $this->getListOfRequests("MANAGE","VIEW");
	}
	/**
	 * Function to get a list of requests for specific page & function
	 */
	public function getListOfRequests($section,$page) {
		//Get headings
		$headObject = new HELPDESK_HEADINGS();
		$headings = $headObject->getMainObjectHeadings("REQUEST","LIST",$section,"",true);
		$head = array();
		foreach($headings as $fld=>$h) {
			$head[$fld] = $h['name'];
		}
		//$this->arrPrint($headings);
		//Get rows
		$listObject = new HELPDESK_LIST("travel_type");
		$projectObject = new HELPDESK_PROJECTS();
		$project_owners = $projectObject->getAllOwnersForRequestDisplay();
		$sql = "SELECT req.*
				  , req_booker as raw_req_booker
				  , CONCAT(tk.tkname,' ',tk.tksurname) as req_booker
				  , TT.id as raw_req_traveltypeid
				  , TT.name as req_traveltypeid
				  , req_policyid as raw_req_policyid
				  , CONCAT('".HELPDESK_POLICY::REFTAG."',req_policyid) as req_policyid 
				  , req_benefitsid as raw_req_benefitsid
				  , CONCAT('".HELPDESK_BENEFITS::REFTAG."',req_benefitsid) as req_benefitsid
				  , req_authoriser as raw_req_authoriser
				  , IF(req_authoriser='project','".$this->getObjectName("project")."-dependent',IF(req_authoriser='any','Any',CONCAT(auth.tkname,' ',auth.tksurname))) as req_authoriser
				  , req_approver as raw_req_approver 
				  , IF(req_approver='project','".$this->getObjectName("project")."-dependent',IF(req_approver='any','Any',CONCAT(appr.tkname,' ',appr.tksurname))) as req_approver
				  , req_project as raw_req_project
				  , proj.".$projectObject->getNameFieldName()." as req_project
				FROM ".$this->getTableName()." req 
				INNER JOIN ".$this->getUserTableName()." tk
				  ON req_booker = tk.tkid
				INNER JOIN ".$listObject->getListTable()." TT
				  ON req_traveltype = TT.id
				LEFT OUTER JOIN ".$this->getUserTableName()." auth
				  ON req_authoriser = auth.tkid
				LEFT OUTER JOIN ".$this->getUserTableName()." appr
				  ON req_approver = appr.tkid
				LEFT OUTER JOIN ".$projectObject->getTableName()." proj
				  ON req_project = proj.".$projectObject->getIDFieldName()."
				WHERE 
					(req_status & ".self::ACTIVE.") = ".self::ACTIVE."
				";
			switch($page) {
				case "NEW":
					$where = " AND req_status = ".self::ACTIVE." 
					AND req_booker = '".$this->getUserID()."'";
					break;
				case "VIEW":
					$where = "
					AND (req_status & ".self::ACTIVATED.") = ".self::ACTIVATED."
					";
					break;
				case "AUTHORISE":
					$where = "
					AND (req_status & ".self::ACTIVATED.") = ".self::ACTIVATED."
					AND (req_status & ".self::AUTHORISATION_REQUIRED.") = ".self::AUTHORISATION_REQUIRED."
					AND (req_status & ".self::AUTHORISE_ACCEPTED.") <> ".self::AUTHORISE_ACCEPTED."
					AND (req_status & ".self::AUTHORISE_REJECTED.") <> ".self::AUTHORISE_REJECTED."
						
					";
					if($section=="MANAGE") {
						$where.="
							AND (
								req_authoriser = '".$this->getUserID()."'
								OR
								req_authoriser = 'any'
								OR
								req_authoriser = 'project'
							)
							";
					}
					break;
				case "APPROVE":
					break;
			}	
		$sql.=$where;
		$rows = $this->mysql_fetch_all_by_id($sql, "req_id");
		
		//Get list of travel agents
		$agentObject = new HELPDESK_AGENTS();
		$agents = $agentObject->getAllTravelAgentsForHELPDESK();
		
		$displayObject = new HELPDESK_DISPLAY();
		
			$project_ids = array();
			$projectObject = new HELPDESK_PROJECTS();
			//get list of projects ids from rows
			// & set display format of travel insurance
			foreach($rows as $i => $r) {
				if($r['req_travelinsurance']==0) {
					$rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(0);
				} else {
					$rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(1);
				}
				$rows[$i]['raw_req_travelagent'] = $r['req_travelagent'];
				$rows[$i]['req_travelagent'] = isset($agents[$r['req_travelagent']]) ? $agents[$r['req_travelagent']] : $r['req_travelagent'];
				if(!in_array($r['raw_req_project'],$project_ids)) {
					$project_ids[] = $r['raw_req_project'];
				}
			}
			//get list of parents
			if(count($project_ids)>0) {
				$project_parents = $projectObject->getProjectHierarchyForRequest($project_ids,true);
				//get authorisers/approvers for all
				$project_owners = $projectObject->getAllOwners();
				//generate list of owners for original list of ids
				$project_authorisers = array();
				$project_approvers = array();
				foreach($project_parents as $pi => $pp) {
					$project_authorisers[$pi] = array();
					$project_approvers[$pi] = array();
					foreach($pp as $p) {
						if(isset($project_owners[$p])) {
							foreach($project_owners[$p] as $ti => $t) {
								if($t['authoriser']==true) {
									$project_authorisers[$pi][$ti] = $t['name'];
								}
								if($t['approver']==true) {
									$project_approvers[$pi][$ti] = $t['name'];
								}
							}
						}
					}
				}
				foreach($rows as $ri => $r) {
					$pi = $r['raw_req_project'];
					//check rows and remove those without access
					if($section=="MANAGE" && $page=="AUTHORISE" && $r['raw_req_authoriser']=="project" && !isset($project_authorisers[$pi][$this->getUserID()])) {
						unset($rows[$ri]);
					} elseif($section=="MANAGE" && $page=="APPROVE" && $r['raw_req_approver'] && !isset($project_approvers[$pi][$this->getUserID()])) {
						unset($rows[$ri]);
					} else {
						if($r['raw_req_authoriser']=="project") {
							$rows[$ri]['req_authoriser'] = implode(";<br />",$project_authorisers[$pi]).";";
						}
						if($r['raw_req_approver']=="project") {
							$rows[$ri]['req_approver'] = implode(";<br />",$project_approvers[$pi]).";";
						}
					}
				}
			}
		
		
		$req_ids = array_keys($rows);
		if(count($req_ids)>0) {
			$travellerObject = new HELPDESK_REQUEST_TRAVELLERS();
			$travellers = $travellerObject->getList($req_ids);
			foreach($req_ids as $r) {
				$rows[$r]['traveller'] = implode(", ",$travellers[$r]);
			}
		}
		
		$data = array(
			'head'=>$head,
			'rows'=>$rows
		);
		
		
		return $data;
	}


	public function getRawObject($req_id) {
		$listObject = new HELPDESK_LIST("travel_type");
		$sql = "SELECT req.*
				  , CONCAT(tk.tkname,' ',tk.tksurname) as booker
				  , CONCAT('".HELPDESK_POLICY::REFTAG."',req_policyid) as policy 
				  , CONCAT('".HELPDESK_BENEFITS::REFTAG."',req_benefitsid) as benefits
				  , tt.shortcode as travel_type
				FROM ".$this->getTableName()." req
				INNER JOIN ".$this->getUserTableName()." tk
				  ON req_booker = tk.tkid
				INNER JOIN ".$listObject->getListTable()." tt
				  ON tt.id = req.req_traveltype
				WHERE req_id = ".$req_id;
		$data = $this->mysql_fetch_one($sql);
		foreach($data as $key => $d) {
			$k = explode("_",$key);
			unset($k[0]);
			$key = implode("_",$k);
			if(!isset($data[$key])) {
				$data[$key] = $d;
			}
		}
		$data['travellers'] = array();
		$data['travellers_benefit_category'] = array();
		$data['benefit_category_id'] = array();
		$data['travellers_extras'] = array();
		$rtObject = new HELPDESK_REQUEST_TRAVELLERS();
		$rt = $rtObject->getList($req_id,true);  
		foreach($rt as $ti => $t) {
			$data['travellers'][$ti] = $t['name'];
			$data['travellers_benefit_category'][$ti] = $t['benefit_category'];
			$data['travellers_benefit_category_id'][$ti] = $t['benefit_category_id'];
			if(!in_array($t['benefit_category_id'],$data['benefit_category_id'])) {
				$data['benefit_category_id'][] = $t['benefit_category_id'];
			}
			$data['travellers_extras'][$ti] = isset($t['rt_extras']) && strlen($t['rt_extras'])>0 ? $rtObject->decodeExtrasFromDB($t['extras']) : array();
		}
		
		$pObject = new HELPDESK_PROJECTS();
		$data['projects'] = $pObject->getProjectHierarchyForRequest($data['project']);
		
		return $data;
	}



	public function getBenefitSettings($req_id) {
		return $_SESSION[$this->getModRef()]['REQUEST'][$req_id]['benefits']['settings'];
	}



	public function __destruct() {
		parent::__destruct();
	}
} 

?>
