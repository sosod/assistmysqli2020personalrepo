<?php
include("inc_ignite.php");
include("inc_errorlog.php");

$cateid = $_GET['c'];
$sort = $_GET['s'];
if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if($sort == "d") {
    $orderby = "docdate DESC, doctoc ASC";
} else {
    $orderby = "doctoc ASC, docdate DESC";
}
if(strlen($orderby)==0) { $orderby = "doctoc ASC, docdate DESC"; }


$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid;
include("inc_db_con.php");
$cate = mysql_fetch_array($rs);
mysql_close();

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var minfile = me.minfile.value;
/*    var mday = me.mday.value;
    var mmon = me.mmon.value;
    var myear = me.myear.value;
*/    var mindt = me.mindt.value;
    var mintoc = me.mintoc.value;
    var valid8 = "false";
    
//    if(mday.length == 0 || mmon.length == 0 || myear.length == 0)
    if(mindt.length==0)
    {
        alert("Please indicate the date of the document.");
    }
    else
    {
        if(minfile.length == 0)
        {
            alert("Please select a file for upload.");
        }
        else
        {
//            var ftype = minfile.substr(minfile.lastIndexOf(".")+1,3);
//            if(ftype.toLowerCase() != "pdf")
//            {
//                alert("Invalid file.  Only PDF documents may be uploaded.");
//            }
//            else
//            {
                valid8 = "true";
//            }
        }
    }
    if(valid8 == "true")
    {
        if(mintoc.length == 0)
        {
            if(confirm("You have not entered any table of contents.\n\nAre you sure you wish to continue?") == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
    return false;
}

function showMin(mfn) {
    var url = "http://assist.ignite4u.co.za"+mfn;
    //alert(url);
    newwin = window.open(url,"","dependent=0 ,toolbar=1,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=700,height=500")
}

function delMin(m) {
    if(confirm("Are you sure you want to delete document "+m+"?")==true)
    {
        //alert("delete");
        document.location.href = "update_delete.php?m="+m;
    }
    else
    {
        //alert("aborting");
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border-collapse: collapse;
    border: 1px solid #ababab;
}
table td {
    border: 1px solid #ababab;
/*    vertical-align: top;*/
}
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    ?>
<style type=text/css>
    body {
        font-size: 62.5%;
    }
</style>
    <?php
}
?>
		<link type="text/css" href="/lib/blue/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="/lib/blue/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="/lib/blue/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: '/lib/blue/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                });

			});
		</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Update</h1>
<h2 class=fc><?php echo($cate['catetitle']); ?></b></h2>
<form name=update action="update_process.php" method="post" enctype="multipart/form-data" lang=jscript onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0 width=500>
    <tr>
        <td class=tdgeneral><b>Category:</b>&nbsp;</td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?><input type=hidden name=mincateid value=<?php echo($cate['cateid']);?>></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>Date:</b></td>
        <td class=tdgeneral><input type=text readonly=readonly name=mindt id=datepicker1 value="<?php echo(date("d-m-Y",$today)); ?>"></td>
    </tr>
    <tr>
        <td class=tdgeneral valign=top><b>Contents:</b>&nbsp;</td>
        <td class=tdgeneral><textarea name=mintoc rows=5 cols=50></textarea></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>File:</b><br><i><small>Max file size: 5MB</small></i></td>
        <td class=tdgeneral><input type=file name=minfile size=50></td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value=Update> <input type=reset></td>
    </tr>
</table>
</form>
<a name=doc></a><p style="margin-top: 30px"><a href="admin.php"><img src=/pics/tri_left.gif align=absmiddle border=0><span style="text-decoration: none;"> </span>Go Back</a></p>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_content m WHERE doccateid = ".$cateid." AND docyn = 'Y' ORDER BY ".$orderby;
include("inc_db_con.php");
$m = mysql_num_rows($rs);
if($m > 0)
{
    echo("<hr class=fc>");
    echo("<table border=1 cellpadding=3 cellspacing=0 width=500>");
    echo("    <tr>");
    echo("        <td class=tdheader width=20 height=27>Ref</td>");
    echo("        <td class=tdheader width=130>");
                if($sort!="d") { echo("<a href=\"update.php?c=".$cateid."&s=d#doc\" style=\"color: #ffffff;\" >"); }
                echo("Date</a></td>");
    echo("        <td class=tdheader width=300>");
                if($sort!="c") { echo("<a href=\"update.php?c=".$cateid."&s=c#doc\" style=\"color: #ffffff;\" >"); }
                echo("Contents</a></td>");
    echo("        <td class=tdheader width=50>&nbsp;</td>");
    echo("    </tr>");
    while($row = mysql_fetch_array($rs))
    {
        $floc = "/files/".$cmpcode."/".$row['docfilename'];
        $floc2 = "../files/".$cmpcode."/".$row['docfilename'];
        if(file_exists($floc2))
        {
            echo("    <tr>");
            echo("        <td class=tdheader align=center valign=top>".$row['docid']."</td>");
            echo("        <td class=tdgeneral valign=top>&nbsp;".date("d F Y",$row['docdate'])."&nbsp;</td>");
            echo("        <td class=tdgeneral valign=top>");
            if(strlen($row['doctoc']) > 0)
            {
                $mintoc = str_replace(chr(10),"<br>",$row['doctoc']);
                echo($mintoc);
            }
            else
            {
                echo("&nbsp;");
            }
            echo("</td>");
            echo("        <td class=tdgeneral valign=top align=center><input type=button value=View onclick=\"showMin('".$floc."')\"><br><input type=button value=Delete onclick=\"delMin('".$row['docid']."')\"></td>");
            echo("    </tr>");
        }
    }
    echo("</table>");
}
mysql_close();
?>
<p style="margin-top: 30px"><a href="admin.php"><img src=/pics/tri_left.gif align=absmiddle border=0><span style="text-decoration: none;"> </span>Go Back</a></p>

</body>

</html>
