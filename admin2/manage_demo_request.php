<?php

//error_reporting(-1);

include("../module/autoloader.php");

$me = new ADMIN2();

$scripts = array("admin2helper.js");
$styles = array("/assist_jquery.css","/assist3.css");

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);


$source_cmpcode = $_REQUEST['source_cmpcode'];
$company = $me->getCompanyInfo($source_cmpcode);
IF(!isset($save_action)){
    $save_action = "ADD";
}
$page_redirect = "manage_company.php?cc=".$source_cmpcode."&";
?>
<h1>Assist Client Admin > Request New Demo / Training Database</h1>

Submitting request...
<form name=frm_edit_new_demo language="jscript" enctype="multipart/form-data" method="post">
    <input type=hidden name="form_action" id="form_action" value='ADD' class='no-check' />
    <input type=hidden name="page_direct" id="page_direct" value='<?php echo $page_redirect; ?>' class='no-check' />
    <input type=hidden name="cmp_name" id="cmp_name" value='<?php echo $company['cmpname']; ?>' class='no-check' />
    <input type=hidden name="cmp_code" id="cmp_code" value='<?php echo $company['cmpcode']; ?>' class='no-check' />
</form>

<?php
ASSIST_HELPER::goBack();
//No user input required
//Process
/*1. [does_demo_exist] check if a client database exists with the same cmpcode as the $source_cmpcode but ending 999d
	1.2 [is_demo_already_linked] if yes, check if that db is linked to another db as their demo (*_ignite4u.assist_company_demos)
2. save request to assist_new_demo_requests (new table) in the *_ignite4u table
2. send email to JC with details:
	source_cmpcode
	does_demo_exist
	is_demo_already_linked
	user details of requesting user (person logged in)
3. Raise HDR on users behalf and send to IASSIST [standard HDR process] (message = "Please create demo/training database for source_company_name (source_cmpcode)")
4. Give feedback to user with HDR ref and return them back to the manage_company.php?cc=$source_cmpcode page  (HDR ref can appear on the client request list - Soso)*/
?>
<script>
    AssistHelper.processing();
    window.onload = init;
    function init(){

        var $form = $("form[name=frm_edit_new_demo]");

        var page_action = "<?php echo "DEMO." . $save_action; ?>";

        var dta = AssistForm.serialize($form);
        var result = AssistHelper.doAjax("inc_controller.php?action=" + page_action, dta);

        if (result[0] == "ok") {
            var url = "<?php echo $page_redirect; ?>";
            AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
        } else {
            AssistHelper.finishedProcessing(result[0], result[1]);
        }
    }
</script>
