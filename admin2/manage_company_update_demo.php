<?php

//error_reporting(-1);

include("../module/autoloader.php");

$me = new ADMIN2();
$db = new ASSIST_DB("master");

$scripts = array();
$styles = array("/assist_jquery.css","/assist3.css");

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);


$db_user = $me->getDBUser();
$db_pwd = $me->getDBPwd();
$dbs = array();
/**
 * Updated to sqli standard. spotted while doing #AA-624
 * 30 June 2021 [SD]
 * reference : https://stackoverflow.com/questions/4677279/how-to-get-a-list-of-databases
 */
$link = mysqli_connect("localhost",$db_user,$db_pwd);
$db_list =  mysqli_query($link, "SHOW DATABASES");
while ($row = mysqli_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
unset($link);
unset($db_pwd);
unset($db_user);

?>
<h1>Assist Client Admin</h1>
<h2>Update Demo Database</h2>
<ol>
<?php

$cc = $_REQUEST['cc'];


$company = $me->getCompanyInfoForDemoUpdate($cc);
$demo_cc = $company['paired_demo'];


$src_db = $me->getDBPrefix().strtolower($cc);
$dest_db = $me->getDBPrefix().strtolower($company['paired_demo']);

$rename_tbl = true;
$drop_dest = true;
$is_drop_dest = true;
//validate that destination is not blank variable
if($dest_db!=$me->getDBPrefix()) {

//Validate that source exists
echo "<li>Validating that source database '$cc' exists... ";
	if(!in_array($src_db,$dbs)) {
		echo "<span class=idelete>failure</span> - Source database for '$cc' does not appear to exist.  Please contact Action iT."; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Validate that destination does not exist
echo "<li>Validating that demo database exists... ";
	if(in_array($dest_db,$dbs)) {
		$sql = "DROP DATABASE ".$dest_db;
		$db->db_query($sql);
		echo "<span class=isubmit>success</span> - Demo database $demo_cc has been found and removed.</li>";
	} else {
		echo "<span class=isubmit>failure</span> - Paired demo database $demo_cc does not exist. Please contact Action iT.</li>"; die();
	}
//Create blank database
echo "<li>Creating blank database ... ";
	$sql = "CREATE DATABASE IF NOT EXISTS `".$dest_db."`";
	$db->db_query($sql);
	echo "<span class=isubmit>success</span></li>";
//Get/Copy base tables from blank database
echo "<li>Copying tables from source... ";
	$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
			WHERE TABLE_SCHEMA = '".$src_db."'";
	$tables = $db->mysql_fetch_fld_one($sql,"TABLE_NAME");
	//arrPrint($tables);
	$d = explode("_",$dest_db);
	$d_cmpcode = strtolower($demo_cc);
	$dest = new ASSIST_DB("client",$d_cmpcode);
	$db_ref = "assist_".$d_cmpcode;
	$s = explode("_",$src_db);
	$s_cmpcode = strtolower($cc);
	$src = new ASSIST_DB("client",$s_cmpcode);
	//echo "<li class=b>".$db_ref."</li>";
	foreach($tables as $old_tbl) {
		//echo "<li>".$old_tbl;
		//if($rename_tbl===true) {
			//echo " ::> <span class=isubmit>RENAMING!!!</span>";
			$etbl = explode("_",$old_tbl);
			//echo "  ::>  ".$etbl[1]."  ::>  ".$s_cmpcode;
			if($etbl[1]==$s_cmpcode) {
				unset($etbl[0]); unset($etbl[1]);
				$new_tbl = strtolower($db_ref."_".implode("_",$etbl));
			} else {
				$new_tbl = strtolower(implode("_",$etbl));
			}
		//} else {
		//	$new_tbl = $old_tbl;
		//}
		//echo "  ::>  ".$new_tbl."</li>";
		//delete existing table (in case of interrupted creation process)
		$sql = "DROP TABLE IF EXISTS `".$dest_db."`.`".$new_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//create table
		$sql = "CREATE TABLE `".$dest_db."`.`".$new_tbl."` LIKE `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//insert data
		$sql = "INSERT INTO `".$dest_db."`.`".$new_tbl."` SELECT * FROM `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_insert($sql);
	}
	echo "<span class=isubmit>success</span></li>";
//Reset passwords
echo "<li>Reset user passwords / emails... ";
//if(isset($_REQUEST['reset_pwd']) && strtoupper($_REQUEST['reset_pwd'])=="Y") {
	//echo "Yes...";
	$reset_tbl = "assist_".$d_cmpcode."_timekeep";
	$dest->db_update("UPDATE `".$dest_db."`.`".$reset_tbl."` SET tkemail = '".$company['copy_email']."', tkpwd = '417373697374313233'");
	echo "<span class=isubmit>success</span></li>";
//} else {
	//echo "NO</li>";
//}

echo "<li>Updating Paired Demo record...";
$me->updatePairedDemo($cc,$company);
echo "<span class=isubmit>success</span>";

echo "<P class=b>Copy process completed successfully.  Redirecting you back to the Company Management page.</p><script type=text/javascript>document.location.href = 'manage_company.php?cc=".$cc."&r[]=ok&r[]=".urlencode("$cc has been successfully copied to $demo_cc ")."';</script>";











































}



//ASSIST_HELPER::arrPrint($company);





?>