<?php
/*******
ADMIN V2
Created by: Sondelani Dumalisile
Created: September 2020
 ***************************/

class ADMIN2_DISPLAY EXTENDS ASSIST_MODULE_DISPLAY
{

    public function __construct($modref="") {
        $me = new ADMIN2($modref);
        $an = $me->getAllActivityNames($modref);
        $on = $me->getAllObjectNames($modref);
        //$this->mod_ref = $me->getModRef();
        unset($me);

        parent::__construct($an,$on);
    }
    /**
     * (ECHO) displays html of selected form field and returns any required js
     */
    public function drawFormField($type,$options=array(),$val="") {
        //echo "dFF VAL:-".$val."- ARR: "; print_r($options);
        $ff = $this->createFormField($type,$options,$val);
        //if(is_array($ff['display'])) {
        //print_r($ff);
        //}
        echo $ff['display'];
        return $ff['js'];
    }

    /**
     * Returns string of selected form field
     *
     * @param *(String) type = the type of form field
     * @param (Array) options = any additional properties
     * @param (String) val = any existing value to be displayed
     * @return (String) echo
     */
    public function createFormField($type,$options=array(),$val="") {
        //echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
        switch($type){
            case "REF":
                $data = array('display'=>$val,'js'=>"");
                break;
            case "LABEL":
                $data=$this->getLabel($val,$options);
                break;
            case "SMLVC":
                $data=$this->getSmallInputText($val,$options);
                break;
            case "MEDVC":
                $data=$this->getMediumInputText($val,$options);
                break;
            case "LRGVC":
                $data=$this->getLimitedTextArea($val,$options);
                break;
            case "TEXT":
                if(isset($options['rows']) && isset($options['cols'])) {
                    $rows = $options['rows']; unset($options['rows']);
                    $cols = $options['cols']; unset($options['cols']);
                    $data=$this->getTextArea($val,$options,$rows,$cols);
                } else {
                    $data=$this->getTextArea($val,$options);
                }
                break;
            case "LIST":
                $items = $options['options'];
                unset($options['options']);
                $data=$this->getSelect($val,$options,$items);
                break;
            case "MULTILIST":
                $items = $options['options'];
                unset($options['options']);
                if(!is_array($val)) {
                    $val2 = array();
                    if(strlen($val)>0) {
                        $val2[] = $val;
                    }
                } else {
                    $val2 = $val;
                }
                $data=$this->getMultipleSelect($val2,$options,$items);
                break;
            case "DATE": //echo "date!";
                $extra = $options['options'];
                unset($options['options']);
                $data=$this->getDatePicker($val,$options,$extra);
                break;
            case "COLOUR":
                $data=$this->getColour($val,$options);
                break;
            case "RATING":
                $data=$this->getRating($val, $options);
                break;
            case "CURRENCY": $size = 15; $class="right";
            case "PERC":
            case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
            case "NUM": $size = !isset($size) ? 0 : $size;
                if(isset($options['symbol'])) {
                    $symbol = $options['symbol'];
                    $has_sym = true;
                    unset($options['symbol']);
                    $symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
                    unset($options['symbol_postfix']);
                } else {
                    $has_sym = false;
                } //ASSIST_HELPER::arrPrint($options);
                if(isset($class)){ $options['class'] = (isset($options['class']) ? $options['class'] : "")." ".$class; }
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
                    if(!$has_sym) {
                        $data['display']="R&nbsp;".$data['display'];
                    } elseif(strlen($symbol)>0) {
                        if($symbol_postfix==true) {
                            $data['display']=$data['display']."&nbsp;".$symbol;
                        } else {
                            $data['display']=$symbol."&nbsp;".$data['display'];
                        }
                    } else {
                        //don't add a symbol
                    }
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                    $data['display'].="&nbsp;%";
                }
                break;
            case "BOOL_BUTTON":
            case "BOOL":
                //echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
                $val = ASSIST_HELPER::checkIntRef($val) ? $val : 1;
                $options['class'] = (!isset($options['class']) ? "BOOL":$options); //adding a class name to the bool input field
                $data = $this->getBoolButton($val,$options);
                break;
            case "ATTACH"://echo "attach: ".$val;
                //$data = array('display'=>$val,'js'=>"");
                //ASSIST_HELPER::arrPrint($options);
                $data = $this->getAttachmentInput($val,$options);
                break;
            case "CALC" :
                $options['extra'] = explode("|",$options['extra']);
                $data = $this->getCalculationForm($val,$options);
                break;
            default:
                $data = array('display'=>$val,'js'=>"");
                break;
        }
        return $data;
    }


    function drawField($section,$fld,$head) {

        global $full_client_details;
        global $source_cmpcode;
        global $displayObj;
        global $is_add_page;



        $js = "";

        $prop = array(
            'id'=>$fld,
            'name'=>$fld,
        );
        $prop['req'] = isset($head['required']) ? $head['required'] : false;
        switch($head['type']) {
            case "MULTILIST":
            case "LIST":
			//If list table has a module then it takes modules from the man_external class.
			if(isset($head['list_table']) && $head['list_table'] == "modules" || $head['list_table'] == "list_modules" || $head['list_table'] == "module") {
				$externalObj = new MAN_EXTERNAL();
				$list_options = $externalObj->getModulesAvailableForNewModuleRequestsFormattedForSelect();
			} else {
                $listObj = new ADMIN2_LIST($head['list_table']);
                $list_options = $listObj->getActiveListItemsFormattedForSelect();
			}
			$prop['options'] = ($list_options);
                $prop['req'] = isset($head['required']) ? $head['required'] : false;
                $prop['name'] = ($head['type'] == "MULTILIST" ? $fld."[]" : $fld); //adds array to the name prop for multiselect
                $value = $head['default_value'];
                if($is_add_page ) {
                    /*if(isset($full_client_details[$fld])) {
                        $value = $full_client_details[$fld];
                    }*/
                }
                if($is_add_page ) {

                    $js.= $displayObj->drawFormField($head['type'],$prop,$value);
                }
                break;

            case "DATE":
                $prop['options'] = array();
                $value = $head['default_value'];
                if($is_add_page ) {
                   /* if(isset($full_client_details[$fld])) {
                        $value = $full_client_details[$fld];
                    }*/
                    if(strlen($value)>0 && $value!="0000-00-00") {
                        $value = date("d-M-Y",strtotime($value));
                    } else {
                        $value = "";
                    }
                }
                if($is_add_page ) {

                    $js.= $displayObj->drawFormField($head['type'],$prop,$value);
                }
                break;
            case "ATTACH":
                $value = $head['default_value'];


                $prop['allow_multiple'] = false;

                 $prop['action'] = $section.".ADD"; //The value for action is passed on the (EDIT and ADD document) script.
                //Url where user must be redirected - check address
                $prop['page_direct'] = "dialog";

                $js.= $displayObj->drawFormField($head['type'],$prop,$value);

                //Document Add Dialog -> When I click Save button:
                //Emp1Helper.processObjectFormWithAttachment($('my document form name here'));

                break;
            default:
                $value = $head['default_value'];
                if($is_add_page ) {
                    /*if(isset($full_client_details[$fld])) {
                        $value = $full_client_details[$fld];
                    }*/
                }
                if($is_add_page ) {

                    $js.= $displayObj->drawFormField($head['type'],$prop,$value);
                }
                break;
        }

        return $js;
    }


}