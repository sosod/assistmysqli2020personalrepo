<?php

class ADMIN2_LIST EXTENDS ADMIN2
{
    private $my_list = "";
    private $list_table = "";
    private $fields = array();
    private $field_names = array();
    private $field_types = array();
    private $associatedObject;
    private $object_table = "";
    private $object_field = "";
    private $parent_field = "";
    private $sort_by = "sort, name";
    private $sort_by_array = array("sort","name");
    private $is_sort_function_available = false;
    private $my_name = "name";
    private $sql_name = "|X|name";
    private $is_multi_list = false;
    private $is_sort_field_available = true;
    private $default_field_names = array(
        'id'=>"Ref",
        'name'=>"Name",
        'default_name'=>"Default Name",
        'client_name'=>"Your Name",
        'shortcode'=>"Short Code",
        'description'=>"Description",
        'status'=>"Status",
        'colour'=>"Colour",
        'rating'=>"Score",
        'list_num'=>"Supplier Category",
        'sort'=>"Display Order",
    );
    private $default_field_types = array(
        'id'=>"REF",
        'default_name'=>"LRGVC",
        'client_name'=>"LRGVC",
        'name'=>"LRGVC",
        'shortcode'=>"SMLVC",
        'description'=>"TEXT",
        'status'=>"STATUS",
        'colour'=>"COLOUR",
        'rating'=>"RATING",
        'list_num'=>"LIST",
        'sort'=>"ORDER",
    );
    private $required_fields = array(
        'id'=>false,
        'name'=>true,
        'default_name'=>false,
        'client_name'=>true,
        'shortcode'=>true,
        'description'=>false,
        'status'=>false,
        'colour'=>true,
        'rating'=>true,
        'list_num'=>true,
        'sort'=>false,
    );

    public function __construct($list="",$modref=""){
        parent::__construct($modref);
        if(strlen($list)>0) {
            $this->my_list = $list;
            $this->setListDetails();
        }
    }
    /**
     * Returns all active list items formatted for display in SELECT => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItemsFormattedForSelect($options="") {
        $rows = $this->getActiveListItems($options);
        $dta = $this->formatRowsForSelect($rows);
        if(in_array("list_num",$this->fields)) {
            $data = array();
            $data['options'] = $dta;
            $data['list_num'] = array();
            foreach($rows as $key => $r) {
                $data['list_num'][$key] = $r['list_num'];
            }
        } else {
            $data = $dta;
        }
        return $data;
    }
    /**
     * Returns all active list items in an array with the id as key
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItems($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".ADMIN2::ACTIVE." = ".ADMIN2::ACTIVE." )";
        return $this->getListItems($options);
    }
    public function getListItems($options="") {
        $sql = "SELECT * ";
        /*if(in_array("client_name",$this->fields)) {
            $sql.=", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
        }*/
        $sql.= " FROM ".$this->getListTable()." WHERE (status & ".ADMIN2::DELETED.") <> ".ADMIN2::DELETED.(strlen($options)>0 ? " AND ".$options : "")." ORDER BY ".$this->sort_by;
        //echo $sql;
        $mdb = new ASSIST_DB("master");
        return $mdb->mysql_fetch_all_by_id($sql, "id");
    }

    protected function formatRowsForSelect($rows,$name="name") {
        $data = array();
        foreach($rows as $key => $r) {
            $data[$key] = $r[$name];
        }
        return $data;
    }
    public function getListTable(){
        return $this->list_table;
    }


    /****************************
     * Private functions: functions only for use within the class
     *********************/

    /**
     * Sets the various variables needed by the class depending on the list name provided
     */
    private function setListDetails() {
        $this->is_multi_list = false;
        $this->sort_by = "sort, name, shortcode";
        $this->sort_by_array = array("sort","name","shortcode");
        $this->my_name = "name";
        $this->sql_name = "|X|name";
        $this->is_sort_function_available = true;
        //"sort",
        $this->fields = array(
            'id'=>"id",
            'shortcode'=>"shortcode",
            'name'=>"name",
            'description'=>"description",
            'status'=>"status",
        );
        $this->associatedObject = new ADMIN2();
        $this->object_table = "assist";
        switch($this->my_list) {

            case "list_modules": $this->my_list ="modules";
            case "modules":
                $this->object_field = $this->associatedObject->getTableField()."_list_modules";
                $this->is_sort_field_available = false;
                unset($this->fields['shortcode']);
                $this->sort_by = "name";
                $this->sort_by_array = array("name");
                $this->object_field = $this->associatedObject->getTableField()."_list_modules";
                break;
            case "list_finteg": $this->my_list ="finteg";
            case "finteg":
                $this->object_field = $this->associatedObject->getTableField()."_list_finteg";
                $this->is_sort_field_available = false;
                unset($this->fields['shortcode']);
                $this->sort_by = "name";
                $this->sort_by_array = array("name");
                $this->object_field = $this->associatedObject->getTableField()."_list_finteg";
                break;
            /*case "job_loc": $this->my_list = "location";
            case "location":
                $this->is_linked_to_old_master_setups = true;
                $this->old_master_setups_table = "assist_".$this->getCmpCode()."_list_loc";
                $this->is_sort_field_available = false;
                unset($this->fields['shortcode']);
                $this->sort_by = "name";
                $this->sort_by_array = array("name");
                $this->object_field = $this->associatedObject->getTableField()."_loc";
                break;*/


        }
        $this->list_table = $this->object_field;
        if($this->my_name!="name") { $this->sql_name = " if(length(|X|client_name) > 0, |X|client_name, |X|default_name) "; } else {}
        foreach($this->fields as $key){
            if(!isset($this->field_names[$key])) {
                $this->field_names[$key] = $this->default_field_names[$key];
            }
            if(!isset($this->field_types[$key])) {
                $this->field_types[$key] = $this->default_field_types[$key];
            }
        }
    }
}