<?php
/*******
ADMIN V2
Created by: Sondelani Dumalisile
Created: September 2020
 ***************************/

class ADMIN2_FINTEG EXTENDS ADMIN2
{
    const TABLE = "finteg_requests";
    const TABLE_FLD = "req";
    const OBJECT_NAME = "Financial Integration";
    protected $ref_tag = "F";
    protected $attachment_field = "attachments";
    protected $progress_field = "_progress";
    protected $id_field = "req_id";
    protected $parent_field = "req_user_tkid";
    const OBJECT_TYPE = "FINTEG";


    public function __construct($modref="") {
        parent::__construct($modref);

    }
    public function getTableName() { return "assist_new_".self::TABLE;	}
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }




    public function addObject($var,$headings=array()) {
        $result = array("error","Nothing done.");
		$finteg_id = "";
		$user_id = "";
        unset($var['form_action']);
        $finteg_id = $var['finteg_id'];
        unset($var['finteg_id']);
        $user_id = $var['user_id'];
        unset($var['user_id']);
        $cmp_name = $var['cmp_name'];
        unset($var['cmp_name']);
        $cmp_code = $var['cmp_code'];
        unset($var['cmp_code']);

        $reseller = $this->getResellerInfo($this->getCmpCode());
        $username = (isset($reseller['cmpcode']) && isset($reseller['default_admin']) && ($reseller['cmpcode'] =="IGN0001")? $reseller['default_admin'] :$this->getActualUserName());
        //set default fields
        $insert_data = array(
            self::TABLE_FLD.'_status' => ADMIN2::ACTIVE,
            self::TABLE_FLD.'_date' => date("Y-m-d H:i:s"),
            self::TABLE_FLD.'_user_tkid' => $this->getActualUserID(),
            self::TABLE_FLD.'_user_name' => $username,
            self::TABLE_FLD.'_user_cmpcode' => $this->getCmpCode(),
            self::TABLE_FLD.'_cmp_code' => $cmp_code,
            self::TABLE_FLD.'_cmp_name' => $cmp_name
        );



        foreach($var as $fld => $v) {
            if($fld == "req_date" ) {
                $v = date("Y-m-d",strtotime($v));
            }
            $insert_data[$fld] =  ASSIST_HELPER::code($v);

        }

        $sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
        $mdb = new ASSIST_DB("master");
        $finteg_id = $mdb->db_insert($sql);

        if ($finteg_id > 0){

            //checking for company id
            $help_db = new ASSIST_DB('Helpdesk','helpdesk');
            $sql_st = "SELECT hdc_id FROM helpdesk_companies WHERE hdc_cc ='".$cmp_code."';";
            $comp_id = $help_db->mysql_fetch_one($sql_st);

            //checking integration type
            $mdb = new ASSIST_DB('master');
            $sql_s = "SELECT type_name FROM integration_type WHERE type_id ='".$insert_data['req_integ_type']."';";
            $type_name = $mdb->mysql_fetch_one($sql_s);

            $helpdesk = array(
                'has_attachments'=>(isset($insert_data['has_attachments'])?$insert_data['has_attachments']:"N/A"),
                'hdrc_company_id'=>(isset($comp_id['hdc_id'])?$comp_id['hdc_id'] : 0),
                'hdr_moduleid'=>implode('_',array(0)),
                'hdr_description'=>"
                Topic: Request Financial Integration 
                For: ".$cmp_name." (".$cmp_code.") 
                Additional notes: ".(!empty($insert_data['req_additional_notes'])? $insert_data['req_additional_notes']:"N/A")." 
                Financial Service Provider (FSP): ".$insert_data['req_fsp']." 
                Type of Integration: ".$type_name['type_name'],

                'hdr_topicid'=> 5
            );

            $help_desk = new HELPDESK_REQUEST();
            $response = $help_desk->addObject($helpdesk);
            if($response[0] == "ok") {

                //adding an HDR to the the assist_new_module_requests table on the record that was just added
                $sql = "UPDATE ".$this->getTableName()." SET req_hdr = '" .strtoupper($this->getCmpCode())."/HDR".$response['object_id']."' WHERE req_id = '".$finteg_id."';";
                $mdb = new ASSIST_DB("master");
                $update_id = $mdb->db_insert($sql);

                //inserting response to all request table
                $insert_dta = array(//set default fields
                    self::TABLE_FLD.'_status' => ADMIN2::ACTIVE,
                    self::TABLE_FLD.'_date' => date("Y-m-d H:i:s"),
                    self::TABLE_FLD.'_user_tkid' => $this->getActualUserID(),
                    self::TABLE_FLD.'_user_cmpcode' => $this->getCmpCode(),
                    self::TABLE_FLD.'_source_cmp_name' => strtoupper($cmp_name),
                    self::TABLE_FLD.'_hdr' => strtoupper($this->getCmpCode())."/HDR".$response['object_id'],
                    self::TABLE_FLD.'_request' => "Financial Integration Request "
                );
                $sql = "INSERT INTO assist_new_all_requests SET " . $this->convertArrayToSQLForSave($insert_dta);
                $mdb = new ASSIST_DB("master");
                $req_id = $mdb->db_insert($sql);
            }
            //INFORMING JANET ABOUT THE REQUEST
            $emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","Financial Integration Request ".strtoupper($this->getCmpCode())."/HDR".$response['object_id'],$helpdesk['hdr_description'],"TEXT","");
            $emailObject->sendEmail();

        }




        if ($response[0] == "ok"){

            $result = array("ok",$response[1]." Any edits or updates must be done through the created ticket on the help desk.",'object_id'=>$finteg_id);
        }else{
            $result = array("error","Error occurred, Please consult the help desk".$response[1],'object_id'=>$finteg_id);
        }


        return $result;

    }
}