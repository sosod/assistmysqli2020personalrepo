<?php
/*******
ADMIN V2
Created by: Sondelani Dumalisile
Created: September 2020
 ***************************/

class ADMIN2_HEADINGS EXTENDS ADMIN2
{




    /********************************
     * CONSTRUCTOR
     */
    public function __construct($modref="") {
        parent::__construct($modref);
    }


    public function getMainObjectHeadings($object ,$fn="DETAILS",$section="",$fld_prefix="",$replace_names=false,$specific_fields = array()) {
        //echo "<P>".$object.":".$fn.":",$section."<p>";


        $sql = "SELECT h_id as id

				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required
				, h_system_glossary as help
				FROM assist_setup_headings
			WHERE (h_status & ".ADMIN2::ACTIVE.") = ".ADMIN2::ACTIVE."
				";//.($this->isIASClient() ? "" : "AND h_field <> 'job_manager' ");
        if(count($specific_fields)>0) {
            $order_by = "h_details_order";
            $sql.=" AND h_field IN ('".implode("','",$specific_fields)."') ";
        } else {
            $sql.="


							".($fn=="FORM" ? "AND h_section NOT LIKE '".strtoupper($object."_UPDATE")."%' AND ( (h_status & ".self::SYSTEM_MANAGED.") <> ".self::SYSTEM_MANAGED.")" : "")."
							".($fn=="LIST" || $fn=="FULL" ? "AND ( (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW." OR  (h_status & ".self::SYSTEM_LIST_VIEW.") = ".self::SYSTEM_LIST_VIEW.")" : "")."
							".($fn=="COMPACT" ? "AND ( (h_status & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW.")" : "")."
							AND h_section LIKE '".($section=="NEW"?strtoupper($object):strtoupper($object)."%")."' ";
            if($fn=="LIST" || $fn == "FULL") {
                $order_by = "h_list_order";
                if($fn=="LIST"){
                    switch($section) {
                        case "NEW":
                            $sql.= " AND ( (h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW.") ";
                            break;
                        case "MANAGE":
                            $sql.= " AND ( (h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE.") ";
                            break;
                        case "ADMIN":
                            $sql.= " AND ( (h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN.") ";
                            break;
                        case "DASHBOARD":
                            $sql.= " AND ( (h_list_display & ".self::DISPLAY_DASHBOARD.") = ".self::DISPLAY_DASHBOARD.") ";
                            break;
                        default:
                            $sql.= " AND ( h_list_display > 0) ";
                            break;
                    }
                }
            } else {
            	$order_by = ($object == "NEW_CLIENT") ? "h_list_order" : "h_details_order";
                if($fn!="FORM") {
                    $sql.= " AND ( (h_section NOT LIKE '%".strtoupper($object."_UPDATE")."%') OR ( h_section LIKE '%".strtoupper($object."_UPDATE")."%' AND (h_list_display > 0 && (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."  ) ) ) ";
                }
            }
        }
        $sql.= " ORDER BY ".$order_by;  //echo $sql;
        $mdb = new ASSIST_DB("master");
        $rows = $mdb->mysql_fetch_all_by_id($sql, ($fn=="REPORT" ? "id" :"field"));
        if($replace_names===true) {
            $rows = $this->replaceObjectNames($rows);
        }
        if($object=="ASSURANCE") {
            $original = $rows;
            $rows = array();
            foreach($original as $fld=>$r){
                $r['field'] = $fld_prefix.$r['field'];
                $rows[$fld_prefix.$fld] = $r;

            }
        }

        if($fn=="LIST" || $fn=="FULL" || $fn == "REPORT") {
            return $rows;
        } else {
            $data = array('rows'=>$rows,'sub'=>array());
            foreach($rows as $fld=>$r){
                if($r['parent_id']>0) {
                    $data['sub'][$r['parent_id']][] = $r;
                    unset($data['rows'][$r['field']]);
                }
            }
            return $data;
        }
    }



}