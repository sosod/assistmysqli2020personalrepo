<?php
/*******
ADMIN V2
Created by: Sondelani Dumalisile
Created: September 2020
 ***************************/


class ADMIN2_DEMO EXTENDS ADMIN2
{
    const TABLE = "demo_requests";
    const TABLE_FLD = "req";
    const OBJECT_NAME = "Demo";
    protected $ref_tag = "D";
    protected $attachment_field = "attachments";
    protected $progress_field = "_progress";
    protected $id_field = "req_id";
    protected $parent_field = "req_user_tkid";
    const OBJECT_TYPE = "DEMO";


    public function __construct($modref="") {
        parent::__construct($modref);

    }
    public function getTableName() { return "assist_new_".self::TABLE;	}
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }




    public function addObject($var,$headings=array()) {
        $result = array("error","Nothing done.");

        unset($var['form_action']);
        /*$demo_id = $var['demo_id'];
        unset($var['demo_id']);
        $user_id = $var['user_id'];
        unset($var['user_id']);*/
        $cmp_name = $var['cmp_name'];
        unset($var['cmp_name']);
        $cmp_code = $var['cmp_code'];
        unset($var['cmp_code']);

        $reseller = $this->getResellerInfo($this->getCmpCode());
        $username = (isset($reseller['cmpcode']) && isset($reseller['default_admin']) && ($reseller['cmpcode'] =="IGN0001")? $reseller['default_admin'] :$this->getActualUserName());
        $original_cmpcode = $cmp_code;
        $first3letters = substr($original_cmpcode, 0, 3); // cutting the string
        $demo_cmpcode = $first3letters;
        $length = strlen($first3letters);
        if ($length == 3){
            $demo_cmpcode.= "999D";
            //checking if demo database exists
            $sql = "SELECT
				*
				FROM assist_company
				WHERE cmpcode = '".strtoupper($demo_cmpcode)."'";
            $mdb = new ASSIST_DB("master");
            $row = $mdb->mysql_fetch_one($sql);
        }
        if (count($row) > 0) {

                $demo['demo_details'] = $row['cmpcode'];
        }else{
            $demo['demo_details'] = array();
        }
        $demo_exists = false;
        if (count($demo['demo_details']) > 0){
            $demo_exists = true;
        }
        $demo_linked = false;
        if ($demo_exists === true){

            $sql = "SELECT
				cd_client_cmpcode
				FROM assist_company_demos
				WHERE cd_demo_cmpcode = '".strtoupper($demo_cmpcode)."' and cd_status = 2";
            $mdb = new ASSIST_DB("master");
            $row_a = $mdb->mysql_fetch_one($sql);
            $linked_client_cmpcode = $row_a['cd_client_cmpcode'];
            if ($linked_client_cmpcode == strtoupper($original_cmpcode)){
                $demo_linked = true;
            }
        }

        if ($demo_linked === false) {
            //set default fields
            $insert_data = array(
                self::TABLE_FLD . '_status' => ADMIN2::ACTIVE,
                self::TABLE_FLD . '_date' => date("Y-m-d H:i:s"),
                self::TABLE_FLD . '_user_tkid' => $this->getActualUserID(),
                self::TABLE_FLD . '_user_name' => $username,
                self::TABLE_FLD . '_user_cmpcode' => $this->getCmpCode(),
                self::TABLE_FLD . '_source_cmpcode' => $cmp_code,
                self::TABLE_FLD . '_source_cmp_name' => $cmp_name,
                self::TABLE_FLD . '_does_demo_exist' => $demo_exists === true? "Yes":"No",
                self::TABLE_FLD . '_is_demo_already_linked' => $demo_linked === true? "Yes":"No"
            );


            foreach ($var as $fld => $v) {
                if ($fld == "req_date") {
                    $v = date("Y-m-d", strtotime($v));
                }
                $insert_data[$fld] = ASSIST_HELPER::code($v);

            }

            $sql = "INSERT INTO " . $this->getTableName() . " SET " . $this->convertArrayToSQLForSave($insert_data);
            $mdb = new ASSIST_DB("master");
            $demo_id = $mdb->db_insert($sql);

            if ($demo_id > 0){

                //checking for company id
                $help_db = new ASSIST_DB('Helpdesk','helpdesk');
                $sql_st = "SELECT hdc_id FROM helpdesk_companies WHERE hdc_cc ='".$cmp_code."';";
                $comp_id = $help_db->mysql_fetch_one($sql_st);

                $helpdesk = array(
                    'has_attachments'=>(isset($insert_data['has_attachments'])?$insert_data['has_attachments']:"N/A"),
                    'hdrc_company_id'=>(isset($comp_id['hdc_id'])?$comp_id['hdc_id'] : 0),
                    'hdr_moduleid'=>implode('_',array(0)),
                    'hdr_description'=>"
                    Topic: Request New Demo / Training Database 
                    For: ".$cmp_name." (".$cmp_code.") 
                    Does demo exist :".($demo_exists === true? 'Yes':'No')." 
                    Is demo already linked :".($demo_linked === true? 'Yes':'No')." 
                    Requested by :".$username." (".$this->getActualUserID().")" ,

                    'hdr_topicid'=> 5
                );

                $help_desk = new HELPDESK_REQUEST();
                $response = $help_desk->addObject($helpdesk);
                if($response[0] == "ok") {

                    //adding an HDR to the the assist_new_module_requests table on the record that was just added
                    $sql = "UPDATE ".$this->getTableName()." SET req_hdr = '" .strtoupper($this->getCmpCode())."/HDR".$response['object_id']."' WHERE req_id = '".$demo_id."';";
                    $mdb = new ASSIST_DB("master");
                    $update_id = $mdb->db_insert($sql);

                    //inserting response to all request table
                    $insert_dta = array(//set default fields
                        self::TABLE_FLD.'_status' => ADMIN2::ACTIVE,
                        self::TABLE_FLD.'_date' => date("Y-m-d H:i:s"),
                        self::TABLE_FLD.'_user_tkid' => $this->getActualUserID(),
                        self::TABLE_FLD.'_user_cmpcode' => $this->getCmpCode(),
                        self::TABLE_FLD.'_source_cmp_name' => strtoupper($cmp_name),
                        self::TABLE_FLD.'_hdr' => strtoupper($this->getCmpCode())."/HDR".$response['object_id'],
                        self::TABLE_FLD.'_request' => "New Demo / Training Database Request "
                    );
                    $sql = "INSERT INTO assist_new_all_requests SET " . $this->convertArrayToSQLForSave($insert_dta);
                    $mdb = new ASSIST_DB("master");
                    $req_id = $mdb->db_insert($sql);
                }
                //INFORMING JANET ABOUT THE REQUEST
            $emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","New Demo / Training Database Request. ".strtoupper($this->getCmpCode())."/HDR".$response['object_id'],$helpdesk['hdr_description'],"TEXT","");
            $emailObject->sendEmail();
            }

            if($response[0] == "ok"){
            $result = array("ok",$response[1]." Any edits or updates must be done through the created ticket on the help desk.",'object_id'=>$demo_id);
            }

        }elseif($demo_linked == true){
            $result = array("ok","Your current company code is already linked to an existing demo database of company code ".strtoupper($linked_client_cmpcode),'object_id'=>$row['cd_id']);
        }else{
            $result = array("error","An unexpected error occurred, please notify the help desk.",'object_id'=>$row['cd_id']);
        }







        return $result;

    }
}