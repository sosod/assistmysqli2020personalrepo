<?php
/*******
ADMIN V2
Created by: Sondelani Dumalisile
Created: September 2020
 ***************************/

class ADMIN2_CLIENT EXTENDS ADMIN2
{
    const TABLE = "client_requests";
    const TABLE_FLD = "req";
    const OBJECT_NAME = "Client";
    protected $ref_tag = "C";
    protected $attachment_field = "attachments";
    protected $progress_field = "_progress";
    protected $id_field = "req_id";
    protected $parent_field = "_admin_tkid";
    const OBJECT_TYPE = "CLIENT";


    public function __construct($modref="") {
        parent::__construct($modref);

    }
    public function getTableName() { return "assist_new_".self::TABLE;	}
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }




    public function getDefaultAdminDetails(){
        $reseller = $this->getResellerInfo($this->getCmpCode());
        $default['req_admin_id'] = $this->getActualUserID();
        $default['req_admin_name'] = (isset($reseller['cmpcode']) && isset($reseller['default_admin']) && ($reseller['cmpcode'] =="IGN0001")? $reseller['default_admin'] :$this->getActualUserName());
	    $default['req_support_provider'] = "Action iT [AIT0001]";
        $default['req_reseller'] = $reseller['site_name'];
        if(strtoupper($this->getCmpCode())=="IASSIST") {
            $default['req_support_email'] = "helpdesk@actionassist.co.za";
        }else {
            $default['req_support_email'] = $reseller['help_email'];
        }
        $default['req_admin_email'] = $default['req_support_email'];
        return $default;
    }



    public function addObject($var,$headings=array()) {
        $result = array("error","Nothing done.");
	    $response = array("error","Nothing done.");
        unset($var['form_action']);
        $client_id = $var['client_id'];
        unset($var['client_id']);
        $admin_id = $var['admin_id'];
        unset($var['admin_id']);


        //set default fields
        $insert_data = array(
            self::TABLE_FLD.'_status' => ADMIN2::ACTIVE,
            self::TABLE_FLD.'_date' => date("Y-m-d H:i:s"),
            self::TABLE_FLD.'_admin_tkid' => $admin_id,
            self::TABLE_FLD.'_admin_cmpcode' => $this->getCmpCode()
        );



        foreach($var as $fld => $v) {
            if($fld == "req_date" ) {
                $v = date("Y-m-d",strtotime($v));
            }elseif ($fld == "req_mod_needed"){
                $v = implode(' ',$v);
            }elseif ($fld == "req_pref_cmp_code"){
	            $v = strtoupper($v)."0001";
            }
            if(is_array($v)){
	            $insert_data[$fld] =  $v;
            }else{
	            $insert_data[$fld] =  ASSIST_HELPER::code($v);
            }


        }

        $sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
        $mdb = new ASSIST_DB("master");
        $client_id = $mdb->db_insert($sql);

        if ($client_id > 0){
            $modules = explode(" ",$insert_data['req_mod_needed']); //debunking the modules selected
	        $insert_data['req_mod_needed'] = implode(" ",$modules);

            //checking for company id
            $help_db = new ASSIST_DB('Helpdesk','helpdesk');
            $sql_st = "SELECT hdc_id FROM helpdesk_companies WHERE hdc_cc ='".$this->getCmpCode()."';";
            $comp_id = $help_db->mysql_fetch_one($sql_st);

            $helpdesk = array(
                'has_attachments'=>(isset($insert_data['has_attachments'])?$insert_data['has_attachments']:"N/A"),
                'hdrc_company_id'=>(isset($comp_id['hdc_id'])?$comp_id['hdc_id'] : 0),
                'hdr_moduleid'=>implode('_',array(0)),
                'hdr_description'=>"
                Topic: New Client Request 
                Preferred Company Code: ".strtoupper($insert_data['req_pref_cmp_code'])." 
                Company Name: ".$insert_data['req_cmp_name']." 
                Admin Name: ".$insert_data['req_admin_name']." 
                Admin Email: ".$insert_data['req_admin_email']."
                Support Email: ".$insert_data['req_support_email']."
                Reseller: ".$insert_data['req_reseller']."
                Support Provider: ".$insert_data['req_support_provider']."
                Is Demo: ".( $insert_data['req_is_demo'] == 1 ?"Yes" : "No")." 
                Include training database: ".( $insert_data['req_inc_training_db'] == 1 ?"Yes" : "No")."  
                (Modules Needed: ".$insert_data['req_mod_needed'].")
                Additional notes: ".(!empty($insert_data['req_additional_notes'])? $insert_data['req_additional_notes']:"N/A"),
                'hdr_topicid'=> 6
            );

            $help_desk = new HELPDESK_REQUEST();
            $response = $help_desk->addObject($helpdesk);
            if($response[0] == "ok") {

                //adding an HDR to the the assist_new_client_requests table on the record that was just added
                $sql = "UPDATE ".$this->getTableName()." SET req_hdr = '" .strtoupper($this->getCmpCode())."/HDR".$response['object_id']."' WHERE req_id = '".$client_id."';";
                $mdb = new ASSIST_DB("master");
                $update_id = $mdb->db_insert($sql);

                //inserting response to all request table
                $insert_dta = array(//set default fields
                    self::TABLE_FLD.'_status' => ADMIN2::ACTIVE,
                    self::TABLE_FLD.'_date' => date("Y-m-d H:i:s"),
                    self::TABLE_FLD.'_user_tkid' => $admin_id,
                    self::TABLE_FLD.'_user_cmpcode' => $this->getCmpCode(),
                    self::TABLE_FLD.'_source_cmp_name' => strtoupper($insert_data['req_pref_cmp_code']),
                    self::TABLE_FLD.'_hdr' => strtoupper($this->getCmpCode())."/HDR".$response['object_id'],
                    self::TABLE_FLD.'_request' => "New client DB Request"
                );
                $sql = "INSERT INTO assist_new_all_requests SET " . $this->convertArrayToSQLForSave($insert_dta);
                $mdb = new ASSIST_DB("master");
                $req_id = $mdb->db_insert($sql);
            }
            //INFORMING JANET ABOUT THE REQUEST
            $emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","New Client DB Request ".strtoupper($this->getCmpCode())."/HDR".$response['object_id'],$helpdesk['hdr_description'],"TEXT","");
            $emailObject->sendEmail();

        }

        if ($response[0] == "ok"){

            $result = array("ok",$response[1]." Any edits or updates must be done through the created ticket on the help desk.",'object_id'=>$client_id,'hdr_id' => $response['object_id']);
        }else{
            $result = array("error","Error occurred, Please consult the help desk".$response[1],'object_id'=>$client_id);
        }




        return $result;

    }

}