<?php

class ADMIN2 extends ADMIN2_HELPER {

    /**
     * Display in New, Manage or Admin list pages
     */
    const DISPLAY_NEW = 128;
    const DISPLAY_MANAGE = 256;
    const DISPLAY_ADMIN = 1024;

    /**
     * Module Wide Constants
     * */
    const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;
    /**
     * Display in frontpage dashboard
     */
    const DISPLAY_DASHBOARD = 512;
    /**
     * Can a heading be displayed in the list view?
     */
    const CAN_LIST_VIEW = 64;
    const SYSTEM_LIST_VIEW = 32;
    /**
     * Is a normal list field
     */
    const IS_STANDARD_LIST = 2048;
    /**
     * System managed field? i.e. Do not display in Add/Edit form (Used to disable Date Completed field which is updated automatically by system on update where status_id = 3)
     */
    const SYSTEM_MANAGED = 4096;
    /**
     * Display in compact/summary view
     */
    const COMPACT_VIEW = 8192;

    /**
     * Display when sending lines to other internal
     */
    const INTERNAL = 16384;

    const TABLE_FLD = "assist";

    private $is_iassist = false;
	private $my_cmpcode = "";

	private $available_companies = array();
	private $available_databases = array();

	private $paired_demo_companies = array();

	public function __construct() {
		parent::__construct();
		$this->my_cmpcode = strtoupper($this->getCmpCode());
		if($this->my_cmpcode=="IASSIST") {
			$this->is_iassist = true;
		}
	}

    public function getStatusFieldName() {
        return $this->status_field;
    }
    public function getProgressStatusFieldName() {
        return $this->progress_status_field;
    }
    public function getProgressFieldName() {
        return $this->progress_field;
    }
    public function getIDFieldName() {
        return $this->id_field;
    }
    public function getParentFieldName() {
        return $this->parent_field;
    }
    public function getUpdateAttachmentFieldName() {
        return $this->update_attachment_field;
    }

	/**
	 * Get list of companies for the logged in reseller
	 */
	public function getAvailableCompaniesFromDB($sort="cmpname") {
		//get companies from table
		$company = array();
		if($this->is_iassist) {
			$my_server = $_SERVER["HTTP_HOST"];
			if($my_server=="pwc.assist.action4u.co.za") {
				$where_server = "cmp_site = '".$my_server."'";
			} else {
				$where_server = "cmp_site <> 'pwc.assist.action4u.co.za'";
			}
			$sql = "SELECT cmpreseller as reseller, cmpcode, cmpname as name, cmp_is_demo as is_demo FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' AND $where_server ORDER BY cmpreseller, ".$sort;

			//$sql = "SELECT cmpreseller as reseller, cmpcode, cmpname as name, cmp_is_demo as is_demo FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' AND cmp_site = '".$_SERVER["HTTP_HOST"]."' ORDER BY cmpreseller, cmpname";
		} else {
			$sql = "SELECT cmpreseller as reseller, cmpcode, cmpname as name, cmp_is_demo as is_demo FROM assist_company WHERE cmpstatus = 'Y' AND cmpreseller = '".$this->my_cmpcode."' AND cmp_site = '".$_SERVER["HTTP_HOST"]."' ORDER BY ".$sort;
		}
		$mdb = new ASSIST_DB("master");
		$company = $mdb->mysql_fetch_all_by_id2($sql,"reseller","cmpcode");

		$db_prefix = $mdb->getDBPrefix();
		unset($mdb);

		//get actual databases
		$this->available_databases = $this->getListOfAvailableDatabases();

		//compare lists
		foreach($company as $res => $res_cmp) {
			foreach($res_cmp as $cc => $cmp) {
				if(!in_array($db_prefix.strtolower($cc),$this->available_databases)) {
					unset($company[$res][$cc]);
				}
			}
		}

		$this->available_companies = $company;
	}

	public function getAvailableCompanies($sort) {
		if(count($this->available_companies)==0) {
			$this->getAvailableCompaniesFromDB($sort);
		}
		return $this->available_companies;
	}

    public function getTableField() { return self::TABLE_FLD;	}











	public function getPairedDemoCompaniesFromDB() {
		//get companies from table
		$company = array();
		$sql = "SELECT cd_client_cmpcode as client_cmpcode, cd_demo_cmpcode as demo_cmpcode FROM assist_company_demos WHERE cd_status = 2";
		$mdb = new ASSIST_DB("master");
		$company = $mdb->mysql_fetch_value_by_id($sql,"client_cmpcode","demo_cmpcode");

		$this->paired_demo_companies = $company;

	}


	public function getPairedDemoCompanies() {
		if(count($this->paired_demo_companies)==0) {
			$this->getPairedDemoCompaniesFromDB();
		}
		return $this->paired_demo_companies;
	}



	/**
	 * Get basic info about companies
	 */
	public function getBasicInfoAboutCompany($cc,$extended=false) {
		$cdb = new ASSIST_DB("client",$cc);
		$details = array(
			'module_count'=>0,
			'active_user_count'=>0,
			'inactive_user_count'=>0
		);

		//Module count
		$sql = "SELECT count(modid) as count FROM assist_menu_modules WHERE modyn = 'Y'";
		$data = $cdb->mysql_fetch_one($sql);
		$details['module_count'] = $data['count'];

		if($extended===true) {
			$sql = "SELECT modref, modtext as mod_name, modlocation as modloc FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
			$details['module_list'] = $cdb->mysql_fetch_all_by_id($sql, "modref");
			$sql = "SELECT date as last_date, tkname as last_user FROM assist_".strtolower($cc)."_timekeep_activity ORDER BY date DESC LIMIT 1";
			$row  = $cdb->mysql_fetch_one($sql);
			$details['last_date'] = $row['last_date'];
			$details['last_user'] = $row['last_user'];
		}

		//User count
		$sql = "SELECT tkstatus, count(tkid) as count FROM assist_".strtolower($cc)."_timekeep GROUP BY tkstatus";
		$data = $cdb->mysql_fetch_value_by_id($sql,"tkstatus","count");
		$details['active_user_count'] = isset($data['1']) ? $data['1'] : 0;
		$details['inactive_user_count'] = isset($data['2']) ? $data['2'] : 0;

		unset($cdb);

		return $details;
	}

    public function getResellerInfo($cc) {
        $sql = "SELECT * FROM assist_reseller WHERE cmpcode = '$cc'";
        $mdb = new ASSIST_DB("master");
        return $mdb->mysql_fetch_one($sql);
    }


	public function getCompanyInfo($cc) {
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '$cc'";
		$mdb = new ASSIST_DB("master");
		$company = $mdb->mysql_fetch_one($sql);
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '".$company['cmpreseller']."'";
		$company['reseller'] = $mdb->mysql_fetch_one($sql);
		$company['details'] = $this->getBasicInfoAboutCompany($cc,true);
		$sql = "SELECT
				cd_demo_cmpcode
				, cd_last_copied
				, cd_copy_user
				, cd_copy_cmpcode
				, cmpname as cd_copy_cmpname
				FROM assist_company_demos
				LEFT OUTER JOIN assist_company
				  ON cd_copy_cmpcode = cmpcode
				WHERE cd_client_cmpcode = '$cc' and cd_status = 2";
		$row = $mdb->mysql_fetch_one($sql);
		$company['paired_demo'] = $row['cd_demo_cmpcode'];
		if(strlen($company['paired_demo'])>0) {
			$pd = strtolower($company['paired_demo']);
			$cdb = new ASSIST_DB("client",$pd);
			$sql = "SELECT date as last_date, tkname as last_user FROM assist_".$pd."_timekeep_activity ORDER BY date DESC LIMIT 1";
			$company['demo_details'] = $cdb->mysql_fetch_one($sql);
			$company['demo_details']['last_copied'] = $row['cd_last_copied'];
			$company['demo_details']['last_copy_user'] = $row['cd_copy_user'];
			$company['demo_details']['last_copy_cmpname'] = $row['cd_copy_cmpname'];
		}
        $sql = "SELECT
				set_id
				, integration_supplier.supp_name as supplier
				, integration_type.type_name as type
				, set_status as status
				FROM integration_settings
				LEFT OUTER JOIN integration_supplier
				  ON set_supplierid = supp_id 
				LEFT OUTER JOIN integration_type
				  ON set_typeid = type_id
				  
				WHERE set_cmpcode = '$cc'";
        $company['finteg'] = $mdb->mysql_fetch_one($sql);


		return $company;

	}







	public function getCompanyInfoForDemoUpdate($cc) {
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '$cc'";
		$mdb = new ASSIST_DB("master");
		$company = $mdb->mysql_fetch_one($sql);

		$sql = "SELECT
				cd_demo_cmpcode
				, cd_last_copied
				, cd_copy_user
				, cd_copy_cmpcode
				, cmpname as cd_copy_cmpname
				FROM assist_company_demos
				LEFT OUTER JOIN assist_company
				  ON cd_copy_cmpcode = cmpcode
				WHERE cd_client_cmpcode = '$cc' and cd_status = 2";
		$row = $mdb->mysql_fetch_one($sql);
		$company['paired_demo'] = $row['cd_demo_cmpcode'];

		$company['copy_user'] = $this->getActualUserName();
		$company['copy_tkid'] = $this->getActualUserID();
		if(strtoupper($this->getCmpCode())=="IASSIST") {
			$company['copy_email'] = "helpdesk@actionassist.co.za";
		} else {
			$company['copy_email'] = $this->getUserEmail($this->getActualUserID());
		}



		return $company;
	}



	public function updatePairedDemo($cc,$company) {
		$mdb = new ASSIST_DB("master");
		$sql = "UPDATE assist_company_demos SET cd_last_copied = now(), cd_copy_cmpcode = '".strtoupper($this->getCmpCode())."', cd_copy_user = '".$company['copy_user']."', cd_copy_tkid = '".$company['copy_tkid']."' WHERE cd_client_cmpcode = '".strtoupper($cc)."' AND cd_demo_cmpcode = '".$company['paired_demo']."'";
		$mdb->db_update($sql);
	}

    public function suspendDemoDb($var) {
        $mdb = new ASSIST_DB("master");
        $status = isset($var['status']) ? $var['status'] : 'D';
        $sql = "UPDATE assist_company SET cmpstatus = '".$status."' WHERE cmpcode = '".strtoupper($var['cmpcode'])."';";
        $response = $mdb->db_update($sql);

        if ($response > 0){
            $result = array("ok","Update Successful.",'object_id'=>$response);
        }else{
            $result = array("error","An error occurred, Please consult the help desk.",'object_id'=>$response);
        }
        return $result;
    }
    public function checkSuspensionStatus($cc) {
        $sql = "SELECT cmpstatus FROM assist_company WHERE cmpcode = '$cc'";
        $mdb = new ASSIST_DB("master");
        $result = $mdb->mysql_fetch_one($sql);
        return $result;
    }


    public function getRequestStatus(){
        //$requests = array();
        $mdb = new ASSIST_DB("master");

        $sql = "SELECT * FROM assist_new_all_requests WHERE req_user_tkid = '".$this->getUserID()."' AND req_user_cmpcode ='".$this->getCmpCode()."' AND req_status <> '".ADMIN2::DELETED."';";
        $requests = $mdb->mysql_fetch_all_by_id($sql,"req_id");

            //pagination
        // Get the total number of records from our table "students".
        $sql = "SELECT * FROM assist_new_all_requests WHERE req_user_tkid = '".$this->getUserID()."' AND req_user_cmpcode ='".$this->getCmpCode()."' AND req_status <> '".ADMIN2::DELETED."';";
        $requests['total_pages'] = $mdb->db_get_num_rows($sql);
        return $requests;

    }




	public function __destruct() {
		parent::__destruct();
	}

}



?>