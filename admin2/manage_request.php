<?php
	//error_reporting(-1);

	include("../module/autoloader.php");

	$me = new ADMIN2();
	$requests = $me->getRequestStatus();
	echo "
<style>
table {
				border-collapse: collapse;
				width: 800px;
			}
			td, th {
				padding: 10px;
			}
			th {
				background-color: #54585d;
				color: #ffffff;
				font-weight: bold;
				font-size: 13px;
				border: 1px solid #54585d;
			}
			td {
				color: #636363;
				border: 1px solid #dddfe1;
			}
			tr {
				background-color: #f9fafb;
			}
			tr:nth-child(odd) {
				background-color: #ffffff;
			}
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  background-color: #f4f4f4;
 margin: 1px;
}

.pagination a.active {
  background-color: #c0c0c0;
  color: white;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
	</style>
";


	$scripts = array("admin2helper.js");
	$styles = array("/assist_jquery.css","/assist3.css");
	ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);

	// Check if the page number is specified and check if it's a number, if not return the default page number which is 1.
	$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;

	// Number of results to show on each page.
	$num_results_on_page = 20;
	$mdb = new ASSIST_DB("master");
	$calc_page = ($page - 1) * $num_results_on_page;
	echo "<h1>Assist Client Admin > Request History</h1>";
    $result = $mdb->mysql_fetch_all("SELECT * FROM assist_new_all_requests WHERE req_user_tkid = '".$me->getUserID()."' AND req_user_cmpcode ='".$me->getCmpCode()."' AND req_status <> '".ADMIN2::DELETED."' LIMIT $calc_page, $num_results_on_page");
	if (count($result) >0) {
// Calculate the page to get the results we need from our table.

		$total_pages = $requests['total_pages'];
		if (ceil($total_pages / $num_results_on_page) > 0){ ?>

			<div class="pagination">
				<?php if ($page > 1): ?>
					<a href="manage_request.php?page=<?php echo $page-1 ?>">Prev</a>
				<?php endif; ?>

				<?php if ($page > 3): ?>
					<a href="manage_request.php?page=1">1</a>

				<?php endif; ?>

				<?php if ($page-2 > 0): ?><a href="manage_request.php?page=<?php echo $page-2 ?>"><?php echo $page-2 ?></a><?php endif; ?>
				<?php if ($page-1 > 0): ?><a href="manage_request.php?page=<?php echo $page-1 ?>"><?php echo $page-1 ?></a><?php endif; ?>

				<a href="manage_request.php?page=<?php echo $page ?>"><?php echo $page ?></a>

				<?php if ($page+1 < ceil($total_pages / $num_results_on_page)+1): ?><a href="manage_request.php?page=<?php echo $page+1 ?>"><?php echo $page+1 ?></a><?php endif; ?>
				<?php if ($page+2 < ceil($total_pages / $num_results_on_page)+1): ?><a href="manage_request.php?page=<?php echo $page+2 ?>"><?php echo $page+2 ?></a><?php endif; ?>

				<?php if ($page < ceil($total_pages / $num_results_on_page)-2): ?>

					<a href="manage_request.php?page=<?php echo ceil($total_pages / $num_results_on_page) ?>"><?php echo ceil($total_pages / $num_results_on_page) ?></a>
				<?php endif; ?>

				<?php if ($page < ceil($total_pages / $num_results_on_page)): ?>
					<a href="manage_request.php?page=<?php echo $page+1 ?>">Next</a>
				<?php endif; ?>
			</div>

		<?php } ?>
		<?php

	}
?>

<table id=tbl_requests class='list'>

	<tr>
		<th>Ref</th>
		<th>Date</th>
		<th>Company Name</th>
		<th>Topic</th>
	</tr>

	<?php
		if (empty($result)){
			echo "<tr><td colspan='5'>No requests found to display.</td></tr>";
		}else{
			//while ($row = mysqli_fetch_array($result)){
			foreach ($result as $row){
				echo "<tr>
        <td>".$row['req_hdr']."</td>
        <td>".$row['req_date']."</td>
        <td>".$row['req_source_cmp_name']."</td>
        <td>".$row['req_request']."</td>
    </tr>";
			}

		}
	?>


</table>
<?php
	ASSIST_HELPER::goBack("main.php");
?>
<script>
    $("#tbl_requests tr").find("th").css({"min-width": "150px"});

    $(".pagination a").button({
        //icons:{primary:"ui-icon-plus"}
    }).addClass("ui-state-default").css({"margin": "1px", "padding": "1px 1px"});

	var tblwidth = $("#tbl_requests").width();
    $(".pagination").css({"margin": "5px 0px", "padding": "3px 0px", "width": tblwidth, "border": "1px solid #ababab"});

</script>
