<?php

//error_reporting(-1);

include("../module/autoloader.php");

$me = new ADMIN2();
$new_string = "";
$value = "";
$scripts = array();
$styles = array("/assist_jquery.css","/assist3.css");

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);



?>
<h1>Assist Client Admin</h1>
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$cc = $_REQUEST['cc'];

$company = $me->getCompanyInfo($cc);



?>
<div id=div_full_container >
	<div id=div_right class='float blue-border-size div-container'>
		<h2>Stats</h2>
		<table id=tbl_stats class=tbl-stats>
			<tr>
				<td>Module Count:</td>
				<td><?php echo $company['details']['module_count']; ?></td>
			</tr>
			<tr>
				<td>Active Users:</td>
				<td><?php echo $company['details']['active_user_count']; ?></td>
			</tr>
			<tr>
				<td>Non-System Employees:</td>
				<td><?php echo $company['details']['inactive_user_count']; ?></td>
			</tr>
		</table>
		<table class=tbl-stats>
			<tr class=last-row>
				<td colspan=2><span class=b>Last Accessed:&nbsp;</span> <?php echo date("d M Y H:i",strtotime($company['details']['last_date']))." by ".$company['details']['last_user']; ?></td>
			</tr>

		</table>
		<h2>Paired Demo Database</h2>
		<?php
		if(strlen($company['paired_demo'])==0) {
			if($company['cmp_is_demo']==true) {
				echo "<p>A paired demo database is not available for this database as it is itself a demo database.  Demo databases can only be paired with client databases.</p>";
			} else {
				echo "<p>A paired demo/training database is not available for this client. ". ($_SESSION['cc'] !=="iassist"? "<span class='float'><button id='btn_new_demo'>Request Demo/Training Database</button></span>":"")."</p>";
			}

		} else {
		    $demoDbSuspensionStatus = $me->checkSuspensionStatus($cc);
		    $value = (isset($demoDbSuspensionStatus['cmpstatus']) && $demoDbSuspensionStatus['cmpstatus'] == "D" ? "Restore Demo Database" :"Suspend Demo Database");
		?>
		<table id=tbl_demo>
			<tr>
				<td>Company Code:</td>
				<td><?php echo $company['paired_demo']; ?></td>
			</tr>
			<tr>
				<td>Last Copied:</td>
				<td><?php
				if(strlen($company['demo_details']['last_copied'])==0 || $company['demo_details']['last_copied']=="0000-00-00 00:00:00" || strtotime($company['demo_details']['last_copied'])==0) {
					echo "Not available";
				} else {
					echo date("d M Y H:i",strtotime($company['demo_details']['last_copied']))." by ".$company['demo_details']['last_copy_user']." of ".$company['demo_details']['last_copy_cmpname'];
				}
				?></td>
			</tr>
			<tr>
				<td>Last Accessed:</td>
				<td><?php echo date("d M Y H:i",strtotime($company['demo_details']['last_date']))." by ".$company['demo_details']['last_user']; ?></td>
			</tr>
			<tr>
				<td></td>
				<td><button id=btn_update_demo>Update Demo Database</button>***<button id=btn_suspend_demo><?php echo $value; ?></button></td>
			</tr>
		</table>
		<?php
			ASSIST_HELPER::displayResult(array("error","<span class=b>***Warning</span>: The process can take a long time and uses significant system resources.  Please try not to update demo database during normal working hours. Also, the update button will be disabled between 6pm-8pm every day and 1pm-2pm on Sundays to ensure that a copy process is not running when scheduled maintenance and backups begin."));
			ASSIST_HELPER::displayResult(array("info","Note: Updating the paired demo database will:<ul class=black><li>Over-write the existing demo database with a copy of the current client database;</li><li>Reset all demo database user passwords to Assist123;</li><li>Reset all demo database user emails to the email address of the currently logged in user (i.e. YOU);</li></ul><p>Updating the Demo Database will NOT copy the existing attachments / files / POEs associated to the client database to the demo database.  Any attachment links will no longer work.</p>
            <p>Suspending the Demo Database will prevent users from accidentally logging in outside of training sessions. If a user tries to login, a message will display alerting them that they are attempting to access a Demo Database which is only for demo / training purposes. The Support user is not impacted by the suspension so that the Demo Database can be used for testing purposes if required.</p>
            <p>Remember to Restore the Demo Database before it is needed for a demo or training sessions.</p>"));


		}
		?>
	</div>
	<div id=div_left class='div-container blue-border-size'>
		<h2>Company Details: <?php echo $company['cmpname']; ?></h2>
		<table id=tbl_company>
			<tr>
				<th>Code:</th>
				<td><?php echo $company['cmpcode']; ?></td>
			</tr>
			<tr>
				<th>Name:</th>
				<td><?php echo $company['cmpname']; ?></td>
			</tr>
			<tr>
				<th>Type:</th>
				<td><?php echo $company['cmp_is_demo']==true?"Demo":"Client"; ?> Database</td>
			</tr>
			<tr>
				<th>Reseller:</th>
				<td><?php echo $company['reseller']['cmpname']; ?></td>
			</tr>
			<tr>
				<th>Admin:</th>
				<td><?php echo $company['cmpadmin']." (".$company['cmpadminemail'].")"; ?></td>
			</tr>
			<tr>
				<th>Logo:</th>
				<td><?php
				echo "<img src='/pics/logos/".$company['cmplogo']."' />";
				?></td>
			</tr>
		</table>
        <?php echo ( $_SESSION['cc'] !=="iassist"? "<span class='float'><button id='btn_new_module'>Request New Module</button></span>" :"");?>
		<h2>Modules</h2>
		<table id=tbl_modules>
			<tr>
				<th>Module Ref</th>
				<th>Module Version</th>
				<th>Module Name</th>
			</tr>
			<?php
			$financial_integration_available = false;
			foreach($company['details']['module_list'] as $modref => $mod) {
				$mod_name = $mod['mod_name'];
				$modloc = $mod['modloc'];
				if($me->isFintegModule($modloc)) {   //Find isFintegModule
					$financial_integration_available = true;
				}
				?>
			<tr>
				<td><?php echo $modref; ?></td>
				<td><?php echo $modloc; ?></td>
				<td><?php echo $mod_name; ?></td>
			</tr>
			<?php } ?>
		</table>
		<h2>Financial Integration</h2>
		<?php
		if($financial_integration_available!==true) {
			echo "<P>No Financial Integration is available for this client as the required module (SDBP6) is not active.</P>";
		} else {
			//finteg not an option for demo databases
			if($company['cmp_is_demo']==true) {
				echo "<P>No Financial Integration is available for this client as it is a demo client.</P>";
			} else {
			//1. check if finteg is active for this client [finteg_status] & get details - look in *_ignite4u.integration_settings where set_cmpcode = $cc [finteg_settings] - if no record OR set_status != 2 then finteg_status = Inactive else = Active
			//2. if not, offer option to activate [can_request_finteg]

                    ?>
                    <table id=tbl_finteg>
                        <?php
                        if (!isset($company['finteg']) || $company['finteg']['status'] != ADMIN2::ACTIVE)
                        { ?>
                        <tr>
                            <th class="b" style='text-align: left'>Status:</th>
                            <td>Inactive</td>
                        </tr>
                            <?php
                            //if can_request_finteg
                            echo($_SESSION['cc'] !== "iassist" ? "<tr><td colspan='2' class='right'><button id='btn_request_finteg'>Request Financial Integration</button></td></tr>" : "");

                        }else {
                    ?>
                                <tr>
                                    <th class="b" style='text-align: left'>Status:</th>
                                    <td>Active</td>
                                </tr>
                                <tr>
                                     <th class="b" style='text-align: left'>Financial Service Provider:</th>
                                    <td><?php echo $company['finteg']['supplier']; ?></td>
                              </tr>
                                 <tr>
                                     <th class="b" style='text-align: left'>Type:</th>
                                    <td><?php echo $company['finteg']['type']; ?></td>
                                </tr>
                        <?php
                        }//endif finteg is active
                        ?>
                    </table>
                    <?php

			}//endif demo database
		}//endif financial_integration_available

        //preparing confirmation pop up message for demo suspension

        $new_string = str_replace( "Demo", "the Demo", $value);
		?>

	</div>
</div>

<style type=text/css>
.div-container {
	border-radius: 5px;
	padding: 20px;
	width:45%;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type=text/javascript>
$(function() {
	$(".div-container").find("h2:first").css("margin-top","0px");
	$("#tbl_company tr").find("th:first").addClass("b");
    $("#tbl_company tr").find("th:first").css("text-align","left");
    $("#tbl_company tr").find("th:first").css("min-width","100px");
    $("#tbl_finteg tr").find("td:first").css("min-width","190px");
    $("#tbl_finteg tr").find("th:first").css("min-width","190px");
	$("#tbl_demo tr").find("td:first").addClass("b");
	$("#tbl_stats tr:not(.last-row)").find("td:first").addClass("b");
	$("#tbl_stats tr:not(.last-row)").find("td:last").addClass("right");
	$(".tbl-stats").addClass("noborder").find("td").addClass("noborder");


//	$("ul.black").find("li").addClass("black");

	var h = $("#div_left").height();
	if($("#div_right").height()>h) {
		h = $("#div_right").height();
		$("#div_left").height(h);
	} else {
		$("#div_right").height(h);
	}

	$("#btn_update_demo").button({

    }).addClass("ui-state-default").click(function(e) {
		e.preventDefault();
			$("<div />",{id:"dlg_confirm",html:"<p id=p_focus>Are you sure you wish to update the demo database?</p>"}).dialog({
				modal:true,
				buttons:[
					{ text: "Confirm & Continue", click: function() {
						$(this).dialog("close");
						AssistHelper.processing();
						var url = "manage_company_update_demo.php?cc=<?php echo $cc; ?>";
						document.location.href = url;
					}, class: 'ui-button-state-grey'
					, icons: { primary: "ui-icon-trash" } },
					{ text: "Cancel", click: function() {
						$( this ).dialog( "destroy" );
					}, class: 'ui-button-minor-grey'
					, icons: { primary: "ui-icon-closethick" }}
				]
			});
			AssistHelper.hideDialogTitlebar('id','dlg_confirm');
			$("#p_focus").focus();

	});
    $("#btn_suspend_demo").button({

    }).addClass("ui-state-default").click(function(e) {
        e.preventDefault();

        var text ="<p id=p_focus>Are you sure you wish to <?php echo strtolower($new_string); ?>?</p>";
        $("<div />",{id:"dlg_confirm",html:text}).dialog({
            modal:true,
            buttons:[
                { text: "Confirm & Continue", click: function() {
                        $(this).dialog("close");
                        AssistHelper.processing();
                        suspendDemoDb();

                    }, class: 'ui-button-state-grey'
                    , icons: { primary: "ui-icon-trash" } },
                { text: "Cancel", click: function() {
                        $( this ).dialog( "destroy" );
                    }, class: 'ui-button-minor-grey'
                    , icons: { primary: "ui-icon-closethick" }}
            ]
        });
        AssistHelper.hideDialogTitlebar('id','dlg_confirm');
        $("#p_focus").focus();

    });


	$("#btn_new_demo").button({
		icons:{primary:"ui-icon-plus"}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green")
	.click(function(e) {
		e.preventDefault();
		document.location.href = "manage_demo_request.php?source_cmpcode=<?php echo $company['cmpcode']; ?>";
	});
	$("#btn_new_module").button({
		icons:{primary:"ui-icon-plus"}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green")
	.click(function(e) {
		e.preventDefault();
		document.location.href = "manage_module_request.php?source_cmpcode=<?php echo $company['cmpcode']; ?>";
	});
	$("#btn_request_finteg").button({
		icons:{primary:"ui-icon-plus"}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green")
	.click(function(e) {
		e.preventDefault();
		document.location.href = "manage_finteg_request.php?source_cmpcode=<?php echo $company['cmpcode']; ?>";
	});

	<?php
	//disable the update button for 2 hours before maintenance starts so that it isn't running when backups begin
if((date("H")>17 && date("H")<20) || (strtoupper(date("l"))=="SUNDAY" && date("H")>12 && date("H")<18)) {
	?>
	$("#btn_update_demo").prop("disabled",true);
	<?php
}
	?>
});
function suspendDemoDb() {
var cmpcode = "<?php echo $cc; ?>";
    var val = document.getElementById("btn_suspend_demo").innerText;
    if (val == "Suspend Demo Database") {
        var page_action = "ADMIN.suspendDemoDb";
        var dta = "&status=D&cmpcode="+cmpcode;
        var result = AssistHelper.doAjax("inc_controller.php?action=" + page_action + dta,"");

    }else {
        var page_action = "ADMIN.suspendDemoDb";
        var dta = "&status=Y&cmpcode="+cmpcode;
        var result = AssistHelper.doAjax("inc_controller.php?action=" + page_action + dta,"");
    }
    if (result[0] == "ok") {
        var url = "manage_company.php?cc=<?php echo $cc; ?>";
        AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
    } else {
        AssistHelper.finishedProcessing(result[0], result[1]);
    }

}
</script>