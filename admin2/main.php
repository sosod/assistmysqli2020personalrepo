<?php

//error_reporting(-1);

include("../module/autoloader.php");
if (!isset($_REQUEST['sort'])){$_REQUEST['sort'] = "cmpname";}
if ($_REQUEST['sort'] == "cmpname"){$value = "Sort by code";}else{$value = "Sort by name";}
if (!isset($sort)) {
    $sort = $_REQUEST['sort'];
}
$me = new ADMIN2();
$available_companies = $me->getAvailableCompanies($sort);
$paired_demo_companies = $me->getPairedDemoCompanies();


$scripts = array();
$styles = array("/assist_jquery.css","/assist3.css");

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);


//REMINDER: check user access settings - can user access Setup?

?>
<table style="width: 100%;" class="heading_table"><tr><td>
			<h1>Assist Client Admin</h1></td><td>
<?php 
echo ( $_SESSION['cc'] !=="iassist"? "<span class=float><button id=btn_requests>Request History</button></span><span class=float><button id=btn_newclient>Request New Client</button></span>" :"");

if(count($available_companies)==0) {
	$reseller_code = $me->getCmpCode();
	echo "</td></tr></table><P>No client details to display.  Please use the 'Request New Client' form to generate a new client request.</p>";
} else {

		?>
	<span class=float><button id=btn_sort onclick="sortby()"><?php echo $value; ?></button></span>
			</td>
		</tr>
	</table>

	<?php


	foreach($available_companies as $reseller_code => $companies) {
		echo "<table class=tbl-companies width=90%><tr><td>";
			$reseller = $companies[$reseller_code];
			$reseller['details'] = $me->getBasicInfoAboutCompany($reseller_code);
			unset($companies[$reseller_code]);
			echo "
			<div class='div_reseller div_company' style='margin:10px auto;' cc='".$reseller_code."'>
				<h2>".$reseller['name']."</h2>
				<p>Active Modules: ".$reseller['details']['module_count']."</p>
				<p>Active Users: ".$reseller['details']['active_user_count']."</p>
				<p>Non-System Employees: ".$reseller['details']['inactive_user_count']."</p>
			</div>";
		echo "</td></tr><tr><td class=center>";
		foreach($companies as $cc => $cmp) {
			//only display companies that aren't demos paired to client databases
			if(!in_array($cc,$paired_demo_companies)) {
				$cmp['details'] = $me->getBasicInfoAboutCompany($cc);

				echo "
				<div class='div_client div_company' style='display:inline-block; margin:10px' cc=".$cc.">
					<h2>".$cmp['name']."</h2>
					<p>Company Code: ".($cc)."</p>
					<p>Type: ".($cmp['is_demo']==true?" Demo":" Client")." Database</p>
					<p>Active Modules: ".$cmp['details']['module_count']."</p>
					<p>Active Users: ".$cmp['details']['active_user_count']."</p>
					<p>Non-System Employees:".$cmp['details']['inactive_user_count']."</p>
				</div>";
			}
		}
		echo "</td></tr></table>";
	}
}
?>

<style type=text/css>
div.div_reseller {
	border: 1px solid #000099;
	background-color: #ddddff;
}
div.div_company {
	width:250px;
}
div.div_client {

	border: 1px solid #9999ff;
}
table.tbl-companies {
	border-color: #ababab;
	margin: 25px auto;
	margin-bottom: 100px;
	border-collapse: separate;
	border-radius: 10px;
}
table.tbl-companies td {
	border-color: #ffffff;
	padding: 10px;
}
table.heading_table td{
	border-left: hidden;
	border-top: hidden;
	border-bottom:hidden;
	border-right: hidden;
}
</style>
<script type=text/javascript>
$(function() {
	$("#btn_newclient").button({
		icons:{primary:"ui-icon-plus"}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green")
	.click(function(e) {
		e.preventDefault();
		document.location.href = "manage_client_request.php?source_cmpcode=<?php echo $reseller_code;?>";
	});

    $("#btn_requests").button({
		icons:{primary:"ui-icon-newwin"}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green")
        .click(function(e) {
            e.preventDefault();
            document.location.href = "manage_request.php";
        });

	$(".div_company").css({
		"border-radius":"5px",
		"text-align":"center",
		"padding":"10px",
	}).click(function(e) {
		e.preventDefault();
		var cc = $(this).attr("cc");
		document.location.href = "manage_company.php?cc="+cc;
	}).find("h2").css("font-family","Tahoma");

	$(".div_reseller").hover(
		function() {
			$(this).css("cursor","pointer");
			$(this).removeClass("div_reseller").addClass("ui-button-bold-orange").find("h2").addClass("orange");
		},function() {
			$(this).css("cursor","");
			$(this).removeClass("ui-button-bold-orange").addClass("div_reseller").find("h2").removeClass("orange");
		}
	);
	$(".div_client").hover(
		function() {
			$(this).css("cursor","pointer");
			$(this).removeClass("div_client").addClass("ui-button-bold-orange").find("h2").addClass("orange");
		},function() {
			$(this).css("cursor","");
			$(this).removeClass("ui-button-bold-orange").addClass("div_client").find("h2").removeClass("orange");
		}
	);
    $("#btn_sort").button({
        icons:{primary:"ui-icon-shuffle"}
    }).addClass("ui-state-default")
        .click(function (e) {
            e.preventDefault();
        });

});
function sortby() {

    var val = document.getElementById("btn_sort").innerText;
    if (val == "Sort by name") {
        document.location.href = "main.php?sort=cmpname";
    }else
    {
        document.location.href = "main.php?sort=cmpcode";
    }
}
</script>