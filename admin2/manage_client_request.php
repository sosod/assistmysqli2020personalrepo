<?php

//error_reporting(-1);

include("../module/autoloader.php");

$me = new ADMIN2();

unset($_REQUEST['r']);
$scripts = array("admin2helper.js");
$styles = array("/assist_jquery.css","/assist3.css");
$headingObject = new ADMIN2_HEADINGS();
$displayObj = new ADMIN2_DISPLAY();
$listObj = new ADMIN2_LIST();
$clientObject = new ADMIN2_CLIENT();


ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);


function replace($search,$replace,$value){$val = strtolower($value);return str_replace($search,$replace,$val);}

if(!isset($source_cmpcode)) {
    $source_cmpcode = $_REQUEST['source_cmpcode'];
}




//Get headings
$headings = array();
$headings['NEW_CLIENT'] = $headingObject->getMainObjectHeadings("NEW_CLIENT");


if(!isset($is_add_page)) {
    $is_add_page = true;
}
$page_direct = "main.php?";
$resellerID = $clientObject->getDefaultAdminDetails();
$js = "";
?>
<h1>Assist Client Admin > Request New Client</h1>


<div id=div_new_client >

    <form name="frm_edit_new_client" language="jscript" enctype="multipart/form-data" method="post">
        <!--<input type=hidden name="form_action" id="form_action" value='ADD' class='no-check' />-->
        <!--<input type=hidden name="page_direct" id="page_direct" value='dialog' class='no-check' />-->
        <input type=hidden id="client_id" name="client_id" value='0' class='no-check'/>
        <input type=hidden id="admin_id" name="admin_id"  value='<?php echo $resellerID['req_admin_id']; ?>' class='no-check' />
    <table id="tbl_client_view" class=form>
    <?php
    $section = "NEW_CLIENT";
    $client_head = $headings[$section]['rows']['client_heading'];
    $client_headings = $headings[$section]['sub'][$client_head['id']];

    foreach($client_headings as $h_id => $head) {
        $name = $head['name'];
        if ($name != "") {
            echo "
			<tr id=tr_edit_" . $head['field'] . ">
				<th style='text-align: left'>". $head['name'] . ($head['required'] == true ? "*" : "") . ":</th>
				<td>";
			$tooltip = !empty($head['help']) ? "<span class='ui-icon ui-icon-info float' id='info-text' title='".$head['help']."'></span>" : "";
			echo $tooltip;
            $js .= $displayObj->drawField("CLIENT",$head['field'],$head);
            echo "</td>
			</tr>";
        }
    }
    ?>
    <tr>
        <th></th><td colspan=3 class='td-buttons right'><button id='btn_save_client' class='btn_save'>Save</td>
    </tr>



    </table>
    </form>
</div>
<?php
ASSIST_HELPER::goBack();
?>
<script>
    <?php echo $js; ?>
    window.onload = init;
    function init(){
        AssistHelper.processing();
        var dta = "client_id=0";
        var load = AssistHelper.doAjax("inc_controller.php?action=CLIENT.getDefaultAdminDetails", dta);
	    $('<option>', {value:"PERFSUITE", text:'Performance Suite'}).prependTo("#req_mod_needed"); //adding performance suite first to the list
        $("#div_new_client").find("input, select, textarea").not(".no-check").each(function () {
        var i = $(this).prop("id");
	        if(typeof(load[i])!=="undefined") {
		        $(this).val(load[i]);
	        }
        });
        $("#req_pref_cmp_code").attr("onkeyup","this.value=this.value.replace(/[^a-zA-Z]/g,'');"); //only allows letters
	    $("#req_pref_cmp_code").attr("placeholder","e.g ABC").attr('size',5).attr('maxlength',3);
	    $("#req_is_demo_no").trigger("click");
	    $("#req_inc_training_db_yes").trigger("click");
	    AssistHelper.closeProcessing();
    }
	$("#tbl_client_view #info-text").css({"cursor":"pointer","background-image": "url(/library/jquery/css/images/ui-icons_888888_256x240.png)"});

    $("#btn_save_client").button({
        icons: {primary: "ui-icon-disk"}
    }).click(function (e) {
        e.preventDefault();
        AssistHelper.processing();
        var $form = $("form[name=frm_edit_new_client]");
        //var $form = $("form[name=frm_edit_doc]");

        var err = false;
        $form.find("select").each(function () {
            //console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
            $(this).removeClass("required");
            if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
                if (this.selectedIndex < 0 ||$(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0 || $(this).val() == "SELECT") {
                    err = true;
                    $(this).addClass("required");
                }
                //Multiselect
                var options = $('#req_mod_needed option:selected');
                if(options.length == 0 || options.selectedIndex == " "){
                    err = true;
                    $(this).addClass("required");
                }

            }
        });

        $form.find("input:text, textarea").each(function () {
            //console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
            $(this).removeClass("required");
            if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
                if ($(this).val() == "" || $(this).val().length == 0) {
                    err = true;
                    $(this).addClass("required");
                }
            }
        });
		$form.find("input:file").each(function () {
			$(this).removeClass("required");
			if ($(this).val() == "" || $(this).val().length == 0) {
				err = true;
				$(this).addClass("required");
			}
		});
        if (err) {
            AssistHelper.finishedProcessing("error", "Please complete the required fields highlighted in red.");
        } else {
	        ADMIN2Helper.processObjectFormWithAttachment($form);
        }


    }).removeClass("ui-state-default").addClass("ui-button-bold-green");

    function dialogFinished(icon, result) {
	    if(typeof(icon)!=="undefined") {
		    if (icon == "ok") {
			    AssistHelper.processing();
			    var url = "<?php echo $page_direct; ?>";
			    AssistHelper.finishedProcessingWithRedirect(icon, result, url);

		    } else {
			    AssistHelper.processing();
			    AssistHelper.finishedProcessing(icon, result);
		    }
	    }
    }


    //Process:
    /*user captures form
    copy is saved to assist_new_client_requests (new table) in the _ignite4u database & email is sent to JC with info above.
        a new helpdesk request is raised on the user's behalf with the topic set to new client database & description populated with the details (HDR is to be sent to IASSIST); normal HDR emails are sent etc
    response back to user is to include the HDR ref and user is redirected back to the main page*/
</script>

