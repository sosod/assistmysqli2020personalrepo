<?php

//error_reporting(-1);

include("../module/autoloader.php");
$me = new ADMIN2();


$scripts = array("admin2helper.js");
$styles = array("/assist_jquery.css","/assist3.css");
$headingObject = new ADMIN2_HEADINGS();
$displayObj = new ADMIN2_DISPLAY();
$listObj = new ADMIN2_LIST();
$clientObject = new ADMIN2_CLIENT();

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts,$styles);


$source_cmpcode = $_REQUEST['source_cmpcode'];
$company = $me->getCompanyInfo($source_cmpcode);

$full_client_details = array();
//$full_client_details = $empObject->getRawClientDetailsByID($object_id,false);
//Get headings
$headings = array();
$headings['NEW_FINTEG'] = $headingObject->getMainObjectHeadings("NEW_FINTEG");
$resellerID = $clientObject->getDefaultAdminDetails();

if(!isset($is_add_page)) {
    $is_add_page = true;
}
IF(!isset($save_action)){
    $save_action = "ADD";
}
$page_redirect = "manage_company.php?cc=".$source_cmpcode."&";
$js = "";
?>

<h1>Assist Client Admin > Request Financial Integration</h1>

<div id=div_new_finteg >

    <form name=frm_edit_new_finteg language="jscript" enctype="multipart/form-data" method="post">
        <input type=hidden name="form_action" id="form_action" value='ADD' class='no-check' />
        <input type=hidden name="page_direct" id="page_direct" value='<?php echo $page_redirect; ?>' class='no-check' />
        <input type=hidden name="cmp_name" id="cmp_name" value='<?php echo $company['cmpname']; ?>' class='no-check' />
        <input type=hidden name="cmp_code" id="cmp_code" value='<?php echo $company['cmpcode']; ?>' class='no-check' />
        <table class=form>
            <?php
            $section = "NEW_FINTEG";
            $module_head = $headings[$section]['rows']['finteg_heading'];
            $module_headings = $headings[$section]['sub'][$module_head['id']];

            foreach($module_headings as $h_id => $head) {
                $name = $head['name'];
                if ($name != " ") {
                    echo "
			<tr id=tr_edit_" . $head['field'] . ">
				<th style='text-align: left'>" . $head['name'] . ($head['required'] == true ? "*" : "") . ":</th>
				<td>";

                    $js .= $displayObj->drawField("FINTEG",$head['field'],$head);
                    echo "</td>
			</tr>
		
			";
                } }
            ?>
            <tr>
                <th></th><td colspan=3 class='td-buttons right'><button id=btn_save_finteg class=btn_save>Save</td>
            </tr>



        </table>
    </form>
</div>
<?php
ASSIST_HELPER::goBack();
?>
<script>
    <?php echo $js; ?>

    $("#btn_save_finteg").button({
        icons: {primary: "ui-icon-disk"}
    }).click(function (e) {
        e.preventDefault();
        AssistHelper.processing();
        var $form = $("form[name=frm_edit_new_finteg]");


        var err = false;
        $form.find("select").each(function () {
            //console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
            $(this).removeClass("required");
            if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
                if (this.selectedIndex == 0 ||$(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0 || $(this).val() == ""|| $(this).val() == "SELECT" ) {
                    err = true;
                    $(this).addClass("required");
                }
            }
        });
        $form.find("input:text, textarea").each(function () {
            //console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
            $(this).removeClass("required");
            if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
                if ($(this).val() == "" || $(this).val().length == 0) {
                    err = true;
                    $(this).addClass("required");
                }
            }
        });

        if (err) {
            AssistHelper.finishedProcessing("error", "Please complete the required fields highlighted in red.");
        } else {


            var page_action = "<?php echo "FINTEG." . $save_action; ?>";

            var dta = AssistForm.serialize($form);
            var result = AssistHelper.doAjax("inc_controller.php?action=" + page_action, dta);
            //console.log(AssistForm.serialize($form));
            //var result = AssistHelper.processObjectFormWithAttachment("inc_controller.php?action=DOCUMENT." +$("#div_doc #form_action").val(),dta);
            if (result[0] == "ok") {
                var url = "<?php echo $page_redirect; ?>";
                AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
            } else {
                AssistHelper.finishedProcessing(result[0], result[1]);
            }
        }
        //DEV PURPOSES ONLY - hide on live
        //$form.prop("action","form_process.php");
        //$form.prop("method","post");
        //$form.submit();

    }).removeClass("ui-state-default").addClass("ui-button-bold-green");

    function dialogFinished(icon,message) {
        AssistHelper.processing();
        var page_direct = "<?php echo $page_redirect; ?>";
        AssistHelper.finishedProcessingWithRedirect(icon,message,page_direct);
    }

</script>

