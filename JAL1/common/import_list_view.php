<?php
require_once("../../module/autoloader.php");
$today = time();
$me = new JAL1_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$page_type = isset($_REQUEST['page']) ? $_REQUEST['page'] : "onscreen";

$table = $_REQUEST['t'];
$name = $_REQUEST['n'];
$type = $_REQUEST['y'];
$extra_info = "";
	switch($type) {
		case "MULTILIST":
		case "LIST":
			$processObject = new JAL1_LIST($table);
			break;
		case "MULTIOBJECT":
		case "OBJECT":
			if(strpos($table,"|")!==false) {
				$lon = explode("|",$table);
				$table = $lon[0];
				$extra_info = $lon[1];
			}
			if(strpos($table,"JAL1")===false) {
				$table = "JAL1_".$table;
			}
			$processObject = new $table();
			break;
	}
	$valid_items = $processObject->getActiveListItemsFormattedForSelect($extra_info);


if($page_type=="onscreen"){
	ASSIST_HELPER::echoPageHeader();
	echo "<h1>".$name."</h1>";
	if(count($valid_items)>0) {
		echo "<table>";
		foreach($valid_items as $i => $item) {
			echo "<tr><td>$item</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<p>No valid list items available.</p>";
	}
} else {
	$fdata = "\"".$name."\"\r\n\"\"\r\n";
	foreach($valid_items as $i => $item) {
		$fdata.= "\"".ASSIST_HELPER::decode($item)."\"\r\n";
	}

	//WRITE DATA TO FILE
	$filename = "../../files/".$cmpcode."/".$modref."_".strtolower($table)."_list_".date("Ymd_Hi",$today).".csv";
	$newfilename = strtolower($table)."_list_".date("YmdHis",$today).".csv";
	$file = fopen($filename,"w");
	fwrite($file,$fdata."\n");
	fclose($file);
	//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
	header('Content-type: text/plain');
	header('Content-Disposition: attachment; filename="'.$newfilename.'"');
	readfile($filename);






}
?>