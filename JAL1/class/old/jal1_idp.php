<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class JAL1_IDP extends IDP1 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = false;
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "IDP";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "IDP"; 
    const OBJECT_NAME = "idp"; 
	const PARENT_OBJECT_TYPE = false;
     
    const TABLE = "idp";
    const TABLE_FLD = "idp";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
	
	/**
	 * Status Constants
	 */
    const CONFIRMED = 32;
	const ACTIVATED = 64;
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		//$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
		unset($var['object_id']); //remove incorrect field value from add form
		/*foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}*/
		$var[$this->getTableField().'_status'] = IDP1::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		//remove FORECAST YEARs & prep for adding later
		$financial_year = $var['idp_finyear_id'];
		$forecast_years = $var['iy_finyear_id'];
		unset($var['iy_finyear_id']);
		
		//[JC] removed - not sure if required... do we need to force more than the primary financial year?
		//Validate that forecast years have been set
		/*$all_years = array($financial_year);
		$add = false;
		$id = 0;
		foreach($forecast_years as $fy) {
			if(!in_array($fy,$all_years)) {
				$all_years[] = $fy;
			}
		}
		if(count($all_years)>0) {
			$add = true;
		}*/
		

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			//Add FORECAST YEARS
			$iyObject = new JAL1_IDPYEARS();
			//Add primary financial year
			$order = 0;
			$iyObject->addObjectById($id,$financial_year,$order);
			$order++;
			$done_years = array($financial_year);
			//add any additional forecast years that aren't the financial year
			foreach($forecast_years as $fy) {
				if($this->checkIntRef($fy) && !in_array($fy,$done_years)) {
					$iyObject->addObjectByID($id,$fy,$order);
					$order++;
				}
			}
			unset($iyObject);
			
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> JAL1_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
			);
				//'log_var'=>$log_var
			return $result;
		}
		return array("error","Testing: ".$sql);
	}






	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach,$recipients,$noter,$include);
		return $result;
	}




	public function confirmObject($var) {
		//$result = array("error","Confirmation functionality pending");
		
		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE + self::CONFIRMED;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$obj_id." confirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> JAL1_LOG::CONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully confirmed.",
				'object_id'=>$obj_id,
			);
		
		
		return $result;
	}



	public function undoConfirmObject($var) {
//		$result = array("error","Confirmation functionality pending");
		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$obj_id." unconfirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> JAL1_LOG::UNDOCONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully unconfirmed.",
				'object_id'=>$obj_id,
			);
		/*
		*/
		
		return $result;
	}


	public function activateObject($var) {
		//$result = array("error","Confirmation functionality pending");
		
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status + self::ACTIVATED;
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$obj_id." activated.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> JAL1_LOG::ACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully activated.",
				'object_id'=>$obj_id,
			);
		
		
		return $result;
	}	
	

	public function undoActivateObject($var) {
		//$result = array("error","Confirmation functionality pending");
		
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status - self::ACTIVATED;
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$obj_id." activation reversed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> JAL1_LOG::UNDOACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"Reversal of prior activation of ".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." successfull.",
				'object_id'=>$obj_id,
			);
		
		
		return $result;
	}	
    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		//get headings
		$headingObject = new JAL1_HEADINGS();
		$headings = $headingObject->getMainObjectHeadings($this->getMyObjectType(),"DETAILS","NEW");
		//get raw idp record from database
		$raw = $this->getRawObject($id);
		$raw[$this->getIDFieldName()] = $this->getRefTag().$raw[$this->getIDFieldName()];
		//get financial years
		$finyearObject = new JAL1_MASTER("financial_year");
		$finyears = $finyearObject->getAllItemsFormattedForSelect();
		//replace all financial years with text
		$finyear_id = $raw[$this->getTableField().'_finyear_id'];
		$raw[$this->getTableField().'_finyear_id'] = $finyears[$raw[$this->getTableField().'_finyear_id']];
		//get raw idp forecast years from database
		$iyObject = new JAL1_IDPYEARS();
		$iy_years = $iyObject->getYearsForSpecificIDP($id);
		unset($iy_years[$finyear_id]);
		$raw['iy_finyear_id'] = implode("<br />",$iy_years);
		
		//format into expected return format
		$data = array(
			'head'=>$headings['rows'],
			'rows'=>$raw,
		);
		//return getAObject
		return $data;
	}

	public function getHeading($id) {
		$row = $this->getRawObject($id);
		return $row[$this->getNameFieldName()].(strlen($row[$this->getTableField()."_ref"])>0 ? " (".$row[$this->getTableField()."_ref"].")":"");
	}

	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	

	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		if($parent_id==0) {
			$parentObject = new JAL1_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}


	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Actions are fully activated & available outside of NEW 
	 */
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW / ACTIVATE / WAITING
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATE",$t,false);
	}
		 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>