<?php

class JAL1_PMS extends ASSIST_MODULE_HELPER {
	protected $result_settings = array(
		0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."),
		1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"R"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% >= Actual/Target < 75%"),
		2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"O"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75% >= Actual/Target < 100%"),
		3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"G"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual/Target = 100%"),
		4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"G2"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100% > Actual/Target < 150%"),
		5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"B"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "Actual/Target >= 150%"),
	);

	protected $sections = array(
		'KPI'	=> array(
			'code'	=>"D",
			'name'	=>"Departmental SDBIP",
			'status'=>1,
			'page'	=>"dept",
			'field'	=>"kpi_",
		),
		'KAS'	=> array(
			'code'	=>"D",
			'name'	=>"Departmental Assurance Review",
			'status'=>1,
			'page'	=>"dept",
			'field'	=>"kas_",
		),
		'TOP'	=> array(
			'code'	=>"TL",
			'name'	=>"Top Layer SDBIP",
			'status'=>1,
			'page'	=>"top",
			'field'	=>"top_",
		),
		'CAPITAL'	=> array(
			'code'	=>"CAP",
			'name'	=>"Capital Projects",
			'status'=>1,
			'page'	=>"capital",
		),
		'CASHFLOW'	=> array(
			'code'	=>"CF",
			'name'	=>"Monthly Cashflows",
			'status'=>1,
			'page'	=>"cashflow",
		),
		'REVBYSRC'	=> array(
			'code'	=>"RS",
			'name'	=>"Revenue By Source",
			'status'=>1,
			'page'	=>"revbysrc",
		),
	);
	private $module_defaults = array();
	
	private $pms_heading_fields = array(
		"activity_name",
		"activity_description",
		"activity_deliverable",
		"activity_perfstd",
		"activity_criteria",
		"activity_poe",
		"activity_baseline",
		"activity_kpi_link",
		"activity_frequency_id",
		"activity_annual_target",
	);
	private $scoring_heading_fields = array(
		"kpi_munkpaid",
		"kpi_value",
		"kpi_unit",
		"kpi_baseline",
		"kpi_poe",
	);
	
	private $modref;
	
	//SDBP5B->DEPT_ASSURANCE status fields
	private $assurance_status_text = array(
		0 => "Not reviewed",
		2=>"Signed-off",
		4=>"Rejected",
		8=>"Updated after Rejection",
	);
	const ASSURANCE_ACCEPT = 2;
	const ASSURANCE_REJECT = 4;
	const ASSURANCE_UPDATED = 8;	
	

	public function __construct($modref="") {
		parent::__construct("client","",false,$modref);
		if(strlen($modref)>0) { $this->modref = $modref; }
	}

	
	public function getSections() {
		return $this->sections;
	}
	
	public function getSection($s) {
		return $this->sections[$s];
	}
	
	public function getAssuranceText($a) {
		if(isset($this->assurance_status_text[$a])) {
			return $this->assurance_status_text[$a];
		} else {
			return $a;
		}
	}
	
	
	
	
	
	
	
	
	
	
	/*******************************************************
	 * INDIVIDUAL PERFORMANCE FUNCTIONS
	 ******************************************************/
	public function getFilters($var) {
		$db = new ASSIST_DB();
		$db->setDBRef($var['modref']);
				$sub_sql = "SELECT S.sub_id as id, CONCAT(F.function_name,' - ',S.sub_name) as value, count(A.activity_id) as c
							FROM ".$db->getDBRef()."_subfunction S
							INNER JOIN ".$db->getDBRef()."_function F ON S.sub_function_id = F.function_id 
							INNER JOIN ".$db->getDBRef()."_activity A ON A.activity_subfunction_id = S.sub_id
							WHERE A.activity_status & 2 = 2
							GROUP BY S.sub_id
							HAVING count(A.activity_id)>0
							ORDER BY F.function_name, S.sub_name;
						";
		$subs = $db->mysql_fetch_all($sub_sql);
		$names = array('subs'=>"Functions",'owners'=>"Job Titles");
		return array('subs'=>$subs,'owners'=>array(),'names'=>$names);
	}
	
	
	/**
	 * Get Objects from the module for the PM* module
	 * Not all variables will be required here but are required in other modules where the same function is called so kept here for standardisation purposes
	 * @param $var
	 * @param bool $get_headings
	 * @param false $get_results
	 * @param array $time_ids
	 * @param bool $active_only
	 * @return array[]
	 */
	public function getObjects($var,$get_headings=true,$get_results=false,$time_ids=array(),$active_only=true) {
		$modref = $var['modref'];
		$filter_type = $var['filter_type'];
		$filter_id = $var['filter_id'];
		
		$db = new ASSIST_DB();
		$db->setDBRef($modref);
		
		$data = array('head'=>array(),'objects'=>array());
		
		//GET HEADING DETAILS
			$data['head'] = $this->getHeadings($var['primary_source'],true,false);
			
		//GET LISTS
		$lists = array();
		foreach($data['head']['raw'] as $fld=>$h) {
			if($h['h_type']=="LIST" || $h['h_type']=="OBJECT") {
				if($fld=="activity_subfunction_id") {
					$sql = "SELECT S.sub_id as id, IF(F.function_name=S.sub_name, S.sub_name, CONCAT(F.function_name,': ',S.sub_name)) as value 
							FROM ".$db->getDBRef()."_subfunction S INNER JOIN ".$db->getDBRef()."_function F ON F.function_id = S.sub_function_id";
				} else {
					$sql = "SELECT id, name as value FROM ".$db->getDBRef()."_list_".$h['h_table'];
				}
				$l = $db->mysql_fetch_value_by_id($sql, "id", "value");
				$lists[$fld] = $l;
			}
		}
		
		//GET Objects
		//Added option to get all objects regardless of active status #AA-568 JC
		$activityObject = new JAL1_ACTIVITY(0,$modref);
		$sql = "SELECT activity_id as id
					, CONCAT('".$modref."/".$activityObject->getRefTag()."',activity_id) as ref
					, activity_subfunction_id
					, ".implode(",",$this->pms_heading_fields)."
					, (activity_status & ".JAL1::ACTIVE.") = ".JAL1::ACTIVE."  as active
				FROM `".$db->getDBRef()."_activity` 
				WHERE ".($active_only===true?"(activity_status & ".JAL1::ACTIVE.") = ".JAL1::ACTIVE." 
				AND ":"");
		switch($filter_type) {
			case "ID":
				$sql.=" activity_id IN (".implode(",",$filter_id).") ";
				break;
			default:
			case "sub":
				$sql.=" activity_subfunction_id = ".$filter_id;
				break;
		} 
		$rows = $db->mysql_fetch_all_by_id($sql, "id");
		//Check for deleted log records #AA-568 JC
		if(count($rows)>0) {
			$sql = "SELECT log_object_id as object_id, log_insertdate as log_date FROM ".$db->getDBRef()."_object_log WHERE log_object_type = '".$activityObject->getMyObjectType()."' AND log_log_type = '".JAL1_LOG::DELETE."' AND log_object_id IN (".implode(",",array_keys($rows)).") ORDER BY log_insertdate ASC";
			$deleted_logs = $db->mysql_fetch_all_by_id($sql,"object_id");
//			echo "<hr /><h1 class='red'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//			echo $sql; ASSIST_HELPER::arrPrint($deleted_logs);
		} else {
			$deleted_logs = array();
			}
			
		foreach($rows as $i=>$r) {
//KPA index required for PM3 & PM4
			$r['kpa'] = $r['activity_subfunction_id'];
//GROUP index required for PM5			
			$r['group'] = $r['activity_subfunction_id'];
			//check if item is marked as deleted & record deleted date if that is the case (default to now if date not available) #AA-568
			if($r['active']==false) {
				$r['is_deleted'] = true;
				$r['deleted_date'] = isset($deleted_logs[$i]['log_date']) ? $deleted_logs[$i]['log_date'] : date("Y-m-d H:i:s");
			} else {
				$r['deleted_date'] = false;
				$r['is_deleted'] = false;
			}
			foreach($data['head']['raw'] as $fld=>$h) {
				if($h['h_type']=="LIST" || $h['h_type']=="OBJECT") {
					if($r[$fld]==0 || $r[$fld]=="X") {
						$r[$fld] = $this->getUnspecified();
					} else {
						$r[$fld] = $lists[$fld][$r[$fld]];
					}
				} elseif($h['h_type']=="BOOL") {
					switch($r[$fld]*1) {
						case 1:
							$r[$fld] = "Yes";
							break;
						case 0:
						default:
							$r[$fld] = "No";
							break;
					}
				}
			}
			$data['objects'][$i] = $r;
		}
		
		return $data;
	}
	
	
	
	
	public function getHeadings($modref,$main=true,$results=true) {
		$db = new ASSIST_DB();
		$db->setDBRef($modref);
		
		$data = array();
		
		$sql = "SELECT h_section as section, h_field as field, IF(LENGTH(h_client)=0,h_default,h_client) as h_client, h_type, h_table 
				FROM `".$db->getDBRef()."_setup_headings` 
				WHERE ( 
					h_section = 'ACTIVITY' AND h_field IN ('".implode("','",$this->pms_heading_fields)."') 
				)  "; 
		$head = $db->mysql_fetch_all_by_id($sql, "field"); //echo $sql;
		
		//Get SUB-FUNCTION name
		$sql = "SELECT 'ACTIVITY' as section,'activity_subfunction_id' as field, name_client as h_client, 'OBJECT' as h_type, 'subfunction' as h_table 
				FROM ".$db->getDBRef()."_setup_names WHERE name_section = 'object_sub'";
		$sub = $db->mysql_fetch_one($sql);
		if($main) {
			$data['main']['ref'] = "Ref";
			$data['main']['activity_subfunction_id'] = $sub['h_client'];
			$data['raw']['activity_subfunction_id'] = $sub;
			foreach($this->pms_heading_fields as $fld) {
				$data['main'][$fld] = $head[$fld]['h_client'];
				$data['raw'][$fld] = $head[$fld];
				unset($head[$fld]);
			}
		}
//REMOVED - FROM ORIGINAL SDBIP VERSION & NOT REQUIRED FOR JAL
/*		if($results) {
			foreach($head as $fld=>$h) {
				$data['results'][$fld] = $h['h_client'];
				$data['raw'][$fld] = $h;
			}
		}
	*/					
		return $data;
	}
	
	
	public function getScoreHeadings($modref,$main=true,$results=true) {
		$db = new ASSIST_DB();
		$db->setDBRef($modref);
		
		$data = array();
		
		$sql = "SELECT section, field, h_client, h_type, h_table 
				FROM `".$db->getDBRef()."_setup_headings_setup` 
				INNER JOIN ".$db->getDBRef()."_setup_headings 
				  ON h_id = head_id 
				WHERE ( 
					section = 'KPI' AND field IN ('".implode("','",$this->scoring_heading_fields)."') 
				) OR ( section = 'KPI_R' )";
		$head = $db->mysql_fetch_all_by_id($sql, "field");
		
		if($main) {
			$data['main']['ref'] = "Ref";
			foreach($this->scoring_heading_fields as $fld) {
				$data['main'][$fld] = $head[$fld]['h_client'];
				$data['raw'][$fld] = $head[$fld];
				unset($head[$fld]);
			}
		}

		if($results) {
			foreach($head as $fld=>$h) {
				$data['results'][$fld] = $h['h_client'];
				$data['raw'][$fld] = $h;
			}
		}
						
		return $data;
	}
		
	//merge & primary in place due to multiple SDBIP modules
	public function getKPAs($modref,$merge=false,$primary="") {
		if($modref!==false) {
		if(is_array($modref)) {
			foreach($modref as $mr) {
				$modref = $mr['modref'];
				break;
			}
		}
		//$db = new ASSIST_DB();
		//$db->setDBRef($modref);
		
		$data = array();
		
		//$sql = "SELECT * FROM ".$db->getDBRef()."_list_munkpa";
		//$data = $db->mysql_fetch_value_by_id($sql, "id", "value");
		$rows = $this->getFilters(array('modref'=>$modref));
		//$this->arrPrint($rows);
		foreach($rows['subs'] as $i => $r) {
			$data[$r['id']] = $r['value'];
		}
		
		return $data;
		}else {
			return false;
		}
	}	
	
	

	
	
	
/*
	public function getProjects($var,$get_headings=true,$get_results=false,$time_ids=array()) {
		$modref = $var['modref'];
		$filter_type = $var['filter_type'];
		$filter_id = $var['filter_id'];
		
		$db = new ASSIST_DB();
		$db->setDBRef($modref);
		
		$data = array('head'=>array(),'objects'=>array());
		
		//GET HEADING DETAILS
		//if($get_headings) {
			$data['head'] = $this->getHeadings($var['primary_source'],true,false);
		//}
		
		//GET LISTS
		$lists = array();
		foreach($data['head']['raw'] as $fld=>$h) {
			if($h['h_type']=="LIST") {
				if($fld=="kpi_subid") {
					$sql = "SELECT S.id, IF(D.value=S.value, S.value, CONCAT(D.value,': ',S.value)) as value 
							FROM ".$db->getDBRef()."_subdir S INNER JOIN ".$db->getDBRef()."_dir D ON D.id = S.dirid";
				} else {
					$sql = "SELECT * FROM ".$db->getDBRef()."_list_".$h['h_table'];
				}
				$l = $db->mysql_fetch_value_by_id($sql, "id", "value");
				$lists[$fld] = $l;
			}
		}
		
		//GET KPIS
		$sql = "SELECT cap_id as id, kpi_id as k_id
					, CONCAT('".$modref."/".SDBP5_CAPITAL::REFTAG."',cap_id) as ref
					, kpi_natkpaid, kpi_munkpaid, kpi_idpid, kpi_pdoid, kpi_value, kpi_unit, kpi_baseline, kpi_poe, kpi_ownerid, kpi_subid
					, kpi_calctype
					, kpi_targettype
					, cap_active 
					, kpi_active as active 
				FROM `".$db->getDBRef()."_capital`
				INNER JOIN ".$db->getDBRef()."_kpi
				ON kpi_capitalid = cap_id 
				WHERE 
				";
		switch($filter_type) {
			case "ID":
				$sql.=" kpi_capitalid IN (".implode(",",$filter_id).") ";
				break;
			case "sub":
				$sql.=" kpi_subid = ".$filter_id;
				break;
			default:
				$sql.=" kpi_ownerid = ".$filter_id;
				break;
		} 
		$rows = $db->mysql_fetch_all_by_id($sql, "id"); 
		if($get_results && count($rows)>0 && count($time_ids)>0) {
			$k_id = array();
			foreach($rows as $r) { $k_id[$r['k_id']] = $r['id']; }
			$sql = "SELECT kr_target , kr_actual , kr_timeid as time_id, kr_kpiid as id , kr_assurance_status as kr_assurance 
					FROM ".$db->getDBRef()."_kpi_results
					WHERE kr_kpiid IN (".implode(",",array_keys($k_id)).")"; //echo $sql;
			$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");
			$results = array();
			foreach($res as $kid => $rs) {
				$kpi_id = $k_id[$kid];
				$values = array('target'=>array(), 'actual'=>array());
				$calctype = $rows[$kpi_id]['kpi_calctype'];
				$targettype = $rows[$kpi_id]['kpi_targettype'];
				//foreach($rs as $t_id=>$tr) {
					$assurance = "N/A";
				foreach($time_ids as $t_id) {
					$tr = $rs[$t_id];
					$values['target'][$t_id] = $tr['kr_target'];
					$values['actual'][$t_id] = $tr['kr_actual'];
					$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, $t_id);
					$results[$kpi_id][$t_id] = $k_res;
					$results[$kpi_id][$t_id]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
					$results[$kpi_id][$t_id]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
					if($tr['kr_target']>0 || $tr['kr_assurance']>0) {
						$assurance = $tr['kr_assurance'];
					}
				}
				$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
				$results[$kpi_id]["YTD"] = $k_res;
				$results[$kpi_id]["YTD"]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
				$results[$kpi_id]["YTD"]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
				$results[$kpi_id]["YTD"]['kr_assurance'] = $this->getAssuranceText($assurance);
			}
			
		}
		
		foreach($rows as $i=>$r) {
			$r['kpa'] = $r['kpi_munkpaid'];
			foreach($data['head']['raw'] as $fld=>$h) {
				if($h['h_type']=="LIST") {
					$r[$fld] = $lists[$fld][$r[$fld]];
				}
			}
			$data['objects'][$i] = $r;
			if($get_results && count($data['objects'])>0 && count($time_ids)>0) {
				$data['objects'][$i]['results'] = $results[$i];
			}
					
		}
		
		
		return $data;
	}
		
	*/
	public function getKPIIDFromCAP($cap_id,$modref) {
		$db = new ASSIST_DB();
		$db->setDBRef($modref);
			
		$sql = "SELECT kpi_id FROM ".$db->getDBRef()."_kpi WHERE kpi_capitalid = $cap_id ";
		$kpi_id = $db->mysql_fetch_one_value($sql, "kpi_id");
		return $kpi_id;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************
	 * Functions for drawing KPI details
	 */
	/*
	public function getKPIDetailDisplay($id,$is_capital_project=false) {
		$data = array('display'=>"",'js'=>"",'kpi_id'=>$id);
		
		
		
		//change $data['kpi_id'] if is_capital_project=true
		return $data;
	}
	
	public function getKPIResultsDisplay($id) {
		$data = array('display'=>"",'js'=>"");
		
		
		
		
		return $data;
	}
	
	public function getCAPDetailDisplay($id) {
		$data = array('display'=>"",'js'=>"");
		
		
		
		return $data;
	}
	
	public function getCAPResultsDisplay($id) {
		$data = array('display'=>"",'js'=>"");
		
		
		
		
		return $data;
	}
	
	
	*/	
	
	
	
	
	
	
	
	
	private function formatResult($val,$tt,$decimal=2) {
		switch($tt) {
			case 1:	case "currency":		return "R ".number_format($val,$decimal); break;
			case 2:	case "perc": case "%": 	return number_format($val,$decimal)."%"; break;
			case 3: case "number":			return number_format($val,$decimal); break;
			default: 						return $val; break;
		}
	}
		
	
	/***
	 * function KPIcalcResult(
	 * 		array of all targets/actuals, 
	 * 		calctype, 
	 * 		time period range, 
	 * 		current time id
	 * )
	 */
	private function KPIcalcResult($values,$ct,$filter,$t) {
		$result_settings = $this->result_settings;
		//$result = array('r'=>0,'style'=>"result0",'text'=>"N/A",'target'=>0,'actual'=>0);
		$result = $result_settings[0];
		/* determine target & actual to use in calculating result */
		$targ = 0; $actl = 0;
		$tc = 0; $targsum=0; $actlsum=0;
		if($t=="ALL") {		//calculate overall result
			switch($ct) {
				case "CO":
					foreach($values['target'] as $i => $vt) {
						$va = $values['actual'][$i];
						if($targ < ($vt*1)) { $targ = $vt; }
						if($actl < ($va*1)) { $actl = $va; }
					}
					break;
				case "ACC":
				case "ZERO":
					$targ = array_sum($values['target']);
					$actl = array_sum($values['actual']);
					break;
				case "STD":
				case "REV":
					//$targ = array_sum($values['target']);		$targsum = $targ;
					//$actl = array_sum($values['actual']);		$actlsum = $actl;
					$tc = 0;
					foreach($values['target'] as $i => $vt) {
						if(($vt*1)>0) { $tc++; }
						$targ+=($vt*1);
						$actl+=($values['actual'][$i]*1);
					}
					$targsum = $targ;
					$actlsum = $actl;
					if($tc == 0) { $tc = 1; }
					$targ/=$tc;
					$actl/=$tc;
					break;
				case "LAST":
				case "LASTREV":
					foreach($values['target'] as $i => $vt) {
						$va = $values['actual'][$i];
						if(($vt*1)>0) { $targ = $vt; }
						if(($va*1)>0) { $actl = $va; }
					}
					break;
				case "NA":
					break;
			}
		} else {
					$targ = $values['target'][$t];
					$actl = $values['actual'][$t];
		}
		$targ*=1;	$actl*=1;
		/* compare targ & actl to determine result value */
		switch($ct) {
			case "CO":
			case "STD":
			case "ACC":
			case "LAST":
				if($targ!= 0 || $actl != 0) {
					if($targ==0) { 
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
						$result = $result_settings[5];
					} else {
						$div = $actl/$targ;
						if($div<0.75) {
							$result = $result_settings[1];
							//$result['r']=1;	$result['style']="result1";	$result['text']="R";
						} elseif($div < 1) {
							$result = $result_settings[2];
							//$result['r']=2;	$result['style']="result2";	$result['text']="O";
						} elseif($div==1) {
							$result = $result_settings[3];
							//$result['r']=3;	$result['style']="result3";	$result['text']="G";
						} elseif($div < 1.5) {
							$result = $result_settings[4];
							//$result['r']=4;	$result['style']="result4";	$result['text']="G2";
						} elseif($div >= 1.5) {
							$result = $result_settings[5];
							//$result['r']=5;	$result['style']="result5";	$result['text']="B";
						}
					}
				}
				break;
			case "REV":
			case "ZERO":
			case "LASTREV":
				if($ct=="ZERO" || (in_array($ct,array("REV","LASTREV")) && ($targ!=0 || $actl!=0))) {
					if($actl < $targ) {
						$result = $result_settings[5];
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					} elseif($actl==$targ) {
						$result = $result_settings[3];
						//$result['r']=3;	$result['style']="result3";	$result['text']="G";
					} else {
						$result = $result_settings[1];
						//$result['r']=1;	$result['style']="result1";	$result['text']="R";
					}
				}
				break;
			case "NA":
				break;
		}
		$result['target'] = $targ;
		$result['actual'] = $actl;
		$result['tc'] = $tc;
		$result['sum'] = array('target'=>$targsum,'actual'=>$actlsum);
		$result['values'] = $values;
		return $result;
	}		
	
	
	
}


?>