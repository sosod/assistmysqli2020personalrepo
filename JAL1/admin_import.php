<?php
/**
 * @var string $js - from inc_header
 * @var JAL1_DISPLAY $displayObject - from inc_header
 */

$url = "admin_import_";
require_once("inc_header.php");
$sdbipObject = new JAL1();

$available_sdbips = array(
		'function'=>array('name'=>$sdbipObject->getObjectName("FUNCTION")),
		'sub'=>array('name'=>$sdbipObject->getObjectName("SUB")),
		'activity'=>array('name'=>$sdbipObject->getObjectName("ACTIVITY")),
);

?>
<p class="i">Pick a Section to import.</p>
<table style="width:1100px;margin: 0 auto" id=tbl_container><tr><td style='text-align: center' class=container>
<?php



if(isset($button_name)) {
	$button_name = $sdbipObject->replaceActivityNames($button_name);
} else {
	$button_name = $sdbipObject->replaceActivityNames("|open|");
}



	foreach($available_sdbips as $key => $s) {
		$s['colour'] = "blue";
		$js.= $displayObject->drawVisibleBlock($s['name'],$key, "Import multiple ".$sdbipObject->getObjectName($key."s"),$button_name,$s['colour']);
	}


	?>
	</td></tr></table>

	<script type="text/javascript">

	$(function() {
		<?php echo $js; ?>



		var scr_dimensions = AssistHelper.getWindowSize();
		var div_h = 0;
		var tbl_h = 0;
		$("div.div_segment").button().click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).attr("page");
			var url = '<?php echo $url; ?>'+page.toLowerCase()+".php";
			document.location.href = url;

		});
		$("div.div_segment").hover(
			function() {
				$(this).removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
				$title = $(this).find("h2.object_title");
				var title_color = $title.attr("colorhistory");
				$title.removeClass(title_color).addClass("orange");
			},
			function() {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.addClass("ui-button-state-blue").removeClass("ui-button-state-orange");
				$title = $(this).find("h2.object_title");
				var title_color = $title.attr("colorhistory");
				$title.addClass(title_color).removeClass("orange");
			}
		).css({"margin":"10px","background":"url()","padding-bottom":"30px"});//,"border-color":"#0099FF"});

		$("button.btn_open").button({
			icons:{primary:"ui-icon-newwin"}
		}).css({
			"position":"absolute","bottom":"5px","right":"5px"
		});
		$("button.btn_open").click(function(e) {
			e.preventDefault();
			$(this).parent("div.div_segment").trigger("click");
		});
		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
			$("button.xbutton").css({"margin":"2px"});
		}
		formatButtons();

		var description_height = 0;
		$(".tbl-segment-description").find("td").css({"vertical-align":"middle","text-align":"center","padding":"5px"});
		$(".tbl-segment-description").each(function() {
			var h = parseInt(AssistString.substr($(this).css("height"),0,-2));
			if(h>description_height) {
				description_height = h;
			}
		});
		$(".tbl-segment-description").css("height",(description_height+5)+"px");
		$("#tbl_container, #tbl_container td").css("border","1px solid #ffffff");
		// $(".div_segment").css("border","1px solid #9999ff").addClass("blue-border");
		$(".div_segment").removeClass("ui-state-default").addClass("ui-button-state-blue");
		$(".btn_open").removeClass("ui-state-default").addClass("ui-button-state-blue");
	});
	</script>

