<?php
class ClientLegislationCategory extends LegislationCategory
{
     
     function __construct()
     {
        parent::__construct();
     }     

	function fetchAll( $options = "")
	{
		$results = $this->db->get("SELECT * FROM #_legislative_categories WHERE status & 2 <> 2 $options ORDER BY id, name");
	     $categories = array();
	     foreach($results as $index => $category)
	     {
	          $categories[$category['id']] = $category;
	          $categories[$category['id']]['imported'] = false;
	     }
	     return $categories;		
	}
	
	function fetch( $id )
	{
		$result  = $this->db->getRow("SELECT * FROM #_legislative_categories WHERE id = $id");
		return $result;
	}
	
	function searchCategories( $text )
	{
		$results = $this->db->get("SELECT id FROM #_legislative_categories WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' s");
		return $results;
	}
			
	function updateCategories( $newCats , $id)
	{
	   $del = $this->db->delete("category_legislation", "legislation_id = '".$id."'");
	   foreach( $newCats as $index => $catId)
	   {
		$this->db->insert("category_legislation", array("category_id" => $catId, "legislation_id" => $id));
	   }
	}
	
	function saveLegislativeCategory( $data )
	{
	   $this->setTablename("legislative_categories");
	   $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_legislative_categories');

	   if(!empty($get_last_id))
	   {
	     $next_id = $get_last_id['id'] + 1;
	     if($get_last_id['id'] < 10000)
	     {
	        $data['id'] = 10000 + $next_id;
	     } else {
	        $data['id'] = $next_id;  
	     }
	   }
	   $legid 	 = $this->save($data);
	   $response = $this->saveMessage("legislative_category", $legid); 
	   return $response;
	}
	
     function updateLegislativeCategory( $data )
	{
		$this->setTablename("legislative_categories");
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this ,new ActionNaming());
		$response = $this->updateMessage("legislative_category", $res);
		return $response;
	}	
	
	function getLegCat($id)
	{
       $legcategory = $this->db->getRow("SELECT category_id, legislation_id FROM #_category_legislation WHERE category_id = '".$id."' ");
	  return $legcategory;
	}	
}
?>
