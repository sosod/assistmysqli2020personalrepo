<?php
class ClientFunctionalService extends Model
{
     
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options="")
     {
        $result = $this -> db -> get("SELECT id, value AS name FROM assist_".$_SESSION['cc']."_list_functional_service WHERE yn = 'Y' ORDER BY value");
        return $result;
     }

     function fetch($id)
     {
        $result = $this -> db -> getRow("SELECT id, value AS name FROM assist_".$_SESSION['cc']."_list_functional_service WHERE id = '".$id."'");
        return $result;
     }
}
?>