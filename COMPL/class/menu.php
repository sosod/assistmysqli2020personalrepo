<?php
class Menu extends Model
{
   protected $user;
   
   protected $folder = "";

   protected $pagename = "";
   
   const MAIN       = 4;
   
   const TOP        = 8;
   
   const LOWER      = 16;
   
   const CUSTOM     = 32;

   
   function __construct()
   {
      
      parent::__construct();
      $userObj = new UserSetting();
      $this -> user = $userObj -> getUser(" UA.user_id = '".$_SESSION['tid']."' ");          
   }
   
   function getMenu($options = "")
   {
     $results = $this -> db -> get("SELECT * FROM #_menu WHERE status & 2 <> 2 AND id < 66 $options");
     return $results;
   }     
   
   function fetch($id)
   {
     $result = $this->db->getRow("SELECT * FROM #_menu WHERE id = '".$id."'");
     return $result;
   }

   function updateMenu($data)
   {
     $id = $data['id'];
     unset($data['id']);
     $this->setTableName("menu");
     $res = $this -> update($id, $data, new SetupAuditLog(), $this, new ActionNaming());
     $response = $this->updateMessage("menu", $res);
     return $response;	
   }   

   function isActivePage($menuname, $page)
   {
      if($menuname == $page)
      {
          return TRUE;
          break;
      } else if(strstr($page, $menuname)) {
          return TRUE;
          break;
      }
      return FALSE;
   }
   
   function displayMenu($menuItems, $level)
   {
     if(!empty($menuItems))
     {
        echo echoNavigation($level, $menuItems); 
     }
   }


}
?>
