<?php
/*
 * Defined how and what legislation to appear on the admin section
 */
class AdminLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getOptionSql($options = array())
	{

	   $optionSql  = array();
	   $clientSql  = "";
	   $masterSql  = "";
	   $activeSql  = "";
	   $activeSql .= " (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
				        OR L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
				        OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
		             ";	   
        $activeSql .= " AND legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";		             
        $activeSql .= " AND legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." ";		             
        if(isset($options['page']) && $options['page'] !== "reassign")
        {
          $activeSql .= " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
        }
        if(isset($options['limit']))
        {
          $masterSql .= " LIMIT ".$options['start'].",".$options['limit'];
        }  
        
        //$clientSql = " AND ((".$activeSql.") OR (L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED."))";
        $clientSql = " AND ((".$activeSql.") )";
                   
        if(isset($options['financial_year']))
        {
          $clientSql .= " AND L.financial_year = '".$options['financial_year']."' ";
        } 
        $optionSql['master'] = $masterSql;
        $optionSql['client'] = $clientSql;
        return $optionSql;          
	}

     function sortLegislation($masterLegislations, $clientLegislations = array())
     {
        $headerObj                          = new LegislationNaming(); 
        $headers                            = $headerObj -> getHeaderList();
        $legAdminObj                        = new LegislationAdmin();
        $legAuthorizerObj                   = new LegislationAuthorizer();
        $legislations                       = array();
        $legislations['usedId']             = array();
        $legislations['admin']              = array();
        $legislations['authorizor']         = array();
        $legislations['status']             = array();
        $legislations['isLegislationOwner'] = array();
        $legislations['headers']            = array();
        $legislations['legislations']       = array();
        foreach($masterLegislations as $mIndex => $mLegislation)
        {
          if(isset($clientLegislations[$mIndex]))
          {	     
            $ref                                                    = $clientLegislations[$mIndex]['id'];	          
            $legislations['headers']['id']                          = $headers['id'];
            $legislations['headers']['legislation_name']            = $headers['name'];
            $legislations['headers']['responsible_business_patner'] = $headers['business_patner'];
          
            $legislations['legislations'][$ref]['id']               = $ref;
            $legislations['legislations'][$ref]['legislation_name'] = $mLegislation['name'];
            $legislations['legislations'][$ref]['responsible_business_patner'] = $mLegislation['responsible_business_patner'];
            $authorizers = $legAuthorizerObj -> getLegislationAuthorizers($ref);
             
            $admins      = $legAdminObj -> getLegislationAdmins($ref);

            $legislations['admin'][$ref]      = $admins; //explode(",", $clientLegislations[$mIndex]['admin']);
            $legislations['authorizor'][$ref] = $authorizers; //explode(",", $clientLegislations[$mIndex]['authorizer']);
            	      
            //$setupLegislations['admin'][$mIndex] = explode(",", $clientLegislations[$mIndex]['admin']);
            //$setupLegislations['authorizor'][$mIndex] = explode(",", $clientLegislations[$mIndex]['authorizer']);
            $legislations['status'][$ref] = $clientLegislations[$mIndex]['legislation_status'];
            $legislations['usedId'][]     = (int)$mIndex;
            //array_push($setupLegislations['usedId'], $mIndex);
            if(Legislation::isLegislationOwner($admins))
            {
              $legislations['isLegislationOwner'][$ref] = TRUE;
            }
          }
        }  	   
        if(empty($legislations['headers']))
        {
           $_headers['id']                          = $headers['id'];
           $_headers['legislation_name']            = $headers['name'];
           $_headers['responsible_business_patner'] = $headers['business_patner'];
           $legislations['headers']                 = $_headers;
        }
        
        //$legislations['headers'] = (!empty($legislations['headers']) ? $legislations['headers'] : $headers);	   
        $legislations['columns'] = count($legislations['headers']);
        $legislations['total']   = count($legislations['legislations']);
        return $legislations;         
         /*$allowedHeaders = array("ref", "legislation_name", "responsible_business_patner");
          $legislationData = array();
		$legislationRef = array_flip($clientLegislations['legislation_reference']);
          foreach($masterLegislations as $index => $mLegislation)
          {
            foreach($mLegislation as $key => $value)
            {
               if(in_array($key, $allowedHeaders))
               {
                 if(isset($legislationRef[$mLegislation['ref']]))
                 {
                    $clientID = $legislationRef[$mLegislation['ref']];
                    $masterId = $mLegislation['ref'];
                    $legislationData['legislations'][$masterId][$key] = $value;
                    $legislationData['authorizor'][$masterId] = $clientLegislations['authorizor'][$clientID];
                    $legislationData['admin'][$masterId]      = $clientLegislations['admin'][$clientID];
                    $legislationData['status'][$masterId]     = $clientLegislations['status'][$clientID];
                 }
               }
            }    
          }
          foreach($allowedHeaders as $headerIndex => $headerVal)
          {
             if(array_key_exists($headerVal, $clientLegislations['headers']))
             {
                 $legislationData['headers'][$headerVal]  = $clientLegislations['headers'][$headerVal];
             }
          }            
          $legislationData['headers']['responsible_business_patner'] = "Responsible Business Patner";
          $legislationData['columns'] = count($legislationData['headers']);
          return $legislationData;
          */
     }     
	
     function getEditLegislations($start, $limit, $options = array())
     {
         $optionSql = " AND L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." ";
         
         $_legislations = $this->legislationObj->fetchLegislations("", "", $optionSql);      
         $total          = $this->legislationObj->getTotallegislations($optionSql);
	    $legislations = $this->sortLegislation($_legislations, new LegislationNaming("manage"));
         $legislations['company'] = ucfirst($_SESSION['cc']);
         $legislations['user']   = $_SESSION['tkn'];
         $legislations['total']  = $total['totalleg'];
         return $legislations;
     }
     
	function newSort($legislations, $myLegislations, $usedId, $allowedHeaders)
	{
		$data = array();
		foreach($legislations['headers'] as $hKey => $header)
		{
		   if( in_array($hKey, $allowedHeaders))
		   {
			 $data['headers'][$hKey] = $header; 
		   }			
		}
		foreach($legislations['legislations'] as $lIndex => $leg)
		{	
			//if( $_SESSION['tid'] != "0000")
			//{
				if( in_array($leg['ref'], $usedId['usedId']))
				{	
					foreach($myLegislations as $i => $myArr)
					 {
					 	//if the legislation has been used ,then check if the user has access to it
					 	if($this->checkAccessPermissions($myArr) || $_SESSION['tid'] == "0000")
					 	{
					 		//if user has access to this act , then get the header sorting 
					 		foreach($leg as $key => $val)
							{
								if( in_array($key, $allowedHeaders))
								{
									if($leg['ref'] == $myArr['legislation_reference'])
									{
										$data['data'][$lIndex][$key] = htmlentities($val);
									}
								}
							}
					 		if( $leg['ref'] == $myArr['legislation_reference'])
							{
								$data['status'][$leg['ref']]     = $myArr['legislation_status'];
								$data['authorizor'][$leg['ref']] = explode(",", $myArr['authorizer']);
								$data['admin'][$leg['ref']] 	 = explode(",", $myArr['admin']); 
							}	
					 	} else {
					 		$data['message'] = "Only legislation administrator can edit legislations ";
					 	}
					}
				}
			//} 				
		}					
		return $data;
	}	
	
	//update changes made to the legislation in the setup
	function editSetupLegislation($active, $legislationId, $newAuthorisor = array(), $newAdmin = array())
	{
         $clientObj     = new ClientLegislation();
         $legislation   = $clientObj -> findById($legislationId);	
         $adminObj      = new LegislationAdmin();
          //$admins              = $legislationAdminObj -> getLegislationAdmins($legislationId);
         $authorizerObj = new LegislationAuthorizer();
          //$authorizers   = $authorizerObj  -> getLegislationAuthorizers($legislationId);
         $res           = 0;
         $res           = $adminObj -> updateAdmins($newAdmin, $legislationId);
         $res          += $authorizerObj -> updateAuthorizers($newAuthorisor, $legislationId);
         $aLegislation  = array();
	    if(!empty($legislation))
	    {
             if(($legislation['legislation_status'] & Legislation::ACTIVE) == Legislation::ACTIVE)
		   {
		      //if de-activated , then set status to de-activated
		      if($active != 1)
		      {
		          $legislation['legislation_status'] = $legislation['legislation_status'] - Legislation::ACTIVE;	
		      }
		   } else {
		     if($active == 1)
		     {
		          $legislation['legislation_status'] = $legislation['legislation_status'] + Legislation::ACTIVE;
		     }   
		   }
	        $response = array(); 
		   $res     += $clientObj -> update($legislation['id'], $legislation, new LegislationAuditLog());		
		}
		if($res > 0) 
		{
		   $response = array("text" => "Legislation updated successfully . .  .", "error" => false);
		} else if($res == 0){
		  $response = array("text" => "No change was made to the legislation . .  . ", "error" => false);
		} else {
		  $response = array("text" => "Error updating the legislation . .  . ", "error" => true);
		}			
		return $response;
	}
		
	
}
