<?php
/**
	This class sends and makes log of the emails send 
	@package	: Email 
	@author		: admire<azinamo@gmail.com>
	@copyright	: 
	@filename	: email.php
**/
class Email extends DBConnect
{
	/**
		Email id
		@int 
	**/
	protected $id;
	/**
		Type of email send
		@char
	**/
	protected $ref;
	/**
		Id reference of the email send
		@int
	**/
	protected $ref_id;
	/**
		From email address
		@char
	**/	
	var $from;
	/**
		To email address
		@char
	**/
	var $to;
	/**
		CC email address
		@char
	**/
	protected $cc;
	/**
		BCC addreess
		@char
	**/
	protected $bcc;
	/**
		Email body
		@char
	**/	
	var $body;
	/**
		Emial attachments
		@char
	**/
	protected $attach;
	/**
	Subject of the email	
	**/
	var $subject;
	var $userFrom;
	
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function sendEmail()
	{
		$message = "";
		//Send email
		$headers  = 'MIME-Version:1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=us-ascii' . "\r\n";
		$headers .= 'From: no-reply@ignite4u.co.za' . "\r\n";
		$headers .= 'Reply-to: '.$this -> userFrom.' <'.$this -> from.'>' . "\r\n";
		
		if(@mail($this -> to, $this -> subject, $this -> body, $headers) ) 
		{
			return TRUE;
		} else {
			return FALSE;
		}
		exit();
	}
	

}
?>
