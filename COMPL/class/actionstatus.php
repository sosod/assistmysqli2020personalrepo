<?php
class ActionStatus extends Status
{
     protected static $table = "action_status";
     
     function __construct()
     {
         parent::__construct();
     }

	function getActionStatuses()
	{
		$response = $this->db->get("SELECT * FROM #_action_status WHERE status & 2 <> 2");
			$response[0] =array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		return $response;
	}
	
	function getAll()
	{
		$response = $this->db->get("SELECT * FROM #_action_status WHERE status & 2 <> 2");
			$response[0] =array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);

		return $response;
	}	
	
	
	function fetch( $id ) {
		if($id==0) {
			$response = array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		} else {
			$response = $this->db->getRow("SELECT * FROM #_action_status WHERE id = $id");
		}
		return $response;
	}
	
	function getStatusList()
	{
	   $statuses = $this->getActionStatuses();
	   $list     = array();
	   foreach($statuses as $sIndex => $sVal)
	   {
	     $list[$sVal['id']] = (!empty($sVal['client_terminology']) ? $sVal['client_terminology'] : $sVal['name']);
	   }
	   return $list;
	}


}
?>
