<?php
class DeliverableComplianceTo extends Model
{
     //stores the status of the deliverable-complianceto where the deliverable is using the match id , not the actual compliance-id
     const MATCHID = 4;
     
     function __construct()
     {
         parent::__construct();
     }     
     
     function fetchByDeliverableId($id)
     {
        $results  = $this -> db -> get("SELECT * FROM #_complianceto_deliverable WHERE deliverable_id = '".$id."' ");
        return $results;
     }
     
     
     function fetchByComplianceToId($id)
     {
        $result = $this->db->get("SELECT * FROM #_complianceto_deliverable WHERE complianceto_id = '".$id."' ");
        return $result;
     }
     
	function updateDeliverableComplianceTo($newCt, $id)
	{
		$this -> db -> delete("complianceto_deliverable", "deliverable_id=$id");
		foreach($newCt as $index => $val)
		{
		     $insertdata = array("status" => (1 + DeliverableComplianceTo::MATCHID), "deliverable_id" => $id, "complianceto_id" => $val);
			$this -> db -> insert("complianceto_deliverable", $insertdata);
		}
	}
	
	function updateComplianceToDeliverable($newId, $oldId)
	{
	    $insertdata = array("status" => (1 + DeliverableComplianceTo::MATCHID), "complianceto_id" => $newId);
	    $res = $this -> db -> updateData("complianceto_deliverable", $insertdata, array("complianceto_id" => $oldId));
	  return $res;  	
	}

     
     public static function isUsingDeliverableComplianceToMatch($status)
     {
        if(($status & DeliverableComplianceTo::MATCHID) == DeliverableComplianceTo::MATCHID)
        {
          return TRUE;
        }
        return FALSE;    
     }

	
}
?>