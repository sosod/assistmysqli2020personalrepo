<?php
class ComplianceManager implements ISortable{
	
	protected $headers = array();
	
	const ACTIVE 	= 1;
	
	const DELETE    = 2;
	
	const INACTIVE  = 4; 
	
		function __constructor()
		{
			
		}
	
	  function __call( $methodname , $args )
	  {
		 $classname  = substr($methodname, 3, -4);
		 $resultList = array();
		 if(class_exists($classname))
		 {
			 $classObj = new $classname();
			 $results  = $classObj -> getAll( $args[0] );
			 if(isset($results) && !empty($results))
			 {
			    //load a result list
			    foreach($results as $index => $val)
			    {
			  if(isset($val['name']))
			  {
				 $resultList[$val['id']] = $val['name'];
			  }
			    }    
			 }
		 }
		 return $resultList;
		}   
   
      function sortLegislation( $legislations , Naming $naming, $page)
      {  
       	$headers  = $naming->headersList( $page );               
         $setupObj = new SetupManager();
         $data    = array();      
			foreach($legislations as $key => $value)
			{
				$categories = $setupObj->getLegislativeCategoriesList($value['ref']);  	   
				$orgsizes   = $setupObj->getOrganisationSizeList($value['ref']);	   
				$types      = $setupObj->getLegOrganisationTypesList($value['ref']);	   			
				$capacities = $setupObj->getOrganisationCapacityList($value['ref']);								
				foreach($headers as $index => $val)
				{
					if($index == "action_progress"){
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = 0;						
					} else if( $index == "legislation_type") {
						$orgtypes =  $setupObj -> getLegislativeTypes();	
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = $this->multipleSort( $orgtypes, $value[$index] );						
					} else if( $index == "legislation_category")	{
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = implode(",", $categories);
					} else if($index == "organisation_size") {
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = implode(",", $orgsizes);                  	        
					} else if($index == "organisation_type"){
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = implode(",", $types);                  	        
					} else if($index == "organisation_capacity") {	         
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = implode(",", $capacities);                  	        
					} else if(isset($value[$index]) || array_key_exists($index, $value)) {
						$data['headers'][$index]  = $val;
						$data['legislations'][$value['ref']][$index] = $value[$index];
				  }
			  }
			}			
			$data['data'] = (isset($data['legislations']) ? $data['legislations'] : array());
			$data['columns'] = (isset($data['headers']) ? count($data['headers']) : count($headers));
			$data['headers'] = (isset($data['headers']) ? $data['headers'] : $headers);
			return $data;
    }
    
    
	function sortHeaders( $data , Naming $naming, $page = "")
	{
		$headers  = $naming->headersList( $page );
		$setupObj = new SetupManager();
		$response = array();
		if( isset($data) && !empty($data))
		{
			foreach($data as $index => $dataValue){
				foreach( $headers as $key => $header)
				{
					if( $key == "action_progress"){
						$this->headers[$key]	 			= $header;
						$response[$dataValue['ref']][$key]  = "0";						
					} elseif(array_key_exists($key, $dataValue)) {	
						$this->headers[$key]	 			= $header;
						if( $key == "organisation_capacity") {
							$capacities =  $setupObj -> getOrganisationCapacity();	
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $capacities, $dataValue[$key] );	
						} else if( $key == "legislation_category") {
							$categories =  $setupObj -> getLegislativeCategories();	
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $categories, $dataValue[$key] );						
						} else if( $key == "organisation_type") {
							$orgtypes =  $setupObj -> getLegOrganisationTypes();	
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $orgtypes, $dataValue[$key] );
						} else if( $key == "legislation_type") {
							$orgtypes =  $setupObj -> getLegislativeTypes();	
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $orgtypes, $dataValue[$key] );						
						} else if( $key == "organisation_size") {
							$orgsizes =  $setupObj -> getOrganisationSizes();	
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $orgsizes, $dataValue[$key] );				
						} else if( $key == "sub_event"){
							$subevents =  $setupObj -> getSubEvents();
							$response[$dataValue['ref']][$key]  = $this->multipleSort( $subevents , $dataValue[$key] );
						} else if( $key == "responsibility_owner"){
							$responsibleowners =  $setupObj -> getResponsibleUsers();
							$response[$dataValue['ref']][$key]  = $this->multipleSortUsers( $responsibleowners , $dataValue[$key] );
						}  else {						
							$this->headers[$key]	 			= $header;
							$response[$dataValue['ref']][$key]  = $dataValue[$key];
						}
					}		
				}
			}	
		} else {
			$this->headers = $headers;
		}
		return array("headers" => $this->headers, "columns" => count($this->headers) , "data" => $response, "total" => count($response) );
	}
	
	function multipleSort( $data, $match)
	{
		$results = "";
		if( $match != ""){
			$matchArr = explode(",", $match);

			foreach( $data as $index => $valArr)
			{
				if(in_array($valArr['id'], $matchArr)){
					$results .= $valArr['name'].",";
				}	
			}
			$results = rtrim($results, ",");			
		}
		return $results;
	}
	
	function multipleSortUsers( $data , $match)
	{
		$results = "";
		if( $match != ""){
			$matchArr = explode(",", $match);

			foreach( $data as $index => $valArr)
			{
				if(in_array($valArr['ref'], $matchArr)){
					$results .= $valArr['user'].",";
				}	
			}
			$results = rtrim($results, ",");			
		}
		return $results;
	}
	
}
