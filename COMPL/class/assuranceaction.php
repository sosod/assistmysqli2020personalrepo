<?php
class AssuranceAction
{
	private $actionAssuranceObj = null;
	
	protected static $tablename = "action_assurance";
	
	function __construct()
	{
		$this->actionAssuranceObj = new ActionAssurance();
	} 


	function getActionAssurance( $start, $limit, $id)
	{
		$actions   = $this->actionAssuranceObj->fetchAll($start, $limit, $id);
		$actioTotal = $this->actionAssuranceObj->getActionAssurance($id);
		return array("total" => $actioTotal['total'] , "data" => $actions); 
	}
	
	function saveActionAssurance( $data )
	{
		$this->actionAssuranceObj->setTablename("action_assurance");
		$res = $this->actionAssuranceObj->save($data);
		if( $res > 0)
		{
			$response = array("text" => "Action assurance saved", "error" => false);
		} else {
			$response = array("text" => "Error saving action. . . ", "error" => true);
		}
		return $response;		
	}
		
	function updateActionAssurance( $data )
	{ $res = 0;
		$id 	   = $data['id']; 		
		unset( $data['id'] );
		$this->actionAssuranceObj->setTablename("action_assurance");
		$res = $this->actionAssuranceObj->update($id, $data, new ActionAssuranceAuditLog(), $this->actionAssuranceObj, new ActionNaming());
		if( $res > 0)
		{
			$response = array("text" => "Action Assurance updated . . ", "error" => false);
		} else {
			$response = array("text" => "Error updating action assurance", "error" => true);
		}
		return $response;
	}
	

}
?>
