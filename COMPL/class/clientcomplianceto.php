<?php
class ClientComplianceTo extends Model
{
     
     function __construct()
     {
         parent::__construct();
     }     
     
     function fetchAll($options = "")
     {
       $results = $this->db->get("SELECT id, name, status FROM #_compliance_to WHERE status & 2 <> 2 $options ORDER BY name");     
       return $results;
     }
     
     function fetch($id)
     {
       $result = $this->db->getRow("SELECT id, name, status FROM #_compliance_to WHERE id = '".$id."' ");
       return $result;
     }
      	
    function savenewComplianceTo($data)
    {
      $this->setTablename("compliance_to");      	
	 $res = $this->save($data);
	 $response = $this->saveMessage("new_compliance_to_title", $res); 
	 return $response;   	 
    }
    
}
?>