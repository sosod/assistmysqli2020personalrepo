<?php
class DeliverableStatus extends Status
{
     protected static $table = "deliverable_status";

     function __construct()
     {
         parent::__construct();
     }

	function getDeliverableStatuses($optionSql = "")
	{
	   $response = $this->db->get("SELECT * FROM #_deliverable_status WHERE status & 2 <> 2 $optionSql ");
	   return $response; 
	}
	
	function getAll($optionSql = "")
	{
	   $response = $this -> db -> get("SELECT * FROM #_deliverable_status WHERE status & 2 <> 2 $optionSql ");
			$response[0] = array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);

	   return $response; 
	}
		
	
	function getActiveDeliverableStatuses()
	{
		$response = $this->db->get("SELECT * FROM #_deliverable_status WHERE status & 1 = 1  AND status & 2 <> 2 ");
			$response[0] = array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		return $response; 	
	}
	
	function fetch( $id )
	{
		if($id==0) {
			$response = array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		} else {
			$response  = $this->db->getRow("SELECT * FROM #_deliverable_status WHERE id = $id");
		}
		return $response;
	}	
	
	function fetchAll($option_sql = "")
	{
	   $response = $this -> db -> get("SELECT * FROM #_deliverable_status WHERE status & 2 <> 2 $option_sql");
	   $results  = array(0=>"Unable to Comply");
	   if(!empty($response))
	   {
	      foreach($response as $s_index => $s_val)
	      {
	        $results[$s_val['id']] = $s_val;
	      }
	   }
	   return $results;
	}
}
?>