<?php
/*
 Deliverables from the client database
*/
class ClientDeliverable extends Deliverable {

     function __construct()
     {
         parent::__construct();
     }

  	function fetchAll($optionSql = "" )
  	{
  		$sql = "SELECT DISTINCT D.id, D.id AS deliverable_ref, D.* FROM #_deliverable D 
  	                                   INNER JOIN #_legislation L ON L.id = D.legislation AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
  	                                   LEFT OUTER JOIN #_legislation_admins LA ON LA.legislation_id = L.id AND LA.status = 1
  	                                   WHERE 1  $optionSql";
									   //echo $sql;	
  	   $response = $this -> db -> get($sql);
  	   return $response;
  	}
	
  	function fetch($id)
  	{
  	   $response = $this -> db -> getRow("SELECT D.id AS ref, D.*, L.legislation_status FROM #_deliverable D 
  	                                      INNER JOIN #_legislation L ON L.id = D.legislation
  	                                      WHERE D.id = {$id} 
  	                                     ");
  	   return $response;
  	}

  	function totalDeliverables($optionSql = "")
  	{
  	   $response = $this -> db -> getRow("SELECT COUNT(*) AS total FROM #_deliverable D 
  	                                      INNER JOIN #_legislation L ON L.id = D.legislation 
  	                                      WHERE 1 $optionSql");
  	   return $response;
  	}
  	/*
  	 from fetchAllDeliverables to fetchDeliverableByLegislationId
  	*/
  	function fetchDeliverableByLegislationId($legislationId)
  	{
  		$results = $this -> db -> get("SELECT * FROM #_deliverable WHERE legislation = {$legislationId}");
  		return $results;
  	} 
	
  	function fetchDeliverableSubEvent($optionSql = "")
  	{
  		$results = $this -> db -> get("SELECT D.id ,D.short_description, D.sub_event, D.main_event, SD.subevent_id AS subeventid
      							       FROM #_deliverable D
      							       LEFT JOIN #_subevent_deliverable SD ON SD.deliverable_id = D.id
      							       WHERE 1 $optionSql
  							          ");
  		return $results;
  	}
       
    function getCurrentVersions()
    {
       $results = $this -> db -> get("SELECT id, deliverable_id, current_version, version_in_use FROM #_deliverable_usage");
      return $results;	
    }
  	
  	function getCurrentVersion($id)
  	{
  	  $result = $this -> db -> get("SELECT id, deliverable_id, current_version, version_in_use 
  	                                FROM #_deliverable_usage WHERE deliverable_id = {$id}
  	                              ");
  	  return $result;
  	}
     
    function generateReport($reportData, Report $reportObj)
    {
       $deliverableManagerObj = new DeliverableReport();
       $joinWhere     = $this -> createJoinsWhere($reportData['value']);
	   $fields        = $reportObj -> prepareFields($reportData['deliverables'], "D");
	   $groupBy       = $reportObj -> prepareGroupBy($reportData['group_by'], "D"); 
	   $sortBy        = $reportObj -> prepareSortBy($reportData['sort'], "D");
	   $whereStr      = $reportObj -> prepareWhereString($reportData['value'], $reportData['match'], "D");   
	   $whereStr     .= $joinWhere['where'];
	   $deliverables  = $this -> filterDeliverables($fields, $whereStr, $groupBy, $sortBy, $joinWhere['joins']);
	   $dels          = $deliverableManagerObj -> sortDeliverables($deliverables, $reportData['deliverables']); 
	   $dels['title'] = $reportData['report_title'];
       $reportObj -> displayReport($dels, $reportData['document_format'], "deliverable");    
    }
     
    function filterDeliverables($fields, $whereStr = "", $groupBy = "", $sortBy = "", $joins = "")
    {
	     $orderBy = (!empty($sortBy) ? "ORDER BY $sortBy " : "");
	     $groupBy = (!empty($groupBy) ? "GROUP BY $groupBy " : "");
		 $results = $this -> db -> get("SELECT D.id, D.event_occuranceid, D.recurring, D.recurring_type, 
		                                D.days_options, D.recurring_period, D.status,
		                                $fields
					                    FROM #_deliverable D
					                    INNER JOIN #_legislation L ON L.id = D.legislation 
					                    $joins
					                    WHERE 1
                                        AND (
                                         ( D.status & ".Deliverable::ACTIVATED_BYSUBEVENT." = ".Deliverable::ACTIVATED_BYSUBEVENT."
                                            OR D.status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED." 
                                            OR D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED."
                                            OR D.status & ".Deliverable::CONFIRMED." = ".Deliverable::CONFIRMED."
                                          ) OR L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."
                                         )    
                                         AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE."    
					                     AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED."
					                    $whereStr
					                    $groupBy
					                    $orderBy 
					                  ");
          $deliverables = array();
  		  foreach($results as $index => $deliverable)
  		  {
  		     $deliverables[$deliverable['id']] = $deliverable;
  		  }			               
  		return $deliverables;		
    }	
  
    function createJoinsWhere($options)
    {
          
        $whereStr = "";
        $joinStr  = "";
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
           $whereStr = " AND L.financial_year = '".$options['financial_year'][0]."' ";
        }
        
        if(isset($options['compliance_frequency']) && !empty($options['compliance_frequency']))
        {
           $compWhere = "";
           foreach($options['compliance_frequency'] as $index => $complianceFrequency)
           {
             if($complianceFrequency !== "all")
             {
               $compWhere .= "  D.compliance_frequency = '".$complianceFrequency."' OR";
             }
           }
           if(!empty($compWhere))
           {
               $whereStr .= " AND (".rtrim($compWhere, "OR").") \r\n";
           }
           
        }  
        if(isset($options['applicable_org_type']) && !empty($options['applicable_org_type']))
        {
          $allowOrgtype = 0;
          $orgtypeSql = "";
          foreach($options['applicable_org_type'] as $index => $applicationOrgType)
          {
             if($applicationOrgType !== "all")
             {
               $orgtypeSql .= " D.applicable_org_type = '".$applicationOrgType."' OR";
               $allowOrgtype += 1;
             }
          }
          if($allowOrgtype > 0)
          {
               $whereStr .= " AND (".rtrim($orgtypeSql, "OR").")";
              //$joinStr .= " INNER JOIN #_orgtype_deliverable DAOT ON DAOT.deliverable_id = D.id \r\n";
          }         
        } 
        if(isset($options['responsible']) && !empty($options['responsible']))
        {
          $respWhere = "";
          foreach($options['responsible'] as $index => $department)
          {
            if($department != "all")
            {
               $respWhere .= " D.responsible = '".$department."' OR";
            }
          }
          if(!empty($respWhere))
          {
             $whereStr .= " AND (".rtrim($respWhere, "OR").") \r\n";
          }
        }
        if(isset($options['functional_service']) && !empty($options['functional_service']))
        {
          $functionalWhere = "";
          foreach($options['functional_service'] as $index => $functionalService)
          {
             if($functionalService != "all")
             {
               $functionalWhere .= " D.functional_service  = '".$functionalService."' OR";
             }
          }
          if(!empty($functionalWhere))
          {
             $whereStr .= " AND (".rtrim($functionalWhere, "OR").") \r\n";
          }
          
        }
        if(isset($options['accountable_person']) && !empty($options['accountable_person']))
        {
          $allowAccountable = 0;
          foreach($options['accountable_person'] as $index => $accountable)
          {
              if($accountable != "all")
              {
                $whereStr .= " AND DACC.accountableperson_id = '".$accountable."' \r\n";
                $allowAccountable += 1;
              }
          }
          if($allowAccountable > 0)
          {
             $joinStr .= " INNER JOIN #_accountableperson_deliverable DACC ON DACC.deliverable_id = D.id  \r\n";
          }  
        }
        if(isset($options['responsibility_owner']) && !empty($options['responsibility_owner']))
        {
          $ownerWhere = "";
          foreach($options['responsibility_owner'] as $index => $owner)
          {
             if($owner !== "all")
             {
               $ownerWhere .=  " D.responsibility_owner = '".$owner."' OR";               
             }
          }
          if(!empty($ownerWhere))
          {
             $whereStr .= " AND (".rtrim($ownerWhere, "OR").") \r\n";
          }
        }
        if(isset($options['compliance_to']) && !empty($options['compliance_to']))
        {
          $allowComplianceTo  = 0;
          foreach($options['compliance_to'] as $index => $complianceto)
          {
             if($complianceto != "all")
             {
               $whereStr .= " AND DCO.complianceto_id = '".$complianceto."' \r\n";
               $allowComplianceTo += 1;
             }
          }
          if($allowComplianceTo > 0)
          {
             $joinStr .= " INNER JOIN #_complianceto_deliverable DCO ON DCO.deliverable_id = D.id \r\n";
          }
        }
        if(isset($options['sub_event']) && !empty($options['sub_event']))
        { 
          $allowSubevent = 0;
          foreach($options['sub_event'] as $index => $subevent)
          {
             if($subevent != "all")
             {
               $whereStr .= " AND DSE.subevent_id = '".$subevent."' ";
               $allowSubevent += 1;
             }
          } 
          if($allowSubevent > 0)
          {
             $joinStr .=  " INNER JOIN #_subevent_deliverable DSE ON DSE.deliverable_id = D.id ";
          }
        }
        if(isset($options['main_event']) && !empty($options['main_event']))
        {
           $eventWhere = "";
           foreach($options['main_event'] as $index => $mainevent)
           {
              if($mainevent != "all")
              {
                 $eventWhere .= " D.main_event = '".$mainevent."' OR";   
              }
           }
           if(!empty($eventWhere))
           {
             $whereStr .= " AND (".rtrim($eventWhere, "OR").") ";
           }
        }
        $joinWhere['where'] = $whereStr;
        $joinWhere['joins'] = $joinStr;       
        return $joinWhere;
    }
     
    function deliverableType($type, &$delStatus, &$logAdd)
    {
	  //if($type === "manage")
	  //{
	    $logAdd    = TRUE;
	    $delStatus = $delStatus + Deliverable::MANAGE_CREATED;
	  //}      
    } 
     
    function saveDeliverable( $data )
    {
	     $createAction = FALSE;
	     $logAdd       = FALSE;
    	 //initialize the deliverable status to 1, ie active
    	 $delStatus    = Deliverable::ACTIVE;
    	 //echo "Deliverable Status before passing by reference ".$delStatus." and logAdd ".$logAdd."\r\n\n";
    	 $this -> deliverableType($data, $delStatus, $logAdd); 
    	 if(isset($data['deliverabletype']))
    	 {
             //$deliverbleTypes
    	    unset($data['deliverabletype']);
    	 }	
    	 //echo "Deliverable Status is now ".$delStatus." and logAdd ".$logAdd."\r\n\n";
    	 //exit();	
    	//$this->deliverableObj->setTablename("deliverable");
    	 if($data["hasActions"] == 0)
    	 {
    	   //create a dummy action for this deliverable
    	   $createAction = TRUE;	
    	 } 				
    	 if(Deliverable::isUsingClientEvent($data['main_event']))
    	 {
    	    $delStatus += Deliverable::USINGEVENT_CLIENTID;
    	 }
    	 $insertMultiple = array();
    	 //if the field is a multiple select where there is a many to many relationship , save it in their relationship tables
    	 foreach($this -> multiple as $index => $key)
    	 {
            if(array_key_exists($key, $data))
    	    {
    		   if(!empty($data[$key]))
    		   {
    	          if($key == "sub_event")
    			  {    
			         if(Deliverable::isDeliverableSubEventActivated($data[$key]))
			         {
                        $delStatus = $delStatus + Deliverable::EVENT_ACTIVATED;
			         }
			         $insertMultiple[$key] = $data[$key];  
			      } else {
		             $insertMultiple[$key] = $data[$key]; 
			      }
    			  unset($data[$key]);
    	       }
    	    }
    	 }
    	  $data['status'] = $delStatus + Deliverable::USING_USERID + Deliverable::USING_DEPARTMENTID + Deliverable::USING_FUNCTIONALSERVICEID;
    	  $actiondeadline = $data['actiondeadlinedate'];
    	  $actionowner    = $data['actionowner'];
    	  unset($data['actiondeadlinedate']);
    	  unset($data['actionowner']);
    	  unset($data["hasActions"]);
    	  $this -> setTablename('deliverable');
    	  $res       =  $this -> save($data);
    	  $response  = array();
    	  $actionStr = "";
      	  if($res > 0) 
      	  {	
      	     //if deliverable added under manage section , log it
      	     if($logAdd)
      	     {
      	        DeliverableAuditLog::processAddLog($this, $res, $data);
      	     }
      		//save the multiple fields into the relationship tables 
      		foreach($this -> multipleInsert as $k => $multipleArr)
      		{
      		   if(isset($insertMultiple[$k]) && !empty($insertMultiple[$k]))
      		   {
      		      if(is_array($insertMultiple[$k]) && !empty($insertMultiple[$k]))
      		      {
          		     foreach($insertMultiple[$k] as $index => $val)
          			 {	
          				$this -> setTablename($multipleArr['t']);
          				$dRes = $this -> save(array("deliverable_id" => $res, $multipleArr['key'] => $val, "status" => 5));
          			 }      		        
      		      } else {
      				$this -> setTablename($multipleArr['t']);
      				$_val = $insertMultiple[$k];
      				$dRes = $this -> save(array("deliverable_id" => $res, $multipleArr['key'] => $_val, "status" => 5));     
      		      }		
      		   }
      		}		
      		//if set to create a dumpy action 	
      		if($createAction)
      		{

      			
      			$actionObj = new Action();
      			$action    = array("action"			 => $data['short_description'], 
      						    "owner"              => $actionowner , 
      						    "action_deliverable" => $data['short_description'],
      						    "deadline"           => $actiondeadline, 
      						    "reminder"           => "",
      						    "deliverable_id"     => $res
      						   );
      			if($logAdd)
      			{
      			     $action['actionstatus'] = Action::MANAGE_CREATED + 1;
      			}
      			
      			$actionObj -> setTablename("action");
      			$actionObj -> save( $action );
      			$actionStr = " and an action was created";
      		}		
      		
      		$_SESSION['deliverblesavedtext'] = "Deliverable saved ".$actionStr;
      		$response = array("text" => "Deliverable saved ".$actionStr, "error" => false, "id" => $res ); 
      	} else {
      		$response = array("text" => "Error saving deliverable", "error" => true, "id" => $res );
      	}
    	return $response;	
	  }

	  
	  


    function saveClientImportDeliverable( $data ) {
//echo "<pre>"; print_r($data); echo "</pre>";
      		$actionObj = new Action();
   			$actionObj -> setTablename("action");
		$action_deadline_types = $this->getActionDeadlineTypes();
		$legislation_id = $data['legislation'];
		$legislationObj = new ClientLegislation();
		$legislation    = $legislationObj -> findById($legislation_id);
		$financialYearObj = new FinancialYear();
		$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);
		$legislation_deadline = strtotime($data['legislation_deadline']);
		$end_of_financial_year = strtotime($financialyear['end_date']);
		$max_action_deadline = ($legislation_deadline > $end_of_financial_year) ? $end_of_financial_year : $legislation_deadline;
		$start_of_financial_year = strtotime($financialyear['start_date']);
	
//	     $createAction = FALSE;
	     $logAdd       = FALSE;
    	 //initialize the deliverable status to 1, ie active
    	 $delStatus    = Deliverable::ACTIVE;
    	 //echo "Deliverable Status before passing by reference ".$delStatus." and logAdd ".$logAdd."\r\n\n";
    	 //$this -> deliverableType($data, $delStatus, $logAdd); 
    	 if(isset($data['deliverabletype'])) {
             //$deliverbleTypes
    	    unset($data['deliverabletype']);
    	 }	
    	 //echo "Deliverable Status is now ".$delStatus." and logAdd ".$logAdd."\r\n\n";
    	 //exit();	
    	//$this->deliverableObj->setTablename("deliverable");
    	 if(Deliverable::isUsingClientEvent($data['main_event']))
    	 {
    	    $delStatus += Deliverable::USINGEVENT_CLIENTID;
    	 }
    	 $insertMultiple = array();
    	 //if the field is a multiple select where there is a many to many relationship , save it in their relationship tables
    	 foreach($this -> multiple as $index => $key)
    	 {
            if(array_key_exists($key, $data))
    	    {
    		   if(!empty($data[$key]))
    		   {
    	          if($key == "sub_event")
    			  {    
			         if(Deliverable::isDeliverableSubEventActivated($data[$key]))
			         {
                        $delStatus = $delStatus + Deliverable::EVENT_ACTIVATED;
			         }
			         $insertMultiple[$key] = $data[$key];  
			      } else {
		             $insertMultiple[$key] = $data[$key]; 
			      }
    			  unset($data[$key]);
    	       }
    	    }
    	 }
    	  $data['status'] = $delStatus + Deliverable::USING_USERID + Deliverable::USING_DEPARTMENTID + Deliverable::USING_FUNCTIONALSERVICEID;
    	  $actiondeadline = $data['action_deadline'];
    	  $actionowner    = $data['action_owner'];
    	  unset($data['action_deadline']);
    	  unset($data['action_owner']);
    	  unset($data["hasActions"]);
    	  $this -> setTablename('deliverable');
    	  $res       =  $this -> save($data);
    	  $response  = array();
    	  $actionStr = "";
      	  if($res > 0) 
      	  {	
      	     //if deliverable added under manage section , log it
      	     if($logAdd)
      	     {
      	        DeliverableAuditLog::processAddLog($this, $res, $data);
      	     }
      		//save the multiple fields into the relationship tables 
      		foreach($this -> multipleInsert as $k => $multipleArr)
      		{
      		   if(isset($insertMultiple[$k]) && !empty($insertMultiple[$k]))
      		   {
      		      if(is_array($insertMultiple[$k]) && !empty($insertMultiple[$k]))
      		      {
          		     foreach($insertMultiple[$k] as $index => $val)
          			 {	
          				$this -> setTablename($multipleArr['t']);
          				$dRes = $this -> save(array("deliverable_id" => $res, $multipleArr['key'] => $val, "status" => 5));
          			 }      		        
      		      } else {
      				$this -> setTablename($multipleArr['t']);
      				$_val = $insertMultiple[$k];
      				$dRes = $this -> save(array("deliverable_id" => $res, $multipleArr['key'] => $_val, "status" => 5));     
      		      }		
      		   }
      		}		
      		//if set to create a dumpy action 	
      		//if($createAction) {
			
			
      			
      			$action    = array("action"			 => $data['description'], 
      						    "owner"              => $actionowner , 
      						    "action_deliverable" => $data['description'],
      						    "deadline"           => "", 
      						    "reminder"           => "",
      						    "deliverable_id"     => $res,
								"actionstatus"		=> Action::ACTIVE
      						   );
				$adeadline = array();
				
				$start_date = $start_of_financial_year;
				$Hr = date("H",$start_date);
				$min = date("i",$start_date);
				$sec = date("s",$start_date);
				switch($actiondeadline) {
					case 'startfinyear'	: $adeadline[] = date("d-M-Y",$start_date); break;
					case 'endfinyear'	: $adeadline[] = date("d-M-Y",$max_action_deadline); break;
					case 'start1'		:
						for($i=0;$i<12;$i++) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
						break;
					case 'end1'			:
						for($i=0;$i<12;$i++) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							$ldom = date("t",$ad);
							$ad = mktime($Hr,$min,$sec,date("m",$ad),$ldom,date("Y",$ad));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
						break;
					case 'start3'		:
						for($i=0;$i<12;$i+=3) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
						break;
					case 'end3'			:
						for($i=0;$i<12;$i+=3) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							$ldom = date("t",$ad);
							$ad = mktime($Hr,$min,$sec,date("m",$ad),$ldom,date("Y",$ad));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
					case 'start4'		:
						for($i=0;$i<12;$i+=4) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
						break;
					case 'end4'			:
						for($i=0;$i<12;$i+=4) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							$ldom = date("t",$ad);
							$ad = mktime($Hr,$min,$sec,date("m",$ad),$ldom,date("Y",$ad));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
					case 'start6'		:
						for($i=0;$i<12;$i+=6) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
						break;
					case 'end6'			:
						for($i=0;$i<12;$i+=6) {
							$ad = mktime($Hr,$min,$sec,date("m",$start_date)+$i,1,date("Y",$start_date));
							$ldom = date("t",$ad);
							$ad = mktime($Hr,$min,$sec,date("m",$ad),$ldom,date("Y",$ad));
							if($ad<=$max_action_deadline) { 
								$adeadline[] = date("d-M-Y",$ad);
							}
						}
					default				: $adeadline[] = $actiondeadline; 	break;
				}
				
				
				
				$a = 0;
				foreach($adeadline as $ad) {
					$action['deadline'] = $ad;
					$actionObj -> save( $action );
					$a++;
				}
				if($a>1) {
					$actionStr = " and an action was created";
				} else {
					$actionStr = " and $a actions were created";
				}
      		//}		
      		
      		$_SESSION['deliverblesavedtext'] = "Deliverable saved ".$actionStr;
      		$response = array("text" => "Deliverable saved ".$actionStr, "error" => false, "id" => $res ); 
      	} else {
      		$response = array("text" => "Error saving deliverable", "error" => true, "id" => $res );
      	}
    	return $response;	
	  }

	  
	  
	  
	  
	  
	  
     /*
      Checks if the deliverable is event activated and if the status of event activation is not set , then set it , 
      if the deliverable is no longer event activated then remove the event activation status
      @return void
     */
     function _checkDeliverableStatus($deliverableId, $keyVal, &$delStatus)
     {
        //if the deliverable is not already activated by an event then check if a valid sub-event was selected
       if(($delStatus & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED)
       {	
         if(!Deliverable::isDeliverableSubEventActivated($keyVal))
         {
            //echo "Removing the event activation status , therefore must also remove it from the associated actions";
            $actionObj    = new ClientAction(); 
            $actionObj -> updateEventDeliverableActionStatus($deliverableId); 
            $delStatus    = $delStatus - Deliverable::EVENT_ACTIVATED;     
            if($delStatus == 0)
            {
              $delStatus = Deliverable::ACTIVE;
            }
         }                    	          
       } else {
         //adding the eventing activation status to the deliverable, thus becomes inactive until the event occures or unlocked
         if(Deliverable::isDeliverableSubEventActivated($keyVal))
         {
            $actionObj = new ClientAction(); 
            $actionObj -> updateNonEventDeliverableActionStatus($deliverableId); 
            //echo "Adding the event activation status , therefore must also remove it from the associated actions";
            $delStatus = $delStatus + Deliverable::EVENT_ACTIVATED;     
            //if the deliverable had the activated status, then we remove the activated status of that deliverable and it becomes unactivated, and its activation also becomes unactive
            if(($delStatus & Deliverable::ACTIVATED) == Deliverable::ACTIVATED)
            {
               $delStatus = ($delStatus - Deliverable::ACTIVATED);
            }
         }                                  
       }          
     }

	function editDeliverable($data)
	{
        $id 	       = $data['id'];
        unset($data['id']);
        $deliverable = $this -> fetch($id);
        $delStatus   = $deliverable['status'];
        foreach($this -> multiple as $key)
        {
           if($key == "sub_event" && !empty($data[$key]))
           { 
             //check if deliverable is event activated if not remove the event activation status
             $this -> _checkDeliverableStatus($id, $data[$key], $delStatus);               
           }     
           if(array_key_exists($key, $data))
           {
              $data['multiple'][$key] = $data[$key];
              unset($data[$key]);
           }
        }		
        $data['status'] = $delStatus;
        $res 	          = $this -> update($id, $data, new DeliverableAuditLog(), $this, new DeliverableNaming());
        $response       = array();
        if($res > 0)
        {
          $response = array("text" => "Deliverable updated", "error" => false, "updated" => true );
        } else if( $res == 0){
          $response = array("text" => "There was no change made to the deliverable", "error" => false, "updated" => true);
        } else {
           $response = array("text" => "Error saving the deliverable", "error" => true, "updated" => false);
        }
      return $response;	
    }	
	     
     function reAssign($data, $statuses = array())
     {
        $response =  array();
        //$this->deliverableObj->setTablename("deliverable");
        $status_list = array();
        if(!empty($statuses))
        {
          foreach($statuses as $s_index => $del_status)
          {
            $del_id               = substr($del_status['name'], 11);
            $status_list[$del_id] = $del_status['value'];
          }
        }
        $res = 0;
        if(!empty($data))
        {
           foreach($data as $index => $valArr)
           {
              $deliverableId      = substr($valArr['name'], 9);
              if(isset($status_list[$deliverableId]))
              {
                 $deliverable_status    =  $status_list[$deliverableId];
                 if(($status_list[$deliverableId] & Deliverable::USING_MATCHEDUSERID) == Deliverable::USING_MATCHEDUSERID)
                 {
                    $deliverable_status = ($status_list[$deliverableId] - Deliverable::USING_MATCHEDUSERID);
                    $deliverable_status = ($status_list[$deliverableId] + Deliverable::USING_USERID);
                 }
              }
              if(isset($deliverable_status) && !empty($deliverable_status))
              {
                 $updatedata['status'] = $deliverable_status;
              }
              $updatedata['responsibility_owner'] = $valArr['value'];
              $res                               += $this  -> update("id={$deliverableId}", $updatedata, new DeliverableAuditLog());
           }         
        }
        if($res > 0)
        {
            $response = array('text' => 'Users re-assignment was successfull ...', 'error' => false);
        } else if($res == 0) {
            $response = array('text' => 'No change has been made to the users responsible ...', 'error' => false, 'updated' => true);
        } else {
           $response = array('text' => 'Error updating user responsible ...', 'error' => false);
        }
       return $response;       
     }     
     
     function fetchDeliverableBySubEvent_Id($subeventId)
     {
	   $results = $this -> db -> get("SELECT D.*, D.id AS ref , SD.subevent_id AS subeventid
						              FROM #_deliverable D
						              INNER JOIN #_subevent_deliverable SD ON SD.deliverable_id = D.id
						              WHERE SD.subevent_id = '".$subeventId."'  
						            ");
	   return $results;          
     }  
     
      //copy deliverables triggered by sub events and save them 
      function copyBySubEvent($occuranceId, $subeventId)
      {
      	$actionObj            = new ClientAction();
      	$deliverableSubEvents = $this -> fetchDeliverableBySubEvent_Id($subeventId);
      	$newdeliverableId     = array();
      	if(!empty($deliverableSubEvents))
      	{
		   foreach($deliverableSubEvents as $index => $val)
		   {
			 if(!empty($val))
			 {
  				//check if this deliverable was supposed to be activated/triggered by event occurance
  				if(($val['status'] & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED) 
  				{
  				   $val['deliverable_reference'] = $val['id'];					
  				   unset($val['id']);
  				   unset($val['subeventid']);
  				   unset($val['ref']);
  				   unset($val['insertuser']);
  				   unset($val['insertdate']);
  				   $val['event_occuranceid'] = $occuranceId;
  				   $val['status']            = $val['status'] - Deliverable::EVENT_ACTIVATED;
  				   $val['status']            = $val['status'] + Deliverable::CREATED_SUBEVENT;
  				   $res                      = $this -> save($val);
  				   if($res > 0)
  				   {
    				  $this -> copySaveRelationship("accountableperson_deliverable", $res, $val['deliverable_reference']);
    			      $this -> copySaveRelationship("subevent_deliverable", $res, $val['deliverable_reference']);
    				  $this -> copySaveRelationship("complianceto_deliverable", $res, $val['deliverable_reference']);
    			      $actionObj -> copyActionBySubEvent($res, $val['deliverable_reference']);
  				   }						
  				   $newdeliverableId[$subeventId][] = $res;
  				}
			 }
		   }      	
      	}      
      	return $newdeliverableId;	
      }        
	
     /*
      Copy deliverables relationship when copying deliverables 
     */    
	function copySaveRelationship($table, $id, $ref)
	{
	   $relationships = $this -> db -> get("SELECT * FROM #_".$table." WHERE deliverable_id = '".$ref."' ");
	   if(!empty($relationships))
	   {
		 foreach($relationships as $index => $relation)
		 {
		    $relation['deliverable_id'] = $id;	
		    $this -> db -> insert($table, $relation);						
		 }
	   }
	}
     /*
       Copy the legislation deliverables from the one year to another 
    */
	function copySaveDeliverable($legislationRef, $legislationId, $financial_year, $finObj, $delObj, $actionObj)
	{
	   $deliverables = $this -> fetchDeliverableByLegislationId($legislationRef);
	   $res          = 0;
	   $totalActions = 0;
	   foreach($deliverables as $index => $deliverable)
	   {
	      if(  (  ($deliverable['status'] & Deliverable::ACTIVATED_BYSUBEVENT) != Deliverable::ACTIVATED_BYSUBEVENT)  && (  ($deliverable['status'] & Deliverable::CREATED_SUBEVENT) != Deliverable::CREATED_SUBEVENT) && (   (  ($deliverable['status'] & Deliverable::ACTIVATED) == Deliverable::ACTIVATED)  || (  ($deliverable['status'] & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED)   ) )
	      {
	          $deliverable['legislation'] = $legislationId;
	          $deliverableRef             = $deliverable['id'];
	          $deliverableDeadline        = $finObj -> createFinancialYearDate($financial_year, $deliverable['legislation_deadline']);
	          $deliverable['legislation_deadline'] = $deliverableDeadline;
	          if(($deliverable['status'] & Deliverable::ACTIVATED) == Deliverable::ACTIVATED)
	          {
	             $deliverable['status'] = $deliverable['status'] - Deliverable::ACTIVATED;
	          }
	          if(($deliverable['status'] & Deliverable::CONFIRMED) == Deliverable::CONFIRMED)
	          {
	             $deliverable['status'] = $deliverable['status'] - Deliverable::CONFIRMED;
	          }
			$deliverable['deliverable_status'] = 1;
			$deliverable['reminder'] = "";
			$deliverable['attachment'] = "";
	          unset($deliverable['id']);
	          $delRes = $delObj -> save($deliverable);
	          if($delRes > 0)
	          {
	             $delObj -> copySaveRelationship("accountableperson_deliverable", $delRes, $deliverableRef);
                 $delObj -> copySaveRelationship("subevent_deliverable", $delRes, $deliverableRef);
		         $delObj -> copySaveRelationship("complianceto_deliverable", $delRes, $deliverableRef);  
	             $totalActions += $actionObj -> copyActionSave($deliverableRef, $delRes, $finObj, $financial_year, $actionObj);
	             $res++;
	          }	 
	      }  
	   }
	   return array("actions" => $totalActions, "deliverables" => $res);
	}

	function updateDeliverable($data)
	{
		$updatedata['reminder'] 		  = $data['reminder'];
		$updatedata['deliverable_status'] = $data['deliverable_status']; 
		$updatedata['status'] 			  = $data['status']; 
		$res 	                          = $this -> update($data['id'], $updatedata, new DeliverableAuditLog(), $this, new DeliverableNaming());
		$response                         = array();
		if(  $res > 0)
		{
			$response = array("text" => "Deliverable updated", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the deliverable", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the deliverable", "error" => true, "updated" => false);
		}
		return $response;	
	}
	  
	function getUpdatedDeliverables()
	{
		$latestusage 	       = $this -> getCurrentVersions();
		$deliverableRef        = array();
		$deliverables	       = array();
		$deliverableReferences = array();
		foreach( $latestusage as $index => $valArr)
		{
			$deliverableusage = $this -> getLatestVersion($valArr['deliverable_id']);
			if(!empty($deliverableusage))
			{
			  if($valArr['version_in_use'] != $deliverableusage['id'])
			  {
			    $deliverableRef[] = "'".$valArr['id']."'";
			  }
			}			
		}
		if( !empty($deliverableRef))
		{
		  $options = " AND D.id IN (".implode(",", $deliverableRef).")";
		  $deliverables 	 =  $this -> fetchAll($options);			
		}
		foreach( $deliverables as $index => $deliverable)
		{
		  $deliverableReferences[$deliverable['id']]  = $deliverable['deliverable_reference'];
		}
		$manageObj       = new ManageDeliverable();
		$deliverablesArr = $manageObj -> sortDeliverable($deliverables, new DeliverableNaming("manage"));
		return array("deliverables" => $deliverablesArr , "deliverableReferences" => $deliverableReferences);		
	}
	

	function allowUpdate($data)
	{
		list($refId , $idRef ) = explode("_", $data['ref'] );
		$ref 	               = substr($refId, 6);
		$id 		           = substr($idRef, 2 );

		$latestdeliverables = $this -> importDeliverable( $ref );
		$latestversion 	    = $this -> getLatestVersion( $ref );

		if(!empty($latestdeliverables)) 
		{
		   $this -> setTablename("deliverable");
		   $res = $this -> update($id, $latestdeliverables, new DeliverableAuditLog());
		   if( $res > 0)
		   {
				$usage = array("current_version" => $latestversion['id'], "version_in_use" => $latestversion['id'] );
				$this -> setTablename("deliverable_usage");
				$this -> update("deliverable_id={$id}", $usage, new DeliverableAuditLog() );
		   }				
		}
		$response = array();
		if($res > 0)
		{
			$response = array("text" => "Deliverable updated", "error" => false, "updated" => true );
		} else if( $res == 0) {
			$response = array("text" => "There was no change made to the deliverable", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the deliverable", "error" => true, "updated" => false);
		}
		return $response;	
	}	
	 
	/*
	  Update all the deliverable using the master-department id so that they now use the client-match ids
	*/
	function updateDepartmentStatus($masterDeptId, $clientDeptId)
	{
	  // get deliverables that are still using the master department id and update them so they now use the client-match id
	  $optionSql    = " AND D.responsible  = '".$masterDeptId."' 
	                    AND D.status & ".Deliverable::USING_DEPARTMENTID." <> ".Deliverable::USING_DEPARTMENTID." ";
	  $deliverables = $this -> fetchAll($optionSql);
	  if(!empty($deliverables))
	  {
	     foreach($deliverables as $index => $deliverable)
	     {
	        $status     = $deliverable['status'] + Deliverable::MATCH_DEPARTMENTID;
	        $updateData = array("responsible" => $clientDeptId, "status" => $status);
	        $this -> db -> updateData("deliverable", $updateData, array("id" => $deliverable['id']));
	     }
	  }
	}   

	/*
	  Remove the use of the matched department and go back to using the department from the master 
	*/
	function undoDepartmentMatch($refId, $deptId)
	{
	  $optionSql = " AND D.responsible  = '".$deptId."' 
	                 AND D.status & ".Deliverable::MATCH_DEPARTMENTID." = ".Deliverable::MATCH_DEPARTMENTID." ";
	  $deliverables = $this -> fetchAll($optionSql);
	  if(!empty($deliverables))
	  {
	     foreach($deliverables as $index => $deliverable)
	     {
	        $status     = $deliverable['status'] - Deliverable::MATCH_DEPARTMENTID;
	        $updateData = array("responsible" => $refId, "status" => $status);
	        $this -> db -> updateData("deliverable", $updateData, array("id" => $deliverable['id']));
	     }
	  }
	}
     
     function updateDeliverableDepartmentStatus($deliverable)
     {    
        if(!(Deliverable::isUsingDepartmentid($deliverable['status'])))
        {
          if(Deliverable::isMatchDepartmentid($deliverable['status']))
          {
            $status = ($deliverable['status'] - Deliverable::MATCH_DEPARTMENTID) + Deliverable::USING_DEPARTMENTID;
          } else {
            $status = $deliverable['status'] + Deliverable::USING_DEPARTMENTID;
          }
          //$this -> db -> updateData("deliverable", array("status" => $status), array("id" => $deliverable['ref']));           
          return $status;
        }
        return $deliverable['status'];
     }
     
     /*
      Update the deliverable that are using the master functional services so that they now use the client functional matched to the master one
     */	
     function updateDeliverableFunctionalService($masterfunctionalServiceId, $clientFunctionalServiceId)
     {
       $optionSql = " AND D.functional_service = '".$masterfunctionalServiceId."' 
                      AND D.status & ".Deliverable::USING_FUNCTIONALSERVICEID." <> ".Deliverable::USING_FUNCTIONALSERVICEID." ";
       $deliverables  = $this -> fetchAll($optionSql);
       foreach($deliverables as $dIndex => $deliverable)
       {
          $status     = $deliverable['status'] + Deliverable::MATCH_FUNCTIONALSERVICEID;
          $updatedata = array("status" => $status, "functional_service" => $clientFunctionalServiceId); 
          $this -> db -> updateData("deliverable", $updatedata, array("id" => $deliverable['id']));
       }  
     }
     /*
      Removing the client functional service and revert back to using master functional services
     */
      function undoClientFunctionalServices($ref, $functionalServiceId)
      {
        $optionSql = " AND D.functional_service = '".$functionalServiceId."' 
                       AND D.status & ".Deliverable::MATCH_FUNCTIONALSERVICEID." = ".Deliverable::MATCH_FUNCTIONALSERVICEID." ";
         $deliverables = $this -> fetchAll($optionSql);
         if(!empty($deliverables))
         {
            foreach($deliverables as $index => $deliverable)
            {
               $status     = $deliverable['status'] - Deliverable::MATCH_FUNCTIONALSERVICEID;
               $updatedata = array("status" => $status, "functional_service" => $ref);
               $this -> db -> updateData("deliverable", $updatedata, array("id" => $deliverable['id']));
            }
         }
      }     
      
      function updateDeliverableFunctionalServiceStatus($deliverable)
      {
         if(!(Deliverable::isUsingFunctionalserviceid($deliverable['status'])))
         {
             if(Deliverable::isMatchFunctionalserviceid($deliverable['status']))
             {
               $status = ($deliverable['status'] - Deliverable::MATCH_FUNCTIONALSERVICEID) + Deliverable::USING_FUNCTIONALSERVICEID;
             } else {
               $status = $deliverable['status'] + Deliverable::USING_FUNCTIONALSERVICEID;
             }       
             $updatedata = array("status" => $status);
             //$this -> db -> updateData("deliverable", $updatedata, array("id" => $deliverable['ref']));
             return $status;
         }
         return $deliverable['status'];
      }
      /*
       Update the deliverable which are not currently using the userid , ie those still using the master deliverable responsible owner
      */
      function updateDeliverablwOwnerMatch($refId, $ownerId)
      {
         $optionSql    = " AND D.responsibility_owner = '".$refId."' 
                           AND D.status & ".Deliverable::USING_USERID." <> ".Deliverable::USING_USERID."";
         $deliverables = $this -> fetchAll($optionSql);
         if(!empty($deliverables))
         {
            foreach($deliverables as $index => $deliverable)
            {
               $status     = $deliverable['status'] + Deliverable::USING_MATCHEDUSERID;
               $updatedata = array("status" => $status, "responsibility_owner" => $ownerId);
               $this -> db -> updateData("deliverable", $updatedata, array("id" => $deliverable['id']));
            }
         }
      }
      /*
       undo the responsible owner matched to the deliverable , 
      */
      function undoDeliverableResponsibleOwnerMatch($refId, $userId)
      {
         $optionSql    = " AND D.responsibility_owner = '".$userId."' 
                           AND D.status & ".Deliverable::USING_MATCHEDUSERID." = ".Deliverable::USING_MATCHEDUSERID." ";
         $deliverables = $this -> fetchAll($optionSql);

         if(!empty($deliverables))
         {
           foreach($deliverables  as $index => $deliverable)
           {
              $status     = $deliverable['status'] - Deliverable::USING_MATCHEDUSERID;
              $updatedata = array("status" => $status, "responsibility_owner" => $refId);
              $this -> db -> updateData("deliverable", $updatedata, array("id" =>  $deliverable['id']));
           }
         }
      }
      
      function updateDeliverableResponsibleOwnerStatus($deliverable)
      {
         if(!(Deliverable::isUsingUserid($deliverable['status'])))
         {
            if(Deliverable::isUsingMatcheduserid($deliverable['status']))
            {
               $status = ($deliverable['status'] - Deliverable::USING_MATCHEDUSERID) + Deliverable::USING_USERID;
            } else{
               $status = $deliverable['status'] + Deliverable::USING_USERID;
            }
            //$this -> db -> updateData("deliverable", array("status" => $status), array("id" => $deliverable['ref']));
            return $status;
         }
         return $deliverable['status'];
      }
     
     function getDeliverableSubEvent($key, $value)
     {
	     $results = $this -> db -> get("SELECT D.*, D.id AS ref , SD.subevent_id AS subeventid
						           FROM #_deliverable D
						           INNER JOIN #_subevent_deliverable SD ON SD.deliverable_id = D.id
						           WHERE ".$key." = '".$value."'  
						       ");
	     return $results; 
     }

	function activateDeliverables($id, $actionObj)
	{
	   $optionSql     = " AND legislation = '".$id."' AND D.status & ".Deliverable::CONFIRMED." = ".Deliverable::CONFIRMED." ";
	   $deliverables  = $this -> fetchAll($optionSql);	   
	   foreach($deliverables as $index => $deliverable)
	   {
	      if(($deliverable['status'] & Deliverable::CONFIRMED) == Deliverable::CONFIRMED)
	      {
	        $status     = ($deliverable['status'] - Deliverable::CONFIRMED) + Deliverable::ACTIVATED;  
	      } else {
	        $status     = $deliverable['status'] + Deliverable::ACTIVATED;
	      }
	      $updatedata = array("status" => $status);
	      $res        = $this -> db -> updateData("deliverable", $updatedata, array("id" => $deliverable['id']));
	      if($res > 0)
	      {
	         $actionObj -> activateActions($deliverable['id']);
	      }
	   }  
	}
	   /*
	    Activate/Unlock an event-driven deliverable under admin, so it becomes a non event
	   */
	  public function activateEventDeliverable($deliverableId)
	  {
	     $deliverable  = $this -> fetch($deliverableId);
	     $res          = 0;             
	     if(($deliverable['status'] & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED)
	     {
	        $status = $deliverable['status'] - Deliverable::EVENT_ACTIVATED;
	        if(($deliverable['status'] & Deliverable::ACTIVATED) != Deliverable::ACTIVATED)
	        {
	            if(($deliverable['legislation_status'] & Legislation::ACTIVATED) == Legislation::ACTIVATED)
	            {
	               $status   += Deliverable::ACTIVATED;
	               $actionObj = new ClientAction();
	               $actionObj -> activateEventActions($deliverableId);
	            }
	        } else {
	            if(($deliverable['legislation_status'] & Legislation::ACTIVATED) == Legislation::ACTIVATED)
	            {
	               $actionObj = new ClientAction();
	               $actionObj -> activateEventActions($deliverableId);
	            }	        
	        }
	        $updatedata['status']     = $status;
	        $updatedata['main_event'] = 1;
	        $res                      = $this -> update($deliverableId, $updatedata, new DeliverableAuditLog(), $this, new DeliverableNaming());
	        if($res > 0)
	        {
	           $subEventObj = new DeliverableSubEvent();
	           $subEventObj -> updateDeliverableSubevent(array(1), $deliverableId);
	        }
	     }
	     $response = array();
	     if($res > 0)
	     {
	        $response = array('text' => 'Deliverable D'.$deliverableId." succussefully updated", 'error' => false);
	     } else {
	        $response = array('text' => 'An error occured updating the deliverable D'.$deliverableId, 'error' => true);
	     }
	     return $response;
	  } 
	   
	
      function activateDeliverable($data = array() )
      {  
         $countActivated   = 0;
         $countDeactivated = 0;
         if(isset($data['checked']) && !empty($data['checked']))
         {
            foreach($data['checked'] as $index => $checkedVal)
            {
               $id                   = substr($checkedVal, 17);  
               $updatedata['status'] = 0;
               $del                  = $this -> fetch($id);
               $updatedata['status'] =  $del['status'] + Deliverable::ACTIVATED_BYSUBEVENT;
               $res                  = $this -> update($id, $updatedata, new DeliverableAuditLog());
               $countActivated++;
            }
         }
         if(isset($data['unchecked']) && !empty($data['unchecked']))
         {
         	foreach($data['unchecked'] as $un => $unchecked)
         	{
         		if(!empty($unchecked))
         		{
         			$countDeactivated++;
         		}         		
         	}
         }
         if(($countActivated > 0) || ($countDeactivated > 0))
         {     
           $eventOccuranceObj = new EventOccurance();
           $eventRes          = $eventOccuranceObj -> updateEventOccurance(array("occuranceid" => $data['eventoccuranceid']
                                                                                , "status" => EventOccurance::EVENT_ACTIVATED
                                                                                )
                                                                           );
         }
                  
         $text  = "";     
         $text .=  "&nbsp;&nbsp;".$countActivated." deliverables have been activated and updated, and are available under manage ";
         $text .=  $countDeactivated." deliverables have not been activated <br />";
         return array("text" => $text, "error" => false);           
      }	
      
      /*
       check to see if the user id for the deliverable is present in users' list, and if the deliverable is using a matching status
       if not, update the status to match the user's id bieng used
      */
      function isUserStatusUptoDate($users, $deliverable)
      {
         $status = $deliverable['status'];
         if(isset($users[$deliverable['responsibility_owner']]))
         {
            if(!(Deliverable::isUsingUserId($deliverable['status'])))
            {
               $status = ($deliverable['status'] - Deliverable::USING_MATCHEDUSERID) + Deliverable::USING_USERID;
               $this -> db -> updateData('deliverable', array('status' => $status), array('id' => $deliverable['id']));
            }
         }
         return $status;
      }
      
      function makeDeliverableCsv()
      {
          $deliverablenameObj = new DeliverableNaming();
          $headers            = $deliverablenameObj -> getHeaderList();
          $fieldsList = array(
                            array(
                                       	$headers['short_description'],
                                        $headers['description'],
                                        $headers['legislation_section'],
                                        $headers['compliance_frequency'],
                                        $headers['applicable_org_type'],
                                        'IS date reccuring',
                                        $headers['compliance_date'],
                                        'Number of days',
                                        'Days option',                       
                                        'Days from',         
                                        $headers['responsible'],
                                        $headers['functional_service'],
                                        $headers['accountable_person'],
                                        $headers['responsibility_owner'],
                                        $headers['legislation_deadline'],
                                        $headers['sanction'],
                                        $headers['reference_link'],
                                        $headers['assurance'],
                                        $headers['compliance_to'],
                                        $headers['main_event'],
                                        $headers['sub_event'],
                                        $headers['reporting_category'],
                                        $headers['guidance'],
                                        'Does this deliverable has actions'
                                   ),
                              array(
                                        "text",
                                        "text",
                                        "text",
                                        "text - select options",
                                        "text - select options",
                                        "text(Yes/No)",
                                        "date YYYY/MM/DD eg. (2012/06/17)",
                              			"number - days",                      
                              			"text - (Days/Working Days)",
                              			"text - select options",
                                        "text - select options",
                                        "text - select options",
                                        "text - select options",
                                        "text - select options",
                                        "date YYYY/MM/DD eg. (2012/06/17)",
                                        "text",
                                        "text",
                                        "text",
                                        "text - select options",
                                        "text - select options",
                                        "text - select options",
                                        "text - select options",
                                        "text",
                              			"text(Yes/No)"
                                   )

                            );
           $filename =  "deliverable.csv";
           $fp       =  fopen($filename, "w+");
           foreach($fieldsList as $fields)
           {
                fputcsv($fp, $fields);
           }
           fclose($fp);
           header('Content-Type: application/csv'); 
           header("Content-length: " . filesize($filename)); 
           header('Content-Disposition: attachment; filename="' . $filename . '"'); 
           readfile($filename);
           unlink($filename);
           exit();	
      }

	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	function getActionViewLog($id) {
		require_once("../../library/class/assist_dbconn.php");
		require_once("../../library/class/assist_db.php");
		$db = new ASSIST_DB();
		$sql = "SELECT * FROM ".$db->getDBRef()."_deliverable WHERE id = '".$id."' ";
		$action = $db->mysql_fetch_one($sql);
       //$action      = $this -> db -> getRow($sql);
	   $action_logs_merge = array();
	   $sql = "SELECT * FROM ".$db->getDBRef()."_deliverable_update WHERE deliverable_id = '".$id."' ";
	   $action_update_logs = $db->mysql_fetch_all($sql);
	   //$sql = "SELECT * FROM #_legislation_update WHERE legislation_id = '".$id."' ";
       //$action_update_logs = $this -> db -> get($sql;);
	   $sql = "SELECT * FROM ".$db->getDBRef()."_deliverable_edit WHERE deliverable_id = '".$id."' ";
	   $action_edit_logs = $db->mysql_fetch_all($sql);
	   //$sql = "SELECT * FROM #_legislation_edit WHERE legislation_id = '".$id."' ";
	   //$action_edit_logs =  $this -> db -> get($sql);
	   //$key = 0;
	   foreach($action_update_logs as $l) {
			$z = $l['changes'];
			if(substr($z,0,2)=="a:") { 
				$l['changes'] = base64_encode($z);
			}
			$l['sort_fld'] = strtotime($l['insertdate']);
			$key = $l['sort_fld'];
			while(isset($action_logs_merge[$key])) { $key++; }
			$action_logs_merge[$key] = $l;
			//$key++;
	   }
	   foreach($action_edit_logs as $l) {
			$z = $l['changes'];
			if(substr($z,0,2)=="a:") { 
				$l['changes'] = base64_encode($z);
			}
			$l['sort_fld'] = strtotime($l['insertdate']);
			$key = $l['sort_fld'];
			while(isset($action_logs_merge[$key])) { $key++; }
			$action_logs_merge[$key] = $l;
			//$key++;
	   }
	   ksort($action_logs_merge);
/*
*/
       $table       = $this->drawActivityLog($action,$action_logs_merge,$id);
	   return $table;
	}
	function drawActivityLog($action,$action_logs, $id=0) {
		$nObj = new DeliverableNaming();
		$headers = $nObj-> getNaming(); 
		//ASSIST_HELPER::arrPrint($headers);
		$action_attachments = array();
		$table = "";
		if(!empty($action_logs)) {
			$current_progress = ASSIST_HELPER::format_percent(0,0);
			foreach($action_logs as $log_id => $action_log) {           
				$current_status = "";
				$user           = "";
				$change_str     = "";
				if(!empty($action_log['changes'])) {
					$z = $action_log['changes'];

					if(substr($z,0,2)=="a:") {
						$changes = $z;
						//$changes = array('response'=>$z);
						$changes = unserialize($changes);
						//$change_str = "not base64_decoded : ".$z."<br />";
					} else {
						$changes = $z;
						$changes = base64_decode($z);
						$changes = unserialize($changes);
						//$change_str = "IS base64_decoded : ".$z."<br />";
					}
					
					if(!empty($changes)) {
		
						if(isset($changes['deliverable_status']) ) {
							if(!is_array($changes['deliverable_status']) && strlen($changes['legislation_status'])>0) {
								$change_str.= $changes['deliverable_status'];
							//} elseif(is_array($changes['deliverable_status']) && isset($changes['deliverable_status']['to']) && isset($changes['deliverable_status']['from']) ) {
							//	$to = $changes['deliverable_status']['to'];
							//	$from = $changes['deliverable_status']['from'];
							//	$change_str.="deliverable_status changed to $to from $from";
							unset($changes['deliverable_status']);
							}
						}

						if(isset($changes['user'])) {
							$user  = $changes['user'];
							unset($changes['user']);
						}                      
						if(isset($changes['currentstatus'])) {
							$current_status  = $changes['currentstatus']."<br />";
							unset($changes['currentstatus']);
						}
						if(isset($changes['response'])) {
							$change_str  .= "Added response <span class=i>".$changes['response']."</span><br />";
							unset($changes['response']);
						}                    
						if(isset($changes['attachments']) && !empty($changes['attachments'])) {
							foreach($changes['attachments'] as $a_index => $att_str) {
								$change_str  .= $att_str;//."  last word ".$last_word."<br />";
							}
							unset($changes['attachments']);
						}
						if(!isset($changes['progress'])) {
							$current_progress = "";
						}
						if(!empty($changes)) {
							foreach($changes as $log_key => $change) {
								if(is_array($change) && isset($change['to']) && isset($change['from'])) {
									$from        = $change['from'];
									$to          = $change['to'];
									$key = "";
									if(isset($headers[$log_key])) {
										$key = strlen($headers[$log_key]['client_terminology'])>0 ? $headers[$log_key]['client_terminology'] : $headers[$log_key]['ignite_terminology'];
									} else {
										$key         = ucwords(str_replace("_", " ", $log_key));
									}
									if($log_key == "progress") {
										$current_progress = ASSIST_HELPER::format_percent($change['to']*1,0);
										$from = ASSIST_HELPER::format_percent($from*1,0);
										$to = ASSIST_HELPER::format_percent($to*1,0);
									}
									$change_str .= $key."  changed to <span class=i>".$to."</span> from <span class=i>".$from."</span><br />";
								} else {
									$change_str.=$change."<br />";
								}
							}
						} //endif last check for from-to changes
					} //endif first check for changes
				} //endif not empty action_log['changes']  
				if(strlen($change_str)>0) {
					$table .= "
					<tr>
						<td>".date("d-M-Y H:i:s", strtotime($action_log['insertdate']))."</td>
						<td>".$user."</td>
						<td>".str_replace(" ,",", ",$change_str)."</td>
						<td>".$current_status.(strlen($current_progress)>0 ? " (".$current_progress.")" : "")."</td>
					</tr>";
				}
			} //foreach action_logs
		}
		
       return $table;
    }	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
}
?>