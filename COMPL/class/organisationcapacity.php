<?php
class OrganisationCapacity extends Model
{
	
	protected static $tablename = "organisation_capacity";
     
     function getAll()
     {
       $clientObj = new ClientOrganisationCapacity();
       $clientcapacities = $clientObj -> fetchAll();
       
       $masterObj = new MasterOrganisationCapacity();
       $mastercapacities = $masterObj -> fetchAll();
       $list = array_merge($mastercapacities, $clientcapacities);
       $capacities = array();
       foreach($list as $index => $capacity)
       {
          $capacities[$capacity['id']] = $capacity;
       }
       return $capacities;
     }     
     

	function getCapacityByLegislationId( $id )
	{
		$results = $this->db->get("SELECT * FROM #_orgcapacity_legislation WHERE legislation_id = $id ");
		return $results;
	}

	function importCapacityByLegislationId( $id) 
	{
		$results = $this->db2->get("SELECT * FROM #_orgcapacity_legislation WHERE legislation_id = $id ");
		return $results;
	}
	
	function updateOrganisationalCapacities( $newCats , $id)
	{
		$del = $this->db->delete("orgcapacity_legislation", "legislation_id = '".$id."'");
		foreach( $newCats as $index => $catId)
		{
			$this->db->insert("orgcapacity_legislation", array("orgcapacity_id" => $catId, "legislation_id" => $id));
		}
	}		
	
	function getCapLeg( $capid )
	{
		$result = $this->db->getRow("SELECT orgcapacity_id, legislation_id 
											  FROM #_orgcapacity_legislation WHERE orgcapacity_id = '".$capid."'
											 ");
		return $result;
	}
}
