<?php
class ClientEvent extends Model
{
  
  function __construct()
  {
    parent::__construct();
  }
  
  function fetchAll($options = "")
  {
    $results = $this->db->get("SELECT id, name, description, status FROM #_events WHERE status & 2 <> 2 $options ");
    return $results;
  }
  
  function fetch($id)
  {
    $result = $this->db->getRow("SELECT id, name, description, status FROM #_events WHERE id = '".$id."' ");
    return $result;
  }

  function updateEvent($data)
  {
     $this -> setTablename("events");
     $id        = ClientSubEvent::getActualId($data['id']);
     $res       = $this -> update($id, $data, new SetupAuditLog(), $this, new ActionNaming());
     $response  = array();
     if(isset($data['status']) && $data['status'] == 2)
     {
        if($res > 0)
        {
            $response = array('text' => 'Event deleted successfully', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the event', 'error' => true);
        }
     } else {
        $response = $this -> updateMessage("event ", $res);
     }
     return $response;
  }
  
  function saveEvent($data)
  {
    $this->setTablename("events");
    $res  = $this->save($data);
    $response = $this->saveMessage("event", $res);
    return $response;    
  }

  function getEventUsed()
  {
    $results = $this -> db -> get("SELECT id, main_event FROM #_deliverable WHERE main_event > 9999 ");
    $deliverableEvents = array();
    if(!empty($results))
    {
      foreach($results as $index => $deliverable)
      {
         $deliverableEvents[$deliverable['main_event']] = $deliverable['main_event'];
      }
    }
    return $deliverableEvents;
  }
  

}
?>