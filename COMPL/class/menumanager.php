<?php
class MenuManager
{
	
	public static function createMenu($folder, $pagename, $level)
	{
	   $menuObj = null;
	   $menuLevel = "";
	   switch($level)
	   {
	     case Menu::TOP:
	          $menuObj = new UpperMenu($folder, $pagename);
	          $menuLevel = 1;
	     break;
	     case Menu::LOWER:
	          $menuObj = new LowerMenu($folder, $pagename);
	          $menuLevel = 2;
	     break;
	     case Menu::CUSTOM:
	       $menuObj = new LowerCustomMenu($folder, $pagename);
	       $menuLevel = 2;
	     break;
	   }
	   $menuItems  = $menuObj -> createMenu();
        $menuObj -> displayMenu($menuItems, $level);
	}
	/*protected $menu = null; 
	
	function getMenu(Menu $menuObj, $folder, $pagename)
	{
	  $menuObj -> displayMenu( $folder, $pagename );	
	}*/

}
