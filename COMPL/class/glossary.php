<?php
class Glossary extends Model
{
	
	protected static $table = "glossary";
	
     public function __construct()
     {
        parent::__construct();
     }

	function getGlossary($options = "")
	{
		$results = $this -> db -> get("SELECT id, category , purpose, rules,status FROM #_glossary WHERE status & 2 <> 2");
		return $results;
	}
	
	function fetch($id )
	{
		$result = $this->db->getRow("SELECT id, category , purpose, rules, field, status FROM #_glossary WHERE id = {$id} ");
		return $result;
	}
	
	function getRules($field)
	{    
	   $field = substr($field, 1);
	   $result = $this -> db -> getRow("SELECT G.rules FROM #_glossary G INNER JOIN #_header_names H ON H.id = G.field WHERE H.name = '".$field."' ");
	   $rulesStr = "";
	   if(!empty($result['rules']))
	   {
	      $rulesStr = utf8_encode($result['rules']);  
	   }
	   return $rulesStr;
	}
	
	function getGlossaries($categoryId = "")
	{
	   $optionSql = "";
	   if(!empty($categoryId))
	   {
	     $optionSql = " AND G.category = '".$categoryId."'";
	   }
	   $results = $this -> db -> get("SELECT G.id, G.category, G.purpose, G.rules, G.status, H.client_terminology, H.ignite_terminology 
	                                 FROM #_glossary G
	                                 LEFT JOIN #_header_names H ON G.field = H.id 
	                                 WHERE G.status & 2 <> 2 $optionSql 
	                                ");   
	   return $results;
	}
	
	function getCategoryGlossary($categoryId = "")
	{
        $results = $this -> getGlossaries($categoryId);
                        
        $table = "<form>";
          $table .= "<table>";
           $table .= "<tr>";
             $table .= "<th>Ref</th>";
             $table .= "<th>Field Name</th>";
             $table .= "<th>Purpose</th>";
             $table .= "<th>Rules</th>";
             $table .= "<th></th>";
           $table .= "</tr>";
           if(!empty($results))
           {
                foreach($results as $index => $glossary)
                {
                     $table .= "<tr id='tr_".$glossary['id']."'>";
                       $table .= "<td>".$glossary['id']."</td>";
                       $table .= "<td>".$glossary['client_terminology']."</td>";
                       $table .= "<td>".$glossary['purpose']."</td>";
                       $table .= "<td>".utf8_encode($glossary['rules'])."</td>";
                       $table .= "<td><input type='button' name='edit_".$glossary['id']."' id='edit_".$glossary['id']."' value='Edit' class='edit' />";
                         $table .= "<input type='button' name='delete_".$glossary['id']."' id='delete_".$glossary['id']."' value='Delete' class='delete' />";
                       $table .= "</td>";
                     $table .= "</tr>";
                }
           }
          $table .= "</table>";
        $table .= "</form>";
	   return $table;
	}
	
	function saveGlossary($data)
	{
       $res = $this -> save($data);
	  $response = array();
	  if($res > 0)
	  {
	     $response = array("text" => "Glossary successfully saved . . . ", "error" => false);
	  } else {
	    $response = array("text" => "Error saving glossary, please try again . . . ", "error" => true); 
	  }
	  return $response;   
	}
	
	function updateGlossary($data)
	{
	   $res 	  = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
	   $response = array();
	   if($res > 0)
	   {
	     $response = array("text" => "Glossary successfully updated . . . ", "error" => false);
	   } else if($res == 0) {
	     $response = array("text" => "No change made to the glossary . . . ", "error" => false); 
	   } else {
	     $response = array("text" => "Error updating glossary, please try again . . . ", "error" => true); 
	   }
        return $response;
	}
	
		
}
