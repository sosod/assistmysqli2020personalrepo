<?php
class LegislativeFrequency extends Model
{
	
	protected static $tablename = "legislation_frequency";
	
	function getLegislativeFrequency()
	{
		$result = $this->db2->get("SELECT id, name , description, status FROM #_legislative_frequency WHERE status & 2 <> 2");
		return $result;
	}

	function fetchAll()
	{
		$result = $this->db->get("SELECT id, name , description, status FROM #_legislative_frequency WHERE status & 2 <> 2");
		return $result;	
	}
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT id, name , description , status FROM #_legislative_frequency WHERE id = $id");
		return $result;
	}
	
}
