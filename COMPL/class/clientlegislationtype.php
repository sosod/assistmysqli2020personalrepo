<?php
/*
   Legislation types from the client database
*/
class ClientLegislationType extends LegislationType
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT id,name,description,status  FROM #_legislative_types 
                                       WHERE status & 2 <> 2 $options ORDER BY id, name $options");
        return $results;     
     }
     
	function fetch( $id )
	{
		$result  = $this -> db -> getRow("SELECT * FROM #_legislative_types WHERE id = $id");
		return $result;		
	}

	function searh($text)
	{
		$results = $this -> db -> get("SELECT id FROM #_legislative_types WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
		return $results;								  
	}	


	function updateLegislativeType( $data )
	{
		$this -> setTablename("legislative_types");
		$res 	   = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
		$response  = $this -> updateMessage("legislation", $res);
		return $response;
	}
	
	function saveLegislativeType( $data )
	{
	    $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_legislative_types');

	    if(!empty($get_last_id))
	    {
	      $next_id = $get_last_id['id'] + 1;
	      if($get_last_id['id'] < 10000)
	      {
	        $data['id'] = 10000 + $next_id;
	      } else {
	        $data['id'] = $next_id;  
	      }
	    }	  		
		$this -> setTablename("legislative_types");
		$legid 	  = $this -> save($data);
		$response = $this -> saveMessage("legislation_type", $legid); 
		return $response;
	} 

}
?>
