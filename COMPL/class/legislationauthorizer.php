<?php
class LegislationAuthorizer extends Model
{
     
     protected static $table = "legislation_authorizers";
     
     function __construct()
     {
         parent::__construct();
     }

     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT * FROM #_legislation_authorizers WHERE 1 $options");
        return $results;
     }
     
     function fetch($id)
     {
       $result = $this -> db -> getRow("SELECT * FROM #_legislation_authorizers WHERE id = '".$id."' ");
     }
     
     function isLegislationAuthorizor($userid)
     {
        $authorizers = $this -> db -> get("SELECT * FROM #_legislation_authorizers WHERE user_id = '".$userid."' ");
        if(!empty($authorizers))
        {
          return TRUE;
        }
        return FALSE;
     }
     
     function getLegislationAuthorizers($legislationId)
     {
        $results = $this -> fetchAll(" AND legislation_id = '".$legislationId."' ");
        $users   = array();
        if(!empty($results))
        {
          foreach($results as $index => $user)
          {
             $users[] = $user['user_id'];
          }
        }
        return $users;
     }
     
     function getUserLegislations($userid)
     {
        $results = $this -> fetchAll(" AND user_id = '".$userid."' ");
        $legislations = array();
        if(!empty($results))
        {
          foreach($results as $index => $legislation)
          {
             $legislations[] = $legislation['legislation_id'];
          }
        }
        return $legislations;
     }     

     function updateAuthorizers($authorizers, $legislationId)
     {
        $res = $this -> db -> delete("legislation_authorizers", "legislation_id={$legislationId}");
        if(is_array($authorizers))
        {
          foreach($authorizers as $index => $authorizer)
          {
             $res += $this -> db -> insert("legislation_authorizers", array("user_id" => $authorizer, "legislation_id" => $legislationId));
          }
        }
        return $res;
     }
 
     function getLegislations($optionSql = "")
     {
        $results = $this -> db -> get("SELECT LA.legislation_id, LA.user_id, L.financial_year FROM #_legislation_authorizers LA 
                                       INNER JOIN #_legislation L ON LA.legislation_id = L.id WHERE 1 $optionSql ");
        $legislations = array();
        if(!empty($results))
        {
          foreach($results as $index => $legislation)
          {
             $legislations[] = $legislation['legislation_id'];
          }
        }                            
        return $legislations;
     }
     
}
?>
