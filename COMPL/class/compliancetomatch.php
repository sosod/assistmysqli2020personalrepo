<?php
class ComplianceToMatch extends Model
{
     
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options = "")
     {
        $results  = $this->db->get("SELECT * FROM #_compliance_to_users WHERE status & 2 <> 2 $options ");
        return $results;
     }
     
     function fetch($id)
     {
        $result = $this->db->getRow("SELECT * FROM #_compliance_to_users WHERE id = '".$id."' ");
        return $result;
     }
     
	function saveComplianceTo( $data )
	{
		if(isset($data['usernotinlist']) && !empty($data['usernotinlist']))
		{
     		$data['status'] = 1 + SetupManager::NOUSER_IN_LIST;
		}
     	unset($data['usernotinlist']);
	     $this->setTablename("compliance_to_users");      	
		$res = $this->save( $data);
          if($res > 0)
          {
             $delCompObj = new DeliverableComplianceTo();
             $delCompObj -> updateComplianceToDeliverable($res, $data['ref']);    
          }		
		$response = $this->saveMessage("compliance_to", $res); 
		return $response;
	}	     
     
	function updateComplianceTo( $data )
	{
          $this->setTablename("compliance_to_users");      	
          $id 	= $data['id'];
          unset($data['id']);
          $res 	  = $this->update($id, $data, new ComplianceToAuditLog(), $this, new ActionNaming());
          $response = $this->updateMessage("compliance_to", $res);
          return $response;
	}		
	
	function deleteDeliverableComplianceTo($data)
	{
	   $id = $data['id'];
	   $matchData = $this -> fetch($id);
	   $ref = $matchData['ref'];
	   $updatedata = array("complianceto_id" => $ref, "status" => 1);
	   $this -> db -> updateData("complianceto_deliverable", $updatedata, array("complianceto_id" => $data['id'], "status & 4" => 4 ));
	   
         $auditLog = new ComplianceToAuditLog();
         $log  = $auditLog -> processChanges($this, $data, $id, new ActionNaming());     	   
         
        $res = $this -> db -> delete("compliance_to_users", "id={$id}"); 	   
        return $this -> updateMessage("compliance_to", $res);
     }	     
}
?>