<?php
/*
 * Defined how and what legislation to appear on the eventing section
 */
class EventLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getOptionSql($options = array())
	{
	   $optionSql = array();
	   $clientSql = "";
	   $masterSql = "";
	   $activeSql = "";
	   $activeSql .= " (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
				    OR L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
				    OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
		             ";	   
        if(isset($options['page']) && $options['page'] !== "reassign")
        {
          $activeSql .= " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
          //$optionSql = " AND legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
        } 
        if(isset($options['limit']))
        {
          $masterSql .= " LIMIT ".$options['start'].",".$options['limit'];
        }  
        
        $clientSql = " AND ((".$activeSql.") OR (L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED."))";
                   
        if(isset($options['financial_year']))
        {
          $clientSql .= " AND L.financial_year = '".$options['financial_year']."' ";
        } 
        $optionSql['master'] = $masterSql;
        $optionSql['client'] = $clientSql;
        return $optionSql;          
	}	
	
}
