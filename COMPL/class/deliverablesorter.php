<?php 
class DeliverableSorter extends Sorter
{
     public static function sortData($deliverables, $naming, $page="")
     {
      $data = array();
      $data['deliverable'] = array(); //holds the deliverable data
      $data['headers']     = array(); //holds the active deliverable headers for a give section
      $data['deliverableStatus'] = array();  //holds the deliverable statuses
      $data['events']      = array(); //holds the event that triggers the deliverable
      $data['isDeliverableOwner'] = array(); //holds status of the deliverable is current user is their owner
      $data['deliverableActionProgress'] = array(); //holds the deliverable progress
      $headers = $naming -> headersList();    
      
      if(!empty($deliverables))
      {
         foreach($deliverables as $index => $deliverable)
         {
             /*$progress = 0;
             if(isset(self::deliverablesProgress[$deliverable['ref']]))
             {
                $progress = round(intval(self::deliverablesProgress[$deliverable['ref']]), 2);
                $data['deliverableActionProgress'][$deliverable['ref']] = $progress;
             }*/  
             $data['isDeliverableOwner'][$deliverable['ref']] = Deliverable::isDeliverableOwner($deliverable['responsibility_owner']);
             //hold the deliverables sub-events information even if the sub-event is not displayed on the page
			//if a deliverable has already been created by the sub-event , then get the event that created it
			$subeventStr = "";
			/*
			if($this->deliverableObj->isCreatedSubevent($deliverable['status']))
			{
			     $eventOccuranceObj = new EventOccurance();
			     $eventStr          = $eventOccuranceObj -> fetch($deliverable['event_occuranceid']);
				$subeventStr    = (!empty($eventStr) ? $eventStr : "");        
			} else {
                   $subeventObj = new SubEventManager();
                   $subeventStr = $subeventObj -> matchSubEventToDeliverable($deliverable['ref']); 
			}*/
			$subeventStr = "";
		     $data['events'][$deliverable['id']]  = $subeventStr;		               
             //check if the deliverables keys are allowed in this section
             foreach($headers as $key => $head)
             {
               if($key == "action_progress")
               {
                  $data['headers'][$key]                          = $head;
                  $data['deliverable'][$deliverable['ref']][$key] = $progress;  
               } else if(array_key_exists($key, $deliverable)) {
                  $data['headers'][$key] = $head;
                  if($key == "sub_event")
                  {
                     $data['deliverable'][$deliverable['ref']][$key] = $subeventStr;	
                  } else if($key == "main_event") {
                     $eventObj  = new Event(); 
                     $mainevent = $eventObj -> getAll();
                     $eventStr = (isset($mainevent[$deliverable[$key]]) ? $mainevent[$deliverable[$key]]['name'] : "");
                     $data['deliverable'][$deliverable['ref']][$key] = $eventStr;
                  } else if($key == "applicable_organisation_type") {
                    $orgtypeObj = new OrganisationTypes();
                    $orgtype    = $orgtypeObj->getAll(); 		
                    $orgtypeStr = (isset($orgtype[$deliverable[$key]]) ? $orgtype[$deliverable[$key]]['name'] : "");	  	
                    $data['deliverable'][$deliverable['ref']][$key] = $orgtypeStr;                     
                  } else if($key == "responsible_department") {
                    $deptObj  = new DepartmentManager();
                    $deptStr =   $deptObj->getDeliverableDepartment($deliverable[$key], $deliverable['status']);      
                    $data['deliverable'][$deliverable['ref']][$key] = $deptStr; 
                  } else if($key == "functional_service") {
			  	 $funcObj = new FunctionalServiceManager();
	  			 $functionalStr     = $funcObj -> deliverableFunctionalService($deliverable[$key], $deliverable['status']);
	  			 $data['deliverable'][$deliverable['ref']][$key] = $functionalStr;
                  } else if($key == "frequency_of_compliance") {
                    $complianceFreqObj = new ComplianceFrequency();  
                    $complianceFreq = $complianceFreqObj -> getAll();
                    $freqStr = (isset($complianceFreq[$deliverable[$key]]) ? $complianceFreq[$deliverable[$key]]['name'] : "");
                    $data['deliverable'][$deliverable['ref']][$key] = $freqStr;
                  } else if($key == "compliance_name_given_to") {
                    $complObj 	= new CompliancetoManager();
                    $compliancetoStr  = $complObj->getDeliverableComplianceTo($deliverable['ref']);
                    $data['deliverable'][$deliverable['ref']][$key] = (!empty($compliancetoStr) ? $compliancetoStr : "");
                  } else if($key == "accountable_person") {
                    $accountableObj = new AccountablePersonManager();
                    $accountablesStr = $accountableObj->getDeliverableAccountablePerson($deliverable['ref']); 
                    $data['deliverable'][$deliverable['ref']][$key] = (!empty($accountablesStr) ? $accountablesStr : "");      
                  } else if($key == "responsibility_owner") {
                    $resownerObj  = new ResponsibleOwnerManager(); 
                    $ownerStr      = $resownerObj -> getDeliverableResponsiblePerson($deliverable[$key], $deliverable['status']);
                    $data['deliverable'][$deliverable['ref']][$key] = $ownerStr;
                  } else if($key == "compliance_date") {
                    if($deliverable['recurring'])
                    {
                      $type = Deliverable::recurringTypes($deliverable['recurring_type']);
                      $date = $deliverable['recurring_period']." ".(isset($deliverables['days_options']) ? $deliverables['days_options'] : "")." ".$type;
                      $data['deliverable'][$deliverable['ref']][$key] = $date;
                    } else {
                      $data['deliverable'][$deliverable['ref']][$key] = $deliverable[$key];
                    }    
                  } else {
                     $data['deliverable'][$deliverable['ref']][$key] = $deliverable[$key];
                  }  
               }
             }
         }
      }
      $data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
      $data['columns'] = count($data['headers']);
      return $data;   
          
     }     
     
}
?>