<?php
class ResponsibleOrganisations extends Model
{
     protected static $tablename = "organisation";

    function getResponsibleOrganisations()
    {
       $results = $this->db2->get("SELECT * FROM #_organisation WHERE status & 2 <> 2 ORDER BY id");
        return $results;
    }
    
    function getSortedResponsibleOrganisations()
    {
    	$compliances = $this->getResponsibleOrganisations();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = array("name" => $compliance['name'], "id" => $compliance['id'] );
		}
		return $compArr;
    }

    function fetch( $id )
    {
        $result = $this->db->getRow("SELECT * FROM #_organisation WHERE id = $id");
        return $result;
    }
    
    function fetchAll( $options = "")
    {
    	$sql = "";
    	if( $options == 1){
    		$sql = " AND status & 1 = 1";
    	}
    	$results = $this->db->get("SELECT id, name , description , status 
    							   FROM #_organisation
    							   WHERE status & 2 <> 2
    							   $sql 
    							   ORDER BY id
    							  ");
    	return $results;
    }


}
