<?php
class SubEvent extends Model
{
   
    protected static $tablename = "sub_event";
   
	function getAll($options = array())
	{
		$optionSql = "";
		$option2Sql = "";	     
		$fetchClient = true;
		$fetchMaster = true;
		if(isset($options['status']) && !empty($options['status']))
		{
		     $optionSql  = " AND SE.status & ".$options['status']." = ".$options['status'];
		     $option2Sql = " AND SE.status & ".$options['status']." = ".$options['status'];
		}
		
		if(isset($options['eventid']) && !empty($options['eventid']))
		{
		    if($options['eventid'] > 9999)
		    {
		       $eventid     = ($options['eventid']/10000);
		       $optionSql  .= " AND SE.main_event = ".$eventid."";
		       $fetchMaster = false;
		    } else {
               $option2Sql .= " AND SE.main_event = ".$options['eventid']."";		
               $fetchClient = false;          
		    }
		}

	    $cSubEvents = array();
	    $mSubEvents = array();
	    
	    $deliverableSubEventObj = new DeliverableSubEvent();
	    $_subevents             = $deliverableSubEventObj -> getClientEventsUsed();

	    if($fetchClient)
	    {
            $clientObj = new ClientSubEvent();
            $cSubEvents = $clientObj -> fetchAll($optionSql);	     
	    }
         if($fetchMaster)
         {
           $masterObj = new MasterSubEvent();
           $mSubEvents = $masterObj -> fetchAll($option2Sql);          
         }         
         $masterList = array();
         $clientList = array();
         
         foreach($cSubEvents as $cIndex => $cVal)
         {
           $fakeId = $cVal['id'] * 10000;
           $used   = false;
           if(isset($_subevents[$fakeId]))
           {
              $used = true;
           }        
            $clientList[$cVal['id']] = array("name"        => $cVal['name'],
                                             "status"      => $cVal['status'],
                                             "description" => $cVal['description'],
                                             "event"       => $cVal['eventname'],
                                             "eventid"       => $cVal['eid'],
                                             "imported"    => false,
                                             "id"          => $fakeId,
                                             "used"        => $used
                                             );
         }
         
         foreach($mSubEvents as $mIndex => $mVal)
         {
            $masterList[$mVal['id']] = array("name"        => $mVal['name'],
                                             "status"      => $mVal['status'],
                                             "description" => $mVal['description'],
                                             "event"       => $mVal['eventname'],
                                             "eventid"       => $mVal['eid'],
                                             "imported"    => true,
                                             "id"          => $mVal['id'],
                                             "used"        => true
                                             );
         }
         $subeventList = array_merge($masterList, $clientList);
         $subevents    = array();
         foreach($subeventList as $index => $subevent)
         {
            $subevents[$subevent['id']] = $subevent;
         }
         return $subevents;         
	}
	
	function getList($options = array(), $flip = FALSE)
	{
	   $subevents    = $this -> getAll($options);
	   $subEventList = array();
	   foreach($subevents as $sIndex => $sVal)
	   {
	      if($flip)
	      {
	        $subEventList[$sVal['name']] = $sVal['id'];
	      } else {
	        $subEventList[$sVal['id']]   = $sVal['name'];
	      }
	   }
	   return $subEventList;
	}
	

		
}
?>