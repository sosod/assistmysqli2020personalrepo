<?php
class NewAction extends ActionManager
{
	
	function __construct()
	{
		parent::__construct();	
	}	
	
	function getOptions($options = array())
	{
	   $optionSql = "";
	   if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
	   {
	     $optionSql .= " AND A.deliverable_id = '".$options['deliverable_id']."' ";
	   }
	   
	   if(isset($options['limit']) && !empty($options['limit']))
	   {
	     $optionSql .= " LIMIT ".$options['start']." , ".$options['limit']."";
	   } 
	   return $optionSql;
	}
	
	
	/*function getActions( $start, $limit, $options = array())
	{
		$optionSql = "";
		if(isset($options['deliverable_id']))
		{
		   $this->deliverableId = $options['deliverable_id'];
	        $optionSql = " AND A.deliverable_id = '".$options['deliverable_id']."'";
		}	
		$actions  = $this->actionObj->fetchAll($start,$limit, $optionSql);
		$total    = $this->actionObj->fetchActions($options['deliverable_id']);
		$response = $this->sortActions($actions, new ActionNaming("new"));
		$response['totalActions'] = $total['totalActions'];	
		return $response;				
	}

	function importDeliverableActions($id, $deliverable_ref, $financial_year, $finyearObj)
	{
		$actObj  = new Action();
		$actions = $actObj -> importActions($deliverable_ref);
		$countSaved    = 0;
		$countNotSaved = 0;
		$response 	   = array();
		if(!empty($actions))
		{			
			foreach($actions as $index => $action)
			{
				$action['deliverable_id']	 = $id;
				$action['action_reference']  = $action['id'];
				$action['action_status']	 = Action::ACTION_ACTIVE + Action::ACTIVE_IMPORTED;
				$action['owner']          = (!empty($action['owner']) ? $action['owner'] : 1);
				$actionDeadlineDate       = $finyearObj -> createFinancialYearDate($financial_year, $action['deadline']);
				$action['deadline']       = $actionDeadlineDate;   
				unset($action['id']);
				$actObj->setTablename("action");
				if(($result = $actObj->save($action)))
				{
					//save the version of the current action that is in setup 1 , if the action had been updated that side
					$latestactionversion = $actObj -> getLatestVersion( $action['action_reference'] );
					$usagearray 		 = array();	
					if(!empty($latestactionversion))
					{
						$usagearray = array("action_id" => $action['action_reference'], "current_version" => $latestactionversion['id'], "version_in_use" => $latestactionversion['id']);		
					} else{
						$usagearray = array("action_id" => $action['action_reference'], "current_version" => 0, "version_in_use" => 0);
					}				
					$actObj -> setTablename("action_usage");
					$actObj -> save( $usagearray );
					$countSaved++;
				} else {
					$countNotSaved++;
				}
			}
			return array("saved" => $countSaved, "notSaved" => $countNotSaved);
		}
		
	}
	
	function copySaveAction($data)
	{
		$response = array();
		// if remind on date is greater than the deadline date
		if(isset($data['actiontype']))
		{
		  if($data['actiontype'] == "manage")
		  {
		     $manageAddAction = TRUE;
		     $data['actionstatus'] = Action::MANAGE_CREATED + 1;
		  }  
		  unset($data['actiontype']);
		}
		if(!Validator::validStartEndDate($data['reminder'], $data['deadline']))
		{
		  $response = array("text" => "The remind on date cannot be after the deadline date", "error" => true);						
		} else {		
		  $this->actionObj->setTablename("action");
		  $res = $this->actionObj->save($data);
		  if($res > 0) 
		  {
			$response = array("text" => "Action copied and saved successfully . . .", "error" => false);	
		  } else {
			$response = array("text" => "Error saving action . . .", "error" => true);
		  }		
		}
		return $response;
	}

     function getEditAction( $data )
	{
		$action = $this->actionObj->fetch( $data['actionid']);
		$namingObj = new ActionNaming();
		$headers	  = $namingObj->getHeaderList();
		
		$usersObj = new User();
		$owners   = $usersObj->getList();	
		$form = new Form();
		$table = "<form method='post'>";
			$table .= "<table>";
			$table .= "<tr>";
				$table .= "<td colspan='2'><div id='actionmessage' style='padding:5px;'></div></td>";
			$table .= "</tr>";	
			if($this->_isDisplayableField($data, "action"))
			{				
			     $table .= "<tr>";
				     $table .= "<th>".$headers['action']."</th>";
				     $table .= "<td>".$form->renderTextarea("_actionname", $action['action_required'])."</td>";
			     $table .= "</tr>";
			}
			if($this->_isDisplayableField($data, "owner"))
			{		
		        $table .= "<tr>";
			     $table .= "<th>".$headers['owner']."</th>";			    
	               if(Action::isUsingUserId($action['actionstatus']))
	               {
				  $table .= "<td>".$form->renderSelect("_actionowner", $owners, $action['owner'])."</td>";	                  
	               } else {
				  $table .= "<td>".$form->renderSelect("_actionowner", $owners, "")."<small>Linked to <em><a href=''>Unspecified</em><small></td>";
	               }			
			   $table .= "</tr>";	
			}
			if($this->_isDisplayableField($data, "action_deliverable"))
			{									
			     $table .= "<tr>";
				     $table .= "<th>".$headers['action_deliverable']."</th>";
				     $table .= "<td>".$form->renderTextarea("_deliverable", $action['action_deliverable'])."</td>";
			     $table .= "</tr>";	
               }
			if($this->_isDisplayableField($data, "deadline"))
			{				               					
			     $table .= "<tr>";
				     $table .= "<th>".$headers['deadline']."</th>";
				     $table .= "<td>".$form->renderDateField("_deadline", $action['deadline'])."</td>";
			     $table .= "</tr>";
			}
			if($this->_isDisplayableField($data, "reminder"))
			{																
			     $table .= "<tr>";
				     $table .= "<th>".$headers['reminder']."</th>";
				     $table .= "<td>".$form->renderDateField("_reminder", $action['reminder'])."</td>";
			     $table .= "</tr>";
			}
               if($this->_isDisplayableField($data, "recurring"))
               {				
			     $table .= "<tr>";
				     $table .= "<th>Recurring</th>";
				     $table .= "<td>".$form->renderCheckbox("recurring",  FALSE)."</td>";
			     $table .= "</tr>";	
			}										
			$table .= "</table>";
		$table .= "</form>";
		return $table;
	}
	
	function _isDisplayableField($options , $field)
	{
	   $reAssignFields = array("action_owner");
	   
	   if(isset($options['reAssignAction']))
	   {
	     
	     if($options['reAssignAction'] == 1)
	     {   
           if(in_array($field, $reAssignFields))
           {
              return TRUE;
           }
	     } else {
	          return TRUE;
	     }
	   }
	   return FALSE;
	}*/
}
?>
