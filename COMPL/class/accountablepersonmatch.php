<?php
/*
Matches between the accountable person title and users
*/
class AccountablePersonMatch extends Model
{
   
   function __construct()
   {
     parent::__construct();
   }
   
   function fetchAll($options="")
   {
     $results = $this -> db -> get("SELECT * FROM #_accountable_person_users WHERE status & 2 <> 2 $options");
     return $results;
   }
   
   function fetch($id)
   {
     $result = $this -> db -> getRow("SELECT * FROM #_accountable_person_users WHERE id = '".$id."' ");
     return $result;
   }
   
   function getUserAccountableDeliverable($option_sql = "")
   {
     $results  = $this -> db -> get("SELECT APU.*, APD.* FROM #_accountable_person_users APU 
                                     INNER JOIN #_accountableperson_deliverable APD ON APU.id = APD.accountableperson_id
                                     WHERE 1 $option_sql
                                   ", false);
     return $results;                                
   }

   function getList()
   {
     $list = array();
     $accPersons = $this -> fetchAll();
     foreach($accPersons as $index => $accPerson)
     {
        $list[$accPerson['id']] = $accPerson;
     }
     return $list;
   }

   function updateAccountable($data)
   {
	$this->setTablename("accountable_person_users");
	$id 	    = $data['id'];
	unset($data['id']);
	$res 	    = $this -> update($id, $data, new AccountablePersonAuditLog(), $this, new ActionNaming());
	$response   = $this -> updateMessage("accountable_person", $res);
	return $response;
   }
   
   function deleteAccountablePersonMatch($data)
   {
     $id    = $data['id'];
     $match = $this -> fetch($id);
     $ref   = $match['ref'];
     //when the match is deleted , also remove and update the deliverable accountable person to where it was at first
     $this -> db -> updateData("accountableperson_deliverable", array("status" => 1, "accountableperson_id" => $ref), array("accountableperson_id" => $id));
    $auditLog = new AccountablePersonAuditLog();
    $log  = $auditLog -> processChanges($this, $data, $id, new ActionNaming());     
     $res = $this -> db -> delete("accountable_person_users", "id=$id");
     return $this -> updateMessage("accountable_person", $res);
   }
	
   function saveAccountablePerson($data)
   {
     $this->setTablename("accountable_person_users");
     if(isset($data['usernotinlist']) && !empty($data['usernotinlist']))
     {
        $data['status'] = 1 + SetupManager::NOUSER_IN_LIST;
     }
     unset($data['usernotinlist']);
     $res = $this->save( $data);
     if($res > 0)
     {
        $delAccObj = new DeliverableAccountablePerson();
        $delAccObj -> updateAccountablePersonId($res, $data['ref']);
     }
     $response = $this->saveMessage("accountable_person", $res); 
     return $response;
   }
			

}
?>