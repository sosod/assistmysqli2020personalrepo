<?php
class LegislativeCategories extends Model
{
	
	protected static $tablename = "legislation_categories";
	
	function __construct()
	{
		parent::__construct();
	}

	function fetchAll( $options = "")
	{
		$results = $this->db->get("SELECT * FROM #_legislative_categories WHERE status & 2 <> 2 $options ORDER BY id, name");
		return $results;		
	}
	
	function fetch( $id )
	{
		$result  = $this->db->getRow("SELECT * FROM #_legislative_categories WHERE id = $id");
		return $result;
	}
	
	function getLegislativeCategory( $ids )
	{
		$result   = $this->db->get("SELECT * FROM #_legislative_categories WHERE id IN( ".$ids." )");
		return $result;	
	}
	
	function getCategoryByLegislationId( $id )
	{
		$results = $this->db->get("SELECT * FROM #_category_legislation WHERE legislation_id = $id");
		return $results;
	}

	
	function searchCategories( $text )
	{
		$results = $this->db->get("SELECT id FROM #_legislative_categories WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%'  ");
		return $results;
	}

	
	function updateCategories( $newCats , $id)
	{
		$del = $this->db->delete("category_legislation", "legislation_id = '".$id."'");
		foreach( $newCats as $index => $catId)
		{
			$this->db->insert("category_legislation", array("category_id" => $catId, "legislation_id" => $id));
		}
	}
	
	function saveLegislativeCategory( $data )
	{
		$this->setTablename("legislative_categories");
		$legid 	 = $this->save($data);
		$response = $this->saveMessage("legislative_category", $legid); 
		return $response;
	}
	
	function updateLegislativeCategory( $data )
	{
		$this->setTablename("legislative_categories");
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this ,new ActionNaming());
		$response = $this->updateMessage("legislative_category", $res);
		return $response;
	}	

	function getLegCat($id)
	{
	   $legcategory = $this->db->getRow("SELECT category_id, legislation_id FROM #_category_legislation WHERE category_id = '".$id."' ");
        return $legcategory;
	}	
}
