<?php
class MasterResponsibleOwner extends Model
{
     
     function __construct()
     {
        parent::__construct();          
     }
     
     function fetchAll()
     {
       $results = $this->db2->get("SELECT id, name, status FROM #_responsible_owner WHERE status & 2 <> 2 ORDER BY name");
       return $results;
     }
     
     function fetch($id)
     {
       $results = $this->db2->getRow("SELECT * FROM #_responsible_owner WHERE id = '".$id."'");
       return $results;
     }


}
?>