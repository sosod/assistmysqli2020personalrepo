<?php
/*
 Display the deliverable 
*/
class DeliverableEditDisplay extends HtmlResponse
{
     
     private $deliverableId;
     
     private $deliveable = array();
     
     private $deliverableObj;
     
     function __construct($deliverableid = "")
     {
          $this -> deliverableId = $deliverableid;
          $this -> deliverableObj = new ClientDeliverable();
     }
     
     function _getDeliverble()
     { 
         if(!empty($this -> deliverableId))
         {
            $this -> deliverable = $this -> deliverableObj -> fetch($this -> deliverableId);
         }
     }
     
     
     function displayForm(Form $form, Naming $namingObj, $options = array())
     {
          $this -> _getDeliverble();
          $headers    = $namingObj -> getNaming();
          //get the compliance frequencies
          $complianceFrequencyObj = new ComplianceFrequency();
          $complianceFrequecies   = $complianceFrequencyObj -> getList();
     
          //get the organisation types 
          $organisationTypesObj = new OrganisationType();
          $organisationTypes    = $organisationTypesObj -> getList();

          //get the departments
          $deptObj       = new ClientDepartment();
          $departmentObj = new DepartmentManager();
          $departments 	 = $deptObj -> getList(); 
     
          //get the functional services
          $functionalServiceObj = new FunctionalServiceManager();
          $functionals          = $functionalServiceObj -> getClientFunctionalServices(); 
          //$fnObj = new FunctionalServiceManager();
          //$deliverableFunctionalServiceId = $fnObj -> getFunctionalServiceToUse();
          
          //get accoountable persons list and deliverable accountable persons          
          $accObj 			  = new AccountablePersonManager();
          $accountablePersons = $accObj -> getList();
          
          $deliverableAccountablePersons = $accObj -> getDeliverableAccountablePersonIds($this->deliverableId);
          //get all the compliances to 
          $comptoObj      = new ComplianceToManager(); 
          $complianceTo   = $comptoObj->getList();
          $delCompTo      = $comptoObj->getDeliverableComplianceToIds($this->deliverableId);
     
          //get all the users who are active in the module
          $responsownerObj = new ResponsibleOwnerManager();
          $userObj = new User();
          $userList = $userObj->getList();
          
          //get the subevent deliverable relationships 
          $subeventobj = new SubeventManager();
          $subObj = new SubEvent(); 
          $subEvents   = $subObj->getList(array("eventid" => $this->deliverable['main_event']));
          $deliverableSubEvents = $subeventobj->getDeliverableSubEvents($this->deliverableId);      
          
          //get all the reporting categories
          $reportingCatObj = new ReportingCategories();
          $reportingCategories = $reportingCatObj->getList();

          //get all the main events
          $eventsObj  = new Event();
          $mainEvents = $eventsObj -> getList();
		$table = "<form method='post' id='edit-deliverable' name='edit-deliverable'>";
		$table .= "<div id='message' style='padding:5px;'></div>";
		$table .= "<table width='80%'>";			
		if($this -> deliverableObj -> isEventActivated($this -> deliverable['status']))
		{
			$table .= "<tr>";
				$table .= "<td></td>";
				$table .= "<td style='text-align:right;'>This deliverable is active ".$form -> renderCheckbox("confirmavailable	", $this->deliverableObj->isConfirmed($this->deliverable['status']))."</td>";
			$table .= "</tr>";		
		}
		//don't show if its a quick edit
		if($this->_isDisplayable($options, "id"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("id")."</th>";
			     $table .= "<td>".$this->deliverableId."</td>";
		     $table .= "</tr>";
		 }
		 if($this->_isDisplayable($options, "short_description"))
		 {
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("short_description")."</th>";
			     $table .= "<td>".$form->renderTextarea("short_description", $this->deliverable['short_description'])."</td>";
		     $table .= "</tr>";
		 }
		 if($this->_isDisplayable($options, "description"))
		 {
		    $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("description")."</th>";
			     $table .= "<td>".$form->renderTextarea("description", $this->deliverable['description'])."</td>";
		     $table .= "</tr>";
		  }
		  if($this->_isDisplayable($options, "legislation_section"))
		  {
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("legislation_section")."</th>";
			     $table .= "<td>".$form->renderText("legislation_section", $this->deliverable['legislation_section'])."</td>";
		     $table .= "</tr>";
		   }
		   if($this->_isDisplayable($options, "compliance_frequency"))
		   {
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("compliance_frequency")."</th>";
			     $table .= "<td>".$form->renderSelect("compliance_frequency", $complianceFrequecies, $this->deliverable['compliance_frequency'])."</td>";
		     $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "applicable_org_type"))
           {
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("applicable_org_type")."</th>";
			     $table .= "<td>".$form->renderSelect("applicable_org_type", $organisationTypes, $this->deliverable['applicable_org_type'])."</td>";
		     $table .= "</tr>";              
           }
           if($this->_isDisplayable($options, "compliance_date"))
           {
		          $table .= "<tr>";
		          $table .= "<th>".$namingObj -> getHeader("compliance_date")."</th>";
			          //$table .= "<td>".$form->renderText("compliance_date", $deliverable['compliance_date'])."</td>";
			          $table .= "<td>";
				          $table .= "<table>";
					          $table .= "<tr>";
					          $table .= "<td>Is the deadline date recurring within the financial year</td>";
					          $table .= "<td>".$form->renderSelect("recurring", array(1 => "Yes", 0 =>"No"), $this->deliverable['recurring'])."</td>";
					          $table .= "</tr>";
					          $table .= "<tr id='days' class='recurr nonfixed' style='display:".($this->deliverable['recurring'] == 1 ? 'table-row' : 'none').";'>";
					          $timeperiod = "";
					          $period     = "";  
					          if(!empty($this->deliverable['recurring_period']))
					          {
					               $recurringPeriod = explode(" ", $this->deliverable['recurring_period']);
					               if(!empty($recurringPeriod) && is_array($recurringPeriod))
					               {
					                 $timeperiod = (isset($recurringPeriod[0]) ? $recurringPeriod[0] : "");
					                 $period = (isset($recurringPeriod[1]) ? $recurringPeriod[1] : "");
					               }
					          }
					          
					          //$rperiod = substr($this->deliverable['recurring_period'], 0, strpos($this->deliverable['recurring_period'], "           "));
					          //$period = substr($this->deliverable['recurring_period'], strpos($this->deliverable['recurring_period'], " ")+1);
					          $table .= "<td>".$form->renderText("recurring_period", $timeperiod)."</td>";
					          //$table .= "<td>".$form->renderSelect("recurring", array(1 => "Yes", 0 =>"No"), $deliverable['recurring'])."</td>";
					          $table .= "<td colspan='2'>".$form->renderSelect("period", array("weekly" => "Working Days", "days" =>"Days"), $period)."</td>";
					          $table .= "</tr>";					
					          $table .= "<tr class='recurr' style='display:".($this->deliverable['recurring'] == 1 ? 'table-row' : 'none').";'>";
					          $table .= "<td>Days From :</td>";
					          $table .= "<td>".$form->renderSelect("recurring_type", Deliverable::recurringTypes(), $this->deliverable['recurring_type'])."</td>";
					          $table .= "<td></td>";
					          $table .= "</tr>";	
					          $table .= "<tr class='fixed' style='display:".($this->deliverable['recurring'] == 0 ? 'table-row' : 'none').";'>";
					          $table .= "<td>Fixed</td>";
					          $table .= "<td>".$form->renderDateField("compliance_date", $this->deliverable['compliance_date'])."</td>";
					          $table .= "</tr>";															
				          $table .= "</table>";
			          $table .= "</td>";
		          $table .= "</tr>";
	     }
	     if($this->_isDisplayable($options, "responsible"))
	     {
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("responsible")."</th>";

                   if(!(Deliverable::isUsingClientDepartment($this -> deliverable['status'])))
                   {
                      $masterDepts  = $departmentObj -> getMasterDepartments();
                      $clientObj    = new ClientDepartment();
                      $clientDepart = $clientObj -> getAll();                         
                      $deptStr      = $departmentObj -> getDeliverableDepartment($this -> deliverable['responsible'], $this -> deliverable['status'], $clientDepart, $masterDepts);
                      $table .= "<td>".$form -> renderSelect("responsible", $departments, "")."<small>Linked to  <a href=''>".$deptStr."</a></small></td>";
                   } else {
			     $table .= "<td>".$form -> renderSelect("responsible", $departments, $this -> deliverable['responsible'])."</td>";
			    }
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "functional_service"))
		{
	       $table .= "<tr>";
		  $table .= "<th>".$namingObj -> getHeader("functional_service")."</th>";
            if(!(Deliverable::isUsingClientFunctionalService($this -> deliverable['status'])))
            {
              $fxnId = $this->deliverable['functional_service'];
              $fxntitles     = $functionalServiceObj -> getFunctionalServicesTitles();              
              $fxnServices = $functionalServiceObj -> getClientFunctionalServices();              
              $funcStr = $functionalServiceObj -> deliverableFunctionalService($fxnId, $this -> deliverable['status'], $fxntitles, $fxnServices);
              $table .= "<td>".$form->renderSelect("functional_service", $functionals, "")." <small>Linked to  <a href=''>".$funcStr."</a></small></td>";
            } else {
		     $table .= "<td>".$form->renderSelect("functional_service", $functionals, $this->deliverable['functional_service'])."</td>";
		  }
	       $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "accountable_person"))
		{
		     $table .= "<tr>";
			  $table .= "<th>".$namingObj -> getHeader("accountable_person")."</th>";
			  $table .= "<td>".$form->renderMultipleSelect("accountable_person", $accountablePersons, $deliverableAccountablePersons)."</td>";
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "responsibility_owner"))
		{
		  $table .= "<tr>";
	          $table .= "<th>".$namingObj -> getHeader("responsibility_owner")."</th>";
		      $deliverablestatus = $this -> deliverableObj -> isUserStatusUptoDate($userList, $this -> deliverable);	         
              if(!(Deliverable::isUsingUserId($deliverablestatus)))
              {
                $userObj   = new User();
                $users     = $userObj -> getList();    
                $ownerTitles = $responsownerObj -> getResponsibleOwnerTitles();                 
                $ownerId = $this -> deliverable['responsibility_owner'];
                $ownerStr = $responsownerObj -> getDeliverableResponsiblePerson($ownerId, $this -> deliverable['status'], $users, $ownerTitles);
                $table .= "<td>".$form->renderSelect("responsibility_owner", $userList, "")." <small>Linked to  <a href=''>".$ownerStr."</a></small></td>";
              } else {
			 $table .= "<td>".$form->renderSelect("responsibility_owner", $userList, $this->deliverable['responsibility_owner'])."</td>";
		    }			     	     
		  $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "legislation_deadline"))
		{		
               $table .= "<tr>";
               $table .= "<th>".$namingObj -> getHeader("legislation_deadline")."</th>";
               $table .= "<td>".$form->renderDateField("legislation_deadline", $this->deliverable['legislation_deadline'])."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "sanction"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$namingObj -> getHeader("sanction")."</th>";
                    $table .= "<td>".$form->renderTextarea("sanction", $this->deliverable['sanction'])."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "assurance"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$namingObj -> getHeader("assurance")."</th>";
                    $table .= "<td>".$form->renderTextarea("assurance", $this->deliverable['assurance'])."</td>";
               $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "compliance_to"))
		{
		     $table .= "<tr >";
			  $table .= "<th>".$namingObj -> getHeader("compliance_to")."</th>";
                 $table .= "<td>".$form->renderMultipleSelect("compliance_to", $complianceTo, $delCompTo)."</td>";
		     $table .= "</tr>";																							
		}
        if($this->_isDisplayable($options, "main_event"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("main_event")."</th>";
	             $table .= "<td>";
	               $table .= $form -> renderSelect("main_event", $mainEvents, $this->deliverable['main_event']);
	               $table .= $form -> renderHidden("mainevent", $this->deliverable['main_event'])."";
	             $table .= "</td>";
		     $table .= "</tr>";																							
		}
		if($this->_isDisplayable($options, "sub_event"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("sub_event")."</th>";
			     $table .= "<td>";
			       $table .= $form->renderMultipleSelect("sub_event",  $subEvents, $deliverableSubEvents);
			     $table .= "</td>";
		     $table .= "</tr>";																							
		}
		if($this->_isDisplayable($options, "reporting_category"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("reporting_category")."</th>";
			     $table .= "<td>".$form->renderSelect("reporting_category", $reportingCategories, $this->deliverable['reporting_category'])."</td>";
		     $table .= "</tr>";																							
		}		
		if($this->_isDisplayable($options, "guidance"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$namingObj -> getHeader("guidance")."</th>";
			     $table .= "<td>".$form->renderTextarea("guidance", $this->deliverable['guidance'])."</td>";
		     $table .= "</tr>";	
		}
		if($this->_isDisplayable($options, "buttons"))
		{	
               $table .= "<tr>";
                  $table .= "<th></th>";
                  $table .= "<td>".$form->renderHidden("deliverableid", $this->deliverableId)." ".$form->renderHidden("legislationid", $this->deliverable['legislation'])."
                  ".$form->renderButton("edit", "Edit", array('class' => 'isubmit'))." ".$form->renderButton("cancel", "Cancel")."</td>";
               $table .= "</tr>"; 
          }                          
          /*
          if($this->_isDisplayable($options, "auditlog"))
		{	
               $table .= "<tr>";
                  $table .= "<td>".displayGoBack("", "")."</td>";    
                  $table .= "<td>".displayAuditLogLink("deliverable_edit", false)."</td>";
               $table .= "</tr>";   	          
          }
          */
		$table .= "</table>";	
		$table .= "</form>";
		$this->showResponse($table); 
     }     
     
	private function _isDisplayable($options,  $field)
	{   
	     $reassignFields = array("responsibility_owner");
	     //$quickEditFields = array("id", "ref", "responsible", "functional_service", "accountable_person", "responsibility_owner", "compliance_to", "reporting_category");
	     $quickEditFields = array("id", "ref", "compliance_frequency", "responsible", "functional_service", "accountable_person", "responsibility_owner", "compliance_to", "reporting_category");
	     $manageEditable = array("id", "ref", "compliance_frequency", "compliance_date", "responsible", "functional_service", "accountable_person", "responsibility_owner", "assurance", "compliance_to", "main_event", "sub_event", "reporting_category", "buttons", "auditlog");
	     
	     $adminEditable = array("id", "ref", "compliance_frequency", "accountable_person", "responsibility_owner", "compliance_to", "main_event", "sub_event", "reporting_category", "buttons", "auditlog");	
	     //var_dump($this->deliverableObj->isManageCreated($this->deliverable['status']);
	     //echo $this -> deliverable['status']." --  ".($this -> deliverable['status'] & Deliverable::MANAGE_CREATED)."<br />";
	     if($this -> deliverableObj -> isManageCreated($this -> deliverable['status']))
	     {
	          return TRUE;
	     } else {   
	          if(!empty($options))
	          {
	              if(isset($options['section']) && $options['section'] == "support")
	              {
                    return TRUE;
	              } else {
                    if(isset($options['reAssignDeliverable']) && $options['reAssignDeliverable'] == 1)
                    {
                        if(in_array($field, $reassignFields))
                        {
                             return TRUE;
                        }    
                    }
                    if(isset($options['quickEdit']) && $options['quickEdit'] == 1)
                    {
                        if(in_array($field, $quickEditFields))
                        {
                             return TRUE;
                        }    
                    }	  

                    if(isset($options['manageEditable']) && $options['manageEditable'] == 1)
                    {
                        if(in_array($field, $manageEditable))
                        {
                             return TRUE;
                        }    
                    }
                    if(isset($options['adminEditable']) && $options['adminEditable'] == 1)
                    {
                        if(in_array($field, $adminEditable))
                        {
                             return TRUE;
                        }    
                    }	                
	              }
	               return FALSE;  	     
	          } else {
	               return TRUE;
	          }
	     }
	}     
     
     
}
?>