<?php
class ClientActionOwner extends Model
{
     
     function __construct()
     {
       parent::__construct();
     }     
     
     function fetchAll($options = "")
     {
        $results = $this->db->get("SELECT id, name, status FROM #_action_owner WHERE status & 2 <> 2 $options");
        return $results; 
     }
     
     function fetch($id)
     {
        $result = $this->db->getRow("SELECT id, name, status FROM #_action_owner WHERE id = '".$id."' ");
        return $result;
     }

	function savenewActionOwner($data)
	{
		$this->setTablename("action_owner");
		$res = $this->save($data);
		$response = $this->saveMessage("new_action_owner_title", $res);
		return $response;
	}}
?>