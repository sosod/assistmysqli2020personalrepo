<?php
/*
 Accountable Persons from the client databse
*/
class ClientAccountablePerson extends Model
{
   
   function __construct()
   {
     parent::__construct();
   }
   
   function fetchAll($options = "")
   {
     $results = $this->db->get("SELECT id, name, status FROM #_accountable_person WHERE status & 2 <> 2 $options ORDER BY name");
     return $results;
   }
   
   function fetch($id)
   {
     $result = $this->db->getRow("SELECT id, name, status FROM #_accountable_person WHERE id = '".$id."' ");
     return $result;
   }
	
   function savenewAccountablePerson( $data )
   {
     $this->setTablename("accountable_person");
     $res = $this->save( $data);
     $response = $this->saveMessage("new_accountable_person_title", $res); 
     return $response;
   }	
   
 
	
} 
?>