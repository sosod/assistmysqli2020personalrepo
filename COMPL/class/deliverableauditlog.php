<?php
class DeliverableAuditLog implements Auditlog
{

     private $currentStatus = "";

	function processChanges($obj, $postArr, $id, Naming $naming)
	{
		$deliverable   = $obj -> fetch( $id );
		$tablename     = $obj -> getTablename();	
		$changeMessage = "";
		$changeArr	   = array();
        $usersObj      = new User();
        $users         = $usersObj -> getAll();
        $status        = $deliverable['status'];
        $_delStatus    = $postArr['status'];
		foreach($postArr as $key => $value)
		{
			if($key ==  "multiple")
			{
				if(isset($value["accountable_person"])) 
				{
				   //process accountable person changes
				   $accountableChange = $this -> _getAccountablePersonChanges($id, $value['accountable_person']);
				   if(!empty($accountableChange))
				   {
				     $changeArr['accountable_person'] = $accountableChange;
				   }
				} 
 				if(isset($value["compliance_to"]))
 				{						
 				   //process compliance to changes
 	                  $complianceToChange = $this -> _getComplianceToChanges($id, $value['compliance_to']);
 	                  if(!empty($complianceToChange))
 	                  {
 	                    $changeArr['compliance_to'] = $complianceToChange;
 	                  }
				}
				if(isset($value["sub_event"])) 
				{
				   //process sub-event changes
	                  $subeventChange = $this -> _getSubeventChange($id, $value['sub_event']);
	                  if(!empty($subeventChange))
	                  {
	                     $changeArr['sub_event'] = $subeventChange;
	                  }
				}
			} else {
				if(array_key_exists($key, $deliverable))
				{
				     if($key == "responsible") 
				     {
                        $departmentChanges = $this -> _getDepartmentChanges($deliverable[$key], $value, $deliverable['status']);
                        if(!empty($departmentChanges))
                        {
                          //update the deliverable status so that it now uses the client department id 
                          $deliverable['status']  = $status;
                          $deptStatus             = $obj -> updateDeliverableDepartmentStatus($deliverable);
                          //echo "Status of the deliverable b4 dept changes ".$status." and after dept change ".$deptStatus."\r\n\n";
                          $_delStatus            += ($deptStatus - $status);
                          $deliverable['status']  = $deptStatus;
                          $changeArr['responsible_department'] = $departmentChanges;
                        }
					} elseif($key == "functional_service") {
                        $functionalServiceChanges = $this -> _getFunctionalServicesChanges($deliverable[$key], $value, $deliverable['status']);
                        if(!empty($functionalServiceChanges))
                        {
                          $deliverable['status']  = $status;
                          $functionalStatus = $obj -> updateDeliverableFunctionalServiceStatus($deliverable);
                          //echo "Status of the deliverable b4 functional changes ".$status." and after functional  change ".$functionalStatus."\r\n\n";
                          $deliverable['status']  = $functionalStatus;
                          $_delStatus            += ($functionalStatus - $status);
                          $changeArr['functional_service'] = $functionalServiceChanges;
                        }
					} elseif($key == "responsibility_owner") {
					     $ownerChanges  = $this -> _getResponsibleOwnerChanges($deliverable[$key], $value, $users, $deliverable['status']);
					     if(!empty($ownerChanges))
					     {
					         $deliverable['status']  = $status;
					         $responsibleStatus =  $obj -> updateDeliverableResponsibleOwnerStatus($deliverable);
					         //echo "Status of the deliverable b4 owner changes ".$status." and after owner  change ".$responsibleStatus."\r\n\n";
					         $deliverable['status']  = $responsibleStatus;
					         $_delStatus            += ($responsibleStatus - $status);
					         $changeArr['responsibility_owner'] = $ownerChanges;
					     }
					} elseif($value !== $deliverable[$key]) {	
						$from = "";
						$to   = "";		
						if($key == "deliverable_status")
						{
                              $deliverableStatusChanges = $this -> _getDeliverableStatusChanges($deliverable[$key], $value);
                              if(!empty($deliverableStatusChanges))
                              {
                                 $changeArr['deliverable_status'] = $deliverableStatusChanges;
                              }
						} elseif($key == "reporting_category") {
                              $reportingCategoriesChanges = $this -> _getReportingCategoriesChanges($deliverable[$key], $value);
                              if(!empty($reportingCategoriesChanges))
                              {
                                $changeArr['reporting_category'] = $reportingCategoriesChanges;
                              }
						}  elseif($key == "compliance_frequency") {
                                 $compliancefrequencyChange = $this -> _getComplianceFrequencyChange($deliverable[$key], $value);
					        if(!empty($compliancefrequencyChange))
					        {
					          $changeArr['compliance_frequency'] = $compliancefrequencyChange;
					        }	
						} elseif($key == "applicable_org_type") {
                              $orgtypesChanges  = $this -> _getOrganisationTypesChanges($deliverable[$key], $value);
                              if(!empty($orgtypeChanges))
                              {
                                $changeArr['applicable_org_type'] = $orgtypesChanges;
                              }
						} elseif( $key == "main_event") {
                             $eventChanges = $this -> _getEventChanges($deliverable[$key], $value);
                             if(!empty($eventChanges))
                             {
                               $changeArr['main_event'] = $eventChanges;
                             }
						} elseif($key == "status") {
                              $statusChange = $this -> _getStatusChange($deliverable['status'], $value);
                              if(!empty($statusChange))
                              {
                                $changeArr['status'] = $statusChange['status'];
                              }   
						} else {
					        if(trim(stripslashes($deliverable[$key])) !== trim(stripslashes($value)))
					        {
					           $from = ($deliverable[$key] == "" ? " - " : $deliverable[$key]);
	                           $changeArr[$key] = array("from" => $from , "to" => $value );
	                        }
						}
					} 	
				}
			}
		}	
		$result = array();
		if(!empty($changeArr))
		{
			if(!empty($this -> currentStatus))
			{
		       $changeArr['currentstatus'] = $this -> currentStatus;			
			} else {
                    $statusObj = new DeliverableStatus();
                    $statuses  =  $statusObj -> fetchAll();
                    $changeArr['currentstatus'] = (isset($statuses[$deliverable['deliverable_status']]) ? $statuses[$deliverable['deliverable_status']]['name'] : "New");
			}		
			$changeArr['user'] =   $_SESSION['tkn'];
			if(isset($_POST['data']['response']))
			{
				$changeArr['response'] = $_POST['data']['response'];
				$obj -> setTablename("deliverable_update");
				$insertdata = array("response" => (isset($_POST['data']['response']) ? $_POST['data']['response'] : ""), "changes" => base64_encode(serialize( $changeArr )), "deliverable_id" => $id, "insertuser" => $_SESSION['tid'] );				
			} else {
				$obj->setTablename("deliverable_edit");
				$insertdata = array("changes" => base64_encode(serialize( $changeArr )), "deliverable_id" => $id, "insertuser" => $_SESSION['tid'] );
			}
			$obj->save( $insertdata );	
			$obj->setTablename("deliverable");
		} else {
			$changeArr['user'] = $_SESSION['tkn'];
			if(isset($_POST['data']['response']))
			{
                $statusObj = new DeliverableStatus();
                $statuses  =  $statusObj -> fetchAll();
                if(isset($statuses[$deliverable['deliverable_status']]))
                {
                   $changeArr['currentstatus']  = $statuses[$deliverable['deliverable_status']]['name'];
                } else {
                  $changeArr['currentstatus']   = "New";
                }	
				$changeArr['response'] = $_POST['data']['response'];
				$obj -> setTablename("deliverable_update");
				$insertdata['response']       = (isset($_POST['data']['response']) ? $_POST['data']['response'] : "");
				$insertdata['changes']        = base64_encode(serialize( $changeArr ));
				$insertdata['deliverable_id'] = $id;
				$insertdata['insertuser']     = $_SESSION['tid'];
			    $obj->save( $insertdata );	
			    $obj->setTablename("deliverable");				
			}   
		}
		$result['status'] = $_delStatus;
		return $result;
	}
	
	private function _getAccountablePersonChanges($id, $value)
	{
		$accObj  		  = new AccountablePersonManager();
		$oldAccountables = $accObj -> getDeliverableAccountablePersonIds($id);
		$accountablePersonslList = $accObj->getAll();
		$newAccountables = (array)$value;
		$changes = array();
		$to = $from = "";
		
          foreach($accountablePersonslList as $uaccIndex => $userAccountable)
          {
             if(in_array($uaccIndex, $newAccountables))
             {
                $to .= $userAccountable['role'].", ";
             }
             
             if(isset($oldAccountables[$uaccIndex]))
             {
                $from .= $userAccountable['role'].", ";
             }
          }
          $from = rtrim($from, ", ");
          $to   = rtrim($to, ", ");
		if($from != $to)
		{
		   $delAccPersonObj = new DeliverableAccountablePerson(); 		     
		   $delAccPersonObj -> updateDeliverableAccountablePerson($newAccountables, $id);	
		   $changes = array("from" => (empty($from) ? "Unspecified" : $from), "to" => (empty($to) ? "Unspecified" : $to));
		}
		return $changes;
	} 
	
	private function _getComplianceToChanges($deliverableId, $toComplianceTo)
	{
          $compObj 	     = new ComplianceToManager();
          $compliancesto = $compObj -> getAll();
          $mastertitles  = $compObj -> getComplianceToTitles();
	     $matches       = $compObj -> getMatchList();	                
          $fromComplianceTo = $compObj -> getDeliverableComplianceToIds($deliverableId);
          $from  = $compObj -> getDeliverableComplianceTo($deliverableId, $compliancesto, $mastertitles, $matches);

          $to = "";
          $toComplianceTo = (array)$toComplianceTo;
          $complianceTo = (array)$toComplianceTo;
          if(isset($toComplianceTo) && !empty($toComplianceTo))
          {
             foreach($toComplianceTo as $index => $value)
             {
               if(isset($compliancesto[$value]))
               {
                  if(isset($fromComplianceTo[$value]))
                  {
                    unset($toComplianceTo[$index]);
                    unset($fromComplianceTo[$value]);
                    $to .= $compliancesto[$value]['title']." (".$compliancesto[$value]['user'].") ,";
                  } else {
                    $to .= $compliancesto[$value]['title']." (".$compliancesto[$value]['user'].") ,";
                  }
               }
             }  
          }
          $to = rtrim($to, " ,");         
          $changes  = array();
          if($from != $to)
          {             
             if(!empty($toComplianceTo) || !empty($fromComplianceTo))
             {
               $deliverableComplianceToObj = new DeliverableComplianceTo();                 
	          $deliverableComplianceToObj -> updateDeliverableComplianceTo($complianceTo, $deliverableId);	
	          $changes = array("from" => (empty($from) ? "Unspecified" : $from), "to" => (empty($to) ? "Unspecified" : $to));
	        }
          }
          return $changes;	
	}
	
	private function _getSubeventChange($id, $toSubEvents)
	{
	    $changes            = array();
        $subeventObj        = new Subevent();
        $subevents          = $subeventObj -> getAll();		
        $subeventmanagerObj = new SubEventManager();
        $fromSubEvents      = $subeventmanagerObj -> getDeliverableSubEvents($id);
          //store posted sub events 
        $postedsubevents    = (array)$toSubEvents;
        $from               = $to = "";
        if(!empty($fromSubEvents))
        {
            foreach($fromSubEvents as $seId => $seVal)
            {
               if(isset($subevents[$seId]))
               {
                  $from .= $subevents[$seId]['name']." ,";
               }
            }
         }
         if(!empty($postedsubevents)) 
         {
              foreach($postedsubevents as $index => $subeventId)
              {
                if(isset($subevents[$subeventId]))
                {
                    $to .= $subevents[$subeventId]['name']." ,";
                }
              }
         }
         $from = rtrim($from, ",");
         $to   = rtrim($to, ",");
         if($from != $to)
         {
            $deliverableSubeventObj = new DeliverableSubEvent();
	        $deliverableSubeventObj -> updateDeliverableSubevent($postedsubevents, $id);
	        $changes = array("from" => (empty($from) ? "Unspecified" : $from), "to" => (empty($to) ? "Unspecified" : $to));		
         }     
       return $changes;
	}
	
	function _getResponsibleOwnerChanges($fromUser, $toUser, $users, $deliverableStatus)
	{    
	   $changes = array();
        $respObj    = new ResponsibleOwnerManager();
        $userObj   = new User();
        $users     = $userObj -> getList();    
        $ownerTitles = $respObj -> getResponsibleOwnerTitles();         
        $from = $to = "";
        $from = $respObj -> getDeliverableResponsiblePerson($fromUser, $deliverableStatus, $users, $ownerTitles);
        if(!(Deliverable::isUsingUserId($deliverableStatus)))
        {
          $deliverableStatus = $deliverableStatus + Deliverable::USING_USERID;
        }               
        $to   = $respObj -> getDeliverableResponsiblePerson($toUser, $deliverableStatus, $users, $ownerTitles);
        if($from !== $to)
        {
          $changes = array("from" => $from, "to" => $to);		
        }      
       return $changes;
	}
	
	
	public static function processAddLog($obj, $deliverableid, $postData)
	{
	     //$insertdata]'user'] = $_SESSION['tkn'];
	     $addInfo = array("message" => "Added deliverable ref #".$deliverableid." , description ".$postData['description'], "user" => $_SESSION['tkn']);
          $obj->setTablename("deliverable_edit");	     
	     $insertdata = array("changes" => base64_encode(serialize($addInfo)), "deliverable_id" => $deliverableid, "insertuser" => $_SESSION['tid']);
	     $obj->save($insertdata);   
	}
	
	function _getCompliancefrequencyChange($complianceFreq, $value)
	{
       $cmpObj 		 = new ComplianceFrequency();
       $complianceFrequencies = $cmpObj -> getAll();					
       $change = array();
       $from = $to = "";
       if(isset($complianceFrequencies[$complianceFreq]))
       {
         $from = $complianceFrequencies[$complianceFreq]['name'];
       }
       if(isset($complianceFrequencies[$value]))
       {
         $to = $complianceFrequencies[$complianceFreq]['name'];
       }						
       if($from != $to)
       {
         $change = array("from" => $from , "to" => $to);	     
       }
       return $change;
	}

    function _getFunctionalServicesChanges($fromFunctionalService, $toFunctionalService, $deliverableStatus)
    {
      $functionalServicesObj    = new FunctionalServiceManager(); 
      $titles      = $functionalServicesObj -> getFunctionalServicesTitles();
      $fxnServices = $functionalServicesObj -> getClientFunctionalServices();             
      $from = $to = "";
      $change = array();
      $from = $functionalServicesObj -> deliverableFunctionalService($fromFunctionalService, $deliverableStatus, $titles, $fxnServices);
      if(!(Deliverable::isUsingClientFunctionalService($deliverableStatus)))
      {
          $deliverableStatus = $deliverableStatus + Deliverable::USING_FUNCTIONALSERVICEID;
      }      
      $to   = $functionalServicesObj -> deliverableFunctionalService($toFunctionalService, $deliverableStatus, $titles, $fxnServices);
      if($from != $to)
      {
        $change = array("from" => $from, "to" => $to);
      }   
      return $change;
    }
	
    function _getDepartmentChanges($fromDepartment, $toDepartment, $deliverableStatus)
    {
       
       $departmentObj = new DepartmentManager();
       $masterDepts = $departmentObj -> getMasterDepartments();
       $clientObj   = new ClientDepartment();
       $clientDepart = $clientObj -> getAll();              
       $from = $to = "";
       $changes = array();       
       $from = $departmentObj -> getDeliverableDepartment($fromDepartment, $deliverableStatus, $clientDepart, $masterDepts);
       if(!(Deliverable::isUsingClientDepartment($deliverableStatus)))
       {
          $deliverableStatus = $deliverableStatus + Deliverable::USING_DEPARTMENTID;
       }
       $to   = $departmentObj -> getDeliverableDepartment($toDepartment, $deliverableStatus, $clientDepart, $masterDepts);
       if($from != $to)
       {
          $changes = array("from" => $from, "to" => $to);
       }
       return $changes; 
    }   
    
    function _getEventChanges($fromEvent, $toEvent)
    {
        $eventObj = new Event();
        $events   = $eventObj -> getAll();
        $changes = array();
        $from = $to = "";
        if(isset($events[$fromEvent]))
        {
           $from = $events[$fromEvent]['name'];
        }
        
        if(isset($events[$toEvent]))
        {
          $to = $events[$toEvent]['name'];
        }
     
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);
        }
        return $changes;
    }
     
    function _getReportingCategoriesChanges($fromReportingCategory, $toReportingCategory)
    {
	    $repcatObj = new ReportingCategories();  
	    $reportingCategories = $repcatObj->getList();
	    $changes = array();
	    $from = $to = "";
	    if(!empty($toReportingCategory))
	    {
	      if(isset($reportingCategories[$toReportingCategory]))
	      {
	          $to = $reportingCategories[$toReportingCategory];
	      } else {
	          $to = "Unspecified";
	      }
	      
	      if(isset($reportingCategories[$fromReportingCategory]))
	      {
	         $from = $reportingCategories[$fromReportingCategory];
	      }
	    }
	    if($from !== $to)
	    {
            $changes = array("from" => $from, "to" => $to);
         }    
       return $changes;     
    } 
  
    function _getDeliverableStatusChanges($fromStatus, $toStatus)
    {
      $statusObj = new DeliverableStatus();
      $statuses  =  $statusObj -> getAll();
      $changes   = array();
      $from      = $to = "";
      foreach($statuses as $index => $status)
      {
        if($status['id'] == $fromStatus)
        {
          $from = $status['name'];
        }
        if($status['id'] == $toStatus)
        {
          $to = $status['name'];
        }
      }
      if($from != $to)
      {
          $this -> currentStatus = $to;
          $changes = array("from" => $from, "to" => $to);
      }
      return $changes;   
    }
    
    function _getOrganisationTypesChanges($fromOrganisationType, $toOrganisationType)
    {
	  $orgtypeObj  = new OrganisationTypes();
	  $orgTypes  	= $orgtypeObj -> getAll();	
	  $from = $to = "";
	  $changes = array();
	  if(!empty($orgTypes))
	  {
	    if(isset($orgTypes[$fromOrganisationType]))
	    {
	      $from = $orgTypes[$fromOrganisationType]['name'];
	    }
	    if(isset($orgTypes[$toOrganisationType]))
	    {
	      $to = $orgTypes[$toOrganisationType]['name'];
	    }
	  } 
	  if($from != $to)
	  {
          $changes = array("from" => $from , "to" => $to);	     
	  }
	  return $changes;
    }
    
    function _getStatusChange($fromStatus, $toStatus)
    {
       $statusTo = abs($fromStatus - $toStatus);              
       $changes = array();
       /*if the status to is less than the status from , then the a status has been removed from the deliverable
       */
       if($toStatus == 2)
       {
         $changes['status']  = "Deliverable has been deleted";
       } elseif(($fromStatus  & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED) {
         $statusChange  = ""; 
         $statusChange  = "Deliverable event activation status has been removed <br />";
         if(($toStatus & Deliverable::ACTIVATED) == Deliverable::ACTIVATED)
         {
              $statusChange  .= "Deliverable has been activated ";
         } 
         $changes['status'] = $statusChange;
       } elseif(($toStatus & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED) {
         $statusChange  = ""; 
         $statusChange  = "Deliverable status changed to event activated <br />";
         if(($toStatus & Deliverable::ACTIVATED) != Deliverable::ACTIVATED)
         {
             $statusChange .= "Deliverable has been deactivated";
         }          
         $changes['status'] = $statusChange;
       } elseif(($toStatus & Deliverable::CONFIRMED) == Deliverable::CONFIRMED){ 
         $changes['status']  = "Deliverable has been confirmed";
       } elseif(($toStatus & Deliverable::ACTIVATED) == Deliverable::ACTIVATED){
          $changes['status']  = "Deliverable has been activated";
       } 
       return $changes;     
   }
     	
} 
?>