<?php
class AssuranceDeliverable
{
   private $deliverableAssuranceObj = null; 
   
   protected static $tablename = "deliverable_assurance";
   
   function __construct()
   {
      $this->deliverableAssuranceObj = new DeliverableAssurance();
   }
   
	function getDeliverableAssurance( $start, $limit, $id)
	{
		$deliverables 	= $this->deliverableAssuranceObj->fetchAll($start, $limit, $id);
		$dels 			= $this->deliverableAssuranceObj->fetchDeliverableAssurance( $id );
		return array("total" => $dels['total'], "data" => $deliverables ); 
	}
	
	function saveDeliverableAssurance( $data )
	{
		$this->deliverableAssuranceObj->setTablename("deliverable_assurance");
		$res 	  = $this->deliverableAssuranceObj->save( $data );
		$response = array();
		if(  $res > 0)
		{
			$response = array("text" => "Deliverable saved", "error" => false, "updated" => true );
		} else {
			$response = array("text" => "Error saving the deliverable", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function updateDeliverableAssurance( $data )
	{
		$id 	= $data['id'];
		unset( $data['id'] ); 
		$this->deliverableAssuranceObj->setTablename("deliverable_assurance");
		$res 	  = $this->deliverableAssuranceObj->update($id, $data, new DeliverableAssuranceAuditLog(), $this->deliverableAssuranceObj, new DeliverableNaming());
		$response = array();
		if(  $res > 0)
		{
			$response = array("text" => "Deliverable updated", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the deliverable", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the deliverable", "error" => true, "updated" => false);
		}
		return $response;	
	}

}

?>