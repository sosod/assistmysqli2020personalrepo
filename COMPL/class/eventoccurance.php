<?php
class EventOccurance extends Model
{
     const EVENT_ACTIVATED = 4;

	
     protected static $tablename = "event_occurance";	
	
	function __construct()
	{
		parent::__construct();
	}
	
	function fetchAll($options="")
	{
	   $results = $this->db->get("SELECT id, event_id, date_occured, comment, insertdate, insertuser FROM #_event_occurance ORDER BY insertdate");
	   return $results;	
	}
	
	function getEventOccurance()
	{
	   $results = $this->db->get("SELECT id, event_id, date_occured, comment, insertdate, insertuser FROM #_event_occurance");
	   $events  = array(); 
	   if(!empty($results))
	   {
	      foreach( $results as $i => $val)
	      {
	         $events[$val['event_id']] = $val;
	      }	   
	   }
	   return $events;
	}	
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT * FROM #_event_occurance WHERE id = '".$id."'");
		return $result;		
	}
	
	function getEventOccuranceWhereAdmin($optionSql = "")
	{
	     $results = $this -> db -> get("SELECT E.id, E.event_id, E.date_occured, E.comment, E.insertdate, E.insertuser, E.status, D.legislation
	                                     FROM #_event_occurance E 
	                                     INNER JOIN  #_deliverable D ON E.id = D.event_occuranceid 
	                                     $optionSql
	                                    ");
	    return $results;                           
	}
	
	function updateEventOccurance($data)
	{
	   $eventoccurance = $this->fetch($data['occuranceid']);
	   $updatedata     = array();
	   $updatedata['status'] = $eventoccurance['status'] + $data['status'];
 	   $this->setTablename("event_occurance");
	   $res = $this->update("id=".$data['occuranceid'], $updatedata, new SetupAuditLog());
	   return $res;
	}
	
	function updateEvent( $data )
	{
       $this -> setTablename("event_occurance");    
	   $res 	    = $this -> save($data);        	
	   if($res > 0)
	   {
		   //copy the main deliverable and make new one to be used/activated
		   $delObj = new ClientDeliverable();
		   $delIds = $delObj -> copyBySubEvent($res, $data['event_id'] );
        }
	   $response = $this->saveMessage("events_occurance", $res);
      return $response;
	}
	

}
?>