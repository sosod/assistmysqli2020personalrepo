<?php

class COMPL_LEGISLATION extends COMPL {

	protected $section = "legislation";
	protected $attachment_folder = "legislation";
	protected $tablename = "legislation";

	public function __construct() {
		parent::__construct();
	}
	
	public function getRecord($i) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_".$this->tablename." WHERE id = ".$i;
		return $this->mysql_fetch_one($sql);
	}
	
	public function getFormattedRecord($i) {
		$row = $this->getRecord($i);
		$a = unserialize(base64_decode($row['attachment']));
		$row['attachment'] = $a;
		return $row;
	}

	public function saveUpdateAttachments($i,$new_attach,$updated_attach,$stat) {
		$attach = base64_encode(serialize($updated_attach));
		$sql = "UPDATE ".$this->getDBRef()."_".$this->tablename." SET attachment = '".$attach."' WHERE id = ".$i;
		$this->db_update($sql);
		
		$log = array();
		foreach($new_attach as $x=>$val) {
			$log['attachments'][$x] = "Attachment <i>".$val."</i> has been added";
		}
		$log['user'] = $_SESSION['tkn'];
		//$log['currentstatus']= $status;
		$statusObj 	= new ActionStatus();
		$statuses    = $statusObj -> getActionStatuses();
			  if($statuses[$stat])
			  {
			     $log['currentstatus'] = $statuses[$stat]['name'];
			  } else {
			     $log['currentstatus'] = "New";
			  }		
			$obj = new ClientAction();
			$obj -> setTablename($this->tablename."_update");
			$insertdata['response']   = "Added attachments during update.";		
			$insertdata['changes']    = base64_encode(serialize($log));
			$insertdata['action_id']  = $i;
			$insertdata['insertuser'] = $_SESSION['tid'];										
		$res = $obj -> save( $insertdata );	
		
	}
	
	public function displayAttachmentList($i,$can_edit=false,$attach=array()) {
		$echo = "";
		if(count($attach)==0) {
			$record = $this->getFormattedRecord($i);
			$attach = $record['attachment'];
		}
		$a = array();
		$x = 0;
		if(isset($attach) && is_array($attach) && count($attach)>0) {
			foreach($attach as $src=>$filename) {
				$key = $i."_".$x; $x++;
				$a[$key] = array(
					'system_filename'=>$src,
					'original_filename'=>$filename
				);
			} //echo "<pre>"; print_r($a); echo "</pre>";
			$echo = $this->getObjectAttachmentDisplay($can_edit,$a,$this->getModRef()."/".$this->attachment_folder);
		}
		return $echo;
	}
	
	
	public function deleteAttachment($i,$deleted_attach,$updated_attach,$stat) {
		$attach = base64_encode(serialize($updated_attach));
		$sql = "UPDATE ".$this->getDBRef()."_".$this->tablename." SET attachment = '".$attach."' WHERE id = ".$i;
		$this->db_update($sql);
		
		$log = array();
		foreach($deleted_attach as $x=>$val) {
			$log['attachments'][$x] = "Attachment <i>".$val."</i> has been deleted";
		}
		$log['user'] = $_SESSION['tkn'];
		//$log['currentstatus']= $status;
		$statusObj 	= new ActionStatus();
		$statuses    = $statusObj -> getActionStatuses();
			if($statuses[$stat]) {
				$log['currentstatus'] = $statuses[$stat]['name'];
			} else {
				$log['currentstatus'] = "New";
			}		
			$obj = new ClientAction();
			$obj -> setTablename($this->tablename."_update");
			$insertdata['response']   = "Deleted attachment";		
			$insertdata['changes']    = base64_encode(serialize($log));
			$insertdata['action_id']  = $i;
			$insertdata['insertuser'] = $_SESSION['tid'];										
		$res = $obj -> save( $insertdata );	
	}
	
	
	
	
	/** SHORTCUT FUNCTIONS **/
	public function getStatusID($record) { return $record['status']; }
	
}


?>