<?php
class LegislationOrganisationTypeManager 
{
	
	private $legorgtypeObj = null;
	
	function __construct()
	{
		$this->legorgtypeObj = new LegislationOrganisationType();	
	}
	
	function getAll( $options = array())
	{
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql = " AND status & ".$optins['status']." = ".$options['status']."";
		}
		$results = $this->legorgtypeObj->fetchAll( $optionSql );
		$mresults = $this->legorgtypeObj->getLegOrganisationTypes( $optionSql );
		$orgtypes = array();
		foreach($mresults as $index => $orgtype)
		{
			$orgtypes[$orgtype['id']] = $orgtype;
			$orgtypes[$orgtype['id']]['imported'] = true;
		}		
		$legorgtypes	= array_merge($orgtypes, $results);
		return $legorgtypes;
	}

	function getAllList()
	{
		$results  = $this->getAll();
		$orgtypes = array();
		foreach($results as $index => $orgtype)
		{
			$orgtypes[$orgtype['id']] = $orgtype['name'];
		}
		return $orgtypes;	
	}

	function getLegOrganisationTypes($id)
	{
	     
		$results = $this -> legorgtypeObj -> getOrganisationalTypeByLegislationId( $id );
		$typesList = array();
		foreach($results as $index => $type)
		{
			$typesList[$type['orgtype_id']] = array("typeid" => $type['orgtype_id'], "legid" => $type['legislation_id']);
		}
		return $typesList;
	}
	
	function updateOrganisationalTypes($newtypes, $id)
	{
		$res = $this->legorgtypeObj->updateOrganisationalTypes( $newtypes, $id);
		return $res;		
	}

	function search($text)
	{
		$clientorgTypes = $this->legorgtypeObj->searchOrganisationTypes($text);
		$masterorgTypes = $this->legorgtypeObj->searchOrganisationTypes2($text);
		
		$orgtypes = array();
		$orgtypes = array_merge($masterorgTypes , $clientorgTypes);
		$orgrtypeSql = "";
		foreach($orgtypes as $index => $val)
		{
			$orgtypeleg = $this->legorgtypeObj->getOrgtypeLeg($val['id']);
			if(!empty($orgtypeleg))	
			{
				$orgrtypeSql .= " OR L.id = '".$orgtypeleg['legislation_id']."' ";
			}
	   }
	  return $orgrtypeSql;
	}

     public static function getMasterLegislationOrganisationType($legislationId)
     {
        $legorgtypeObj = new LegislationOrganisationType();
        $types  = $legorgtypeObj -> getAll();
        
        $legorgtypes = $legorgtypeObj -> importOrganisationalTypeByLegislationId($legislationId);
        $typeStr = "";
        foreach($legorgtypes as $index => $type)
        {
          if(isset($types[$type['orgtype_id']]))
          {
            $typeStr .= $types[$type['orgtype_id']]['name'].", ";
          }
          
        } 
        return rtrim($typeStr, ", "); 
     }
     
     public static function getLegislationOrganisationType($legislationId)
     {
        $legorgtypeObj = new LegislationOrganisationType();
        $types  = $legorgtypeObj -> getAll();
        
        $legorgtypes = $legorgtypeObj -> getOrganisationalTypeByLegislationId($legislationId);
        $typeStr = "";
        foreach($legorgtypes as $index => $type)
        {
          if(isset($types[$type['orgtype_id']]))
          {
            $typeStr .= $types[$type['orgtype_id']]['name'].", ";
          }
          
        } 
        return rtrim($typeStr, ", "); 
     }     

}
?>
