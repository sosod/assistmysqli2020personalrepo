<?php
class MasterSubEvent extends Model
{
     
     function __construct()
     {
       parent::__construct();
     }     
     
     function fetchAll($options = "")
     {
       $results = $this->db2->get("SELECT SE.id, SE.name, SE.description, SE.status, E.name AS eventname, E.id as eid FROM #_sub_event SE
                                   INNER JOIN #_events E ON E.id = SE.main_event 
                                   WHERE SE.status & 2 <> 2 $options ");
       return $results;
     }
     
     function fetch($id)
     {
       $result = $this->db2->getRow("SELECT id, name, description, main_event, status FROM #_sub_event WHERE id = '".$id."' ");
       return $result;
     }
     
   	function getByMainEvent($id)
	{
		$results = $this->db2->get("SELECT id, name, description , main_event , status FROM #_sub_event WHERE main_event = '".$id."'");
		return $results;
	}  
	
	
	function getMany( $options = "")
	{
		$results = $this->db2->get("SELECT id, name, description , main_event , status FROM #_sub_event WHERE main_event IN($options)");
		return $results;
	}	
	

}
?>