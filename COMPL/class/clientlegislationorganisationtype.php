<?php
class ClientLegislationOrganisationType extends LegislationOrganisationType
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
	function fetchAll( $options = "")
	{
		$results = $this->db->get("SELECT * FROM #_leg_organisation_types WHERE status & 2 <> 2 $options ");
		$types = array();
		foreach($results as $index => $type)
		{
		   $types[$type['id']] = $type;
		   $types[$type['id']]['imported'] = false;
		}
		return $types;		
	}

	function fetch( $id )
	{
		$result  = $this->db->getRow("SELECT * FROM #_leg_organisation_types WHERE id = $id");
		return $result;
	}


	function searchOrganisationTypes( $text )
	{
		$results = $this->db->get("SELECT id FROM #_leg_organisation_types WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
		return $results;
	} 
}
?>
