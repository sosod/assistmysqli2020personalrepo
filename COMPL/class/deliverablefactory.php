<?php 
class DeliverableFactory 
{
     
     public static function getInstance($section)
     {
        $deliverableManagerObj = ""; 
        if(!empty($section))
        {
             $classname = ucfirst($section)."Deliverable";
             if(class_exists($classname))
             {
               $deliverableManagerObj = new $classname();
             }          
        }
       return $deliverableManagerObj; 
     }     
     
     
}
?>