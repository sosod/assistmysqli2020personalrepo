<?php
class ReportingCategories extends Model
{
     protected static $tablename = "reporting_categories";
     
     function __construct()
     {
         parent::__construct();
     }
     
     function getReportingCategories($options = "")
     {    
        $sql = "";
        if(isset($options['status']) && !empty($options['status']))
        {
          $sql = " AND status & ".$options['status']." = ".$options['status'];
        }
        $results = $this->db->get("SELECT * FROM #_reporting_categories WHERE status & 2 <> 2 $sql ");
        return $results;
     }
     
     function fetch($id)
     {
          $result = $this->db->getRow("SELECT * FROM #_reporting_categories WHERE 1 AND id = $id");
          return $result;
     }
     
     function saveCategory($data)
     {  
          $this->setTablename("reporting_categories");
          $id = $this->save($data);
          $response = $this->saveMessage("reporting category", $id); 
          return $response;          
     }
     
     function updateCategory($data)
     {
        $this -> setTablename("reporting_categories");
        $res      = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new Naming());
        $response = array();
        if($res > 0)
        {
          $response = array("text" => "Reporting category updated successfully", "error" => false);
        } else if($res == 0){
          $response = array("text" => "No change made to the reporting category", "error" => false, "updated" => true);
        } else {
          $response = array("text" => "Error deleting reporting category", "error" => true);
        }
        return $response;
     }
     
     function getList()
     {
        $categories = $this -> getReportingCategories();
        $list       = array();
        foreach($categories as $cIndex => $cVal)
        {
          $list[$cVal['id']] = $cVal['name'];
        }
        return $list;
     }

}
?>