<?php
class SubeventManager
{
	
	private $subeventObj = null;
	
	function __construct()
	{
		$this -> subeventObj = new SubEvent();
	}

     function getDelSubEvents($deliverableId, $subevents, $deliverbleEventObj)
     {
         //echo "Deliverable is looking for its subevents . . .".$deliverableId."<br /><br />";

         $deliverableSubevents = $deliverbleEventObj -> fetchByDeliverableId($deliverableId);
         $subeventStr          = "";
         foreach($deliverableSubevents as $index => $delSubevent)
         {
            foreach($subevents as $eventIndex => $subevent)
            {
                if($subevent['id'] == $delSubevent['subevent_id'])
                {
                    $subeventStr .= $subevent['name']." ,";
                }
            }
         }
        return rtrim($subeventStr, " ,"); 
     }

     /*
      Match the subevent to a given deliverable-id
     */
     function matchSubEventToDeliverable($deliverableId)
     {
         $deliverableEventObj = new DeliverableSubEvent();
         $subevents = $deliverableEventObj->fetchByDeliverableId($deliverableId);
         $subeventObj  = new SubEvent();
         $subeventList = $subeventObj -> getAll();
         $subeventStr  = "";
         foreach($subevents as $index => $subevent)
         {
            if(isset($subeventList[$subevent['subevent_id']]))
            {
               $subeventStr .= $subeventList[$subevent['subevent_id']]['name']." ,";
            }
         }
        return rtrim($subeventStr, " ,");
     }

	function getAll($options = array())
	{
		$optionSql = "";
		$option2Sql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql  = " AND status & ".$options['status']." = ".$options['status']."";
			$option2Sql = " AND SE.status & ".$options['status']." = ".$options['status']."";
		}
		
		if(isset($options['eventid']) && !empty($options['eventid']))
		{
			$optionSql .= " AND main_event = ".$options['eventid']."";
			$option2Sql = " AND SE.main_event = ".$options['eventid']."";
		}

		$eventManager = new Event();
		$oEvents      = $this -> subeventObj -> getSubEvents($option2Sql);
		$myEvents     = $this -> subeventObj -> fetchAll($optionSql);
		debug($oEvents);
		debug($myEvents);		
		exit();
		$events       = $eventManager -> getAll(); 
		$subEvents    = array();
		foreach($myEvents as $index => $val)
		{
			foreach($events as $key => $event)
			{
				if($event['id']  == $val['main_event'])
				{
					$subEvents[$val['id']]  = array(
										     "id" 		   => $val['id'],
										     "main_event"  => $event['name'],
										     "name"		   => $val['name'],
										     "description" => $val['description'],
										     "status"	   => $val['status']
									     );			
			     }			
			}
		}
		$events  = array();
		foreach ($oEvents as $index => $val)
		{
			$oEvents[$index] = $val;
			$oEvents[$index]['imported'] = true;
 		}
 		$events  = array_merge($oEvents, $subEvents );
 		return $events;
	}
	
	function getList($options = array())
	{
		$subevents = $this->getAll($options);
		$list      = array();
		foreach($subevents as $index => $subevent)
		{
			$list[$subevent['id']] = $subevent['name'];
		}
		return $list;
	}

	function getSubEvents($id)
	{
		$results   = $this -> subeventObj -> getAll($id);
		$subevents = array();
		foreach($results as $i => $val)
		{
			$subevents[$val['subevent_id']] = array("subid" => $val['subevent_id'], "deliverableid" => $val['deliverable_id']);
		}
		return $subevents;
	}
     
     function getDeliverableSubEvents($deliverableId)
     {
         $delsubObj                = new DeliverableSubEvent();
         $delEvents                = $delsubObj -> fetchByDeliverableId($deliverableId);
         $deliverableSubEventsList = array();
         foreach($delEvents as $index => $delSubEvent)
         {
            $deliverableSubEventsList[$delSubEvent['subevent_id']] = $delSubEvent['deliverable_id'];
         } 
         return $deliverableSubEventsList;
     }

}
?>