<?php
class UpperMenu extends Menu
{
	
	function __construct($folder, $pagename)
	{
	   parent::__construct();
	   $this -> folder   = $folder;
	   $this -> pagename = $pagename;  
	}
	
	function createMenu()
	{
	  $results   = $this->getMenu();  
	  $menuItems = array();
		$uaObj = new UserAccess();
		$user = $uaObj->getUserAccess();
		foreach($user as $u) {
			$user_setting = $u;
			break;
		}
		$user_access = array(
			"admin"=>UserAccess::ADMIN_ALL,
			"edit"=>UserAccess::EDIT_ALL,
			"update"=>UserAccess::UPDATE_ALL,
		);
		//echo "<pre>";	print_r($user_setting);  echo "</pre>";
	  foreach($results as $key => $menu)
	  {
	    $id     = $menu['id'];
	    $url    = "";
	    $active = "";
	    //$url 	= $this->processUrl( $menu['name'] );
	    $_url = str_replace($this->folder."_", "", $menu['name']);
	    //echo $this -> pagename." and menu name is ".$menu['name']." and this folder is ".$this -> folder." url is ".$_url."<br /><br />";
	    $active   = ($this->isActivePage($_url, $this->pagename) ? "Y" : "");	    
	    if(strstr($menu['name'], $this->folder))
	    {
	      $url = $_url;
	    } else {
	      $url = $menu['name'];
	    }
	    
		$display_menu = true;
		//$x = "a";
		if($this->folder == "admin") {
			//$x = $user_setting['status'];
			switch($menu['name']) {
				case "admin_copy_financial_year":
				case "admin_reassign":
					if( ($user_setting['status'] & UserAccess::ADMIN_ALL) != UserAccess::ADMIN_ALL) {
						$display_menu = false;
			//			$x = (($user_setting['status'] & UserAccess::ADMIN_ALL));
					}
					break;
				case "admin_edit":
					if( ($user_setting['status'] & UserAccess::EDIT_ALL) != UserAccess::EDIT_ALL) {
						$display_menu = false;
			//			$x = (($user_setting['status'] & UserAccess::EDIT_ALL));
					}
					break;
				case "admin_update":
					if( ($user_setting['status'] & UserAccess::UPDATE_ALL) != UserAccess::UPDATE_ALL) {
						$display_menu = false;
			//			$x = (($user_setting['status'] & UserAccess::UPDATE_ALL));
					}
					break;
			}
			
		}
		
		if($display_menu) {
			$menuItems[$menu['id']] = array( 'id'     => $id, 
							'url'    => $url.".php", 
							'active' => $active,
							'display'=> ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
						) ;
		}
	   }	
	   return $menuItems;   
	}
	
	function getMenu($options = "")
	{
	   $results  = $this->db->get("SELECT id, name, ignite_terminology, client_terminology, status, parent_id, accesscode FROM #_menu
	                               WHERE parent_id = (SELECT id FROM #_menu WHERE name = '".$this->folder."')
	                              ");
	   return $results;                           
	}
	
     /*
	function getMenu($key)
	{
		$mainMenu = array();
		$results = $this->db->get("SELECT id, name, ignite_terminology, client_terminology
						       FROM #_menu 
					   	       WHERE parent_id = (SELECT id FROM #_menu WHERE name = '".$key."') ORDER BY position
					   	      ");
		return $results;
	}
	
	function createMenu($folder, $pagename)
	{
	  $results  = $this->getMenu($folder);	
       $mainMenu = array(); 	  	
	  foreach( $results as $key => $menu)
	  {
	    $id     = $menu['id'];
	    $url    = "";
	    $active = "";
	    //$url 	= $this->processUrl( $menu['name'] );
	    $active   = $this->getActivePage( $menu['name'], $pagename);	    
	    if(strstr($menu['name'], $folder))
	    {
	      $url = str_replace($folder."_", "", $menu['name']);
	    } else {
	      $url = $menu['name'];
	    }
	    
	    $mainMenu[$menu['id']] = array( 'id'     => $id, 
					    'url'    => $url.".php", 
					    'active' => $active,
					    'display'=> ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
					) ;
	   }
	   echo echoNavigation(1,$mainMenu);
	 }	
	 
		 */
}
