<?php
class DeliverableAssuranceAuditLog implements Auditlog
{

	function processChanges($obj, $postArr, $id, Naming $naming)
	{
		$deliverables = $obj -> fetch( $id );
		$obj_id = $postArr['deliverable_id'];
		$changeArr 	  = array();		
		$tablename 	  = $obj -> getTableName();
		foreach( $postArr as $key => $value)
		{
			if( $key == "status"){
				if($value == 2)
				{
					$changeArr['message'] = "Assurance #".$id." has been deleted "; 	
				} 
			} else if($key == "sign_off"){
				$changeArr[$key] = array("from" => $this->_checkSignOff( $deliverables[$key] ), "to" => $this->_checkSignOff( $value ) );		
			} else if( array_key_exists($key, $deliverables)){
				if($value != $deliverables[$key])
				{
					$changeArr[$key] = array("from" => $deliverables[$key], "to" => $value );
				}
			}
		}
		
		$result = "";
		if(!empty($changeArr))
		{
			$statusObj = new DeliverableStatus();
			$status    = $statusObj -> fetch( $deliverables['status'] );
			if(!empty($status))
			{
				$changeArr['currentstatus'] = $status['name'];
			} else {
				$changeArr['currentstatus'] = "New";
			}			
			$changeArr['user'] = $_SESSION['tkn'];
			$obj->setTablename( $tablename."_log");
			$insertdata = array("changes" => base64_encode(serialize($changeArr)), "deliverable_id" => $obj_id, "assurance_id"=> $id, "insertuser" => $_SESSION['tid'] );
			$result = $obj->save( $insertdata );	
			$obj->setTablename( $tablename );
		}
		return $result;		
	}
	
	
	private function _checkSignOff( $val )
	{
		if( $val == 1)
		{
			return "Yes";
		} else {
			return "No";
		}
	}
		
}