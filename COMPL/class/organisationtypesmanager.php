<?php
class OrganisationTypesManager
{
	private $organisationTypesObj = null;
	
	function __construct()
	{
	   $this->organisationTypesObj = new OrganisationType();
	}

	function getAll($options = array())
	{
		$orgTypes  = $this -> organisationTypesObj -> getAll($options);
		return $orgTypes;
	}
	
	function getAllList($options=array())
	{
		$types  = $this->getAll($options);
		$list = array();
		foreach($types as $index => $type)
		{
			$list[$type['id']] = $type['name'];
		}
		return $list;
	}
	
	public static function getMasterLegislationOrganisationTypes($legislationId)
	{
	   $typeObj = new OrganisationType();
	   $types   = $typeObj -> getAll();
	   
	   $legislationtype = $typeObj -> getMasterLegislationType($legislationId);
	   $typeStr = "";
	   foreach($legislationtype as $index => $type)
	   {
	     if(isset($types[$legislationtype['orgtype_id']]))
	     {
	       $typeStr .= $types[$legislationtype['orgtype_id']]['name'];
	     }
	   }
	   return $typeStr;
	} 
	
	public static function getLegislationOrganisationTypes($legislationId)
	{
	   $typeObj = new OrganisationType();
	   $types   = $typeObj -> getAll();
	   
	   $legislationtype = $typeObj -> getMasterLegislationType($legislationId);
	   foreach($legislationtype as $index => $type)
	   {
	     if(isset($types[$legislationtype['orgtype_id']]))
	     {
	       $typeStr .= $types[$legislationtype['orgtype_id']]['name'];
	     }
	   }
	   return $typeStr;
	} 	
	

}
?>