<?php
class ComplianceFrequencyManager
{
	
	private $complianceFrequenciesObj = null;
	
	function __construct()
	{
		$this->complianceFrequenciesObj = new ComplianceFrequency();
	}
	
	function getAll( $options = array())
	{
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql = " AND status & ".$options['status']." = ".$options['status']."";
		}
		$oCompFreq  = $this -> complianceFrequenciesObj -> getComplianceFrequencies($optionSql);
		$myCompFreq = $this -> complianceFrequenciesObj -> fetchAll($optionSql);
		$complFreqs  = array();
		foreach ($oCompFreq as $i => $val)
		{
			$oCompFreq[$i] = $val;
			$oCompFreq[$i]['imported'] = true;
		}
		$complFreqs = array_merge($oCompFreq,  $myCompFreq);
		return $complFreqs;
	}
	
	function getAllList($options = array())
	{
		$compfrequencies = $this->getAll($options);
		$list = array();
		foreach($compfrequencies as $index => $compfreq)
		{
			$list[$compfreq['id']] = $compfreq['name'];
		}
		return $list;
	}

} 
?>