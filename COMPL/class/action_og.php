<?php
class Action extends Model
{

	private $id;	 	 	 	 	 	 	
	
	private $action;
			 	 	 				
	private $actionstatus;
			 	 	 	 	 	 	
	private $deliverable_id;
			 	 	 	 	 	 	
	private $action_deliverable;
			 	 	 	 	 	 	
	private $owner;
			 	 	 	 	 	 	
	private $status;
			 	 	 	 	 	 	
	private $deadline;
			 	 	 	 	 	 	
	private $progress;
			 	 	 	 	 	 	
	private $reminder;
			 	 	 	 	 	 	
	private $description;
			 	 	 	 	 	 	
	private $insertdate;
		 	 	 	 	 	 	
	private $insertuser;
	
	protected static $table = "action";
	
	const ACTION_ACTIVE 	    = 1;

	const ACTION_DELETE	 	    = 2;
	
	const ACTION_INACTIVE        = 4;
	//action imported from compliance master database
	const ACTIVE_IMPORTED        = 8;
	//action completed and awaiting manager approval
	const AWAITING_APPROVAL      = 16;
	//action approved that its completed
	const APPROVED			    = 32;
	//action declined 
	const DECLINED 		    = 64;
	//action created when an even occurs
	const CREATEDBY_SUBEVENT     = 128;
	//status when the legislation has been confirmed
	const CONFIRMED               = 256;
	//status of actions created under manage, ie they will be confirmed and activated 
	const MANAGE_CREATED         = 512;
	//stores the status of the actions who are using the actual user id
	const USING_USERID           = 1024;
	//stores the status of action holding the user-id matched under setup
	const USING_MATCHEDUSERID    = 2048;
	//stores the status of actions that  have been activated after being confirmed
	const ACTIVATED              = 4096;
	
	
	function __construct()
	{
		parent::__construct();
	}
	
	/*
	 Get actions from the master table by deliverable id 
	 @params id deliverable id
	*/
	function importActions($id)
	{	
		$response = $this->db2->get("SELECT * FROM #_action WHERE deliverable_id = {$id} ");
		return $response;
	}
	/*
	 Get the actions as specified by the optional filters
	*/
	function fetchAll( $start , $limit, $options = "")
	{
		$limitStr = (!empty($limit) ? " LIMIT ".$start.",".$limit : "");
		$result = $this->db->get("SELECT A.id AS ref, A.action AS action_required, A.progress, A.deadline AS action_deadline_date,
							 A.status, ST.name AS action_status, A.owner, A.reminder AS action_reminder, A.actionstatus, A.action_deliverable, 
							 A.action_reference, CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner
							 FROM #_action A
							 LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.owner
							 INNER JOIN #_action_status ST ON ST.id = A.status
							 WHERE 1 AND A.actionstatus & ".Action::ACTION_DELETE." <> ".Action::ACTION_DELETE."
							 $options
							 $limitStr
						     ");
		return $result;
	}
	
	function fetchAllActions($options = "")
	{
	   $results = $this -> db -> get("SELECT * FROM #_action A WHERE 1 $options");
	   return $results;
	}
		
	/*
	 Get action by the specified deliverable id
	*/
	function fetchByDeliverableId( $id )
	{
		$results = $this->db->get("SELECT * FROM #_action WHERE deliverable_id = '".$id."'");
		return $results;
	}

	function fetchActionStats($whereStr)
	{
	     $results = $this->db->get("SELECT SUM(A.progress) AS totalProgres,
                                     AVG(A.progress) AS averageProgress,
                                     MAX(A.date_completed) AS actionCompleted 
	                                FROM #_action A 
	                                WHERE 1 
                                     AND (A.actionstatus & ".Action::ACTION_DELETE." <> ".Action::ACTION_DELETE." 
                                          AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                               OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                               OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                                              )                           
                                      )		                                
	                                $whereStr
	                              ");
	    return $results;
	}	
	/*
	 Get an action from an action id
	*/
	function fetch($id)
	{
		$result = $this->db->getRow("SELECT A.id AS ref, A.action AS action_required, A.action, A.progress, A.deadline AS action_deadline_date,
							    A.deadline, A.status, A.owner, A.reminder, A.reminder AS action_reminder, A.actionstatus, A.action_deliverable,
							    A.deliverable_id, SA.name AS status_name FROM #_action A
							    LEFT JOIN #_action_status SA ON SA.id = A.status
							    WHERE A.id = '".$id."'
							   ");
		return $result;
	}
	/*
	 get action by deliverable id
	*/
	function fetchActions($id)
	{
		$result = $this->db->getRow("SELECT A.id AS ref, COUNT(*) AS totalActions, SUM(A.progress) AS totalProgress, AVG(A.progress) AS averageProgress
							    FROM #_action A WHERE 1 AND deliverable_id = '".$id."'
							  ");
		return $result;
	}
     /*
      Get the total action as specified by the filter options
     */
     function fetchActionsCount($options="")
     {
         $result = $this->db->getRow("SELECT COUNT(*) AS total FROM #_action A WHERE 1 $options");
         return $result;
     }
     /* 
      Get the what version of action is available in compliance master
     */
	function getLatestVersion( $id )
	{	
		$result = $this->db2->getRow("SELECT id, action_id  FROM #_action_edit WHERE action_id = {$id} ORDER BY insertdate DESC");
		return $result;
	}
	/*
	  Get the current versions of the action as it is in client database
	*/
	function getCurrentVersions()
	{
		$results = $this->db->get("SELECT id, action_id, current_version, version_in_use FROM #_action_usage ");
		return $results;
	}
	/*
	  Get the current version of the action as specified by the action id filter
	*/
	function getCurrentVersion( $id )
	{
		$result = $this->db->getRow("SELECT id, action_id, current_version, version_in_use FROM #_action_usage WHERE action_id = {$id}");
		return $result;
	}
	/*
	  Get actions from the master compliance database aas specified by the action id
	*/
	function importAction( $id )
	{
		$result = $this->db2->getRow("SELECT id, action, action_deliverable, owner, deadline, recurring FROM #_action WHERE id = {$id} ");
		return $result;
	}
	/*
	 Activate actions that have been confimed 
	*/
	function activateActions($id)
	{
	   $optionSql = " AND A.deliverable_id = '".$id."' AND A.actionstatus & ".Action::CONFIRMED." = ".Action::CONFIRMED." ";
        $actions   = array();
        $actions = $this -> fetchAllActions($optionSql);	
        if(!empty($actions))
        {
           foreach($actions as $index => $action)
           {
              $status = ($action['actionstatus'] - Action::CONFIRMED) + Action::ACTIVATED;
              $updatedata = array("actionstatus" => $status);
              $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
           }
        }
     	
	}
	/*
	 Copy actions from one financial year to anothere
	*/
	function copyActionSave($deliverableRef, $deliverableId, $finObj, $financial_year, $actionObj)
	{
	   $actions = $actionObj -> fetchByDeliverableId($deliverableRef);  
	   $res     = 0; 
	   foreach($actions as $index => $action)
	   {
	      $action['deliverable_id'] = $deliverableId;
	      $action['deadline']       = $finObj -> createFinancialYearDate($financial_year, $action['deadline']);
	      if(($action['actionstatus'] & Action::ACTIVATED) == Action::ACTIVATED)
	      {
	        $action['actionstatus']   = $action['actionstatus'] - Action::ACTIVATED;
	      }
	      if(($action['actionstatus'] & Action::CONFIRMED) == Action::CONFIRMED)
	      {
	         $action['actionstatus']   = $action['actionstatus'] - Action::CONFIRMED;
	      }
	      $action['status']   = 1;
	      $action['progress'] = 0;
	      unset($action['id']);  
	      $actionRes = $this -> save($action);
	      if($actionRes > 0)
	      {
	        $res ++;
	      }
        }
        return $res;
	}
     /*
      Copy and save actions that's whose deliverable have been activated by an event
	*/
	function copyActionBySubEvent($newRef, $deliverableId)
	{
	   $actions = $this->fetchByDeliverableId($deliverableId);
	   if(!empty($actions))
	   {	
		foreach($actions as $aIndex => $action)
		{			
			$action['actionstatus']   = $action['actionstatus'] + Action::CREATEDBY_SUBEVENT;
		   $action['deliverable_id'] = $newRef;
		   unset($action['id']);				
		   $id = $this->db->insert("action", $action);
		}
	   }
	}	
	/*
	  Get the actions statitics as specified by the action filters 
	*/
	 function getActionProgressStatitics($optionSql = "")
	{
		$result = $this->db->getRow("SELECT SUM(progress) AS totalProgress, AVG(progress) AS averageProgress, COUNT(id) AS totalActions
                                       FROM #_action A WHERE 1 $optionSql 
		                            ");
		return $result;
	}
	
	function updateActionOwnerMatch($ref, $userId)
	{
	    $optionSql = " AND A.owner = '".$ref."' AND A.actionstatus & ".Action::USING_USERID." <> ".Action::USING_USERID." ";
	    $actions = $this -> fetchAllActions($optionSql);
	    if(!empty($actions))
	    {
	      foreach($actions as $index => $action)
	      {
	         $status = $action['actionstatus'] + Action::USING_MATCHEDUSERID;
	         $updatedata = array("actionstatus" => $status, "owner" => $userId);
	         $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
	      }
	    }
	}
	
	function undoActionOwnerMatch($refId, $userId)
	{
	   $optionSql = " AND A.owner = '".$userId."' AND A.actionstatus & ".Action::USING_MATCHEDUSERID." = ".Action::USING_MATCHEDUSERID." ";
	   $actions   = $this ->  fetchAllActions($optionSql);
	   if(!empty($actions))
	   {
	     foreach($actions as $index => $action)
	     {
	        $status     = $action['actionstatus'] - Action::USING_MATCHEDUSERID;
	        $updatedata = array("actionstatus" => $status, "owner" => $refId);
	        $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
	     }
	   }
	}
	
	public static function isUsingUserId($actionStatus)
	{
	  if((($actionStatus & Action::USING_USERID) == Action::USING_USERID) || 
	     (($actionStatus & Action::USING_MATCHEDUSERID) == Action::USING_MATCHEDUSERID))
	     {
	       return TRUE;
	     }
	  return FALSE;
	}
	
	function updateActionOwnerStatus($action)
	{
	  if((Action::USING_USERID & $action['actionstatus']) != Action::USING_USERID)
	  {
	     $status = "";
	     if((Action::USING_MATCHEDUSERID & $action['actionstatus']) == Action::USING_MATCHEDUSERID)
	     {
	       $status = ($action['actionstatus'] - Action::USING_MATCHEDUSERID) + Action::USING_USERID;
	     } else {
	       $status = $action['actionstatus'] + Action::USING_USERID;
	     }
	     $updatedata = array("actionstatus" => $status);
	     $this -> db -> updateData("action", $updatedata, array("id" => $action['ref']));
	     return $status;
	  }
	}
	
	function generateReport($reportData, Report $reportObj)
	{
        $actionManagerObj = new ActionReport();
        $joinWhere = $this -> createJoinsWhere($reportData['value']);
	   $fields    = $reportObj -> prepareFields($reportData['actions'], "A");
	   $groupBy   = $reportObj -> prepareGroupBy($reportData['group_by'], "A"); 
	   $sortBy    = $reportObj -> prepareSortBy($reportData['sort'], "A");
	   $whereStr  = $reportObj -> prepareWhereString($reportData['value'], $reportData['match'], "A");   
	   $whereStr  .= $joinWhere['where'];
	   $actions = $this -> filterActions($fields, $whereStr, $groupBy, $sortBy, $joinWhere['joins']);
	   $repActions = $actionManagerObj -> sortActions($actions, $reportData['actions']); 
	   $repActions['title'] = $reportData['report_title'];
        $reportObj -> displayReport($repActions, $reportData['document_format'], "actions");     
	}
	
     function filterActions($fields, $whereStr = "", $groupBy = "", $sortBy = "", $joins = "")
     {
	     $orderBy = (!empty($sortBy) ? "ORDER BY $sortBy " : "");
	     $groupBy = (!empty($groupBy) ? "GROUP BY $groupBy " : "");
		$results = $this -> db -> get("SELECT A.id, A.actionstatus, ".$fields." 
					                FROM #_action A
					                INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
					                INNER JOIN #_legislation L ON L.id = D.legislation
					                $joins
					                WHERE 1 
                                         AND (A.actionstatus & ".Action::ACTION_DELETE." <> ".Action::ACTION_DELETE." 
                                               AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                                    OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                                    OR A.actionstatus & ".ACTION::CONFIRMED." = ".ACTION::CONFIRMED."
                                                    OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                                                   )                           
                                           )					             				             
					                $whereStr
					                $groupBy
					                $orderBy
					            ");
		return $results;		
     }	
     
     function createJoinsWhere($options)
     {
         $whereStr = "";
         $joinStr  = "";
         if(isset($options['owner']) && !empty($options['owner']))
         {
            $ownerWhere = "";
            foreach($options['owner'] as $index => $owner)
            {
               if($owner !== "all")
               {
                  $ownerWhere .= " A.owner = '".$owner."' OR";
               }
            }
            if(!empty($ownerWhere))
            {
               $whereStr = " AND (".rtrim($ownerWhere, "OR").")";
            }           
         }
         if(isset($options['status']) && !empty($options['status']))
         {
            $allowStatus = 0;
            $statusWhere = "";
            foreach($options['status'] as $index => $status)
            {
               if($status != "all")
               {
                 $statusWhere .=  "  A.status = '".$status."'  OR";
                 $allowStatus += 1;
               }
            }
            if($allowStatus > 0)
            {
               $whereStr .= " AND (".rtrim($statusWhere, "OR").") ";
               $joinStr .= " INNER JOIN #_action_status AST ON AST.id = A.status ";
            }
         }
        $data['joins']  = $joinStr;
        $data['where']  = $whereStr; 
        return $data;
     }
	
}
