<?php
class DepartmentMatch extends Model
{
	
	function __construct()
	{
		parent::__construct();
	} 	
	
	function fetchAll()
	{
		$results = $this -> db -> get("SELECT * FROM #_dir_matches WHERE status & 2 <> 2");
		return $results;
	}
	
	function fetch($id)
	{
		$results = $this->db->getRow("SELECT * FROM #_dir_matches WHERE id = $id");
		return $results; 
	}
	
   /*
     Save the match between the master department and the compliance 2 client departments
   */	
	function saveDeptMatch($data)
	{
		$this -> setTablename("dir_matches");
		$id 	 = $this -> save($data);
		$response = array();
		if($id > 0)
		{
		  $deliverableObj = new Deliverable();
		  $deliverableObj -> updateDepartmentStatus($data['ref_id'], $data['dept_id']);   
		  $response = array("text" => "Department saved successfully", "error" => false);
		} else {
		   $response = array("text" => "Error saving the departments ", "error" => true);
		}
		return $response; 
	}
	
	function editMatch($data)
	{
		$id = $data['id'];
		unset($data['id']);
		$this -> setTablename("dir_matches");
		$res 	 = $this -> update($id, $data , new SetupAuditLog(), $this, new ActionNaming());
		$response  = array();
		if($res > 0)
		{
		    $response = array("text" => "Department updated successfully", "error" => false);
		} else if($res == 0) {
		  $response = array("text" => "No change was made to the departments ", "error" => true);   
		} else {
		   $response = array("text" => "Error updating the departments ", "error" => true);
		}		
		return $response; 
	}	
	
	function getObjKey()
	{
	   return "directorate";
	}
	
	function changeMatch( $data )
	{
		$this -> setTablename("dir_matches");
		$res 	  = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
		$response = array();
		if($res > 1)
		{
		    $response = array("text" => "Department updated successfully", "error" => false);
		} else if($res == 0) {
		  $response = array("text" => "No change was made to the departments ", "error" => true);   
		} else {
		   $response = array("text" => "Error updating the departments ", "error" => true);
		}
		return $response; 
	}		
	
	function deleteMatch($data)
	{
	   $id = $data['id'];
	   $match = $this -> fetch($id); 
        $auditLog = new SetupAuditLog();
        $dataArr['status'] = 2; 
        $this -> setTablename("dir_matches");
        $log  = $auditLog -> processChanges($this, $dataArr, $id, new ActionNaming());	   
	   
	   $res = $this -> db -> delete("dir_matches", "id=$id");
	   if($res > 0)
	   {
	      $delObj = new Deliverable();
	      $delObj -> undoDepartmentMatch($match['ref_id'], $match['dept_id']);	   	   
	   }
	   return $this -> updateMessage(" department matches ref #".$id, $res);
	}
	
}