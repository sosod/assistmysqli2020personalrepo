<?php
class DepartmentManager
{
     
   /*  
     Get list of compliance master departments , used and also those not used
   */  
   function getMasterDepartments()
   {
     //master dir-sub list 
     $deptObj = new MasterDepartment();
     $masterDirSub = $deptObj->getAll();
     //matches list
     $matchList    = $this->getDepartmentsMatch();
	$deptArr = array(); 
	foreach($masterDirSub as $dIndex => $dVal)
	{		
		if( array_key_exists($dIndex, $matchList))
		{					
	        $deptArr[$dIndex] = array("used" => true, "name" => $dVal['name'], "id" => $dIndex);	
		} else {
	       $deptArr[$dIndex] = array("used" => false, "name" => $dVal['name'], "id" => $dIndex);
		}
	}
	return $deptArr;     
   }
   /*
    Get the match of the master-department to the client departments
   */
   function getDepartmentsMatch()
   {
     //fetch the matches
	$deptmatchObj = new DepartmentMatch();
	$mainMatches  = $deptmatchObj -> fetchAll(); 		 			
	$matchList   = array(); 
	foreach($mainMatches as $m => $val)
	{
		$matchList[$val['ref_id']] = array("deptID" => $val['dept_id'] , "matchID" => $val['id'], "status" => $val['status']); 
	}     
	return $matchList;
   }

   /*
     Get the matches between the compliance master and client departments
   */
   function getAll()
   {
     $masterObj = new MasterDepartment();
     
     $clientObj = new ClientDepartment();     
     //master departments, from compliance 1
	$masterDepartments 	= $masterObj -> getAll();	
	//client departments, compliance 2
	$clientDepartments  = $clientObj -> getAll();	
     //department matches
	$departmentMatches        = $this -> getDepartmentsMatch();	

	$dirMatches  = array();
	foreach($departmentMatches as $masterID => $match)
	{
		if(isset($masterDepartments[$masterID]) && isset($clientDepartments[$match['deptID']]))
		{
		   $dirMatches[$match['matchID']]  = array("masterDept" => $masterDepartments[$masterID]['name'],
							                  "clientDept" => $clientDepartments[$match['deptID']]['name'],
							                  "status"     => $match['status']
   							                );		
		}
	}
	return $dirMatches;   
   }
   
   function getDeliverableDepartment($departmentId, $deliverableStatus, $clientDepart, $masterDepts)
   {
      $dptStr  = "";   
      if(Deliverable::isUsingClientDepartment($deliverableStatus))
      {
        if(isset($clientDepart[$departmentId]))
        {
          $dptStr = $clientDepart[$departmentId]['name'];
        } else {
          $dptStr = "Unspecified";
        }         
      } else {
         if(isset($masterDepts[$departmentId]))
         {
            $dptStr = $masterDepts[$departmentId]['name'];
         } else {
            $dptStr = "Unspecified";
         }
      }
      return $dptStr;
   }
   
   function getDelDepartment($departmentId, $deliverableStatus, $clientDeparts, $masterDepts)
   {
      $dptStr  = "";   
      if(Deliverable::isUsingClientDepartment($deliverableStatus))
      {
        if(isset($clientDepart[$departmentId]))
        {
          $dptStr = $clientDepart[$departmentId]['name'];
        } else {
          $dptStr = "Unspecified";
        }         
      } else {
         if(isset($masterDepts[$departmentId]))
         {
            $dptStr = $masterDepts[$departmentId]['name'];
         } else {
            $dptStr = "Unspecified";
         }
      }
      return $dptStr;
   }   

   


}
?>