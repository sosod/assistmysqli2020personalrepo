<?php
class ActionFactory
{
   
   public static function getInstance($section)
   {
      $classname = ucfirst($section)."Action";
      $obj = null;
      if(class_exists($classname))
      {
         $obj = new $classname();
      }   
      return $obj;
   }   
   

}
?>
