<?php
class ClientDeliverableOrganisationType extends Model
{
     
     function __construct()
     {
         parent::__construct();
     }     
     
     function fetchAll($options = "")
     {
        $results = $this->db->get("SELECT id, name, description, status FROM #_organisation_types WHERE status & 2 <> 2 $options ");
        return $results;
     }
     
     function fetch($id)
     {
        $result = $this->db->getRow("SELECT id, name, description, status FROM #_organisation_types WHERE id = '".$id."' ");
        return $result;
     }
     
	
	function updateOrganisationType( $data )
	{
		$this -> setTablename("organisation_types");
		$res 	  = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
		if(isset($data['status']) && $data['status'] == 2)
		{
		   if($res > 0)
		   {
		      $response = array('text' => 'Organisation type deleted successfully', 'error' => false);
		   } else {
		      $response = array('text' => 'Error deleting organisation type', 'error' => true);
		   }
		} else {
		   $response = $this -> updateMessage("organisation_types", $res);
		}
		return $response;
	}
	
	function saveOrganisationType( $data )
	{
		$this -> setTablename("organisation_types");
	    $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_organisation_types');

	    if(!empty($get_last_id))
	    {
	      $next_id = $get_last_id['id'] + 1;
	      if($get_last_id['id'] < 10000)
	      {
	        $data['id'] = 10000 + $next_id;
	      } else {
	        $data['id'] = $next_id;  
	      }
	    }	         		
		$id 	  = $this -> save($data);
		$response = $this -> saveMessage("organisation_types", $id); 
	   return $response;
	}
}
?>