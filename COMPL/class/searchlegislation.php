<?php
class SearchLegislation extends LegislationManager
{

	function __construct()
	{
		parent::__construct();	
	}	
	
	function getLegislations($start, $limit, $options = array())
	{
		$optionSql	= "";
		$optionSql .= $this->searchLegislationType($options['searchtext']);
		$optionSql .= $this->searchLegislationCategory($options['searchtext']);
		$optionSql .= $this->searchOrganisationSize($options['searchtext']);
		$optionSql .= $this->searchOrganisationCapacity($options['searchtext']);
		$optionSql .= $this->searchOrganisationType($options['searchtext']);
		$results = $this->legislationObj->searchLegislation( $options['searchtext'], $optionSql );
		$legislations = $this->sortLegislation( $results, new LegislationNaming(), "manage");
			
		print "<pre>";
			print_r($legislations);
		print "</pre>";
	}
	
	function searchLegislationType( $text )
	{
		$legtypes  = "";
		$legtypeObj = new LegislativetypeManager();
		$legtypes = $legtypeObj->search( $text );
		return $legtypes;
	}
	
	function searchLegislationCategory( $text )
	{
		$legcatObj = new LegislativeCategoriesManager();
		$categories = $legcatObj->search( $text );
		return $categories;	
	}	
	
	function searchOrganisationSize( $text )
	{
		$orgsizeObj = new OrganisationSizeManager();
		$orgsizeLeg = $orgsizeObj->search( $text );
		return $orgsizeLeg;
	}	
	
	function searchOrganisationCapacity( $text )
	{
		$capacityObj = new OrganisationCapacityManager();
		$capleg = $capacityObj->search($text);
		return $capleg;
	}
	
	function searchOrganisationType($text)
	{
		$orgtypeObj = new LegOrganisationTypesManager();
		$orgtypeleg = $orgtypeObj->search($text);
		return $orgtypeleg;
	}
	
}
?>
