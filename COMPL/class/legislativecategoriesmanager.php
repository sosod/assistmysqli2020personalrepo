<?php
class LegislativeCategoriesManager 
{
	
	private $legislativeCatObj =  null;
	
	function __construct()
	{
		$this->legislativeCatObj = new LegislativeCategories();
	}
	
	function getAll( $options = array())
	{
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql = " AND status & ".$options['status']." = ".$options['status']."";
		}
		$results = $this->legislativeCatObj->fetchAll( $optionSql );
		$mResults = $this->legislativeCatObj->getLegislativeCategories( $optionSql );
	
		foreach($mResults as $index => $cat)
		{
			$mcategories[$cat['id']] = $cat;
			$mcategories[$cat['id']]['imported'] = true;
		}
		$categories = array_merge($mcategories, $results);
		return $categories;
	}

	function getAllList($options=array())
	{
		$allCategories = $this->getAll($options);
		$categories 	= array();
		foreach($allCategories as $index => $cat)
		{
			$categories[$cat['id']] = $cat['name'];
		}
		return $categories;
	}
	
	function getLegislationCategories($id)
	{
        $categories = $this->legislativeCatObj->getCategoryByLegislationId($id);
	   $categoryList = array();
	   foreach($categories as $index => $category)
	   {
		$categoryList[$category['category_id']] = array("catid" => $category['category_id'], "legislation_id" => $category['legislation_id']);
	   }
	   return $categoryList;
	}

	function updateCategories($newcategories , $id)
	{
		$res = $this->legislativeCatObj->updateCategories( $newcategories, $id);
		return $res;	
	}
	
	function search( $text )
	{
		$clientCategories = $this->legislativeCatObj->searchCategories($text);
		$masterCategories = $this->legislativeCatObj->searchCategories2($text);
		$categories = array_merge($masterCategories, $clientCategories);
		$catSql = "";
		foreach($categories as $index => $val)
		{
			$legCats = $this->legislativeCatObj->getLegCat($val['id']);
			if(!empty($legCats))
			{
				$catSql .= " OR L.id = '".$legCats['legislation_id']."' ";
			}
		}
		return $catSql;
	}
	
	public static function getMasterLegislationCategory($legislationId)
	{
	  $categoryObj = new LegislationCategory();
	  $categories  = $categoryObj -> getAll();

	  $legislationcategories = $categoryObj -> masterCategoryByLegislationId($legislationId);
	    //$legislationCategories = $
	  $catStr = "";
	  foreach($legislationcategories as $index => $legCat)
	  {
	    if(isset($categories[$legCat['category_id']]))
	    {
	      $catStr .= $categories[$legCat['category_id']]['name']." ,";
	    }
	  }
	  return rtrim($catStr, " ,");
	} 
	
	public static function getLegislationCategory($legislationId)
	{
	    $categoryObj = new LegislationCategory();
	    $categories  = $categoryObj -> getAll();
  
	    $legislationcategories = $categoryObj -> getCategoryByLegislationId($legislationId);
	    //$legislationCategories = $
	    $catStr = "";
	    foreach($legislationcategories as $index => $legCat)
	    {
	      if(isset($categories[$legCat['category_id']]))
	      {
	        $catStr .= $categories[$legCat['category_id']]['name'].", ";
	      }
	    }
	    return rtrim($catStr, ", ");
	}
	
}
?>
