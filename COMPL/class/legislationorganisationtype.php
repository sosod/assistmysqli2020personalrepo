<?php
class LegislationOrganisationType extends Model
{
     protected static $tablename = "leg_organisation_types";

     function __construct()
     {
        parent::__construct();
     }

     function getAll()
     {
        $clientObj = new ClientLegislationOrganisationType();
        $clientTypes = $clientObj -> fetchAll();
        
        $masterObj = new MasterLegislationOrganisationType();
        $masterTypes = $masterObj -> fetchAll();
       
        $types = array_merge($masterTypes, $clientTypes);      
        $orgtypes = array();
        foreach($types as $tIndex => $type)
        {
          $orgtypes[$type['id']] = $type;
        }
        return $orgtypes;
     }

	function getOrganisationalTypeByLegislationId( $id )
	{
	   $results = $this -> db -> get("SELECT * FROM #_orgtype_legislation WHERE legislation_id = $id");
	   return $results;
	}	
	
	function importOrganisationalTypeByLegislationId( $id )
	{
	   $results = $this -> db2 -> get("SELECT * FROM #_orgtype_legislation WHERE legislation_id = $id");
	   return $results;
	}
	
	function getOrganisationType( $id )
	{
		$result  = $this -> db -> get("SELECT * FROM #_leg_organisation_types WHERE id IN('".$id."')");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
		    $response = "";
		}
		return trim($response, ",");		
	}	
	
	function updateOrganisationalTypes($newCats, $id)
	{
		$del = $this -> db -> delete("orgtype_legislation", "legislation_id = '".$id."'");
		foreach( $newCats as $index => $catId)
		{
			$this -> db -> insert("orgtype_legislation", array("orgtype_id" => $catId, "legislation_id" => $id));
		}
	}	
	
	function saveLegOrganisationType( $data )
	{
		$this->setTablename("leg_organisation_types");
	   $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_leg_organisation_types');

	   if(!empty($get_last_id))
	   {
	     $next_id = $get_last_id['id'] + 1;
	     if($get_last_id['id'] < 10000)
	     {
	        $data['id'] = 10000 + $next_id;
	     } else {
	        $data['id'] = $next_id;  
	     }
	   }		
		$legid 	 = $this->save($data);
		$response = $this->saveMessage("legislative_organisation_type", $legid); 
		return $response;
	}
	
	function updateLegOrganisationType( $data )
	{
		$this->setTablename("leg_organisation_types");
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this,new ActionNaming());
		$response = $this->updateMessage("legislation_organisation_type", $res);
		return $response;
	}
	
	function getOrgtypeLeg($orgtypeId)
	{
		$result = $this->db->getRow("SELECT orgtype_id, legislation_id FROM #_orgtype_legislation WHERE orgtype_id = '".$orgtypeId."' ");
		return $result;									
	}
		
	
}
