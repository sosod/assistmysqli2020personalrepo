<?php
class NewLegislation extends LegislationManager
{
	function __construct($section = "")
	{
		parent::__construct("new");
	}
	
	function getOptionSql($options = array())
	{
	   $optionSql = array();
	   $clientSql = "";
	   if(isset($options['page']) && $options['page']==="activate") {
		   $clientSql .= " AND L.legislation_status & ".Legislation::ACTIVATED." <> ".Legislation::ACTIVATED." 
							AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." 
							AND L.legislation_status & ".Legislation::CONFIRMED." = ".Legislation::CONFIRMED."";
	   } else {
		   $clientSql .= " AND L.legislation_status & ".Legislation::ACTIVATED." <> ".Legislation::ACTIVATED." 
							AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." 
							AND L.legislation_status & ".Legislation::CONFIRMED." <> ".Legislation::CONFIRMED." ";
		   if(isset($options['page']) && $options['page'] === "confirmation") {
			$clientSql .= "   AND (L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
							   OR  L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED."
							   OR  L.legislation_status & ".Legislation::COPIED." = ".Legislation::COPIED."
							) 
						";
		   } elseif(isset($options['page']) && $options['page']==="list") {
				/*$clientSql .= " AND L.legislation_status & ".Legislation::CONFIRMED." <> ".Legislation::CONFIRMED."
							 AND (L.legislation_status & ".Legislation::COPIED." = ".Legislation::COPIED." 
							 OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
							  ";*/
				$clientSql .= " AND (
										L.legislation_status & ".Legislation::COPIED." = ".Legislation::COPIED." 
										OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED."
										OR L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED."
									)
							  ";
		   } else {
			$clientSql .= " AND ( (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
						 AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." )
						 OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED."
						 OR L.legislation_status & ".Legislation::COPIED." = ".Legislation::COPIED."
						)  ";
			}
		}
	   if(isset($options['legoption']) && $options['legoption'] == "active")
	   {
			$clientSql .= " AND L.legislation_status & 1024 = 1024";
	   }		
		
	   if(isset($options['legoption']) && $options['legoption'] !== "confirmation") 
	   {
           if($options['viewType'] == "mine") 
           {
	        //$optionSql .= " AND L.admin IN(".$_SESSION['tid'].")";
           }		
	   }
       if(isset($options['legislation_id']) && !empty($options['legislation_id']))
       {
          $clientSql .= " AND L.id = '".$options['legislation_id']."' ";
       }
        

        if(isset($options['financial_year']))
        {
          $clientSql .= " AND financial_year = '".$options['financial_year']."' ";
        }
        
        if(isset($options['limit']) && !empty($options['limit']))
        {
          $clientSql .= " LIMIT ".$options['start'].",".$options['limit']; 
        }
     
        $optionSql['master'] = "";
        $optionSql['client'] = $clientSql;
        return $optionSql;
	}
	/*
	function sortLegislation($masterLegislations, $clientLegislations)
	{
	    $headerObj = new LegislationNaming();
	    $headers   = $headerObj -> headersList(); 

         $data = array();
         $data['status'] = array();
         $data['headers'] = array();
         $data['usedId'] = array();
         $data['admin'] = array();
         $data['authorizor'] = array();
         $data['isLegislationOwner'] = array();         
         foreach($clientLegislations as $masterId => $legislation)
         {
            if(isset($masterLegislations[$masterId]))
            {
               $data['usedId'][] = $masterId;
               if(Legislation::isLegislationOwner($legislation['admin']))
               {
                  $data['isLegislationOwner'][$masterId] = true;
               }
               if(isset($legislation['admin']) && !empty($legislation['admin']))
               {
                  $data['admin'][$masterId] = explode(",", $legislation['admin']);
               }
               if(isset($legislation['authorizer']) && !empty($legislation['authorizer']))
               {
                  $data['authorizor'][$masterId] = explode(",", $legislation['authorizer']);
               }               
               foreach($legislation as $key => $value)
               {
                 if(isset($headers[$key]))
                 {
                    if($key == "type")
                    {
                       $legtype = LegislativetypeManager::getLegislationType($value);
                       $data['legislations'][$masterId][$key] = $legtype;  
                    } else if($key == "category") {
                       $categories = LegislativeCategoriesManager::getMasterLegislationCategory($masterId);
                       $data['legislations'][$masterId][$key] = $categories;  
                    } else if($key == "organisation_size") {
                       $sizes  = OrganisationSizeManager::getMasterLegislationOrganisationSizes($masterId);
                       $data['legislations'][$masterId][$key] = $sizes;
                    } else if($key == "organisation_type") {
                       $orgtypeStr = LegOrganisationTypesManager::getMasterLegislationOrganisationType($masterId);
                       $data['legislations'][$masterId][$key] = $orgtypeStr;
                    } else if($key == "organisation_capacity") {
                       $capacityStr = OrganisationCapacityManager::getMasterLegislationOrganisationCapacity($masterId);
                       $data['legislations'][$masterId][$key] = $capacityStr;
                    } else {
                      $data['legislations'][$masterId][$key] = $value;
                    }
                    $data['headers'][$key] = $headers[$key];                
                 }
               }
               //$masterLegislations[$legislation['legislation_reference']];
               $data['status'][$masterId] = $legislation['legislation_status'];
            }
         }
         
		$data['company']= ucfirst($_SESSION['cc']);
		$data['user']   = $_SESSION['tkn'];
		$data['users']  = array();
		$data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
		$data['columns']= count($data['headers']);	         
          return $data;
	}	*/
	
	
	/*function getLegislations( $start, $limit, $options = array())
	{
	     $optionSql = "";
		$optionSql .= " AND L.legislation_status & ".Legislation::ACTIVATED." <> ".Legislation::ACTIVATED." ";
		if($options['legoption'] == "confirmation")
		{
			$optionSql .= " AND L.legislation_status & 512 <> 512  
						 AND (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE." 
						      AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
						   	 OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.") 
						 ";
		}
		if($options['legoption'] == "inactive") 
		{
			$optionSql .= " AND (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
							 AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
							 OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
					     ";
		}
		if($options['legoption'] == "active")
		{
			$optionSql .= " AND L.legislation_status & 1024 = 1024";
		}		
		
		if($options['legoption'] !== "confirmation") 
		{
            if($options['viewType'] == "mine") 
            {
	          //$optionSql .= " AND L.admin IN(".$_SESSION['tid'].")";
            }		
		}

		$myLegislations = $this->legislationObj->fetchLegislations($start, $limit, $optionSql);
		$legStatus      = array();
		//get legislation statuses
		foreach($myLegislations as $index => $leg)
		{
		   $legStatus[$leg['ref']] = $leg['legislation_status'];
		}
		$data = array();
		//sort legislation data
		$data = $this->sortLegislation($myLegislations, new LegislationNaming($options['page'])); 
		//$this->sortHeaders( $myLegislations, new LegislationNaming(), $page );
		if( isset($legislations['headers']['action_progress']) )
		{
			$legislationProgress = self::calculateLegislationProgress($legislations); 
			$legislations 		 = array_merge($legislations , $legislationProgress);
		}
		$totalLeg = $this->legislationObj->getTotallegislations($optionSql);
		$users = $this -> getUsers();
		$data['users'] = $users;  
		$data['usedId'] = array();
		$data['admin'] = array();
		$data['total'] = $totalLeg['totalleg'];
		$data['authorizor'] = array();		
		return $data;	
	}
	*/

	/*			
	function importLegislation($start, $limit, $options = "")
	{
	     //all the legislations from master setup
		$legislations  = $this->legislationObj->importLegislation( "", "" ); 
          //get all legislations that are availbale for importing
          $optionsSql    = " AND L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."";	
		$myLegislations= $this->legislationObj->fetchLegislations( $start, $limit, $optionsSql );
		
		$_legislations = array();
		$status        = array();
		$usedId 	   = array();
		$authorizor    = "";
		$admin         = "";
		$data          = array();
          $data['isLegislationOwner'] = array();	
		foreach($legislations as $index => $legData)
		{
			foreach($myLegislations as $k => $valArr )
			{
				if( $valArr['legislation_reference'] == $legData['ref'] )
				{
				//($valArr['legislation_status'] & 1)." and ".$valArr['legislation_status']."\r\n\n";
				if(($valArr['legislation_status'] & 1) == 1)
				{
				  $_legislations[$k] = $legData;
				}
				$admin 	  = (string)$valArr['admin'];
				$authorizor = (string)$valArr['authorizer'];
				$data['usedId'][$legData['ref']]     = $valArr['legislation_reference'];
				$data['status'][$legData['ref']]     = $valArr['legislation_status'];
				$legAdmin = explode(',', $admin);
				$data['admin'][$legData['ref']]  = $legAdmin; 
				if(in_array($_SESSION['tid'], $legAdmin))
				{
				    $data['isLegislationOwner'][$legData['ref']] = true;
				} else {
				     $data['isLegislationOwner'][$legData['ref']] = false;
				}
				$data['authorizor'][$legData['ref']] = explode(',', $authorizor);			
				}		
			}
		}	
			
		$data = array_merge($this->sortImports( $_legislations, new LegislationNaming(), new ComplianceManager()), $data);
		$data['company']= ucfirst($_SESSION['cc']);
		$data['user']   = $_SESSION['tkn'];
		$data['users']  = $this->getUsers();
		$total          = $this->legislationObj->getTotallegislations( $optionsSql );
		$data['total']  = $total['totalleg'];
		$data['columns']= count($data['headers']);		
		return $data;	
	}
	
	
	function importSaveAll( $legislationData ){
		
		$legislations = array();
		foreach( $legislationData  as $index => $legData)
		{
			//extracting the legislation id and the name of the status from a string
			$dataString = strrev( $legData['name'] );
			preg_match('/\d{1,9}/', $legData['name'], $matches);
			$id 		=  $matches[0];	
			$key 		= str_replace( "_".$id, "" ,$legData['name'] );   
			$legislations[$id][$key] =  $legData['value'];
		}
		$response = $this->saveImports($legislations);
		return $response;
		//retrieve and save	
	}

	function saveImports( $legislations )
	{
		$legislation  = new Legislation();
		$avilableLegs = $legislation->fetchAllAvailable();

		$legs 		  = array();
		$countSave    = 0; 
		$countError   = 0;
		$delResponse  = array();
		foreach( $legislations as $id => $leg){			
			foreach($avilableLegs as $index => $l){
				if( $id == $l['id']){
					$status = $this->calculateStatus($leg['available'], $leg['import_del'], $leg['edit_del'], $leg['create_del'], $leg['import_actions'], $leg['edit_actions'], $leg['create_actions'] );
					$l['legislation_status']    = $status;
					$l['legislation_reference'] = $l['id']; 
					unset( $l['id'] ); 
					$legislation->setTablename("legislation");
					if( $response  = $legislation->save($l) > 0 ){
						$countSave++;
					} else {
						$countError++;
					}
				}
			}
		}	
		$response = array();
		if( $countSave == 0 ){
			$response = array("error" => true, "text" => "An error occured importing and saving the legislations" );
		} else {
			$response = array("error" => false, "text" => $countSave." legislations successfully imported ".($countError == 0 ? "" : $countError." were not successfully saved") );
		}
		return $response;
	}
	
	
	public static function calculateLegislationProgress( $legislations )
	{
		$delObj     	= new DeliverableManager();
	    $deliverableObj = new Deliverable();
	    $legislationProgress = array();					
		foreach( $legislations as $index => $val)
		{
			if($index == "data"){
			foreach( $val as $id => $leg)
			 {
					$totalDeliverables = $deliverableObj -> fetchAll( $leg['ref'] );
	
					$deliverablesData  = $delObj -> getDeliverables(0, $totalDeliverables['totalDeliverables'], $leg['ref']);		
					$total = $deliverablesData['total'];
					$t = 0;
					foreach( $deliverablesData['totalProgress'] as $in => $progress)
					{
						$t = $t + intval($progress);  
					}
					$average = ($total == 0 ? 0 : round( ($t / $total), 2) );
					$legislationProgress[$index][$id] = $leg;
					$legislationProgress[$index][$id]['action_progress'] = $average." %";
				}	
			 }			
		 }	
		return $legislationProgress;
	}

	function confirmLegislation( $data )
	{
		$legislationData = $this->legislationObj-> fetch($data['id']);
		$legislationData['legislation_status'] = $legislationData['legislation_status'] + Legislation::CONFIRMED;
		$updatedata = array("legislation_status" => $legislationData['legislation_status'] );
		$this->legislationObj->setTablename("legislation");
		$res = $this->legislationObj->update($data['id'], $updatedata, new LegislationAuditLog());
		$response = array();

		if( $res == 1) {
			$activated 	 = array();
			$inactivated = array();	
			$ids 			 = array();
			$deactivatedIds = array();
			$activated 	 = (array)$data['activated'];
			$inactivated = (array)$data['deactivated'];
			$ids			 = $data['ids'];
			$deactivatedIds = $this->_checkIactivatedActivated( $inactivated, $activated );
			$deliverableObj = new Deliverable();			
			//get through all the deliverables and if its in deactivated array , do no confirm the deliverable
			//ie that inactivated deliverable is not going to be used in the calculations of compliance
			foreach($ids as $aIndex => $aVal)
			{
				if(!in_array($aVal['id'], $deactivatedIds))
				{
					$deliverable     		 = $deliverableObj->fetch($aVal['id']); 
					//if the deliverables is activated using the subevent ,
					//then do not confirm it , so it does not appear on the manage pages and for calculations 
					if(($deliverable['status'] & Deliverable::EVENT_ACTIVATED) !=  Deliverable::EVENT_ACTIVATED)
					{
						$delupdatedata['status'] = $deliverable['status'] + Deliverable::CONFIRMED;
						$deliverableObj->setTablename("deliverable");
						$res = $deliverableObj -> update($aVal['id'], $delupdatedata, new DeliverableAuditLog());		
						//update the actions for this deliverable as well , so that they can now appear on the dashboard 
						//and can be used to calculate the computations now
						if( $res > 0)
						{
							$actionObj = new Action();
							$actions   = $actionObj -> fetchByDeliverableId($aVal['id']);
							foreach($actions as $actionIndex => $actionVal)
							{
							  $actionupdateData['actionstatus'] = $actionVal['actionstatus'] + ACTION::LEGISLATION_ACTIVATED;
							  $actionObj->setTablename("action");
							  $ares = $actionObj->update($actionVal['id'], $actionupdateData, new ActionAuditLog());							
							}
						}
					}			
				} 
			}
			$response = array("text" => "Legislation confirmed . . .", "error" => false, "updated" => true);
		} else if( $res == 0){
			$response = array("text" => "No changes made to the legislation . . .", "error" => false, "updated" => false);
		} else {
			$response = array("text" => "Error confirming the legislation. . .", "error" => true, "updated" => false);
		}
		return $response;
	}	
	
	//checks the state of the deliverable id when form is submitted
	//
	function _checkIactivatedActivated( $inactivated, $activated)
	{
		//contain ids which appear more than once in the inactivated array
		$inactivatedDuplicates = array();
		//holds the ids which appear more than once in the activated array
		$activateDuplicates = array();
		//holds the ids of ths deliverables that have been deactivated and are in that state
		$deactivatedIds   = array();
		if(!empty($inactivated))
		{
			foreach($inactivated as $inIndex => $inVal)
			{
				$inactivatedDuplicates[$inVal][$inIndex] = $inVal; 
			}
		}
		if(!empty($activated))
		{
			foreach($activated as $aIndex => $aVal)
			{
				$activateDuplicates[$aVal][$aIndex] = $aVal;
			}
		}	
		foreach($inactivatedDuplicates as $keyId => $keyVal)
		{
			if(isset($activateDuplicates[$keyId]))
			{
				//if the number activated is less than number deactivated then its state is deactivated 
				if(count($activateDuplicates[$keyId]) < count($keyVal))
				{
					$deactivatedIds[$keyId]	= $keyId;
				}
			} else {
				$deactivatedIds[$keyId] = $keyId;
			}
		}
		return $deactivatedIds;
	}
	
	function editLegislation( $data )
	{
		$id 	= $data['id'];
		unset($data['id']);		
		$data = $this->prepareLegislation($data);
		$this->legislationObj->setTablename("legislation");
		$res 	  = $this->legislationObj->update($id, $data, new LegislationAuditLog(), $this->legislationObj, new LegislationNaming());
		$response = array();
		if(  $res > 0 )
		{
			$response = array("text" => "Legislation updated", "error" => false, "id" => $res, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function prepareLegislation( $data )
	{
		$insertdata  = array();
	    $multiple    = array("category", "organisation_size", "organisation_capacity", "organisation_type");
		foreach( $data as $key => $value)
		{
            if(in_array($key, $multiple))
            {
                $insertdata[$key] = implode(",",$value);
            } else{
                $insertdata[$key] = $value;
            }
		}
		return $insertdata;		
	}

	
	function updateEditedLegislation( $data )
	{
		$legislation	   = $this->legislationObj->fetch( $data['id'] );
		$editedLegislation = $this->legislationObj->importALegislation( $legislation['legislation_reference'] );
		$latestVersio = $this->legislationObj->getLatestLegislationVersion( $legislation['legislation_reference'] );
		$currentVersion = $this->legislationObj->getCurrentLegislationVersion( $data['id'] );
		$this->legislationObj->setTablename("legislation");
		$res 	  = $this->legislationObj->update($data['id'], $editedLegislation, new LegislationAuditLog(), $legObj, new LegislationNaming());
		$response = array();
		if(  $res > 0)
		{
			$updatedversion = array("current_version" => $latestVersion['id'], "version_in_use" => $latestVersion['id'] );
			$this->legislationObj->setTablename("legislation_usage");
			$text  = "";
			if( $this->legislationObj->update($currentVersion['id'], $updatedversion, new LegislationAuditLog()) > 0 ){
				$text = "Legislation updated to the current version";
			} else {
				$text = "";
			}
			$response = array("text" => ($text == "" ? "Legislation updated" : $text), "error" => false, "id" => $res, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
		
	}
		
	function searchLegislation($start, $limit, $options)
	{
		$legObj  	= new Legislation();
		$catObj 	= new LegislativeCategories(); 
		$orgSizeObj = new OrganisationSizes();
		$orgCapObj  = new OrganisationCapacity();
		$orgTypes   = new LegOrganisationTypes();
		$statObj 	= new LegislationStatus(); 
		
		$categoryId1  = $catObj -> searchCategories( $options ) ;
		$categoryId2  = $catObj -> searchCategories2( $options );
		$categoryIds  = $this->sortIds( $categoryId1, $categoryId2);
		
		$orgsizeIds1 = $orgSizeObj -> searchOrganisationSizes( $options );		
		$orgsizeIds2 = $orgSizeObj -> searchOrganisationSizes2( $options );
		$orgsizeIds  = $this->sortIds( $orgsizeIds1, $orgsizeIds2 ); 
		
		$capacityId1 = $orgCapObj -> searchCapacities( $options );
		$capacityId2 = $orgCapObj -> searchCapacities2( $options );
		$capacityIds = $this->sortIds( $capacityId1, $capacityId2);
		
		$typesId1 	 = $orgTypes -> searchOrganisationTypes( $options );
		$typesId2	 = $orgTypes -> searchOrganisationTypes2( $options ); 	
		$typesId 	 = $this->sortIds( $typesId1, $typesId2);  		
		
		$idsArray 	 =  array(
							  "category" 				=> $categoryIds,
							  "organisation_size" 		=> $orgsizeIds,
							  "organisation_capacity" 	=> $capacityIds,
							  "organisation_type"		=> $typesId
							 );
		$legislations = $legObj -> searchLegislation( $options,$idsArray);
		$legislations = $this->sortHeaders( $legislations, new LegislationNaming(), "manage" );
		
		if( isset($legislations['headers']['action_progress']) ){
			$legislationProgress = self::calculateLegislationProgress($legislations); 
			$legislations 		 = array_merge($legislations , $legislationProgress);
		}
		$response = array();
		if( $legislations['total'] > 1)
		{
			$response  = $legislations['total']." results found";
		} else {
			$response  = $legislations['total']." result found";
		}
		return array("usedId" => array(), "data" => $legislations, "status" => array(), "text" => $response );
	}
	
	function sortIds( $idArr1, $idArr2 )
	{
		$idArr = array_merge($idArr1, $idArr2);
		$ids = "";
		if(!empty($idArr))
		{
			foreach( $idArr as $valArr){
				$ids = $valArr['id'].",";
			}
		}	
		$ids = rtrim( $ids, ",");
		return $ids;
	}
	
	function advancedSearch( $start, $limit, $options)
	{		
		$legObj  	= new Legislation();
		$searchArray = array();
		foreach( $options as $index => $valArr)
		{
			if( strstr($valArr['name'], "_multiple"))
			{	
				$name = str_replace("_multiple", "", $valArr['name'] );
				$searchArray[$name][]= $valArr['value'];
			} else {
				$searchArray[$valArr['name']]= $valArr['value'];
			} 
		}
		$legislations = $legObj -> advancedLegislationSearch( $searchArray );
		$legislations = $this->sortHeaders( $legislations, new LegislationNaming(), "manage" );
		if( isset($legislations['headers']['action_progress']) ){
			$legislationProgress = self::calculateLegislationProgress($legislations); 
			$legislations 		 = array_merge($legislations , $legislationProgress);
		}
		$response = array();
		if( $legislations['total'] > 1)
		{
			$response  = $legislations['total']." results found";
		} else {
			$response  = $legislations['total']." result found";
		}
		return array("usedId" => array(), "data" => $legislations, "status" => array(), "text" => $response );		
	}
	
	function getUpdatedLegislations()
	{
		$legObj 		= new Legislation();
		$currentVersion = $legObj -> getCurrentVersion();
		$newUpdate 		=  array();
		foreach( $currentVersion as $index => $value)
		{		
			$latestVersion  = $legObj -> getLatestLegislationVersion( $value['legislation_id'] );
			if($latestVersion['id'] != $value['version_in_use'])
			{
				$newUpdate[] = $value['legislation_id'];
			}			
		}
		$results = array();
		if(!empty($newUpdate))
		{
			$options = " AND L.legislation_reference IN('".implode(",", $newUpdate)."')";
			$results = $this-> getLegislations( 0,10, $options, "manage");
		}
		return $results;
	}
	
	function allowUpdate( $data )
	{
		list($refId , $idRef ) = explode("_", $data['ref'] );
		$ref 	 	  	   = substr($refId, 6);
		$id 		  	   = substr($idRef, 2 );				
		$legObj 		   = new Legislation();
		$latestlegislation = $legObj -> importALegislation( $ref );
		unset($latestlegislation['status']);
		$latestversion     = $legObj -> getLatestLegislationVersion( $ref );
		if(!empty($latestlegislation)) {
			$legObj->setTablename("legislation");
			$res = $legObj -> update($id, $latestlegislation, new LegislationAuditLog());
			if( $res > 0)
			{
				$usage = array("current_version" => $latestversion['id'], "version_in_use" => $latestversion['id'] );
				$legObj->setTablename("legislation_usage");
				$legObj -> update("legislation_id={$id}", $usage, new LegislationAuditLog() );
			}				
		}
		$response = array();
		if(  $res > 0)
		{
			$response = array("text" => "Legislation updated", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function fetchLegislations( $args = "" )
	{
		$option = "";
		if(isset($args) && !empty($args))
		{
			if(is_array($args))
			{
				foreach($args as $key => $value)
				{
					$option .= " AND ".$key." = '".$value."' ,"; 
				}
			}
		}
		$option = rtrim($option, ",");
		$legObj = new Legislation();
		$legislation = $legObj -> fetchAll( $option );
		return $legislation;		
	}		
	*/
}
?>
