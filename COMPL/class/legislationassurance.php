<?php
class LegislationAssurance extends Model
{
     protected static $tablename = "legislation_assurance";
     
	function __construct()
	{
		parent::__construct();			
	}
	
	function fetchAll( $start, $limit, $id)
	{
		$results = $this->db->get("SELECT LA.id, LA.insertdate , LA.date_tested, LA.response, LA.sign_off,
							LA.attachment ,CONCAT(TK.tkname,' ',TK.tksurname) AS user 
							FROM #_legislation_assurance LA
							INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = LA.insertuser
							WHERE LA.legislation_id = {$id}
							AND LA.status & 2 <> 2
							ORDER BY LA.id 
 						 ");
		return $results;
	}
	
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT LA.id,LA.insertdate, LA.date_tested, LA.response, LA.sign_off, 
								CONCAT(TK.tkname,' ', TK.tksurname) AS user, LA.status, LA.attachment 
								FROM #_legislation_assurance LA 
								INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = LA.insertuser
								WHERE LA.id = $id
								  ");
		return $result;
	}
	
	function fetchAssurances( $id )
	{
		$result = $this->db->getRow("SELECT COUNT(*) AS total 
							      FROM #_legislation_assurance LA
							      WHERE legislation_id = {$id} 
							      AND LA.status & 2 <> 2
							      ");
		return $result;
	}
}
