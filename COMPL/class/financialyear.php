<?php
class FinancialYear extends Model
{
	protected static $table = "financial_year";
	
	protected $financialyear;
	
	protected $startdate;
	
	protected $enddate;
	
	function __construct($financialyear = "", $startdate = "", $enddate = "")
	{
	     $this -> financialyear = $financialyear;
		parent::__construct();
	}
	
	function fetchAll() {
	   $results = $this -> db -> get("SELECT 
	                                  CONCAT(date_format(start_date, '%d-%M-%Y'), ' - ' ,date_format(end_date, '%d-%M-%Y')) 
	                                  AS year_date, start_date, end_date, id, status, value
	                                  FROM assist_".$_SESSION['cc']."_master_financialyears WHERE status & 1 = 1");
	   $financialyears = array();
	   foreach($results as $index => $financialyear)
	   {
		 $financialyears[$financialyear['id']] = $financialyear;
	   }
	   return $financialyears;
	}

	function fetchAllSorted() {
	   $results = $this -> db -> get("SELECT 
	                                  CONCAT(date_format(start_date, '%d-%M-%Y'), ' - ' ,date_format(end_date, '%d-%M-%Y')) 
	                                  AS year_date, start_date, end_date, id, status, value
	                                  FROM assist_".$_SESSION['cc']."_master_financialyears WHERE status & 1 = 1
									  ORDER BY start_date");
	   $financialyears = array();
	   //$x =10;
	   foreach($results as $index => $financialyear)
	   {
			$x = strtotime($financialyear['start_date']);
			while(isset($financialyears[$x])) {
				$x++;
			}
		 $financialyears[$x] = $financialyear; 
		 //$x++;
	   }
	   return $financialyears;
	}
	
	
	function fetch($id)
	{
		$result = $this->db->getRow("SELECT * FROM assist_".$_SESSION['cc']."_master_financialyears WHERE id = $id");
		return $result;
	}
	
	function fetchFinancialYear()
	{
	   $results = $this->db->get("SELECT id , master_id, status , insertdate, insertuser FROM #_financial_year WHERE status & 2 <> 2");
	   return $results;
	}
	
	function createFinancialYearDate($financialyearId, $date)
	{
	   $this -> financial_year   = $financialyearId;
	   $finacialyear = $this -> fetch($financialyearId);
	   $financialYearStartMonth  = substr($finacialyear['start_date'], 5, 2);
	   $parsedDate               = date_parse($date);
	   $dateMonthValue           = $parsedDate['month'];
	   
	   $day      = str_pad($parsedDate['day'], 2, 0, STR_PAD_LEFT);
	   $month    = str_pad($dateMonthValue, 2, 0, STR_PAD_LEFT);
	   $dayMonth = $day."-".$month;
	   //calculate the months from the send date to the financial year start date
	   $monthOfDateFromStartOfFinYear = $dateMonthValue - $financialYearStartMonth;
	   $newDateStr = "";
	   //if the send date is after the financial year start and its in the this year to end of this year
	   if($monthOfDateFromStartOfFinYear > 0)
	   {
	     $year = substr($finacialyear['start_date'], 0, 4);
	     $newDateStr = $dayMonth."-".$year;
	   } else {
	     $year = substr($finacialyear['end_date'], 0, 4);
	     $newDateStr = $dayMonth."-".$year;	     
	   }
        return date("d-M-Y", strtotime($newDateStr));     
	}
	
	function saveLegislationCopied($legislationid, $fromyear, $toyear)
	{    
	  $this -> setTablename("legislation_financialyear");
	  $insertdata = array("legislation_id" => $legislationid, "fromyear" => $fromyear, "toyear" => $toyear);
	  $res = $this -> save($insertdata);
	  return  $res;
	}
	
	function getLegislationFinancilaYear($from, $to)
	{
	   $results  = $this -> db -> get("SELECT * FROM #_legislation_financialyear WHERE fromyear = '".$from."' AND toyear = '".$to."' ");
	   return $results;	
	}
}
?>