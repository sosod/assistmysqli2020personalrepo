<?php
/*
 * Defines how and what legislation are to appear on the setup pages 
 */
class SetupLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct();
	}
     /*
	function getLegislations($start ,$limit, $options = "")
	{
          //$legislations  = $this->legislationObj-> getAll();	
	     
		$legislations   = $this->legislationObj->importLegislation( $start, $limit ); 
		$myLegislations = $this->legislationObj->fetchLegislations("", "");
		//get the sorted data, with headers that have been setup
		$results 		= $this->sortLegislation($legislations, new LegislationNaming("setup"));
		$allowedHeaders = array("ref", "legislation_name", "responsible_business_patner");
		$data           = array();
		$usedId         = $this->getUsedId();			
		$total          = $this->legislationObj->getTotalImports();		
		$data 		 = $this->newSort( $results, $myLegislations, $usedId, $allowedHeaders , $total['totalDel']);
		$data['usedId'] = $usedId['usedId'];
		$data['status'] = $usedId['status']; 			
		$data['company']= ucfirst($_SESSION['cc']);
		$data['user']   = $_SESSION['tkn'];
		$data['users']  = $results['users'];
		$data['total']  = $total['totalDel'];		
          $data['isLegislationOwner']  = array();
		$data['columns']= count($data['headers']);
		return $data;
	}*/
	
	function sortLegislation($masterLegislations, $clientLegislations = array())
	{
	   $headerObj                               = new LegislationNaming(); 
	   $headers                                 = $headerObj -> getHeaderList();
	   
	   $legAdminObj                             = new LegislationAdmin();
	   $legAuthorizerObj                        = new LegislationAuthorizer();
	   $setupLegislations                       = array();
	   $setupLegislations['usedId']             = array();
	   $setupLegislations['admin']              = array();
	   $setupLegislations['authorizor']         = array();
	   $setupLegislations['status']             = array();
	   $setupLegislations['isLegislationOwner'] = array();
	   foreach($masterLegislations as $mIndex => $mLegislation)
	   {
	      $setupLegislations['headers']['id']                          = $headers['id'];
	      $setupLegislations['headers']['legislation_name']            = $headers['name'];
	      $setupLegislations['headers']['responsible_business_patner'] = $headers['business_patner'];
	      
	      $setupLegislations['legislations'][$mIndex]['id']                          = $mIndex;
	      $setupLegislations['legislations'][$mIndex]['legislation_name']            = $mLegislation['name'];
	      $setupLegislations['legislations'][$mIndex]['responsible_business_patner'] = $mLegislation['responsible_business_patner'];
	      if(isset($clientLegislations[$mIndex]))
	      {
	         $ref         = $clientLegislations[$mIndex]['id'];
	         $authorizers = $legAuthorizerObj -> getLegislationAuthorizers($ref);
	         $admins      = $legAdminObj -> getLegislationAdmins($ref);

	         $setupLegislations['admin'][$mIndex]      = $admins; 
	         $setupLegislations['authorizor'][$mIndex] = $authorizers; 
	         $setupLegislations['status'][$mIndex]     = $clientLegislations[$mIndex]['legislation_status'];
	         $setupLegislations['usedId'][]            = (int)$mIndex;
	         if(Legislation::isLegislationOwner($admins))
	         {
	           $setupLegislations['isLegislationOwner'][$mIndex] = TRUE;
	         }
	      }
	    }  	   
		if(empty($setupLegislations['headers']))
		{
		  $setupLegislations['headers'] = $headers;
		}
	   $setupLegislations['columns'] = count($setupLegislations['headers']);
       return $setupLegislations;
	}
	
	function getOptionSql($options = array())
	{
	   $optionSql = array();
	   $masterSql = "";
        if(isset($options['id']) && !empty($options['id']))
        {
          $masterSql .= " AND id = '".$options['id']."' ";
        }  
        if(isset($options['limit']))
        {
          $masterSql = " LIMIT ".$options['start'].",".$options['limit'];
        }  
        $optionSql['master'] = $masterSql;
        $optionSql['client'] = "";
        return $optionSql;                 	     
	}
	
		
	
	/*function newSort($legislations, $myLegislations, $usedId, $allowedHeaders, $total)
	{
	     $totalL = 0;
		$data   = array();
		$data['authorizor'] = array();
		$data['admin']      = array();
		$legislations  = $this->sortLegislation($legislations, new LegislationNaming("setup"));
		foreach($legislations['headers'] as $hKey => $header)
		{
			if( in_array($hKey, $allowedHeaders))
			{
				$data['headers'][$hKey] = $header; 
			}			
		}
		foreach($legislations['legislations'] as $lIndex => $leg)
		{
			foreach($leg as $key => $val)
			{
				if( in_array($key, $allowedHeaders))
				{
					$data['legislations'][$lIndex][$key] = $val;
				}
			}	
			if( in_array($leg['ref'], $usedId['usedId']))
			{	
				$totalL++;
				foreach($myLegislations as $i => $myArr)
				 { 
			 		if( $leg['ref'] == $myArr['legislation_reference'])
					{
						//$data['status'][$leg['ref']]     = $myArr['legislation_status'];
						$data['authorizor'][$leg['ref']] = explode(",", $myArr['authorizer']);
						$data['admin'][$leg['ref']] 	 = explode(",", $myArr['admin']); 
					}	
				}
			} else {
				$total--;				
			} 				
		}		
		return $data;
	}*/
	

	

		
}
