<?php
@session_start();
class FileUpload
{
	
	private $uploadDir = "";
	
	private $files = array();
	
	private $errors = array();
	
	private $uploadtype = "";
	
	//optional upload type defined by user
	private $type 		= "";
	
	function __construct( $uploadtype, $key = "attachment")
	{
		$this->uploadtype = $uploadtype;
		if(isset($_FILES) && !empty($_FILES)){
			$this->files 	  = $_FILES[$key]; 
		}
	}

	public function upload($filetypes = array(), $files = "", $attachmentname = "")
	{
		$name  = $this->files['name'];
		$type  = $this->files['type'];
		$tmp   = $this->files['tmp_name'];	
		$ext = substr($name, strpos($name, ".")+1);
		if( in_array($ext, $filetypes)) {	 		
			if( $this->_validateFile($this->files))
			{			
				$this->_createDir();
				$attName = "";
				if( $attachmentname == "")
				{
					$attName = $this->uploadtype; 
				} else {
					$attName = $attachmentname;
				}					
				
				$file = "cmpl_".date("YmdHis").".".$ext;
				$destination = $this->uploadDir."/".$this->uploadtype."/".$file; 
				if( move_uploaded_file($tmp, $destination))
				{
					$_SESSION['uploads'][$attName][$file] = $name;
					$fileUploads = array();
					foreach($_SESSION['uploads'][$attName] as $key => $fValue)
					{
						if( file_exists($this->uploadDir."/".$this->uploadtype."/".$key))
						{
							$fileUploads[$key] = $fValue;
						} else {
							unset($_SESSION['uploads'][$attName][$key]);
						}
					}
					$response = array(
									  "files" => $fileUploads,
									  "file"  => $name,
									  "text"  => $name." successfully uploaded",
									  "error" => false 
									);
					return  $response;
				} else {
					return  array("error" => true, "text" => "Error uploading file");
				}						
			} else {
				return $this->_errorMessage( $this->errors );
			}
		} else {
			$error['file_type'] = "You selected an invalid file type, use csv file type only";
			return $this->_errorMessage( $error );
		}	
	}
	
	private function _createDir()
	{
		$this->uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref'];
		if(!is_dir($this->uploadDir))
		{
			if(mkdir($this->uploadDir))
			{
				if(mkdir($this->uploadDir."/".$this->uploadtype))
				{
					mkdir($this->uploadDir."/".$this->uploadtype."/deleted");
				}				
			}
		}		
	}
	 
	private function _validateFile( $file )
	{
		switch ($this->files['error'])
		{
			case UPLOAD_ERR_OK:
				return true;
			break;
			case UPLOAD_ERR_INI_SIZE:
				$this->errors['ini_size'] = "Upload size exceeds maximum set";
				return false;
			break;
			case UPLOAD_ERR_FORM_SIZE:
				$this->errors['form_size'] = "Upload size exceeds maximum set in the form";
				return false;
			break;
			case UPLOAD_ERR_PARTIAL:
				$this->errors['partial']   = "Uploaded file was only partial";
				return false;
			break;
			case UPLOAD_ERR_NO_FILE:
				$this->errors['nofile']    = "There was no file chosen";
				return false;
			break;
			case UPLOAD_ERR_CANT_WRITE:
				$this->errors['cant_write'] = "Failed to write to disk";
				return false;
			break;
			default:
				return true;
			break;
		}
		
	}
	
	// remove recently uploaded file from the file system
	function removeFile( $file, $name)
	{
		$this->_createDir();	
		$oldname = $this->uploadDir."/".$this->uploadtype."/".$file;
		if(unlink($oldname))
		{
			unset($_SESSION['uploads']['cmplaction'][$file]);
			return array("text" => $name." successfully removed", "error" => false);
		} else {
			return array("text" => "An error occured removing ".$name.", please try again");
		}
	}
	
	//deletes previously saved file from the the file system and the database
	function deleteFile($file, $name, $attname = "")
	{
		$this->_createDir();
		$attName = "";
		if( $attname == "")
		{
			$attName = $this->uploadtype; 
		} else {
			$attName = $attname;
		}				
		$oldname = $this->uploadDir."/".$this->uploadtype."/".$file;
		$newname = $this->uploadDir."/".$this->uploadtype."/deleted/".$file;
		if( rename($oldname, $newname))
		{
			$_SESSION['uploads'][$attName][$file] = $name;
			return array("text" => $name." successfully deleted", "error" => false); 
		} else {
			return array("text" => "Error occured removing ".$name, "error" => true);
		}	
	}
	
	//check the diffence made on the attachment , added, deleted from the system
	public static function processAttachmentChange( $attachments )
	{
		$changes = array();
		$att 	 = array();
		if( isset($attachments) && !empty($attachments))
		{
			$att = unserialize(base64_decode($attachments)); 		
		}
		
		if( isset($_SESSION['uploads']['cmpldeleted']) && !empty($_SESSION['uploads']['cmpldeleted']))
		{
			if(!empty($att)){
				foreach( $att as $key => $val)
				{
					if( isset($_SESSION['uploads']['cmpldeleted'][$key]))
					{
						$changes[$key] = "Attachment ".$val." has been deleted";
						unset($att[$key]);
					}
				}
			}
		}
		
		if( isset($_SESSION['uploads']['cmpladded']) && !empty($_SESSION['uploads']['cmpladded']))
		{
			foreach ($_SESSION['uploads']['cmpladded'] as $key => $val)
			{
			  $att[$key] 	   = $val;
			  $changes[$key] = "Attachment ".$val." has been added";  
			}
		}
		if(isset($changes) && !empty($changes))
		{
			$_SESSION['uploads']['actionchanges'] = $changes;
		}
		if(isset($att) && !empty($att))
		{
			$_SESSION['uploads']['attachments'] = base64_encode(serialize($att));
		}
	}
	
	private function _errorMessage( $errorArr )
	{
		$errorsResponse = array();
		$errorText = "";
		if( !empty($errorArr))
		{				
				foreach($errorArr as $key => $eVal)
				{
			      $errorText .=  $eVal."<br />";
				}			
 				$errorsResponse = array("text" => $errorText, "error" => true);
		}
		return $errorsResponse;
	}
	
	
	//write the meta data of the imported or uploaded files to a file 
	public function writeImported( $key )
	{
		$this->_createDir();
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype))
		{
			try{
				$contents = "";
				$filelocation = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/importedfiles.txt";
				$handle = fopen($filelocation, "a+");
				//$size   = filesize("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/importedfiles.txt") * 1024;
				//echo "File size is ".$size."<br />";  
				while(!feof($handle))
				{
					$contents .= fread($handle, 1024);
				}
				$newContents = array();
				$oldContents = array();
				if(!empty($contents))
				{
					$oldContents = unserialize($contents);
					if(!empty($_SESSION['uploads'][$this->uploadtype]))
					{
						$newContents = array_merge($oldContents, $_SESSION['uploads'][$this->uploadtype]);
						unset($_SESSION['uploads'][$this->uploadtype]);
					} else {
						$newContents = $oldContents; 
					}
				} else {
					$newContents = $_SESSION['uploads'][$this->uploadtype];
					unset($_SESSION['uploads'][$this->uploadtype]);
				}	
				if(!empty($newContents))
				{
					fwrite($handle, serialize($newContents));
				}					
				fclose($handle);
			} catch(Exception $e){
				echo "An exception occured ".$e->getMessage();
			}
		}
	} 
	
	//load the files uploaded from the imports directory
	public function readImported()
	{
		$this->_createDir();
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype))
		{
			try{
				$contents = "";
				$filelocation = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/importedfiles.txt";
				$handle = fopen($filelocation, "r");
			    while (($buffer = fgets($handle, 4096)) !== false) {
			        echo $buffer;			        
			    }
			    if (!feof($handle)) {
			        echo "Error: unexpected fgets() fail\n";
			    }				
				/*
				while(!feof($handle))
				{
					$contents .= fread($handle, 1024);
				}
				*/
				$newContents = array();
				$oldContents = array();
				if(!empty($contents))
				{
					$oldContents = unserialize($contents);
					print "<pre>";
						print_r($oldContents);
					print "</pre>";
				}			
				fclose($handle);
			} catch(Exception $e){
				echo "An exception occured ".$e->getMessage();
			}
		}
		
	/*	$this->_createDir();
		try{
			$contents = "";
			$filelocation = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/importedfiles.txt";
			$handle = fopen($filelocation, "a+");
			while(!feof($handle))
			{
				$contents .= fread($handle, 1024);
			}
			if(!empty($contents))
			{
				$oldContents = unserialize($contents);
			}
			print "<pre>";
				print_r($oldContents);
			print "</pre>";
			fclose($handle);
		} catch(Exception $e){
			echo "Exception occured ".$e->getMessage();
		}	
		*/
	}	
	
	
	//read the contents of the file and store them into an array
	public function readfileContents( $folder , $file )
	{
		$fileLocation = $this->uploadDir."/".$folder."/".$file; 
		try{
			$handle = fopen($fileLocation, "r");
			$fileContents= array();
			while(!feof($handle)){
				$contents    = fgetcsv($handle);
			       if(count($contents) > 1){
			           $fileContents[] = $contents;
			       }
			       unset($contents);
			}
			fclose($handle);
			return $fileContents;
		} catch(Exception $e){
			echo "An un-expected error occured ".$e -> getMessage();
		}		
	}
	
}
