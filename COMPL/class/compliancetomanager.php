<?php
class ComplianceToManager
{
	private $compliancetoObj = null;

	function __construct()
	{
	   $this->compliancetoObj = new ComplianceTo();
	}
	
	function getComplianceToTitles()
	{
	    $compliancetoObj = new ComplianceTo();
	    $titleList = $compliancetoObj -> getAll();
	   
	    $matchList = $this->getMatchList();
	    
	    $compliancetoList = array();
	    foreach($titleList as $cID => $cVal)
	    {
	      if(isset($matchList[$cID]))
	      {
	         $compliancetoList[$cID] = array("name" => $cVal['name'], "status" => $cVal['status'], "used" => true);
	      } else {
	         $compliancetoList[$cID] = array("name" => $cVal['name'], "status" => $cVal['status'], "used" => false);
	      }
	    }
	    return $compliancetoList;
	}
	
	 function getMatchList()
	 {
	     $matchObj = new ComplianceToMatch();
	     $matches  = $matchObj -> fetchAll();
	     
	     $matchlist = array();
	     foreach($matches as $mIndex => $mVal)
	     {
	        $matchlist[$mVal['ref']] = array("userID" => $mVal['user_id'], "status" => $mVal['status'], "matchID" => $mVal['id'] );
	     }
	     return $matchlist;
	 }
	
	function getAll()
	{
	     $titlelist = $this->getComplianceToTitles();
	     
	     $matchlist  = $this->getMatchList();
	     
	     $userObj = new User();
	     $userlist = $userObj -> getAll();
	     
	     $complianceToMatches = array();
	     
	     foreach($matchlist as $refID => $match)
	     {
	       $user = $title = "";
	       if(isset($titlelist[$refID]))
	       {
	          $title = $titlelist[$refID]['name'];
	       }
	       if(isset($userlist[$match['userID']]))
	       {
	          $user = $userlist[$match['userID']]['user'];
	       } else if(isset($titlelist[$match['userID']])) {
	          $user = $titlelist[$match['userID']]['name'];
	       } else {
	          $user = $title." (Unknown)";
	       }

	       $complianceToMatches[$match['matchID']] = array("user" => $user, "title" => $title, "status" => $match['status'], "id" => $match['matchID']);
	     }	
	    return $complianceToMatches;
	}
	
	function getList()
	{
	    $compliances = $this->getAll();
	    $list = array();
	    foreach($compliances as $cIndex => $cVal)
	    {
	     $list[$cVal['id']] = ($cVal['user']==$cVal['title']) ? $cVal['title'] : $cVal['title']." (".$cVal['user'].")";
	    }
	    return $list;
	}
	
	function getImportList() {
	    $compliances = $this->getAll();
	    $list = array();
	    foreach($compliances as $cIndex => $cVal) {
			$list[$cVal['id']] = $cVal['title'];
	    }
	    return $list;
	}
	
     /*
      @deliverableid - deliverable id
      @compliancesTo - all compliances to matches
      @mastertitles  - titles from the master tables
    
     */
	function getDeliverableComplianceTo($deliverableId, $compliancesto, $mastertitles, $complianceToMatches)
	{
	   $delCompObj          = new DeliverableComplianceTo();
	   $deliverableComplianceTo = $delCompObj -> fetchByDeliverableId($deliverableId); 
	   //if there are no matches , look for the titles coming from the master compliance table
        $dCompliancetoStr = "";
	   if(!empty($complianceToMatches))
	   {
	      //if there are matches then look in the match list
	      foreach($deliverableComplianceTo as $dIndex => $dVal)
	      { 
	        //if the delierable is using the match id, then use the match compliance to 
	        // else use the old match
	        if(DeliverableComplianceTo::isUsingDeliverableComplianceToMatch($dVal['status']))
	        {
	             if(isset($compliancesto[$dVal['complianceto_id']]))
	             {
	               $dCompliancetoStr .= $compliancesto[$dVal['complianceto_id']]['title']." (".$compliancesto[$dVal['complianceto_id']]['user'].") ,";
	             }	          
	        } else {
	          if(isset($mastertitles[$dVal['complianceto_id']]))
	          {
	             $dCompliancetoStr .= $mastertitles[$dVal['complianceto_id']]['name']." ,";
	          }
	        }
	      }
	   } else {
	      foreach($deliverableComplianceTo as $dIndex => $dVal)
	      { 
	         if(isset($mastertitles[$dVal['complianceto_id']]))
	         {
	           $dCompliancetoStr .= $mastertitles[$dVal['complianceto_id']]['name']." ,";
	         }
	      }
	      if(empty($dCompliancetoStr))
	      {
	          $dCompliancetoStr = "Unspecified" ;
	      }
	   }
	   return rtrim($dCompliancetoStr, " ,");
	}
	
	function getDeliverableComplianceToIds($deliverableId)
	{
	   $delCompObj = new DeliverableComplianceTo();
	   $deliverableComplianceTo = $delCompObj -> fetchByDeliverableId($deliverableId);
	   
	   $listId = array();
	   foreach($deliverableComplianceTo as $dIndex => $dVal)
	   {
	     $listId[$dVal['complianceto_id']] = $dVal['deliverable_id'];
	   }
	   return $listId;
	}

}
?>