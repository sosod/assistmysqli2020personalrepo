<?php
//responsible for creating setup objects
class SetupFactory implements Setup{

		private static $instance;
	
		public static function createInstance( $classname )
		{
			if(!self::$instance){
				$instance = new $classname();
			}
			return $instance;
		}

}