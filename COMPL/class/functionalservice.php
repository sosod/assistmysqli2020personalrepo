<?php
class FunctionalService extends Model
{
     
     protected static $table  = "functional_service";
     
     function getAll()
     {
        //functional services from the master tables
        $masterObj = new MasterFunctionalService();
        $masterFxnServices = $masterObj -> fetchAll();
        
        //custom functional service titles from the client database
        $customObj = new CustomFunctionalService();
        $customFxnServices = $customObj -> fetchAll();
        
        $matchObj = new FunctionalServiceMatch();
        $titleMatches  = $matchObj -> fetchAll(); 
        
        $titles = array_merge($masterFxnServices, $customFxnServices);
        
        $titleList = array();
        foreach($titles as $index => $title)
        {
          $titleList[$title['id']] = array("name" => $title['name'], "status" => $title['status'], "id"=>$title['id']); 
        }
        return $titleList;     
     }
     
     
     function getList()
     {  
        $fxnServices = $this -> getAll();
        $fxnList     = array();
        foreach($fxnServices  as $fId => $val)
        {
           $fxnList[$fId] = $val['name'];
        }
        return $fxnList;
     }
     
     
     

}
?>