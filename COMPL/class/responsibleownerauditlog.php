<?php
class ResponsibleOwnerAuditLog extends SetupAuditLog
{

  function processChanges($obj, $postedArr, $id, Naming $naming)
  {
      $data = $obj -> fetch($id);
      $obj -> getTablename();
      $changes = array();

	 if(isset($postedArr['user_id']))
	 {
	  if($postedArr['user_id'] != $data['user_id'])
	  {
	     $respownerObj      = new ResponsibleOwner(); 
	     $responsibleOwner  = $respownerObj -> getAll();

          $userObj = new User();
          $userList = $userObj -> getAll();
	     $from = $to = "";
          if(isset($responsibleOwner[$data['user_id']]))
          {
              $from = $responsibleOwner[$data['user_id']]['name'];
          } else {
              if(isset($userList[$data['user_id']]))
              {
                $from = $userList[$data['user_id']]['user'];
              }
          }
          
          if(isset($responsibleOwner[$postedArr['user_id']]))
          {
              $to = $responsibleOwner[$postedArr['user_id']]['name'];
          } else {
              if(isset($userList[$postedArr['user_id']]))
              {
                $to = $userList[$postedArr['user_id']]['user'];
              }
          }
	     $changes['responsible_owner_ref_'.$id] = array("from" => $from, "to" => $to); 		  
	  }
	} else if(isset($postedArr['status'])) {
	     $changes['status_ref_'.$id] = $this->processStatusChanges($postedArr['status'], $data['status']);
	}
	$res = 0;	
	if(!empty($changes))
	{
	   $res = $this->saveChanges($changes, $id, "responsible_owner_users", $obj);
	   $obj->setTablename("responsible_owner_users");
	}
     return $res;
  
  
  }
  
 


}
?>