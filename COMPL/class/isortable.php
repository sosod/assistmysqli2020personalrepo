<?php 
/*
 * Allows implemation class for sorting of data , using the naming convension setup
 */
interface ISortable
{
	function sortHeaders( $data ,Naming $naming, $page = "");
}
