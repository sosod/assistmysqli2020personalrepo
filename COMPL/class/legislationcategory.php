<?php
/*
 Legislation categories from the master table 
*/
class LegislationCategory extends Model
{
     
     protected static $table = "category_legislation";
     
     function __construct()
     {
        parent::__construct();
     }
     
     
    function getAll($options = array())
    {
       $clientObj = new ClientLegislationCategory();
       $clientCategories = $clientObj -> fetchAll();
       $clientList = array();
       foreach($clientCategories as $index => $cat)
       {
          $clientList[$cat['id']] = array('name' => $cat['name'], 'imported' => false, 'id' => $cat['id'], 'status' => $cat['status']);
       }
       
       
       $masterObj = new MasterLegislationCategory();
       $masterCategories = $masterObj -> fetchAll();
       $masterList = array();
       foreach($masterCategories as $index => $cat)
       {
          $masterList[$cat['id']] = array('name' => $cat['name'], 'imported' => true, 'id' => $cat['id'], 'status' => $cat['status']);
       }       
       
       
       $list  = array_merge($clientList, $masterList); 
       $categories = array();
       foreach($list as $cIndex => $category)
       {
          $categories[$category['id']] = $category;
       }
       return $categories;
    }
	
    function getCategoryByLegislationId($id)
    {
	  $results = $this->db->get("SELECT * FROM #_category_legislation WHERE legislation_id = $id");
	  return $results;
    }
     
    function masterCategoryByLegislationId($legislationId)
    {
	  $results = $this->db2->get("SELECT * FROM #_category_legislation WHERE legislation_id = $legislationId");
	  return $results;     
    } 
	
}
?>
