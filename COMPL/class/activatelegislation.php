<?php
class ActivateLegislation extends LegislationManager
{

	function __construct()
	{
		parent::__construct();
	}
	
	/*
	function getLegislations($start, $limit, $options = "")
	{
		//get legislations awaiting activation
		$conditions   = " AND L.legislation_status & 512 = 512 AND L.legislation_status & 1024 <> 1024 ";
		$legiAwaiting = $this->legislationObj->fetchLegislations( $start, $limit, $conditions);
		$legiAwaiting = $this->sortLegislation( $legiAwaiting, new LegislationNaming("new"));
		//$legiAwaiting   = $this->getLegislations($start, $limit, $conditions );
		//get legislations that have been activated
		$condtionsA    = " AND L.legislation_status & 1024 = 1024 AND L.legislation_status & 512 = 512 ";
		$legiActivated = $this->legislationObj->fetchLegislations( $start, $limit, $condtionsA);
		$legiActivated = $this->sortLegislation($legiActivated, new LegislationNaming("new"));
		//$legiActivated   = $this->getLegislations($start, $limit , $condtionsA);
		
		$results = array("awaiting" => $legiAwaiting, "activated" => $legiActivated, "user" => $_SESSION['tkn'], "company" => ucfirst($_SESSION['cc']) );
		return $results;		
	}
	
	function activateLegislation( $data )
	{
		$legislationObj  = new Legislation();
		$legislationData = $this->legislationObj->fetch( $data['id'] );
		
		$legislationData['legislation_status']   = $legislationData['legislation_status'] + Legislation::ACTIVATED;
		$updatedata = array("legislation_status" => $legislationData['legislation_status'] );

		$this->legislationObj->setTablename("legislation");
		$res = $this->legislationObj->update($data['id'], $updatedata, new LegislationAuditLog(), $this->legislationObj, new LegislationNaming());
		$response = array();
		if( $res > 0) {
			$response = array("text" => "Legislation activated . . .", "error" => false, "updated" => true);
		} else if( $res == 0){
			$response = array("text" => "No changes made to the legislation . . .", "error" => false, "updated" => false);
		} else {
			$response = array("text" => "Error activating the legislation. . .", "error" => true, "updated" => false);
		}
		return $response;	
	}*/
	
	function sortLegislations($legislations)
	{
	     $headers = array("id", "name", "type", "reference");
	     $legislationList = array();
	     foreach($headers as $index => $val)
	     {
	       if(isset($this -> legislationHeaders[$val]))
	       {
	          $legislationList['headers'][$val] = $this -> legislationHeaders[$val];
	       }
	     }
	     foreach($legislations as $legislationId => $legislation)
	     {     
	       foreach($headers as $index => $val)
	       {
	         if(array_key_exists($val, $legislation))
	         {
	           if($val == "type")
	           {
	               $legtype = LegislativetypeManager::getLegislationType($legislation[$val]);
	               $legislationList['legislation'][$legislationId][$val] = $legtype;
	           } else {
	               $legislationList['legislation'][$legislationId][$val] = $legislation[$val];
	           }
	         }	       
	       }
	     }  
          return $legislationList;
	}
	
	
}
