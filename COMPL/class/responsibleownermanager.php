<?php
class ResponsibleOwnerManager
{
	
	private $responsibleOwner = null;
	
	function __construct()
	{
		//$this->responsibleOwner = new ResponsibleOwner();
	}
	
	//get list of the matches between the client and the master responible owners     
	function getAll()
	{
	   //get the responsible owner titles
	   $responsibleOwnerTitles = $this->getResponsibleOwnerTitles(); 
	   
	   //get the users list  
	   $userObj   = new User();
	   $userList  = $userObj -> getAll();
	   
	   //get the matches list
	   $matches  = $this->getMatches();
	   
	   $matchArr = array();

	   foreach($matches as $refTitle => $mVal)
	   {
	        $username = $title = "";	        
	        //get the name of the custom ref/ master ref
	        if(isset($responsibleOwnerTitles[$refTitle])) 
	        {
	          //if the title is linked to a custom title
	          $title = $responsibleOwnerTitles[$refTitle]['name'];
	        }
	   
	        //if the match is linked to the user , then get the user name
	        if(isset($userList[$mVal['clientID']]))
	        {
	          $username = $userList[$mVal['clientID']]['user'];
	        } else if(isset($responsibleOwnerTitles[$mVal['clientID']])) {
	          //if the title is linked to a custom title
	          $username = $responsibleOwnerTitles[$mVal['clientID']]['name'];
	        } else {
	          $username = $title." (Unknown)"; 
	        }
	        $matchArr[$mVal['matchID']] = array("title" => $title, "user" => $username, "id" => $mVal['matchID'], "status" => $mVal['status']);
	   }
	   return $matchArr;
	}     
	
	/*
	 Get the matches between the client and master responsble owners
	*/
	function getMatches()
	{
	   $matchObj = new ResponsibleOwnerMatch();
	   $matches  = $matchObj -> fetchAll();
	   $matchList = array();
	   foreach($matches as $mIndex => $mVal)
	   {
	     $matchList[$mVal['ref']] = array("clientID" => $mVal['user_id'], "status" => $mVal['status'], "matchID" => $mVal['id'] );
	   }
	   return $matchList;
	}
	
	/*
	 Responsible owner titles setup in compliance 1
	*/
	function getResponsibleOwnerTitles()
	{
	  $respOwnerObj = new ResponsibleOwner();
	  $responsibleOwners = $respOwnerObj -> getAll();
	  $matches           = $this->getMatches(); 
	  $responsibleOwnerList = array();
	  foreach($responsibleOwners as $rIndex => $rVal)
	  {
	     if(isset($matches[$rVal['id']]))
	     {
	       $responsibleOwnerList[$rVal['id']]= array("name" => $rVal['name'], "used" => true, "status" => $rVal['status']);
	     } else {
	       $responsibleOwnerList[$rVal['id']]= array("name" => $rVal['name'], "used" => false, "status" => $rVal['status']);
	     }
	  }
	  return $responsibleOwnerList;
	}

     function getDeliverableResponsiblePerson($responsibleOwner, $deliverableStatus, $users, $ownerTitles)
     {
        $ownerStr  = "";
        if(Deliverable::isUsingUserId($deliverableStatus))
        {
          if(isset($users[$responsibleOwner]))
          {
            $ownerStr  = $users[$responsibleOwner];
          } else {
            $ownerStr  = "Unspecified";
          }
        } else {
          if(isset($ownerTitles[$responsibleOwner]))
          {
            $ownerStr  = $ownerTitles[$responsibleOwner]['name']; 
          } elseif(isset($users[$responsibleOwner])) {
             $ownerStr  = $users[$responsibleOwner]; 
          } else {
            $ownerStr  = "Unspecified";  
          }
        }
       return $ownerStr; 
     }

     function getDelResponsiblePerson($responsibleOwner, $deliverableStatus, $users, $ownerTitles)
     {    
        $ownerStr  = "";
        if(Deliverable::isUsingUserId($deliverableStatus))
        {
          if(isset($users[$responsibleOwner]))
          {
            $ownerStr  = $users[$responsibleOwner];
          } else {
            $ownerStr  = "Unspecified";
          }
        } else {
          if(isset($ownerTitles[$responsibleOwner]))
          {
            $ownerStr  = $ownerTitles[$responsibleOwner]['name']; 
          } else {
            $ownerStr  = "Unspecified";  
          }
        }
       return $ownerStr; 
     }

	     
     
}
?>