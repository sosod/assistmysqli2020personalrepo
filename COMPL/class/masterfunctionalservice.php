<?php
class MasterFunctionalService extends Model
{
     protected static $tablename = "functional_service";

	
	function __construct()
	{
	  parent::__construct();	
	}

	function fetchAll( $options =  "")
	{
		$results = $this -> db2 -> get("SELECT id, name, description, status FROM #_functional_service WHERE status & 2 <> 2  ");
		return $results;
	}
	
	function fetch($id)
	{
		$result = $this -> db2 -> get("SELECT id, name, description, status FROM #_functional_service WHERE id = '".$id."' ");
		return $result;
	}
	
	function getList()
	{
	   $functionalServices = $this->fetchAll();
	   $list = array();
	   foreach($functionalServices as $fIndex => $fVal)
	   {
	     $list[$fVal['id']] = $fVal['name'];
	   }
	   return $list;
	}
}
?>