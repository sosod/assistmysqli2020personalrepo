<?php
/*
Legislation types from the master table
*/
class MasterLegislationType extends LegislationType
{
     
     function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll()
     {
        $results = $this->db2->get("SELECT id,name,description,status FROM #_legislative_types 
                                      WHERE status & 2 <> 2 AND status & 1 = 1 ORDER BY id, name");
	   return $results;
     }
     
     function fetch($id)
     {
       $result  = $this->db2->getRow("SELECT * FROM #_legislative_types WHERE id = $id");
	  return $result;	
     }

	function search($text)
	{
	   $results = $this->db2->get("SELECT id FROM #_legislative_types WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%'  ");
	   return $results;								  
	}  
     

}
?>
