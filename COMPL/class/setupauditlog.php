<?php
//uses the observer pattern , this observes the changes made on the setup
class SetupAuditLog implements Auditlog
{
	private $headers;
	
	//process changes made on the setup
	function processChanges($obj, $postArr, $id, Naming $naming)
	{	    
	    $obj_key   = "";
	    if(method_exists($obj, 'getObjKey'))
	    {
	        $obj_key = $obj -> getObjKey();
	    }
		$data 	   = $obj -> fetch($id);
		$tablename = $obj -> getTablename();
		$changes   = array();	

		foreach($postArr as $key => $value)
		{
            if( array_key_exists($key, $data))
		    {
               if( $value != $data[$key])
               {
                    if(!empty($obj_key))
                    {
                        $keyname = $obj_key;   
                    } else {
                        $keyname = $this -> _createHeader($tablename, $key);
                    }
                    if( $key == "user_id")
                    {
	                    $userObj = new UserAccess();
	                    $users   = $userObj -> getUsers();
	                    $from 	=  $to = "";
	                    foreach($users as $index => $user)
	                    {
		                    if($user['tkid'] == $value)
		                    {
			                    $to = $user['user'];
		                    }
		                    if($user['tkid'] == $data[$key])
		                    {
			                    $from = $user['user'];
		                    }
	                    }
	                    $changes['user_title']   = array("from" => $from , "to" => $to );							
                    } elseif($key == "master_id"){
	                    $from = $to = "";
                        $fxnService               = new FunctionalServiceManager();
                        $clientFunctionalServices = $fxnService -> getClientFunctionalServices();
                        if(!empty($clientFunctionalServices))
                        {
                            if(isset($clientFunctionalServices[$data['master_id']]))
                            {
                                $from = $clientFunctionalServices[$data['master_id']];
                            }
                            if(isset($clientFunctionalServices[$postArr['master_id']]))
                            {
                                $to = $clientFunctionalServices[$postArr['master_id']];
                            }
                        }
                        $changes[$keyname] = array("from" => $from, "to" => $to);	
                    } else if($key === "dept_id") {
                         $deptObj = new ClientDepartment();
                         $clientDepts = $deptObj -> getAll();

                         $from = $to = "";
                         $from = $clientDepts[$data['dept_id']]['name'];
                         $to   = $clientDepts[$value]['name'];
                         //$keyname = "deliverable_department_title_".$ 
                         if($from != $to)
                         {
                            $changes['client_department'] = array("from" => $from, "to" => $to);
                         } 				
                    } else if($key == "ref") {
	                    $setupObj = new SetupManager();
	                    $from = $to = "";	

	                    if($tablename == "action_owner_users")
	                    {
		                    $owners = $setupObj->getOwners();
		                    foreach($owners as $oIndex => $oVal)
		                    {
			                    if($oVal['id'] == $value)
			                    {
				                    $to = $oVal['name'];
			                    }
			                    if($oVal['id'] == $data[$key])
			                    {
				                    $from = $oVal['name'];
			                    }
		                    }
	                    }
	                    $changes[$tablename]   = array("from" => $from, "to" => $to);				
                    } else if($key == "status") {
	                    $fromStatus = $this -> getStatus( $data[$key] );
	                    $toStatus   = $this -> getStatus( $value );
	                    $changes[$keyname]   = array("from" => $fromStatus, "to" => $toStatus);
                    } else {
                       $_from             = ($data[$key] == "" ? " - " : $data[$key]);
	                   $changes[$keyname] = array("from" => $_from, "to" => $value);							
                    }
               }				
		    }
		}	
		$result = "";
		if(!empty($changes))
		{
			/*
			if(substr($tablename, -5) == "users")
			{
				$tablename = substr($tablename, 0, -6);
			}*/		
			if($tablename == "events")
			{
                $_changes["ref_".$id] = "Ref ".($id * 10000);  			
			} else {
			    $_changes["ref_".$id] = "Ref ".$id;  
		    }
			
			$_changes             = array_merge($_changes, $changes);
			$result               = $this -> saveChanges($_changes, $id, $tablename, $obj);
			$obj->setTablename($tablename);
		}	
		return $result;
	}

     function saveChanges($changes, $id, $tablename, $obj)
     {
         $obj->setTablename($tablename."_logs");
         $changes['user'] = $_SESSION['tkn'];
         $insertdata      = array("changes" => base64_encode(serialize($changes)), "setup_id" => $id, "insertuser" => $_SESSION['tid'] );
         $result          = $obj->save($insertdata);	
         return $result;
     }

	function _createHeader($tablename, $key)
	{
	   /*if(substr($tablename, -1, 1) == "s")
	   {
			$tablename = substr($tablename, 0, -1);
	   }*/
	   $keyname = $tablename."_".$key;
	   return $keyname;
	}
	
	protected function getStatus($status)
	{
		$statusAction = "";
		if((($status & 1) == 0) && ($status & 2) !== 2)
		{
			$statusAction = "Deactivaed";
		} else if(($status & 1) == 1){
			$statusAction = "Activated";
		} else if(($status & 2) == 2) {
			$statusAction = "Deleted";
		}
		return $statusAction;	
	}
	
	protected function processStatusChanges($posted, $previous)
	{
	  $statusChanges = array();
	  $fromStatus = $toStatus = "";
       $fromStatus = $this -> getStatus($previous);
       $toStatus   = $this -> getStatus($posted);
       $statusChanges   = array("from" => $fromStatus , "to" => $toStatus ); 
       return $statusChanges;
	}
	
}
