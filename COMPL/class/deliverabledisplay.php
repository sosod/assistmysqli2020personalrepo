<?php
/*
 Display the deliverable 
*/
class DeliverableDisplay extends HtmlResponse
{
     
     private $deliverableId;
     
     private $deliveable = array();
     
     private $deliverableObj;
	 
	 const REFTAG = "D";
     
     function __construct($deliverableid = "")
     {
          $this->deliverableId = $deliverableid;
          $this->deliverableObj = new ClientDeliverable();
     }
     
     function _getDeliverble()
     { 
         if(!empty($this -> deliverableId))
         {
            $this -> deliverable = $this -> deliverableObj -> fetch($this->deliverableId);
         }
     }
     
     function display(Naming $namingObj, $options = array())
     {
          $this->_getDeliverble();    
          $header                 = $namingObj -> getHeaderList();
          //get the compliance frequencies
          $complianceFrequencyObj = new ComplianceFrequency();
          $complianceFrequecies   = $complianceFrequencyObj -> getList();
     
          //get the organisation types 
          $organisationTypesObj = new OrganisationType();
          $organisationTypes    = $organisationTypesObj -> getList();

          //get the departments
          $deptObj       = new ClientDepartment();
          $departmentObj = new DepartmentManager();
          $departments 	 = $deptObj -> getList(); 
     
          //get the functional services
          $functionalServiceObj = new FunctionalServiceManager();
          $functionals    = $functionalServiceObj -> getClientFunctionalServices(); 
          //$fnObj = new FunctionalServiceManager();
          //$deliverableFunctionalServiceId = $fnObj -> getFunctionalServiceToUse();
          
          //get accoountable persons list and deliverable accountable persons          
          $accObj 			= new AccountablePersonManager();
          $accountablePersons = $accObj->getList();
          $deliverableAccountablePersons = $accObj->getDeliverableAccountablePersonIds($this->deliverableId);
          //get all the compliances to 
          $comptoObj      = new ComplianceToManager(); 
          $complianceTo   = $comptoObj->getList();
          $delCompTo      = $comptoObj->getDeliverableComplianceToIds($this->deliverableId);
     
          //get all the users who are active in the module
          $responsownerObj = new ResponsibleOwnerManager();
          $userObj   = new User();
          $users     = $userObj -> getList();    
          $ownerTitles = $responsownerObj -> getResponsibleOwnerTitles();
          
          //get the subevent deliverable relationships 
          $subeventobj = new SubeventManager();
          $subObj = new SubEvent(); 
          $subEvents   = $subObj->getList(array("eventid" => $this->deliverable['main_event']));
          $deliverableSubEvents = $subeventobj->getDeliverableSubEvents($this->deliverableId);      
          
          //get all the reporting categories
          $reportingCatObj = new ReportingCategories();
          $reportingCategories = $reportingCatObj->getList();

          //get all the main events
          $eventsObj  = new Event();
          $mainEvents = $eventsObj -> getList();
		$table = "<form method='post' id='edit-deliverable' name='edit-deliverable'>";
		$table .= "<table width='100%' id=deliverable_details_table class=form>";
		$table .= "<tr>";
			$table .= "<td colspan='2'><div id='message' style='padding:5px;'></div></td>";
		$table .= "</tr>";			
		//don't show if its a quick edit
	     $table .= "<tr>";
		     $table .= "<th>".$header['id'].":</th>";
		     $table .= "<td>".self::REFTAG."".$this -> deliverableId."</td>";
	     $table .= "</tr>";
		if($this->_isDisplayable($options, "short_description"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['short_description'].":</th>";
			     $table .= "<td>".$this->deliverable['short_description']."</td>";
		     $table .= "</tr>";
		 }
		 if($this->_isDisplayable($options, "description"))
		 {
		    $table .= "<tr>";
			     $table .= "<th>".$header['description'].":</th>";
			     $table .= "<td>".$this->deliverable['description']."</td>";
		     $table .= "</tr>";
		  }
		  if($this->_isDisplayable($options, "legislation_section"))
		  {
		     $table .= "<tr>";
			     $table .= "<th>".$header['legislation_section'].":</th>";
			     $table .= "<td>".$this->deliverable['legislation_section']."</td>";
		     $table .= "</tr>";
		   }
		   if($this->_isDisplayable($options, "compliance_frequency"))
		   {
		     $table .= "<tr>";
			     $table .= "<th>".$header['compliance_frequency'].":</th>";
			     $table .= "<td>".$complianceFrequecies[$this->deliverable['compliance_frequency']]."</td>";
		     $table .= "</tr>";
              }
              if($this->_isDisplayable($options, "applicable_org_type"))
              {
		     $table .= "<tr>";
			     $table .= "<th>".$header['applicable_org_type'].":</th>";
			     $table .= "<td>".$organisationTypes[$this->deliverable['applicable_org_type']]."</td>";
		     $table .= "</tr>";              
              }
              if($this->_isDisplayable($options, "compliance_date"))
              {
                    $rTypes = Deliverable::recurringTypes();
		          $table .= "<tr>";
		          $table .= "<th>".$header['compliance_date'].":</th>";
			          //$table .= "<td>".$form->renderText("compliance_date", $deliverable['compliance_date'])."</td>";
			          $table .= "<td>";
				          $table .= "<table>";
					          $table .= "<tr>";
					          $table .= "<td>Deadline date recurring within the financial year:</td>";
					          $table .= "<td>".($this->deliverable['recurring'] == 1 ? "Yes" : "No")."</td>";
					          $table .= "</tr>";
					          $table .= "<tr id='days' class='recurr nonfixed' style='display:".($this->deliverable['recurring'] == 1 ? 'table-row' : 'none').";'>";
					          $table .= "<td>".$this->deliverable['recurring_period']."</td>";
					          //$table .= "<td>".$form->renderSelect("recurring", array(1 => "Yes", 0 =>"No"), $deliverable['recurring'])."</td>";
					          $table .= "<td colspan='2'>".($this->deliverable['days_options'] == "weekly" ? "Working Days" : "Days")."</td>";
					          $table .= "</tr>";					
					          $table .= "<tr class='recurr' style='display:".($this->deliverable['recurring'] == 1 ? 'table-row' : 'none').";'>";
					          $table .= "<td>Days From:</td>";
					          $table .= "<td>".(isset($rTypes[$this->deliverable['recurring_type']]) ? $rTypes[$this->deliverable['recurring_type']] : "")."</td>";
					          //$table .= "<td></td>";
					          $table .= "</tr>";	
					          $table .= "<tr class='fixed' style='display:".($this->deliverable['recurring'] == 0 ? 'table-row' : 'none').";'>";
					          $table .= "<td>Fixed:</td>";
					          $table .= "<td>".$this->deliverable['compliance_date']."</td>";
					          $table .= "</tr>";															
				          $table .= "</table>";
			          $table .= "</td>";
		          $table .= "</tr>";
	     }
	     if($this->_isDisplayable($options, "responsible"))
	     {
		     $table .= "<tr>";
			     $table .= "<th>".$header['responsible']."</th>";
			     $table .= "<td>".(isset($departments[$this->deliverable['responsible']]) ? $departments[$this->deliverable['responsible']] : "") ."</td>";
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "functional_service"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['functional_service'].":</th>";
			     $table .= "<td>".(isset($functionals[$this->deliverable['functional_service']]) ? $functionals[$this->deliverable['functional_service']] : "")."</td>";
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "accountable_person"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['accountable_person'].":</th>";
			     $table .= "<td>".$this->_displayMultiple($accountablePersons, $deliverableAccountablePersons)."</td>";
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "responsibility_owner"))
		{
		     $userStr = $responsownerObj -> getDeliverableResponsiblePerson($this->deliverable['responsibility_owner'], $this->deliverable['status'], $users, $ownerTitles);
		     $table .= "<tr>";
			     $table .= "<th>".$header['responsibility_owner'].":</th>";
			     $table .= "<td>".$userStr."</td>";
		     $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "legislation_deadline"))
		{		
               $table .= "<tr>";
               $table .= "<th>Deliverable Legislative Deadline Date:</th>";
               $table .= "<td>".$this->deliverable['legislation_deadline']."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "sanction"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$header['sanction'].":</th>";
                    $table .= "<td>".$this->deliverable['sanction']."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "assurance"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$header['assurance'].":</th>";
                    $table .= "<td>".$this->deliverable['assurance']."</td>";
               $table .= "</tr>";
		}
		if($this->_isDisplayable($options, "compliance_to"))
		{
		     $table .= "<tr >";
			     $table .= "<th>".$header['compliance_to'].":</th>";
			     $table .= "<td>".$this->_displayMultiple($complianceTo, $delCompTo)."</td>";
		     $table .= "</tr>";																							
		}
          if($this->_isDisplayable($options, "main_event"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['main_event'].":</th>";
			     $table .= "<td>".$mainEvents[$this->deliverable['main_event']]."</td>";
		     $table .= "</tr>";																							
		}
		if($this->_isDisplayable($options, "sub_event"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['sub_event'].":</th>";
			     $table .= "<td>".$this->_displayMultiple($subEvents, $deliverableSubEvents)."</td>";
		     $table .= "</tr>";																							
		}
		if($this->_isDisplayable($options, "reporting_category"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['reporting_category'].":</th>";
			     $table .= "<td>".(isset($reportingCategories[$this->deliverable['reporting_category']]) ? $reportingCategories[$this->deliverable['reporting_category']] : "")."</td>";
		     $table .= "</tr>";																							
		}		
		if($this->_isDisplayable($options, "guidance"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['guidance'].":</th>";
			     $table .= "<td>".$this->deliverable['guidance']."</td>";
		     $table .= "</tr>";	
		}
		$table .= "</table>";	
		$table .= "</form>";
		$this -> showResponse($table); 
     }     
     
	private function _isDisplayable($options,  $field)
	{
	     $reassignFields = array("responsibility_owner");
	     $quickEditFields = array("responsible_department", "functional_service", "accountable_person", "responsibility_owner", "compliance_name_given_to", "deliverable_reporting_category");
	     if(!empty($options))
	     {
	          if(isset($options['reAssignDeliverable']) && $options['reAssignDeliverable'] == 1)
	          {
	               if(in_array($field, $reassignFields))
	               {
	                    return TRUE;
	               }    
	          }
	          if(isset($options['quickEdit']) && $options['quickEdit'] == 1)
	          {
	               if(in_array($field, $quickEditFields))
	               {
	                    return TRUE;
	               }    
	          }	     
	          return FALSE;  	     
	     } else {
	          return TRUE;
	     }
	}     
	
	private function _displayMultiple($options, $value)
	{
	     $valueStr = "";
	     if(!empty($options))
	     {
	          foreach($options as $index => $val)
	          {
	               if(isset($value[$index]))
	               {
	                   $valueStr = $val.", ";
	               }
	          }
	     }
	     return rtrim($valueStr, ", ");
	}
     
     
}
?>