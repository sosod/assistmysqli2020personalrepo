<?php
/*
 Accountable person from the master database
*/
class MasterAccountablePerson extends Model
{

     function __construct()
     {
        parent::__construct();
     }     
     
	function fetchAll($options="")
	{
		$results = $this->db2->get("SELECT id, name, status FROM #_accountable_person WHERE status & 2<> 2 $options ORDER BY name");
		return $results;
	}
	
	function fetch($id)
	{
	   $result = $this->db2->getRow("SELECT id, name, status FROM #_accountable_person WHERE id = '".$id."' ");
	   return $result;
	}
     
}

?>