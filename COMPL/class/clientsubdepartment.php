<?php
class ClientSubDepartment extends Model
{
	
	protected static $tablename = "dirsub";
	
	function __construct()
	{
		parent::__construct();		
	}
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT subid AS id,subtxt AS name,subyn,subdirid AS dirid, subsort, subhead 
		                             FROM assist_".$_SESSION['cc']."_list_dirsub 
							    WHERE subid = $id
							    ORDER  BY subsort
							   ");
		return $result;
	}
	
	function fetchAll()
	{
		$results = $this->db->get("SELECT subid AS id,subtxt AS name,subyn,subdirid AS dirid, subsort, subhead 
					 		  FROM assist_".$_SESSION['cc']."_list_dirsub 
					 		  ORDER BY subsort"
					 	      );
		return $results;
	}
	
	function getSubDirs( $dirid )
	{
		$results = $this->db->get("SELECT subid AS id,subtxt AS name,subyn,subdirid AS dirid, subsort, subhead
						       FROM assist_".$_SESSION['cc']."_list_dirsub 
						       WHERE subdirid = $dirid"
						     );
		return $results;
	}
	
}
?>