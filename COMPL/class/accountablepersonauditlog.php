<?php
class AccountablePersonAuditLog extends SetupAuditLog
{
 
  function processChanges($obj, $postedArr, $id, Naming $naming)
  {
     $data = $obj -> fetch($id);
     $tablename = $obj -> getTablename();
     $changes = array();

     $accountableObj      = new AccountablePerson(); 
     $accountablePerson  = $accountableObj -> getAll();
     
     $userObj = new User();
     $userList = $userObj -> getAll();
     
     $titleStr = "";
     if(isset($accountablePerson[$data['ref']]))
     {
         $titleStr = $accountablePerson[$data['ref']]['name'];
     }
     $title = str_replace(" ", "_", $titleStr);
     $key = "deliverable_accountable_person_title_".strtolower($titleStr);
	 if(isset($postedArr['user_id']))
	 {
	  if($postedArr['user_id'] != $data['user_id'])
	  {

          
	     $from = $to = "";
          if(isset($accountablePerson[$data['user_id']]))
          {
              $from = $accountablePerson[$data['user_id']]['name'];
          } else {
              if(isset($userList[$data['user_id']]))
              {
                $from = $userList[$data['user_id']]['user'];
              }
          }
          
          if(isset($accountablePerson[$postedArr['user_id']]))
          {
              $to = $accountablePerson[$postedArr['user_id']]['name'];
          } else {
              if(isset($userList[$postedArr['user_id']]))
              {
                $to = $userList[$postedArr['user_id']]['user'];
              }
          }
	     $changes[$key] = array("from" => $from, "to" => $to); 		  
	  }
	} else if(isset($postedArr['status'])) {
	     $changes[$key] = $this -> processStatusChanges($postedArr['status'], $data['status']);
	}
	$res = 0;	
	if(!empty($changes))
	{
	   $res = $this->saveChanges($changes, $id, "accountable_person_users", $obj);
	   $obj->setTablename("accountable_person_users");
	}
     return $res;
  }

}
?>