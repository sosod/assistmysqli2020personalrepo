<?php
class FunctionalServiceManager
{
     
     function getAll($options = array())
     {
        //functional services titles 
        $titles    = $this -> getFunctionalServicesTitles();
        
        //client functional services 
        $functionalServices = $this -> getClientFunctionalServices();  
          
        $matches  = $this -> getMatchList();  
        
        $matchList = array();
        foreach($matches as $mIndex => $mVal)
        {
          $title  = $fxnService = "";
          if(isset($titles[$mIndex]))
          {
            $title = $titles[$mIndex]['name'];
          } 
          
          if(isset($functionalServices[$mVal['clientId']]))
          {
            $fxnService = $functionalServices[$mVal['clientId']];
          }
          $matchList[$mVal['matchId']] = array("id" => $mVal['matchId'], "title" => $title, "masterTitle" => $fxnService, "status" => $mVal['status']);
        }
        return $matchList;
     }

     
     function getFunctionalServicesTitles()
     {
        $titlesObj = new FunctionalService();
        $titles    = $titlesObj  -> getAll();
        
        $matches  = $this ->  getMatchList();
        $titleList = array();
        foreach($titles as $tIndex => $title)
        {
          $titleList[$tIndex] = $title;
          if(isset($matches[$tIndex]))
          {
             $titleList[$tIndex]['used'] = true;
          } else {
             $titleList[$tIndex]['used'] = false;
          } 
        }
        return $titleList;
      }


     function getMatchList()
     {
         $matchObj = new FunctionalServiceMatch();
         $functionals = $matchObj -> fetchAll();
         $list = array();
         foreach($functionals as $index => $fxn)
         {
           $list[$fxn['ref']] = array("clientId" => $fxn['master_id'], "matchId" => $fxn['id'], "status" => $fxn['status']);
         }
        return $list;
     }

     function getClientFunctionalServices()
     {
        $fxnObj = new ClientFunctionalService();
        $functionals = $fxnObj -> fetchAll();
        $list = array();
        foreach($functionals as $fIndex => $fVal)
        {
          $list[$fVal['id']] = $fVal['name'];
        }
        return $list;
     }
     
     function deliverableFunctionalService($functionalServiceId, $deliverableStatus, $titles, $clientFunctionalServices)
     {
       $functionalStr = "";
       if(Deliverable::isUsingClientFunctionalService($deliverableStatus))
       {
          if(isset($clientFunctionalServices[$functionalServiceId]))
          {
            $functionalStr = $clientFunctionalServices[$functionalServiceId];
          } else {
             $functionalStr = "Unspecified";  
          }       
       } else {     
         if(isset($titles[$functionalServiceId]))
         {
           $functionalStr = $titles[$functionalServiceId]['name'];
         } else {
           $functionalStr = "Unspecified";
         }
       }
       return $functionalStr;
     }
     
     function getDelFunctionalService($functionalServiceId, $deliverableStatus, $titles, $clientFunctionalServices)
     {
       $functionalStr = "";
       if(Deliverable::isUsingClientFunctionalService($deliverableStatus))
       {
          if(isset($clientFunctionalServices[$functionalServiceId]))
          {
            $functionalStr = $clientFunctionalServices[$functionalServiceId];
          } else {
             $functionalStr = "Unspecified";  
          }       
       } else {

         if(isset($titles[$functionalServiceId]))
         {
           $functionalStr = $titles[$functionalServiceId]['name'];
         } else {
           $functionalStr = "Unspecified";
         }
       }
       return $functionalStr;
     } 
     
     

} 
?>