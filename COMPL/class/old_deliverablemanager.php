   	     $deliverableProgress = $this->calculateDeleverableProgress( $this->legislationId );
   	     $deliverableActions  = $this->getDeliverablesActions( $this->legislationId );   	
		$headers  = $naming->headersList();
		$setupObj = new SetupManager();

		
		$response = array(); 
		$data     = array();	
		$data['deliverable'] = array();	
		$data['headers']      = array();	
		$data['deliverableStatus']  = array();	
		$data['events'] = array();
		$data['hasActions'] = true;
		$data['isDeliverableOwner'] = array();
		$freqList 		 = $setupObj->getComplianceFrequencyManagerList();
		
		if( isset($deliverables) && !empty($deliverables))
		{	  	
		  foreach($deliverables as $index => $deliverable) {
		  

			if(isset($deliverableProgress[$deliverable['ref']]) && !empty($deliverableProgress[$deliverable['ref']]))
			{
				if($deliverableProgress[$deliverable['ref']]['totalActions'] == 0)
				{	
					$data['deliverableActionProgress'][$deliverable['ref']] = 0;					
				} else {			
					$data['deliverableActionProgress'][$deliverable['ref']] = round($deliverableProgress[$deliverable['ref']]['averageProgress'],2);
				}
			} else {
				//$data['hasActions'] = false;
				$data['deliverableActionProgress'][$deliverable['ref']] = 0;								
			}		  
		     
		     if(isset($deliverable['responsibility_owner']))
		     {
		          if($deliverable['responsibility_owner'] === $_SESSION['tid'])
		          {
		            $data['isDeliverableOwner'][$deliverable['ref']] = true;
		          } else {
		            $data['isDeliverableOwner'][$deliverable['ref']] = false;   
		          } 
		     }   
		     
		  
		     $data['deliverableStatus'][$deliverable['id']]  = $deliverable['status'];
		  	$subevents = $accountables = array();
			//$orgtype      = $setupObj->getOrganisationTypesList($deliverable['ref']); 	   	  
	  	    
			//$mainevent    = $setupObj->getEventsList($deliverable['ref']);		   
		 	$sbevents = "";
		 	//($deliverable['status'] & Deliverable::CREATED_SUBEVENT) == Deliverable::CREATED_SUBEVENT
			if(Deliverable::isCreatedSubevent($deliverable['status']))
			{
				$sbevents    = $setupObj->getSubEventOccuranceId($deliverable['event_occuranceid']);        
			} else {
			     $subeventObj = new SubeventManager();
				$sbevents  = $subeventObj->matchSubEventToDeliverable($deliverable['ref']);        		
			}
		     $data['events'][$deliverable['id']]  = $sbevents;				  
			foreach( $headers as $key => $header)
			{				
				if($key == "action_progress") {
					$data['headers'][$key] = $header;
					if(isset($deliverableProgress[$deliverable['ref']]) && !empty($deliverableProgress[$deliverable['ref']]))
					{
						$data['deliverable'][$deliverable['ref']][$key] = round($deliverableProgress[$deliverable['ref']]['averageProgress'],2); 
					} else {
						$data['deliverable'][$deliverable['ref']][$key] = "0";	
					}
				} else if(array_key_exists($key, $deliverable)) {
				  $data['headers'][$key] = $header;
				  if($key == "sub_event")
				  {
				  	$data['deliverable'][$deliverable['ref']][$key] = $sbevents;	
				  } else if($key == "main_event") {
				       $eventObj  = new Event()
					  $mainevent = $eventObj -> getAll();

				  	 $data['deliverable'][$deliverable['ref']][$key] = (isset($mainevent[$deliverable[$key]]) ? $mainevent[$deliverable[$key]]['name'] : "");
				  	 //$data['deliverable'][$deliverable['ref']][$key] = "";
				  } else if($key == "applicable_organisation_type") {
				       $orgtypeObj = new OrganisationTypes();
				  
					  $orgtype = $orgtypeObj->getAll(); 			  	
				   $data['deliverable'][$deliverable['ref']][$key] = (isset($orgtype[$deliverable[$key]]) ? $orgtype[$deliverable[$key]]['name'] : "");
				  } else if($key == "responsible_department") {
				  	$deptObj  = new Department();
				     $departmentList =   $deptObj->getClientDepartments();
				  	if(isset($departmentList[$deliverable[$key]]))
				  	{
				  	  $data['deliverable'][$deliverable['ref']][$key] = $departmentList[$deliverable[$key]];  
				  	} else {
				  	  $data['deliverable'][$deliverable['ref']][$key] = "";  
				  	}			  	
				  } else if($key == "functional_service") {
				  	 $funcObj = new FunctionalServiceManager();
		  			 $functionalList = $funcObj->getDeliverableFunctionalService($deliverable[$key] );
					 /*if(isset($functionalList[$deliverable[$key]]))
					 {
					 	 $data['deliverable'][$deliverable['ref']][$key] = $functionalList[$deliverable[$key]];  
					 } else {
					    $data['deliverable'][$deliverable['ref']][$key] = "";
					 }*/
					 $data['deliverable'][$deliverable['ref']][$key] = $functionalList;
				  } else if($key == "frequency_of_compliance") {
					if(isset($freqList[$deliverable[$key]]))
					{
					  $data['deliverable'][$deliverable['ref']][$key] = $freqList[$deliverable[$key]];
					} else {
					  $data['deliverable'][$deliverable['ref']][$key] = "";
					} 	
				  } else if($key == "compliance_name_given_to") {
				    $compStr = "";
				  	 $complObj 	   = new CompliancetoManager();
	   			 $complianceto = $complObj->getTitleComplianceTo($deliverable['ref']);
	   			 $compStr = implode(", ", $complianceto);
					 $data['deliverable'][$deliverable['ref']][$key] = $compStr;
				  } else if($key == "accountable_person"){
				  	 $accountableObj = new AccountablePersonManager();
	  				 $accountablesList = $accountableObj->getUserAccountableList($deliverable['ref']); 
				  
					 $data['deliverable'][$deliverable['ref']][$key] = implode(",", $accountablesList);
				  } else if($key == "responsibility_owner") {
				  	   $username = "";
				  	   //echo "Responsible person for this deliverable id ref #".$deliverable['ref']." is -- ".$deliverable[$key]."\r\n\n\n";
				  	   $users = $setupObj -> getResUsers();
				  	   if(isset($users[$deliverable[$key]]))
				  	   {
				  	 	$data['deliverable'][$deliverable['ref']][$key] = $users[$deliverable[$key]]['name'];  
				  	   } else {
				  	     $resownerObj  = new ResponsibleOwnerManager(); 
				  	   	$responsiblityowner = $resownerObj -> getDeliverableResUser($deliverable[$key] );
				  	   	$data['deliverable'][$deliverable['ref']][$key] = $responsiblityowner;	
				  	   }
						
					  /*foreach($responsiblityowner as $rIndex => $user)
					  {	
					  	 //if the user has been matched 
					  	 if( $user['ref'] == $deliverable[$key])
					  	 {
					  	 	//if in our database we have the id from compliance 1 , check where it matches and get the user matched to it
					  	 	$username = $user['user'];
					  	 } else if( $user['user_id'] == $deliverable[$key]){
					  	 	//if the user in our db is matched and in user in our system match and get user matched
					  	 	$username = $user['user'];
					  	 }
					  }*/
					 
				  } else if($key == "compliance_date") {
				  		if($deliverable['recurring'])
				  		{
				  			$type = Deliverable::recurringTypes($deliverable['recurring_type']);
				  			$date = $deliverable['recurring_period']." ".(isset($deliverables['days_options']) ? $deliverables['days_options'] : "")." ".$type;
				  			$data['deliverable'][$deliverable['ref']][$key] = $date;
				  		} else {
				  			$data['deliverable'][$deliverable['ref']][$key] = $deliverable[$key];
				  		}
				  } else {
				     //if($deliverabnce)
			          $data['deliverable'][$deliverable['ref']][$key] = $deliverable[$key];//utf8_encode($deliverable[$key]);
				  }
				 }			
			 }
		  }   	
		}
		
		if(empty($data['deliverable']))
		{
			$data['headers'] = $headers;
		}
		$data['columns'] = (isset($data['headers']) ? count($data['headers']) : "");
		return $data;
