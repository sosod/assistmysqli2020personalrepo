<?php
class DeliverableReport extends DeliverableManager
{

   function __construct()
   {
     parent::__construct();
   }
   
   function sortDeliverables($deliverables, $fields = array())
   {
     $headerObj = new DeliverableNaming();
     $deliverablesHeaders = $headerObj -> getHeaderList();
     $headers = array();
     foreach($deliverablesHeaders as $index => $header)
     {
       if(isset($fields[$index]))
       {  
          $headers[$index] = $header;
       }
     }
     
      $subeventManagerObj = new SubEventManager();
      $eventOccuranceObj = new EventOccuranceManager();
      $subeventObj        = new SubEvent();
      $subeventList       = $subeventObj -> getAll();
      
      $eventObj  = new Event(); 
      $mainevent = $eventObj -> getAll();

      $orgtypeObj = new OrganisationType();
      $orgtype    = $orgtypeObj->getAll(); 	

      $deptObj  = new DepartmentManager();
      $masterDepts = $deptObj -> getMasterDepartments();
      $clientObj   = new ClientDepartment();
      $clientDepart = $clientObj -> getAll();

      $funcObj = new FunctionalServiceManager();
      $titles = $funcObj -> getFunctionalServicesTitles();
      $fxnServices = $funcObj -> getClientFunctionalServices();
      
      $complianceFreqObj = new ComplianceFrequency();  
      $complianceFreq = $complianceFreqObj -> getAll(); 
      
      $complObj 	= new CompliancetoManager();      
      $compliancesto = $complObj -> getAll();
      $mastertitles  = $complObj -> getComplianceToTitles();
	 $matches       = $complObj -> getMatchList();	   
      
      $accountableObj = new AccountablePersonManager();
      $accountables   = $accountableObj -> getAll();
      $titles         = $accountableObj -> getAccPersonsTitles();      
      $matchObj      = new AccountablePersonMatch();
      $accountableMatches = $matchObj -> getList();
      
      $resownerObj  = new ResponsibleOwnerManager(); 
      $userObj   = new User();
      $users     = $userObj -> getList();    
      $ownerTitles = $resownerObj -> getResponsibleOwnerTitles();        
      
      $deliverableStatusObj = new DeliverableStatus();
      $statuses             = $deliverableStatusObj -> getDeliverableStatuses();
      
      $actionManager = new ManageAction();      

      $reportingCategoryObj = new ReportingCategories();  
      $reportingCartegories = $reportingCategoryObj -> getList();
      
      foreach($deliverables as $deliverableId => $deliverable)
      {
          $progress = 0;
		$subeventStr = "";
		if($this -> deliverableObj -> isCreatedSubevent($deliverable['status']))
		{
		   $eventStr          = $eventOccuranceObj -> getDelSubEvents($deliverable['event_occuranceid'], $subeventList);
	        $subeventStr    = (!empty($eventStr) ? $eventStr : "");        
		} else {
		   $subeventStr = $subeventManagerObj -> getDelSubEvents($deliverableId, $subeventList, new DeliverableSubEvent());
		}
          if(isset($this -> deliverablesProgress[$deliverableId]))
          {
            $progress = round(intval($this -> deliverablesProgress[$deliverableId]), 2);
          }  	          
        foreach($headers as $key => $head)
        {	
         if(array_key_exists($key, $deliverable)) 
         {               
              $data['headers'][$key]  = $head;
              if($key == "action_progress")
              {
                $data['deliverable'][$deliverableId][$key] = $progress;  
              } else if($key == "sub_event") {
                $data['deliverable'][$deliverableId][$key] = $subeventStr;	
              } else if($key == "main_event") {
                $eventStr = (isset($mainevent[$deliverable[$key]]) ? $mainevent[$deliverable[$key]]['name'] : "");
                $data['deliverable'][$deliverableId][$key] = $eventStr;
              } else if($key == "applicable_org_type") {
                $orgtypeStr = (isset($orgtype[$deliverable[$key]]) ? $orgtype[$deliverable[$key]]['name'] : "");	  	
                $data['deliverable'][$deliverableId][$key] = $orgtypeStr;                     
              } else if($key == "responsible") {
                $deptStr =   $deptObj -> getDeliverableDepartment($deliverable[$key], $deliverable['status'], $clientDepart, $masterDepts);            
                $data['deliverable'][$deliverableId][$key] = $deptStr; 
              } else if($key == "functional_service") {
  			 $functionalStr     = $funcObj -> deliverableFunctionalService($deliverable[$key], $deliverable['status'], $titles, $fxnServices);
  			 $data['deliverable'][$deliverableId][$key] = $functionalStr;
              } else if($key == "compliance_frequency") {
                $freqStr = (isset($complianceFreq[$deliverable[$key]]) ? $complianceFreq[$deliverable[$key]]['name'] : "");
                $data['deliverable'][$deliverableId][$key] = $freqStr;
              } else if($key == "compliance_to") {
                $compliancetoStr  = $complObj -> getDeliverableComplianceTo($deliverableId, $compliancesto, $mastertitles, $matches);
                $data['deliverable'][$deliverableId][$key] = (!empty($compliancetoStr) ? $compliancetoStr : "");
              } else if($key == "accountable_person") {
                $accountablesStr = $accountableObj -> getDeliverableAccountablePerson($deliverableId, $accountables, $titles, $accountableMatches); 
                $data['deliverable'][$deliverableId][$key] = (!empty($accountablesStr) ? $accountablesStr : "");      
              } else if($key == "responsibility_owner") {
                $ownerStr      = $resownerObj -> getDeliverableResponsiblePerson($deliverable[$key], $deliverable['status'], $users, $ownerTitles);
                $data['deliverable'][$deliverableId][$key] = $ownerStr;
              } else if($key == "compliance_date") {
               if($deliverable['recurring'])
               {
                 $type = Deliverable::recurringTypes($deliverable['recurring_type']);
                 $date = $deliverable['recurring_period']." ".(isset($deliverables['days_options']) ? $deliverables['days_options'] : "")." ".$type;
                 $data['deliverable'][$deliverableId][$key] = $date;
               } else {
                 $data['deliverable'][$deliverableId][$key] = $deliverable[$key];
               }    
              } else if($key == "reporting_category") {
                $repCatStr = (isset($reportingCartegories[$deliverable[$key]]) ?  $reportingCartegories[$deliverable[$key]] : "Unspecified");
                $data['deliverable'][$deliverableId][$key] = $repCatStr;
              } else {
                $data['deliverable'][$deliverableId][$key] = $deliverable[$key];
              }
          }            
        }
     }
     $data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);  
     return $data;
   }

}    
?>