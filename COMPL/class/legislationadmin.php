<?php
class LegislationAdmin extends Model
{
 
     protected static $table = "legislation_admins";
     
     function __construct()
     {
         parent::__construct();
     }

     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT * FROM #_legislation_admins WHERE 1 $options");
        return $results;
     }
     
     function fetch($id)
     {
       $result = $this -> db -> getRow("SELECT * FROM #_legislation_admins WHERE id = '".$id."' ");
     }
     
     function isLegislationAdmin($userid)
     {
        $admins = $this -> db -> get("SELECT * FROM #_legislation_admins WHERE user_id = '".$userid."' ");
        if(!empty($admins))
        {
          return TRUE;
        }
        return FALSE;
     }
     
     function getLegislationAdmins($legislationId)
     {
        $results = $this ->fetchAll(" AND legislation_id = '".$legislationId."' ");
        $users   = array();
        if(!empty($results))
        {
          foreach($results as $index => $user)
          {
             $users[] = $user['user_id'];
          }
        }
        return $users;          
     }
     
     function getUserLegislations($userid)
     {
        $results = $this -> fetchAll(" AND user_id = '".$userid."' ");
        $legislations = array();
        if(!empty($results))
        {
          foreach($results as $index => $legislation)
          {
             $legislations[] = $legislation['legislation_id'];
          }
        }
        return $legislations;
     }

     function updateAdmins($admins, $legislationId)
     {
        $res = $this -> db -> delete("legislation_admins", "legislation_id={$legislationId}");
        if(is_array($admins))
        {
          foreach($admins as $index => $admin)
          {
             $res += $this -> db -> insert("legislation_admins", array("user_id" => $admin, "legislation_id" => $legislationId));
          }
        }
        return $res;
     }
     
     function getLegislations($optionSql = "")
     {
        $results = $this -> db -> get("SELECT LA.legislation_id, LA.user_id, L.financial_year FROM #_legislation_admins LA 
                                       INNER JOIN #_legislation L ON LA.legislation_id = L.id WHERE 1 $optionSql ");
        $legislations = array();
        if(!empty($results))
        {
          foreach($results as $index => $legislation)
          {
             $legislations[] = $legislation['legislation_id'];
          }
        }                            
        return $legislations;
     }

}
?>
