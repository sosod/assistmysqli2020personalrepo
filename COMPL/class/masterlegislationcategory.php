<?php
/*
 Legislation categories from the master compliance 
*/
class MasterLegislationCategory extends LegislationCategory
{

     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options = "")
     {
	  $results = $this->db2->get("SELECT * FROM #_legislative_categories WHERE status & 2 <> 2 AND status & 1 = 1 $options ORDER BY id, name");
	  $categories = array();
	  foreach($results as $index => $category)
	  {
	     $categories[$category['id']] = $category;
	     $categories[$category['id']]['imported'] = true;
	  }
	  return $categories;
     }
     
     function fetch($id)
     {
        $results = $this->db2->get("SELECT * FROM #_category_legislation WHERE legislation_id = $id ");
	   return $results;          
     }
     
	function searchCategories2($text = "")
	{
        $results = $this->db2->get("SELECT id FROM #_legislative_categories WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%'  ");
	   return $results;
	} 
	
    
}
?>
