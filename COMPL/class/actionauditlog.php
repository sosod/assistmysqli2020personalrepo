<?php
class ActionAuditLog implements Auditlog
{

     private $currentStatus = ""; 	
	
	function processChanges($obj, $postArr, $id, Naming $naming)
	{		
		$action 		= $obj -> fetch( $id );
		$setupObj       = new SetupManager();
		$tablename      = $obj -> getTablename();	

		$changeMessage  = "";
		$changeArr	    = array();
		$changeMessage .=  $_SESSION['tkn']." has made the following changes <br />";
		$result         = array();
        
        Attachment::processAttachmentChange($action['attachment'], 'action_'.$id);
        if(isset($_SESSION['uploads']))
        {
           if(isset($_SESSION['uploads']['actionchanges']))
           {
              $changeArr['attachments'] = $_SESSION['uploads']['actionchanges'];
              $result['attachment']     = $_SESSION['uploads']['attachments'];
           }
        }
        unset($postArr['attachment']);
        unset($action['attachment']);
		foreach($postArr as $key => $val)
		{
			if(isset($action[$key]) || array_key_exists($key,$action) )
			{
				if($action[$key] !== $val)
				{
					if($key == "owner")
					{
                       $ownerChanges = $this -> _getOwnerChanges($action[$key], $val, $action['actionstatus']);
                       if(!empty($ownerChanges))
                       {
                          $obj -> updateActionOwnerStatus($action);
                          $changeArr['owner'] = $ownerChanges;
                       }						
					}  elseif($key == "status") {
					   $statusChanges = $this -> _getStatusChanges($action[$key], $val);
                       if(!empty($statusChanges))
                       {
                          $changeArr['status'] = $statusChanges;
                       }
					} elseif($key == "actionstatus") {
					   $actionStatusChange = $this -> _getStatusChange($action['actionstatus'], $val);
					   if(!empty($actionStatusChange))
					   {
					      $changeArr['action_status'] = $actionStatusChange;
					   }
					} elseif($key == "date_completed") {
					    $from            = (strtotime($action[$key]) == 0 ? " - " : $action[$key]);
					    $changeArr[$key] = array("from" => $from , "to" => $val);
					} elseif($key == "reminder") {
					  $from            = (strtotime($action[$key]) == 0 ? " - " : $action[$key]);
					  $to              = (strtotime($val) == 0 ? " - " : $val);
					  if($from != $to)
					  {
					     $changeArr[$key] = array("from" => $action[$key] , "to" => $val);
					  }
					} else {
					   $changeArr[$key] = array("from" => $action[$key] , "to" => $val);
					}					
				}
			}
		}
		$updateAction = false;
		if(isset($_POST['data']['response']))
		{
		   $changeArr['response'] = (isset($_POST['data']['response']) ? $_POST['data']['response'] : "");
		   $updateAction          = true;
		}

		if(!empty($changeArr))
		{
			if(!isset($changeArr['currentstatus']))
			{	
		        $changeArr['currentstatus'] = (!empty($this->currentStatus) ?  $this->currentStatus : "New");
			}
			
			//if the status did not change, then get the current action status
			if(empty($this -> currentStatus))
			{
                 $statusObj 	= new ActionStatus();
                 $statuses    = $statusObj -> getActionStatuses();
			  if($statuses[$action['status']])
			  {
			     $changeArr['currentstatus'] = $statuses[$action['status']]['name'];
			  } else {
			     $changeArr['currentstatus'] = "New";
			  }    			     
			}		
			$changeArr['user'] = $_SESSION['tkn'];
			$insertdata        = array();
			
			if($updateAction)
			{
				$obj -> setTablename($tablename."_update");
				$insertdata['response']   = (isset($_POST['data']['response']) ? $_POST['data']['response'] : "");		
				$insertdata['changes']    = base64_encode(serialize($changeArr));
				$insertdata['action_id']  = $id;
				$insertdata['insertuser'] = $_SESSION['tid'];										
			} else {
				$obj -> setTablename($tablename."_edit");
				$insertdata['changes']   = base64_encode(serialize($changeArr));
				$insertdata['action_id'] = $id;  
				$insertdata['insertuser'] = $_SESSION['tid'];
			}		
			$res = $obj -> save( $insertdata );	
			$obj->setTablename( $tablename );
		}
		//if(isset($_SESSION['uploads']['attachments']) && !empty($_SESSION['uploads']['attachments']))
		//{
		     //$result['attachment'] = $_SESSION['uploads']['attachments'];
		     //unset($_SESSION['uploads']);
		//}
		return $result;
	}

	public static function processAddLog($obj ,$actionid, $postData)
	{
	     $namingObj = new ActionNaming();
	     $headers   = $namingObj -> getHeaderList();
	     $actionAdd['message']            = 'Added action with ref #'.$actionid; 
	     $actionAdd['user']               = $_SESSION['tkn'];
	     $actionAdd['action']             = $headers['action']." ".$postData['action'];
	     $actionAdd['owner']              = $headers['owner']." ".$postData['owner'];
	     $actionAdd['action_deliverable'] = $headers['action_deliverable']." ".$postData['action_deliverable'];
	     $actionAdd['deadline']           = $headers['deadline']." ".$postData['deadline'];
	     $actionAdd['reminder']           = $headers['reminder']." ".$postData['reminder'];
	     $actionAdd['currentstatus']      = "New";
	     $obj -> setTablename("action_edit");
	     $insertdata = array('changes'    => base64_encode(serialize($actionAdd)), 
	                         'action_id'  => $actionid, 
	                         'insertuser' => $_SESSION['tid']
	                        );
	     $obj -> save($insertdata);
	}
	
	function _getOwnerChanges($fromActionOwner, $toActionOwner, $actionStatus)
	{
	  $from = $to = "";
	  $actionownerObj = new ActionOwner();
	  $actionowners   = $actionownerObj -> getAll();  
	  
	  $userObj = new User();
	  $users   = $userObj -> getAll();
	  $changes = array();
	  if(Action::isUsingUserId($actionStatus))
	  {
	      if(isset($users[$fromActionOwner]))
	      {
	        $from = $users[$fromActionOwner]['user'];
	      } else {
	        $from = "Unspecified";
	      }
	  } else {
	     if(isset($actionowners[$fromActionOwner]))
	     {
	       $from = $actionowners[$fromActionOwner]['name'];
	     } else {
	        $from  = "Unspecified";
	     }
	  }
	  if(isset($users[$toActionOwner]))
	  {
	    $to = $users[$toActionOwner]['user'];
	  } else {
	     $to  = "Unspecified";
	  }	     
       if($from != $to)
       {
          $changes = array("from" => $from, "to" => $to);
       }
       return $changes;
	}
	
	function _getStatusChanges($fromStatus, $toStatus)
	{
	  $statusObj 	= new ActionStatus();
	  $statuses    = $statusObj -> getActionStatuses();

	  $from = $to = "";
	  $change = array();
	  foreach($statuses as $sIndex => $sVal)
	  {
	     if($sVal['id'] == $fromStatus)
	     {
	        $from = $sVal['name'];
	     }
	     if($sVal['id'] == $toStatus)
	     {
	        $to = $sVal['name'];
	     }
	  }
	  if($from != $to)
	  {
	     $this -> currentStatus = $to;
	     $change = array("from" => $from, "to" => $to);
	  }
	  return $change;
	}
	
    function _getStatusChange($fromStatus, $toStatus)
    {
       $statusTo = abs($fromStatus - $toStatus);
         //if the status to is less than the status from , then the a status has been removed from the deliverable     
       $changes = array();
       //echo "From  -- ".$fromStatus." and to status -- ".$toStatus." --  and status to ".$statusTo."\r\n";
       if($toStatus == 2)
       {
         $changes['status']  = "Action has been deleted";
       } elseif(($toStatus & Action::APPROVED) == Action::APPROVED) {
         $changes['status']  = "Action has been approved";
       } elseif( (($toStatus & Action::DECLINED) == Action::DECLINED) ||
                 (($fromStatus  & Action::AWAITING_APPROVAL) == Action::AWAITING_APPROVAL)
        ) {
         $changes['status']  = "Action has been declined";
		} elseif( ( ($toStatus & Action::APPROVED)!= Action::APPROVED) 
					&& ( ($fromStatus & Action::APPROVED)==Action::APPROVED)
			) {
         $changes['status']  = "Action approval has been reversed";
       } elseif(($toStatus  & Action::AWAITING_APPROVAL) == Action::AWAITING_APPROVAL){
           $changes['status']  = "Action is awaiting approval";
       } elseif(($toStatus & Action::CONFIRMED) == Action::CONFIRMED){
          
         $changes['status']  = "Action has been confirmed";
       } elseif(($toStatus & Action::ACTIVATED) == Action::ACTIVATED){
          $changes['status']  = "Action has been activated";
       } 
       /*
       if($toStatus == 2)
       {
         $changes['status'] = Action::statusMessage($toStatus, TRUE);
       } else if($statusTo < $fromStatus) {
          $changes['status'] = Action::statusMessage($statusTo, FALSE);
       } else { 
         if($statusTo != 0)
         {
          $changes['status'] = Action::statusMessage($statusTo, TRUE);
         }	
       }
       */	
       return $changes['status'];     
   }	
}
