<?php
class OrganisationType
{
     protected static $tablename = "orgtype_deliverable";

	function getAll($options = array())
	{
	   $optionSql = "";
	   if(isset($options['status']) && !empty($options['status']))
	   {
	      $optionSql = " AND status & ".$options['status']." = ".$options['status']." ";
	   }
	  //client list of organisation types 
	  $clientObj = new ClientDeliverableOrganisationType();
	  $clientOrgTypes = $clientObj -> fetchAll($optionSql);
	  //master list of organisation types
	  $masterObj = new MasterDeliverableOrganisationType();
	  $masterOrgTypes = $masterObj -> fetchAll($optionSql);	  
	  //$orgtypes = array_merge($clientOrgTypes , $masterOrgTypes);
	  
	  $orgtypeList = array();
	  $masterList  = array();
	  $clientList  = array();
	  foreach($masterOrgTypes as $oIndex => $mVal)
	  {
	     $masterList[$mVal['id']] = array("name"         => $mVal['name'],
	                                      "description" => $mVal['description'],
	                                      "status"      => $mVal['status'], 
	                                      "id"          => $mVal['id'],
	                                      "imported"    => true
	                                     );
	  }
	  
	  foreach($clientOrgTypes as $cIndex => $cVal)
	  {
	     $clientList[$cVal['id']] =  array("name"        => $cVal['name'],
	                                       "description" => $cVal['description'],
	                                       "status"      => $cVal['status'], 
	                                       "id"          => $cVal['id'],
	                                       "imported"    => false
	                                      );
	  }
	  $orgtypeList = array_merge($masterList, $clientList);
	  $orgtypes = array();
	  foreach($orgtypeList as $index => $orgtype)
	  {
	     $orgtypes[$orgtype['id']] = $orgtype;
	  }
 	  return $orgtypes;
	}

     function getOrganisationType($id)
     {
         $orgtypeList = $this->getAll();
         $orgtype = array();
         if(isset($orgtypeList[$id]))
         {
           $orgtype[$id] = $orgtypeList[$id];
         }
         return $orgtype;
     }

     function getList()
     {
        $orgtypes = $this->getAll();
        $list     = array();
        foreach($orgtypes as $oIndex => $oVal)
        {
          $list[$oVal['id']] = $oVal['name'];
        }
        return $list;
     }

	function updateOrganisationalTypes($newCats, $id)
	{
	   $clientObj = new ClientDeliverableOrganisationType();
	   $del       = $clientObj -> delete("orgtype_deliverable", "deliverable_id = '".$id."'");
	   foreach($newCats as $index => $catId)
	   {
		  $clientObj -> insert("orgtype_deliverable", array("orgtype_id" => $catId, "deliverable_id" => $id));
	   }
	}	
	
	
}
?>