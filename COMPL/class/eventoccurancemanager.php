<?php
class EventOccuranceManager
{
	
	private $eventoccuranceObj = null;
	
	function __construct()
	{
	   $this->eventoccuranceObj = new EventOccurance();
	}
     /*
       Get deliverables where the currelty logged in user is the legislation administor
     */
     function getDeliverablesWhereAdmin($deliverableObj, $legSql)
     {
        $adminObj         = new LegislationAdmin();  
        $legislationIds   = array(); 
        $_sql             = " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
        $legSql          .= " AND LA.user_id = '".$_SESSION['tid']."' ".$_sql; 
        $legislationIds   = $adminObj -> getLegislations($legSql);
        //get the administrators of client created legislations
        $legislation_sql  = " AND L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." 
                             AND L.admin = ".$_SESSION['tid']." ".$_sql;
        $legislationObj              = new ClientLegislation();
        $client_created_legislations = $legislationObj -> fetchAll($legislation_sql);
        if(!empty($client_created_legislations))
        {
           foreach($client_created_legislations as $index => $leg)
           {
             $legislationIds[$leg['id']] = $leg['id'];
           }
        }     
        $whereAdmin    = array();
        if(!empty($legislationIds))
        {
           $whereAdmin  = $deliverableObj -> getDeliverableEvent($legislationIds);
        }
        return $whereAdmin;
     }
     
     /*
      Get deliverable where the currenlty logged user is the legislation authorizor
     */
     function getDeliverableWhereAuthorizer($deliverableObj, $legSql)
     {
        $authorizerObj   = new LegislationAuthorizer();  
        $legislationIds  = array();
        $_sql            = " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
        $legSql         .= " AND LA.user_id = '".$_SESSION['tid']."' ".$_sql;   
        $legislationIds  = $authorizerObj -> getLegislations($legSql);

        //get the authorizors of client created legislations
        $legislation_sql = " AND L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." 
                             AND L.authorizer = ".$_SESSION['tid']." 
                             ".$_sql;  
        $legislationObj              = new ClientLegislation();
        $client_created_legislations = $legislationObj -> fetchAll($legislation_sql);
        if(!empty($client_created_legislations))
        {
           foreach($client_created_legislations as $index => $leg)
           {
             $legislationIds[$leg['id']] = $leg['id'];
           }
        }       
        
        $whereAuthorizer  = array();
        if(!empty($legislationIds))
        {
          $whereAuthorizer  = $deliverableObj -> getDeliverableEvent($legislationIds);    
        }
        return $whereAuthorizer;
     }

	function listSubEvents($options = array())
	{
		$whereAdmin      = array();
		$whereAuthorizor = array(); 
		$subeventObj     = new SubEvent();
		//get the legislations where user logged in is owner/authorizor and where admin
		//$managelegObj = new AdminLegislation();
		$legSql          = "";
		$delEvents       = array();
		if(isset($options['financialyear']) && !empty($options['financialyear']))
		{
		   $legSql              = " AND L.financial_year = '".$options['financialyear']."' ";
		   $deliverableObj      = new Deliverable();
		   $delWhereAuthorizor  = array();
		   $delWhereAdmin       = array();
		   //get the deliverables
		   //get deliverable where the user is the authorizor 
		   $delWhereAuthorizor  = $this -> getDeliverableWhereAuthorizer($deliverableObj, $legSql);
		   if($options['viewtype'] == "viewall")
		   {
		      //get the deliverable where the user is the admin
		      $delWhereAdmin  = $this -> getDeliverablesWhereAdmin($deliverableObj, $legSql);			
		   }
		   //merge the sub-events for which the user is a admin or authorizor of the legislation
		   $delEvents = array_merge($delWhereAuthorizor, $delWhereAdmin);
		}
		//get the sub events from the master and client database
		$events    = $subeventObj -> getAll(); 
 		$eventsArr = array();
 		foreach($events as $eIndex => $eVal)
 		{
 		     //if the subevent is not the Unspecified event
 			if( $eVal['id'] != 1)
 			{
 			     //if the subevent id is in subevents-id of the deliverables where user is admin/authorizor of the legislation
	 			if(in_array($eVal['id'], $delEvents))
	 			{
	 		   	   $eventsArr[$eIndex] = $eVal;
		 		   if(isset($eventOccurance[$eVal['id']]))
		 		   {	   
		 		      $eventsArr[$eIndex]['statusText']   = "Occured";
		 		      $eventsArr[$eIndex]['status']       = 9;
		 		      $eventsArr[$eIndex]['date_occured']  = $eventOccurance[$eVal['id']]['date_occured'];
		 		   } else if(($eVal['status'] & 1) == 1){
		 		     $eventsArr[$eIndex]['statusText'] = "Active";
		 		   } else {
		 		      $eventsArr[$eIndex]['statusText'] = "InActive";
		 		   }
	 		   } 			
 			}
 		}	
 		return $eventsArr;		 
	}

	//get the events that have been triggered before, for this user logged in 
	function getSubEventOccurance($options ="")
	{
		$sbeventObj = new SubEvent();
		$eventOccurances =  array();
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
		   $optionSql = " AND E.status & ".EventOccurance::EVENT_ACTIVATED." <> ".EventOccurance::EVENT_ACTIVATED." ";
		}
		//get the information for the events that have been triggerd  where the current user is the legislation admin 
		$eventOccurances = $this -> eventoccuranceObj -> getEventOccuranceWhereAdmin($optionSql);
          $subevents  = array();
		$subevents  = $sbeventObj -> getAll();
		$triggeredEvent = array();
		$events   = array();
		 
		 foreach($eventOccurances as $index => $eventOccurance)
		 {
		     foreach($subevents as $eventIndex => $eventVal)
		     {
		         if($eventVal['id'] == $eventOccurance['event_id'])
		         {
		             $events[$eventOccurance['id']] = array("name"  => $eventVal['name']." (".$eventOccurance['date_occured'].")",
		                                                    "date_occured" => $eventOccurance['date_occured'],
		                                                    "comment"      => $eventOccurance['comment'],
		                                                    "insertdate"   => $eventOccurance['insertdate'],
		                                                    "event_id"     => $eventOccurance['event_id'],
		                                                    "id"           => $eventOccurance['id'],
		                                                    "status"       => $eventOccurance['status']
       		                                               );   
		         }
		     }
		 }
		usort($events, function($a, $b){
			return ($a['insertdate'] < $b['insertdate']) ? 1 : -1;
		});
		return $events;
		
	}
	
	function getEventOccurance($id)
	{
	     $result = $this->eventoccuranceObj->fetch($id);
	     return $result;
	}
	
	function getDeliverableSubEvent($id)
	{
	   $subeventsObj = new SubEvent();
	   $subEvents = $subeventsObj -> getAll();
	   $result = $this->eventoccuranceObj->fetch($id);
	   $eventStr = array();
	   if(!empty($result))
	   {
	     if(isset($subEvents[$result['event_id']]))
	     {
	        $eventStr = $subEvents[$result['event_id']]['name'];
	     } 
	   }
	   return $eventStr;
	}
	
	function getDelSubEvents($deliverableId, $subevents)
	{
	   $result = $this -> eventoccuranceObj -> fetch($deliverableId);  
	   $eventStr = "";
	   if(!empty($result))
	   {
	     if(isset($subevents[$result['event_id']]))
	     {
	        $eventStr = $subevents[$result['event_id']]['name'];
	     } 
	   }
	   return $eventStr;
	}
}
?>