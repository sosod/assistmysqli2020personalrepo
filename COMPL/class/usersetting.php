<?php
/*
   User settings 
*/
class UserSetting extends User
{
   //defining user access constants
  
  protected static $table = "useraccess";
  
  function __construct()
  {
     parent::__construct();
  }     
  
  function getUsers($options = "")
  {
     $response = $this -> db-> get("SELECT TK.tkid, UA.id, UA.status, UA.user_id, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
					           FROM #_useraccess UA 
					           INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
					           WHERE UA.status & ".User::DELETED." <> ".User::DELETED." $options
					           ORDER BY TK.tkname, TK.tksurname 
				              ");
     return $response;
  }
  
  function getUser($options="")
  {
     $options  = (!empty($options) ? $options : " UA.user_id = '".$_SESSION['tid']."' ");
	$result    = $this -> db -> getRow("SELECT UA.id, UA.status, CONCAT(TK.tkname, ' ', TK.tksurname) AS user, 
	                                    TK.tkid, UA.financialyear,
	                                    FN.value, FN.start_date, FN.end_date, FN.id AS financialyear_id FROM #_useraccess UA 
							            INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
							            LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = UA.financialyear
							            WHERE 1 AND $options
	     					          ");
	return $result;
  }    
  
  function saveUserSettings($uArr)
  {
	 $userAccesSetting = $this -> getUser(" UA.user_id = '".$uArr['user']."' ");
	 $updateExisting   = FALSE;
	 $userId           = $uArr['user'];

	 unset($uArr['user']);
	 //check if the user has already been set before , if not create new else update existing
	 $id = 0;
	 if(isset($userAccesSetting) && !empty($userAccesSetting))  
	 {
	    $updateExisting = TRUE;
	    $id         = $userAccesSetting['id'];		
	 }
	 
	 $userAccess['status']      = User::calculateUserSettingStatus($uArr);
	 $userAccess['user_id']     = $userId; 
     $userAccess['insert_user'] = $_SESSION['tid']; 		
	 $response 				    = array();
	 if($updateExisting == TRUE)
	 {
		 $response = $this -> updateSettings($id, $uArr);	
	 } else {  
          $response = $this -> db -> insert("useraccess", $userAccess);
          $accessId = $this -> db -> insertedId();
	     if($accessId > 0)
	     {
		   $response = array("text" => "User access setting have been saved", "error" => false);
	     } else {
		   $response = array("text" => "There was an error saving user access settings", "error" => true);
	     }		   
	  } 		 
	  return $response;
   }
   
   function updateFinancialYear($financialyear)
   {
      $this -> setTablename("useraccess"); 
	  $res = $this -> db -> updateData("useraccess", array("financialyear" => $financialyear), array("user_id" => $_SESSION['tid']));
	  if( $res == 1)
	  {
		$response = array("text" => "User access setting have been updated", "error" => false);
	  } else if( $res == 0){
		$response = array("text" => "No change was made to the user access settings", "error" => false);
	  } else {
		$response = array("text" => "There was an error updating user access settings", "error" => true);
	  }
      return $response; 
   }
   
   function updateSettings($id, $data)
   {
	  $userAccess 	             = array();
      $userAccess['status']      = User::calculateUserSettingStatus($data);			
	  $userAccess['insert_user'] = $_SESSION['tid']; 		
      $this -> setTablename("useraccess"); 
	  $res = $this -> update($id, $userAccess, new UserAuditLog(), $this, new Naming());
	  if( $res == 1)
	  {
		 $response = array("text" => "User access setting have been updated", "error" => false);
	  } else if( $res == 0){
		$response = array("text" => "No change was made to the user access settings", "error" => false);
	  } else {
		$response = array("text" => "There was an error updating user access settings", "error" => true);
	  }
      return $response;
    }
	
    function deleteSetting( $settingId )
    {         
	    $this -> setTablename("useraccess");
		$data['status'] = User::DELETED;
		$res            = $this -> update($settingId, $data, new UserAuditLog(), $this, new Naming());
		if( $res > 0)
		{
			$response = array("text" => "User access setting have been deleted", "error" => false);
		} else {
			$response = array("text" => "There was an error deleting user access settings", "error" => true);
		}
		return $response;
	}
		
	
}
?>