<?php
class AssuranceLegislation
{
	private $legislationAssuranceObj = null;

     protected static $tablename = "legislation_assurance";

	function __construct()
	{
		$this->legislationAssuranceObj = new LegislationAssurance(); 
	}
	
	function saveLegislationAssurance( $data )
	{	
		$attachment  = Attachment::makeAttachment("assurancelegislation");
		$data['attachment'] = $attachment;
		$this->legislationAssuranceObj->setTablename("legislation_assurance");
		$res   = $this->legislationAssuranceObj->save( $data );
		$response = array();  
		if( $res > 0)
		{
			$response = array("text" => "Legislation assurance saved . . ", "error" => false);
		} else {
			$response = array("text" => "Error saving legislation assurance . . ", "error" => true);
		}
		return $response; 	
	}
	
	function updateLegislationAssurance( $data )
	{
		$id 	= $data['id'];
		unset($data['id']);
		$this->legislationAssuranceObj->setTablename("legislation_assurance");
		$res 	  = $this->legislationAssuranceObj->update($id, $data, new LegislationAssuranceAuditLog(), $this->legislationAssuranceObj, new LegislationNaming());
		$response = array();  
		if( $res > 0)
		{
			$data = $this->legislationAssuranceObj->fetch( $res );
			$response = array("text" => "Legislation assurance updated . . ", "error" => false, "data" => $data);
		} else {
			$response = array("text" => "Error updating legislation assurance . . ", "error" => true);
		}
		return $response;
	}
	
	function getLegislationAssurance( $start, $limit, $id )
	{
		$results = $this->legislationAssuranceObj->fetchAll($start, $limit, $id);
		$assuranceLegislations = array();
		foreach($results as $index => $leg)
		{
			foreach($leg as $key => $val)
			{
				if($key == 'attachment')
				{
					$assuranceLegislations[$leg['id']]['attachment'] = Attachment::getAttachment($leg['attachment'], "legislation");
				} else{
					$assuranceLegislations[$leg['id']][$key] = $val;
				}
			}
		}	
		$total 	 = $this->legislationAssuranceObj->fetchAssurances($id);
		return array("total" => $total['total'], "data" => $assuranceLegislations );		
	}
		
	function upload()
	{
		$attObj = new Attachment("legislation", "attachDoc");
		$response = $attObj -> upload("assurancelegislation");
		echo json_encode($response);
	}
	
	function removeFile($file, $filename)
	{
		$attObj = new Attachment("legislation", "attachDoc");
		$response = $attObj-> removeFile($file, $filename);
		return $response;
	}
}
