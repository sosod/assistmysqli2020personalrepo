<?php
@session_start();
class Attachment
{
	
	private $uploadDir = "";
	
	private $files = array();
	
	private $errors = array();
	
	private $uploadtype = "";
	
	//optional upload type defined by user
	private $type 		= "";
	
	function __construct($uploadtype, $key = "attachment")
	{
	    $this -> uploadtype = $uploadtype;
	    if(isset($_FILES) && !empty($_FILES))
	    {
		  $this -> files  = $_FILES[$key]; 
	    }
	}

	function upload($attachmentname = "")
	{
	   if($this -> _validateFile($this -> files))
	   {
			$attName = "";
			if($attachmentname == "")
			{
			   $attName = $this -> uploadtype; 
			} else {
			   $attName = $attachmentname;
			}								
			$this -> _createDir();		
	        $uploadtype  = substr($this -> uploadtype, 0, strpos($this -> uploadtype, "_"));
			$name        = $this -> files['name'];
			$tye         = $this -> files['type'];
			$tmp         = $this -> files['tmp_name'];
			$ext_arr     = explode(".", $name);

			$ext         = $ext_arr[count($ext_arr)-1];
			$file_ref    = "compl_".$this -> uploadtype."_".date("YmdHis");
			$file        = $file_ref.".".$ext;
			$destination = $this -> uploadDir."/".$uploadtype."/".$file;

			if(move_uploaded_file($tmp, $destination))
			{
				$_SESSION['uploads']['cmpladded'][$attName][$file] = $name;
				$fileUploads = array();
				$files 		 = array_reverse($_SESSION['uploads']['cmpladded'][$attName]); 
				foreach($files as $key => $fValue)
				{
				   if(file_exists($this -> uploadDir."/".$uploadtype."/".$key))
				   {
					  $ref = substr($key, 0, strpos($key, '.'));
					  $ext = substr($key, strpos($key, '.')+1);
					  $fileUploads[$ref] = array('name' => $fValue, 'ref' => $ref, "key" => $key, "ext" => $ext);
				   } else {
					  unset($_SESSION['uploads']['cmpladded'][$attName][$key]);
				   }
				}				
	
				$response = array(
							  'files'      => $fileUploads,
							  'file'       => $name,
							  'file_ref'   => $file_ref,
							  'text'       => $name.' successfully uploaded',
							  'error'      => false 
							);		
				return  $response;
			} else {
				return  array("error" => true, "text" => "Error uploading file");
			}
		} else {
			$this -> _errorMessage($this -> errors );
		}
	}
	
	private function _createDir()
	{
	    try
	     { 
	        $uploadtype = substr($this -> uploadtype, 0, strpos($this -> uploadtype, "_"));
	     	if(is_dir("../../files/".$_SESSION['cc']))
	     	{
			    $this -> uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref'];
			    //var_dump(is_dir($this -> uploadDir."/".$this -> uploadtype));
			    if(!is_dir($this -> uploadDir."/".$uploadtype))
			    {
				   if(!is_dir($this -> uploadDir))
				   {
				      if(mkdir($this -> uploadDir))
				      {
				          throw new Exception("An error occured trying to create the directory try again ");
				      }			
				   } 
				
				   if(is_dir($this -> uploadDir))
				   {
					  if(mkdir($this -> uploadDir."/".$uploadtype))
					  {
						 if(!mkdir($this -> uploadDir."/".$uploadtype."/deleted"))
						 {
						     throw new Exception("An error occured trying to create directory   please try again");
						 }
					  } else {
					     throw new Exception("An error occured creating the directory, please try again ");
					  }				
				   }
			    }
			  } else {
			  	 throw new Exception("An error occured creating the directory, please try again ");
			  }
		} catch(Exception $e){
		   echo json_encode(array('text' => 'Exception occured '.$e -> getMessage() , "error" => true));
		   exit();
		}	
	}
	 
	private function _validateFile( $file )
	{
		switch ($this->files['error'])
		{
			case UPLOAD_ERR_OK:
				return true;
			break;
			case UPLOAD_ERR_INI_SIZE:
				$this->errors['ini_size'] = "Upload size exceeds maximum set";
				return false;
			break;
			case UPLOAD_ERR_FORM_SIZE:
				$this->errors['form_size'] = "Upload size exceeds maximum set in the form";
				return false;
			break;
			case UPLOAD_ERR_PARTIAL:
				$this->errors['partial']   = "Uploaded file was only partial";
				return false;
			break;
			case UPLOAD_ERR_NO_FILE:
				$this->errors['nofile']    = "There was no file chosen";
				return false;
			break;
			case UPLOAD_ERR_CANT_WRITE:
				$this->errors['cant_write'] = "Failed to write to disk";
				return false;
			break;
			default:
				return true;
			break;
		}
	}
	
	// remove recently uploaded file from the file system
	function removeFile( $file, $attname = "")
	{
		$this -> _createDir();	
		$uploadtype  = substr($this -> uploadtype, 0, strpos($this -> uploadtype, "_"));
		$oldname     = $this -> uploadDir."/".$uploadtype."/".$file;
		if($attname == "")
		{
		   $attName = $this -> uploadtype; 
		} else {
		   $attName = $attname;
		}			
		if(unlink($oldname))
		{
			unset($_SESSION['uploads'][$attname][$file]);
			return array("text" => $attname." attachment successfully removed", "error" => false);
		} else {
			return array("text" => "An error occured removing ".$attname.", please try again");
		}
	}
	
	//deletes previously saved file from the the file system and the database
	function deleteFile($file, $name, $attname = "")
	{
		$this -> _createDir();
		$uploadtype  = substr($this -> uploadtype, 0, strpos($this -> uploadtype, "_"));
		$attName     = "";
		if( $attname == "")
		{
			$attName = $this -> uploadtype; 
		} else {
			$attName = $attname;
		}				
		$oldname = $this -> uploadDir."/".$uploadtype."/".$file;
		$newname = $this -> uploadDir."/".$uploadtype."/deleted/".$file;
		if(rename($oldname, $newname))
		{
			$_SESSION['uploads']['cmpldeleted'][$attName][$file] = $name;
			return array("text" => $name." attachment successfully deleted", "error" => false); 
		} else {
			return array("text" => "Error occured removing ".$name, "error" => true);
		}	
	}
	
	//check the diffence made on the attachment , added, deleted from the system
	public static function processAttachmentChange($attachments, $uploaded_key)
	{
		$changes = array();
		$att 	 = array();
		if( isset($attachments) && !empty($attachments))
		{
		   $att = unserialize(base64_decode($attachments)); 		
		}
        $change_str = "";
		if(isset($_SESSION['uploads']['cmpldeleted']) && !empty($_SESSION['uploads']['cmpldeleted']))
		{
		    if(!empty($att))
		    {
			  foreach($att as $key => $val)
			  { 
				if(isset($_SESSION['uploads']['cmpldeleted'][$uploaded_key]))
				{  
                    if(isset($_SESSION['uploads']['cmpldeleted'][$uploaded_key][$key]))
                    {			    
				      $changes[$key] = "Attachment <i>".$val."</i> has been deleted";
				      $change_str   .= "Attachment <i>".$val."</i> has been deleted <br />\r\n";
				      unset($att[$key]);				      
                    }
				}
			  }
		    }
		}

		if(isset($_SESSION['uploads']['cmpladded'][$uploaded_key]) && !empty($_SESSION['uploads']['cmpladded'][$uploaded_key]))
		{
			foreach($_SESSION['uploads']['cmpladded'][$uploaded_key] as $key => $val)
			{
			   $att[$key] 	  = $val;
			   $changes[$key] = "Attachment <i>".$val."</i> has been added";  
			   $change_str   .= "Attachment <i>".$val."</i> has been added <br />\r\n";
			}
		}
		
		if(isset($changes) && !empty($changes))
		{
			$_SESSION['uploads']['actionchanges'] = $changes;
		}
		
		if(isset($att) && !empty($att))
		{
			$_SESSION['uploads']['attachments'] = base64_encode(serialize($att));
		}
		return $change_str;
	}
	
	private function _errorMessage( $errorArr )
	{
		$errorsResponse = array();
		if( !empty($errorArr))
		{
			foreach($errorArr as $key => $eVal)
			{
				$errorsResponse[$key] = array("text" => $eVal, "error" => true);
			}
		}
		return $errorsResponse;
	}
	
	public static function makeAttachment($type)
	{
		$attachment = "";
		if(isset($_SESSION['uploads']) && !empty($_SESSION['uploads']))
		{
			if(isset($_SESSION['uploads'][$type]) && !empty($_SESSION['uploads'][$type]))	
			{
				$attachment = base64_encode(serialize($_SESSION['uploads'][$type]));
			}
			unset($_SESSION['uploads']);
		}
		return $attachment;
	}
	
	public static function getAttachment($attachment, $type )
	{
	   $attachMents = array();
	   if( !empty($attachment))
	   {
		  $attachs	= unserialize(base64_decode($attachment));
		  if(!empty($attachs))
		  {
			foreach($attachs as $file => $name)
			{
				if(file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type."/".$file))
				{
					$attachMents[$file] = $name;
				}
			}	
		  }
	   }
	   return $attachMents;
	}	
	
	
	public static function displayAttachmentList($attachment)
	{
	   $dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action";
	   if(!empty($attachment))
	   {
		 $attachments = unserialize(base64_decode($attachment));
		 if(!empty($attachments))
		 {
			echo "<span id='result_message'></span><ul id='attachment_list'>";
			foreach($attachments as $file => $name) 
			{
			    $id  = substr($file,0,strpos($file, ".")-1);
			    $_id = substr($file,0,strpos($file, "."));
			    $ext = substr($file, strpos($file, ".") + 1);		
			    if(file_exists($dir."/".$file))
			    {
			       echo "<li id='li_".trim($_id)."'><span id='parent_".$_id."'><a href='../class/request.php?action=ClientAction.downloadFile&file=".$file."&name=".$name."'>".$name."</a>&nbsp;&nbsp;<a href='#' alt='".$name."' file='".$file."' id='".$file."' title=".$_id." class='remove_attach'>Delete</a></span></li>";
			    }
			} 
		    echo "</ul>";
	      }
	   }
	}		
	
}
