<?php
/*
Master legislations from the master compliance database
*/
class MasterLegislation extends Legislation
{
     
     function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll($optionSql = "")
     {
  	  $response = $this -> db2 -> get("SELECT L.*, O.name AS responsible_business_patner FROM #_legislation L
						               LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
						               WHERE L.status & 2 <> 2 AND L.status & 8 =  8 $optionSql
						              ");
        $legislations = array();
        foreach($response as $index => $legislation)
        {
            $legislations[$legislation['id']] = $legislation;
        }
        return $legislations;
     }
     
     function fetch($id)
     {
	   $response	 = $this -> db2 -> getRow("SELECT L.* FROM  #_legislation L WHERE L.id = $id");
        return $response;
     }
     
     function totalLegislations($optionSql = "")
     {
        $result = $this -> db2 -> getRow("SELECT COUNT(*) AS total FROM  #_legislation L 
                                          LEFT JOIN #_organisation O ON O.id = L.responsible_organisation 
                                          WHERE L.status & 2 <> 2 AND L.status & 8 =  8
                                          $optionSql
                                        ");
       return $result;
     }
     
     function getLatestLegislationVersion($id)
     {
        $result = $this -> db2 -> getRow("SELECT id, legislation_id, insertdate FROM #_legislation_edit 
                                          WHERE legislation_id = {$id} 
                                          ORDER BY insertdate DESC 
                                         ");
        return $result;	
     }
            
     
     
     

}
?>
