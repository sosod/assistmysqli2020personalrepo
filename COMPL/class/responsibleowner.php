<?php
class ResponsibleOwner extends Model
{
        protected static $tablename = "responsible_owner";  

       function __construct()
	  {
			parent::__construct();
	  }

	  function getAll()
	  {
	    //get responsible owners from master 
	     $masterObj = new MasterResponsibleOwner();
	     $mResponsibleOwners = $masterObj -> fetchAll();
	     //get client custom ressponsible owners 
	     $clientObj = new ClientResponsibleOwner();
	     $cResponsibleOwners = $clientObj -> fetchAll();
	     
	     $_owners = array_merge($mResponsibleOwners , $cResponsibleOwners);
	     $responsibleOwners = array();
	     foreach($_owners as $index => $responsibleOwner)
	     {
	       $responsibleOwners[$responsibleOwner['id']] = $responsibleOwner;
	     }
	     return $responsibleOwners;
	  }
	  
	  function getOne($id)
	  {
	    $responsibleOwners = $this->getAll();
	    $responsibleOwner  = array();
	    if(isset($responsibleOwners[$id]))
	    {
	      $responsibleOwner[$id] = $responsibleOwners[$id];
	    }
	    return $responsibleOwner;
	  }

       function getList()
       {
          $responsibleowners  = $this->getAll();
          $list = array();
          foreach($responsibleowners as $rIndex => $rVal)
          {
               $list[$rIndex] = $rVal['name'];
          }
          return $list;
       }
     
	        
}
?>