<?php
class DeliverableManager
{

	
	protected $multipleInsert = array();
	
	protected $deliverableObj = null;
	
	protected $legislationId;
	
	protected $deliverablesProgress = array();
	
	protected $deliverableStatus    = array();
	
	function __construct()
	{
		$this->deliverableObj  = new Deliverable();		
		$this -> multipleInsert  = array(
					          "sub_event" 		   => array("t" => "subevent_deliverable", "key" => "subevent_id"),
					          "compliance_to" 	   => array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
					          "accountable_person" => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
	  		);	 		
	}

   /*function __call( $methodname , $args )
   {
	 $classname  = substr($methodname, 3, -4);
	 $resultList = array();
	 if(class_exists($classname))
	 {
	    $classObj = new $classname();
	    $results  = $classObj -> getAll( $args[0] );
	    if(isset($results) && !empty($results))
	    {
	       //load a result list
	       foreach($results as $index => $val)
	       {
		  if(isset($val['name']))
		  {
		    $resultList[$val['id']] = $val['name'];
		  }
	       }    
	    }
	 }
	 return $resultList;
   } */  
   
   function sortDeliverables($deliverables, $options = array(), $delProgress = array())
   {  
      $data                              = array();
      $data['deliverable']               = array(); //holds the deliverable data
      $data['headers']                   = array(); //holds the active deliverable headers for a give section
      $data['deliverableStatus']         = array();  //holds the deliverable statuses
      $data['events']                    = array(); //holds the event that triggers the deliverable
      $data['isDeliverableOwner']        = array(); //holds status of the deliverable is current user is their owner
      $data['deliverableActionProgress'] = array(); //holds the deliverable progress
      $data['deliverablesId']            = array(); //holds the deliverable progress
      $headerObj                         = new DeliverableNaming();
      $page                              = (isset($options['section']) ? $options['section'] : "");
      $headerObj -> setPage($page);
      $headers                           = $headerObj -> getHeaderList(); 
      //Util::debug($headers);
      $time                              = microtime();
      $time                              = explode(' ', $time);
      $time                              = $time[1] + $time[0];
      $start                             = $time;
      
      $subeventManagerObj = new SubEventManager();
      $eventOccuranceObj  = new EventOccuranceManager();
      $subeventObj        = new SubEvent();
      $subeventList       = $subeventObj -> getAll();
      
      $eventObj  = new Event(); 
      $mainevent = $eventObj -> getAll();

      $orgtypeObj = new OrganisationType();
      $orgtype    = $orgtypeObj->getAll(); 	

      $deptObj      = new DepartmentManager();
      $masterDepts  = $deptObj -> getMasterDepartments();
      $clientObj    = new ClientDepartment();
      $clientDepart = $clientObj -> getAll();

      $funcObj      = new FunctionalServiceManager();
      $fxnTitles    = $funcObj -> getFunctionalServicesTitles();
      $fxnServices  = $funcObj -> getClientFunctionalServices();
      
      $complianceFreqObj = new ComplianceFrequency();  
      $complianceFreq    = $complianceFreqObj -> getAll(); 
      
      $complObj 	 = new CompliancetoManager();      
      $compliancesto = $complObj -> getAll();
      $mastertitles  = $complObj -> getComplianceToTitles();
	  $matches       = $complObj -> getMatchList();	   
      
      $accountableObj     = new AccountablePersonManager();
      $accountables       = $accountableObj -> getAll();
      $titles             = $accountableObj -> getAccPersonsTitles();      
      $matchObj           = new AccountablePersonMatch();
      $accountableMatches = $matchObj -> getList();
      
      $resownerObj  = new ResponsibleOwnerManager(); 
      $userObj      = new User();
      $users        = $userObj -> getList();    
      $ownerTitles  = $resownerObj -> getResponsibleOwnerTitles();        
      
      $deliverableStatusObj = new DeliverableStatus();
      $statuses             = $deliverableStatusObj -> getDeliverableStatuses();
      
      $actionManager        = new ManageAction();

      $reportingCategoryObj = new ReportingCategories();  
      $reportingCartegories = $reportingCategoryObj -> getList();
      
      if(!empty($deliverables))
      {
         foreach($deliverables as $deliverableId => $deliverable)
         {   
             $progress                               = 0;
             $deliverableId                          = $deliverable['id'];
             $data['deliverablesId'][$deliverableId] = $deliverableId; 
             //$actionData                             = $actionManager -> calculateActionProgress($deliverableId); 
             $progress                               = 0;
             if(isset($delProgress[$deliverableId]))
             {
                $progress = (float)$delProgress[$deliverableId];
             }
             $data['deliverableActionProgress'][$deliverableId] = $progress;
             
             $data['isDeliverableOwner'][$deliverableId] = Deliverable::isDeliverableOwner($deliverable['responsibility_owner']);
             //hold the deliverables sub-events information even if the sub-event is not displayed on the page
			//if a deliverable has already been created by the sub-event , then get the event that created it
			$subeventStr = "";
			if($this -> deliverableObj -> isCreatedSubevent($deliverable['status']))
			{
			    $eventStr    = $eventOccuranceObj -> getDelSubEvents($deliverable['event_occuranceid'], $subeventList);
				$subeventStr = (!empty($eventStr) ? $eventStr : "");        
			} else {
                $subeventStr = $subeventManagerObj -> getDelSubEvents($deliverableId, $subeventList, new DeliverableSubEvent()); 
			}
		     $data['events'][$deliverableId]  = $subeventStr;		               
             //check if the deliverables keys are allowed in this section
                
             foreach($headers as $key => $head)
             {
               if($key == "action_progress")
               {
                  $data['headers'][$key]                     = $head;
                  $data['deliverable'][$deliverableId][$key] = ASSIST_HELPER::format_percent($progress);
               } else if(array_key_exists($key, $deliverable)) {             
                  $data['headers'][$key] = $head;
                  if($key == "sub_event")
                  {
                     $data['deliverable'][$deliverableId][$key] = ($subeventStr == "" ? "Unspecified" : $subeventStr);	
                  } else if($key == "main_event") {
                     $eventStr = (isset($mainevent[$deliverable[$key]]) ? $mainevent[$deliverable[$key]]['name'] : "");
                     $data['deliverable'][$deliverableId][$key] = $eventStr;
                  } else if($key == "applicable_org_type") {
                    $orgtypeStr = (isset($orgtype[$deliverable[$key]]) ? $orgtype[$deliverable[$key]]['name'] : "");	  	
                    $data['deliverable'][$deliverableId][$key] = $orgtypeStr;                     
                  } else if($key == "responsible") {
                    $deptStr =   $deptObj -> getDeliverableDepartment($deliverable[$key], $deliverable['status'], $clientDepart, $masterDepts);      
                    $data['deliverable'][$deliverableId][$key] = $deptStr; 
                  } else if($key == "functional_service") {
	  			 $functionalStr     = $funcObj -> deliverableFunctionalService($deliverable[$key], $deliverable['status'], $fxnTitles, $fxnServices);
	  			 $data['deliverable'][$deliverableId][$key] = $functionalStr;
                  } else if($key == "compliance_frequency") {
                    $freqStr = (isset($complianceFreq[$deliverable[$key]]) ? $complianceFreq[$deliverable[$key]]['name'] : "");
                    $data['deliverable'][$deliverableId][$key] = $freqStr;
                  } else if($key == "compliance_to") {
                    $compliancetoStr  = $complObj -> getDeliverableComplianceTo($deliverableId, $compliancesto, $mastertitles, $matches);
                    $data['deliverable'][$deliverableId][$key] = (!empty($compliancetoStr) ? $compliancetoStr : "");
                  } else if($key == "accountable_person") {
                    $accountablesStr = $accountableObj -> getDeliverableAccountablePerson($deliverableId, $accountables, $titles, $accountableMatches); 
                    $data['deliverable'][$deliverableId][$key] = (!empty($accountablesStr) ? $accountablesStr : "");      
                  } else if($key == "responsibility_owner") {
                    $ownerStr = $resownerObj -> getDeliverableResponsiblePerson($deliverable[$key], $deliverable['status'], $users, $ownerTitles);
                    $data['deliverable'][$deliverableId][$key] = $ownerStr;
                  } else if($key == "deliverable_status") {
                    $statusStr = (isset($statuses[$deliverable[$key]]) ? $statuses[$deliverable[$key]]['name'] : "New");
                    $data['deliverable'][$deliverableId][$key] = $statusStr;
                  } else if($key == "compliance_date") {
                    if($deliverable['recurring'])
                    {
                      $type = Deliverable::recurringTypes($deliverable['recurring_type']);
                      $date = $deliverable['recurring_period']." ".(isset($deliverables['days_options']) ? $deliverables['days_options'] : "")." ".$type;
                      $data['deliverable'][$deliverableId][$key] = $date;
                    } else {
                      $data['deliverable'][$deliverableId][$key] = $deliverable[$key];
                    }    
                  } else if($key == "reporting_category") {
                     $repCatStr = (isset($reportingCartegories[$deliverable[$key]]) ?  $reportingCartegories[$deliverable[$key]] : "Unspecified");
                     $data['deliverable'][$deliverableId][$key] = $repCatStr;
                  } else {
                    if(Util::isTextTooLong($deliverable[$key]))
                    {
                        $text = Util::cutString($deliverable[$key], $key."_".$deliverableId);
                        $data['deliverable'][$deliverableId][$key] = $text;
                    } else{
				      if($key == 'id')
				      {
				         $data['deliverable'][$deliverableId][$key] = "D".$deliverable[$key];
				      } else {
				         $data['deliverable'][$deliverableId][$key] = $deliverable[$key];
				      }                         
                    }                     
                  }  
               }              
             }
         }
      }
      
      $data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
      $data['columns'] = count($data['headers']);
      $time            = microtime();
      $time            = explode(' ', $time);
      $time            = $time[1] + $time[0];
      $finish          = $time;
      $total_time      = round(($finish - $start), 4);
      //echo 'Page generated in '.$total_time.' seconds.';      
      $data['total_time'] = $total_time;
      return $data;
   }

	public static function calculateDeleverableProgress($legislationId)
	{
		$delObj       = new ClientDeliverable();		
		$optionSql    = "";
          //get those that have been activated by sub-event
        $optionSql .= " AND ( D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT." ";
          //get those created under manage
        $optionSql .= " OR D.status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED." ";
          //get those that have been activated 
        $optionSql .= " OR D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED.")";
        $optionSql .= " AND D.legislation = '".$legislationId."'";
		$deliverables = $delObj -> fetchAll($optionSql);
		$deliverebleStats = array();
		$t = 0;
		foreach($deliverables as $dIndex => $dVal)
		{
		   $t++;
		   $deliverableSql = " AND A.deliverable_id = '".$dIndex."' ";
		   $deliverebleStats[$dIndex]  = Action::calculateActionProgress($deliverableSql);		
		}		
		$deliverebleStats['total'] = $t;
		return $deliverebleStats;
	}	
	

	
     

}
