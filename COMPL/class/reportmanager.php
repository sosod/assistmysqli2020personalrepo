<?php
/*
     manages the generation of different reports 
*/
class ReportManager
{

     static $tablePrefix        = array("legislations" => "L", "deliverables" => "D", "actions" => "A");

     static $allowActionProgress;

     static $reportData         = array();
     
     static $reportTitle        = "";
     
     private $actionObj         = "";
     
     private $deliverableObj    = "";
     
     private $legislationObj    = "";
     
     private $finacialyear      = 0;
     
     function __construct()
     {
        $this -> legislationObj = new ClientLegislation();
        
        $this -> deliverableObj = new ClientDeliverable();
        
        $this -> actionObj      = new ClientAction();   
     }
     /*
      compliance dashboard report for legislation , deliverable and action statuses
     */
     function complianceDashboard($financialyear)
     {
         $legislationStatusCompliance = array();
         $deliverableStatusCompliance = array();
         $actionStatusCompliance      = array();
         $legislationStatusCompliance = $this -> getCompliance($financialyear);
         $deliverableStatusCompliance = $this -> deliverableCompliance();
         $actionStatusCompliance      = $this -> getActionComplianceStatus();
         /*
         $legislationStatusCompliance = $this->_getLegislationCompliance();
         $deliverableStatusCompliance = $this->_getDeliverableCompliance();
         $actionStatusCompliance      = $this->_getActionComplianceStatus();         
         */
         return array("action"      => $actionStatusCompliance,
                      "legislation" => $legislationStatusCompliance,
                      "deliverable" => $deliverableStatusCompliance
                     );
     }     
     
     function getCompliance($financialyear)
     {
         $fields       = array("L.id, L.name, L.reference, L.type");
         $groupBy      = "L.id";
         $optionSql    = " AND L.financial_year = '".$financialyear."' ";
         $results      = $this -> legislationObj -> filterLegislations(implode(",", $fields), $optionSql, $groupBy);
         $legislationsCompleted    = 0;
         $legislationsNotCompleted = 0;
         $legislations             = array();
         if(!empty($results))
         {
           foreach($results as $index => $legislation)
           {
              $deliverableProgress = DeliverableManager::calculateDeleverableProgress($legislation['id']);
              $legislationProgress = 0;
              $legProgress = 0; 
              if(!empty($deliverableProgress))
              {
                foreach($deliverableProgress as $index => $delProgress)
                {
                   //echo $delProgress['totalProgress']."\r\n";
                   $legislationProgress += $delProgress['totalProgress'];
                }
                if($delProgress['total'] != 0)
                {
                  $legProgress = ($legislationProgress/$delProgress['total']); 
                }
              }
              if($legProgress == 100)
              {
               $legislationsCompleted  += 1; 
              } else {
               $legislationsNotCompleted += 1;
              }
           }
          $legislations = array("compliant" => $legislationsCompleted, "notcompliant" => $legislationsNotCompleted);           
         }
         
        return $legislations;           
     }
          
     function getLegislationComplianceStatus($financialyear)
     {
         $fields               = array("L.id, L.name, L.reference, L.type");
         $groupBy              = "L.id";
         $optionSql            = " AND L.financial_year = '".$financialyear."' ";
         $results              = $this -> legislationObj -> filterLegislations(implode(",", $fields), $optionSql, $groupBy);
         $legislations         = array();
         $deliverableActionProgressStatus = array(); 
         $completedBeforeDeadline   = 0;
         $completedOnDeadlineDate   = 0;
         $completedAfterDeadline    = 0;
         $notCompletedAndOverdue    = 0;
         $notCompletedAndNotOverdue = 0;    
         $legislationslist          = array();
         $complianceStatus          = array();
         if(!empty($results))
         {
              foreach($results as $index => $val)
              {
                 $optionSql                    = "";
                 $optionSql                   .= " AND D.legislation = '".$val['id']."' ";                    
                 $deliverableComplianceStatus  = $this -> _getDeliverableCompliance($optionSql, $financialyear);
                 if(!empty($deliverableComplianceStatus))
                 {
                      $legislationslist[$val['id']] = $val['name'];
                      $legislations[$val['id']] = array(
                                               "completedBeforeDeadline" => $deliverableComplianceStatus['completedBeforeDeadline'],
                                               "completedOnDeadlineDate" => $deliverableComplianceStatus['completedOnDeadlineDate'],
                                               "completedAfterDeadline"  => $deliverableComplianceStatus['completedAfterDeadline'],
                                               "notCompletedAndOverdue"  => $deliverableComplianceStatus['notCompletedAndOverdue'],
                                               "notCompletedAndNotOverdue" => $deliverableComplianceStatus['notCompletedAndNotOverdue']
                                              );                    
                 }
              }           
         }
         $complianceStatus = array("legislations" => $legislationslist, "compliance" => $legislations);
         return $complianceStatus;
     }
     
     function _getDeliverableCompliance($optionSql = "", $financial_year = "")
     {
          //$deliverableCompliances = $this->_getLegislationCompliance();
          if(!empty($financial_year))
          {
             $optionSql .= " AND L.financial_year = '".$financial_year."' ";
          }
          $deliverables = array();
          $deliverables = $this -> deliverableObj -> filterDeliverables("D.id, D.legislation, D.legislation_deadline", $optionSql);    
          $completedBeforeDeadline   = 0;
          $completedOnDeadlineDate   = 0;
          $completedAfterDeadline    = 0;
          $notCompletedAndOverdue    = 0;
          $notCompletedAndNotOverdue = 0;
          $compliances               = array();
          if(!empty($deliverables))
          {
               foreach($deliverables as $index => $deliverable)
               {
                  $action_sql  = " AND A.deliverable_id = '".$deliverable['id']."' ";
                  if(!empty($financial_year))
                  {
                     $action_sql .= " AND L.financial_year = '".$financial_year."' ";
                  }
                  $dCompliance = $this -> getActionComplianceStatuses($action_sql, $deliverable['legislation_deadline']);    
                  if(!empty($dCompliance))
                  {
                       $completedBeforeDeadline   += $dCompliance['completedBeforeDeadline'];
                       $completedOnDeadlineDate   += $dCompliance['completedOnDeadlineDate'];
                       $completedAfterDeadline    += $dCompliance['completedAfterDeadline'];
                       $notCompletedAndOverdue    += $dCompliance['notCompletedAndOverdue']; 
                       $notCompletedAndNotOverdue += $dCompliance['notCompletedAndNotOverdue'];                     
                  } else {
                    $notCompletedAndNotOverdue += 1;
                  }                                    
               }                                 
               $compliances['completedBeforeDeadline']   = $completedBeforeDeadline;
               $compliances['completedOnDeadlineDate']   = $completedOnDeadlineDate;
               $compliances['completedAfterDeadline']    = $completedAfterDeadline;
               $compliances['notCompletedAndOverdue']    = $notCompletedAndOverdue;
               $compliances['notCompletedAndNotOverdue'] = $notCompletedAndNotOverdue;                  
          }         
          return $compliances;      
     }
     
     function deliverableCompliance()
     {
        $delCompliance = $this -> _getDeliverableCompliance();
        $deliverables   = array(array("name"  => "Completed\r\nBefore\r\nDeadline", "total" =>  $delCompliance['completedBeforeDeadline']),
                              array("name" => "Completed\r\nOn\r\nDeadline\r\nDate", "total" => $delCompliance['completedOnDeadlineDate']),
                              array("name" => "Completed\r\nAfter\r\nDeadline", "total" => $delCompliance['completedAfterDeadline']),
                              array("name" => "Not\r\nCompleted\r\nAnd\r\nOverdue", "total" => $delCompliance['notCompletedAndOverdue']),
                              array("name" => "Not\r\nCompleted\r\nAnd\r\nNot\r\nOverdue", "total" => $delCompliance['notCompletedAndNotOverdue'])
                           );          
          return $deliverables;                      
     }
     
     function getActionComplianceStatuses($optionSql = "", $delLegislationDate = "")
     {
       //stores those completed on the deadline date
       $actionCompletedOnDeadlineDate     = 0;
       //stores actions completed before deadline date for this deliverable and legislation
       $actionCompletedBeforeDeadlineDate = 0;
       //stotes actions completed on or after the deadline date for this deliverable and legislation
       $actionCompletedAfterDeadlineDate  = 0;
       //stores those that are not completed and are overdue , ie deadline date passed
       $actionNotCompletedAndOverdue      = 0;
       //stores those that are not complete and are not overdude
       $actionNotCompleteAndNotOverDue    = 0;
       //$deliverableActionProgressStatus[$legislationId] = array();
       $actions                           =  $this -> actionObj -> filterActions("A.*", $optionSql);        
       $complianceStatuses                = array();
       if(!empty($actions))
       {
             foreach($actions as $aIndex => $aVal)
             {
              //get the actions that have been completed and have a progress of 100%
                if($aVal['status'] == 3 && $aVal['progress'] == "100")
                {    
                    if(self::completedOnDeadline($delLegislationDate, $aVal['date_completed'])){
                       //get those completed before the legislation deadline date
                       $actionCompletedOnDeadlineDate += 1;
                    } else if(self::completedBeforeLegislationDeadline($delLegislationDate, $aVal['date_completed'])){
                       $actionCompletedBeforeDeadlineDate += 1;
                    } else if(self::completedAfterLegislationDeadline($delLegislationDate, $aVal['date_completed'])){
                       $actionCompletedAfterDeadlineDate  += 1;
                    }
               } else {
                 if(self::notCompletedAndOverdue($delLegislationDate, $aVal['date_completed']))
                 {
                    $actionNotCompletedAndOverdue += 1;
                 } else {                  
                    //remains with those not completed and not overdude
                    $actionNotCompleteAndNotOverDue += 1;
                 }
               }
             }          
            $complianceStatuses = array("completedBeforeDeadline" => $actionCompletedBeforeDeadlineDate,
                                        "completedOnDeadlineDate" => $actionCompletedOnDeadlineDate,
                                        "completedAfterDeadline"  => $actionCompletedAfterDeadlineDate,
                                        "notCompletedAndOverdue"  => $actionNotCompletedAndOverdue,
                                        "notCompletedAndNotOverdue" => $actionNotCompleteAndNotOverDue         
                                       );                  
        }               
        return $complianceStatuses;
     }


     public static function completedOnDeadline($deadlineDate, $actionOn)
     {
          $_deadlineDate = date("dFy", strtotime($deadlineDate));
          $_actionOn     = date("dFy", strtotime($actionOn));
          if($_deadlineDate == $_actionOn)
          {
            return TRUE;
          } else {
            return FALSE;
          }
     }
     
     public static function completedBeforeLegislationDeadline($legislationDeadline, $actionOn)
     {
        if(strtotime($legislationDeadline) > strtotime($actionOn))
        {
           return TRUE; 
        } else {
           return FALSE;
        }
     }
     
     public static function completedAfterLegislationDeadline($legislationDeadline, $actionOn)
     {
        if(strtotime($legislationDeadline." 23:59:59") >= strtotime($actionOn))
        {
          return FALSE; 
        } else {
           return TRUE;
        }
     }     
     
     public static function notCompletedAndOverdue($legislationDeadline, $actionOn)
     {
          if(strtotime($legislationDeadline." 23:59:59") > strtotime(date("dFy")))
          {
             return FALSE;
          } else {
             return TRUE;  
          }
     }
     
     //generate the legislation compliance status per deliverable status report 
     function legislationComplianceStatusPerDeliverable($financial_year)
     {  
         $optionSql    = (!empty($financial_year) ? " AND L.financial_year = '".$financial_year."' " : "");
         $fields       = array("L.id, L.name, L.reference, L.type");
         $groupBy      = "L.id";
         $results      = $this -> legislationObj -> filterLegislations(implode(",", $fields), $optionSql, $groupBy);      
         $legislationDeliverableStatus = array(); 
         $delStatusObj        = new DeliverableStatus();
         $statuses            = $delStatusObj -> getDeliverableStatuses();
         $legislationStatuses = array();
         $legislations        = array();
         $categories          = array();
         $colors              = array("1" => "red", "2" => "orange", "3" => "green");
         if(!empty($results))
         {
              foreach($results as $index => $val)
              {               
                  $legislations[$val['id']]  = $val['name'];
                  $deliverables              = $this -> deliverableObj -> filterDeliverables("D.deliverable_status, COUNT(D.id) AS deliverables", " AND D.legislation = '".$val['id']."'", "D.deliverable_status");                
                  foreach($deliverables as $dIndex => $dVal)
                  { 
                    foreach($statuses as $statusIndex => $status)
                    {
                       $color = ((empty($status['color']) || strtoupper($status['color']) == "FFFFFF") ? $colors[$status['id']] : "#".$status['color']);
                       $categories[$status['id']] = array("code" => $status['id'], "text" => $status['name'], "color" => $color);
                    
                         //get the deliverable for this status
                      if($status['id'] === $dVal['deliverable_status'])
                      {
                        $legislationStatuses[$val['id']][$status['id']] = $dVal['deliverables'];
                      } else {
                        $legislationStatuses[$val['id']][$status['id']] = 0;  
                      }
                    }
                  }
              }              
         }
         $data = array("legislations" => $legislations, "statuses" => $legislationStatuses, "categories" => $categories);
         return $data;
     }
     
     public function getActionProgressStatus($financial_year)
     {
          $usersObj            = new UserAccess();
          $users               = $usersObj -> getUsers();
          $usersActionProgress = array();
          $optionSql           = "";
          $userList            = array();
          foreach($users as $uIndex => $user)
          {
             //store actions completed on the deadline date for this user
             $actionCompletedOnDeadline         = 0;
             //store action completed before the deadline date for this user
             $actionCompletedBeforeDeadline     = 0;
             //store action completed after deadline date for this user
             $actionCompletedAfterDeadline      = 0;
             //store actions not completed and are overdue
             $actionsNotCompletedAndOverdue     = 0;
             //store actions not completed and not overdue
             $actionsNotCompletedAndNotOverdude = 0;
             $optionSql                         = " AND L.financial_year = '".$financial_year."' 
                                                    AND A.owner = '".$user['tkid']."' ";
             $actions                           = $this -> actionObj -> filterActions("A.*", $optionSql);  
             $userList[$user['tkid']]           = $user['user'];             
             if(!empty($actions))
             {
               foreach($actions as $aIndex => $action)
               {
                  if($action['status'] == 3 && $action['progress'] == "100")
                  {
                       if(self::completedOnDeadline($action['deadline'], $action['date_completed']))
                       {
                         $actionCompletedOnDeadline += 1;
                       } else if(self::completedBeforeLegislationDeadline($action['deadline'], $action['date_completed'])){
                         $actionCompletedBeforeDeadline += 1; 
                       } else if(self::completedAfterLegislationDeadline($action['deadline'], $action['date_completed'])){
                         $actionCompletedAfterDeadline += 1;
                       }
                  } else {
                      if(self::notCompletedAndOverdue($action['deadline'], $action['date_completed']))
                      {
                         $actionsNotCompletedAndOverdue += 1;
                      } else {
                         $actionsNotCompletedAndNotOverdude += 1;
                      }
                  }
               }
              $usersActionProgress[$user['tkid']] = array(
                                            "completedBeforeDeadline" => $actionCompletedBeforeDeadline,
                                            "completedOnDeadlineDate" => $actionCompletedOnDeadline,
                                            "completedAfterDeadline"  => $actionCompletedAfterDeadline,
                                            "notCompletedAndOverdue"  => $actionsNotCompletedAndOverdue,
                                            "notCompletedAndNotOverdue" => $actionsNotCompletedAndNotOverdude             
                                           );                              
             }            
          }
          $data = array("users" => $userList, "progressStatus" => $usersActionProgress);          
          return $data;
     }     
     
     public static function checkActionComplianceStatus($action, $actionDeadline)
     {
          $action_compliance_status = "";
          if($action['action_status'] == 3 && $action['action_progress'] == "100")
          {    
              if(self::completedOnDeadline($actionDeadline, $action['datecompleted'])){
                 //get those completed before the legislation deadline date
                 $action_compliance_status = "<span style='background-color:#009900;'>&nbsp;&nbsp;&nbsp;</span>Completed On Deadeline Date";
              } else if(self::completedBeforeLegislationDeadline($actionDeadline, $action['datecompleted'])){
                 $action_compliance_status = "<span style='background-color:#000077;'>&nbsp;&nbsp;&nbsp;</span>Completed Before Deadeline Date";
                 //$actionCompletedBeforeDeadlineDate += 1;
              } else if(self::completedAfterLegislationDeadline($actionDeadline, $action['datecompleted'])){
                 //$actionCompletedAfterDeadlineDate  += 1;
                 $action_compliance_status = "<span style='background-color:#FE9900;'>&nbsp;&nbsp;&nbsp;</span>Completed After Deadeline Date";
              }
         } else {
           if(self::notCompletedAndOverdue($actionDeadline, $action['datecompleted']))
           {
              $action_compliance_status = "<span style='background-color:#FF0000;'>&nbsp;&nbsp;&nbsp;</span>Not Completed And Overdue";
              //$actionNotCompletedAndOverdue += 1;
           } else {                  
              //remains with those not completed and not overdude
              $action_compliance_status = "<span style='background-color:#999999;'>&nbsp;&nbsp;&nbsp;</span>Not Completed And Not Overdue";
              //$actionNotCompleteAndNotOverDue += 1;
           }
         }
         return $action_compliance_status;
     }  


     public static function getLegislationCompliance()
     {   
          $fields       = array("L.id, L.type AS legislation_type");
          $optionSql    = " AND legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."";
          $groupBy      = "GROUP BY L.id";
          $legislations = $this -> legislationObj -> fetchReportData(implode(",", $fields), $optionSql, $groupBy, "");
          $legbytypes   = array();
          foreach($legislations as $index => $leg)
          {
               $legbytypes[$leg['legislation_type']][] = $leg['id'];
          }
          $typeObj             = new LegislativetypeManager();
          $legtypes            = $typeObj -> getAllList();        
          $legislationsByTypes = array();
          foreach($legislations as $legIndex => $legVal)
          {
             if(isset($legtypes[$legVal['legislation_type']]))
             {
               $legislationsByTypes[] = array("header" => $legtypes[$legVal['legislation_type']], 
                                              "total" => count($legbytypes[$legVal['legislation_type']])
                                             );
             }
          }                  
          $response = array("title" => "Legislation TYpes" , "data" => $legislationsByTypes);
          return $legislationsByTypes;
     } 

     
     function getActionComplianceStatus($financial_year)
     {
          if(!empty($financial_year))
          {
              $optionSql = " AND L.financial_year = '".$financial_year."' ";
          }
          $actions      = $this -> actionObj -> filterActions("COUNT(A.id) AS total, A.status", $optionSql, " A.status ");  
          $statusObj    = new ActionStatus();
          $status       = $statusObj->getStatusList(); 
          $actionStatus = array();
          foreach($actions as $aIndex => $actionVal)
          {
            if(isset($status[$actionVal['status']]))
            {
              $actionStatus[$actionVal['status']]  = $actionVal['total'];   
            }
          }
          return $actionStatus;
     }

     /*
      get all the deliverables with their compliance status per reporting category , ie
      per each reporting category , what the compliance status of the deliverables(#)
     */
	function getDeliverableComplianceStatus($financialyear)
	{
	     //get all the deliverables
		$repcategoryObj               = new ReportingCategories();
		$reportingCategories          = $repcategoryObj->getList();
		$reportingCategories[0]       = 'Unspecified';
		$deliverableReportingCategory = array();
		foreach($reportingCategories as $categoryId => $categoryName)
		{ 
            $deliverables = $this -> deliverableObj -> filterDeliverables("D.reporting_category, D.deliverable_status, D.status, D.legislation_deadline", " AND D.reporting_category = '".$categoryId."' AND L.financial_year = '".$financialyear."' ");       
            $deliverableComplianceStatus = array();
            if(isset($deliverables) && !empty($deliverables))
            {
               $completedBeforeDeadline  = 0;
               $completedOnDeadlineDate = 0;
               $completedAfterDeadline  = 0;
               $notCompletedAndOverdue  = 0;
               $notCompletedAndNotOverdue = 0;               
               foreach($deliverables as $index => $val)
               {
                 $dCompliance = $this -> getActionComplianceStatuses(" AND A.deliverable_id = '".$val['id']."' ", $val['legislation_deadline']);   
                 if(!empty($dCompliance))
                 {
                    $completedBeforeDeadline   += $dCompliance['completedBeforeDeadline'];
                    $completedOnDeadlineDate   += $dCompliance['completedOnDeadlineDate'];
                    $completedAfterDeadline    += $dCompliance['completedAfterDeadline'];
                    $notCompletedAndOverdue    += $dCompliance['notCompletedAndOverdue']; 
                    $notCompletedAndNotOverdue += $dCompliance['notCompletedAndNotOverdue'];                        
                 }                   
               }
               $name = $categoryName;
               $deliverableReportingCategory[$categoryId] = array(
                                                              "completedBeforeDeadline" => $completedBeforeDeadline,
                                                              "completedOnDeadlineDate" => $completedOnDeadlineDate,
                                                              "completedAfterDeadline"  => $completedAfterDeadline,
                                                              "notCompletedAndOverdue"  => $notCompletedAndOverdue,
                                                              "notCompletedAndNotOverdue" => $notCompletedAndNotOverdue
                                                             );
            } 
		}
            
            $data = array("data" => $deliverableReportingCategory, "reportingcategories" => $reportingCategories); 
       return $data;
	}


	
	public static function generateReport($postArr, $reportType)
	{
	     $prefix        = self::$tablePrefix[$reportType];
	     $fieldValues   = self::prepareQueryFields($postArr[$reportType], $prefix);
	     $values        = self::prepareValues($postArr['value'], $postArr['match'], $prefix);
	     $groupby       = self::prepareGroupBy($postArr['group_by'],$prefix);
	     $sort          = self::prepareSortBy($postArr['sort'], $prefix);

          $typeInstance = ReportFactory::createInstance($reportType);
          $results      = $typeInstance -> fetchReportData($fieldValues, $values, $groupby, $sort); 
          $sortInstance = ReportFactory::sortInstance($reportType);
          //$namingInstance = ReportFactory::namingInstance($reportType);
          //var_dump($namingInstance);
          $data 	    = $sortInstance -> sortData($results,  $postArr[$reportType]);
          //print "<pre>";
               //print_r($data);
          //print "</pre>";
          self::$reportData  = $data;
          self::$reportTitle = $postArr['report_title'];  
          self::processResponse($postArr['document_format'], $reportType);	
	}
	
	//prepare the legislation field to be obtained by the  query 
	static function prepareQueryFields($fields, $tablePrefix)
	{
		$lstring    = ""; 
		$asFields  = array(
						"id" 		 => "ref",
						"name"		 => "legislation_name",
						"reference"	 => "legislation_reference",
						"action"     => "action_required",
						"owner"      => "action_owner",
						"deadline"   => "action_deadline_date"		
				        );  
		foreach($fields as $key => $val)
		{
			if(array_key_exists($key, $asFields) ){
				$lstring .= $tablePrefix.".".$key." AS ".$asFields[$key].",";
			} else {
				if( $key != "action_progress") {
					$lstring .= $tablePrefix.".".$key.",";
				}
			}
		}	
		$lstring = rtrim($lstring, ',');
		return $lstring;
	}
	//prepare the action fields to be obtained by the query
	function prepareActionFields( $actionArr )
	{
		$actionstring = "";
		$aliasFields  = array(
							"id"		=> "ref",
							"deadline"	=> "action_deadline_date",
							"owner"		=> "action_owner",
							"progress"	=> "action_progress"
							);
		foreach( $actionArr as $key => $val)
		{
			if( array_key_exists($key,$aliasFields))
			{
				$actionstring .= "A.".$key." AS ".$aliasFields[$key].",";
			} else {
				$actionstring .= "A.".$key.",";
			}
		}	
		$actionstring = rtrim($actionstring, ',');
		return $actionstring;
	}
	
	//prepare deliverable fields to be obtained by the query 
	function prepareDeliverableFields( $delivArr )
	{
		$string 		= "";
		$aliasFields 	= array(
								"id"					=> "ref",	
								"compliance_date"		=> "legislation_compliance_date",
								"short_description" 	=> "short_description_of_legislation",
								"applicable_org_type"	=> "applicable_organisation_type",
								"legislative_deadline"	=> "legislative_deadline_date"
							);
		foreach( $delivArr as $key => $val)
		{
			if( array_key_exists($key, $aliasFields))
			{
				$string .= "D.".$key." AS ".$aliasFields[$key].",";
			} else {
				if( $key != "action_progress"){
					$string .= "D.".$key.",";
				}
			}
		}
		$string = rtrim($string, ',');
		return $string;
	}
	
	function prepareValues( $postVaules, $matchArr, $tablePrefix)
	{	
		$wherestring = "";
		$checkId     = array("organisation_size", "organisation_type", "organisation_capacity", "category");
		foreach( $postVaules as $key => $val)
		{
				if(strstr($key, "_date"))
				{   
				     $field = rtrim($key, "_date");
					if( $val['from'] != "" || $val['to'] != "") {
						$wherestring .= " AND ".$tablePrefix.".".$field." BETWEEN '".$val['from']."' AND '".$val['to']."'";
					}
				} else if( is_array($val))
				{
					$values = implode(",", $val);
					if( $values != "all"){
						if( in_array($key, $checkId)){
							if($values !== "2")
							{
								$wherestring .= " AND ".$tablePrefix.".".$key." IN (".$values.")";
							}
						} else {
							$wherestring .= " AND ".$tablePrefix.".".$key." IN (".$values.")";
						}
					}
				} else {
					if( $val != "")
					{
						if( array_key_exists($key, $matchArr))
						{	
							if( $matchArr[$key] == "any")
							{
								$wherestring .= " AND ".$tablePrefix.".".$key." LIKE '%".$val."%' ";
							} else if( $matchArr[$key] == "all"){
								$wherestring .= " AND ".$tablePrefix.".".$key." LIKE '".$val."' ";
							} else if( $matchArr[$key] == "'exact"){
								$wherestring .= " AND ".$tablePrefix.".".$key." = '".$val."' ";
							} 				
						} else {
							$wherestring .= " AND ".$tablePrefix.".".$key." LIKE '".$val."'";
						}						
					} 	
				}
		}
		trim($wherestring);
		$wherestring = rtrim($wherestring,',');
		return $wherestring;
	}
	
	static function prepareGroupBy($groupBy, $tablePrefix)
	{	
		$groupstring = "";
		if($groupBy == "no_grouping")
		{
			$groupstring = "";
		} else {
			$groupstring = $tablePrefix.".".rtrim($groupBy, "_");
		}
		return $groupstring;
	}
	
	static function prepareSortBy($sortArr, $tablePrefix)
	{
		$sortstring = "";
		foreach( $sortArr as $key => $val)
		{
			if( $val != "__action_progress"){
				$sortstring .= $tablePrefix.".".ltrim($val, "__").",";
			}	
		}
		$sortstring = rtrim($sortstring, ',');
		return $sortstring;
	}
	
	static function processResponse($format, $reportType)
	{
		$res = null;
		$data = "";
		self::$reportData['title'] = self::$reportTitle;
		switch ( $format )
		{
			case "on_screen":
				//$res = new HtmlResponse();
				$data = ReportDisplay::displayHtml(self::$reportData, new HtmlResponse(), $reportType);
			break;
			case "microsoft_excell":
				//$res = new CsvResponse();
				$data = ReportDisplay::displayCsv(self::$reportData, new CsvResponse(), $reportType);
			break;
			case "pdf":
				//$res = new PdfResponse();
				$data = ReportDisplay::displayPdf(self::$reportData,new PdfResponse(), $reportType);
			break;
		}
	}
	
	
	
}
