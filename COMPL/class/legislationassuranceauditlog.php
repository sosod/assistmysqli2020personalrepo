<?php
class LegislationAssuranceAuditlog implements Auditlog
{

	function processChanges($obj, $postArr, $id, Naming $naming)
	{		
		$legislationAssurance = $obj->fetch($id);
		$obj_id = $postArr['legislation_id'];
		$changeArr			  = array();
		$tablename 			  = $obj -> getTableName();

		foreach( $postArr as $key => $value)
		{
			if( array_key_exists($key, $legislationAssurance))
			{
			  if( $key == "status"){
					if($value == 2)
					{
						$changeArr['message'] = "Assurance #".$id." has been deleted "; 	
					} 
				} else if($key == "sign_off"){
					$changeArr[$key] = array("from" => $this->_checkSignOff( $legislationAssurance[$key] ), "to" => $this->_checkSignOff( $value ) );		
				} else if( $value != $legislationAssurance[$key]) {
						$changeArr[$key] = array("from" => $legislationAssurance[$key] , "to" => $value);
				}
			}
		}
		
		$result = "";
		if(!empty($changeArr))
		{
			$statusObj = new LegislationStatus();
			$status    = $statusObj -> fetch( $legislationAssurance['status'] );
			if(!empty($status))
			{
				$changeArr['currentstatus'] = $status['name'];
			} else {
				$changeArr['currentstatus'] = "New";
			}			
			$changeArr['user'] = $_SESSION['tkn'];		
			$obj->setTablename( $tablename."_log");
			$insertdata = array("changes" => base64_encode(serialize($changeArr)), "assurance_id" => $id, "legislation_id"=>$obj_id, "insertuser" => $_SESSION['tid'] );
			$result = $obj->save( $insertdata );	
			$obj->setTablename( $tablename );
		}
		return $result;
	}
	
	private function _checkSignOff( $val )
	{
		if( $val == 1)
		{
			return "Yes";
		} else {
			return "No";
		}
	}
}