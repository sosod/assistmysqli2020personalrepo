<?php
class Menu extends Model{

 	private $id;

 	private $name;

 	private $ignite_terminology;

 	private $client_terminology;

 	private $parent_id;

 	private $status;

 	private $position;
 	
 	protected  $tablename = "menu";
 		
	protected $userObj = null;
 	
	function __construct()
	{
		parent::__construct();
		$this->setTablename( $this->tablename );
		$this->userObj  = new UserAccess();
	}
	
	function getMenu()
	{
		
		$response = $this->db->get("SELECT id, name, ignite_terminology, client_terminology FROM #_menu ");
		return $response;
	}
	
	public function getSubMenu( $folder , $pagename)	
	{	
		$mainMenu = array();
		$result = $this->db->get("SELECT id, name, ignite_terminology, client_terminology
							  FROM #_menu 
				   			  WHERE parent_id = (SELECT id 
				   			  					 FROM #_menu 
				   			  					 WHERE name = '".$folder."') ORDER BY position"
						  );
		foreach( $result as $key => $menu)
		{
			$id  	= $menu['id'];
			$url 	= $this->processUrl( $menu['name'] );
			$active = $this->getActivePage( $menu['name'], $pagename);
			$mainMenu[$menu['id']] = array( "id"  		=> $id, 
										    "url" 		=> $url , 
										    'active'	=> $active,
										    'display'=> ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])) ;
		}
		return $mainMenu;			  
	}
	
	function getLowerMenu( $name , $options = "")
	{	
		$userAccess = $this->userObj->getAccessSettings();  	
		$result = $this->db->get("SELECT id, name, ignite_terminology, client_terminology
							      FROM #_menu 
				   			      WHERE parent_id = (SELECT id 
				   			  					 FROM #_menu 
				   			  					 WHERE name = '".$name."')"
						  );
		$menuItems = array();
		foreach( $result as $index => $menu)
		{
			//if menu is create legislation , check if you have access
			if($menu['id'] == 24)
			{
				if(isset($userAccess[$menu['id']]))
				{
					$active = $this->getActivePage( $menu['name'], $name, $options);
					$menuItems[$menu['id']] = array( "id"  	=> $menu['id'], 
												      "url" 	=> $menu['name'].".php" , 
												      'active'	=> $active,
												      'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
													) ;
						
				}
			} else {
				$active = $this->getActivePage( $menu['name'], $name, $options);
				$menuItems[$menu['id']] = array( "id"  	=> $menu['id'], 
											      "url" 	=> $menu['name'].".php" , 
											      'active'	=> $active,
											      'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
												) ;
				
			} 
		}
		return $menuItems;
	}

	private function processUrl( $folder )
	{
		if( $folder == "actions")
		{
			return "../manage/".$folder;
		} else {
			return $folder.".php";
		}
	}

	private function getActivePage( $name , $pagename, $originalPageName = "")
	{		
		$columns = array("columns", "deliverable_columns", "action_columns");
		if( $originalPageName != "")
		{
			$pagename  = $originalPageName;			
		}
		//echo "After name is ".$name." and page name is ".$pagename." and original  page name ".$originalPageName."<br /><br />";
		if( $pagename == strtolower($name) ){
			return "Y";		
		} else if(strstr($pagename, strtolower($name))){
			return "Y";
		} else {		 
			return "";
		}		
	}
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT id, name , client_terminology, ignite_terminology, status FROM #_menu WHERE id = {$id}");
		return $result;
	}
	
	
}