<?php
class ActionReport extends ActionManager
{
     
     function __construct()
     {
          parent::__construct();
     }
     
     function sortActions($actions, $fields = array())
     {    
         $actionnamingObj          = new ActionNaming();
         $naming                   =  $actionnamingObj -> getHeaderList();
         $headers                  = array();
         $naming['date_completed'] = "Date Completed";         
         foreach($naming as $index => $val)
         {
           if(isset($fields[$index]))
           {
              $headers[$index]  = $val;
           }
         }
         if(isset($fields['action_result']))
         {
            $headers['action_result'] = "Action Result";
         }
         $data            = array();
         $data['actions'] = array();
         $data['headers'] = array();
         $ownerObj        = new ActionOwnerManager();
    		 $statusObj       = new ActionStatus();
    		 $statuses        = $statusObj -> getStatusList();    
         foreach($actions as $index => $action)
         {
            foreach($headers as $key => $head)
            {
               if($key == "action_result")
               {
                  $data['headers'][$key]                = $head;
                  $data['actions'][$action['id']][$key] = ReportManager::checkActionComplianceStatus($action, $action['action_deadline']); 
               } else if($key == "owner") {
          			  $data['headers'][$key]	 = $head;
          			  $ownerStr                = $ownerObj -> getActionOwnerStr($action['owner'], $action['actionstatus']);
          			  $data['actions'][$action['id']][$key]  = $ownerStr;						
          		} else if($key == "date_completed") {
                  $data['headers'][$key]                 = $head;
                  $data['actions'][$action['id']][$key]  = (strtotime($action[$key]) == "" ? "" : date("d-M-Y", strtotime($action[$key])) );           
              } else if($key == "status") {
          			  $data['headers'][$key]	 			         = $head;
          			  $data['actions'][$action['id']][$key]  = $statuses[$action['status']];		
          		} elseif(array_key_exists($key, $action)) {	
          			  $data['headers'][$key]	 			         = $head;
          			  $data['actions'][$action['id']][$key]  = $action[$key];						
          		}               
            }
         }    
	       $data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);         
        return $data;
     }
}
?>
