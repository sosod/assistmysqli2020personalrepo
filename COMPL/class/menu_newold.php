<?php
abstract class Menu extends Model
{
   protected $userAccess = null;
   
   const LOWERMENU_USEID = 4;
   
   protected static $tablename = "menu";
   
   function __construct()
   {
      parent::__construct();
      $userAccess = new UserAccess();
      $this->userAccess = $userAccess -> getUserByUserId( $_SESSION['tid'] );
   }

	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT id, name , client_terminology, ignite_terminology, status FROM #_menu WHERE id = {$id}");
		return $result;
	}

	function getMenu()
	{		
		$response = $this->db->get("SELECT id, name, ignite_terminology, client_terminology FROM #_menu ");
		return $response;
	}

	function getActivePage( $menuname, $pagename)
	{
	  $active = '';
     if($menuname == $pagename)
     {
         $active = "Y";
     } else if ( strstr($menuname, $pagename))
     {
       $active = "Y";
     }
     return $active;
	}
	
	function displayMenu($folder, $pagename)
	{
	   $this->createMenu($folder, $pagename);
	}

	abstract function createMenu($folder, $pagename);
}
