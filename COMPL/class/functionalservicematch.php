<?php
class FunctionalServiceMatch extends Model
{
     public function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT id, ref, master_id, status FROM #_functional_service_users WHERE status & 2 <> 2 $options");
        return $results;
     }
     
     function fetch($id)
     {
       $result = $this -> db -> getRow("SELECT id, ref, master_id, status FROM #_functional_service_users WHERE id = '".$id."'");
       return $result;
     }

	function saveFunctionalService($data)
	{
		if(isset($data['nouserinlist']) && !empty($data['nouserinlist']))
		{
			$data['status'] = 1 + SetupManager::NOUSER_IN_LIST;
		}
		unset($data['nouserinlist']);
		$this->setTablename("functional_service_users");
		$id 	 = $this->save($data);
		if($id > 0)
		{
		   $delObj = new Deliverable();
		   $delObj -> updateDeliverableFunctionalService($data['ref'], $data['master_id']);
		}
		$response = $this->saveMessage("functional_service", $id); 
		return $response;
	}
	
	function getObjKey()
	{
	   return "functional_service";
	}
		
	function updateFunctionalService($data)
	{
		$this -> setTablename("functional_service_users");
		$res 	  = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
		$response = $this -> updateMessage("functional_service", $res);
		return $response;
	}	
     
     function deleteFunctionalService($data)
     {
        $id = $data['id'];
        $match = $this -> fetch($id);
        $dataArr['status'] = 2; 
        $auditLog = new SetupAuditLog();
        $this -> setTablename("functional_service_users");
        $log  = $auditLog -> processChanges($this, $data, $id, new ActionNaming());	  
        
        $delRes  = $this -> db -> delete("functional_service_users", "id={$id}");
        if($delRes > 0)
        {
          $delObj = new Deliverable();
          $delObj -> undoClientFunctionalServices($match['ref'], $match['master_id']);
        }
        return $this -> updateMessage("functional_service", $delRes);
     }
     
}
?>