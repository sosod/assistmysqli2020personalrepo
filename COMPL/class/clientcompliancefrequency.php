<?php
class ClientComplianceFrequency extends Model
{
     
     function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll($options = "")
     {
        $results = $this->db->get("SELECT * FROM #_compliance_frequency WHERE status & 2 <> 2 $options ORDER BY id");
        return $results;
     }
     
     function fetch($id)
     {
        $result = $this->db->getRow("SELECT * FROM #_compliance_frequency WHERE id = '".$id."' ");
        return $result;
     }
     
	function updateComplianceFrequency( $data ) 
	{
		$this->setTablename("compliance_frequency");
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this,new ActionNaming());
		$response = $this->updateMessage("compliance_frequency", $res);
		return $response;
	}
	
	function saveComplianceFrequency( $data )
	{
		$this->setTablename("compliance_frequency");
		$id 	 = $this->save($data);
		$response = $this->saveMessage("compliance_frequency", $id); 
		return $response;
	}     

}
?>