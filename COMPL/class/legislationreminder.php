<?php
class LegislationReminder 
{
  private $dbObj;
     
  private $users;
  
  private $legislations;
  
  
  private $legislationOwners = array();
     
  private $emailObj;

  function __construct($dbObj, $emailObj)
  {
     $this -> dbObj = $dbObj;
     
     $this -> emailObj = $emailObj;
  }   
 
  function sendReminders($users)
  {
     $legislations = $this -> getReminders();
     $legislationSend    = 0;  
     $legislationReminders = array();
     
     $headersObj = new LegislationNaming($this -> dbObj -> getDBRef());  
     $headers    = $headersObj -> getHeaderList();          
     
     $statusObj = new LegislationStatus();
     $statuses  = $statusObj -> getList();
     $legAdminObj = new LegislationAdmin();
     $legAuthorizerObj = new LegislationAuthorizer();  
     
     if(!empty($legislations))
     {
        foreach($legislations as $legislationId => $legislation)
        {
           if(isset($this -> legislationOwners[$legislationId]))
           {
               foreach($this -> legislationOwners[$legislationId] as $userIndex => $user)
               {
                  if(isset($users[$userIndex]))
                  {
	               //set variables
	               $statusStr =  (isset($statuses[$value['status']]) ? $statuses[$value['status']] : "New");
	               $capacityStr = OrganisationCapacityManager::getLegislationOrganisationCapacity($legislationId);
	               $orgtypeStr = LegislationOrganisationTypeManager::getLegislationOrganisationType($legislationId);
	               $sizeStr = OrganisationSizeManager::getLegislationOrganisationSizes($legislationId);
	               $categoryStr = LegislativeCategoriesManager::getLegislationCategory($legislationId);
	               $legtype = LegislativetypeManager::getLegislationType($legislation['type']);
	               
	                $emailStr = "";
		           $emailStr .= $headers['id']." : ".$legislation['id']."\r\n\n";
		           $emailStr .= $headers['name']." : ".$legislation['name']."\r\n\n";
		           $emailStr .= $headers['reference']." : ".$legislation['reference']."\r\n\n";
		           $emailStr .= $headers['legislation_number']." : ".$legislation['legislation_number']."\r\n\n";
		           $emailStr .= $headers['type']." : ".$legtype."\r\n\n";
		           $emailStr .= $headers['category'].": ".$categoryStr."\r\n\n";
		           $emailStr .= $headers['organisation_size']." : ".$sizeStr."\r\n\n";
		           $emailStr .= $headers['legislation_date']." : ".$legislation['legislation_date']."\r\n\n";
		           $emailStr .= $headers['organisation_type']." : ".$orgtypeStr."\r\n\n";
		           $emailStr .= $headers['organisation_capacity']." : ".$capacityStr."\r\n\n";
		           $emailStr .= $headers['hyperlink_to_act']." : ".$legislation['hyperlink_to_act']."\r\n";
		           $emailStr .= $headers['status'].": ".$statusStr."\r\n\r\n";
		           //$emailStr .= $headers['action_progress']." : ".$legislation['hyperlink_to_act']."\r\n\r\n";		           
			          /* page through each action and add to table */
								
		           $emailStr.="\r\nTo see all details please log onto ".(isset($_SESSION['DISPLAY_INFO']['ignite_name']) ? $_SESSION['DISPLAY_INFO']['ignite_name'] : "Ignite Assist").".";	
		                 
                     $this ->  emailObj -> setRecipient($users[$userIndex]['email']);					//REQUIRED
                     //$this ->  emailObj -> setRecipient("anesuzina@gmail.com");					//REQUIRED
	                $this ->  emailObj -> setSubject("Reminder for Compliance Legislation ".$legislation['id']);				//REQUIRED
	                $this ->  emailObj -> setBody($emailStr);			//REQUIRED
                   //$me->setBody($html_message);			
                   //$me->setContentType($html_content_type);		//Defaults to TEXT
                   //$me->setCC($cc);						//not required
                   //$me->setBCC($bcc);						//not required
	               //send the email
	                 if($this ->  emailObj ->sendEmail())
	                 {
                         $legislationSend++;
                         $legislationReminders[$legislationSend] = " reminder email sent to ".$users[$userIndex]['email']." for legislation id ".$legislationId;
	                 }                   
                  }  
               }    
           }
        }
     }
     $legislationReminders['totalSend'] = $legislationSend;      
     return $legislationReminders;  
  }
  
  function getReminders()
  {
     $today = date("Y-m-d");
     $results = $this -> dbObj -> mysql_fetch_all("SELECT L.*, LAU.user_id AS authorizers, LA.user_id AS admins 
                                                   FROM ".$this -> dbObj -> getDBRef()."_legislation L
                                                   INNER JOIN ".$this -> dbObj -> getDBRef()."_legislation_admins LA ON L.id = LA.legislation_id
                                                   INNER JOIN ".$this -> dbObj -> getDBRef()."_legislation_authorizers LAU ON L.id = LAU.legislation_id
                                                   WHERE L.reminder = '".$today."' AND L.status <> 3 
												   AND ".Legislation::getStatusSQLForWhere("L")."
                                                 ");
     $legislationList = array();                            
     $legislationOwners = array();                
     $authorizers = array();
     foreach($results as $index => $legislation)
     {
       $this -> legislationOwners[$legislation['id']][$legislation['admins']] = $legislation['admins'];
       $this -> legislationOwners[$legislation['id']][$legislation['authorizers']] = $legislation['authorizers'];
       unset($legislation['authorizers']);
       unset($legislation['admins']);         
       $legislationList[$legislation['id']] = $legislation;
     }
     return $legislationList;
  }
     
}
?>
