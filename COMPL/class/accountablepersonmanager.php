<?php
class AccountablePersonManager
{
	
	private $accountablePersonObj = null;
	
	function __construct()
	{
		$this->accountablePersonObj = new AccountablePerson();
	}
	
	
	function getAccPersonsTitles()
	{
	   $accountablePersonObj = new AccountablePerson();
	   $titles               = $accountablePersonObj -> getAll();
	   
	   $matches = $this -> getMatchesList();
	   $titleList  = array();
	   foreach($titles as $tIndex => $title)
	   {
	     if(isset($matches[$tIndex]))
	     {
	       $titleList[$tIndex] = array("name" => $title['name'], "status" => $title['status'], "used" => true); 
	     } else {
	       $titleList[$tIndex] = array("name" => $title['name'], "status" => $title['status'], "used" => false); 
	     }
	   }
	   return $titleList;
	}
	
	
	function getMatchesList()
	{
	  $matchObj = new AccountablePersonMatch();
	  $matches = $matchObj -> fetchAll();
	  $matchList = array();
	  foreach($matches as $mIndex => $mVal)
	  {
	    $matchList[$mVal['ref']] = array("matchID" => $mVal['id'], "clientID" => $mVal['user_id'], "status" => $mVal['status']); 
	  }
	  return $matchList;
	}
	
	function getAll($options = array())
	{
	   $titleList = $this -> getAccPersonsTitles();     
	   
	   $userObj   = new User();
	   $userList  = $userObj -> getAll();
	   
	   $matches   = $this -> getMatchesList();    

	   $accountablePersonsMatch = array();  
	   foreach($matches as $mIndex => $mVal)
	   {
	     $title = $user = "";
	     if(isset($titleList[$mIndex]))
	     {
	       $title = $titleList[$mIndex]['name'];
	     }
	     
	     if(isset($userList[$mVal['clientID']]))
	     {
	       $user = $userList[$mVal['clientID']]['user'];
	     } else if(isset($titleList[$mVal['clientID']])) {
	        $user =  $titleList[$mVal['clientID']]['name'];	        
	     } else {
	        $user = $title." (Unknown) ";
	     }
	     
	     $accountablePersonsMatch[$mVal['matchID']] = array("title" => $title, "user" => $user, "status" => $mVal['status'], "id" => $mVal['matchID'], "role" => $user." (role of ".$title.") ");
	   } 
        return $accountablePersonsMatch;  	     
	}
	
	function getList()
	{
	   $accountables =  $this -> getAll();
	   $list = array();
	   foreach($accountables as $aIndex => $aVal)
	   {
	     $list[$aVal['id']] = ($aVal['title']==$aVal['user']) ? $aVal['title'] : $aVal['role'];
	   }
	   return  $list;
	}
	
	function getImportList()
	{
	   $accountables =  $this -> getAll();
	   $list = array();
	   foreach($accountables as $aIndex => $aVal) {
	     $list[$aVal['id']] = $aVal['title'];
	   }
	   return  $list;
	}
	
	
	function getDeliverableAccountablePerson($id, $accountables, $titles, $accountableMatches)
	{
        $delAccObj = new DeliverableAccountablePerson();
        $deliverableAccPersons = $delAccObj -> fetchByDeliverableId($id);	   
	   $delAccStr = "";   	   
	   if(!empty($accountableMatches))
	   {
	      foreach($deliverableAccPersons as $dIndex => $dVal)
	      {
	        //if the accountable person id is the match id , then use the matche accountable person role
	        if(($dVal['status'] & DeliverableAccountablePerson::MATCHEDID) == DeliverableAccountablePerson::MATCHEDID)
	        {
	          if(isset($accountables[$dVal['accountableperson_id']]))
	          {
	             $delAccStr .=  $accountables[$dVal['accountableperson_id']]['role']." ,";     
	          } 
	        } else {
	           if(isset($titles[$dVal['accountableperson_id']])) 
	           {
	             $delAccStr .=  $titles[$dVal['accountableperson_id']]['name']." ,";
	           } else {
	              $delAccStr = "Unspecified";
	           }
	        }
	      }
	   } else {

	     foreach($deliverableAccPersons as $dIndex => $dVal)
	     {
	       if(isset($titles[$dVal['accountableperson_id']]))
	       {
	         $delAccStr .=  $titles[$dVal['accountableperson_id']]['name']." ,";
	       }
	     }
	     if(empty($delAccStr))
	     {
	       $delAccStr = "Unspecified";
	     }
	  }	
	  return rtrim($delAccStr, " ,");
	}
	
	function getDeliverableAccountablePersonIds($deliverableId)
	{
	
	    $delAccObj             = new DeliverableAccountablePerson();
	    $deliverableAccPersons = $delAccObj -> fetchByDeliverableId($deliverableId);
	    $listID                = "";
	    foreach($deliverableAccPersons as $dIndex => $dVal)
	    {
	      $listID[$dVal['accountableperson_id']] = $dVal['deliverable_id'];
	    }
	    return $listID; 
	}

	function getAccoutablePersonsRoles($options = array())
	{
	     $accountablePersons = $this->getAll();
	     $accountables = array(); 
		foreach($accountablePersons as $aIndex => $aVal)
		{
             $userStr =  $aVal['user_name'];
             if($aVal['ref_id'] !== $aVal['user_id'])
             {
               $userStr .= " ( role of ".$aVal['title']." )"; 
             }		     
		   $accountables[$aIndex] = $userStr;
		} 
		return $accountables;
	}
		
	function updateDeliverableAccountable($newAccs, $id)
	{
		$this->accountablePersonObj->updateAccountableDeliverable($newAccs, $id);
	}		
	
	function getDelAccountablePerson($id, $accountables, $titles)
	{
	   $matchObj = new AccountablePersonMatch();
	   $accountableMatches = $matchObj -> getList();
        $delAccObj = new DeliverableAccountablePerson();
        $deliverableAccPersons = $delAccObj -> fetchByDeliverableId($id);	   
	   $delAccStr = "";
	   if(!empty($accountableMatches))
	   {
	      foreach($deliverableAccPersons as $dIndex => $dVal)
	      {
	        //if the accountable person id is the match id , then use the matche accountable person role
	        if(($dVal['status'] & DeliverableAccountablePerson::MATCHEDID) == DeliverableAccountablePerson::MATCHEDID)
	        {
	          if(isset($accountables[$dVal['accountableperson_id']]))
	          {
	             $delAccStr .=  $accountables[$dVal['accountableperson_id']]['role']." ,";     
	          } 
	        } else {
	           if(isset($titles[$dVal['accountableperson_id']])) 
	           {
	             $delAccStr .=  $titles[$dVal['accountableperson_id']]['name']." ,";
	           } else {
	              $delAccStr = "Unspecified";
	           }
	        }
	      }
	   } else {

	     foreach($deliverableAccPersons as $dIndex => $dVal)
	     {
	       if(isset($titles[$dVal['accountableperson_id']]))
	       {
	         $delAccStr .=  $titles[$dVal['accountableperson_id']]['name']." ,";
	       }
	     }
	     if(empty($delAccStr))
	     {
	       $delAccStr = "Unspecified";
	     }
	  }	
	  return rtrim($delAccStr, " ,");
	}

}
?>