<?php
class Notification extends Model
{
   
   protected static $table = "user_notifications";
   
   function __construct()
   {
     parent::__construct();   
   }     
   
   function saveNotification($data)
   {
     $res = 0;
     if(!empty($data))
     {
        $data['user_id'] = $_SESSION['tid'];
        $res             = $this -> save($data);
     }
     $response = array();
     if($res > 0)
     {
        $response = array("text" => "User notification successfully saved ", "error" => false);
     } else {
       $response = array("text" => "User notification successfully saved ", "error" => true);
     }
     return $response;
   }  
  
   
   function updateNotification($data)
   {
	    $updatedata         = array();
	    $changes            = array();
	    $notification       = $this -> fetch($data['id']);
		$notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
		                            'due_this_week'            => 'Actions due this week',
		                            'overdue_or_due_this_week' => 'Actions overdue or due this week',
		                            'due_today'                => 'Actions due today',
		                            'overdue_or_due_today'     =>  'Actions overdue or due today'
		                            );
		$recieve_day        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
	    if((int)$notification['recieve_email'] != (int)$data['recieve_email'])
	    {
	        $to                 = ((int)$data['recieve_email'] == 0 ? "No" : "Yes");
	        $from               = ((int)$notification['recieve_email'] == 0 ? "No" : "Yes");
	        $changes['recieve'] = "Recieve email changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_email'] = $data['recieve_email'];
	    }
	    if($notification['recieve_when'] != $data['recieve_when'])
	    {
            if($notification['recieve_when'] == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$notification['recieve_day']]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$notification['recieve_day']]."</em>";    
               }
               $from = $when_str;
            } else {
               $from  = "Daily";
            }
            if($data['recieve_when'] == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$data['recieve_day']]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$data['recieve_day']]."</em>";    
               }
               $to = $when_str;
            } else {
               $to  = "Daily";
            }
	        $changes['recieve_when']    = "Recieve when changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_when'] = $data['recieve_when'];
	    }	    
	    if($notification['recieve_day'] != $data['recieve_day'])
	    {
            $from = $to = "";
            if(isset($recieve_day[$notification['recieve_day']]))
            {
              $from  = $recieve_day[$notification['recieve_day']];    
            }
            if(isset($recieve_day[$data['recieve_day']]))
            {
              $to  = $recieve_day[$data['recieve_day']];    
            }            
	        $changes['recieve_day']    = "Recieve day changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_day'] = $data['recieve_day'];
	    }
	    if($notification['recieve_what'] != $data['recieve_what'])
	    {
	        $to                         = $notification_types[$data['recieve_what']];
	        $from                       = $notification_types[$notification['recieve_what']];
	        $changes['recieve_what']    = "Recieve what changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_what'] = $data['recieve_what'];
	    }   
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['setup_id']    = $data['id'];
	       $changes['user']        = $_SESSION['tkn'];
	       $_changes['ref_']       = "Ref #".$data['id']." has been updated";
	       $changes                = array_merge($_changes, $changes);
	       $updates['insertuser']  = $_SESSION['tid'];
	       $updates['changes']     = base64_encode(serialize($changes));
	       $res                   +=  $this -> db -> insert('user_notifications_logs', $updates);
	       
	    }

		$res = $this -> update($data['id'], $data, new SetupAuditLog());
        if($res > 0)
        {
            $response = array("text" => "Notification updated successfully . . .", "error" => false, 'updated' => true);
        } else if($res === 0){
            $response = array("text" => "No change was made to the notifications . . .", "error" => false, 'updated' => false);  
        } else {
           $response = array("text" => "Error updating the notifications . . .", "error" => false);
        }
       return $response;
   }
     
   function getAll($options = array())
   {
     $results = $this -> db -> get("SELECT * FROM #_user_notifications WHERE status & 2 <> 2 
                                    AND user_id = '".$_SESSION['tid']."' ");
	  $notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
	                            'due_this_week'            => 'Actions due this week',
	                            'overdue_or_due_this_week' => 'Actions overdue or due this week',
	                            'due_today'                => 'Actions due today',
	                            'overdue_or_due_today'     =>  'Actions overdue or due today'
	                            );
	  $recieve_day        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
	  $data               = array();
	  if(!empty($results))
	  {
	   foreach($results as $index => $result)
	   {
	      $data[$result['id']]['recieve']       = $result['recieve_email'];
	      $data[$result['id']]['what']          = $result['recieve_what'];
	      $data[$result['id']]['when']          = $result['recieve_when'];
	      $data[$result['id']]['day']           = $result['recieve_day'];
	      $data[$result['id']]['status']        = $result['status'];
	      $data[$result['id']]['id']            = $result['id'];
	      $data[$result['id']]['recieve_email'] = ($result['recieve_email'] == 0 ? "No" : "Yes");
	      $data[$result['id']]['recieve_what']  = $notification_types[$result['recieve_what']];
	      if($result['recieve_when'] == "weekly")
	      {
	        $when_str   = "Weekly";
	        if(isset($recieve_day[$result['recieve_day']]))
	        {
	            $when_str  .= "<em> on ".$recieve_day[$result['recieve_day']]."</em>";    
	        }
	        $data[$result['id']]['recieve_when'] = $when_str;
	      } else {
	        $data[$result['id']]['recieve_when']  = "Daily";
	      }
	      
	      $data[$result['id']]['status_str']        = ($result['status'] == 0 ? "Inactive" : "Active");
	   }		
	  }                          
	  return $data;	
   }
   
   function fetch($id)
   {
     $result = $this -> db -> getRow("SELECT * FROM #_user_notifications WHERE id = '".$id."' ");
     return $result;
   }
   
	function deleteNotification( $data )
	{
	    $updatedata           = array();
	    $changes              = array();
	    $notification         = $this -> fetch($data['id']);	    
        $changes['status_']   = "Notification setting deleted \r\n\n";
        $updatedata['status'] = 2;
	    $res                  = 0;
	    if(!empty($changes))
	    {
	       $updates['setup_id']    = $data['id'];
	       $changes['user']        = $_SESSION['tkn'];
	       $_changes['ref_']       = "Ref #".$data['id']." has been updated";
	       $changes                = array_merge($_changes, $changes);
	       $updates['insertuser']  = $_SESSION['tid'];
	       $updates['changes']     = base64_encode(serialize($changes));
	       $res                   +=  $this -> db -> insert('user_notifications_logs', $updates);
	    }
		$res        = $this -> update($data['id'], $updatedata, new SetupAuditLog());
		$response   = array();
        if($res > 0)
        {
            $response = array("text" => "Notification deleted successfully . . .", "error" => false, 'updated' => true);
        } else if($res === 0){
            $response = array("text" => "No change was made to the notifications . . .", "error" => false, 'updated' => false);  
        } else {
           $response = array("text" => "Error deleting the notifications . . .", "error" => false);
        }
		return $response;
	}  
	
	function updateStatus($data)
	{
	    $updatedata           = array();
	    $changes              = array();
	    $notification         = $this -> fetch($data['id']);	    
	    if($data['status'] == 0)
	    {
            $changes['status_']   = "Notification setting deactivated \r\n\n";
            $updatedata['status'] = 0;	        
	    } elseif($data['status'] == 1){
            $changes['status_']   = "Notification setting activated \r\n\n";
            $updatedata['status'] = 1;	        
	    }
	    $res                  = 0;
	    if(!empty($changes))
	    {
	       $updates['setup_id']    = $data['id'];
	       $changes['user']        = $_SESSION['tkn'];
	       $_changes['ref_']       = "Ref #".$data['id']." has been updated";
	       $changes                = array_merge($_changes, $changes);
	       $updates['insertuser']  = $_SESSION['tid'];
	       $updates['changes']     = base64_encode(serialize($changes));
	       $res                   +=  $this -> db -> insert('user_notifications_logs', $updates);
	    }
		$res        = $this -> update($data['id'], $updatedata, new SetupAuditLog());
		$response   = array();
        if($res > 0)
        {
            $response = array("text" => "Notification updated successfully . . .", "error" => false, 'updated' => true);
        } else if($res === 0){
            $response = array("text" => "No change was made to the notifications . . .", "error" => false, 'updated' => false);  
        } else {
           $response = array("text" => "Error updated the notifications . . .", "error" => false);
        }
		return $response;
	}  	
	 
   
}
?>
