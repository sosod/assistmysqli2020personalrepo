<?php
class User extends Model
{
   const MODULE_ADMIN 	     = 1;//1000000000	
   const DELETED  	         = 2;   //00000001
   const CREATE_LEGISLATION  = 4;
   const ADMIN_ALL   	     = 8;   //00000010
   const UPDATE_ALL		     = 16;  //00001000
   const VIEW_ALL		     = 32;	//00010000		 
   const EDIT_ALL		     = 64;  //00100000 
   const REPORTS		     = 128; //01000000
   const ASSURANCE 		     = 256; //10000000
   const SETUP		         = 512; //100000000	
   const IMPORT_LEGISLATION  = 1024;//1000000000	

    private $userSettinObj = null;
     
    private $moduleUserObj = null; 
     
    function __construct()
    {
       parent::__construct();
    }     

    /*
       Get all the users as well as their statuses
    */
    function getAll()
    {
        $userSettinObj = new UserSetting();
        $moduleUserObj = new ModuleUser();        
        $moduleusers   = $moduleUserObj -> getUsers();
        $activeUsers   = $userSettinObj -> getUsers();
        $usersList     = array();
        foreach($moduleusers as $uIndex => $user)
        {
          if(isset($activeUsers) && !empty($activeUsers))
          {
             foreach($activeUsers as $aIndex => $aval)
             {
                if($user['tkid'] === $aval['user_id'])
                {
                  $usersList[$user['tkid']] = array("user" => $user['user'], "used" => true, "id" => $user['tkid']);   
                } else {
                  $usersList[$user['tkid']] = array("user" => $user['user'], "used" => false, "id" => $user['tkid']);   
                }
             }          
          } else {
             $usersList[$user['tkid']] = array("user" => $user['user'], "used" => false, "id" => $user['tkid']);   
          }
        }
        return $usersList; 
    }

     
    function getList()
    {
      $users = $this -> getAll();
      $list  = array();
      foreach($users as $uIndex => $uVal)
      {
         $list[$uIndex] = $uVal['user'];
      }
      return $list;
    }

    function __call($methodname, $args)
    {
       if(substr($methodname, 0, 2) == "is")
       {
         return $this->_checkStatus($methodname, $args);
       }
    }
    
	/*
	  calculate the user status of the user as per user-access constants 
	*/
	public static function calculateUserSettingStatus($userStatuses)
	{
		$userAccessConstants = self::userConstants();
		$status  = 0;	
		foreach($userAccessConstants as $constant => $constantVal)
		{
		   $constantName = strtolower($constant);
		   if(isset($userStatuses[$constantName]))
		   {
		     if($userStatuses[$constantName] == 1)
		     {     
		       $status += $constantVal;
		     }
		   }
		}    
		return $status;
	}
	/*
	 get an array of the user-access class constants
	*/
     public static function userConstants()
     {
        $oClass              = new ReflectionClass("User");
        $userAccessConstants = $oClass->getConstants();        
        return $userAccessConstants;
     }	    
    
    function _checkStatus($methodname, $args)
    {
       $constantStr          = preg_replace("/(?=[A-Z])/", "_", $methodname);     
       $constStr             = strtoupper(substr($constantStr, strpos($constantStr, "_")+1)); 
       $userSettingConstants = User::userConstants();
       if(isset($userSettingConstants[$constStr]))
       {
           if(($userSettingConstants[$constStr] & $args[0]) == $userSettingConstants[$constStr])
           {
               return TRUE;
           }
           return FALSE;
       }
       return FALSE;
    }

}
?>