<?php
class ImportLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getOptionSql($options = array())
	{
	    $optionSql = array();
	    $clientSql = "";
        if(isset($options['legoption']) && !empty($options['legoption']))
        {
          $clientSql = " AND legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE." ";
        }
        if(isset($options['legislation_id']) && !empty($options['legislation_id']))
        {
          $clientSql .= " AND L.id = '".$options['legislation_id']."' ";
        }
        if(isset($options['limit']) && !empty($options['limit']))
        {
          $clientSql .= " LIMIT ".$options['start'].",".$options['limit']; 
        }
        $optionSql['client'] = $clientSql;
        $optionSql['master'] = ""; 
        return $optionSql;
	}
	
	function sortLegislation($masterLegislations, $clientLegislations = array())
	{
        $headerObj                  = new LegislationNaming();
        $headerObj -> setPage("new"); 
        $headers                    = $headerObj -> getHeaderList(); 
        $data                       = array();
        $data['status']             = array();
        $data['headers']            = array();
        $data['usedId']             = array();
        $data['admin']              = array();
        $data['authorizor']         = array();
        $data['isLegislationOwner'] = array();  
        $data['legislations']       = array();
	    $legAdminObj                = new LegislationAdmin();
	    $legAuthorizerObj           = new LegislationAuthorizer();
	    $financialYearObj           = new FinancialYear();
	    $years                      = $financialYearObj -> fetchAll();
	    $statusObj                  = new LegislationStatus();
	    $statuses                   = $statusObj -> getList();
        foreach($clientLegislations as $masterId => $legislation)
        {
            if(isset($masterLegislations[$masterId]))
            {
               $data['usedId'][]              = $masterId;
	           $authorizers                   = $legAuthorizerObj -> getLegislationAuthorizers($legislation['id']);
	           $admins                        = $legAdminObj -> getLegislationAdmins($legislation['id']);               
	           $data['admin'][$masterId]      = $admins;
	           $data['authorizor'][$masterId] = $authorizers;
               if(Legislation::isLegislationOwner($admins))
               {
                  $data['isLegislationOwner'][$masterId] = true;
               }	          
               /*if(isset($legislation['admin']) && !empty($legislation['admin']))
               {
                  $data['admin'][$masterId] = explode(",", $legislation['admin']);
               }
               if(isset($legislation['authorizer']) && !empty($legislation['authorizer']))
               {
                  $data['authorizor'][$masterId] = explode(",", $legislation['authorizer']);
               }*/               
               foreach($legislation as $key => $value)
               {
                 if(isset($headers[$key]))
                 {
                    if($key == "type")
                    {
                       $legtype = LegislativetypeManager::getLegislationType($masterLegislations[$masterId][$key]);
                       $data['legislations'][$masterId][$key] = $legtype;  
                    } elseif($key == "category") {
                       $categories = LegislativeCategoriesManager::getMasterLegislationCategory($masterId);
                       $data['legislations'][$masterId][$key] = $categories;  
                    } elseif($key == "organisation_size") {
                       $sizes  = OrganisationSizeManager::getMasterLegislationOrganisationSizes($masterId);
                       $data['legislations'][$masterId][$key] = $sizes;
                    } elseif($key == "organisation_type") {
                       $orgtypeStr = LegislationOrganisationTypeManager::getMasterLegislationOrganisationType($masterId);
                       $data['legislations'][$masterId][$key] = $orgtypeStr;
                    } elseif($key == "organisation_capacity") {
                       $capacityStr = OrganisationCapacityManager::getMasterLegislationOrganisationCapacity($masterId);
                       $data['legislations'][$masterId][$key] = $capacityStr;
                    } elseif($key == "status") {
                       $statusStr                             = (isset($statuses[$value]) ? $statuses[$value] : "New");
                       $data['legislations'][$masterId][$key] = $statusStr;                       
                    } else {
                      if(isset($masterLegislations[$masterId][$key]))
                      {
                        $data['legislations'][$masterId][$key] = $masterLegislations[$masterId][$key];
                      }
                    }
                    $data['headers'][$key] = $headers[$key];
                 } elseif($key == "financial_year") {
                     $data['legislations'][$masterId]['financial_year'] = $years[$value]['value'];
                     $data['headers'][$key]                             = 'Financial Year';
                 }
               }
               //$masterLegislations[$legislation['legislation_reference']];
               $data['status'][$masterId] = $legislation['legislation_status'];
            }
        }
        
		$data['company']= ucfirst($_SESSION['cc']);
		$data['user']   = $_SESSION['tkn'];
		$data['users']  = array();
		$data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
		$data['columns']= count($data['headers']);	         
          return $data;
	}
}
?>

