<?php
class UserAccess extends Model
{
	//defining user access constants
	const MODULE_ADMIN 		 = 1;//1000000000	
	const DELETED  	      = 2;   //00000001
	const CREATE_LEGISLATION  = 4;
	const ADMIN_ALL   		 = 8;   //00000010
	const UPDATE_ALL		 = 16;  //00001000
	const VIEW_ALL		      = 32;	//00010000		 
	const EDIT_ALL			 = 64;  //00100000
	const REPORTS			 = 128; //01000000
	const ASSURANCE 		 = 256; //10000000
	const SETUP		      = 512; //100000000	
	const IMPORT_LEGISLATION  = 1024;//1000000000	
	
	protected static $tablename = "useraccess";
	
	function __construct()
	{
		parent::__construct();
	}
	 /*
	   get all the users enabled for this module
	 */
	 function getUsers($options = "")
	 {
	    $results = $this->db->get("SELECT TK.tkid, CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkstatus, TK.tkemail AS email
	                               FROM assist_".$_SESSION['cc']."_timekeep TK 
	                               INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
	                               WHERE TK.tkid <> '0000' AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."' $options
	                               ORDER BY TK.tkname, TK.tksurname 
	                              ");
	    return $results;
	 }
	/*
	  get the user's list , by specifying the key you want as an array 
	*/
	function getUserList($args = array())
	{
	   
	   //store the list of users
	   $list = array();
	   $users = $this->getUsers();
	   if(isset($args) && !empty($args))
	   {
	     foreach($users as $uIndex => $user)
	     {
	       foreach($args as $index => $arg)
	       {
	          if(array_key_exists($arg, $user))
	          {
	             $list[$user['tkid']] = $user[$arg];
	          }
	       }
	     }
	   }
	   return $list;
	}
	
	/*
        Get users who have been configured for this module  	
	*/
	function getActiveUsers()
	{
	     //all users
		$users = $this -> getUsers();
		$configuredUsers = $this -> getUserAccessSettings();	
		$usersList = array();
		foreach($users as $uIndex => $uArr)
		{
	        foreach($configuredUsers as $aIndex => $aArr)
		   {
			if($uArr['tkid'] === $aArr['user_id'])
			{
			  $usersList[$aArr['user_id']] = $uArr;
			}
		   }		
		}
		return $usersList;
	}	
	/*
	  get all the user access settings details
	*/
	function getUserAccessSettings()
	{
		$response = $this -> db-> get("SELECT UA.id, UA.status, UA.user_id, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								 FROM #_useraccess UA 
								 INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								 WHERE UA.status & ".UserAccess::DELETED." <> ".UserAccess::DELETED." ORDER BY TK.tkname, TK.tksurname 
							     ");
		return $response;
	}
	/*
	 get user detailts as per specified user id
	*/
	function getUser($userId)
	{
		$response = $this -> db -> getRow("SELECT CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkemail AS email
		                                   FROM assist_".$_SESSION['cc']."_timekeep TK 
								     WHERE TK.tkid  = $userId 
								    ");
		return $response;	
	}	
	/*
	  get user access setting for a user
	*/
	function getUserAccessSetting($options = array())
	{
	     $field = "";
	     $value = "";
	     $sqlOption = "";
	     if(isset($options) && !empty($options))
	     {
	       $field   = key($options);
	       if(isset($options[$field]))
	       {
	          $value = $options[$field]; 
	       }
	       $sqlOption = " AND UA.".$field." = ".$value;
	     }	     
		$response = $this->db->getRow("SELECT UA.id, UA.status, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								 FROM #_useraccess UA 
								 INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								 WHERE 1 $sqlOption
							    ");
         return $response;
	}
	/*
	  get  the user access settings for the current logged in user
	*/
	function getUserAccess()
	{
		$response = $this -> db -> get("SELECT * FROM #_useraccess WHERE user_id = '".$_SESSION['tid']."'");
		return $response;
	}
	/*
	  get the user access settings for specified user_id
	*/
	function getUserByUserId( $userId )
	{
		$result = $this->db->getRow("SELECT id, user_id, status FROM #_useraccess WHERE user_id = '".$userId."'");
		return $result;
		
	}
	
	/*	
       get all the users who are not yet configured or have their configuration deleted
     */
     function getInActiveUsers()
     {
        $users = $this->getUsers();
        $activeUsers = $this->getActiveUsers();
        $usersList = array();
        foreach($users as $uIndex => $user)
        {
          if(isset($activeUsers[$user['tkid']]))
          {
            $usersList[$user['tkid']] = array("user" => $user['user'], "used" => true);
          } else {
            $usersList[$user['tkid']] = array("user" => $user['user'], "used" => false);
          }
        }
        return $usersList; 
     }
     
     
	function saveUserSettings( $uArr )
	{
		$userAccesSetting = $this->getUserAccessSetting(array("user_id" => $uArr['user']));
		$updateExisting = FALSE;
		$userId         = $uArr['user'];
		unset($uArr['user']);
		//check if the user has already been set before , if not create new else update existing
		$id = 0;
		if(isset($userAccesSetting) && !empty($userAccesSetting))  
		{
		  $updateExisting = TRUE;
		  $id         = $userAccesSetting['id'];		
		}
		$userAccess['status']      = self::calculateUserAccessStatus($uArr);
		$userAccess['user_id']     = $userId; 
          $userAccess['insert_user'] = $_SESSION['tid']; 		
		$response 				   = array();
		if($updateExisting == TRUE)
		{
			$response = $this->updateUserAccess($id, $uArr);	
		} else {  
		     $response = $this->db->insert("useraccess", $userAccess);
		     $accessId = $this->db->insertedId();
			if($accessId > 0)
			{
				$response = array("text" => "User access setting have been saved", "error" => false);
			} else {
				$response = array("text" => "There was an error saving user access settings", "error" => true);
			}		   
		} 		 
		return $response;
	}

	
	function updateUserAccess($id, $data)
	{
		$userAccess 	       = array();
          $userAccess['status'] = self::calculateUserAccessStatus($data);			
		$userAccess['insert_user'] = $_SESSION['tid']; 		
	     $this->setTablename("useraccess"); 
		$res = $this->update($id, $userAccess, new UserAuditLog(), $this, new Naming());
		if( $res == 1)
		{
			$response = array("text" => "User access setting have been updated", "error" => false);
		} else if( $res == 0){
			$response = array("text" => "No change was made to the user access settings", "error" => false);
		} else {
			$response = array("text" => "There was an error updating user access settings", "error" => true);
		}
         return $response;
	}
	
	function deleteUserSetting( $settingId )
	{         
	     $this->setTablename("useraccess");
		$data['status'] = UserAccess::DELETED;
		$res = $this->update($settingId, $data, new UserAuditLog(), $this, new Naming());
		if( $res > 0)
		{
			$response = array("text" => "User access setting have been deleted", "error" => false);
		} else {
			$response = array("text" => "There was an error deleting user access settings", "error" => true);
		}
		return $response;
	}
		
	/*
	  calculate the user status of the user as per user-access constants 
	*/
	public static function calculateUserAccessStatus($userStatuses)
	{
		$userAccessConstants = self::userAccessConstants();
		$status  = 0;	
		foreach($userAccessConstants as $constant => $constantVal)
		{
		   $constantName = strtolower($constant);
		   if(isset($userStatuses[$constantName]))
		   {
		     if($userStatuses[$constantName] == 1)
		     {     
		       $status += $constantVal;
		     }
		   }
		}    
		return $status;
	}
	/*
	 get an array of the user-access class constants
	*/
     public static function userAccessConstants()
     {
        $oClass              = new ReflectionClass("UserAccess");
        $userAccessConstants = $oClass->getConstants();
        
        return $userAccessConstants;
     }
     /*
	function getAccessSettings()
	{
		$user 		= $this->getUserByUserId($_SESSION['tid']);
		$userAccess = array();
		if(!empty($user))
		{
			if(($user['status'] & UserAccess::MODULE_ADMIN) == UserAccess::MODULE_ADMIN)
			{
				$userAccess['MODULE_ADMIN'] = TRUE;	
			}
			 
			if(($user['status'] & UserAccess::CREATE_LEGISLATION) == UserAccess::CREATE_LEGISLATION)
			{
				//the id for the menu to create legislation is 24 , so 
				$userAccess['24'] = TRUE;
				$userAccess['CREATE_LEGISLATION'] = TRUE;	
			} 
			
			if(($user['status'] & UserAccess::ADMIN_ALL) == UserAccess::ADMIN_ALL)
			{
				$userAccess['ADMIN_ALL'] = TRUE;	
			}

			if(($user['status'] & UserAccess::UPDATE_ALL) == UserAccess::UPDATE_ALL)
			{
				$userAccess['UPDATE_ALL'] = TRUE;	
			}			
			
			if(($user['status'] & UserAccess::VIEW_ALL) == UserAccess::VIEW_ALL)
			{
				$userAccess['VIEW_ALL'] = TRUE;	
			}				
					
			if(($user['status'] & UserAccess::EDIT_ALL) == UserAccess::EDIT_ALL)
			{
				$userAccess['EDIT_ALL'] = TRUE;	
			}		

			if(($user['status'] & UserAccess::ASSURANCE) == UserAccess::ASSURANCE)
			{
				$userAccess['16'] = TRUE;
				$userAccess['ASSURANCE'] = TRUE;	
			}					
		}
		return $userAccess;
	}
     */

}
?>
