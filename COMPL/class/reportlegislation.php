<?php
class ReportLegislation extends LegislationManager
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function sortLegislation($legislations, $fields = array())
     { 
        $headers = array();
        foreach($this -> legislationHeaders as $index => $header)
        {
          if(isset($fields[$index]))
          {
             $headers[$index] = $header; 
          }
        }
        $data = array();
        foreach($legislations as $legislationId => $value)
        {
          foreach($headers as $key => $header)
          {
                 if($key == "action_progress") 
                  {
                    $data['headers'][$key]  = $header;
                    $progress = self::calculateLegislationProgress($legislationId);
                    $data['legislations'][$legislationId][$key] = (isset($progress[$legislationId]) ? $progress[$legislationId]['action_progress'] : "0" );
                  }  else if($key == "type") {
                    $data['headers'][$key]  = $header;
                    $legtype = LegislativetypeManager::getLegislationType($value[$key]);
				$data['legislations'][$legislationId][$key] = $legtype;						
			   } else if($key == "category") {
				$data['headers'][$key]  = $header;
				$categoryStr = LegislativeCategoriesManager::getLegislationCategory($legislationId);
				$data['legislations'][$legislationId][$key] = $categoryStr;
			   } else if($key == "organisation_size") {
				$data['headers'][$key]  = $header;
				$sizeStr = OrganisationSizeManager::getLegislationOrganisationSizes($legislationId);
				$data['legislations'][$legislationId][$key] = $sizeStr;                  	        
			   } else if($key == "organisation_type") {
				$data['headers'][$key]  = $header;
				$orgtypeStr = LegislationOrganisationTypeManager::getLegislationOrganisationType($legislationId);
				$data['legislations'][$legislationId][$key] = $orgtypeStr;                  	        
			   } else if($key == "organisation_capacity") {	         
				$data['headers'][$key]  = $header;
				$capacityStr = OrganisationCapacityManager::getLegislationOrganisationCapacity($legislationId);
			     $data['legislations'][$legislationId][$key] = $capacityStr;   
			   } else if($key == 'status') {
                    $data['headers'][$key]  = $header;
                    $data['legislations'][$legislationId]['status'] = (isset($statuses[$value['status']]) ? $statuses[$value['status']] : "New");
                  } else if(isset($value[$key])) {
                    $data['legislations'][$legislationId][$key] = $value[$key];
                    $data['headers'][$key] = $header;               
                  }

          }
        }
        $data['columns'] = (isset($data['headers']) ? count($data['headers']) : count($headers));
	   $data['headers'] = (isset($data['headers']) ? $data['headers'] : $headers);
	  return $data;
     }
     

}
?>
