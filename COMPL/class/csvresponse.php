<?php
class CsvResponse extends Response
{

	function showResponse($data)
	{
	   $string   = "";
	   $string   = $_SESSION['cn']."\r\n";
	   $string  .=  $_POST['report_title']."\r\n";
	   $string  .= "\r\n";
	   foreach($data['headers']  as $key => $head)
	   {
	 		$string .= $head.",";
	   }
	   $string .= "\r\n"; 
	   foreach( $data['data'] as $index => $valArr)
	   {
			foreach( $valArr as $key => $value)
			{
				$string .= $value.",";
			}
			$string .= "\r\n";
	   }		
	   $string .= "";
	   $filename =  "report_".date("YmdHis").".csv";
	   $fp       =  fopen($filename, "w+");
	   fwrite($fp, $string);
	   fclose($fp);
	   header('Content-Type: application/csv'); 
	   header("Content-length: " . filesize($filename)); 
	   header('Content-Disposition: attachment; filename="' . $filename . '"'); 
	   readfile( $filename );
	   unlink($filename);		
	   exit();
	}
	
}
