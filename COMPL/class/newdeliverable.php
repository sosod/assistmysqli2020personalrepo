<?php
//handle deliverables activities under the new section
class NewDeliverable extends DeliverableManager
{
	
	function __construct()
	{
	   parent::__construct();
	}	
	
	function getOptions($options = array())
	{
	   $optionSql = " AND D.status ".Deliverable::ACTIVATED." <> ".Deliverable::ACTIVATED." ";
	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
		//$this->legislationId = $options['legislation_id'];
		$optionSql = " AND D.legislation = '".$options['legislation_id']."'";
	   }
	   if(isset($options['reference']) && !empty($options['reference']))
	   {
		$optionSql .= " AND D.reference = '".$options['reference']."'";
	   }
	   
	   if(isset($options['page']) && !empty($options['page']))
	   {
		$delOption['page'] = $options['page'];
	   }  
	   
	   if(isset($options['limit']) && !empty($options['limit']))
	   {
	     $optionSql  .=  " LIMIT ".$options['start']." , ".$options['limit']; 
	   }
	  return $optionSql; 
	}
	
	/*
	 get the deliverable under the new section
	function getDeliverables( $start , $limit, $options = array())
	{
		$optionSql = "";
		$delOption = array();
		if(isset($options['legislation_id']) && !empty($options['legislation_id']))
		{
			$this->legislationId = $options['legislation_id'];
			$optionSql = " AND D.legislation = '".$options['legislation_id']."'";
		}
		if(isset($options['reference']) && !empty($options['reference']))
		{
			$optionSql .= " AND D.reference = '".$options['reference']."'";
		}
		if(isset($options['page']) && !empty($options['page']))
		{
			$delOption['page'] = $options['page'];
		}
		$deliverables  = array();
          $allDeliverables        = $this->allDeliverableInfo($optionSql);	
		$results  		= $this->deliverableObj->fetchAll($start, $limit, $optionSql);
		$deliverables       = $this->sortDeliverables( $results , new DeliverableNaming($delOption['page']));
		
		$delStats 		= $this->deliverableObj->totalDeliverables($optionSql);
          $deliverables['total']  =  $delStats['total'];	
          $deliverables = array_merge($deliverables, $allDeliverables);					
		return $deliverables;
	}
	
	function getDeliverableActions( $start, $limit, $options = array())
	{
		$optionSql = "";	
		if(isset($options['legislationId']) && !empty($options['legislationId']))
		{
			$optionSql = " AND legislation = '".$options['legislationId']."'";
		}
		$results 	  = $this->deliverableObj->fetchAll($start, $limit, $optionSql);
		$deliverables = $this->sortDeliverables( $results, new DeliverableNaming(), "new");
		$total		  = $this->deliverableObj->totalDeliverables( $optionSql ); 
		$output       = $this->createDeliverableHtml($deliverables ,$total['total'], $options['legislationId'] );
		return  $output;	
	}
     */


	
}
?>
