<?php
class AdminDeliverable extends DeliverableManager
{
      function __construct()
      {
         parent::__construct();
      }
      
      function getOptions($options = array())
      {
        $option_sql = "";
		if(isset($options['events']) && !empty($options['events']))
		{
		   $option_sql  = " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED;
		   $option_sql .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT;
		}
		if(isset($options['user']) && !empty($options['user']))
		{
		   $option_sql .= " AND D.responsibility_owner = '".$options['user']."' "; 
		}
		if(isset($options['occuranceid']) && !empty($options['occuranceid']))
		{
		  $option_sql .= " AND D.event_occuranceid = '".$options['occuranceid']."' ";
		}
		if(isset($options['legislation_id']) && !empty($options['legislation_id']))
		{
		  $option_sql .= " AND D.legislation  = '".$options['legislation_id']."' ";
		}
		if(isset($options['financial_year']) && !empty($options['financial_year']))
		{
		   $option_sql .= " AND L.financial_year = '".$options['financial_year']."' ";
		   $option_sql .= " AND (L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
		                    OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.") 
		                    ";
		}
		if(isset($options['page']))
		{
		   if($options['page'] == "edit" || $options['page'] == "update")
		   {
              //DELIVETABLES OBTAINED UNDER MANAGE MUST MEET THE CRETERIA SET BELOW
              //get those that have been confirmed and activated but not created by sub-events
              $option_sql .= " AND( (D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." ";
              $option_sql .= " AND  D.status & ".Deliverable::CREATED_SUBEVENT." <> ".Deliverable::CREATED_SUBEVENT." ) ";
                
                //get those created under manage
              $option_sql .= " OR D.status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED." ";            
                //get those that have been activated by sub-event, but they must have been created by subevent and activated
              $option_sql .= " OR (D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." ";
              $option_sql .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT." ";
              $option_sql .= " AND D.status & ".Deliverable::ACTIVATED_BYSUBEVENT." = ".Deliverable::ACTIVATED_BYSUBEVENT.") )";
              
		      $option_sql .= " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ";
		   }
		}
		//$optionSql .= " AND D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." "; 
		$option_sql .= " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
		if(isset($options['limit']) && !empty($options['limit']))
		{
		   $option_sql .= " LIMIT ".$options['start']." , ".$options['limit'];  
		}
		
	   return $option_sql;	          
      }
      
      /*
      function getDeliverables($start, $limit, $options = array())
      {
		$optionSql    = "";
		
		if(isset($options['events']) && !empty($options['events']))
		{
		   $optionSql    = " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED;
		   $optionSql   .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT;
		}
		if(isset($options['user']) && !empty($options['user']))
		{
		   $optionSql .= " AND D.responsibility_owner = '".$options['user']."' "; 
		}
		if(isset($options['occuranceid']) && !empty($options['occuranceid']))
		{
		  $optionSql   .= " AND D.event_occuranceid = '".$options['occuranceid']."' ";
		}
		if(isset($options['legislation_id']) && !empty($options['legislation_id']))
		{
		  $optionSql   .= " AND D.legislation  = '".$options['legislation_id']."' ";
		}
		$optionSql .= " AND D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." "; 
		
		$data          = array();
		$totalProgress = array();		
		$actionObj     = new Action(); 
		//$deliverables  = $this->deliverableObj->fetchDeliverableBySubEvent_Id( $options['event'] );
		$deliverables = $this->deliverableObj->fetchAll($start, $limit,$optionSql);
		$deliverablesCount = $this->deliverableObj->totalDeliverables($optionSql);
		$deliverableList = array();
		$deliverablesArr = array();
		$data             = $this->sortDeliverables($deliverables, new DeliverableNaming("new"));
		$allDeliverables  = $this->deliverableObj->fetchAll("", "", "");				
		foreach($allDeliverables as $index => $del)
		{
			$data['deliverablesId'][$del['ref']] = array("id" => $del['ref'], "status" => $del['status']);
		}		
		//$totalDel      = $this->deliverableObj->totalDeliverables($optionssql);
		$data['total'] = $deliverablesCount['total'];
		$data['deliverableActionProgress'] = array();
		$data['totalProgress'] = array();	
		return $data;
      }

      
      /*
       activate the deliverables after a subevents has occured and event date has been set
       this only applies to deliverables that have been checked 
     */
      function activateDeliverable($data = array() )
      {  
         $countActivated = 0;
         $countDeactivated = 0;
         if(isset($data['checked']) && !empty($data['checked']))
         {
            foreach($data['checked'] as $index => $checkedVal)
            {
               $id  = substr($checkedVal, 17);  
               $updatedata['status'] = 0;
               $del = $this->deliverableObj->fetch($id);
               $updatedata['status'] =  $del['status'] + Deliverable::ACTIVATED_BYSUBEVENT;
               $this->deliverableObj->setTablename("deliverable");
               $res      = $this->deliverableObj->update($id, $updatedata, new DeliverableAuditLog());
               $countActivated++;
            }
         }
         if(isset($data['unchecked']) && !empty($data['unchecked']))
         {
         	foreach($data['unchecked'] as $un => $unchecked)
         	{
         		if(!empty($unchecked))
         		{
         			$countDeactivated++;
         		}         		
         	}
         }
         if(($countActivated > 0) || ($countDeactivated > 0))
         {     
           $eventOccuranceObj = new EventOccurance();
           $eventRes = $eventOccuranceObj -> updateEventOccurance(array("occuranceid" => $data['eventoccuranceid'], "status" => EventOccurance::EVENT_ACTIVATED));
         }
                  
         $text  = "";     
         $text .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$countActivated." deliverables have been activated and updated, and are available under manage <br />";
         $text .=  $countDeactivated." deliverables have not been activated <br />";
         return array("text" => $text, "error" => false);           
      }
     
      //
      function getDeliverableEvent( $options = array())
      {
      	$results = "";
      	//get all the deliverable for each of the legislation where user is admin or authorizor 
      	foreach( $options as $legislationId )
      	{
      		$results[$legislationId] = $this->deliverableObj->fetchDeliverableSubEvent( $legislationId );
      	}
      	//load the subevents that are available for triggering for these deliverable where the user is the legislation owner or authorizor

      	$subeventsAvailable = array();
		if(!empty($results))
		{
			foreach($results as $legislationArr)
			{
				foreach($legislationArr as $index => $deliverable)
				{
					if(!empty($deliverable['subeventid']))
					{
						$subeventsAvailable[$deliverable['subeventid']] = $deliverable['subeventid'];
					}
				}
			}
		}
	     return $subeventsAvailable;			
      }
      
      /*
       copy deliverables triggered by sub events and save them 
     */
      function copyBySubEvent( $occuranceId, $subeventId )
      {
      	$actionObj = new Action();
      	$deliverableSubEvents = $this->deliverableObj->fetchDeliverableBySubEvent_Id( $subeventId );

      	$newdeliverableId = array();
      	if(!empty($deliverableSubEvents))
      	{
			foreach($deliverableSubEvents as $index => $val)
			{
				if(!empty($val))
				{
					//check if this deliverable was supposed to be activated/triggered by event occurance
					if(($val['status'] & Deliverable::EVENT_ACTIVATED) == Deliverable::EVENT_ACTIVATED) 
					{
						$val['deliverable_reference'] = $val['id'];					
						unset($val['id']);
						unset($val['subeventid']);
						unset($val['ref']);
						unset($val['insertuser']);
						unset($val['insertdate']);
						$val['event_occuranceid'] = $occuranceId;
						$val['status'] = $val['status'] & ~Deliverable::EVENT_ACTIVATED;
						$val['status'] = $val['status'] + Deliverable::CREATED_SUBEVENT;
						$this->deliverableObj->setTablename("deliverable");
						$res = $this->deliverableObj->save($val);
						if($res > 0)
						{
							$this->deliverableObj->copySaveRelationship("accountableperson_deliverable", $res, $val['deliverable_reference']);
							$this->deliverableObj->copySaveRelationship("subevent_deliverable", $res, $val['deliverable_reference']);
							$this->deliverableObj->copySaveRelationship("complianceto_deliverable", $res, $val['deliverable_reference']);
							
							$actionObj->copyActionBySubEvent($res, $val['deliverable_reference']);
						}						
						$newdeliverableId[$subeventId][] = $res;
				  }
			   }
			}      	
      	}      
      	return $newdeliverableId;	
      }      
      
      function getLegislationDeliverables($options = array())
      {
         $deliverbles  = $this -> deliverableObj -> fetchAllDeliverables($options['legislationid']);
         return $deliverbles;
      }
      
}
?>