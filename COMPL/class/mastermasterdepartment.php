<?php
/*
 Master departmnents from the master compliance table
*/
class MasterMasterDepartment extends Model
{
	protected static $tablename = "dir";
	
	function __construct()
	{
		parent::__construct();
	}

	function fetch( $id )
	{
		$result = $this -> db2 -> getRow("SELECT id, name ,status ,insertdate, insertuser FROM #_dir WHERE id = $id ");
		return $result;
	}
	
	function fetchAll()
	{
		$results = $this -> db2 -> get("SELECT id, name, status, insertdate, insertuser FROM #_dir WHERE status & 2 <> 2");
		return $results;
	}

	
}
?>