<?php
class LegislationAuditLog implements Auditlog
{

    private $logtype = "";

    function processChanges($obj, $postData, $id, Naming $naming)
    {
        $legislation  = $obj -> findById($id);
        $tablename    = $obj -> getTablename(); 
        $changes      = array();
        //debug($postData);
        //debug($legislation);
        foreach($postData as $key => $postValue)
        {
            if($key === "multiple")
            {    
               foreach($postValue as $mKey => $mValue)
               {
                  if($mKey == "category")
                  {
                    $categoryChanges = $this -> _categoryChange($mValue, $legislation);
                    if(!empty($categoryChanges))
                    {
                        $changes['legislation_category'] = $categoryChanges;
                    }
                  } else if($mKey == "organisation_size") {
                    $organisationSizeChanges      = $this -> _organisationSizeChange($mValue, $legislation);
                    if(!empty($organisationSizeChanges))
                    {
                        $changes['organisation_size'] = $organisationSizeChanges;
                    }                         
                  } else if($mKey == "organisation_type") {
                    $organisationTypeChanges = $this -> _organisationTypeChange($mValue, $legislation);
                    if(!empty($organisationTypeChanges))
                    {
                       $changes['organisation_type'] = $organisationTypeChanges;
                    }     
                  } else if($mKey == "organisation_capacity") {
                    $organisationCapacityChange = $this -> _organisationCapacityChange($mValue, $legislation);
                    if(!empty($organisationCapacityChange))
                    {
                       $changes['organisation_capacity'] = $organisationCapacityChange;
                    }
                  }
               }
            } else {
                 if(isset($legislation[$key]))
                 {
                    $from = $to = "";
                    if($postValue != $legislation[$key])
                    {
                       if($key == "type")
                       {
                          $typeChanges = $this -> _legislationTypeChange($postValue, $legislation);
                          if(!empty($typeChanges))
                          {
                             $changes['legislation_type'] = $typeChanges; 
                          }
                       } elseif($key == "status") {
                          $statusChange  = $this -> _legislationStatusChange($postValue, $legislation);
                          if(!empty($statusChange))
                          {
                             $changes['legislation_status'] = $statusChange;
                          }
                       } elseif($key == "legislation_status") {
                          $changes['legislation_status'] = $this -> _getStatusChange($legislation[$key], $postData[$key]);
                       //} elseif($key == "reminder") {
                          /*echo "Process legislation reminder changes to ";
                          print "<pre>";
                            print_r($postValue);
                            print_r($legislation[$key]);
                          print "</pre>";               */
                       } else {
                         $from 	           = $legislation[$key];
                         $to 		       = $postValue;
                         $updateData[$key] = $postValue;						
                         $changes[$key]    = array("from" => $from, "to" => $to);				
                         //$changeMessage   .= $naming -> setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";
                       }
                    }
                 }
            }   
        }
/*
        print "<pre>";
         print_r($changes);
        print "</pre>";
        exit();
*/	
        $result = "";
        if(!empty($changes))
        {
            if(empty($this -> currentstatus))
            {
               $statusObj                = new Legislationstatus();
               $status                   = $statusObj -> fetch($legislation['status']);                
               $changes['currentstatus'] = $status['name'];
            } else {
               $changes['currentstatus'] = $this -> currentstatus;
            }
	        $changes['user'] = $_SESSION['tkn'];
	        $changeMessage 	 = $_SESSION['tkn']." has made the following changes \r\n";		
	        if(isset($_POST['data']['response']))
	        {
		        $changes['response']          = $_POST['data']['response'];
		        $obj -> setTablename( $tablename."_update");
		        $insertdata['response']       = $_POST['data']['response'];
		        $insertdata['changes']        = base64_encode(serialize($changes));
		        $insertdata['legislation_id'] = $id;
		        $insertdata['insertuser']     = $_SESSION['tid'];
	        } else {
		        $obj -> setTablename($tablename."_edit");
		        $insertdata['changes']        = base64_encode(serialize($changes));
		        $insertdata['legislation_id'] = $id;
		        $insertdata['insertuser']     = $_SESSION['tid'];
    	        //$insertdata['type']           = $this -> logtype;
	        }
	        $result             = $obj -> save($insertdata);	
	        $obj -> setTablename($tablename);
        }
        return $result;
     }
     

    function _categoryChange($toValues, $legislation)
    {
        $legcatObj  = new LegislativeCategoriesManager();
        $fromValues = $legcatObj -> getLegislationCategories($legislation['id']);
        $toValues   = explode(",", $toValues);
        
        $categoryObj= new LegislationCategory();
        $categories = $categoryObj -> getAll(); 						
        $from       = "";
        $to         = "";		
        $changes    = array();
        foreach($categories as $index => $val)
        {
            if(array_key_exists($index, $fromValues))
            {
                $from .= $val['name'].",";
            }
            if( in_array($index, $toValues))
            {
                $to  .= $val['name'].","; 
            }
        }
        $from = rtrim($from, ',');
        $to   = rtrim($to, ',');
        if($from !== $to )
        {                
            $legcatObj -> updateCategories($toValues, $legislation['id']);
            $changes        = array("from" => $from, "to" => $to);
            //$changeMessage .= $naming->setHeader('legislation_category')." has changed to ".$to." from ".$from."\r\n\n";
        }    
       return $changes;
    }
    
    function _organisationSizeChange($toValues, $legislation)
    {
        $orgsizeObj = new OrganisationSizeManager();
        $fromValues = $orgsizeObj -> getOrganisationSizes($legislation['id']);
        $toValues   = explode(",", $toValues);
        
        
        $orgsizeObj = new OrganisationSize();
        $sizes      = $orgsizeObj -> getAll();
        $from       = $to = "";
        $changes    = array();
        foreach($sizes as $index => $size)
        {
            if(array_key_exists($index, $fromValues))
            {
                $from .= $size['name'].","; 
            }
            if(in_array($index, $toValues))
            {
                $to  .= $size['name'].",";
            }
        }							
        $from = rtrim($from, ',');
        $to   = rtrim($to , ',');
        if($from !== $to)
        {
            $orgsizeObj -> updateOrganisationalSizes($toValues, $legislation['id']);		
            $changes = array("from" => $from, "to" => $to);
            //$changeMessage  .= $naming -> setHeader('legisaltion_organisation_size')." has changed to ".$to." from ".$from."\r\n\n";	
        }	
       return $changes;   
    }

    function _organisationTypeChange($toValues, $legislation)
    {
        $orgtypeObj  = new LegislationOrganisationTypeManager();
        $fromValues  = $orgtypeObj -> getLegOrganisationTypes($legislation['id']);
        $toValues    = explode(",", $toValues);
        
        $orgtypeObj  = new LegislationOrganisationType();
        $orgtypes    = $orgtypeObj -> getAll();
        $from        = $to = ""; 
        $changes     = array();
        foreach($orgtypes as $index => $vals)
        {
            if(array_key_exists($index, $fromValues))
            {
               $from .= $vals['name'].",";
            }
            if(in_array($index, $toValues))
            {
               $to  .= $vals['name'].",";
            }
        }	
        $from = rtrim($from, ',');
        $to   = rtrim($to , ',');
        if($from !== $to)
        {
           $orgtypeObj -> updateOrganisationalTypes($toValues, $legislation['id']);
           $changes = array("from" => $from, "to" => $to);
           //$changeMessage  .= $naming->setHeader('legislation_organisation_type')." has changed to ".$to." from ".$from."\r\n\n";	
        }
       return $changes;    
    }      

    function _organisationCapacityChange($toValues, $legislation)
    {
        $capacityObj    = new OrganisationCapacityManager();
        $fromValues     = $capacityObj -> getOrganisationCapacity($legislation['id']);						
        $toValues       = explode(",", $toValues);
        
        $orgcapacityObj = new OrganisationCapacity();
        $capacities     = $orgcapacityObj -> getAll();					

        $from          = $to = "";
        $changes       = array(); 
        foreach($capacities as $index => $capacity)
        {
          if(array_key_exists($index, $fromValues))
          { 
             $from .= $capacity['name'].",";
          }
          if(in_array($index, $toValues))
          {
             $to  .= $capacity['name'].","; 
          }	
        }					
        $from = rtrim($from,',');
        $to   = rtrim($to,',');
        if($to !== $from)
        {
          $capacityObj -> updateOrganisationalCapacities($toValues, $legislation['id']);
          $changes = array("from" => $from, "to" => $to);
          //$changeMessage .= $naming -> setHeader('legislation_organisation_capacity')." has changed to ".$to." from ".$from."\r\n\n";
        }
       return $changes;    
    }
      
    function _getStatusChange($fromStatus, $toStatus)
    {    
       $statusTo    = abs($fromStatus - $toStatus);              
       $changes_str = "";
       /*if the status to is less than the status from , then the a status has been removed from the deliverable
       */
       if($toStatus == 2)
       {
         $changes_str  = "Deliverable has been deleted";
       } elseif(($statusTo & Legislation::ACTIVATED) == Legislation::ACTIVATED) {
          $changes_str     = "Legislation has been activated";
          $this -> logtype = "activation";
       } elseif(($statusTo & Legislation::CONFIRMED) == Legislation::CONFIRMED) { 
         $changes_str  = "Legislation has been confirmed";
         $this -> logtype = "confirmation";
       } elseif(($statusTo & Legislation::DEACTIVATED) == Legislation::DEACTIVATED) { 
         $changes_str  = "Legislation has been deactivated";
         $this -> logtype = "deactivation";
       } elseif(($statusTo & Legislation::IMPORTED) == Legislation::IMPORTED) {
         $changes_str  = "Legislation has been imported <br />";
         $this -> logtype = "importing";
       }
       return $changes_str;            
   }
   
   function _legislationTypeChange($toValue, $legislation)
   {
       $typeObj = new LegislationType();
       $types   = $typeObj -> getAll(); 
       $changes = array();
       foreach($types as $index => $type)
       {
          if($legislation['type'] == $type['id'])
          {
             $from = $type['name'];
          }
          if($toValue == $type['id'])
          {
             $to  = $type['name'];
          }
       }
       $changes  = array("from" => $from, "to" => $to);
       //$changeMessage .= $naming -> setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";   
       return $changes;
   }
   
   function _legislationStatusChange($toValue, $legislation)
   {
      $changes                = array();
      $statusObj              = new Legislationstatus();
      $fromStatus             = $statusObj -> fetch($legislation['status']);
      $toStatus               = $statusObj -> fetch($toValue);
      $this -> currentstatus  = $toStatus['name']; 
      $changes                = array("from" => $fromStatus['name'], "to" => $toStatus['name'] );
      return $changes;
   }

}
