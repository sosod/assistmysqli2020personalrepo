<?php
class Form
{

	function renderLabel($for, $value= "")
	{
		$value = utf8_decode(html_entity_decode($value));
		return "<label for='".$for."'>".$value."</label>";
	}

	function renderText($name, $value = "")
	{
		$value = utf8_decode(html_entity_decode($value));
		return "<input type='text' name='".$name."' id='".$name."' value='".$value."' />";
	}
	
	function renderHidden($name, $value = "")
	{
		$value = utf8_decode(html_entity_decode($value));
		return "<input type='hidden' name='".$name."' id='".$name."' value='".$value."' />";
	}	
	
	function renderDateField($name, $value = "")
	{
	  $value = utf8_decode(html_entity_decode($value));
	  return "<input type='text' name='".$name."' id='".$name."' value='".$value."' class='datepicker' readonly='readonly' />";
	}
	
	function renderTextarea($name, $value = "")
	{
		$value = utf8_decode(html_entity_decode($value));
		return "<textarea name='".$name."' id='".$name."' cols='70' rows='6'>".$value."</textarea>";
	}
	
	function renderSelect($name, $options, $value = "" )
	{
		$select = "<select name='".$name."' id='".$name."' >";
		$select .= "<option value=''>--please select--</option>";
		if(isset($options) && !empty($options))
		{
			foreach($options as $index => $val )
			{
				$select .= "<option value='".$index."'";
					if($value == $index)
					{
						$select .= "selected='selected'";
					}
				$select .= ">".$val."</option>";
			}
		}
		$select .= "</select>";
		return $select;
	}
	
	function renderMultipleSelect($name, $options, $value = array())
	{
		$select = "<select name='".$name."' id='".$name."' multiple='multiple'>";
		if(isset($options) && !empty($options))
		{
			foreach($options as $index => $val )
			{
				$select .= "<option value='".$index."'";
				if(!empty($value))
				{
					if(isset($value[$index]))
					{
					  $select .= "selected='selected'"; 
					}				
				}
				$select .= ">".$val."</option>";
			}
		}
		$select .= "</select>";
		return $select;
	}

	function renderCheckbox( $name, $value = FALSE)
	{
		$value = utf8_decode(html_entity_decode($value));
		$checkbox = "<input type='checkbox' name=".$name." id=".$name." value=".$name."";
		if($value == TRUE)
		{
			$checkbox .= "checked='checked'";
		} 
		$checkbox .= "/>";
		return $checkbox;
	}			
	
	function renderSubmit( $name, $value)
	{
		return "<input type='submit' name='".$name."' id='".$name."' value='".$value."' />";
	}

	function renderButton($name, $value)
	{
		return "<input type='button' name='".$name."' id='".$name."' value='".$value."' />";
	}

} 
