<?php
/*
 * Defines how the legislation structure at the manage level
 */
class ManageLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct("manage");
	}
	
	function getOptionSql($options = array())
	{
	   $option_sql  = array();
	   $active_sql  = " L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
	   $client_sql  = "";
       $client_sql .= " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";			
       $client_sql .= " AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." ";			
	   if(isset($options['page']) && $options['page'] === "copy")
	   {
		 $client_sql .= " AND (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
						  AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
						  OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
				     ";		
	   }	   
       if(isset($options['page']) && $options['page'] === "edit")
       {
         $client_sql .= " AND L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." ";
       }
       if(isset($options['legislation_id']) && !empty($options['legislation_id']))
       {
          $client_sql .= " AND L.id = '".$options['legislation_id']."' ";
       }       
       //if we are viewing action under manage, then show legislations which have deliverable that the user logged is the 
       //responsible person or accountable person
       if((isset($options['type']) && $options['type'] == "action") && $options['page'] == "edit")
       {
           $accountableObj = new AccountablePersonMatch(); 
           $user_sql       = " AND APU.user_id = '".$_SESSION['tid']."' ";
           $accountables   = $accountableObj -> getUserAccountableDeliverable($user_sql);
           $acc_sql        = "";
           if(!empty($accountables))
           {
              foreach($accountables as $index => $val)
              {
                 $acc_sql .= " OR D.id = '".$val['deliverable_id']."' ";
              }   
           }                  
           $del_sql         = "";
           $del_sql        .= " AND (D.responsibility_owner = '".$_SESSION['tid']."' ".$acc_sql.") 
                                AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."  
                                GROUP BY D.legislation ";
           $deliverablesObj = new ClientDeliverable();
           $deliverables    = $deliverablesObj  -> fetchAll($del_sql);
           $deliverable_sql = "";
           foreach($deliverables as $d_index => $deliverable)
           {
              $deliverable_sql .= " L.id = ".$deliverable['legislation']." OR";
           }
           $client_sql .= " OR (".rtrim($deliverable_sql, 'OR').") ";
       } 	  
           
       //    
	   if(isset($options['viewType']) && $options['viewType'] == "mine") 
	   {
	      $adminObj          = new LegislationAdmin();
	      $userlegislations  = $adminObj -> getUserLegislations($_SESSION['tid']);
	      if(!empty($userlegislations))
	      {
	         $client_sql .= " AND ( ";
	         $legSql     = "";
	         foreach($userlegislations as $index => $legislationId)
	         {
	            $legSql .= "  L.id = '".$legislationId."' OR";   
	         }
             $client_sql .= rtrim($legSql, " OR");
	         $client_sql .= " )";
	      }
	      
	      //get legislations custom created and the user who created them;
	      //$adminObj          = new ClientLegislation();
	      //$userlegislations  = $adminObj -> getCustomCreatedUserLegislations($options);
	      
	   }
        
        //if the copying to anothere year , check if we haven't aleardy copied some legislation to the same year , if so then remove them 
        if(isset($options['tofinancialyear']) && !empty($options['tofinancialyear']))
        {        
          $finObj      = new FinancialYear();
          $legislation = $finObj -> getLegislationFinancilaYear($options['financial_year'], $options['tofinancialyear']);
          if(!empty($legislation))
          {
            foreach($legislation as $index => $legFin)
            {
              $client_sql .= " AND L.id <> ".$legFin['legislation_id']." ";
            }
          }
        }
        if($options['section'] == "manage")
        {
           /*$client_sql = " AND ((".$active_sql." ". $client_sql.") 
                           OR (L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." 
                           AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."))";*/
           $client_sql = " AND ((".$active_sql." ". $client_sql.") 
                           OR (L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."))";
        } else {
            $client_sql = "AND ".$active_sql." ". $client_sql; 
        }
        if(isset($options['financial_year']))
        {
          $client_sql = $client_sql." AND L.financial_year = '".$options['financial_year']."' ";
        }
        //echo $clientSql."\r\n\n";
        if(isset($options['limit']) && !empty($options['limit']))
        {
          $client_sql .= " LIMIT ".$options['start'].",".$options['limit']; 
        }
        
        
        $option_sql['client'] = $client_sql;  
        $option_sql['master'] = "";
        return $option_sql;
	}
	/*function getLegislations( $start, $limit, $options = array())
	{
          $optionSql = "";
		$optionSql = " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."";			
		if($options['viewType'] == "mine") 
		{
		   $optionSql .= " AND FIND_IN_SET('".$_SESSION['tid']."', L.admin)";
		}
		$myLegislations = $this->legislationObj->fetchLegislations( $start, $limit, $optionSql);

		$data = array();
		$data = $this->sortLegislation($myLegislations, new LegislationNaming($options['page'])); 
			
		/*if(isset($data['headers']['action_progress']) )
		{
			$legislation = self::calculateLegislationProgress($data['legislations']); 
			$data['data'] = $legislation;			
		}
		$users = $this -> getUsers();
		$totalLeg = $this->legislationObj->getTotallegislations($optionSql);
		$data['users'] = $users;  
		$data['usedId'] = array();
		$data['total'] = $totalLeg['totalleg'];
		$data['currentUser'] = $_SESSION['tid'];		
		return $data;	
	}
				
	function importLegislation($start, $limit, $options = "")
	{
		$optionsSql    = " AND L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."";
		$legislations  = $this->legislationObj->importLegislation( "", "" ); 
		$myLegislations= $this->legislationObj->fetchLegislations( $start, $limit, $optionsSql );
	
		$_legislations = array();
		$status        = array();
		$usedId 	   = array();
		$authorizor    = "";
		$admin         = "";
		$data          = array();
		foreach( $legislations as $index => $legData)
		{
			foreach( $myLegislations as $k => $valArr ){
				if( $valArr['legislation_reference'] == $legData['ref'] )
				{
				//($valArr['legislation_status'] & 1)." and ".$valArr['legislation_status']."\r\n\n";
				if(($valArr['legislation_status'] & 1) == 1)
				{
				  $_legislations[$k] = $legData;
				}
				$admin 	  = (string)$valArr['admin'];
				$authorizor = (string)$valArr['authorizer'];
				$data['usedId'][$legData['ref']]     = $valArr['legislation_reference'];
				$data['status'][$legData['ref']]     = $valArr['legislation_status'];
				$data['admin'][$legData['ref']]      = explode(',', $admin);
				$data['authorizor'][$legData['ref']] = explode(',', $authorizor);			
				}		
			}
		}	
			
		$data = array_merge($this->sort( $_legislations, new LegislationNaming(), new ComplianceManager()), $data);
		$data['company']= ucfirst($_SESSION['cc']);
		$data['user']   = $_SESSION['tkn'];
		$data['users']  = $this->getUsers();
		$total          = $this->legislationObj->getTotallegislations( $optionsSql );
		$data['total']  = $total['totalleg'];
		$data['columns']= count($data['headers']);		
		return $data;	
	}
		
	function importSave( $legislation_id, $financial_year )
	{
		$leg 	    = $this->legislationObj->fetchALegislationByRef( $legislation_id );	
		$aLegislation = $this->legislationObj->importALegislation( $legislation_id );	

		$response 	 = array();
		$delResponse = array();
		if(!empty($aLegislation))
		{
			$aLegislation['legislation_status']    = $leg['legislation_status'] + Legislation::IMPORTED;
			$legislationRef = $aLegislation['id'];  
			$aLegislation['financial_year'] = $financial_year;  
			unset($aLegislation['id']);
			$this->legislationObj->setTablename("legislation");
			if( $response  = $this->legislationObj->update( $leg['id'], $aLegislation, new LegislationAuditLog() ) > 0) {
				//get the latest version of the version , if there has been updates then log the version of the update in the legislation usage
				$currnetlegVersion 	= $this->legislationObj->getLatestLegislationVersion( $legislationRef );
				if(!empty($currnetlegVersion)){
					$usage = array("legislation_id" => $legislationRef, "current_version" => $currnetlegVersion['id'], "version_in_use" => $currnetlegVersion['id'] );
				} else {
					$usage = array("legislation_id" => $legislationRef, "current_version" => 0, "version_in_use" => 0 );
				}
				//import and save the relation tables 
				//import the orgcapacity legislation tables
				$this->legislationObj->importSaveRelationship($legislationRef, $leg['id'], "orgcapacity_legislation");
				$this->legislationObj->importSaveRelationship($legislationRef, $leg['id'], "orgtype_legislation");
				$this->legislationObj->importSaveRelationship($legislationRef, $leg['id'], "orgsize_legislation");
				$this->legislationObj->importSaveRelationship($legislationRef, $leg['id'], "category_legislation");
				$this->legislationObj->setTablename("legislation_usage");
				$this->legislationObj->save($usage);
				if( ($leg['legislation_status'] & Legislation::IMPORT_DEL )  == Legislation::IMPORT_DEL ){
					//if the setting allows us to import the deliverables , the lets import them here
					$delObj 		= new NewDeliverable();
					$delResponse 	= $delObj -> importDeliverables( $leg['legislation_reference'] ,  $leg['id']) ;
				}				
			
				$text  = ""; 
				if(!empty($delResponse)){
					
					$text .= $delResponse['imported']."  deliverables  were successfully imported <br />";
					if( $delResponse['actionSaved'] != 0){
						$text .= " &nbsp;&nbsp;&nbsp;&nbsp;  ".$delResponse['actionSaved']." actions were successfully imported ";
					}
					if( $delResponse['actionNotSaved'] != 0){
						$text .= "&nbsp;&nbsp;&nbsp;&nbsp; ".$delResponse['actionNotSaved']." actions were not successfully imported ";
					}					
					if( $delResponse['notImported'] != 0){
						$text .= $delResponse['notImported']." deliverables  were not successfully imported ";
					}
				} else {
					$text .= " There were no deliverables for this legislation or you did not allow for importation of deliverables";
				}				
				$response = array("text" => "Legislation imported successfully . . <br /> &nbsp;&nbsp;&nbsp;&nbsp; ".$text, "error" => false);
			} else {
				$response = array("text" => "Error importing the legislation. . ", "error" => false);
			}
		}
		return $response;
	}
	
	
	function importSaveAll( $legislationData ){
		
		$legislations = array();
		foreach( $legislationData  as $index => $legData)
		{
			//extracting the legislation id and the name of the status from a string
			$dataString = strrev( $legData['name'] );
			preg_match('/\d{1,9}/', $legData['name'], $matches);
			$id 		=  $matches[0];	
			$key 		= str_replace( "_".$id, "" ,$legData['name'] );   
			$legislations[$id][$key] =  $legData['value'];
		}
		$response = $this->saveImports($legislations);
		return $response;
		//retrieve and save	
	}

	function saveImports( $legislations )
	{
		$legislation  = new Legislation();
		$avilableLegs = $legislation->fetchAllAvailable();

		$legs 		  = array();
		$countSave    = 0; 
		$countError   = 0;
		$delResponse  = array();
		foreach( $legislations as $id => $leg){			
			foreach($avilableLegs as $index => $l){
				if( $id == $l['id']){
					$status = $this->calculateStatus($leg['available'], $leg['import_del'], $leg['edit_del'], $leg['create_del'], $leg['import_actions'], $leg['edit_actions'], $leg['create_actions'] );
					$l['legislation_status']    = $status;
					$l['legislation_reference'] = $l['id']; 
					unset( $l['id'] ); 
					$legislation->setTablename("legislation");
					if( $response  = $legislation->save($l) > 0 ){
						$countSave++;
					} else {
						$countError++;
					}
				}
			}
		}	
		$response = array();
		if( $countSave == 0 ){
			$response = array("error" => true, "text" => "An error occured importing and saving the legislations" );
		} else {
			$response = array("error" => false, "text" => $countSave." legislations successfully imported ".($countError == 0 ? "" : $countError." were not successfully saved") );
		}
		return $response;
	}
	
	function saveLegislation( $data )
	{
		$insertdata = array();
      $multiple = array("category"              => array("name" => "category_legislation", "key" => "category_id"), 
                        "organisation_size"     => array("name" => "orgsize_legislation", "key" => "orgsize_id"),
                        "organisation_capacity" => array("name" => "orgcapacity_legislation", "key" => "orgcapacity_id"),
                        "organisation_type"     => array("name" => "orgtype_legislation", "key" => "orgtype_id")
                       );
      $multipledata  = array();              
		foreach( $data as $key => $value)
		{
		
         if(array_key_exists($key, $multiple))
         {
         	if(is_array($value) && !empty($value)){
             	$multipledata[$key] = implode(",",$value);
         	}
         } else{
             $insertdata[$key] = $value;
         }
		}
		$insertdata['legislation_status']	 = Legislation::CREATED; 
		$insertdata['authorizer'] 				 = $_SESSION['tid'];
		$insertdata['admin'] 					 = $_SESSION['tid'];
	
		$this->legislationObj->setTablename("legislation");
		$response = array();
		if(  ($res = $this->legislationObj->save($insertdata )) > 0)
		{
		   if(isset($multipledata) && !empty($multipledata))
		   {
		      foreach($multipledata as $key => $valArr)
		      {
		         if( isset($multiple[$key]) && !empty($multiple[$key]))
		         {
		            $values = explode(",", $valArr);
		            {
		               foreach($values as $i => $val)
		               {
		               	$this->legislationObj->setTablename($multiple[$key]['name']);
		                  $r = $this->legislationObj->save(array($multiple[$key]['key'] => $val, 'legislation_id' => $res));
		                  $r = $r++;
		               }
		            }
		         }
		      }  
		   }		
			$response = array("text" => "Legislation saved", "error" => false, "id" => $res );
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true);
		}
		return $response;
	}
	
	function confirmLegislation( $data )
	{
		$legislationData = $this->legislationObj-> fetch($data['id']);
		$legislationData['legislation_status'] = $legislationData['legislation_status'] + Legislation::CONFIRMED;
		$updatedata = array("legislation_status" => $legislationData['legislation_status'] );

		$this->legislationObj->setTablename("legislation");
		$res = $this->legislationObj->update($data['id'], $updatedata, new LegislationAuditLog());
		$response = array();
		if( $res == 1) {
			$activated = array();
			$inactivated = array();	
			$ids 			 = array();
			if(isset($data['activated']))
			{
				$activated = $data['activated'];
				$inactivated = $data['deactivated'];
				$ids			 = $data['ids'];
				foreach($inactivated as $index => $valId)
				{
					if(in_array($valId, $activated))
					{
						unset($inactivated[$index]);
					}
				}
				$deliverableObj = new Deliverable();			
				foreach($ids as $aIndex => $aVal)
				{
					if(!in_array($aVal, $inactivated))
					{
						$deliverable     		 = $deliverableObj->fetch($aVal); 
						$delupdatedata['status'] = $deliverable['status'] + Deliverable::CONFIRMED;
						$deliverableObj->setTablename("deliverable");
						$res = $deliverableObj -> update($aVal, $delupdatedata, new DeliverableAuditLog());					
					} 
				}
			}
			$response = array("text" => "Legislation confirmed . . .", "error" => false, "updated" => true);
		} else if( $res == 0){
			$response = array("text" => "No changes made to the legislation . . .", "error" => false, "updated" => false);
		} else {
			$response = array("text" => "Error confirming the legislation. . .", "error" => true, "updated" => false);
		}
		return $response;
	}	
		
	function editLegislation( $data )
	{
		$id 	= $data['id'];
		unset($data['id']);		
			
		$data = $this->prepareLegislation($data);
		$this->legislationObj->setTablename("legislation");
		$res 	  = $this->legislationObj->update($id, $data, new LegislationAuditLog(), $this->legislationObj, new LegislationNaming());
		$response = array();
		if(  $res > 0 )
		{
			$response = array("text" => "Legislation updated", "error" => false, "id" => $res, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
	} 	 
	
	function prepareLegislation( $data )
	{
		$insertdata  = array();
	   $multiple    = array("category", "organisation_size", "organisation_capacity", "organisation_type");
		foreach( $data as $key => $value)
		{
            if(in_array($key, $multiple))
            {
                $insertdata['multiple'][$key] = implode(",",$value);
            } else{
                $insertdata[$key] = $value;
            }
		}
		return $insertdata;		
	}
	
	function updateLegislation( $data )
	{
		$id 				= $data['id'];
		$updatedata['status'] = $data['status'];
		unset($data['id']);		
		$legObj 	= new Legislation();
		$legObj->setTablename("legislation");
		$res 	  = $legObj->update($id, $updatedata, new LegislationAuditLog(), $legObj, new LegislationNaming());
		$response = array();
		if(  $res > 0)
		{
			$response = array("text" => "Legislation updated", "error" => false, "id" => $res, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function updateEditedLegislation( $data )
	{
		$legObj 		   = new Legislation();
		$legislation	   = $legObj -> fetch( $data['id'] );
		$editedLegislation = $legObj -> importALegislation( $legislation['legislation_reference'] );
		$latestVersion 	   = $legObj -> getLatestLegislationVersion( $legislation['legislation_reference'] );
		$currentVersion    = $legObj -> getCurrentLegislationVersion( $data['id'] );
		$legObj->setTablename("legislation");
		$res 	  = $legObj->update($data['id'], $editedLegislation, new LegislationAuditLog(), $legObj, new LegislationNaming());
		$response = array();
		if(  $res > 0)
		{
			$updatedversion = array("current_version" => $latestVersion['id'], "version_in_use" => $latestVersion['id'] );
			$legObj->setTablename("legislation_usage");
			$text  = "";
			if( $legObj -> update($currentVersion['id'], $updatedversion, new LegislationAuditLog()) > 0 ){
				$text = "Legislation updated to the current version";
			} else {
				$text = "";
			}
			$response = array("text" => ($text == "" ? "Legislation updated" : $text), "error" => false, "id" => $res, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
		
	}
	
	function guidanceData( $legislationId )
	{
		$legObj 	 = new Legislation();
		$legislation = $legObj ->fetch( $legislationId );
		$respoOrg    = new ResponsibleOrganisations();
		$responsible = $respoOrg -> getResponsibleOrganisations();
		$org = ""; 
		
		foreach( $responsible as $index => $val)
		{
			if( $val['id'] == $legislation['business_patner'] ){
				$org = $val['name'];
			}
		}
		$guidanceInfo  = array(
								"request_by"  => $_SESSION['tkn'],
							    "request" 	  => ucfirst( $_SESSION['cc'] ),
								"bus_partner" => $org,
								"leg_owner"	  => $legislation['owner'],
								"legislation" => $legislation['legislation_name'],
		 						"email"		  => $legislation['tkemail']
		);
		return $guidanceInfo;
	}
	
	function requestGuidance( $id, $contact, $message )
	{
		$userObj = new UserAccess(); 
		$legObj  = new Legislation();
		$gData   = $this->guidanceData( $id );
		$gData['message'] = $message;
		$data    = array("setup_id" => $id, "changes" => serialize($gData), "insertuser" => $_SESSION['tid'] );
		$legObj -> setTablename("guidance_logs");
		$rs = $legObj -> save( $data );
		$response  = array();
		$userEmail = $userObj -> getUser( $_SESSION['tid'] );
		$emails   = (!empty($userEmail['email']) ? $userEmail['email']."," : "").$gData['email'].""; 
		$body  = "";
		$body .= "<b>Request By:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['request_by']."<br />";
		$body .= "<b>Requesting Company:</b>&nbsp;".$gData['request']."<br />";
		$body .= "<b>Business Partner:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['bus_partner']."<br />";
		$body .= "<b>Legislation Name:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['legislation']."<br />";
		$body .= "<b>Contact Number:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$contact."<br />";
		$body .= "<b>Message:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$message."<br />";

		EmailNotifier::$to 		= $emails;
		EmailNotifier::$subject = "Request Guidance";
		EmailNotifier::$body 	= $body; 
	
		if( EmailNotifier::notify()){
			$response = array("text" => "Request Guidance email successfull send", "error" => false);		
		} else {
			$response = array("text" => "Error sending guidance email", "error" => true);
		}					
		return $response;
	}

	function searchLegislation( $start, $limit, $options)
	{
		$legObj  	= new Legislation();
		$catObj 		= new LegislativeCategories(); 
		$orgSizeObj = new OrganisationSizes();
		$orgCapObj  = new OrganisationCapacity();
		$orgTypes   = new LegOrganisationTypes();
		$statObj 	= new LegislationStatus(); 
		
		$categoryId1  = $catObj -> searchCategories( $options ) ;
		$categoryId2  = $catObj -> searchCategories2( $options );
		$categoryIds  = $this->sortIds( $categoryId1, $categoryId2);
		
		$orgsizeIds1 = $orgSizeObj -> searchOrganisationSizes( $options );		
		$orgsizeIds2 = $orgSizeObj -> searchOrganisationSizes2( $options );
		$orgsizeIds  = $this->sortIds( $orgsizeIds1, $orgsizeIds2 ); 
		
		$capacityId1 = $orgCapObj -> searchCapacities( $options );
		$capacityId2 = $orgCapObj -> searchCapacities2( $options );
		$capacityIds = $this->sortIds( $capacityId1, $capacityId2);
		
		$typesId1 	 = $orgTypes -> searchOrganisationTypes( $options );
		$typesId2	 = $orgTypes -> searchOrganisationTypes2( $options ); 	
		$typesId 	 = $this->sortIds( $typesId1, $typesId2);  		
		
		$idsArray 	 =  array(
							  "category" 				=> $categoryIds,
							  "organisation_size" 		=> $orgsizeIds,
							  "organisation_capacity" 	=> $capacityIds,
							  "organisation_type"		=> $typesId
							 );
		$legislations = $legObj -> searchLegislation( $options,$idsArray);
		$legislations = $this->sortHeaders( $legislations, new LegislationNaming(), "manage" );
		
		if( isset($legislations['headers']['action_progress']) ){
			$legislationProgress = self::calculateLegislationProgress($legislations); 
			$legislations 		 = array_merge($legislations , $legislationProgress);
		}
		$response = array();
		if( $legislations['total'] > 1)
		{
			$response  = $legislations['total']." results found";
		} else {
			$response  = $legislations['total']." result found";
		}
		return array("usedId" => array(), "data" => $legislations, "status" => array(), "text" => $response );
	}
	
	function sortIds( $idArr1, $idArr2 )
	{
		$idArr = array_merge($idArr1, $idArr2);
		$ids = "";
		if(!empty($idArr))
		{
			foreach( $idArr as $valArr){
				$ids = $valArr['id'].",";
			}
		}	
		$ids = rtrim( $ids, ",");
		return $ids;
	}
	
	function advancedSearch( $start, $limit, $options)
	{		
		$legObj  	= new Legislation();
		$searchArray = array();
		foreach( $options as $index => $valArr)
		{
			if( strstr($valArr['name'], "_multiple"))
			{	
				$name = str_replace("_multiple", "", $valArr['name'] );
				$searchArray[$name][]= $valArr['value'];
			} else {
				$searchArray[$valArr['name']]= $valArr['value'];
			} 
		}
		$legislations = $legObj -> advancedLegislationSearch( $searchArray );
		$legislations = $this->sortHeaders( $legislations, new LegislationNaming(), "manage" );
		if( isset($legislations['headers']['action_progress']) ){
			$legislationProgress = self::calculateLegislationProgress($legislations); 
			$legislations 		 = array_merge($legislations , $legislationProgress);
		}
		$response = array();
		if( $legislations['total'] > 1)
		{
			$response  = $legislations['total']." results found";
		} else {
			$response  = $legislations['total']." result found";
		}
		return array("usedId" => array(), "data" => $legislations, "status" => array(), "text" => $response );		
	}
	
	function getUpdatedLegislations()
	{
		$legObj 		= new Legislation();
		$currentVersion = $legObj -> getCurrentVersion();
		$newUpdate 		=  array();
		foreach( $currentVersion as $index => $value)
		{		
			$latestVersion  = $legObj -> getLatestLegislationVersion( $value['legislation_id'] );
			if($latestVersion['id'] != $value['version_in_use'])
			{
				$newUpdate[] = $value['legislation_id'];
			}			
		}
		$results = array();
		if(!empty($newUpdate))
		{
			$options = " AND L.legislation_reference IN('".implode(",", $newUpdate)."')";
			$results = $this-> getLegislations( 0,10, $options, "manage");
		}
		return $results;
	}
	
	function allowUpdate( $data )
	{
		list($refId , $idRef ) = explode("_", $data['ref'] );
		$ref 	 	  	   = substr($refId, 6);
		$id 		  	   = substr($idRef, 2 );				
		$legObj 		   = new Legislation();
		$latestlegislation = $legObj -> importALegislation( $ref );
		unset($latestlegislation['status']);
		$latestversion     = $legObj -> getLatestLegislationVersion( $ref );
		if(!empty($latestlegislation)) {
			$legObj->setTablename("legislation");
			$res = $legObj -> update($id, $latestlegislation, new LegislationAuditLog());
			if( $res > 0)
			{
				$usage = array("current_version" => $latestversion['id'], "version_in_use" => $latestversion['id'] );
				$legObj->setTablename("legislation_usage");
				$legObj -> update("legislation_id={$id}", $usage, new LegislationAuditLog() );
			}				
		}
		$response = array();
		if(  $res > 0)
		{
			$response = array("text" => "Legislation updated", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the legislation", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function fetchLegislations( $args = "" )
	{
		$option = "";
		if(isset($args) && !empty($args))
		{
			if(is_array($args))
			{
				foreach($args as $key => $value)
				{
					$option .= " AND ".$key." = '".$value."' ,"; 
				}
			}
		}
		$option = rtrim($option, ",");
		$legObj = new Legislation();
		$legislation = $legObj -> fetchAll( $option );
		return $legislation;		
	}	*/	
	
}
