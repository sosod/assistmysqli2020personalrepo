<?php
class MasterLegislationOrganisationType extends LegislationOrganisationType
{
     
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options = "")
     {
		$results = $this->db2->get("SELECT * FROM #_leg_organisation_types WHERE status & 2 <> 2  $options ");
		$types = array();
		foreach($results as $index => $type)
		{
		   $types[$type['id']] = $type;
		   $types[$type['id']]['imported'] = true;
		}
		return $types;     
     }
     
     function fetch($id)
     {
         $result = $this -> db2 -> getRow("SELECT * FROM #_leg_organisation_type WHERE id = '".$id."'");
         return $result;
     }
     
     
	function search( $text )
	{
		$results = $this->db2->get("SELECT id FROM #_leg_organisation_types WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
		return $results;
	} 	

}
?>
