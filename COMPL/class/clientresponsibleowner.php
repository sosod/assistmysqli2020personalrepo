<?php
class ClientResponsibleOwner extends Model
{
     
     function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll($options="")
     {
       $results = $this->db->get("SELECT id, name, status FROM #_responsible_owner WHERE status & 2 <> 2 $options");
       return $results;
     }
     
     function fetch($id)
     {
       $result = $this->db->getRow("SELECT * FROM #_responsible_owner WHERE id = '".$id."'");
       return $result;
     }


	function savenewResponsibleOwner( $data )
	{
		$this->setTablename("responsible_owner");      	
		$res = $this->save($data);
		$response = $this->saveMessage("new_responsible_owner", $res); 
		return $response;
	}      
	

}
?>