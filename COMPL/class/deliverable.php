<?php
class Deliverable extends Model
{
	
	private $id;
		 	 	 	 	 	 	
	private $description;
			 	 	 				
	private $short_description;
			 	 	 				
	private $legislation_section;
			 	 	 	 	 	 	
	private $compliance_frequency;

	private $applicable_org_type;
			 	 	 	 	 	 	
	private $compliance_date;
			 	 	 	 	 	 	
	private $responsible;
			 	 	 	 	 	 	
	private $functional_service;
			 	 	 	 	 	 	
	private $accountable_person;
			 	 	 	 	 	 	
	private $responsibility_owner;
			 	 	 	 	 	 	
	private $legislation_deadline;
			 	 	 	 	 	 	
	private $action_deadline;
			 	 	 	 	 	 	
	private $reminder;
			 	 	 	 	 	 	
	private $sanction;
			 	 	 	 	 	 	
	private $attachment;
			 	 	 	 	 	 	
	private $reference_link;
			 	 	 	 	 	 	
	private $org_kpi_ref;
			 	 	 	 	 	 	
	private $individual_kpi_ref;
			 	 	 	 	 	 	
	private $assurance;
			 	 	 	 	 	 	
	private $compliance_to;
			 	 	 	 	 	 	
	private $main_event;
			 	 	 	 	 	 	
	private $sub_event;
			 	 	 	 	 	 	
	private $guidance;
			 	 	 	 	 	 	
	private $status;
			 	 	 	 	 	 	
	private $insertdate;	 	 	 	 	 	 	

	private $insertuser;		 	 	 	 	 	 	
	
	private $legislation;
	
	protected static $table = "deliverable";
	
	protected $multiple = array("compliance_to", "sub_event", "accountable_person");
	
	protected $multipleInsert = array();
		
	static $recurranceTypes = array();
	
	var $deliverablesProgress;
	
	var $deliverableStatus;
	
	const ACTIVE                    = 1;
	//store the status of deliverables that are using the actual users-id
	const USING_USERID              = 4;
	//store the status of deliverable that are using the user-id which was matched at the setup
     const USING_MATCHEDUSERID      = 8;
	//store the status of the deliverable that is using the department id from the client-master database
	const USING_DEPARTMENTID        = 16;
	//store the status of the deliverable that is using the functional service ID from the client database
	const USING_FUNCTIONALSERVICEID = 32;
	//store the status of deliverables whose functional service id is the match id and not the functional service id 
	const MATCH_FUNCTIONALSERVICEID = 64;
	//store the status of deliverable whose department id is the master_id not (the department id from master setup) or (actual department id)
	const MATCH_DEPARTMENTID        = 128;
	//deliverables that have bee imported from the Compliance Master have this status
	const IMPORTED                  = 256;
	//this is for deliverables that have event and need events to activate them , 
	//they are just templates these deliverables
	const EVENT_ACTIVATED           = 512;
	//the is for new deliverables/copies of deliverables created when the subevent is activated
	const CREATED_SUBEVENT          = 1024;
	//stores the status of deliverables that have been confirmed and awaiting activation 
	const CONFIRMED                 = 2048;
	//activation of the deliverable that was created using an event to make it live and be available for under manage
	const ACTIVATED_BYSUBEVENT      = 4096;
	//created under manage
	const MANAGE_CREATED            = 8192;
	//stores the status of the deliverables that have been activated and can be under manage and used in computations and reports
	const ACTIVATED                 = 16384;
	
	const USINGEVENT_CLIENTID       = 32768;
	
	const EVENT                     = 10000;
	
	const REFTAG				  	= "D";
	
	function __construct()
	{
		parent::__construct();
		$this -> multipleInsert  = array("sub_event"    => array("t" => "subevent_deliverable", "key" => "subevent_id"),
                                        "compliance_to" => array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
                                        "accountable_person"  => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
	  		);	 		
	}
	
	function getAll($options = array())
	{
	   $clientObj         = new ClientDeliverable();
	   $clientDeliverable = $clientObj -> fetchAll();
	   
	   $masterObj         = new MasterDeliverable();
	   $masterDeliverable = $masterObj -> fetchAll();
	     
	   $deliverables      = array();  
	}
	
	/*
	  Get the information for all the deliverables that fits the selected criteria and section we are in 
	*/
	public function allDeliverableInfo($deliverableObj, $optionSql)
	{
		$actionObj        = new ClientAction();
		$deliverables     = $deliverableObj -> fetchAll($optionSql);
		$deliverablesInfo = array();
		$hasActions       = TRUE;
		$t                = 0;
		$delActions       = array();
		foreach($deliverables as $dIndex => $dVal)
		{
		   $t++;
		   $sql                                       = " AND deliverable_id = '".$dVal['id']."'";
		   $actions                                   = $actionObj -> fetchAll($sql);
		   $averageActionProgress                     = 0;
		   if(!empty($actions))
		   {
		      $totalActions  = 0;
		      $totalProgress = 0;
		      foreach($actions as $a_index => $action)
		      {
		          $totalActions                          += 1;
		          $totalProgress                         += $action['progress'];
		          $delActions[$dVal['id']][$action['id']] = $action['action_status'];
		      }
		      if($totalActions != 0)
		      {
                $averageActionProgress = ASSIST_HELPER::format_percent(($totalProgress / $totalActions));		      
		      }
		   } else {
		       $hasActions =  FALSE;    
		   }
		   //$delActionInfo                             = $actionManager -> getTotalActions($dVal['id']);	
		   $this -> deliverablesProgress[$dVal['id']] = $averageActionProgress; 
		   $this -> deliverableStatus[$dVal['id']]    = $dVal['status'];			

	        $deliverablesInfo['deliverablesId'][$dVal['id']] = array( "id"      => $dVal['id'],
                                                                      "status"  => $dVal['status'],
                                                                      "owner"   => $dVal['responsibility_owner']  
                                                                     );
		}
		$deliverablesInfo['total']      = $t;
		$deliverablesInfo['hasActions'] = $hasActions;
		$deliverablesInfo['progress']   = $this -> deliverablesProgress;
		$deliverablesInfo['delActions'] = $delActions;   
		return $deliverablesInfo;		
	}
	
	
	function getDeliverableConfimation($options = array())
	{
	   $deliverableManagerObj   = DeliverableFactory::getInstance($options['section']);
	   $optionSql               = "";
	   $optionSql               = $deliverableManagerObj -> getOptions($options);
	   $clientObj               = new ClientDeliverable(); 
	   $results                 = $clientObj -> fetchAll($optionSql);
	   //Util::debug($results);
	   $optionSql               = substr($optionSql  , 0, strpos($optionSql, "LIMIT"));
       $alldeliverables         = $this -> allDeliverableInfo($clientObj, $optionSql);
	   //$total   = $clientObj -> totalDeliverables($optionSql);	   
	   $deliverables            = $deliverableManagerObj -> sortDeliverables($results, $options);
	   
	   $deliverables['isOwner'] = FALSE; 
	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
          $legislationAdminObj = new LegislationAdmin();
          $admins              = $legislationAdminObj -> getLegislationAdmins($options['legislation_id']);
          if(Legislation::isLegislationOwner($admins))
          {
             $deliverables['isOwner'] = true; 
          }
	   }
	   //$deliverables['total'] = $total['total'];
   	   $deliverables = array_merge($deliverables, $alldeliverables);
	   return $deliverables;
	}
	
	function getDeliverables($options = array())
	{
	   $deliverableManagerObj = DeliverableFactory::getInstance($options['section']);
	   $optionSql             = "";
	   $optionSql             = $deliverableManagerObj -> getOptions($options);
	   $clientObj             = new ClientDeliverable();
	   $results               = $clientObj -> fetchAll($optionSql);
	   //echo $optionSql."\r\n\n\n";
	   if(strpos($optionSql, "LIMIT") > 0)
	   {
	     $optionSql = substr($optionSql  , 0, strpos($optionSql, "LIMIT"));
	   }
	   //echo $optionSql."\r\n\n\n";
	   $alldeliverables         = $this -> allDeliverableInfo($clientObj, $optionSql);
	   $total                   = $clientObj -> totalDeliverables($optionSql);
	   $deliverables            = $deliverableManagerObj -> sortDeliverables($results, $options, $this -> deliverablesProgress);
	   $deliverables['isOwner'] = FALSE; 
	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
          $legislationAdminObj = new LegislationAdmin();
          $admins              = $legislationAdminObj -> getLegislationAdmins($options['legislation_id']);
          if(Legislation::isLegislationOwner($admins))
          {
             $deliverables['isOwner'] = true; 
          }
	   }
	   $deliverables['total'] = $total['total'];
	   $deliverables          = array_merge($deliverables, $alldeliverables);
	   return $deliverables;
	}
	
	function getDeliverablesList($options = array())
	{
	   $deliverableObj = new ClientDeliverable();
	   $optionSql      = "";

	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
	     $optionSql .= " AND D.legislation  = '".$options['legislation_id']."' ";
	     $optionSql .= " AND D.status & ".Deliverable::EVENT_ACTIVATED."  <> ".Deliverable::EVENT_ACTIVATED." ";
	   }
	   $results      = $deliverableObj ->  fetchAll($optionSql);
       $deliverables = array();
       foreach($results as $index => $deliverable)
       {
          $shortDescription = "";
          if(Util::isTextTooLong($deliverable['short_description']))
          {    
             $shortDescription = Util::cutText($deliverable['short_description'], $deliverable['id']."_short_description");
          } else {
             $shortDescription = $deliverable['short_description'];
          }
          $deliverables[$deliverable['id']] = $deliverable;
          $deliverables[$deliverable['id']]['shortdescription'] = $shortDescription;
       }
       return $deliverables;
	}
	
	function importDeliverables($id, $legislation, $finyearObj)
	{

		$actionObj 	     = new Action();	

		$masterObj       = new MasterDeliverable(); 
				
		$clientObj       = new ClientDeliverable(); 
	
		$deliverables    = $masterObj -> fetchByLegislationId($id);
		//$del 			= $deliverableObj -> fetchDeliverable( $id );
		$countImported	 = 0;
		$countNotImpoted = 0;
		$actionsSaved	 = array();
		$actionS         = 0;
		$actionNS		 = 0;
		if(!empty($deliverables))
		{
			//echo "deliverables are available import them . . .\r\n\n";
			foreach($deliverables as $key => $deliverable)
			{
				//$this->deliverableObj->setTablename("deliverable");
				$deliverable['status']                = Deliverable::IMPORTED + ComplianceManager::ACTIVE;
				$deliverable['deliverable_reference'] = $deliverable['id'];
				$deliverable['legislation']           = $legislation['id'];
				$deliverableLegislationDeadlineDate   = $finyearObj -> createFinancialYearDate($legislation['financial_year'], $deliverable['legislation_deadline']);
				$deliverable['legislation_deadline']  = $deliverableLegislationDeadlineDate;
				if($masterObj -> checkEvents($deliverable['id']))
				{
				   $deliverable['status'] = $deliverable['status'] + Deliverable::EVENT_ACTIVATED;
				}
				$delRef = $deliverable['id'];
				unset($deliverable['id']);
				//keep a copy of the deliverable to be contiously used before an event occurs for event driven deliverables
				//$data['deliverable_before_trigger'] = base64_encode(serialize($deliverable));
				if(($res = $masterObj -> save($deliverable)) > 0) 
				{	

				$masterObj -> importRelationshipSave("accountableperson_deliverable", $res, $delRef);
				//$this->deliverableObj->importRelationshipSave("event_deliverable", $res, $delRef);
				//$this->deliverableObj->importRelationshipSave("orgtype_deliverable", $res, $delRef);
				$masterObj -> importRelationshipSave("subevent_deliverable", $res, $delRef);
				$masterObj -> importRelationshipSave("complianceto_deliverable", $res, $delRef);
				
				//update the deliverable usage table , with the current and latest version number of the deliverable that has been imported
			    $latestDeliverableVersion = $clientObj -> getLatestVersion($deliverable['deliverable_reference']);
				$deliverableversion 	  = array();
			    if(!empty($latestDeliverableVersion))
				{
				  $deliverableversion = array("deliverable_id"  => $deliverable['deliverable_reference'],
				                              "current_version" => $latestDeliverableVersion['id'],
				                              "version_in_use"  => $latestDeliverableVersion['id'] 
				                             );
				} else {
				$deliverableversion = array("deliverable_id"  => $deliverable['deliverable_reference'], 
				                            "current_version" => 0,
				                            "version_in_use" => 0 
				                           );
				}
				$this -> setTablename("deliverable_usage");
				$this -> save($deliverableversion);
					//if its set that you must import actions then , continue importing otherwise leave it
				if(($legislation['legislation_status'] & Legislation::IMPORT_ACTION) == Legislation::IMPORT_ACTION)
				{
			       $actionsSaved = $actionObj -> importDeliverableActions($res, $deliverable['deliverable_reference'], $legislation['financial_year'], $finyearObj);
				   $actionS     += $actionsSaved['saved'];
				   $actionNS    += $actionsSaved['notSaved']; 
				}
					$countImported++;
				} else {
					$countNotImpoted++;
				}			
			}
		}
		return array("imported"       => $countImported, 
		             "notImported"    => $countNotImpoted,
		             "actionSaved"    => $actionS,
		             "actionNotSaved" => $actionNS 
		            );		
	}
	
	function getEditDeliverableForm( $data )
	{ 
          $deliverableFormObj = new DeliverableEditDisplay($data['deliverableid']);
          $deliverableFormObj -> displayForm(new Form(), new DeliverableNaming(), $data);
	}		

      /*
       Get the deliverables sub-events from the legislation-id
      */
      function getDeliverableEvent($legisaltionIds = array())
      {
          $results   = array();
          $clientObj = new ClientDeliverable();
            //get all the deliverable for each of the legislation where user is admin or authorizor 
          $optionSql = "";
          foreach($legisaltionIds as $legislationId)
          { 
            $optionSql .= " D.legislation = '".$legislationId."' OR";      		
          }
          if(!empty($optionSql))
          {
            $optionSql  = " AND (".rtrim($optionSql, " OR").")";  
          }
          $results              = $clientObj -> fetchAll($optionSql);
          $deliverableSubevents = array();
          $subeventsObj         = new DeliverableSubEvent();
          foreach($results as $index => $val)
          {
            $tmpSubEvents = $subeventsObj -> fetchByDeliverableId($val['id']);
            $deliverableSubEvent[$val['id']] = $tmpSubEvents;
          }

      	//load the subevents that are available for triggering for these deliverable where the user is the legislation owner or authorizor
      	 $subeventsAvailable = array();
		 if(!empty($deliverableSubEvent))
		 {
		   foreach($deliverableSubEvent as $deliverableId => $deliverable)
		   {
		      foreach($deliverable as $index => $delSubEvent)
		      {		        
			   if(!empty($delSubEvent['subevent_id']))
			   {
			         $subeventsAvailable[$delSubEvent['subevent_id']] = $delSubEvent['subevent_id'];
			    }		          
		      }
		    }
		  }		
		return $subeventsAvailable;			
      }

       //activate the deliverables after a subevents has occured and event date has been set
      //this only applies to deliverables that have been checked 
      function activateDeliverable($data = array())
      {  
         $countActivated   = 0;
         $countDeactivated = 0;
         if(isset($data['checked']) && !empty($data['checked']))
         {
            foreach($data['checked'] as $index => $checkedVal)
            {
               $id                   = substr($checkedVal, 17);  
               $updatedata['status'] = 0;
               $del                  = $this -> fetch($id);
               $updatedata['status'] =  $del['status'] + Deliverable::ACTIVATED_BYSUBEVENT;
               //$this->setTablename("deliverable");
               $res      = $this -> deliverableObj -> update($id, $updatedata, new DeliverableAuditLog());
               $countActivated++;
            }
            /*
            if($countActivated > 0)
            {
            	$subeventObj = new SubEvent();
            	$subeventObj->setTablename("event_occurance");
            	$eventupdatedata['status']  = SubEvent::
            	$evenRes     = $subeventObj->update($id, $eventupdatedata, new DeliverableAuditLog());
            }*/
         }
         if(isset($data['unchecked']) && !empty($data['unchecked']))
         {
         	foreach($data['unchecked'] as $un => $unchecked)
         	{
         		if(!empty($unchecked))
         		{
         			$countDeactivated++;
         		}         		
         	}
         }
         $text  = "";     
         $text .=  "&nbsp;&nbsp;".$countActivated." deliverables have been activated and updated, and are available under manage ";
         $text .=  $countDeactivated." deliverables have not been activated <br />";
         return array("text" => $text, "error" => false);           
      }
        
     
	public static function recurringTypes($key="")
	{
          $recurranceTypes = array( "fixed"           => "Fixed", 
                                    "start"           => "Start Of Financial Year",
                                    "end"             => "End Of Financial Year",
                                    "startofmonth"    => "Start Of Month",
                                    "endofmonth"      => "End Of Month",
                                    "startofquarter"  => "Start Of Quarter",
                                    "endofquarter"    => "End Of Quarter",
                                    "startofsemester" => "Start Of Semester",
                                    "endofsemester"   => "End Of Semester",
                                    "startofweek"	  => "Start Of Week",
                                    "endofweek"       => "End Of Week" 
                                   );								
		if(isset($recurranceTypes[$key]))
		{
			return $recurranceTypes[$key];
		}
		return $recurranceTypes;
	}

	function __call($methodname, $args)
	{  
       if(substr($methodname, 0, 2) == "is")
       {
         return $this -> _checkStatus($methodname, $args[0]);
       }
	}
	
	function _checkStatus($methodname, $args)
	{
	    $statusToCheck        = substr($methodname, 2);
	    $const                = preg_replace("/(?=[A-Z])/", "_", $statusToCheck);
	    $constantStr          = strtoupper(substr($const, strpos($const, "_")+1));   
        $deliverableConstants = $this -> deliverableConstants();
        if(isset($deliverableConstants[$constantStr]))
        {
            if(($deliverableConstants[$constantStr] & $args) == $deliverableConstants[$constantStr])
            {
                return TRUE;
            }
            return FALSE;          
        }
        return FALSE;
	}	
	/*
	 get an array of the user-access class constants
	*/
     public static function deliverableConstants()
     {
        $oClass              = new ReflectionClass("Deliverable");
        $userAccessConstants = $oClass -> getConstants();
        return $userAccessConstants;
     }	
     
     /*
      check if the deliverable edited/added is going to be actived by a an event
     */
     public static function isDeliverableSubEventActivated($subevents = array())
     {
         $totalSubevents = count($subevents);
         if($totalSubevents > 0)
         {
           if($totalSubevents == 1)
           {		               
               //if that sub-event is Unspecified/a then the deliverable is active by default else its activated by sub-event
               if($subevents[0] == 1)
               {
                 return FALSE;
               } else {
                  return TRUE;	              
               }
           } else {
                //if there are more than one sub-events , 
                //then they are event-activated thus set the status of the deliverable to event activated
                return TRUE;
           }
         } else {
           return FALSE;
         }
     }
     
     public static function isUsingClientEvent($deliverableEventId)
     {
        if($deliverableEventId >= Deliverable::EVENT)
        {
            return TRUE;
        }
       return FALSE;
     } 
     
     /*
         Check if the users is the deliverable owner
     */
    public static function isDeliverableOwner($deliverableOwner)
    {
       if($_SESSION['tid'] === $deliverableOwner)
       {
          return TRUE;
       }
      return FALSE;
    }	
    
     public static function statusMessage($statusTo, $changeStatus)
     {
        $constants = Deliverable::deliverableConstants();
        foreach($constants as $key => $value)
        {
           if(($value & $statusTo) == $value)
           {
               if($changeStatus)
               {
                  return "Deliverable status changed to <b>".strtolower(str_replace("_", " ", $key))."</b>";
               } else {
                  return "<b>".ucfirst(strtolower(str_replace("_", " ", $key)))."</b> status has been removed ";
               }
               
           }
        }  
     }

     public static function isUsingClientFunctionalService($deliverableStatus)
     {
       if( (($deliverableStatus & Deliverable::MATCH_FUNCTIONALSERVICEID) == Deliverable::MATCH_FUNCTIONALSERVICEID) || 
           (($deliverableStatus & Deliverable::USING_FUNCTIONALSERVICEID) == Deliverable::USING_FUNCTIONALSERVICEID) )
        {
          return TRUE;
        }
       return FALSE;   
     }

     public static function isUsingClientDepartment($deliverableStatus)
     { 
       if( (($deliverableStatus & Deliverable::MATCH_DEPARTMENTID) == Deliverable::MATCH_DEPARTMENTID) || 
           (($deliverableStatus & Deliverable::USING_DEPARTMENTID) == Deliverable::USING_DEPARTMENTID) )
       {
          return TRUE;
       }
      return FALSE;
     }
     
      /*
       Check if the deliverable is using a user's id
      */
      public static function isUsingUserId($deliverableStatus)
      {
        if( (($deliverableStatus & Deliverable::USING_USERID) == Deliverable::USING_USERID)  || 
           (($deliverableStatus & Deliverable::USING_MATCHEDUSERID) == Deliverable::USING_MATCHEDUSERID) 
         )
         {
           return TRUE;
         } 
        return FALSE; 
      }    
      
      function search($options = array())
      {
        $deliverableObj        = new ClientDeliverable();
        $deliverableManagerObj = DeliverableFactory::getInstance("admin");
        $whereStr              = " AND( D.description LIKE '%".$options['searchtext']."%' 
                                   OR  D.short_description LIKE '%".$options['searchtext']."%'
                                   OR D.legislation_section LIKE '%".$options['searchtext']."%' 
                                   OR D.sanction LIKE '%".$options['searchtext']."%'
                                   OR D.assurance LIKE '%".$options['searchtext']."%' 
                                   OR D.guidance LIKE '%".$options['searchtext']."%'
                                   OR D.reference_link LIKE '%".$options['searchtext']."%'
                                 )";
        $results              = $deliverableObj ->  filterDeliverables("D.*", $whereStr);
        $deliverables         = $deliverableManagerObj -> sortDeliverables($results);
        return $deliverables;
      }
      
      function getActionDeliverable($actionid)
      {
        $actionObj          = new ClientAction();
        $clientaction       = $actionObj -> getLegislationAction($actionid);
        $deliverableFormObj = new DeliverableDisplay($clientaction['deliverable_id']);
        $deliverableFormObj -> display(new DeliverableNaming());         
      }
      
       static function getStatusSQLForWhere($d) 
       {
            $sql = "(
		(
                            ".$d.".status & ".Deliverable::ACTIVATED_BYSUBEVENT." = ".Deliverable::ACTIVATED_BYSUBEVENT."
                            OR ".$d.".status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED."
                            OR ".$d.".status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED."
            ) AND ".$d.".status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ) ";
            return $sql;
        }

      
}
?>