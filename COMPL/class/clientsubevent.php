<?php
class ClientSubEvent extends Model
{
     protected static $table = "sub_event";
     
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options = "")
     {
       $results = $this->db->get("SELECT SE.id, SE.name, SE.description ,SE.status, E.name AS eventname, E.id as eid FROM #_sub_event SE
                                  INNER JOIN #_events E ON E.id = SE.main_event
                                  WHERE SE.status & 2 <> 2 $options");
       return $results;
     }
    
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT id, name, description, main_event, status FROM #_sub_event WHERE id = {$id}");
		return $result;
	}
	
	function fetchByMainEvent( $id )
	{
		$results = $this->db->get("SELECT id, name, description , main_event , status FROM #_sub_event WHERE main_event = '".$id."'");
		return $results;
	}   
	
	function fetchMany( $options = "")
	{
		$results = $this->db->get("SELECT id, name, description , main_event , status FROM #_sub_event WHERE main_event IN($options)");
		return $results;
	}
	
     
     function saveSubEvent($data)
     {
       $this->setTablename("sub_event");
       $res = $this->save($data);
       $response = $this->saveMessage("sub_event", $res);
       return $response;
     }

     function updateSubEvent($data)
     {
        $this->setTablename("sub_event");
        $actualId = self::getActualId($data['id']);
        $data['id'] = $actualId;
        $res = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
        $response = $this -> updateMessage("sub_event", $res);
        return $response;
     }
     
     public static function getActualId($fakeId)
     {
        $id = 0;
        if($fakeId > 9999)
        {
          $id = ($fakeId / 10000);
        } else {
          $id = $fakeId;
        }      
        return $id;
     }
    

}
?>