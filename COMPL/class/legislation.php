<?php
class Legislation extends Model
{

       private $id;
       
       private $name;
       
       private $reference;
       
       private $type;
       
       private $category;
       
       private $organisation_size;
       
       private $legislation_date;
       
       private $legislation_number;
       
       private $organisation_type;
       
       private $organisation_capacity;
       
       private $responsible_organisation;
       
       private $hyperlink_to_act;
       
       private $status;
       
       private $insertdate;
       
       private $insertuser;
       
       protected $db2          = "";
       
       protected static $table = "legislation";

       const INACTIVE 	       = 0;  
       
       const ACTIVE		       = 1; //stores the status of the legislations that are active for importation   
       
       const AVAILABLE 	       = 2; //stores the status of the legislation that are available for importing under New > Import

       const IMPORT_DEL        = 4; //stores the status of the legislation that allows deliverable to be also imported from the master compliance

       const EDIT_DEL          = 8; //stored the status of the imported deliverables that can be edited once fully imported

       const CREATE_DEL        = 16; //store the status of legislation that allows deliverables to be created under it

       const IMPORT_ACTION     = 32; //stores the status of the legislation that allows actions to be also imported from the master compliance

       const EDIT_ACTION	   = 64; //stored the status of the imported actions that can be edited once fully imported
       
       const CREATE_ACTION     = 128; //store the status of the legislation that allows actions to be created under it
       
       const IMPORTED	       = 256; //stores the status of those that are imported from the master compliance setup
       
       const CONFIRMED	       = 512; //store the status of those confirmed and awaits activations
       
       const ACTIVATED	       = 1024; //store the status of the legislations that have been activated and should now appear under manage and used for reports

       const CREATED           = 2048; //this is the state of a manually created legislation , ie not imported
       
       const COPIED            = 4096; //store the status of the legislations created by copied from one financial year to another 
	   
       const DEACTIVATED       = 8192; //legislations which have been created or copied and subsequently deactivated
	   
	   const REFTAG			   = "L";
       
       protected $legslationManagerObj;
       
       function __construct()
       {
	     parent::__construct();
       }	
       
       function getAll($options=array())
       {
	      $legislationManagerObj = LegislationFactory::createInstance($options['section']);
          $sqlArr                = array();          
          
          $userObj = new User();
          $users   = $userObj -> getAll();  
          $user    = $userObj -> getUser();            
          if(isset($options['financial_year']) && !empty($options['financial_year']))
          {
            $options['financial_year'] = (int)$options['financial_year'];
          } else {
            $options['financial_year'] = (int)$user['financialyear_id'];   
          }            
          $sqlArr                   = $legislationManagerObj -> getOptionSql($options); 
          //legislations from the master database
          $masterObj                = new MasterLegislation();
          $masterLegislations       = $masterObj -> fetchAll($sqlArr['master']);
          $totalLegislations        = $masterObj -> totalLegislations();
          //legislations from the client database                  
          $clientObj                = new ClientLegislation();
          $clientLegislations       = $clientObj -> getLegislationReference($sqlArr['client']);

          $legislations             = $legislationManagerObj -> sortLegislation($masterLegislations, $clientLegislations);
          $legislations['users']    = $users;
      	  $legislations['company']  = ucfirst($_SESSION['cc']);
      	  $legislations['user']     = $_SESSION['tkn'];
      	  $legislations['total']    = (isset($legislations['total']) ? $legislations['total'] : $totalLegislations['total']);
          return $legislations;
       }
       
       function legislationImport($options = array())
       {
          $legislationManagerObj  = LegislationFactory::createInstance($options['section']);
          
          $optionSql              = $legislationManagerObj -> getOptionSql($options);
          $clientObj              = new ClientLegislation();
          
          
          $clientLegislations     = $clientObj -> getLegislationReference($optionSql['client']);
          $totalClient            = $clientObj -> getTotalLegislations($optionSql['client']);
          //debug($optionSql);
          //debug($totalClient);
          //debug($clientLegislations);
          $masterObj              = new MasterLegislation();
          $masterLegislations     = $masterObj -> fetchAll();
          
          $userObj                = new User();
          $users                  = $userObj -> getAll(); 
                    
          $legislations           = $legislationManagerObj -> sortLegislation($masterLegislations, $clientLegislations);
          if(empty($legislations['legislations']))
          {
            $legislations['error_message'] = "There are no legislations available to import";
          }
          $legislations['total']  = $totalClient['total'];
          $legislations['users']  = $users;
          return $legislations;      
       }
       
       function getClientLegislations($options = array())
       {
          $legislationManagerObj = LegislationFactory::createInstance($options['section']);
          $userObj               = new UserSetting();
          $users                 = $userObj -> getAll(); 
          $user                  = $userObj -> getUser();       
          //$options['financial_year'] = $user['financialyear_id'];
          if(isset($options['financial_year']) && !empty($options['financial_year']))
          {
            $options['financial_year'] = (int)$options['financial_year'];
          } else {
            $options['financial_year'] = (int)$user['financialyear_id'];   
          }
          $optionSql                         = $legislationManagerObj -> getOptionSql($options);  //$this->saveTestData($optionSql['client']);
          $clientObj                         = new ClientLegislation();
          $clientLegislations                = $clientObj -> fetchAll($optionSql['client']);
          $totalClient                       = $clientObj -> getTotalLegislations($optionSql['client']);
          $legislations                      = array();                            
          $legislations                      = $legislationManagerObj -> sortLegislation($clientLegislations, $options);
          $legislations['total']             = $totalClient['total'];
          $legislations['users']             = $users;
          $legislations['userfinancialyear'] = $user['financialyear_id'];
          return $legislations;      
       }
       
       function getLegislations($options = array())
       {
          $clientObj             = new ClientLegislation();
          $legislationManagerObj = LegislationFactory::createInstance($options['section']);
          $optionSql             = $legislationManagerObj -> getOptionSql($options);
          /*if(isset($options['financialyear']) && !empty($options['financialyear']))
          {
             $activeSql = " AND L.financial_year  = '".$options['financialyear']."' ";          
          } else {
            $activeSql = " AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
                         OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." ";               
          }*/
          $legislations           = $clientObj -> fetchAll($optionSql['client']);
          return $legislations;
       }
       
       /*
        Copy legislation from one financial year to another
       */
       function copyLegislation($legislation_id, $financial_year)
       {
          $legislationObj = new ClientLegislation();
          $finObj         = new FinancialYear();
          $finyear        = $finObj -> fetch($financial_year);
          $legislation    = $legislationObj -> findById($legislation_id);
          $deliverableObj = new ClientDeliverable();
          $actionObj      = new ClientAction();
          if(!empty($legislation))
          {
             if((Legislation::ACTIVATED & $legislation['legislation_status']) == Legislation::ACTIVATED)
             {
               $legislation['legislation_status'] = $legislation['legislation_status'] - Legislation::ACTIVATED;
             } 
             if((Legislation::CONFIRMED & $legislation['legislation_status']) == Legislation::CONFIRMED) 
             {
               $legislation['legislation_status'] = $legislation['legislation_status'] - Legislation::CONFIRMED;
             }
             if((Legislation::COPIED & $legislation['legislation_status']) != Legislation::COPIED) 
             {
               $legislation['legislation_status'] = $legislation['legislation_status'] + Legislation::COPIED;
             }
             $fromFinancialyear                 = $legislation['financial_year'];
             $legislation['financial_year']     = $financial_year;
		$legislation['status'] = 1;
		$legislation['reminder'] = "";
             //$legislation['legislation_date']   = $finObj -> createFinancialYearDate($financial_year, $legislation['legislation_date']);
             unset($legislation['id']);
             unset($legislation['insertdate']);
             $saveRes = $legislationObj -> save($legislation);         
             if($saveRes > 0)
             {
                 $finObj -> saveLegislationCopied($legislation_id, $fromFinancialyear, $financial_year);
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "orgcapacity_legislation");
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "orgtype_legislation");
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "orgsize_legislation");
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "category_legislation");
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "legislation_admins");
          			$legislationObj -> copySaveRelationship($legislation_id, $saveRes, "legislation_authorizers");
          			$delActions = $deliverableObj -> copySaveDeliverable($legislation_id, $saveRes, $financial_year, $finObj, $deliverableObj, $actionObj);
             }
          }
          $response  = "Copying was successful . . <br />";
          $response .= $delActions['deliverables']." Deliverables and ".$delActions['actions']." actions copied for Financial Year <em>(".$finyear['value'].") ".$finyear['start_date']." -  ".$finyear['end_date']."";
          return array("text" => $response, "error" => false);
      }
       
       /*
        Import a legislation from the master table
       */
       function importSave($legislation_id, $financial_year)
       {
           $clientObj         = new ClientLegislation();
           $masterObj         = new MasterLegislation();
           $finyearObj        = new FinancialYear();
      	   $clientLegislation = $clientObj -> findByLegislation_Reference($legislation_id);	
      	   $masterLegislation = $masterObj -> fetch($legislation_id);	
      	   $response 	      = array();
      	   $delResponse       = array();    	   
      	   if(!empty($masterLegislation))
      	   {
      			$masterLegislation['legislation_status'] = $clientLegislation['legislation_status'] + Legislation::IMPORTED;
      			$masterLegislation['status']             = Legislation::ACTIVE;
      			$legislationRef                          = $masterLegislation['id'];  
      			$masterLegislation['financial_year']     = $financial_year;  
      		//$masterLegislation['legislation_date']   = $finyearObj -> createFinancialYearDate($financial_year, $masterLegislation['legislation_date']);
      			
      			unset($masterLegislation['id']);
      			unset($masterLegislation['parent_id']);
      			if($response  = $clientObj -> update($clientLegislation['id'], $masterLegislation, new LegislationAuditLog()) > 0)
      			{
      				//get the latest version of the version , if there has been updates then log the version of the update in the legislation usage
      				$currnetlegVersion 	= $clientObj -> getLatestLegislationVersion($legislationRef);
      				if(!empty($currnetlegVersion)) 
      				{
      				   $usage = array("legislation_id"  => $legislationRef, 
      				                  "current_version" => $currnetlegVersion['id'],
      				                  "version_in_use"  => $currnetlegVersion['id']
      				                  );
      				} else {
      				   $usage = array("legislation_id" => $legislationRef, "current_version" => 0, "version_in_use" => 0 );
      				}
      				//import and save the relation tables 
      				//import the orgcapacity legislation tables
      				$this -> importSaveRelationship($legislationRef, $clientLegislation['id'], "orgcapacity_legislation");
      				$this -> importSaveRelationship($legislationRef, $clientLegislation['id'], "orgtype_legislation");
      				$this -> importSaveRelationship($legislationRef, $clientLegislation['id'], "orgsize_legislation");
      				$this -> importSaveRelationship($legislationRef, $clientLegislation['id'], "category_legislation");
      				$clientObj -> setTablename("legislation_usage");
      				$clientObj -> save($usage);
      				if(($clientLegislation['legislation_status'] & Legislation::IMPORT_DEL) == Legislation::IMPORT_DEL)
      				{
      				     
      					//if the setting allows us to import the deliverables , the lets import them here
      					$delObj 		         = new Deliverable();				
      					$masterLegislation['id'] = $clientLegislation['id'];
      					$delResponse 	         = $delObj -> importDeliverables($clientLegislation['legislation_reference'], $masterLegislation, $finyearObj);
      				}				
      			
      				$text  = ""; 
      				if(!empty($delResponse))
      				{
      				  $text .= $delResponse['imported']."  deliverables  were successfully imported <br />";
      				  if($delResponse['actionSaved'] != 0)
      				  {
      					$text .= " &nbsp;&nbsp;&nbsp;&nbsp;  ".$delResponse['actionSaved']." actions were successfully imported ";
      				  }
      				  if($delResponse['actionNotSaved'] != 0)
      				  {
      					$text .= "&nbsp;&nbsp;&nbsp;&nbsp; ".$delResponse['actionNotSaved']." actions were not successfully imported ";
      				  }					
      				  if($delResponse['notImported'] != 0)
      				  {
      					$text .= $delResponse['notImported']." deliverables  were not successfully imported ";
      				  }
      				} else {
      				  $text .= " There were no deliverables for this legislation or you did not allow for importation of deliverables";
      				}				
      				$response = array("text" => "Legislation imported successfully . . <br /> &nbsp;&nbsp;&nbsp;&nbsp; ".$text, "error" => false);
      			} else {
      			  $response = array("text" => "Error importing the legislation. . ", "error" => false);
      			}
      		}
      		return $response;          
       }
       
       function getActivateLegislations($options = array())
       {
//[JC] Deactivated status added on 2013-10-16 10:47 to account for confirmed legislations being added to the deactivate legislation function
          $clientObj             = new ClientLegislation();
          $legislationManagerObj = LegislationFactory::createInstance($options['section']);
          $awaitingSql           = " AND L.legislation_status & ".Legislation::CONFIRMED." = ".Legislation::CONFIRMED." 
                                     AND L.legislation_status & ".Legislation::ACTIVATED." <> ".Legislation::ACTIVATED." 
                                     AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."  ";
          $legislationsAwaitingActivation = $clientObj -> fetchAll($awaitingSql);
          $approvedSql                    = "  AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." 
							AND ( 
								( L.legislation_status & ".Legislation::CONFIRMED." = ".Legislation::CONFIRMED." 
		                                          	AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." )
								OR (L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED.") 
							)";
          $legislationsActivated          = $clientObj -> fetchAll($approvedSql);
          $awaiting                       = $legislationManagerObj -> sortLegislations($legislationsAwaitingActivation);
          $activated                      = $legislationManagerObj -> sortLegislations($legislationsActivated);
          $headers                        = $awaiting['headers'];
          unset($activated['headers']);
          unset($awaiting['headers']);
      		$results = array("awaiting"  => $awaiting, 
                             "activated" => $activated,
                             "user"      => $_SESSION['tkn'],
                             "company"   => ucfirst($_SESSION['cc']),
                             "headers" => $headers
                            );
      		return $results;
       }
       
       /*
        Confirma legislations 
       */
       function confirmLegislation($data)
       {  
            $clientObj       = new ClientLegislation();
      		$legislationData = $clientObj -> findById($data['id']);
      		$legislationData['legislation_status'] = $legislationData['legislation_status'] + Legislation::CONFIRMED;
      		$updatedata                            = array("legislation_status" => $legislationData['legislation_status'] );
      		//$this->legislationObj->setTablename("legislation");
      		$res      = $this -> update($data['id'], $updatedata, new LegislationAuditLog(), $clientObj, new LegislationNaming());
      		$response = array();
      		if($res > 0) 
      		{
      			$activated 	    = array();
      			$inactivated    = array();	
      			$ids 			= array();
      			$deactivatedIds = array();
      			if(isset($data['activated']))
      			{
      			   $activated 	 = (array)$data['activated'];
      			}
      			if(isset($data['deactivated']))
      			{
        		   $inactivated    = (array)$data['deactivated'];
      			}			
      			$ids			= $data['ids'];
      			$deactivatedIds = $this -> _checkIactivatedActivated($inactivated, $activated);
      			$deliverableObj = new ClientDeliverable();			
      			$actionObj      = new ClientAction();
      			//get through all the deliverables and if its in deactivated array , do no confirm the deliverable
      			//ie that inactivated deliverable is not going to be used in the calculations of compliance

      			foreach($ids as $aIndex => $aVal)
      			{
      				if(!in_array($aVal['id'], $deactivatedIds))
      				{
      					$deliverable     		 = $deliverableObj -> fetch($aVal['id']); 
      					//if the deliverables is activated using the subevent ,
      					//then do not confirm it , so it does not appear on the manage pages and for calculations 
      					if(($deliverable['status'] & Deliverable::EVENT_ACTIVATED) !=  Deliverable::EVENT_ACTIVATED)
      					{
      						$delupdatedata['status'] = $deliverable['status'] + Deliverable::CONFIRMED;
      						$deliverableObj -> setTablename("deliverable");
      						$res = $deliverableObj -> update($aVal['id'], $delupdatedata, new DeliverableAuditLog());		
      						//update the actions for this deliverable as well , so that they can now appear on the dashboard 
      						//and can be used to calculate the computations now
      						if( $res > 0)
      						{
      						  $actions   = $actionObj -> fetchByDeliverableId($aVal['id']);
      						  foreach($actions as $actionIndex => $actionVal)
      						  {
      							$actionupdateData['actionstatus'] = $actionVal['actionstatus'] + ACTION::CONFIRMED;
      						     $actionObj -> setTablename("action");
      						     $ares = $actionObj -> update($actionVal['id'], $actionupdateData, new ActionAuditLog());
      						   }
      						}
      					}			
      				} 
      			}
      			$response = array("text" => "Legislation confirmed . . .", "error" => false, "updated" => true);
      		} else if( $res == 0){
      		  $response = array("text" => "No changes made to the legislation . . .", "error" => false, "updated" => false);
      		} else {
      		  $response = array("text" => "Error confirming the legislation. . .", "error" => true, "updated" => false);
      		}
      		return $response;       
       }
       
    	/*
    	 checks the state of the deliverable id when form is submitted
    	*/
    	function _checkIactivatedActivated($inactivated, $activated)
    	{
    		//contain ids which appear more than once in the inactivated array
    		$inactivatedDuplicates = array();
    		//holds the ids which appear more than once in the activated array
    		$activateDuplicates    = array();
    		//holds the ids of ths deliverables that have been deactivated and are in that state
    		$deactivatedIds        = array();
    		if(!empty($inactivated))
    		{
    		  foreach($inactivated as $inIndex => $inVal)
    		  {
    			$inactivatedDuplicates[$inVal][$inIndex] = $inVal; 
    		  }
    		}
    		if(!empty($activated))
    		{
    		  foreach($activated as $aIndex => $aVal)
    		  {
    			$activateDuplicates[$aVal][$aIndex] = $aVal;
    		  }
    		}	
    		foreach($inactivatedDuplicates as $keyId => $keyVal)
    		{
    		  if(isset($activateDuplicates[$keyId]))
    		  {
    			//if the number activated is less than number deactivated then its state is deactivated 
    			if(count($activateDuplicates[$keyId]) < count($keyVal))
    			{
    			   $deactivatedIds[$keyId]	= $keyId;
    			}
    		  } else {
    			$deactivatedIds[$keyId] = $keyId;
    		  }
    		}
    		return $deactivatedIds;
    	}
       
       /*
         Get the legislation constants
         @return array containing the legislation constants
       */
       public static function legislationConstants()
       {
         $reflectionClass      = new ReflectionClass("Legislation");
         $legislationConstants = $reflectionClass -> getConstants();
         return $legislationConstants;
       }
     
       public static function __callStatic($methodname, $arguments)
       {
          if(substr($methodname, 0, 2) === "is")
          {
             $status = 0;
             if(isset($arguments[0]))
             {
               $status = $arguments[0];
             }
             return self::checkLegislationStatus($methodname, $status);        
          }      
       }
       
       /*
         Check the provide status if its one of the legislation statuses
         @return boolean 
       */
       private static function checkLegislationStatus($methodname, $status)
       {
     	   $statusToCheck        = substr($methodname, 2);
    	   $const                = preg_replace("/(?=[A-Z])/", "_", $statusToCheck);
    	   $constantStr          = strtoupper(substr($const, strpos($const, "_")+1));   
           $legislationConstants = self::legislationConstants();
           if(isset($legislationConstants[$constantStr]))
           {
              if(($legislationConstants[$constantStr] & $status) == $legislationConstants[$constantStr])
              {
                  return TRUE;
              }
              return FALSE;          
           }
           return FALSE; 
       } 

 	 /*
	   check if the current logged user is the legislation admin/owner 
	 */		
	  public static function isLegislationOwner($admins)
	  {
	    //$adminsArr = explode(",", $admins);
	    if(in_array($_SESSION['tid'], $admins))
	    {
	       return TRUE;
	    }
	    return FALSE;
	  }
	
	 /*
	   check if the current logged user is the legislation authorizor 
	 */		
	 public static function isLegislationAuthorizor($authorizors)
	 {
	    //$authorizorArr = explode(",", $authorizors);
	    if(in_array($_SESSION['tid'], $authorizors))
	    {
	       return TRUE;
	    }
	    return FALSE;
	 } 
          
       /*   
         Import the master legislation utilities and save them in the client database
       */ 
       function importSaveRelationship($id, $legislationId, $table)
       {
       	  $results = $this -> db2 -> get("SELECT * FROM #_".$table." WHERE legislation_id = '".$id."'");
       	  if(!empty($results))
       	  {
		    foreach($results as $key => $insertdata)
		    {
		       $insertdata['legislation_id'] = $legislationId;
		       $dbRes                        = $this -> db -> insert($table, $insertdata);
		    }
       	}
       }
       /*
         Calculate the legislation from the given legislation data
         @return int
       */
       public static function calculateStatus($data)
       {	     
          $legislationConstants = self::legislationConstants();
          $status               = 0;
          foreach($data as $key => $value)
          {
            if(!is_array($key))
            {
               $_key = strtoupper($key);
               if(isset($legislationConstants[$_key]) && $value == 1)
               {
                  $status += $legislationConstants[$_key];
               }
            }
          }
          return $status;
       }
       
     public static function statusMessage($statusTo, $changeStatus)
     {
        $constants = self::legislationConstants();
        unset($constants['INACTIVE']);
        foreach($constants as $key => $value)
        {
           if(($value & $statusTo) == $value)
           {
               if($changeStatus)
               {
                  return "Legislation status changed to <b>".strtolower(str_replace("_", " ", $key))."</b>";
               } else {
                  return "<b>".ucfirst(strtolower(str_replace("_", " ", $key)))."</b> status has been removed ";
               }
           }
        }  
     }
     
     public static function statusDescription($status)
     {    
        if(($status & Legislation::ACTIVATED) == Legislation::ACTIVATED)
        {
           return "Activated";
        } else if(($status & Legislation::CONFIRMED) == Legislation::CONFIRMED){
           return "Confirmed";
        } else if(($status & Legislation::CREATED) == Legislation::CREATED){
           return "Custom Created";
        } else if(($status & Legislation::IMPORTED) == Legislation::IMPORTED){
           return "Imported";
        }
     }  
     
     function generateReport($reportData, Report $reportObj)
     {
        $legislationObj        = new ClientLegislation();
        $legislationManagerObj = LegislationFactory::createInstance("report");
        $joins                 = $legislationObj -> createLegislationJoins($reportData['value']);	   
        $fields                = $reportObj -> prepareFields($reportData['legislations'], "L");
        $groupBy               = $reportObj -> prepareGroupBy($reportData['group_by'], "L"); 
        $sortBy                = $reportObj -> prepareSortBy($reportData['sort'], "L");
        $whereStr              = $reportObj -> prepareWhereString($reportData['value'], $reportData['match'], "L");   
        $whereStr             .= $joins['where'];
        $legislations          = $legislationObj -> filterLegislations($fields, $whereStr, $groupBy, $sortBy, $joins['joins']);
        $legs                  = $legislationManagerObj -> sortLegislation($legislations, $reportData['legislations']); 
        $legs['title']         = $reportData['report_title'];
        $reportObj -> displayReport($legs, $reportData['document_format'], "legislations");   
     }
     
     function search($options = array())
     {
        $legislationObj        = new ClientLegislation();
        $legislationManagerObj = LegislationFactory::createInstance("updateadmin");
        $optionSql             = "";
        if(isset($options['searchtext']) && !empty($options['searchtext']))
        {
          $optionSql .= " AND ( L.name LIKE '%".$options['searchtext']."%' OR L.reference LIKE '%".$options['searchtext']."%' 
                          OR L.legislation_number LIKE '%".$options['searchtext']."%' ) "; 
        }
        $results      = $legislationObj -> filterLegislations("L.*", $optionSql);
        $legislations = $legislationManagerObj -> sortLegislation($results);
        return $legislations;
     }
     
     function getActionLegislation($actionid)
     {
        $actionObj          = new ClientAction();
        $clientaction       = $actionObj -> getLegislationAction($actionid);
        $deliverableFormObj = new LegislationDisplay($clientaction['legislation_id']);
        $deliverableFormObj -> display(new LegislationNaming());         
     }

     static function getStatusSQLForWhere($l) 
     {
        $sql = "(
                        ".$l.".legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."
						AND ".$l.".legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
        )";
        return $sql;
     }
  
}
