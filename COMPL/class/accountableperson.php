<?php
class AccountablePerson
{
     static $tablename = "accountable_person";


	function getAll( $options = "")
	{
	   //get all master accountable persons 
	   $mObj = new MasterAccountablePerson();
        $masterAccPerson = $mObj -> fetchAll();
         
        $cObj = new ClientAccountablePerson();
        $clientAccPerson = $cObj -> fetchAll();
        
        $accPersons = array_merge($masterAccPerson, $clientAccPerson); 
        $accountablePersonList = array();
        foreach($accPersons as $accIndex => $accPerson)
        {
          $accountablePersonList[$accPerson['id']] = array("name" => $accPerson['name'], "status" => $accPerson['status']);
        }
        return $accountablePersonList;
	}

     function getAccountablePerson($id)
     {
        $accountablePersons = $this -> getAll();
        $accountablePerson  = array();
        if(isset($accountablePersons[$id]))
        {
          $accountablePerson[$id] = $accountablePersons[$id];
        }  
        return $accountablePerson;
     }

	

}
?>