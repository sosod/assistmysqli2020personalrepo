<?php
class MasterAction extends Action
{
     
     function __construct()
     {
        parent::__construct();
     }
	/*
	 Get actions from the master table by deliverable id 
	 @params id deliverable id
	*/
	function importActions($id)
	{	
	   $response = $this -> db2 -> get("SELECT * FROM #_action WHERE deliverable_id = {$id} ");
	   return $response;
	}     
     /*
	  Get actions from the master compliance database aas specified by the action id
	*/
	function importAction($id)
	{
	  $result = $this -> db2 -> getRow("SELECT id, action, action_deliverable, owner, deadline, recurring FROM #_action WHERE id = {$id} ");
	  return $result;
	}
     /* 
      Get the what version of action is available in compliance master
     */
	function getLatestVersion($id)
	{	
        $result = $this -> db2 -> getRow("SELECT id, action_id  FROM #_action_edit WHERE action_id = {$id} ORDER BY insertdate DESC");
	   return $result;
	}
}
?>
