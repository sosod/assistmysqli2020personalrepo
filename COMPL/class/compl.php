<?php

class COMPL extends ASSIST_MODULE_HELPER {

	public function __construct() {
		parent::__construct();
		if(isset($this->section)) {
			$this->setHelperAttachmentDownloadOptions("action=GET_ATTACH&section=".$this->section);
			$this->setHelperAttachmentDownloadLink("../common/attachment.php");
			$this->setHelperAttachmentDeleteOptions("action=DELETE_ATTACH&section=".$this->section);
			//$this->setHelperAttachmentDeleteByAjax(false);
			$this->setHelperAttachmentDeleteByAjax(true);
			$this->setHelperAttachmentDeleteFunction("doDeleteAttachment");
			$this->setHelperAttachmentDeleteLink("../common/attachment.php");
		}
	}

	
	/******************************
		ATTACHMENT FUNCTIONS 
	******************************/
	public function getSection() {			return $this->section; }
	public function getAttachmentFolder() {	return $this->getModRef()."/".$this->attachment_folder; }
	public function getAttachmentDeleteFolder() { return $this->getModRef()."/deleted"; }
	
	/*******************************
	  USER MANUALS
	*******************************/
	
	public function getUserManuals($modref="",$user_id="") {
		$manuals = array();
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		if(strlen($modref)>0) { $this->setDBRef($modref); }
		
		$userObj = new UserAccess();
		$user = $userObj->getUserByUserID($user_id);
		
		$sql = "SELECT * FROM ".$this->getDBRef()."_user_manuals WHERE status & 1 = 1 ORDER BY sort";
		$rows = $this->mysql_fetch_all($sql);
		
		foreach($rows as $r) {
			$active = false;
			$name = $r['link'];
			$e = explode(".",$name);
			$ext = $e[1];
			switch($r['usage']) {
				case "new_leg_deactivate":
				case "new":
					$active = ( ( ($user['status'] & USERACCESS::CREATE_LEGISLATION) == USERACCESS::CREATE_LEGISLATION ) ||  ( ($user['status'] & USERACCESS::IMPORT_LEGISLATION) == USERACCESS::IMPORT_LEGISLATION ) );
					break;
				case "admin":
					$active = ( ($user['status'] & USERACCESS::ADMIN_ALL) == USERACCESS::ADMIN_ALL);
					break;
			}
			if($active) {
				$manuals[] = array(
					'text'=>$r['text'],
					'name'=>$name,
					'date'=>strtotime($r['date']),
					'link'=>$r['link']
				);
			}
		}
		
		return $manuals;
	}
	
	
	
	
	
	
}


?>