<?php
class DeliverableAssurance extends Model
{
     protected static $tablename = "deliverable_assurance";

	function __construct()
	{
		parent::__construct();
	}
	
	function fetchAll( $start, $limit, $id)
	{
		$results = $this->db->get("SELECT DA.id, DA.insertdate , DA.date_tested, DA.response, DA.sign_off,
							CONCAT(TK.tkname,' ',TK.tksurname) AS user 
							FROM #_deliverable_assurance DA
							INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = DA.insertuser
							WHERE DA.deliverable_id = {$id}
							AND DA.status & 2 <> 2
							ORDER BY DA.id 
							LIMIT $start, $limit
						");
		return $results;
	}

	function fetch( $id )
	{
		$results = $this->db->getRow("SELECT DA.id, DA.insertdate , DA.date_tested, DA.response, DA.sign_off, 
												CONCAT(TK.tkname,' ',TK.tksurname) AS user, DA.status 
												FROM #_deliverable_assurance DA
												INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = DA.insertuser
												WHERE DA.id = {$id}
												ORDER BY DA.id 
											");
		return $results;
	}
	
	function fetchDeliverableAssurance()
	{
		$result = $this->db->getRow("SELECT COUNT(*) AS total FROM #_deliverable_assurance WHERE status & 2 <> 2 ");
		return $result;		
	}
}
