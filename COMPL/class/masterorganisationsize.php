<?php
/*
Organisation sizes from the master table
*/
class MasterOrganisationSize extends OrganisationSize
{
     
     function __construct()
     {
       parent::__construct();
     }
     
	function fetchAll($options = "")
	{
		$results = $this->db2->get("SELECT * FROM #_organisation_sizes WHERE status & 2 <> 2 AND status & 1 = 1 $options ORDER BY id");
		$sizes = array();
		foreach($results as $index => $size)
		{
		   $sizes[$size['id']] = $size;
		   $sizes[$size['id']]['imported'] = true;
		}
		return $sizes;
	}

	function searchOrganisationSizes2( $text )
	{
	  $results = $this->db2->get("SELECT id FROM #_organisation_sizes WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
	  return $results;	
	}
	     

	     
}
?>
