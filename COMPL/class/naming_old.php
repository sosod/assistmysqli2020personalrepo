<?php
class Naming extends Model 
{
	const MANAGE 	 = 2;
	
	const NEWPAGES   = 4;
	
	protected static $page;
	
     protected static $table = "header_names";	
	
	protected $queryOptions = array();	
	
	protected $headers = array();
	
	function __construct()
	{
          $this->queryOptions = array(
	                                "manage" => " AND H.status & ".Naming::MANAGE." = ".Naming::MANAGE."", 
	                                "new"    => " AND H.status & ".Naming::NEWPAGES." = ".Naming::NEWPAGES.""
	                               );	
		parent::__construct();
		$this->setTablename("header_names");
	}
	
     function getSqlOption($page)
     {
          $sql  = "";
		if(isset($this->queryOptions[static::$page]))
		{
		  $sql = $this->queryOptions[static::$page];
		}     
		return $sql;
     }
     
     function setSqlOptions($option)
     {
         static::$page = $option;
     }
     
	function headersList($page = "")
	{
	   $headers  = $this -> getNaming($page);
		
	   $reData   = array();
	   foreach( $headers as $index => $header)
	   {
		  $resData[$header['name']] = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology'] );
	   }
	   return $resData;	
	}
	
	function getHeaderNames()
	{
		$response = $this->db->get("SELECT name, client_terminology, ignite_terminology FROM #_header_names 
		                            WHERE status  & ".ComplianceManager::ACTIVE." =  ".ComplianceManager::ACTIVE." " );
		$resData  = array();
		foreach($response as $key => $header)
		{
			$resData[$header['name']] = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology'] );
		}		
		return $resData;	
	}
	
	function formatHeaders($data)
	{
		$headers = $this->getHeaderNames();
		foreach($data as $index => $dataValue)
		{
		  foreach( $headers as $key => $header)
		  {
			if(array_key_exists($key, $dataValue))
			{	
			   $this -> headers[$key]	 = $header;
			   $response[$dataValue['ref']][$key]  = $dataValue[$key];
		     }		
		  }
		}
		return array("headers" => $this->headers, "data" => $response, "total" => count($response) );
	}
	
	function getNaming($options = array())
	{    
	   $optionSql = "";
	   if(!empty($options))
	   {
	     if(is_array($options))
	     {
	        foreach($options as $index => $val)
	        {
	          $optionSql .= " AND ".$index." = '".$val."' ";
	        }
	     }
	   }
	   $results = $this -> db -> get("SELECT * FROM #_header_names H WHERE  1 $optionSql");
	   return $results;
	}	
	
	function setHeader($key, $headers, $color = FALSE)
	{
	   //$header   = "";
        $terminology = "";
        $purpose     = "";
        $rule        = "";
	   foreach($headers as $index => $header)
	   {          
	     if($header['name'] == $key)
	     {
	       $terminology = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology']);
	       $purpose     = $header['purpose'];
	       $rule        = $header['rules'];
	       $nameStr  = "<span title='".$purpose."'>";
	       $nameStr .= "<a href='#' id='header_".$key."' class='header' style='color:".($color ? "black" : "white")."; text-decoration:none;' padding-left:5px;'>".$terminology."</a>";
	        $nameStr .= "<span id='rules_".$key."' style='display:none'>".$rule."</span>";
	        $nameStr .= "</span>";
	        return $nameStr;
	     } else {
	       $terminology =  ucwords(str_replace("_", " ", $key));   
	       return $terminology;
	     }  
	   }

	   /*if(isset($headers[$key]))
	   {
	     $terminology = ($value['client_terminology'] == "" ? $value['ignite_terminology'] : $value['client_terminology']);
	     //return $terminology;
	   } else {
	     $terminology =  ucwords(str_replace("_", " ", $key));
	   }*/
	   
	   
	   
		/*if(isset($namingList[$key]))
		{
			$header =  $namingList[$key];
			return $header;
		} else {
			$header  = ucwords( str_replace("_", " ", $key) );
			return $header;				
		}*/
		
	}
	
	function getNamingList($options = array())
	{
		$response = $this -> db -> get("SELECT name, client_terminology, ignite_terminology FROM #_header_names");
		$namingList = array();
		foreach($response as $i => $val)
		{
		    if(isset($options['fields']))
		    {
		      if(in_array($val['name'], $options['fields']))
		      {
		        $namingList[$val['name']] = ($val['client_terminology'] == "" ? $val['ignite_terminology'] : $val['client_terminology'] );     
		      } 
		    } else {
			$namingList[$val['name']] = ($val['client_terminology'] == "" ? $val['ignite_terminology'] : $val['client_terminology'] );
		   }
		}
		return $namingList;
	}
	

	function saveColumnOrder($columns)
	{
		$this->setTablename("header_names");
		$manageColumns = array();
		$newColumns    = array();
		$i = 0;
		$x = 0;
		//get all the column ids for this group
		foreach($columns as $index => $valArr)
		{
			if( $valArr['name'] == "new_hidden")
			{
				$newColumns[$i] 	= $valArr['value'];
				$i++;
				unset($columns[$index]);
			} else if( $valArr['name'] == "manage_hidden"){
				unset($columns[$index]);
			}
		}	
		//get the active columns array
		$newActiveColumns 	 = array();
		$manageActiveColumns = array();
		foreach( $columns as $index => $activeArr )
		{
			if( strstr($activeArr['name'], "manage")){
				$mId = substr($activeArr['name'], 7);
				$manageActiveColumns[$i] = $mId;
				$i++;
			} else {
				$nId = substr($activeArr['name'], 4);
				$newActiveColumns[$x] = $nId;
				$x++;				
			}
		}
		//sort the colums for udpading
		$columnSorted = array();
		foreach( $newColumns as $index => $id)
		{
			if( in_array($id, $newActiveColumns))
			{
				$columnSorted[$id]['status']   = 4;
				$columnSorted[$id]['position'] = $index;
			} else {
				$columnSorted[$id]['status']   = 0;
				$columnSorted[$id]['position'] = $index;
			}
			
			//saving manage 			
			if(in_array($id, $manageActiveColumns)) {
				$columnSorted[$id]['status']   = $columnSorted[$id]['status'] + 2;
				$columnSorted[$id]['position'] = $index;
			} else {
				$columnSorted[$id]['status']   = $columnSorted[$id]['status'] + 0;
				$columnSorted[$id]['position'] = $index;			}	 	
		}
		$result = 0; 		
		foreach ( $columnSorted as $id => $valArr)
		{
			$res = $this -> update($id, $valArr, new SetupAuditLog());
			$result = $result + $res;  
		}	
		$response = array();	
		if( $result > 0){
			$response = array("text" => "Columns updated successfully", "error" => false);
		} else if($result == 0){
			$response = array("text" => "No change was made", "error" => false);
		} else {
			$response = array("text" => "Error updating columns", "error" => true);		
		}
		return $response;
	}	

	function updateLegislationNaming($data)
	{
		$id = $data['id'];
		unset($data['id']);
		$this -> setTableName("header_names");
		$res = $this -> update( $id, $data, new SetupAuditLog());
		$response = $this->updateMessage("naming", $res);
		return $response;
	}
		
	//abstract function getNaming();
	
}
