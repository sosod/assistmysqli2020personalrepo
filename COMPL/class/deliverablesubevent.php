<?php
class DeliverableSubEvent extends Model
{
     
     const CLIENTID = 4;
     
     function __construct()
     {
       parent::__construct();
     }     
     
     function fetchByDeliverableId($deliverableId)
     {
        $results = $this -> db -> get("SELECT * FROM #_subevent_deliverable WHERE deliverable_id = '".$deliverableId."' ");
        return $results;
     }
     
     function fetchBySubEventId($eventId)
     {
        $results = $this -> db ->get("SELECT * FROM #_subevent_deliverable WHERE subevent_id = '".$eventId."' ");
        return $results;
     }     

     
	function updateDeliverableSubevent($new, $id)
	{
		$this -> db -> delete("subevent_deliverable", "deliverable_id=$id");
		if(!empty($new))
		{
		   foreach($new as $i => $val)
		   {
			  $this -> db -> insert("subevent_deliverable", array("deliverable_id" => $id, "subevent_id" => $val));
		   }		    
		}
	}
	
	function getClientEventsUsed()
	{
	   $results = $this -> db -> get("SELECT * FROM #_subevent_deliverable WHERE subevent_id > 9999");
	   $subevents = array();
	   if(!empty($results))
	   {
	     foreach($results  as $index => $event)
	     {
	        $subevents[$event['subevent_id']]  = $event['subevent_id'];
	     }
	   }
	   return $subevents;
	}

}
?>