<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
class Directorate extends Model {

    protected static $tablename = "dir";

    function __construct()
    {
    	parent::__construct();
    }
    
    function getDirectorates()
    {
       $response = $this->db2->get("SELECT * FROM #_dir D WHERE D.status & 1  = 1 ");
       return $response;
    }
        
   	function getSubDirectorates()
   	{
       $results  = $this->db2->get("SELECT  DR.id, DS.id AS ref, CONCAT_WS('-', DR.name, DS.name) AS dirtxt, DR.status
                                    FROM ".$_SESSION['dbref']."_dir DR
                                    INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.id = DS.dirid
                                    WHERE DR.status & 1 = 1 AND DR.status & 1 = 1
                                   ");
            $response = array();
           foreach( $results as $key => $val)
           {
           	if( substr($val['dirtxt'], -1 , 1) == "-")
           	{
           	  $val['dirtxt'] = substr($val['dirtxt'],0, strpos($val['dirtxt'],"-"));
           	}
           	$response[$key] = $val; 
           }
         return $response;
      }
		
     function getSubDirectoratesList()
     {
        $results = $this->getSubDirectorates();
        $directorates = array();
        foreach( $results as $d => $dVal)
        {
        	$directorates[$dVal['ref']]	 = $dVal['dirtxt'];
        }
        ksort($directorates);
        return $directorates;
     }
        
        function fetchSubDirectorates( $id )
        {
        $response = $this->db2->get("SELECT id, name, dirid FROM #_dirsub WHERE dirid = {$id}");
        return $response;
        }
        
        function getSubDirectorate( $id )
        {
            $response  = $this->db2->get("SELECT  id,
                                       DS.name AS dirtxt,
                                       DS.status,
                                       DS.dirid
                                       FROM #_dirsub DS 
                                       WHERE DS.dirid = $id
                                      ");
            return $response;
        }

        function saveActionOwnerAssigned( $insertdata, $changes )
        {
        	$res = $dirObj -> insert("dir_admins", $insertdata );   	
        	return $res;
        }
   
	function getSubOwner( $id )
	{
		$results = $this->db2->get("SELECT * FROM #_dir_admins WHERE subdir_id = $id");
		return $results;
	}        
	
	function _getSubDirectorates()
	{
		$results = $this->get("SELECT id, name, dirid FROM #_dirsub WHERE 1");
		return $results;
	}
		
}
