<?php
class ReportFactory
{

     public static function createInstance($reportType)
     {
          switch($reportType)
          {
               case "legislations":
                  return new LegislationReport();
               break;
               case "deliverables":
                    return new DeliverableReport();
               break;
               case "actions":
                   return new ActionReport();
               break;
          }
     }
     
     public static function namingInstance($reportType)
     {
          switch($reportType)
          {
               case "legislations":
                  return new LegislationNaming();
               break;
               case "deliverables":
                    return new DeliverableNaming();
               break;
               case "actions":
                   return new ActionNaming();
               break;
          }
     }
     
     public static function sortInstance($reportType)
     {
          switch($reportType)
          {
               case "legislations":
                  return new LegislationSorter();
               break;
               case "deliverables":
                    return new DeliverableSorter();
               break;
               case "actions":
                   return new ActionSorter();
               break;
          }
     } 

}
?>
