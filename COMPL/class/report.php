<?php
class Report extends Model
{        
     const LEGISLATION_REPORT = 4;
     
     const DELIVERABLE_REPORT = 8;
     
     const ACTION_REPORT      = 16;

     private $financialYear   = 0;

	function prepareFields($fields, $tablePrefix)
	{
	     $lstring    = ""; 
	     $asFields  = array(); 
	     foreach($fields as $key => $val)
	     {
		     if(array_key_exists($key, $asFields) )
		     {
			   $lstring .= $tablePrefix.".".$key." AS ".$asFields[$key].",";
		     } else {
			  if( $key != "action_progress") 
			  {
			    $lstring .= $tablePrefix.".".$key.",";
			  }
		     }
	     }	
	     $lstring = rtrim($lstring, ',');
	     return $lstring;
	}

     function prepareGroupBy($groupBy, $tablePrefix)
     {
	     $groupstring = "";
	     if($groupBy == "no_grouping")
	     {
		     $groupstring = "";
	     } else {
		     $groupstring = $tablePrefix.".".rtrim($groupBy, "_");
	     }
	     return $groupstring;
     }
     
     function prepareSortBy($sortArr, $tablePrefix)
     {
		$sortstring = "";
		foreach($sortArr as $key => $val)
		{
		   if(strstr($val, "_date"))
		   {
		     $field = substr($val, 0, -5);
		     $sortstring .= "STR_TO_DATE(".$tablePrefix.".".ltrim($field, "__").", '%d-%M-%Y'),";
		   } else if($val != "__action_progress") {
			$sortstring .= $tablePrefix.".".ltrim($val, "__").",";
		   }	
		}
		$sortstring = rtrim($sortstring, ',')." ASC";
		return $sortstring; 
     }
     
     function prepareWhereString($postValues, $matchArr, $tablePrefix)
     {
		$wherestring = "";
		foreach($postValues as $key => $val)
		{
			if(strstr($key, "_date"))
			{   
			  $field = substr($key, 0, -5);
		       if($val['from'] != "" || $val['to'] != "") 
			  {
			    $wherestring .= " AND STR_TO_DATE(".$tablePrefix.".".$field.", '%d-%M-%Y') 
			                      BETWEEN '".date("Y-m-d", strtotime($val['from']))."' 
			                      AND '".date("Y-m-d", strtotime($val['to']))."'";
			  }
			} else if(is_array($val)) {
                  if($key == 'financial_year')
                  {
                    $this -> financialYear = $val[0];
                  }
			} else {
			     if($key == "financial_year")
			     {
			        $this -> financialYear = $val;
			     }
			
				if($val != "")
				{
				   if( array_key_exists($key, $matchArr))
				   {	
					if( $matchArr[$key] == "any")
					{
					   $wherestring .= " AND ".$tablePrefix.".".$key." LIKE '%".$val."%' ";
					} else if( $matchArr[$key] == "all"){
					   $wherestring .= " AND ".$tablePrefix.".".$key." LIKE '".$val."' ";
					} else if( $matchArr[$key] == "'exact"){
					   $wherestring .= " AND ".$tablePrefix.".".$key." = '".$val."' ";
					} 				
				   } else {
					$wherestring .= " AND ".$tablePrefix.".".$key." LIKE '".$val."'";
			        }						
				} 	
			}
		}
		trim($wherestring);
		$wherestring = rtrim($wherestring,',');
		return $wherestring;
     }
     
     function displayReport($data, $type, $key)
     {
        switch($type)
        {
          case "on_screen":
            ReportDisplay::displayHtml($data, new HtmlResponse(), $key, $this -> financialYear);
          break;
          case "microsoft_excell":
            ReportDisplay::displayCsv($data, new CsvResponse(), $key, $this -> financialYear);
          break;
          case "save_pdf":
          break;
          default:
            ReportDisplay::displayHtml($data, new HtmlResponse(), $key);
          break;
        }    
     }
     
     function deleteQuickReport($data)
     {
        $res = $this -> db -> updateData("quick_report", array("status" => 2), array("id" => $data['id']));
        $response  = array();
        if($res > 0)
        {
          $response = array("text" => "Quick report successfully deleted", "error" => false);
        } else {
          $response = array("text" => "Error deleting quick report", "error" => true); 
        }
        return $response;
     }
     
     function updateQuickReport($postArr)
	{ 
		$name  		 = $postArr['report_name'];
		$description    = $postArr['report_description'];
		$id			 = $postArr['quickid'];
		unset($postArr['quickid']);
		unset($postArr['report_name']);
		unset($postArr['report_description']);		
		$insertdata = array("report" => base64_encode(serialize($postArr)), "name" => $name, "description" => $description);		
		$res = $this -> db -> updateData("quick_report", $insertdata, array("id" => $id));
		if($res > 0)
		{
		   echo "<div class='ui-widget ui-icon-closethick ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report successfully updated</div>";
		} else {
		  echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error updating report, try again</div>";		
		}	
	}
	
	function totalQuickReports()
	{
		$result = $this->db->getRow("SELECT COUNT(Q.id) As total FROM #_quick_report Q");
		return $result['total'];	
	}	
	
	function getQuickReport( $start, $limit)
	{
		$total  = $this -> totalQuickReports();
		$results = $this->db->get("SELECT id, name, description, insertuser, status FROM #_quick_report WHERE status & 2 <> 2 
		                           ORDER BY id LIMIT $start, $limit
		                          ");
		return array("reports" => $results, "total" => $total);
	}
	
	function getAQuickReport($id)
	{
		$result  = $this -> db -> getRow("SELECT * FROM #_quick_report WHERE id = '".$id."'");
		$data 	 = unserialize(base64_decode($result['report']));
		$naming  = new LegislationNaming();
		$headers = $naming -> getHeaderList();
		$sort	 = array();
		$sortOptions = $data['sort'];
		foreach( $sortOptions as $index => $field)
		{
			$keyField = ltrim($field, "__");
			if( isset($headers[$keyField]))
			{
				$sort[$keyField] = $headers[$keyField];
			} else {
				$sort[$keyField] = ucfirst(str_replace("_", " ", $keyField)); 
			}			
		} 	
		$response = array("name" => $result['name'], "description" => $result['description'], "data"=> $data, "sort"=> $sort);
		return $response;
	}     
	
	function saveQuickReport($postArr, $reporttype)
	{
		$name  		 = $postArr['report_name'];
		$description    = $postArr['report_description'];
		unset($postArr['report_name']);
		unset($postArr['report_description']);		
		$status  = $this -> getReportStatus($reporttype) + 1;
		$insertdata = array("report" => base64_encode(serialize($postArr)), "name" => $name, "description" => $description, "status" => $status);		
		$this -> setTablename("quick_report");
		$res = $this -> save($insertdata);
		if($res > 0)
		{
		   echo "<div class='ui-widget ui-icon-closethick ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report successfully saved as quick report</div>";
		} else {
		  echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error saving report, try again</div>";		
		}	  
	}
	
	function getReportStatus($report)
	{
	   $status = "";
	   if($report == "legislation")
	   {
	     $status = Report::LEGISLATION_REPORT;
	   } else if($report == "deliverable"){
	     $status = Report::DELIVERABLE_REPORT;
	   } else if($report == "action"){
	     $status = Report::ACTION_REPORT;
	   }
	   return $status; 
	}
	
	function generateQuickReport($id, $reporttype)
	{
	   $status = "";
	   if($report == "legislation")
	   {
	     $reportObj  = new LegislationReport();
	   } else if($report == "deliverable"){
	     $status = Report::DELIVERABLE_REPORT;
	   } else if($report == "action"){
	     $status = Report::ACTION_REPORT;
	   }
	}
}
?>
