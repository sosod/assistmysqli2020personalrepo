<?php
/* Organisation sizes from the client databse */
class ClientOrganisationSize extends OrganisationSize
{
     
     function __construct()
     {
        parent::__construct();
     }
     
	function fetchAll($options = "")
	{
	   $results = $this->db->get("SELECT * FROM #_organisation_sizes WHERE status & 2 <> 2 $options ORDER BY id");
	   $sizes   = array();
	   foreach($results as $index => $size)
	   {
	     $sizes[$size['id']] = $size;
	     $sizes[$size['id']]['imported'] = false;
	   }
	   return $sizes;
	}
	
	function fetch($id)
	{
	   $result  = $this->db->getRow("SELECT * FROM #_organisation_sizes WHERE id = $id");
	   return $result;
	}
	
	function getOrganisationSize($ids)
	{
	   $result  = $this->db->get("SELECT * FROM #_organisation_sizes WHERE id IN (".$ids.")");
	   return $result;
	}	
	

	function searchOrganisationSizes( $text )
	{
	  $results = $this->db->get("SELECT id FROM #_organisation_sizes WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
	  return $results;
	}
     
}
?>