<?php
class ActionOwnerMatch extends Model
{
     
     function __construct()
     {
       parent::__construct();
     }
     
     function fetchAll($options = "")
     {
       $results = $this->db->get("SELECT * FROM #_action_owner_users WHERE status & 2 <> 2 $options");
       return $results;
     }
     
     function fetch($id)
     {
       $result = $this->db->getRow("SELECT * FROM #_action_owner_users WHERE id = '".$id."' ");
       return $result;
     }

     function updateActionOwner($data)
     {
       $this->setTablename("action_owner_users");
       $id = $data['id'];
       unset($data['id']);
       $res  = $this->update($id, $data, new SetupAuditLog(), $this, new ActionNaming());
       $response = $this->updateMessage("action_owner", $res);
       return $response;
     }
     
	
	function saveActionOwner( $data )
	{
		$this->setTablename("action_owner_users");
		if(isset($data['usernotinlist']) && !empty($data['usernotinlist']))
		{
			$data['status'] = 1 + SetupManager::NOUSER_IN_LIST;
		}
		unset($data['usernotinlist']);
		$res = $this->save($data);
		if($res > 0)
		{
		   $actionObj = new Action();
		   $actionObj -> updateActionOwnerMatch($data['ref'], $data['user_id']);
		}
		$response = $this->saveMessage("action_owner", $res);
		return $response;
	}
	
	function deleteActionOwner($data)
	{
	  $id = $data['id'];
	  $match = $this -> fetch($id);
	  $res = $this -> db -> delete("action_owner_users", "id={$id}");
	  if($res > 0)
	  {
	     $actionObj = new Action();
	     $actionObj -> undoActionOwnerMatch($match['ref'], $match['user_id']);
	  }
	 return $this -> updateMessage("action_owner_ref_#".$id, $res);
	}
	
     
}
?>
