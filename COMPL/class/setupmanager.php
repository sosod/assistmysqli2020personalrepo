<?php
include_once("../loader.php");
spl_autoload_register("Loader::autoload");
class SetupManager
{
	
	private static $instance;
	
	const NOUSER_IN_LIST = 8;
		
	public function __construct()
	{
		//parent::__construct();				
	}
	
	function __call($methodname , $args)
	{
		$response = "";
		if(!empty($methodname))
		{
			$classname = substr($methodname, 3, -4);
			$classObj = new $classname();
			$params = "";
			if(isset($args[0]))
			{
			   $params = $args[0];
			}
			$response = $classObj->getAllList($params);
		}	
		return $response;
	}
	// ----------------------------------- Legislation Types -------------------------
	
	function getLegislativeTypesList( )
	{
		$legObj 	  = new LegislativeTypes();
		$oLegitype = $legObj->fetchLegislativeTypes();	
		$typesList = array();
		foreach($oLegitype as $index => $leg){
		  $typesList[$leg['id']] = $leg['name'];	
		}			
		return $typesList;
	}
		
	//--------------------------------- Legislative Categories ---------------------------
	function getLegislativeCategoriesList( $legId )
	{
		$legcatObj = new LegislativeCategories();
		$legCats   = array();
		//get the legislations from setup 1
		$oLegCats  = $legcatObj -> getLegislativeCategories();
		//set the flag of imported to true
		foreach ( $oLegCats as $key => $value)
		{
			$oLegCats[$key]  = $value;
			$oLegCats[$key]['imported']  = true;
		}		
		//get the category legisaltion relationship, ie categories for this legislation 
		$categoryLeg = array();
		$categoryLeg = $legcatObj -> getCategoryByLegislationId( $legId );
		//get the legislations from the current database, ie client database
		$myLegCat  = $legcatObj -> fetchAll();	
		$legCats   = array_merge($oLegCats, $myLegCat );
		//get the list of all the categories
		$catList = array(); 
		foreach($legCats as $i => $cat)
		{
			$catList[$cat['id']] = $cat['name']; 
		}
		$legcategoryList = array();
		foreach($categoryLeg as $index => $val)
		{			
			if( isset($catList[$val['category_id']]) && !empty($catList[$val['category_id']]) )
			{
				$legcategoryList[$val['category_id']] = $catList[$val['category_id']];
			}
 		}		
 		return $legcategoryList;
	}
	

	
	// -----------------------------Legislative Orgainasation types -------------------------
	
	function getLegOrganisationTypesList( $legId )
	{
		$legObj 	   = new LegOrganisationTypes();	
		$oLegOrgTypes  = $legObj -> getLegOrganisationTypes();
		$myLegOrgTypes = $legObj -> fetchAll(); 
		
		//get the organisational types for this legislation
		$_legOrgTypes = array();
		$_legOrgTypes = $legObj -> getOrganisationalTypeByLegislationId($legId);
		

		foreach( $oLegOrgTypes as $index => $val)
		{
			$oLegOrgTypes[$index]['imported'] = true;
		}
		$legOrgTypes = array_merge($oLegOrgTypes, $myLegOrgTypes);
		$typesList   = array();
		foreach($legOrgTypes as $i => $orgtype)
		{
			$typesList[$orgtype['id']] = $orgtype['name'];
		}
		
		$orgtypeList = array();
		foreach($_legOrgTypes as $index => $val)
		{
			if(isset($typesList[$val['orgtype_id']]) && !empty($typesList[$val['orgtype_id']]))
			{
				$orgtypeList[$val['orgtype_id']] = $typesList[$val['orgtype_id']];
			}
		}
		return $orgtypeList;
	}
	

	// -------------------------- Legislative Frequency ---------------------------
	function saveLegislativeFrequency( $data ) 
	{
		$legFreqObj  = new LegislativeFrequency();
		$legFreqObj->setTablename("legislative_frequency");
		$id 	 = $legFreqObj->save($data);
		$response = $this->saveMessage("legislative_frequency", $id); 
		return $response;	
	}
	
	
	function getLegislativeFrequency()
	{
		$legFreqObj  = new LegislativeFrequency();
		$olegFreqs   = $legFreqObj -> getLegislativeFrequency();
		$mylegFreq   = $legFreqObj -> fetchAll();
		$legfreq 	 = array(); 
		foreach( $olegFreqs as $key => $val)
		{
			$olegFreqs[$key] = $val;
			$olegFreqs[$key]['imported'] = true;
		}
		$legfreq = array_merge($olegFreqs, $mylegFreq );
		return $legfreq;
	}
	
	function updateLegislativeFrequency( $data )
	{
		$compFreqObj 	= new LegislativeFrequency();
		$compFreqObj->setTablename("legislative_frequency");
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $compFreqObj->update($id, $data, new SetupAuditLog(), $compFreqObj,new ActionNaming());
		$response = $this->updateMessage("legislative_frequency", $res);
		return $response;
	}

//------------------------- sub events -------------------------------
	//get the sub-event for deliverables created by triggering a subevent
	function getSubEventOccuranceId( $occuranceId )
	{
		$sbEvent    = "";	
		if(!empty($occuranceId))
		{
			$sbeventObj = new SubEvent();
			$event 		= $sbeventObj -> getOccurance( $occuranceId );
			$oEvents  	= $sbeventObj->getSubEvents();
			$myEvents 	= $sbeventObj->fetchAll();	
			$subevents  = array_merge($oEvents, $myEvents );		
			foreach($subevents as $sIndex => $sVal)
			{	
				if($sVal['id'] == $event['event_id'])
				{
					$sbEvent = $sVal['name'];
				}
			}		
		}
		return $sbEvent;
	}
	
	
	function getEventOccurance( $id )
	{
		$sbeventObj = new SubEvent();
		$event = $sbeventObj -> getOccurance( $id );
		return $event;
	}

	//fetch the sub events from the options of the main events selected
	function getMainSubEvents( $options = "")
	{
		$eventsObj = new SubEvent();
		$mEvents  = $eventsObj->getSubEvents();
		$aEvents  = $eventsObj->fetchByMainEvent( $options['id'] );
		$msEvents = $eventsObj->getByMainEvent( $options['id'] );
		$subEvents = array();
		foreach( $mEvents as $index => $val)
		{
			foreach( $aEvents as $key => $event)
			{
				if( $event['id']  == $val['main_event'])
				{
					$subEvents[$val['id']]  = array(
													"id" 		  => $val['id'],
													"main_event"  => $event['name'],
													"name"		  => $val['name'],
													"description" => $val['description'],
													"status"	  => $val['status']
												);			
				}			
			}
		}
		//$subEvents = array_merge($msEvents, $aEvents);
		$events  = array();
		foreach ($msEvents as $index => $val)
		{
			$msEvents[$index] = $val;
			$msEvents[$index]['imported'] = true;
 		}
 		$events  = array_merge($msEvents, $subEvents );
 		return $events; 
	}

	function getSubEvent( $id )
	{
		$subEvents = $this->getSubEvents();
		$result    = "";
		foreach( $subEvents as $index => $val)
		{
			if( $val['id'] == $id)
			{
				$result = $val['name'];
			}
		} 
		return $result;
	}
	// --------------------------- Action Owners -------------------------------------------	

     /*
       get all users , those who have been setup/configured will be set as used = true
     */
	function getuserList()
	{
		$ownerObj  = new ActionOwner();
		$userObj   = new UserAccess();
		$owners    = $ownerObj->fetchAll();
		$ownerUser = $userObj->getUserList(array("user"));
		$ownersArr = array();
		
		$ownerList = array();
		foreach($owners as $i => $own)
		{
			$ownerList[$own['user_id']] = $own['user_id']; 
		}
		
	     foreach($ownerUser as $index => $owner)
		{
			if(isset($ownerList[$index]))
			{
			   $ownersArr[$index] = array("name" => utf8_encode(html_entity_decode($owner)), "id" => $index, "used" => true );
			} else {
			   $ownersArr[$index] = array("name" => utf8_encode(html_entity_decode($owner)), "id" => $index, "used" => false );
			}
		}
		return $ownersArr;
	}
	
	/*
	    get all users , those who have been configured and also setup as action owners , are used = true
	*/
	function getUsers()
	{
		$ownerObj  = new ActionOwner();
		$userObj   = new UserAccess();
		$owners    = $ownerObj->fetchAll();
		$ownerUser = $userObj->getUserList(array("user"));
		$userAccess = $userObj->getUserAccessSettings();
		$ownersArr = array();
		
		$accessList  = array();
		foreach($userAccess as $u => $uArr)
		{
			$accessList[$uArr['user_id']] = $uArr['user_id'];
		}
		
		$ownerList = array();
		foreach( $owners as $i => $own)
		{
			$ownerList[$own['user_id']] = $own['user_id']; 
		}
		
	     foreach($ownerUser as $index => $owner)
		{
			if(isset($accessList[$index]))
			{
				if(isset($ownerList[$index]))
				{
					$ownersArr[$index] = array("name" =>  utf8_encode(html_entity_decode($owner)), "id" => $index, "used" => true );
				} else {
					$ownersArr[$index] = array("name" =>  utf8_encode(html_entity_decode($owner)), "id" => $index, "used" => false );
				}
			}
		}
		return $ownersArr;
	}
	
	function getUsedActionOwners()
	{
		$actionowners = $this->getActionOwners();
		$userowner    = array();
		foreach($actionowners as $index => $useraction)
		{
			$userowner['user'][$useraction['user_id']]  = $useraction['user_id'];
			$userowner['owner'][$useraction['id']] 		= $useraction['id'];
		}	
		return $userowner;
	}
	// ------------------------------------- user access setup ----------------------------------

	function getUserSetting()
	{
		$userObj = new UserAccess();
		$results = $userObj -> getUserAccessSettings();
		return $results;
	}

	//---------------------------------------- responsible organisations -----------------------
	
	function getResponsibleOrganisations(  $options = "")
	{
		$orgObj 	   = new ResponsibleOrganisations();
		$oOrganisation =  $orgObj -> getResponsibleOrganisations();
		$myOrganisation  = $orgObj -> fetchAll( $options );
		$organisations    = array();
		foreach( $oOrganisation as $index => $val )
		{
			$oOrganisation[$index] = $val;
			$oOrganisation[$index]['imported'] = true;
		}
		$organisations = array_merge($oOrganisation, $myOrganisation);
		return $organisations;
	}
	
    function updateResponsibleOrganisation( $data )
    {
    	$orgObj = new ResponsibleOrganisations();
        $orgObj->setTablename("organisation");      	
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $orgObj->update($id, $data, new SetupAuditLog(), $orgObj,new ActionNaming());
		$response = $this->updateMessage("organisation", $res);
        return $response;
    }
    
    function saveResponsibleOrganisation()
    {
    	$orgObj = new ResponsibleOrganisations();
        $orgObj->setTablename("organisation");  
		$res = $orgObj->save($data);
		$response = $this->saveMessage("organisation", $res);
		return $response;        
    }
    // ---------------------------------------- organisation capacity ---------------------
    
    function getOrganisationCapacityList( $legId )
    {
    	$capacityObj   = new OrganisationCapacity();
    	//import the organisation capacitied from compliance master
    	$oOrgCapacity  = $capacityObj -> getOrganisationCapacity();
    	//get the org capacities from the client database
    	$myOrgCapacity =  $capacityObj->fetchAll();
    	$orgcapacity   = array();
    	foreach ($oOrgCapacity as $index => $capacity)
    	{
    		$oOrgCapacity[$index] = $capacity;
    		$oOrgCapacity[$index]['imported'] = true;	
    	}   	
    	$orgcapacity = array_merge($oOrgCapacity, $myOrgCapacity);
    	foreach($orgcapacity as $index => $orgcap)
    	{
    		$orgcapList[$orgcap['id']] = $orgcap['name'];
    	}
    	$legCapacity = array();
    	// get the relationship in the client database
    	$legCapacity = $capacityObj->getCapacityByLegislationId( $legId );
    	$legcapacityList = array();
    	foreach($legCapacity as $i => $val)
    	{
    		if(isset($orgcapList[$val['orgcapacity_id']]) && !empty($orgcapList[$val['orgcapacity_id']]))
    		{
    			$legcapacityList[$val['orgcapacity_id']] = $orgcapList[$val['orgcapacity_id']]; 
    		}	
    	}	
    	return $legcapacityList;
    }
    
    function importOrganisationCapacityList( $legId )
    {
    	$capacityObj   = new OrganisationCapacity();
    	//import the organisation capacitied from compliance master
    	$oOrgCapacity  = $capacityObj -> getOrganisationCapacity();
    	
    	$orgcapList   = array();
    	foreach ($oOrgCapacity as $index => $capacity)
    	{
    		$orgcapList[$capacity['id']] = $capacity['name'];
    	}   	
    	$legCapacity = array();
    	//import the relationship from compliance master
    	$legCapacity = $capacityObj->importCapacityByLegislationId( $legId );
    	$legcapacityList = array();
	
    	foreach($legCapacity as $i => $val)
    	{
    		if(isset($orgcapList[$val['orgcapacity_id']]) && !empty($orgcapList[$val['orgcapacity_id']]))
    		{
    			$legcapacityList[$val['orgcapacity_id']] = $orgcapList[$val['orgcapacity_id']]; 
    		}	
    	}	
    	return $legcapacityList;
    }       


  // ------------------------------------------- organisation size	------------------------------------

	public static function _setOptions( $options = array())
	{
		$option = "";
	    if(isset($options) && !empty($options))
    	    {
    		if(is_array($options))
    		{
    			foreach( $options as $key => $val )
    			{
    				if(is_array($val))
    				{
        				$k = key($val);
    					if( is_numeric($k))
    					{
	    					$val[$key] = $val[$k];
	    					unset($val[$k]);
    					} 
    					return self::_setOptions($val);
    				} else {
	    				if( $key == "status")
	    				{
	    					$option .= " AND ".$key." & ".$val." = ".$val;
	    				} else {
	    					$option .= " AND ".$key." = ".$val;
	    				}
    				}
    			}
    		} 
        }	
    	  return $option;
	}
	
	function getOrganisationSizeList( $legId )
	{
    	$sizeObj   = new OrganisationSizes();
    	$oOrgSize  = $sizeObj -> getOrganisationSizes();
    	$myOrgSize = $sizeObj->fetchAll();
    	//get the organosational sizes for  this legiasltion
    	$legOrgsizes = array();
    	$legOrgsizes = $sizeObj->getOrganisationalSizeByLegislationId( $legId );
    	$orgcapacity   = array();
    	foreach ($oOrgSize as $index => $capacity)
    	{
    		$oOrgSize[$index] = $capacity;
    		$oOrgSize[$index]['imported'] = true;	
    	}
    	$orgsize = array_merge($oOrgSize, $myOrgSize);
    	foreach($orgsize as $i => $orgs)
    	{
    		$orgzList[$orgs['id']] = $orgs['name']; 
    	}
    	$legorgsizeList = array();
    	foreach($legOrgsizes as $index => $val)
    	{
    		if(isset($orgzList[$val['orgsize_id']]) && !empty($orgzList[$val['orgsize_id']]))
    		{
    			$legorgsizeList[$val['orgsize_id']] = $orgzList[$val['orgsize_id']];
    		}
    	}
    	return $legorgsizeList;
	}
	
	function importOrganisationSizeList( $legId)
	{
    	$sizeObj   = new OrganisationSizes();
    	//get the organisational sizes from compliance master
    	$oOrgSize  = $sizeObj -> getOrganisationSizes();
    	
    	$legOrgsizes = array();
    	//import the organisational size and legislation relationships from compliance master
    	$legOrgsizes = $sizeObj->importOrganisationalSizeByLegislationId( $legId );
    	$orgcapacity   = array();
    	foreach ($oOrgSize as $index => $capacity)
    	{
    		$orgzList[$capacity['id']] = $capacity['name'];	
    	}
    	$legorgsizeList = array();
    	foreach($legOrgsizes as $index => $val)
    	{
    		if(isset($orgzList[$val['orgsize_id']]) && !empty($orgzList[$val['orgsize_id']]))
    		{
    			$legorgsizeList[$val['orgsize_id']] = $orgzList[$val['orgsize_id']];
    		}
    	}
    	return $legorgsizeList;
	}

	
	//----------------------------------------- sort multiple selections ---------------------
	
	function multipleSort( $result, $nameField = "" )
	{
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				if( $nameField == ""){
					$response .= $value['name'].",";
				} else {
					$response .= $value[$nameField].",";
				}
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}
	
	function _list( $data )
	{
		$list = array();
		if(isset($data) && !empty($data))
		{
			foreach($data as $index => $dataArr)
			{
				$list[$dataArr['id']] = $dataArr['name'];		
			}
		}
		return $list;
	}
	//----------------------------------------- message response --------------------------
	function updateMessage( $context, $status )
	{
		$response = array();
		if( $status >= 0){
			$response = array("text" => ucwords( str_replace("_", " ", $context ) )." updated successfully", "error" => false);
		} else {
			$response = array("text" => "Error updating ".ucwords( str_replace("_", " ", $context ) )."", "error" => true);
		}
		return $response;
	}
	
	function saveMessage( $context, $status)
	{
		$response = array();
		if( $status > 0){
			$response = array("text" => ucwords(str_replace("_", " ", $context ) )." saved successfully", "error" => false, "id" => $status );
		} else {
			$response = array("text" => "Error saving ".ucwords(str_replace("_", " ", $context ) )."", "error" => true);
		}
		return $response;
	}
	
}


function compare( $a, $b)
{
	return (($a['insertdate'] < $b['insertdate']) ? 1 : -1);
}
