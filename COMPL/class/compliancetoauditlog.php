<?php
class ComplianceToAuditLog extends SetupAuditLog
{

     function processChanges($obj, $postedArr, $id, Naming $naming)
     {
          $data = $obj -> fetch($id);
          $tablename = $obj -> getTablename();
          $compliancetoObj  = new ComplianceTo(); 
          $complianceTo     = $compliancetoObj -> getAll();

          $userObj = new User();
          $userList = $userObj -> getAll();          
          
          $titleStr = "";
          if(isset($complianceTo[$data['ref']]))
          {
              $titleStr = $complianceTo[$data['ref']]['name'];
          }
          $title = str_replace(" ", "_", $titleStr);
          $key = "deliverable_compliance_to_title_".strtolower($titleStr);

          
          $changes = array();
	      if(isset($postedArr['user_id']))
	      {
	       if($postedArr['user_id'] != $data['user_id'])
	       {

	          $from = $to = "";
               if(isset($complianceTo[$data['user_id']]))
               {
                   $from = $complianceTo[$data['user_id']]['name'];
               } else {
                   if(isset($userList[$data['user_id']]))
                   {
                     $from = $userList[$data['user_id']]['user'];
                   }
               }
               
               if(isset($complianceTo[$postedArr['user_id']]))
               {
                   $to = $complianceTo[$postedArr['user_id']]['name'];
               } else {
                   if(isset($userList[$postedArr['user_id']]))
                   {
                     $to = $userList[$postedArr['user_id']]['user'];
                   }
               }
	          $changes[$key] = array("from" => $from, "to" => $to); 		  
	       }
	     } else if(isset($postedArr['status'])) {
	          $changes[$key] = $this -> processStatusChanges($postedArr['status'], $data['status']);
	     }     
	     $res = 0;	
	     if(!empty($changes))
	     {
	        $res = $this->saveChanges($changes, $id, "compliance_to_users", $obj);
	        $obj->setTablename("compliance_to_users");
	     }
          return $res;
     }     
     

}
?>