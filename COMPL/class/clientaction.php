<?php
require_once("../../library/class/assist_email.php");
class ClientAction extends Action
{
     
     function __construct()
     {
        parent::__construct();
     }     
     /*
	 Get the actions as specified by the optional filters
	*/
     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT A.*, L.legislation_status, D.status AS deliverable_status
                                       FROM #_action A 
                                       INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
                                       INNER JOIN #_legislation L ON L.id = D.legislation AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
                                       WHERE A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." $options ");
        return $results;
     }

     function totalActions($options = "")
     {
       $results = $this -> db -> getRow("SELECT COUNT(*) AS total FROM #_action A 
                                         INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
                                         INNER JOIN #_legislation L ON L.id = D.legislation  AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
                                         WHERE A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." $options ");
       return $results;
     }
     
     function getLegislationAction($actionId)
     {
	 $sql = "SELECT A.id, A.action, A.actionstatus, A.deadline, A.action_deliverable, 
                                         A.owner, A.status, A.reminder,
                                         A.recurring, A.action_reference, L.legislation_status, L.id AS legislation_id, 
                                         D.id AS deliverable_id, A.progress
										 , ST.name AS status_name
										 , CONCAT(TK.tkname, ' ', TK.tksurname) AS user, 
                                         A.date_completed, A.attachment
                                         FROM #_action A 
                                         INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
                                         INNER JOIN #_legislation L ON L.id = D.legislation  AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
                                         LEFT JOIN #_action_status ST ON ST.id = A.status
                                         LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.owner  
                                         WHERE A.id = '".$actionId."'
                                          "; //echo $sql;
        $result = $this -> db -> getRow($sql);
		if($result['status']==0) {
			$result['status_name'] = "Unable to Comply";
		}
        return $result;                                  
     }

     function getApprovalOwners()
     {
        $result = $this -> db -> get("SELECT DISTINCT CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkid 
                                         FROM #_action A 
                                         INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
                                         INNER JOIN #_legislation L ON L.id = D.legislation  AND L.legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED."
                                         INNER JOIN #_action_status ST ON ST.id = A.status
                                         LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.owner  
                                         WHERE A.actionstatus & ".Action::AWAITING_APPROVAL." = ".Action::AWAITING_APPROVAL." 
                                          ");
        return $result;                                  
     }

     
     function fetch($id)
     {
       $result = $this -> db -> getRow("SELECT * FROM #_action A WHERE id = '".$id."' ");
       return $result;
     }
	/*
	 Get action by the specified deliverable id
	*/
	function fetchByDeliverableId( $id )
	{
	  $results = $this -> db -> get("SELECT * FROM #_action WHERE deliverable_id = '".$id."'");
	  return $results;
	}

	function fetchActionStats($whereStr)
	{
	     $results = $this -> db -> get("SELECT SUM(A.progress) AS totalProgres,
                                        AVG(A.progress) AS averageProgress,
                                        MAX(A.date_completed) AS actionCompleted 
	                                    FROM #_action A 
	                                    WHERE 1 
                                        AND (A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." 
                                        AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                              OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                              OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                                            )                           
                                         )		                                
	                                    $whereStr
	                                  ");
	    return $results;
	}	     
	/*
	  Get the current versions of the action as it is in client database
	*/
	function getCurrentVersions()
	{
		$results = $this->db->get("SELECT id, action_id, current_version, version_in_use FROM #_action_usage ");
		return $results;
	}
	/*
	  Get the current version of the action as specified by the action id filter
	*/
	function getCurrentVersion( $id )
	{
		$result = $this -> db -> getRow("SELECT id, action_id, current_version, version_in_use FROM #_action_usage 
		                                 WHERE action_id = {$id}");
		return $result;
	}
	
	function saveAction($data)
	{	
		$response        = array();
		$manageAddAction = FALSE;
		if(isset($data['actiontype']))
		{
		  if($data['actiontype'] == "manage")
		  {
		     $manageAddAction = TRUE;
		     $data['actionstatus'] = Action::MANAGE_CREATED + 1;
		  }  
		  unset($data['actiontype']);
		}
		$data['actionstatus'] = (isset($data['actionstatus']) ? $data['actionstatus'] : 1) + Action::USING_USERID;
		$deliverableObj       = new ClientDeliverable();
		$deliverable          = $deliverableObj -> fetch($data['deliverable_id']);
		if(!Validator::validStartEndDate($data['deadline'], $deliverable['legislation_deadline']))
		{
            $response = array("text" => "The action deadline date cannot be after the legislative compliance deadline date (".$deliverable['legislation_deadline'].")", "error" => true);		
		} else if(!Validator::validStartEndDate($data['reminder'], $data['deadline'])) {
		   $response = array("text" => "The remind on date cannot be after the deadline date", "error" => true);	
		} else {	
	       $res = $this -> save($data);
	       if( $res > 0) 
	       {
               if($manageAddAction)
               {
                  ActionAuditLog::processAddLog($this, $res, $data);
               }
	          $response = array("text" => "Action Saved successfully . . .", "error" => false);	
	       } else {
		     $response = array("text" => "Error saving action . . .", "error" => true);
	       }	
		}
		return $response;
	}	
	
	/*
	 Activate actions that have been confimed 
	*/
	function activateActions($id)
	{
	    $optionSql = " AND A.deliverable_id = '".$id."' AND A.actionstatus & ".Action::CONFIRMED." = ".Action::CONFIRMED." ";
        $actions   = array();
        $actions   = $this -> fetchAll($optionSql);	
        if(!empty($actions))
        {
           foreach($actions as $index => $action)
           {
              if(($action['actionstatus'] & Action::CONFIRMED) == Action::CONFIRMED)
              {
                $status = ($action['actionstatus'] - Action::CONFIRMED) + Action::ACTIVATED;
              } else {
                $status = $action['actionstatus'] + Action::ACTIVATED;
              }
              $updatedata = array("actionstatus" => $status);
              $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
           }
        }
     	
	}
	/*
	   Activate the event-driven actions so they could be available like normal actions under manage, etc
	   Occurs when a deliverable is unlocked from bieng an event deliverable
	*/
	function activateEventActions($deliverableId)
	{   
	   $actions = $this -> fetchByDeliverableId($deliverableId);
	   if(!empty($actions))
	   {	
		  foreach($actions as $aIndex => $action)
		  {
		     $updatedata = array();
		     if(($action['actionstatus'] & Action::ACTIVATED) != Action::ACTIVATED)
		     {
		        $updatedata['actionstatus'] = $action['actionstatus'] + Action::ACTIVATED;
		     }
		     //debug($updatedata);
		     if(!empty($updatedata))
		     {
		        $this -> update($action['id'], $updatedata, new ActionAuditLog(), $this, new ActionNaming());
		        //$this -> db -> updateData("action", $updatedata, array("id" => ));
		     }
		  }
	   }
	}

     /*
      Copy and save actions that's whose deliverable have been activated by an event
	*/
	function copyActionBySubEvent($newRef, $deliverableId)
	{
	   $actions = $this -> fetchByDeliverableId($deliverableId);
	   if(!empty($actions))
	   {	
		  foreach($actions as $aIndex => $action)
		  {			
	         $action['actionstatus'] = $action['actionstatus'] + Action::CREATEDBY_SUBEVENT;
	         $action['progress']     = 0;
	         $action['status']       = 1;
		     $action['deliverable_id'] = $newRef;
		     unset($action['id']);				
		     $id = $this -> db -> insert("action", $action);
		  }
	   }
	}	
	
	/*
	 Copy actions from one financial year to anothere
	*/
	function copyActionSave($deliverableRef, $deliverableId, $finObj, $financial_year, $actionObj)
	{
	   $actions = $actionObj -> fetchByDeliverableId($deliverableRef);  
	   $userObj = new User();
	   $users   =  $userObj -> getAll(); 
	   $res     = 0; 
	   foreach($actions as $index => $action)
	   {
		if(  ( ($action['actionstatus'] & Action::DELETED) != Action::DELETED ) && ( ($action['actionstatus'] & Action::CREATEDBY_SUBEVENT) != Action::CREATEDBY_SUBEVENT )  ) {
		      $action['deliverable_id'] = $deliverableId;
		      $action['deadline']       = $finObj -> createFinancialYearDate($financial_year, $action['deadline']);
		      if(($action['actionstatus'] & Action::ACTIVATED) == Action::ACTIVATED) {
	       	 $action['actionstatus']   = $action['actionstatus'] - Action::ACTIVATED;
		      }
		      if(($action['actionstatus'] & Action::CONFIRMED) == Action::CONFIRMED) {
		         $action['actionstatus']   = $action['actionstatus'] - Action::CONFIRMED;
		      }
		      if(($action['actionstatus'] & Action::AWAITING_APPROVAL) == Action::AWAITING_APPROVAL) {
		         $action['actionstatus']   = $action['actionstatus'] - Action::AWAITING_APPROVAL;
		      }
		      if(($action['actionstatus'] & Action::DECLINED) == Action::DECLINED) {
		         $action['actionstatus']   = $action['actionstatus'] - Action::DECLINED;
		      }
		      if(($action['actionstatus'] & Action::APPROVED) == Action::APPROVED) {
		         $action['actionstatus']   = $action['actionstatus'] - Action::APPROVED;
		      }
		      if(isset($users[$action['owner']])) {
		         if(($action['actionstatus'] & Action::USING_USERID) != Action::USING_USERID) {
		            $action['actionstatus'] = $action['actionstatus'] + Action::USING_USERID;
		         }   
		      }
	      
		      $action['status']   = 1;
		      $action['progress'] = 0;
			$action['reminder'] = "";
			$action['date_completed'] = "";
			$action['attachment'] = "";
		      unset($action['id']);  
		      $actionRes = $this -> save($action);
		      if($actionRes > 0) {
		        $res ++;
		      }
		}
        }
        return $res;
	}
	/*
	  Copy an action from another action
	*/
	function copySaveAction($data)
	{
	   $response             = array();
	   $clientDeliverableObj = new ClientDeliverable();
	   $deliverable          = $clientDeliverableObj -> fetch($data['deliverable_id']);
	   if(isset($data['actiontype']))
	   {
		  if($data['actiontype'] == "manage")
		  {
		     $manageAddAction = TRUE;
		     $data['actionstatus'] = Action::MANAGE_CREATED + 1;
		  }  
		  unset($data['actiontype']);
	   }
	   // if remind on date is greater than the deadline date
	   if(!Validator::validStartEndDate($data['deadline'], $deliverable['legislation_deadline']))
	   {
	     $response = array("text" => "The action deadline date cannot be after the deliverable deadline date ".$deliverable['legislation_deadline'], "error" => true);
	   } else if(!Validator::validStartEndDate($data['reminder'], $data['deadline'])) {
		 $response = array("text" => "The remind on date cannot be after the deadline date ".$data['deadline'], "error" => true);
	   } else {		
		  $res = $this -> save($data);
		  if($res > 0) 
		  {
		     $response = array("text" => "Action copied and saved successfully . . .", "error" => false);	
		  } else {
		     $response = array("text" => "Error saving action . . .", "error" => true);
		  }		
	   }
	  return $response;
	}	

	function deleteAction($data)
	{
	   $res 	  = $this -> update($data['id'], $data, new ActionAuditLog(), $this, new ActionNaming());
	   $response = array();
	   if($res > 0)
	   {
		$response = array("text" => "Action deleted", "error" => false, "updated" => true );
	   } else if($res == 0){
		$response = array("text" => "There was no change made to the action", "error" => false, "updated" => true);
	   } else {
		$response = array("text" => "Error deleting the action", "error" => true, "updated" => false);
	   }
	   return $response;
	}
		
	function editAction($data)
	{    
	   $error                = FALSE;
	   $response             = "";
	   $clientDeliverableObj = new ClientDeliverable();
	   $deliverable          = $clientDeliverableObj -> fetch($data['deliverable_id']);
	   if(!Validator::validStartEndDate($data['deadline'], $deliverable['legislation_deadline']))
	   {
	     $response = array("text" => "Action deadline cannot be after deliverable deadline ".$deliverable['legislation_deadline'], "error" => true);
	   } else if(!Validator::validStartEndDate($data['reminder'], $data['deadline'])) {
	     $response = array("text" => "Action reminder cannot be after action deadline date ".$data['deadline'], "error" => true);
	   } else {
	     $res 	  = $this -> update($data['id'], $data, new ActionAuditLog(), $this, new ActionNaming());
	     $response = array();
	     if($res > 0)
	     {
		    $response = array("text" => "Action updated", "error" => false, "updated" => true );
	     } else if( $res == 0) {
		    $response = array("text" => "There was no change made to the action", "error" => false, "updated" => false);
	     } else {
		    $response = array("text" => "Error saving the action", "error" => true, "updated" => false);
	     }	 
	   }
        return $response;
	}
	
	function uploadAttachment($action_id)
	{
	    $attObj = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id); 
	    $results  = $attObj -> upload('action_'.$action_id);
        echo json_encode($results);
        exit();
	}
	
	function downloadFile($file, $name)
	{
	   $ext = substr($file, strpos($file, ".") + 1);
       header("Content-type:".$ext);
       header("Content-disposition: attachment; filename=".$name);
       readfile("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$file);  
	   exit();
	}
	
	function removeAttachment($file, $ext, $action_id)
	{
	    $file     = $file.".".$ext;
	    $attObj   = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
	    $response = $attObj -> removeFile($file, 'action_'.$action_id);
        echo json_encode($response);
        exit();
	}

	function deleteAttachment($file, $ext, $action_id)
	{
	   $attObj   = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
	   $response = $attObj -> deleteFile($file, 'action_'.$action_id);
       echo json_encode($response);
       exit();
	}
	
	function updateAction( $data )
	{
		$action     = $this -> fetch($data['id']);
		$action_id = $data['id'];
		if(!Validator::validStartEndDate($data['reminder'], $action['deadline']))
		{
	       $response = array("text" => "Action reminder cannot be after action deadline date ".$action['deadline'], "error" => true);
		} else {
	        /*$accountableObj = new AccountablePersonMatch(); 
	        $user_sql       = " AND APD.deliverable_id = '".$action['deliverable_id']."' ";
	        $accountables   = $accountableObj -> getUserAccountableDeliverable($user_sql);
	        */
	        $deliverableObj = new ClientDeliverable();
	        $deliverable    = $deliverableObj -> fetch($action['deliverable_id']);     
	        
	        $deliverable_action_owners = array();
	        $userObj                   = new ModuleUser();
	        /*if(!empty($accountables)) {
	            foreach($accountables as $index => $user_accountable) {
	                $users[$user_accountable['user_id']] = $userObj -> getUser(" AND TK.tkid = '".$user_accountable['user_id']."' ");
	            }
	        }*/
	        if(!empty($deliverable)) {
	            $users[$deliverable['responsibility_owner']] = $userObj -> getUser(" AND TK.tkid = '".$deliverable['responsibility_owner']."' ");
	        }
	        $email_to = array();
	        if(!empty($users)) {
	            $email_to_arr = array();
	            foreach($users as $u_index => $user) {
	               $email_to[] = array('name'=>$user['user'],'email'=>$user['email']);
	            }
	        }
		   $updatedata             = array();
		   $updatedata['progress'] = $data['progress'];
		   $updatedata['status']   = $data['status'];
		   $updatedata['reminder'] = $data['reminder'];
		   $updatedata['date_completed'] = $data['date_completed'];
		   if($data['status']=="3" && $data['progress']=="100" && isset($data['req_approval']) && $data['req_approval'] == 1) {

				$deliverable_id = $action['deliverable_id']; 
				$delObj = new ClientDeliverable();
				$deliverable = $delObj->fetch($deliverable_id);
				$legislation_id = $deliverable['legislation'];	
				$legObj 	 = new ClientLegislation();
				$legislation = $legObj -> findById($legislation_id);
				$financialYearObj = new FinancialYear();
				$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);
				
				$fromuser = $userObj->getUser(" AND TK.tkid = '".$_SESSION['tid']."' ");
				$sender = array('name'=>$user['user'],'email'=>$user['email']);
				$message = "";
			    $message.= $_SESSION['tkn']." updated Action ".Action::REFTAG.$data['id']." related to Deliverable ".Deliverable::REFTAG.$action['deliverable_id'].": \r\n";
			    $message.= " \r\n";
			    $message.= "<b>Financial Year:</b> ".$financialyear['value']." \r\n";
			    $message.= "<b>Legislation:</b> ".$legislation['name']." \r\n";
			    $message.= "<b>Deliverable:</b> ".$deliverable['short_description']." \r\n";
			    $message.= "<b>Action:</b> ".$action['action']." \r\n";
			    $message.= "<b>Response:</b> ".$data['response']." \r\n";
			    $message.= "<b>Progress:</b> ".$data['progress']."% \r\n";
			    $message.= "<b>Status:</b> Completed \r\n";
			    $message.= " \r\n";
			    $message.= "Please log onto Ignite Assist in order to view the Action.\n";
			    $message = nl2br($message);			
				$message = "<p>".$message."</p>";
				$subject = "Request Approval of Completed Action ".Action::REFTAG.$action_id;
				$email = new ASSIST_EMAIL($email_to,$subject,$message,"HTML");
				$email->setSender($sender);
			    if( $email->sendEmail() ) {
				    $etext = "Notification email has been sent.";
			    } else {
				    $state = true;
				    $etext = "There was an error sending the notification email.";
			    }
		   }
		   		   
		   if( ($action['actionstatus'] & Action::AWAITING_APPROVAL) != Action::AWAITING_APPROVAL)
		   {
		     if($data['progress'] == "100" || $data['status'] == "3")
		     {
		       $updatedata['date_completed'] = (!empty($data['date_completed']) ? $data['date_completed'] : date("d-M-Y") );   
			   $updatedata['actionstatus'] = $action['actionstatus'] + Action::AWAITING_APPROVAL;
		     }		     
		   }
		   /*if(isset($_SESSION['uploads']['cmpladded']))
		   {
		      if(isset($_SESSION['uploads']['cmpladded']['action_'.$data['id']]))
		      {
			    $updatedata['attachment'] = base64_encode(serialize($_SESSION['uploads']['cmpladded']['action_'.$data['id']]));
		      }
		   }*/
		   
		   
		   $res 	  = $this -> update($data['id'], $updatedata, new ActionAuditLog(), $this, new ActionNaming());	   
		   /*if(isset($_SESSION['uploads']))
		   {
		     if(isset($_SESSION['uploads']['cmpladded']))
		     {
		        if(isset($_SESSION['uploads']['cmpladded']['action_'.$data['id']]))
		        {
		            unset($_SESSION['uploads']['cmpladded']['action_'.$data['id']]);
		        }   
		     }
		     if(isset($_SESSION['uploads']['cmpldeleted']))
		     {
		        if(isset($_SESSION['uploads']['cmpldeleted']['action_'.$data['id']]))
		        {
		            unset($_SESSION['uploads']['cmpldeleted']['action_'.$data['id']]);
		        }   
		     }		     
		     if(isset($_SESSION['uploads']['actionchanges']))
		     {
		        unset($_SESSION['uploads']['actionchanges']);
		     }
		     if(isset($_SESSION['uploads']['attachments']))
		     {
		        unset($_SESSION['uploads']['attachments']);
		     }
		   }*/
	   
		   $response = array();
		   if( $res > 0 || strlen($data['response'])>0)  {
				$display = ASSIST_HELPER::getDisplayResult(array("ok","Action updated successfully"));
				$display = str_replace(chr(10),"",$display);
			    $response = array("text" => "Action updated successfully. ".(isset($etext) ? $etext:""), "error" => false, "updated" => true , 'display'=>$display);
		   } else if( $res == 0){
				$display = ASSIST_HELPER::getDisplayResult(array("info","No changes made."));
				$display = str_replace(chr(10),"",$display);
			    $response = array("text" => "There was no change made to the action", "error" => false, "updated" => true, 'display'=>$display);
		   } else {
				$display = ASSIST_HELPER::getDisplayResult(array("error","An error occurred.  Please try again."));
				$display = str_replace(chr(10),"",$display);
			    $response = array("text" => "Error saving the action", "error" => true, "updated" => false, 'display'=>$display);
		   }		
		}
		return $response;
	}	
	/*
	 Approve the completed actions and awaiting approval
	*/
	function approveAction( $data )
	{		
       $action     = $this -> fetch($data['id']);		
	   $updatedata = array();
	   if(($action['actionstatus'] & Action::APPROVED) != Action::APPROVED)
	   {
	      if(($action['actionstatus'] & Action::AWAITING_APPROVAL) == Action::AWAITING_APPROVAL)
	      {
    	    //remove the awaiting approval status
    	    
	        $updatedata['actionstatus'] = ($action['actionstatus'] - Action::AWAITING_APPROVAL) + Action::APPROVED;
	      }
	   }
	   $res 	  = $this -> update($data['id'], $updatedata, new ActionAuditLog(), $this, new ActionNaming());
	   $response = array();
	   if( $res > 0)
	   {
		$response = array("text" => "Action A".$data['id']." approved", "error" => false, "updated" => true );
	   } else if($res == 0){
		$response = array("text" => "There was no change made to the action A".$data['id'], "error" => false, "updated" => true);
	   } else {
		$response = array("text" => "Error approving the action A".$data['id'], "error" => true, "updated" => false);
	   }
        return $response;
	}
	
	function declineAction( $data )
	{
	   $action     = $this -> fetch($data['id']);		
	   $updatedata = array();
	   //unsetting or removing the awaiting approval status
	   $updatedata['actionstatus'] = ($action['actionstatus'] - Action::AWAITING_APPROVAL);
	   $updatedata['status'] 	   = 2;
	   $updatedata['progress'] 	   = 99;
	   //adding the approved status
	   $res 	  = $this -> update($data['id'], $updatedata, new ActionAuditLog(), $this, new ActionNaming());
	   $response = array();
	   if($res > 0)
	   {
			$text = "";
			if(strlen($action['owner'])>0) {
				$deliverable_id = $action['deliverable_id']; 
				$delObj = new ClientDeliverable();
				$deliverable = $delObj->fetch($deliverable_id);
				$legislation_id = $deliverable['legislation'];	
				$legObj 	 = new ClientLegislation();
				$legislation = $legObj -> findById($legislation_id);
				$financialYearObj = new FinancialYear();
				$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);

				$userObj = new ModuleUser();
				$user = $userObj->getUser(" AND TK.tkid = '".$action['owner']."' ");
				$to = array('name'=>$user['user'],'email'=>$user['email']);
				$user = $userObj->getUser(" AND TK.tkid = '".$_SESSION['tid']."' ");
				$sender = array('name'=>$user['user'],'email'=>$user['email']);
			    $subject  = "Action ".Action::REFTAG.$data['id']." Declined";
			    $message.= $_SESSION['tkn']." declined Action ".Action::REFTAG.$data['id']." related to Deliverable ".Deliverable::REFTAG.$action['deliverable_id'].": \r\n";
			    $message.= " \r\n";
			    $message.= "<b>Financial Year:</b> ".$financialyear['value']." \r\n";
			    $message.= "<b>Legislation:</b> ".$legislation['name']." \r\n";
			    $message.= "<b>Deliverable:</b> ".$deliverable['short_description']." \r\n";
			    $message.= "<b>Action:</b> ".$action['action']." \r\n";
			    $message.= " \r\n";
			    $message.= "<b>Response:</b> ".$data['response']." \r\n";
			    $message.= " \r\n";
				 //blank row above Please log on line
			    $message.= "Please log onto Ignite Assist in order to view the action .\n";
			    $message           = nl2br($message);	
				$message = "<p>".$message."</p>";
				$email = new ASSIST_EMAIL($to,$subject,$message,"HTML");
				$email->setSender($sender);
				//$er = $email->sendEmail();
			    if( $email -> sendEmail() ) {
				    $text = "User has been notified.";
			    } else {
				    $text = "There was an error notifying the user.";
				}
			}
		$response = array("text" => "Action A".$data['id']." declined.  ".$text, "error" => false, "updated" => true );
	   } else if($res == 0){
		$response = array("text" => "There was no change made to the action A".$data['id'], "error" => false, "updated" => true);
	   } else {
		$response = array("text" => "Error declining Action A".$data['id'], "error" => true, "updated" => false);
	   }
	   return $response;
      }

	function unlockAction( $data )
	{
	   $action     = $this -> fetch($data['id']);		
	   $updatedata = array();
	   //unsetting or removing the awaiting approval status
	   $updatedata['actionstatus'] = ($action['actionstatus'] - Action::APPROVED);
	   $updatedata['status'] 	   = 2;
	   $updatedata['progress'] 	   = 99;
	   //adding the approved status
	   $res 	  = $this -> update($data['id'], $updatedata, new ActionAuditLog(), $this, new ActionNaming());
	   $response = array();
	   if($res > 0)
	   {
		$response = array("text" => "Action A".$data['id']." unlocked ", "error" => false, "updated" => true );
	   } else if($res == 0){
		$response = array("text" => "There was no change made to the action A".$data['id'], "error" => false, "updated" => true);
	   } else {
		$response = array("text" => "Error unlocking the action A".$data['id'], "error" => true, "updated" => false);
	   }
	   return $response;
      }
	  
     function reAssign($data, $actionstatuses)
     {	
        $res 	  = 0;
        $response = array();
        if(!empty($data))
        {
          foreach($data as $index => $owner)
          {
          	 $updatadata  		  = array();
          	 $updatedata['owner'] = $owner['value'];
             $actionid    		  = substr($owner['name'], 9);
             if(isset($actionstatuses[$actionid]))
             {
             	$action_status = $actionstatuses[$actionid];
             	if(($action_status & Action::USING_USERID) != Action::USING_USERID)
             	{
             		$updatedata['actionstatus'] = $action_status + Action::USING_USERID;
             	}
             } 
             $res 	  += $this -> update($actionid, array("owner" => $owner['value']), new ActionAuditLog());               
             unset($updatedata);
          }
        }        
        if($res > 0)
        {
          $response = array("text" => "Action reassignment was successfull", "error" => false);
        } else if($res == 0){
          $response = array("text" => "No change was made to the actions", "error" => false, "updated" => true);
        } else {
          $response = array("text" => "Error occured re-assigning actions", "error" => false);
        }
       return $response;
     }	
     /*
      Update action owner status so that the action is now using client users 
     */
	function updateActionOwnerStatus($action)
	{
	  if((Action::USING_USERID & $action['actionstatus']) != Action::USING_USERID)
	  {
	     $status = "";
	     if((Action::USING_MATCHEDUSERID & $action['actionstatus']) == Action::USING_MATCHEDUSERID)
	     {
	       $status = ($action['actionstatus'] - Action::USING_MATCHEDUSERID) + Action::USING_USERID;
	     } else {
	       $status = $action['actionstatus'] + Action::USING_USERID;
	     }
	     $updatedata = array("actionstatus" => $status);
	     $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
	     return $status;
	  }
	}
	/*
	  Get the actions statitics as specified by the action filters 
	*/
	 function getActionProgressStatitics($optionSql = "")
	{
		$result = $this -> db -> getRow("SELECT SUM(A.progress) AS totalProgress, AVG(A.progress) AS averageProgress, 
		                                 COUNT(A.id) AS totalActions
                                           FROM #_action A 
                                           INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
                                           INNER JOIN #_legislation L ON L.id = D.legislation
                                           WHERE A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." $optionSql 
		                               ");
		return $result;
	}
	
	
	function updateActionOwnerMatch($ref, $userId)
	{
	    $optionSql = " AND A.owner = '".$ref."' AND A.actionstatus & ".Action::USING_USERID." <> ".Action::USING_USERID." ";
	    $actions = $this -> fetchAllActions($optionSql);
	    if(!empty($actions))
	    {
	      foreach($actions as $index => $action)
	      {
	         $status = $action['actionstatus'] + Action::USING_MATCHEDUSERID;
	         $updatedata = array("actionstatus" => $status, "owner" => $userId);
	         $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
	      }
	    }
	}
	
	function undoActionOwnerMatch($refId, $userId)
	{
	   $optionSql = " AND A.owner = '".$userId."' 
	                  AND A.actionstatus & ".Action::USING_MATCHEDUSERID." = ".Action::USING_MATCHEDUSERID." ";
	   $actions   = $this ->  fetchAllActions($optionSql);
	   if(!empty($actions))
	   {
	     foreach($actions as $index => $action)
	     {
	        $status     = $action['actionstatus'] - Action::USING_MATCHEDUSERID;
	        $updatedata = array("actionstatus" => $status, "owner" => $refId);
	        $this -> db -> updateData("action", $updatedata, array("id" => $action['id']));
	     }
	   }
	}

	function generateReport($reportData, Report $reportObj)
	{

       $actionManagerObj = new ActionReport();
       $joinWhere 		 = $this -> createJoinsWhere($reportData['value']);
       $include_result	 = false;
       if(isset($reportData['actions']['action_result']))
       {	
       	   $include_result = true;
       	   unset($reportData['actions']['action_result']);
       }
	   $fields    					 = $reportObj -> prepareFields($reportData['actions'], "A");
	   $groupBy   					 = $reportObj -> prepareGroupBy($reportData['group_by'], "A"); 
	   $sortBy    				     = $reportObj -> prepareSortBy($reportData['sort'], "A");
	   $whereStr  					 = $reportObj -> prepareWhereString($reportData['value'], $reportData['match'], "A");   
	   $whereStr 					.= $joinWhere['where'];
	   $actions    					 = $this -> filterActions($fields, $whereStr, $groupBy, $sortBy, $joinWhere['joins']);
	   $reportData['actions']['action_result'] = $include_result;
	   $repActions 					 = $actionManagerObj -> sortActions($actions, $reportData['actions']); 
	   $repActions['title'] 		 = $reportData['report_title'];
       $reportObj -> displayReport($repActions, $reportData['document_format'], "actions");     
	}
	
     function filterActions($fields, $whereStr = "", $groupBy = "", $sortBy = "", $joins = "")
     {
	     $orderBy = (!empty($sortBy) ? "ORDER BY $sortBy " : "");
	     $groupBy = (!empty($groupBy) ? "GROUP BY $groupBy " : "");
		 $results = $this -> db -> get("SELECT A.id, A.actionstatus, A.status AS action_status, 
		                                A.progress AS action_progress, A.date_completed AS datecompleted, 
		 								D.legislation_deadline, A.deadline AS action_deadline, ".$fields." 
						                FROM #_action A
						                INNER JOIN #_deliverable D ON D.id = A.deliverable_id 
						                INNER JOIN #_legislation L ON L.id = D.legislation
						                $joins
						                WHERE 1 
	                                    AND A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." 
                                        AND 
                                        (
                                           (A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                            OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                            OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                                           ) OR (
                                             A.actionstatus & ".Action::CONFIRMED." = ".Action::CONFIRMED." AND 
                                             L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED."
                                           )
                                        )                           				             				                            AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
                                        AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED."
						                $whereStr
						                $groupBy
						                $orderBy
					            ");
		return $results;		
     }	
     
     function createJoinsWhere($options)
     {
         $whereStr = "";
         $joinStr  = "";
         if(isset($options['owner']) && !empty($options['owner']))
         {
            $ownerWhere = "";
            foreach($options['owner'] as $index => $owner)
            {
               if($owner !== "all")
               {
                  $ownerWhere .= " A.owner = '".$owner."' OR";
               }
            }
            if(!empty($ownerWhere))
            {
               $whereStr = " AND (".rtrim($ownerWhere, "OR").")";
            }           
         }
         if(isset($options['status']) && !empty($options['status']))
         {
            $allowStatus = 0;
            $statusWhere = "";
            foreach($options['status'] as $index => $status)
            {
               if($status != "all")
               {
                 $statusWhere .=  "  A.status = '".$status."'  OR";
                 $allowStatus += 1;
               }
            }
            if($allowStatus > 0)
            {
               $whereStr .= " AND (".rtrim($statusWhere, "OR").") ";
               $joinStr .= " INNER JOIN #_action_status AST ON AST.id = A.status ";
            }
         }
        $data['joins']  = $joinStr;
        $data['where']  = $whereStr; 
        return $data;
     }
		
    function getActionUpdateLog($action_id)
    {
       $action      = $this -> db -> getRow("SELECT * FROM #_action WHERE id = '".$action_id."' ");
       $action_logs = $this -> db -> get("SELECT * FROM #_action_update WHERE action_id = '".$action_id."' ");
       $table       = $this->drawActivityLog($action,$action_logs,$action_id);
	   return $table;
	}
	function getActionViewLog($action_id) {
       $action      = $this -> db -> getRow("SELECT * FROM #_action WHERE id = '".$action_id."' ");
	   $action_logs_merge = array();
       $action_update_logs = $this -> db -> get("SELECT * FROM #_action_update WHERE action_id = '".$action_id."' ");
	   $action_edit_logs =  $this -> db -> get("SELECT * FROM #_action_edit WHERE action_id = '".$action_id."' ");
	   foreach($action_update_logs as $l) {
			$l['sort_fld'] = strtotime($l['insertdate']);
			$key = $l['sort_fld'];
			while(isset($action_logs_merge[$key])) { $key++; }
			$action_logs_merge[$key] = $l;
			
	   }
	   foreach($action_edit_logs as $l) {
			$l['sort_fld'] = strtotime($l['insertdate']);
			$key = $l['sort_fld'];
			while(isset($action_logs_merge[$key])) { $key++; }
			$action_logs_merge[$key] = $l;
	   }
	   ksort($action_logs_merge);
       $table       = $this->drawActivityLog($action,$action_logs_merge,$action_id);
	   return $table;
	}
	function drawActivityLog($action,$action_logs, $action_id=0) {
		$nObj = new ActionNaming();
		$headers = $nObj-> getNaming(); 
		//ASSIST_HELPER::arrPrint($headers);
       $action_attachments = array();
		$table = "";
       if(!empty($action_logs))
       {
			$current_progress = ASSIST_HELPER::format_percent(0,0);
         foreach($action_logs as $log_id => $action_log)
          {           
            $current_status = "";
            $user           = "";
            $change_str     = "";
            $table .= "<tr>";
              $table .= "<td>".date("d-M-Y H:i:s", strtotime($action_log['insertdate']))."</td>";          
               if(!empty($action_log['changes']))
                {
                   $changes = unserialize(base64_decode($action_log['changes']));
                   if(!empty($changes))
                   {
                      
                      if(isset($changes['action_status']))
                      {
                         $change_str .= $changes['action_status']."<br />";
                         unset($changes['action_status']);
                      }                        
                      if(isset($changes['user']))
                      {
                        $user  = $changes['user'];
                        unset($changes['user']);
                      }                      
                      if(isset($changes['currentstatus']))
                      {
                        $current_status  = $changes['currentstatus']."<br />";
                        unset($changes['currentstatus']);
                      }
                      if(isset($changes['response']))
                      {
                        $change_str  .= "Added response <span class=i>".$changes['response']."</span><br />";
                        unset($changes['response']);
                      }                    
                      if(isset($changes['attachments']) && !empty($changes['attachments']))
                      {
                        foreach($changes['attachments'] as $a_index => $att_str)
                        {
                           $change_str  .= $att_str;//."  last word ".$last_word."<br />";
                        }
                        unset($changes['attachments']);
                      }                  
                      if(!empty($changes))
                      {
                        foreach($changes as $log_key => $change)
                        {
                          if(isset($change['to']) && isset($change['from']))
                          {
                            $from        = $change['from'];
                            $to          = $change['to'];
							$key = "";
							if(isset($headers[$log_key])) {
								$key = strlen($headers[$log_key]['client_terminology'])>0 ? $headers[$log_key]['client_terminology'] : $headers[$log_key]['ignite_terminology'];
							} else {
								$key         = ucwords(str_replace("_", " ", $log_key));
							}
							 if($log_key == "progress") {
								$current_progress = ASSIST_HELPER::format_percent($change['to']*1,0);
								$from = ASSIST_HELPER::format_percent($from*1,0);
								$to = ASSIST_HELPER::format_percent($to*1,0);
							 }
                             $change_str .= $key."  changed to <span class=i>".$to."</span> from <span class=i>".$from."</span><br />";
                          }
                        }
                      }
                   } 
                }    
             $table .= "<td>".$user."</td>";        
             $table .= "<td>";      
                $table .= $change_str;    
              $table .= "</td>";
              $table .= "<td>".$current_status." (".$current_progress.")</td>";
            $table .= "</tr>";
          }
       }
	   if($action_id>0) {
			require_once '../../library/class/assist_helper.php';
			require_once '../../library/class/assist.php';
			require_once '../../library/class/assist_dbconn.php';
			require_once '../../library/class/assist_db.php';
			require_once '../../library/class/assist_module_helper.php';
			require_once '../class/compl.php';
			require_once '../class/compl_action.php';
				$compl = new COMPL_ACTION();
				$attach_display = $compl->displayAttachmentList($action_id);
				if(strlen($attach_display)>0) {
					$table.= "<tr><td colspan=4>".$attach_display."</td></tr>";
				}
		}
		
/*       if(!empty($action['attachment']))
       {
          $attachments = unserialize(base64_decode($action['attachment']));
		if(count($attachments)>0) {
          $table .= "<tr>";
          $table .= "<td colspan='4'><b>Action Attachments</b>";
          $dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action";
		  $a = 0;
             if(!empty($attachments))
             {
/*	            $table .= "<span id='result_message'></span><ul id='attachment_list'>";
	            foreach($attachments as $file => $name) 
	            {
	                $id = substr($file,0,strpos($file, ".")-1);
	                $_id = substr($file,0,strpos($file, "."));
	                $ext = substr($file, strpos($file, ".") + 1);		
	                if(file_exists($dir."/".$file))
	                {
						$a++;
	                   //$table .= "<li id='li_".trim($_id)."'><span id='parent_".$_id."'><a href='../class/request.php?action=ClientAction.downloadFile&file=".$file."&name=".$name."'>".$name."</a>&nbsp;&nbsp;</li>";
	                   $table .= "<li id='li_".trim($_id)."'><span id='parent_".$_id."'><a href='#' class=downloadAttach filen='".$file."' fname='".$name."'>".$name."</a>&nbsp;&nbsp;</li>";
	                }
	            } 
                $table .= "</ul>";
              }
			  if($a>0) {
				$table.="
				<script type=text/javascript>
					$(function() {
						$('a.downloadAttach').click(function() {
							var fn = $(this).attr('filen');
							var n = $(this).attr('fname');
							url = '../class/request.php?action=ClientAction.downloadFile&file='+fn+'&name='+n;
							document.location.href = url;
						});
					});
				</script>";
			  }
          $table .= "</td>";  
          $table .= "</tr>"; 
		}
       }  */
       return $table;
    }
    
    /*
      Update the action status of the actions whose deliverable was an event-activated, now none-event
    */
    function updateEventDeliverableActionStatus($deliverableId)
    {
        $eventActivatedActions = $this -> fetchAll(" AND A.deliverable_id = '".$deliverableId."' ");
        $res                   = 0;
        if(!empty($eventActivatedActions))
        {
          foreach($eventActivatedActions as $index => $action)
          {
             $actionstatus = $action['actionstatus'];
             if(($action['legislation_status'] & Legislation::ACTIVATED) == Legislation::ACTIVATED)
             {
                if(($action['legislation_status'] & Action::CONFIRMED) == Action::CONFIRMED)
                {
                    $actionstatus = $actionstatus - Action::CONFIRMED;    
                }
                $actionstatus = $actionstatus + Action::ACTIVATED;
             } else if(($action['legislation_status'] & Legislation::CONFIRMED) == Legislation::CONFIRMED)
             {
                $actionstatus = $actionstatus + Action::CONFIRMED;
             }
             $res += $this -> update($action['id'], array('actionstatus' => $actionstatus), new ActionAuditLog());             
          }        
        }
        return $res;
    }
    
    /*
        Update the status of the actions so that they can now be activated by sub-events of deliverables
    */
    function updateNonEventDeliverableActionStatus($deliverableId)
    {
        $action_sql = " AND A.deliverable_id = '".$deliverableId."' 
                        AND A.actionstatus & ".Action::CREATEDBY_SUBEVENT." <> ".Action::CREATEDBY_SUBEVENT." ";
        $nonEventActivatedActions = $this -> fetchAll($action_sql);
        $res                   = 0;
        if(!empty($nonEventActivatedActions))
        {
            foreach($nonEventActivatedActions as $index => $action)
            {
               $actionstatus = $action['actionstatus'];
               if(($action['legislation_status'] & Legislation::ACTIVATED) == Legislation::ACTIVATED)
               {
                    
                   if(($action['legislation_status'] & Action::CONFIRMED) == Action::CONFIRMED)
                   {
                      $actionstatus = $actionstatus - Action::CONFIRMED;    
                   }
                   if(($action['actionstatus'] & Action::ACTIVATED) == Action::ACTIVATED)
                   {
                      $actionstatus = $actionstatus - Action::ACTIVATED;
                   }
               } else if(($action['legislation_status'] & Legislation::CONFIRMED) == Legislation::CONFIRMED)
               {
                    $actionstatus = $actionstatus - Action::CONFIRMED;
               }
               $res += $this -> update($action['id'], array('actionstatus' => $actionstatus), new ActionAuditLog());   
            }
        }
       return $res;
    }
						     
}
?>