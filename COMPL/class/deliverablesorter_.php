<?php
//implements the sorter interface to sort the deliverables headers
class DeliverableSorter implements ISortable
{
   
   function sortHeaders( $data , Naming $naming, $page = "")
	{
		   $headers  = $naming->headersList( $page );
		   $setupObj = new SetupManager();
		   $response = array();
		   if( isset($data) && !empty($data))
		   {
			   foreach($data as $index => $dataValue){
				   foreach( $headers as $key => $header)
				   {
					   if( $key == "action_progress"){
						   $this->headers[$key]	 			= $header;
						   $response[$dataValue['ref']][$key]  = "0";						
					   } elseif(array_key_exists($key, $dataValue))
					   {	
						   $this->headers[$key]	 			= $header;
                      if( $key == "sub_event"){
							   $subevents =  $setupObj -> getSubEvents();
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $subevents , $dataValue[$key] );
						   } else if( $key == "responsibility_owner"){
							   $responsibleowners =  $setupObj -> getResponsibleUsers();
							   $response[$dataValue['ref']][$key]  = $this->multipleSortUsers( $responsibleowners , $dataValue[$key] );
						   } else if( $key == "compliance_name_given_to"){
							   $compliancesto =  $setupObj -> getComplianceTo();
							    $compliancesTo = array();
							    foreach($compliancesto as $i => $valArr)
							    {
							    	$compliancesTo[$i] = array("id" => $valArr['ref_id'], "name" => $valArr['user'], "status" => $valArr['status'], "user_id" => $valArr['user_id'] ); 
							    }							
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $compliancesTo , $dataValue[$key] );	
						   } else if( $key == "main_event"){
							   $mainevents =  $setupObj -> getEvents();
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $mainevents, $dataValue[$key] );							
						   } else if( $key == "frequency_of_compliance"){
							   $compliancefrequency=  $setupObj -> getComplianceFrequencies();
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $compliancefrequency, $dataValue[$key] );						
						   } else if( $key == "applicable_organisation_type"){
							   $apporgtypes =  $setupObj -> getOrganisationTypes();
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $apporgtypes, $dataValue[$key] );						
						   } else if( $key == "responsible_department"){
					          /*  $dir 	   				 = new Directorate();
					         	$responsibledepartments  = $dir ->getSubDirectorates();
					         	$depts = "";
					         	foreach($responsibledepartments as $i => $valArr)
					         	{
					         		if( $valArr['subid'] == $dataValue[$key])
					         		{
					         			$depts = $valArr['dirtxt'];
					         		}	
					         	}
					         	*/
							   $response[$dataValue['ref']][$key]  = "";				
		
						   } else if( $key == "functional_service"){
							   $functionalservices =  $setupObj -> getFunctionalService();
							    $functionalServices = array();
							    foreach($functionalservices as $i => $valArr)
							    {
							    	$functionalServices[$i] = array("id" => $valArr['ref_id'], "name" => $valArr['user'], "status" => $valArr['status'], "user_id" => $valArr['user_id'] ); 
							    }											
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $functionalServices, $dataValue[$key] );						
						   } else if( $key == "accountable_person"){
							   $accountablepersons = $setupObj -> getAccountablePersons();
							   $accountablePersons = array();
							   foreach($accountablepersons as $i => $valArr)
							    {
							    	$accountablePersons[$i] = array("id" => $valArr['ref_id'], "name" => $valArr['user_name'], "status" => $valArr['status'], "user_id" => $valArr['user_id'] ); 
							    }															
							   $response[$dataValue['ref']][$key]  = $this->multipleSort( $accountablePersons, $dataValue[$key] );						
						   } else {						
							   $this->headers[$key]	 			= $header;
							   $response[$dataValue['ref']][$key]  = $dataValue[$key];
						   }
					   }		
				   }
			   }	
		   } else {
			   $this->headers = $headers;
		   }
		   return array("headers" => $this->headers, "columns" => count($this->headers) , "data" => $response );
	   }

	function multipleSort( $data, $match)
	{
		$results = "";
		if( $match != ""){
			$matchArr = explode(",", $match);

			foreach( $data as $index => $valArr)
			{
				if(in_array($valArr['id'], $matchArr)){
					$results .= $valArr['name'].",";
				}	
			}
			$results = rtrim($results, ",");			
		}
		return $results;
	}
	
	function multipleSortUsers( $data , $match)
	{
		$results = "";
		if( $match != ""){
			$matchArr = explode(",", $match);

			foreach( $data as $index => $valArr)
			{
				if(in_array($valArr['ref'], $matchArr)){
					$results .= $valArr['user'].",";
				}	
			}
			$results = rtrim($results, ",");			
		}
		return $results;
	}

}

?>
