<?php
class ManageDeliverable extends DeliverableManager
{
      
      function __construct()
      {
         parent::__construct();
      }
      
      function getOptions($options = array())
      {
         $option_sql = "";
         $action_sql = "";
         if(isset($options['legislation_id']) && !empty($options['legislation_id']))
         {
            //$this->legislationId = $options['legislation_id'];
            $legislationAdminObj = new LegislationAdmin();
            $admins              = $legislationAdminObj -> getLegislationAdmins($options['legislation_id']); 
    		$option_sql         .= " AND legislation = '".$options['legislation_id']."' ";
    		$action_sql         .= " AND D.legislation = '".$options['legislation_id']."' ";
         }  
         if($options['page']=="update")
         {
            $option_sql .= " AND D.responsibility_owner = '".$_SESSION['tid']."' ";
         } elseif($options['page']=="edit") {
         	$option_sql .= " AND LA.user_id = '".$_SESSION['tid']."' ";
         }
          //DELIVETABLES OBTAINED UNDER MANAGE MUST MEET THE CRETERIA SET BELOW
          //get those that have been confirmed and activated but not created by sub-events
          $option_sql .= " AND( (D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." ";
          $option_sql .= " AND  D.status & ".Deliverable::CREATED_SUBEVENT." <> ".Deliverable::CREATED_SUBEVENT." ) ";
            
            //get those created under manage
          $option_sql .= " OR D.status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED." ";            
            //get those that have been activated by sub-event, but they must have been created by subevent and activated
          $option_sql .= " OR (D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED." ";
          $option_sql .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT." ";
          $option_sql .= " AND D.status & ".Deliverable::ACTIVATED_BYSUBEVENT." = ".Deliverable::ACTIVATED_BYSUBEVENT.") )";
          
         if(isset($options['financial_year']))
         {
            $option_sql .= " AND L.financial_year = '".$options['financial_year']."' ";
            //get the actions withing this selected financial year
            $action_sql .= " AND L.financial_year = '".$options['financial_year']."' ";

            //make sure the legislation of these deliverable is also activated or the legislation is created under manage
            /*$option_sql .= " AND (L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
                             OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.") ";	            */
            $option_sql .= " AND (L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ) ";	            
							 
         }
        
         if(isset($options['view']) && $options['view'] == "mine")
         {
           //if the user has actions he/she owns get the deliverables as well so they can view their actions
            $action_sql .= " AND A.owner = '".$_SESSION['tid']."' 
                             AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." 
                             GROUP BY A.deliverable_id";
            $actionObj   = new ClientAction();
            $actions     = $actionObj -> fetchAll($action_sql);
            $del_sql     = "";
            if(!empty($actions))
            {
               foreach($actions as $a_index => $action)
               {
                  $del_sql .= " D.id = '".$action['deliverable_id']."' OR";
               }
               $del_sql = " OR (".rtrim($del_sql, "OR").") ";
            }
            if(!empty($del_sql))
            {
                $option_sql .= $del_sql; 
            }
         }        
         //every deliverable under manage should not be a deliverable aiwaiting to be activated by a subevent
         $option_sql .= " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ";
         if(isset($options['limit']) && !empty($options['limit']))
         {
           $option_sql .= " LIMIT ".$options['start']." , ".$options['limit'];
         } 
        // echo $option_sql."\r\n\n";
         return $option_sql;          
      }
      
      /*function getDeliverables($start, $limit, $options = array())
      {
         $optionSql = "";
         $delOptions = array();
         $legislation = array();
         $admins      = array(); 
         if(isset($options['legislation_id']) && !empty($options['legislation_id']))
         {
            $this->legislationId = $options['legislation_id'];
            $legObj = new Legislation();
            $legislation = $legObj -> fetch($options['legislation_id']);
            $legislationAdminObj = new LegislationAdmin();
            $admins = $legislationAdminObj -> getLegislationAdmins($options['legislation_id']);
            if(isset($options['view']) && $options['view'] == "all")
            {
               
            } else {
               $optionSql .= " AND D.responsibility_owner = '".$_SESSION['tid']."' ";               
            }      
    		  $optionSql    .= " AND legislation = '".$options['legislation_id']."'";
         }
         $isLegislationOwner = false;
         //check to see the currenlty logged users is the owner of any of the legislation obtained with specified id
         if(in_array($_SESSION['tid'], $admins))
         {
            $isLegislationOwner = true;
         }   
          //DELIVETABLES OBTAINED UNDER MANAGE MUST MEET THE CRETERIA SET BELOW
          //get those that have been activated by sub-event
          $optionSql .= " AND ( D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT." ";
          //get those created under manage
          $optionSql .= " OR D.status & ".Deliverable::MANAGE_CREATED." = ".Deliverable::MANAGE_CREATED." ";
          //get those that have been confirmed 
          $optionSql .= " OR D.status & ".Deliverable::ACTIVATED." = ".Deliverable::ACTIVATED.") ";
          
          $data          = array();
		$totalProgress = array();
		$dataDeliverables  = array();		
		//$actionObj     = new Action();
		$deliverables  = $this -> deliverableObj -> fetchAll($start, $limit, $optionSql);;
               
		$allDeliverableInfo = $this -> allDeliverableInfo($optionSql);			
		$data               = $this -> sortDeliverables($deliverables, new DeliverableNaming((isset($options['page']) ? $options['page'] : "")));
		$data['isOwner']    = $isLegislationOwner;
		$deliverableActionProgress = array();
		$data['hasActions']	= true; //if a deliverable has actions , then its true else its false
		//get all the deliverables ids and from the database without a limit
		//to use in calculating and loading the deliverables id
          $dataDeliverables = array_merge($data, $allDeliverableInfo);     
         return $dataDeliverables;
      }
	/*
	  get the deliverabls and action for detailed view of the legislation
	
	function getDeliverableActions( $start , $limit , $legislationId )
	{
		$actionsObj   	= new ActionManager(); 
		$deliverables 	= $this->getDeliverables( $start, $limit, $legislationId);
		$delActions 	= array();
		$delHeaders  	= $deliverables['deliverable']['headers'];
		$actionHeaders  = array();
		foreach( $deliverables['deliverable']['data'] as $index => $deliverable)
		{
			$actions 	  						= $actionsObj ->getActions($start, $limit, $deliverable['ref'] );
			$delActions[$deliverable['ref']] 		= $deliverable;
			if(!empty($actions['actions']['data']))
			{
			  $actionHeaders								= $actions['actions']['headers'];
			  $delActions[$deliverable['ref']]['actions'] = $actions['actions']['data'];
			} else {
			  $delActions[$deliverable['ref']]['actions'] = array();
			}
		}
	   return array("data" => $delActions, "actionHeaders" => $actionHeaders, "deliverableHeaders" => $delHeaders); 
	}*/

 

    
}
?>