<?php

class COMPL_MODULE_DEFAULTS extends ASSIST_MODULE_HELPER {

	//inactive questions don't impact on the module
	const INACTIVE = 0;
	//active questions which must be used in the module
	const ACTIVE = 1;
	
	private $all_questions = array();
	
	public function __construct($get = false) {
		parent::__construct();
		if($get === true) {
			$this->all_questions = $this->getAll();
		}
		//echo "<h2>new module defaults class created!</h2>";
	}

	
	function getAll() {
		$result = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_defaults WHERE status & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY sort";
		$q = $this->mysql_fetch_all($sql);
		foreach($q as $r) {
			$r['options'] = $this->formatOptions($r['options']);
			$result[$r['name']] = $r;
		}
		return $result;
	}
	
	function getOne($i) {
		$result = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_defaults WHERE id=".$i." AND status & ".self::ACTIVE." = ".self::ACTIVE."";
		$result = $this->mysql_fetch_one($sql);
		if(isset($result['options'])) {
			$result['options'] = $this->formatOptions($result['options']);
		}
		return $result;
	}

	function getOneByName($i) {
		$result = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_defaults WHERE name='".$i."' AND status & ".self::ACTIVE." = ".self::ACTIVE."";
		$result = $this->mysql_fetch_one($sql);
		if(isset($result['options'])) {
			$result['options'] = $this->formatOptions($result['options']);
		}
		return $result;
	}

	private function formatOptions($o) {
		$result = array();
		$a = explode("_",$o);
		foreach($a as $b) {
			$c = explode("|",$b);
			$result[$c[0]] = $c[1];
		}
		return $result;
	}
	
	private function saveDefault($i,$value) {
		$old = $this->getOne($i);
		$mar = 0;
		if($old['value']!=$value) {
			$sql = "UPDATE ".$this->getDBRef()."_defaults SET value='$value' WHERE id = $i";
			$mar = $this->db_update($sql);
			$mar+=1;
			//LOG ME HERE
			$t = $old['text'];
			if(substr($t,-1,1)=="+") { $t = substr($t,0,-1); }
			if(substr($t,-1,1)=="*") { $t = substr($t,0,-1); }
			$changes = array(
				'user'=>$this->getUserName(),
				'value'=>array(
					'old'=>$old['value'],
					'new'=>$value,
				),
				'display'=>array(
					'field'=>$t,
					'from'=>$old['options'][$old['value']],
					'to'=>$old['options'][$value],
				),
			);
			$this->saveLog($i,$changes);
		} 
		return $mar;
	}
	
	public function saveUpdate($data) {
		$mar = 0;
		foreach($data as $i=>$v) {
			$mar+=$this->saveDefault($i,$v);
		}
		if($mar>0) {
			return array("ok","The update has been saved successfully.");
		} else {
			return array("info","No change was found to be made.");
		}
	}
	
	private function saveLog($i,$changes) {
		$sql = "INSERT INTO ".$this->getDBRef()."_defaults_logs SET
				ref = '$i',
				changes = '".serialize($changes)."',
				insertuser = '".$this->getUserID()."',
				insertdate = now()";
		$this->db_insert($sql);
	}
	
	private function getLogs() {
		$result = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_defaults_logs ORDER BY insertdate DESC";
		$logs = $this->mysql_fetch_all($sql);
		foreach($logs as $l) {
			$c = unserialize($l['changes']);
			$d = "<i>".$c['display']['field']."</i> was changed to '".$c['display']['to']."' from '".$c['display']['from']."'.";
			$result[] = array(
				'date'=>$l['insertdate'],
				'user'=>$c['user'],
				'display'=>$d,
			);
		}
		return $result;
	}
	
	public function displayLogs() {	
		$logs = $this->getLogs();
		//$this->arrPrint($logs);
		$flds = array('date'=>"date",'user'=>"user",'action'=>"display");
		$this->displayAuditLog($logs,$flds,$this->getGoBack("defaults.php"));
	}
	
	
	public function updateDeliverableToIP() {
		$name = "deliverable_ip";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}


	public function updateLegislationToIP() {
		$name = "legislation_ip";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}
	
	
	public function updateDeliverableToUC() {
		$name = "deliverable_uc";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}


	public function updateLegislationToUC() {
		$name = "legislation_uc";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}
	
	public function updateLegislationToCompleted() {
		$name = "complete_legislation";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}

	public function updateDeliverableToCompletedOnApproval() {
		$name = "complete_deliverable";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
			$this->all_questions[$name] = $r;
		}
		if($r['value']=="A") {
			return true;
		}
		return false;
	}
	
	public function updateDeliverableToCompletedWithoutApproval() {
		$name = "complete_deliverable";
		if(isset($this->all_questions[$name])) {
			$r = $this->all_questions[$name];
		} else {
			$r = $this->getOneByName($name);
			$this->all_questions[$name] = $r;
		}
		if($r['value']=="Y") {
			return true;
		}
		return false;
	}
	
}


?>