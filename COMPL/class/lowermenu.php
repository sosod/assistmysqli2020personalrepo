<?php
class LowerMenu extends Menu
{
	protected static $tablename = "menu";
	
	function __construct($folder, $pagename)
	{
       parent::__construct();	
       $this -> folder   = $folder;
       $this -> pagename = $pagename;
	}
	
	function createMenu()
	{
	  $results = $this->getMenu();
	  $menuItems = array();
	  foreach($results as $index => $menu)
	  {
	    $active = $page = "";;
	    /*
          if the access code is not set then, its accessible to everyone without user access restrictions
          if the access code is set or not zero , then user status is checked against the access code , 
          to check if user status matches and has access to the menu item 
        */
         //echo "page name ".$this -> pagename." menu name is ".$menu['name']." and folder is ".$this -> folder."<br />";
         $_url = $this -> folder."_".$this -> pagename;
         if($this -> pagename == "view")
         {
           $this -> pagename = "view_mine";
         }
         $active   = ($this->isActivePage($_url, $menu['name']) ? "Y" : "");	 
         $urlPage = $this->_getUrl($menu['name'], $menu['status']);
        
	   if($menu['accesscode'] == 0)
	   {
	      $menuItems[$menu['id']] = array( 'id'     => $urlPage['page'], 
			                             'url'    => $urlPage['url'], 
			                             'active'  => $active,
			                             'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
				                       );
	                        	   
	   } else {
	      $userStatus = (int)$this -> user['status'];
	      $accessCode = (int)$menu['accesscode'];
	      if(($userStatus & $accessCode) == $accessCode)
	      {
	         $menuItems[$menu['id']] = array('id'  	 => $menu['name'], 
                                              'url' 	 => $urlPage['url'], 
                                              'active'  => $active,
                                              'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
	                                        ); 
	      }	   
	   }
	  }	
	  return $menuItems;   
	}
	
     function _getUrl($menuname, $menustatus)
     {
        $urlPage = array();
        if(strstr($menuname, $this->folder))
        {
           $urlPage['page'] = str_replace($this->folder."_", "", $menuname);
           $urlPage['url']  = $urlPage['page'].".php"; 
        } else {
           $urlPage['page'] = $menuname;
           $urlPage['url']  = $menuname.".php"; 
        }
        return $urlPage;
     }
     
	function getMenu($options = "")
	{	
	  $filename = str_replace($this->folder."_", "", $this->pagename);
	  if(strstr($filename, "_"))
	  {
	     $filename = substr($filename, 0, strpos($filename, "_"));
	  }
	  $result = $this->db->get("SELECT id, name, ignite_terminology, client_terminology, accesscode, status
		                       FROM #_menu 
		                       WHERE parent_id = (SELECT id FROM #_menu WHERE name = '".$this->folder."_".$filename."')"
				            );
	  return $result;
	}
     /*
	function createMenu($folder, $pagename)
	{
	  $filename = str_replace($folder."_", "", $pagename);
	  if(strstr($filename, "_"))
	  {
	     $filename = substr($filename, 0, strpos($filename, "_"));
	  } 
	  $results = $this->getMenu($folder."_".$filename);
	  $menuItems = array();
	  foreach( $results as $index => $menu)
	  {
	    $active = "";
	    /*
	     if menu is create legislation , check if you have access
	     $active = $this->getActivePage( $menu['name'], $name, $options);
          if the access code is not set then, its accessible to everyone without user access restrictions
          if the access code is set or not zero , then user status is checked against the access code , 
          to check if user status matches and has access to the menu item 
        */
        /*if($menu['name'] == "legislation_generate")
        {
          $url = (($menu['status'] &  Menu::LOWERMENU_USEID) == Menu::LOWERMENU_USEID ? '#' : 'generate.php');
        } else {
          $url = (($menu['status'] &  Menu::LOWERMENU_USEID) == Menu::LOWERMENU_USEID ? '#' : $menu['name'].'.php');
        }
        $page = "";
        if(strstr($menu['name'], $folder))
        {
           $page = str_replace($folder."_", "", $menu['name']);
           $url  = (($menu['status'] &  Menu::LOWERMENU_USEID) == Menu::LOWERMENU_USEID ? '#' : $page.".php"); 
        } else {
           $page = $menu['name'];
           $url  = (($menu['status'] &  Menu::LOWERMENU_USEID) == Menu::LOWERMENU_USEID ? '#' : $menu['name'].".php"); 
        }
        
	   if( $menu['accesscode'] == 0)
	   {
	      $menuItems[$menu['id']] = array( 'id'     => $page, 
			                             'url'    => $url, 
			                             'active'  => $active,
			                             'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
				                       );
	                        	   
	   } else {
	      //convert string to int for easy comparison
	      $userStatus = (int)$this->userAccess['status'];
	      $accessCode = (int)$menu['accesscode'];
	      if(($userStatus & $accessCode) == $accessCode)
	      {
	         $menuItems[$menu['id']] = array('id'  	 => $menu['name'], 
                                              'url' 	 => $url, 
                                              'active'  => $active,
                                              'display' => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
	                                        ); 
	      }	   
	   }
	  }
	  if(!empty($menuItems))
	  {
	    echo echoNavigation(2, $menuItems); 
	  }
	}
     */
	
}
