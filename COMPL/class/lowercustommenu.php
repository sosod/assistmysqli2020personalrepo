<?php
class LowerCustomMenu extends Menu
{
     protected static $tablename = "menu";
	
	function __construct($folder, $pagename)
	{
        parent::__construct();
	   $this -> pagename = $pagename;
	   $this -> folder   = $folder;
	}
	
	function createMenu()
	{
        $generalname = substr($this -> pagename, 0, strpos($this -> pagename, "_")); 	     
	   $results = $this->getMenu($generalname);   
        $menuItems = array();	
	   foreach($results as $index => $menu)
	   {
          $active = "";
          $url = "";
          if(!empty($generalname))
          {
               if(strstr($menu['name'], $generalname))
               {
                    if(($menu['status'] & Menu::CUSTOM) == Menu::CUSTOM)
                    {
                     if(strstr($menu['name'], $this->folder))
                     {	               
                        $page = str_replace($this->folder."_", "", $menu['name']);
                        $url = $page.".php";
                        if($page === $this->pagename)
                        {
                          $active = "Y";
                        }
                     } else {
                        if($menu['name'] === $this->pagename)
                        {
                          $active = "Y";
                        }	               
                        $url = $menu['name'].".php";
                     }               
                    } else {
                      $url = "#";
                   }
                   
	              $menuItems[$menu['id']] = array('id'  	  => $menu['name'], 
		                                      'url'    => $url."?ref=".$this->pagename,
		                                      'active' => $active,
		                                      'display'=> ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
			                                );
	        }	
	             
          }
        }
        return $menuItems;
	}
	
     function getMenu($generalname = "")
     {
   
	    $results  = $this->db->get("SELECT * FROM #_menu 
	                                WHERE name LIKE '".$this->folder."_".$generalname."%' 
	                                AND status & ".Menu::CUSTOM." = ".Menu::CUSTOM." 
	                               ");
	    return $results;
      }	
	
	/*function createMenu( $folder, $pagename )
	{
		$menuArr   = array();
		$menuItems = array();
		//if($folder != "useraccess")
		$menuItems = $this->getMenu( $folder, $pagename );	
		foreach($menuItems as $index => $menu)
		{
	        $active = "";
	        $url = "";
             if(($menu['status'] & Menu::LOWERMENU_USEID) == Menu::LOWERMENU_USEID)
             {
	           if(strstr($menu['name'], $folder))
	           {	               
	              $page = str_replace($folder."_", "", $menu['name']);
                   $url = $page.".php";
                   if($page === $pagename)
                   {
                     $active = "Y";
                   }
	           } else {
                   if($menu['name'] === $pagename)
                   {
                     $active = "Y";
                   }	               
	              $url = $menu['name'].".php";
	           }               
             } else {
                 $url = "#";
             }
	        
		   $menuArr[$menu['id']] = array('id'  	  => $menu['name'], 
			                            'url'    => $url."?ref=".$pagename,
			                            'active' => $active,
			                            'display'=> ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])
				                      );
		}	
          if(!empty($menuItems))
          {
            echo echoNavigation(2, $menuArr); 
          }		
	}
	
	function getMenu( $folder, $pagename )
	{
		$results  = $this->db->get("SELECT * FROM #_menu WHERE name LIKE '%".$folder."_".$pagename."%' AND status & ".Menu::LOWERMENU_USEID." = ".Menu::LOWERMENU_USEID."");
		return $results;
	}*/

}
?>
