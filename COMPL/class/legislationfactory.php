<?php
class LegislationFactory
{
     
     public static function createInstance($section)
     {
        $legislationManagerObj = null;        
        if(!empty($section))
        {
           $classname = ucfirst($section)."Legislation";
           if(class_exists($classname))
           {
               $legislationManagerObj = new $classname($section);
           }
        }
        return $legislationManagerObj;
     }     
     

}
?>
