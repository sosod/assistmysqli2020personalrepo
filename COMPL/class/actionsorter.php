<?php
class ActionSorter extends Sorter
{
     
     public static function sortData($actions, $naming, $page="")
     {
		$actionStats = array(); //$this->calculateActionProgress( $this->deliverableId );
		$headers  = $naming->headersList( $page );
		$setupObj = new SetupManager();
		$users = $setupObj->getResUsers();
		$data     = array();
		$data['headers'] = array();
		$data['actions'] = array();
		$data['actionStatus'] = array();
		$data['average']		 = 0;//round($actionStats['averageProgress'], 2);
		$data['totalActions'] = 0;//$actionStats['totalActions'];
		$response = array();
		if(isset($actions) && !empty($actions))
		{
			foreach($actions as $index => $action)
			{
			     if(isset($action['status']))
			     {
			         $data['actionStatus'][$action['ref']] = $action['status'];    
			     }
				foreach($headers as $hIndex => $head)
				{
					if( $hIndex == "action_progress") {
						$data['headers'][$hIndex]	 			= $head;
						$data['actions'][$action['ref']][$hIndex]  = $action['progress'];						
					} else if($hIndex == "action_owner") {	
					     $data['headers'][$hIndex]	 			= $head;
					     $user = "";
					     if(isset($users[$action['action_owner']]))
					     {
					       $user = $users[$action['action_owner']]['name']; 
					     } 
					     $data['actions'][$action['ref']][$hIndex]  = $user;
					} elseif(array_key_exists($hIndex, $action)) {	
						$data['headers'][$hIndex]	 			= $head;
						$data['actions'][$action['ref']][$hIndex]  = $action[$hIndex];						
					}
				}			
			}
		}
		$data['columns'] = (!empty($data['headers']) ? count($data['headers']) : count($headers));
		$data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
 		return $data;
     }
}
?>
