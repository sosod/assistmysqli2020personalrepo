<?php
class UserAuditLog implements AuditLog
{
     
     function processChanges($obj, $postArr, $id, Naming $naming)
     {
        $user = $obj -> getUser(" UA.id = '".$id."' ");
        $tablename = $obj -> getTablename();
        //get all the user access constants uses in setting user access statuses
        $userAccessConstants = User::userConstants();
         foreach($userAccessConstants as $constant => $constantValue)
         {
             $constactName = ucfirst(strtolower(str_replace("_", " ",$constant)));
             //if the user has the status alreay set , ie its already a yes, then check if its been changed to a no 
             if(($user['status'] & $constantValue) == $constantValue)
             {
               if(($postArr['status'] & $constantValue) != $constantValue)
               {
                  $changes[$constant] =  $user['user']." ".$constactName." status has changed from <b>Yes</b> to <b>No</b> ";
               }
             } else {
               //if the user status constant was not already set , then it changed from No to Yes
               if(($postArr['status'] & $constantValue) == $constantValue)
               {
                 if($constantValue == 2)
                 {
                    $changes[$constant] =  $user['user']." status has changed from <b>active</b> to <b>deleted</b>";
                 } else {
                    $changes[$constant] =  $user['user']." ".$constactName." status has changed from <b>No</b> to <b>Yes</b>";
                 }
               }
             }
         } 

         $result = null;      
         if(!empty($changes))
         {
           $obj->setTablename($tablename."_logs");
           $changes['user'] = $_SESSION['tkn'];
           $insertdata = array("changes" => base64_encode(serialize($changes)), "insertuser" => $_SESSION['tid'], "setup_id" => $id);             
           $result = $obj->save($insertdata);
         }  
         $obj->setTablename($tablename);  
        return;
     }
     
}
?>
