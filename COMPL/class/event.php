<?php
class Event 
{
     const EVENT_OCCURED = 8;

     protected static $tablename = "events";

	function getAll( $options = "")
	{
        //get client events 
        $clientObj = new ClientEvent();
        $cEvents   = $clientObj -> fetchAll();
        
        $eventsUsed = $clientObj -> getEventUsed();
        
        $masterObj  = new MasterEvent();
        $mEvents    = $masterObj -> fetchAll();
        
        $clientList = array();
        $masterList = array();
        foreach($cEvents as $cIndex => $cVal)
        {
          $used = false;
          $fakeId = ($cVal['id']*10000);
          if(isset($eventsUsed[$fakeId]))
          {
              $used = true;
          }
          $clientList[$cVal['id']] = array("name"         => $cVal['name'],
                                           "description"  => $cVal['description'],
                                           "id"           => $fakeId,
                                           "status"       => $cVal['status'],
                                           "imported"     => false,
                                           "used"         => $used
                                           );
        }  
        foreach($mEvents as $mIndex => $mVal)
        {
          $masterList[$mVal['id']] = array("name"         => $mVal['name'],
                                           "description"  => $mVal['description'],
                                           "id"           => $mVal['id'],
                                           "status"       => $mVal['status'],
                                           "imported"     => true,
                                           "used"         => true
                                           );
        }  
        $eventsList = array_merge($masterList, $clientList);
        $events     = array();
        foreach($eventsList as $index => $event)
        {
          $events[$event['id']] = $event;
        }
        return $events;
	}
	   
	function getList($options = array(), $flip = FALSE)
	{
		$events     = $this -> getAll($options);
		$eventList  = array();
		foreach($events as $index => $event)
		{
		    if($flip)
		    {
		      $eventList[$event['name']] = $event['id'];
		    } else {
		      $eventList[$event['id']] = $event['name'];
		    }
		}
		return $eventList;
	}	
	

}
?>