<?php
class OrganisationCapacityManager
{

	private $organisationcapacityObj = null;
	
	function __construct()
	{
		$this->organisationcapacityObj = new OrganisationCapacity();
	}
	
	function getAll( $options = array())
	{
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql = " AND status & ".$options['status']." = ".$options['status']."";	
		}
    	$oOrgCapacity  = array(); //$this->organisationcapacityObj->getOrganisationCapacity($optionSql);
    	$myOrgCapacity = array(); //$this->organisationcapacityObj->fetchAll($optionSql);
    	$orgcapacity   = array();
    	foreach ($oOrgCapacity as $index => $capacity)
    	{
    		$oOrgCapacity[$index] = $capacity;
    		$oOrgCapacity[$index]['imported'] = true;	
    	}
    	$orgcapacity = array_merge($oOrgCapacity, $myOrgCapacity);
    	return $orgcapacity;
	}
	
	function getAllList($options=array())
	{
		$capacities = $this->getAll($options);
		$list = array();
		foreach($capacities as $index => $capacity)
		{
			$list[$capacity['id']] = $capacity['name'];
		}
		return $list;
	}
	
	function getOrganisationCapacity( $id)  
	{
		$results = $this->organisationcapacityObj->getCapacityByLegislationId( $id );
		$capacityList = array();
		foreach($results as $index => $capacity)
		{
			$capacityList[$capacity['orgcapacity_id']] = array("capacityid" => $capacity['orgcapacity_id'], 
																			 "legislationid" => $capacity['legislation_id']
																			);
		}
		return $capacityList;
	}
	
	function updateOrganisationalCapacities( $newCapacities, $id )
	{
		$res = $this->organisationcapacityObj->updateOrganisationalCapacities($newCapacities, $id);
		return $res;
	}
	
	function search( $text )
	{
		$clientCaps = $this->organisationcapacityObj->searchCapacities($text);
		$masterCaps = $this->organisationcapacityObj->searchCapacities2($text);
		$caps = array();
		$caps = array_merge($masterCaps , $clientCaps);
		$capsSql = "";
		foreach($caps as $index => $val)
		{
			$capsLeg = $this->organisationcapacityObj->getCapLeg($val['id']);
			if(!empty($capsLeg))
			{
				$capsSql .= " OR L.id = ".$capsLeg['legislation_id']." ";		
			}
		}
		return $capsSql;
	}
	
	public static function getLegislationOrganisationCapacity($legislationId)
	{
	   $capacityObj = new OrganisationCapacity();
	   $capacities  = $capacityObj -> getAll();
	   
	   $legislationCapacities = $capacityObj -> getCapacityByLegislationId($legislationId);
	   $capacityStr           = "";
	   foreach($legislationCapacities as $index => $cap)
	   {
	     if(isset($capacities[$cap['orgcapacity_id']]))
	     {
	        $capacityStr .= $capacities[$cap['orgcapacity_id']]['name'].", ";
	     }
	   }
	   return rtrim($capacityStr,", ");
	}
	
	public static function getMasterLegislationOrganisationCapacity($legislationId)
	{
	   $capacityObj = new OrganisationCapacity();
	   $capacities  = $capacityObj -> getAll();
	   
	   $legislationCapacities = $capacityObj -> importCapacityByLegislationId($legislationId);
	   $capacityStr           = "";
	   foreach($legislationCapacities as $index => $cap)
	   {
	     if(isset($capacities[$cap['orgcapacity_id']]))
	     {
	        $capacityStr .= $capacities[$cap['orgcapacity_id']]['name'].", ";
	     }
	   }
	   return rtrim($capacityStr, ", ");
	}	
	
	
}
