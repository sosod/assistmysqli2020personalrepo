<?php
/*
   Defines the users for this modules
*/
class ModuleUser extends User
{
     
     protected static $tablename = "timekeep";     
     
     function __construct()
     {
        parent::__construct();
     }
     
     function getUsers($options = "")
     {
          $results = $this->db->get("SELECT TK.tkid, CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkstatus, TK.tkemail AS email
                                     FROM assist_".$_SESSION['cc']."_timekeep TK 
                                     INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                     WHERE TK.tkid <> '0000' AND TK.tkstatus = 1 AND 
                                     MU.usrmodref = '".$_SESSION['modref']."' $options
                                     ORDER BY TK.tkname, TK.tksurname 
                                   ");            
			foreach($results as $key => $arr) {
				$results[$key]['user'] = ASSIST_HELPER::decode($arr['user']);
			}
          return $results;   
     }

     function getUser($options = "")
     {
          $results = $this -> db -> getRow("SELECT TK.tkid, CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkstatus, 
                                         TK.tkemail AS email
                                         FROM assist_".$_SESSION['cc']."_timekeep TK 
                                         INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                         WHERE TK.tkid <> '0000' AND TK.tkstatus = 1 AND 
                                         MU.usrmodref = '".$_SESSION['modref']."' $options
                                         ORDER BY TK.tkname, TK.tksurname 
                                       ");
          return $results;  
     }

}
?>