<?php
class Naming
{
     protected $page;     
          
     protected $type;     
          
     protected $namingObj;
          
	const MANAGE 	 = 2;
	
	const NEWPAGE   = 4;
	          
     function __construct($dbRef = "",$type="")
     {
        $this -> namingObj = new BaseNaming($dbRef);  
		$this->setType($type);
     }
     
     function setType($type = "")
     {
        $this -> type = $type;
     }

     function getNaming($options = "")
     {
	   $optionSql = "";
        $optionSql = $this -> getSqlOption(); 
	   if(!empty($options))
	   {
	     if(is_array($options))
	     {
	        foreach($options as $index => $val)
	        {
	          $optionSql .= " AND ".$index." = '".$val."' ";
	        }
	     }
	   }          
       $headers = $this -> namingObj -> fetchAll($optionSql);
       foreach($headers as $id => $val)
       {
          $this -> headers[$val['name']] = $val;
       }
       return $this -> headers;
     } 
     
     function getHeaderList()
     {
        $optionSql = $this -> getSqlOption();
        $headers = $this -> namingObj -> fetchAll($optionSql);

        $headerList = array();
        foreach($headers as $index => $header)
        {
          $headerList[$header['name']] = (!empty($header['client_terminology'])  ? $header['client_terminology'] : $header['ignite_terminology']); 
        }
        $this -> headers  = $headerList;
        return $headerList;  
     }
     
     function getHeader($key, $color = FALSE)
    {
        $headerStr = "";
        if(!empty($this -> headers))
        {
           if(isset($this -> headers[$key]))
           {
             $header = $this -> headers[$key];
	        $terminology = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology']);
	        $purpose     = $header['purpose'];
	        $rule        = $header['rules'];
	        $nameStr  = "<span title='".$purpose."' style:'padding-right:10px; margin:0px;'>";
	         $nameStr .= "<a href='#' id='header_".$key."' class='header' style='color:".($color ? "black" : "white")."; text-decoration:none;' padding-left:5px;'>".$terminology."</a>";
	        $nameStr .= "</span>";           
             return $nameStr;
           } else {
             return ucwords(str_replace("_", " ", $key));
           }
        }
    }    
     
     function setHeader($key)
     {
        $headerStr = "";
        if(!empty($this -> headers))
        {
           if(isset($this -> headers[$key]))
           {
             $header = $this -> headers[$key];
	        $terminology = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology']);          
             return $terminology;
           } else {
             return ucwords(str_replace("_", " ", $key));
           }
        }  
     }
     
     function getHeaderNames()
     {
        return $this -> getNaming();
     }
     
     function getSqlOption()
     {
        $optionSql = "";
        //$this -> setType();
        if(!empty($this -> type))
        {
          $optionSql = " AND H.type = '".$this -> type."'";
        }
        if(!empty($this -> page))
        {
          if($this -> page == "new")
          {
             $optionSql .= " AND H.status & ".Naming::NEWPAGE." = ".Naming::NEWPAGE." ";          
          } else if($this -> page == "manage"){
            $optionSql .= " AND H.status & ".Naming::MANAGE." = ".Naming::MANAGE." ";
          }
        }
        return $optionSql;
     }
     
     function getPage()
     {
        return $this -> page;
     }  
     
     function setPage($page)
     {
        $this -> page = $page;
     }
     

}
?>