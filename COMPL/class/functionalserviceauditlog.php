<?php
class FunctionalServiceAuditLog extends SetupAuditLog
{

     function processChanges($obj ,$postedArr, $id, Naming $naming)
     {
		$data 	   = $obj -> fetch($id);
		$tablename   = $obj -> getTablename();
		$changes     = array();
		
		
		if(isset($postedArr['master_id']))
		{
		  if($postedArr['master_id'] != $data['master_id'])
		  {
		     $functionalObj      = new FunctionalService(); 
		     $fxn = $functionalObj -> getList();

		     $from = $to = "";
		     $from = $fxn[$data['master_id']];
		     $to   = $fxn[$postedArr['master_id']];
		     $changes['functional_service_ref_'.$id] = array("from" => $from, "to" => $to); 		  
		  }
		} else if(isset($postedArr['status'])) {
		     $changes['status_ref_'.$id] = $this->processStatusChanges($postedArr['status'], $data['status']);
		}
		$res = 0;	
		if(!empty($changes))
		{
		   $res = $this->saveChanges($changes, $id, "functional_service_users", $obj);
		   $obj->setTablename("functional_service_users");
		}
	     return $res;
     }
     
}
?>