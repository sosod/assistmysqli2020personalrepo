<?php
class LegislativetypeManager
{
	private $legislativeObj = null;
	
	function __construct()
	{
		$this->legislativeObj = new LegislativeTypes();
	}

	function getAll( $options = array())
	{
		$optionSql = "";
		if(isset($options['status']) && !empty($options['status']))
		{
			$optionSql = " AND status & ".$options['status']." = ".$options['status']."";
		}
		$results = $this->legislativeObj->fetchAll($optionSql);
		$_results = $this->legislativeObj->fetchLegislativeTypes($optionSql);
		foreach($_results  as $index => $type)
		{			
			$lType[$type['id']] =  $type; 	
			$lType[$type['id']]['imported'] =   true;
		}
		$lTypes = array_merge($lType, $results);
		return $lTypes;
	}
	
	function getAllList( $optionsId = "")
	{
		$allTypes =  $this->getAll();
		$types 	 = array();
		foreach($allTypes as $index => $type)
		{
			if(isset($optionsId) && !empty($optionsId))
			{
				if($optionsId == $type['id'])
				{
					return $type['name'];	
				}
			} else {
				$types[$type['id']] = $type['name'];
			}		
			
		}
		return $types;
	}
	
	function search($optionText)
	{
		$masterTypes = $this->legislativeObj->searchMaster($optionText);
		$clientTypes = $this->legislativeObj->searchClient($optionText); 
		$typeIds = array_merge($masterTypes , $clientTypes);
		$typeSql = "";		
		foreach($typeIds as $index => $val)
		{
			$typeSql .= " OR L.type = '".$val['id']."', ";
		}
		$typeSql = rtrim($typeSql, ", ");
		return $typeSql;	
	}
	
	public static function getLegislationType($typeId)
	{
	   $typeObj = new LegislationType();
	   $types   = $typeObj -> getAll();
	   $legislationType = ""; 
	   if(isset($types[$typeId]))
	   {
	      $legislationType = $types[$typeId]['name'];
	   }
	   return $legislationType;
	}
	
}
?>
