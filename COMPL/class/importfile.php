<?php
class ImportFile
{
    static $uploadDir;
    
    static $errors;
    
	public static function upload($key, $importtype)
	{
		$name  = $_FILES[$key]['name'];
		$type  = $_FILES[$key]['type'];
		$tmp   = $_FILES[$key]['tmp_name'];	
		$ext   = substr($name, strpos($name, ".")+1);
		if(in_array($ext, array('csv')))
		{	 		
			if(self::_validateFile($_FILES[$key]))
			{		
				self::_createDir($importtype);
				$file        = "compl_".date("YmdHis").".".$ext;
				$destination = self::$uploadDir."/".$file; 
				if(move_uploaded_file($tmp, $destination))
				{
					$response = array('file' => $name, 'text' => $name.' successfully uploaded', 'error' => false, 'file' => $file);
					return  $response;
				} else {
				    $response = array('error' => true, 'text' => 'Error uploading file');
					return  $response; 
				}						
			} else {
				return self::_errorMessage(self::$errors );
			}
		} else {
			$error['file_type'] = 'You selected an invalid file type, use csv file type only';
			return self::_errorMessage($error);
		}	
	}    
	
	private static function _validateFile( $file )
	{
		switch ($file['error'])
		{
			case UPLOAD_ERR_OK:
				return true;
			break;
			case UPLOAD_ERR_INI_SIZE:
				self::$errors['ini_size'] = "Upload size exceeds maximum set";
				return false;
			break;
			case UPLOAD_ERR_FORM_SIZE:
				self::$errors['form_size'] = "Upload size exceeds maximum set in the form";
				return false;
			break;
			case UPLOAD_ERR_PARTIAL:
				self::$errors['partial']   = "Uploaded file was only partial";
				return false;
			break;
			case UPLOAD_ERR_NO_FILE:
				self::$errors['nofile']    = "There was no file chosen";
				return false;
			break;
			case UPLOAD_ERR_CANT_WRITE:
				self::$errors['cant_write'] = "Failed to write to disk";
				return false;
			break;
			default:
				return true;
			break;
		}
		
	}	
    
    private static function _createDir($importtype)
	{
	    $mainfolder = "../../files";
	    if(is_dir($mainfolder."/".$_SESSION['cc']))
	    {
            if(is_dir($mainfolder."/".$_SESSION['cc']."/".$_SESSION['modref']))
            {
                self::createSubDir($importtype);
            } else {
	            if(mkdir($mainfolder."/".$_SESSION['cc']."/".$_SESSION['modref']))
	            {
	               self::createSubDir($importtype);
	            }
            }
	    } else {
	        if(mkdir($mainfolder."/".$_SESSION['cc']))
	        {
	            if(mkdir($mainfolder."/".$_SESSION['cc']."/".$_SESSION['modref']))
	            {
	                self::createSubDir($importtype);
	            }
	        }
	    }
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
		{
			self::$uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype;
		}
	}

    private static function createSubDir($importtype)
    {
        if(!is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports"))
        {
	        if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports"))
	        {
		       if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
		       {
			      mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype."deleted");
		       }
	        }            
        }
		/*
		if(!is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']))
		{
		   if(mkdir( "../../files/".$_SESSION['cc']."/".$_SESSION['modref'] ))
		   {
			   if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports"))
			   {
				  if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
				  {
					 mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype."deleted");
				  }
			   }	
		   }	
		} else {
			if(!is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports")) 
			{
			   if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports"))
			   {
				  if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
				  {
					 mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype."deleted");
				  }
			   }
			}
		}
		*/					
    }

	//write the meta data of the imported or uploaded files to a file 
	public static function writeImported($importtype)
	{
		self::_createDir($importtype);
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
		{
			try 
			{
				$contents     = "";
				$filelocation = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype."/importedfiles.txt";
				$handle       = fopen($filelocation, "a+");
				//$size   = filesize("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/importedfiles.txt") * 1024;
				//echo "File size is ".$size."<br />";  
				while(!feof($handle))
				{
					$contents .= fread($handle, 1024);
				}
				$newContents = array();
				$oldContents = array();
				if(!empty($contents))
				{
					$oldContents = unserialize(base64_decode($contents));
					if(!empty($_SESSION['uploads'][$importype]))
					{
						$newContents = array_merge($oldContents, $_SESSION['uploads'][$importype]);
						unset($_SESSION['uploads'][$importype]);
					} else {
						$newContents = $oldContents; 
					}
				} else {
					$newContents = $_SESSION['uploads'][$importype];
					unset($_SESSION['uploads'][$importype]);
				}	
				if(!empty($newContents))
				{
					fwrite($handle, base64_encode(serialize($newContents)));
				}					
				fclose($handle);
			} catch(Exception $e) {
				echo "An exception occured ".$e -> getMessage();
			}
		}
	} 
	
	//load the files uploaded from the imports directory
	public static function readImported($importtype)
	{
		self::_createDir($importtype);
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype))
		{
			try
			{
				$contents       = "";
				$filelocation   = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/imports/".$importtype."/importedfiles.txt";
				$handle         = fopen($filelocation, "r");
			    while (($buffer = fgets($handle, 4096)) !== false) 
			    {
			        echo $buffer;			        
			    }
			    if (!feof($handle))
			    {
			        echo "Error: unexpected fgets() fail\n";
			    }				
				$newContents = array();
				$oldContents = array();
				if(!empty($contents))
				{
					$oldContents = unserialize($contents);
					print "<pre>";
						print_r($oldContents);
					print "</pre>";
				}			
				fclose($handle);
			} catch(Exception $e) {
				echo "An exception occured ".$e->getMessage();
			}
		}
	}	
	
	//read the contents of the file and store them into an array
	public function readfileContents($folder, $file)
	{
		$fileLocation = self::$uploadDir."/".$file; 
		try 
		{
			$handle       = fopen($fileLocation, "r");
			$fileContents = array();
			while(!feof($handle))
			{
			    $contents    = fgetcsv($handle);
		        if(count($contents) > 1)
		        {
		           $fileContents[] = $contents;
		        }
		       unset($contents);
			}
			fclose($handle);
			return $fileContents;
		} catch(Exception $e){
			echo "An un-expected error occured ".$e -> getMessage();
		}		
	}
	
	private static function _errorMessage($errorArr)
	{
		$errorsResponse = array();
		$errorText      = "";
		if( !empty($errorArr))
		{				
			foreach($errorArr as $key => $eVal)
			{
		      $errorText .=  $eVal."<br />";
			}			
			$errorsResponse = array("text" => $errorText, "error" => true);
		}
		return $errorsResponse;
	}	
}
?>

