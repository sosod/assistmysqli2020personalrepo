<?php
/*
 * LegislationManager creates the base for managing the legislations in different views and scenarios
 */
class LegislationManager
{
	
	protected $naming; 
	
	protected $legislationObj = null;
	
	protected $legislationHeaders = array();
	
	function __construct($section = "")
	{
	  $headersObj                 = new LegislationNaming();  
	  $headersObj -> setPage($section);
	  $this -> legislationHeaders = $headersObj -> getHeaderList();
	}

      /*
        sort the client legislations data , depending on the page where we are 
      */
      function sortLegislation($legislations,  $options = array())
      {  
	     $headers                        = $this -> legislationHeaders;
         $data                           = array();      
         $data['authorizor']             = array();
         $data['admin']                  = array();     
         $data['isLegislationOwner']     = array();
         $data['status']                 = array();
         $data['statusDescription']      = array();
         $data['legislations']           = array();
         $data['legislation_reference']  = array();
         $statusObj                      = new LegislationStatus();
         $statuses                       = $statusObj -> getList();
	     $legAdminObj                    = new LegislationAdmin();
	     $legAuthorizerObj               = new LegislationAuthorizer();             
		foreach($legislations as $legislationId => $value)
		{
	          $authorizers = $legAuthorizerObj -> getLegislationAuthorizers($legislationId);
	          $admins      = $legAdminObj -> getLegislationAdmins($legislationId);		
		     
		     //get the legislation owners and check if current user is legisaltion owner
	          //$legAdministrators            = explode(",", $value['admin']);
	          if(Legislation::isLegislationOwner($admins))
	          {
	            $data['isLegislationOwner'][$legislationId] = TRUE;
	          }
	          if(Legislation::isActivated($value['legislation_status']))
	          {
	             $data['statusDescription'][$legislationId] = "<b>".(Legislation::statusDescription($value['legislation_status']))."</b>";
	          } else {
	              $data['statusDescription'][$legislationId] = "<b>Not activated</b> <em>(".Legislation::statusDescription($value['legislation_status']).")</em>";
	          }
	          
	          $data['authorizor'][$legislationId]  = $authorizers;
	          $data['admin'][$legislationId]       = $admins;
		     //store the legislation statuses
		     if(isset($value['legislation_status']))
		     {
		        $data['status'][$legislationId] = $value['legislation_status'];
		     }

		     //get the link or reference of this legislations		
	          if(isset($value['legislation_reference']))
	          {
			     $data['legislation_reference'][$legislationId] = $value['legislation_reference'];
			     //unset($value['legislation_reference']);
	          }		     	     							
			foreach($headers as $index => $val)
			{
                  if($index == "action_progress") 
                  {
                    if(isset($options['section']) && $options['section']!="new") {
						$data['headers'][$index]  = $val;
						$progress                 = self::calculateLegislationProgress($legislationId, $options);
						$_progress                = 0;
						if(isset($progress[$legislationId]))
						{
							$_progress = ASSIST_HELPER::format_percent($progress[$legislationId]['action_progress']);
						} else {
							$_progress = ASSIST_HELPER::format_percent($progress[$legislationId]['action_progress']);
						}
						$data['legislations'][$legislationId][$index] = $_progress;
					} else {
						unset($headers['action_progress']);
					}
                  }  else if($index == "type") {
                    $data['headers'][$index]  = $val;
                    $legtype = LegislativetypeManager::getLegislationType($value[$index]);
                    $data['legislations'][$legislationId][$index] = $legtype;						
			   } else if( $index == "category") {
				$data['headers'][$index]  = $val;
				$categoryStr = LegislativeCategoriesManager::getLegislationCategory($legislationId);
				$data['legislations'][$legislationId][$index] = $categoryStr;
			   } else if($index == "organisation_size") {
				$data['headers'][$index]  = $val;
				$sizeStr = OrganisationSizeManager::getLegislationOrganisationSizes($legislationId);
				$data['legislations'][$legislationId][$index] = $sizeStr;                  	        
			   } else if($index == "organisation_type") {
				$data['headers'][$index]  = $val;
				$orgtypeStr = LegislationOrganisationTypeManager::getLegislationOrganisationType($legislationId);
				$data['legislations'][$legislationId][$index] = $orgtypeStr;                  	        
			   } else if($index == "organisation_capacity") {	         
				$data['headers'][$index]  = $val;
				$capacityStr = OrganisationCapacityManager::getLegislationOrganisationCapacity($legislationId);
			     $data['legislations'][$legislationId][$index] = $capacityStr;   
			   } else if($index == 'status') {
                    $data['headers'][$index]  = $val;
                    $data['legislations'][$legislationId]['status'] = (isset($statuses[$value['status']]) ? $statuses[$value['status']] : "New");
                  } else if(isset($value[$index]) || array_key_exists($index, $value)) {
			     $data['headers'][$index]  = $val;                    
                    if($index == "id")
                    {
				  $data['legislations'][$legislationId][$index] = "L".$value[$index];
                    } else{
				  $data['legislations'][$legislationId][$index] = $value[$index];
                    }
			   } 		  
		  }
		}			

		$data['columns'] = (isset($data['headers']) ? count($data['headers']) : count($headers));
		$data['headers'] = (isset($data['headers']) ? $data['headers'] : $headers);
		return $data;
    }
	/*
	  sort imported legislations data
	
	function sortImports($legislations , Naming $naming, $page)
	{
          $headers  = $naming->headersList( $page );               
          $setupObj = new SetupManager();
          $data    = array();     
	     
          foreach($legislations as $key => $value)
          {
	          $categories = $setupObj->importLegislativeCategoriesList($value['ref']);  	   
	          $orgsizes   = $setupObj->importOrganisationSizeList($value['ref']);	   
	          $types      = $setupObj->importLegOrganisationTypesList($value['ref']);	   			
	          $capacities = $setupObj->importOrganisationCapacityList($value['ref']);			
	          $data['isLegislationOwner'][$value['ref']] = false;
	          foreach($headers as $index => $val)
	          {
		          if($index == "action_progress"){
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = 0;						
		          } else if( $index == "legislation_type") {
			          $legObj   = new LegislativetypeManager(); 
			          $legtypes =  $legObj->getAllList( $value[$index]);	

			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = $legtypes;						
		          } else if( $index == "legislation_category")	{
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = implode(",", $categories);
		          } else if($index == "organisation_size") {
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = implode(",", $orgsizes);                  	        
		          } else if($index == "organisation_type"){
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = implode(",", $types);                  	        
		          } else if($index == "organisation_capacity") {	         
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = implode(",", $capacities);                  	        
		          } else if(isset($value[$index]) || array_key_exists($index, $value)) {
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = $value[$index];
	            }
            }
          }			
          $data['data'] = (isset($data['legislations']) ? $data['legislations'] : array());
          $data['columns'] = (isset($data['headers']) ? count($data['headers']) : count($headers));
          $data['headers'] = (isset($data['headers']) ? $data['headers'] : $headers);
          return $data;	
	}
	*/
	function _getLegislationData($key, $legislations, $heades)
	{
          foreach($legislation as $key => $value)
          {
            if(isset($headers[$key]))
            {
               if($key == "type")
               {
                  $data['legislations'][$masterId][$key] = $value;  
               } else if($key == "category") {
                  $data['legislations'][$masterId][$key] = $value;  
               } else if($key == "organisation_size") {
                  $data['legislations'][$masterId][$key] = $value;
               } else if($key == "organisation_type") {
                  $data['legislations'][$masterId][$key] = $value;
               } else if($key == "organisation_capacity") {
                  $data['legislations'][$masterId][$key] = $value;
               } else {
                 $data['legislations'][$masterId][$key] = $value;
               }
               $data['headers'][$key] = $headers[$key];                
            }
          }	
	}
	
	
	static function calculateLegislationProgress($legislationId, $options)
	{
	    $options['legislation_id'] = $legislationId;
	    $options['view']           = "all";
	    $legislationProgress       = array();				
	    $deliverableObj            = new Deliverable();
	    $options['section']        = ($options['section'] == "updateadmin" ? "manage" : $options['section']);
	    $deliverableManagerObj     = DeliverableFactory::getInstance($options['section']);
	    $optionSql                 = "";
	    $optionSql                 = (is_object($deliverableManagerObj) ? $deliverableManagerObj -> getOptions($options) : "");
	    $clientObj                 = new ClientDeliverable(); 
	    $optionSql                 = substr($optionSql  , 0, strpos($optionSql, "LIMIT"));
        $alldeliverables           = $deliverableObj -> allDeliverableInfo($clientObj, $optionSql);
		//$total			       = $legislationStats['total'];
		$t_progress                = 0;
		$total_del                 = 0;
		if(!empty($alldeliverables))
		{   
		   if(isset($alldeliverables['progress']) && !empty($alldeliverables['progress']))
		   {
		      foreach($alldeliverables['progress'] as $delId => $progress)
		      {
			     $t_progress += $progress;  
			     $total_del  += 1;
		      }		    
		   }		    
		}
		$average                                                = ($total_del == 0 ? 0 : round( ($t_progress / $total_del), 2));
		$legislationProgress[$legislationId]['action_progress'] = $average;
		return $legislationProgress;
	} 
	
	/*
	  get the ids of the used legislations, to compare with those from master setup
	
	function getUsedId()
	{
		$usedId	 = array();
		$legStatus = array();
		$legislations = $this->legislationObj->fetchAll(); 
		foreach($legislations as $key => $valArr )
		{
			array_push($usedId, $valArr['legislation_reference'] );
			$legStatus[$valArr['legislation_reference']] = $valArr['legislation_status'];  		
		}
		
		return array("usedId" => $usedId, "status" => $legStatus);
	}
	
	function getUsers()
	{
		$setupObj  = new User();
		$users     = $setupObj -> getAll();
		return $users;
	}
    		
	function checkAccessPermissions($legislation)
	{
		$admins = explode(",", $legislation['admin']);
		if( in_array($_SESSION['tid'], $admins))
		{	
			return TRUE;
		}
		return FALSE;
	}
	*/
	function legislationOwners($optionSql)
	{
		$legislations   = $this->legislationObj->fetchAll($optionSql);
		//get legislation where you are the administrator
		$legwhereAdmin 	  = array();
		$legwhereAuthorizor = array();
		foreach($legislations as $index => $legislation)
		{
			//$adminList = (!empty($legislation['admin']) ? explode(",", $legislation['admin']) : array());
			//$authorizorList = (!empty($legislation['authorizer']) ? explode(",",$legislation['authorizer']) : array());

			if(Legislation::isLegislationOwner($legislation['admin']))
			{
				$legwhereAdmin[$legislation['id']] = $legislation['id'];
			}
			if(Legislation::isLegislationAuthorizor($legislation['authorizer']))
			{
				$legwhereAuthorizor[$legislation['id']] = $legislation['id'];
			}
		}	
		return array("admin" => $legwhereAdmin, "authorizor" => $legwhereAuthorizor);	
	}

	function guidanceData( $legislationId )
	{
		$legObj 	 = new Legislation();
		$legislation = $legObj ->fetch( $legislationId );
		$respoOrg    = new ResponsibleOrganisations();
		$responsible = $respoOrg -> getResponsibleOrganisations();
		$org = ""; 
		
		foreach( $responsible as $index => $val)
		{
			if($val['id'] == $legislation['business_patner'])
			{
	             $org = $val['name'];
			}
		}
		$guidanceInfo  = array(
					         "request_by"  => $_SESSION['tkn'],
						    "request" 	  => ucfirst( $_SESSION['cc'] ),
						    "bus_partner" => $org,
						    "leg_owner"	  => $legislation['owner'],
						    "legislation" => $legislation['legislation_name'],
	 					    "email"		  => $legislation['tkemail']
		);
		return $guidanceInfo;
	}
	
	function requestGuidance( $id, $contact, $message )
	{
		$userObj = new UserAccess(); 
		$legObj  = new Legislation();
		$gData   = $this->guidanceData( $id );
		$gData['message'] = $message;
		$data    = array("setup_id" => $id, "changes" => serialize($gData), "insertuser" => $_SESSION['tid'] );
		$legObj -> setTablename("guidance_logs");
		$rs = $legObj -> save( $data );
		$response  = array();
		$userEmail = $userObj -> getUser( $_SESSION['tid'] );
		$emails   = (!empty($userEmail['email']) ? $userEmail['email']."," : "").$gData['email'].""; 
		$body  = "";
		$body .= "<b>Request By:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['request_by']."<br />";
		$body .= "<b>Requesting Company:</b>&nbsp;".$gData['request']."<br />";
		$body .= "<b>Business Partner:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['bus_partner']."<br />";
		$body .= "<b>Legislation Name:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$gData['legislation']."<br />";
		$body .= "<b>Contact Number:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$contact."<br />";
		$body .= "<b>Message:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$message."<br />";

		EmailNotifier::$to 		= $emails;
		EmailNotifier::$subject = "Request Guidance";
		EmailNotifier::$body 	= $body; 
	
		if( EmailNotifier::notify()){
			$response = array("text" => "Request Guidance email successfull send", "error" => false);		
		} else {
			$response = array("text" => "Error sending guidance email", "error" => true);
		}					
		return $response;
	}
			
}
