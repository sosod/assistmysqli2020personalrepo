<?php
/*
     Combining the departments from the master setup in compliance master
*/
class MasterDepartment
{
     
     function getAll()
     {
	    //main departments
	    $deptObj = new MasterMasterDepartment();
	    $mainDepartments = $deptObj ->fetchAll();
	    //sub departments 
	    $subdeptObj = new MasterSubDepartment();
	    $subdepartments = $subdeptObj -> fetchAll();

	    $masterDepts = array();
	    foreach($mainDepartments as $cIndex => $mDept)
	    {
            foreach($subdepartments as $sIndex => $sDept)
	       {	
                if($mDept['id'] == 1)
                {
                    $masterDepts[$mDept['id']] = array("name" => $mDept['name'] );
                } else {
		         if($sDept['dirid'] === $mDept['id'])
		         {
		           $masterDepts[$sDept['id']] = array("name" => $mDept['name']." - ".$sDept['name'] );
		         }
		      }
	        }
	    }    
	    return $masterDepts; 
     }    
     

}
?>