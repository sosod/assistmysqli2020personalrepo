<?php
class MasterSubDepartment extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function fetch($id)
	{
		$result = $this -> db2 -> getRow("SELECT id , name , status, dirid , insertdate FROM #_dirsub WHERE id = $id");
		return $result;
	}
	
	function getSubDirs($dirid)
	{
		$results = $this -> db2 -> get("SELECT id, name, status, dirid , insertdate, insertuser FROM #_dirsub WHERE dirid = $dirid");
		return $results;
	}
	
	function fetchAll()
	{
		$results = $this -> db2 -> get("SELECT id, name, status, dirid, insertdate, insertuser FROM #_dirsub WHERE status & 2 <> 2");
		return $results;
	}
}
?>