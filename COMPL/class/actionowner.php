<?php
class ActionOwner extends Model
{
	
	static $tablename = "action_owner";

     function getAll()
     {
         //client action owners list
         $clientObj   = new ClientActionOwner();
         $cActionOwners = $clientObj -> fetchAll();

         $masterObj   = new MasterActionOwner();
         $mActionOwners  = $masterObj -> fetchAll();

         $actionOwners = array_merge($mActionOwners, $cActionOwners);
         $actionOwnerList = array();
         foreach($actionOwners as $aIndex => $aVal)
         {
           $actionOwnerList[$aVal['id']] = array("name" => $aVal['name'], "id" => $aVal['id'], "status" => $aVal['status']);
         }
         return $actionOwnerList;          
     }

}
