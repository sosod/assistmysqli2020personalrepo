<?php
class EventDeliverable extends DeliverableManager
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function getOptions($options = array())
     {
		$optionSql    = "";
		if(isset($options['events']) && !empty($options['events']))
		{
		   $optionSql    = " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED;
		   $optionSql   .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT;
		}
		if(isset($options['user']) && !empty($options['user']))
		{
		   $optionSql .= " AND D.responsibility_owner = '".$options['user']."' "; 
		}
		if(isset($options['occuranceid']) && !empty($options['occuranceid']))
		{
		  $optionSql   .= " AND D.event_occuranceid = '".$options['occuranceid']."' ";
		}
		
		if(isset($options['page']) && !empty($options['page']))
		{
		    if($options['page'] == "event_deliverable")
		    {
		        //get those deliverables which are supposed to be activated by sub-events
		       $optionSql    = " AND D.status & ".Deliverable::EVENT_ACTIVATED." = ".Deliverable::EVENT_ACTIVATED;
		       $optionSql   .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." <> ".Deliverable::CREATED_SUBEVENT;
		    }
		}
		
		if(isset($options['viewtype']) && !empty($options['viewtype']))
		{
		   if($options['viewtype'] == "viewmine")
		   {
		        //if user logged is legislation admin or authorizor, then view the associated deliverables
                $authorizerObj      = new LegislationAuthorizer();  
                $legislationIds     = array();
                $legSql             = ""; 
                $legSql            .= " AND LA.user_id = '".$_SESSION['tid']."' ";   
                $authLegislations   = $authorizerObj -> getLegislations($legSql);
                
                $adminObj         = new LegislationAdmin();  
                $legislationIds   = array(); 
                $legSql           = "";
                $legSql          .= " AND LA.user_id = '".$_SESSION['tid']."' "; 
                $adminLegislations= $adminObj -> getLegislations($legSql);
                $legislations     = array_unique(array_merge($adminLegislations, $authLegislations));
                $sql              = "";
                if(!empty($legislations))
                {
                   foreach($legislations as $index => $legislation_id)
                   {
                      $sql  .= " D.legislation = '".$legislation_id."' OR";
                   }
                } else {
                    $sql  .= " D.legislation = 0 OR";
                }
                $optionSql .= " AND (".rtrim($sql, "OR").") ";
		   } elseif($options['viewtype'] == "viewall"){
              $userObj = new UserSetting();
              $user    = $userObj -> getUser(" UA.user_id = '".$_SESSION['tid']."' ");
              //if the user currently logged doesn't have the admin all user access, then don't view all 
              if(!$userObj -> isAdminAll($user['status']))
              {
                 $optionSql .= " AND D.legislation = 0 ";
              }
		   } 
		}
		
		if(isset($options['financial_year']) && !empty($options['financial_year']))
		{
		   $optionSql   .= " AND L.financial_year = '".$options['financial_year']."' ";
		}
		
		if(isset($options['legislation_id']) && !empty($options['legislation_id']))
		{
		    $optionSql .= " AND D.legislation = '".$options['legislation_id']."' ";
		}
		
		if(isset($options['limit']) && !empty($options['limit']))
		{
		   $optionSql .= "  LIMIT ".$options['start']." , ".$options['limit']; 
		}
	    return $optionSql;	          
     }
     
     /*
     function getDeliverables($start, $limit, $options = array())
     {
		$optionSql    = "";
		if(isset($options['events']) && !empty($options['events']))
		{
		   $optionSql    = " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED;
		   $optionSql   .= " AND D.status & ".Deliverable::CREATED_SUBEVENT." = ".Deliverable::CREATED_SUBEVENT;
		}
		if(isset($options['user']) && !empty($options['user']))
		{
		   $optionSql .= " AND D.responsibility_owner = '".$options['user']."' "; 
		}
		if(isset($options['occuranceid']) && !empty($options['occuranceid']))
		{
		  $optionSql   .= " AND D.event_occuranceid = '".$options['occuranceid']."' ";
		}

		$data          = array();
		$totalProgress = array();		
		$actionObj     = new Action(); 
		//$deliverables  = $this->deliverableObj->fetchDeliverableBySubEvent_Id( $options['event'] );
		$deliverables = $this->deliverableObj->fetchAll("", "", $optionSql);

		$deliverableList = array();
		$total         = 0;
		$deliverablesArr = array();
		
		$data          = $this->sortDeliverables($deliverables, new DeliverableNaming("new"));
		$allDeliverables  = $this->deliverableObj->fetchAll("", "", "");				
		foreach($allDeliverables as $index => $del)
		{
		   $data['deliverablesId'][$del['ref']] = array("id" => $del['ref'], "status" => $del['status']);
		}		
		//$totalDel      = $this->deliverableObj->totalDeliverables($optionssql);
		$data['total'] = $total;
		$data['deliverableActionProgress'] = array();
		$data['totalProgress'] = array();	
		return $data;     
     }     
     */

}
?>