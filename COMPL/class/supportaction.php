<?php
class SupportAction extends ActionManager
{
	
	function __construct()
	{
		parent::__construct();	
	}	
	
	function getOptions($options = array())
	{
	   $option_sql = ""; 
	   if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
	   {
	     $option_sql .= "  AND A.deliverable_id = '".$options['deliverable_id']."' ";
	   } else {
            /*$optionSql .= "  AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                  OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                  OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                              )"; 	     */
	   }  
	   if(isset($options['user']) && !empty($options['user']))
	   {
	     $option_sql .= " AND A.owner = '".$options['user']."' ";
	   }
	   
	   if(isset($options['financial_year']) && !empty($options['financial_year']))
	   {
	     $option_sql .= " AND L.financial_year = '".$options['financial_year']."' ";
	     //$optionSql .= " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
         $option_sql .= " AND (L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
                            OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.") ";	     
	   }
	   
	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
	     $option_sql .= " AND L.id = '".$options['legislation_id']."' ";
	   }
	   if($options['page'] == "edit" || $options['page'] == "update")
	   {
	     $option_sql .= " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ";
	   }
	   
	   $option_sql .= " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
	   
	   if(isset($options['limit']) && !empty($options['limit']))
	   {
	     $option_sql .= " LIMIT ".$options['start']." , ".$options['limit'];
	   }
	   return $option_sql;  
	}
	
	/*
	function getActions( $start, $limit, $options = array())
	{
	     $optionSql = "";
		if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
		{
			$this->deliverableId = $options['deliverable_id'];
			$optionSql = " AND A.deliverable_id = '".$options['deliverable_id']."'";
		} else {
		  //$optionSql = " AND A.actionstatus & ".Action::LEGISLATION_ACTIVATED." = ".Action::LEGISLATION_ACTIVATED." AND ";
            $optionSql .= "  AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                  OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                  OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                              )"; 
		}	
		if(isset($options['user']) && !empty($options['user']))
		{
		   $optionSql .= " AND A.owner = '".$options['user']."' ";
		}
		$actions       = $this->actionObj->fetchAll($start,$limit, $optionSql);
		$actionsInfor  = $this->actionObj->getActionProgressStatitics($optionSql);
		$response      = $this->sortActions($actions, new ActionNaming("new"));
		$response['total'] = $actionsInfor['totalActions'];
		$response['totalActions'] = $actionsInfor['totalActions'];
		$response['average'] = round($actionsInfor['averageProgress'], 2);
		return $response;				
	}
 

         */
          
}
?>
