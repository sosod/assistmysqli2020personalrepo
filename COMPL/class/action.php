<?php
class Action extends Model
{

	private $id;	 	 	 	 	 	 	
	
	private $action;
			 	 	 				
	private $actionstatus;
			 	 	 	 	 	 	
	private $deliverable_id;
			 	 	 	 	 	 	
	private $action_deliverable;
			 	 	 	 	 	 	
	private $owner;
			 	 	 	 	 	 	
	private $status;
			 	 	 	 	 	 	
	private $deadline;
			 	 	 	 	 	 	
	private $progress;
			 	 	 	 	 	 	
	private $reminder;
			 	 	 	 	 	 	
	private $description;
			 	 	 	 	 	 	
	private $insertdate;
		 	 	 	 	 	 	
	private $insertuser;
	
	protected static $table 	  = "action";
	
	const ACTIVE 	              = 1;

	const DELETED	 	          = 2;
	
	const INACTIVE                = 4;
	//action imported from compliance master database
	const ACTIVE_IMPORTED         = 8;
	//action completed and awaiting manager approval
	const AWAITING_APPROVAL       = 16;
	//action approved that its completed
	const APPROVED			      = 32;
	//action declined 
	const DECLINED 		    	  = 64;
	//action created when an event occurs
	const CREATEDBY_SUBEVENT      = 128;
	//status of action when the legislation has been confirmed
	const CONFIRMED               = 256;
	//status of actions created under manage, ie they will be confirmed and activated 
	const MANAGE_CREATED          = 512;
	//stores the status of the actions who are using the actual user id
	const USING_USERID            = 1024;
	//stores the status of action holding the user-id matched under setup
	const USING_MATCHEDUSERID     = 2048;
	//stores the status of actions that  have been activated after being confirmed
	const ACTIVATED               = 4096;
	
	const REFTAG 				  = "A";
	
	private $action_deadline_types = array(
		'date'			=> "",
		'startfinyear'	=> "Start Of Financial Year",
		'endfinyear'	=> "End Of Financial Year",
		'start1'		=> "Start Of Month",
		'end1'			=> "End Of Month",
		'start3'		=> "Start Of Quarter",
		'end3'			=> "End Of Quarter",
		'start4'		=> "Start Of Semester",
		'end4'			=> "End Of Semester",
		'start6'		=> "Start Of 6-month Period",
		'end6'			=> "End Of 6-month Period"
	);

	
	function __construct()
	{
		parent::__construct();
	}
	
	
	
	function getActions($options = array())
	{
	    $actionManagerObj        = ActionFactory::getInstance($options['section']);
	    $option_sql              = $actionManagerObj -> getOptions($options);
	    $clientObj               = new ClientAction();
        $results                 = $clientObj -> fetchAll($option_sql);
        $option_sql              = substr($option_sql, 0, strpos($option_sql, "LIMIT"));
        $all_actions             = $clientObj -> fetchAll($option_sql);
        $actionDeliverable       = array();
        foreach($all_actions as $index => $action)
        {
           if(isset($actionDeliverable[$action['deliverable_id']])) {
             $actionDeliverable[$action['deliverable_id']] += 1;
           } else {
             $actionDeliverable[$action['deliverable_id']] = 1;
		   }
        }
        $totalActions            = $clientObj -> totalActions($option_sql);
        $actionStats             = self::calculateActionProgress($option_sql);
        $options['delActions']   = $actionDeliverable;
        $actions                 = $actionManagerObj -> sortActions($results, $options);
        $actions['average']      = (isset($actionStats['averageProgress']) ? round($actionStats['averageProgress'], 2) : 0);
        $actions['totalActions'] = $totalActions['total'];
        $actions['user']         = $_SESSION;
        return $actions;
	}

	public static function calculateActionProgress($optionSql = "")
	{
       $actionObj     = new ClientAction();
	   $totalProgress = 0;
	   $actionStats   = ""; 
	   $actionStats   = $actionObj -> getActionProgressStatitics($optionSql);
	  return $actionStats;
	}
	
	public static function isUsingUserId($actionStatus)
	{
	  if((($actionStatus & Action::USING_USERID) == Action::USING_USERID) || 
	     (($actionStatus & Action::USING_MATCHEDUSERID) == Action::USING_MATCHEDUSERID))
	     {
	       return TRUE;
	     }
	  return FALSE;
	}
		
	function importDeliverableActions($id, $deliverable_ref, $financial_year, $finyearObj)
	{
		$actObj  = new MasterAction();
		$actions = $actObj -> importActions($deliverable_ref);
		
		$clientObj = new ClientAction();

		$countSaved    = 0;
		$countNotSaved = 0;
		$response 	   = array();
		if(!empty($actions))
		{			
			foreach($actions as $index => $action)
			{
				$action['deliverable_id']	 = $id;
				$action['action_reference']  = $action['id'];
				$action['action_status']	 = Action::ACTIVE + Action::ACTIVE_IMPORTED;
				$action['owner']          = (!empty($action['owner']) ? $action['owner'] : 1);
				$actionDeadlineDate       = $finyearObj -> createFinancialYearDate($financial_year, $action['deadline']);
				$action['deadline']       = $actionDeadlineDate;   
				unset($action['id']);
				$clientObj -> setTablename("action");
				if(($result = $clientObj -> save($action)))
				{
					//save the version of the current action that is in setup 1 , if the action had been updated that side
					$latestactionversion = $actObj -> getLatestVersion( $action['action_reference'] );
					$usagearray 		 = array();	
					if(!empty($latestactionversion))
					{
						$usagearray = array("action_id" => $action['action_reference'], "current_version" => $latestactionversion['id'], "version_in_use" => $latestactionversion['id']);		
					} else{
						$usagearray = array("action_id" => $action['action_reference'], "current_version" => 0, "version_in_use" => 0);
					}				
					$clientObj -> setTablename("action_usage");
					$clientObj -> save($usagearray);
					$countSaved++;
				} else {
					$countNotSaved++;
				}
			}
			return array("saved" => $countSaved, "notSaved" => $countNotSaved);
		}
	}
	
	function copySaveAction($data)
	{
		$response = array();
		// if remind on date is greater than the deadline date
		if(isset($data['actiontype']))
		{
		  if($data['actiontype'] == "manage")
		  {
		     $manageAddAction      = TRUE;
		     $data['actionstatus'] = Action::MANAGE_CREATED + 1;
		  }  
		  unset($data['actiontype']);
		}
		if(!Validator::validStartEndDate($data['reminder'], $data['deadline']))
		{
		  $response = array("text" => "The remind on date cannot be after the deadline date", "error" => true);						
		} else {		
		  $this -> actionObj -> setTablename("action");
		  $res = $this -> actionObj -> save($data);
		  if($res > 0) 
		  {
			$response = array("text" => "Action copied and saved successfully . . .", "error" => false);	
		  } else {
			$response = array("text" => "Error saving action . . .", "error" => true);
		  }		
		}
		return $response;
	}

     function getEditAction( $data )
	{
	    $actionObj   = new ClientAction();
	    $action      = $actionObj -> fetch( $data['actionid']);
	    $namingObj   = new ActionNaming();
	    $headers	 = $namingObj -> getHeaderList();
		
		$usersObj    = new User();
		$owners      = $usersObj -> getList();	
		$form        = new Form();
		$table = "<form method='post'>";
			$table .= "<table>";
			$table .= "<tr>";
				$table .= "<td colspan='2'><div id='actionmessage' style='padding:5px;'></div></td>";
			$table .= "</tr>";	
			if($this->_isDisplayableField($data, "action"))
			{				
			     $table .= "<tr>";
				     $table .= "<th>".$headers['action']."</th>";
				     $table .= "<td>".$form->renderTextarea("_actionname", $action['action'])."</td>";
			     $table .= "</tr>";
			}
			if($this->_isDisplayableField($data, "owner"))
			{		
		        $table .= "<tr>";
			     $table .= "<th>".$headers['owner']."</th>";			    
	               if(Action::isUsingUserId($action['actionstatus']))
	               {
				  $table .= "<td>".$form->renderSelect("_actionowner", $owners, $action['owner'])."</td>";	                  
	               } else {
				  $table .= "<td>".$form->renderSelect("_actionowner", $owners, "")."<small>Linked to <em><a href=''>Unspecified</em><small></td>";
	               }			
			   $table .= "</tr>";	
			}
			if($this->_isDisplayableField($data, "action_deliverable"))
			{									
			     $table .= "<tr>";
				     $table .= "<th>".$headers['action_deliverable']."</th>";
				     $table .= "<td>".$form->renderTextarea("_deliverable", $action['action_deliverable'])."</td>";
			     $table .= "</tr>";	
               }
			if($this->_isDisplayableField($data, "deadline"))
			{				               					
			     $table .= "<tr>";
				     $table .= "<th>".$headers['deadline']."</th>";
				     $table .= "<td>".$form->renderDateField("_deadline", $action['deadline'])."</td>";
			     $table .= "</tr>";
			}
			if($this->_isDisplayableField($data, "reminder"))
			{																
			     $table .= "<tr>";
				     $table .= "<th>".$headers['reminder']."</th>";
				     $table .= "<td>".$form->renderDateField("_reminder", $action['reminder'])."<a href='#'' id='clear_remind'>Clear Date</a></td>";
			     $table .= "</tr>";
			}
               if($this->_isDisplayableField($data, "recurring"))
               {				
			     $table .= "<tr>";
				     $table .= "<th>Recurring</th>";
				     $table .= "<td>".$form->renderCheckbox("recurring",  FALSE)."</td>";
			     $table .= "</tr>";	
			}										
			$table .= "</table>";
			$table .= $form->renderHidden("deliverable_id",  $action['deliverable_id']);
		$table .= "</form>";
		return $table;
	}
	
	function _isDisplayableField($options , $field)
	{
	   $reAssignFields = array("action_owner");
	   
	   if(isset($options['reAssignAction']))
	   {
	     
	     if($options['reAssignAction'] == 1)
	     {   
           if(in_array($field, $reAssignFields))
           {
              return TRUE;
           }
	     } else {
	          return TRUE;
	     }
	   }
	   return FALSE;
	}		

	
	function getApprovalActions($options = array())
	{	
	    $actionManagerObj = ActionFactory::getInstance($options['section']);	

		$accountables = array();
        $acc_sql = array();
		//if deliverable accountable person is selected
		if($options['accountable']=="1") {
			$accountableObj   = new AccountablePersonMatch(); 
			$user_sql         = " AND APU.user_id = '".$_SESSION['tid']."' ";
			$accountables     = $accountableObj -> getUserAccountableDeliverable($user_sql);
			if(!empty($accountables)) {
				foreach($accountables as $index => $val) {
					$acc_sql[] = " D.id = '".$val['deliverable_id']."' ";
				}   
			}              
		}
		$resp_sql = "";
		//if deliverable responsible person is selected
		if($options['responsible']=="1") {
			$resp_sql = " D.responsibility_owner = '".$_SESSION['tid']."' ";
		}
			
			
			
		$actionObj        = new ClientAction();
		//get the actions awaiting to be approved
		$option_sql       = " AND A.progress = 100 AND A.status = 3  ";
		$option_sql      .= " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ";
        $option_sql      .= "AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                               OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                               OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."   
                         ) ";
		if(strlen($resp_sql)>0 || count($acc_sql)>0) {
			$option_sql.= " AND ( ".$resp_sql;
			if(strlen($resp_sql)>0 && count($acc_sql)>0) {
				$option_sql.= " OR ";
			}
			$option_sql     .= implode(" OR ",$acc_sql)." )";         
		}

		$actions = array('awaiting'=>array(),'approved'=>array());
		if(isset($options['display']) && $options['display']=="approved") {
			$approved_sql    = " AND A.actionstatus & ".Action::APPROVED." = ".Action::APPROVED
								  .(strlen($options['financialyear'])>0 && $options['financialyear']!="0" ? " AND L.financial_year = ".$options['financialyear'] : "")
								  .(strlen($options['legislationId'])>0 && $options['legislationId']!="0" ? " AND L.id = ".$options['legislationId'] : "")
								  .(strlen($options['user'])==4 ? " AND A.owner = '".$options['user']."'" : "")
								  ." $option_sql";
			$approvedActions = $actionObj -> fetchAll($approved_sql);
			$approved		 = $actionManagerObj -> sortActions($approvedActions, $options);
			$actions['approved'] = $approved;
		} else {
			$awaiting_sql    =  " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
								  AND A.actionstatus & ".Action::AWAITING_APPROVAL." = ".Action::AWAITING_APPROVAL
								  .(strlen($options['financialyear'])>0 && $options['financialyear']!="0" ? " AND L.financial_year = ".$options['financialyear'] : "")
								  .(strlen($options['legislationId'])>0 && $options['legislationId']!="0" ? " AND L.id = ".$options['legislationId'] : "")
								  .(strlen($options['user'])==4 ? " AND A.owner = '".$options['user']."'" : "")
								  ."  $option_sql ";
/*			$awaiting_sql    =  " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
								  AND A.actionstatus & ".Action::AWAITING_APPROVAL." = ".Action::AWAITING_APPROVAL
								  ."  $option_sql";*/
			$awaitingActions = $actionObj -> fetchAll($awaiting_sql);
			$awaiting 	     = $actionManagerObj -> sortActions( $awaitingActions, $options); 
			$actions['awaiting'] = $awaiting;
		}
		return $actions;
	}
	
	function getUpdatedActions()
	{
		$actionObj = new Action();
		$updatedActions = array();
		$actionVersions = $actionObj -> getCurrentVersions();

		$actions  = array();
		foreach( $actionVersions as $index => $action) 
		{
			$latestVersion  = $actionObj -> getLatestVersion( $action['id'] );

			if(!empty($latestVersion))
			{
				if($latestVersion['id'] != $action['version_in_use'])
				{
					$actions[] = "'".$action['id']."'";
				}
			}		
		}
	
		if(!empty($actions))
		{
			$options = " AND A.id IN(".implode(",", $actions).")";
			$updatedActions =  $actionObj -> fetchAll( 0, 10, "", $options );
		}
		$actionReferences = array();
		foreach( $updatedActions as $index => $action)
		{
			$actionReferences[$action['ref']]= $action['action_reference'];
 		}
		$response  = array("actions" => $this->sortHeaders($updatedActions, new ActionNaming()) , "actionsRef" => $actionReferences);
		return $response;
	}
	
	function allowUpdate( $data )
	{
		list($refId , $idRef ) = explode("_", $data['ref'] );
		$ref 	 	  = substr($refId, 6);
		$id 		  = substr($idRef, 2 );
		$actionObj 	  = new Action(); 
		$latestaction = $actionObj -> importAction( $ref );
		$version  	  = $actionObj -> getLatestVersion( $id );
		$res		  =  ""; 
		$response  	  = array();
		if(!empty($latestaction))
		{
			$actionObj -> setTablename("action");
			$res = $actionObj -> update($id, $latestaction, new ActionAuditLog());
			if( $res > 0){
				$actionObj -> setTablename("action_usage");
				$usage = array("current_version" => $version['id'], "version_in_use" => $version['id'] );

				$actionObj -> update($id, $usage, new ActionAuditLog());
			}
		}
		if( $res > 0)
		{
			$response = array("text" => "Action updated . . ", "error" => false);
		} else {
			$response = array("text" => "Error updating action", "error" => true);
		}
		return $response;
	}
		
    
     public static function statusMessage($statusTo, $changeStatus)
     {
        $constants = Action::actionConstants();
        foreach($constants as $key => $value)
        {
           if(($value & $statusTo) == $value)
           {
             if($changeStatus)
             {
               return "Action status changed to <b>".strtolower(str_replace("_", " ", $key))."</b>";
             } else {
               return "<b>".ucfirst(strtolower(str_replace("_", " ", $key)))."</b> status has been removed ";
             }  
           }
        }  
     }	
     
	/*
	 get an array of the user-access class constants
	*/
     public static function actionConstants()
     {
        $oClass      = new ReflectionClass("Action");
        $constants   = $oClass->getConstants();
        return $constants;
     }     
     
     function search($options = array())
     {
          $actionManagerObj = ActionFactory::getInstance("admin");	
          
	     $actionObj = new ClientAction();
	     $optionSql = "";
	     if(isset($options['searchtext']) && !empty($options['searchtext']))
	     {
	        $optionSql = " AND ( A.action LIKE '%".$options['searchtext']."%' 
	                       OR A.action_deliverable LIKE '%".$options['searchtext']."%') ";
	     }
          $actionStats  = self::calculateActionProgress($optionSql);
          //$totalActions = $actionObj -> totalActions($optionSql);
          $results = $actionObj -> filterActions("A.*", $optionSql);
          $actions = $actionManagerObj -> sortActions($results);
          
          $actions['average']      = (isset($actionStats['averageProgress']) ? round($actionStats['averageProgress'], 2) : 0);
          $actions['totalActions'] = 0;
          return $actions;
     }

       static function getStatusSQLForWhere($a) 
       {
            $sql = "(".$a.".actionstatus & ".Action::DELETED." <> ".Action::DELETED."
                      AND ( ".$a.".actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                           OR ".$a.".actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                           OR ".$a.".actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                          )
                  )";
           return $sql;
        }     

		public function getActionDeadlineTypes() { return $this->action_deadline_types; }
}
