<?php
/*
 * Defined how and what legislation to appear on the admin section
 */
class SupportLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getOptionSql($options = array())
	{

	   $optionSql  = array();
	   $clientSql  = "";
	   $masterSql  = "";
	   $activeSql  = "";
	   $activeSql .= " (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE."
				        OR L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
				        OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.")
		             ";	   
        $activeSql .= " AND legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";		             
        $activeSql .= " AND legislation_status & ".Legislation::DEACTIVATED." <> ".Legislation::DEACTIVATED." ";		             
        if(isset($options['page']) && $options['page'] !== "reassign")
        {
          $activeSql .= " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
        }
        
        $clientSql = "(".$activeSql.") ";
                   
        if(isset($options['financial_year']))
        {
          $clientSql .= " AND L.financial_year = '".$options['financial_year']."' ";
        }
         
        if(isset($options['limit']))
        {
          $clientSql .= " LIMIT ".$options['start'].",".$options['limit'];
        }           
        $optionSql['master'] = $masterSql;
        $optionSql['client'] = $clientSql;
        return $optionSql;          
	}

		
	
}
