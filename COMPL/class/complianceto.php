<?php
class ComplianceTo extends Model
{
     protected static $tablename = "compliance_to";

     function getAll()
     {
         //get compliance to from the master database
         $masterObj = new MasterComplianceTo();
         $mComplianceTo = $masterObj -> fetchAll(); 
         //get compliance to from the client database
         $clientObj = new ClientComplianceTo();
         $cComplianceTo = $clientObj -> fetchAll();
          
         $complianceTo = array_merge($mComplianceTo, $cComplianceTo); 
         $complianceToList = array(); 
         foreach($complianceTo as $cIndex => $cVal)
         {
            $complianceToList[$cVal['id']] = array("name" => $cVal['name'], "status" => $cVal['status']);
         }
         return $complianceToList;
     }
     
     

}
?>