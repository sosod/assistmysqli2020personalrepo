<?php 
class LegislationSorter
{
     
     public static function sortData($legislations, $fields)
     {
       $naming   = new Naming();
       $headers  = $naming->getNamingList();           
       print "<pre>";
         print_r($legislations);
         print_r($fields);
         print_r($headers);
       print "</pre>";           
       $setupObj = new SetupManager();
       $data    = array();      
       $categoryObj = new LegislativeCategoriesManager();
       $categories  = $categoryObj -> getAllList();
       
       foreach($legislations as $key => $value)
       {
	     $categories = $setupObj->getLegislativeCategoriesList($value['ref']);  	   
	     $orgsizes   = $setupObj->getOrganisationSizeList($value['ref']);	   
	     $types      = $setupObj->getLegOrganisationTypesList($value['ref']);	   			
	     $capacities = $setupObj->getOrganisationCapacityList($value['ref']);								
	     foreach($headers as $index => $val)
	     {
		     if($index == "action_progress"){
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = 0;						
		     } else if( $index == "type") {
		          if(isset($value[$index]))
		          {
			          $legObj   = new LegislativetypeManager(); 
			          $legtypes =  $legObj->getAllList( $value[$index]);							
			          $data['headers'][$index]  = $val;
			          $data['legislations'][$value['ref']][$index] = $legtypes;								          
		          }
		     } else if( $index == "legislation_category")	{
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = implode(",", $categories);
		     } else if($index == "organisation_size") {
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = implode(",", $orgsizes);                  	        
		     } else if($index == "organisation_type"){
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = implode(",", $types);                  	        
		     } else if($index == "organisation_capacity") {	         
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = implode(",", $capacities);                  	        
		     } else if(isset($value[$index]) || array_key_exists($index, $value)) {
			     $data['headers'][$index]  = $val;
			     $data['legislations'][$value['ref']][$index] = $value[$index];
	       }
         }
       }			
       //$data['legislations'] = (isset($data['legislations']) ? $data['legislations'] : array());
       $data['columns'] = (isset($data['headers']) ? count($data['headers']) : count($headers));
       $data['headers'] = (isset($data['headers']) ? $data['headers'] : $headers);
       return $data;    
     }     
     
}
?>
