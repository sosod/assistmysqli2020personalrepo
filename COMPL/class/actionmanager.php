<?php
class ActionManager
{
	
	protected $actionObj = null;
	
	protected $deliverableId = "";
	
	function __construct()
	{
	   //$this->actionObj = new Action();
	}
	
	function sortActions($actions, $options = array())
	{
		//$actionStats = Action::calculateActionProgress($this->deliverableId);
        $namingObj             = new ActionNaming();
        $page                  = (isset($options['section']) ? $options['section'] : "");
        $namingObj -> setPage($page);          
		$headers               = $namingObj -> getHeaderList();
		$data                  = array();
		$data['headers']       = array();
		$data['actions']       = array();
		$data['actionStatus']  = array();
		$data['actionOwner']   = array();
		$data['actionProgress']= array();
		$data['deliverable']   = array();
		$delActions            = array();
		if(isset($options['delActions']) && !empty($options['delActions']))
		{
		   $delActions = $options['delActions'];
		}
		//$data['average']      = (isset($actionStats['averageProgress']) ? round($actionStats['averageProgress'], 2) : 0);
		//$data['totalActions'] = (isset($actionStats['totalActions']) ? $actionStats['totalActions'] : 0);
		$response              = array();
		$ownerObj              = new ActionOwnerManager();
		$statusObj             = new ActionStatus();
		$statuses              = $statusObj -> getStatusList();
		if(isset($actions) && !empty($actions))
		{
		  foreach($actions as $index => $action)
		  {
			$data['actionStatus'][$action['id']]   = $action['actionstatus'];
			$data['actionOwner'][$action['id']]    = $action['owner'];
			$data['actionProgress'][$action['id']] = $action['progress'];
			if(isset($delActions[$action['deliverable_id']]))
			{
			    if($delActions[$action['deliverable_id']] > 1)
			    {
			       $data['deliverable'][$action['id']] = TRUE;    
			    } else {
			        $data['deliverable'][$action['id']] = FALSE;
			    }
			} else{
			    $data['deliverable'][$action['id']]    = FALSE;
			}
			
			foreach($headers as $hIndex => $head)
			{
			  if($hIndex == "status") 
			  {
				    $data['headers'][$hIndex]	 			= $head;
				    $data['actions'][$action['id']][$hIndex]  = $statuses[$action['status']];						
			   } elseif( $hIndex == "owner") {
				    $data['headers'][$hIndex]	 			= $head;
				    $ownerStr = $ownerObj -> getActionOwnerStr($action['owner'], $action['actionstatus']);
				    $data['actions'][$action['id']][$hIndex]  = $ownerStr;						
			   } elseif($hIndex == "progress"){
                   $data['headers'][$hIndex]	 			= $head;
                   $data['actions'][$action['id']][$hIndex]  = ASSIST_HELPER::format_percent($action['progress']);
			   } elseif(array_key_exists($hIndex, $action)) {	
				    $data['headers'][$hIndex]	 			= $head;
				    if(Util::isTextTooLong($action[$hIndex], 15))
				    {
				      $data['actions'][$action['id']][$hIndex]  = Util::cutString($action[$hIndex], $action['id']."_".$hIndex, 15);
				    } else {
				      if($hIndex == 'id')
				      {
				         $data['actions'][$action['id']][$hIndex]  = "A".$action[$hIndex];
				      } else {
				         $data['actions'][$action['id']][$hIndex]  = $action[$hIndex];
				      }						    
				    }
			   }
			}			
		  }
		}
		$data['columns'] = (!empty($data['headers']) ? count($data['headers']) : count($headers));
		$data['headers'] = (!empty($data['headers']) ? $data['headers'] : $headers);
 		return $data;
	}
	
	public static function calculateActionProgress($deliverableId, $options = "")
	{
          $actionObj = new ClientAction();
		$totalProgress = 0;
		//if the deliverableId is not empty 
		$actionStats = ""; 
		if(!empty($deliverableId))
		{
			$sql  = " AND A.deliverable_id = '".$deliverableId."'";
			$sql .= " AND (A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
			    		 OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
			    		 OR A.actionstatus & ".ACTION::MANAGE_CREATED." = ".ACTION::MANAGE_CREATED."
			    		 )
						";
			$actionStats = $actionObj -> getActionProgressStatitics( $sql );
		}
		return $actionStats;
	}
	
	function getTotalActions( $deliverableId )
	{
	    $actionObj = new ClientAction();
		$totalProgress = 0;
		//if the deliverableId is not empty 
		if(!empty($deliverableId))
		{
			$sql  = " AND deliverable_id = '".$deliverableId."'";
			$actionStats = $actionObj -> getActionProgressStatitics( $sql );
		}
		return $actionStats;
	}	
	
	
}
