<?php
class LegislationType extends Model
{
     protected static $table = "legislative_types";

     function getAll()
     {
          $clientObj = new ClientLegislationType();
          $clienttypes = $clientObj -> fetchAll();
          
          $masterObj  = new MasterLegislationType();
          $mastertypes = $masterObj -> fetchAll();     
          
          $masterlist = array();
          foreach($mastertypes as $index => $mastertype)
          {
            $masterlist[$mastertype['id']] = $mastertype;
            $masterlist[$mastertype['id']]['imported'] = true;
          }
          $list = array();
          $list = array_merge($masterlist, $clienttypes);
          $types = array();
          foreach($list as $lIndex => $type)
          {
            $types[$type['id']] = $type;
          }
          return $types;
     }
     
     function getOne($id)
     {
        $list = $this -> getAll(); 
        $type = array();
        if(isset($list[$id]))
        {
          $type = $list[$id];
        }
        return $type;
     }

     
	

	
}
