<?php
class MasterOrganisationCapacity extends OrganisationCapacity
{

   function __construct()
   {
     parent::__construct();
   }
   
   function fetchAll($options = "")
   {
	$results = $this->db2->get("SELECT id, name, description, status FROM #_organisation_capacity WHERE status & 2 <> 2 AND status & 1 = 1 ORDER by id");
     $capacities = array();
     foreach($results as $cIndex => $capacity)
     {
       $capacities[$capacity['id']] = $capacity;
       $capacities[$capacity['id']]['imported'] = true;
     }
     return $capacities;	
   }
   
   function search( $text )
   {
	 $results = $this->db2->get("SELECT id FROM #_organisation_capacity WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
	 return $results;
   }	
 
}
?>
