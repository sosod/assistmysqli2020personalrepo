<?php
class DeliverableReminder 
{
     private $dbObj;
     
     private $emailObj;
     
     function __construct($db, $emailObj)
     {
        $this -> dbObj = $db;
        $this -> emailObj = $emailObj;
     }     
     
     function sendReminders($userArr)
     {
       $remindersSent = 0;
       $deliverablesSend = array();

       $namingObj = new DeliverableNaming($this -> dbObj -> getDBRef());
       $headers = $namingObj -> getHeaderList();
        
      $subeventManagerObj = new SubEventManager();
      $eventOccuranceObj = new EventOccuranceManager();
      $subeventObj        = new SubEvent();
      $subeventList       = $subeventObj -> getAll();
      
      $eventObj  = new Event(); 
      $mainevent = $eventObj -> getAll();

      $orgtypeObj = new OrganisationType();
      $orgtype    = $orgtypeObj->getAll(); 	

      $deptObj  = new DepartmentManager();
      $masterDepts = $deptObj -> getMasterDepartments();
      $clientObj   = new ClientDepartment();
      $clientDepart = $clientObj -> getAll();

      $funcObj = new FunctionalServiceManager();
      $titles = $funcObj -> getFunctionalServicesTitles();
      $fxnServices = $funcObj -> getClientFunctionalServices();
      
      $complianceFreqObj = new ComplianceFrequency();  
      $complianceFreq = $complianceFreqObj -> getAll(); 
      
      $complObj 	= new CompliancetoManager();      
      $compliancesto = $complObj -> getAll();
      $mastertitles  = $complObj -> getComplianceToTitles();
	 $matches       = $complObj -> getMatchList();	   
      
      $accountableObj = new AccountablePersonManager();
      $accountables   = $accountableObj -> getAll();
      $titles         = $accountableObj -> getAccPersonsTitles();      
      $matchObj      = new AccountablePersonMatch();
      $accountableMatches = $matchObj -> getList();
      
      $resownerObj  = new ResponsibleOwnerManager(); 
      $userObj   = new User();
      $users     = $userObj -> getList();    
      $ownerTitles = $resownerObj -> getResponsibleOwnerTitles();        
      
      $deliverableStatusObj = new DeliverableStatus();
      $statuses             = $deliverableStatusObj -> getDeliverableStatuses();
      
      $deliverableObj = new Deliverable();

      $reportingCategoryObj = new ReportingCategories();  
      $reportingCartegories = $reportingCategoryObj -> getList();
      $deliverables = $this -> getReminders();      
        if(!empty($deliverables))
        {        
          foreach($deliverables as $index => $deliverable)
          {
             if(isset($userArr[$deliverable['responsibility_owner']]))
             {
               
               $freqStr = (isset($complianceFreq[$deliverable['compliance_frequency']]) ? $complianceFreq[$deliverable['compliance_frequency']]['name'] : "");
               $orgtypeStr = (isset($orgtype[$deliverable['applicable_org_type']]) ? $orgtype[$deliverable['applicable_org_type']]['name'] : "");
               $deptStr =   $deptObj -> getDeliverableDepartment($deliverable['responsible'], $deliverable['status'], $clientDepart, $masterDepts);   
               $functionalStr     = $funcObj -> deliverableFunctionalService($deliverable['functional_service'], $deliverable['status'], $titles, $fxnServices);
               $accountablesStr = $accountableObj -> getDeliverableAccountablePerson($deliverable['id'], $accountables, $titles, $accountableMatches); 
               $ownerStr      = $resownerObj -> getDeliverableResponsiblePerson($deliverable['responsibility_owner'], $deliverable['status'], $users, $ownerTitles);
                $repCatStr = (isset($reportingCartegories[$deliverable['reporting_category']]) ?  $reportingCartegories[$deliverable['reporting_category']] : "Unspecified");
                $statusStr = (isset($statuses[$deliverable['deliverable_status']]) ? $statuses[$deliverable['deliverable_status']]['name'] : "New");
                $compliancetoStr  = $complObj -> getDeliverableComplianceTo($deliverable['compliance_to'], $compliancesto, $mastertitles, $matches);
                $eventStr = (isset($mainevent[$deliverable['main_event']]) ? $mainevent[$deliverable['main_event']]['name'] : "");
			 $subeventStr = "";
			 if($deliverableObj->isCreatedSubevent($deliverable['status']))
			 {
			    $eventStr          = $eventOccuranceObj -> getDelSubEvents($deliverable['event_occuranceid'], $subeventList);
			    $subeventStr    = (!empty($eventStr) ? $eventStr : "");        
			 } else {
                   $subeventStr = $subeventManagerObj -> getDelSubEvents($deliverable['id'], $subeventList, new DeliverableSubEvent()); 
			 }
	          //set variables
	           $emailStr = "";
		      $emailStr .= $headers['id']." : ".$deliverable['id']."\r\n\n";
		      $emailStr .= $headers['short_description']." : ".$deliverable['short_description']."\r\n\n";
		      $emailStr .= $headers['description']." : ".$deliverable['description']."\r\n\n";
		      $emailStr .= $headers['legislation_section'].": ".$deliverable['legislation_section']."\r\n\n";
		      $emailStr .= $headers['compliance_frequency']." : ".$freqStr."\r\n\n";
		      $emailStr .= $headers['applicable_org_type']." : ".$orgtypeStr."\r\n\n";		      
		      $emailStr .= $headers['compliance_date']." : ".$deliverable['compliance_date']."\r\n\n";
		      $emailStr .= $headers['responsible']." : ".$deptStr."\r\n\n";
		      $emailStr .= $headers['functional_service'].": ".$functionalStr."\r\n\n";
		      $emailStr .= $headers['accountable_person']." : ".$accountablesStr."\r\n\n";
		      $emailStr .= $headers['responsibility_owner']." : ".$ownerStr."\r\n\n";		      		   
		      $emailStr .= $headers['legislation_deadline']." : ".$deliverable['legislation_deadline']."\r\n\n";
		      $emailStr .= $headers['sanction']." : ".$deliverable['sanction']."\r\n\n";
		      $emailStr .= $headers['reference_link'].": ".$deliverable['reference_link']."\r\n\n";
		      $emailStr .= $headers['assurance']." : ".$deliverable['assurance']."\r\n\n";
		      $emailStr .= $headers['compliance_to']." : ".$compliancetoStr."\r\n\n";		      		   		         
		      $emailStr .= $headers['main_event']." : ".$eventStr."\r\n\n";
		      $emailStr .= $headers['sub_event']." : ".$subeventStr."\r\n\n";		      		   		         
		      $emailStr .= $headers['guidance']." : ".$deliverable['guidance']."\r\n\n";
		      $emailStr .= $headers['reporting_category']." : ".$repCatStr."\r\n\n";		      		   		         
		      $emailStr .= $headers['deliverable_status']." : ".$statusStr."\r\n\n";	      		      		      
			 /* page through each action and add to table */	
		      $emailStr.="\r\nTo see all details please log onto ".(isset($_SESSION['DISPLAY_INFO']['ignite_name']) ? $_SESSION['DISPLAY_INFO']['ignite_name'] : "Ignite Assist").".";	
		            
                //$this ->  emailObj -> setRecipient("anesuzina@gmail.com");					//REQUIRED
                $this ->  emailObj -> setRecipient($users[$deliverable['responsibility_owner']]['email']);					//REQUIRED
	           $this ->  emailObj -> setSubject("Reminder for Compliance Deliverable ".$deliverable['id']);				//REQUIRED
	           $this ->  emailObj -> setBody($emailStr);			//REQUIRED
	          //send the email
	            if($this ->  emailObj ->sendEmail())
	            {
                    $remindersSent += 1;
                    $deliverablesSend[$remindersSent] = " reminder send to ".$userArr[$deliverable['responsibility_owner']]['email']." for deliverable ".$deliverable['id'];
	            }               
               //$this -> dbObj -> sendEmail("anesutest@gmail.com", "Test", "Testing the sending of email", "text");
             }  
          }
        }
        $deliverablesSend['totalSend'] = $remindersSent;
        return $deliverablesSend;
     }
     
    function getReminders()
    {
        $today = date("d-M-Y");
        $results = $this -> dbObj -> mysql_fetch_all("SELECT D.* FROM ".$this -> dbObj -> getDBRef()."_deliverable D
													INNER JOIN ".$this->dbObj->getDBRef()."_legislation L
													ON L.id = D.legislation
													WHERE ".Deliverable::getStatusSQLForWhere("D")." AND ".Legislation::getStatusSQLForWhere("L")."
                                                      AND STR_TO_DATE(D.reminder, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y') 
                                                      AND D.deliverable_status <> 3 ");
        return $results;
    }
   
   function getActionHeaders()
   {
     $headernames = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_header_names WHERE type = 'deliverable' ");
     $headers = array();
     foreach($headernames as $index => $header)
     {
        $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
     }
     return $headers;
   }

}
?>