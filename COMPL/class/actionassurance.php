<?php
class ActionAssurance extends Model
{
	
	static $tablename = "action_assurance";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function fetchAll( $start, $limit, $id)
	{
	$sql = "SELECT AA.id, AA.insertdate, AA.date_tested, AA.response, AA.sign_off, CONCAT(TK.tkname,' ',TK.tksurname) AS user
							   FROM #_action_assurance AA 
							   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser
							   WHERE AA.action_id = {$id}
							   AND AA.status & 2 <> 2
							   ORDER BY AA.id
							   LIMIT $start, $limit								   
							 ";
		$results = $this->db->get($sql);
/* //DEVELOPMENT PURPOSES!!!
ob_start();
echo "<html><body><h3>SQL</h3><p>".$sql."<h3>Response</h3><pre>"; print_r($results); echo "</pre></body></html>";
$echo = ob_get_contents();
ob_end_clean();



$filename = "fetchAll_".date("YmdHis").".html";

			$file = fopen($filename,"w");
			fwrite($file,$echo."\n");
			fclose($file);					  
					  
					  
//*/		return $results;
	}
	
	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT AA.id, AA.insertdate, AA.date_tested, AA.response, AA.sign_off, CONCAT(TK.tkname,' ',TK.tksurname) AS user, AA.status
								     FROM #_action_assurance AA 
								     INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser
								     WHERE AA.id = {$id}								   
								 ");
		return $result;
	}
	
	function getActionAssurance( $id )
	{
		$result = $this->db->getRow("SELECT COUNT(*) AS total 
								     FROM #_action_assurance AA 
								     INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser
								     WHERE AA.action_id = {$id}
								     AND AA.status & 2 <> 2								   
								 	");
		return $result;
	}
	
}
