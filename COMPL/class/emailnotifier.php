<?php
class EmailNotifier implements Notifier{
		
	public static $from;
	
	public static $to;
	
	public static $subject;
		
	public static $body;
	
	public static function notify()
	{
		$headers  = 'MIME-Version:1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=us-ascii' . "\r\n";
		$headers .= "From: no-reply@ignite4u.co.za \r\n";
		$headers .= "Reply-to: no-reply@ignite4u.co.za \r\n";
		
		if( mail(self::$to, self::$subject, self::$body, $headers) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
