<?php
class ClientDepartment
{
     
     function getAll()
     {
        //get the main departments
        $masterObj = new ClientMasterDepartment();
        $masterlist = $masterObj -> fetchAll();
        
        //get the sub directorate list
        $subObj = new ClientSubDepartment();
        $sublist = $subObj -> fetchAll();
        
		foreach($masterlist as $index=>$md) {
			$masterlist[$index]['subs'] = array();
		}
		
		foreach($sublist as $index => $sd) {
			$mi = $sd['dirid'];
			$masterlist[$mi]['subs'][] = $sd;
		}
		
        $list = array();
		/*foreach($masterlist as $index => $masterDept) {
			foreach($masterDept['subs'] as $sIndex => $subDept) {
					$list[$subDept['id']] = array("name" => $masterDept['name']." (".$masterDept['dirsort'].")"." - ".$subDept['name']." (".$subDept['subsort'].")", "id" => $subDept['id'], "diryn"=>$masterDept['diryn'], "subyn"=>$subDept['subyn']);
			}
		}*/
		foreach($masterlist as $index => $masterDept) {
			//foreach($sublist as $sIndex => $subDept) {
			//	if($subDept['dirid'] == $masterDept['id']) {
			//		$list[$subDept['id']] = array("name" => $masterDept['name']." - ".$subDept['name'], "id" => $subDept['id'], "diryn"=>$masterDept['diryn'], "subyn"=>$subDept['subyn']);
			//	}
			//}
			//$list[] = array("name" => $masterDept['name']." (".$masterDept['dirsort'].")", "id" => $masterDept['id'], "diryn"=>$masterDept['diryn'], "subyn"=>"Y");
			foreach($masterDept['subs'] as $sIndex => $subDept) {
				if($subDept['dirid']==$masterDept['id']) {
					$list[$subDept['id']] = array("name" => $masterDept['name']." - ".$subDept['name'], "id" => $subDept['id'], "diryn"=>$masterDept['diryn'], "subyn"=>$subDept['subyn']);
				}
			}
		}
        return $list;
     }    
     
	 function getAllSorted() {
        //get the main departments
        $masterObj = new ClientMasterDepartment();
        $masterlist = $masterObj -> fetchAll();
        
        //get the sub directorate list
        $subObj = new ClientSubDepartment();
        $sublist = $subObj -> fetchAll();
        
        $list = array();
		foreach($masterlist as $index => $masterDept) {
			foreach($sublist as $sIndex => $subDept) {
				if($subDept['dirid'] == $masterDept['id']) {
					$list[] = array("name" => $masterDept['name']." - ".$subDept['name'], "id" => $subDept['id'], "diryn"=>$masterDept['diryn'], "subyn"=>$subDept['subyn']);
				}
			}
		}
        return $list;
	 }
	 
     function getList()
     {
       $departments = $this -> getAll();
       $list        = array();
       foreach($departments as  $dId => $dVal)
       {
         $list[$dId] = $dVal['name'];
       }   
       return $list;
     }
     
     function getDepartments()
     {
        $depts = $this -> getAll();
        $deptArr = array();
        foreach($depts as $index => $dept)
        {
           $deptArr[] = array('name' => $dept['name'], 'id' => $dept['id']);
        }  
        return $deptArr;
     }
     

}
?>