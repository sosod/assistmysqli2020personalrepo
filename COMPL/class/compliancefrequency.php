<?php
class ComplianceFrequency 
{
     protected static $tablename = "compliance_frequency";
     
     function  getAll()
     {
          $masterObj = new MasterComplianceFrequency();
          $masterComplianceFreq = $masterObj -> fetchAll();
          
          $clientObj = new ClientComplianceFrequency();
          $clientComplianceFreq = $clientObj -> fetchAll();
          
          $masterList = array();
          $clientList = array();
          
          foreach($masterComplianceFreq as $mIndex => $mVal)
          {
             $masterList[$mVal['id']] = array("name" => $mVal['name'], 
                                              "description" => $mVal['description'],
                                              "id"          => $mVal['id'],
                                              "status"      => $mVal['status'],
                                              "imported"    => true
                                             ); 
          }
          
          foreach($clientComplianceFreq as $cIndex => $cVal)
          {
             $clientList[$cVal['id']] = array("name" => $cVal['name'], 
                                              "description" => $cVal['description'],
                                              "id"          => $cVal['id'],
                                              "status"      => $cVal['status'],
                                              "imported"    => false
                                             ); 
          }          
          
          $complianceFreq = array_merge($masterList, $clientList);
          $complianceFrequencies = array();
          foreach($complianceFreq as $index => $freq)
          {
             $complianceFrequencies[$freq['id']] = $freq;
          }
          return $complianceFrequencies;
     }
     
     function getList()
     {
         $complianceFrequencies = $this->getAll();
         $list                  = array();
         foreach($complianceFrequencies as $cIndex => $cVal)
         {
           $list[$cVal['id']] = $cVal['name'];
         }
         return $list; 
     }
     
}
?>