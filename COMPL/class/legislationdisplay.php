<?php
/*
 Display the deliverable 
*/
class LegislationDisplay extends HtmlResponse
{
     
     private $legislationId;
     
     private $legislation = array();
     
     private $legislationObj;
	 
	 const REFTAG = "L";
     
     function __construct($legislation = "")
     {
         $this -> legislationId  = $legislation;
         $this -> legislationObj = new ClientLegislation();
               
     }
     
     function _getLegislation()
     { 
         if(!empty($this -> legislationId))
         {
            $this -> legislation = $this -> legislationObj -> findById($this -> legislationId);
         }
     }
     
     function display(Naming $namingObj, $options = array())
     {
        $this -> _getLegislation();       
        $header             = $namingObj -> getHeaderList();
        $categories	        = LegislativeCategoriesManager::getLegislationCategory($this -> legislationId);

        $orgSizes	        = OrganisationSizeManager::getLegislationOrganisationSizes($this -> legislationId); 

        $orgTypes           = LegislationOrganisationTypeManager::getLegislationOrganisationType($this -> legislationId);

        $orgCaps 	        = OrganisationCapacityManager::getLegislationOrganisationCapacity($this -> legislationId);

        $legtype            = LegislativetypeManager::getLegislationType($this -> legislation['type']);

        $financialYearObj   = new FinancialYear();
        $financialyear      = $financialYearObj -> fetch($this -> legislation['financial_year']);

            
		$table = "<form method='post' id='view-legislation' name='view-legislation'>";
		$table .= "<table width='100%' class=form>";		
		//don't show if its a quick edit
	     $table .= "<tr>";
		     $table .= "<th>".$header['id']."</th>";
		     $table .= "<td>".self::REFTAG.$this -> legislationId."</td>";
	     $table .= "</tr>";
		if($this->_isDisplayable($options, "name"))
		{
		     $table .= "<tr>";
			     $table .= "<th>".$header['name'].":</th>";
			     $table .= "<td>".$this -> legislation['name']."</td>";
		     $table .= "</tr>";
		 }
         if($this->_isDisplayable($options, "reference"))
		 {
		    $table .= "<tr>";
			     $table .= "<th>".$header['reference'].":</th>";
			     $table .= "<td>".$this -> legislation['reference']."</td>";
		     $table .= "</tr>";
		  }
		  if($this->_isDisplayable($options, "legislation_number"))
		  {
		     $table .= "<tr>";
			     $table .= "<th>".$header['legislation_number'].":</th>";
			     $table .= "<td>".$this -> legislation['legislation_number']."</td>";
		     $table .= "</tr>";
		   }
		   if($this->_isDisplayable($options, "type"))
		   {
		     $table .= "<tr>";
			     $table .= "<th>".$header['type'].":</th>";
			     $table .= "<td>".$legtype."</td>";
		     $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "legislation_category"))
           {
		     $table .= "<tr>";
			     $table .= "<th>".$header['category'].":</th>";
			     $table .= "<td>".$categories."</td>";
		     $table .= "</tr>";              
           }
	       if($this->_isDisplayable($options, "organisation_size"))
	       {
		      $table .= "<tr>";
			     $table .= "<th>".$header['organisation_size'].":</th>";
			     $table .= "<td>".$orgSizes."</td>";
		     $table .= "</tr>";
	 	   }
		   if($this->_isDisplayable($options, "organisation_type"))
		   {
		     $table .= "<tr>";
			     $table .= "<th>".$header['organisation_type'].":</th>";
			     $table .= "<td>".$orgTypes."</td>";
		     $table .= "</tr>";
		   }
		   if($this->_isDisplayable($options, "organisation_capacity"))
	 	   { 
		     $table .= "<tr>";
			     $table .= "<th>".$header['organisation_capacity'].":</th>";
			     $table .= "<td>".$orgCaps."</td>";
		     $table .= "</tr>";
		   }
		   if($this->_isDisplayable($options, "legislation_date"))
		   {		
               $table .= "<tr>";
               $table .= "<th>".$header['legislation_date'].":</th>";
               $table .= "<td>".$this -> legislation['legislation_date']."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "hyperlink_to_act"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$header['hyperlink_to_act'].":</th>";
                    $table .= "<td>".$this -> legislation['hyperlink_to_act']."</td>";
               $table .= "</tr>";
           }
           if($this->_isDisplayable($options, "financial_year"))
           {
               $table .= "<tr>";
                    $table .= "<th>".$header['financial_year'].":</th>";
                 $fin_year = "";
                 if(!empty($financialyear))
                 {
                   $fin_year = $financialyear['start_date']." to ".$financialyear['end_date']." (".$financialyear['value'].")";
                 }                    
                    $table .= "<td>".$fin_year."</td>";
               $table .= "</tr>";
		  }
		$table .= "</table>";	
		$table .= "</form>";
		$this -> showResponse($table); 
     }     
    
   
	private function _isDisplayable($options,  $field)
	{
	     $reassignFields  = array("responsibility_owner");
	     $quickEditFields = array("responsible_department", "functional_service", "accountable_person", "responsibility_owner", "compliance_name_given_to", "deliverable_reporting_category");
	     if(!empty($options))
	     {
	          if(isset($options['reAssignDeliverable']) && $options['reAssignDeliverable'] == 1)
	          {
	               if(in_array($field, $reassignFields))
	               {
	                    return TRUE;
	               }    
	          }
	          if(isset($options['quickEdit']) && $options['quickEdit'] == 1)
	          {
	               if(in_array($field, $quickEditFields))
	               {
	                    return TRUE;
	               }    
	          }	     
	          return FALSE;  	     
	     } else {
	          return TRUE;
	     }
	     
	}     
	
	/*private function _displayMultiple($options, $value)
	{
	     $valueStr = "";
	     if(!empty($options))
	     {
	          foreach($options as $index => $val)
	          {
	               if(isset($value[$index]))
	               {
	                   $valueStr = $val.", ";
	               }
	          }
	     }
	     return rtrim($valueStr, ", ");
	}
     */
     
}
?>
