<?php
class ResponsibleOwnerMatch extends Model
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function fetchAll($options="")
     {
        $results = $this->db->get("SELECT * FROM #_responsible_owner_users WHERE 1 $options");
        return $results;
     }
     
     function fetch($id)
     {
        $result = $this->db->getRow("SELECT * FROM #_responsible_owner_users WHERE id = '".$id."'");
        return $result;
     }
     
      function updateResponsibleOwner( $data ) 
      {
		$this->setTablename("responsible_owner_users");    
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new ResponsibleOwnerAuditLog(), $this, new ActionNaming());
		$response = $this->updateMessage("responsible_owner", $res);
		return $response;
     }
     
	function saveOwner( $data ) 
	{
		$this->setTablename("responsible_owner_users");      	
		if(isset($data['usernotinlist']) && !empty($data['usernotinlist']))
		{
			$data['status'] = 1 + SetupManager::NOUSER_IN_LIST;
		}
		unset($data['usernotinlist']);
		$res = $this->save( $data);
		if($res > 0)
		{
		   $delObj = new Deliverable();
		   $delObj -> updateDeliverablwOwnerMatch($data['ref'], $data['user_id']);
		}
		$response = $this->saveMessage("responsible_owner", $res); 
		return $response;
	}	     

     function deleteResponsibleOwnerMatch($data)
     {
        $id = $data['id'];
        $match = $this -> fetch($id);
        $delRes = $this -> db -> delete("responsible_owner_users", "id={$id}");
        if($delRes > 0)
        {
          $delObj = new Deliverable();
          $delObj -> undoDeliverableResponsibleOwnerMatch($match['ref'], $match['user_id']);
        }
        return $this -> updateMessage("responsible_owner_match_ref_#".$id, $delRes);          
     }

}
?>