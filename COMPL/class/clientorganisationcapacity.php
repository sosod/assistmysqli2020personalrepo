<?php
class ClientOrganisationCapacity extends OrganisationCapacity
{
   
     function __construct()
     {
       parent::__construct();
     }  

	function fetchAll( $options = "")
	{
	   $results = $this->db->get("SELECT id, name, description, status FROM #_organisation_capacity WHERE status & 2 <> 2 $options ORDER by id ");
	   $capacities = array();
	   foreach($results as $cIndex => $capacity)
	   {
	     $capacities[$capacity['id']] = $capacity;
	     $capacities[$capacity['id']]['imported'] = false;
	   }
	   return $capacities;
	}
	
	function fetch( $id )
	{
	   $result  = $this->db->getRow("SELECT * FROM #_organisation_capacity WHERE id = $id");
	   return $result;
	}
	
	function getOrganisationCapacities( $ids )
	{
	   $result  = $this->db->get("SELECT * FROM #_organisation_capacity WHERE id IN (".$ids.")");
	   return $result;	
	}
		
	function searchCapacities( $text )
	{
	  $results = $this->db->get("SELECT id FROM #_organisation_capacity WHERE name LIKE '%".$text."%' OR description LIKE '%".$text."%' ");
	  return $results;
	}
	
    
	function saveOrganisationCapacity( $data )
	{
          $this->setTablename("organisation_capacity");  
	   $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_organisation_capacity');

	   if(!empty($get_last_id))
	   {
	     $next_id = $get_last_id['id'] + 1;
	     if($get_last_id['id'] < 10000)
	     {
	        $data['id'] = 10000 + $next_id;
	     } else {
	        $data['id'] = $next_id;  
	     }
	   }	                 
          $res = $this->save($data);
          $response = $this->saveMessage("organisation_capacity", $res);
          return $response;   
	}
	
	function updateOrganisationCapacity( $data )
	{
          $this->setTablename("organisation_capacity");      	
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this,new ActionNaming());
		$response = $this->updateMessage("organisation_capacity", $res);
        return $response;
	}		
}
?>