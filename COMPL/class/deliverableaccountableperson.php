<?php
class DeliverableAccountablePerson extends Model
{
    
    //stores status of deliverable accountable persons where the accountable person id is the match id , from the match table
    const MATCHEDID = 4;
     
     
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchByDeliverableId($deliverableId)
     {
        $results = $this->db->get("SELECT * FROM #_accountableperson_deliverable WHERE deliverable_id = '".$deliverableId."' ");
        return $results;
     }
     
     function fetchByAccountablePersonId($id)
     {
        $results = $this -> db -> get("SELECT * FROM #_accountableperson_deliverable WHERE  accountableperson_id = '".$id."' ");
        return $results;
     }

   function updateDeliverableAccountablePerson($newUsers, $id)
   {
     $res = $this -> db -> delete("accountableperson_deliverable", "deliverable_id=$id");
     $res = 0;
     foreach($newUsers as $uIndex => $uVal)
     {
        $updatedata = array("status" => 1 + DeliverableAccountablePerson::MATCHEDID, "deliverable_id" => $id, "accountableperson_id" => $uVal);
        $res = $this -> db -> insert("accountableperson_deliverable", $updatedata);
     }
     return $res;
   }  
   
   function updateAccountablePersonId($newId, $oldId)
   {
     $updatedata = array("status" => 1 + DeliverableAccountablePerson::MATCHEDID, "accountableperson_id" => $newId);
     $this -> db -> updateData("accountableperson_deliverable", $updatedata, array("accountableperson_id" => $oldId));
   }
       

}
?>