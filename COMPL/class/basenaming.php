<?php
class BaseNaming extends Model
{
     
     protected $id;
     
     protected $name;
     
     protected $client_terminology;
     
     protected $ignite_terminology;
     
     protected $position;
     
     protected $type;
     
     protected $status;

     protected $page;
     
     protected static $table = "header_names";
     
     function __construct($dbRef = "")
     {
        parent::__construct($dbRef);
     }
     
     function fetchAll($options = "")
     {
        $results = $this -> db -> get("SELECT G.purpose, G.rules, H.* FROM #_".static::$table." H 
                                       LEFT JOIN #_glossary G ON H.id = G.field WHERE 1 $options 
                                       ORDER BY H.position
                                      ");
        return $results;
     }
     
     function fetch($id)
     {
        $result = $this -> db -> getRow("SELECT * FROM #_".static::$table." WHERE H.id = '".$id."' ");
        return $result;
     }
     
     function updateNaming($id, $data)
     {
       $res = $this -> db -> updateData(static::$table, $data, array("id" => $id));
       return $res;     
     }
     
	function saveColumnOrder($columns)
	{
		$manageColumns = array();
		$newColumns    = array();
		$i = 0;
		$x = 0;
		//get all the column ids for this group
		foreach($columns as $index => $valArr)
		{
		  if($valArr['name'] == "new_hidden")
		  {
		     $newColumns[$i] 	= $valArr['value'];
		     $i++;
		     unset($columns[$index]);
		  } else if( $valArr['name'] == "manage_hidden"){
			unset($columns[$index]);
		  }
		}	
		//get the active columns array
		$newActiveColumns 	 = array();
		$manageActiveColumns = array();
		foreach( $columns as $index => $activeArr)
		{
		   if(strstr($activeArr['name'], "manage"))
		   {
			$mId = substr($activeArr['name'], 7);
			$manageActiveColumns[$i] = $mId;
			$i++;
		   } else {
			$nId = substr($activeArr['name'], 4);
			$newActiveColumns[$x] = $nId;
			$x++;				
		   }
		}
		//sort the colums for udpading
		$columnSorted = array();
		foreach( $newColumns as $index => $id)
		{
		   if( in_array($id, $newActiveColumns))
		   {
			$columnSorted[$id]['status']   = 4;
			$columnSorted[$id]['position'] = $index;
		   } else {
			$columnSorted[$id]['status']   = 0;
			$columnSorted[$id]['position'] = $index;
		   }
			
			//saving manage 			
		   if(in_array($id, $manageActiveColumns))
		   {
			$columnSorted[$id]['status']   = $columnSorted[$id]['status'] + 2;
			$columnSorted[$id]['position'] = $index;
		   } else {
			$columnSorted[$id]['status']   = $columnSorted[$id]['status'] + 0;
			$columnSorted[$id]['position'] = $index;			
		   }	 	
		}
		$result = 0; 		
		foreach($columnSorted as $id => $valArr)
		{
		   $res = $this -> update($id, $valArr, new SetupAuditLog());
		   $result = $result + $res;  
		}	
		$response = array();	
		if($result > 0)
		{
			$response = array("text" => "Columns updated successfully", "error" => false);
		} else if($result == 0) {
			$response = array("text" => "No change was made", "error" => false);
		} else {
			$response = array("text" => "Error updating columns", "error" => true);		
		}
		return $response;
	}	

	function updateLegislationNaming($data)
	{
		$id = $data['id'];
		unset($data['id']);
		$res = $this -> update($id, $data, new SetupAuditLog());
		$response = $this -> updateMessage("naming", $res);
		return $response;
	}

}
?>