<?php
class ActionOwnerManager
{
	
	private $actionownerObj = null;
	
	function __construct()
	{
		$this->actionownerObj = new ActionOwner();	
	}	
	
	function getActionOwnerTitles()
	{
	  $actionownerObj = new ActionOwner();	
	  $titles         = $actionownerObj -> getAll();
	  $matches        = $this->getActionOwnerMatches();
	  $titleList = array();
	  foreach($titles as $index => $title)
	  {
	     $titleList[$index] = $title;
	     if(isset($matches[$index]))
	     {
	        $titleList[$index]['used'] = true; 
	     } else {
	        $titleList[$index]['used'] = false;  
	     }
	  } 
	  return $titleList;
	}
	
	function getActionOwnerMatches()
	{
	   $matchObj = new ActionOwnerMatch();
	   $matches = $matchObj -> fetchAll();
	   $matchList = array();
	   foreach($matches as $mIndex => $mVal)
	   {
	     $matchList[$mVal['ref']] = array("userId" => $mVal['user_id'], "matchId" => $mVal['id'], "status" => $mVal['status']);
	   }
	   return $matchList;
	}
	
	function getAll()
	{
	     $titleList = $this->getActionOwnerTitles();
	     
	     $matchList = $this->getActionOwnerMatches();
	     
	     $userObj    = new User();
	     $userList    = $userObj -> getAll();
	     
	     $actionOwnerMatchList = array();
	     foreach($matchList as $mIndex => $mVal)
	     {
	       $title = $user = "";
	     
	       if(isset($titleList[$mIndex]))
	       {
	          $title = $titleList[$mIndex]['name'];
	       }
	       
	       if(isset($userList[$mVal['userId']]))
	       {
	          $user = $userList[$mVal['userId']]['user'];
	       } else if(isset($titleList[$mVal['userId']])){
	          $user = $titleList[$mVal['userId']]['name'];
	       } else {
	          $user = $title." (Unknown)";
	       }
	     
	        $actionOwnerMatchList[$mVal['matchId']] = array("title" => $title, "user" => $user, "status" => $mVal['status'], "id" => $mVal['matchId'] );
	     }
	    return $actionOwnerMatchList;
	}
	
	function getOwners()
	{
		$userObj   = new UserAccess();
		$owners    = $this->actionownerObj->fetchAll();
		
		$ownerUser = $this->actionownerObj->getOwners();
		$oUsers    = $this->actionownerObj->fetchActionOwners();
		$actionOwners = array_merge($ownerUser, $oUsers);
		
		$ownersList = array();
		foreach( $owners as $i => $own)
		{
			$ownersList[$own['ref']]  = $own['ref'];
		}
		
		$ownersArr = array();
		foreach( $actionOwners as $index => $owner)
		{
			if( isset($ownersList[$owner['id']]) )
			{
				$ownersArr[$owner['id']] = array("name" => $owner['name'], "id" => $owner['id'], "used" => true );
			} else {
				$ownersArr[$owner['id']] = array("name" => $owner['name'], "id" => $owner['id'], "used" => false );
			}			
		}
		return $ownersArr;
	}	
	
	
	function getActionOwnerStr($ownerId, $actionStatus)
	{
	     $titleList = $this -> getActionOwnerTitles();
	     $userObj    = new User();
	     $userList    = $userObj -> getAll();
	     
	     $ownerStr = "";
	     if(Action::isUsingUserId($actionStatus))
	     {
	        if(isset($userList[$ownerId]))
	        {
	          $ownerStr = $userList[$ownerId]['user'];
	        } else {
	          $ownerStr = "Unspecified";
	        }
	     } else {
	       
	       if(isset($titleList[$ownerId]))
	       {
	          $ownerStr = $titleList[$ownerId]['name'];
	       } else {
	             if(isset($userList[$ownerId]))
	             {
	               $ownerStr = $userList[$ownerId]['user'];
	             } else {
	               $ownerStr = "Unspecified";
	             }
	       }
	     }
	   return $ownerStr;
	}



}
?>
