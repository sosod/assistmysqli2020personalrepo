<?php
class ActionAssuranceAuditLog implements Auditlog
{
	
	
	function processChanges($obj, $postArr, $id, Naming $naming)
	{
		$result = "";
		$action    = $obj -> fetch( $id );
		$action_id = $postArr['action_id'];
		$changeArr = array();
		$tablename = $obj -> getTableName();
		
		foreach( $postArr as $key => $val)
		{
			if( $key == "status"){
				if($val == 2)
				{
					$changeArr['message'] = "Assurance #".$id." has been deleted "; 	
				} 
			} else if($key == "sign_off") {
				$changeArr[$key] = array("from" => $this->_checkSignOff( $action[$key] ), "to" => $this->_checkSignOff( $val ) );		
			} else if( array_key_exists($key, $action)){
				if($action[$key] != $val){
					$changeArr[$key] = array("from" => $action[$key], "to" => $val);
				}
			}
		}
		if(!empty($changeArr))
		{
			$statusObj = new ActionStatus();
			$status    = $statusObj -> fetch( $action['status'] );
			if(!empty($status))
			{
				$changeArr['currentstatus'] = $status['name'];
			} else {
				$changeArr['currentstatus'] = "New";
			}	
			$changeArr['user'] =   $_SESSION['tkn'];			
			$obj->setTablename( $tablename."_log");
			$insertdata = array("changes" => base64_encode(serialize($changeArr)), "assurance_id"=>$id,"action_id" => $action_id, "insertuser" => $_SESSION['tid'] );
			$result = $obj->save( $insertdata );	
			$obj->setTablename( $tablename );
		}
		return $result;		
	}
	
	private function _checkSignOff( $val )
	{
		if( $val == 1)
		{
			return "Yes";
		} else {
			return "No";
		}
	}
	
}
