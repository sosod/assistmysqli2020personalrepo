<?php
class EventAction extends ActionManager
{
	
	function __construct()
	{
		parent::__construct();	
	}	
	
	function getOptions($options = array())
	{
	   $optionSql = ""; 
	   if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
	   {
	     $optionSql .= "  AND A.deliverable_id = '".$options['deliverable_id']."' ";
	   } else {
            $optionSql .= "  AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                  OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                  OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                              )"; 	     
	   }  
	   if(isset($options['user']) && !empty($options['user']))
	   {
	     $optionSql .= " AND A.owner = '".$options['user']."' ";
	   }
	   
	   if(isset($options['limit']) && !empty($options['limit']))
	   {
	     $optionSql .= " LIMIT ".$options['start']." , ".$options['limit'];
	   }
	   return $optionSql;  
	}
	
	/*
	function getActions( $start, $limit, $options = array())
	{
	     $optionSql = "";
		if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
		{
			$this->deliverableId = $options['deliverable_id'];
			$optionSql = " AND A.deliverable_id = '".$options['deliverable_id']."'";
		} else {
		  //$optionSql = " AND A.actionstatus & ".Action::LEGISLATION_ACTIVATED." = ".Action::LEGISLATION_ACTIVATED." AND ";
            $optionSql .= "  AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                                  OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                                  OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                              )"; 
		}	
		if(isset($options['user']) && !empty($options['user']))
		{
		   $optionSql .= " AND A.owner = '".$options['user']."' ";
		}
		$actions       = $this->actionObj->fetchAll($start,$limit, $optionSql);
		$actionsInfor  = $this->actionObj->getActionProgressStatitics($optionSql);
		$response      = $this->sortActions($actions, new ActionNaming("new"));
		$response['total'] = $actionsInfor['totalActions'];
		$response['totalActions'] = $actionsInfor['totalActions'];
		$response['average'] = round($actionsInfor['averageProgress'], 2);
		return $response;				
	}
 

         */
          
}
?>