<?php
/*
 * Defines how the legislation structure at the manage level
 */
class UpdateAdminLegislation extends LegislationManager
{
	function __construct()
	{
		parent::__construct("manage");
	}
	
	function getOptionSql($options = array())
	{
	     $option_sql  = array();
	     $client_sql  = "";
	     $client_sql .= " AND  L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
	     $client_sql .= " AND L.legislation_status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
         $client_sql .= " AND (L.legislation_status & ".Legislation::AVAILABLE." = ".Legislation::AVAILABLE." 
				           AND L.legislation_status & ".Legislation::IMPORTED." = ".Legislation::IMPORTED." 
				         ) ";        
         //if(isset($options['page']) && $options['page'] === "edit")
         //{
           //$clientSql .= " OR ( L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED." ) ";
            //$client_sql = " AND (".$active_sql.")";           
         //} 	        
               
        if(isset($options['financial_year']))
        {
          $client_sql .= " AND L.financial_year = '".$options['financial_year']."' ";
        }
        
        if(isset($options['limit']) && !empty($options['limit']))
        {
          $client_sql .= " LIMIT ".$options['start'].",".$options['limit']; 
        }
        //echo $client_sql;
        $optionSql['client'] = $client_sql;
        $optionSql['master'] = "";
        return $optionSql;
	}
	
	
}
