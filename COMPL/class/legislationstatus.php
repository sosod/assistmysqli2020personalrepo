<?php
class Legislationstatus extends Status
{
     protected static $table = "legislation_status";	

	function getStatuses()
	{
		$response  = $this->db-> get("SELECT * FROM #_legislation_status WHERE status & 2 <> 2");
		$response[0]=array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		return $response;
	}
	
	function getAll()
	{
		$response  = $this->db-> get("SELECT * FROM #_legislation_status WHERE status & 2 <> 2");
		$response[0]=array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
	   //$response = array_merge($response1,$response2);
		return $response;
	}	
	
	function fetch( $id )
	{
		if($id==0) {
			$response = array(
				'id'=>0,
				'name'=>"Unable to Comply",
				'client_terminology'=>"Unable to Comply",
				'color'=>"",
				'status'=>5,
				'insertdate'=>"",
				'insertuser' =>"0000",
			);
		} else {
			$response = $this->db->getRow("SELECT * FROM #_legislation_status WHERE id = $id ");
		}
		return $response;
	}
	
	function getList()
	{
	   $statuses = $this -> getStatuses();
	   $list = array(0=>"Unable to Comply");
	   foreach($statuses as $index => $status)
	   {
	     $list[$status['id']]  = $status['name'];
	   }
	  return $list;
	}
	
}
?>
