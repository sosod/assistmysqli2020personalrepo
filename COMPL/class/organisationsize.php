<?php
class OrganisationSize extends Model
{
     
     protected static $table = "organisation_sizes";
     
     
     function __construct()
     {
        parent::__construct();
     }
     
     function getAll()
     {
        $clientObj = new ClientOrganisationSize();
        $clientsizes = $clientObj -> fetchAll();
        
        $masterObj = new MasterOrganisationSize();
        $mastersizes = $masterObj -> fetchAll();
        
        $list = array_merge($mastersizes, $clientsizes);
        $sizes = array();
        foreach($list as $sIndex => $size)
        {
          $sizes[$size['id']] = $size;
        }
        return $sizes;
     }
     
	function getOrganisationalSizeByLegislationId($id)
	{
	   $results = $this->db->get("SELECT * FROM #_orgsize_legislation WHERE legislation_id = $id ");
	   return $results;
	}
	
	function getMasterOrganisationSizeByLegislationId($id)
	{
	   $results = $this->db2->get("SELECT * FROM #_orgsize_legislation WHERE legislation_id = $id ");
	   return $results;
	}
	
	function updateOrganisationalSizes( $newCats , $id)
	{
		$del = $this->db->delete("orgsize_legislation", "legislation_id = '".$id."'");
		foreach( $newCats as $index => $catId)
		{
		  $this->db->insert("orgsize_legislation", array("orgsize_id" => $catId, "legislation_id" => $id));
		}
	}	
	
	function saveOrganisationSize( $data )
	{
          $this->setTablename("organisation_sizes"); 
	   $get_last_id = $this -> db -> getRow('SELECT MAX(id) AS id FROM #_organisation_sizes');

	   if(!empty($get_last_id))
	   {
	     $next_id = $get_last_id['id'] + 1;
	     if($get_last_id['id'] < 10000)
	     {
	        $data['id'] = 10000 + $next_id;
	     } else {
	        $data['id'] = $next_id;  
	     }
	   }	           
          $res = $this->save($data);
          $response = $this->saveMessage("organisation_size", $res);
          return $response;   
	}
	
	function updateOrganisationSize( $data )
	{
      $this->setTablename("organisation_sizes");      	
		$id 	= $data['id'];
		unset($data['id']);
		$res 	  = $this->update($id, $data, new SetupAuditLog(), $this,new ActionNaming());
		$response = $this->updateMessage("organisation_size", $res);
        return $response;
	}	
	
	function getLegSize($sizeId)
	{
		$sizeLeg = array();
		$results = $this->db->getRow("SELECT orgsize_id, legislation_id FROM #_orgsize_legislation WHERE orgsize_id = '".$sizeId."' ");
		return $results;									 
	}
		
	
}
