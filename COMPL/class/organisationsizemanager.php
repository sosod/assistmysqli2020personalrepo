<?php
class OrganisationSizeManager
{

	 private $organisationsizeObj = null;
	 
	 function __construct()
	 {
	 	$this->organisationsize	 = new OrganisationSize();
	 }	

    function getAll( $options = array())
    {
    	$optionSql = "";
    	if(isset($options['status']) && !empty($options['status']))
    	{
    		$optionSql = " AND status & ".$options['status']." = ".$options['status']."";
    	}
    	$myOrgSize = $this->organisationsize->fetchAll( $optionSql );
    	$oOrgSize  = $this->organisationsize->getOrganisationSizes( $optionSql );    	
    	foreach ($oOrgSize as $index => $capacity)
    	{
    		$oOrgSize[$index] = $capacity;
    		$oOrgSize[$index]['imported'] = true;	
    	}
    	$orgsize = array_merge($oOrgSize, $myOrgSize);
    	return $orgsize;
    }
    
	function getAllList($options=array())
	{
		$sizes = $this->getAll($options);
		$list  = array();
		foreach($sizes as $index => $size)
		{
			$list[$size['id']] = $size['name'];
		} 
		return $list;
	}
	
	function getOrganisationSizes($id)
	{
		$sizes = $this->organisationsize->getOrganisationalSizeByLegislationId( $id );
		$sizeList = array();
		foreach($sizes as $index => $legSize)
		{
			$sizeList[$legSize['orgsize_id']]  = array("sizeid" => $legSize['orgsize_id'], "legid" => $legSize['legislation_id']);
		}
		return $sizeList;
	}
	
	function updateOrganisationalSizes( $new, $id)
	{
		$res = $this->organisationsize->updateOrganisationalSizes($new, $id);
		return $res;	
	}
	
	function search( $text )
	{
		$clientSizes = $this->organisationsize->searchOrganisationSizes($text);
		$masterSizes = $this->organisationsize->searchOrganisationSizes2($text);	
		
		$sizes	= array(); 
		$sizes = array_merge($clientSizes, $masterSizes);
		$sizesSql = "";
		foreach($sizes as $index => $val)
		{
			$legorgsize  = $this->organisationsize->getLegSize($val['id']);
			if(!empty($legorgsize))
			{
				$sizesSql .= " OR L.id = '".$legorgsize['legislation_id']."' "; 
			}
		}
		return $sizesSql;
	}
	
	public static function getMasterLegislationOrganisationSizes($legislationId)
	{
	   $orgObj = new OrganisationSize();
	   $orgsizes = $orgObj -> getAll();
	   
	   $legislationOrgsizes = $orgObj -> getMasterOrganisationSizeByLegislationId($legislationId);
	   $sizeStr = "";
	   foreach($legislationOrgsizes as $lIndex => $lVal)
	   {
	     if(isset($orgsizes[$lVal['orgsize_id']]))
	     {
	        $sizeStr .= $orgsizes[$lVal['orgsize_id']]['name']." ,";
	     }
	   }
	   return rtrim($sizeStr, " ,");
	}
	
	public static function getLegislationOrganisationSizes($legislationId)
	{
	   $orgObj = new OrganisationSize();
	   $orgsizes = $orgObj -> getAll();
	   
	   $legislationOrgsizes = $orgObj -> getOrganisationalSizeByLegislationId($legislationId);
	   $sizeStr = "";
	   foreach($legislationOrgsizes as $lIndex => $lVal)
	   {
	     if(isset($orgsizes[$lVal['orgsize_id']]))
	     {
	        $sizeStr .= $orgsizes[$lVal['orgsize_id']]['name'].", ";
	     }
	   }
	   return rtrim($sizeStr, ", ");
	}
	
}

?>
