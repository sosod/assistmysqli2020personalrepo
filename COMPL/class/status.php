<?php
class Status extends Model
{
     protected $id;
     
	protected $name;
	
	protected $client_terminology;
	
	protected $color;
	
	protected $status; 
	
	const ACTIVE = 1;
	const DELETED = 2;
	const SYSTEM = 4;
	   
     function __construct()
     {
        parent::__construct();
     }

	function updateStatus($data)
	{
	   $res = $this -> update($data['id'], $data, new SetupAuditLog(), $this, new ActionNaming());
	   return $this -> updateMessage("status", $res);
	}
	
	/*
	    Order the status in the order of their occurances
	*/
	function getStatusInOrder()
	{
	   $statuses = $this -> getAll();
	   $statusList = array();
	   foreach($statuses as $index => $status)
	   {
	      if($status['id'] == 3)
	      {
	        $tmp[] = $status;
	        continue;
	      } else {
	         $statusList[] = $status;
	      }
	   }
	   $list = array_merge($statusList, $tmp);
	   return $list;
	}

}

?>