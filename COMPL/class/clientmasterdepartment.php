<?php
/*
 Master departments from the client database
*/
class ClientMasterDepartment extends Model
{
	
	protected static $tablename = "dir";
	
	function __construct()
	{
		parent::__construct();
	}

	function fetch( $id )
	{
		$result = $this->db->getRow("SELECT dirid AS id,dirtxt AS name ,dirsort,diryn FROM assist_".$_SESSION['cc']."_list_dir WHERE dirid = $id");
		return $result;
	} 
	
	function fetchAll()
	{
		$results = $this->db->get("SELECT dirid AS id,dirtxt AS name ,dirsort,diryn FROM assist_".$_SESSION['cc']."_list_dir ORDER BY dirsort");
		return $results;
	}

}
?>