<?php
class CustomFunctionalService extends Model
{
     protected static $tablename = "functional_service";
          
     function __construct()
     {
        parent::__construct();
     }
     
     function fetchAll($options="")
     {
        $results = $this -> db -> get("SELECT id, name, status FROM #_functional_service WHERE 1 $options");     
        return $results;
     }
     
     function fetch($id)
     {
       $result = $this -> db -> getRow("SELECT id, name, status FROM #_functional_service WHERE id = '".$id."' ");
       return $result;
     }

}
?>