<?php
/*
 Deliverables from the master setup
*/
class MasterDeliverable extends Deliverable
{
     
     function __construct()
     {
         parent::__construct();
     }
     /*
       Get all the deliverables from  master set up
     */
	function fetchAll($optionSql = "")
	{
		$response =  $this -> db2 -> get("SELECT D.id AS ref , D.* FROM #_deliverable D WHERE D.status & 2 <> 2  WHERE 1 $optionSql");
		return $response;
	}
	
	function fetchByLegislationId($id)
	{	
	  $response = $this -> db2 -> get("SELECT * FROM #_deliverable WHERE legislation = {$id} ");
	  return $response;	
	}
	
	function fetch($id)
	{	
	  $response = $this -> db2 -> getRow("SELECT * FROM #_deliverable WHERE id = {$id} ");
	  return $response;	
	}
	
	function checkEvents( $deliverableid )
	{
	   $results = $this -> db2 -> get("SELECT * FROM #_subevent_deliverable WHERE deliverable_id = '".$deliverableid."'");
	   if(empty($results))
	   {
	     return FALSE;	
	   } else {
          //if the event is unspecified then return false
          if(count($results) == 1)
          {
            foreach($results as $e => $result)
            {
              if($result['subevent_id'] == 1)
              {
              	 return FALSE;
              }	
            }
          } 
          return TRUE;
	   }
	}
	
	function importRelationshipSave($table,$delId,$delRef)
	{
		$results = $this -> db2 -> get("SELECT * FROM #_".$table." WHERE deliverable_id = '".$delRef."'");
		if(!empty($results))
		{
		  foreach($results as $dIndex => $dVal)
		  {
		     $dVal['deliverable_id'] = $delId;
			  $delRes = $this -> db -> insert($table, $dVal);		  	
		  }
		}
	}
	
	function fetchDeliverable($optionSql = "")
	{
	   $result = $this -> db2 -> getRow("SELECT FROM #_deliverable WHERE 1 AND $optionSql ");
	   return $result;
	}
	
	function getLatestVersion( $id )
	{
	   $result = $this -> db2 -> getRow("SELECT id, deliverable_id FROM #_deliverable_edit WHERE deliverable_id = {$id} ORDER BY insertdate DESC");
	   return $result;
	}		

}
?>