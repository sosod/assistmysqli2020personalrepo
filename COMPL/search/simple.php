<?php
$scripts = array("jquery.ui.legislation.js", "jquery.ui.deliverable.js", "jquery.ui.action.js");
include("../header.php");
?>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />
<script>
$(function(){
	$("#go").click(function(){
	    var type = $("#type").val(); 
	    $("#searchresults").html("")
	    if(type == "action")
	    {	    
	      $("#searchresults").action({searchtext : $("#searchtext").val(), url:"../class/request.php?action=Action.search", searchAction:true});
	    } else if(type == "deliverable"){
	      $("#searchresults").deliverable({searchtext : $("#searchtext").val(), url:"../class/request.php?action=Deliverable.search", searchDeliverable: true});
	    } else {
	       $("#searchresults").legislation({searchtext : $("#searchtext").val(), url:"../class/request.php?action=Legislation.search", showFinancialYear:false, searchLegislation:true});
	    }		
	    return false;
	});
});
</script>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<th class="noborder">
			Search : 
		</th>
		<td class="noborder">
			<input type="text" name="searchtext" id="searchtext" value="" size="50" />  
		</td>	
		<td class="noborder">
			<select name="type" id="type">
			   <option value="legislation">Legislation</option>
			   <option value="deliverable">Deliverable</option>
			   <option value="action">Action</option>
			<select>  
		</td>				
		<td  class="noborder">
			<input type="button" name="go" id="go" value=" Go  " size="50" />  
		</td>				
	</tr>
	<tr>
		<td class="noborder" colspan="3"></td>
	</tr>
</table>
<div id="searchresults"></div>
