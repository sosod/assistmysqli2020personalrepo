<?php
$scripts = array("search.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="" id="advanced_search_form">
				<table class="noborder">
					<tr>
						<td colspan="2" class="noborder">
						<table id="tablelegislation">
							<tr>
						    	<th>Legislation Name:</th>    
						    	<td>
						    		<textarea rows="5" cols="30" id="name" name="name" class="checkcounter"></textarea>
						    		<span class="textcounter" id="name_textcounter"></span>
						    	</td>
						    </tr>
							<tr>
						    	<th>Legislation Reference Number:</th>    
						    	<td><input type="text" name="reference" id="reference" value="" /></td>
						    </tr>
							<tr>
						    	<th>Legislation Additional Information:</th>    
						    	<td>
						    		<textarea rows="5" cols="30" name="legislation_number" id="legislation_number" class="checkcounter"></textarea>
						    		<span class="textcounter" id="legislation_number_textcounter"></span>
						        </td>
						    </tr>        
							<tr>
						    	<th>Legislation Type:</th>    
						    	<td>
						        	<select name="type_multiple" id="type" multiple="multiple">

						            </select>
						        </td>
						    </tr>        
							<tr>
						    	<th>Legislation Category:</th>    
						    	<td>
						        	<select name="category_multiple" id="category" multiple="multiple">
						            </select>        
						        </td>
						    </tr>    
							<tr>
						    	<th>Legislation Organisation Size:</th>    
						    	<td>
						        	<select name="organisation_size_multiple" id="organisation_size" multiple="multiple">
						            </select>        
						        </td>
						    </tr>    
						    <tr>
						    	<th>Legislation Organisation Type:</th>    
						    	<td>
						        	<select name="organisation_type_multiple" id="organisation_type" multiple="multiple">
						            </select>        
						        </td>
						    </tr>   
							<tr>
						    	<th>Organisation Capacity Level:</th>    
						    	<td>
						        	<select name="organisation_capacity_multiple" id="organisation_capacity" multiple="multiple">
						            </select>        
						        </td>
						    </tr>
							<!-- <tr>
						    	<th>Responsible Business Patner:</th>
						    	<td>
						        	<select name="responsible_organisation" id="responsible_organisation">
						            </select>
						        </td>
						    </tr> 
							<tr>
						    	<th>Is the legislation available to non-business partner clients?:</th>
						    	<td>
						        	<select name="legislation_available" id="legislation_available">
						        		<option value="0">No</option>
						        		<option value="1">Yes</option>
						            </select>
						        </td>
						    </tr> -->   		         
							<tr>
						    	<th>Legislation Date:</th>    
						    	<td> 
						          <input type="text" name="legislation_date" id="legislation_date" class="datepicker" value="" readonly="readonly"/>
						        </td>
						    </tr>                       
							<tr>
						    	<th>Legislation Hyperlink to act:</th>    
						    	<td>
						          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="" />
						        </td>
						    </tr> 
							<tr>
						    	<th></th>    
						    	<td>
						          <input type="button" name="advancedsearch" id="advancedsearch" value=" Search " class="isubmit" />
						        </td>
						    </tr>                    
						</table>
						</td>
					</tr>
				</table>		
			</form>
		</td>
	</tr>
	<tr style="display:none;" id="tr_searchagain">
		<td colspan="2" class="noborder"> 
			<a href="#" id="searchagain">Search Again</a>	
		</td>
	</tr>
	<tr>
		<td class="noborder" colspan="2">
			<table id="search_result">
			</table>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
