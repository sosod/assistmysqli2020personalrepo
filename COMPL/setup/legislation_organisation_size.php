<?php
$scripts = array("organisation_size.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
				<table id="organisation_size_table">
					<tr>
				    	<th>Ref #</th>    
				    	<th>Organisation Size Name</th>        
				        <th>Organisation Size Description</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>                
				    	<td>
				    		<textarea name="name" id="name" class="checkcounter" rows="5" cols="30"></textarea>
				    		<span class="textcounter" id="name_textcounter"></span>
				    	</td>        
				        <td><textarea id="description" name="description" cols="30" rows="7"></textarea></td>
				        <td><input type="button" name="save" id="save" value="Add" /></td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>		
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("organisation_sizes_logs", true)?></td>
	</tr>
</table>
