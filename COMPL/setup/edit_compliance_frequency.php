<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
$complianceObj  = new ClientComplianceFrequency();
$complianceFreq = $complianceObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>   
<table id='edit_compliance_frequency_table'>
	<tr>
		<th>Ref #</th>
		<td><?php echo $_REQUEST['id']; ?></td>
	</tr>
	<tr>
		<th>Name:</th>
		<td>
			<textarea rows="5" cols="30" class="checkcounter" name='name' id='name'><?php echo $complianceFreq['name']; ?></textarea>
			<span class="textcounter" id="name_textcounter"></span>
	</tr>
	<tr>
		<th>Description:</th>
		<td>
			<textarea name='description' id='description' cols="30" rows="7"><?php echo $complianceFreq['description']; ?></textarea>
		</td>	
	</tr>
	<tr>
		<th></th>
		<td>
			<input type='hidden' name='complianceId' id='complianceId' value='<?php echo $_REQUEST['id']; ?>' />
			<input type='submit' id='edit' value='Edit' name='edit' />
		</td>		
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td></td>
	</tr>
</table>
