<?php 
$scripts = array("sub_event.js");
include("../header.php");
$id        = ClientSubEvent::getActualId($_GET['id']);
$eventObj  = new ClientSubEvent();
$event     = $eventObj -> fetch($id);

$stpObj    = new ClientEvent();
$events    = $stpObj -> fetchAll();  
?>
<?php JSdisplayResultObj(""); ?>  
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $event['id']; ?></td>
    </tr>
    <tr>  
    	<th>Sub Event Name:</th>  
        <td>
 			<textarea rows="5" cols="30" class="checkcounter" id="name" name="name"><?php echo $event['name']; ?></textarea>
			<span class="textcounter" id="name_textcounter"></span>       	
        </td>      
    </tr>
    <tr>    
        <th>Sub Event Description:</th>
        <td>
        	<textarea rows="7" cols="30" name="description" id="description"><?php echo $event['description']; ?></textarea>
        </td>
    </tr>
    <tr>    
        <th>Main Event</th>
        <td>
        	<select id="main_event" name="main_event">
            	<option>--main event--</option>
                <?php
					foreach( $events as $key => $e){
				?>
               	 <option value="<?php echo $e['id']; ?>" <?php if($event['main_event'] == $e['id']) {?> selected <?php } ?>><?php echo $e['name']; ?></option>
                <?php
				 }
				?>
            </select>
        </td>
    </tr>    
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="eventid" value="<?php echo $id; ?>" id="eventid" />
        </td>        
    </tr> 
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>       
</table>
