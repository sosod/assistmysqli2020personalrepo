<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
$setupObj = new SetupManager();
$deptObj  = new ComplianceDepartment();
$dept     = $deptObj -> fetch($_GET['ref_id']);
$masterDepObj =  new MasterDepartment();
$masterDept   = $masterDepObj->fetch( $_GET['deptid'] );  
$sub_departments = $setupObj->getSubDepartments( new ComplianceSubdepartment(), $_GET['ref_id'] );
$master_subdepartments = $setupObj->getSubDepartments( new MasterSubDepartment() , $_GET['deptid']);
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="table_directorates">
				<tr>
					<th>Ref</th>
					<th>Master Directorate</th>
					<th>Directorates</th>
					<th></th>
					<th></th>
					<th></th>					
				</tr>
				<tr>
					<td></td>
					<td><b><?php echo $dept['name']; ?></b></td>
					<td><b><?php echo $masterDept['name']; ?></b></td>
					<td></td>
					<td></td>
					<td></td>					
				</tr>				
				<tr>
					<td>#</td>
					<td>
						<select name="subdepartment" id="subdepartment">
						  <option value="">--please select--</option>
						  <?php 
						   if(!empty($sub_departments))
						   {
							  	foreach($sub_departments as $sIndex => $sDept)
							  	{
							  ?>
								<option value="<?php echo $sDept['id']; ?>"><?php echo $sDept['name']; ?></option>
							  <?php 
							  	}						   
						   } else {
						   ?>
						    <option value="" disabled="disabled">no sub departments</option>
						   <?php
						   }
						  ?>
						</select>
					</td>
					<td>
						<select name="mastersubdepartment" id="mastersubdepartment" multiple="multiple">
						  <?php 
						  	foreach($master_subdepartments as $mIndex => $mDept)
						  	{
						  ?>
						   <option value="<?php echo $mDept['id']; ?>"><?php echo $mDept['name']; ?></option>
						  <?php 
						  	}
						  ?>						  
						</select>
					</td>
					<td>
						<input type="button" name="assignsub" id="assignsub" value="Match Sub Directorates" />
					</td>
					<td></td>
					<td></td>					 
				</tr>				
			</table>	
		</td>
	</tr>
	<tr>
		<td class="noborder">
			<input type="hidden" name="refid" id="refid" value="<?php echo $_GET['id']; ?>" />
			<input type="hidden" name="dirid" id="dirid" value="<?php echo $_GET['dirid']; ?>" />
			<input type="hidden" name="ref_id" id="ref_id" value="<?php echo $_GET['ref_id']; ?>" />
			<input type="hidden" name="deptid" id="deptid" value="<?php echo $_GET['deptid']; ?>" />
			<?php displayGoBack("", ""); ?>
		</td>
		<td class="noborder"><?php displayAuditLogLink("dir_logs", true)?></td>
	</tr>	
</table>
