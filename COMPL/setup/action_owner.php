<?php
$scripts = array("action_owner.js");
include("../header.php");
?>

<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
		<form action="">		
				<table id="action_owner_table">
				   <tr>
				   	 <td colspan="6">
				   	 	<input type="button" name="add" id="add" value="Add New" /> 
				   	 </td>
				   </tr>
					<tr>
				    	<th>Ref #</th>
				    	<th>Action Owner Titles</th>        
				        <th>Assign a user to the title</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td>
				    		<select id="owners" name="owners">
				    		  <option value="">--action owner --</option>	
				    		</select>
				    	</td>        
				        <td>
				    		<select id="users" name="users">
				    		  <option value="">--users--</option>	
				    		</select>
				    	</td>
				        <td>
				        		<input type="button" name="assign_user" id="assign_user" value="Assign" />
				        		<input type="hidden" name="usernotinlist" id="usernotinlist" value="" />
				        </td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>
			</form>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("action_owner_users_logs", true)?></td>
	</tr>
</table>
