<?php
$scripts = array("functional_service.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
		<form action="">		
				<table id="functional_service_table" width="100%">
					<!-- <tr>
						<td colspan="6">
							<input type="button" name="add" id="add" value="Add New" />
						</td>
					</tr> -->				
					<tr>
				    	<th>Ref #</th>
				    	<th>Functional Service Titles</th>        
				        <th>Assign a functional service to the title</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td>
				    		<select id="functional_service_title" name="functional_service_title">
				    		  <option value="">--please select--</option>	
				    		</select>
				    	</td>        
				        <td>
				    		<select id="master_functional_service" name="master_functional_service">
				    		  <option value="">--please select--</option>	
				    		</select>
				    	</td>
				        <td>
				        	  <input type="button" name="assign_user" id="assign_user" value="Assign" />
				        	  <input type="hidden" name="nouserinlist" id="nouserinlist" value="" />
				        	</td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>
			</form>			
		</td>
		</tr>
		<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("functional_service_users_logs", true)?></td>
	</tr>
</table>
