<?php
$scripts = array("user.js");
include("../header.php");
?>
<?php 
JSdisplayResultObj(""); 
?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
				<form method="post" id="useraccess-form" name="useraccess-form">
				<table border="1" id="useraccess_table">
				  <tr>
				    <th>Ref</th>
				    <th>User</th>
				    <th>Module Admin</th>
				    <th>Create</th>
				    <th>Import</th>
				    <th>Admin All</th>
<!--				    <th>View All</th> -->
				    <th>Admin Edit All</th>
				    <th>Admin Update All</th>
				    <th>Reports</th>
				    <th>Assurance</th>
				    <th>Setup</th>
				    <th>Action</th>
				  </tr>
				  <tr>
				    <td>#</td>
				    <td>
						<select name="userselect" id="userselect">
				        	<option value="">--select user--</option>
				        </select>
				    </td>
				    <td>
				    	<select id="module_admin" name="module_admin">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="create_legislation" name="create_legislation">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="import_legislation" name="import_legislation">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>				    
				    <td>
				    	 <select id="admin_all" name="admin_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>     
<!--				    <td>
				    	 <select id="view_all" name="view_all">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td> -->
				    <td>
				    	<select id="edit_all" name="edit_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	<select id="update_all" name="update_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>				    
				    <td>
				    	 <select id="reports" name="reports">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="assurance" name="assurance">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>    
				    <td>
				    	 <select id="usersetup" name="usersetup">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td><input type="submit" value="Add" id="setup_access" name="setup_access" /></td>
				  </tr>
				</table>
				</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "useraccess_logs", true); ?></td>
	</tr>
</table>