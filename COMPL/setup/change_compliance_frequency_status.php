<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
$complianceObj  = new ClientComplianceFrequency();
$complianceFreq = $complianceObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>  
<table id='change_compliance_frequency_table'>
	<tr>
		<th>Ref #:</th>
		<td><?php echo  $_REQUEST['id']; ?></td>
	</tr>
	<tr>
		<th>Status:</th>
		<td>
			<select id='status' name='status'>
				<option value='1' <?php echo (($complianceFreq['status'] & 1) == 1 ? "selected='selected'" : "");  ?>>Active</option>
				<option value='0' <?php echo (($complianceFreq['status'] & 1) == 0 ? "selected='selected'" : ""); ?>>Inactive</option>
			</select>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>
			<input type='hidden' name='complianceId' id='complianceId' value='<?php echo $_REQUEST['id']; ?>' />
			<input type='submit' id='update' value='Update' name='update' />
		</td>		
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td></td>
	</tr>	
</table>
