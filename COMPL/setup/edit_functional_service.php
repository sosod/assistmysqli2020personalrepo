<?php 
$scripts = array("functional_service.js");
include("../header.php");
$fxnService       = new FunctionalServiceManager();
$masterFunctionalServices = $fxnService -> getFunctionalServicesTitles();

$matchObj             = new FunctionalServiceMatch();
$fxnServiceMatch      = $matchObj -> fetch($_GET['id']);

$clientFunctionalServices = $fxnService -> getClientFunctionalServices();
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Functional Service Title:</th>
        <td>
        	<select id="functional_service_title" name="functional_service_title">
    	  		<option value="">--functional service title--</option>
    	  		<?php 
    	  			foreach($masterFunctionalServices as $key => $valu){
    	  		?>
    	  			<option <?php echo ($key == $fxnServiceMatch['ref'] ? "selected='selected'" : "disabled='disabled'"); ?>
    	  			 value="<?php echo $key; ?>"><?php echo $valu['name']; ?></option>
    	  		<?php 		
    	  			}
    	  		?>	
	    	</select>
        </td>
    </tr>
    <tr>
        <th>Functional Service:</th>
        <td>
	       <select id="master_functional_service" name="master_functional_service">
	    	  <option value="">--functional services--</option>
	    	  <?php 
	    	  	foreach($clientFunctionalServices as $index => $val){
	    	  		?>
	    	  		<option <?php echo ($index == $fxnServiceMatch['master_id'] ? "selected='selected'" : ""); ?>
	    	  		 value="<?php echo $index; ?>"><?php echo $val; ?></option>
	    	  		<?php 
	    	  	}
	    	  ?>	
	       </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="functionalserviceid" value="<?php echo $_GET['id']; ?>" id="functionalserviceid" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("deliverable_functional_service.php?ref=deliverable", ""); ?></td>
		<td class="noborder"></td>
	</tr>    
</table>
