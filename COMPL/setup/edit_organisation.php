<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:20 AM
 * To change this template use File | Settings | File Templates.
 */
 $scripts = array("responsible_organisations.js");
include("../header.php");
$legObj  = new ResponsibleOrganisations();
$leg     = $legObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>  
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Organisation Type Name</th>
        <td>
    		<textarea rows="5" cols="30" name="name" id="name" class="checkcounter"><?php echo $leg['name']; ?></textarea>
    		<span class="textcounter" id="name_textcounter"></span> 
        </td>
    </tr>
    <tr>
        <th>Organisation Type Description</th>
        <td><textarea id="description" name="description" cols="30" rows="7"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="orgid" value="<?php echo $_GET['id']; ?>" id="orgid" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>

