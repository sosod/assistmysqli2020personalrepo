 <?php
$scripts = array("reportingcategories.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
	   <td colspan="2" class="noborder">
			<table id="categories_table"> 
			  <tr>
				    <td colspan="6"><input type="button" id="addnew" name="addnew" value="Add"/></td>
			  </tr>
			  <tr>
			    <th>Ref</th>
			    <th> Name</th>
			    <th>Description</th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			</table>	   
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("reporting_categories_logs", true)?></td>
	</tr>
</table>
