<?php
$scripts = array("organisation_capacity.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
				<table id="organisation_capacity_table">
					<tr>
				    	<th>Ref</th>      
				    	<th>Organisation Capacity Name</th>        
				        <th>Organisation Capacity Description</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>               
				    	<td>
				    		<textarea rows="5" cols="30" name="name" id="name" class="checkcounter"></textarea>
				    		<span id="name_textcounter" class="textcounter"></span>
				    	</td>        
				        <td>
				        	<textarea id="description" name="description" cols="30" rows="7"></textarea>
				        </td>
				        <td><input type="button" name="save" id="save" value="Add" /></td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>		
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("organisation_capacity_logs", true)?></td>
	</tr>
</table>
