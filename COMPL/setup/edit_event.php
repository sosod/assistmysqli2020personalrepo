<?php 
$scripts = array("events.js");
include("../header.php");
$id        = ClientSubEvent::getActualId($_GET['id']);
$eventObj  = new ClientEvent();
$event     = $eventObj -> fetch( $id);  
?>
<?php JSdisplayResultObj(""); ?>   
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $event['id']; ?></td>
    </tr>
    <tr>  
    	<th>Frequency name:</th>  
        <td>
			<textarea rows="5" cols="30" class="checkcounter" id="name" name="name"><?php echo $event['name']; ?></textarea>
			<span class="textcounter" id="name_textcounter"></span>        	
        </td>      
    </tr>
    <tr>    
        <th>Frequency Description:</th>
        <td>
        	<textarea rows="7" cols="30" id="description" name="description"><?php echo $event['description']; ?></textarea>
        </td>
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="eventid" value="<?php echo $id; ?>" id="eventid" />
        </td>        
    </tr>   
	<tr>
		<td class="noborder"><?php displayGoBack("deliverable_events.php?ref=deliverable", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
