<?php
$scripts = array("glossary.js");
include("../header.php");
$category = isset($_GET['category']) ? $_GET['category'] : ''; 
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" width="100%">
     <tr>
       <td colspan="2" align="center">
          <select id="category" name="category">
             <option value="">--please select--</option>
             <option value="legislation" <?php echo ($category == "legislation" ? "selected='selected'" : ""); ?>>Legislation</option>
             <option value="deliverable" <?php echo ($category == "deliverable" ? "selected='selected'" : ""); ?>>Deliverable</option>
             <option value="action" <?php echo ($category == "action" ? "selected='selected'" : ""); ?>>Action</option>
          </select>
       </td>
     </tr>    
     <tr id="add_glossary">
       <td colspan="6" align="center">
          <input type="button" name="add" id="add" value="Add New" />
          <input type="hidden" name="_category" id="_category" value="<?php echo $category; ?>" />
       </td>
     </tr> 		
	<tr>   
	   <td colspan="2" id="table_glossary"></td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("glossary_logs", true)?></td>
	</tr>
</table>
