<?php 
$scripts = array("acts.js");
include("../header.php");
$actObj  = new Acts();
$act     = $actObj -> getAAct( $_GET['id'] );  
?>
<div>
<div class="message" id="acts_message"></div>
<table>
	<tr>
    	<th>Ref #</th>
        <td>#<?php echo $act['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($act['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($act['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td><div><a href="javascript:history.back()">Go Back</a></div></td>
        <td>
        	<input type="button" name="update_act" id="update_act" value="Update" />
        	<input type="hidden" name="actid" value="<?php echo $_GET['id']; ?>" id="actid" />
        </td>        
    </tr>    
</table>
