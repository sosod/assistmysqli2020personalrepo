<?php 
$scripts = array("legislationcategories.js");
include("../header.php");
$legObj  = new ClientLegislationCategory();
$leg     = $legObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>  
<table width="30%">

	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($leg['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($leg['status'] & 1) == 0 ? "selected='selected'" : ""); ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="legcatid" value="<?php echo $_GET['id']; ?>" id="legcatid" />
        </td>        
    </tr> 
    <tr>
    	<td><?php displayGoBack("/COMPL/setup/legislative_categories.php?ref=legislative", ""); ?></td>
    	<td></td>
    </tr>       
</table>
