<?php 
$scripts = array("events.js");
include("../header.php");
$id        = ClientSubEvent::getActualId($_GET['id']);
$eventObj  = new ClientEvent();
$event     = $eventObj -> fetch($id); 
?>
<?php JSdisplayResultObj(""); ?>  
<table >
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $id; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($event['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($event['status'] & 1) != 1 ? "selected='selected'" : ""); ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="eventid" value="<?php echo $id; ?>" id="eventid" />
        </td>        
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>  
</table>
