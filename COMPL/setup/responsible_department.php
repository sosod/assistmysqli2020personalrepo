<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="table_directorates">
				<tr>
					<th>Ref</th>
					<th>Compliance Master Directorate - Sub Directorate</th>
					<th>Client Directorate - Sub Directorate</th>
					<th></th>
					<th></th>
					<th></th>					
				</tr>
				<tr>
					<td>#</td>
					<td>
						<select name="department" id="department">
						  <option value="">--please select--</option>
						</select>
					</td>
					<td>
						<select name="masterdepartment" id="masterdepartment">
						  <option value="">--please select--</option>
						</select>
					</td>
					<td>
						<input type="button" name="assign" id="assign" value="Assign" />
					</td>
					<td></td>
					<td></td>					 
				</tr>				
			</table>	
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("dir_matches_logs", true)?></td>
	</tr>	
</table>
