<?php 
$scripts = array("legislative_frequency.js");
include("../header.php");
$legObj  = new Frequency();
$leg     = $legObj -> getAFrequency( $_GET['id'] );
?>
<div>
<div class="message" id="legislative_frequency_message"></div>
<table>
	<tr>
    	<th>Ref #</th>
        <td>#<?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Frequency name</th>
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Frequency  Description</th>
        <td><input type="text" id="description" name="description" value="<?php echo $leg['description']; ?>" class="datepicker" /></td>
    </tr>
    <tr>    
        <td><div><a href="javascript:history.back()">Go Back</a></div></td>
        <td><input type="button" name="edit_freq" id="edit_freq" value="Edit" />
        	<input type="hidden" name="freqid" value="<?php echo $_GET['id']; ?>" id="freqid" />
        </td>        
    </tr>    
</table>
