<?php
$scripts = array("jquery.ui.legislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
  <tr>
   <td colspan="2" class="noborder">
     <script>
     $(function(){
     $("#legislation").legislation({url               : "../class/request.php?action=Legislation.getAll",
                                    setupLegislation  : true,
                                    setupActivate     : true,
                                    page              : "setup_activate",
                                    section           : "setup",
                                    showFinancialYear : false,
                                    autoLoad          : true
                                   });
     });
     </script>
     <div id="legislation"></div>			
    </td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("", ""); ?></td>
    <td class="noborder"><?php //displayAuditLogLink("legislations_logs", true); ?></td>
  </tr>
</table>
