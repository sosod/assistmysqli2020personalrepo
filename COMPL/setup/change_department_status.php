<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
$matchObj     = new DepartmentMatch();
$match        = $matchObj->fetch($_GET['id']);
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="50%">
	<tr>
		<td colspan="2" class="noborder">
			<table id="tabledirectorates" width="100%">
				    <tr>
						<th>Ref#</th>
						<td><?php echo $_GET['id']; ?></td>
					</tr>
					<tr>
						<th>Status</th>
						<td>
							<select name="status" id="status">
							  <option value="">--please select--</option>
							  <option value="1" <?php echo ($match['status'] & 1) == 1 ? "selected='selected'" : ""; ?>>Activate</option>
							  <option value="0" <?php echo ($match['status'] & 1) == 0 ? "selected='selected'" : ""; ?>>Inactivate</option>
							</select>
						</td>
					</tr>
					<tr>
						<th></th>
						<td><input type="button" name="changestatus" id="changestatus" value="Save Changes" /></td>
					</tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td class="noborder">
			<input type="hidden" name="refid" id="refid" value="<?php echo $_GET['id']; ?>" />
			<?php displayGoBack("responsible_department.php?ref=responsible", ""); ?>
		</td>
		<td class="noborder"><?php displayAuditLogLink("dir_matches_logs", true)?></td>
	</tr>	
</table>
