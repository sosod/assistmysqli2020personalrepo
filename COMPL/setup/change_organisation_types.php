<?php 
$scripts = array("organisation_types.js");
include("../header.php");
$orgtypeObj  = new ClientDeliverableOrganisationType();
$orgtype     = $orgtypeObj -> fetch( $_GET['id'] );   
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($orgtype['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($orgtype['status'] & 1) != 1 ? "selected='selected'" : ""); ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td></td>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="orgtypeid" value="<?php echo $_GET['id']; ?>" id="orgtypeid" />
        </td>        
    </tr>   
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td>
    </tr> 
</table>
