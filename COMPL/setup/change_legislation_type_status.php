<?php 
$scripts = array("legislativetypes.js");
include("../header.php");
$legObj  = new ClientLegislationType();
$leg     = $legObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table width="40%">
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($leg['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($leg['status'] & 1) == 0 ? "selected='selected'" : ""); ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="legid" value="<?php echo $_GET['id']; ?>" id="legid" />
        </td>        
    </tr>    
    <tr>
    	<td class="noborder"><?php displayGoBack("/COMPL/setup/legislative_types.php?ref=legislative",""); ?></td>
    	<td class="noborder"></td>
    </tr>
</table>
