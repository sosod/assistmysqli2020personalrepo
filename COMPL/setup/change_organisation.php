<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:24 AM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("responsible_organisations.js");
include("../header.php");
$legObj  = new ResponsibleOrganisations();
$leg     = $legObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #</th>
        <td>#<?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Status</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="orgid" value="<?php echo $_GET['id']; ?>" id="orgid" />
        </td>
    </tr>
 	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>