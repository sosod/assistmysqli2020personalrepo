<?php
/**
 */
$scripts = array("responsible_owner.js");
include("../header.php");
$respObj = new ResponsibleOwnerManager();
$titles  = $respObj -> getResponsibleOwnerTitles(); 

$userObj  = new User(); 
$users    = $userObj -> getAll();

$matchObj   = new ResponsibleOwnerMatch();
$match      = $matchObj -> fetch($_GET['id']); 

?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Responsible Owner Title:</th>
        <td>
	    	<select id="responsible_owner" name="responsible_owners">
	    	  <option value="">--responsible owners--</option>	
	    	  <?php 
	    	  	foreach($titles as $index => $title){
	    	  	?>	
	    	      <option 
	    	      <?php
	    	        echo (($index == $match['ref']) ? "selected='selected'" : "disabled='disabled'");
	    	      ?>
	    	      value="<?php echo $index; ?>"><?php echo $title['name']; ?></option>
	    	  	<?php 	
	    	  	}
	    	  ?>
	    	</select>        
        </td>
    </tr>
    <tr>
        <th>User:</th>
        <td>
	    	<select id="users" name="users">
	    	  <option value="">--users--</option>
	    	  <?php foreach($users as $index => $val){ ?>	
	    	  		<option  <?php echo (($index == $match['user_id']) ? "selected='selected'" : ""); ?>
	    	  		 value="<?php echo $index; ?>"><?php echo $val['user']; ?></option>
	    	  <?php } ?>
	    	</select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" id="id" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr> 
</table>
