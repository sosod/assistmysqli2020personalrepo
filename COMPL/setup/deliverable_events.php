<?php
$scripts = array("events.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="event_table">
				<tr>
					<th>Ref</th>
					<th>Event Name</th>
					<th>Description</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
				     <td></td>
					<td>
						<textarea rows="5" cols="30" class="checkcounter" id="name" name="name"></textarea>
						<span class="textcounter" id="name_textcounter"></span>
					</td>
					<td>
						<textarea rows="7" cols="30" id="description" name="description"></textarea>
					</td>
					<td>
						<input type="button" id="save" name="save" value="Add" />
					</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("events_logs", true)?></td>
	</tr>
</table>
