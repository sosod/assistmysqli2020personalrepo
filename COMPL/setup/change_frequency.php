<?php 
$scripts = array("legislative_frequency.js");
include("../header.php");
$legObj  = new Frequency();
$leg     = $legObj -> getAFrequency( $_GET['id'] );  
?>
<div>
<div class="message" id="legislative_frequency_message"></div>
<table>
	<tr>
    	<th>Ref #</th>
        <td>#<?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td><div><a href="javascript:history.back()">Go Back</a></div></td>
        <td>
        	<input type="button" name="update_freq" id="update_freq" value="Update" />
        	<input type="hidden" name="freqid" value="<?php echo $_GET['id']; ?>" id="freqid" />
        </td>        
    </tr>    
</table>
