<?php
$scripts = array("compliance_to.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
		<form action="">		
				<table id="compliance_table">
					<tr>
						<td colspan="6">
							<input type="button" name="add" id="add" value="Add New" />
						</td>
					</tr>				
					<tr>
				    	<th>Ref #</th>
				    	<th>Compliance To Titles</th>        
				        <th>Assign a user to the title</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td>
				    		<select id="compliance_title" name="compliance_title">
				    		  <option value="">--please select--</option>	
				    		</select>
				    	</td>        
				        <td>
				    		<select id="users" name="users">
				    		  <option value="">--select--</option>	
				    		</select>
				    		<input type="text" name="titleusername" id="titleusername" placeholder="title user-name" value="" style="display:none;" /> 
				    	</td>
				        <td>
				        	<input type="hidden" name="usernotinlist" id="usernotinlist" value="" />	
				        	<input type="button" name="assign_user" id="assign_user" value="Assign" />
				        </td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>
			</form>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("compliance_to_users_logs", true)?></td>
	</tr>
</table>
