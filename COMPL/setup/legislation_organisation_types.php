<?php 
$scripts = array("legislation_organisation_types.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
			<table id="organisation_type_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Organisation Type Name</th>        
			        <th>Organisation Type Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td>
			    		<textarea rows="5" cols="30" name="name" id="name" class="checkcounter"></textarea>
			    		<span class="textcounter" id="name_textcounter"></span>
			    	</td>        
			        <td><textarea id="description" name="description" rows="7" cols="30"></textarea></td>
			        <td><input type="button" name="save" id="save" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("leg_organisation_types_logs", true)?></td>
	</tr>
</table>
</div>