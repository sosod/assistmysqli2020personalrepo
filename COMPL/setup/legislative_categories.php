<?php
$scripts = array("legislationcategories.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
				<table id="legislative_category_table">
					<tr>
				    	<th>Ref</th>
				    	<th>Legislative Categories Name</th>        
				        <th>Legislative Categories Description</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td>
				    		<textarea name="name" id="name" cols="30" rows="5" class="checkcounter"></textarea>
				    		<span class="textcounter"></span>
				    	</td>        
				        <td>
				        	<textarea id="description" name="description" cols="30" rows="7"></textarea>
				        </td>
				        <td><input type="button" name="save_legcat" id="save_legcat" value="Add" /></td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>	
			</form>	
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("legislative_categories_logs", true)?></td>
	</tr>
</table>

