<?php
$scripts = array("legislativetypes.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2"  class="noborder">
			<form action="">		
			<table id="legislative_type_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Legislative Type Name</th>        
			        <th>Legislative Type Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td>
			    		<textarea name="name" id="name" rows="5" cols="30" class="checkcounter"></textarea>
			    		<span class="textcounter"></span>
			    	</td>        
			        <td><textarea id="description" name="description" cols="30" rows="7"></textarea></td>
			        <td><input type="button" name="save_leg" id="save_leg" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("legislative_types_logs", true)?></td>
	</tr>
</table>
