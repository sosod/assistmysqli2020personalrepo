<?php
$scripts = array("sub_event.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="event_table">
				<tr>
					<th>Ref</th>
					<th>Name</th>
					<th>Description</th>
					<th>Main Event</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>#</td>
					<td>
						<textarea rows="5" cols="30" class="checkcounter" id="name" name="name"></textarea>
						<span class="textcounter" id="name_textcounter"></span>
					</td>
					<td>
						<textarea rows="7" cols="30" name="description" id="description"></textarea>
					</td>
					<td>
						<select id="main_event" name="main_event">
							<option value=""> -- event -- </option>
						</select>
					</td>
					<td><input type="button" id="save" name="save" value="Add" /></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("sub_event_logs", true)?></td>
	</tr>
</table>
