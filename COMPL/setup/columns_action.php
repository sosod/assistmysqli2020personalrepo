<?php
$scripts = array("action_columns.js");
include("../header.php");
?>
<style>
	#sortable
	{
		list-style-type:none;
		margin:0;
		padding:0;
		float:left;
		margin-right:10px;
	}
	#activeColumns li, #inactiveColumns	li
	{
		list-style:none;
		margin:0 5px 5px 5px;
		padding:5px;
		font-size:1.2em;
		width:120px;
	}
	#columnsContent
	{
		border:1px solid #000000;
		width:500px;
		clear:both;
		position:static;
	}
</style>
<?php 
JSdisplayResultObj(""); 
?>
<form id="columns_sortable"> 
<table id="sortable">
	<tr>
    	<th>Heading</th>
        <th>Show On Manage Pages</th>
        <th>Show On New Pages</th>
    </tr>   
</table>
</form>
<div style="clear:both"><?php displayGoBack("",""); ?></div>