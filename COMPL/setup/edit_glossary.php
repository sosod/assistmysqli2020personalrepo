<!-- Load jQuery -->
<?php 
$scripts = array("glossary.js");
include("../header.php");
$glossaryObj = new Glossary();
$glossary = $glossaryObj -> fetch($_GET['id']);

$category = ucwords($glossary['category'])."Naming";
$namingObj = new $category();
$fields = $namingObj -> getNaming();
?>
<!-- Load TinyMCE -->
<style type="text/css">
   /* body { background: #ccc;} */
   div.jHtmlArea .ToolBar ul li a.custom_disk_button 
   {
       background: url(images/disk.png) no-repeat;
       background-position: 0 0;
   }
   
   div.jHtmlArea { border: solid 1px #ccc; }
</style>
<script type="text/javascript" src="../../library/tinymce/jtextarea/scripts/jHtmlArea-0.7.5.js"></script>
<link rel="Stylesheet" type="text/css" href="../../library/tinymce/jtextarea/style/jHtmlArea.css" />
<script type="text/javascript">    
   // You can do this to perform a global override of any of the "default" options
   // jHtmlArea.fn.defaultOptions.css = "jHtmlArea.Editor.css";

   $(function() {
       //$("textarea").htmlarea(); // Initialize all TextArea's as jHtmlArea's with default values

       //$("#txtDefaultHtmlArea").htmlarea(); // Initialize jHtmlArea's with all default values

       $("#txtDefaultHtmlArea").htmlarea({
           // Override/Specify the Toolbar buttons to show
           toolbar: [
               ["bold", "italic", "underline", "|", "forecolor", "orderedList", "unorderedList", "justifyleft", "justifycenter"],
               ["p", "h1", "h2", "h3", "h4", "h5", "h6", "increasefontsize", "decreasefontsize"],
           ],

           // Override any of the toolbarText values - these are the Alt Text / Tooltips shown
           // when the user hovers the mouse over the Toolbar Buttons
           // Here are a couple translated to German, thanks to Google Translate.
           toolbarText: $.extend({}, jHtmlArea.defaultOptions.toolbarText, {
                   "bold": "fett",
                   "italic": "kursiv",
                   "underline": "unterstreichen"
               }),

           // Specify a specific CSS file to use for the Editor
          css: "../../library/tinymce/style//jHtmlArea.Editor.css",

           // Do something once the editor has finished loading
           loaded: function() {
               //// 'this' is equal to the jHtmlArea object
               //alert("jHtmlArea has loaded!");
               //this.showHTMLView(); // show the HTML view once the editor has finished loading
           }
       });
   });
</script>
<?php JSdisplayResultObj(""); ?> 
<form id="glossary" name="glossary">
<table>
	<tr>
    	   <th>Ref #</th>
        <td><?php echo ucwords($glossary['category']); ?></td>
    </tr>
    <tr>  
    	   <th>Field : </th>  
        <td>
          <?php              
             foreach($fields as $index => $value)
             {
                echo ($glossary['field'] == $value['id'] ? $value['client_terminology'] : "");  
             }
          ?>
        </td>      
    </tr>
    <tr>    
        <th>Purpose : </th>
        <td><textarea id="purpose" name="purpose" cols="120" rows="25"><?php echo $glossary['purpose']; ?></textarea></td>
    </tr>
    <tr>    
        <th>Rules : </th>
        <td>
         <textarea id="txtDefaultHtmlArea" cols="80" rows="15" name="rules"><?php echo $glossary['rules']; ?></textarea>
         <!--
         <input type="button" value="Alert HTML" onclick="alert($('#txtDefaultHtmlArea').htmlarea('html'));" />
         <input type="button" value="Set HTML" onclick="$('#txtDefaultHtmlArea').htmlarea('html', 'Some <strong>HTML</strong> value.')" />
         <input type="button" value="Change Color to Blue" onclick="$('#txtDefaultHtmlArea').htmlarea('forecolor', 'blue');" />
         -->
         
         <br /><hr /><br />          
          <!-- <textarea id="rule_description" name="rule_description" cols="80" rows="4" ></textarea>
          <div>Rule <textarea id="rule_1" name="rule_1" cols="80" rows="4" class="rules" ></textarea></div>
          <div id="addRule" ><input type="button" name="add_rule" id="add_rule" value="Add Rule" /></div> -->
        </td>
    </tr>    
    <tr>    
        <th></th>
        <td>
          <input type="button" name="edit" id="edit" value="Save Changes" />
          <input type="hidden" name="category" id="category" value="<?php echo $glossary['category']; ?>" />
          <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
        </td>        
    </tr>
    <tr>
	   <td class="noborder"><?php displayGoBack("glossary.php?category=".$glossary['category'], "Go Back"); ?></td>
	   <td class="noborder"></td>
    </tr>    
</table>
</form> 
