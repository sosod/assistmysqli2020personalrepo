<?php
$scripts = array("jscolor.js", "status.js", "deliverablestatus.js");
include("../header.php");
JSdisplayResultObj("");
?>
<table class="noborder">
 <tr>
   <td colspan="2" class="noborder">
   <table width="100%" id="deliverablestatus_table"">
      <tr>
        <td colspan="6">
          <input type="button" name="add" value="Add New" id="add" />
        </td>
      </tr>
      <tr>
        <th>Ref</th>
        <th>Deliverable Status</th>
        <th>Client Terminology</th>
        <th>Color</th>
        <th>Status</th>
        <th></th>
      </tr>  
      <tr class="status">
        <td colspan="6">There are no deliverable status</td>
      </tr>                      
   </table>
   </td>
 </tr>
 <tr>
   <td class="noborder"><?php displayGoBack("", ""); ?></td>
   <td class="noborder"><?php displayAuditLogLink("deliverable_status_logs", true) ?></td>
 </tr>
</table>
