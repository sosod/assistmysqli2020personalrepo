<?php
$scripts = array("accountable_person.js");
include("../header.php");
$actObj  = new AccountablePersonMatch();
$act     = $actObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $act['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($act['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($act['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="accountable_id" value="<?php echo $_GET['id']; ?>" id="accountable_id" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>   
</table>
