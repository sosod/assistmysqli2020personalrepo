<?php 
$scripts = array("setup_defaults.js");
include("../header.php");
?>
<div class="displayview">
<table class=form id="link_to_page">
    <tr>
    	<th>Module Defaults:</th>
        <td>Configure the module defaults.</td>
        <td><input type="button" name="module_defaults" id="module_defaults" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the menu headings to be displayed in the module</td>
        <td><input type="button" name="menu_and_headings" id="menu_and_headings" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the naming convention of the Compliance fields for the module </td>
        <td><input type="button" name="naming_legislation" id="naming_legislation" value="Configure" class="setup_defaults"  /></td>
    </tr>  
    <tr>
    	<th>Legislation Types:</th>
        <td>Configure the Legislation Types</td>
        <td><input type="button" name="legislative_types" id="legislative_types" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Legislation Categories:</th>
        <td>Define Legislation Categories</td>
        <td><input type="button" name="legislative_categories" id="legislative_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th>Legislation Organisation Types:</th>
        <td>Configure the Legislation Organisation Types</td>
        <td><input type="button" name="legislation_organisation_types" id="legislation_organisation_types" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    	<th>Legislation Organisation Size:</th>
        <td>Configure the Legislation Organisation Size</td>
        <td><input type="button" name="legislation_organisation_size" id="legislation_organisation_size" value="Configure" class="setup_defaults"  /></td>
    </tr>  
     <tr>
    	<th>Legislation Organisation Capacity:</th>
        <td>Configure the Legislation Organisation Capacity</td>
        <td><input type="button" name="legislation_organisation_capacity" id="legislation_organisation_capacity" value="Configure" class="setup_defaults"  /></td>
    </tr>    
   <tr>
    	   <th>Legislation Status:</th>
        <td>Configure the Legislation Status</td>
        <td><input type="button" name="legislation_status" id="legislation_status" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Deliverable Responsible Department:</th>
        <td>Configure the Deliverable Responsible Department</td>
        <td><input type="button" name="responsible_department" id="responsible_department" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Compliance Frequency:</th>
        <td>Define the Deliverable Compliance Frequency</td>
        <td><input type="button" name="deliverable_compliance_frequency" id="deliverable_compliance_frequency" value="Configure" class="setup_defaults"  /></td>
    </tr>	
    <tr>
    	<th>Deliverable Functional Service:</th>
        <td>Configure the Deliverable Functional Service</td>
        <td><input type="button" name="deliverable_functional_service" id="deliverable_functional_service" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Responsible Person:</th>
        <td>Define Deliverable Responsible Person</td>
        <td><input type="button" name="deliverable_responsible_person" id="deliverable_responsible_person" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Accountable Person:</th>
        <td>Define Deliverable Accountable Person</td>
        <td><input type="button" name="deliverable_accountable_person" id="deliverable_accountable_person" value="Configure" class="setup_defaults"  /></td>
    </tr>	    
    <tr>
    	<th>Deliverable Compliance To:</th>
        <td>Configure the Deliverable Compliance To</td>
        <td><input type="button" name="deliverable_compliance_to" id="deliverable_compliance_to" value="Configure" class="setup_defaults"  /></td>
    </tr>   
    <tr>
     <tr>
    	<th>Deliverable Organisation Types:</th>
        <td>Configure the Deliverable Organisation Types</td>
        <td><input type="button" name="deliverable_organisation_types" id="deliverable_organisation_types" value="Configure" class="setup_defaults"  /></td>
    </tr>   
    <tr>
    	<th>Deliverable Events:</th>
        <td>Define the Deliverable Events</td>
        <td><input type="button" name="deliverable_events" id="deliverable_events" value="Configure" class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Deliverable Sub Events:</th>
        <td>Define the Deliverable Sub Events</td>
        <td><input type="button" name="deliverable_sub_events" id="deliverable_sub_events" value="Configure" class="setup_defaults" /></td>
    </tr> 
    <tr>
    	<th>Deliverable Reporting Category:</th>
        <td>Define the Deliverable Reporting Categories</td>
        <td><input type="button" name="deliverable_reporting_category" id="deliverable_reporting_category" value="Configure" class="setup_defaults" /></td>
    </tr> 
   <tr>
    	   <th>Deliverable Status:</th>
        <td>Configure the Deliverable Status</td>
        <td><input type="button" name="deliverable_status" id="deliverable_status" value="Configure" class="setup_defaults"  /></td>
    </tr>        
    <tr>
    	<th>Action Owner:</th>
        <td>Define the Action Owner</td>
        <td><input type="button" name="action_owner" id="action_owner" value="Configure" class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Activate Legislations:</th>
        <td>Select legislations to be used</td>
        <td><input type="button" name="activate_legislations" id="activate_legislations" value="Configure" class="setup_defaults"  /></td>
    </tr>  
   <tr>
    	   <th>Action Status:</th>
        <td>Configure the Action Status</td>
        <td><input type="button" name="action_status" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr>       	
    <tr>
    	<th>Glossary:</th>
        <td>Define Glossary Terms</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>  	
    <tr>
    	<th>List Columns:</th>
        <td>Configure which columns dipslay on the New and Manage pages</td>
        <td><input type="button" name="columns_legislation" id="columns_legislation" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Data Usage Report:</th>
        <td>View the data storage in use in this module.</td>
        <td><input type="button" name="datareport" id="datareport" value="Configure" class="setup_defaults"  /></td>
    </tr>    
</table>
</div>
<div><?php displayGoBack("", ""); ?></div>
