<?php 
$scripts = array("legislationcategories.js");
include("../header.php");
$legObj  = new ClientLegislationCategory();
$leg     = $legObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>  
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Legislative Category Name:</th>  
        <td>
        	<textarea rows="5" cols="30" class="checkcounter" id="name" name="name"><?php echo $leg['name']; ?></textarea>
			<span class="textcounter"></span>
        </td>      
    </tr>
    <tr>    
        <th>Legislative Category Description:</th>
        <td><textarea id="description" rows="7" cols="30" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="legcatid" value="<?php echo $_GET['id']; ?>" id="legcatid" />
        </td>        
    </tr>    
    <tr>
    	<td><?php displayGoBack("/COMPL/setup/legislative_categories.php?ref=legislative", ""); ?></td>
    	<td></td>
    </tr>
</table>
