<?php
$scripts = array("compliance_to.js");
include("../header.php");
$matchObj = new ComplianceToMatch();
$match    = $matchObj -> fetch($_GET['id']); 
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $match['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($match['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>
            	<option value="0" <?php echo (($match['status'] & 1) == 0 ? "selected='selected'" : ""); ?>>Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="complianceid" value="<?php echo $_GET['id']; ?>" id="complianceid" />
        </td>
    </tr>
 	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
