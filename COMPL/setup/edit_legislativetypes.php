<?php 
$scripts = array("legislativetypes.js");
include("../header.php");
$legObj  = new ClientLegislationType();
$leg     = $legObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Legislative Type Name:</th>  
        <td>
           <textarea id="name" name="name" cols="30" rows="5" class="checkcounter"><?php echo $leg['name']; ?></textarea>
           <span class="textcounter"></span>
       	</td>      
    </tr>
    <tr>    
        <th>Legislative Type Description:</th>
        <td>
        	<textarea id="description" name="description" cols="30" rows="7"><?php echo $leg['description']; ?></textarea>
        </td>
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="edit_leg" id="edit_leg" value="Edit" />
        	<input type="hidden" name="legid" value="<?php echo $_GET['id']; ?>" id="legid" />
        </td>        
    </tr>    
    <tr>
       <td class="noborder"><?php displayGoBack("/COMPL/setup/legislative_types.php?ref=legislative",""); ?></td>
       <td class="noborder"></td>
    </tr>
</table>
