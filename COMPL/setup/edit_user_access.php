<?php
$scripts = array("user.js");
include("../header.php");
$userObj = new UserSetting();
$optionSql  = " UA.id = '".$_GET['id']."' ";
$user       = $userObj -> getUser($optionSql); 
?>
<?php 
JSdisplayResultObj(""); 
?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
				<form method="post" id="useraccess-form" name="useraccess-form">
				<table border="1" id="useraccesstable">
				  <tr>
				    <th>Ref#</th>
					<td><?php echo $user['id']; ?></td>
				  </tr>
				  <tr>
				    <th>User</th>
				    <td>
						<?php echo $user['user']; ?>		
				    </td>					
				  </tr>
				  <tr>
				    <th>Module Admin</th>	
				    <td>
				    	<select id="module_admin" name="module_admin">
				        	<option>--module admin--</option>
				            <option value="1" <?php echo (($user['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 1) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>
				  <tr>
				    <th>Create</th>
				    <td>
				    	 <select id="create_legislation" name="create_legislation">
				        	<option>--create legislation--</option>
				            <option value="1" <?php echo (($user['status'] & 4) == 4 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 4) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>
				  <tr>
				    <th>Import</th>
				    <td>
				    	 <select id="import_legislation" name="import_legislation">
				        	<option>--import legislation--</option>
				            <option value="1" <?php echo (($user['status'] & 1024) == 1024 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 1024) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>				  
				  <tr>
				    <th>Admin All</th>
				    <td>
				    	 <select id="admin_all" name="admin_all">
				        	<option>--admin all--</option>
				            <option value="1" <?php echo (($user['status'] & 8) == 8 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 8) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>				  
				    <!-- <th>Create Actions</th> -->
<!--				  <tr> 
					<th>View All</th>
				    <td>
				    	 <select id="view_all" name="view_all">
				        	<option>--view all--</option>
				            <option value="1" <?php //echo (($user['status'] & 32) == 32 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php //echo (($user['status'] & 32) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr> -->
				  <tr>
				    <th>Edit All</th>
				    <td>
				    	<select id="edit_all" name="edit_all">
				        	<option>--edit_all--</option>
				            <option value="1" <?php echo (($user['status'] & 64) == 64 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 64) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>
				  </tr>
				  <tr>
					<th>Update All</th>
				    <td>
				    	<select id="update_all" name="update_all">
				        	<option>--update all--</option>
				            <option value="1" <?php echo (($user['status'] & 16) == 16 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 16) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>	
				  <tr>
				    <th>Reports</th>
				    <td>
				    	 <select id="reports" name="reports">
				        	<option>--reports--</option>
				            <option value="1" <?php echo (($user['status'] & 128) == 128 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 128) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>	
				  <tr>
				    <th>Assurance</th>
				    <td>
				    	 <select id="assurance" name="assurance">
				        	<option>--assurance--</option>
				            <option value="1" <?php echo (($user['status'] & 256) == 256 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 256) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>   					
				  </tr>	
				  <tr>
				    <th>Setup</th>
				    <td>
				    	 <select id="usersetup" name="usersetup">
				        	<option>--setup--</option>
				            <option value="1" <?php echo (($user['status'] & 512) == 512 ? "selected='selected'" : ""); ?>>yes</option>
				            <option value="0" <?php echo (($user['status'] & 512) == 0 ? "selected='selected'" : ""); ?>>no</option>            
				        </select>
				    </td>					
				  </tr>	
				  <tr>
				    <td></td>
				    <td>
						<input type="hidden" value="<?php echo $_GET['id'] ?>" id="userid" name="userid" />
						<input type="submit" value="Save Changes" id="edit" name="edit" />
					</td>
				  </tr>
				</table>
				</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("user_access.php", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
