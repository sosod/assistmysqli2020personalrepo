<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="" method="post">
			<table id='compliance_frequency_table'>
				<tr>
					<th>Ref</th>
					<th>Name</th>
					<th>Description</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>#</td>
					<td>
						<textarea rows="5" cols="30" name="name" id="name" class="checkcounter"></textarea>
						<span class="textcounter" id="name_textcounter"></span>
					</td>
					<td>
						<textarea name='description' id='description' cols="30" rows="7"></textarea>
					</td>
					<td><input type='submit' id='save' value='Add' name='save' /></td>
					<td></td>
					<td></td>		
				</tr>
			</table>
			</form>	
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("compliance_frequency_logs", true)?></td>
	</tr>
</table>