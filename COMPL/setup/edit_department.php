<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
$departmentObj = new DepartmentManager();
$masterDepartment = $departmentObj -> getMasterDepartments();

$matchObj     = new DepartmentMatch();
$match        = $matchObj->fetch($_GET['id']);

$clientObj   = new ClientDepartment();
$clientDepartments     = $clientObj -> getAll();

?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="tabledirectorates">
				    <tr>
						<th>Ref</th>
						<td><?php echo $_GET['id']; ?></td>
					</tr>
					<tr>
						<th>Compliance Master Directorate - Sub Directorate</th>
						<td>
							<select name="department" id="department">
							  <option value="">--please select--</option>
							  <?php 
							  	foreach($masterDepartment as $sIndex => $sDept)
							  	{
							  	?>
							   <option value="<?php echo $sIndex; ?>"
								<?php echo ($match['ref_id'] == $sIndex ? "selected='selected'" : "disabled='disabled'"); ?>
							   ><?php echo $sDept['name']; ?></option>
							  <?php 
							  	}
							  ?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Client Directorate - Sub Directorate</th>
						<td>
							<select name="masterdepartment" id="masterdepartment" >
							  <?php 
							  	foreach($clientDepartments as $mIndex => $mDept)
							  	{
							  ?>
							   <option value="<?php echo $mDept['id']; ?>"
								<?php echo ($match['dept_id'] == $mDept['id'] ? "selected='selected'" : "") ?>
								><?php echo $mDept['name']; ?></option>
							  <?php 
							  	}
							  ?>						  
							</select>
						</td>
					</tr>
					<tr>
						<th></th>
						<td><input type="button" name="edit" id="edit" value="Save Changes" /></td>
					</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td class="noborder">
			<input type="hidden" name="refid" id="refid" value="<?php echo $_GET['id']; ?>" />
			<?php displayGoBack("responsible_department.php?ref=responsible", ""); ?>
		</td>
		<td class="noborder"><?php displayAuditLogLink("dir_matches_logs", true)?></td>
	</tr>	
</table>
