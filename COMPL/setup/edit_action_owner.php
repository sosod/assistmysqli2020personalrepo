<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("action_owner.js");
include("../header.php");
$ownerObj  	= new ActionOwnerManager();
$ownerTitles   = $ownerObj -> getActionOwnerTitles(); 

$usersObj 	= new User();
$users 		= $usersObj -> getAll();

$matchObj = new ActionOwnerMatch();
$match    = $matchObj -> fetch($_GET['id']); 

?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Title:</th>
        <td>
        	<select id="owner" name="owner">
    	  		<option value="">--accountable person--</option>
    	  		<?php 
    	  			foreach($ownerTitles as $titleId => $title)
    	  			{
    	  		?>
    	  			<option <?php echo (($titleId == $match['ref']) ? "selected='selected'" : "disabled=''disabled'"); ?>
    	  			 value="<?php echo $titleId; ?>"><?php echo $title['name']; ?></option>
    	  		<?php 		
    	  			}
    	  		?>	
	    	</select>
        </td>
    </tr>
    <tr>
        <th>User:</th>
        <td>
	       <select id="users" name="users">
	    	  <option value="">--users--</option>
	    	  <?php 
	    	  	foreach($users as $userId => $user)
	    	  	{
	    	  ?>
    	  		<option <?php echo (($userId == $match['user_id']) ? "selected='selected'" : ""); ?>
    	  		 value="<?php echo $userId; ?>"><?php echo $user['user']; ?></option>
	    	  <?php 
	    	  	}
	    	  ?>	
	       </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="ownerid" value="<?php echo $_GET['id']; ?>" id="ownerid" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>    
</table>
