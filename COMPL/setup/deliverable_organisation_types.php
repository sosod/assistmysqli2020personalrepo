<?php
$scripts = array("organisation_types.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>  
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="organisation_type_table">
				<tr>
					<th>Ref</th>
					<th>Organisation Type Name</th>
					<th>Description</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>#</td>
					<td>
						<textarea name="name" id="name" cols="30" rows="5" class="checkcounter"></textarea>
						<span class="textcounter" id="name_textcounter"></span>
					</td>
					<td><textarea id="description" name="description" cols="30" rows="7"></textarea></td>
					<td><input type="button" name="add" value="Add" id="add" /></td>
					<td></td>
					<td></td>
				</tr>							
			</table>
		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("organisation_types_logs", true)?></td>
	</tr>
</table>
