<?php
$scripts = array("module_defaults.js");
$declare_loader = false;
require_once '../../module/loader.php';
include("../header.php");
error_reporting(-1);

$me = new COMPL_MODULE_DEFAULTS();

$defaults = $me->getAll();

$me->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$i = 1;
//echo date("d M Y H:i:s");
?>
<div class=float style="width: 350px;">
<p class=iinform>Please note:</p>
<p>* Automatic updates will occur at midnight each night and not immediately following an Action update.</p>
<p>+ If a new Action/Deliverable is added (this includes triggering an event-driven Deliverable) after a Deliverable/Legislation is marked as Completed, the Deliverable/Legislation will be restored to In Progress during the nightly automatic update.</p>
</div>
<table class=list>
<?php
foreach($defaults as $d) {
	echo "
	<tr>
		<th>".$i."</th>
		<td>".$d['text']."
		<span class=float style='margin-left: 20px'><select ref=".$d['id']." id=answer_".$d['id'].">";
	foreach($d['options'] as $x => $v) {
		echo "<option ".($x==$d['value'] ? "selected" : "")." value=$x>$v</option>";
	}
	echo "
			</select> <input type=button value=Save id=".$d['id']." class=save_one /></span></td>
	</tr>";
	$i++;
}
?>
	<tr>
		<th></th>
		<td class=right><input type=button value='Save All' class='isubmit save_all' /></td>
	</tr>
</table>
<?php
$me->displayLogs();
//$me->displayGoBack("defaults.php");
?>