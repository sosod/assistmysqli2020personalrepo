<?php 
$scripts = array("compliance_to.js");
include("../header.php");
$complianceObj = new ComplianceToManager();
$titles   = $complianceObj -> getComplianceToTitles(); 

$matchObj = new ComplianceToMatch();
$match    = $matchObj -> fetch($_GET['id']); 

$userObj = new User();
$users   = $userObj -> getAll(); 

?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Title:</th>
        <td>
        	<select id="compliance_title" name="compliance_title">
    	  		<option value="">--compliance title--</option>
    	  		<?php 
    	  			foreach($titles as $titleId => $title)
    	  			{
    	  		?>
    	  			<option <?php echo ($titleId == $match['ref'] ? "selected='selected'" : "disabled='disabled'"); ?>
    	  			 value="<?php echo $titleId; ?>"><?php echo $title['name']; ?></option>
    	  		<?php 		
    	  			}
    	  		?>	
	    	</select>
        </td>
    </tr>
    <tr>
        <th>Users:</th>
        <td>
		       <select id="users" name="users">
		    	  <option value="">--users--</option>
		    	  <?php 
		    	  	foreach($users as $userID => $user)
		    	  	{
		    	   ?>
	    	  		<option <?php echo ($userID == $match['user_id'] ?  "selected='selected'" : ""); ?>
	    	  		 value="<?php echo $userID; ?>"><?php echo $user['user']; ?></option>
	    	  		<?php 
		    	  	}
		    	  ?>	
		       </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="complianceid" value="<?php echo $_GET['id']; ?>" id="complianceid" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>    
</table>
