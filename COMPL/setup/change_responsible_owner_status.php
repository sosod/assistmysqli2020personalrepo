<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("responsible_owner.js");
include("../header.php");
$matchObj  = new ResponsibleOwnerMatch();
$match     = $matchObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $match['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($match['status'] & 1) == 1 ? "selected='selected'" : "");  ?>>Active</option>
            	<option value="0" <?php echo (($match['status'] & 1) == 0 ? "selected='selected'" : ""); ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" id="id" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>     
</table>
