<?php 
$scripts = array("organisation_types.js");
include("../header.php");
$orgtypeObj  = new ClientDeliverableOrganisationType();
$orgtype     = $orgtypeObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgtype['id']; ?></td>
    </tr>
    <tr>  
    	<th>Organisation Type Name:</th>  
        <td><textarea name="name" id="name" cols="30" rows="5"><?php echo $orgtype['name']; ?></textarea></td>      
    </tr>
    <tr>    
        <th>Organisation Type Description:</th>
        <td><textarea id="description" name="description" cols="30" rows="7"><?php echo $orgtype['description']; ?></textarea></td>
    </tr>
    <tr>    
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="orgtypeid" value="<?php echo $_GET['id']; ?>" id="orgtypeid" />
        </td>        
    </tr> 
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td>
    </tr>        
</table>
