<?php 
$scripts = array("legislation_organisation_types.js");
include("../header.php");
$legObj  = new ClientLegislationOrganisationType();
$leg     = $legObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Organisation Type Name</th>  
        <td>
        	<textarea name="name" id="name" rows="5" cols="30" class="checkcounter"><?php echo $leg['name']; ?></textarea>
        	<span class="textcounter" id="name_textcounter"></span>
        </td>      
    </tr>
    <tr>    
        <th>Organisation Type Description</th>
        <td><textarea id="description" name="description" rows="7" cols="30"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="orgtypeid" value="<?php echo $_GET['id']; ?>" id="orgtypeid" />
        </td>        
    </tr>
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td>
    </tr>    
</table>
