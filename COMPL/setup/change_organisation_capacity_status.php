<?php 
$scripts = array("organisation_capacity.js");
include("../header.php");
$orgcapacityObj = new ClientOrganisationCapacity();
$orgcapacity  	= $orgcapacityObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgcapacity['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($orgcapacity['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($orgcapacity['status'] & 1) == 0 ? "selected='selected'" : ""); ?>>Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="update_orgcapacity" id="update_orgcapacity" value="Update" />
        	<input type="hidden" name="orgcapacityid" value="<?php echo $_GET['id']; ?>" id="orgcapacityid" />
        </td>        
    </tr>    
  	<tr>
		<td class="noborder"><?php displayGoBack("legislation_organisation_capacity.php?ref=legislation", ""); ?></td>
		<td class="noborder"></td>
	</tr>    
</table>
