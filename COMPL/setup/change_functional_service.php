<?php
$scripts = array("functional_service.js");
include("../header.php");
$funcObj  = new FunctionalServiceMatch();
$leg     = $funcObj -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="functionalserviceid" value="<?php echo $_GET['id']; ?>" id="functionalserviceid" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("deliverable_functional_service.php?ref=deliverable", ""); ?></td>
		<td class="noborder"></td>
	</tr>        
</table>
