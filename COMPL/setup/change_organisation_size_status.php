<?php 
$scripts = array("organisation_size.js");
include("../header.php");
$orgsizeObj = new ClientOrganisationSize();
$orgsize  	= $orgsizeObj -> fetch( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgsize['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php echo (($orgsize['status'] & 1) == 1 ? "selected='selected'" : ""); ?>>Active</option>                
            	<option value="0" <?php echo (($orgsize['status'] & 1) == 0 ? "selected='selected'" : ""); ?>>Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="orgsizeid" value="<?php echo $_GET['id']; ?>" id="orgsizeid" />
        </td>        
    </tr>    
    <tr>
    	<td class="noborder"><?php displayGoBack("",""); ?></td>
    	<td class="noborder"></td>
    </tr>
</table>
