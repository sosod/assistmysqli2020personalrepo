<?php
$scripts = array("responsible_owner.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">		
				<table id="responsible_owner_table">
					<tr>
						<td colspan="6">
							<input type="button" name="add" id="add" value="Add New" />
						</td>
					</tr>
					<tr>
				    	<th>Ref #</th>
				    	<th>Responsible Owners Titles</th>        
				        <th>Assign a user to the title</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td>
				    		<select id="responsible_owner" name="responsible_owners">
				    		  <option value="">--responsible owners--</option>	
				    		</select>
				    	</td>        
				        <td>
				    		<select id="users" name="users">
				    		  <option value="">--users--</option>	
				    		</select>
				    	</td>
				        <td>
				        	 <input type="button" name="assign_user" id="assign_user" value="Assign" />
				        	 <input type="hidden" name="usernotinlist" id="usernotinlist" value="" />
				        	</td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>
			</form>	
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("responsible_owner_users_logs", true)?></td>
	</tr>
</table>
