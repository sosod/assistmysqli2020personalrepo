<?php
$scripts = array("accountable_person.js");
include("../header.php");
$accObj  	= new AccountablePerson();
$titles   = $accObj -> getAll();

$usersObj    = new User();
$users 	   = $usersObj -> getAll();

$matchObj    = new AccountablePersonMatch();
$match       = $matchObj -> fetch($_GET['id']);
?>
<?php JSdisplayResultObj(""); ?> 
<table>
	<tr>
    	<th>Ref #</th>
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
    	<th>Title:</th>
        <td>
        	<select id="accountable_person" name="accountable_person">
    	  		<option value="">--accountable person--</option>
    	  		<?php 
    	  			foreach($titles as $titleID => $title){
    	  		?>
    	  			<option <?php echo (($titleID == $match['ref']) ? "selected='selected'" : "disabled='disabled'"); ?>
    	  			 value="<?php echo $titleID; ?>"><?php echo $title['name']; ?></option>
    	  		<?php 		
    	  			}
    	  		?>	
	    	</select>
        </td>
    </tr>
    <tr>
        <th>Accountable Person:</th>
        <td>
	       <select id="users" name="users">
	    	  <option value="">--users--</option>
	    	  <?php 
	    	  	foreach($users as $userID => $user){
	    	  		?>
	    	  		<option <?php echo (($userID == $match['user_id']) ? "selected='selected'" : ""); ?>
	    	  		 value="<?php echo $userID; ?>"><?php echo $user['user']; ?></option>
	    	  		<?php 
	    	  	}
	    	  ?>	
	       </select>
        </td>
    </tr>
    <tr>
        <th></th>
        <td><input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="accountable_id" value="<?php echo $_GET['id']; ?>" id="accountable_id" />
        </td>
    </tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>    
</table>
