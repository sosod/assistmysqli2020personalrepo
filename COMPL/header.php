<?php
include_once("inc_ignite.php");

//error_reporting(-1);

$unspecified_text = "Unspecified";
if(!isset($declare_loader) || $declare_loader===true) {
	include_once("loader.php");
} else {
	require_once("../../library/include_for_admire_modules.php");
}
@session_start();
spl_autoload_register("Loader::autoload");
$controller = new MainController();

$helper = new ASSIST_HELPER();

?>
<html>
<head>
<meta charset="utf8" />
<script type="text/javascript" src="/library/js/assisthelper.js"></script>
<script type="text/javascript" src="/library/js/assiststring.js"></script>
<script type="text/javascript" src="/library/js/assistform.js"></script>

<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="/library/jquery-ui-1.9.1/css/jquery-ui-1.9.1.custom.css" type="text/css" />
<link rel="stylesheet" href="/assist.css" type="text/css" />
<?php 
$scripts = array_merge( $scripts, array("generic_functions.js"));
if(!empty($scripts)){
	$controller->loadJs($scripts);
}
if(!empty($styles)){ 
	$controller->loadCss($styles);
}
?>
</head>
<style type=text/css>
.ui-state-info .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);
}
.ui-state-fail .ui-icon, .ui-state-error .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_cd0a0a_256x240.png);
}
.ui-state-ok .ui-icon, .ui-state-yes .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png);
}
body{
	font-size:60%;
}
.ui-datepicker{
	display:none;
}
.hand { cursor: hand; }
.small-button { font-size: 7pt; }

#audit_logs tr.log td {
	color: #777777;
}
#audit_logs th {
	background-color: #777777;
	text-align: center;
}

</style>
<body topmargin="0" leftmargin="5" bottommargin="0" rightmargin="5">
<?php 
$controller->displayMenu();
if(isset($customMenu) && !empty($customMenu))
{
   echoNavigation(2,$customMenu); 
}
$page_title = isset($page_title) ? $page_title : "";
$controller->wasWhere($page_title);
?>