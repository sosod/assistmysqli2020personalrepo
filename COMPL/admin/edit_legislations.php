<?php
$scripts = array("jquery.ui.legislation.js");
include("../header.php");
?>
<?php 
//JSdisplayResultObj(""); 
ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r'])) ? $_REQUEST['r'] : array());
?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<script>
				$(function(){
					$("#legislation").legislation({page:"edit", section:"manage", editLegislation:true, viewType:"all", adminPage:true});
				});
			</script>
			<div id="legislation"></div>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("menu_logs", true)?></td>
	</tr>
</table>
