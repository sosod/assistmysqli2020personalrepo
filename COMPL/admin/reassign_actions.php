<?php
$scripts = array('jquery.ui.action.js');
include("../header.php");
?>
<script>
$(function(){
     $("#display_action").action({reassignAction:true, filterByFinancialYear:true, autoLoad:false, section:"admin"});		
});

</script>
<?php JSdisplayResultObj(""); ?>   
<div id="display_action"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
	<span class="ui-icon ui-icon-info" style="float:left;"></span>
	<span>Please select the user to list their actions for re-assignment</span>
</p>
