
<?php
$scripts = array("action.js", "jquery.ui.action.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?> 
<script language="javascript">
$(function(){
     $("#display_action").action({editAction:true, deleteActions:true, page:"edit", section:"admin", filterByFinancialYear:true, filterByLegislation:true, filterByDeliverable:true, autoLoad:false});	
})
</script>
<div id="display_action"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
	<span class="ui-icon ui-icon-info" style="float:left;"></span>
	<span>Please select the the legislation and deliverable to list their action</span>
</p>
