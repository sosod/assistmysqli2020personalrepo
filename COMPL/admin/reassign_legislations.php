<?php
$scripts = array("jquery.ui.legislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<script>
$(function(){
$("#legislation").legislation({url              : "../class/request.php?action=Legislation.getAll",
                               setupLegislation : true,
                               setupEdit        : true,
                               editSetup        : true,
                               section          : "admin",
                               page             : "reassign"
                              });
});
</script>
<div id="legislation"></div>			
