<?php
$scripts = array('jquery.ui.deliverable.js');
include("../header.php");
?>
<script>
$(function(){
     $("#deliverables_table").deliverable({autoLoad:false, filterByFinancialYear:true, reassignDeliverable:true, section:"admin", page:"reassign"});	
});

</script>
<?php JSdisplayResultObj(""); ?>   
<div id="deliverables_table"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
	<span class="ui-icon ui-icon-info" style="float:left;"></span>
	<span>Please select the user to list their deliverable for re-assignment</span>
</p>
