<?php
$scripts    = array("jquery.compl.deliverableactions.js", "jquery.compl.action.js");
$customMenu = array("1" => array
        (
            "id"  	  => "deliverablemine",
            "url" 	  => "#",
            "active"  => "Y", 
            "display" => "View Mine"
        ),
    "2" => array
        (
            "id" 	  => "deliverableall",
            "url" 	  => "#",
            "active"  => "",
            "display" => "View All"
        )    
);
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>  
<script language="javascript">
$(function(){
   	$("#display_deliverable").deliverableactions({ unlockEvent           :true,
   	                                               page                  :"event_deliverable",
	                                               section     	         : "event",
	                                               view                  : "viewmine",
	                                               autoLoad              : false,
	                                               filterByFinancialYear : true,
	                                               filterByLegislation   : true,
	                                               showActions           : true
	                                            }); 
 
  //$("#display_deliverable").deliverable({unlockEvent:true, page:"event_deliverable", section:"admin", filterByLegislation:true, filterByFinancialYear:true, autoLoad:false,});
})
</script>

<?php JSdisplayResultObj(""); ?>   
<div id="display_deliverable"></div>
