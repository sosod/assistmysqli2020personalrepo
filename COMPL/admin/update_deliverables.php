<?php
$scripts = array("actions.js","jquery.ui.deliverable.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>  
<script language="javascript">
$(function(){
  $("#display_deliverable").deliverable({updateDeliverable:true, page:"update", section:"admin", filterByLegislation:true, filterByFinancialYear:true, autoLoad:false});
})
</script>

<?php JSdisplayResultObj(""); ?>   
<div id="display_deliverable"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
     <span class="ui-icon ui-icon-info" style="float:left;"></span>
     <span>Please select the legislation to list their deliverables</span>
</p>
