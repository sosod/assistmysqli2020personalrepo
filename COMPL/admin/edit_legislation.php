<?php
$scripts = array("legislation.js");
include("../header.php");
$nObj = new LegislationNaming();
$nObj -> getNaming();

$legObj 	 = new ClientLegislation();
$legislation = $legObj -> findById($_GET['id']);

//ASSIST_HELPER::arrPrint($legislation);

$statusObj = new Legislationstatus();
$statuses  =  $statusObj -> getStatuses();


$orgSizes	 = OrganisationSizeManager::getLegislationOrganisationSizes($_GET['id']); 

$orgTypes    = LegislationOrganisationTypeManager::getLegislationOrganisationType($_GET['id']);

$orgCaps 	 = OrganisationCapacityManager::getLegislationOrganisationCapacity($_GET['id']);

$legtype = LegislativetypeManager::getLegislationType($_GET['id']);	

$typeObj = new LegislationType();
$types   = $typeObj -> getAll(); 

$orgsizeObj = new OrganisationSize();
$orgsizes   = $orgsizeObj -> getAll();
$legOrgsizeObj = new OrganisationSizeManager();
$legOrgsize    = $legOrgsizeObj -> getOrganisationSizes($_GET['id']);

$categoryObj = new LegislationCategory();
$categories  = $categoryObj -> getAll(); 
$legcategoryObj = new LegislativeCategoriesManager();
$legCategories	 = $legcategoryObj -> getLegislationCategories($_GET['id']); 

$orgtypeObj = new OrganisationType();
$orgtypes   = $orgtypeObj -> getAll();
$legOrgtypeObj = new LegislationOrganisationTypeManager();
$legOrgtype = $legOrgtypeObj -> getLegOrganisationTypes($_GET['id']);

$orgcapacityObj = new OrganisationCapacity();
$capacities = $orgcapacityObj -> getAll();
$legOrgcapacityObj = new OrganisationCapacityManager();
$legOrgCapacity = $legOrgcapacityObj -> getOrganisationCapacity($_GET['id']);

?>
<?php JSdisplayResultObj(""); ?>
<table width="60%">
	<tr>
    	<th><?php echo $nObj->getHeader("legislation_reference"); ?>:</th>    
    	<td>	
    		<?php echo LEGISLATION::REFTAG.$_GET['id']; ?>
    	</td>
    </tr>
	<tr>
    	<th><?php echo "Source"; ?>:</th>    
    	<td>	
    		<?php if( ( $legislation['legislation_status'] & LEGISLATION::CREATED ) == LEGISLATION::CREATED) { echo "Client created"; } else { echo "Imported Master"; } ?>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $nObj->getHeader("name"); ?>:</th>    
    	<td>
	        <textarea rows="5" cols="30" id="name" name="name"><?php echo $legislation['name']; ?></textarea>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $nObj->getHeader("legislation_reference"); ?>:</th>    
    	<td><input type="text" name="reference" id="reference" value="<?php echo $legislation['reference']; ?>" /></td>
    </tr>
	<tr>
    	<th><?php echo $nObj->getHeader("legislation_number"); ?>:</th>    
    	<td>
          <textarea rows="5" cols="30" name="legislation_number" id="legislation_number"><?php echo $legislation['legislation_number']; ?></textarea>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->getHeader("type"); ?>:</th>    
    	<td>
        <select name="type" id="type">
           <?php 
            foreach($types as $lIndex => $type)
            {
           ?>
             <option <?php echo ($type['id'] == $legislation['type'] ? "selected='selected'" : ""); ?> value="<?php echo $type['id']; ?>">
               <?php echo $type['name']; ?>
             </option>
          <?php 
            }
          ?>
         </select>
      </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->getHeader("category"); ?>:</th>    
    	<td>
        	<select name="category" id="category" multiple="multiple">
            	<?php 
            	 foreach($categories as $cIndex => $category)
            	 {
            	?>
            	 <option <?php echo (isset($legCategories[$cIndex]) ? "selected='selected'" : ""); ?> value="<?php echo $cIndex; ?>" >
            	    <?php echo $category['name']; ?>
            	 </option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr>    
	<tr>
    	<th><?php echo $nObj->getHeader("organisation_size"); ?>:</th>    
    	<td>
        	<select name="organisation_size" id="organisation_size" multiple="multiple">
            	<?php
            	 foreach($orgsizes as $oIndex => $size)
            	 {
            	?>
            	<option <?php echo (isset($legOrgsize[$oIndex]) ? "selected='selected'" : ""); ?> value="<?php echo $oIndex; ?>" >
            	  <?php echo $size['name']; ?>
            	</option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr> 
    <tr>   
    	<th><?php echo $nObj->getHeader("organisation_type"); ?>:</th>    
    	<td>
        	<select name="organisation_type" id="organisation_type" multiple="multiple">
            <?php 
             foreach($orgtypes as $tIndex => $type)
             {
            ?>
              <option <?php echo (isset($legOrgtype[$tIndex]) ? "selected='selected'" : ""); ?> value="<?php echo $tIndex; ?>" >
            	 <?php echo $type['name']; ?>
              </option>
            <?php 
            	 }
            ?>
         </select>        
      </td>
    </tr>   
	<tr>
    	<th><?php echo $nObj->getHeader("organisation_capacity"); ?>:</th>    
    	<td>
        <select name="organisation_capacity" id="organisation_capacity" multiple="multiple">
       	<?php 
       	 foreach($capacities as $capIndex => $capacity)
       	 {
       	?>
       	  <option <?php echo (isset($legOrgCapacity[$capIndex]) ? "selected='selected'" : ""); ?> value="<?php echo $capIndex; ?>" >
       	   <?php echo $capacity['name']; ?>
       	  </option>
       	<?php 
       	 }
       	?>
       </select>        
      </td>
    </tr>  
	<tr>
    	<th><?php echo $nObj->getHeader("legislation_date"); ?>:</th>    
    	<td> 
          <input type="text" name="legislation_date" id="legislation_date" value="<?php echo $legislation['legislation_date']; ?>" class="datepicker" readonly="readonly" />
        </td>
    </tr>                    
	<tr>
    	<th><?php echo $nObj->getHeader("hyperlink_to_act"); ?>:</th>    
    	<td>
          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="<?php echo $legislation['hyperlink_to_act']; ?>" />
        </td>
    </tr> 
	<tr>
    	<th></th>    
    	<td>
          <input type="button" name="edit" id="edit" value=" Save Changes " class="isubmit" />
          <?php if( ( $legislation['legislation_status'] & LEGISLATION::CREATED ) == LEGISLATION::CREATED) { echo "<input type=\"button\" name=\"delete\" id=\"delete\" value=\" Deactivate \" ref=".$_GET['id']." class=\"idelete\" />"; } ?>
          <input type="hidden" name="legislationId" id="legislationId" value="<?php echo $_GET['id']; ?>" />
        </td>
    </tr>  
	<tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>    
    	<td class="noborder"><?php displayAuditLogLink("legislation_edit", false); ?></td>
    </tr>                       
</table>
