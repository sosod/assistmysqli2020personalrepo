<?php
$scripts = array('guidance.js', 'action.js', 'ajaxfileupload.js');
include("../header.php");

$compl = new COMPL_ACTION();
$obj_id = $_REQUEST['id'];


$actionObj 	   = new ClientAction();
$action        = $actionObj -> getLegislationAction($_GET['id']);
//debug($action);
$statusObj     = new ActionStatus();
$statuses      = $statusObj -> getStatusInOrder();
$datecompleted = strtotime($action['date_completed']);
$date_completed = ""; 
if(isset($datecompleted) && !empty($datecompleted))
{
  $date_completed = $action['date_completed'];
} else {
	$date_completed = date("d-M-Y");
}

if(isset($_REQUEST['page_src'])) {
	$go_back_link = "update_actions.php?page_src=".$_REQUEST['page_src'];
}
?>
<?php JSdisplayResultObj(""); ?> 
<form name=frm_update method=post action="../common/attachment.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=section value='<?php echo $compl->getSection(); ?>' id=section />
		<input type=hidden name=action value='UPDATE_ATTACH' id=action />
		<input type=hidden name=after_action value='' id=after_action />
		<input type=hidden name=obj_id value='<?php echo $obj_id; ?>' id=obj_id />
		<input type=hidden name=page_src value='<?php echo isset($_REQUEST['page_src']) ? $_REQUEST['page_src'] : ""; ?>' id=page_src />
<table width="100%" border="2" class="noborder" style="table-layout:fixed;">
	<tr>
		<td class="noborder" with="50%">
              <?php include_once("../common/action.inc.php"); ?>
		</td>
		<td class="noborder" width="50%">
			<table width="100%" class=form>
				<tr>
					<td><h4>Update Action</h4></td>
					 <td style="text-align:right;"><span style="float:left">** Indicates required fields.</span>
					 <?php if(Legislation::isImported($action['legislation_status']))
					 {
					     $controller->printRequestGuidance("");
					 }
            			?>
            		</td>					
				</tr>
				<tr>
					<th>Response:**</th>
					<td>
						<textarea rows="7" cols="50" name="update_response" id="update_response"></textarea>
					</td>
				</tr>
				<tr>
					<th>Status:**</th>
					<td>
						      <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:0px; margin-bottom: 0px; float: right;"><small>Completed actions will be sent to the relevant person(s) <br />for approval and cannot be updated further.</small></p>
						        <select name="status" id="status">
							        <option value="">--status--</option>
							        <?php 
									foreach($statuses as $index => $val) {
								        echo "<option ";
											echo ($val['id'] == 1 ? "disabled='disabled'" : "") ;
											if($val['id'] == $action['status']) { 
												echo "selected=selected";
											}
										echo " value='".$val['id']."'>".$val['name']."</option>";
							        } 
									?>
						        </select>		
						        <input type="hidden" name="_status" id="_status" value="<?php echo $action['status']; ?>" />
					</td>
				</tr>
				<tr>
					<th>Action On:**</th>
					<td>
						<p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both;  margin-top:0px; margin-bottom: 0px;  float:right;"><small>This is the date on which you performed the activity.</small></p>
						<input type="text" name="date_completed" id="date_completed" value="" class="historydate" readonly="readonly"/> 
					</td>
				</tr>
				<tr>
					<th>Progress:**</th>
					<td>
					   <input type="text" name="progress" id="progress" value="<?php echo $action['progress']; ?>" />
					   <input type="hidden" name="_progress" id="_progress" value="<?php echo $action['progress']; ?>" />
					   <em><font color="red">%</font></em> 
					</td>
				</tr>
				<tr>
					<th>Remind On:</th>
					<td>
						<input type="text" name="remindon" id="remindon" value="<?php echo $action['reminder']; ?>" class="datepicker" readonly="readonly"/> 
						<a href="#" id="clear_remind">Clear Date</a>
					</td>
				</tr>
				<tr>
					<th>Attach Document:</th>
					<td><?php 
						$compl->displayAttachmentForm(); 
						echo $compl->displayAttachmentList($obj_id,true);
					?></td>
				</tr>
				<tr>
					<th>Send Approval Notification:</th>
					<td>
					    <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; margin-top:0px; margin-bottom: 0px;  float: right"><small>This will send an email notification to the person(s)<br />responsible for approving this Action.</small></p>
						<input type="checkbox" id="request_approval" name="request_approval"
							<?php if($action['progress'] !== "100") { ?> disabled="disabled" <?php } ?> value="1"  />
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="button" name="update" class=isubmit id="update" value="Update" />
						<input type="reset" name="cancel" id="cancel" value="Reset" />
						<input type="hidden" name="actionId" id="actionId" value="<?php echo $_GET['id']; ?>" />
						<input type="hidden" name="action_id" id="action_id" value="<?php echo $_GET['id']; ?>" class="logid"  />
						<input type="hidden" name="legislationid" id="legislationid" value="<?php echo $action['legislation_id']; ?>" />
					</td>
				</tr>
<!--				<tr>
					<td colspan="2">
					    <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px;">
					      Please note: There are known issue with attaching documents in Internet Explorer 10. We are working on the solution. In the meantime please use another browser e.g. Google Chrome if you wish to attach documents and you are working in Internet Explorer 10.(Tip:to find out which version of Internet Explorer you are using, click on "Help" > "About Internet Explorer" and a window will pop up which indicates which version of IE you are using)
					    </p>                        
					</td>
				</tr>					-->
			</table>	
		</td>
	</tr>
</table>
</form>
