<?php
$scripts = array("jquery.ui.action.js");
include("../header.php");


//Code to recreate previously viewed page
$finYear = 0;
$legId = 0;
$delId = 0;
$pageNum = 1;	//development purposes

if(isset($_REQUEST['page_src']) && strlen($_REQUEST['page_src'])>0) {
	$ps = explode(";",$_REQUEST['page_src']);
	foreach($ps as $p) {
		if(strlen($p)>0) {
			$s = explode("-",$p);
			if($s[1]!="undefined" && $s[1]!="0") {
				switch($s[0]) {
					case "finYear"	:	$finYear = $s[1];	break;
					case "legId"	:	$legId = $s[1]; 	break;
					case "delId"	:	$delId = $s[1];		break;
					case "currentPage"	:	$pageNum = $s[1];	break;
				}
			}
		}
	}
}

$start = ($pageNum*10)-10;
$upper = $start+10;

?>
<?php JSdisplayResultObj(""); ?> 
<script language="javascript">
$(function(){
     $("#display_action").action({updateAction:true, page:"update", section:"admin", filterByFinancialYear:true, filterByLegislation:true, filterByDeliverable:true, autoLoad:false, financialyear:<?php echo $finYear; ?>, legislationId:<?php echo $legId;?>, deliverableId:<?php echo $delId; ?>, currentPage:<?php echo $pageNum; ?>, upperLimit:<?php echo $upper; ?>});	
})
</script>
<div id="display_action"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
	<span class="ui-icon ui-icon-info" style="float:left;"></span>
	<span>Please select the the legislation and deliverable to list their action</span>
</p>
