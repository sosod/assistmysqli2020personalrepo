<?php
$scripts = array("jquery.ui.legislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<script>
				$(function(){
					$("#legislation").legislation({url              : "../class/request.php?action=Legislation.getClientLegislations",
					                              copyFinancialYear : true,
					                              section           : "manage",
					                              viewType          : "all",
					                              page              : "copy"
					                             });
				});
			</script>
			<div id="legislation"></div>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
