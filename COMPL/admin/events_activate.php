<?php
$scripts = array("jquery.ui.deliverableactions.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>  
<script language="javascript">
$(function(){
   	$("#display_deliverable").deliverableactions({
	                                                 eventid  		 : $("#eventid").val(),
	                                                 page     		 : "admin",
	                                                 section     	 : "event",
	                                                 occuranceid 	 : $("#eventOccuranceid").val(),
	                                                 eventActivation  : true,
	                                                 editDeliverable  : true,
                                                      url      		 : "../class/request.php?action=Deliverable.getDeliverables",
	                                                 view             : "viewmine",
	                                                 autoLoad         : false
	                                         });
})
</script>
<div id="display_deliverable"></div>
<p class="ui-state-highlight" style="padding:5px;" id="message">
  <span class="ui-icon ui-icon-info" style="float:left;"></span>
  <span style="margin-left:3px;">Please select the event to list their deliverable and actions for activation</span>
</p>
<?php displayGoBack("", ""); ?>
<input type="hidden" name="show" id="show" value="1" />
<input 
      type="hidden" 
      name="eventid" 
      id="eventid" 
      value="<?php echo (isset($_GET['eventid']) && !empty($_GET['eventid'])) ? $_GET['eventid'] : ""; ?>" />
<input 
      type="hidden" 
      name="eventOccuranceid" 
      id="eventOccuranceid" 
      value="<?php echo (isset($_GET['eventoccuranceid']) && !empty($_GET['eventoccuranceid'])) ? $_GET['eventoccuranceid'] : ""; ?>" />      
