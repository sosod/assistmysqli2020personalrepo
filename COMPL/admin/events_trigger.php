<?php
$scripts = array("jquery.ui.events.js");
$customMenu = array("1" => array
        (
            "id"  	  => "eventmine",
            "url" 	  => "#",
            "active"  => "Y", 
            "display" => "View Mine"
        ),
    "2" => array
        (
            "id" 	  => "eventall",
            "url" 	  => "#",
            "active"  => "",
            "display" => "View All"
        )    
);
include("../header.php");
?>
<script>
$(function(){		   
   $("#events").events({triggerOcccured:true, viewtype:"eventmine", autoLoad:false});
});
</script>
<?php JSdisplayResultObj(""); ?>   
<div id="events"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
	<span class="ui-icon ui-icon-info" style="float:left;"></span>
	<span>Please select the financial year to trigger events</span>
</p>
<?php displayGoBack("", ""); ?>
<input type="hidden" name="show" id="show" value="1" />
