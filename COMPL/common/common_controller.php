<?php
@session_start();
include_once("inc.php");

include_once("../../library/class/assist_dbconn.php");
include_once("../../library/class/assist_db.php");
include_once("../../library/class/assist_helper.php");
include_once("../../library/class/assist.php");

$result = array();

$db = new ASSIST_DB();
$me = new ASSIST();

switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {
case "getLogs":
	$tbl = $_REQUEST['table'];
	$where = $_REQUEST['options'];
	$field = $_REQUEST['field'];
	$sql = "SELECT * FROM ".$db->getDBRef()."_".$tbl." WHERE ".$where;
	$rows = $db->mysql_fetch_all($sql);
	foreach($rows as $r) {
		$raw_changes = unserialize(base64_decode($r[$field]));
		$user = $raw_changes['user'];
		unset($raw_changes['user']);
		$changes = array();
		foreach($raw_changes as $key => $c) {
			$f = ucwords(str_replace("_"," ",$key));
			if(is_array($c) && $c['from']!=$c['to']) {
				$x = $f." changed [b]to[/b] [i]".$c['to']."[/i] [b]from[/b] [i]".$c['from']."[/i]";
			} elseif($key == "message") {
				$x = $c;
			} else {
				$x = "";
			}
			if(strlen($x)>0) {
				$changes[] = ASSIST_HELPER::code($x);
			}
		}
		$result[] = array(
			'date'=>date("d-M-Y H:i:s",strtotime($r['insertdate'])),
			'user'=>$user,
			'changes'=>implode("<br />",$changes),
		);
	}
	break;		
	
case "updateDeliverableAssurance":
	unset($_REQUEST['action']);
	$object = new AssuranceDeliverable();
	$result = $object->updateDeliverableAssurance($_REQUEST);
	break;
case "updateLegislationAssurance":
	unset($_REQUEST['action']);
	$object = new AssuranceLegislation();
	$result = $object->updateLegislationAssurance($_REQUEST);
	break;
case "updateActionAssurance":
	unset($_REQUEST['action']);
	$object = new AssuranceAction();
	$result = $object->updateActionAssurance($_REQUEST);
	break;
}

echo json_encode($result);



?>