<?php
$nObj = new LegislationNaming();
$legislationHeaders = $nObj -> getNaming();

$legislation_id = isset($legislation_id) ? $legislation_id : $_GET['id'];

if(!isset($legislation)) {
	$legislationObj = new ClientLegislation();
	$legislation    = $legislationObj -> findById($legislation_id);
}

$categories	 = LegislativeCategoriesManager::getLegislationCategory($legislation_id);

$orgSizes	 = OrganisationSizeManager::getLegislationOrganisationSizes($legislation_id); 

$orgTypes    = LegislationOrganisationTypeManager::getLegislationOrganisationType($legislation_id);

$orgCaps 	 = OrganisationCapacityManager::getLegislationOrganisationCapacity($legislation_id);

$legtype = LegislativetypeManager::getLegislationType($legislation_id);	

$financialYearObj = new FinancialYear();
$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);
?>
<table width='100%' id=legislation_details_table>
<tr>
	<td colspan="2"><h4>Legislation Details</h4></td>
</tr>
 <tr class=min_display>
     <th><?php echo $nObj->getHeader("financial_year"); ?>:</th>    
     <td><?php  
          if(!empty($financialyear))
          {
            echo $financialyear['start_date']." to ".$financialyear['end_date']." ( ".$financialyear['value']." )";
          }
          ?></td>
 </tr> 
 <tr class=min_display>
     <th><?php echo $nObj->getHeader("id"); ?>: </th>    
     <td><?php echo Legislation::REFTAG.$legislation['id']; ?></td>
 </tr>
 <tr class=min_display>
     <th><?php echo $nObj->getHeader("legislation_name"); ?>: </th>    
     <td><?php echo $legislation['name']; ?></td>
 </tr>
 <tr>
     <th><?php echo $nObj->getHeader("legislation_reference"); ?>:</th>    
     <td><?php echo $legislation['reference']; ?></td>
 </tr>    
 <tr>
     <th><?php echo $nObj->getHeader("legislation_number"); ?>:</th>    
     <td><?php echo $legislation['legislation_number']; ?></td>
 </tr>                    
 <tr>
     <th><?php echo $nObj->getHeader("type"); ?>:</th>    
     <td><?php echo $legtype; ?></td>
 </tr>    
 <tr>
     <th><?php echo $nObj->getHeader("category"); ?>:</th>    
     <td><?php echo $categories; ?></td>
 </tr>    
 <tr>
     <th><?php echo $nObj->getHeader("organisation_size"); ?>:</th>    
     <td><?php echo $orgSizes; ?></td>
 </tr>         
 <tr>
     <th><?php echo $nObj->getHeader("organisation_type"); ?>:</th>    
     <td><?php echo $orgTypes; ?></td>
 </tr>   
 <tr>
     <th><?php echo $nObj->getHeader("organisation_capacity"); ?>:</th>    
     <td><?php echo $orgCaps; ?></td>
 </tr>                
 <tr>
     <th><?php echo $nObj->getHeader("legislation_date"); ?>:</th>    
     <td><?php echo $legislation['legislation_date']; ?></td>
 </tr>                            
 <tr>
     <th><?php echo $nObj->getHeader("hyperlink_to_act"); ?>:</th>    
     <td><?php echo $legislation['hyperlink_to_act']; ?></td>
 </tr> 
 <tr>
     <td class="noborder"><?php displayGoBack("", ""); ?></td>    
     <td class="noborder"><?php displayAuditLogLink("legislation_update", false) ?></td>
 </tr>                 
</table>
