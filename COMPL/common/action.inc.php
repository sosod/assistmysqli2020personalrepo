<?php
$nObj = new ActionNaming();
$headers = $nObj-> getNaming();
?>
<table width="100%" id=action_details_table>
	<tr>
		<td><h4>Action Details </h4></td>
		<td align="right">
		  <input type="button" name="show_deliverable" id="show_deliverable" value="Deliverable" />
		  &nbsp;&nbsp;&nbsp;
		  <input type="button" name="legislation" id="legislation" value="Legislation" />
		</td>
	</tr>
	<tr>
		<th><?php echo $nObj -> getHeader("id"); ?>:</th>
		<td><?php echo Action::REFTAG.$_GET['id']; ?></td>
	</tr>
	<tr>
		<th><?php echo $nObj->getHeader("deadline"); ?>:</th>
		<td><?php echo $action['deadline']; ?></td>
	</tr>								
	<tr>
		<th><?php echo $nObj->getHeader("action"); ?>:</th>
		<td><?php echo $action['action']; ?></td>
	</tr>
	<tr>
		<th><?php echo $nObj->getHeader("action_deliverable"); ?>:</th>
		<td><?php echo $action['action_deliverable']; ?></td>
	</tr>		
	<tr>
		<th><?php echo $nObj->getHeader("progress"); ?>:</th>
		<td>
              <span id="actionProgress"><?php echo $action['progress']; ?></span>%
           </td>
	</tr>	
	<tr>
		<th><?php echo $nObj->getHeader("owner"); ?>:</th>
		<td><?php echo $action['user']; ?></td>
	</tr>					
	<tr>
		<th><?php echo $nObj->getHeader("status"); ?>:</th>
		<td><?php echo $action['status_name']; ?></td>
	</tr>	
	<tr>
		<td class="noborder"><?php 
			displayGoBack(isset($go_back_link) ? $go_back_link : ""); 
		?></td>
		<td class="noborder"><?php displayAuditLogLink("action_update", false) ?></td>
	</tr>										
</table>
