<?php

$nObj = new ActionNaming();
$setupObj = new SetupManager();
$usersObj = new UserAccess();
$owners   = $usersObj->getActiveUsers();

$dOb = new Action();
$action = $dOb -> fetch( $_GET['id'] );

$dOb = new CLientDeliverable();
$deliverable = $dOb -> fetch( $action['deliverable_id'] );


$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> fetch( $deliverable['legislation'] );

?>
<?php JSdisplayResultObj(""); ?>   
<table>
<tr>
	<td class='noborder'><h4>Edit Action</h4></td>
	 <td style="text-align:right;">
	 <?php if(Legislation::isImported($legislation['legislation_status']))
	 {
	     $controller->printRequestGuidance("");
	 }
	?>
    </td>	
</tr>
<tr>
   <th><?php echo $nObj->getHeader("action"); ?>:</th>    
   <td>
   	<textarea rows="8" cols="30" id="action_name" name="action_name"><?php echo $action['action_required']; ?></textarea>
   </td>
</tr> 
 <tr>
     <th><?php echo $nObj->getHeader("owner"); ?>:</th>    
     <td>
         <select name="action_owner" id="action_owner">
             <option value="">--action owner--</option>
             <?php foreach( $owners as $index => $val) { ?>
             <option
             <?php if( $val['tkid'] == $action['owner']) { ?>
             selected="selected"
             <?php } ?>
              value="<?php echo $val['tkid']; ?>"><?php echo $val['user']; ?></option>	
             <?php } ?>
         </select>        
     </td>
 </tr>    
 <tr>
     <th><?php echo $nObj->getHeader("action_deliverable"); ?>:</th>    
     <td>
     <textarea rows="5" cols="30" id="deliverable" name="deliverable"><?php echo $action['action_deliverable']; ?></textarea>
     </td>
 </tr>   
 <tr>
     <th><?php echo $nObj->getHeader("deadline"); ?>:</th>    
     <td><input type="text" name="deadline" id="deadline" value="<?php echo $action['action_deadline_date']; ?>" class="datepicker"/></td>
 </tr>    
 <tr>
     <th><?php echo $nObj->getHeader("reminder"); ?>: </th>    
     <td><input type="text" name="reminder" id="reminder" value="<?php echo $action['action_reminder']; ?>" class="datepicker"/></td>
 </tr>     
<tr>
     <th>Recurring</th>    
     <td>
       <input type="checkbox" name="recurring" id="recurring" value="1" />
     </td>
 </tr>                                              
 <tr>
     <td></td>    
     <td>
       <input type="button" name="edit" id="edit" value=" Edit " class='isubmit' />
       <input type="hidden" name="actionId" id="actionId" value="<?php echo $_GET['id']; ?>">
       <input type="hidden" class="logid" name="logid" id="action_id" value="<?php echo $_GET['id']; ?>">
       <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $deliverable['legislation']; ?>" />
     </td>
 </tr>    
 <tr>
     <td class="noborder"><?php displayGoBack("", ""); ?></td>    
     <td class="noborder"><?php displayAuditLogLink("action_edit", false); ?></td>
 </tr>                            
</table>  
