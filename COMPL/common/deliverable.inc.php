<?php
$nObj = new DeliverableNaming();
$headers = $nObj-> getNaming();
?>
<table>
  	<tr>
  		<td colspan="2"><h4>Deliverable Details</h4></td>
  	</tr>
      <tr>
        <th width="20%"><?php $nObj -> getHeader("id"); ?> #:</th>
        <td><?php echo DELIVERABLE::REFTAG.$deliverable['ref']; ?></td>
      </tr>
     <tr>
          <th><?php echo $nObj -> getHeader("short_description_of_legislation"); ?>: </th>
          <td><?php echo $deliverable['short_description']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj -> getHeader("description"); ?>:</th>
          <td><?php echo $deliverable['description']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj -> getHeader("legislation_section"); ?>: </th>
          <td><?php echo $deliverable['legislation_section']; ?></td>
      </tr>				
     <!--  <tr>
          <th><?php echo $nObj->getHeader("frequency_of_compliance"); ?>:</th>
          <td><?php echo $deliverable['compliance_frequency']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("applicable_organisation_type"); ?>:</th>
          <td><?php echo $deliverable['applicable_org_type']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("legislation_compliance_date"); ?>:</th>
          <td><?php echo $deliverable['compliance_date']; ?></td>
      </tr> 
      <tr>
          <th><?php echo $nObj->getHeader("responsible_department"); ?>:</th>
          <td><?php echo $deliverable['responsible']; ?></td>
      </tr>                
      <tr>
          <th><?php echo $nObj->getHeader("functional_service"); ?>:</th>
          <td><?php echo $deliverable['functional_service']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("accountable_person"); ?>:</th>
          <td><?php echo $deliverable['accountable_person']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("responsibility_ower"); ?>:</th>
          <td><?php echo $deliverable['responsiblity_owner']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("legislative_deadline_date"); ?>:</th>
          <td><?php echo $deliverable['legistation_deadline']; ?></td>
      </tr>
      <!-- <tr>
          <th>Organisation KPI Ref</th>
          <td><?php echo $deliverable['org_kpi_ref']; ?>:</td>
      </tr> --> 
      <tr>
          <th><?php echo $nObj -> getHeader("sanction"); ?>:</th>    
          <td>
            <?php echo $deliverable['sanction']; ?>
          </td>
      </tr>     
      <tr>
          <th><?php echo $nObj -> getHeader("reference_link"); ?>:</th>    
          <td><?php echo $deliverable['reference_link']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj -> getHeader("assurance"); ?>:</th>    
          <td><?php echo $deliverable['assurance']; ?></td>
      </tr>                                             
     <!--  <tr>
          <th><?php echo $nObj->getHeader("compliance_name_given_to"); ?>:</th>
          <td><?php echo $deliverable['compliance_to']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("main_event"); ?>:</th>
          <td><?php echo $deliverable['main_event']; ?></td>
      </tr>
      <tr>
          <th><?php echo $nObj->getHeader("sub_event"); ?>:</th>
          <td><?php echo $deliverable['sub_event']; ?></td>
      </tr> -->
      <tr>
          <th><?php echo $nObj->getHeader("guidance"); ?>:</th>
          <td><?php echo $deliverable['guidance']; ?></td>
      </tr> 
		<tr>
			<td class="noborder"><?php displayGoBack("", ""); ?></td>
			<td class="noborder"><?php displayAuditLogLink("deliverable_update", false) ?></td>
		</tr>
  </table>
