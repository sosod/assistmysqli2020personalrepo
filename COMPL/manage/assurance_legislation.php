<?php //error_reporting(-1);
$scripts = array("legislation.js", 'ajaxfileupload.js', "jquery.ui.legislationassurance.js");
include("../header.php");
$leg_id = $_REQUEST['id'];
$nObj = new LegislationNaming();
$legObj 	 = new ClientLegislation();
$legislation = $legObj -> findById($_GET['id']);
$categories	 = LegislativeCategoriesManager::getLegislationCategory($_GET['id']);

$orgSizes	 = OrganisationSizeManager::getLegislationOrganisationSizes($_GET['id']); 

$orgTypes    = LegislationOrganisationTypeManager::getLegislationOrganisationType($_GET['id']);

$orgCaps 	 = OrganisationCapacityManager::getLegislationOrganisationCapacity($_GET['id']);

$legtype = LegislativetypeManager::getLegislationType($_GET['id']);		
?>
<?php JSdisplayResultObj(""); ?>   
<input type="hidden" name="legislationId" id="legislationId" value="<?php echo $leg_id; ?>" />	
<table class="noborder" width="100%" style="table-layout:fixed;">
	<tr>
		<td class="noborder" width="50%">
		<h2>Legislation Details</h2>
             <?php include_once("../common/legislation.inc.php"); ?>
			<h2>Activity Log</h2>
			<table id=action_activity_log width=100% class=list>
			<tr>
				<th>Date/Time</th><th>User</th><th>Activity</th><th>Status</th>
			</tr>
			<?php 
				echo $legObj->getActionViewLog($leg_id); 
				/*$compl = new COMPL_ACTION();
				$attach_display = $compl->displayAttachmentList($action_id);
				if(strlen($attach_display)>0) {
					echo "<tr><td colspan=4>".$attach_display."</td></tr>";
				}*/
			?>
			</table>			
		</td>
		<td class="noborder">
			<script type="text/javascript">
				$(function(){
					$("#legislation_assurance").legislationassurance({id:$("#legislationId").val()});
				});
			</script>
			<table id="legislation_assurance" width="100%">
			</table>
			<div id=assurance_logs style='padding: 3px;'>
				<div class='right' style='margin: 10px'><span class='show' id=display_audit_log style='cursor: pointer; margin-right: 10px; text-decoration: underline;'><img src="/pics/tri_down.gif" style='vertical-align: middle; padding-right: 3px' /><span id=word>Display</span> Activity Log</span></div>
				<table id=audit_logs class=list>
					<tr>
						<th>Date</th>
						<th>User</th>
						<th>Activity</th>
					</tr>
				</table>
				<script type=text/javascript>
					$(function() {
						var tbl = "legislation_assurance_log";
						var where = "legislation_id = "+$("#legislationId").val();
						var fld = "changes";
						$("#audit_logs").hide();
						$("#display_audit_log").click(function() {
							if($(this).hasClass("show")) {
								var result = AssistHelper.doAjax("../common/common_controller.php",{action:"getLogs",table:tbl,options:where,field:fld});
								$.each(result, function(index, r) {
									//alert(r['changes']);
									//alert(AssistString.convert_to_html(r['changes']));
									var chg = AssistString.str_replace("\n"," ",r['changes']);
									var chg = AssistString.convert_to_html(chg);
									$("#audit_logs")
										.append($("<tr />",{class:"log"})
											.append($("<td />",{html:r['date'], class:"center"}))
											.append($("<td />",{html:r['user'], class:"center"}))
											.append($("<td />",{html:chg}))
										);
								});
								if($("#audit_logs tr.log").size()==0) {
									$("#audit_logs")
										.append($("<tr />",{class:"log"})
											.append($("<td />",{html:"No logs available.", colspan:"3"}))
										);
								}
								$("#audit_logs").show();
								$(this).removeClass("show");
								$("#display_audit_log img").prop("src","/pics/tri_up.gif");
								$("#word").html("Hide");
							} else {
								$(this).addClass("show");
								$("#word").html("Display");
								$("#display_audit_log img").prop("src","/pics/tri_down.gif");
								$("#audit_logs tr.log").remove();
								$("#audit_logs").hide();
							}
							
						});
					});
				</script>
			</div>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
});
</script>