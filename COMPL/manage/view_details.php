<?php
$scripts = array("view.js","jquery.ui.deliverableactions.js");
include("../header.php");

?>
<?php JSdisplayResultObj(""); ?>
<script type="text/javascript">
$(function(){
  $("#deliverable_actions").deliverableactions({legislationId:$("#legislationid").val(), details:false, viewAction: true, page:"manage", editAction:false, view:$("#viewtype").val(), section:"manage"});
});
</script>
<div id="deliverable_actions"></div>
<input type="hidden" value="<?php echo $_GET['id']; ?>" id="legislationid" name="legislationid" />
<input type="hidden" value="<?php echo $_GET['viewtype']; ?>" id="viewtype" name="viewtype" />
<?php displayGoBack("", ""); ?>
