<?php
$scripts = array("deliverable.js", "jquery.ui.deliverableassurance.js");
include("../header.php");
$obj_id = $_REQUEST['id'];
$statusObj   = new DeliverableStatus();
$statuses    = $statusObj -> getDeliverableStatuses();

$delObj = new ClientDeliverable();
$deliverable = $delObj->fetch($obj_id);

$legislation_id = $deliverable['legislation'];	

?>
<?php JSdisplayResultObj(""); ?>   
		<input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $obj_id; ?>" />	
<table width="100%" class="noborder" style="table-layout:fixed;">
<tr>
	<td class="noborder">
			<h2>Deliverable Details
			<span class=float style=' padding: 0px; margin-top: -5px;'>
				<input type=button value="Legislation" id=btn_leg />
			</span>
			</h2>
	
      <?php
       $displayDeliverableObj = new DeliverableDisplay($obj_id);
       $displayDeliverableObj -> display(new DeliverableNaming());
      ?>
			<h2>Activity Log</h2>
			<table id=action_activity_log width=100% class=list>
			<tr>
				<th>Date/Time</th><th>User</th><th>Activity</th><th>Status</th>
			</tr>
			<?php 
				echo $delObj->getActionViewLog($obj_id); 
			?>
			</table>			
	</td>
	<td class="noborder">
		<script type="text/javascript">
			$(function(){
				$("#deliverable_assurance").deliverableassurance({id:$("#deliverableid").val()})						
			});
		</script>
		<table width="100%" id="deliverable_assurance">
		</table>
			<div id=assurance_logs style='padding: 3px;'>
				<div class='right' style='margin: 10px'><span class='show' id=display_audit_log style='cursor: pointer; margin-right: 10px; text-decoration: underline;'><img src="/pics/tri_down.gif" style='vertical-align: middle; padding-right: 3px' /><span id=word>Display</span> Activity Log</span></div>
				<table id=audit_logs class=list>
					<tr>
						<th>Date</th>
						<th>User</th>
						<th>Activity</th>
					</tr>
				</table>
				<script type=text/javascript>
					$(function() {
						var tbl = "deliverable_assurance_log";
						var where = "deliverable_id = "+$("#deliverableid").val();
						var fld = "changes";
						$("#audit_logs").hide();
						$("#display_audit_log").click(function() {
							if($(this).hasClass("show")) {
								var result = AssistHelper.doAjax("../common/common_controller.php",{action:"getLogs",table:tbl,options:where,field:fld});
								$.each(result, function(index, r) {
									//alert(r['changes']);
									//alert(AssistString.convert_to_html(r['changes']));
									var chg = AssistString.str_replace("\n"," ",r['changes']);
									var chg = AssistString.convert_to_html(chg);
									$("#audit_logs")
										.append($("<tr />",{class:"log"})
											.append($("<td />",{html:r['date'], class:"center"}))
											.append($("<td />",{html:r['user'], class:"center"}))
											.append($("<td />",{html:chg}))
										);
								});
								if($("#audit_logs tr.log").size()==0) {
									$("#audit_logs")
										.append($("<tr />",{class:"log"})
											.append($("<td />",{html:"No logs available.", colspan:"3"}))
										);
								}
								$("#audit_logs").show();
								$(this).removeClass("show");
								$("#display_audit_log img").prop("src","/pics/tri_up.gif");
								$("#word").html("Hide");
							} else {
								$(this).addClass("show");
								$("#word").html("Display");
								$("#display_audit_log img").prop("src","/pics/tri_down.gif");
								$("#audit_logs tr.log").remove();
								$("#audit_logs").hide();
							}
							
						});
					});
				</script>
			</div>
	</td>
</tr>
<tr>
     <td colspan="2" class="noborder"><?php displayGoBack("", ""); ?></td>    
</tr>  
</table>

<div id=div_leg title="Legislation Details">
            <h2>Legislation Details</h2>
            <?php 
				include_once("../common/legislation.inc.php"); 
			?>
</div>
<script type=text/javascript>
$(function() {
	$("#deliverable_details_table tr:first").hide();
	$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
	$("#btn_del, #btn_leg").button().addClass("small-button");
var screen = AssistHelper.getWindowSize();
	$("#div_del, #div_leg").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		height: (screen['height']-25)
	});
	$("#btn_del").click(function() {
		$("#div_del").dialog("open");
	});
	$("#btn_leg").click(function() {
		$("#div_leg").dialog("open");
	});
});
</script>