<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
$legiObj   = new LegislationManager();
$updated   = $legiObj -> getUpdatedLegislations();
/*print "<pre>";
	print_r($updated);
print "</pre>";
*/
JSdisplayResultObj("");
?>   
<?php 
	if( isset($updated['data']) && !empty($updated)){
?>
<table width="60%">
  <tr>
	<?php 
		foreach( $updated['data']['headers'] as $index  => $header){
	?>
		<th><?php echo $header; ?></th>
	<?php 
		}
	?>
  <th>&nbsp;</th>		
  </tr>   
	<?php
		if(isset($updated['data']['data']) && !empty($updated['data']['data'])){ 
		foreach( $updated['data']['data']  as $i => $legislation){
	?>
		<tr>
			<?php 
				foreach( $legislation as $k => $val){
			?>
				<td><?php echo $val; ?></td>
			<?php 		
				}
			?>
			<td>
				<input type="button" name="allowupdate" id="update<?php echo $legislation['ref']; ?>_id<?php echo $legislation['ref']; ?>" class="allow_update" value=" Allow Update" />
			</td>
		</tr>	    
	<?php 
		}
	}
	?>
   <tr style="display:none;" id="update_legislation">
 	<td colspan="3">
 		<table></table>
 	</td> 	
  </tr>      
</table>
<?php 
	} else {
		echo "There are no legislation updates";		
	}
?>
<script type="text/javascript">
	$(function(){
		$(".allow_update").live("click", function(){

			var id 	 = this.id;
			var data = {};
			data.ref = id;
			$("<div />",{id:"allowupdate_"+id})
			 .dialog({
						autoOpen	: true,
						modal		: true,
						title		: "Allow Legislation update",
						buttons		: {
										"I Agree "	: function()
										{
											jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
											$.post("../class/request.php?action=LegislationManager.allowUpdate",{ data : data }, function( response ){
												if( response.error ){
													jsDisplayResult("error", "error", response.text);
												} else {
													jsDisplayResult("ok", "ok", response.text );
												}	
											},"json")
											$("#allowupdate_"+id).dialog("destroy");
											$("#allowupdate_"+id).remove();
										} ,
										"Decline "  : function()
										{
											$("#allowupdate_"+id).dialog("destroy");
											$("#allowupdate_"+id).remove();
										} 
									  }
			 })
		})
	})
</script>
<?php displayGoBack("", ""); ?>
