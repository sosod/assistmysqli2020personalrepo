<?php
$scripts = array("action.js", "jquery.ui.action.js");
include("../header.php");
//$nObj = new DeliverableNaming();
//$dOb = new ClientDeliverable();
//$deliverable = $dOb -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>    
<script language="javascript">
$(function(){
	$("#display_action").action({assuranceAction:true, deliverableId:$("#deliverableId").val(), page:"manage", section:"manage"});
})
</script>
<table class="noborder" style="table-layout:fixed;">
  <tr>
    <td class="noborder" width="50%">
          <?php
          $displayDeliverableObj = new DeliverableDisplay($_GET['id']);
          $displayDeliverableObj -> display(new DeliverableNaming());
          ?>
    </td>
    <td class="noborder" width="50%">
       <div id="display_action"></div>
    </td>
  </tr>
 <tr>
     <td colspan="2" class="noborder"><?php displayGoBack("", ""); ?></td>    
</tr>  
</table>
<input type="hidden" name="deliverableId" id="deliverableId" value="<?php echo $_GET['id']; ?>" />
