<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?> 
<table width="60%"> 
  <tr>
    <td><p class="ui-widget ui-state-info" style="margin:5px 0px 10px 0px; padding:0.6em;"><span class="ui-icon ui-icon-info" style="float:left; margin-right:0.3em;"></span><span style="padding-top:30px;">The following are changes made to legislations in master setup</span></p></td>
  </tr>
  <tr>
    <td>
     <table width="100%">
       <tr>
         <th>Ref</th>
         <th>Legisaltion Change</th>
         <th>Date</th>
         <th>Edited By</th>
         <th></th>
       </tr>
       <tr>
         <td>Ref</td>
         <td>Legislation  Change</td>
         <td>Date</td>
         <td>Edited By</td>
         <td><input type="button" name="update" id="update" value="Update" /></td>
       </tr>       
     </table>
    </td>
  </tr>

  <tr>
    <td><input type="button" name="update_all" id="update_all" value="Update All Changes" /></td>
  </tr>

</table>
<script>
	$(function(){

		$("#view_update_action").click(function(){
			document.location.href = "approval_action.php";
			return false;
		});

		$("#view_update_deliverable").click(function(){
			document.location.href = "approval_deliverables.php";
			return false;
		});		

		$("#view_update_legislation").click(function(){
			document.location.href = "approval_legislations.php";
			return false;
		});		
		//$("#legislation").legislation({approvalUpdate:true, legoption : "active", url : "../class/request.php?action=legislationManager.getUpdatedLegislations",page:"manage"});
	});
</script>
<?php displayGoBack("", ""); ?>
