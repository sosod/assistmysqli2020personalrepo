<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
$delObj   = new DeliverableManager();
$updated  = $delObj -> getUpdatedDeliverables();
?>
<?php JSdisplayResultObj(""); ?>
<table width="60%">
  <tr>
	<?php
		foreach( $updated['deliverables']['headers'] as $index => $header){ 
	?>
		<th><?php echo $header; ?></th>	
	<?php 
		}
	?>
	<th>&nbsp;</th>
  </tr>
  <?php 
  	if(isset($updated['deliverables']['data']) && !empty($updated['deliverables']['data'])){
  		foreach( $updated['deliverables']['data'] as $i => $deliverable){
  ?>
  	<tr>
  		<?php foreach ( $deliverable as $key => $val){ ?>
  			<td><?php echo $val; ?></td>
  		<?php } ?>
  		<td><input type="button" name="updatedeliverable" class="update_deliverable" id="<?php echo "update".$deliverable['ref']."_id".$updated['deliverableReferences'][$deliverable['ref']]; ?>" value="Allow Update" /></td>
  	</tr>
  <?php 
  		}
  	}
  ?> 	       
</table>
<script>
	$(function(){

		$(".update_deliverable").live("click", function(){
			var data = {};
			var id 	 = this.id;
			data.ref = id;
			
			$("<div />",{id:"updatedeliverable_"+id, html:"Allow Deliverable Update"})
			 .dialog({
						autoOpen	: true,
						modal		: true,
						title 		: "Allow Deliverable Update",
						buttons		: {
										"I Agree"	: function()
										{
											jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
											$.post("../class/request.php?action=DeliverableManager.allowUpdate",{ data : data }, function( response ){
												if( response.error ){
													jsDisplayResult("error", "error", response.text);
												} else {
													jsDisplayResult("ok", "ok", response.text );
												}	
											},"json")
											$("#updatedeliverable_"+id).dialog("destroy");
											$("#updatedeliverable_"+id).remove();											
										} , 
										"Decline"	 : function()
										{
											$("#updatedeliverable_"+id).dialog("destroy");
											$("#updatedeliverable_"+id).remove();
										}  
									  }
			  })	
			
		});
	});
</script>   
<?php displayGoBack("", ""); ?>