<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
$actionObj = new ActionManager();
$updated   = $actionObj -> getUpdatedActions();
?>
<?php JSdisplayResultObj(""); ?> 
<table>
  <tr>
	<?php
		foreach($updated['actions']['headers'] as $index => $header){
	?>
		<th><?php echo $header; ?></th>
	<?php 
		} 
	?>
	<th>&nbsp;</th>
  </tr>
	<?php
		if(isset($updated['actions']['data']) && !empty($updated['actions']['data'])){
			foreach($updated['actions']['data'] as $index => $actions){
		?>	
			<tr>
				<?php 
					foreach($actions as $key => $valArr){
							?>
								<td><?php echo $valArr; ?></td>
							<?php
						}
					?>
				<td><input type="button" name="update" class="updated" id="update<?php echo $updated['actionsRef'][$actions['ref']]."_id".$actions['ref']; ?>" value="Allow Update Action" /></td>
			</tr>
		<?php 
	     }
		?>
	  <tr>
	 	<td colspan=<?php echo $updated['actions']['columns']+1; ?> style="text-align:right;"><input type="button" name="update_all" id="update_all" value="Update All" /></td> 
	  </tr>
	 <?php 
	} else {
	 ?>
	 <tr>
	 	<td colspan="<?php echo count($updated['actions']['headers'])+1; ?>">
	 		There are no actions to update
	 	</td>
	 </tr>
	<?php 
	}
	?>
</table>
<script>
	$(function(){
		$(".updated").live("click",function(){
			var data  = {}
			var id 	  = this.id
			data.ref   =  id;
			$("<div />",{html:"Allow action update", id:"allowupdate_"+id})
			 .dialog({
 						autoOpen	: true, 
 						modal		: true,
 						title		: "Allow Action Update",
 						buttons		: {
										"I Agree"	: function()
										{
											jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
											$.post("../class/request.php?action=ActionManager.allowUpdate", { data : data}, function( response ){
												if( response.error ){
													jsDisplayResult("error", "error", response.text);
												} else {
													jsDisplayResult("ok", "ok", response.text );
												}												
											},"json");
											$("#allowupdate_"+id).dialog("destroy");
											$("#allowupdate_"+id).remove();
										} , 
										"Decline"	: function()
										{
											$("#allowupdate_"+id).dialog("destroy");
											$("#allowupdate_"+id).remove();											
										}
								}
			 })
		})		
	});
</script>  
<?php displayGoBack("", ""); ?>
