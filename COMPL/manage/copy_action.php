<?php
$scripts = array("action.js");
include("../header.php");
$nObj = new ActionNaming();
$headers  = $nObj -> getNaming();
//$setupObj = new SetupManager();
$usersObj = new UserAccess();
$owners   = $usersObj->getActiveUsers();

$actionObj = new ClientAction();
$action = $actionObj -> getLegislationAction($_GET['id']); 


?>
<?php JSdisplayResultObj(""); ?>   
<table>
<tr>
	<td class='noborder'><h4>Copy Action</h4></td>
	 <td style="text-align:right;">
	 <?php 	 	
        if((Legislation::IMPORTED & $action['legislation_status']) == Legislation::IMPORTED)
        {
			$controller->printRequestGuidance("");  	
		} 
    	?>
    </td>	
</tr>
<tr>
   <th><?php echo $nObj->getHeader("action"); ?>:</th>    
   <td>
   	<textarea rows="8" cols="80" id="action_name" name="action_name"><?php echo $action['action']; ?></textarea>
   </td>
</tr> 
<tr>
<th><?php echo $nObj->getHeader("owner"); ?>:</th>    
<td>
    <select name="action_owner" id="action_owner">
        <option value="">--action owner--</option>
        <?php foreach( $owners as $index => $val) { ?>
        <option value="<?php echo $val['tkid']; ?>"><?php echo $val['user']; ?></option>	
        <?php } ?>
    </select>        
</td>
</tr>    
<tr>
<th><?php echo $nObj->getHeader("action_deliverable"); ?>:</th>    
<td>
<textarea rows="5" cols="80" id="deliverable" name="deliverable"><?php echo $action['action_deliverable']; ?></textarea>
</td>
</tr>   
<tr>
<th><?php echo $nObj->getHeader("deadline"); ?>:</th>    
<td><input type="text" name="deadline" id="deadline" value="" class="datepicker" readonly="readonly" /></td>
</tr>    
<tr>
<th><?php echo $nObj->getHeader("reminder"); ?>: </th>    
<td><input type="text" name="reminder" id="reminder" value="" class="datepicker" readonly="readonly" /></td>
</tr>     
<tr>
<th>Recurring</th>    
<td>
  <input type="checkbox" name="recurring" id="recurring" value="1" />
</td>
</tr>                                              
<tr>
<td></td>    
<td>
  <input type="button" name="copysave" id="copysave" value=" Save " class='isubmit' />
  <input type="hidden" name="actionId" id="actionId" value="<?php echo $_GET['id']; ?>">
  <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $action['legislation']; ?>" />
  <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $action['deliverable_id']; ?>" /> 
  <input type="hidden" name="actiontype" id="actiontype" value="manage" />
</td>
</tr>    
<tr>
<td class="noborder"><?php displayGoBack("", ""); ?></td>    
<td class="noborder"><?php displayAuditLogLink("action_edit", false); ?></td>
</tr>                            
</table>  
