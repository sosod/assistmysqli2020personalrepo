<?php
$scripts = array("jquery.ui.actionassurance.js");
include("../header.php");
$actionObj = new ClientAction();
$action = $actionObj -> getLegislationAction($_GET['id']); 

$statusObj  = new ActionStatus();
$statuses   = $statusObj -> getActionStatuses();
?>
<?php JSdisplayResultObj(""); ?> 
<table width="100%" class="noborder">
	<tr>
		<td class="noborder" width="40%">
             <?php include_once("../common/action.inc.php"); ?>
		</td>
		<td class="noborder" width="60%">
			<script type="text/javascript">
				$(function(){
					$("#assurance_action").actionassurance({id:$("#actionid").val()})
				});
			</script>
			<table width="100%" id="assurance_action">
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="actionid" value="<?php echo $_GET['id']; ?>" id="actionid" />
