<?php
$scripts = array("guidance.js","action.js");
include("../header.php");
$nObj = new ActionNaming();
$headers = $nObj-> getNaming();

$usersObj = new User();
$owners   = $usersObj->getAll();

$actionObj = new ClientAction();
$action = $actionObj -> getLegislationAction($_GET['id']); 
?>
<?php JSdisplayResultObj(""); ?>   
<table style="table-layout:fixed;">
<tr>
	<td class='noborder'><h4>Edit Action</h4></td>
	 <td style="text-align:right;">
	 <?php 	 	
        if((Legislation::IMPORTED & $action['legislation_status']) == Legislation::IMPORTED)
        {
		$controller->printRequestGuidance("");  	
	   } 
    	?>
    </td>	
</tr>
<tr>
   <th><?php echo $nObj->getHeader("action"); ?>:</th>    
   <td>
   	<textarea rows="8" cols="80" id="action_name" name="action_name"><?php echo $action['action']; ?></textarea>
   </td>
</tr> 
 <tr>
     <th><?php echo $nObj -> getHeader("owner"); ?>:</th>    
     <td>
         <?php
           if(Action::isUsingUserId($action['actionstatus']))
           {
         ?>
         <select name="action_owner" id="action_owner">
             <option value="">--action owner--</option>
             <?php foreach($owners as $index => $val) { ?>
             <option <?php echo ($index == $action['owner'] ? "selected='selected'" : ""); ?>
              value="<?php echo $index; ?>"><?php echo $val['user']; ?></option>	
             <?php } ?>
         </select>        
         <?php 
         } else {
         ?>
         <select name="action_owner" id="action_owner">
             <option value="">--action owner--</option>
             <?php foreach($owners as $index => $val) { ?>
             <option value="<?php echo $index; ?>"><?php echo $val['user']; ?></option>	
             <?php } ?>
         </select>                        
         <?php 
           $ownerObj = new ActionOwnerManager();
           echo "Linked to <a href=''>".$ownerObj -> getActionOwnerStr($action['owner'], $action['actionstatus'])."</a>";
         } ?>
     </td>
 </tr>    
 <tr>
     <th><?php echo $nObj -> getHeader("action_deliverable"); ?>:</th>    
     <td>
     <textarea rows="8" cols="80" id="deliverable" name="deliverable"><?php echo $action['action_deliverable']; ?></textarea>
     </td>
 </tr>   
 <tr>
     <th><?php echo $nObj -> getHeader("deadline"); ?>:</th>    
     <td><input type="text" name="deadline" id="deadline" value="<?php echo $action['deadline']; ?>" class="datepicker" readonly="readonly" /></td>
 </tr>    
 <tr>
     <th><?php echo $nObj -> getHeader("reminder"); ?>: </th>    
     <td><input type="text" name="reminder" id="reminder" value="<?php echo $action['reminder']; ?>" class="futuredate" readonly="readonly" /><a href="#" id="clear_remind">Clear Date</a></td>
 </tr>     
<tr>
     <th>Recurring</th>    
     <td>
       <input type="checkbox" name="recurring" id="recurring" value="1" />
     </td>
 </tr>                                              
 <tr>
     <td></td>    
     <td>
       <input type="button" name="edit" id="edit" value=" Edit " class='isubmit' />
       <input type="hidden" name="actionId" id="actionId" value="<?php echo $_GET['id']; ?>">
       <input type="hidden" class="logid" name="logid" id="action_id" value="<?php echo $_GET['id']; ?>">
       <input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $action['deliverable_id']; ?>" />
     </td>
 </tr>    
 <tr>
     <td class="noborder"><?php displayGoBack("", ""); ?></td>    
     <td class="noborder"><?php displayAuditLogLink("action_edit", false); ?></td>
 </tr>                            
</table>  
