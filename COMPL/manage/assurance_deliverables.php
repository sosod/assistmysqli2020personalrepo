<?php
$scripts = array("actions.js","jquery.ui.deliverable.js");
include("../header.php");
$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById( $_GET['id'] );	
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" style="table-layout:fixed;">
 <tr>
    <td class="noborder" width="35%">
        <?php include_once("../common/legislation.inc.php"); ?>
    </td>
    <td class="noborder">
		<script language="javascript">
		$(function(){
			$("#display_deliverable").deliverable({assuranceAction:true, assuranceDeliverable:true, legislationId:$("#legislationid").val(), reference:$("#legislationid").val(), page:"manage", section:"manage"});
		})
		</script>
		<div id="display_deliverable"></div>
	</td>
  </tr>
</table>  
<!-- 
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<script language="javascript">
			$(function(){
				$("#display_deliverable").deliverable({assuranceAction:true, assuranceDeliverable:true, legislationId:$("#legislationid").val(), reference : $("#legislationid").val() , headers:["reference_link", "guidance", "sanction"], page:"manage"});
			})
			</script>
			<div id="display_deliverable"></div>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table> -->
<input type="hidden" value="<?php echo $_GET['id']; ?>" id="legislationid" name="legislationid" />
