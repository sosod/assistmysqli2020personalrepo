<?php
$scripts = array("jquery.ui.deliverable.js");
include("../header.php");
?>
<script language="javascript">
$(function(){
	$("#display_deliverable").deliverable({updateAction:true, updateDeliverable:true, legislationId:$("#legislationid").val(), user: "0005", page:"update", section:"manage", filterByLegislation:true, filterByFinancialYear:true, autoLoad:false});
})
</script>
<div id="display_deliverable"></div>
<p class="ui-state-highlight" style="margin:0; padding:5px;" id="message">
     <span class="ui-icon ui-icon-info" style="float:left;"></span>
     <span>Please select the legislation to list their deliverables</span>
</p>
