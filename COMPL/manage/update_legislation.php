<?php
$scripts = array("legislation.js");
include("../header.php");

$legObj 	 = new ClientLegislation();
$legislation = $legObj -> findById( $_GET['id'] );
$legReminder = strtotime($legislation['reminder']);
$reminder = (!empty($legReminder) ? date("d-M-Y", strtotime($legislation['reminder'])) : "");

$statusObj = new Legislationstatus();
$statuses  =  $statusObj -> getStatusInOrder();

?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" width="100%">
	<tr>
		<td class="noborder" width="50%">
             <?php include_once("../common/legislation.inc.php"); ?>
		</td>
		<td class="noborder" width="50%">
			<form>
			<table width="100%">
				<tr>
					<td colspan="2"><h4>Update Legislation</h4></td>
				</tr>
				<tr>
					<th>Response:</th>
					<td>
						<textarea rows="7" cols="30" name="response" id="response"></textarea>
					</td>
				</tr>
				<tr>
					<th>Status:</th>
					<td>
						<select name="status" id="status">
							<option value="">--status--</option>
							<?php foreach($statuses as $index => $status){ ?>							
								<option
								<?php if( $status['id'] == 1 ){?>
									disabled="disabled"
								 <?php } if($status['id'] == $legislation['status']) { ?>
								 	selected="selected"
								 <?php } else if($legislation['status'] == "0" || $legislation['status'] == 0) { ?>
								 	selected="selected"
								 <?php } ?>
								 value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th>Remind On:</th>
					<td>
						<input type="text" name="reminder" id="reminder" value="<?php echo $reminder; ?>" class="datepicker" readonly="readonly"/> 
						<a href="#" id="clear_remind">Clear Date</a>
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="button" name="update" id="update" value="Update" />
						<input type="reset" name="cancel" id="cancel" value="Cancel" class="idelete" />
						<input type="hidden" name="legislation_id" id="legislation_id" value="<?php echo $_GET['id'] ?>" class="logid"  />
						<input type="hidden" name="legislationId" id="legislationId" value="<?php echo $_GET['id']; ?>" /> 
					</td>
				</tr>
			</table>	
			</form>		
		</td>
	</tr>
</table>
