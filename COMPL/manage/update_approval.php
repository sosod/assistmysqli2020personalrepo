<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
//$actionObj = new Action();
//$deliObj   = new DeliverableManager();
//$legiObj   = new LegislationManager();
///$legiObj -> getUpdatedLegislations();


?>
<table width="60%">
  <tr>
    <th>Ref</th>
    <th>Description</th>
    <th>Action</th>
  </tr>
  <tr>
    <td>Action</td>
    <td><p class="ui-widget ui-state-info" style="margin:5px 0px 10px 0px; padding:0.6em;"><span class="ui-icon ui-icon-info" style="float:left; margin-right:0.3em;"></span><span style="padding-top:30px;">There has been changes to the actions</span></p></td>
    <td><input type="button" name="view_update_action" id="view_update_action" value="View and Update" /></td>    
  </tr>
  <tr style="display:none;" id="update_action">
 	<td colspan="3">
 		<table></table>
 	</td> 	
  </tr> 
  <tr>
    <td>Deliverable</td>
    <td><p class="ui-widget ui-state-info" style="margin:5px 0px 10px 0px; padding:0.6em;"><span class="ui-icon ui-icon-info" style="float:left; margin-right:0.3em;"></span><span style="padding-top:30px;">There has been changes to the deliverables</span></p></td>
    <td><input type="button" name="view_update_deliverable" id="view_update_deliverable" value="View and Update" /></td>    
  </tr>
  <tr style="display:none;" id="update_deliverable">
 	<td colspan="3">
 		<table></table>
 	</td> 	
  </tr>    
  <tr>
    <td>Legislation</td>
    <td><p class="ui-widget ui-state-info" style="margin:5px 0px 10px 0px; padding:0.6em;"><span class="ui-icon ui-icon-info" style="float:left; margin-right:0.3em;"></span><span style="padding-top:30px;">There has been changes to the legislation</span></p></td>
    <td><input type="button" name="view_update_legislation" id="view_update_legislation" value="View and Update" /></td>    
  </tr> 
   <tr style="display:none;" id="update_legislation">
 	<td colspan="3">
 		<table></table>
 	</td> 	
  </tr>      
</table>
<script>
	$(function(){

		$("#view_update_action").click(function(){
			document.location.href = "approval_action.php";
			return false;
		});

		$("#view_update_deliverable").click(function(){
			document.location.href = "approval_deliverables.php";
			return false;
		});		

		$("#view_update_legislation").click(function(){
			document.location.href = "approval_legislations.php";
			return false;
		});		
		//$("#legislation").legislation({approvalUpdate:true, legoption : "active", url : "../class/request.php?action=legislationManager.getUpdatedLegislations",page:"manage"});
	});
</script>
<?php JSdisplayResultObj(""); ?>   
<?php displayGoBack("", ""); ?>
