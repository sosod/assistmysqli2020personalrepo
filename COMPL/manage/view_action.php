<?php //error_reporting(-1);
$scripts = array('guidance.js', 'action.js', 'ajaxfileupload.js');
include("../header.php");

$action_id = $_REQUEST['id'];

$actionObj 	   = new ClientAction();
$action        = $actionObj -> getLegislationAction($action_id);

$deliverable_id = $action['deliverable_id']; 
$delObj = new ClientDeliverable();
$deliverable = $delObj->fetch($deliverable_id);

$legislation_id = $deliverable['legislation'];	

$legObj 	 = new ClientLegislation();
$legislation = $legObj -> findById($legislation_id);

?>
<form>
<table width="100%" border="2" class="noborder" style="table-layout:fixed;">
	<tr>
		<td class="noborder" width="50%">
			<h2>Action Details</h2>
            <?php 
				include_once("../common/action.inc.php"); 
			?>
			<h2>Deliverable Details</h2>
			<?php
				$deliverableFormObj = new DeliverableDisplay($deliverable_id);
				$deliverableFormObj -> display(new DeliverableNaming());         
			?>
            <h2>Legislation Details</h2>
            <?php 
				include_once("../common/legislation.inc.php"); 
			?>
		</td>
		<td class="noborder" width="50%" style='padding-left: 25px'>
			<h2>Activity Log</h2>
			<table id=action_activity_log width=100% class=list>
			<tr>
				<th>Date/Time</th><th>User</th><th>Activity</th><th>Status</th>
			</tr>
			<?php 
				echo $actionObj->getActionViewLog($action_id); 
				//$compl = new COMPL_ACTION();
				//$attach_display = $compl->displayAttachmentList($action_id);
				//if(strlen($attach_display)>0) {
				//	echo "<tr><td colspan=4>".$attach_display."</td></tr>";
				//}
			?>
			</table>
		</td>
	</tr>
</table>
</form>
<?php displayGoBack(); ?>
<script type=text/javascript>
$(function() {
	$("#action_details_table tr:last, #action_details_table tr:first").hide();
	$("#deliverable_details_table tr:first").hide();
	$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
	$("table.list th").css("text-align","center");
	$("#action_details_table th, #deliverable_details_table th, #legislation_details_table th").prop("width","25%");
});
</script>