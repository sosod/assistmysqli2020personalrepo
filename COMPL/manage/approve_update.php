<?php
$scripts = array("view.js","jquery.ui.legislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?> 
<table width="60%"> 

  <tr>
    <td>
     <table width="100%">
       <tr>
         <th>Ref</th>
         <th>Action Change</th>
         <th>Date</th>
         <th>Edited By</th>
         <th></th>
       </tr>      
       <tr>
         <td>Action Ref</td>
         <td>Action Change</td>
         <td>Date Changes</td>
         <td>Edited By</td>
         <td><input type="button" name="update" id="update" value="Update" /></td>
       </tr>       
     </table>
    </td>
  </tr>

  <tr>
    <td><input type="button" name="update_all" id="update_all" value="Update All Changes" /></td>
  </tr>

</table>
<script>
	$(function(){

		$("#view_update_action").click(function(){
			document.location.href = "approval_action.php";
			return false;
		});

		$("#view_update_deliverable").click(function(){
			document.location.href = "approval_deliverables.php";
			return false;
		});		

		$("#view_update_legislation").click(function(){
			document.location.href = "approval_legislations.php";
			return false;
		});		
		//$("#legislation").legislation({approvalUpdate:true, legoption : "active", url : "../class/request.php?action=legislationManager.getUpdatedLegislations",page:"manage"});
	});
</script>  
<?php displayGoBack("", ""); ?>
