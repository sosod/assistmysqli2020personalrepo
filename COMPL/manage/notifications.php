<?php
$scripts = array("jquery.compl.notification.js");
include("../header.php");
JSdisplayResultObj("");
?>
<script language="javascript">
	$(function(){
		$("#notifications").notification();
	});
</script>
<table class="noborder">
 <tr>
   <td colspan="2" class="noborder">
        <div id="notifications"></div>
   </td>
 </tr>
 <tr>
   <td class="noborder"><?php displayGoBack("", ""); ?></td>
   <td class="noborder"><?php displayAuditLogLink("user_notifications_logs", true) ?></td>
 </tr>
</table>
