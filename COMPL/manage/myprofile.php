<?php
$scripts = array("user.js");
include("../header.php");
$userObj = new UserSetting();
$optionSql  = " UA.user_id = '".$_SESSION['tid']."' ";
$user       = $userObj -> getUser($optionSql); 
JSdisplayResultObj(""); ?>
<form name="addkpaform" id="addkpaform">
<table width="50%">
    <tr>
        <th>Default Financial Year</th>
        <td>
            <span id='default_year'>
            <?php 
            if(empty($user['financialyear']))
            {
                echo "
                     <p class='ui-state ui-state-error' style='padding:5px;'>
                        <span class='ui-icon ui-icon-closethick' style='float:left; margin-right:10px;'></span>
                        <span>Default Financial Year not set up</span>
                      </p>";
            } else {     
              $finObj = new FinancialYear();
              $financialYear = $finObj -> fetch($user['financialyear']);                     
              echo $financialYear['start_date']." <i>to</i> ".$financialYear['end_date'];  
            }
           ?>
           </span>
        </td>        
        <td>
            <input type="button" name="configure" id="configure" value="Configure" />
            <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>" />
        </td>
    </tr>
    <tr>
        <th>Notifications</th>
        <td>Configure email notifications</td>        
        <td>
            <input type="button" name="notifications" id="notifications" value="Configure" disabled=disabled />*
        </td>
    </tr>    	
</table>    
</form>
<p>* This functionality is temporarily unavailable.  Please try again later.</p>
