<?php
$scripts = array("guidance.js", "deliverable.js");
include("../header.php");
$dOb = new ClientDeliverable();

$statusObj   = new DeliverableStatus();
$statuses    = $statusObj -> getStatusInOrder();

$deliverable = $dOb -> fetch( $_GET['id'] );

$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById( $deliverable['legislation'] );
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" width="100%">
 <tr>
   <td width="50%" class="noborder">
   <?php            
     $deliverableFormObj = new DeliverableDisplay($_GET['id']);
     $deliverableFormObj -> display(new DeliverableNaming());    	   
    ?></td>
   <td width="50%" class="noborder">
	<form>
	<table width="100%" class="noborder">
		<tr>
			<td><h4>Update Deliverable </h4></td>
       		<td style="text-align:right;">
			      <?php if(Legislation::isImported($legislation['legislation_status']))
			      {
			          $controller->printRequestGuidance("");
			      }
       			?>
       		</td>					
		</tr>
		<tr>
			<th>Response:</th>
			<td>
				<textarea rows="7" cols="30" name="response" id="response"></textarea>
			</td>
		</tr>
		<tr>
			<th>Status:</th>
			<td>
				<select name="deliverablestatus" id="deliverablestatus">
					<option value="">--status--</option>
					<?php foreach( $statuses as $index => $status){ ?>							
						<option
						<?php echo ($status['id'] == 1 ? "disabled='disabled'" : ""); ?>
						 <?php if($status['id'] == $deliverable['deliverable_status']) { ?>
						 	selected="selected"
						 <?php } else if(empty($deliverable['deliverable_status']) || $status['id'] == 1) { ?>
						 	selected="selected"
						 <?php } ?>
						 value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Remind On:</th>
			<td>
			     <input type="text" name="remindon" id="remindon" value="<?php echo $deliverable['reminder']; ?>"  class="datepicker" readonly="readonly" />
			     <a href="#" id="clear_remind">Clear Date</a>
			</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<input type="button" name="update" id="update" value="Update" class="isubmit" />
				<input type="reset" name="cancel" id="cancel" value="Cancel" class="idelete" />
				<input type="hidden" name="deliverableId" id="deliverableId" value="<?php echo $_GET['id']; ?>" />
				<input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $_GET['id'] ?>" class="logid"  />
				<input type="hidden" name="legislationid" id="legislationid" value="<?php echo $deliverable['legislation']; ?>" /> 
				<?php displayAuditLogLink("deliverable_update", false) ?>
			</td>
		</tr>
	</table>	
    </form>
   </td>
 </tr>
 <tr>
     <td class="noborder"><?php displayGoBack("", ""); ?></td>    
     <td class="noborder"></td>
 </tr>    
<table>
