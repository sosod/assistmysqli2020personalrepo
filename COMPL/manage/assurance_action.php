<?php //error_reporting(-1);
$scripts = array("jquery.ui.actionassurance.js");
include("../header.php");

$action_id = $_REQUEST['id'];

$actionObj 	   = new ClientAction();
$action        = $actionObj -> getLegislationAction($action_id);

$deliverable_id = $action['deliverable_id']; 
$delObj = new ClientDeliverable();
$deliverable = $delObj->fetch($deliverable_id);

$legislation_id = $deliverable['legislation'];	

$legObj 	 = new ClientLegislation();
$legislation = $legObj -> findById($legislation_id);

?>
<?php JSdisplayResultObj(""); ?> 
<form>
<input type="hidden" name="action_id" value="<?php echo $action_id; ?>" id="action_id" class=logid />
<table width="100%" border="2" class="noborder" style="table-layout:fixed;">
	<tr>
		<td class="noborder" width="50%">
			<h2>Action Details
			<span class=float style=' padding: 0px; margin-top: -5px;'>
				<input type=button value="Deliverable" id=btn_del />
				<input type=button value="Legislation" id=btn_leg />
			</span>
			</h2>
            <?php 
				include_once("../common/action.inc.php"); 
			?>
			<h2>Activity Log</h2>
			<table id=action_activity_log width=100% class=list>
			<tr>
				<th>Date/Time</th><th>User</th><th>Activity</th><th>Status</th>
			</tr>
			<?php 
				echo $actionObj->getActionViewLog($action_id); 
				/*$compl = new COMPL_ACTION();
				$attach_display = $compl->displayAttachmentList($action_id);
				if(strlen($attach_display)>0) {
					echo "<tr><td colspan=4>".$attach_display."</td></tr>";
				}*/
			?>
			</table>			
		</td>
		<td class="noborder" width="50%" style='padding-left: 25px'>
			<table width="100%" id="assurance_action">
			</table>
			<div id=assurance_logs style='padding: 3px;'>
				<div class='right' style='margin: 10px'><span class='show' id=display_audit_log style='cursor: pointer; margin-right: 10px; text-decoration: underline;'><img src="/pics/tri_down.gif" style='vertical-align: middle; padding-right: 3px' /><span id=word>Display</span> Activity Log</span></div>
				<table id=audit_logs class=list>
					<tr>
						<th>Date</th>
						<th>User</th>
						<th>Activity</th>
					</tr>
				</table>
				<script type=text/javascript>
					$(function() {
						var tbl = "action_assurance_log";
						var where = "action_id = "+$("#action_id").val();
						var fld = "changes";
						$("#audit_logs").hide();
						$("#display_audit_log").click(function() {
							if($(this).hasClass("show")) {
								var result = AssistHelper.doAjax("../common/common_controller.php",{action:"getLogs",table:tbl,options:where,field:fld});
								$.each(result, function(index, r) {
									//alert(r['changes']);
									//alert(AssistString.convert_to_html(r['changes']));
									var chg = AssistString.str_replace("\n"," ",r['changes']);
									var chg = AssistString.convert_to_html(chg);
									$("#audit_logs")
										.append($("<tr />",{class:"log"})
											.append($("<td />",{html:r['date'], class:"center"}))
											.append($("<td />",{html:r['user'], class:"center"}))
											.append($("<td />",{html:chg}))
										);
										
								});
								
								$("#audit_logs").show();
								$(this).removeClass("show");
								$("#display_audit_log img").prop("src","/pics/tri_up.gif");
								$("#word").html("Hide");
							} else {
								$(this).addClass("show");
								$("#word").html("Display");
								$("#display_audit_log img").prop("src","/pics/tri_down.gif");
								$("#audit_logs tr.log").remove();
								$("#audit_logs").hide();
							}
						});
					});
				</script>
			</div>
		</td>
	</tr>
</table>

</form>
<?php displayGoBack(); ?>
<script type=text/javascript>
$(function() {
	$("#assurance_action").actionassurance({id:$("#action_id").val()})


	$("#action_details_table tr:last, #action_details_table tr:first").hide();
	$("table.list th").css("text-align","center");
	$("#action_details_table th, #deliverable_details_table th, #legislation_details_table th").prop("width","25%");
});
</script>


<div id=div_del title="Deliverable Details">
			<h2>Deliverable Details</h2>
			<?php
				$deliverableFormObj = new DeliverableDisplay($deliverable_id);
				$deliverableFormObj -> display(new DeliverableNaming());         
			?>
</div>
<div id=div_leg title="Legislation Details">
            <h2>Legislation Details</h2>
            <?php 
				include_once("../common/legislation.inc.php"); 
			?>
</div>
<script type=text/javascript>
$(function() {
	$("#deliverable_details_table tr:first").hide();
	$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
	$("#btn_del, #btn_leg").button().addClass("small-button");
var screen = AssistHelper.getWindowSize();
	$("#div_del, #div_leg").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		height: (screen['height']-25)
	});
	$("#btn_del").click(function() {
		$("#div_del").dialog("open");
	});
	$("#btn_leg").click(function() {
		$("#div_leg").dialog("open");
	});
});
</script>