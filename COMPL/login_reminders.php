<?php
include_once("../library/dbconnect/dbconnect.php");	
include_once("class/model.php");
include_once("class/basenaming.php");
include_once("class/naming.php");
include_once("class/deliverablenaming.php");
include_once("class/action.php");
include_once("class/deliverable.php");
include_once("class/legislation.php");
include_once("class/actionreminder.php");
include_once("class/deliverablereminder.php");
include_once("class/legislationreminder.php");
include_once("../library/class/assist_email.php");
//include_once("loader.php");
//spl_autoload_register("Loader::autoload");


function emailCOMPL($db, $userarray = array())
{  

   $emailObj = new ASSIST_EMAIL();
	$emailObj->setSenderUserID("AUTO");
   
   $actionObj = new ActionReminder($db, $emailObj);
   $actionResponse = $actionObj -> sendReminders($userarray);

   $deliverableObj = new DeliverableReminder($db, $emailObj);
   $deliverableResponse = $deliverableObj -> sendReminders($userarray);

   $legislationObj = new LegislationReminder($db, $emailObj);
   $legislationResponse       = $legislationObj -> sendReminders($userarray);
   
   $total = $deliverableResponse['totalSend'] + $actionResponse['totalSend'] + $legislationResponse['totalSend'];

    unset($actionResponse['totalSend']);
    unset($deliverableResponse['totalSend']);
    unset($legislationResponse['totalSend']);
    $response = array_merge($deliverableResponse, $actionResponse, $legislationResponse);     
    $reminders = array();
    $x = 1;
    foreach($response as $resIndex => $reminder)
    {
      $reminders[$x++] = $reminder;
    }
    $reminders[0] = $total." reminders send ";
    return $reminders;
}
?>
