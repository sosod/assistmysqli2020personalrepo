<?php
	require_once '../../inc_session.php';
	require_once '../../inc_db_conn.php'; 
     include_once("../loader.php");
     @session_start();
     spl_autoload_register("Loader::autoload");	
	ini_set("error_reporting", 1);
	ini_set("display_errors", 1);
	$dbref = "assist_".$_SESSION['cc']."_".strtolower($_SESSION['modref']);
	$colours = array('#bb0000','#bb5f00','#00bb00','#0000bb','#dd0000','#dd6e00','#00dd00','#0000dd','#ff1111','#ff7811','#11ff11','#1111ff','#ff3333','#ff8733','#33ff33','#3333ff','#ff5555','#ffaa55','#55ff55','#5555ff','#ff7777','#ffbb77','#77ff77','#7777ff','#ff9999','#ffcc99','#99ff99','#9999ff','#990000','#995000','#009900','#000099');
	//$colours = array('#000055','#000077','#000099','#0000bb','#0000dd','#1111ff','#3333ff','#5555ff','#7777ff','#9999ff','#bbbbff','#ddddff');
?>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
	<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
	<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script> 
	<script src="../js/legislation_compliance_status_per_deliverable.js" type="text/javascript"></script>        
	<link rel="stylesheet" href="/assist.css" type="text/css">
	<!-- <script type ="text/javascript" src="/assist.js"></script> -->
<style type=text/css>
#color-check td, #legend td, #legend, #legend2, #legend2 td, #tbl_graphs, #tbl_graphs td { border: 0px solid #ffffff; }
</style>
</head>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<table width="100%"  class="noborder">
 <tr>
   <td align="center" style="text-align=center;" class="noborder">
     <select id="financialyear" name="financialyear">
     </select>
   </td>
 </tr>
 <tr>
   <td class="noborder">
     <div id="report" align="center"><p><em>Select the financial year to generate the report.<br />Please note that the report can take some time to generate.  Please be patient</em></p></div>
   </td>
 </tr>
</table>
</body>
</html>
