<?php
$scripts = array("report.js");
include("../header.php");
$naming = new LegislationNaming();
$headers = $naming -> getNaming();
if(isset($_POST['update'])) 
{
    if($_POST['report_name'] == "")
    {
		echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Please enter the name of the report</div>";
    } else {
	  $reportObj = new Report();
	  $reportObj -> updateQuickReport($_POST);
    }
} 
?>
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table width="70%" class="noborder"> 
	<tr>
		<th colspan="3">1.Select the legislation to be displayed on the report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<td>
		     <input type="checkbox" name="legislations[id]" id="legislation_id" class="legislation" />
		     <?php echo $naming -> getHeader("id", TRUE); ?>
		 </td>
	 <td><input type="checkbox" name="legislations[type]" id="legislation_type" class="legislation" /><?php echo $naming -> getHeader("type", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[organisation_capacity]" id="legislation_organisation_capacity" class="legislation" /><?php echo $naming->getHeader("organisation_capacity",  TRUE); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="legislations[name]" id="legislation_name" class="legislation" /><?php echo $naming->getHeader("name", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[category]" id="legislation_category" class="legislation" /><?php echo $naming->getHeader("category", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[legislation_date]" id="legislation_legislation_date" class="legislation" /><?php echo $naming->getHeader("legislation_date", TRUE); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="legislations[reference]" id="legislation_reference" class="legislation" /><?php echo $naming->getHeader("reference", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[organisation_size]" id="legislation_organisation_size" class="legislation" /><?php echo $naming->getHeader("organisation_size", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[hyperlink_to_act]" id="legislation_hyperlink_to_act" class="legislation" /><?php echo $naming->getHeader("hyperlink_to_act", TRUE); ?></td>
	</tr>		
	<tr>	     
		<td><input type="checkbox" name="legislations[organisation_type]" id="legislation_organisation_type" class="legislation" /><?php echo $naming->getHeader("organisation_type", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[action_progress]" id="legislation_action_progress" class="legislation" /><?php echo $naming->getHeader("action_progress", TRUE); ?></td>
		<td><input type="checkbox" name="legislations[status]" id="legislation_status" class="legislation" /><?php echo $naming->getHeader("status", TRUE); ?></td>		
	</tr>				
	<tr>	     
		<td><input type="checkbox" name="legislations[legislation_number]" id="legislation_legislation_number" class="legislation" /><?php echo $naming->getHeader("legislation_number", TRUE); ?></td>
		<td></td>
		<td></td>		
	</tr>			
	<tr>
		<td></td>
		<td><input type="button" name="check_legislation" id="check_legislation" value=" Check All" /></td>
		<td><input type="button" name="invert_legislation" id="invert_legislation" value=" Invert" /></td>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th colspan="3">2. Select the filters you wish to apply</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>
	<tr>
    	<th><?php echo $naming->getHeader("legislation_name"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" id="name" name="value[name]" class="checkcounter"></textarea>
    		<span class="textcounter" id="name_textcounter"></span>
    	</td>
    	<td>
    		<select name="match[name]" id="match_name" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $naming->getHeader("legislation_reference"); ?>:</th>    
    	<td><input type="text" name="value[reference]" id="reference" value="" /></td>
    	<td>
    		<select name="match[reference]" id="match_reference" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>  	
    	</td>
    </tr>
	<tr>
    	<th><?php echo $naming->getHeader("legislation_number"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[legislation_number]" id="legislation_number" class="checkcounter"></textarea>
    		<span class="textcounter" id="legislation_number_textcounter"></span>
        </td>
        <td>
			<select name="match[legislation_number]" id="match_legislation_number" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>        
	<tr>
    	<th><?php echo $naming->getHeader("legislation_type"); ?>:</th>    
    	<td>
        	<select name="value[type][]" id="type" multiple="multiple">
        		<option value="all" selected="selected">All</option>
            </select>
        </td>
        <td></td>
    </tr>        
	<tr>
    	<th><?php echo $naming->getHeader("legislation_category"); ?>:</th>    
    	<td>
        	<select name="value[category][]" id="category" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>    
	<tr>
    	<th><?php echo $naming->getHeader("organisation_size"); ?>:</th>    
    	<td>
        	<select name="value[organisation_size][]" id="organisation_size" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>    
    <tr>
    	<th><?php echo $naming->getHeader("organisation_type"); ?>:</th>    
    	<td>
        	<select name="value[organisation_type][]" id="organisation_type" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>   
	<tr>
    	<th><?php echo $naming->getHeader("organisation_capacity"); ?>:</th>    
    	<td>
        	<select name="value[organisation_capacity][]" id="organisation_capacity" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>  		 
	<tr>
    	<th><?php echo $naming->getHeader("legislation_status"); ?>:</th>    
    	<td>
        	<select name="value[status][]" id="status" multiple="multiple">
        	 	<option value="all" selected="selected">All</option>
            </select>        
        </td>
        <td></td>
    </tr>                                    
	<tr>
    	<th><?php echo $naming->getHeader("hyperlink_act"); ?>:</th>    
    	<td>
          <input type="text" name="value[hyperlink_to_act]" id="hyperlink_to_act" value="" />
        </td>
        <td>
        	<select name="match[hyperlink_to_act]" id="match_hyperlink_to_act" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>
    </tr> 
	<tr>
    	<th><?php echo $naming->getHeader("legislation_date"); ?>:</th>    
    	<td> 
          From : <input type="text" name="value[legislation_date_date][from]" id="from_legislation_date" class="datepicker" value="" readonly="readonly"/>
        </td>
       <td> To	: <input type="text" name="value[legislation_date_date][to]" id="to_legislation_date" class="datepicker" value="" readonly="readonly"/>
       </td>
    </tr>     
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>    
    <tr>
    	<th colspan="3">3. Choose the group and sort options </th>
 	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 	
  <tr>
    <th>Group By:</th>
    <td>
    	<select name="group_by" id="group">
    		<option value="no_grouping">No Grouping</option>
    		<option value="type_">Legislation Type</option>
    		<option value="category_">Legislation Category</option>
    		<option value="organisation_size_">Organisation Size</option>
    		<option value="organisation_capacity_">Organisation Capacity</option>
    		<option value="organisation_type_">Organisation Type</option>
    	</select>
    </td>
     <td></td>
  </tr>
  <tr>
  	<th>Sort By:</th>
  	<td>
		<ul id="sortable" style="list-style:none;">
		</ul>  	  	
  	</td>
    <td></td>
  </tr>
	<!-- <tr>
		<td colspan="3" class="noborder"></td>
	</tr>  
	<tr>
	    <th colspan="3">6.Choose the document format of your report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	  
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="on_screen" value="on_screen" checked="checked" />On Screen</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell(<i>plain</i>)</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell(<i>formated</i>)</td>
  </tr>
  <tr>
  	<td colspan="3"><input type="radio" name="document_format" id="save_pdf" value="save_pdf" />Save to Pdf</td>
  </tr> -->
 	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 
	<tr>
		<th colspan="3">4.Generate Report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th>Report Title</th>
		<td colspan="2"><input type="text" name="report_title" id="report_title" value="" /></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td colspan="2"><input type="text" name="report_name" id="report_name" value="" /></td>
	</tr>	
	<tr>
		<th>Description</th>
		<td colspan="2"><textarea name="report_description" id="report_description" cols="30" rows="7"></textarea></td>
	</tr>
	<tr>
		<th></th>
		<td colspan="2">
			<input type="submit" name="update" id="update" value="Update Report" />
			<input type="hidden" name="quickid" id="quickid" value="<?php echo $_GET['id']; ?>" />
			<input type="hidden" name="document_format" id="document_format" value="on_screen" />
		</td>
	</tr>		
<tr>
 <!-- <td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">* Please note the following with regards to the formatted Microsoft Excel report:<br />
		<ol>
			<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
			It is safe to click on the "Yes" button to open the document.</li>
		</ol>
	</td> -->
</tr>
</table>
</form>
