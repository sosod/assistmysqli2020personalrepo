<?php
require_once '../../inc_session.php';
require_once '../../inc_db_conn.php'; 
require_once( "../../library/class/graph.php" );
include_once("../loader.php");
@session_start();
spl_autoload_register("Loader::autoload");	
$reportObj = new ReportManager();
$page_title = "Compliance Dashboard";
$blurb = "for the ".$_GET['financialyear']." financial year";
echo "<table class='noborder'>";
 echo "<tr>";
   echo "<td class='noborder'>";
      $legislationCompliance = $reportObj -> getCompliance($_GET['financialyear']);
      $compliantCategories = array();
      $compliantCategories[]  = array("text" => "Compliant", "code" => "compliant", "color" => "green");
      $compliantCategories[]  = array("text" => "Not Compliant", "code" => "notcompliant", "color" => "red");
      if(!empty($legislationCompliance))
      {
          $legG = new Assist_Graph(1);
          //only JAVA graphs are working at the moment:
          $legG->setFormatAsJava();
          //this page has no html header so set draw_header = true;
          $legG->setDrawHeader(true);
          //set the graph categories
          $legG->setGraphCategories($compliantCategories);
          //set the data for the main graph
          $legG->setMainCount($legislationCompliance);
          //don't display the sub graphs
          $legG->setHasSubGraph(false);
          //set the page title
          $legG->setPageTitle("Legislation Compliance Summary");
          //draw the graph
          $legG->drawPage();          
     } else {
          echo "There is no legislation data for reporting";    
     }
   echo "</td>";
   echo "<td class='noborder'>";
     $graph_categories = array();
     $graph_categories[] = array(
                 'text' => "Completed Before Deadline",
                 'code' => "completedBeforeDeadline",
                 'color' => "#000077",
     );
     $graph_categories[] = array(
                 'text' => "Completed On Deadline Date",
                 'code' => "completedOnDeadlineDate",
                 'color' => "#009900",
     );
     $graph_categories[] = array(
                 'text' => "Completed After Deadline Date",
                 'code' => "completedAfterDeadline",
                 'color' => "#FE9900",
     );
     $graph_categories[] = array(
                 'text' => "Not Completed And Overdue",
                 'code' => "notCompletedAndOverdue",
                 'color' => "#FF0000",
     );
     $graph_categories[] = array(
                 'text' => "Not Completed And Not Overdue",
                 'code' => "notCompletedAndNotOverdue",
                 'color' => "#999999",
     );
     $deliverableCompliance = $reportObj -> _getDeliverableCompliance("", $_GET['financialyear']);
     if(!empty($deliverableCompliance))
     {
          $my_title = "Deliverable Compliance Summary";
          $graph_id = 0;
          $myG = new Assist_Graph(2);
          //only JAVA graphs are working at the moment:
          $myG->setFormatAsJava();
          //this page has no html header so set draw_header = true;
          $myG->setDrawHeader(true);
          //set the graph categories
          $myG->setGraphCategories($graph_categories);
          //set the data for the main graph
          $myG->setMainCount($deliverableCompliance);
          //don't display the sub graphs
          $myG->setHasSubGraph(false);
          //set the page title
          $myG->setPageTitle($my_title);
          //draw the graph
          $myG->drawPage();
          //line break between demos
     } else {
          echo "There is deliverable data for reporting";
     }
   echo "</td>";
 echo "</tr>";
 echo "<tr>";
   echo "<td class='noborder' colspan='2'>";
     $actionCompliance = $reportObj -> getActionComplianceStatus($_GET['financialyear']);
     /* Action Progress graph */

     $actionObj = new ActionStatus();
     $statuses  = $actionObj -> getActionStatuses();
     $stausList = array();
     $colors = array("1" => "red", "2" => "orange", "3" => "green"); 
     foreach($statuses as $index => $status)
     {
       $name  = (!empty($status['client_terminology']) ? $status['client_terminology'] : $status['name']);
       $color = (empty($status['color']) ? $colors[$status['id']] : $status['color']);
       $stausList[$status['id']] = array("text" => $name, "code" => $status['id'], "color" => $color);
     }
     if(!empty($actionCompliance))
     {
          $actionG = new Assist_Graph(3);
          //only JAVA graphs are working at the moment:
          $actionG->setFormatAsJava();
          //this page has no html header so set draw_header = true;
          $actionG->setDrawHeader(true);
          //set the graph categories
          $actionG->setGraphCategories($stausList);
          //set the data for the main graph
          $actionG->setMainCount($actionCompliance);
          //don't display the sub graphs
          $actionG->setHasSubGraph(false);
          //set the page title
          $actionG->setPageTitle("Action Progress Summary");
          //draw the graph
          $actionG->drawPage();          
     } else {
          echo "There is no action data for reporting";    
     }
   echo "</td>";  
 echo "</tr>";
echo "<table>";

/*
LEGISLATION COMPLIANCE GRAPH
*/


/** DEMO DATA **/


//PIE Graph: Full settings changed:
/** DEMOS **/
//Minimum setting changes:





?>
