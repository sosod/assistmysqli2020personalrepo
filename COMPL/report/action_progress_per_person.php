<?php
	require_once '../../inc_session.php';
	require_once '../../inc_db_conn.php'; 
     include_once("../loader.php");
     @session_start();
     spl_autoload_register("Loader::autoload");	

?>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
	<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
	<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script> 
	<script src="../js/action_progress_per_person.js" type="text/javascript"></script>        
	<link rel="stylesheet" href="/assist.css" type="text/css">
</head>
<body>
<table width="100%" class="noborder">
 <tr>
   <td align="center" style="text-align=center;" class="noborder">
     <select id="financialyear" name="financialyear">
     </select>
   </td>
 </tr>
 <tr>
   <td class="noborder">
     <div id="report" align="center"><p><em>Select the financial year to generate the report.<br />Please note that the report can take some time to generate.  Please be patient</em></p></div>
   </td>
 </tr>
</table>
</body>
</html>

