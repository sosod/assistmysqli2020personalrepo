<?php
$scripts = array("report.js");
include("../header.php");
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<?php JSdisplayResultObj(""); ?>   
<script type="text/javascript">
	var chart;
	$(function(){
		
		$.getJSON("../class/request.php?action=ReportManager.getDeliverableProgressStatus", function( reportData ){
		    var chartData = [
		     				{year:"2003",europe:2.5,namerica:2.5,asia:2.1,lamerica:0.3,meast:0.2,africa:0.1},
		     				{year:"2004",europe:2.6,namerica:2.7,asia:2.2,lamerica:0.3,meast:0.3,africa:0.1},
		     				{year:"2005",europe:2.8,namerica:2.9,asia:2.4,lamerica:0.3,meast:0.3,africa:0.1}
		     				];

	        chart = new AmCharts.AmSerialChart();
	        chart.dataProvider = reportData;
	        chart.categoryField = "legislation";
	        chart.marginLeft = 47;
	        chart.marginTop = 30;
			chart.plotAreaBorderAlpha = 0.2;
			chart.rotate = true;

			var graph = new AmCharts.AmGraph();
			graph.title = "New";
			graph.labelText="[[value]]";
			graph.valueField = "new";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "#2B65EC";
			chart.addGraph(graph);

			var graph = new AmCharts.AmGraph();
			graph.title = "In-Progress";
			graph.labelText="[[value]]";
			graph.valueField = "in_progress";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "#800517";
			chart.addGraph(graph);

			var graph = new AmCharts.AmGraph();
			graph.title = "Completed";
			graph.labelText="[[value]]";
			graph.valueField = "completed";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "#4E9258";
			chart.addGraph(graph);

			var valAxis = new AmCharts.ValueAxis();
			valAxis.stackType = "regular";
			valAxis.gridAlpha = 0.1;
			valAxis.axisAlpha = 0;
			chart.addValueAxis(valAxis);

			var catAxis = chart.categoryAxis;
			catAxis.gridAlpha = 0.1;
			catAxis.axisAlpha = 0;
			catAxis.gridPosition = "start";

			var legend = new AmCharts.AmLegend();
			legend.position = "right";
			legend.borderAlpha = 0.2;
			legend.horizontalGap = 10;
			legend.switchType = "h";
			chart.addLegend(legend);

			chart.write("chartdiv");
		});
		
	})
	
	function setType(){

		if(document.getElementById("rb1").checked){
			chart.rotate = true;
		}
		else{
			chart.rotate = false;
		}
		chart.validateNow();
	}	
</script>
<table class="noborder" width="100%">
	<tr>
		<td align="center" colspan="2" class="noborder">
	   		<center>
	   		<h1>
			  	<?php 
			  		echo $_SESSION['cn'];
			  	?>
	  		</h1>
	  		</center>		
		</td>
	</tr>
	<tr>
		<td colspan="2" class="noborder">
			<div id="chartdiv" style="width: 100%; height: 400px;"></div>
		</td>
	</tr>
  <tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
 		<input type="radio" checked="true" name="group" id="rb1" onclick="setType()"> bars
		<input type="radio" name="group" id="rb2" onclick="setType()"> columns
  	</td>
  </tr> 
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>