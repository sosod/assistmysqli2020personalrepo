<?php
$scripts = array("generate_deliverable.js");
include("../header.php");
$naming = new DeliverableNaming();
$naming -> getNaming();
if(isset($_POST['update'])) 
{
	if($_POST['report_name'] == "")
	{
	  echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Please enter the name of the report</div>";
	} else {
	  $reportObj = new Report();
	  $reportObj -> updateQuickReport($_POST);
	}
} 
?>
<?php JSdisplayResultObj(""); ?>
<form method="post">
  <table class="noborder">
    <tr>
     <td class="noborder">
          <table class="noborder" width="100%">																						
	          <tr>
		          <th colspan="3">1.Select the deliverable information to be displayed on the report</th>
	          </tr>
	          <tr>
		          <td colspan="3" class="noborder"></td>
	          </tr>	
	          <tr>
		       <td><input type="checkbox" name="deliverables[id]" id="deliverable_id" class="deliverable" /><?php echo $naming->getHeader("id", TRUE); ?></td>
		       <td><input type="checkbox" name="deliverables[compliance_date]" id="deliverable_compliance_date" class="deliverable" /><?php echo $naming->getHeader("compliance_date", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[sanction]" id="deliverable_sanction" class="deliverable" /><?php echo $naming->getHeader("sanction", TRUE); ?></td>
	          </tr>		
	          <tr>
		          <td><input type="checkbox" name="deliverables[short_description]" id="deliverable_short_description" class="deliverable" /><?php echo $naming->getHeader("short_description", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[responsible]" id="deliverable_responsible" class="deliverable" /><?php echo $naming->getHeader("responsible", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[reference_link]" id="deliverable_reference_link" class="deliverable" /><?php echo $naming->getHeader("reference_link", TRUE); ?></td>
	          </tr>		
	          <tr>
		          <td><input type="checkbox" name="deliverables[description]" id="deliverable_description" class="deliverable" /><?php echo $naming->getHeader("description", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[functional_service]" id="deliverable_functional_service" class="deliverable" /><?php echo $naming->getHeader("functional_service", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[assurance]" id="deliverable_assurance" class="deliverable" /><?php echo $naming->getHeader("assurance", TRUE); ?></td>
	          </tr>		
	          <tr>
		          <td><input type="checkbox" name="deliverables[legislation_section]" id="deliverable_legislation_section" class="deliverable" /><?php echo $naming->getHeader("legislation_section", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[accountable_person]" id="deliverable_accountable_person" class="deliverable" /><?php echo $naming->getHeader("accountable_person", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[compliance_to]" id="deliverable_compliance_to" class="deliverable" /><?php echo $naming->getHeader("compliance_to", TRUE); ?></td>
	          </tr>
	          <tr>
		          <td><input type="checkbox" name="deliverables[compliance_frequency]" id="deliverable_compliance_frequency" class="deliverable" /><?php echo $naming->getHeader("compliance_frequency", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[responsibility_owner]" id="deliverable_responsibility_owner" class="deliverable" /><?php echo $naming->getHeader("responsibility_owner", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[main_event]" id="deliverable_main_event" class="deliverable" /><?php echo $naming->getHeader("main_event", TRUE); ?></td>
	          </tr>	
	          <tr>
		          <td><input type="checkbox" name="deliverables[applicable_org_type]" id="deliverable_applicable_org_type" class="deliverable" /><?php echo $naming->getHeader("applicable_org_type", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[legislation_deadline]" id="deliverable_legislation_deadline" class="deliverable" /><?php echo $naming->getHeader("legislation_deadline", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[sub_event]" id="deliverable_sub_event" class="deliverable" /><?php echo $naming->getHeader("sub_event", TRUE); ?></td>
	          </tr>
	          <tr>
		          <td><input type="checkbox" name="deliverables[guidance]" id="deliverable_guidance" class="deliverable" /><?php echo $naming->getHeader("guidance", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[action_progress]" id="deliverable_action_progress" class="deliverable" /><?php echo $naming->getHeader("action_progress", TRUE); ?></td>
		          <td><input type="checkbox" name="deliverables[status]" id="deliverable_status" class="deliverable" /><?php echo $naming->getHeader("deliverable_status", TRUE); ?></td>
	          </tr>
	          <tr>
		          <td><input type="checkbox" name="deliverables[reporting_category]" id="deliverable_reporting_category" class="deliverable" /><?php echo $naming->getHeader("reporting_category", TRUE); ?></td>
		          <td></td>
		          <td></td>
	          </tr>	
	          <tr>
		          <td><input type="button" name="check_deliverable" id="check_deliverable" value=" Check All" /></td>
		          <td><input type="button" name="invert_deliverable" id="invert_deliverable" value=" Invert" /></td>
		          <td></td>
	          </tr>
          </table>      
     </td>
    </tr>
    <tr>
      <td class="noborder">
          <table width="100%">
	          <tr>
		          <td colspan="3" class="noborder"></td>
	          </tr>																											
	          <tr>
		          <th colspan="3">2.Select the filters you wish to apply</th>
	          </tr>
	          <tr>
		          <td colspan="3" class="noborder"></td>
	          </tr>
	          <tr>
              	<th><?php echo $naming->getHeader("short_description"); ?>:</th>    
              	<td>
              		<textarea name="value[short_description]" id="short_description"cols="30" rows="5"></textarea>
              	</td>
              	<td>
              		<select name="match[short_description]" id="match_short_description" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select>
              	</td>
              </tr>
	          <tr>
              	<th><?php echo $naming->getHeader("description"); ?>:</th>    
              	<td>
              	     <textarea name="value[description]" id="description"cols="30" rows='10'></textarea>
              	</td>
              	<td>
              		<select name="match[description]" id="match_description" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select>  	
              	</td>
              </tr>
	          <tr>
              	<th><?php echo $naming->getHeader("legislation_section"); ?>:</th>    
              	    <td>
                    <input type="text" name="value[legislation_section]" id="legislation_section" />
                  </td>
                  <td>
			          <select name="match[legislation_section]" id="match_legislation_section" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select> 		        
                  </td>  
              </tr>        
	          <tr>
              	<th><?php echo $naming->getHeader("compliance_frequency"); ?>:</th>    
              	<td>
		          <select name="value[compliance_frequency][]" id="compliance_frequency" multiple="multiple">
		            <option value="all" selected="selected">All</option> 
		          </select>    	     
                  </td>
                  <td></td>
              </tr>        
	          <tr>
              	<th><?php echo $naming->getHeader("applicable_org_type"); ?>:</th>    
              	<td>
                  	<select name="value[applicable_org_type][]" id="applicable_org_type" multiple="multiple">
                  	    <option value="all" selected="selected">All</option>
                    </select>        
                  </td>
                  <td></td>
              </tr>    
	          <tr>
              	<th><?php echo $naming->getHeader("compliance_date"); ?>:</th>    
                    <td> 
                         From : <input type="text" name="value[compliance_date_date][from]" id="from_compliance_date" class="datepicker" value="" readonly="readonly"/>
                    </td>
                    <td> 
                         To	: <input type="text" name="value[compliance_date_date][to]" id="to_compliance_date" class="datepicker" value="" readonly="readonly"/>
                    </td>     
                  </td>
              </tr>    
              <tr>
              	<th><?php echo $naming->getHeader("responsible"); ?>:</th>    
              	<td>
                  	  <select name="value[responsible][]" id="responsible" multiple="multiple">
                  	     <option value="all" selected="selected">All</option>
                      </select>        
                  </td>
                  <td></td>
              </tr>   
	          <tr>
              	<th><?php echo $naming->getHeader("functional_service"); ?>:</th>    
              	<td>
                  	  <select name="value[functional_service][]" id="functional_service" multiple="multiple">
                  	     <option value="all" selected="selected">All</option>
                      </select>        
                  </td>
                  <td></td>
              </tr>  
	          <tr>
              	<th><?php echo $naming->getHeader("accountable_person"); ?>:</th>    
              	<td>
                  	  <select name="value[accountable_person][]" id="accountable_person" multiple="multiple">
                  	     <option value="all" selected="selected">All</option>
                      </select>        
                  </td>
                  <td></td>
              </tr>  
	          <tr>
              	<th><?php echo $naming->getHeader("responsibility_owner"); ?>:</th>    
              	<td>
                  	  <select name="value[responsibility_owner][]" id="responsibility_owner" multiple="multiple">
                  	     <option value="all" selected="selected">All</option>
                      </select>        
                  </td>
                  <td></td>
              </tr>  
	          <tr>
                   	<th><?php echo $naming->getHeader("legislative_deadline_date"); ?>:</th>    
                    <td> 
                         From : <input type="text" name="value[legislation_deadline_date][from]" id="from_legislation_deadline" class="datepicker" value="" readonly="readonly"/>
                    </td>
                    <td> 
                         To	: <input type="text" name="value[legislation_deadline_date][to]" id="to_legislation_deadline" class="datepicker" value="" readonly="readonly"/>
                    </td>     
               </tr>  
	          <tr>
              	<th><?php echo $naming->getHeader("sanction"); ?>:</th>    
              	<td>
                    <textarea name="value[sanction]" id="sanction" cols="30" rows='5'></textarea>      
               </td>
               <td>
                  	<select name="match[sanction]" id="match_sanction" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select>           
               </td>
              </tr>                  		 
	          <tr>
              	<th><?php echo $naming->getHeader("reference_link"); ?>:</th>    
               <td>
                    <input type="text" name="value[reference_link]" id="reference_link" value="" />      
               </td>
               <td>
                  	<select name="match[reference_link]" id="match_reference_link" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select>            
               </td>
              </tr>                                    
	          <tr>
              	<th><?php echo $naming->getHeader("assurance"); ?>:</th>    
              	<td>
                    <textarea name="value[assurance]" id="assurance" cols="30" rows='4'></textarea>
                  </td>
                  <td>
                  	<select name="match[assurance]" id="match_assurance" class="match">
              			<option value="any">Match any word</option>
              			<option value="all">Match all words</option>
              			<option value="excact">Match excact phrase</option>
              		</select> 		        
                  </td>
              </tr> 
	          <tr>
              	<th><?php echo $naming->getHeader("compliance_to"); ?>:</th>    
                    <td> 
                      <select name="value[compliance_to][]" id="compliance_to" multiple="multiple">
                        <option value="all" selected="selected">All</option>
                      </select>  
                    </td>
                    <td></td>
              </tr>    
	          <tr>
              	<th><?php echo $naming->getHeader("main_event"); ?>:</th>    
                    <td> 
                        <select name="value[main_event][]" id="main_event" multiple="multiple">
                         <option value="all" selected="selected">All</option>
                        </select>     
                    </td>
                    <td></td>
              </tr>  
	          <tr>
              	<th><?php echo $naming->getHeader("sub_event"); ?>:</th>    
                    <td> 
                        <select name="value[sub_event][]" id="sub_event" multiple="multiple">
                         <option value="all" selected="selected">All</option>
                        </select>      
                    </td>
                    <td></td>
              </tr>  
	          <tr>
              	<th><?php echo $naming->getHeader("guidance"); ?>:</th>    
                    <td> 
                         <textarea name="value[guidance]" id="guidance" cols="30" rows='4' class="checkcounter"></textarea>    
                    </td>
                    <td></td>
              </tr>   
              </table>          
      </td>
    </tr>
    <tr>
     <td class="noborder">
         <table width="100%">                    
	     <tr>
		     <td colspan="3" class="noborder"></td>
	     </tr>    
         <tr>
         	<th colspan="3">3.Choose the group and sort options </th>
      	</tr>
	     <tr>
		     <td colspan="3" class="noborder"></td>
	     </tr> 	
       <tr>
         <th>Group By:</th>
         <td>
         	<select name="group_by" id="group">
         		<option value="no_grouping">No Grouping</option>
         		<option value="compliance_frequency_">Frequency of compliance</option>
         		<option value="applicable_org_type_">Organisation Type</option>
         		<option value="responsible_">Responsible Department</option>
         		<option value="functional_service_">Functional Service</option>
         		<option value="responsibility_owner_">Responsibile Person</option>
         		<option value="main_event_">Main Event</option>
         	</select>
         </td>
          <td></td>
       </tr>
       <tr>
       	<th>Sort By:</th>
       	<td>
		     <ul id="sortable" style="list-style:none;">

		     </ul>  	  	
       	</td>
         <td></td>
       </tr>
	     <tr>
		     <td colspan="3" class="noborder"></td>
	     </tr>  
	     <!-- <tr>
	         <th colspan="3">4.Choose the document format of your report</th>
	     </tr>
	     <tr>
		     <td colspan="3" class="noborder"></td>
	     </tr>	  
      <tr>
         <td colspan="3"><input type="radio" name="document_format" id="on_screen" value="on_screen" checked="checked" />On Screen</td>
      </tr>
      <tr>
         <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em> plain </em> )</td>
      </tr>
      <tr>
         <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em>formated</em> )</td>
       </tr>
       <tr>
       	<td colspan="3"><input type="radio" name="document_format" id="save_pdf" value="save_pdf" />Save to Pdf</td>
       </tr>
      	<tr>
		     <td colspan="3" class="noborder"></td>
	     </tr> -->
      	<tr>
		     <td colspan="3" class="noborder"></td>
	     </tr> 
	     <tr>
		     <th colspan="3">4.Generate Report</th>
	     </tr>
	     <tr>
		     <td colspan="3" class="noborder"></td>
	     </tr>	
	     <tr>
		     <th>Report Title</th>
		     <td colspan="2"><input type="text" name="report_title" id="report_title" value="" /></td>
	     </tr>
	     <tr>
		     <th>Report Name:</th>
		     <td colspan="2"><input type="text" name="report_name" id="report_name" value="" /></td>
	     </tr>	
	     <tr>
		     <th>Description</th>
		     <td colspan="2"><textarea name="report_description" id="report_description" cols="30" rows="7"></textarea></td>
	     </tr>
	     <tr>
		     <th></th>
		     <td colspan="2">
			     <input type="submit" name="update" id="update" value="Update Report" />
			     <input type="hidden" name="quickid" id="quickid" value="<?php echo $_GET['id']; ?>" />
			     <input type="hidden" name="document_format" id="document_format" value="on_screen" />
		     </td>
	     </tr>	
     <!-- <td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">* Please note the following with regards to the formatted Microsoft Excel report:<br />
		     <ol>
			     <li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			     <li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			     <span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			     Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			     This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
			     It is safe to click on the "Yes" button to open the document.</li>
		     </ol>
	     </td> -->
     </tr>
     </table>          
     </td>
    </tr>
  <table>
</form>
