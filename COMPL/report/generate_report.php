<?php
echo "<link rel='stylesheet' href='../../assist.css' type='text/css' />";
include_once("../loader.php");
@session_start();
spl_autoload_register("Loader::autoload");	
$reportObj = new Report();
$postArr  =  $reportObj -> getAQuickReport($_GET['id']); 
$postArr['data']['report_title'] = $postArr['name'];
switch($_GET['reporttype'])
{
   case "legislation":
     $reportObj = new ClientLegislation();
     $reportObj -> generateReport($postArr['data'], new Report());     
   break;
   case "deliverable":
     $reportObj = new ClientDeliverable();
     $reportObj -> generateReport($postArr['data'], new Report());        
   break;
   case "action":
     $reportObj = new ClientAction();
     $reportObj -> generateReport($postArr['data'], new Report());        
   break;     
}

//$reportObj -> prepareReport( $postArr['data'] );	
?>
