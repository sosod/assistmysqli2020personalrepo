<?php
$scripts = array("process_report.js");
include("../header.php");
$naming = new LegislationNaming();
if(isset($_POST['update'])) {
	if( $_POST['report_name'] == ""){
		echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Please enter the name of the report</div>";
	} else {
		$reportObj = new Report();
		$reportObj -> updateQuickReport($_POST);
	}
} 
?>
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table width="70%" class="noborder"> 
	<tr>
		<th colspan="3">1.Select the legislation to be displayed on the report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<td><input type="checkbox" name="legislations[id]" id="id" class="legislation" /><?php echo $naming->setHeader("legislation_ref"); ?></td>
		<td><input type="checkbox" name="legislations[type]" id="type" class="legislation" /><?php echo $naming->setHeader("legislation_type"); ?></td>
		<td><input type="checkbox" name="legislations[organisation_capacity]" id="organisation_capacity" class="legislation" /><?php echo $naming->setHeader("organisation_capacity"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="legislations[name]" id="name" class="legislation" /><?php echo $naming->setHeader("legislation_name"); ?></td>
		<td><input type="checkbox" name="legislations[category]" id="category" class="legislation" /><?php echo $naming->setHeader("legislation_category"); ?></td>
		<td><input type="checkbox" name="legislations[legislation_date]" id="legislation_date" class="legislation" /><?php echo $naming->setHeader("legislation_date"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="legislations[reference]" id="reference" class="legislation" /><?php echo $naming->setHeader("legislation_reference"); ?></td>
		<td><input type="checkbox" name="legislations[organisation_size]" id="organisation_size" class="legislation" /><?php echo $naming->setHeader("organisation_size"); ?></td>
		<td><input type="checkbox" name="legislations[hyperlink_to_act]" id="hyperlink_to_act" class="legislation" /><?php echo $naming->setHeader("hyperlink_act"); ?></td>
	</tr>		
	<tr>	     
		<td><input type="checkbox" name="legislations[organisation_type]" id="organisation_type" class="legislation" /><?php echo $naming->setHeader("organisation_type"); ?></td>
		<td><input type="checkbox" name="legislations[action_progress]" id="action_progress" class="legislation" /><?php echo $naming->setHeader("action_progress"); ?></td>
		<td><input type="checkbox" name="legislations[status]" id="status" class="legislation" /><?php echo $naming->setHeader("legislation_status"); ?></td>		
	</tr>				
	<tr>	     
		<td><input type="checkbox" name="legislations[legislation_number]" id="legislation_number" class="legislation" /><?php echo $naming->setHeader("legislation_number"); ?></td>
		<td></td>
		<td></td>		
	</tr>			
	<tr>
		<td></td>
		<td><input type="button" name="check_legislation" id="check_legislation" value=" Check All" /></td>
		<td><input type="button" name="invert_legislation" id="invert_legislation" value=" Invert" /></td>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>																				
	<!-- <tr>
		<th colspan="3">2.Select the deliverable information to be displayed on the report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<td><input type="checkbox" name="deliverable[id]" id="id" class="deliverable" /><?php echo $naming->setHeader("ref"); ?></td>
		<td><input type="checkbox" name="deliverable[legislation_compliance_date]" id="legislation_compliance_date" class="deliverable" /><?php echo $naming->setHeader("legislation_compliance_date"); ?></td>
		<td><input type="checkbox" name="deliverable[sanction]" id="sanction" class="deliverable" /><?php echo $naming->setHeader("sanction"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="deliverable[short_description]" id="short_description" class="deliverable" /><?php echo $naming->setHeader("short_description_of_legislation"); ?></td>
		<td><input type="checkbox" name="deliverable[responsible]" id="responsible" class="deliverable" /><?php echo $naming->setHeader("responsible_department"); ?></td>
		<td><input type="checkbox" name="deliverable[reference_link]" id="reference_link" class="deliverable" /><?php echo $naming->setHeader("reference_link"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="deliverable[description]" id="description" class="deliverable" /><?php echo $naming->setHeader("description_of_legislation"); ?></td>
		<td><input type="checkbox" name="deliverable[functional_service]" id="functional_service" class="deliverable" /><?php echo $naming->setHeader("functional_service"); ?></td>
		<td><input type="checkbox" name="deliverable[assurance]" id="assurance" class="deliverable" /><?php echo $naming->setHeader("assurance"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="deliverable[legislation_section]" id="legislation_section" class="deliverable" /><?php echo $naming->setHeader("legislation_section"); ?></td>
		<td><input type="checkbox" name="deliverable[accountable_person]" id="accountable_person" class="deliverable" /><?php echo $naming->setHeader("accountable_person"); ?></td>
		<td><input type="checkbox" name="deliverable[compliance_to]" id="compliance_to" class="deliverable" /><?php echo $naming->setHeader("compliance_name_given_to"); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="deliverable[compliance_frequency]" id="compliance_frequency" class="deliverable" /><?php echo $naming->setHeader("frequency_of_compliance"); ?></td>
		<td><input type="checkbox" name="deliverable[responsibility_ower]" id="responsibility_ower" class="deliverable" /><?php echo $naming->setHeader("responsibility_ower"); ?></td>
		<td><input type="checkbox" name="deliverable[main_event]" id="main_event" class="deliverable" /><?php echo $naming->setHeader("main_event"); ?></td>
	</tr>	
	<tr>
		<td><input type="checkbox" name="deliverable[applicable_org_type]" id="applicable_org_type" class="deliverable" /><?php echo $naming->setHeader("applicable_organisation_type"); ?></td>
		<td><input type="checkbox" name="deliverable[legislation_deadline]" id="legislation_deadline" class="deliverable" /><?php echo $naming->setHeader("legislative_deadline_date"); ?></td>
		<td><input type="checkbox" name="deliverable[sub_event]" id="sub_event" class="deliverable" /><?php echo $naming->setHeader("sub_event"); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="deliverable[guidance]" id="guidance" class="deliverable" /><?php echo $naming->setHeader("guidance"); ?></td>
		<td><input type="checkbox" name="deliverable[action_progress]" id="action_progress" class="deliverable" /><?php echo $naming->setHeader("action_progress"); ?></td>
		<td><input type="checkbox" name="deliverable[status]" id="status" class="deliverable" /><?php echo $naming->setHeader("deliverable_status"); ?></td>
	</tr>		
	<tr>
		<td></td>
		<td><input type="button" name="check_deliverable" id="check_deliverable" value=" Check All" /></td>
		<td><input type="button" name="invert_deliverable" id="invert_deliverable" value=" Invert" /></td>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>																										
	<tr>
		<th colspan="3">3.Select the action information to be displayed on the report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<td><input type="checkbox" name="action[id]" id="action_ref" class="action" /><?php echo $naming->setHeader("action_ref"); ?></td>
		<td><input type="checkbox" name="action[deadline]" id="deadline" class="action" /><?php echo $naming->setHeader("action_deadline_date"); ?></td>
		<td><input type="checkbox" name="action[owner]" id="owner" class="action" /><?php echo $naming->setHeader("action_owner"); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="action[action]" id="action" class="action" /><?php echo $naming->setHeader("action_required"); ?></td>
		<td><input type="checkbox" name="action[action_deliverable]" id="action_deliverable" class="action" /><?php echo $naming->setHeader("action_deliverable"); ?></td>
		<td><input type="checkbox" name="action[reminder]" id="reminder" class="action" /><?php echo $naming->setHeader("action_reminder"); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="action[actionstatus]" id="actionstatus" class="action" /><?php echo $naming->setHeader("action_status"); ?></td>
		<td><input type="checkbox" name="action[progress]" id="progress" class="action" /><?php echo $naming->setHeader("action_progress"); ?></td>
		<td></td>
	</tr>					
	<tr>
		<td></td>
		<td><input type="button" name="check_action" id="check_action" value=" Check All" /></td>
		<td><input type="button" name="invert_action" id="invert_action" value=" Invert" /></td>
	</tr>-->
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th colspan="3">2. Select the filters you wish to apply</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>
	<tr>
    	<th><?php echo $naming->setHeader("legislation_name"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" id="_name" name="value[name]" class="checkcounter"></textarea>
    		<span class="textcounter" id="name_textcounter"></span>
    	</td>
    	<td>
    		<select name="match[name]" id="match_name" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="any">Match excact phrase</option>
    		</select>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $naming->setHeader("legislation_reference"); ?>:</th>    
    	<td><input type="text" name="value[reference]" id="_reference" value="" /></td>
    	<td>
    		<select name="match[reference]" id="match_reference" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="any">Match excact phrase</option>
    		</select>  	
    	</td>
    </tr>
	<tr>
    	<th><?php echo $naming->setHeader("legislation_number"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[legislation_number]" id="_legislation_number" class="checkcounter"></textarea>
    		<span class="textcounter" id="legislation_number_textcounter"></span>
        </td>
        <td>
			<select name="match[legislation_number]" id="match_legislation_number" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="any">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>        
	<tr>
    	<th><?php echo $naming->setHeader("legislation_type"); ?>:</th>    
    	<td>
        	<select name="value[type][]" id="_type" multiple="multiple">
        		<option value="all" selected="selected">All</option>
            </select>
        </td>
        <td></td>
    </tr>        
	<tr>
    	<th><?php echo $naming->setHeader("legislation_category"); ?>:</th>    
    	<td>
        	<select name="value[category][]" id="_category" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>    
	<tr>
    	<th><?php echo $naming->setHeader("organisation_size"); ?>:</th>    
    	<td>
        	<select name="value[organisation_size][]" id="_organisation_size" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>    
    <tr>
    	<th><?php echo $naming->setHeader("organisation_type"); ?>:</th>    
    	<td>
        	<select name="value[organisation_type][]" id="_organisation_type" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>   
	<tr>
    	<th><?php echo $naming->setHeader("organisation_capacity"); ?>:</th>    
    	<td>
        	<select name="value[organisation_capacity][]" id="_organisation_capacity" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>  		 
	<tr>
    	<th><?php echo $naming->setHeader("legislation_status"); ?>:</th>    
    	<td>
        	<select name="value[status][]" id="_status" multiple="multiple">
        	 	<option value="all" selected="selected">All</option>
            </select>        
        </td>
        <td></td>
    </tr>                                    
	<tr>
    	<th><?php echo $naming->setHeader("hyperlink_act"); ?>:</th>    
    	<td>
          <input type="text" name="value[hyperlink_to_act]" id="_hyperlink_to_act" value="" />
        </td>
        <td>
        	<select name="match[hyperlink_to_act]" id="match_hyperlink_to_act" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="any">Match excact phrase</option>
    		</select> 		        
        </td>
    </tr> 
	<tr>
    	<th><?php echo $naming->setHeader("legislation_date"); ?>:</th>    
    	<td> 
          From : <input type="text" name="value[legislation_date][from]" id="from_legislation_date" class="datepicker" value="" readonly="readonly"/>
        </td>
       <td> To	: <input type="text" name="value[legislation_date][to]" id="to_legislation_date" class="datepicker" value="" readonly="readonly"/>
       </td>
    </tr>     
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>    
    <tr>
    	<th colspan="3">3. Choose the group and sort options </th>
 	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 	
  <tr>
    <th>Group By:</th>
    <td>
    	<select name="group_by" id="group">
    		<option value="no_grouping">No Grouping</option>
    		<option value="type_">Legislation Type</option>
    		<option value="category_">Legislation Category</option>
    		<option value="organisation_size_">Organisation Size</option>
    		<option value="organisation_capacity_">Organisation Capacity</option>
    		<option value="organisation_type_">Organisation Type</option>
    	</select>
    </td>
     <td></td>
  </tr>
  <tr>
  	<th>Sort By:</th>
  	<td>
		<ul id="sortable" style="list-style:none;">
			<!-- 
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__id"></span><input type=hidden name=sort[] value="__id"><?php echo $naming ->setHeader("legislation_name"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__type"></span><input type=hidden name=sort[] value="__type"><?php echo $naming ->setHeader("legislation_type"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__legislation_number"></span><input type=hidden name=sort[] value="__legislation_number"><?php echo $naming ->setHeader("legislation_number"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__reference"></span><input type=hidden name=sort[] value="__reference"><?php echo $naming ->setHeader("legislation_reference"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__organisation_capacity"></span><input type=hidden name=sort[] value="__organisation_capacity"><?php echo $naming ->setHeader("organisation_capacity"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__category"></span><input type=hidden name=sort[] value="__category"><?php echo $naming ->setHeader("legislation_category"); ?></li>
		            <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__organisation_size"></span><input type=hidden name=sort[] value="__organisation_size"><?php echo $naming ->setHeader("organisation_size"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__organisation_type"></span><input type=hidden name=sort[] value="__organisation_type"><?php echo $naming ->setHeader("organisation_type"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__legislation_date"></span><input type=hidden name=sort[] value="__legislation_date"><?php echo $naming ->setHeader("legislation_date"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__hyperlink_to_act"></span><input type=hidden name=sort[] value="__hyperlink_to_act"><?php echo $naming ->setHeader("hyperlink_act"); ?></li>
					<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__action_progress"></span><input type=hidden name=sort[] value="__action_progress"><?php echo $naming ->setHeader("action_progress"); ?></li>
			 -->
		</ul>  	  	
  	</td>
    <td></td>
  </tr>
	<!-- <tr>
		<td colspan="3" class="noborder"></td>
	</tr>  
	<tr>
	    <th colspan="3">6.Choose the document format of your report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	  
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="on_screen" value="on_screen" checked="checked" />On Screen</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell(<i>plain</i>)</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell(<i>formated</i>)</td>
  </tr>
  <tr>
  	<td colspan="3"><input type="radio" name="document_format" id="save_pdf" value="save_pdf" />Save to Pdf</td>
  </tr> -->
 	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 
	<tr>
		<th colspan="3">4.Generate Report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th>Report Title</th>
		<td colspan="2"><input type="text" name="report_title" id="report_title" value="" /></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td colspan="2"><input type="text" name="report_name" id="report_name" value="" /></td>
	</tr>	
	<tr>
		<th>Description</th>
		<td colspan="2"><textarea name="report_description" id="report_description" cols="30" rows="7"></textarea></td>
	</tr>
	<tr>
		<th></th>
		<td colspan="2">
			<input type="submit" name="update" id="update" value="Update Report" />
			<input type="hidden" name="quickid" id="quickid" value="<?php echo $_GET['id']; ?>" />
			<input type="hidden" name="document_format" id="document_format" value="on_screen" />
		</td>
	</tr>		
<tr>
	<td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">* Please note the following with regards to the formatted Microsoft Excel report:<br />
		<ol>
			<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
			It is safe to click on the "Yes" button to open the document.</li>
		</ol>
	</td>
</tr>
</table>
</form>
