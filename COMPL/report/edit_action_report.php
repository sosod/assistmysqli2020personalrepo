<?php
$scripts = array("action_report.js");
include("../header.php");
$naming = new ActionNaming();
$naming -> getNaming();
if(isset($_POST['update'])) 
{
	if( $_POST['report_name'] == "")
	{
		echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Please enter the name of the report</div>";
	} else {
		$reportObj = new Report();
		$reportObj -> updateQuickReport($_POST);
	}
} 
?>
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table width="70%" class="noborder"> 
	<tr>
		<th colspan="3">1.Select the legislation to be displayed on the report</th>
	</tr>
	<tr>
		<td><input type="checkbox" name="actions[id]" id="action_id" class="action" /><?php echo $naming->getHeader("id", TRUE); ?></td>
		<td><input type="checkbox" name="actions[deadline]" id="action_deadline" class="action" /><?php echo $naming->getHeader("deadline", TRUE); ?></td>
		<td><input type="checkbox" name="actions[owner]" id="action_owner" class="action" /><?php echo $naming->getHeader("owner", TRUE); ?></td>
	</tr>		
	<tr>
		<td><input type="checkbox" name="actions[action]" id="action_action" class="action" /><?php echo $naming->getHeader("action", TRUE); ?></td>
		<td><input type="checkbox" name="actions[action_deliverable]" id="action_action_deliverable" class="action" /><?php echo $naming->getHeader("action_deliverable", TRUE); ?></td>
		<td><input type="checkbox" name="actions[reminder]" id="action_reminder" class="action" /><?php echo $naming->getHeader("reminder", TRUE); ?></td>
	</tr>
	<tr>
	  <td><input type="checkbox" name="actions[status]" id="action_status" class="action" /><?php echo $naming->getHeader("action_status", TRUE); ?></td>
	  <td><input type="checkbox" name="actions[progress]" id="action_progress" class="action" /><?php echo $naming->getHeader("action_progress", TRUE); ?></td>
	  <td></td>
	</tr>					
	<tr>
		<td></td>
		<td><input type="button" name="check_action" id="check_action" value=" Check All" /></td>
		<td><input type="button" name="invert_action" id="invert_action" value=" Invert" /></td>
	</tr>	
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>																				
	<tr>
		<th colspan="3">2.Select the filters you wish to apply</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>
	<tr>
    	<th><?php echo $naming->getHeader("action_required"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" id="action" name="value[action]"></textarea>
    	</td>
    	<td>
    		<select name="match[action]" id="match_action" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>
    	</td>
     </tr>
	<tr>
    	<th><?php echo $naming->getHeader("action_owner"); ?>:</th>    
    	<td>
        	<select name="value[owner][]" id="owner" multiple="multiple">
        		<option value="all" selected="selected">All</option>
            </select>
        </td>
        <td></td>
    </tr>    
	<tr>
    	<th><?php echo $naming->getHeader("action_deliverable"); ?>:</th>    
    	<td>
    	   <textarea rows="5" cols="30" id="action_deliverable" name="value[action_deliverable]"></textarea>
    	</td>
    	<td>
    		<select name="match[action_deliverable]" id="match_action_deliverable" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>  	
    	</td>
    </tr>
	<tr>
    	<th><?php echo $naming->getHeader("action_deadline_date"); ?>:</th>    
    	<td> 
          From : <input type="text" name="value[deadline_date][from]" id="from_deadline" class="datepicker" value="" readonly="readonly"/>
        </td>
       <td> To	: <input type="text" name="value[deadline_date][to]" id="to_deadline" class="datepicker" value="" readonly="readonly"/>
       </td>
    </tr>     
	<tr>
    	<th><?php echo $naming->getHeader("action_status"); ?>:</th>    
    	<td>
    		<select name="value[status][]" id="status" multiple="multiple">
    	       <option value="all" selected="selected">All</option>
    		</select>
    	</td>
    	<td>

    	</td>
     </tr>    
     <!-- <tr>
          <th>Recurring:</th>    
          <td><input type="checkbox" name="recurring" id="recurring" value="1" /></td>
          <td></td>          
      </tr>   -->   
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>    
    <tr>
    	<th colspan="3">3.Choose the group and sort options </th>
 	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 	
  <tr>
    <th>Group By:</th>
    <td>
    	<select name="group_by" id="group">
    		<option value="no_grouping">No Grouping</option>
    		<option value="owner_">Action Owner</option>
    	</select>
    </td>
     <td></td>
  </tr>
  <tr>
  	<th>Sort By:</th>
  	<td>
		<ul id="sortable" style="list-style:none;">
		</ul>  	  	
  	</td>
    <td></td>
  </tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>  
	<!-- 
	<tr>
	    <th colspan="3">4.Choose the document format of your report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	  
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="on_screen" value="on_screen" checked="checked" />On Screen</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em> plain </em> )</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em>formated</em> )</td>
  </tr>
  <tr>
  	<td colspan="3"><input type="radio" name="document_format" id="save_pdf" value="save_pdf" />Save to Pdf</td>
  </tr> -->
 	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 
	<tr>
		<th colspan="3">3.Generate Report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th>Report Title</th>
		<td colspan="2"><input type="text" name="report_title" id="report_title" value="" /></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td colspan="2"><input type="text" name="report_name" id="report_name" value="" /></td>
	</tr>	
	<tr>
		<th>Description</th>
		<td colspan="2"><textarea name="report_description" id="report_description" cols="30" rows="7"></textarea></td>
	</tr>
	<tr>
		<th></th>
		<td colspan="2">
		  <input type="submit" name="update" id="update" value="Update Report" />
		  <input type="hidden" name="quickid" id="quickid" value="<?php echo $_GET['id'] ?>" />
		  <input type="hidden" name="document_format" id="on_screen" value="on_screen" />
		</td>
	</tr>		
<tr>
	<!-- <td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">* Please note the following with regards to the formatted Microsoft Excel report:<br />
		<ol>
			<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
			It is safe to click on the "Yes" button to open the document.</li>
		</ol>
	</td> -->
</tr>
</table>
</form>
