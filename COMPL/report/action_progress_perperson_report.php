<?php
require_once '../../inc_session.php';
require_once '../../inc_db_conn.php'; 
require_once( "../../library/class/graph.php" );
include_once("../loader.php");
@session_start();
spl_autoload_register("Loader::autoload");
?>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
	<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
	<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script> 
	<script src="../js/action_progress_per_person.js" type="text/javascript"></script>        
	<link rel="stylesheet" href="/assist.css" type="text/css">
	<!-- <script type ="text/javascript" src="/assist.js"></script> -->
</head>
<?php
$complianceCategories = array();
$complianceCategories[] = array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "#000077");
$complianceCategories[] = array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "#009900");
$complianceCategories[] = array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "#FE9900");
$complianceCategories[] = array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "#FF0000");
$complianceCategories[] = array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "#999999");

$reportObj              = new ReportManager();
$page_title             = "Compliance Dashboard";
$financialyearObj       = new FinancialYear();
$year                   = $financialyearObj -> fetch($_GET['financialyear']);
$page_title             = "Action Progress Compliance Per Person";
$blurb                  = "for the (".$year['value'].")".$year['start_date']." ".$year['end_date']." financial year";
echo "<table class='noborder' width='100%'>";
 echo "<tr>";
   echo "<td class='noborder'>";
     //BAR GRAPH demo -> with ONLY BARS
     $actionProgress = $reportObj -> getActionProgressStatus($_GET['financialyear']);
     $users          = $actionProgress['users'];
     $progress       = $actionProgress['progressStatus'];  
     if(!empty($progress))
     {
          $myG4 = new Assist_Graph(4);
          $myG4->setFormatAsJava();
          //header was drawn with previous demo
          $myG4->setDrawHeader(false);
          //Set the bar graph settings
          $myG4->setGraphAsBar();
          $myG4->setBarGraphByPercent();		//OR	$myG4->setBarGraphByNumber()
          $myG4->setDisplayBarGrid(false);		//Display the background grid [default=false]
          //Set the page details
          $myG4->setPageTitle("Action Progress Compliance per Person");
          $myG4->setBlurb($blurb);
          //Set the graph categories
          $myG4->setGraphCategories($complianceCategories);
          //Set the main graph details
          $myG4->setHasMainGraph(false);
          //Set the sub graph details
          $myG4->setHasSubGraph(true);
          $myG4->setSubTitle("Users");
          $myG4->setSubList($users);
          $myG4->setSubCount($progress);
          //draw the graph
          $myG4->drawPage();
     } else {
          echo "There is no reporting data for the selected year";
     }
   echo "</td>";
 echo "</tr>";
echo "<table>";

/*
LEGISLATION COMPLIANCE GRAPH
*/


/** DEMO DATA **/


//PIE Graph: Full settings changed:
/** DEMOS **/
//Minimum setting changes:
?>
