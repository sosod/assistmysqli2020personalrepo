<?php
@session_start();
require_once '../../inc_session.php';
require_once '../../inc_db_conn.php'; 
require_once( "../../library/class/graph.php" );
include_once("../loader.php");
//@session_start();
spl_autoload_register("Loader::autoload");
$finyear = (isset($_GET['financialyear']) ? $_GET['financialyear'] : 1);
$complianceCategories = array();
$complianceCategories[] = array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "#000077");
$complianceCategories[] = array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "#009900");
$complianceCategories[] = array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "#FE9900");
$complianceCategories[] = array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "#FF0000");
$complianceCategories[] = array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "#999999");

$reportObj = new ReportManager();
$page_title = "Compliance Dashboard";
$financialyearObj = new FinancialYear();
$year = $financialyearObj -> fetch($finyear);
$page_title = "Action Progress Compliance Per Person";
$blurb = "for the (".$year['value'].")".$year['start_date']." ".$year['end_date']." financial year";

$actionProgress = $reportObj -> getActionProgressStatus($finyear);
$users = $actionProgress['users'];
$progress = $actionProgress['progressStatus'];


?>
