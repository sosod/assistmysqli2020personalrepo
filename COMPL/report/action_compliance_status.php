<?php
$scripts = array("action_compliance_status.js");
include("../header.php");
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<?php JSdisplayResultObj(""); ?>   
<script type="text/javascript">
	$(function(){

		$.getJSON("../class/request.php?action=ReportManager.getActionComplianceStatus", function( responseData ){	
				var chart;
				chart = new AmCharts.AmSerialChart();		
				chart.dataProvider = responseData;
				chart.categoryField = "legislation";
				chart.color = "#000000";
				chart.fontSize = 9;
				chart.marginTop = 25;
				chart.marginBottom = 40;
				chart.marginRight = 60;
				chart.angle = 45;
				chart.depth3D = 15;
				chart.plotAreaFillAlphas= 0.9;
				chart.plotAreaBorderAlpha = 0.2;
				
				var graph1 = new AmCharts.AmGraph();
				graph1.title = "Not Due";
				graph1.valueField = "not_due";
				graph1.type = "column";
				graph1.lineAlpha = 0;
				graph1.lineColor = "#151B8D";
				graph1.fillAlphas = 1;
				graph1.balloonText="Not Due : [[value]]";		
				chart.addGraph(graph1);
	
				var graph2 = new AmCharts.AmGraph();				
				graph2.title = "Over Due";
				graph2.valueField = "overdue";
				graph2.type = "column";
				graph2.lineAlpha = 0;
				graph2.lineColor = "#800517";
				graph2.fillAlphas = 1;
				graph2.balloonText="Over Due : [[value]]";
				chart.addGraph(graph2);
				
				var graph3 = new AmCharts.AmGraph();				
				graph3.title = "Completed";
				graph3.valueField = "completed";
				graph3.type = "column";
				graph3.lineAlpha = 0;
				graph3.lineColor = "#437C17";
				graph3.fillAlphas = 1;
				graph3.balloonText="Completed : [[value]]";
				chart.addGraph(graph3);
				
				var valAxis = new AmCharts.ValueAxis();
				valAxis.gridAlpha = 0.1;
				valAxis.axisAlpha = 0;
				valAxis.minimum = 0;
				valAxis.fontSize=8;
				chart.addValueAxis(valAxis);
				
				var catAxis = chart.categoryAxis;
				chart.categoryAxis.gridAlpha = 0.1;
				chart.categoryAxis.axisAlpha = 0;
				chart.categoryAxis.gridPosition = "start";			
				chart.write("chartdiv");

		});	
	})
</script>
<table class="noborder" width="100%">
	<tr>
		<td align="center" colspan="2" class="noborder">
	   		<center>
	   		<h1>
			  	<?php 
			  		echo $_SESSION['cn'];
			  	?>
	  		</h1>
	  		</center>		
		</td>
	</tr>
	<tr>
		<td colspan="2" class="noborder">
			<div id="chartdiv" style="width: 100%; height: 400px;"></div>
		</td>
	</tr>
	<tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
  	</td>
  </tr> 	
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
