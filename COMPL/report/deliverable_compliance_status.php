<?php
$scripts = array("deliverable_compliance_status.js");
//include("../header.php");
require_once '../../inc_session.php';
require_once '../../inc_db_conn.php'; 
include_once("../loader.php");
@session_start();
?>
<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<script src="../js/deliverable_compliance_status.js" type="text/javascript"></script>  

<?php //JSdisplayResultObj(""); ?>  
<table class="noborder" width="100%">
	<tr>
		<td align="center" colspan="2" class="noborder">
	   		<center>
	   		<h1>
			  	<?php 
			  		echo $_SESSION['cn'];
			  	?>
	  		</h1>
	  		</center>		
		</td>
	</tr>
	<tr>
		<td colspan="2" class="noborder">
			<div id="chartdiv" style="width: 100%; height: 400px;"></div>
		</td>
	</tr>
	<tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
  	</td>
  </tr> 	
	<tr>
		<td class="noborder"><?php //displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
