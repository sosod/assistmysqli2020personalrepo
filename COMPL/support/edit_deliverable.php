<?php 
$scripts = array("guidance.js", "deliverable.js");
include("../header.php");
JSdisplayResultObj("");
$deliverableFormObj = new DeliverableEditDisplay($_GET['id']);
$deliverableFormObj -> displayForm(new Form(), new DeliverableNaming(), array("adminEditable" => 1, 'section' => 'support'));
?>
<table class="noborder" width="80%">    
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>    
        <td class="noborder">
          <input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $_GET['id'] ?>" class="logid"  />
          <?php displayAuditLogLink("deliverable_edit", false); ?>
        </td>
    </tr>                                  
</table>
