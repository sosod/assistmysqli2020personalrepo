<?php
$scripts = array('jquery.ui.legislation.js');
include("../header.php");
?>
<script>
$(function(){

  $("#legislation_table").legislation({deactivateLegislation : true,
                                       page               : "support_edit",
                                       section            : "support",
                                       autoLoad           : true,
									   actionProgress		: false
                                      });
});	
</script>
<?php JSdisplayResultObj(""); ?>  
<div id="legislation_table"></div>
