<?php
/* requires $tdb as object of ASSIST_DB class => created in main/tables.php */ //error_reporting(-1);
require_once("class/model.php");
require_once("class/action.php");
require_once("class/deliverable.php");
require_once("class/legislation.php");

$sqlStatus = Action::getStatusSQLForWhere("RA")." AND ".Deliverable::getStatusSQLForWhere("D")." AND ".Legislation::getStatusSqlForWhere("L");

				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'deadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'action'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'owner'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'action_deliverable'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'status'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

$sql = "SELECT name, client_terminology FROM ".$dbref."_header_names WHERE type = 'action'";
$rs = $tdb->db_query($sql);
	while($row = mysql_fetch_array($rs)) 
	{
		if(isset($head[$row['name']])) 
		{
			$head[$row['name']]['text'] = $row['client_terminology'];
		}
	}
unset($rs);
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
    $sql   = "SELECT RA.id , RA.action, RA.action_deliverable, RA.deadline, RA.progress, RA.status AS statusid, 
              RAS.name as status, RAS.color as statuscolor, CONCAT(TK.tkname,' ',TK.tksurname) AS owner 
	          FROM ".$dbref."_action RA 
	          INNER JOIN ".$dbref."_deliverable D ON D.id = RA.deliverable_id 
	          INNER JOIN ".$dbref."_legislation L ON D.legislation = L.id 
	          LEFT JOIN ".$dbref."_action_status RAS ON RA.status = RAS.id 
	          LEFT JOIN assist_".$cmpcode."_timekeep TK ON TK.tkid = RA.owner 
	          WHERE RA.owner = '$tkid' 
	          AND STR_TO_DATE(RA.deadline,'%d-%M-%Y') <= ADDDATE(now(), ".$next_due.") 
	          AND RA.status <> 3 
			  AND ".$sqlStatus."
	          ORDER BY  ";
	switch($action_profile['field3']) 
	{
		case "dead_desc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') DESC "; break;
		default:
		case "dead_asc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') ASC "; break;
	}
	
$crs = $tdb -> db_get_num_rows($sql);
//var_dump($crs);
//echo $sql;
$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
//echo "<br /><br />".$sql;
    $tasks = $tdb->mysql_fetch_all_fld($sql,"id");

    //echo "Total tasks ---  ".count($tasks)."<br /><br />";
	foreach($tasks as $id => $task) 
	{
	    $deadline = "";
        $stattusid = $task['statusid'];		
	    if($stattusid !== "3") 
	    {
		    $deaddiff = $today - strtotime($task['deadline']);
		    $diffdays = floor($deaddiff/(3600*24));
		    if($deaddiff<0) 
		    {
			    $diffdays*=-1;
			    $days = $diffdays > 1 ? "days" : "day";
			    $deadline =  "<span class='soon'>Due in $diffdays $days</span>";
		    } elseif($deaddiff==0) {
			    $deadline =  "<span class='today'><b>Due today</b></span>";
		    } else {
			    $days = $diffdays > 1 ? "days" : "day";
			    $deadline = "<span class='overdue'><b>$diffdays $days</b> overdue</span>";
		    }			

		    $actions[$id]                       = array();
		    $actions[$id]['ref']                = $id;
		    $actions[$id]['deadline']           = $deadline;
		    $actions[$id]['action']             = utf8_decode($task['action']);
		    $actions[$id]['owner']              = $task['owner'];
		    $actions[$id]['action_deliverable'] = utf8_decode($task['action_deliverable']);
		    $actions[$id]['status']             = ($task['status'] == "" ? "New" : $task['status'])."<br />(".($task['progress'] == "" ? 0 : $task['progress'])."%)";
		    $actions[$id]['link'] = "manage/update_action.php?id=".$task['id']."|src=FRONTPAGE";		
	    }
	}

?>
