<?php
$scripts = array("");
include("../header.php");
$glossaryObj = new Glossary();
$glossaries  = $glossaryObj -> getGlossaries();
//Util::debug($glossaries);
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" id="table_glossary">
<tr>
	<th>Category</th>
	<th>Field Name</th>
	<th>Purpose</th>
	<th>Rules</th>		
</tr>
<?php
foreach($glossaries as $index => $glossary)
{
?>
  <tr>
	<td><?php echo ucwords($glossary['category']); ?></td>
	<td><?php echo ucwords($glossary['client_terminology']); ?></td>
	<td><?php echo ucwords($glossary['purpose']); ?></td>
	<td><?php echo ucwords($glossary['rules']); ?></td>		
  </tr>
<?php
}
?>
</table>
<?php displayGoBack("", ""); ?>
<input type="hidden" name="show" id="show" value="1" />
