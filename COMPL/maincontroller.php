<?php
class MainController extends BaseController{
	
	protected $httpRequest = array();
	
	protected $httpResponse = array();
	
	protected $requestUri;
	
	protected $folder;
	
	protected $action;
	
	protected $pageName;
	
	protected $page;
	
	protected $wasWhere  = array(); 
	
	protected $uriPieces = array();
	
	protected $queryString = array();
	
	public function __construct()
	{
		$_SESSION['modlocation'] = "COMPL";
		$this->requestUri        = $_SERVER['REQUEST_URI'];
		$this->queryString		 = $_SERVER['QUERY_STRING'];
		$this->getrulParts();
	}
	//split the url into parts
	function getrulParts()
	{
		$this->uriPieces = preg_split('[\\/]', $this->requestUri, -1, PREG_SPLIT_NO_EMPTY);
		$pieces    		 = count($this->uriPieces);
		$this->folder    = "";
		if(!empty($this->uriPieces))
		{
			foreach( $this->uriPieces as $key => $val)
			{
				if(strpos($val, ".") > 0)
				{
				     $this->page     = $val;
					$this->pageName = substr($val,0, strpos($val, "."));
					$this->folder 	 = $this->uriPieces[$pieces-2];	
				} else {
					$this->pageName = $this->uriPieces[$pieces-1]; 
				     $this->page     = $this->pageName;
					$this->folder 	= $this->uriPieces[$pieces-1];
				}
			}
		} 
	}
	
	function displayMenu()
	{
	     MenuManager::createMenu($this->folder, $this->pageName, 8);
	     echo "<div style='padding:2px;'></div>";
	     MenuManager::createMenu($this->folder, $this->pageName, 16);
	     echo "<div style='padding:2px;'></div>";
	     MenuManager::createMenu($this->folder, $this->pageName, 32);	     
		/*$ogname = $this->pageName;
		$menu   = new MenuManager();		
		//generate the upper manu of the page
		$menu -> getMenu(new UpperMenu(), $this->folder, $this->pageName );
	     
	     $menu->getMenu(new LowerMenu(), $this->folder, $this->folder."_".$this->pageName);	
	     //echo "Page name is ".$this->pageName." and folder name is ".$this->folder."<br /><br />";
      	if((strpos($this->page, "?") > 0))
      	{
      	     $pageStr = $this->folder." ".$this->pageName;
      	     $this->pageName = substr(basename($this->page), 0, (strpos($this->page, ".")) );
               if(strpos($this->page,"_") > 0)
               {
                    $this->pageName = substr($this->page, 0, strpos($this->page,"_"));
               }
      	     $menu->getMenu(new LowerCustomMenu(), $this->folder, $this->pageName );
   		}
   		*/
	}
	
	function wasWhere($page_title = "")
	{
		$arrayF = array_flip($this->uriPieces);
		$moloc = $arrayF[$_SESSION['modlocation']];		
		$this->uriPieces   = array_slice($this->uriPieces, $moloc+1);
		$this->loadHeaders($page_title);
	}
	
	function loadHeaders($page_title="")
	{
		$name = "";		
		$i = 0;
		$breadcrumbStr = "";
		$modlocation = $_SESSION['modlocation'];
		$modref      = $_SESSION['modref'];
		if(strlen($page_title)>0) {
			$breadcrumbStr = ucwords(str_replace("_"," ",$this->uriPieces[0]))." >> ".$page_title;
		} else {
			foreach( $this->uriPieces as $key => $value)
			{
				$i++;
				if($i > 1)
				{
					if(strpos($value, ".") > 0)
					{
						$value = substr($value, 0, strpos($value, "."));	
					}
					$_SESSION[$modlocation][$modref]['menu']['root'][$key] = $value;
					$breadcrumbStr .=" >> <a href='".$_SERVER['REQUEST_URI']."' class=breadcrumb>".ucwords(str_replace("_", " ",$value))."</a>";
				} else {
					$_SESSION[$modlocation][$modref]['menu']['root'] = $value;
					$breadcrumbStr .="  <a href='/".$_SESSION['modlocation']."/".$this->folder."/' class=breadcrumb>".ucwords(str_replace("_", " ",$value))."</a>";
				}	
			}
		}
		echo "<h1>".$breadcrumbStr."</h1>";
	}
	
	function loadJs( $scripts = null )
	{
		if(!empty($scripts)){
			if(is_array($scripts))
			{
				foreach ($scripts as $script)
				{
					if(file_exists("../js/".strtolower($script))){
						?>
							<script type="text/javascript" src="../js/<?php echo $script."?".time(); ?>"></script>
						<?php 
					}
				}		
			} else {
					if(file_exists("../js/".strtolower($scripts))){
					?>
						<script type="text/javascript" src="../js/<?php echo $script."?".time(); ?>"></script>
					<?php 
				}				
			}		
		}
	}
	
	function loadCss( $styles = null )
	{
		if(!empty($styles)){
			if(is_array($styles))
			{
				foreach ($styles as $style)
				{
					if(file_exists("css/".strtolower($style))){
						?>
							<link href="css/<?php echo $style."?".time(); ?>" rel="stylesheet" type="text/css"/>
						<?php 
					}
				}		
			} else {
					if(file_exists("css/".strtolower($styles))){
					?>
						<link href="css/<?php echo $styles."?".time(); ?>" rel="stylesheet" type="text/css"/>
					<?php 
				}				
			}		
		}
	}	
	
	function printRequestGuidance($txt = "") {
		if(strlen($txt)==0) { $txt = "Request Guidance"; }
		?>
			<!-- <a href="#" class="request_guidance">Request Guidance</a>&nbsp;&nbsp;<a href="#" class="request_guidance"><img src="/pics/tri_right.gif" style="vertical-align: middle; border: 0px; text-align:right;"></a> -->
		<?php 
	}	
	
} 
