$.widget("ui.legislation",{
	
	options			: {
		tableRef 			: "legislation_"+Math.floor(Math.random(57)*98),
		url 				: "../class/request.php?action=LegislationManager.importLegislation",
		start 				: 0,
		limit				: 10,
		headers				: [],
		total 				: 0,
		current				: 1,
		usedIds 			: [],
		page				: "",
		legoption   		: "",
		legStatus		 	: [],
		importDeliverable	: false,
		importLegislation	: false,
		createDeliverable	: false,
		addAction			: false,
		editLegislation		: false,
		editDeliverable		: false,
		updateLegislation	: false,
		updateDeliverable	: false,
		assuranceLegislation: false,
		assuranceDeliverable: false,
		confirmLegislation	: false,
		setupActivate		: false,
		setupLegislation	: false,
		setupEdit			: false,
		viewDetails			: false,
		approvalUpdate		: false,
		user				: "",
		company				: "",
		approval			: false,
		searchLegislation	: false,
		advancedSearchLegislation	: false
	} ,
	
	_init		: function()
	{
		this._getLegislation();	
	} ,
	
	_create		: function()
	{	
	} , 
	_getLegislation		: function()
	{
		var self = this;		
		$(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
				.css({
						position	: "absolute",
						"z-index"	: "9999",
						top			: "250px",
						left		: "350px",
						border		: "1px solid #FFFFF",
						padding		: "5px"
				})
			)			
		$.get(self.options.url , {
			start	: self.options.start,
			limit	: self.options.limit,
			which 	: self.options.legoption,
			page	: self.options.page
		} , function( legislationResponse ){
				
			console.log("Legislation Response");
			console.log( legislationResponse );
			
			$(self.element).html("");
			$(self.element).append($("<form />",{method:"post", id:"legislation_form", name:"legislation_form"})
						.append($("<table />",{id:self.options.tableRef}))
			)			
			
			self.options.usedIds   = legislationResponse.usedId;
			if( legislationResponse.total != undefined)
			{
				self.options.total     = legislationResponse.total;
			} else {
				self.options.total     = legislationResponse.data.total;
			}
			self.options.legStatus = legislationResponse.status; 
			self.options.user	   = legislationResponse.user;
			self.options.company   = legislationResponse.company;
			self._displayPaging( legislationResponse.data.total ,legislationResponse.data.columns );
			self._displayHeaders( legislationResponse.data.headers  );
			if( self.options.advancedSearchLegislation)
			{
				$("#tablelegislation").hide()
				$("#tr_searchagain").show();
			}
			if( $.isEmptyObject( legislationResponse.data.data ) ){
				if( self.options.searchLegislation || self.options.advancedSearchLegislation)
				{
					jsDisplayResult("ok", "ok", legislationResponse.text );	
				} else {
					$("#"+self.options.tableRef).append($("<tr />")
					  .append($("<td />",{colspan:legislationResponse.data.columns+2, html:"There are no legisltions"}))		
					);		
				}
			} else {
				if( self.options.searchLegislation || self.options.advancedSearchLegislation)
				{
					jsDisplayResult("ok", "ok", legislationResponse.text );	
				}
				//self._displayPaging( legislationResponse.data.total ,legislationResponse.data.columns );
				//self._displayHeaders( legislationResponse.data.headers  );
				self._display( legislationResponse.data.data ,legislationResponse.users, legislationResponse.authorizor , legislationResponse.admin );
			}
		} ,"json");
	 } , 
	 
	 _display: function( legislations, users, authorizor , adminstrator)
	 { 
		var self = this;
		$.each( legislations , function( index, leg){
	
			var tr = $("<tr />",{id:"tr_"+index});
			$.each( leg, function( key , val){
				if( $.inArray( key , self.options.headers) >= 0 ){
					//tr.append($("<td />",{html:value}))
				} else {			
					if( self.options.searchLegislation)
					{					
						if( val != null)
						{							
							if( val.indexOf(self.options.legoption) >= 0) {
								tr.append($("<td />")
									.append($("<span />",{html:val}).css({"background-color":"yellow"}))		
								)
							} else{
								tr.append($("<td />",{html:val}))
							}
						} else {
							tr.append($("<td />",{html:val}))							
						}						
					} else {
						tr.append($("<td />",{html:val}))
					}
				}
			});
			
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
			  .append($("<select />",{id:"legislationauthorisor_"+index, name:"legislationauthorisor_"+index, multiple:"multiple"}))
			)		
			
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
			  .append($("<select />",{id:"legislationadminstrator_"+index, name:"legislationadminstrator_"+index, multiple:"multiple"}))
			)			
			
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<select />",{id:"available_"+index, name:"available_"+index})
					.append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 2) == 0 ? "selected" : "")}))
					.append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 2) == 2 ? "selected" : "")}))
				)
			)		
			$("table").delegate("#available_"+index, "change", function(){
				
				if( $(this).val() == 1){
					$("select#import_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_del_"+index+" option[value='0']").attr("selected", "selected")
					$("select#create_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#import_actions_"+index+" option[value='0']").attr("selected", "selected")
					$("select#edit_actions_"+index+" option[value='0']").attr("selected", "selected")
					$("select#create_actions_"+index+" option[value='1']").attr("selected", "selected")
				} else {
					$("select#import_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#create_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#import_actions_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_actions_"+index+" option[value='1']").attr("selected", "selected")
					$("select#create_actions_"+index+" option[value='1']").attr("selected", "selected")											
				}
				return false;
			});	
			var selectYes;
			if( self.options.legStatus[index] == undefined)
			{
				var selectYes = true;
			} 	
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<table />")
				  .append($("<tr />")
					  .append($("<td />")
						  .append($("<select />",{id:"import_del_"+index, name:"import_del_"+index})
							  .append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 4) == 4 ? (selectYes ? "selected" : "") : "")}))
							  .append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 4) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
					  )	  
					  .append($("<td />")
						  .append($("<select />",{id:"edit_del_"+index, name:"edit_del_"+index})
							  .append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 8) == 8 ? (selectYes ? "selected" : "") : "")}))
							  .append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 8) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
						)	  
					  .append($("<td />")
						  .append($("<select />",{id:"create_del_"+index, name:"create_del_"+index})
							  .append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 16) == 16 ? (selectYes ? "selected" : "") : "")}))
							  .append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 16) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
						  )	  						  
				   )		
				)	
			)	
		
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<table />")
				  .append($("<tr />")
					  .append($("<td />")
						  .append($("<select />",{id:"import_actions_"+index, name:"import_actions_"+index})
								.append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 32) == 32 ? (selectYes ? "selected" : "") : "")}))
								.append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 32) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
					  )	  
					  .append($("<td />")
						  .append($("<select />",{id:"edit_actions_"+index, name:"edit_actions_"+index})
								.append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 64) == 64 ? (selectYes ? "selected" : "") : "")}))
								.append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 64) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
						  )	  
					  .append($("<td />")
						  .append($("<select />",{id:"create_actions_"+index, name:"create_actions_"+index})
								.append($("<option />",{value:"1", text:"Yes", selected:((self.options.legStatus[index] & 128) == 128 ? (selectYes ? "selected" : "") : "")}))
								.append($("<option />",{value:"0", text:"No", selected:((self.options.legStatus[index] & 128) == 0 ? (selectYes ? "" : "selected") : "")}))
							)							  
						  )	  						  
				   )		
				)	
			)				
			
			tr.append($("<td />").css({"display" : (self.options.viewDetails ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "view_details_"+index,
										name     : "view_details_"+index,
										disabled : ((self.options.legStatus[index] & 1024) == 1024 ? "" : "diasbled" ),
										value    : "View Details "
										}).addClass("imports"))
			)		

			tr.append($("<td />").css({"display" : (self.options.addAction ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "add_actions_"+index,
										name     : "add_actions_"+index,
										disabled : ((self.options.legStatus[index] & 128) == 128  ? "" : ((self.options.legStatus[index] & 2048) == 2048 ? "" : "diasbled") ) ,
										value    : "Add Actions"}).addClass("imports"))
			)		

			tr.append($("<td />").css({"display" : ( self.options.updateLegislation ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "update_legislation_"+index,
										name     : "update_legislation_"+index,
										disabled : ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ),
										value    : "Update Legislation"}).addClass("imports"))
				.append($("<input />",{
										type     : "button",
										id       : "update_deliverable_"+index,
										name     : "update_deliverable_"+index,
										disabled : ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ),
										value    : "Update Deliverable"}).addClass("imports"))				    						
			)	
			
			//tr.append($("<td />").css({"display" : ( self.options.updateDeliverable ? "table-cell" : "none") })
			//)		
													
			//edit legislation
			tr.append($("<td />").css({"display" : ( self.options.editLegislation ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "edit_legislation_"+index,
										name     : "edit_legislation_"+index,
										value    : "Edit Legislation"}).addClass("imports"))
				.append($("<input />",{
										type     : "button",
										id       : "edit_deliverable_"+index,
										name     : "edit_deliverable_"+index,
										disabled : ((self.options.legStatus[index] & 8) == 8 ? "" : "diasbled" ),
										value    : "Edit Deliverable "
										}).addClass("imports"))				    						
			)	
			
			tr.append($("<td />").css({"display" : ( self.options.assuranceLegislation ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "assurance_legislation_"+index,
										name     : "assurance_legislation_"+index,
										disabled : ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ),
										value    : "Assurance Legislation"}).addClass("imports"))
				.append($("<input />",{
										type     : "button",
										id       : "assurance_deliverable_"+index,
										name     : "assurance_deliverable_"+index,
										disabled : ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ),
										value    : "Assurance Deliverable"}).addClass("imports"))				    						
			)		
								
				//tr.append($("<td />").css({"display" : ( self.options.assuranceDeliverable ? "table-cell" : "none") })
				//)		
				
			tr.append($("<td />").css({"display" : ( self.options.approval ? "table-cell" : "none") })
					.append($("<input />",{
											type     : "button",
											id       : "approval_deliverable_"+index,
											name     : "approval_deliverable_"+index,
											disabled : ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ),
											value    : "Approval"}).addClass("imports"))
			)					
			
				//import
			tr.append($("<td />").css({"display" : (self.options.importLegislation  ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "import_"+index,
										name     : "import_"+index,
										disabled : ((self.options.legStatus[index] & 256) == 256 ? "diasbled" : "" ),
										value    : "Import"}).addClass("imports"))
			)
			
			tr.append($("<td />").css({"display" : (self.options.importDeliverable ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "import_deliverable_"+index,
										name     : "import_deliverable_"+index,
										disabled : ($.inArray( index , self.options.usedIds) > -1 ? "" : "disabled" ),
										value    : "Import Deliverable"}).addClass("imports"))
			)			
				
			tr.append($("<td />").css({"display" : (self.options.createDeliverable ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "create_deliverable_"+index,
										name     : "create_deliverable_"+index,
										value    : "Add Deliverable"}).addClass("imports"))
		   )						
			
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "activate_"+index,
										name     : "activate_"+index,
										disabled : ($.inArray( index , self.options.usedIds) > -1 ? ((self.options.legStatus[index] & 1) == 1 ? "diasbled" : "" ) : "" ),
										value    : "Activate "}).addClass("imports").css({"display" : (self.options.setupActivate ? "table-cell" : "none") }))
										
				.append($("<input />",{
										type     : "button",
										id       : "inactivate_"+index,
										name     : "inactivate_"+index,
										disabled : ($.inArray( index , self.options.usedIds) > -1 ? ((self.options.legStatus[index] & 1) == 1 ? "" : "diasbled" ) : "diasbled" ),
										value    : "Deactivate"}).addClass("imports").css({"display" : (self.options.setupActivate ? "table-cell" : "none") }))
										
				.append($("<input />",{
										type     : "button",
										id       : "save_changes_"+index,
										name     : "save_changes_"+index,
										value    : "Save Changes"}).addClass("imports").css({"display" : (self.options.setupEdit ? "table-cell" : "none") }))										
			)			
					

			tr.append($("<td />").css({"display" : (self.options.approvalUpdate ? "table-cell" : "none") })
				.append($("<input />",{
										type     : "button",
										id       : "allow_update_"+index,
										name     : "allow_update_"+index,
										value    : "Allow Update "}).addClass("imports"))
			)				

	
			$("#allow_update_"+index).live("click", function(){
					
					$("<div />",{id:"allowupdate_"+index, html:"I, <b>username surname</b> of <b>company name</b> hereby:"})
					 .append($("<ul />")
					   .append($("<li />",{html:"declare that i have the required authority to approve the update this leegislation"}))
					   .append($("<li />",{html:"declare that i have reviewed and am hereby authorising all the edits (including deletions) made to the deliverables and actions associated to this <b> name "+legislation.legislation_name+" "+legislation.legislation_reference+"</b>, legislation"}))
					   .append($("<li />",{html:"confirm that this update will overwrite the data that is currently in the Legislation, Deliverable and Action fields"}))				   
					 )
					 .dialog({
						 		autoOpen	: true,
						 		modal 		: true,
						 		title 		: "Update Approval",
						 		buttons		: {
						 						"I Agree"	: function()
						 						{
						  							$.post("../class/request.php?action=LegislationManager.updateEditedLegislation",{ data : { id : index }}, function( response ){
														if( response.error ){
															 jsDisplayResult("error", "error",  response.text );
														} else {
															 jsDisplayResult("ok", "ok", response.text );
														}					 								
						 							},"json");
						 							$("#allowupdate_"+index).dialog("destroy");
						 							$("#allowupdate_"+index).remove();
						 						} , 
						 						"Decline"	: function()
						 						{
						 							$("#allowupdate_"+index).dialog("destroy");
						 							$("#allowupdate_"+index).remove();
						 						}
					 						  }
					 });
					 
					
					
					
					return false;
				});
									
				$("#activate_"+index).live("click", function(){
				
					//$("<div />",{id:"disclaimer_"+index, html:"You are about to activate the legisaltion with the following details <br />  Ref #:"+legislation.ref+"<br />Legislation Name:"+legislation.legislation_name+"<br /> blah  blah blah blah blah "})
					$("<div />",{id:"disclaimer_"+index})
					.append($("<p />").addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px"})
					  	.append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))	
					  	.append($("<span />")
					  	  .append($("<span />",{html:"I,"+self.options.user+", of "+self.options.company+" hereby :"}).css({"margin-left":"8px"}))
						  .append($("<ul />").css({"margin-left":"10px"})
						    .append($("<li />",{html:" declare that I have the required authority to activate this legislation"}))
						    .append($("<li />",{html:" confirm my understanding that this legislation, "+leg.legislation_name+"  "+leg.legislation_reference+", contains only the required deliverables and actions (where applicable) relating to the matters that are required to be addressed in this legislation;"}))
						    .append($("<li />",{html:" acknowledge that this legislation, its deliverable(s) and action(s) are published by Ignite Reporting Systems (Pty) Ltd on behalf of, "+leg.responsible_business_patner+" and that the information is not intended to constitute advice on the legislation covered as every situation depends on its own facts and circumstances for which professional advice should be sought from "+legislation.responsible_business_patner+""}))
						    .append($("<li />",{html:" agree that the views expressed (published) are those of the business partner, "+leg.responsible_business_patner+"; and"}))
						    .append($("<li />",{html:" acknowledge that although reasonable care has been taken to ensure the accuracy of this work, Ignite Reporting Systems (Pty) Ltd has expressly disclaimed all and any liability to any person relating to anything done or omitted to be done or to the consequences thereof in reliance upon this work"}))
						   )					  	 		
					  	)
					)				
					 .dialog({
						 		autoOpen	: true,
						 		modal		: true,
						 		title		: "Activating a legislation",
						 		width		: "500px",
						 		buttons		: {
						 						"Confirm Activation"	: function()
						 						{
														jsDisplayResult("info", "info", "Activating legislation . . . .<img src='../images/loaderA32.gif' />" );
														var data 		  = {};		 
														data.available     = $("#available_"+index+" :selected").val()
														data.import_del    = $("#import_del_"+index+" :selected").val()
														data.edit_del      = $("#edit_del_"+index+" :selected").val()
														data.create_del    = $("#create_del_"+index+" :selected").val()
														data.import_action = $("#import_actions_"+index+" :selected").val()
														data.edit_action   = $("#edit_actions_"+index+" :selected").val()
														data.create_action = $("#create_actions_"+index+" :selected").val();
														data.legislation_id= index;
														data.authoriser	   = $("#legislationauthorisor_"+index).val();
														data.admin  	   = $("#legislationadminstrator_"+index).val();
														$.post("../class/request.php?action=LegislationManager.activateSave",  data , function( response ){
															if( response.error ){
																 jsDisplayResult("error", "error",  response.text );
															} else {
																 $("#disclaimer_"+index).dialog("destroy");
																 $("#disclaimer_"+index).remove();
																 jsDisplayResult("ok", "ok", response.text );
																 $("#activate_"+index).attr("disabled", "disabled");
																 $("#inactivate_"+index).attr("disabled", "");
															}
															
														},"json");
						 						} , 
						 						"Cancel Activation"		 : function()
						 						{
						 							$(this).dialog("destroy");
						 						}
					 						 }
					 });

					return false;
				});			
				
				
				
				$("#save_changes_"+index).live("click", function(){
					var data 		  = {};		 
					data.available     = $("#available_"+index+" :selected").val()
					data.import_del    = $("#import_del_"+index+" :selected").val()
					data.edit_del      = $("#edit_del_"+index+" :selected").val()
					data.create_del    = $("#create_del_"+index+" :selected").val()
					data.import_action = $("#import_actions_"+index+" :selected").val()
					data.edit_action   = $("#edit_actions_"+index+" :selected").val()
					data.create_action = $("#create_actions_"+index+" :selected").val();
					data.legislation_id= index;
					data.authoriser	   = $("#legislationauthorisor_"+index).val();
					data.admin         = $("#legislationadminstrator_"+index).val();
					
					jsDisplayResult("info", "info", "Activating legislation . . . .<img src='../images/loaderA32.gif' />" );					
					$.post("../class/request.php?action=LegislationManager.editSetupLegislation",  data , function( response ){
						if( response.error ){
							 jsDisplayResult("error", "error",  response.text );
						} else {							 
							 jsDisplayResult("ok", "ok", response.text );
							 $("#activate_"+index).attr("disabled", "disabled");
							 $("#inactivate_"+index).attr("disabled", "");
						}
					},"json");
					
					return false;
				});
										
				$("#inactivate_"+index).live("click", function(){
					
					jsDisplayResult("info", "info", "Inactivating legislation . . . .<img src='../images/loaderA32.gif' />" );
					var data 		    = {};		 
					data.legislation_id = index;
					
					$.post("../class/request.php?action=LegislationManager.inactivateSave",  data , function( response ){
						if( response.error ){
							 jsDisplayResult("error", "error",  response.text );
						} else {
							 jsDisplayResult("ok", "ok", response.text );
							 $("#activate_"+index).attr("disabled", "");
							 $("#inactivate_"+index).attr("disabled", "disabled");
						}
						
					},"json");
					
					return false;
				});	
				
				$("#import_"+index).live("click", function(){
					
					jsDisplayResult("info", "info", "Importing a legislation . . . .<img src='../images/loaderA32.gif' />" );				
					$.post("../class/request.php?action=LegislationManager.importSave",
					{ legislation_id : index , financial_year : $("#financial_year :selected").val() } , function( response ){
						if( response.error ){
							 jsDisplayResult("error", "error",  response.text );
						} else {
							 jsDisplayResult("ok", "ok", response.text );
							 $("#import_"+index).attr("disabled", "disabled")
							 $("#status_"+index).html("<b>Active</b>")
						}
						
					},"json");
					
					return false;
				});
				
				if( self.options.confirmLegislation == true){
					
					$("#legislation_list").append($("<option />",{text:leg.legislation_name, value:leg.ref}))
					$("#legislation_list").live("change", function(){
						var legid = $(this).val()
						$(self.element).html("");
						$.getScript("../js/jquery.ui.deliverableactions.js", function(){
							$(self.element).deliverableactions({ legislationId : legid, page:"manage", confirmation : true, details : false, editDeliverable:true})					
							$("#table_").append($("<tr />")
							   .append($("<td >").addClass("noborder")
								 .append($("<input />",{type:"button", name:"confirm_activation_"+index, id:"confirm_activation_"+index, value:"Confirm  and Request Activation"}))	   
							   )		
							)
						})
											
					});
				}
							
				
				$("#import_deliverable_"+index).live("click", function(){
					document.location.href = "import_a_deliverable.php?id="+index;
					return false;
				});
				
				$("#assurance_deliverable_"+index).live("click", function(){
					document.location.href = "assurance_deliverables.php?id="+index;
					return false;
				});			

				$("#assurance_legislation_"+index).live("click", function(){
					document.location.href = "assurance_legislation.php?id="+index;
					return false;
				});
				
				$("#create_deliverable_"+index).live("click", function(){
					document.location.href = "add_deliverable.php?id="+index;
					return false;
				});		
				
				$("#add_actions_"+index).live("click", function(){
					document.location.href = "add_actions.php?id="+index;
					return false;
				});					

				$("#create_deliverable_"+index).live("click", function(){
					document.location.href = "add_deliverable.php?id="+index;
					return false;
				});					

				$("#update_deliverable_"+index).live("click", function(){
					document.location.href = "update_deliverables.php?id="+index;
					return false;
				});	
				
				$("#update_legislation_"+index).live("click", function(){
					document.location.href = "update_legislation.php?id="+index;
					return false;
				});								
				
				$("#edit_legislation_"+index).live("click", function(){
					document.location.href = "edit_legislation.php?id="+index;
					return false;
				});
				
				$("#edit_deliverable_"+index).live("click", function(){
					document.location.href = "edit_deliverables.php?id="+index;
					return false;
				});

				$("#view_details_"+index).live("click", function(){
					document.location.href = "view_details.php?id="+index;
					return false;
				});			
				
				$("#approval_deliverable_"+index).live("click", function(){
					document.location.href = "approval_deliverable.php?id="+index;
					return false;
				});						
			
				$("#"+self.options.tableRef).append( tr );
	
		});		
		$.each( legislations , function( index, legislation ){
			$.each( users, function( i, val){
				$("#legislationauthorisor_"+index).append($("<option />",{text:val.name, value:val.id}))
				$("#legislationadminstrator_"+index).append($("<option />",{text:val.name, value:val.id}))					
				if( authorizor[index] != undefined)
				{
					$.each( authorizor[index], function( aIndex, aVal){
						 $("#legislationauthorisor_"+index+" option[value='"+aVal+"']").attr("selected", "selected");	
					})						
				}
				if( adminstrator[index] != undefined)
				{
					$.each( adminstrator[index], function( aIndex, aVal){
						 $("#legislationadminstrator_"+index+" option[value='"+aVal+"']").attr("selected", "selected");	
					})						
				}					
			});	
		});		

	 } , 
	 
	_displayHeaders		: function( headers )
	{
		var self = this;
		var tr 	 = $("<tr />")
		$.each( headers, function( index, head){
			if( $.inArray( index , self.options.headers) >= 0){
				//tr.append($("<th />",{html:head}))
			} else {
				tr.append($("<th />",{html:head}))
			}						
		});	
		//$("#"+self.options.tableRef).append($("<tr />")
		tr.append($("<th />",{html:"Legislation Authorisor"}).css({"display" : ( self.options.setupLegislation  ? "table-cell" : "none") }))
		tr.append($("<th />",{html:"Legislation Administrator"}).css({"display" : ( self.options.setupLegislation  ? "table-cell" : "none") }))
		tr.append($("<th />",{html:"Must this legislation made available?"}).css({"display" : ( self.options.setupLegislation  ? "table-cell" : "none") }))
		tr.append($("<th />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
			  .append($("<table />",{width:"100%"}).addClass("noborder")
				  .append($("<tr />")
				 	 .append($("<th />",{html:"Deliverable",colspan:"3",align:"center"}).addClass("noborder"))
				  )
				  .append($("<tr />")
					.append($("<th />",{html:"Import"}).addClass("noborder"))
					.append($("<th />",{html:"Edit"}).addClass("noborder"))
					.append($("<th />",{html:"Create"}).addClass("noborder"))
				  )						
				)
			)
		tr.append($("<th />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<table />",{width:"100%"}).addClass("noborder")
				  .append($("<tr />")
					  .append($("<th />",{html:"Action",colspan:"3", align:"center"}).addClass("noborder"))
				  )
				  .append($("<tr />")
					.append($("<th />",{html:"Import"}).addClass("noborder"))
					.append($("<th />",{html:"Edit"}).addClass("noborder"))
					.append($("<th />",{html:"Create"}).addClass("noborder"))
				  )						
				)
			)	
			
		  .append($("<th />",{html:"&nbsp;"}).css({"display" : ( self.options.page == "import_legislation" ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;Status&nbsp;&nbsp;", colspan:"1"}).css({"display" : (self.options.setupLegislation ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.viewDetails ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.createDeliverable ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.importLegislation ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.addAction ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.updateLegislation ? "table-cell" : "none") }))
		  //.append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.updateDeliverable ? "table-cell" : "none") }))
		  //.append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editLegislation ? "table-cell" : "none") }))
		  //.append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.assuranceDeliverable ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.assuranceLegislation ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approval ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approvalUpdate ? "table-cell" : "none") }))
		  //)		
		 
  		$("#"+self.options.tableRef).append( tr )
		
	} ,
	 
	_displayPaging 		: function( total , columns ) {
			var self = this;
			
			var pages;
			if( self.options.total%self.options.limit > 0){
				pages   = Math.ceil(self.options.total/self.options.limit); 				
			} else {
				pages   = Math.floor(self.options.total/self.options.limit); 				
			}
			if( self.options.setupLegislation )
			{
				columns = 8;
			}
			$("#"+self.options.tableRef)
			  .append($("<tr />")
				.append($("<td />",{colspan:columns})
				   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
				   .append("&nbsp;")
				   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
				   .append("&nbsp;&nbsp;&nbsp;")
				   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
				   .append("&nbsp;&nbsp;&nbsp;")
				   .append($("<input />",{type:"button", name:"next", id:"next", value:" > ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))
				   .append("&nbsp;")
				   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))			   
			   )			
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.page == "import_legislation" ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;",colspan:"1"}).css({"display" : (self.options.setupLegislation ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.viewDetails ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.createDeliverable ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.importLegislation ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.addAction ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.updateLegislation ? "table-cell" : "none") }))
			  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.updateDeliverable ? "table-cell" : "none") }))
			  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editLegislation ? "table-cell" : "none") }))
			  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.assuranceDeliverable ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.assuranceLegislation ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approval ? "table-cell" : "none") }))
			  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approvalUpdate ? "table-cell" : "none") }))
			  )

			$("#next").bind("click", function(evt){
				self._getNext( self );
			});
			$("#last").bind("click",  function(evt){
				self._getLast( self );
			});
			$("#previous").bind("click",  function(evt){
				self._getPrevious( self );
			});
			$("#first").bind("click",  function(evt){
				self._getFirst( self );
			});			
		} ,		
		
		_getNext  			: function( rk ) {
			rk.options.current = rk.options.current+1;
			rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
			this._getLegislation();
		},	
		_getLast  			: function( rk ) {
			rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
			rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
			this._getLegislation();
		},	
		_getPrevious    	: function( rk ) {
			rk.options.current  = parseFloat( rk.options.current ) - 1;
			rk.options.start 	= (rk.options.current-1)*rk.options.limit;
			this._getLegislation();			
		},	
		_getFirst  			: function( rk ) {
			rk.options.current  = 1;
			rk.options.start 	= 0;
			this._getLegislation();				
		}  ,
		
		_displayUsers		: function(){
			$.each( users, function( key , val){
				$("#legislationauthorisor_"+index).append($("<option />",{text:val.name, value:val.id}))
			});
		} 		
});