// JavaScript Document
$(function(){
	   
	 OrganisationType.get()  
	   
	$("#add").click(function(){
		OrganisationType.save();
		return false;
	});
	
	$("#edit").click(function(){
		OrganisationType.edit();	
		return false;
	});
	
	$("#update").click(function(){
		OrganisationType.update();	
		return false;
	});	
});

var OrganisationType 	= {
	
	get 		: function()
	{
		$.post("../class/request.php?action=OrganisationType.getAll", function( response ){
			$.each( response, function( index, orgtype){
				OrganisationType.display( orgtype )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the act name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientDeliverableOrganisationType.saveOrganisationType", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					data.id  	= response.id; 	
					data.status = 1; 	
					OrganisationType.display( data )																   
					OrganisationType.empty( data );					
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#organisation_type_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		data.id			 = $("#orgtypeid").val()
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the act name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientDeliverableOrganisationType.updateOrganisationType", 
			 {
			   data : data 
			 },function(response ){		
				if(response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					if(response.updated)
					{
					   jsDisplayResult("ok", "ok", response.text);
					} else {
					   jsDisplayResult("info", "info", response.text);
					}
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var  data  = {};
		data.status	= $("#status :selected").val()
		data.id 	= $("#orgtypeid").val()
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientDeliverableOrganisationType.updateOrganisationType", {  data : data }, function( response ) { 	
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					if(response.updated)
					{
					   jsDisplayResult("ok", "ok", response.text);
					} else {
					   jsDisplayResult("info", "info", response.text);
					}
				}
		}, "json");
	} , 
	
	display		: function( orgtype )
	{
		$("#organisation_type_table")
		 .append($("<tr />",{id:"tr_"+orgtype.id})
		   .append($("<td />",{html:orgtype.id}))
		   .append($("<td />",{html:orgtype.name}))
		   .append($("<td />",{html:orgtype.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "edit_"+orgtype.id,
				  					id			: (orgtype.imported == true ? "edit" : "edit_"+orgtype.id),
				  					value		: "Edit"
				  				}))
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "del_"+orgtype.id,
				  					id		 	: (orgtype.imported == true ? "del" : "del_"+orgtype.id),
				  					value		: "Del"
				  					}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((orgtype.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "change_"+orgtype.id,
				  					id			: (orgtype.imported == true ? "change" : "change_"+orgtype.id),
				  					value		: "Change Status"
				  					}))			  					
		    )		   
		 )
		 if(orgtype.imported == true)
		 {
		     $("input[name='change_"+orgtype.id+"']").attr('disabled', 'disabled')
		     $("input[name='edit_"+orgtype.id+"']").attr('disabled', 'disabled')
		     $("input[name='del_"+orgtype.id+"']").attr('disabled', 'disabled')
		 }	 
		 $("#edit_"+orgtype.id).live("click", function(){
			document.location.href = "edit_organisation_types.php?id="+orgtype.id;								   
			return false;								   
		  });
		 
		 $("#del_"+orgtype.id).live("click", function(){
			var data 	 = {};
			data.id 	 = orgtype.id
			data.status  = 2;
			if( confirm("Are you sure you want to delete this act")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ClientDeliverableOrganisationType.updateOrganisationType", 
				     { data : data }, function( response ){		
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
						$("#tr_"+orgtype.id).fadeOut();
					}
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+orgtype.id).live("click", function(){
			document.location.href = "change_organisation_types.php?id="+orgtype.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 

}
