$(function(){
	
	$(".request_guidance").click(function(){
		
		var legislationId = 0;
		
		$("input[name=accept]").each(function(){
				if( $(this).is(":checked") ){
					legislationId = $(this).val() 
				} 
		})

		if( legislationId == 0  ){
			alert("Please select the legislation")			
		} else {
		
				$.post("../class/request.php?action=LegislationManager.guidanceData", { legislationId : legislationId }, function( response ){
		
		
					$("<div />")
					.append($("<table />")
					   .append($("<tr />")
						 .append($("<th />",{html:"Request Submitted by:"}))
						 .append($("<td />",{html:response.request_by}))
					   )		
					   .append($("<tr />")
						 .append($("<th />",{html:"Request Company:"}))
						 .append($("<td />",{html:response.request}))
					   )		
					   .append($("<tr />")
						 .append($("<th />",{html:"Business Patner:"}))
						 .append($("<td />",{html:response.bus_partner}))
					   )						   
					   .append($("<tr />")
						 .append($("<th />",{html:"Legislation Owner:"}))
						 .append($("<td />",{html:response.leg_owner}))
					  )	
					   .append($("<tr />")
						 .append($("<th />",{html:"Legislation:"}))
						 .append($("<td />",{html:response.legislation}))
						)				  
					   .append($("<tr />")
						 .append($("<th />",{html:"Contact Number:"}))
						 .append($("<td />")
						    .append($("<input />",{type:"text", name:"contact", id:"contact"}))		 
						 )
					   )
					   .append($("<tr />")
						 .append($("<th />",{html:"Message:"}))
						 .append($("<td />")
						   .append($("<textarea />",{cols:"30", rows:"7", id:"message", name:"message"}))		 
						 )
					   )		   
					)
					.dialog({
							autoOpen	: true,
							modal		: true,
							width		: "400",
							title		: "Request Legal Guidance",
							buttons		: {
											"Accept" : function()
											{
												jsDisplayResult("info", "info", "Sending email   . . . <img src='../images/loaderA32.gif' />");
												$.post("../class/request.php?action=LegislationManager.requestGuidance", { legislationId : legislationId } , function( response ){
													if( response.error ){
														jsDisplayResult("error", "error", response.text);		
													} else {					
														jsDisplayResult("ok", "ok", response.text);
													}
												},"json");
												$(this).dialog("destroy")
											} , 
											
											"Decline" : function()
											{
												$(this).dialog("destroy")
											}
							
										}			
					})
					
				},"json");
		
		

		}
		
	});	
	
});