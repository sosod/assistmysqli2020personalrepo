var Status = {
  
  addStatus     : function(statusContext)
  {
        var self = this;         
        if($("#"+statusContext+"_status").length > 0)
        {
          $("#"+statusContext+"_status").remove();
        }
        var self = this;
        $("<div />",{id:statusContext+"_status"}).append($("<table />", {width:"100%"})
          .append($("<tr />")
            .append($("<th />",{html:"Status Name :"}))
            .append($("<td />")
              .append($("<input />",{id:"name", name:"name", value:""}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Client Terminology :"}))
            .append($("<td />")
              .append($("<input />",{id:"client_terminology", name:"client_terminology", value:""}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Status Color :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", name:"color", id:"color", readonly:"readonly"}))
            )
          )    
          .append($("<tr />")
            .append($("<td />",{colspan:2})
              .append($("<span />",{id:"message"}).css({"padding":"5px;"}))
            )
          )                                      
        ).dialog({
            autoOpen          : true,
            position          : "top",
            modal             : true,
            width             : "auto",
            title             : "Add New "+statusContext+" Status",
            buttons           : {
                                   "Save"         : function()
                                   {
                                      $("#message").addClass("ui-state-info").html("saving . . .<img src='resources/images/loaderA32.gif' />" );
                                      if($("#name").val() == "")
                                      {
                                        $("#message").addClass("ui-state-error").html("Status Name is required ");
                                      } else if($("#client_terminology").val() == ""){
                                        $("#message").addClass("ui-state-error").html("Client terminology is required ");
                                      } else {
                                        self.save(statusContext);
                                      }
                                   } ,
                                   
                                   "Cancel"       : function()
                                   {
                                      $("#"+statusContext+"_status").dialog("destroy");
                                      $("#"+statusContext+"_status").remove();
                                   }
            } ,
            close             : function(event, ui)
            {
              $("#"+statusContext+"_status").dialog("destroy");
              $("#"+statusContext+"_status").remove();            
            } ,
            open              : function(event, ui)
            {
			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable 
			 
			 $(this).css("height", "auto");  
            }
        });  
        
  } , 
  
  save              : function(statusContext)
  {
     var self = this;
     $.post("../class/request.php?action="+statusContext+".save", {
          data : { 
                 name      : $("#name").val(),
                 client_terminology  : $("#client_terminology").val(),
                 color               : $("#color").val()
          }
     },function(response) {
        if(response < 0)
        {
          $("#message").addClass("ui-state-error").html("Error saving the status . . please try again") 
        } else {
          jsDisplayResult("ok", "ok", "Status successfully saved . . . " );
          $("#"+statusContext+"_status").dialog("destroy");
          $("#"+statusContext+"_status").remove();          
          self.get(statusContext);
        }
     }, "json");
  
  } ,
     get            : function(statusContext)
     {
       var self = this;
       $(".status").remove();
       $.getJSON("../class/request.php?action="+statusContext+".getAll", function(contractStatuses) {
          if($.isEmptyObject(contractStatuses))
          {
               $("#"+statusContext+"_table").append($("<tr />").addClass("contractstatus")
                .append($("<td />",{html:"There are no contract statuses configured"}))
              )
          } else {
             $.each(contractStatuses,function(index, contractstatus) {
               self._display(contractstatus, self, statusContext);
             });
          }       
       });
     } ,
     
     _display        : function(status, self, statusContext)
     { //alert((status.status & 4)==4);
          $("#"+statusContext+"_table").append($("<tr />").addClass("status")
           .append($("<td />",{html:status.id}))
           .append($("<td />",{html:status.name}))
           .append($("<td />",{html:status.client_terminology}))
           .append($("<td />")
             .append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":status.color, padding:"1px", width:"100px"}))
           )
          .append($("<td />",{html:((status.status & 1) ? "<b>Active</b>" : "<b>Inactive</b>")}))           
           .append($("<td />")
              .append($("<input />",{type:"button", name:"edit_"+status.id, id:"edit_"+status.id, value:"Edit",disabled:((status.status & 4)==4)}))
           )
          )
          
          $("#edit_"+status.id).live("click", function(e){
            self._editContractStatus(status, statusContext);
            e.preventDefault();
          });
     } ,  
     
     _editContractStatus      : function(status, statusContext)
     {
        if($("#"+statusContext+"_"+status.id+"_status").length > 0)
        {
          $("#"+statusContext+"_"+status.id+"_status").remove();
        }
        var self = this;
        $("<div />",{id:statusContext+"_"+status.id+"_status"}).append($("<table />", {width:"100%"})
          .append($("<tr />")
            .append($("<th />",{html:"Status Name :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", id:"name", value:status.name, readonly:(status.defaults ? "readonly" : "")}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Client Terminology :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", id:"client_terminology", value:status.client_terminology}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Status Color :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", name:"color", id:"color", readonly:"readonly"}))
            )
          )    
          .append($("<tr />")
            .append($("<td />",{colspan:2})
              .append($("<span />",{id:"message"}).css({padding:"5px;"}))
            )
          )                                      
        ).dialog({
            autoOpen          : true,
            position          : "top",
            modal             : true,
            width             : "auto",
            title             : "Edit "+statusContext+" Status",
            buttons           : {
                                   "Save Changes"         : function()
                                   {
                                      $("#message").addClass("ui-state-info").html("updating . . .<img src='resources/images/loaderA32.gif' />" );
                                      if($("#name").val() == "")
                                      {
                                        $("#message").addClass("ui-state-error").html("Status Name is required ");
                                      } else if($("#client_terminology").val() == ""){
                                        $("#message").addClass("ui-state-error").html("Client terminology is required ");
                                      } else {
                                        self.update(status.id, statusContext);
                                      }
                                   } ,
                                   
                                   "Cancel"       : function()
                                   {
                                      $("#"+statusContext+"_"+status.id+"_status").dialog("destroy");
                                      $("#"+statusContext+"_"+status.id+"_status").remove();
                                   } , 
                                   
                                   "Activate"          : function()
                                   {
                                       self.updateStatus(status.id, statusContext, 1);                                   
                                   } , 
                                   
                                   "De-Activate"       : function()
                                   {
                                      self.updateStatus(status.id, statusContext, 0);
                                   } ,
                                   
                                   "Delete"            : function()
                                   {
                                      self.updateStatus(status.id, statusContext, 2);
                                   }
            } ,
            close             : function(event, ui)
            {
              $("#"+statusContext+"_"+status.id+"_status").dialog("destroy");
              $("#"+statusContext+"_"+status.id+"_status").remove();            
            } ,
            open              : function(event, ui)
            {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var saveBtn       = btns[0];
                var cancelBtn     = btns[1];
                var activateBtn = btns[2];
                var deactivateBtn   = btns[3];
                var deleteBtn     = btns[4];
                    	                
                if((status.status & 1) == 0)
                {
                  $(deactivateBtn).css({"display":"none"});
                } else {
                  $(activateBtn).css({"display":"none"});
                }
                
                $(saveBtn).css({"color":"#090"});
                $(deactivateBtn).css({"color":"red"});
                $(activateBtn).css({"color":"#090"});
                $(deleteBtn).css({"color":"red"});

			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString(status.color)  // now you can access API via 'myPicker' variable   
			 $(this).css("height", "auto");
            }
        });     
     } ,
     
     update              : function(id, statusContext)
     {
          var self = this;
          $.post("../class/request.php?action="+statusContext+".updateStatus", {
               data : { id                  : id,          
                        name      : $("#name").val(),
                        client_terminology  : $("#client_terminology").val(),
                        color               : $("#color").val()
               }
          },function(response) {
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               jsDisplayResult("ok", "ok", response.text );
               $("#"+statusContext+"_"+id+"_status").dialog("destroy");
               $("#"+statusContext+"_"+id+"_status").remove();          
               self.get(statusContext);
             }
          }, "json");
     } ,
     
     updateStatus        : function(id, statusContext, status)
     {
        var self = this;
          $.post("../class/request.php?action="+statusContext+".updateStatus", {
               data : { id          : id,
                        status      : status
                      }
          },function(response) {
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               jsDisplayResult("ok", "ok", response.text );
               $("#"+statusContext+"_"+id+"_status").dialog("destroy");
               $("#"+statusContext+"_"+id+"_status").remove();          
               self.get(statusContext);
             }
          }, "json");
     }
     
     

}
