// JavaScript Document
$(function(){
	   
	 Events.get()  
	   
	$("#save").click(function(){
		Events.save();
		return false;
	});
	
	$("#edit").click(function(){
		Events.edit();	
		return false;
	});
	
	$("#update").click(function(){
		Events.update();	
		return false;
	});	
});

var Events 	= {
	
	get 		: function()
	{
	     $(".events").remove();
		$.post("../class/request.php?action=Event.getAll", function( response ){
			$.each( response, function( index, events){
				Events.display( events )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the events name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientEvent.saveEvent", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
                    Events.get();
		            $("#name").val("");
                    $("#description").val("");
				}															   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		data.id 		 =  $("#eventid").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the events name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientEvent.updateEvent", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					if(response.updated)
					{
					   jsDisplayResult("ok", "ok", response.text);
					} else {
					   jsDisplayResult("info", "info", response.text);
					}
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var data     = {}
		data.status	 = $("#status :selected").val()
		data.id 	 = $("#eventid").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ClientEvent.updateEvent", { data : data }, function( response ){	
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				if(response.updated)
				{
				   jsDisplayResult("ok", "ok", response.text);
				} else {
				   jsDisplayResult("info", "info", response.text);
				}
			}
		}, "json");
	} , 
	
	display		: function( events )
	{
		$("#event_table")
		 .append($("<tr />",{id:"tr_"+events.id}).addClass("events")
		   .append($("<td />",{html:events.id}))
		   .append($("<td />",{html:events.name}))
		   .append($("<td />",{html:events.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "edit_"+events.id,
				  					id			: (events.imported == true ? "edit" : "edit_"+events.id),
				  					value		: "Edit "
				  					}))
			  .append($("<input />",{
				  					  type		: "button",
				  					  name		: "del_"+events.id,
				  					  id		     : ((events.imported || events.used) == true ? "del" : "del_"+events.id),
				  					  value		: "Del"
				  					}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((events.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
			
		   .append($("<td />")
			  .append($("<input />",{
		  						type		: "button",
		  						name		: "change_"+events.id,
		  						id		: (events.imported == true ? "change" : "change_"+events.id),
		  						value	: "Change Status"
		  					}))			  					
		    )		   
		 )
		 if(events.imported == true)
		 {
		     $("input[name='change_"+events.id+"']").attr('disabled', 'disabled')
		     $("input[name='edit_"+events.id+"']").attr('disabled', 'disabled')
		     $("input[name='del_"+events.id+"']").attr('disabled', 'disabled')
		 }	 	 		 
		 $("#edit_"+events.id).live("click", function( e ){
			document.location.href = "edit_event.php?id="+events.id;								   
			e.preventDefault();								   
		  });
		 
		 $("#del_"+events.id).live("click", function(e){
			var data    = {};
			data.id     =  events.id;
			data.status = 2;
			if( confirm("Are you sure you want to delete this event"))
			{
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ClientEvent.updateEvent", { data : data }, function( response ){
      				if( response.error )
      				{
				         jsDisplayResult("error", "error", response.text);		
         				} else {
         			        $("#tr_"+events.id).fadeOut();
         				   jsDisplayResult("ok", "ok", response.text);
         				}
				}, "json");	
			}	
			e.preventDefault();								   
		  });		
		 
		 $("#change_"+events.id).live("click", function(){
			document.location.href = "change_events.php?id="+events.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
