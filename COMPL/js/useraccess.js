$(function(){

	if( $("#userid").val() == undefined)
	{
		UserAccess.getUsers();
		UserAccess.get();
	}
	$("#setup_access").live("click", function(){	
		UserAccess.save();
		return false;
	});
	
	$("#edit").live("click", function(){
		UserAccess.edit();
		return false;
	});
	
});


var UserAccess = {

	getUsers 		: function()
	{
		$.getJSON("../class/request.php?action=UserAccess.getInActiveUsers", function( users ){
			$.each( users , function(i , user){
			    if(used.user == true)
			    {
			       $("#userselect").append($("<option />",{text:user.user, value:i, disabled:"disabled"}));
			    } else {
			      $("#userselect").append($("<option />",{text:user.user, value:i}));
			    }
			});
		});	
	} , 
	
	save			: function()
	{
		var data 		= {};			
		data.user 	          = $("#userselect :selected").val();
		data.module_admin	     = $("#module_admin :selected").val();
		data.create_legislation	= $("#create_legislation :selected").val();
		data.import_legislation	= $("#import_legislation :selected").val();
		data.admin_all	          = $("#admin_all :selected").val();
		data.view_all	          = $("#view_all :selected").val();
		data.edit_all            = $("#edit_all :selected").val();
		data.update_all	     = $("#update_all :selected").val();
		data.reports 	          = $("#reports :selected").val();
		data.assurance           = $("#assurance :selected").val();
		data.setup           = $("#usersetup :selected").val();
		
		jsDisplayResult("info", "info", "Saving user access settings . . . .<img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=UserAccess.saveUserSettings",{
			data	: data
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else{		
				UserAccess.get();
				jsDisplayResult("ok", "ok", response.text);
			} 			
		},"json");	
	} , 
	
	get				: function()
	{
		$(".useraccess").remove();
		$.post("../class/request.php?action=UserAccess.getUserAccessSettings",
		{
			
		}, function( responseUsers  ){
			$.each(responseUsers , function( i , user)
			{
				$("select#userselect option[value="+user.user_id+"]").attr("disabled", "disabled");
				UserAccess._display( user );
			});
		},"json");
		
	} ,
	
	_display			: function( userSetup )
	{
		$("#useraccess_table").append($("<tr />",{id:"tr_"+userSetup.id}).addClass("useraccess")
		   .append($("<td />",{html:userSetup.id}))
		   .append($("<td />",{html:userSetup.user}))
		   .append($("<td />",{html:((userSetup.status & 1) == 1 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 4) == 4 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 1024) == 1024 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 8) == 8 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 32) == 32 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 64) == 64 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 16) == 16 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 128) == 128 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 256) == 256 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 512) == 512 ? "Yes" : "No")}))
		   .append($("<td />")
			  .append($("<input />",{
									 type   : "submit",
									 name	: "edit_"+userSetup.id,
									 id		: "edit_"+userSetup.id,
									 value	: "Edit"
			  }))
			  .append($("<input />",{
									type	: "submit",
									name	: "del_"+userSetup.id,
									id		: "del_"+userSetup.id,
									value	: "Del"
			  }))
		   )
		)
		
		
		$("#edit_"+userSetup.id).live("click", function(){
			document.location.href = "edit_user_access.php?id="+userSetup.id;				
			return false;
		});
		
		
		$("#del_"+userSetup.id).live("click", function(){
			if( confirm("Are you sure you want to delete this user access settings"))
			{
				jsDisplayResult("info", "info", "Updating user access settings . . . .<img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=UserAccess.deleteUserSetting",{
					settingId 	: userSetup.id
				}, function( response ){
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else {
						$("#tr_"+userSetup.id).fadeOut();
						jsDisplayResult("ok", "ok", response.text);
					} 	
				},"json")				
			}
			return false;
		});
		
	} , 
	
	edit				: function()
	{
		var data 		= {}; 		 
		data.module_admin	     = $("#module_admin :selected").val();
		data.create_legislation	= $("#create_legislation :selected").val();
		data.import_legislation	= $("#import_legislation :selected").val();
		data.admin_all	          = $("#admin_all :selected").val();
		data.view_all	          = $("#view_all :selected").val();
		data.edit_all            = $("#edit_all :selected").val();
		data.update_all	     = $("#update_all :selected").val();
		data.reports 	          = $("#reports :selected").val();
		data.assurance           = $("#assurance :selected").val();
		data.setup               = $("#usersetup :selected").val();
		jsDisplayResult("info", "info", "Updating user access settings . . . .<img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=UserAccess.updateUserAccess",{
			settingId	: $("#userid").val(),
			data		: data
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else{		
				UserAccess.get();
				jsDisplayResult("ok", "ok", response.text);
			} 			
		},"json");
	}


}
