$(function() {
//detect and act on save_one button to save only one default
	$("input:button.save_one").click(function() {
		var data = "act=UPDATE&"+getDetails($(this));
		saveChanges(data);
		//alert(data);
	});


//detect and act on save_all button to save all defaults
	$("input:button.save_all").click(function() {
		var data = "act=UPDATE";
		$("input:button.save_one").each(function() {
			data+= "&"+getDetails($(this));
		});
		//alert(data);
		saveChanges(data);
	});

	function getDetails($me) {
		var i = $me.prop("id");
		var v = $("#answer_"+i).val();
		return "answer["+i+"]="+v;
	}
	
	function saveChanges(data) {
		$.post("../common/ajax.php?class=COMPL_MODULE_DEFAULTS", data , function( response ){
							if( response[0]=="error" ) {
								alert(response[1]);
							} else {
								document.location.href = 'module_defaults.php?r[]='+response[0]+'&r[]='+response[1];
							}    
						}, "json");
	}
	
});