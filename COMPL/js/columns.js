// JavaScript Document
$(function(){
	
	Columns.get();
	$("#sortable tbody").sortable().disableSelection(); 
});

Columns			= {
		get				: function()
		{
			$.getJSON("../class/request.php?action=LegislationNaming.getNaming",function( response ){
				$.each( response, function(index, val){
					$("#sortable").append($("<tr />")
					   .append($("<td />",{html:val.client_terminology}))
					   .append($("<td />")
						  .append($("<input />",{type:"hidden", name:"manage_hidden", id:"manage_hidden_"+val.id, value:val.id}))
						  .append($("<input />",{type:"checkbox", name:"manage_"+val.id, id:"manage_"+val.id}))
						)
					   .append($("<td />")
						 .append($("<input />",{type:"hidden", name:"new_hidden", id:"new_hidden_"+val.id, value:val.id }))	   
						 .append($("<input />",{type:"checkbox", name:"new_"+val.id, id:"new_"+val.id }))	   
					   )
					)
					if((val.status & 2) == 2)
					{
						$("#manage_"+val.id).attr("checked", "checked")	
					}
					if((val.status & 4) == 4)
					{
						$("#new_"+val.id).attr("checked", "checked")	
					}
					
				})	
				
				$("#sortable").append($("<tr />")
				   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
					 .append($("<input />",{type:"button", name:"save_changes",id:"save_changes", value:" Save Changes"}))	   
				   )		
				)

				$("#save_changes").live("click", function(){
					
					jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
					$.post("../class/request.php?action=BaseNaming.saveColumnOrder",
					{
						data 		: $("#columns_sortable").serializeArray()						
					},function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);
						} else {
							jsDisplayResult("ok", "ok", response.text );
						}
					},"json");
					
					return false;
				});			
			})	
		
		}	
		
		
		
		
};
