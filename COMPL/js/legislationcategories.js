// JavaScript Document
$(function(){
	   
	 LegislativeCategories.get()  
	   
	$("#save_legcat").click(function(){
		LegislativeCategories.save();
		return false;
	});
	
	$("#edit").click(function(){
		LegislativeCategories.edit();	
		return false;
	});
	
	$("#update").click(function(){
		LegislativeCategories.update();	
		return false;
	});	
});

var LegislativeCategories 	= {
	
	get 		: function()
	{
		$.post("../class/request.php?action=LegislationCategory.getAll", function( response ){
			$.each( response, function( index, legcat){
				LegislativeCategories.display( legcat )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the act name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientLegislationCategory.saveLegislativeCategory", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					data.id  	= response.id; 	
					data.status = 1; 	
					LegislativeCategories.display( data )																   
					LegislativeCategories.empty( data );
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			data.id = $("#legcatid").val()
			$.post("../class/request.php?action=ClientLegislationCategory.updateLegislativeCategory", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{ 
		var status	= $("#status :selected").val()
		var data	= {}
		data.status = status;
		data.id		= $("#legcatid").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ClientLegislationCategory.updateLegislativeCategory", {  data : data }, function( response ){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}					  
		}, "json");
	} , 
	
	display		: function( legcat )
	{

		$("#legislative_category_table")
		 .append($("<tr />",{id:"tr_"+legcat.id})
		   .append($("<td />",{html:legcat.id}))
		   .append($("<td />",{html:legcat.name}))
		   .append($("<td />",{html:legcat.description}))
		   .append($("<td />")
			  .append($("<input />",{
			  					 type		: "button",
			  					 name		: "edit_"+legcat.id, 
			  					 id			: (legcat.imported == true ? "edit" : "edit_"+legcat.id),
			  					 value	  	: "Edit "
			  					}))
			  .append($("<input />",{
								 type		: "button",
			  					 name		: "del_"+legcat.id, 
			  					 id			: (legcat.imported == true ? "edit" : "del_"+legcat.id),
			  					 value	  	: "Del "
		  					}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((legcat.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
									 type		: "button",
				  					 name		: "change_"+legcat.id, 
				  					 id			: (legcat.imported == true ? "change" : "change_"+legcat.id),
				  					 value	  	: "Change Status"
				  					}))			  					
		    )		   
		 )
		 if(legcat.imported == true)
		 {
		     $("input[name='change_"+legcat.id+"']").attr('disabled', 'disabled')
		     $("input[name='edit_"+legcat.id+"']").attr('disabled', 'disabled')
		     $("input[name='del_"+legcat.id+"']").attr('disabled', 'disabled')
		 }
		
		 $("#edit_"+legcat.id).live("click", function(){
			document.location.href = "edit_legislative_categories.php?id="+legcat.id;								   
			return false;								   
		  });
		 
		 $("#del_"+legcat.id).live("click", function(){
				if( confirm("Are you sure you want to delete this legislation category")){
					jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					var data 	= {};
					data.status = 2;
					data.id     = legcat.id
					$.post("../class/request.php?action=ClientLegislationCategory.updateLegislativeCategory",  { data : data  }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {
							$("#tr_"+legcat.id).fadeOut();
							jsDisplayResult("ok", "ok", response.text);
						}					  
					}, "json");	
			   }
			return false;								   
	     });		
		 
		 $("#change_"+legcat.id).live("click", function(){
			document.location.href = "change_legislation_category_status.php?id="+legcat.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
