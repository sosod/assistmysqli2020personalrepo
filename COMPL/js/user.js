$(function(){

	if( $("#userid").val() == undefined)
	{
	   UserAccess.getUsers();
	   UserAccess.get();
	}
	$("#notifications").click(function(){
	  document.location.href = "notifications.php?userid="+$("#userid").val();
	  return false;
	});
	
	$("#setup_access").live("click", function(){	
	   UserAccess.save();
	   return false;
	});
	
	$("#edit").live("click", function(){
	   UserAccess.edit();
	   return false;
	});
    UserAccess.getUserSettings();	
    var userObject =  $('body').data('usersetting');
    $("#configure").click(function()
    {
    if($("#configure_financialyear").length > 0)
    {
       $("#configure_financialyear").remove();
    }
  
     $("<div />",{id:"configure_financialyear"})
     .append($("<form />",{id:"setup_financialyear"})
        .append($("<table />",{id:"table_financialyear", width:"100%"}))     
     )
     .dialog({
              autoOpen  : true,
              modal     : true,
              position  : "top",
              title     : "Setup Financial Year",
              width     : 500,
              buttons   : {
                            "Save Changes"  : function()
                            {
                               var checkedVal = $("#setup_financialyear input[name='setdafault']:checked").val();
                               if( checkedVal == undefined)
                               {
                                $("#message").html("Please select the default financial year");
                                return false;
                               } else {
                                  $.post("../class/request.php?action=UserSetting.updateFinancialYear",{
                                    evaluationyear  : $("#setup_financialyear input[name='setdafault']:checked").val()                               
                                  }, function(response){
		                            if( response.error )
		                            {
			                            $("#message").html(response.text)			
		                            } else {    
		                                UserAccess.getUserSettings();
		                                if(!$.isEmptyObject(userObject))
		                                {
		                                    userObject["default_year"] = checkedVal;
			                                $('body').data('usersetting', userObject)			                            			                                
			                            }
			                            jsDisplayResult("ok", "ok", response.text );
			                            $("#configure_financialyear").dialog("destroy");
			                            $("#configure_financialyear").remove();	
			                            		
		                            }                                 
                                  },"json");
                               } 
                            },
                            "Cancel"        : function()
                            {
                              $("#configure_financialyear").dialog("destroy");
                              $("#configure_financialyear").remove();
                            } 
              },
              open     : function(event, ui)
              {
 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
 				var saveButton = buttons[0];
 				$(saveButton).css({"color":"#090"});
               
		        $.post("../class/request.php?action=FinancialYear.fetchAll", {status:1}, function(responseData){
		            $("#table_financialyear").append($("<tr />")
		              .append($("<th />",{html:"Value"}))
		              .append($("<th />",{html:"From"}))
		              .append($("<th />",{html:"To"}))
		              .append($("<th />",{html:"Set As Default"}))
		            )				        
		            if($.isEmptyObject(responseData))
		            {
		                $("#table_financialyear").append($("<tr />")
		                  .append($("<td />",{colspan:4, html:"There are no financial years set yet"}))
		                )
		            } else {
			            $.each(responseData, function( index, finYear) {
			                $("#table_financialyear").append($("<tr />")
			                  .append($("<td />",{html:finYear.value}))
			                  .append($("<td />",{html:finYear.start_date}))
			                  .append($("<td />",{html:finYear.end_date}))
			                  .append($("<td />")
			                    .append($("<input />",{type:"radio", name:"setdafault", id:"set_"+finYear.id, value:finYear.id}))
			                  )
			                )
			            });	
		            }		
	                $("#table_financialyear").append($("<tr />")
	                  .append($("<td />",{colspan:4})
	                    .append($("<span />",{id:"message"}))
	                  )
	                )	
	                $("#table_financialyear tr:even").css({"background-color":"#F7F7F7"});
	                				
		        },"json");
		        var userObject =  $('body').data('usersetting');
		        if(!$.isEmptyObject(userObject))
		        {
		            var defaultYear =  userObject['financialyear'];
		            $("#set_"+defaultYear).attr("checked", "checked");
		        }
              },
              close    : function()
              {
                  $("#configure_financialyear").dialog("destroy");
                  $("#configure_financialyear").remove();              
              }     
     });   
    return false;  
  });
	
});


var UserAccess = {
     getUserSettings          : function()
     {
          jQuery.ajaxSetup({async:false});
		$.getJSON("../class/request.php?action=UserSetting.getUser", function( user){
	        $('body').data("usersetting", user);
	        if(user.financialyear !== "0")
	        {
	          $("#default_year").html("<b>From </b><em>"+user.start_date+"</em> <b> To </b> <em>"+user.end_date+"</em>").addClass("gray");
	        }
		});         
     } ,
          
	getUsers 		: function()
	{
		$.getJSON("../class/request.php?action=User.getAll", function( users ){
			$.each( users , function(i , user){
			    if(user.used == true)
			    {
			       //$("#userselect").append($("<option />",{text:user.user, value:i, disabled:"disabled"}));
			    } else {
			      $("#userselect").append($("<option />",{text:user.user, value:i}));
			    }			
				
			});
		});	
	} , 
	
	save			: function()
	{
		var data 		= {};			
		data.user 	          = $("#userselect :selected").val();
		data.module_admin	     = $("#module_admin :selected").val();
		data.create_legislation	= $("#create_legislation :selected").val();
		data.import_legislation	= $("#import_legislation :selected").val();
		data.admin_all	          = $("#admin_all :selected").val();
		data.view_all	          = 1; //$("#view_all :selected").val();
		data.edit_all            = $("#edit_all :selected").val();
		data.update_all	     = $("#update_all :selected").val();
		data.reports 	          = $("#reports :selected").val();
		data.assurance           = $("#assurance :selected").val();
		data.setup               = $("#usersetup :selected").val();
		
		jsDisplayResult("info", "info", "Saving user access settings . . . .<img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=UserSetting.saveUserSettings",{
			data	: data
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else{		
				UserAccess.get();
				jsDisplayResult("ok", "ok", response.text);
			} 			
		},"json");	
	} , 
	
	get				: function()
	{
		$(".useraccess").remove();
		$.post("../class/request.php?action=UserSetting.getUsers",
		{
			
		}, function( responseUsers  ){
			$.each(responseUsers , function( i , user)
			{
				$("select#userselect option[value="+user.user_id+"]").attr("disabled", "disabled");
				UserAccess._display( user );
			});
		},"json");
		
	} ,
	
	_display			: function( userSetup )
	{
//		   .append($("<td />",{html:((userSetup.status & 32) == 32 ? "Yes" : "No")}))
		$("#useraccess_table").append($("<tr />",{id:"tr_"+userSetup.id}).addClass("useraccess")
		   .append($("<td />",{html:userSetup.id}))
		   .append($("<td />",{html:userSetup.user}))
		   .append($("<td />",{html:((userSetup.status & 1) == 1 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 4) == 4 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 1024) == 1024 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 8) == 8 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 64) == 64 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 16) == 16 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 128) == 128 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 256) == 256 ? "Yes" : "No")}))
		   .append($("<td />",{html:((userSetup.status & 512) == 512 ? "Yes" : "No")}))
		   .append($("<td />")
			  .append($("<input />",{type:"submit", name:"edit_"+userSetup.id, id:"edit_"+userSetup.id, value:"Edit" }))
			  .append($("<input />",{type:"submit",	name:"del_"+userSetup.id, id:"del_"+userSetup.id,	value:"Del" }))
		   )
		)
		
		
		$("#edit_"+userSetup.id).live("click", function(){
			document.location.href = "edit_user_access.php?id="+userSetup.id;				
			return false;
		});
		
		
		$("#del_"+userSetup.id).live("click", function(){
			if( confirm("Are you sure you want to delete this user access settings"))
			{
				jsDisplayResult("info", "info", "Updating user access settings . . . .<img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=UserSetting.deleteSetting",{
					settingId 	: userSetup.id
				}, function( response ){
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else {
						$("#tr_"+userSetup.id).fadeOut();
						jsDisplayResult("ok", "ok", response.text);
					} 	
				},"json")				
			}
			return false;
		});
		
	} , 
	
	edit				: function()
	{
		var data 		= {}; 		 
		data.module_admin	     = $("#module_admin :selected").val();
		data.create_legislation	= $("#create_legislation :selected").val();
		data.import_legislation	= $("#import_legislation :selected").val();
		data.admin_all	          = $("#admin_all :selected").val();
		data.view_all	          = "1"; //$("#view_all :selected").val();
		data.edit_all            = $("#edit_all :selected").val();
		data.update_all	     = $("#update_all :selected").val();
		data.reports 	          = $("#reports :selected").val();
		data.assurance           = $("#assurance :selected").val();
		data.setup               = $("#usersetup :selected").val();
		jsDisplayResult("info", "info", "Updating user access settings . . . .<img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=UserSetting.updateSettings",{
			settingId	: $("#userid").val(),
			data		: data
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else{		
				UserAccess.get();
				jsDisplayResult("ok", "ok", response.text);
			} 			
		},"json");
	}


}
