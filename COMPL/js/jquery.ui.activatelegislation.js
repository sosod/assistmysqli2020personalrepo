$.widget("ui.activatelegislation", {
	
	options		: {
		start 		: 0,
		limit		: 10,
		current		: 1,
		total		: 0,
		url			: "../class/request.php?action=Legislation.getActivateLegislations",
		tableId		: "activate_"+(Math.floor(Math.random(45)+45)),
		user		: "",
		company		: "",
		headers		: ['ref', 'legislation_name', 'legislation_type', 'legislation_category', 'legislation_reference'],
		section        : ""
	} , 
	
	_init					: function(){
		this._getLegislation();		
	} , 
	
	_create					: function()
	{
		$(this.element).append($("<table />",{width:"100%", id:this.options.tableId}).addClass("noborder")
			.append($("<tr />")
				.append($("<td />",{"width":"50%"}).addClass("noborder")
					.append($("<table />",{id:"awaiting_approval", width:"100%"})
					  .append($("<tr />")
						 .append($("<td />",{colspan:"6", html:"<h4>Legislations Awaiting Activation</h4>"}))	  
					  )		
					)	
				)
				.append($("<td />",{"width":"50%"}).addClass("noborder")
				   .append($("<table />",{id:"activated_legislations", width:"100%"})
					   .append($("<tr />")
						 .append($("<td />",{colspan:"5", html:"<h4>Legislations Activated</h4>"}))	   
					   )	   
				   )		
				)
			)		
		)
		
	} , 
	
	_getLegislation				: function()
	{
		var self = this;
		$.getJSON(self.options.url, {
			data : {  start		: self.options.start, 	
			          limit		: self.options.limit,
			          section        : self.options.section
			}
		}, function(responseData){		
			self.options.user 	 = responseData.user;
			self.options.company = responseData.company 
			//self.displayPaging();
			$(".legislationactivation").remove();
			if($.isEmptyObject(responseData.awaiting))
			{
				self._displayHeaders(responseData.headers, responseData.headers ,"awaiting_approval", false);
				$("#awaiting_approval").append($("<tr />",{id:"no_awaiting"}).addClass("legislationactivation")
				   .append($("<td />",{colspan:"7", html:"There are no legislations awaiting activation"}))
				)				
			} else { 
			   if($("#no_awaiting").length > 0)
			   {
			     $("#no_awaiting").remove();
			   }
			   self._displayHeaders(responseData.headers, responseData.headers, "awaiting_approval", true);
			   self._display(responseData.awaiting.legislation, "awaiting_approval");				
			}
			
			if( $.isEmptyObject(responseData.activated))
			{				    
				self._displayHeaders(responseData.headers, responseData.headers, "activated_legislations" );
				$("#activated_legislations").append($("<tr />",{id:"no_activated"}).addClass("legislationactivation")
				   .append($("<td />",{colspan:"7", html:"There are no activated legislations"}))
				)								
			} else {	    
			     if($("#no_activated").length > 0)
			     {
			       $("#no_activated").remove();
			     }			      		     
				self._displayHeaders(responseData.headers, responseData.headers, "activated_legislations" );
				self._display(responseData.activated.legislation, "activated_legislations");
			}					
			
		})
	} , 
	
	_display					: function(legislations, table)
	{
		var self = this;
		$.each(legislations, function(index, legislation) {
			var tr = $("<tr />",{id:"tr_"+legislation.id}).addClass("legislationactivation")
			$.each(legislation, function( key , val ) {
			  tr.append($("<td />",{html:val }))
			});
			
			tr.append($("<td />").css({display:(table == "awaiting_approval" ? "table-cell" : "none")})
			    .append($("<input />",{type:"button", name:"activate_"+legislation.id, id:"activate_"+legislation.id, value:"Activate"}))
			)
			
			$("#activate_"+legislation.id).live("click", function() {
			
				if( $("#activation_"+legislation.id).length > 0)
				{
					$("#activation_"+legislation.id).remove();
				}
			
				$("<div />",{id:"activation_"+legislation.id, html:"I, "+self.options.user+", of "+self.options.company+" hereby:"})
				.append($("<ul />") 
					.append($("<li />",{html:"declare that I have the required authority to activate this legislation;"}))
					.append($("<li />",{html:"declare that I have reviewed and am hereby authorising all the additions and edits (including deletions) made to the deliverables and actions associated to this, "+legislation.name+" "+legislation.reference+", legislation;"}))
				)
				.dialog({
					autoOpen	: true,
					modal		: true,
					position	: "top",
					width		: "500px",
					title		: "Activating legislation",
					buttons		: {
									"Activate"  : function(){
									    var data = {};
									    data.id 	 = legislation.id,
									    jsDisplayResult("info", "info", "Activating legislation. . . <img src='../images/loaderA32.gif' />");
									    $.post("../class/request.php?action=ClientLegislation.activateLegislation", {
											data		: data 
										}, function( response ) {
											if( response.error ){
												jsDisplayResult("error", "error", response.text);		
											} else {					
												jsDisplayResult("ok", "ok", response.text);
			                                    self._getLegislation();
											}									
											$("#activation_"+legislation.id).remove();
										},"json")
									} ,
									"Cancel"	: function(){
										$(this).dialog("destroy");
										$("#activation_"+legislation.id).remove();
									}
					 } ,
				     close      : function(events, ui)
				     {
						$(this).dialog("destroy");
						$("#activation_"+legislation.id).remove();  
				     } ,
				     open 		: function(event, ui)
				     {
		 				var dialog 			= $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons 		= dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var activateButton  = buttons[0];
		 				$(activateButton).css({"color":"#090"});
		 				$(this).css("height", "auto")
				     }
				});
				return false;
			});
			$("#"+table).append( tr );
		});
		
	} , 
	
	_displayHeaders				: function(headers, headers2, table, extraHeader)
	{
		var self = this;
		var tr = $("<tr />").addClass("legislationactivation")
		if($.isEmptyObject(headers)){
			headers = headers2
		} else if($.isEmptyObject(headers2)) {
			headers = headers;
		}
		//tr.append($("<th />",{html:"&nbsp;"}).css({display:(table == "awaiting_approval" ? "table-cell" : "none")}))
		$.each( headers, function( index, head){
		   tr.append($("<th />",{html:head}))
		});
		tr.append($("<th />").css({display:((table == "awaiting_approval" && extraHeader) ? "table-cell" : "none")}))
		$("#"+table).append( tr );
	} , 
	
	displayPaging				: function()
	{
		
	}

});
