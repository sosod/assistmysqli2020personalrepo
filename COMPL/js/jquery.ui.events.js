$.widget("ui.events",{
      
      options  : {
            start           : 0,
            limit           : 10,
            total           : 0,
            current         : 1,
            page            : "",
            edit            : false,
            triggerOcccured : false,
            tableRef        : "eventsTable_"+(Math.round(Math.random()*34)),     
            viewtype 	   : "",
            financialyear   : 0,
            autoLoad        : true,
            financialYearsList : {}
      } , 
      
      _init    : function()
      {
         if(this.options.autoLoad)
         {
           this._getEvents();
         }
      } , 
      
      _create  : function(){
         var self = this, options = this.options;
          $(self.element).append($("<table />", {width:"100%"}).addClass("noborder")
            .append($("<tr />")
              .append($("<th />", {html:"Select the financial year :"}).css({"text-align":"left"}))
              .append($("<td />").addClass("noborder")
                .append($("<select />",{id:"financial_year", name:"financial_year"}))
              )
            )
            .append($("<tr />")
              .append($("<td />",{colspan:"2"}).addClass("noborder")
                .append($("<table />",{id:options.tableRef, width:"100%"}))
              )
            )            
          )
         
         self._loadFinancialYears();
         
         $("#financial_year").change(function(){
          options.financialyear = $(this).val();
           self._getEvents();         
         });
         
         $("#eventall").live('click', function(e){
           self.options.viewtype = "viewall";
           if(options.financialyear !== "")
           {
             self._getEvents(); 
           }
           e.preventDefault();
         });
         
         $("#eventmine").live('click', function(e){
           self.options.viewtype = "eventmine";
           if(options.financialyear !== "")
           {
             self._getEvents(); 
           }           
           e.preventDefault();
         });         

      },
         
      _loadFinancialYears     : function()
      {
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $("#financial_year").append($("<option />",{value:"", text:"--please select--"}));
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAll",function(financialYears) {
	          if($.isEmptyObject(financialYears))
	          {
	              $("#financial_year").append($("<option />",{value:"", text:"--please select--"}))
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear){
	               if(self.options.financialyear === financialYear.id)
	               {
                         $("#financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date, selected:"selected"}))	               
	               } else {
                       $("#financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date}))	  
	               }
	             });
	          }	   
	        });
	   } 
      } ,

         
      _getEvents  : function()
      {
         var self = this, options = this.options;
    		$("#"+options.tableRef).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
			   .css({position:"absolute", "z-index":"9999", top:"400px", left: "200px", border:"1px solid #FFFFF", padding:"5px"})
		   )        
	      $.getJSON("../class/request.php?action=EventOccuranceManager.listSubEvents",{
	      	options : { viewtype:options.viewtype, financialyear: options.financialyear}	      
	      }, function( events ){
	         $("#message").remove();
	         $("#"+options.tableRef).html("");
	         self._displayHeaders();
	         if($.isEmptyObject(events))
	         {
	         	$("#"+options.tableRef).append($("<tr />")
	         	  .append($("<td />",{colspan:"6"})
	         	    .append($("<p />",{html:"There are no events to trigger for the selected financial year"}))
	         	  )
	         	)
	         } else {
	            self._display( events );
	         }
	      },"json");
      } ,
      
      _displayHeaders   : function()
      {
         var self = this;
         $("#"+self.options.tableRef).append($("<tr />")
           .append($("<th />",{html:"Ref"}))
           .append($("<th />",{html:"Sub Event Name"}))
           .append($("<th />",{html:"Sub Event Description"}))
           .append($("<th />",{html:"Status"}))
           .append($("<th />",{html:"&nbsp;"}))
         )
      
      } , 
      
      _display           : function( events )
      {
         var self = this;        
         $.each( events, function( index, event){
            var tr = $("<tr />")
            tr.append($("<td />",{html:event.id}))
            tr.append($("<td />",{html:event.name}))
            tr.append($("<td />",{html:event.description}))
            tr.append($("<td />",{html:event.statusText+"<span class='lighter'><em>"+(event.date_occured == undefined ? "" : "  ("+event.date_occured+")  ")+"  </em></span>"}))
            tr.append($("<td />").css({display:(self.options.edit ? "table-cell" : "none")})
              .append($("<input />",{type:"button", name:"edit_"+event.id, id:"edit_"+event.id, value:"Edit"}))
            )  
            tr.append($("<td />").css({display:(self.options.triggerOcccured ? "table-cell" : "none")})
              .append($("<input />",{
                                    type : "button",
                                    name : "triggerevent_"+event.id,
                                    id   : ((event.status  & 8) == 8 ? "triggerevent" : "triggerevent_"+event.id),
                                    value: "Trigger Event"}))                                    
            )              
            $("#triggerevent_"+event.id).attr('selected', ((event.status  & 8) == 8 ? "disabled" : ""));
            
            $("#triggerevent_"+event.id).live("click", function(){
            
              if( $("#dialogoccured_"+event.id).length > 0)
              {
                 $("#dialogoccured_"+event.id).remove();
              }
            
              $("<div />",{id:"dialogoccured_"+event.id})
               .append($("<table />", {width:"100%"})
                 .append($("<tr />")
                   .append($("<th />",{html:"Date event occured:"}))
                   .append($("<td />")
                     .append($("<input />",{type:"text", name:"date_occured", id:"date_occured", readonly:"readonly", value:""}).addClass("datepicker"))
                   )
                 )
                 .append($("<tr />")
                   .append($("<th />",{html:"Comment:"}))
                   .append($("<td />")
                     .append($("<textarea />",{cols:"30", rows:"5", name:"eventcomment", id:"eventcomment"}))
                   )
                 )
                 .append($("<tr />")
                   .append($("<td />",{colspan:"2"})
                    .append($("<p />").css({padding:"5px"})
                      .append($("<span />",{id:"message"}))
                    )
                   )
                 )               
               ).dialog({
                         autoOpen   : true,
                         modal      : true,
                         title      : "Date event occured", 
                         position   : "top",
                         width      : "500px",
                         buttons    : {
                                       "Activate Event"    : function()
                                       {
                                          if( $("#date_occured").val() == "")
                                          {
                                             $("#message").addClass("ui-state-error").html("Please enter the date the event occured")
                                             return false;
                                          } else {
                                             jsDisplayResult("info", "info", "Please wait while copying deliverables and action . . . <img src='../images/loaderA32.gif' />");
                                             $.post("../class/request.php?action=EventOccurance.updateEvent",{
                                               data:{event_id:event.id,date_occured:$("#date_occured").val(), comment:$("#eventcomment").val()}
                                             }, function( response ){
                                                if(response.error)
                                                {
                                                   $("#message").addClass("ui-state-error").html(response.text);
                                                   //jsDisplayResult("error","error",response.text );                            
                                                } else {
                                                   jsDisplayResult("ok","ok",response.text);                                       
                                                   $("#dialogoccured_"+event.id).dialog("destroy");
                                                   $("#dialogoccured_"+event.id).remove(); 
                                                   document.location.href = "events_activate.php?eventid="+event.id+"&eventoccuranceid="+response.id;
                                                }
                                             },"json");
                                          }                    
                                       } ,
                                       "Cancel"          : function()
                                       {
                                          $("#dialogoccured_"+event.id).dialog("destroy");
                                          $("#dialogoccured_"+event.id).remove();                                       
                                       } 
                         } , 
                         open			: function(event, ui)
                         {
                            var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                            var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
                            var saveButton = buttons[0];
                            $(saveButton).css({"color":"#090"});
                            $(".datepicker").datepicker({
                                showOn: 'both',
                                buttonImage: '/library/jquery/css/calendar.gif',
                                buttonImageOnly: true,
                                dateFormat: 'dd-M-yy',
                                changeMonth:true,
                                changeYear:true		
                            });		 				
                         } , 
                         close            : function(event, ui)
                         {
                                $("#dialogoccured_"+event.id).dialog("destroy");
                                $("#dialogoccured_"+event.id).remove();                                      
                         }
               });             
               return false;
            })
            $("#"+self.options.tableRef).append(tr)       
         });
            
      }

});
