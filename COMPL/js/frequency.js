// JavaScript Document
$(function(){
	   
	 Frequency.get()  
	   
	$("#save_freq").click(function(){
		Frequency.save();
		return false;
	});
	
	$("#edit_freq").click(function(){
		Frequency.edit();	
		return false;
	});
	
	$("#update_freq").click(function(){
		Frequency.update();	
		return false;
	});	
});

var Frequency 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getFrequency", function( response ){
			$.each( response, function( index, freq){
				Frequency.display( freq )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#frequency_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			message.html("Please enter the  frequency name");
			return false;
		} else if( data.description == ""){
			message.html("Please enter the description")
			return false;
		} else {
			message.html("Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveFrequency", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				Frequency.display( data )																   
				Frequency.empty( data );
				message.html(response.text);																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#frequency_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			message.html("Please enter the frequency name");
			return false;
		} else if( data.description == ""){
			message.html("Please enter the description")
			return false;
		} else {
			message.html("Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateFrequency", { id : $("#freqid").val(), data : data }, function( response ){		
				message.html( response.text );																   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#frequency_message").show(); 
		var status		= $("#status :selected").val()
		message.html("Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateFrequency", {  id : $("#freqid").val(), status : status }, function( response ){																																																									 					message.html( response.text );
		}, "json");
	} , 
	
	display		: function( freq )
	{
		$("#frequency_table")
		 .append($("<tr />",{id:"tr_"+freq.id})
		   .append($("<td />",{html:freq.id}))
		   .append($("<td />",{html:freq.name}))
		   .append($("<td />",{html:freq.description}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+freq.id, id:"edit_"+freq.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+freq.id, id:"del_"+freq.id, value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((freq.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+freq.id, id:"change_"+freq.id, value:"Change Status"}))			  					
		    )		   
		 )
		 		 
		 $("#edit_"+freq.id).live("click", function(){
			document.location.href = "edit_frequency.php?id="+freq.id;								   
			return false;								   
		  });
		 
		 $("#del_"+freq.id).live("click", function(){
			var message 	 = $("#frequency_message").show(); 
			if( confirm("Are you sure you want to delete this frequency")){
				message.html("Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateFrequency",
						   { id : freq.id , status : 2 }, function( response ){
							   $("#tr_"+freq.id).fadeOut();																		  																																													 							   message.html( response.text );
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+freq.id).live("click", function(){
			document.location.href = "change_frequency.php?id="+freq.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}