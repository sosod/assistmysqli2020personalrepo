$.widget("ui.legislationassurance", {
	
	options			: {
		start			: 0, 
		limit			: 10,
		current			: 1, 
		total			: 0,
		tableId			: "assurance_"+(Math.floor(Math.random(54) *56) + 67),
		legislationId	: 0
		
	} , 
	
	_init			: function()
	{
		this._getLegislationAssurance();				
	} , 
	
	
	_create				: function()
	{
		$(this.element).append($("<form />")
		   .append($("<table />",{id:this.options.tableId}))
		)				
	} , 
	
	
	_getLegislationAssurance		: function()
	{
		var self = this;
		$.post("../class/request.php?action=getLegislationAssurance",
		{	
			start		: self.options.start, 
			limit		: self.options.limit,
			legislationId : self.options.legislationId
		}, function( assuranceResponse ){
			console.log( assuranceResponse )
			
		}, "json")		
		
	} , 
	
	
	_display						: function()
	{
		
		
		
	} , 
	
	
	_displayPaging					: function()
	{
		
		
	}
	
	
	
	
	
	
	
	
	
	
});