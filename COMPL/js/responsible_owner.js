$(function(){
     
     var ownerObj = new ResponsibleOwner();   
     ownerObj.get();
     
});

function ResponsibleOwner()
{
  
   this.responsible_owner = $("#responsible_owner");
   this.user              = $("#users")
   this.id                = $("#id");
   this.responsibleownertable  = $("#responsible_owner_table");
   this.assignBtn         = $("#assign_user");
   this.editBtn           = $("#edit")
   this.updateBtn         = $("#update")
   this.addResponsibleOnwer = $("#add")
      
   var self = this;
   
   this.saveResponsibleOwner = function()
   {
	if(self.responsible_owner.val() == "") {
		jsDisplayResult("error", "error", "Please select the title to assign to a user");
		return false;
	} else if(self.user.val() == ""){
		jsDisplayResult("error", "error", "Please select the user for assigned to the selected title");
		return false;
	} else {
		jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ResponsibleOwnerMatch.saveOwner", 
		{ 
		    data :{ user_id   : self.user.val(),
		            ref       : self.responsible_owner.val(),
		            usernotinlist : $("#usernotinlist").val() 
		          }
		}, function( response ){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {					
				self.get();					
				jsDisplayResult("ok", "ok", response.text);
			}															   
		},"json");	
		
	}
   }     
   
   this._getUsers            = function()
   {
	this.user.empty();
	this.user.append($("<option />",{text:"--please select--", value:""}));		
	$.post("../class/request.php?action=User.getAll", function(response){
	   $.each( response, function( index, owner){
	       self.user.append($("<option />",{text:owner.user, value:index}))
	   })												  											  
	},"json");
   } 
   
   this._getResponsibleOwnerTitles  = function()
   {
	self.responsible_owner.empty();
	self.responsible_owner.append($("<option />",{text:"--please select--", value:""}));
	$.post("../class/request.php?action=ResponsibleOwnerManager.getResponsibleOwnerTitles", function( response ){
	   $.each( response, function( index, owner){
	      if(owner.used)
	      {
	         self.responsible_owner.append($("<option />",{text:owner.name, value:index, disabled:"disabled"}))	
	      } else {
	          self.responsible_owner.append($("<option />",{text:owner.name, value:index}))	
	      }
	   })							  
	},"json");
   }
   
   this._getUserResponsibleOwnerTitle   = function()
   {
	$(".responsibleowner").remove();
	$.post("../class/request.php?action=ResponsibleOwnerManager.getAll", function(response) {
	   if($.isEmptyObject(response))
	   {
	      self.responsibleownertable.append($("<tr />").addClass("responsibleowner")
	        .append($("<td />",{html:"There are responsible owners match", colspan:"6"}))
	      )
	   } else {
	      $.each( response, function( index, owner){
		   self._display(owner);	
	      })	
	   }											  											  
	},"json");
   }
   
   this.get                        = function()
   {
      if(self.id.val() == undefined)
      {
           self._getUsers();
           self._getResponsibleOwnerTitles()
           self._getUserResponsibleOwnerTitle();
      }
   }
   
   
   this._display                    = function(responsible_owner)
   {
	self.responsibleownertable
	 .append($("<tr />",{id:"tr_"+responsible_owner.id}).addClass("responsibleowner")
	   .append($("<td />",{html:responsible_owner.id}))
	   .append($("<td />",{html:responsible_owner.title}))
	   .append($("<td />",{html:responsible_owner.user}))
	   .append($("<td />")
		  .append($("<input />",{type:"button", name:"edit_"+responsible_owner.id, id:"edit_"+responsible_owner.id, value:"Edit"}))
		  .append($("<input />",{type:"button", name:"del_"+responsible_owner.id, id:"del_"+responsible_owner.id, value:"Del"}))			  
		)
	   .append($("<td />")
		.append($("<span />",{html:((responsible_owner.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
		)
	   .append($("<td />")
		  .append($("<input />",{type:"button", name:"change_"+responsible_owner.id, id:"change_"+responsible_owner.id, value:"Change Status"}))
	    )		   
	 )
	 		 
	 $("#edit_"+responsible_owner.id).live("click", function(e){
		document.location.href = "edit_responsible_owner.php?id="+responsible_owner.id;
		e.preventDefault();								   
	  });
	 
	 $("#del_"+responsible_owner.id).live("click", function(e){
		var data    = {}; 
		data.id     = responsible_owner.id;
		data.status = 2;
		if( confirm("Are you sure you want to delete this responsible owner")){
			jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ResponsibleOwnerMatch.deleteResponsibleOwnerMatch", { data : data }, function( response ){
 				if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				 $("#tr_"+responsible_owner.id).fadeOut();	
				jsDisplayResult("ok", "ok", response.text);
				self.get();
			}
			}, "json");
			
		}
		e.preventDefault();							   
	  });		
	 
	 $("#change_"+responsible_owner.id).live("click", function(e){
		document.location.href = "change_responsible_owner_status.php?id="+responsible_owner.id;
		e.preventDefault();
	 });
		   
   }
   
   this.responsible_owner.change(function(e){
       self._displayDialog();    
       e.preventDefault();
   });
   
   this._displayDialog                  = function()
   {
	$("<div />",{id:"hasusers", html:"Is this title associated with a user in the list?"})		
	.dialog({
         autoOpen : true,
         modal		: true,
         position	: "top",
          width      : "auto",
         buttons  : {
         			  "Yes"		: function()
         			  {
         			  	 $("#hasusers").dialog("destroy");
         			  	 $("#hasusers").remove();
         			  } , 
         			  "No"		: function()
         			  {
				 	 self.user.empty();
				      self.user.append($("<option />",{text:$("#responsible_owner :selected").text(), value:$("#responsible_owner :selected").val() }));
					 $("#usernotinlist").val( $("#responsible_owner :selected").val() );		  	 
		            	 $("#hasusers").dialog("destroy");
		            	 $("#hasusers").remove();		         			  
         			  }
         } ,
         
         open       : function()
         {
             $(this).css("height", "auto")
         }	
     })

   }
   
   this.assignBtn.click(function(e){
     self.saveResponsibleOwner();
     e.preventDefault()
   });
          
   this.addResponsibleOnwer.click(function(e){
		$("<div />",{id:"newresponsibleowner"})
		.append($("<table />",{width:"100%"})
		  .append($("<tr />")
			.append($("<th />",{html:"Responsible Owner Name:"}))
			.append($("<td />")
			  .append($("<input />",{name:"name", id:"name", value:""}))		
			)
		  )
		 .append($("<tr />")
		   .append($("<td />",{colspan:"2"})
			 .append($("<p />")
			   .append($("<span />"))		 
			 )	   
		   )		 
		 )
		).dialog({
			autoOpen	:true, 
			modal		: true,
			title 		: "Add new responsible owner title",
			position	: "top",
			width		: "auto",
			buttons		: {
							"Save"	: function()
							{
								if( $("#name").val() == "")
								{
									$("#message").html("Please enter the responsible owner title");
									return false;
								} else {
									self.saveNewTitle();
								}
							} , 
							"Cancel"  : function()
							{
								$("#newresponsibleowner").dialog("destroy");
								$("#newresponsibleowner").remove();
							}
						} ,
						
						open    : function()
						{
						     $(this).css("height", "auto")
						}
		})
     e.preventDefault();
   });  
          
          
   this.saveNewTitle          = function()
   {
	$.post("../class/request.php?action=ClientResponsibleOwner.savenewResponsibleOwner",{
		data 	: { name : $("#name").val() }
	}, function( response ){
		if( response.error )
		{
			$("#message").html(response.text);
		} else {
			self.get();
			jsDisplayResult("ok", "ok", response.text);
			$("#newresponsibleowner").dialog("destroy");
			$("#newresponsibleowner").remove();
		}
		
	},"json");
   }
     
   this.editBtn.click(function(e) {
		
	     if(self.responsible_owner.val() == "") {
		     jsDisplayResult("error", "error", "Please select the title to assign to a user");
		     return false;
	     } else if(self.user.val() == ""){
		     jsDisplayResult("error", "error", "Please select the user for assigned to the selected title");
		     return false;
	     } else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ResponsibleOwnerMatch.updateResponsibleOwner", 
			{
			  data : {ref:self.responsible_owner.val(), user_id:self.user.val(), id :self.id.val()} 
			}, function( response ) { 
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
        e.preventDefault();
   });
   
   
   
   this.updateBtn.click(function(e) {
	jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ResponsibleOwnerMatch.updateResponsibleOwner", 
		{
		  data : { status : $("#status :selected").val(), id:self.id.val()} 
		}, function( response ){	
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {					
				jsDisplayResult("ok", "ok", response.text);
			}
	}, "json");   
	e.preventDefault();
   });

}
