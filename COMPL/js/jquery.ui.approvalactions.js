$.widget("ui.approvalactions", {
	
	options			: {
		start 			: 0,
		limit			: 10, 
		current			: 1,
		total			: 0,
		id				: 0, 
		headers			: ["action_reminder", "action_deadline_date"], 
		section             : "manage",
		page                : "",
		display			: "waiting"
	} , 
		
	_init			: function()
	{
		this._getActions();
	} , 
	
	_create			: function()
	{
		$(this.element).append($("<table />", {width:"100%"}).addClass("noborder")
			  .append($("<tr />")
				 .append($("<td />", {width:"50%"}).addClass("noborder")
				   .append($("<table />", {id:"awaiting_approval", width:"100%"})
					 .append($("<tr />")
					   .append($("<td />",{colspan:"9", html:"<h4>Actions Awaiting Approval</h4>"}))		 
					 )
					 .append($("<tr />")
					   
					 )
				   )		 
				 )
				 .append($("<td />", {width:"50%"}).addClass("noborder")
				   .append($("<table />",{id:"approved_actions", width:"100%"})
					 .append($("<tr />")
					   .append($("<td />",{colspan:"8",html:"<h4>Approved Actions</h4>"}))		 
					 )
					 .append($("<tr />")
					   		 
					 )
				   )		 
				 )
			  )				
		)		
	} , 
	
	_getActions			: function()
	{
		var self = this;
	     $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"550px", border:"0px solid #FFFFF", padding:"5px"})
	     );		
		$.getJSON("../class/request.php?action=Action.getApprovalActions", {
			options : {
                    start			: self.options.start,
                    limit			: self.options.limit,
                    deliverableId	: self.options.id,
                    section        : self.options.section,
                    page           : self.options.page
			}
		}, function( responseData ){
               $(".approval").remove();               
               $("#loadingDiv").remove();
			if($.isEmptyObject(responseData.awaiting.actions) )
			{
				//if($.isEmptyObject(responseData.approved.actions) )
				//{
					self._displayHeaders(responseData.approved.headers, "awaiting_approval", false );
				//}
				//responseData.awaiting.columns+=1;
				$("#awaiting_approval").append($("<tr />",{id:"tr_noawaiting"}).addClass("approval")
					.append($("<td />",{colspan:responseData.awaiting.columns, html:"There are no actions awaiting approval"}))
				)		
			} else {
				self._displayHeaders(responseData.awaiting.headers, "awaiting_approval", true);
				self._display(responseData.awaiting.actions, "awaiting_approval");
			}
			
			if( $.isEmptyObject(responseData.approved.actions) )
			{
				//if( $.isEmptyObject(responseData.awaiting.actions) )
				//{
					self._displayHeaders( responseData.approved.headers , "approved_actions")
				//}
				responseData.approved.columns+=1;
				$("#approved_actions").append($("<tr />",{id:"tr_noapproved"}).addClass("approval")
					.append($("<td />",{colspan:responseData.approved.columns , html:"There are no approved actions"}))
				)									
			} else {
				self._displayHeaders( responseData.approved.headers , "approved_actions")
				self._display( responseData.approved.actions, "approved_actions" )				
			}			
		})
				
	} , 
	
	_display			: function( actions, table )
	{
		var self = this;
		$.each( actions , function( index, action){
			var tr = $("<tr />",{id:"tr_"+index}).addClass("approval")
			$.each( action, function( key, val){
				tr.append($("<td />",{html:val}))
			});
			if(table=="approved_actions") {
				//tr.append("<td><input type=button value='Unlock' class=unlock_action_approval /></td>")
				//tr.append("<td>abc</td>");
                tr.append($("<td />").append($("<input />",{type:"button", name:"unlock_"+index, id:"unlock_"+index, value:"Unlock"})
                  .click(function(e){
                    self._unlockAction(index);                   
					//alert(index);
                  })
                )
				)
				
			}
			tr.append($("<td />").css({display:(table == "awaiting_approval"  ? "table-cell" : "none")}).addClass("center")
                .append($("<input />",{type:"button", name:"view_logs_"+index, id:"view_logs_"+index, value:"View Logs"}).css({"margin-bottom":"10px"})
                  .click(function(e){
                    self._viewActionLog(index);                   
                  })
                )
				.append("<br />")
			    .append($("<input />",{type : "button", name : "approval_"+index, id : "approval_"+index, value : "Approve"})
			       .click(function(e){
				     if($("#approval_decline_"+index).length > 0)
				     {
				         $("#approval_decline_"+index).remove();
				     }
				     $("<div />",{id:"approval_decline_"+index})
				      .append($("<table />", {width:"100%"})
				        .append($("<tr />")
					      .append($("<th />",{html:"Response:"}))
					      .append($("<td />")
					        .append($("<textarea />",{cols:"30", rows:"10", name:"response", id:"response"}))		 
					      )
				        )
				        .append($("<tr />")
					      .append($("<td />",{colspan:"2", id:"response_message"}))
				        )
				      )
				      .dialog({
			           	  autoOpen	: true,
			           	  modal		: true, 
			           	  title		: "Approve/Decline Action A"+index,
			           	  width        : 500,				
			           	  position     : "top",	 	  
			           	  buttons		: {
           							"Approve"	: function()
           							{
           								jsDisplayResult("info", "info", "Approving action . . . <img src='../images/loaderA32.gif' />");
           								var data  	= {};
           								data.id   	= index;
           								data.response	= $("#response").val();
           								data.signoff	= $("#signoff :selected").val()
           								$.post("../class/request.php?action=ClientAction.approveAction",
           								{ data : data },function( response ){
           										if( response.error )
           										{						 										
           											jsDisplayResult("error", "error", response.text);
           										} else {
           											jsDisplayResult("ok", "ok", response.text);
           								               self._getActions();
           										}					 										
           								},"json")
           								$("#approval_decline_"+index).dialog("destroy")
						           	    $("#approval_decline_"+index).remove();
           							} , 
           							
           							"Decline"	: function()
           							{
           								if($("#response").val() == ""){
           									dialogResult("response_message", "Please enter the response", "error" , "error");
           									return false;
           								} else {
           									jsDisplayResult("info", "info", "Declining action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
           									data.id   		= index;
           									data.response	= $("#response").val();
           									data.signoff	= 1
           									$.post("../class/request.php?action=ClientAction.declineAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json")
           									$("#approval_decline_"+index).dialog("destroy")
						           	          $("#approval_decline_"+index).remove();
           								}
           							}	
		           		} ,
		           		open           : function(event, ui)
		           		{
                               var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                               var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                               var approveBtn = buttons[0];
                               var declineBtn = buttons[1];
                               $(approveBtn).css({"color":"#090"});
                               $(declineBtn).css({"color":"red"});
                               $(this).css("height", "auto");
		           		},
		           	     close       :  function(event, ui)
		           	     {
						     $("#approval_decline_"+index).dialog("destroy")
		           	          $("#approval_decline_"+index).remove();	      	          
		           	     }					  
		           	
				      }) 
				      e.preventDefault();
			       })
			    )		
			)
			$("#"+table).append( tr );
		})
		
	} , 
	
	_displayHeaders	: function( headers, table, showTr )
	{
		var self = this;
		var tr   = $("<tr />").addClass("approval")
		$.each( headers, function( key, head){
			tr.append($("<th />",{html:head}))
		});	
		if(table=="approved_actions") {
			tr.append("<th></th>");
		}
		tr.append($("<th />").css({display:((table == "awaiting_approval" && showTr)   ? "table-cell" : "none")}))		
		$("#"+table).append( tr )
	} , 
	
	_viewActionLog			: function(actionId)
	{
       var self = this;
       if($("#viewlogdialog").length > 0)
       {
         $("#viewlogdialog").remove(); 
       }
       
       $("<div />",{id:"viewlogdialog"}).css({height:"100%"})
       .append($("<table />",{width:"100%", id:"action_log_table"})
         .append($("<tr />")
           .append($("<th />",{html:"Date/Time"}))
           .append($("<th />",{html:"User"}))
           .append($("<th />",{html:"Change Log"}))
           .append($("<th />",{html:"Status"}))
         )        
       ).dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Action A"+actionId+" Activity Log ",
                width       : "auto",
                buttons     : {
                                "Ok"    : function()
                                {
                                    $("#viewlogdialog").remove();                               
                                }
                } ,
                close       : function()
                {
                   $("#viewlogdialog").remove();                
                } ,
                open        : function()
                {
                   $.get("../class/request.php?action=ClientAction.getActionUpdateLog", {
                    action_id : actionId
                   }, function(actionUpdates){
                      $("#action_log_table").append(actionUpdates);
                   },"html");           
                   $(this).css("height", "auto");  
                }
                      
       })
	} ,
		
	_unlockAction : function(index) {
		var self = this;
		/*if(confirm("Are you sure you wish to remove the Approval status of Action A"+index+"?")==true) {
           									jsDisplayResult("info", "info", "Unlocking action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
											data.id = index;
           									data.response	= "testing unlock";
           									data.signoff	= 1;
           									$.post("../class/request.php?action=ClientAction.unlockAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json");
		}*/
		if($("#approval_decline_"+index).length > 0)
				     {
				         $("#approval_decline_"+index).remove();
				     }
				     $("<div />",{id:"approval_decline_"+index})
					 .append("<p>Are you sure you wish to reverse the Approval of Action A"+index+"?</p>")
				      .append($("<table />", {width:"100%"})
				        .append($("<tr />")
					      .append($("<th />",{html:"Reason:"}))
					      .append($("<td />")
					        .append($("<textarea />",{cols:"30", rows:"10", name:"response", id:"response"}))		 
					      )
				        )
				        .append($("<tr />")
					      .append($("<td />",{colspan:"2", id:"response_message"}))
				        )
				      )
				      .dialog({
			           	  autoOpen	: true,
			           	  modal		: true, 
			           	  title		: "Unlock Action A"+index,
			           	  width        : 500,				
			           	  position     : "top",	 	  
			           	  buttons		: {
           							"Yes"	: function()
           							{
           								if($("#response").val() == ""){
           									dialogResult("response_message", "Please enter a reason for the reversal.", "error" , "error");
           									return false;
           								} else {
           									jsDisplayResult("info", "info", "Unlocking action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
           									data.id   		= index;
           									data.response	= $("#response").val();
           									data.signoff	= 1
           									$.post("../class/request.php?action=ClientAction.unlockAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json")
           									$("#approval_decline_"+index).dialog("destroy")
						           	          $("#approval_decline_"+index).remove();
           								}
           							}	,
           							"No"	: function()
           							{
           								$("#approval_decline_"+index).dialog("destroy")
						           	    $("#approval_decline_"+index).remove();
           							} 
           							
		           		} ,
		           		open           : function(event, ui)
		           		{
                               var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                               var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                               var approveBtn = buttons[0];
                               var declineBtn = buttons[1];
                               $(approveBtn).css({"color":"#090"});
                               $(declineBtn).css({"color":"red"});
                               $(this).css("height", "auto");
		           		},
		           	     close       :  function(event, ui)
		           	     {
						     $("#approval_decline_"+index).dialog("destroy")
		           	          $("#approval_decline_"+index).remove();	      	          
		           	     }					  
		           	
				      }) 
	  }
});
