$(function(){
	
	$(".request_guidance").click(function(){
		if( $("#request_guidance").length > 0){
			$("#request_guidance").remove();
		}
		$.post("../class/request.php?action=NewLegislation.guidanceData", { legislationId : $("#legislationid").val() }, function( response ){
			$("<div />",{id:"request_guidance"})
			.append($("<table />")
			   .append($("<tr />")
				 .append($("<th />",{html:"Request Submitted by:"}))
				 .append($("<td />",{html:response.request_by}))
			   )		
			   .append($("<tr />")
				 .append($("<th />",{html:"Request Company:"}))
				 .append($("<td />",{html:response.request}))
			   )		
			   .append($("<tr />")
				 .append($("<th />",{html:"Business Patner:"}))
				 .append($("<td />",{html:response.bus_partner}))
			   )						   
			   .append($("<tr />")
				 .append($("<th />",{html:"Legislation Owner:"}))
				 .append($("<td />",{html:response.leg_owner}))
			  )	
			   .append($("<tr />")
				 .append($("<th />",{html:"Legislation:"}))
				 .append($("<td />",{html:response.legislation}))
				)				  
			   .append($("<tr />")
				 .append($("<th />",{html:"Contact Number:"}))
				 .append($("<td />")
				    .append($("<input />",{type:"text", name:"contact", id:"contact"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Message:"}))
				 .append($("<td />")
				   .append($("<textarea />",{cols:"30", rows:"7", id:"message", name:"message"}))		 
				 )
			   )		   
			)
			.dialog({
					autoOpen	: true,
					modal		: true,
					width		: "400",
					title		: "Request Legal Guidance",
					buttons		: {
									"Accept" : function()
									{
										var contact = $("#contact").val();
										var message = $("#message").val()
										jsDisplayResult("info", "info", "Sending email   . . . <img src='../images/loaderA32.gif' />");
										$.post("../class/request.php?action=NewLegislation.requestGuidance", { legislationId : $("#legislationid").val(), contact : contact, message : message } , function( response ){
											if( response.error ) {
												jsDisplayResult("error", "error", response.text);		
											} else {					
												jsDisplayResult("ok", "ok", response.text);
											}
										},"json");
										$(this).dialog("destroy")
									} , 
									
									"Decline" : function()
									{
										$(this).dialog("destroy")
									}
					
								}			
			})
			
		},"json");

	});	
	
});
