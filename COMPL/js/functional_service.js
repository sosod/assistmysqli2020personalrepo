/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/16/11
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
// JavaScript Document
$(function(){
	
	 if( $("#functionalserviceid").val() == undefined){
		 FunctionalService.get() 
		 //FunctionalService.getFunctionalService();
	 }

	$("#assign_user").click(function(){
		FunctionalService.save();
		return false;
	});

	$("#edit").click(function(){
		FunctionalService.edit();
		return false;
	});

	$("#update").click(function(){
		FunctionalService.update();
		return false;
	});
	
	$("#add").click(function(){
		$("<div />",{id:"newfunctionalservice"})
		.append($("<table />",{width:"100%"})
		  .append($("<tr />")
			.append($("<th />",{html:"Functional Service title:"}))
			.append($("<td />")
			  .append($("<textarea />",{cols:"30", rows:"5", id:"name"}))		
			)
		  )	
		  .append($("<tr />")
			.append($("<td />",{colspan:"2"})
			  .append($("<p />")
				.append($("<span />",{id:"message"}))	  
			  )		
			)	  
		  )
		)
		.dialog({
			title 		: "Add new functional service",
			autoOpen	: true,	
			modal		: true,
			width		: "500px",
			position	: "top",
			buttons		: {
							"Save"	: function()
							{
								if( $("#name").val() == "")
								{
									$("#message").html("Please enter the functional service name")
									return false;
								} else {
									FunctionalService.saveNew();
								}					
							}  ,
							
							"Cancel"	: function()
							{
								$("#newfunctionalservice").dialog("destroy")
								$("#newfunctionalservice").remove();
							}
						  }
		})
		return false;
	})
	/*
	$("#functional_service_title").change(function(){
	  $("<div />",{html:"Does this functional service has users in the list", id:"hasusers"})
	   .dialog({
	   			autoOpen : true,
	   			modal 	: true,
	   			position : "top",
	   			width    : "300px",
	   			buttons	: {
	   							"Yes"	: function()
	   							{
	   								$("#hasusers").dialog("destroy");
	   								$("#hasusers").remove();				
	   							} , 
	   							"No"		: function()
	   							{
   								  $("#users").empty();
   								  $("#users").prepend($("<option />",{html:$("#functional_service_title :selected").text()}));
   								  $("#nouserinlist").val($("#functional_service_title :selected").text())	  
   								  $("#hasusers").dialog("destroy");
   								  $("#hasusers").remove();	   							
	   							}	   							
	   			}	
	   })
	  return false;
	});*/
		
});

var FunctionalService 	= {
	get 		: function()
	{
		$("#functional_service_title").empty()
		$("#functional_service_title").append($("<option />",{html:"--please select--", value:""}))
		$.post("../class/request.php?action=FunctionalServiceManager.getFunctionalServicesTitles", function( response ){
			$.each( response, function( index, owner){
			   if(owner.used)
			   {
			     $("#functional_service_title").append($("<option />",{text:owner.name, value:index, disabled:"disabled"}))	
			   } else {
			     $("#functional_service_title").append($("<option />",{text:owner.name, value:index}))	
			   }
			})												  											  
		},"json");
		
		$("#master_functional_service").empty();
		$("#master_functional_service").append($("<option />",{html:"--please select--", value:""}))
		$.post("../class/request.php?action=FunctionalServiceManager.getClientFunctionalServices", function( response ){
			$.each( response, function( index, owner){
				$("#master_functional_service").append($("<option />",{text:owner, value:index}))	
			})												  											  
		},"json");

		$(".functionals").remove();
		$.post("../class/request.php?action=FunctionalServiceManager.getAll", function(response){
		   if($.isEmptyObject(response))
		   {
		     $("#functional_service_table").append($("<tr />").addClass("functionals")
		       .append($("<td />",{colspan:"6", html:"There are no matches of master functional service title and client functional services"}))
		     )
		   } else {
			$.each( response, function( index, freq){
				FunctionalService.display( freq )
			})
		   }
		},"json");
		
	} ,

	saveNew		: function()
	{
		$.post("../class/request.php?action=SetupManager.newFunctionalService",{
			data : { name:$("#name").val() }
		},function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				FunctionalService.get();
				jsDisplayResult("ok", "ok", response.text);
				$("#newfunctionalservice").dialog("destroy")
				$("#newfunctionalservice").remove();
			}		
		},"json");	
	} , 
	
	save		: function()
	{
		var data 	 = {};
		data.ref 	 = $("#functional_service_title :selected").val();
		data.master_id = $("#master_functional_service :selected").val();
		data.nouserinlist = $("#nouserinlist").val();
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please selected the functional service title");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please select the master functional service");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=FunctionalServiceMatch.saveFunctionalService", { data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					FunctionalService.get()
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}
	} ,

	edit		: function()
	{
		var data 		 = {};
		data.ref 	 = $("#functional_service_title :selected").val();
		data.master_id = $("#master_functional_service :selected").val();
		data.id		 = $("#functionalserviceid").val()	
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please selected the functional service title");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please select the functional service");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=FunctionalServiceMatch.updateFunctionalService", { data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}

	} ,

	update		: function()
	{
		var data 	 = {}
		data.status	 = $("#status :selected").val()
		data.id		 = $("#functionalserviceid").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=FunctionalServiceMatch.updateFunctionalService", {  data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
		}, "json");
	} ,

	display		: function( functionalservice )
	{
		$("#functional_service_table")
		 .append($("<tr />",{id:"tr_"+functionalservice.id}).addClass("functionals")
		   .append($("<td />",{html:functionalservice.id}))
		   .append($("<td />",{html:functionalservice.title}))
		   .append($("<td />",{html:functionalservice.masterTitle}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+functionalservice.id, id:"edit_"+functionalservice.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+functionalservice.id, id:"del_"+functionalservice.id, value:"Del"}))
			)
		   .append($("<td />")
			.append($("<span />",{html:((functionalservice.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+functionalservice.id, id:"change_"+functionalservice.id, value:"Change Status"}))
		    )
		 )

		 $("#edit_"+functionalservice.id).live("click", function(){
			document.location.href = "edit_functional_service.php?id="+functionalservice.id;
			return false;
		  });

		 $("#del_"+functionalservice.id).live("click", function(){
			var data 	 = {};
			data.status  = 2;
			data.id 	 = functionalservice.id 
			if( confirm("Are you sure you want to delete this functional service")){
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=FunctionalServiceMatch.deleteFunctionalService",
				{ data : data }, function(response){											  	
					if( response.error ) {
					  jsDisplayResult("error", "error", response.text);		
					} else {					
					  $("#tr_"+functionalservice.id).fadeOut();
					  jsDisplayResult("ok", "ok", response.text);
					  FunctionalService.get();
					}
				}, "json");
			}
			return false;
		  });

		 $("#change_"+functionalservice.id).live("click", function(){
			document.location.href = "change_functional_service.php?id="+functionalservice.id;
		 });
	} ,


	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");
		});
	}



}
