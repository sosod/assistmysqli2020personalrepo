$.widget("ui.deliverable",{

     options		: {
	     importDeliverable	    : false,
	     addActions	            : false,
	     createActions          : false,
	     editDeliverable	    : false,
	     editActions		    : false,
	     updateDeliverable	    : false,
	     updateAction		    : false,
	     assuranceDeliverable   : false,
	     assuranceAction	    : false,
	     reassignDeliverable	: false,
	     tableRef			    : "deliverable_"+(Math.floor(Math.random(55) * 98)),
	     url 				    : "../class/request.php?action=Deliverable.getDeliverables",
	     start			        : 0,
	     current			    : 1,
	     total			        : 0,
	     limit 			        : 10,
	     legislationId		    : "",
	     usedIds 			    : [],
	     reference 		        : "",
	     actionProgress		    : [],
	     legOptions             :{},
	     eventActivation        : false,
	     isLegislationOwner     : false,
	     deliverableStatus      : {},
	     section                : "",
	     page				    : "",
	     users                  : {},
	     user                   : "",
	     autoLoad               : true,
		 filterByFinancialYear  : false,
		 filterByLegislation    : false, 
		 searchtext             : "",
		 searchDeliverable      : false,
		 delStatuses            : false,
		 deliverableid          : 0 
     }, 
     
     _init		: function()
     {
         if(this.options.autoLoad)
         {
           this._getDeliverable();
         }          
     } ,
     
     _create		: function()
     {
        var self    = this,
            options = self.options;
	    var html    = [];
		html.push("<table width='100%' class='noborder'>");
		  html.push("<tr>");
		    html.push("<td class='noborder'>");
		      html.push("<table class='noborder'>");
                if(self.options.filterByFinancialYear)
                {
		            html.push("<tr>");
		              html.push("<th style='text-align:left;'>Financial Year:</th>");
                      html.push("<td class='noborder'>");
                        html.push("<select id='financialyear' name='financialyear' style='width:350px;' class='financial_year'>");
                          html.push("<option value=''>--please select--</option>");
                        html.push("</select>");
                      html.push("</td>");
		            html.push("</tr>");
		        }      
                if(self.options.reassignDeliverable)
                {
		            html.push("<tr>");
		              html.push("<th style='text-align:left;'>Select a user:</th>");
                      html.push("<td class='noborder'>");
                        html.push("<select id='user' name='user' style='width:350px;'>");
                          html.push("<option value=''>--please select--</option>");
                        html.push("</select>");
                      html.push("</td>");
		            html.push("</tr>");
		        }   
                if(self.options.filterByLegislation)
                {
		            html.push("<tr>");
		              html.push("<th style='text-align:left;'>Select legislation:</th>");
                      html.push("<td class='noborder'>");
                        html.push("<select id='legislationlist' name='legislationlist' style='width:350px;'>");
                          html.push("<option value=''>--please select--</option>");
                        html.push("</select>");
                      html.push("</td>");
		            html.push("</tr>");
		        }        
		      html.push("</table>");
		    html.push("</td'>");
		  html.push("</tr>");
		  html.push("<tr>");
		    html.push("<td class='noborder' id='td_"+self.options.tableRef+"'>");
		      html.push("<table id='"+self.options.tableRef+"' class='noborder' width='100%'>");
		        html.push("<tr>");
		           html.push("<td class='noborder'>");
                   html.push("</td>");
                html.push("</tr>");
		      html.push("</table>");
		    html.push("</td'>");
		  html.push("</tr>");
        html.push("</table>");	
	    $(this.element).append(html.join(' '));
        if(self.options.filterByFinancialYear)
        {
          self._financialYears();
        }
        
        if(self.options.reassignDeliverable)
        {
          self._loadUsers();
        } 
       
        $("#financialyear").live("change", function(e){
           self.options.financialyear = $(this).val();
           $("#message").hide();
           if(self.options.reassignDeliverable)
           {
             if(self.options.user !== "")
             {
               self.options.start = 0;
               self.options.limit = 10;
               self.options.current = 1;
               self.options.total = 0;
             } 
           } else {
              $("#legislationlist").empty();
              self._loadLegislations($(this).val());
              e.preventDefault();           
           }
           self._getDeliverable();
        });           
       
        $("#user").live("change", function(){
          self.options.user = $(this).val();
          self.options.start = 0;
          self.options.limit = 10;
          self.options.current = 1;
          self.options.total = 0;
          $("#message").hide();
          self._getDeliverable();   
        });
            
	    $("#legislationlist").live("change", function(){
          self.options.legislationId = $(this).val();
          self.options.start = 0;
          self.options.limit = 10;
          self.options.current = 1;
          self.options.total = 0;
          $("#message").hide();
          self._getDeliverable();   
	    });
     }, 
     
     _loadUsers          : function()
     {
        var self = this,
            options = self.options;
        if($.isEmptyObject(options.users))
        {
             $.getJSON("../class/request.php?action=User.getAll",function(users){
               var u_html = [];
               u_html.push("<option value=''>--please select--</option>");
               if(!$.isEmptyObject(users))
               {
                  options.users = users;
                  $.each(users, function(index, user){
                    u_html.push("<option value='"+index+"'>"+user.user+"</option>");
                  });
               }
               $("select#user").html( u_html.join(' ') );       
             },"json");          
        } else {
           
        }
     },
     
     _financialYears     : function()
     {
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAll",function(financialYears) {
	          var f_html = [];
	          if($.isEmptyObject(financialYears))
	          {
	             f_html.push("<option value=''>--please select--</option>");
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear) {
	               if(self.options.financialyear == financialYear.id)
	               {
	                  var text = " ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date;
	                  f_html.push("<option value='"+financialYear.id+"' selected='selected'>"+text+"</option>");         
	               } else {
                      var text = " ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date;
                      f_html.push("<option value='"+financialYear.id+"'>"+text+"</option>");         
	               }
	             });
	          }
               $(".financial_year").html( f_html.join(' ') );
	        });
	   } 
     } ,
     
     _loadLegislations    : function(financialyear)
     {
        var self = this, 
            options = self.options;
         
        $("#legislationlist").empty();      
        var tmpPage = "";
        if(self.options.page == "edit")
        {
          tmpPage = "update";
        } else {
           tmpPage = self.options.page; 
        }
        $.getJSON("../class/request.php?action=Legislation.getLegislations",
           {data:{financial_year:financialyear, section:self.options.section, page:tmpPage}}, function(legislations){
            var l_html = [];
              l_html.push("<option value=''>--please select--</option>");
              $.each(legislations, function(index, legislation){
                 l_html.push("<option value='"+legislation.id+"'>[L"+legislation.id+"] "+legislation.name+"</option>");
              });        
            $("#legislationlist").html( l_html.join(' ') );
        });
     } ,   
     
     _getDeliverable	: function()
     {              
	     var self = this;
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );
	     $.getJSON( self.options.url ,{
		     options : {
		          start 		 : self.options.start,
		          limit 		 : self.options.limit,
                  legislation_id : self.options.legislationId,
                  reference      : self.options.reference,
                  page           : self.options.page,
                  section        : self.options.section,
                  main_event     : self.options.eventid,
                  user           : self.options.user,
                  financial_year : self.options.financialyear, 
                  searchtext     : self.options.searchtext,
                  deliverableid  : self.options.deliverableid		                     
               }
	     }, function(deliverableResponse) {
	         $("#loadingDiv").remove();
	           //$("#"+self.options.tableId).html("Something here ");
	         if(deliverableResponse.hasOwnProperty('isOwner'))
	         {
	            self.options.isLegislationOwner = deliverableResponse.isOwner;	            
	         }
	         self.options.deliverableStatus = deliverableResponse.deliverableStatus;
		     self.options.actionProgress = deliverableResponse.deliverableActionProgress;
		     if(deliverableResponse.hasOwnProperty('deliverablesId'))
		     {
		        self.options.delStatuses = deliverableResponse.deliverablesId;
		     }
		     
		     $("#"+self.options.tableRef).html("");
		     if( $("#"+self.options.tableRef).length < 1)
		     {
		        self._createTable();
		     }
		     self._displayPaging(deliverableResponse.total, deliverableResponse.columns, deliverableResponse.total_time);	     
		     self._displayHeaders(deliverableResponse.headers, false);	     
		     if( $.isEmptyObject(deliverableResponse.deliverable)) {
     			 var  _html = [];
			    _html.push("<tr>")
			      _html.push("<td colspan='"+(parseInt(deliverableResponse.columns, 10))+"'>");
			         _html.push("<p class='ui-state-highlight' style='padding:5px; border:0; text-align:center; width:500px;'>");
			           _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
			             _html.push("<span style='float:left;'>");
			               if(deliverableResponse.hasOwnProperty('error_message'))
			               {
			                 _html.push(deliverableResponse.error_message);
			               } else {
			                 _html.push("There are no active deliverables allocated to you for the selected financial year or legislations");
			               }
			           _html.push("</span>");
			         _html.push("</p>");
			      _html.push("</td>");
			    _html.push("</tr>");
			     $("#"+self.options.tableRef).append( _html.join(' ') );
		     } else {
		       self.options.total   = deliverableResponse.total;
		       self.options.usedIds = deliverableResponse.usedId;
		       self._display(deliverableResponse.deliverable, deliverableResponse.columns, deliverableResponse.isDeliverableOwner);
		     }			
	     });				
     } ,  
     
     _createTable    : function()
     {
       var self = this;
        $(self.element).append($("<table />", {id:self.options.tableRef}))
     } , 
     

     _display		: function(deliverables, columns, isDeliverableOwner)
     {	              
        var self = this;
	    $.each(deliverables, function( index, deliverable){
	         var html = [];
	         html.push("<tr>");
	         $.each(deliverable, function( key, val) {
	            html.push("<td>"+val+"</td>");
	         }); //each
	         var btnOptions = self._displayButtonOptions(index, isDeliverableOwner);
             var deliverableStatus = "";		
             if(self.options.delStatuses.hasOwnProperty(index))
             {
                deliverableStatus = self.options.delStatuses[index]['status'];
             }
	         //add
             if(self.options.addActions)
             {
                html.push("<td>");
                  html.push("<input type='button' name='add_action_"+index+"' id='add_action_"+index+"' value='Add Action' />");
                  $("#add_action_"+index).live("click", function(e){
                    document.location.href = "create_action.php?id="+index+"&";
                    e.stopImmediatePropagation();
                  });
                html.push("</td>");
             }
             //update
             if(self.options.updateDeliverable)
             {
                html.push("<td>");
                  html.push("<input type='button' name='update_deliverable_"+index+"' id='update_deliverable_"+index+"' value='Update Deliverable' />");
                  $("#update_deliverable_"+index).live("click", function(e){
                    document.location.href = "update_deliverable.php?id="+index+"&";
                    e.stopImmediatePropagation();
                  });
                  html.push("<br />");
                  html.push("<input type='button' name='update_action_"+index+"' id='update_action_"+index+"' value='Update Action' />");
                  $("#update_action_"+index).live("click", function(e){
                    document.location.href = "update_actions.php?id="+index+"&";
                    e.stopImmediatePropagation();
                  });
                html.push("</td>");
             } 
             //edit
             if(self.options.editDeliverable)
             {
                html.push("<td>");
                  if(btnOptions.btnEditDel)
                  {
                      html.push("<input type='button' name='edit_deliverable_"+index+"' id='edit_deliverable_"+index+"' value='Edit Deliverable' />");
                      $("#edit_deliverable_"+index).live("click", function(e){
			             if( self.options.page == "add_deliverable"){
				             document.location.href = "../manage/edit_deliverable.php?id="+index+"&";
			             } else {
				             document.location.href = "edit_deliverable.php?id="+index+"&";					
			             }
                        e.stopImmediatePropagation();
                      });
                  }
                  html.push("<br />");
                  if(btnOptions.editActions)
                  {
                      html.push("<input type='button' name='edit_action_"+index+"' id='edit_action_"+index+"' value='Edit Action' />");
                      $("#edit_action_"+index).live("click", function(e){
			             if(self.options.editActions && self.options.addActions){
				             document.location.href = "../manage/edit_actions.php?id="+index+"&";
			             } else {
				             document.location.href = "edit_actions.php?id="+index+"&";
			             }
                        e.stopImmediatePropagation();
                      });
                  }
                  if(btnOptions.btnAddActions)
                  {
                      html.push("<input type='button' name='add_action_"+index+"' id='add_action_"+index+"' value='Add Action' />");
                      $("#add_action_"+index).live("click", function(e){
                        document.location.href = "create_action.php?id="+index+"&";
                        e.stopImmediatePropagation();
                      });
                  }                  
                html.push("</td>");
             }
              //assurance
             if(self.options.assuranceDeliverable)
             {
                html.push("<td>");
                  if(btnOptions.btnEditDel)
                  {
                      html.push("<input type='button' name='assurance_deliverable_"+index+"' id='assurance_deliverable_"+index+"' value='Assurance Deliverable' />");
                      $("#assurance_deliverable_"+index).live("click", function(e){
	                    document.location.href = "assurance_deliverable.php?id="+index+"&";
                        e.stopImmediatePropagation();
                      });
                  }
                  html.push("<br />");
                  html.push("<input type='button' name='assurance_action_"+index+"' id='assurance_action_"+index+"' value='Assurance Action' />");
                  $("#assurance_action_"+index).live("click", function(e) {
                    document.location.href = "assurance_actions.php?id="+index+"&";
                    e.stopImmediatePropagation();
                  });          
                html.push("</td>");
             }             
             if(self.options.reassignDeliverable)
             {
                html.push("<td>");
                  html.push("<select name='reassign_"+index+"' id='reassign_"+index+"' class='userslist' />");
                  html.push("</select>");
                  html.push("<input type='hidden' name='delistatus_"+index+"' id='delistatus_"+index+"' value='"+deliverableStatus+"' class='usersstatuses' />");
                  $("#add_action_"+index).live("click", function(e){
                    document.location.href = "create_action.php?id="+index+"&";
                    e.stopImmediatePropagation();
                  });
                html.push("</td>");
             }              	         
	         html.push("</tr>");
	         $("#"+self.options.tableRef).append( html.join(' ') );
             if(self.options.users)
             {
               $.each(self.options.users, function(userIndex, user) {
                  var r_html = [];
                  r_html.push("<option value=''>--select user--</option>");
                  if((self.options.user === userIndex) || (self.options.delStatuses[index]['owner'] == userIndex))
                  {
                     r_html.push("<option value='"+userIndex+"' selected='selected'>"+user.user+"</option>");
                  } else {
                     r_html.push("<option value='"+userIndex+"' selected='selected'>"+user.user+"</option>");                   
                  }
                  $("#reassign_"+index).html( r_html.join(' ') );
               });
             } 	         
	    }); //end each 
	    
	    var _html = [];
	    if(self.options.eventActivation)
	    {
	        _html.push("<tr>");
             _html.push("<td>");
                 _html.push("<input type='button' name='activate' id='activate' value='Activate' class='isubmit' />");
                  $("#activate").live("click", function(e){
                    self._activateDeliverables()
                    e.stopImmediatePropagation();
                  });      
            _html.push("</td>");    	    
          _html.push("</tr>");
        }
	    if(self.options.reassignDeliverable)
	    {
	        _html.push("<tr>");
             _html.push("<td>");
                 _html.push("<input type='button' name='reassign' id='reassign' value='Save Changes' class='isubmit' />");
                  $("#reassign").live("click", function(e){
                    self._reassignDeliverables();
                    e.stopImmediatePropagation();
                  });      
            _html.push("</td>");    	    
          _html.push("</tr>");
        }
     } ,
     
     _reassignDeliverables    : function()
     {
        var self = this;
        jsDisplayResult("info", "info", "Re-assigning deliverables . . . .<img src='../images/loaderA32.gif' />" );
        $.post("../class/request.php?action=ClientDeliverable.reAssign", {
          data      : $(".userslist").serializeArray(),
          statuses  : $(".usersstatuses").serializeArray()
        }, function(response) {
          if(response.error)
          {
             jsDisplayResult("error","error",response.text );                            
          } else {
             if(response.updated)
             {
               jsDisplayResult("info", "info", response.text);                                       
             } else {
               jsDisplayResult("ok","ok",response.text);                                       
               self._getDeliverables();             
             }
          }
        },"json");  
     } ,     
     
     _activateDeliverables      : function()
     {
         var self = this;
         $("<div />",{id:"activate_eventdeliverable"})
         .append($("<p />")
           .append($("<span />",{html:"You are now activating the deliverables checked for this event and deliverable unchecked will be inactive"}))
         ).dialog({
                   autoOpen  : true, 
                   modal     : true, 
                   title     : "Activate Deliverables",
                   position  : "top",
                   buttons   : {
                                 "Save Changes"   : function()
                                 {
                                    var _eventDeliverable =  [];
                                    var eventDeliverable =  [];
                                    var eDel  = [];
                                    var _eDel = [];
                                    $(".eventactivation").each(function(index, deliverable){
                                       if($(this).is(":checked"))
                                       {
                                          eDel.push( this.id );
                                       } else {
                                          _eDel.push( this.id );
                                       }
                                    });
                                    jsDisplayResult("info", "info", "Activating deliverables ...<img src='../images/loaderA32.gif' />");
                                    $.post("../class/request.php?action=ClientDeliverable.activateDeliverable", 
                                    {
                                        data: {checked:eDel, unchecked:_eDel}
                                    },function( response ){
                                      if( response.error)
                                      {
                                        jsDisplayResult("error", "error", response.text);
                                      } else {
                                        jsDisplayResult("ok", "ok", response.text);
                                      } 
                                    },"json");                      
                                       $("#activate_eventdeliverable").dialog("destroy").remove();             
                                 } , 
                                 "Cancel"       : function()
                                 {
                                    $("#activate_eventdeliverable").dialog("destroy").remove();                   
                                 }
                   }
         });     

     } ,
     
    
    _displayHeaders     : function(headers, extraHeader)
    {
       var self = this;
       var html = [];
       html.push("<tr>");
       if(self.options.eventActivation)
       {
           html.push("<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>");
       }
       $.each(headers, function(key, header){
          html.push("<th>"+header+"</th>");
       });
       if(self.options.reassignDeliverable)
       {
           html.push("<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>");
       }
       if(self.options.addActions)
       {
          html.push("<th>&nbsp;&nbsp;</th>");
       }
       if(self.options.assuranceDeliverable)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.editDeliverable)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.updateDeliverable)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.reassignActions)
       {
          html.push("<th>&nbsp;</th>");
       }            
       html.push("</tr>");
       $("#"+self.options.tableRef).append( html.join(' ') ); 
    } ,
    

    _displayPaging	: function(total, columns, total_time)
	{
		var self  = this;
        var html  = [];
		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		html.push("<tr>");		    
		  html.push("<td colspan='"+(columns + 1)+"'>");
		    html.push("<input type='button' value=' |< ' id='first' name='first' />");
		    html.push("<input type='button' value=' < ' id='previous' name='previous' />");
            if(pages != 0)
            {
               html.push("Page <select id='select_page' name='select_page'>");
               for(var p=1; p<=pages; p++)
               {
                  html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
               }
               html.push("</select>");		    
            } else {
               html.push("Page <select id='select_page' name='select_page' disabled='disabled'>");
               html.push("</select>");		    
            }
		    html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
		    //html.push(self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
		    html.push("<input type='button' value=' > ' id='next' name='next' />");
		    html.push("<input type='button' value=' >| ' id='last' name='last' />");
          html.push("&nbsp;");
              html.push("<span style='float:right;'>");
                html.push("<b>Quick Search for Deliverable:</b> D <input type='text' name='q_id' id='q_id' value='' />");
                 html.push("&nbsp;&nbsp;&nbsp;");
                html.push("<input type='button' name='search' id='search' value='Go' />");
              html.push("</span>");
          html.push("</td>");
		html.push("</tr>");
		$("#"+self.options.tableRef).append(html.join(' '));
		
		
		$("#select_page").live("change", function(){
		   if($(this).val() !== "")
		   {
		      self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
		      self.options.current = parseFloat($(this).val());
		      self._getDeliverable();
		   }
		});
        $("#search").live("click", function(e){
           var deliverable_id = $("#q_id").val();
           if(deliverable_id != "")
           {
              self.options.deliverableid = $("#q_id").val();
              self._get();
           } else {
              $("#q_id").addClass('ui-state-error');
              $("#q_id").focus().select();
           } 
           e.preventDefault();
        });
		
         if(self.options.current < 2)
         {
             $("#first").attr('disabled', 'disabled');
             $("#previous").attr('disabled', 'disabled');		     
         }
         if((self.options.current == pages || pages == 0))
         {
             $("#next").attr('disabled', 'disabled');
             $("#last").attr('disabled', 'disabled');		     
         }				
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
	} ,		
	
	_getNext  			: function(self) 
	{
		self.options.current = (self.options.current + 1);
		self.options.start   = (self.options.current - 1) * self.options.limit;
		self._getDeliverable();
	},	
	
	_getLast  			: function(self) 
	{
		self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );			
		this._getDeliverable();
	},	
	_getPrevious    	: function(self) 
	{
		self.options.current  = parseFloat(self.options.current) - 1;
		self.options.start 	= (self.options.current-1) * self.options.limit;
		this._getDeliverable();			
	},	
	_getFirst  			: function(self)
	{
		self.options.current  = 1;
		self.options.start 	= 0;
		this._getDeliverable();				
	},

     _indexOf       : function(arrayData, obj)
     {
        for(var i = 0; i < arrayData.length; i++)
        {
           if(arrayData[i] == obj)
           {
             return i;     
           }
        }
        return -1;
     } ,
     
    _displayButtonOptions           : function(deliverableId, isDeliverableOwner)
     {
          var self = this;
          var btnDisplayOptions = {};
          /*
             displayed for legislation owner and deliverable responsible owner
          */
          btnDisplayOptions.btnAddActions = false;
          //show approve button
          btnDisplayOptions.btnApprove = false;
          //deliverable owner or legislation owner can edit deliverable
          //show under new pages
          //under manage pages show only for manually created legislations/deliverables
          btnDisplayOptions.btnEditDel = false;
         
          
          //if create actions sections
          if(self.options.createActions)
          {
             //if the user is deliverabla responsible person or legisaltion owner then show the button
             if(isDeliverableOwner[deliverableId] || self.options.isLegislationOwner)
             {
               btnDisplayOptions.btnAddActions = true;
             }
             if(self.options.section == "admin" || (self.options.page === "manage" && self.options.page === "edit"))
             {
               btnDisplayOptions.btnAddActions = true;
             }             
          }
          if(isDeliverableOwner[deliverableId] || self.options.isLegislationOwner)
          {
              
             if((self.options.deliverableStatus[deliverableId] & 256) != 256)
             {
               btnDisplayOptions.btnEditDel = true;
             }
             
          }	

          if((self.options.page === "new") || (self.options.section === "admin" || self.options.page == "edit"))
          {    
             btnDisplayOptions.btnEditDel = true;
          } 	     
          
          //if the progress is 100 , then show the approve button
          if(self.options.actionProgress[deliverableId] == "100")
          {
             btnDisplayOptions.btnApprove = true;  
          }

          return btnDisplayOptions;
     }     
});
