$.widget("ui.deliverable",{
     options		: {
	     importDeliverable	   : false,
	     addActions	        : false,
	     createActions          : false,
	     editDeliverable	   : false,
	     editActions		   : false,
	     updateDeliverable	   : false,
	     updateAction		   : false,
	     assuranceDeliverable   : false,
	     assuranceAction	   : false,
	     reassignDeliverable	   : false,
	     reassignActions        : false,
	     approval			   : false,
	     tableId			   : "deliverable_"+(Math.floor(Math.random(55) * 98)),
	     url 				   : "../class/request.php?action=Deliverable.getDeliverables",
	     start			   : 0,
	     current			   : 1,
	     total			   : 0,
	     limit 			   : 10,
	     legislationId		   : "",
	     usedIds 			   : [],
	     reference 		   : "",
	     page				   : "",
	     actionProgress		   : [],
	     legOptions             :{},
	     eventActivation        : false,
	     isLegislationOwner     : false,
	     deliverableStatus      : {},
	     section                : "",
	     users                  : {},
	     user                   : "",
	     autoLoad               : true,
		filterByFinancialYear : false,
		filterByLegislation   : false, 
		searchtext            : "",
		searchDeliverable     : false,
		delStatuses           : false    
     }, 

     _init		: function()
     {
         if(this.options.autoLoad)
         {
           this._getDeliverable();
         }          
     } , 

     _create		: function()
     {
	  var self 	= this,
	      options  = self.options;
       $(self.element).append($("<form />")
         .append($("<table />",{width:"100%"}).addClass("noborder")
           .append($("<tr />").css({"display":(options.filterByFinancialYear ? "block" : "none")})
             .append($("<th />",{html:"Filter by financial year", width:"300px"}).css({"text-align":"left"}).addClass("noborder"))
             .append($("<td />",{width:"300px"}).addClass("noborder")
               .append($("<select />",{id:"financialyear", name:"financialyear"}).addClass("financial_year"))
             )
           )           
           .append($("<tr />").css({"display":(options.reassignDeliverable ? "block" : "none")})
             .append($("<th />",{html:"Select a user", width:"300px"}).css({"text-align":"left"}).addClass("noborder"))
             .append($("<td />",{width:"300px"}).addClass("noborder")
               .append($("<select />",{id:"user", name:"user"}))
             )
           )
           .append($("<tr />").css({"display":(options.filterByLegislation ? "block" : "none")})
             .append($("<th />",{html:"Filter by legislation", width:"300px"}).css({"text-align":"left"}).addClass("noborder"))
             .append($("<td />",{width:"300px"}).addClass("noborder")
               .append($("<select />",{id:"legislationlist", name:"legislationlist"}))
             )
           )           
           .append($("<tr />")
             .append($("<td />",{colspan:"2"}).addClass("noborder")
               .append($("<table />",{id:self.options.tableId}))
             )
           )
         )
       )
        
       if(options.filterByFinancialYear)
       {
          self._financialYears();
       }
        
       if(options.reassignDeliverable)
       {
          self._loadUsers();
       } 
       
       $("#financialyear").live("change", function(e){
           self.options.financialyear = $(this).val();
           $("#message").hide();
           if(options.reassignDeliverable)
           {
             if(options.user !== "")
             {
               options.start = 0;
               options.limit = 10;
               options.current = 1;
               options.total = 0;
             } 
           } else {
              $("#legislationlist").empty();
              self._loadLegislations($(this).val());
              e.preventDefault();           
           }
           self._getDeliverable();
       });           
       
       $("#user").live("change", function(){
          options.user = $(this).val();
          options.start = 0;
          options.limit = 10;
          options.current = 1;
          options.total = 0;
          $("#message").hide();
          self._getDeliverable();   
       });
            
	  $("#legislationlist").live("change", function(){
          options.legislationId = $(this).val();
          options.start = 0;
          options.limit = 10;
          options.current = 1;
          options.total = 0;
          $("#message").hide();
          self._getDeliverable();   
	  }); 
     } , 

     _loadUsers          : function()
     {
        var self = this,
            options = self.options;
        if($.isEmptyObject(options.users))
        {
             $("select#user").append($("<option />",{value:"", text:"--please select--"}))   
             $.getJSON("../class/request.php?action=User.getAll",function(users){
               if(!$.isEmptyObject(users))
               {
                  options.users = users;
                  $.each(users, function(index, user){
                    $("select#user").append($("<option />",{value:index, text:user.user})) 
                  })
               }       
             },"json");          
        } else {
           
        }
     },
     
     _financialYears     : function()
     {
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $(".financial_year").append($("<option />",{value:"", text:"--please select--"}));
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAllSorted",function(financialYears) {
	          if($.isEmptyObject(financialYears))
	          {
	              $(".financial_year").append($("<option />",{value:"", text:"--please select--"}))
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear) {
	               if(self.options.financialyear == financialYear.id)
	               {
                        $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date, selected:"selected"}))	                    
	               } else {
                      $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date}))	                    
	               }
	             });
	          }	   
	        });
	   } 
     } ,
     
     _loadLegislations    : function(financialyear)
     {
        var self = this, 
            options = self.options;
         
        $("#legislationlist").empty();    
        $("select#legislationlist").append($("<option />",{value:"", text:"--please select--"}))   
        var tmpPage = "";
        if(self.options.page == "edit")
        {
          tmpPage = "update";
        } else {
           tmpPage = self.options.page; 
        }
        $.getJSON("../class/request.php?action=Legislation.getLegislations",
           {data:{financial_year:financialyear, section:self.options.section, page:tmpPage}}, function(legislations){
          $.each(legislations, function(index, legislation){
             $("#legislationlist").append($("<option />",{text:"[L"+legislation.id+"] "+legislation.name, value:legislation.id}))
          });        
        });
     
     } ,

     _getDeliverable	: function()
     {              
	     var self = this;
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );
	     $.getJSON( self.options.url ,{
		     options : {
		          start 		: self.options.start,
		          limit 		: self.options.limit,
                    legislation_id : self.options.legislationId,
                    reference      : self.options.reference,
                    page           : self.options.page,
                    section        : self.options.section,
                    main_event     : self.options.eventid,
                    user           : self.options.user,
                    financial_year : self.options.financialyear, 
                    searchtext     : self.options.searchtext			                     
               }
	     }, function(deliverableResponse) {
	         $("#loadingDiv").remove();
	           //$("#"+self.options.tableId).html("Something here ");
	         if(deliverableResponse.hasOwnProperty('isOwner'))
	         {
	            self.options.isLegislationOwner = deliverableResponse.isOwner;	            
	         }
	         self.options.deliverableStatus = deliverableResponse.deliverableStatus;
		     self.options.actionProgress = deliverableResponse.deliverableActionProgress;
		     if(deliverableResponse.hasOwnProperty('deliverablesId'))
		     {
		        self.options.delStatuses = deliverableResponse.deliverablesId;
		     }
		     
		     $("#"+self.options.tableId).html("");
		     if( $("#"+self.options.tableId).length < 1)
		     {
		        self._createTable();
		     }		     
		     if( $.isEmptyObject(deliverableResponse.deliverable)) {
     			self._displayHeaders(deliverableResponse.headers, false);
			     $("#"+self.options.tableId).append($("<tr />")
				     .append($("<td />",{colspan:parseInt(deliverableResponse.columns, 10)+1, html:"There are no active deliverables allocated to you for the selected financial year or legislations"}))
			     );
		     } else {
		       self._displayPaging(deliverableResponse.total, deliverableResponse.columns, deliverableResponse.total_time);
		       self.options.total   = deliverableResponse.total;
		       self.options.usedIds = deliverableResponse.usedId; 
	            self._displayHeaders(deliverableResponse.headers, true);
		       self._display(deliverableResponse.deliverable, deliverableResponse.columns, deliverableResponse.isDeliverableOwner);
		     }			
	     });				
     } ,

     _createTable    : function()
     {
       var self = this;
        $(self.element).append($("<table />", {id:self.options.tableId}))
     } ,

     _display		: function(deliverables, columns, isDeliverableOwner)
     {	
	     var self = this;
	     $.each(deliverables , function(index, del ){
		     var tr = $("<tr />");

		     $.each(del, function( key, val) {
			   if(self.options.searchDeliverable)
			   { 
			      if((val !== null || val !== "") && typeof val == "string")
			      {
			         if(val.indexOf(self.options.searchtext) >= 0)
			         {
			            tr.append($("<td />",{html:"<span style='background-color:yellow'>"+val+"</span>"}));
			            //tr.append($("<td />",{html:(key == "action_progress" ? val+"%" : val)}));
			         } else {
			          tr.append($("<td />",{html:(key == "action_progress" ? val : val)}));
			         }			          
			      } else {
			         tr.append($("<td />",{html:(key == "action_progress" ? val : val)}));
			      }  
			   } else {
			     tr.append($("<td />",{html:(key == "action_progress" ? val : val)}));
			   }
		     });
		
		     var btnOptions = self._displayButtonOptions(index, isDeliverableOwner);
		     tr.append($("<td />").css({display:(self.options.addActions ? "table-cell" : "none")})
			     .append($("<input />",{type:"button", name:"add_action_"+index, id:"add_action_"+index, value:"Add Action"}))
		     );
		
		     //update
		     tr.append($("<td />").css({"display" : ( self.options.updateDeliverable ? "table-cell" : "none") })
		        .append($("<input />",{type:"button", id:"update_deliverable_"+index, name:"update_deliverable_"+index,value:"Update Deliverable"})
		          .addClass("imports")
		        )
		       .append($("<br />"))						
		       .append($("<input />",{type:"button", id:"update_action_"+index, name:"update_action_"+index, value:"Update Actions"})
		         .css({display:(self.options.updateActions ? "block" : "none")})
		       )		
		     );				

		     //edit
		     tr.append($("<td />").css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") })
		         .append($("<input />",{type:"button", id:"edit_deliverable_"+index, name:"edit_deliverable_"+index, value:"Edit Deliverable"})
		           .css({display:(btnOptions.btnEditDel ? "block" : "none")})
		          )
		         .append($("<br />"))						
		         .append($("<input />",{type:"button", id:"edit_action_"+index, name:"edit_action_"+index, value:"Edit Actions"})
		           .css({display:(self.options.editActions ? "block" : "none")})
		         )	
		         					  
	             .append($("<input />",{type:"button", name:"add_action_"+index, id:"add_action_"+index, value:"Add Actions"})
	               .css({display:btnOptions.btnAddActions ? "block" : "none"})
	             )
		     );								
		
		     //assurance			
		     tr.append($("<td />").css({"display" : ( self.options.assuranceDeliverable ? "table-cell" : "none") })
		         .append($("<input />",{type:"button", id:"assurance_deliverable_"+index, name:"assurance_deliverable_"+index, value:"Assurance Deliverable"})
		          .addClass("imports")
		         )
		         .append($("<br />"))						
		         .append($("<input />",{type:"button", id:"assurance_action_"+index, name:"assurance_action_"+index,value: "Assurance Actions"})
		           .addClass("imports")
		         )			    						
		     );	
		     var deliverableStatus = "";		
               if(self.options.delStatuses.hasOwnProperty(index))
               {
                   deliverableStatus = self.options.delStatuses[index]['status'];
               }
               
               tr.append($("<td />").css({"display" : ( self.options.reassignDeliverable ? "table-cell" : "none") })
                  .append($("<select />",{name:"reassign_"+index, id:"reassign_"+index}).addClass("userslist"))
                  .append($("<input />",{type:"hidden", id:"delistatus_"+index, name:"delistatus_"+index, value:deliverableStatus}).addClass('usersstatuses'))
               );
							

		     //approval
		     tr.append($("<td />").css({"display" : ( self.options.approval ? "table-cell" : "none") })
		         .append($("<input />",{type:"button", id:"approval_actions_"+index, name:"approval_actions_"+index, value:"Approve Actions"})
		          .css({display:(btnOptions.btnApprove ? "block" : "none")})
		         )
		     );							
			
		     $("#add_action_"+index).live("click", function(){
			     document.location.href = "create_action.php?id="+index+"&";
			     return false;
		     });					

		     $("#edit_deliverable_"+index).live("click", function(){
			     if( self.options.page == "add_deliverable"){
				     document.location.href = "../manage/edit_deliverable.php?id="+index+"&";
			     } else {
				     document.location.href = "edit_deliverable.php?id="+index+"&";					
			     }
			     return false;
		     });						
		
		     $("#edit_action_"+index).live("click", function(){
			     if(self.options.editActions && self.options.addActions){
				     document.location.href = "../manage/edit_actions.php?id="+index+"&";
			     } else {
				     document.location.href = "edit_actions.php?id="+index+"&";
			     }
			     return false;
		     });		
				
		     $("#reassigndeliverable_"+index).live("click", function(e){
			     self._reAssignDeliverable(index);
			     e.preventDefault();
		     });							
		
		     $("#reassignactions_"+index).live("click", function(){
			     document.location.href = "reassign_action.php?id="+index+"&";
			     return false;
		     });			
		
		     $("#update_deliverable_"+index).live("click", function(){
			     document.location.href = "update_deliverable.php?id="+index+"&";
			     return false;
		     });							

		     $("#update_action_"+index).live("click", function(){
			     document.location.href = "update_actions.php?id="+index+"&";
			     return false;
		     });													
		
		     $("#assurance_deliverable_"+index).live("click", function(){
			     document.location.href = "assurance_deliverable.php?id="+index+"&";
			     return false;
		     });							

		     $("#assurance_action_"+index).live("click", function(){
			     document.location.href = "assurance_actions.php?id="+index+"&";
			     return false;
		     });	
		
		     $("#approval_actions_"+index).live("click", function(){
			     document.location.href = "approval_actions.php?id="+index+"&";
			     return false;
		     });				
		     $("#"+self.options.tableId).append( tr );
		     
        
                if(self.options.users)
                {
                   $("#reassign_"+index).append($("<option />",{value:"", text:"--select user--"}))
                   $.each(self.options.users, function(userIndex, user) {
                      if((self.options.user === userIndex) || (self.options.delStatuses[index]['owner'] == userIndex))
                      {
                         $("#reassign_"+index).append($("<option />",{value:userIndex, text:user.user, selected:"selected"}))          
                      } else {
                         $("#reassign_"+index).append($("<option />",{value:userIndex, text:user.user}))                            
                      }
                   })
                }   		     
		     
	     });
      
      $("#"+self.options.tableId).append($("<tr />").css({display:(self.options.eventActivation ? "table-row" : "none")})
        .append($("<td />",{colspan:columns+1}).css({"text-align":"right"})
           .append($("<input />",{type:"button", name:"activate", id:"activate", value:"Activate"}).addClass("isubmit"))
        )     
      );
      
      $("#"+self.options.tableId).append($("<tr />").css({display:(self.options.reassignDeliverable ? "table-row" : "none")})
        .append($("<td />",{colspan:columns+1}).css({"text-align":"right"})
           .append($("<input />",{type:"button", name:"reassign", id:"reassign", value:"Save Changes"}).addClass("isubmit")
             .click(function(){
               self._reassignDeliverables();
             })
           )
        )     
      );
      
            
      $("#activate").click(function(){
         $("<div />",{id:"activate_eventdeliverable"})
         .append($("<p />")
           .append($("<span />",{html:"You are now activating the deliverables checked for this event and deliverable unchecked will be inactive"}))
         ).dialog({
                   autoOpen  : true, 
                   modal     : true, 
                   title     : "Activate Deliverables",
                   position  : "top",
                   buttons   : {
                                 "Save Changes"   : function()
                                 {
                                    var _eventDeliverable =  [];
                                    var eventDeliverable =  [];
                                    var eDel  = [];
                                    var _eDel = [];
                                    $(".eventactivation").each(function( index, deliverable){
                                       if($(this).is(":checked"))
                                       {
                                          eDel.push( this.id );
                                       } else {
                                          _eDel.push( this.id );
                                       }
                                    });
                                    jsDisplayResult("info", "info", "Activating deliverables . . . <img src='../images/loaderA32.gif' />");
                                    $.post("../class/request.php?action=ClientDeliverable.activateDeliverable", 
                                    {
                                        data: {checked:eDel, unchecked:_eDel}
                                    },function( response ){
                                      if( response.error)
                                      {
                                        jsDisplayResult("error", "error", response.text);
                                      } else {
                                        jsDisplayResult("ok", "ok", response.text);
                                      } 
                                    },"json");                      
                                       $("#activate_eventdeliverable").dialog("destroy");             
                                       $("#activate_eventdeliverable").remove();                                                   
                                 } , 
                                 "Cancel"       : function()
                                 {
                                    $("#activate_eventdeliverable").dialog("destroy");             
                                    $("#activate_eventdeliverable").remove();               
                                 }
                   }
         });
         return false;
      });
     } , 

     _reassignDeliverables    : function()
     {
       var self = this;
       jsDisplayResult("info", "info", "Re-assigning deliverables . . . .<img src='../images/loaderA32.gif' />" );
       $.post("../class/request.php?action=ClientDeliverable.reAssign", {
          data      : $(".userslist").serializeArray(),
          statuses  : $(".usersstatuses").serializeArray()
       }, function(response) {
          if(response.error)
          {
             jsDisplayResult("error","error",response.text );                            
          } else {
             if(response.updated)
             {
               jsDisplayResult("info", "info", response.text);                                       
             } else {
               jsDisplayResult("ok","ok",response.text);                                       
               self._getDeliverables();             
             }
          }
       },"json");  
     } ,

     _reAssignDeliverable     : function(deliverableId)
     {
          var self = this;
          
          if($("#reassigndeliverabledialog_"+deliverableId).length > 0)
          {
             $("#reassigndeliverabledialog_"+deliverableId).remove();
          }
          
          $.get("../class/request.php?action=Deliverable.getEditDeliverableForm", {
              data : { 
                    reAssignDeliverable : 1,
                    deliverableid       : deliverableId
               }
          }, function(response){
               
               $("<div />",{id:"reassigndeliverabledialog_"+deliverableId, html:response})
                 .dialog({
                     autoOpen      : true,
                     modal         : true, 
                     position      : "top",
                     width         : 500,
                     height        : 'auto',
                     buttons       : {
                             "Save Changes"       : function()
                             {
                                  if($("#responsibility_owner :selected").val() === "")
                                  {
                                     $("#message").addClass("ui-state-error").html("Please select the user responsible . . ");
                                     return false;
                                  } else {
				               $("#message").addClass("ui-state-error").html("Updating ... <img src='../images/loaderA32.gif' />");
				                  $.post("../class/request.php?action=ClientDeliverable.editDeliverable",
			                		{ 
			                		  data: {
			                		         responsibility_owner : $("#responsibility_owner :selected").val(),
			                		         id 				: deliverableId	
			                		        }
			                	     }, function( response ) {
						               if( response.updated ){
							               if( response.error)
							               {
                                                       $("#message").addClass("ui-state-error").html(response.text);
							               } else {
								               jsDisplayResult("ok", "ok", response.text);
                                             	    $("#reassigndeliverabledialog_"+deliverableId).remove();
                                             	    $("#reassigndeliverabledialog_"+deliverableId).dialog("destroy");
							               }	
							               self._getDeliverable();
						               } else {					
							               $("#message").addClass("ui-state-error").html(response.text);
						               }
					               } ,"json");
				               }                                       
                             } ,
                             "Cancel"             : function()
                             {
                         	  $("#reassigndeliverabledialog_"+deliverableId).remove();
                         	  $("#reassigndeliverabledialog_"+deliverableId).dialog("destroy");
                             }
                          } ,
                     close         : function(event, ui)
                     {
               	    $("#reassigndeliverabledialog_"+deliverableId).remove();
               	    $("#reassigndeliverabledialog_"+deliverableId).dialog("destroy");
                     } , 
                     open          : function(event, ui)
                     {
                         
                     }      	            
                 });    
          },"html");
     } ,

     _displayHeaders	: function(headers, extraHeader)
     {
	     var self = this;
	     var tr   = $("<tr />");
	     tr.append($("<th />",{html:"&nbsp;"}).css({display:(self.options.eventActivation ? "table-cell" : "none")}));		
	     $.each(headers, function(index, head){
		   tr.append($("<th />",{html:head}));			
	     });		
	     tr.append($("<th />",{html:"&nbsp;"}).css({display:((self.options.reassignDeliverable && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;"}).css({display:((self.options.addActions  && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;", colspan:"2"}).css({display:((self.options.assuranceDeliverable  && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;", colspan:"2"}).css({display:((self.options.editDeliverable  && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;", colspan:"1"}).css({display:((self.options.updateDeliverable  && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;", colspan:"1"}).css({display:((self.options.approval  && extraHeader) ? "table-cell" : "none")}));
	     tr.append($("<th />",{html:"&nbsp;", colspan:"1"}).css({display:((self.options.reassignActions  && extraHeader) ? "table-cell" : "none")}));
	     $("#"+self.options.tableId).append(tr);
     } , 

     _displayPaging 		: function(total,columns, total_time) {
	     var self = this;
	
	     var pages;
	     if(total%self.options.limit > 0){
		  pages   = Math.ceil(total/self.options.limit); 				
	     } else {
		  pages   = Math.floor(total/self.options.limit); 				
	     }
	     $("#"+self.options.tableId)
	       .append($("<tr />")
		     .append($("<td />",{colspan:columns})
		        .append($("<input />",{type:"button", name:"first", id:"first", value:" |< "}))
		        .append("&nbsp;")
		        .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < "}))
		        .append("&nbsp;&nbsp;&nbsp;")
		        .append($("<span />",{html:"Page "+self.options.current+"/"+(pages === 0 || isNaN(pages) ? 1 : pages)}))
		        .append("&nbsp;&nbsp;&nbsp;")
		        .append($("<input />",{type:"button", name:"next", id:"next", value:" > "}))
		        .append("&nbsp;")
		        .append($("<input />",{type:"button", name:"last", id:"last", value:" >| "}))
		        .append("&nbsp;")
		        //.append($("<span />",{html:total+" deliverables <small><em> took "+total_time+" seconds to load </em><small>"})) 			   
	        )
        	 .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.reassignDeliverable ? "table-cell" : "none")}))
           .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.eventActivation ? "table-cell" : "none")}))			  	   
	      .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.addActions ? "table-cell" : "none")}))
	      .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({display:(self.options.assuranceDeliverable ? "table-cell" : "none")}))
	      .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({display:(self.options.editDeliverable ? "table-cell" : "none")}))
	      .append($("<td />",{html:"&nbsp;", colspan:"1"}).css({display:(self.options.updateDeliverable ? "table-cell" : "none")}))
	      .append($("<td />",{html:"&nbsp;", colspan:"1"}).css({display:(self.options.approval ? "table-cell" : "none")}))
	      .append($("<td />",{html:"&nbsp;", colspan:"1"}).css({display:(self.options.reassignActions ? "table-cell" : "none")}))
	     );

		if(self.options.current < 2)
		{
              $("#first").attr('disabled', 'disabled');
              $("#previous").attr('disabled', 'disabled');		     
		}
		if((self.options.current == pages || pages == 0))
		{
              $("#next").attr('disabled', 'disabled');
              $("#last").attr('disabled', 'disabled');		     
		}		

	     $("#next").bind("click", function(evt){
		     self._getNext( self );
	     });
	     $("#last").bind("click",  function(evt){
		     self._getLast( self );
	     });
	     $("#previous").bind("click",  function(evt){
		     self._getPrevious( self );
	     });
	     $("#first").bind("click",  function(evt){
		     self._getFirst( self );
	     });			
     } ,		

     _getNext  			: function( rk ) {
	     rk.options.current = rk.options.current+1;
	     rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
	     this._getDeliverable();
     },	
     _getLast  			: function( rk ) {
	     rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
	     rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
	     this._getDeliverable();
     },	
     _getPrevious    	: function( rk ) {
	     rk.options.current  = parseFloat( rk.options.current ) - 1;
	     rk.options.start 	= (rk.options.current-1)*rk.options.limit;
	     this._getDeliverable();			
     },	
     _getFirst  			: function( rk ) {
	     rk.options.current  = 1;
	     rk.options.start 	= 0;
	     this._getDeliverable();				
     } ,

     _displayButtonOptions           : function(deliverableId, isDeliverableOwner)
     {
          var self = this;
          var btnDisplayOptions = {};
          /*
             displayed for legislation owner and deliverable responsible owner
          */
          btnDisplayOptions.btnAddActions = false;
          //show approve button
          btnDisplayOptions.btnApprove = false;
          //deliverable owner or legislation owner can edit deliverable
          //show under new pages
          //under manage pages show only for manually created legislations/deliverables
          btnDisplayOptions.btnEditDel = false;
         
          
          //if create actions sections
          if(self.options.createActions)
          {
             //if the user is deliverabla responsible person or legisaltion owner then show the button
             if(isDeliverableOwner[deliverableId] || self.options.isLegislationOwner)
             {
               btnDisplayOptions.btnAddActions = true;
             }
             if(self.options.section == "admin" || (self.options.page === "manage" && self.options.page === "edit"))
             {
               btnDisplayOptions.btnAddActions = true;
             }             
          }
          if(isDeliverableOwner[deliverableId] || self.options.isLegislationOwner)
          {
              
             if((self.options.deliverableStatus[deliverableId] & 256) != 256)
             {
               btnDisplayOptions.btnEditDel = true;
             }
             
          }	

          if((self.options.page === "new") || (self.options.section === "admin" || self.options.page == "edit"))
          {    
             btnDisplayOptions.btnEditDel = true;
          } 	     
          
          //if the progress is 100 , then show the approve button
          if(self.options.actionProgress[deliverableId] == "100")
          {
             btnDisplayOptions.btnApprove = true;  
          }

          return btnDisplayOptions;
     }

});
