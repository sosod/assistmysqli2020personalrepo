// JavaScript Document
$(function(){
	   
	 LegislativeFrequency.get()  
	   
	$("#save").click(function(){
		LegislativeFrequency.save();
		return false;
	});
	
	$("#edit_legfreq").click(function(){
		LegislativeFrequency.edit();	
		return false;
	});
	
	$("#update_legfreq").click(function(){
		LegislativeFrequency.update();	
		return false;
	});	
});

var LegislativeFrequency 	= {
	
	get 		: function()
	{
		$.post("../class/request.php?action=SetupManager.getLegislativeFrequency", function( response ){
			$.each( response, function( index, legcat){
				LegislativeFrequency.display( legcat )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#legislative_frequency_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the legislative frequency name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=SetupManager.saveLegislativeFrequency", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text );	
				} else {
					data.id  	= response.id; 	
					data.status = 1; 	
					LegislativeFrequency.display( data )																   
					LegislativeFrequency.empty( data );
					jsDisplayResult("ok", "ok", response.text );
				}																	   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {}; 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		data.id			 = $("#legfreqid").val();
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the legislative frequency name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=SetupManager.updateLegislativeFrequency", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text );	
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}																   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{ 
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		var data = {};
		data.id = $("#legfreqid").val()
		data.status = status
		$.post("../class/request.php?action=SetupManager.updateLegislativeFrequency", { data : data }, function( response ){																																																									 					
			if( response.error ){
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}
		}, "json");
	} , 
	
	display		: function( legcat )
	{
		$("#legislative_frequency_table")
		 .append($("<tr />",{id:"tr_"+legcat.id})
		   .append($("<td />",{html:legcat.id}))
		   .append($("<td />",{html:legcat.name}))
		   .append($("<td />",{html:legcat.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type	 : "button", 
				  					name	 : "edit_"+legcat.id,
				  					id		 : (legcat.imported == true ? "edit" : "edit_"+legcat.id),
				  					value	 : "Edit",
				  					disabled : (legcat.imported == true ? "disabled" : "")
				  					}))
			  .append($("<input />",{
				  					 type		: "button",
				  					 name		: "del_"+legcat.id,
				  					 id			: (legcat.imported == true ? "del" : "del_"+legcat.id),
				  					 value		: "Del",
				  					 disabled 	: (legcat.imported == true ? "disabled" : "")
				  					}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:( ((legcat.status & 1) == 1 ) ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "change_"+legcat.id,
				  					id			: (legcat.imported == true ? "change" : "change_"+legcat.id),
				  					value		: "Change Status",
				  					disabled 	: (legcat.imported == true ? "disabled" : "")
				  					}))			  					
		    )		   
		 )
		 		 
		 $("#edit_"+legcat.id).live("click", function(){
			document.location.href = "edit_legislative_frequency.php?id="+legcat.id;								   
			return false;								   
		  });
		 
		 $("#del_"+legcat.id).live("click", function(){ 
			if( confirm("Are you sure you want to delete this legislative frequency")){
				jsDisplayResult("info", "info", "Delete . . . <img src='../images/loaderA32.gif' />");
				var data 	= {};
				data.id  	= legcat.id;
				data.status = 2;
					$.post("../class/request.php?action=SetupManager.updateLegislativeFrequency", { data: data}, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text );
						} else {
							$("#tr_"+legcat.id).fadeOut();
							jsDisplayResult("ok", "ok", response.text );
						}
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+legcat.id).live("click", function(){
			document.location.href = "changelegislative_frequency.php?id="+legcat.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}