(function($){
 $.widget("ui.reassignactions", {
 
     options        : {
          page      : "",
          section   : "",
          user      : "",
          start     : 0,
          limit     : "",
          current   : 1,
          total     : 0,
          section   : "admin"
     } ,
     
     _init          : function()
     {
     
     } ,
     
     _create        : function()
     {
        var self = this;    
        var el   = $(self.element); 
        el.append($("<table />",{width:"100%"}).addClass("noborder")
           .append($("<tr />")
              .append($("<th />",{html:"Select user :"}))
              .append($("<td />").addClass("noborder")
                .append($("<select />",{name:"users", id:"users"}))
              )
           )
           .append($("<tr />",{id:"tr_selectuser"})
             .append($("<td />",{colspan:"2"}).addClass("noborder")
               .append($("<p />").css({padding:"5px"}).addClass("ui-state-highlight")
                 .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
                 .append($("<span />",{html:"Please select the user to list their actions for re-assignment"})
                   .css({"margin-left":"3px"})
                 )
               )
             )
           )
           .append($("<tr />")
                .append($("<td />",{colspan:"2"}).addClass("noborder")
                  .append($("<table />",{width:"100%", id:"actionsTable"}).addClass("noborder"))
                )           
           )
        )           
        
        var usersList = self._getUsers();
        $("#users").append($("<option />",{value:"", text :"--please select--"}))
        $.each(usersList, function(index, user){
           $("#users").append($("<option />",{value:user['tkid'], text:user['user']}))        
        });
          
        $("#users").live("change", function(e){
           self.options.user = $(this).val();
           if(self.options.user !== "")
           {
             $("#tr_selectuser").hide();
           } 
           self._getActions();        
           e.preventDefault();
        });  
     
        
     } ,
     
     _getActions           : function()
     {
        var self = this;
		$("#actionsTable").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
	       .css({position:"absolute", "z-index":"9999",	top:"250px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
		)        
        $.getJSON("../class/request.php?action=Action.getActions", {
          options : { start   : self.options.start,
                      limit   : self.options.limit,
                      user    : self.options.user,
                      section : self.options.section
                    }  
        } , function(actionsList) {   
          $("#actionsTable").html("");
          self._displayHeaders(actionsList.headers);   
          if($.isEmptyObject(actionsList.actions))
          {
            self._displayErrorEmptyActions(actionsList.columns);
          } else {
            self._displayActions(actionsList.actions, actionsList.columns);                  
          }
        });  
     
     } , 
     
     _displayActions      : function(actions, cols)
     {
        var self = this;
        $.each(actions, function(index, action){
          var tr  = $("<tr />");
          $.each(action , function(key, value){
              tr.append($("<td />",{html:value}))
          })        
          tr.append($("<td />").append($("<select />",{id:"userlist_"+index, name:"userlist_"+index}).addClass("userslist")))
          $("#actionsTable").append(tr);
          $.each(self.options.usersList, function(uIndex, user){
             
             $("#userlist_"+index).append($("<option />",{text:user['user'], value:user['tkid'], selected:(self.options.user === user['tkid'] ? "selected" : "")}).addClass("userslist"))
          });          
        });  
        $("#actionsTable").append($("<tr />")
          .append($("<td />",{colspan:(cols)+1}).css({"text-align":"right"})
            .append($("<input />",{type:"button", name:"save_changes_"+self.options.user, id:"save_changes_"+self.options.user, value:"Save Changes"})
              .addClass("isubmit")
            )
          )
        )
        
        $("#save_changes_"+self.options.user).live("click", function(e){
            jsDisplayResult("info", "info", "Re-assingning actions . . . .<img src='../images/loaderA32.gif' />" );
            $.post("../class/request.php?action=ClientAction.reAssign", {
               data : $(".userslist").serializeArray()
            }, function(response){
                  if(response.error)
                  {
                     jsDisplayResult("error","error",response.text );                            
                  } else {
                     jsDisplayResult("ok","ok",response.text);                                       
                     self._getActions();
                  }
            },"json");            
            e.preventDefault();
        });         
     } ,
     
     _displayHeaders          : function(headers)
     {
        var self = this;
        var tr   = $("<tr />") 
        $.each(headers, function(key, value){
          tr.append($("<th />",{html:value}));
        });
        tr.append($("<th />",{html:"Re-Assign Action Owner"}))
        $("#actionsTable").append(tr);
     } ,
     
     _getUsers            : function()
     {
       var self = this;
       var users  = {};
       $.ajaxSetup({async:false});
       $.getJSON("../class/request.php?action=UserAccess.getUsers",function(usersList){
          users = usersList;
          if(!$.isEmptyObject(usersList))
          {
             self.options.usersList = usersList;
          }
       });
       return users;
     } ,
     
     _displayErrorEmptyActions          : function(cols)
     {
        var self = this;
        $("#actionsTable").append($("<tr />")
          .append($("<td />",{colspan:parseInt(cols) + 1, html:"There are no action for this user selected"}) )
        )
     }
 
 });    
})(jQuery)
