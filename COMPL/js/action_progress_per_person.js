$(function(){

    ActionProgressPerPerson.getFinancialYear(); 
    $("#financialyear").change(function(){
        ActionProgressPerPerson.get($(this).val());       
    }); 

})

var ActionProgressPerPerson  = 
{

     getFinancialYear      : function()
     {
       $("#financialyear").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financialyear").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 
          
     get       : function(financialyear)
     {           
          document.location.href = "action_progress_perperson_report.php?financialyear="+financialyear
          /*$.get("action_progress_perperson_report.php",{financialyear:financialyear}, function(
               $("#report").html(reportData);
          },"html");*/
     }

};
