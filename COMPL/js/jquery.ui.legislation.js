$.widget("ui.legislation",{
	
	options		: {
	     tableRef 			     : "legislation_"+Math.floor(Math.random(57)*98),
	     legislationTable         : "legislationtable_"+Math.floor(Math.random(57)*98),
	     url 				     : "../class/request.php?action=Legislation.getClientLegislations",
	     deliverableUrl 	     : "../class/request.php?action=Deliverable.getDeliverables",
	     start 				: 0,
	     limit				   : 10,
	     headers				: [],
	     total 				: 0,
	     current				: 1,
	     usedIds 			     : [],
	     page				     : "",
	     legoption   		     : "",
	     legStatus		 	     : [],
	     importDeliverable	     : false,
	     importLegislation	     : false,
	     createDeliverable	     : false,
	     addDeliverable           : false,
	     addAction			     : false,
	     editLegislation	     : false,
	     editDeliverable	     : false,
	     updateLegislation	     : false,
	     updateDeliverable	     : false,
	     assuranceLegislation     : false,
	     assuranceDeliverable     : false,
	     confirmLegislation	     : false,
	     setupActivate		     : false,
	     setupLegislation	     : false,
	     deactivateLegislation	     : false,
	     setupEdit		   	     : false,
	     viewDetails			: false,
	     approvalUpdate		     : false,
	     reassignDeliverable	     : false,
	     reassignActions          : false,
	     adminPage                : false,
	     user				     : "",
	     company				: "",
	     approval			     : false,
	     searchLegislation	     : false,
	     searchtext               : "",
	     advancedSearchLegislation	: false,
	     viewType                 : "mine",
	     displayBtn               : {},
	     statusDescription        : {},
	     financialyear            : 0,
	     financialYearsList       : [],
	     legislationId            : 0,
	     showFinancialYear        : true,
	     copyFinancialYear        : false,
	     tofinancialyear          : 0,
	     autoLoad                 : true,
		 actionProgress			  : true
	} , 
	
	_init		: function()
	{
	   if(this.options.autoLoad)
	   {
	     this._getLegislation();
	   }
	} , 
	
	_create 	: function()
	{
	    var self = this;
	    $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
	      .append($("<tr />").css({"display":(self.options.showFinancialYear ? (self.options.copyFinancialYear ? "none" : "table-row") : "none")})
	        .append($("<th />",{width:"300px"}).css({"text-align":"left"})
	          .append($("<span />",{html:"Select financial year :"}))
	        )	        
	        .append($("<td />").addClass("noborder")
	          .append($("<select />",{id:"financial_year", name:"financial_year"}).addClass("financial_year"))
	        )
	      )
	      .append($("<tr />").css({"display":(self.options.confirmLegislation ? "table-row" : "none")})
	        .append($("<th />").css({"text-align":"left"})
	          .append($("<span />",{html:"Select a legislation :"}))
	        )	        
	        .append($("<td />").addClass("noborder")
	          .append($("<select />",{id:"legislation_list", name:"legislation_list"})
                 .append($("<option />",{value:"", text:"--please select--"}))
				 .change(function(){
					$("#message").remove();
					self.options.legislationId = $(this).val();
					self._loadDeliverableActions();
				    return false;
				 })
	          )
	        )
	      )
	      .append($("<tr />").css({"display":(self.options.copyFinancialYear ? "table-row" : "none")})
	        .append($("<th />",{width:"300px"}).css({"text-align":"left"})
	          .append($("<span />",{html:"Select the source Financial Year:"}))
	        )	        
	        .append($("<td />").addClass("noborder")
	          .append($("<select />",{id:"financial_year", name:"financial_year"}).addClass("financial_year"))
	        )
	        .append($("<th />",{width:"300px"}).css({"text-align":"left"})
	          .append($("<span />",{html:"Select the recipient Financial Year:"}))
	        )	        
	        .append($("<td />").addClass("noborder")
	          .append($("<select />",{id:"to_financial_year", name:"to_financial_year"}))
	        )	        
	      )     
	      .append($("<tr />")
	        .append($("<td />",{colspan:"4"}).addClass("noborder")
	          .append($("<div />",{id:"legislations_"+self.options.tableRef}))
	        )
	      )	      
	    )	

	    self._loadFinancialYears();		
	    /*	
		$("#legislation_list").live("change", function(e){
	      e.preventDefault();
	    });	    */
	    
	    $("#financial_year").live("change", function(e){
	      $("#message").remove();
	      self.options.financialyear = $(this).val();
	      self._getLegislation();
	      e.preventDefault();
	    });
	    
	    $("#to_financial_year").live("change", function(e){
	      self.options.tofinancialyear = $(this).val();
	      self._getLegislation();
	      e.preventDefault();
	    });	    

	} , 
	
	_loadFinancialYears      : function()
	{
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $(".financial_year").append($("<option />",{value:"", text:"--please select--"}));
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAllSorted",function(financialYears) {
	          if($.isEmptyObject(financialYears))
	          {
	              $(".financial_year").append($("<option />",{value:"", text:"--please select--"}))
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear) {
	               if(self.options.financialyear == financialYear.id)
	               {
                         $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date, selected:"selected"}))	               
	               } else {
                    $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date}))	               
	               }
	             });
	          }	   
	        });
	   } 
	} ,
	
	_loadDeliverableActions   : function()
	{
	     var self = this;
	     $("#legislations_"+self.options.tableRef).html("");
		//var selectedlegislation = $("#legislation_list :selected").val();
		$("#legislations_"+self.options.tableRef).deliverableactions({ legislationId : self.options.legislationId,
																 page			 	 : self.options.page,
																 section             : self.options.section,
																 confirmation        : true,
																 details 		 	 : false,
																 editAction          : true,
																 editDeliverable     : true,
																 triggerBy           : true,
																 url	   			 : self.options.deliverableUrl    
															   });									
		return false;	     
	} ,
	
	_reloadFinancialYear        : function(userFinancialYear)
	{
	     var self = this;
		if(userFinancialYear !== undefined)
		{     
		    if(self.options.financialyear !== 0)
		    {
		        $("select#financial_year option[value="+self.options.financialyear+"]").attr("selected", "selected");
		    } else {
		       $("select#financial_year option[value="+userFinancialYear+"]").attr("selected", "selected");
		    }
		}
		
		if(self.options.copyFinancialYear)
		{
		     var selectedFinyear = self.options.financialyear = $("#financial_year :selected").val();
		    //if we already have the finacials years loaded ,then use the to reload the financial years 
		    if(!($.isEmptyObject(self.options.financialYearsList)))
		    {
                 $("select#to_financial_year").empty();
                 $("select#to_financial_year").append($("<option />",{value:"", text:"--please select--"}));
		       $.each(self.options.financialYearsList, function(index, finyear){
		           //don't display the selected  from-financial 
		           if(self.options.financialyear !== finyear.id)
		           {
		              if(self.options.tofinancialyear == finyear.id)
		              {
                           $("select#to_financial_year").append($("<option />",{value:finyear.id, text:"("+finyear.value+") "+finyear.start_date+" - "+finyear.end_date, selected:"selected"}));		                    
		              } else {
                           $("select#to_financial_year").append($("<option />",{value:finyear.id, text:"("+finyear.value+") "+finyear.start_date+" - "+finyear.end_date}));		               
		              }
		           }				       
		       });
		       /*if(userFinancialYear !== undefined)
		       {
		         if(userFinancialYear !== "")
		         {
		           $("select#to_financial_year option[value="+userFinancialYear+"]").remove();	
		         }
		       }*/
		    }    
		}
	} ,
	
	_getLegislation		: function()
	{
		var self = this;		
		$("#legislations_"+self.options.tableRef).append($("<div />",{id:"loadingDiv", html:"Loading legislations. . . <img src='../images/loaderA32.gif' />"})
		   .css({position:"absolute", "z-index":"9999", top:"250px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
		)
		$.getJSON(self.options.url ,{
		   data : {  start     : self.options.start,
			        limit     : self.options.limit,
			        viewType  : self.options.viewType,
		             page      : self.options.page,
		             legoption : self.options.legoption,
		             section   : self.options.section,
		             financial_year : self.options.financialyear,
		             tofinancialyear : self.options.tofinancialyear,
		             searchtext      : self.options.searchtext
		     }
		} ,function(legislationResponse){

		     $("#message").remove();
		     $("#loadingDiv").remove();
			$("#legislations_"+self.options.tableRef).html("");
			$("#legislation_list").html("");
			$("#legislation_list").append($("<option />",{value:"", text:"--please select--"}));
		     if( $("#legislations_"+self.options.tableRef).length < 1 )
		     {
		          self._createTable();
		     }
		     if( $("#"+self.options.legislationTable).length < 1 )
		     {
		          self._legislationTable();
		     }
			if(legislationResponse.hasOwnProperty("userfinancialyear"))
			{
			   self._reloadFinancialYear(legislationResponse.userfinancialyear);
			} 
			//if(self.options.section=="support") { alert(legislationResponse.total); }
			self.options.total       = legislationResponse.total;  
			self.options.legStatus   = legislationResponse.status; 
			self.options.statusDescription = legislationResponse.statusDescription; 
			self.options.user	     = legislationResponse.user;
			self.options.company     = legislationResponse.company;
			self.options.usedIds     = legislationResponse.usedId;
			self.options.currentUser = legislationResponse.currentUser;
			//self._displayPaging(legislationResponse.columns);
			//self._displayHeaders(legislationResponse.headers);
			if($.isEmptyObject(legislationResponse.legislations))
			{
				if(self.options.page=="setup_activate") {
					var text = "";
					legislationResponse.message = "There are no Master Legislations available for activation.  Please use the New > Legislation > Create function to create new Legislations.";
				} else {
			     var text = "";
			     if(legislationResponse.userfinancialyear !== undefined)
			     {
			       if(legislationResponse.userfinancialyear == "" || legislationResponse.userfinancialyear == 0)
			       {
			         if(self.options.financialyear != "" || self.options.financialyear != 0)
			         {
                          text = " for the selected financial year ";
			         } else {
			          text = "<br />&nbsp;&nbsp;Please setup a default financial year under My Profile <br />";
			         }
			         
			       }
			     }
				}
		     	self._displayHeaders(legislationResponse.headers, false);
				$("#"+self.options.legislationTable).append($("<tr />")
					  .append($("<td />",{colspan:(self.options.setupLegislation ? legislationResponse.columns+4 : legislationResponse.columns + 2)})
					  	.append($("<span />",{html:(legislationResponse.message == undefined ? "There are no legislations allocated to you for the selected financial year "+text : legislationResponse.message+" "+text)}).css({"padding":"5px"}))
					  )		
					);
			} else {
			     self._displayPaging(legislationResponse.columns);
			     self._displayHeaders(legislationResponse.headers, true);			
				self._displayLegislation(legislationResponse.legislations, legislationResponse.users, legislationResponse.admin, legislationResponse.authorizor, legislationResponse.isLegislationOwner );
			}
		})
	} , 
	
	_createTable             : function()
	{
	   var self = this;
	   $(self.element).append($("<div />",{id:"legislations_"+self.options.tableRef}).addClass("noborder"))
	} ,
	
	_legislationTable         : function()
	{
	   var self = this;
	   if( $("#legislations_"+self.options.tableRef).length > 0)
	   {
	      $("#legislations_"+self.options.tableRef).append($("<form />",{method:"post", id:"legislation_form", name:"legislation_form"})
	          .append($("<table />",{id:self.options.legislationTable}).addClass("noborder"))
           )	     
	   }   
	},
	
	_displayLegislation 	: function(legislations, users, adminstrator, authorizor, isLegislationOwner)
	{
		var self = this;
		$.each( legislations, function( index, leg){
			var tr = $("<tr />",{id:"tr_"+index}).addClass((isLegislationOwner[index] || self.options.deactivateLegislation ? "" : "view-notowner"))
			$.each(leg, function( key, val){
			   if(self.options.searchLegislation)
			   { 
			      if(val !== null || val !== "")
			      {
			         if(val.indexOf(self.options.searchtext) >= 0)
			         {
			            tr.append($("<td />",{html:"<span style='background-color:yellow'>"+val+"</span>"}));
			         } else {
			          tr.append($("<td />",{html:val}));
			         }			          
			      } else {
			         tr.append($("<td />",{html:val}));
			      }  
			   } else {
			     tr.append($("<td />",{html:val}));				
			   }
			});
			var btnOptions = self._displayBtnCheck(index, isLegislationOwner);
			
			if(self.options.confirmLegislation === true)
			{
			  if(self.options.legislationId === index)
			  { 
			      $("#legislation_list").append($("<option />",{text:leg.name, value:index, selected:(self.options.legislationId == index ? "selected" : "")}))
			   } else {
			     $("#legislation_list").append($("<option />",{text:"[L"+index+"] "+leg.name, value:index}))
			   }
			}							
			
			if(self.options.setupActivate)
			{	   
			  var usedIds = [];
			  usedIds = self.options.usedIds;
			  if(self._indexOf(self.options.usedIds, index) > -1)
			  {		    
			    if((self.options.legStatus[index] & 1) == 1)
			    {
				tr.append($("<td />",{id:"activated_authorizor_"+index}).css({"display" : (self.options.setupLegislation ? "table-cell" : "none") }))
				tr.append($("<td />",{id:"activated_administator_"+index}).css({"display" : (self.options.setupLegislation ? "table-cell" : "none") }))
			    } else {
		          tr.append($("<td />",{id:"activated_authorizor_"+index}))	
		          tr.append($("<td />",{id:"activated_administator_"+index}))						     
			    }
			  } else {				     
				tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				  .append($("<select />",{id:"legislationauthorisor_"+index, name:"legislationauthorisor_"+index, multiple:"multiple"}))
				)							
				tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				  .append($("<select />",{ id:"legislationadminstrator_"+index, name:"legislationadminstrator_"+index, multiple:"multiple"}))
				)											
			  }	
			} else {
				tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				  .append($("<select />",{id:"legislationauthorisor_"+index, name:"legislationauthorisor_"+index, multiple:"multiple"}))
				)							
				tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				  .append($("<select />",{id:"legislationadminstrator_"+index, name:"legislationadminstrator_"+index, multiple:"multiple"}))
				)	
			}		
			//display the yes/options to 
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<select />",{id:"available_"+index, name:"available_"+index})
					.append($("<option />",{value:0, text:"No"}))
					.append($("<option />",{value:1, text:"Yes"}))
				)
			)	

			
			
			$("table").delegate("#available_"+index, "change", function(){
				
				if( $(this).val() == 1){
					$("select#import_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_del_"+index+" option[value='0']").attr("selected", "selected")
					$("select#create_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#import_actions_"+index+" option[value='0']").attr("selected", "selected")
					$("select#edit_actions_"+index+" option[value='0']").attr("selected", "selected")
					$("select#create_actions_"+index+" option[value='1']").attr("selected", "selected")
				} else {
					$("select#import_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#create_del_"+index+" option[value='1']").attr("selected", "selected")
					$("select#import_actions_"+index+" option[value='1']").attr("selected", "selected")
					$("select#edit_actions_"+index+" option[value='1']").attr("selected", "selected")
					$("select#create_actions_"+index+" option[value='1']").attr("selected", "selected")
				}
				return false;
			});	
			
			tr.append($("<td />").css({"display" : (self.options.setupActivate ? "table-cell" : "none") })
			   .append($("<table />")
				.append($("<tr />")
				  .append($("<td />")
				     .append($("<select />",{id:"import_del_"+index, name:"import_del_"+index})
					 .append($("<option />",{value:"1", text:"Yes"}))
					  .append($("<option />",{value:"0", text:"No"}))
					 )						  
				 )	  
				  .append($("<td />")
					.append($("<select />",{id:"edit_del_"+index, name:"edit_del_"+index})
					  .append($("<option />",{value:"1", text:"Yes"}))
					  .append($("<option />",{value:"0", text:"No"}))
					)							  
				 )	  
				  .append($("<td />")
					.append($("<select />",{id:"create_del_"+index, name:"create_del_"+index})
					  .append($("<option />",{value:"1", text:"Yes"}))
					  .append($("<option />",{value:"0", text:"No"}))
					   )							  
					)	  						  
				   )		
				)	
			)			
			
			tr.append($("<td />").css({"display" : (self.options.setupActivate ? "table-cell" : "none") })
				.append($("<table />")
				  .append($("<tr />")
					  .append($("<td />")
						  .append($("<select />",{id:"import_actions_"+index, name:"import_actions_"+index})
							.append($("<option />",{value:"1", text:"Yes"}))
							.append($("<option />",{value:"0", text:"No"}))
							)							  
					  )	  
					  .append($("<td />")
						  .append($("<select />",{id:"edit_actions_"+index, name:"edit_actions_"+index})
							 .append($("<option />",{value:"1", text:"Yes"}))
							  .append($("<option />",{value:"0", text:"No"}))
							)							  
						  )	  
					  .append($("<td />")
					    .append($("<select />",{id:"create_actions_"+index, name:"create_actions_"+index})
						  .append($("<option />",{value:"1", text:"Yes"}))
						   .append($("<option />",{value:"0", text:"No"}))
							)							  
						  )	  						  
				   )		
				)	
			)	
			
			var statusDescription = "";
			if(self.options.statusDescription != undefined)
			{
			  statusDescription = self.options.statusDescription[index];
			}
			
			tr.append($("<td />").css({"display" : (self.options.copyFinancialYear ? "table-cell" : "none") })
			  .append($("<span />",{html:statusDescription}))
			)	
			
			tr.append($("<td />").css({"display" : (self.options.copyFinancialYear ? "table-cell" : "none") })
			  .append($("<input />",{type:"button", id:"copy_"+index, name:"copy_"+index, value:"Copy"}).click(function(e){
			     if($("#to_financial_year").val() == "")
			     {
			         jsDisplayResult("error", "error",  "Please select the financial year to");
	                   return false;
			     } else {
                         if((self.options.legStatus[index] & 1024) == 0)
                         {
                            if($("#unactivatednotice_"+index).length > 0)
                            {
                              $("#unactivatednotice_"+index).remove();
                            }
                            $("<div />",{id:"unactivatednotice_"+index, html:"You are copying a legislation that have not been activated . ."})
                             .dialog({
                                   autoOpen  : true,
                                   modal     : true,
                                   position  : "top",
                                   width     : 500,
                                   buttons   : {
                                                  "Ok" : function()
                                                  {
                                                     self._copyLegislation(index);
                                                     $("#unactivatednotice_"+index).dialog("destroy");
                                                     $("#unactivatednotice_"+index).remove();                                                       
                                                  } ,
                                                  "Cancel"   : function()
                                                  {
                                                     $("#unactivatednotice_"+index).dialog("destroy");
                                                     $("#unactivatednotice_"+index).remove();                                                  
                                                  }
                                   } ,
                                   open      : function(event, ui)
                                   {
                                       $(this).css("height", "auto")
                                   } ,
                                   close     : function(event, ui)
                                   {
                                      $("#unactivatednotice_"+index).dialog("destroy");
                                      $("#unactivatednotice_"+index).remove();                              
                                   }
                             })

                         } else {
                            self._copyLegislation(index);			  
                         }  
                       }
                       $(this).attr("disabled", "disabled");
			      e.preventDefault();
			   }) 
			  )
			)
								               
			tr.append($("<td />").css({"display" : (self.options.viewDetails ? "table-cell" : "none") })
			  .append($("<input />",{type:"button", id:"view_details_"+index, name:"view_details_"+index, value:"View Details"}).click(function(){
                     document.location.href = "view_details.php?id="+index+"&viewtype="+self.options.viewType;			      
			    })
			  )
			)		

			tr.append($("<td />").css({"display" : (self.options.addAction ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"add_actions_"+index, name:"add_actions_"+index,value:"Add Actions"})
				  .css({display:(btnOptions['displayAddActions'] ? "block" : "none")})
				)
			)		
			tr.append($("<td />").css({"display" : ( self.options.updateLegislation ? "table-cell" : "none") })
			    .append($("<input />",{type:"button", id:"update_legislation_"+index, name:"update_legislation_"+index,value: "Update Legislation"})
			      .css({display:(btnOptions['displayUpdateBtn'] ? "block" : "none")})
			    )
			    .append($("<br />"))
			    .append($("<input />",{type:"button", id:"update_deliverable_"+index,	name:"update_deliverable_"+index,value:"Update Deliverable"})
				 .css({display:(btnOptions['displayUpdateDelBtn'] ? "block" : "none")})
			    ) 		
			)	
			
			tr.append($("<td />").css({"display" : ( self.options.reassignDeliverable ? "table-cell" : "none") })
			   .append($("<input />",{type:"button", id:"reassign_deliverable_"+index, name:"reassign_deliverable_"+index,value:"Re-Assign Deliverables"}).css({display:btnOptions['displayReAssignDelBtn'] ? "block" : "none"}))				    								
			)
			
			tr.append($("<td />").css({"display" : ( self.options.reassignActions ? "table-cell" : "none") })
		        .append($("<input />",{type:"button", id:"reassign_actions_"+index, name:"reassign_actions_"+index, value:"Re-Assign Actions"})
		          .css({display:btnOptions['displayReAssignActionsBtn'] ? "block" : "none"})
		         )
			)					
													
			//edit legislation
			tr.append($("<td />").css({"display" : ( self.options.editLegislation ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"edit_legislation_"+index, name:"edit_legislation_"+index, value:"Edit Legislation"})
				  .css({display:(btnOptions['displayEditBtn'] ? "block" : "none")})
				 )
				.append($("<br />"))						
				.append($("<input />",{type:"button",id:"edit_deliverable_"+index, name:"edit_deliverable_"+index ,value:btnOptions['editDelBtnValue']})
				  .css({display:(btnOptions['displayEditDelBtn'] ? "block" : "none")})
				)	
				.append($("<input />",{type:"button", id:"create_deliverable_"+index, name:"create_deliverable_"+index,value: "Add  Deliverable"})
				 .css({"display":(btnOptions['displayCreateDelBtn'] ? "block" : "none")})
				)			
			)	
			
			tr.append($("<td />").css({"display" : (self.options.assuranceLegislation ? "table-cell" : "none") })
				.append($("<input />",{type:"button",id: "assurance_legislation_"+index, name:"assurance_legislation_"+index, value:"Assurance Legislation"}).
				  css({display:(btnOptions['displayAssuranceBtn'] ? "block" : "none")})
				 )
				.append($("<br />"))						
				.append($("<input />",{type:"button", id:"assurance_deliverable_"+index ,name:"assurance_deliverable_"+index,value: "Assurance Deliverable"})
				  .css({display:(btnOptions['displayAssuranceDelBtn'] ? "block" : "none")})
				)	
			)
			tr.append($("<td />").css({"display" : ( self.options.approval ? "table-cell" : "none") })
			   .append($("<input />",{type:"button",id:"approval_deliverable_"+index, name:"approval_deliverable_"+index,value:"Approval"})
			     .css({display:(btnOptions['displayApprovalBtn'] ? "block" : "none")})
			  )
			)	
								
			//deactivate legislation
			tr.append($("<td />").css({"display" : ( self.options.deactivateLegislation ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"deactivate_legislation_"+index, name:"deactivate_legislation_"+index, value:"Deactivate"})
				  .addClass("idelete")
				 )
			)	
				$("#deactivate_legislation_"+index).live("click", function(){
					if(confirm("Are you sure you wish to deactivate legislation L"+index+"?")) {
						$("#deactivate_legislation_"+index).attr("disabled", "disabled");
						jsDisplayResult("info", "info", "Deactivating legislation . . . .<img src='../images/loaderA32.gif' />" );
						var dta 		    = {};		 
						dta.legislation_id = index;
						url = "../class/request.php?action=ClientLegislation.deactivate";
						//alert(url);
						$.ajax({                                      
							url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
							success: function(response) { 
								 //console.log(index+" => "+response.leg);
								 jsDisplayResult("ok", "ok", response.text );
								 //self._getLegislation();
								$("#deactivate_legislation_"+index).hide();
								$("#tr_"+index).addClass("view-notowner").css("background-color","#dedede");
							},
							error: function(response) { 
								jsDisplayResult("error", "error",  response.text );
								$("#deactivate_legislation_"+index).attr("disabled", "");
								//console.log("error"); 
							}
						});
					}
					return false;
				});	
		
			
			
			//import
			tr.append($("<td />").css({"display" : (self.options.importLegislation  ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"import_"+index, name:"import_"+index,	value:"Import"})
				    .click(function(e) {
					     var financialYear = $("#financial_year :selected").val();
					     if(financialYear == "")
					     {
						     jsDisplayResult("error", "error", "Please select the financial year");
						     return false;
					     } else {
						     jsDisplayResult("info", "info", "Importing a legislation . . . .<img src='../images/loaderA32.gif' />" );
						     $.post("../class/request.php?action=Legislation.importSave",
						     { legislation_id:index , financial_year:financialYear } , function(response) {
							     if( response.error ){
								      jsDisplayResult("error", "error",  response.text );
							     } else {
								      jsDisplayResult("ok", "ok", response.text );
								      $("#status_"+index).html("<b>Active</b>")
								      self._getLegislation();
      								 $("#import_"+index).attr("disabled", "disabled")
							     }
						
						     },"json");					
					     }								      
				      e.preventDefault();
				    })
				    .css({display:(btnOptions['displayImportBtn'] ? "block" : "none")})
			      )
			      .append($("<div />").addClass("ui-state-ok").css({"border":"0px", "background-color":"white"})
			        .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left"}))
			        .append($("<span />",{html:"<b>Imported</b>"}).css({"float":"right"}))
			        .css({display:(btnOptions['displayImportBtn'] ? "none" : "block")})
			      )
			)
	
			tr.append($("<td />").css({"display" : (self.options.createDeliverable ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"create_deliverable_"+index,name:"create_deliverable_"+index,value :"Add Deliverable"})
				  //.addClass("imports")
				)
		      )					 	
			tr.append($("<td />").css({"display" : (self.options.setupLegislation ? "table-cell" : "none") })
				.append($("<input />",{type:"button", id:"activate_"+index,	name:"activate_"+index, value :"Activate "})
				  .css({display:(btnOptions['displayActivateBtn'] ? "block" : "none")})
				  .addClass("isubmit")
				)
				.append($("<input />",{type:"button", id:"inactivate_"+index, name:"inactivate_"+index, value:"Deactivate"})
				   .css({display:(btnOptions['displayDeactivateBtn'] ? "block" : "none")})
				   .addClass("idelete")
				 )				
				.append($("<input />",{type:"button", id:"save_changes_"+index, name:"save_changes_"+index, value:"Save Changes"})
				  .css({"display" : (self.options.setupEdit ? "table-cell" : "none")})
				)				
			)			
					
			tr.append($("<td />").css({"display" : (self.options.approvalUpdate ? "table-cell" : "none") })
			    .append($("<input />",{type:"button", id:"allow_update_"+index, name:"allow_update_"+index, value:"Allow Update "})
				 .addClass("imports")
			    )
			)				
	
			$("#allow_update_"+index).live("click", function(){
				if($("#allowupdate_"+index).length > 0)
				{
				     $("#allowupdate_"+index).remove();
				}	

				$("<div />",{id:"allowupdate_"+index, html:"I, <b>username surname</b> of <b>company name</b> hereby:"})
				 .append($("<ul />")
				   .append($("<li />",{html:"declare that i have the required authority to approve the update this leegislation"}))
				   .append($("<li />",{html:"declare that i have reviewed and am hereby authorising all the edits (including deletions) made to the deliverables and actions associated to this <b> name "+legislation.legislation_name+" "+legislation.legislation_reference+"</b>, legislation"}))
				   .append($("<li />",{html:"confirm that this update will overwrite the data that is currently in the Legislation, Deliverable and Action fields"}))				   
				 )
				 .dialog({
			 		autoOpen	: true,
			 		modal 	: true,
			 		title 	: "Update Approval",
			 		buttons	: {
		 						"I Agree"	: function()
		 						{
		  							$.post("../class/request.php?action=NewLegislation.updateEditedLegislation",
		  							{ data : { id : index }}, function( response ){
										if( response.error ){
											 jsDisplayResult("error", "error",  response.text );
										} else {
											 jsDisplayResult("ok", "ok", response.text );
										}					 								
		 							},"json");
		 							$("#allowupdate_"+index).dialog("destroy");
		 							$("#allowupdate_"+index).remove();
		 						} , 
		 						"Decline"	: function()
		 						{
		 							$("#allowupdate_"+index).dialog("destroy");
		 							$("#allowupdate_"+index).remove();
		 						}
	 						  },
		 			  open         : function(event, ui)
		 			  {
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				 $(this).css("height", "auto")
		 			  } ,
		 			  close        : function(event, ui)
		 			  {
						$("#allowupdate_"+index).dialog("destroy");
						$("#allowupdate_"+index).remove();
		 			  }
				    });
				   return false;
				});
									
				$("#activate_"+index).live("click", function(){
                         if($("#legislationauthorisor_"+index).val() === null || $("#legislationauthorisor_"+index).val() === ""){
                           jsDisplayResult("error", "error", "Please select legislation authorizer . . ." );
                         } else if($("#legislationadminstrator_"+index).val() === null || $("#legislationadminstrator_"+index).val() == ""){
                            jsDisplayResult("error", "error", "Please select legislation administrator . . ." );
                         } else {
				
				     if($("#disclaimer_"+index).length >0)
				     {
				       $("#disclaimer_"+index).remove();    
				     }
					$("<div />",{id:"disclaimer_"+index})
					.append($("<p />").addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px"})
					  	.append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))	
					  	.append($("<span />")
					  	  .append($("<span />",{html:"I,"+self.options.user+", of "+self.options.company+" hereby :"}).css({"margin-left":"8px"}))
						  .append($("<ul />").css({"margin-left":"10px"})
						    .append($("<li />",{html:" declare that I have the required authority to activate this legislation"}))
						    .append($("<li />",{html:" confirm my understanding that this legislation, "+leg.legislation_name+"  "+leg.legislation_reference+", contains only the required deliverables and actions (where applicable) relating to the matters that are required to be addressed in this legislation;"}))
						    .append($("<li />",{html:" acknowledge that this legislation, its deliverable(s) and action(s) are published by Ignite Reporting Systems (Pty) Ltd on behalf of, "+leg.responsible_business_patner+" and that the information is not intended to constitute advice on the legislation covered as every situation depends on its own facts and circumstances for which professional advice should be sought from "+legislation.responsible_business_patner+""}))
						    .append($("<li />",{html:" agree that the views expressed (published) are those of the business partner, "+leg.responsible_business_patner+"; and"}))
						    .append($("<li />",{html:" acknowledge that although reasonable care has been taken to ensure the accuracy of this work, Ignite Reporting Systems (Pty) Ltd has expressly disclaimed all and any liability to any person relating to anything done or omitted to be done or to the consequences thereof in reliance upon this work"}))
						   )					  	 		
					  	)
					)				
					 .dialog({
				 		autoOpen	: true,
				 		modal	: true,
				 		title	: "Activating a legislation",
				 		width	: "500px",
				 		buttons	: {
			 						"Confirm Activation"	: function()
			 						{
	
										jsDisplayResult("info", "info", "Activating legislation . . . .<img src='../images/loaderA32.gif' />" );
										var data 		  = {};		 
										data.available     = $("#available_"+index+" :selected").val()
										data.import_del    = $("#import_del_"+index+" :selected").val()
										data.edit_del      = $("#edit_del_"+index+" :selected").val()
										data.create_del    = $("#create_del_"+index+" :selected").val()
										data.import_action = $("#import_actions_"+index+" :selected").val()
										data.edit_action   = $("#edit_actions_"+index+" :selected").val()
										data.create_action = $("#create_actions_"+index+" :selected").val();
										data.legislation_id= index;
										data.authorisor    = $("#legislationauthorisor_"+index).val();
										data.admin  	    = $("#legislationadminstrator_"+index).val();
										$.post("../class/request.php?action=ClientLegislation.activate", {data:data} , 
										function( response ){
											if( response.error ){
												 jsDisplayResult("error", "error",  response.text );
											} else {
												 $("#disclaimer_"+index).dialog("destroy");
												 $("#disclaimer_"+index).remove();
												 jsDisplayResult("ok", "ok", response.text );
												 $("#activate_"+index).attr("disabled", "disabled");
												 $("#inactivate_"+index).attr("disabled", "");
												 self._getLegislation();
											}
										},"json");
			 						} , 
			 						"Cancel Activation"		 : function()
			 						{
										$("#disclaimer_"+index).dialog("destroy");
										$("#disclaimer_"+index).remove();
			 						}
		 						 } ,
					 			open		: function(event, ui)
					 			{
					 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
					 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
					 				var saveButton = buttons[0];
					 				$(saveButton).css({"color":"#090"});
					 				 $(this).css("height", "auto")
					 			} , 
					 			close      : function(event, ui)
					 			{
									$("#disclaimer_"+index).dialog("destroy");
									$("#disclaimer_"+index).remove();  
					 			}			 
					 });
					}
					return false;
				});			
				
				$("#save_changes_"+index).live("click", function(){
					var data 		     = {};		 
					data.active         = $("#available_"+index+" :selected").val()
					data.legislation_id = index;
					data.authoriser     = $("#legislationauthorisor_"+index).val();
					data.admin          = $("#legislationadminstrator_"+index).val();
					
					jsDisplayResult("info", "info", "Activating legislation . . . .<img src='../images/loaderA32.gif' />" );					
					$.post("../class/request.php?action=AdminLegislation.editSetupLegislation",  data , function( response ){
						if( response.error ){
							 jsDisplayResult("error", "error",  response.text );
						} else {							 
							 jsDisplayResult("ok", "ok", response.text );
							 $("#activate_"+index).attr("disabled", "disabled");
							 $("#inactivate_"+index).attr("disabled", "");
							 self._getLegislation();
						}
					},"json");
					
					return false;
				});
										
				$("#inactivate_"+index).live("click", function(){
					
					jsDisplayResult("info", "info", "Inactivating legislation . . . .<img src='../images/loaderA32.gif' />" );
					var data 		    = {};		 
					data.legislation_id = index;
					
					$.post("../class/request.php?action=ClientLegislation.inactivate",  data , function( response ){
						if( response.error ){
							 jsDisplayResult("error", "error",  response.text );
						} else {
							 jsDisplayResult("ok", "ok", response.text );
							 $("#activate_"+index).attr("disabled", "");
							 $("#inactivate_"+index).attr("disabled", "disabled");
							 self._getLegislation();
						}
						
					},"json");
					
					return false;
				});	
		
				$("#import_deliverable_"+index).live("click", function(){
					document.location.href = "import_a_deliverable.php?id="+index;
					return false;
				});
				
				$("#reassign_deliverable_"+index).live("click", function(){
					document.location.href = "reassign_deliverables.php?id="+index;
					return false;
				});				
				
				$("#assurance_deliverable_"+index).live("click", function(){
					document.location.href = "assurance_deliverables.php?id="+index;
					return false;
				});		
				
				$("#reassign_actions_"+index).live("click", function(){
					document.location.href = "reassign_actions.php?id="+index;
					return false;
				});		

				$("#assurance_legislation_"+index).live("click", function(){
					document.location.href = "assurance_legislation.php?id="+index;
					return false;
				});
				
				$("#create_deliverable_"+index).live("click", function(){
					document.location.href = "add_deliverable.php?id="+index;
					return false;
				});		
				
				$("#add_actions_"+index).live("click", function(){
					document.location.href = "add_actions.php?id="+index;
					return false;
				});					

				$("#create_deliverable_"+index).live("click", function(){
					document.location.href = "add_deliverable.php?id="+index;
					return false;
				});					

				$("#update_deliverable_"+index).live("click", function(){
					document.location.href = "update_deliverables.php?id="+index;
					return false;
				});	
				
				$("#update_legislation_"+index).live("click", function(){
					document.location.href = "update_legislation.php?id="+index;
					return false;
				});								
				
				$("#edit_legislation_"+index).live("click", function(){
					document.location.href = "edit_legislation.php?id="+index;
					return false;
				});
				
				$("#edit_deliverable_"+index).live("click", function(){
					document.location.href = "edit_deliverables.php?id="+index;
					return false;
				});		
				
				$("#approval_deliverable_"+index).live("click", function(){
					document.location.href = "approval_deliverable.php?id="+index;
					return false;
				});									
			$("#"+self.options.legislationTable).append( tr )
		})		
		
		
		$.each(legislations , function(index, legislation ){
			$.each(users, function( i, val) {
			
			  $("#legislationauthorisor_"+index).append($("<option />",{text:val.user, value:i}))
			  $("#legislationadminstrator_"+index).append($("<option />",{text:val.user, value:i}))							
			  			
				if(authorizor[index] != undefined)
				{
				  $.each( authorizor[index], function(aIndex, aVal){
					  if(i === aVal)
					   {
					      $("#legislationauthorisor_"+index+" option[value='"+aVal+"']").attr("selected", "selected");	
					      $("#activated_authorizor_"+index).append(users[aVal].user+"<br />" )
					  }
				   })						
				}
				if(adminstrator[index] != undefined)
				{
					$.each(adminstrator[index], function(aIndex, aVal){
						 //$("#legislationadminstrator_"+index+" option[value='"+aVal+"']").attr("selected", "selected")
						if(i == aVal) 
						{					
						    $("#legislationadminstrator_"+index+" option[value='"+aVal+"']").attr("selected", "selected");	
						    $("#activated_administator_"+index).append( users[aVal].user+"<br />" )
						}					 
						 //$("#activated_administator_"+index).append( users[aVal].name)
					})						
				}					
			});
			var selectYes = false;
			if(self.options.legStatus.hasOwnProperty(index))
			{
			   selectYes = true;
			} 
			if((self.options.legStatus[index] & 2) === 2)
			{
			   $("#available_"+index).val(1);
			} else {
			   $("select#available_"+index).val(0);
			}
						
			if(((self.options.legStatus[index] & 8) == 8) && selectYes == true)  
			{
				$("#edit_del_"+index).val(1);
			} else {
				$("select#edit_del_"+index).val(0);
			}			
			if(((self.options.legStatus[index] & 16) == 16) && selectYes == true)  
			{
				$("#create_del_"+index).val(1);
			} else {
				$("#create_del_"+index).val(0);
			}
			if(((self.options.legStatus[index] & 32) == 32) && selectYes == true)  
			{
				$("select#import_actions_"+index).val(1);
			} else {
				$("select#create_actions_"+index).val(0);
			}
			
			if(((self.options.legStatus[index] & 64) == 64) && selectYes == true)  
			{
				$("select#edit_actions_"+index).val(1);
			} else {
				$("select#create_actions_"+index).val(0);
			}			
			
			if(((self.options.legStatus[index] & 128) == 128) && selectYes == true)  
			{
				$("select#create_actions_"+index).val(1);
			} else {
				$("select#create_actions_"+index).val(0);
			}
			$("select#legislationadminstrator_"+index).attr("size", function() {
		 		return $("#legislationadminstrator_"+index+" option").length; 	
			});		
			  
			$("select#legislationauthorisor_"+index).attr("size", function(){
		 	     return $("#legislationauthorisor_"+index+" option").length; 	
			});								  			
		});	
							
	} , 

     _indexOf       : function(arrayData, obj)
     {
          for(var i = 0; i < arrayData.length; i++)
          {
             if(arrayData[i] == obj)
             {
               return i;     
             }
          }
         return -1;
     } , 

	_displayHeaders 		: function(headers, extraHeader)
	{
		var self = this;
		var tr = $("<tr />")
		$.each(headers , function(index, head){
		   tr.append($("<th />",{html:head}));			
		});
		
		tr.append($("<th />",{html:"Legislation Authorisor"}).css({"display":((self.options.setupLegislation && extraHeader) ? "table-cell" : "none") }))
		tr.append($("<th />",{html:"Legislation Administrator"}).css({"display":((self.options.setupLegislation  && extraHeader) ? "table-cell" : "none") }))
		tr.append($("<th />",{html:(self.options.setupActivate ? "Must this legislation made available?" : "Must this legislation made active?")}).css({"display":((self.options.setupLegislation  && extraHeader) ? "table-cell" : "none")}))
		
		tr.append($("<th />").css({"display":((self.options.setupActivate  && extraHeader) ? "table-cell" : "none") })
		  .append($("<table />",{width:"100%"}).addClass("noborder")
			  .append($("<tr />")
			 	 .append($("<th />",{html:"Deliverable",colspan:"3",align:"center"}).addClass("noborder"))
			  )
			  .append($("<tr />")
				.append($("<th />",{html:"Import"}).addClass("noborder"))
				.append($("<th />",{html:"Edit"}).addClass("noborder"))
				.append($("<th />",{html:"Create"}).addClass("noborder"))
			  )						
			)
		)
		tr.append($("<th />").css({"display":((self.options.setupActivate  && extraHeader) ? "table-cell" : "none") })
			.append($("<table />",{width:"100%"}).addClass("noborder")
			  .append($("<tr />")
				  .append($("<th />",{html:"Action",colspan:"3", align:"center"}).addClass("noborder"))
			  )
			  .append($("<tr />")
				.append($("<th />",{html:"Import"}).addClass("noborder"))
				.append($("<th />",{html:"Edit"}).addClass("noborder"))
				.append($("<th />",{html:"Create"}).addClass("noborder"))
			  )						
			)
		)	
	       .append($("<th />",{html:"Legislation Status"}).css({"display":((self.options.copyFinancialYear && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;Copy&nbsp;"}).css({"display":((self.options.copyFinancialYear && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;"}).css({"display":((self.options.page  == "import_legislation"  && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;Status&nbsp;&nbsp;", colspan:"1"}).css({"display":((self.options.setupLegislation  && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.viewDetails && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.createDeliverable && extraHeader) ? "table-cell" : "none") }))
		  //.append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.importLegislation && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.addAction && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.updateLegislation && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.reassignActions && extraHeader) ? "table-cell" : "none") }))
		  //.append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.editLegislation && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.deactivateLegislation && extraHeader) ? "table-cell" : "none") }))
		 .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.reassignDeliverable && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.assuranceLegislation && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.approval && extraHeader) ? "table-cell" : "none") }))
		  .append($("<th />",{html:"&nbsp;&nbsp;"}).css({"display":((self.options.approvalUpdate && extraHeader) ? "table-cell" : "none") }))
		  //)		

		$("#"+self.options.legislationTable).append(tr);
	} , 
	
	_displayPaging		: function( columns )
	{
		var self = this;
		
		var pages;
		if( self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}
		if( self.options.setupLegislation )
		{
			columns = 8;
		} else if(self.options.deactivateLegislation) {
			columns++;
		}
		$("#"+self.options.legislationTable)
		  .append($("<tr />")
			.append($("<td />",{colspan:columns})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < "}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| "}))			   
		   )			
		  .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({"display":(self.options.copyFinancialYear ? "table-cell" : "none") }))
		  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.page == "import_legislation" ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;",colspan:"1"}).css({"display" : (self.options.setupActivate ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.viewDetails ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.createDeliverable ? "table-cell" : "none") }))
		  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.importLegislation ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.addAction ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.updateLegislation ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.reassignActions ? "table-cell" : "none") }))
		  //.append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.editLegislation ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.reassignDeliverable ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.assuranceLegislation ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approval ? "table-cell" : "none") }))
		  .append($("<td />",{html:"&nbsp;&nbsp;"}).css({"display" : ( self.options.approvalUpdate ? "table-cell" : "none") }))
		  )
		  if(self.options.current < 2)
		  {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
		  }
		  if((self.options.current == pages || pages == 0))
		  {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
		  }		  
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
	},
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getLegislation();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getLegislation();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getLegislation();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getLegislation();				
	} ,
	
	_copyLegislation              : function(legislation_id)
	{
	   var self = this;
	   $("#copy_"+legislation_id).attr("disabled","disabled");
	   jsDisplayResult("info", "info", "Copying . . . .<img src='../images/loaderA32.gif' />" );
         $.post("../class/request.php?action=Legislation.copyLegislation",
           {legislation:legislation_id, financial_year:$("#to_financial_year").val()}, function(response){
           if(response.error)
           {
				$("#copy_"+legislation_id).attr("disabled","");
              jsDisplayResult("error", "error", response.text);
           } else {
				$("#copy_"+legislation_id).hide();
				$("#tr_"+legislation_id).addClass("view-notowner").css("background-color","#dedede");
              jsDisplayResult("ok", "ok", response.text);
   	       //console.log(response)	   
           }
         }, "json");
	} ,
	
	_displayBtnCheck              : function(legislationId, isLegislationOwner)
	{
        var self  = this;
        var  btnDisplayOptions = {}	 
        
        //set which button should be displayed on different conditions
        /*
          displayed for legislation owner
          displayed for legislation that are manully created only
        */
        btnDisplayOptions['displayEditBtn']         = false; 
        /*
          displayed for legislation owner
          displayed for user who manually created the legislation ie legislation status = 2048
        */
        btnDisplayOptions['displayUpdateBtn']       = false; //displayed for legislation owner 
        /*
          has view permissions
        */
        btnDisplayOptions['displayViewBtn']         = false; //all
        /*
          allowed for legislation owner and deliverable responsible owner, 
          allowed for user who have permission to add an action , ie legislation_status = 128
          allowed for legislation manually created ie legislation status = 2048
        
          btnDisplayOptions['displayAddActionsBtn']   = false;        
        */
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayUpdateDelBtn']    = false; 
        /*
          displayed for legislation owner
          displayed if the user has permissions to edit then , ie legislation status = 8
        */
        btnDisplayOptions['displayEditDelBtn']      = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayReAssignDelBtn']  = false; //displayed for legislation owner
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayReAssignActionsBtn']  = false; 
        /*
           displayed for legislation owner 
           and current person's manager
        */
        btnDisplayOptions['displayAssuranceBtn']    = true; //false;
        /*
          displayed for legislation owner 
          and current person's manager
        */
        btnDisplayOptions['displayAssuranceDelBtn'] = true; //false;  
        /*
          displayed for legislation owner 
          and current person's manager 
        */
        btnDisplayOptions['displayApprovalBtn']     = false;
        /*
          displayed for legislation owner 
          display if the legislation has not yet been imported ie if legislation status is not 256 
        */
        btnDisplayOptions['displayImportBtn']       = false; 
        /*
          displayed for legislation owner 
        btnDisplayOptions['displayImportDelBtn']    = false; 
        */
        
        /*
        */
        btnDisplayOptions['displayActivateBtn']     = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayDeactivateBtn']   = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displaySaveBtn']         = false; 
        /*
          displayed for legislation owner and current person's manager
        */
        btnDisplayOptions['displayAllowUpdate']     = false;  
        /*
         display for the legislation owner
         if its addDeliverable is set to true
        (self.options.addDeliverable ? (isLegislationOwner[index] ? "block" : "none") : "none")
        */
        btnDisplayOptions['displayCreateDelBtn']     = false;
        
        btnDisplayOptions['displayAddActions']       = false;
        btnDisplayOptions['editDelBtnValue'] = "Edit Deliverable";
        //if theere is a a legislation owner, then show the following button because the owner the respective actions
        if(isLegislationOwner[legislationId] !== undefined)
        {
           if((self.options.legStatus[legislationId] & 2048) === 2048)
           {
              btnDisplayOptions['displayEditBtn']   = true;
              btnDisplayOptions['displayCreateDelBtn']   = true;
           }
           btnDisplayOptions['displayUpdateBtn'] = true;
           btnDisplayOptions['displayUpdateDelBtn']   = true;
           btnDisplayOptions['displayEditDelBtn']     = true;
           btnDisplayOptions['displayReAssignDelBtn'] = true;
           btnDisplayOptions['displayAssuranceBtn']   = true;
           btnDisplayOptions['displayAssuranceDelBtn']= true;
           btnDisplayOptions['displayApprovalBtn']    = true;
           btnDisplayOptions['displayImportBtn']      = true;
           //btnDisplayOptions['displayDeactivateBtn']  = true;
           btnDisplayOptions['displaySaveBtn']        = true;
           btnDisplayOptions['displayAllowUpdate']    = true;
        }
        
        if(((self.options.legStatus[legislationId] & 128) == 128) || ((self.options.legStatus[legislationId] & 2048) == 2048))
        {
          btnDisplayOptions['displayAddActions']       = true;
        }
        
        //if user has permissions to edit the deliverable , show the edit deliverable button
        if((self.options.legStatus[legislationId] & 8) == 8)
        {
           btnDisplayOptions['displayEditDelBtn'] = true;
        }
        //if the legislation has been imported then , dont display the button
        if((self.options.legStatus[legislationId] & 256) == 256)
        {
          if(self.options.page == "manage")
          {
            btnDisplayOptions['editDelBtnValue'] = "Edit Deliverables";
          }          
          btnDisplayOptions['displayImportBtn'] = false;
        } else {
          btnDisplayOptions['displayImportBtn'] = true;
        }
        
        //if the legislation is not in used array , then show the activate button
        if(self.options.section == "setup")
        {
             //if legislation is active , then show the de-activate button else show active button
             if((self.options.legStatus[legislationId] & 1) == 1)
             {
               btnDisplayOptions['displayDeactivateBtn']   = true; 
             } else {
               btnDisplayOptions['displayActivateBtn'] = true; 
             }
        }
        
        if(self.options.section === "admin" || self.options.adminPage)
        { 
           btnDisplayOptions['displayEditBtn']       = true;
           btnDisplayOptions['displayUpdateBtn']     = true;
           btnDisplayOptions['displayUpdateDelBtn']  = false;
           btnDisplayOptions['displayEditDelBtn']    = false;
           btnDisplayOptions['displayCreateDelBtn']  = false;                      
        }
       return btnDisplayOptions;                   
	}
});
