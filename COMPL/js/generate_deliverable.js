// JavaScript Document
$(function(){

     DeliverableReport.getOrganisationTypes();
     DeliverableReport.getFunctionalService();
     DeliverableReport.getMainEvents();
     DeliverableReport.getDepartments();
     DeliverableReport.getComplianceFrequency();
     DeliverableReport.getResponsibleOwner();		
     DeliverableReport.getAccountablePerson();		
     DeliverableReport.getComplianceTo();	
     DeliverableReport.getSubEvents();				
     DeliverableReport.getFinancialYear();
     
    $("#sortable").sortable().disableSelection();  		
	if($("#quickid").val() === undefined)
	{
	   $(".deliverable").attr("checked","checked");
	} else {
	     $.getJSON("../class/request.php?action=Report.getAQuickReport", {quickid : $("#quickid").val()}, function(response){
		     $("#report_description").text( response.description )
		     $("#report_name").val( response.name )
		     $("#report_title").val( response.data.report_title )
		     //check the legislatin fields that have been selected
		     $(".deliverable").each(function(){
			   var id = $(this).attr("id")
			   $.each(response.data.deliverables, function(key  ,val){
				if("deliverable_"+key === id)
				{ 
				   $("#"+id).attr("checked", "checked");					
				}
			   });
		     });
		
		     $.each(response.data.value, function( index, val){
			    if(index == "legislation_deadline_date")
			    {
				  $("#from_legislation_deadline").val( val.from )
				  $("#to_legislation_deadline").val( val.to )
			    } else if(index == "compliance_date"){
				  $("#from_compliance_date").val( val.from )
				  $("#to_compliance_date").val( val.to )			     
			    } else {				
				  $("#"+index).val(val);
				  $("#"+index).text(val);
				  $("#"+index+" option[value='"+val+"']").attr("selected", "selected")								
			    }
		     });
		     $.each(response.data['match'], function(index , key){
			     $("#match_"+index+" option[value='"+key+"']").attr("selected", "selected")
		     });		
		
		     $("#group option[value='"+response.data.group_by+"']").attr("selected", "selected")
		
		     $.each(response['sort'], function(index , val){
			     $("#sortable").append($("<li />").addClass("ui-state-default")
							     .append($("<span />",{id:"__"+index}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort"))
							     .append($("<input />",{type:"hidden", name:"sort[]", value:"__"+index}))
							     .append( val )
			     );			
		     });
		
	     }); 
	}
	$("#check_deliverable").click(function(e){
		$(".deliverable").each(function(){
			if( !$(this).is(":checked") )
			{
				$(this).attr("checked", "checked");			
			} 
		})
		e.preventDefault();
	});
	
	$("#invert_deliverable").click(function(e){
		$(".deliverable").each(function(){
			if($(this).is(":checked")){
				$(this).attr("checked", "")
			} else {
				$(this).attr("checked", "checked")				
			}			
		});		
		e.preventDefault();
	});
	    
    
});

DeliverableReport			= {
     getFinancialYear      : function()
     {
      // $("#financial_year").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financial_year").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 
     		
	getActionOwner 		: function (){
		$.getJSON("../class/request.php?action=User.getAll", function( users ){
		$.each( users, function( index, user){
				$("#action_owner").append($("<option />",{value:user.id, text:user.name}))								
			})													 
		});
     } ,

	getComplianceFrequency	: function(){
		$.post("../class/request.php?action=ComplianceFrequency.getAll",{data:{status:1}},function( compfrequencies ){
            $.each(compfrequencies, function( index, complianceFrequency){
               $("#compliance_frequency").append($("<option />",{text:complianceFrequency.name, value:complianceFrequency.id }))
            });
        },"json");
	} ,
	
	getComplianceTo		: function(){
		$.post("../class/request.php?action=ComplianceToManager.getAll",{data:{status:1}}, function( data ){
            $.each( data, function( index, compliance){
             $("#compliance_to").append($("<option />",{text:compliance.title+" - "+compliance.user, value:compliance.id }))
           });
        },"json");
	} ,	
	
	getResponsibleOwner				: function(){
	    $.post("../class/request.php?action=User.getAll", function(users){
           $.each(users, function(index, user){
             $("#responsibility_owner").append($("<option />",{text:user.user, value:index}))
           });
        },"json");
	} ,
	
	
	getAccountablePerson				: function(){
		$.post("../class/request.php?action=AccountablePersonManager.getAll",{data:{status:1}},function( data ){
         $.each( data, function( index, accPerson){
            $("#accountable_person").append($("<option />",{text:accPerson.role, value:index}))
         });
        },"json");
	} ,	
	
	getUsers 	: function()
	{
		$.getJSON("../class/request.php?action=User.getAll", function( users ){
			$.each( users, function( index, user){
				$("#user").append($("<option />",{text:user.tkname, value:user.tkid}))		
				
				//$("#accountable_person").append($("<option />",{text:user.tkname, value:user.tkid}))
				
				//$("#responsibility_owner").append($("<option />",{text:user.tkname, value:user.tkid}))

				//$("#compliance_to").append($("<option />",{text:user.tkname, value:user.tkid}))
			});
		});	
	} , 	
	
    getDepartments:function()
    {
        $.get("../class/request.php?action=ClientDepartment.getAll", function( direData ){
          $.each( direData ,function( index, directorate ) {
                $("#responsible").append($("<option />",{text:directorate.name, value:index}));
          })
       }, "json");

    },
    
	getOrganisationTypes	: function()
	{
		$.post("../class/request.php?action=OrganisationTypesManager.getAll",{data:{status:1}},function( orgtypes ){
			$.each( orgtypes, function( index, orgtype ){
				$("#applicable_org_type").append($("<option />",{text:orgtype.name, value:orgtype.id}))
			});
		}, "json");	
	} ,
	
	getFunctionalService	: function()
	{
		$.post("../class/request.php?action=FunctionalServiceManager.getClientFunctionalServices",
			 { active:1 }, function( functional_services ){
			$.each( functional_services, function( index, functional_service ){
				$("#functional_service").append($("<option />",{text:functional_service, value:index}))
			});
		}, "json");		
	} ,

	getMainEvents	: function()
	{
		$.post("../class/request.php?action=Event.getAll",{data:{status:1}}, function( main_events ){
			$.each( main_events, function( index, main_event ){
				$("#main_event").append($("<option />",{text:main_event.name, value:main_event.id}))
			});
		}, "json");
	} ,

	getSubEvents	: function( id )
	{
	   $("#sub_event").empty();
        var data	= {};
        data.eventid = id;
        data.status  = 1;
		$.post("../class/request.php?action=SubEvent.getAll",{data:data}, function( sub_events ){
			$.each(sub_events, function( index, sub_event ){
				$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
			});
			if(id == 1)
			{
			  $("#subevent option[value=1]").attr("selected", "selected")
			}
		}, "json");
	} ,

     getReportingCategory     : function()
     {
		$.getJSON("../class/request.php?action=ReportingCategories.getReportingCategories",{data:{status:1}}, function( reportingCategories ){
		$.each( reportingCategories, function( index, reportingCategory){
				$("#reporting_category").append($("<option />",{value:reportingCategory.id, text:reportingCategory.name}))
			})													 
		});
     } 	
}
