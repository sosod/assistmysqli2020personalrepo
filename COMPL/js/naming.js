$(function(){
	
	Naming.get();
	
});

Naming		= {
		
		get 	: function()
		{
			$.getJSON("../class/request.php?action=Naming.getNaming", function( responseData ){
				
				if( $.isEmptyObject( responseData )){
										
				} else {
					Naming.display( responseData );					
				}		
			});
		} , 
		
		display		: function( data )
		{
			$("#naming").append($("<table />",{id:"naming_table"})
						  .append($("<tr />")
							.append($("<th />",{html:"Ref"}))
							.append($("<th />",{html:"Ignite Terminology"}))
							.append($("<th />",{html:"Client Terminology"}))
							.append($("<th />",{html:"&nbsp;"}))
						  )					
			)
			
			$.each( data , function( index, naming){
				$("#naming_table").append($("<tr />")
									.append($("<td />",{html:naming.id}))
									.append($("<td />",{html:naming.ignite_terminology}))
									.append($("<td />")
									  .append($("<input />",{type:"text", id:"naming_"+naming.id, name:"naming_"+naming.id, value:naming.client_terminology, size:50}))		
									)
									.append($("<td />")
									  .append($("<input />",{type:"button", id:"update_"+naming.id, name:"update_"+naming.id, value:"Update"}))	
									)
				)	
				$("#update_"+naming.id).live("click", function(){
					jsDisplayResult("info", "info", "Updating header names ... <img src='../images/loaderA32.gif' >");
					
					$.post("../class/request.php?action=BaseNaming.update", { client_terminology: $("#menu_"+naming.id).val(), id : naming.id}, function( response ){
						if( response == 1){
							jsDisplayResult("ok", "ok", response.text );
						} else if( response == 0){
							jsDisplayResult("info", "info", response.text );
						} else {
							jsDisplayResult("error", "error", response.text );
						}				
					}, "json");
					
					
				    return false;	
				});				
			});	
		}
		
		
		
};
