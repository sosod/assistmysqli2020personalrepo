$.widget("ui.viewdetail", {
 
    // These options will be used as defaults
    options: {
			start 			: 0,
      	limit 			: 10,
      	current 			: 1,
      	total 			: 0,
      	editDeliverble : false,
      	editActions		: false,
      	legislationId 	: "",
      	url	 			: "../class/request.php?action=NewDeliverable.getDeliverableActions",
		   page				: "",
		   confirmation   : false,
		   details 			: false,
		   triggerBy		: true     		
    },
    
    _init		: function()
    {
		this._get();
    } ,
    // Set up the widget
    _create		: function() {
    	var self = this;
    },
    
    _get			: function()
    {
    	var self = this;
    	$.get( self.options.url, {
    		start 	: self.options.start,
    		limit 	: self.options.limit,
    		options 	: {
    						legislationId  : self.options.legislationId
    		}    	
    	}, function( deliverableActions ){
    		self._display( deliverableActions );    			
    		self.options.total = $("#total").val()
    	},'html');
    	
    } , 
    
    _display			: function( deliverableActions )
    {
    	 var self = this;
    	 $(self.element).html( deliverableActions );

    	 
    	 $("#confirm_"+self.options.legislationId).live("click", function(){
			$("<div />",{html:"Confirmation of legislation", id:"confirmationdialog_"+self.options.legislationId})
			 .dialog({
				 		autoOpen	: true,
				 		modal		: true,
				 		title		: "Confirmation of legislation",
				 		buttons		: {
				 						"Confirm"	: function()
				 						{
					 						var data = {};
											data.id  = self.options.legislationId;
											var confirmActivated = [];
											var confirmInactive  = [];
											data.activated   = deleverableActivated; 
											data.deactivated = deleverableDeactivated
											data.ids 		  = deliverablesId;
											jsDisplayResult("info", "info", "Confirming  . . . .<img src='../images/loaderA32.gif' />");
											$.post("../class/request.php?action=NewLegislation.confirmLegislation",
											{ data : data }, function( response) {
												if( response.error )
												{
													jsDisplayResult("error", "error", response.text);
												} else{
													document.location.href = "";
													jsDisplayResult("ok", "ok", response.text);				
												}											
					 							$("#confirmationdialog_"+self.options.legislationId).dialog("destroy")
					 							$("#confirmationdialog_"+self.options.legislationId).remove()
											}, "json") 
				 						} ,
				 						
				 						"Cancel"	: function()
				 						{
			 							$("#confirmationdialog_"+self.options.legislationId).dialog("destroy")
			 							$("#confirmationdialog_"+self.options.legislationId).remove()			
				 						}
				 
			 						}	
			 })
    	 	return false;
    	 }); 
		
    	 $("#first").live("click", function(){
			self.options.current  = 1;
			self.options.start 	= 0;
    	   self._get();
    	 	return false;
    	 });
  
    	 $("#previous").live("click", function(){
			self.options.current  = parseFloat( self.options.current ) - 1;
			self.options.start 	= (self.options.current-1)*self.options.limit;
    	 	self._get();
    	 	return false;
    	 });
    	     	     	 
    	 $("#next").live("click", function(){
			self.options.current = self.options.current+1;
			self.options.start   = parseFloat( self.options.current - 1) * parseFloat( self.options.limit );   	 	
    	 	self._get();
    	 	return false;
    	 });
    	 
    	 $("#last").live("click", function(){ 
			self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
			self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );		    	 
    	 	self._get(); 
    	 	return false;
    	 });
    	 
    	 $("#currentpage").html( self.options.current );
    	 $("#start").val( self.options.start );
    	 $("#limit").val( self.options.limit );
    }
 
 
});
