$(function(){
   FinancialYear.get();  
});


var FinancialYear    = {

   get      : function()
   {
      $(".financialyear").remove();
      $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function( responseList ){

         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
               FinancialYear.display( financialyear )
            })
         }      
      });
   } , 
   
   display     : function( financialYear )
   {
      $("#table_financialyears").append($("<tr />").addClass('financialyear')
        .append($("<td />",{html:financialYear.id}))
        .append($("<td />",{html:financialYear.value}))
        .append($("<td />",{html:financialYear.start_date}))
        .append($("<td />",{html:financialYear.end_date}))
        .append($("<td />",{html:((financialYear.status & 1) == 1 ? "Active" : "Inactive")}))
        .append($("<td />")
          .append($("<input />",{
                                 type : "button",
                                 name : "copy_"+financialYear.id,
                                 id   : (financialYear.used ? "copy" : "copy_"+financialYear.id),
                                 disabled : (financialYear.used ? "disabled" : ""),
                                 value:"Copy"}))
        )
      )
      
      $("#copy_"+financialYear.id).live("click", function(){
      
         jsDisplayResult("info", "info", "Copying financial year . . . .<img src='../images/loaderA32.gif' />");
         $.post("../class/request.php?action=SetupManager.savecopyFinancialYear",{
            data : {master_id:financialYear.id}
         }, function( response ){
            if(response.error)
            {
               jsDisplayResult("error", "error", response.text);
            } else {
               jsDisplayResult("ok", "ok", response.text);
               FinancialYear.get();
            }
         },"json")
         return false;
      });
   }


}
