$(function(){
	if( $("#actionId").val() == undefined){
		Action.getOwners();
	}
	//function to be called when an attachment is downloaded from the approval logs
	/*$('a.downloadAttach').click(function() {
		var fn = $(this).attr('filen');
		var n = $(this).attr('fname');
		url = '../class/request.php?action=ClientAction.downloadFile&file='+fn+'&name='+n;
		document.location.href = url;
	});*/

     $("#copysave").click(function(e){
          type = "";
          if( $("#actiontype").val() != undefined)
          {
             type = $("#actiontype").val();
          }    
        Action.copy(type);
       e.preventDefault();
     });

	$("#save").click(function(){
	   type = "";
	   if( $("#actiontype").val() != undefined)
	   {
	     type = $("#actiontype").val();
	   }
        Action.save(type);
	   return false;
	});
	
	$("#edit").click(function(){
		Action.edit();
		return false;
	});

	$("#update").click(function(){
		//alert("#update clicked");
		Action.update();
		return false;
	});
		

	$("#status").change(function(){
	    
		if( $(this).val() == 3)
		{
		   $("#progress").val("100")
		   $("#request_approval").removeAttr("disabled");
		} else {
			if($("#progress").val()=="100") {
				$("#progress").val( $("#_progress").val() )
			}
		   $("#request_approval").attr("disabled", "disabled");
		}
		return false;
	});
	
	$("#progress").blur(function(){
		if( $(this).val() == 100)
		{
		   $("#status").val(3);
		   $("#request_approval").removeAttr("disabled");
		} else {
		   $("#status").val(2);
		   $("#request_approval").attr("disabled", "disabled");
			$("#_progress").val($(this).val());
		} 
	});	
	
	$("#saveaction").click(function(e){
	    if($("#saveactionDialog").length > 0)
	    {
	     $("#saveactionDialog").remove();
	    }
	     
	    $("<div />",{id:"saveactionDialog", html:"Confirm adding an action  . . . "})
	     .append($("<ul />")
	       
	     ).dialog({
	               autoOpen  : true,
	               modal     : true,
	               title     : "Add New Action",
	               width     : 300,
	               height    : 300,
	               position  : "top",
	               buttons   : {
	                              "Ok"      : function()
	                              {
	                                Action.save("manage");
	                                $("#saveactionDialog").dialog("destroy");
	                                $("#saveactionDialog").remove();	                                                 
	                              } ,
	                              "Cancel"  : function()
	                              {
	                                $("#saveactionDialog").dialog("destroy");
	                                $("#saveactionDialog").remove();	                              
	                              }
	              },
	              close       : function(event, ui)
	              {
	                 $("#saveactionDialog").dialog("destroy");
	                 $("#saveactionDialog").remove();
	              }, 
	              open        : function(event, ui)
	              {
	              
	              }     
	     }); 
	  e.preventDefault();
	});
	
	$(".remove_attach").click(function(e){
          var id  = this.id
          var ext = $(this).attr('title');
          $.post('../class/request.php?action=ClientAction.deleteAttachment', 
            { attachment : id,
              ext        : ext,
              action_id  : $("#action_id").val()
             }, function(response){     
            if(response.error)
            {
              $("#result_message").html(response.text)
            } else {
               $("#result_message").addClass('ui-state-ok').html(response.text)
               $("#li_"+ext).fadeOut();
            }
          },'json');                   
          e.preventDefault();
	});

	$("#show_deliverable").click(function(e){
	   Action.showDeliverable()
	   e.preventDefault()
	});
	
	$("#legislation").click(function(e){
	  Action.showLegislation()  	
	  e.preventDefault();
	});	

	
});
//create action namespace
var Action = window.Action || {};
var Action 		= {
	
		
	getOwners					: function()
	{
		$.getJSON("../class/request.php?action=User.getAll", function( users ){
			$.each( users, function( index, user){
				$("#action_owner").append($("<option />",{value:index, text:user.user}))									
			})													 
		});
	} ,
	
	copy				: function(type)
	{
		var data		    = {};
		data.action	    = $("#action_name").val();
		data.owner  	    = $("#action_owner :selected").val();
		data.action_deliverable   = $("#deliverable").val();
		data.deadline	    = $("#deadline").val();
		data.reminder	    = $("#reminder").val();
		data.deliverable_id = $("#deliverableid").val();
		data.actiontype     = type;
		if( $("#recurring").is(":checked")) {
			data.recurring	        = 1;
		}		
		var valid = Action.isValid( data ); 
		if( valid == true) {
			jsDisplayResult("info", "info", "Copying Action . . . .<img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientAction.copySaveAction",{ data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
                         $("#action_owner option[value='']").attr("selected", "selected");
                         $("#reminder").val("");
                         $("#deadline").val("");					
					jsDisplayResult("ok", "ok", response.text);
				}    
			},"json")
			
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} ,
	
	save				: function(type)
	{
		var data		           = {};
		data.action	           = $("#action_name").val();
		data.owner  	           = $("#action_owner :selected").val();
		data.action_deliverable   = $("#deliverable").val();
		data.deadline	           = $("#deadline").val();
		data.reminder	           = $("#reminder").val();
		data.deliverable_id       = $("#deliverableid").val();
		data.actiontype           = type;
		if($("#recurring").is(":checked")) 
		{
			data.recurring	        = 1;
		}		
		var valid = Action.isValid( data ); 
		if( valid == true)
		{
             jsDisplayResult("info", "info", "Saving Action . . . .<img src='../images/loaderA32.gif' />");
             $.post("../class/request.php?action=ClientAction.saveAction",{ data : data }, function( response ){
	           if( response.error )
	           {
		          jsDisplayResult("error", "error", response.text);
	           } else{
	               /*$("#display_action").html("");
	               $("#display_action").action({editAction:true, deleteActions:true, id:$("#deliverableid").val(), 
	                                            controller:"newcontroller"});
	               //document.location.href = "";
	               Action.emptyField(data)					*/
	               //jsDisplayResult("ok", "ok", response.text);
				   //document.location.href = document.location.href+"&r[]=ok&r[]="+response.text;
				   redirectAfterActivity(response.text);
	           }    
            },"json")
		} else {
		   jsDisplayResult("error", "error", valid);
		}

	} ,

	
	edit				: function()
	{
		var data		        = {};
		data.action		   = $("#action_name").val();
		data.owner  	        = $("#action_owner :selected").val();
		data.action_deliverable   = $("#deliverable").val();
		data.deadline	        = $("#deadline").val();
		data.reminder	        = $("#reminder").val();
		data.id	   		        = $("#actionId").val();
		data.deliverable_id     = $("#deliverable_id").val();
		if($("#recurring").is(":checked")) 
		{
			data.recurring	        = 1;
		}		
		var valid = Action.isValid( data ); 
		if( valid == true){
			jsDisplayResult("info", "info", "Updating Action . . . .<img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientAction.editAction",{ data : data }, function( response ){
				if( response.error )
				{
				  jsDisplayResult("error", "error", response.text);
				} else {
				   if(response.updated)
				   {
				     jsDisplayResult("ok", "ok", response.text);
				   } else {
				     jsDisplayResult("info", "info", response.text);
				   }
				}    
			},"json")
			
		} else {
			jsDisplayResult("error", "error", valid);
		}
		
	} , 
	
	
	update				: function()
	{
		//alert("action.update");
		var data	   = {};
		var response  = $("#update_response").val();
		var status    = $("#status :selected").val();
		var progress  = $("#progress").val();
		var reminder  = $("#remindon").val();
		var date_completed = $("#date_completed").val();
		var valid8 = true;
		var err = "";
		if( response  == ""){
		   //jsDisplayResult("error", "error", "Please enter the reponse message");
		   //$("#response").focus();
		   err+="<li><b>Response</b> is required.</li>";
		   valid8 = false;
		   $("#update_response").addClass("required");
		   //return false;
		} else {
		   $("#update_response").removeClass("required");
		}
		if(status == "") {
		   //jsDisplayResult("error", "error", "Please select the status");
		   //$("#status").focus();
		   //return false;
		   valid8 = false;
		   $("#status").addClass("required");
		   err+="<li><b>Status</b> is required.</li>";
		} else {
		   $("#status").removeClass("required");
		} 
		if(progress == "") {
		   //jsDisplayResult("error", "error", "Please enter progress");
		   //$("#progress").focus();
		   //return false;
		   valid8 = false;
		   $("#progress").addClass("required");
		   err+="<li><b>Progress %</b> is required.</li>";
		} else {
		   $("#progress").removeClass("required");
		} 
		if(isNaN(progress) ) {
		   //jsDisplayResult("error", "error", "Please enter a valid progress");
		   //$("#progress").focus();
		  //return false;
		   valid8 = false;
		   $("#progress").addClass("required");
			err+="<li>Only numbers are permitted for the <b>Progress %</b>.</li>";
		} else {
		   $("#progress").removeClass("required");
		} 
		if(progress < 0 || progress > 100) {
		  //$("#progress").val( $("#_progress").val() )
		  //$("#progress").focus();
		  //jsDisplayResult("error", "error", "Please enter a valid progress between 0 and 100");
		  //return false;	
		   valid8 = false;
		   $("#progress").addClass("required");
		  err+="<li><b>Progress %</b> must fall between 0 - 100.</li>";
		} else {
		   $("#progress").removeClass("required");
		} 
		if(date_completed == "") {
		   //jsDisplayResult("error", "error", "Please enter the date on which you performed the activity in the Action On field.");
		   //$("#date_completed").focus();
		   //return false;
		   valid8 = false;
		   $("#date_completed").addClass("required");
		   err+="<li><b>Action On</b> is required.<br />Tip: This is the date on which you performed the activity.</li>";
		} else {
		   $("#date_completed").removeClass("required");
		} 
		if(!valid8) {
			//alert("The following error(s) have occurred:"+err+"\n\nPlease correct the error(s) and try again.");
			$("<div />",{id:"dlg_msg",title:"Error"})
				.html("<h1 class=red>Error!</h3><p>The following error(s) have occurred:</p><ul>"+err+"</ul><p>Please correct the error(s) and try again.</p>")
				.dialog({
					modal: true,
					buttons:[{
						text: "Ok",
						click: function() { $(this).dialog("close"); }
					}]
				});
			return false;
		} else {
			data.response       = response;
			data.status         = status;
			data.progress       = progress;
			data.reminder       = reminder;
			data.date_completed = date_completed;
			data.req_approval   = 0;
			if(data.status==3 && $("#request_approval").is(":checked")) { data.req_approval = 1; }
			data.id	   	        = $("#actionId").val();
//for(i in data) { alert(i+"=>"+data[i]); }
			//jsDisplayResult("info", "info", "Updating Action . . . .<img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientAction.updateAction",{ data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
				     //$("#response").val();
					//jsDisplayResult("ok", "ok", response.text);
					$("#result").val("ok");
					$("#response").val(response.text);
					var f = 0;
					$("input:file").each(function() {
						f+=$(this).val().length;
					});
					var page_src = $("#page_src").val();
					if(f>0) {
						$form = $("form[name=frm_update]");
						$form.prop("target","file_upload_target");
						$("<div />",{id:"dlg_msg",title:"Processing..."})
							.html("<p>Your attachments are being processed.<br />Please be patient...</p><p class=center><img src='/pics/ajax_loader_v2.gif' /></p>")
							.dialog({
								modal: true,
								closeOnEscape: false,
								width: 250,
								height: 300
							});
						$(".ui-dialog-titlebar").hide();
							var display = response.display;
							//alert(display);
							
						var post_action = "window.parent.$('#dlg_msg').dialog('close');";
						post_action+="window.parent.$('<div />',{id:'dlg_ok',title:'Success'}).html('"+response.display+"').dialog({modal: true,closeOnEscape: true,width: 350,height: 250,buttons:[{text: 'Ok',click: function() { ";
						if(page_src=="FRONTPAGE") {
							post_action+=" window.parent.parent.header.location.href = '/title_login.php?m=action_dashboard'; ";
						} else {
							post_action+=" window.parent.document.location.href = 'update_actions.php?page_src="+page_src+"'; ";
						}
						post_action+=" } }], close: function() { window.parent.document.location.href = 'update_actions.php?page_src="+page_src+"'; }	});";
						$("form[name=frm_update] #action").val("UPDATE_ATTACH");
						$("form[name=frm_update] #after_action").val(post_action);
						//alert($form.attr("action"));
						$form.submit();
					} else {
						//$("#dlg_msg").dialog("close");
						//$("td."+$("#div_update #obj_id").val()+"updateclass").html(okIcon);
						//alert(page_src);
						$("<div />",{id:"dlg_ok",title:"Success"})
							.html(response.display)
							.dialog({
								modal: true,
								closeOnEscape: true,
								width: 350,
								height: 200,
								buttons:[{
									text: "Ok",
									click: function() { 
										if(page_src=="FRONTPAGE") {
											parent.header.location.href = '/title_login.php?m=action_dashboard';
										} else {
											document.location.href = "update_actions.php?page_src="+page_src; 
										}
									}
								}]
							});
					}
				}    
			},"json")
		}
		
	} , 
	
	emptyField				: function( fields )
	{
		$.each( fields, function( field, value){
		   if(field !== "actiontype")
		   {
		     $("input#"+field).val("");		   
		   }
		});		
		$("#action_owner option[value='']").attr("selected", "selected");
		$("#deliverable").val("");
		$("#action_name").val("");	
	} ,
	
	isValid				: function( fields )
	{
		if(fields.action == ""){
			return "Action name is required";	
		} else if(fields.owner == ""){
			return "Please select the action owner";	
		} else if(fields.action_deliverable == ""){
			return "Please select the deliverable";	
		} else if(fields.deadline == ""){
			return "Please enter the deadline";	
		}
		return true;
	} ,
	
	showDeliverable         : function()
	{
		var size = getWindowSize();
		var h = size['height']-50;
		var w = (size['width']/2)>700 ? size['width']/2 : size['width']*0.9;
       $("<div />",{id:"action_deliverable"})
       .dialog({
            modal      : true,
            autoOpen   : true,
            title      : "Action Deliverable Details",
			position: "left top",
            width      : w,
			height		: h,
            buttons    : {
                        "Ok"        : function()
                        {
                            $("#action_deliverable").remove();
                        }
            } ,
            open       : function(ui, event)
            {
	           $.get("../class/request.php?action=Deliverable.getActionDeliverable",{
	               action_id  : $("#action_id").val()
	           } , function(actionDeliverable){	    	  
	               $("#action_deliverable").append( actionDeliverable );
	           },"html")      
            },
			close		: function() { 
				                $("#action_deliverable").remove();
			}
       })
	} ,
	
	showLegislation         : function()
	{
		//var size = getWindowSize();
		//var h = size['height']-50;
		//var w = (size['width']/2)>700 ? size['width']/2 : size['width']*0.9;
       $("<div />",{id:"action_legislation"})
       .dialog({
            modal      : true,
            autoOpen   : true,
            title      : "Legislation Details",
            position   : "left top",
            width      : "auto",
            buttons    : {
                        "Ok"        : function()
                        {
                            $("#action_legislation").remove();
                        }
            } ,
            open       : function(ui, event)
            {
	           $.get("../class/request.php?action=Legislation.getActionLegislation",{
	               action_id  : $("#action_id").val()
	           } , function(actionDeliverable){	    	  
	               $("#action_legislation").append( actionDeliverable );
	           },"html")                       
	           $(this).css("height", "auto");
            } , 
            close       : function()
            {
               $("#action_legislation").remove();
            }
       })
	}
	
};

function uploadActionAttachment(target)
{
   var action_id = $("#action_id").val();
   $("#uploaded_image").empty();
   $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
   $.ajaxFileUpload({
        url           : "../class/request.php?action=ClientAction.uploadAttachment&action_id="+action_id,              
        dataType      : "json",
        secureuri     : true,
        fileElementId : "action_attachment_"+action_id,
        success       : function(response, status)
        {
             $("#file_upload").html("");
             if(response.error)
             {
               $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
             } else {
               $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
               ).append($("<div />",{id:"files_uploaded"}))
               if(!$.isEmptyObject(response.files))
               {     
                  var list = [];            
                  $.each(response.files, function(ref, file){
                  	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                  	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                  	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                  	list.push("</p>");
                    //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                  });
                  $("#files_uploaded").html(list.join(' '));
                  
                  $(".delete").click(function(e){
                    var id = this.id
                    var ext = $(this).attr('title');
                    $.post('../class/request.php?action=ClientAction.removeAttachment', 
                      {
                       attachment : id,
                       ext        : ext,
                       action_id  : action_id
                      }, function(response){     
                      if(response.error)
                      {
                        $("#result_message").html(response.text)
                      } else {
                         $("#result_message").addClass('ui-state-ok').html(response.text)
                         $("#li_"+id).fadeOut();
                      }
                    },'json');                   
                    e.preventDefault();
                  });
                  $("#action_attachment").val("");
               } 
             }            
        } ,
        error         : function(data, status, e)
        {
          $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");             
        }
    });    
}



function doAjax(url,dta) {
	var result;
	//alert("ajax ::=> "+dta);
	$.ajax({                                      
		url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
		success: function(d) { 
			//alert("d "+d.text); 
			//console.log(d);
			result=d; 
			//showResponse(d.text);
			//return d;
		},
		error: function(d) { console.log(d); result=d; }
	});
	//alert("result "+result.text);
	return result;
}

function doDeleteAttachment(index,url,dta) {
	var r = doAjax(url,dta);
	var i = index.split("_");
		a = i[0];
		$(function() {
			$("#li_"+index).hide();
		});
	showResponse(r.text);
}

function showResponse(t) {
						$("<div />",{id:"dlg_ok",title:"Success"})
							.html("<p>"+t+"</p>")
							.dialog({
								modal: true,
								closeOnEscape: true,
								width: 250,
								height: 250,
								buttons:[{
									text: "Ok",
									click: function() { $(this).dialog("close"); }
								}]
							});
}