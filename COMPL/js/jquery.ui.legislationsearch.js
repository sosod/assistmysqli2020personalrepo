$.widget("ui.legislationsearch", {
	
	options 		: {
		start	 	: 0, 
		limit 	: 10,
		current  : 1, 
		searchtext : "",
		searchOptions : {}		 
	} , 
	
	_init			: function()
	{
		this._get();		
	} , 
	
	__create		: function()
	{
	
	
	} , 
	
	_get			: function()
	{
		var self = this;
		$.getJSON("../class/request.php?action=SearchLegislation.getLegislations",{
			start		: self.options.start,
			limit 	: self.options.limit,
			options 	: {searchtext : self.options.searchtext 
			
			}
		}, function( searchResults ){
			console.log( searchResults );	
		});
	
	} , 
	
	display		: function()
	{
	
	
	} , 
	
	_displayHeader		: function()
	{
	
	
	} 

});
