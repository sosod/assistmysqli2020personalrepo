$(function(){
     
    Dashboard.getFinancialYear(); 
    $("#financialyear").change(function(){
        Dashboard.getLegislationReport($(this).val());       
    }); 
});


Dashboard      = {

     getFinancialYear      : function()
     {
       $("#financialyear").append($("<option />",{text:"--please select--", value:""}));   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financialyear").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}));
            });
         }      
       });
     } , 

     getLegislationReport          : function(financialyear)
     {
         var self = this; 
	    $("#report").html("<div style='position:'absolute, 'z-index':'9999', top:'250px', left:'350px', border:'0px solid #FFFFF', padding:'px''>Loading . . . <img src='../images/loaderA32.gif' /></div>")
         $.get("dashboard_report.php",{financialyear:financialyear}, function(reportData){
            $("#report").html("");
            $("#report").html( reportData );       
         },"html");            
     } ,
     
     _displayLegislationReport     : function(legislation)
     {
          var chart,legend;

          chart = new AmCharts.AmPieChart();
          chart.dataProvider = legislation;
          chart.titleField = "name";
          chart.valueField = "total";
          chart.depth3D = 10;
          chart.angle = 10;
          chart.labelRadius = 20;
          chart.labelText = "[[title]]: [[value]]";
          chart.radius   = "90";
          legend = new AmCharts.AmLegend();
          legend.align = "center";
          legend.markerType = "circle";
          chart.colors = [ "#009900", "#CC0001"];
          chart.addLegend(legend);
          chart.write("legislationChart");            
     } ,
     
     _displayActionReport     : function(action)
     {
          var chart,legend;

          chart = new AmCharts.AmPieChart();
          chart.dataProvider = action;
          chart.titleField = "name";
          chart.valueField = "total";
          chart.depth3D = 10;
          chart.angle = 10;
          chart.labelRadius = 20;
          chart.labelText = "[[title]]: [[value]]";
          chart.radius   = "90";
          legend = new AmCharts.AmLegend();
          legend.align = "center";
          legend.markerType = "circle";
          chart.colors = ["#CC0001", "#FE9900", "#009900"];
          chart.addLegend(legend);
          chart.write("actionChart");            
     } ,
     
     _displayDeliverableReport     : function(deliverable)
     {
          var chart,legend;

          chart = new AmCharts.AmPieChart();
          chart.dataProvider = deliverable;
          chart.titleField = "name";
          chart.valueField = "total";
          chart.depth3D = 10;
          chart.angle = 10;
          chart.labelRadius = 20;
          chart.labelText = "[[title]]: [[value]]";
          chart.radius   = "90";
        
        
          legend = new AmCharts.AmLegend();
          legend.align = "left";
          legend.markerType = "circle";
          legend.fontSize = 6;
          legend.verticalGap = 5;
          chart.colors = ["#000077", "#009900", "#FE9900", "#CC0001", "#999999"];
          chart.addLegend(legend);
          chart.write("deliverableChart");            
     }

}
