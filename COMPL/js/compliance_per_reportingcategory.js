$(function(){
     
     DeliverableComplianceStatus.getFinancialYear();        
     $("#financialyear").change(function(){
        DeliverableComplianceStatus.getReport($(this).val());
     });
});


var DeliverableComplianceStatus    = {

     getFinancialYear      : function()
     {
       $("#financialyear").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financialyear").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 
          
     getReport           : function(financialyear)
     {        
         document.location.href = "compliance_per_reporting_category.php?financialyear="+financialyear;
	    /*$.get("compliance_per_reporting_category.php",{financialyear:financialyear},function(responseData) {
			$("#report").html(responseData)
		},"html");*/
     }, 
     


}
