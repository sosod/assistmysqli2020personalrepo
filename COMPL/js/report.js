$(function(){
	
	Report.getLegislativeTypes();
	Report.getLegislationCategories();
	Report.getOrganisationSize();
	Report.getOrganisationType();
	Report.getOrganisationCapacity();
	Report.getLegislationStatus();
	Report.getFinancialYear();
	
	$("#sortable").sortable().disableSelection(); 
	if($("#quickid").val() === undefined)
	{
	     $(".legislation").attr("checked","checked"); 
	} else {
	     $.getJSON("../class/request.php?action=Report.getAQuickReport", {quickid : $("#quickid").val()}, function(response){
		     $("#report_description").text( response.description )
		     $("#report_name").val( response.name )
		     $("#report_title").val( response.data.report_title )
		     //check the legislatin fields that have been selected
		     $(".legislation").each(function(){
			   var id = $(this).attr("id")
			   $.each(response.data.legislations, function(key  ,val){
				if("legislation_"+key === id)
				{ 
				   $("#"+id).attr("checked", "checked");					
				}
			   });
		     });
		
		     $.each(response.data.value, function( index, val){
			    if(index == "legislation_date_date")
			    {
				  $("#from_legislation_date").val( val.from )
				  $("#to_legislation_date").val( val.to )
			    } else {				
				  $("#"+index).val(val);
				  $("#"+index).text(val);
				  $("#"+index+" option[value='"+val+"']").attr("selected", "selected")								
			    }
		     });
		     $.each(response.data['match'], function(index , key){
			     $("#match_"+index+" option[value='"+key+"']").attr("selected", "selected")
		     });		
		
		     $("#group option[value='"+response.data.group_by+"']").attr("selected", "selected")
		
		     $.each(response['sort'], function(index , val){
			     $("#sortable").append($("<li />").addClass("ui-state-default")
							     .append($("<span />",{id:"__"+index}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort"))
							     .append($("<input />",{type:"hidden", name:"sort[]", value:"__"+index}))
							     .append( val )
			     );			
		     });
		
	     }); 
	}

	
	$("#check_legislation").click(function(){
		$(".legislation").each(function(){
			if( !$(this).is(":checked"))
			{
				$(this).attr("checked", "checked");
			} 			
		})
		return false;
	});
	
	$("#invert_legislation").click(function(){
		$(".legislation").each(function(){
			if( $(this).is(":checked") )
			{
				$(this).attr("checked", "")
			} else {
				$(this).attr("checked", "checked")
			}
		})
		return false;
	})

});

Report		 = {
     
     getFinancialYear      : function()
     {
      // $("#financial_year").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financial_year").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 
     		
		getLegislativeTypes		: function()
		{
			$.getJSON("../class/request.php?action=LegislationType.getAll", function( legTypes ){
				$.each( legTypes, function( index, legtype ){
					$("#type").append($("<option />",{text:legtype.name, value:legtype.id}))
				});		
			});
	
		},
		
		getLegislationCategories : function()
		{
		   $.post("../class/request.php?action=LegislationCategory.getAll", function( legcategories ){
			 $.each( legcategories, function( index, legcat){
			  if(legcat.id == 3)
			  {
			     $("#category").append($("<option />",{text:legcat.name, value:"all", selected:"selected"}))	   
			  } else {
                   $("#category").append($("<option />",{text:legcat.name, value:legcat.id}))			        
			  }
			 })												  											  
		   },"json");
		}  ,
				
		getOrganisationSize : function()
		{
		  $.post("../class/request.php?action=OrganisationSize.getAll",function( sizes ){
		     $.each( sizes, function( index, orgsize){
			  if(orgsize.id == 3)
			  {
			     $("#organisation_size").append($("<option />",{text:orgsize.name, value:"all", selected:"selected"}))	   
			  } else {
                 $("#organisation_size").append($("<option />",{text:orgsize.name, value:orgsize.id}))
			  }
			})												  											  
		  },"json");
		}  ,	
		
		getOrganisationType : function()
		{
			$.post("../class/request.php?action=LegislationOrganisationType.getAll", function(orgtypes){			
				$.each(orgtypes, function(index, orgtype){ 
			       if((orgtype.id == 3 || orgtype.name == "All") || (orgtype.id == 12 || orgtype.name == "ALL"))
			       {
                     $("#organisation_type").append($("<option />",{text:orgtype.name, value:"all", selected:"selected"}))
			       } else {
                     $("#organisation_type").append($("<option />",{text:orgtype.name, value:orgtype.id}))
			       }				     
				})												  											  
			},"json");
		}  ,
		
		getOrganisationCapacity			: function()
		{
			$.post("../class/request.php?action=OrganisationCapacity.getAll", function( capacities ){
				$.each(capacities, function(index, orgcapacity){
			       if((orgcapacity.id == 3 || orgcapacity.name == "All"))
			       {
                     $("#organisation_capacity").append($("<option />",{text:orgcapacity.name, value:"all", selected:"selected"}))
			       } else {
                     $("#organisation_capacity").append($("<option />",{text:orgcapacity.name, value:orgcapacity.id}))  
			       }				  
				  
				})											  											  
			},"json");
		},
		
		getLegislationStatus				: function()
		{
			$.getJSON("../class/request.php?action=LegislationStatus.getStatuses", function( responseData ){
				$.each(responseData, function( index , status){
					$("#status").append($("<option />",{value:status.id, text:status.name}))					
				});
			});

		}		
		
}
