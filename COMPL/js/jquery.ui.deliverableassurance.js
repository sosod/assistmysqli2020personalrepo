$.widget("ui.deliverableassurance", {
	
	options			: {
		start		: 0,
		limit		: 10,		
		current		: 1,
		id			: 0,
		total		: 0,
		toDay		: ""
	} ,
	
	_init			: function()
	{
		this._getDeliverableAssurance();
	} , 
	
	_create			: function()
	{
		var month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
			   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
			   11 : "Nov", 12 : "Dec"
			};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
			min = "0"+min;				
		} else{
			min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min;	 
			
	} , 
	
	_getDeliverableAssurance	: function()
	{
		var self = this;
		$.getJSON("../class/request.php?action=AssuranceDeliverable.getDeliverableAssurance", {
			start		  : self.options.start,
			limit		  : self.options.limit,
			deliverableId : self.options.id
		}, function( responseData ){
			
			$(self.element).html("");
			$(self.element).parent().children("h2").remove();
			$(self.element).parent().prepend($("<h2 />",{html:"Deliverable Assurance"}));
			self._displayPaging(responseData.total);
			self._displayHeaders();
			if( $.isEmptyObject( responseData.data) )
			{
								
			} else {
				self._display( responseData.data )				
			}
			self._addNew();
		})				
		
	} , 
	
	_display			: function( deliverables )
	{
		var self = this;
		$.each( deliverables, function( index, deliverable){
			var tr  = $("<tr />",{id:"tr_assurance_"+deliverable.id})
			$.each( deliverable, function( key, val){ 
				if( key == "insertdate")
				{
					tr.append($("<td />",{html:self.options.toDay }))
				} else {
					tr.append($("<td />",{html:(key == "sign_off" ? (val == 1 ? "Yes" : "No")  : val) }))
				}
			})
			tr.append($("<td />")
			    .append($("<input />",{type:"button", name:"edit_"+index, id:"edit_"+index, value:"Edit"}))
			    .append("&nbsp;&nbsp;")
			    .append($("<input />",{type:"button", name:"del_"+index, id:"del_"+index, value:"Del"}))
			)
			
			$("#del_"+index).live("click", function(){
				if(confirm("Are you sure you want to delete this deliverable assurance?"))
				{
					jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
					/*$.post("../class/request.php?action=AssuranceDeliverable.updateDeliverableAssurance", { data : { id : deliverable.id, status : 2, deliverable_id : self.options.id} }, function( response ){*/
					var result = AssistHelper.doAjax("../common/common_controller.php?action=updateDeliverableAssurance",{ id : deliverable.id, status : 2, deliverable_id : self.options.id});
						if( result.error ){
							jsDisplayResult("info", "info", result.text);
						} else {
							$("#tr_assurance_"+deliverable.id).fadeOut();
							jsDisplayResult("ok", "ok", result.text);
						}						
					//},"json")					
					//alert(result.text);
				}
				return false;
			});
						
			
		$("#edit_"+index).live("click", function(){
			
			if( $("#edit_deliverable_dialog_"+index).length > 0)
			{
				$("#edit_deliverable_dialog_"+index).remove();
			}
			
			$("<div />",{id:"edit_deliverable_dialog_"+index})
			 .append($("<table />")
			   .append($("<tr />")
				 .append($("<th />",{html:"Response:"}))
				 .append($("<td />")
					 .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7", text:deliverable.response}))
				 ) 
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Sign Off"}))
				 .append($("<td />")
				    .append($("<select />",{name:"signoff", id:"signoff"})
				       .append($("<option />",{value:"0", text:"No"}))
				       .append($("<option />",{value:"1", text:"Yes"}))
				    )		 
				 )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Date Tested"}))	
				  .append($("<td />")
					 .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:deliverable.date_tested, readonly:"readonly"}).addClass("datepicker"))	  
				  )
			   )
			 )
			 .dialog({
				 	  autoOpen		: true, 
				 	  modal			: true, 
				 	  title			: "Edit Deliverable Assurance",
				 	  position		: "top",
					  width			: "500px",
				 	  buttons		: {
				 						"Save Changes"	: function()
				 						{	
				 							
				 							if( $("#response").val() == "")
				 							{
					 							jsDisplayResult("error", "error", "Plese enter the response");
					 							return false;
					 						} else {
					 							jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
					 							$.post("../class/request.php?action=AssuranceDeliverable.updateDeliverableAssurance",{
						 							data : { response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val(),id : deliverable.id, deliverable_id : self.options.id } },
					 							function( response ){
						 							if(response.error){
						 								jsDisplayResult("error", "error", response.text);
						 							} else {
						 								 self._getDeliverableAssurance()
						 								jsDisplayResult("ok", "ok", response.text );
						 								
						 							}				 								
					 							},"json")
				 								$("#edit_deliverable_dialog_"+index).dialog("destroy")
												$("#edit_deliverable_dialog_"+index).remove()
				 							}
				 						} , 						
				 						"Cancel"	: function()
				 						{			 							
			 								$("#edit_deliverable_dialog_"+index).dialog("destroy")
											$("#edit_deliverable_dialog_"+index).remove()
				 						}
		 							}
			 })
				$(".datepicker").datepicker({
			        showOn: 'both',
			        buttonImage: '/library/jquery/css/calendar.gif',
			        buttonImageOnly: true,
			        dateFormat: 'dd-M-yy',
			        changeMonth:true,
			        changeYear:true		
			    });
				$("#signoff").val(deliverable.sign_off);
			return false;
		});			
			$(self.element).append( tr )
		});	

		
	} , 
	
	_displayHeaders		: function( )
	{
		var self = this;
		$(self.element).append($("<tr />")
		   .append($("<th />",{html:"Ref"}))
		   .append($("<th />",{html:"System date and time"}))
		   .append($("<th />",{html:"Date Tested"}))
		   .append($("<th />",{html:"Response"}))
		   .append($("<th />",{html:"Sign Off"}))
		   .append($("<th />",{html:"Assurance By"}))
		   .append($("<th />"))
		)
	} , 
	
	_addNew			: function()
	{
		var self = this;
			
		$(self.element).append($("<tr />")
			.append($("<td />",{colspan:"8"})
			   .append($("<input />",{type:"button", name:"add_new", id:"add_new", value:"Add New"}))		
			)
		)
		
		$("#add_new").live("click", function(){
			if($("#add_new_deliverable_dialog").length > 0)
			{
				$("#add_new_deliverable_dialog").remove();
			}
			
			$("<div />",{id:"add_new_deliverable_dialog"})
			 .append($("<table />")
			   .append($("<tr />")
				 .append($("<th />",{html:"Response:"}))
				 .append($("<td />")
					 .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7"}))
				 ) 
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Sign Off"}))
				 .append($("<td />")
				    .append($("<select />",{name:"signoff", id:"signoff"})
				       .append($("<option />",{value:"0", text:"No"}))
				       .append($("<option />",{value:"1", text:"Yes"}))
				    )		 
				 )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Date Tested"}))	
				  .append($("<td />")
					 .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:"", readonly:"readonly"}).addClass("datepicker"))
				  )
			   )
			 )
			 .dialog({
				 	  autoOpen: true, 
				 	  modal	: true, 
				 	  title	: "Add Deliverable Assurance",
				 	  position: "top",
					  width	: "500px",
				 	  buttons	: {
                                             "Save"	: function()
                                             {	
                                                  if( $("#response").val() == "")
                                                  {
	                                                  jsDisplayResult("error", "error", "Plese enter the response");
	                                                  return false;
                                                  } else {
	                                                  jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
	                                                  $.post("../class/request.php?action=AssuranceDeliverable.saveDeliverableAssurance",{
		                                                  data : { response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val(),deliverable_id : self.options.id } },
	                                                  function( response ){
		                                                  if(response.error){
			                                                  jsDisplayResult("error", "error", response.text);
		                                                  } else {
			                                                   self._getDeliverableAssurance()
			                                                  jsDisplayResult("ok", "ok", response.text );
			
		                                                  }				 								
	                                                  },"json")
	                                                  $("#add_new_deliverable_dialog").dialog("destroy")
	                                                  $("#add_new_deliverable_dialog").remove()
                                                  }
                                             } , 						
                                             "Cancel"	: function()
                                             {			 							
                                                  $("#add_new_deliverable_dialog").dialog("destroy");
                                                  $("#add_new_deliverable_dialog").remove();
                                             }
                             },
                         close          : function(event, ui)
                         {
                              $("#add_new_deliverable_dialog").dialog("destroy");
                              $("#add_new_deliverable_dialog").remove();                             
                         }, 
                         open           : function(event, ui)
                         {
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});                         
                         }
			 })
				$(".datepicker").datepicker({
			        showOn: 'both',
			        buttonImage: '/library/jquery/css/calendar.gif',
			        buttonImageOnly: true,
			        dateFormat: 'dd-M-yy',
			        changeMonth:true,
			        changeYear:true		
			    });
			return false;
		});		

	} ,
	
	
	_displayPaging 		: function( total ) {
		var self = this;

		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		//$("#"+self.options.tableId)
		$(self.element)
		  .append($("<tr />")
			.append($("<td />",{colspan:7})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))			   
		   )	
		  )

		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});			
	} ,		
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getDeliverableAssurance();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getDeliverableAssurance();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getDeliverableAssurance();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getDeliverableAssurance();				
	} 
		
});
