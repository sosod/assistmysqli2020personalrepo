$.widget("ui.legislation",{

	options		: {
	     tableRef 			     : "legislation_"+Math.floor(Math.random(57)*98),
	     url 				     : "../class/request.php?action=Legislation.getClientLegislations",
	     deliverableUrl 	     : "../class/request.php?action=Deliverable.getDeliverables",
	     start 				     : 0,
	     limit				     : 10,
	     total 				     : 0,
	     current				 : 1,
	     page				     : "",
	     section                 : "",
	     legoption   		     : "",
	     legStatus		 	     : [],
 	     usedIds 			     : [],
	     importDeliverable	     : false,
	     importLegislation	     : false,
	     createDeliverable	     : false,
	     addDeliverable          : false,
	     addAction			     : false,
	     editLegislation	     : false,
	     editDeliverable	     : false,
	     updateLegislation	     : false,
	     updateDeliverable	     : false,
	     assuranceLegislation    : false,
	     assuranceDeliverable    : false,
	     confirmLegislation	     : false,
	     setupActivate		     : false,
	     setupLegislation	     : false,
	     setupEdit		   	     : false,
	     viewDetails			 : false,
	     approvalUpdate		     : false,
	     reassignDeliverable	 : false,
	     reassignActions         : false,
	     adminPage               : false,
	     approval			     : false,
	     viewType                : "mine",
	     displayBtn              : {},
	     statusDescription       : {},
	     financialyear           : 0,
	     financialYearsList      : [],
	     legislationId           : 0,
	     showFinancialYear       : true,
	     copyFinancialYear       : false,
	     tofinancialyear         : 0,
	     autoLoad                : false,
	     user				     : "",
	     company				 : "",
	     legislationid           : ""
	} , 
	
	_init               : function()
	{
	    if(this.options.autoLoad)
	    {
	        this._get();
	    }
	    
	} ,
	
	_create             : function()
	{
	    var self = this;
	    var html = [];
		html.push("<table width='100%' class='noborder'>");
		 if(self.options.showFinancialYear && !self.options.copyFinancialYear)
		 {
		  html.push("<tr>");
		    html.push("<td class='noborder'>");
		      html.push("<table class='noborder'>");
	          if(self.options.showFinancialYear)
	          {
	             if(!self.options.copyFinancialYear)
	             {		      
		            html.push("<tr>");
		              html.push("<th style='text-align:left;'>Financial Year:</th>");
                      html.push("<td class='noborder'>");
                        html.push("<select id='_financial_year' name='_financial_year' style='width:350px;'>");
                          html.push("<option value=''>--please select--</option>");
                        html.push("</select>");
                      html.push("</td>");
		            html.push("</tr>");
		          }
		       }
		       if(self.options.confirmLegislation)
		       {
		        html.push("<tr>");
		          html.push("<th style='text-align:left;'>Select a legislation:</th>");
                  html.push("<td class='noborder'>");
                    html.push("<select id='_legislation_list' name='_legislation_list' style='width:350px;'>");
                      html.push("<option value=''>--please select--</option>");
                    html.push("</select>");
                  html.push("</td>");
		        html.push("</tr>");	
		        }
		    }
	        if(self.options.copyFinancialYear)
	        {
	            html.push("<tr>");
	              html.push("<th style='text-align:left;'>Select the financial year to copy from:</th>");
                  html.push("<td class='noborder'>");
                    html.push("<select id='_financial_year' name='_financial_year' style='width:350px;'>");
                      html.push("<option value=''>--please select--</option>");
                    html.push("</select>");
                  html.push("</td>");
	              html.push("<th style='text-align:left;'>Select the financial year to copy to :</th>");
                  html.push("<td class='noborder'>");
                    html.push("<select id='to_financial_year' name='to_financial_year' style='width:350px;'>");
                      html.push("<option value=''>--please select--</option>");
                    html.push("</select>");
                  html.push("</td>");
	            html.push("</tr>");			        		        	        
	        }		        
		      html.push("</table>");
		    html.push("</td'>");
		  html.push("</tr>");
		  html.push("<tr>");
		    html.push("<td class='noborder' id='td_"+self.options.tableRef+"'>");
		      html.push("<table id='"+self.options.tableRef+"' class='noborder' width='100%'>");
		        html.push("<tr>");
		           html.push("<td class='noborder'>");
		             html.push("<p class='ui-state-highlight' style='margin:0; padding:5px; width:500px;' id='message'>");
	                   html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
	                   html.push("<span>Please select the financial year</span>");
                     html.push("</p>");
                   html.push("</td>");
                html.push("</tr>");
		      html.push("</table>");
		    html.push("</td'>");
		  html.push("</tr>");
        html.push("</table>");	
	    $(this.element).append(html.join(' '));
	    self._loadFinancialYears();
	    
	    $("#_financial_year").live("change", function(){
	        if($(this).val() != "")
	        {
	           self.options.financialyear = $(this).val();
	           self._get();
	        } else {
	            self.options.financialyear  = 0;
	            self._get();
	        }
	    });
	    
	    $("#_legislation_list").live("change", function(){

	        if($(this).val() != "")
	        {
                self.options.legislationid = $(this).val();
                self._loadDeliverableActions();
	        } else {
	            self.options.legislationid  = 0;
	            self._get();
	        }
	    });	    

	} ,

	_loadFinancialYears     : function()
	{
	    $("#_financial_year").html("");
        $.getJSON("../class/request.php?action=FinancialYear.fetchAll",{status:1}, function(financialyears){
          var finHtml = [];
          finHtml.push("<option value=''>--please select--</option>");
          $.each(financialyears, function(index, year){
              finHtml.push("<option value='"+year.id+"'>"+year.start_date+" - "+year.end_date+"</option>");
          });  
          $("#_financial_year").html( finHtml.join(' ') );
        }); 
	} ,

	_loadDeliverableActions   : function()
	{
	    var self = this;
	    $("#td_"+self.options.tableRef).html("");
		//var selectedlegislation = $("#legislation_list :selected").val();
		$("#td_"+self.options.tableRef).deliverableactions({ legislationId : self.options.legislationid,
														  page			 	 : self.options.page,
														  section             : self.options.section,
														  confirmation        : true,
														  details 		 	 : false,
														  editAction          : true,
														  editDeliverable     : true,
														  triggerBy           : true,
														  url	   			 : self.options.deliverableUrl    
													     });									
		return false;	     
	} ,

	_get                : function(message)
    {
		var self    = this;		
		var message = message || "Loading legislations ";
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );
		//$("#"+self.options.tableRef).append($("<div />",{id:"loadingDiv", html:message+" ...<img src='../images/loaderA32.gif' />"})
		   //.css({position:"absolute", "z-index":"9999", top:"250px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
		//)
		$.getJSON(self.options.url ,{
		   data : {   
		        start           : self.options.start,
			    limit           : self.options.limit,
			    viewType        : self.options.viewType,
		        page            : self.options.page,
		        section         : self.options.section,
		        legoption       : self.options.legoption,
		        financial_year  : self.options.financialyear,
		        tofinancialyear : self.options.tofinancialyear,
		        legislation_id  : self.options.legislationid
		   }
		}, function(legislationData) {
		   $("#loadingDiv").remove();
		   $("#"+self.options.tableRef).html("");
		   self.options.usedIds           = legislationData.usedId;
		   self.options.total             = legislationData.total;
		   self.options.legStatus         = legislationData.status; 
		   self.options.statusDescription = legislationData.statusDescription; 
		   self.options.currentUser       = legislationData.currentUser;		
           self.options.user	          = legislationData.user;
           self.options.company           = legislationData.company;
           self._displayPaging(legislationData.columns);
           self._displayHeaders(legislationData.headers, true);             
		   if($.isEmptyObject(legislationData.legislations))
		   {
		        var columns = 1;
		        if(self.options.setupLegislation )
		        {
			        columns = 8;
		        } else {
		            columns = legislationData.columns + 1;
		        }
                if(self.options.copyFinancialYear)
                {
                    columns += 1;
                }		        
			    var _html = [];
			    _html.push("<tr>")
			      _html.push("<td colspan='"+(columns)+"'>");
			         _html.push("<p class='ui-state-highlight' style='padding:5px; border:0; text-align:center; width:500px;'>");
			           _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
			             _html.push("<span style='float:left;'>");
			               if(legislationData.hasOwnProperty('error_message'))
			               {
			                 _html.push(legislationData.error_message);
			               } else {
			                 _html.push("There are no legislations available for the selected criteria");
			               }
			           _html.push("</span>");
			         _html.push("</p>");
			      _html.push("</td>");
			    _html.push("</tr>");
				$("#"+self.options.tableRef).append( _html.join(' ') );		     
		   } else {	
             self._display(legislationData.legislations, legislationData.users, legislationData.admin, legislationData.authorizor, legislationData.isLegislationOwner);		    
		   }
		});
    } ,
    
    _display            : function(legislations, users, adminstrator, authorizor, isLegislationOwner)
    {
       var self         = this;

       var _legislation = [];
       $.each(legislations, function(legislationid, legislation){
          var html         = [];        
          html.push("<tr>");
          $.each(legislation, function(key, val){
            html.push("<td>"+val+"</td>");
          });
          var btnOptions = self._displayBtnCheck(legislationid, isLegislationOwner);
          if(self.options.confirmLegislation)
          {
             if(legislationid === self.options.legislationid)
             {
                _legislation.push("<option value='"+legislationid+"' select='select'>"+legislation.name+"</option>");
             } else {
                _legislation.push("<option value='"+legislationid+"'>"+legislation.name+"</option>");
             } 
          }
          if(self.options.setupActivate)
          {
            var usedIds = [];
            usedIds     = self.options.usedIds;            
			if(self._indexOf(self.options.usedIds, legislationid) > -1)
			{
			  if((self.options.legStatus[legislationid] & 1) == 1)
			  {
			    if(self.options.setupLegislation)
			    {
			      html.push("<td id='activated_authorizor_"+legislationid+"'>&nbsp;</td>");
			      html.push("<td id='activated_administator_"+legislationid+"'>&nbsp;</td>");
			    }
			  } else {
			      html.push("<td id='activated_authorizor_"+legislationid+"'>&nbsp;</td>");
			      html.push("<td id='activated_administator_"+legislationid+"'>&nbsp;</td>");		    
			  }
			} else {
			    if(self.options.setupLegislation)
			    {
			      html.push("<td>");
			        html.push("<select id='legislationauthorisor_"+legislationid+"' name='legislationauthorisor_"+legislationid+"' multiple='multiple'>");
			        html.push("</select>");
			      html.push("</td>");
			      html.push("<td>");
			        html.push("<select id='legislationadminstrator_"+legislationid+"' name='legislationadminstrator_"+legislationid+"' multiple='multiple'>");
			        html.push("</select>");
			      html.push("</td>");			      
			    }
			}	              
          } else {
		    if(self.options.setupLegislation)
		    {
		      html.push("<td>");
		        html.push("<select id='legislationauthorisor_"+legislationid+"' name='legislationauthorisor_"+legislationid+"' multiple='multiple'>");
		        html.push("</select>");
		      html.push("</td>");
		      html.push("<td>");
		        html.push("<select id='legislationadminstrator_"+legislationid+"' name='legislationadminstrator_"+legislationid+"' multiple='multiple'>");
		        html.push("</select>");
		      html.push("</td>");			      
		    }
          }
	      if(self.options.setupLegislation)
	      {
		      html.push("<td>");
		        html.push("<select id='available_"+legislationid+"' name='available_"+legislationid+"'>");
				  html.push("<option value='0'>No</option>");
				  html.push("<option value='1'>Yes</option>");		            
		        html.push("</select>");
		      html.push("</td>");
			 $("#available_"+legislationid).live("change", function(e){
				if( $(this).val() == 1){
					$("select#import_del_"+legislationid).val(1);
					$("select#edit_del_"+legislationid).val(0);
					$("select#create_del_"+legislationid).val(1);
					$("select#import_actions_"+legislationid).val(0);
					$("select#edit_actions_"+legislationid).val(0);
					$("select#create_actions_"+legislationid).val(1);
				} else {
					$("select#import_del_"+legislationid).val(1);
					$("select#edit_del_"+legislationid).val(1);
					$("select#create_del_"+legislationid).val(1);
					$("select#import_actions_"+legislationid).val(1);
					$("select#edit_actions_"+legislationid).val(1);
					$("select#create_actions_"+legislationid).val(1);
				}
				e.preventDefault();
		 	 });			      
	      }
	      if(self.options.setupActivate)
	      {
             html.push("<td>");
               html.push("<table class='noborder'>");
                 html.push("<tr>");
	                  html.push("<td class='noborder'>");
	                    html.push("<select id='import_del_"+legislationid+"' name='import_del_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");		            
	                    html.push("</select>");
	                  html.push("</td class='noborder'>");
		              html.push("<td class='noborder'>");
		                html.push("<select id='edit_del_"+legislationid+"' name='edit_del_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");		            
		                html.push("</select>");
		              html.push("</td>");
		              html.push("<td class='noborder'>");
		                html.push("<select id='create_del_"+legislationid+"' name='create_del_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");	            
		                html.push("</select>");
		              html.push("</td>");
                 html.push("</tr>");
               html.push("</table>");
             html.push("</td>");
             html.push("<td>");
               html.push("<table class='noborder'>");
                 html.push("<tr>");
	                  html.push("<td class='noborder'>");
	                    html.push("<select id='import_actions_"+legislationid+"' name='import_actions_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");	            
	                    html.push("</select>");
	                  html.push("</td>");
		              html.push("<td class='noborder'>");
		                html.push("<select id='edit_actions_"+legislationid+"' name='edit_actions_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");            
		                html.push("</select>");
		              html.push("</td>");
		              html.push("<td class='noborder'>");
		                html.push("<select id='create_actions_"+legislationid+"' name='create_actions_"+legislationid+"'>");
			              html.push("<option value='1'>Yes</option>");
			              html.push("<option value='0'>No</option>");            
		                html.push("</select>");
		              html.push("</td>");;
                 html.push("</tr>");
               html.push("</table>");
             html.push("</td>");
	      }			
		  if(self.options.copyFinancialYear)
		  {
            var statusDescription = "";
            if(self.options.statusDescription != undefined)
            {
               statusDescription = self.options.statusDescription[legislationid];
            }
		    html.push("<td><span>"+statusDescription+"</span></td>");
		    html.push("<td>");
		      html.push("<input type='button' name='copy_"+legislationid+"' id='copy_"+legislationid+"' value='Copy' />");
		      $("#copy_"+legislationid).live("click", function(e){
			     if($("#to_financial_year").val() == "")
			     {
			         jsDisplayResult("error", "error",  "Please select the financial year to");
	                   return false;
			     } else {
                   if((self.options.legStatus[index] & 1024) == 0)
                   {			        
			          self._copyFinancialYear(legislationid)
			       } else {
			          self._copyLegislation(index);
			       }
			     }		      
			     $(this).attr("disabled", "disabled");
			     e.stopImmediatePropagation();
		      });
		    html.push("</td>");
		  }
		  if(self.options.viewDetails)
		  {
		    html.push("<td>");
		      html.push("<input type='button' name='view_details_"+legislationid+"' id='view_details_"+legislationid+"' value='View Details' />");
		      $("#view_details_"+legislationid).live("click", function(e){
                document.location.href = "view_details.php?id="+legislationid+"&viewtype="+self.options.viewType;
                e.stopImmediatePropagation();
		      });
		    html.push("</td>");
		  }  
		  if(self.options.addAction && btnOptions['displayAddActions'])
		  {
		    html.push("<td>");
		      html.push("<input type='button' name='add_actions_"+legislationid+"' id='add_actions_"+legislationid+"' value='Add Actions' />");
		      $("#add_actions_"+legislationid).live("click", function(e){
                document.location.href = "add_actions.php?id="+legislationid;
                e.stopImmediatePropagation();
		      });
		    html.push("</td>");
		  }	  	   		  
		  if(self.options.editLegislation)
		  { 
	         html.push("<td>");
		     if(btnOptions['displayEditBtn'])
		     {
		          html.push("<input type='button' name='edit_legislation_"+legislationid+"' id='edit_legislation_"+legislationid+"' value='Edit Legislation' />");
		          $("#edit_legislation_"+legislationid).live("click", function(e){
                    document.location.href = "edit_legislation.php?id="+legislationid+"&viewtype="+self.options.viewType;
                    e.stopImmediatePropagation();
		          });	        
		     }
		     html.push("<br />");
		     if(btnOptions['displayEditDelBtn'])
		     {
		          html.push("<input type='button' name='edit_deliverable_"+legislationid+"' id='edit_deliverable_"+legislationid+"' value='Edit Deliverable' />");
		          $("#edit_deliverable_"+legislationid).live("click", function(e){
                    document.location.href = "edit_deliverable.php?id="+legislationid+"&viewtype="+self.options.viewType;
                    e.stopImmediatePropagation();
		          });	        
		     }		
		     if(btnOptions['displayCreateDelBtn'])
		     {
		          html.push("<input type='button' name='create_deliverable_"+legislationid+"' id='create_deliverable_"+legislationid+"' value='Create Deliverable ' />");
		          $("#create_deliverable_"+legislationid).live("click", function(e){
                    document.location.href = "add_deliverable.php?id="+legislationid;
                    e.stopImmediatePropagation();
		          });	        
		     }				       
	         html.push("</td>");	
		  }
		  if(self.options.assuranceLegislations)
		  {
		    html.push("<td>");
		    if(btnOptions['displayAssuranceBtn'])
		    {
                html.push("<input type='button' name='assurance_legislation_"+legislationid+"' id='assurance_legislation_"+legislationid+"' value='Assurance Legislation' />");
                $("#assurance_legislation_"+legislationid).live("click", function(e){
                   document.location.href = "assurance_legislation.php?id="+legislationid;
                   e.stopImmediatePropagation();
                });
		    }
		    html.push("<br />");
		    if(btnOptions['displayAssuranceDelBtn'])
		    {
                html.push("<input type='button' name='assurance_deliverable_"+legislationid+"' id='assurance_deliverable_"+legislationid+"' value='Assurance Deliverable' />");
                $("#assurance_deliverable_"+legislationid).live("click", function(e){
                   document.location.href = "assurance_deliverable.php?id="+legislationid;
                   e.stopImmediatePropagation();
                });
		    }		    
		    html.push("</td>");
		  }
		  if(self.options.importLegislation)
		  {
		    html.push("<td>");
		    if(btnOptions['displayImportBtn'])
		    {
                html.push("<input type='button' name='_import_"+legislationid+"' id='_import_"+legislationid+"' value='Import' />");
                $("#_import_"+legislationid).live("click", function(e){
			       var financialYear = $("#_financial_year :selected").val();
			       if(financialYear == "")
			       {
				      jsDisplayResult("error", "error", "Please select the financial year");
				      return false;
			       } else {                
                      self._importLegislation(legislationid, financialYear);
                   }
                   e.stopImmediatePropagation();
                });
		    } else {
		        html.push("<div style='border:0px; background-color:white;' class='ui-state-ok'>");
		          html.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
		          html.push("<span style='float:right;'><b>Imported</b></span>");
		        html.push("</div>");
		    }		    
		    html.push("</td>");
		  }
		  if(self.options.createDeliverable)
		  {
		    html.push("<td>");
                html.push("<input type='button' name='add_deliverable_"+legislationid+"' id='add_deliverable_"+legislationid+"' value='Add Deliverable' />");
                $("#add_deliverable_"+legislationid).live("click", function(e){
                    document.location.href = "add_deliverable.php?id="+legislationid;
                   e.stopImmediatePropagation();
                });               
		    html.push("</td>");
		  }
		  if(self.options.setupLegislation)
		  {
		    html.push("<td>");
		    if(btnOptions['displayActivateBtn'])
		    {
                html.push("<input type='button' name='activate_"+legislationid+"' id='activate_"+legislationid+"' value='Activate' class='isubmit' />");
                $("#activate_"+legislationid).live("click", function(e){
                   if($("#legislationauthorisor_"+legislationid).val() === null || $("#legislationauthorisor_"+legislationid).val() === ""){
                     jsDisplayResult("error", "error", "Please select legislation authorizer . . ." );
                   } else if($("#legislationadminstrator_"+legislationid).val() === null || $("#legislationadminstrator_"+legislationid).val() == ""){
                     jsDisplayResult("error", "error", "Please select legislation administrator . . ." );
                   } else {
                     self._activateLegislation(legislation, legislationid);
                   }
                   e.stopImmediatePropagation();
                });                                                           
            }
		    if(btnOptions['displayDeactivateBtn'])
		    {
                html.push("<input type='button' name='deactivate_"+legislationid+"' id='deactivate_"+legislationid+"' value='Deactivate' class='idelete' />");
                $("#deactivate_"+legislationid).live("click", function(e){
                   self._deactivateLegislation(legislationid);
                   e.stopImmediatePropagation();
                });                                                       
            }  
		    if(self.options.setupEdit)
		    {
                html.push("<input type='button' name='save_changes_"+legislationid+"' id='save_changes_"+legislationid+"' value='Save Changes' />");
                $("#save_changes_"+legislationid).on("click", function(e){
                   self._saveChanges(legislationid);
                   e.stopImmediatePropagation();
                });                                                       
            }              
		    html.push("</td>");
		  }		  
          html.push("</tr>");
          $("#"+self.options.tableRef).append( html.join(' ') );
		  $.each(users, function(i, user) {
			  $("#legislationauthorisor_"+legislationid).append($("<option />",{text:user.user, value:i}))
			  $("#legislationadminstrator_"+legislationid).append($("<option />",{text:user.user, value:i}))
			  			
				if(authorizor[legislationid] != undefined)
				{
				  $.each(authorizor[legislationid], function(aIndex, aVal){
					  if(i === aVal)
					   {
					      $("#legislationauthorisor_"+legislationid+" option[value='"+aVal+"']").attr("selected", "selected");	
					      $("#activated_authorizor_"+legislationid).append(users[aVal].user+"<br />" )
					  }
				   })						
				}
				if(adminstrator[legislationid] != undefined)
				{
					$.each(adminstrator[legislationid], function(aIndex, aVal){
						 //$("#legislationadminstrator_"+index+" option[value='"+aVal+"']").attr("selected", "selected")
						if(i == aVal) 
						{					
						    $("#legislationadminstrator_"+legislationid+" option[value='"+aVal+"']").attr("selected", "selected");	
						    $("#activated_administator_"+legislationid).append( users[aVal].user+"<br />" )
						}					 
						 //$("#activated_administator_"+index).append( users[aVal].name)
					})						
				}					
	      });          
	      
			var selectYes = false;
			if(self.options.legStatus.hasOwnProperty(legislationid))
			{
			   selectYes = true;
			} 
			if((self.options.legStatus[legislationid] & 2) === 2)
			{
			   $("#available_"+legislationid).val(1);
			} else {
			   $("select#available_"+legislationid).val(0);
			}
						
			if(((self.options.legStatus[legislationid] & 8) == 8) && selectYes == true)  
			{
				$("#edit_del_"+legislationid).val(1);
			} else {
				$("select#edit_del_"+legislationid).val(0);
			}			
			if(((self.options.legStatus[legislationid] & 16) == 16) && selectYes == true)  
			{
				$("#create_del_"+legislationid).val(1);
			} else {
				$("#create_del_"+legislationid).val(0);
			}
			if(((self.options.legStatus[legislationid] & 32) == 32) && selectYes == true)  
			{
				$("select#import_actions_"+legislationid).val(1);
			} else {
				$("select#import_actions_"+legislationid).val(0);
			}
			
			if(((self.options.legStatus[legislationid] & 64) == 64) && selectYes == true)  
			{
				$("select#edit_actions_"+legislationid).val(1);
			} else {
				$("select#edit_actions_"+legislationid).val(0);
			}			
			
			if(((self.options.legStatus[legislationid] & 128) == 128) && selectYes == true)  
			{
				$("select#create_actions_"+legislationid).val(1);
			} else {
				$("select#create_actions_"+legislationid).val(0);
			}
			$("select#legislationadminstrator_"+legislationid).attr("size", function() {
		 		return $("#legislationadminstrator_"+legislationid+" option").length; 	
			});		
			  
			$("select#legislationauthorisor_"+legislationid).attr("size", function(){
		 	   return $("#legislationauthorisor_"+legislationid+" option").length; 	
			});		      
       });
       $("#_legislation_list").append( _legislation.join(' ') );
       
    } ,
    
    _displayHeaders     : function(headers)
    {
       var self = this;
       var html = [];
       html.push("<tr>");
       $.each(headers, function(key, header){
          html.push("<th>"+header+"</th>");
       });
       if(self.options.setupLegislation)
       {
           html.push("<th>Legislation Authorisor</th>");
           html.push("<th>Legislation Administrator</th>");
           if(self.options.setupActivate)
           {
             html.push("<th>Must this legislation made available?</th>");
             html.push("<th class='noborder'>");
               html.push("<table width='100%' class='noborder'>");
                 html.push("<tr>");
                   html.push("<th colspan='3' class='noborder'>Deliverable</th>");
                 html.push("</tr>");
                 html.push("<tr>");
                   html.push("<th class='noborder'>Import</th>");
                   html.push("<th class='noborder'>Edit</th>");
                   html.push("<th class='noborder'>Create</th>");
                 html.push("</tr>");
               html.push("</table>");
             html.push("</th>");
             html.push("<th class='noborder'>");
               html.push("<table  width='100%' class='noborder'>");
                 html.push("<tr>");
                   html.push("<th colspan='3' class='noborder'>Action</th>");
                 html.push("</tr>");
                 html.push("<tr>");
                   html.push("<th class='noborder'>Import</th>");
                   html.push("<th class='noborder'>Edit</th>");
                   html.push("<th class='noborder'>Create</th>");
                 html.push("</tr>");
               html.push("</table>");
             html.push("</th>");
           } else {
             html.push("<th>Must this legislation made active?</th>");
           }
           html.push("<th>&nbsp;&nbsp;Status&nbsp;&nbsp;</th>");
       }
       if(self.options.copyFinancialYear)
       {
          html.push("<th>Legislation Status</th>");
          html.push("<th>&nbsp;Copy&nbsp;</th>");
       }
       if(self.options.importLegislation)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.viewDetails)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.createDeliverable)
       {
          html.push("<th>&nbsp;</th>");
       }
       if(self.options.addAction)
       {
          html.push("<th>&nbsp;</th>");
       }   
       if(self.options.editLegislation)
       {
          html.push("<th>&nbsp;</th>");
       }   
       if(self.options.assuranceLegislation)
       {
          html.push("<th>&nbsp;</th>");
       }             
       html.push("</tr>");
       $("#"+self.options.tableRef).append( html.join(' ') ); 
    } ,
    
	_displayPaging	: function(columns)
	{
		var self  = this;
        var html  = [];
		var pages;
		if( self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}
		if(self.options.setupLegislation )
		{
			columns = 8;
		}	
        if(self.options.copyFinancialYear)
        {
            columns += 2;
        }
		html.push("<tr>");
		  if(self.options.showActions)
		  {
		       html.push("<td>&nbsp;</td>");
		  }				    
		  html.push("<td colspan='"+(columns + 1)+"'>");
		    html.push("<input type='button' value=' |< ' id='first' name='first' />");
		    html.push("<input type='button' value=' < ' id='previous' name='previous' />");
            if(pages != 0)
            {
               html.push("Page <select id='select_page' name='select_page'>");
               for(var p=1; p<=pages; p++)
               {
                  html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
               }
               html.push("</select>");		    
            } else {
               html.push("Page <select id='select_page' name='select_page' disabled='disabled'>");
               html.push("</select>");		    
            }
		    html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
		    //html.push(self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
		    html.push("<input type='button' value=' > ' id='next' name='next' />");
		    html.push("<input type='button' value=' >| ' id='last' name='last' />");
          html.push("&nbsp;");
          if(!self.options.setupLegislation)
          {
              html.push("<span style='float:right;'>");
                html.push("<b>Quick Search for Legislation:</b> L <input type='text' name='q_id' id='q_id' value='' />");
                 html.push("&nbsp;&nbsp;&nbsp;");
                html.push("<input type='button' name='search' id='search' value='Go' />");
              html.push("</span>");
          }
          html.push("</td>");
		html.push("</tr>");
		$("#"+self.options.tableRef).append(html.join(' '));
		
		
		$("#select_page").live("change", function(){
		   if($(this).val() !== "")
		   {
		      self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
		      self.options.current = parseFloat($(this).val());
		      self._get();
		   }
		});
        $("#search").live("click", function(e){
           var legislation_id = $("#q_id").val();
           if(legislation_id != "")
           {
              self.options.legislationid = $("#q_id").val();
              self._get();
           } else {
              $("#q_id").addClass('ui-state-error');
              $("#q_id").focus().select();
           } 
           e.preventDefault();
        });
		
         if(self.options.current < 2)
         {
             $("#first").attr('disabled', 'disabled');
             $("#previous").attr('disabled', 'disabled');		     
         }
         if((self.options.current == pages || pages == 0))
         {
             $("#next").attr('disabled', 'disabled');
             $("#last").attr('disabled', 'disabled');		     
         }				
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
	} ,		
	
	_getNext  			: function(self) 
	{
		self.options.current = (self.options.current + 1);
		self.options.start   = (self.options.current - 1) * self.options.limit;
		self._get();
	},	
	
	_getLast  			: function(self) 
	{
		self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );			
		this._get();
	},	
	_getPrevious    	: function(self) 
	{
		self.options.current  = parseFloat(self.options.current) - 1;
		self.options.start 	= (self.options.current-1) * self.options.limit;
		this._get();			
	},	
	_getFirst  			: function(self)
	{
		self.options.current  = 1;
		self.options.start 	= 0;
		this._get();				
	},

     _indexOf       : function(arrayData, obj)
     {
        for(var i = 0; i < arrayData.length; i++)
        {
           if(arrayData[i] == obj)
           {
             return i;     
           }
        }
        return -1;
     } ,  
     
     _activateLegislation       : function(legislation,legislationid)
     {
        var self = this;
	     if($("#disclaimer_"+legislationid).length >0)
	     {
	       $("#disclaimer_"+legislationid).remove();    
	     }
	     var html = [];
	     html.push("<div class='ui-state-highlight' style='padding:7px;'>");
	       html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
	       html.push("<span>I,"+self.options.user+", of "+self.options.company+" hereby : </span>");
	       html.push("<ul>");
	         html.push("<li> declare that I have the required authority to activate this legislation</li>");
	         html.push("<li>  confirm my understanding that this legislation, "+legislation.legislation_name+"  "+legislation.legislation_reference+", contains only the required deliverables and actions (where applicable) relating to the matters that are required to be addressed in this legislation;</li>");
	         html.push("<li>  acknowledge that this legislation, its deliverable(s) and action(s) are published by Ignite Reporting Systems (Pty) Ltd on behalf of, "+legislation.responsible_business_patner+" and that the information is not intended to constitute advice on the legislation covered as every situation depends on its own facts and circumstances for which professional advice should be sought from "+legislation.responsible_business_patner+"</li>");
	         html.push("<li> agree that the views expressed (published) are those of the business partner, "+legislation.responsible_business_patner+"; and</li>");
	         html.push("<li>  acknowledge that although reasonable care has been taken to ensure the accuracy of this work, Ignite Reporting Systems (Pty) Ltd has expressly disclaimed all and any liability to any person relating to anything done or omitted to be done or to the consequences thereof in reliance upon this work.</li>");
	       html.push("</ul>");
	     html.push("</div>");
	     
		$("<div />",{id:"disclaimer_"+legislationid}).append( html.join(' ') )
		 .dialog({
	 		autoOpen	: true,
	 		modal	    : true,
	 		title	    : "Activating Legislation L"+legislationid,
	 		width	    : "500px",
	 		position    : "top",
	 		buttons	    : {
 						"Confirm Activation"	: function()
 						{
							jsDisplayResult("info", "info", "Activating legislation ...<img src='../images/loaderA32.gif' />" );
							var data 		    = {};		 
							data.available      = $("#available_"+legislationid+" :selected").val()
							data.import_del     = $("#import_del_"+legislationid+" :selected").val()
							data.edit_del       = $("#edit_del_"+legislationid+" :selected").val()
							data.create_del     = $("#create_del_"+legislationid+" :selected").val()
							data.import_action  = $("#import_actions_"+legislationid+" :selected").val()
							data.edit_action    = $("#edit_actions_"+legislationid+" :selected").val()
							data.create_action  = $("#create_actions_"+legislationid+" :selected").val();
							data.legislation_id = legislationid;
							data.authorisor     = $("#legislationauthorisor_"+legislationid).val();
							data.admin  	    = $("#legislationadminstrator_"+legislationid).val();
							$.post("../class/request.php?action=ClientLegislation.activate", {data:data} , 
							function( response ){
								if( response.error ){
									 jsDisplayResult("error", "error",  response.text );
								} else {
									 self._get("updating legislations ...");
									 jsDisplayResult("ok", "ok", response.text );
									 $("#activate_"+legislationid).attr("disabled", "disabled");
									 $("#inactivate_"+legislationid).attr("disabled", "");
 									 $("#disclaimer_"+legislationid).dialog("destroy").remove();
								}
							},"json");
 						} , 
 						"Cancel Activation"		 : function()
 						{
							$("#disclaimer_"+legislationid).dialog("destroy").remove();
 						}
					 } ,
		 			open		: function(event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				$(this).css("height", "auto")
		 			} , 
		 			close      : function(event, ui)
		 			{
						$("#disclaimer_"+legislationid).dialog("destroy").remove();
		 			}			 
		 });
		return false; 
     } ,
     
     _importLegislation    : function(legislationid, financialYear)
     {
        var self = this;
	     jsDisplayResult("info", "info", "Importing a legislation ...<img src='../images/loaderA32.gif' />" );
	     $.post("../class/request.php?action=Legislation.importSave",
	      {legislation_id:legislationid, financial_year:financialYear} , function(response) {
		    if(response.error)
		    {
			    jsDisplayResult("error", "error",  response.text );
		    } else {
			    jsDisplayResult("ok", "ok", response.text );
			    $("#status_"+legislationid).html("<b>Active</b>");
				$("#import_"+legislationid).attr("disabled", "disabled");
				self._get("updating legislations ...");
		    }
	     },"json");	
     },
     
     _saveChanges          : function(legislationid)
     {
        var self = this;
		var data 		    = {};		 
		data.active         = $("#available_"+legislationid+" :selected").val()
		data.legislation_id = legislationid;
		data.authoriser     = $("#legislationauthorisor_"+legislationid).val();
		data.admin          = $("#legislationadminstrator_"+legislationid).val();
		
		jsDisplayResult("info", "info", "Activating legislation . . . .<img src='../images/loaderA32.gif' />" );
		$.post("../class/request.php?action=AdminLegislation.editSetupLegislation",  data , function( response ){
			if(response.error)
			{
			   jsDisplayResult("error", "error",  response.text );
			} else {							 
			   jsDisplayResult("ok", "ok", response.text );
			   $("#activate_"+legislationid).attr("disabled", "disabled");
			   $("#inactivate_"+legislationid).attr("disabled", "");
			   self._get("updating legislations ...");
			}
		},"json");
		return false;        
     }, 
     
     _deactivateLegislation     : function(legislationid)
     {		
        var self = this;			
		jsDisplayResult("info", "info", "Inactivating legislation . . . .<img src='../images/loaderA32.gif' />" );
		var data 		    = {};		 
		data.legislation_id = legislationid;
		$.post("../class/request.php?action=ClientLegislation.inactivate",  data , function( response ){
            if(response.error)
            {
              jsDisplayResult("error", "error",  response.text );
            } else {
              jsDisplayResult("ok", "ok", response.text );
              $("#activate_"+legislationid).attr("disabled", "");
              $("#inactivate_"+legislationid).attr("disabled", "disabled");
              self._get("updating legislations ...");
            }
		},"json");	
		return false;        
     },
     
	_copyLegislation              : function(legislation_id)
	{
	   var self = this;
	   jsDisplayResult("info", "info", "Copying . . . .<img src='../images/loaderA32.gif' />" );
         $.post("../class/request.php?action=Legislation.copyLegislation",
           {legislation:legislation_id, financial_year:$("#to_financial_year").val()}, function(response){
           if(response.error)
           {
              jsDisplayResult("error", "error", response.text);
           } else {
              jsDisplayResult("ok", "ok", response.text); 
           }
         }, "json");
	} ,
	
    _displayBtnCheck              : function(legislationId, isLegislationOwner)
	{
        var self  = this;
        var  btnDisplayOptions = {}	 
        
        //set which button should be displayed on different conditions
        /*
          displayed for legislation owner
          displayed for legislation that are manully created only
        */
        btnDisplayOptions['displayEditBtn']         = false; 
        /*
          displayed for legislation owner
          displayed for user who manually created the legislation ie legislation status = 2048
        */
        btnDisplayOptions['displayUpdateBtn']       = false; //displayed for legislation owner 
        /*
          has view permissions
        */
        btnDisplayOptions['displayViewBtn']         = false; //all
        /*
          allowed for legislation owner and deliverable responsible owner, 
          allowed for user who have permission to add an action , ie legislation_status = 128
          allowed for legislation manually created ie legislation status = 2048
        
          btnDisplayOptions['displayAddActionsBtn']   = false;        
        */
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayUpdateDelBtn']    = false; 
        /*
          displayed for legislation owner
          displayed if the user has permissions to edit then , ie legislation status = 8
        */
        btnDisplayOptions['displayEditDelBtn']      = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayReAssignDelBtn']  = false; //displayed for legislation owner
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayReAssignActionsBtn']  = false; 
        /*
           displayed for legislation owner 
           and current person's manager
        */
        btnDisplayOptions['displayAssuranceBtn']    = false;
        /*
          displayed for legislation owner 
          and current person's manager
        */
        btnDisplayOptions['displayAssuranceDelBtn'] = false;  
        /*
          displayed for legislation owner 
          and current person's manager 
        */
        btnDisplayOptions['displayApprovalBtn']     = false;
        /*
          displayed for legislation owner 
          display if the legislation has not yet been imported ie if legislation status is not 256 
        */
        btnDisplayOptions['displayImportBtn']       = false; 
        /*
          displayed for legislation owner 
        btnDisplayOptions['displayImportDelBtn']    = false; 
        */
        
        /*
        */
        btnDisplayOptions['displayActivateBtn']     = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displayDeactivateBtn']   = false; 
        /*
          displayed for legislation owner
        */
        btnDisplayOptions['displaySaveBtn']         = false; 
        /*
          displayed for legislation owner and current person's manager
        */
        btnDisplayOptions['displayAllowUpdate']     = false;  
        /*
         display for the legislation owner
         if its addDeliverable is set to true
        (self.options.addDeliverable ? (isLegislationOwner[index] ? "block" : "none") : "none")
        */
        btnDisplayOptions['displayCreateDelBtn']     = false;
        
        btnDisplayOptions['displayAddActions']       = false;
        btnDisplayOptions['editDelBtnValue'] = "Edit Deliverable";
        //if theere is a a legislation owner, then show the following button because the owner the respective actions
        if(isLegislationOwner[legislationId] !== undefined)
        {
           if((self.options.legStatus[legislationId] & 2048) === 2048)
           {
              btnDisplayOptions['displayEditBtn']   = true;
              btnDisplayOptions['displayCreateDelBtn']   = true;
           }
           btnDisplayOptions['displayUpdateBtn'] = true;
           btnDisplayOptions['displayUpdateDelBtn']   = true;
           btnDisplayOptions['displayEditDelBtn']     = true;
           btnDisplayOptions['displayReAssignDelBtn'] = true;
           btnDisplayOptions['displayAssuranceBtn']   = true;
           btnDisplayOptions['displayAssuranceDelBtn']= true;
           btnDisplayOptions['displayApprovalBtn']    = true;
           btnDisplayOptions['displayImportBtn']      = true;
           //btnDisplayOptions['displayDeactivateBtn']  = true;
           btnDisplayOptions['displaySaveBtn']        = true;
           btnDisplayOptions['displayAllowUpdate']    = true;
        }
        
        if(((self.options.legStatus[legislationId] & 128) == 128) || ((self.options.legStatus[legislationId] & 2048) == 2048))
        {
          btnDisplayOptions['displayAddActions']       = true;
        }
        
        //if user has permissions to edit the deliverable , show the edit deliverable button
        if((self.options.legStatus[legislationId] & 8) == 8)
        {
           btnDisplayOptions['displayEditDelBtn'] = true;
        }
        //if the legislation has been imported then , dont display the button
        if((self.options.legStatus[legislationId] & 256) == 256)
        {
          if(self.options.page == "manage")
          {
            btnDisplayOptions['editDelBtnValue'] = "Edit";
          }          
          btnDisplayOptions['displayImportBtn'] = false;
        } else {
          btnDisplayOptions['displayImportBtn'] = true;
        }
        
        //if the legislation is not in used array , then show the activate button
        if(self.options.section == "setup")
        {
             //if legislation is active , then show the de-activate button else show active button
             if((self.options.legStatus[legislationId] & 1) == 1)
             {
               btnDisplayOptions['displayDeactivateBtn']   = true; 
             } else {
               btnDisplayOptions['displayActivateBtn'] = true; 
             }
        }
        
        if(self.options.section === "admin" || self.options.adminPage)
        { 
           btnDisplayOptions['displayEditBtn']       = true;
           btnDisplayOptions['displayUpdateBtn']     = true;
           btnDisplayOptions['displayUpdateDelBtn']  = false;
           btnDisplayOptions['displayEditDelBtn']    = false;
           btnDisplayOptions['displayCreateDelBtn']  = false;                      
        }
        if(self.options.section === "support")
        {
           btnDisplayOptions['displayEditBtn']       = true;
           btnDisplayOptions['displayUpdateBtn']     = false;
           btnDisplayOptions['displayUpdateDelBtn']  = false;
           btnDisplayOptions['displayEditDelBtn']    = false;
           btnDisplayOptions['displayCreateDelBtn']  = false;         
        }
        
       return btnDisplayOptions;                   
	}	     
  

});
