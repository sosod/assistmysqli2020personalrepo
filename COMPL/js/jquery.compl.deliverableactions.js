//Javascript Document
$.widget("compl.deliverableactions",{
    
    options     : {
        tableId		    : "deliverableactions_"+(Math.floor(Math.random(55) * 98)), 
		start		       		: 0,
		current		    		: 1,
		total		       		: 0,
		limit 		    		: 10,
		legislationId	    	: 0,
		financialYear           : 0,
		page			    	: "",
		section            		:  "",		
		url				 		: "../class/request.php?action=Deliverable.getDeliverables",
		actionUrl           	: "../class/request.php?action=Action.getActions",
		autoLoad               	: true,
		confirmation	   		: false,
		editAction         		: false,
		editDeliverable 		: false, 
		eventActivation 		: false,
		showActions             : false,
		openActions             : false,
		unlockEvent             : false,
		viewDeliverable         : false,
		viewtype                : "viewmine",
		eventid			 		: 0, 
		triggerBy		 		: false, 
		occuranceid		 		: 0,
		deleverableActivated 	: [],
		deleverableDeactivated 	: [],
		autoLoad               	: false,
		checkedDeliverables	   	: [],
		uncheckedDeliverables  	: [],
		isDeliverableOwner      : {},
		filterByFinancialYear   : false
    } ,
    
    _init        : function()
    {
       if(this.options.autoLoad)
       {
          this._getDeliverables();
       }
    } ,
    
    _create     : function()
    {
       var self = this;
       var html = []; 
       html.push("<table width='100%' class='noborder'>")
         html.push("<tr>")
           html.push("<td class='noborder'>");
               html.push("<table id='deliverable-actions' class='form' class='noborder'>");
                html.push("<tr>");
                  html.push("<th>Select Financial Year: </th>");
                  html.push("<td class='noborder'>");
                    html.push("<select name='financial_year' id='financial_year' class='financial_year'><select>");
                  html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                  html.push("<th>Select Legislation: </th>");
                  html.push("<td class='noborder'>");
                    html.push("<select name='legislation' id='legislation'><select>");
                  html.push("</td>");
                html.push("</tr>"); 
                if(self.options.eventActivation)
                {
                    html.push("<tr>");
                      html.push("<th>Select Event: </th>");
                      html.push("<td class='noborder'>");
                        html.push("<select name='event' id='event'><select>");
                      html.push("</td>");
                    html.push("</tr>");                      
                }         
               html.push("</table>");
           html.push("</td>");
         html.push("</tr>");
         html.push("<tr>");
          html.push("<td colspan='2' class='noborder'>");
            html.push("<table id='"+self.options.tableId+"' width='100%'>");
                html.push("<p class='ui-state ui-state-info' style='padding:5px;' id='load-message'>");
                html.push("<span>Please select the financial year and legislation to list their deliverables<span>");
                html.push("</p>");
            html.push("</table>");
          html.push("</td>");
         html.push("</tr>");           
       html.push("</table>");
       $(self.element).append(html.join(''));
       
       if(self.options.filterByFinancialYear)
       {
          self._loadFinancialYears();
       }
       $("#financial_year").live("change", function(e){
           self.options.financialYear = $(this).val();
           $("#legislation").empty();
           self._loadLegislations($(this).val());
           e.preventDefault();           
   	       $("#load-message").hide();
           self._getDeliverables();
       });  
       
	   $("#legislation").live("change", function(){
          self.options.legislationId = $(this).val();
          self.options.start   = 0;
          self.options.limit   = 10;
          self.options.current = 1;
          self.options.total   = 0;
          $("#load-message").hide();
          self._getDeliverables();   
	   });    
	   
       $("#deliverableall").live('click', function(e){
           self.options.viewtype = "viewall";
           if(self.options.financialYear !== 0)
           {
             self._getDeliverables(); 
           }
           e.preventDefault();
       });
         
       $("#deliverablemine").live('click', function(e){
           self.options.viewtype = "viewmine";
           if(self.options.financialYear !== 0)
           {
             self._getDeliverables(); 
           }           
           e.preventDefault();
       });  	        
        
    } ,
    
    _loadFinancialYears : function()
    {
	    var self    = this;
        $.getJSON("../class/request.php?action=FinancialYear.fetchAll",function(financialYears) {
          var finHtml = [];
          finHtml.push("<option value=''>--please select--</option>");            
          if(!$.isEmptyObject(financialYears))
          {
             //self.options.financialYearsList  = financialYears;
             $.each(financialYears, function(index, financialYear) {
               if(self.options.financialyear == financialYear.id)
               {
                   finHtml.push("<option value='"+financialYear.id+"' selected='selected'>("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date+"</option>");
               } else {
                   finHtml.push("<option value='"+financialYear.id+"'>("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date+"</option>");
               }
             });
          }	
          $(".financial_year").append(finHtml.join(' '));
        }); 
    } ,
    
    _loadLegislations     : function(financialyear)
    {
	    var self    = this;
        $.getJSON("../class/request.php?action=Legislation.getLegislations", {
           data:{financial_year:financialyear, section:self.options.section, page:self.options.page}
        }, function(legislations) {
          var legHtml = [];
          legHtml.push("<option value=''>--please select--</option>");            
          if(!$.isEmptyObject(legislations))
          {
             //self.options.financialYearsList  = financialYears;
             $.each(legislations, function(index, legislation) {
                 legHtml.push("<option value='"+legislation.id+"'>[L"+legislation.id+"] "+legislation.name+"</option>");
             });
          }	
          $("#legislation").append(legHtml.join(' '));
        }); 
    },
    
    _getDeliverables  : function()
    {
        var self = this;
	    $("body").append($("<div />",{id:"loadingDiv", html:"Loading deliverables ...<img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
		  .show()
	    );
	    $.getJSON(self.options.url, {
		     options : {
		          start 		 : self.options.start,
		          limit 		 : self.options.limit,
                  legislation_id : self.options.legislationId,
                  page           : self.options.page,
                  section        : self.options.section,
                  viewtype       : self.options.viewtype,
                  user           : self.options.user,
                  financial_year : self.options.financialYear
               }
	      }, function(deliverableResponse) {
	        $("#"+self.options.tableId).html("");
	        if(deliverableResponse.hasOwnProperty('isOwner'))
	        {
	            self.options.isLegislationOwner = deliverableResponse.isOwner;	            
	        }
	        if(deliverableResponse.hasOwnProperty('deliverableStatus'))
	        {
	            self.options.delStatuses = deliverableResponse.deliverableStatus;	            
	        }
		    if(deliverableResponse.hasOwnProperty('deliverablesId'))
		    {
		        self.options.delStatuses = deliverableResponse.deliverablesId;
		    }
		    self.options.actionProgress = deliverableResponse.deliverableActionProgress;	
		    if( $.isEmptyObject(deliverableResponse.deliverable)) {
     			self._displayHeaders(deliverableResponse.headers, false);
			     $("#"+self.options.tableId).append($("<tr />")
				     .append($("<td />",{colspan:parseInt(deliverableResponse.columns, 10)+1, html:"There are no active deliverables allocated to you for the selected financial year/legislations"}))
			     );
		     } else {
		        self._displayPaging(deliverableResponse.total, deliverableResponse.columns, deliverableResponse.total_time);
		        self.options.total   = deliverableResponse.total;
		        self.options.usedIds = deliverableResponse.usedId; 
	            self._displayHeaders(deliverableResponse.headers, true);
		        self._display(deliverableResponse.deliverable, deliverableResponse.columns, deliverableResponse.isDeliverableOwner, deliverableResponse.delActions);
		     }			    
	        $("#loadingDiv").remove();	
	    });				
    } ,
    
    _display          : function(deliverables, columns, isDeliverableOwner, delActions)
    {
		var self = this;
		$.each(deliverables, function(index, deliverable){
		    var html = [];
		    html.push("<tr>");
		    if(self.options.showActions)
		    {
		       html.push("<td>");
		         html.push("<a href='#' id='more_less_"+index+"'>");
		          html.push("<span class='ui-icon ui-icon-plus'></span>") 
		         html.push("</a>");
		       html.push("</td>");
		       
		       $("#more_less_"+index).live("click", function(e){
                   if( $("#query_action_"+index).is(":hidden") )
                   {
                      $("#more_less_"+index+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                   } else {
                      $("#more_less_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");		
                   }
                   $("#query_action_"+index).toggle();
                   e.preventDefault();
		       });
		    }				    
			$.each(deliverable, function( key , val){
				html.push("<td>"+val+"</td>");
			})
            if(self.options.unlockEvent)
            {
               html.push("<td>&nbsp;");
                html.push("<input type='button' value='Unlock' id='unlock_"+index+"' id='unlock_"+index+"' />");
              html.push("</td>");
			    $("#unlock_"+index).live("click", function(e){
			       self._unlockEventDeliverable(index);
				   e.preventDefault();
			    });	               
            }
            if(self.options.editDeliverable)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='Update Query' id='update_"+index+"' id='update_"+index+"' />");
                html.push("<input type='button' value='Update Actions' id='update_actions_"+index+"' id='update_actions_"+index+"' />");
                html.push("</td>");
			    $("#update_"+index).live("click", function(){
				    document.location.href = "updaterisk.php?id="+index;								  
				    return false;
			    });			
			
			    $("#update_actions_"+index).live("click", function(){
				    document.location.href = "../actions/action_update.php?id="+index;								  
				    return false;
			    });	
            }
            if(self.options.eventActivation)
            {
               html.push("<td>&nbsp;");
               html.push("<input type='button' value='Edit Query' id='edit_"+index+"' id='edit_"+index+"' />");
               html.push("<input type='button' value='Edit Actions' id='edit_actions_"+index+"' id='edit_actions_"+index+"' />");
               html.push("</td>");
			   $("#edit_"+index).live("click", function(){
				    document.location.href = "editrisk.php?id="+index;
				    return false;
			   });
			   $("#edit_actions_"+index).live("click", function(){
				    document.location.href = "../actions/action_edit.php?id="+index;
				    return false;
			   });	
            }
            if(self.options.addAction)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='Add Action' id='add_action_"+index+"' id='add_action_"+index+"' />");
                html.push("</td>");
			    $("#add_action_"+index).live("click", function(){
				    document.location.href = "new_action.php?id="+index;								  
				    return false;
			    });               
            }
            if(self.options.viewDeliverable)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='View Details' id='view_query_"+index+"' id='view_query_"+index+"' />");
                html.push("</td>");
			    $("#view_query_"+index).live("click", function(){
				    document.location.href = "view_query.php?id="+index;								  
				    return false;
			    });               
            }			
			html.push("</tr>");
		    if(self.options.showActions)
		    {
               if(!$.isEmptyObject(delActions))
               {
                  if(delActions.hasOwnProperty(index))
                  { 
                    html.push("<tr id='query_action_"+index+"' class='query_actions' style='display:none;'>");
                      html.push("<td id='show_action_"+index+"' colspan='"+(columns+2)+"'></td>");
                    html.push("</tr>");
                  }
               }
		    }			
			$("#"+self.options.tableId).append( html.join(' ') )
			
		    if(self.options.showActions)
		    {
               if(!$.isEmptyObject(delActions))
               {
                  if(delActions.hasOwnProperty(index))
                  {
                      $("#show_action_"+index).action({deliverableId : index,
                                                       legislationId : self.options.legislationId,
                                                       financialyear : self.options.financialYear,  
                                                       section       : self.options.section,
                                                       viewAction    : self.options.viewDeliverable,
                                                       thClass       : "th2"
                                                      });
                  }
               }
		    }	
		});	
    } ,
    
    _displayHeaders   : function(headers)
    {
		var self = this;
		var html = [];
		html.push("<tr>");
		if(self.options.showActions)
		{
		   html.push("<th>&nbsp;</th>");
		}
		$.each( headers , function( index, head ){
		    html.push("<th>"+head+"</th>");
		});
		if(self.options.unlockEvent)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.editDeliverable)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.eventActivation)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.addAction)
		{
		  html.push("<th>&nbsp;</th>");
		}
        if(self.options.viewDeliverable)
        {
          html.push("<th>&nbsp;</th>");
        }  		

		html.push("</tr>");
		$("#"+self.options.tableId).append(html.join(' '));
    } ,
    
    _displayPaging     : function(total, columns, total_time)
    {
		var self  = this;
        var html  = [];
		var pages;
		if(total%self.options.limit > 0)
		{
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		html.push("<tr>");
		  if(self.options.showActions)
		  {
		       html.push("<td>&nbsp;</td>");
		  }				    
		  html.push("<td colspan='"+columns+"'>");
		    html.push("<input type='button' value=' |< ' id='deliverable_first' name='first' />");
		    html.push("<input type='button' value=' < ' id='deliverable_previous' name='previous' />");
		    html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
		    html.push("<input type='button' value=' > ' id='deliverable_next' name='next' />");
		    html.push("<input type='button' value=' >| ' id='deliverable_last' name='last' />");
		  html.push("</td>");
          if(self.options.unlockEvent)
          {
              html.push("<td>&nbsp;</td>");
          }
          if(self.options.editDeliverable)
          {
              html.push("<td>&nbsp;</td>");
          }
          if(self.options.editQuery)
          {
              html.push("<td>&nbsp;</td>");
          }
          if(self.options.eventActivation)
          {
              html.push("<td>&nbsp;</td>");
          }
          if(self.options.viewDeliverable)
          {
              html.push("<td>&nbsp;</td>");
          }          
		html.push("</tr>");
		$("#"+self.options.tableId).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#deliverable_first").attr('disabled', 'disabled');
            $("#deliverable_previous").attr('disabled', 'disabled');		     
        }
        if((self.options.current == pages || pages == 0))
        {
           $("#deliverable_next").attr('disabled', 'disabled');
           $("#deliverable_last").attr('disabled', 'disabled');		     
        }				
		$("#deliverable_next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#deliverable_last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#deliverable_previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#deliverable_first").bind("click",  function(evt){
			self._getFirst( self );
		});
    }  ,  
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getDeliverables();
	},	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getDeliverables();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getDeliverables();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getDeliverables();				
	} ,
	
	_unlockEventDeliverable         : function(deliverableId)
	{
	    var self = this;
	    var html = [];
        html.push("<table width='100%' class='noborder'>");
          html.push("<tr>");
            html.push("<td class='noborder'>");
              html.push("<p class='ui-state ui-state-info' style='padding:5px;'>");
                html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span><span style='margin-left:3px;'>Warning! This will remove the link between the event and the deliverable and activated the deliverable and any associated actions for updating. Please confirm that you are authorised to activate this deliverables and action(s).</span>")
              html.push("</p>");
            html.push("</td>");
          html.push("</tr>");       
	    html.push("</table>");
	    
	    if($("#activate_"+deliverableId+"_deliverable").length > 0)
	    {
	        $("#activate_"+deliverableId+"_deliverable").remove();
	    }
	    
	    $("<div />",{id:"activate_"+deliverableId+"_deliverable"}).append(html.join(' '))
	      .dialog({
	            autoOpen : true,
	            modal    : true,
	            title    : " Activate deliverable D"+deliverableId,
	            position : "top",
	            buttons  : {
	                         "Confirm"      : function()
	                         {
	                            $.post("../class/request.php?action=ClientDeliverable.activateEventDeliverable", 
	                            {
	                                deliverableId : deliverableId
	                            }, function(response){
	                                if(response.error)
	                                {
	                                   jsDisplayResult("error", "error", response.text);
	                                } else {
						               jsDisplayResult("ok", "ok", response.text);
						               self._getDeliverables();
						               $("#activate_"+deliverableId+"_deliverable").dialog("destroy").remove();
	                                }
	                            },"json");
	                         } ,
	                         
	                         "Cancel"       : function()
	                         {
	                            $("#activate_"+deliverableId+"_deliverable").dialog("destroy").remove();
	                         }
	            } ,
	            open        : function(event, ui)
	            {
		 			var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 			var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");	
		 			var saveButton = buttons[0];
		 			$(saveButton).css({"color":"#090"});
		 		    $(this).css("height", "auto")
	            } ,
	            
	            close       : function(event, ui)
	            {
	               $("#activate_"+deliveraleId+"_deliverable").dialog("destroy").remove();
	            }	            
	      });	    
	}
});

