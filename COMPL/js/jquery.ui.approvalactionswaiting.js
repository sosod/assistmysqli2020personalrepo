$.widget("ui.approvalactionswaiting", {
	
	options			: {
		start 			: 0,
		limit			: 10, 
		current			: 1,
		total			: 0,
		id				: 0, 
		headers			: ["action_reminder", "action_deadline_date"], 
		section             : "manage",
		page                : "",
		display			: "waiting",
		tableId			: "awaiting_approval",
		financialyear	: 0,
		legislationId	: 0,
		user			: "",
		filterWidth		: "200px",
		users			: {},
		financialYearsList : {},
		delResponsible	: true,
		delAccountable	: false,
		delPerson		: "R",
		screen_height			: "500",
		screen_width			: "500"
		
	} , 
		
	_init			: function()
	{
		//this._getActions();
	} , 
	
	_create			: function()
	{
		var self = this;
		var options = self.options;
       $(self.element).append($("<form />")
			.append($("<table />",{width:"100%"}).addClass("noborder")
				.append($("<tr />")
					.append($("<td />").addClass("noborder")
						.append($("<table />",{id:"filters"})
							.append($("<tr />")
								.append($("<th />",{html:"Filter by Financial Year:",width:options.filterWidth}).addClass("left"))
								.append($("<td />",{})
									.append($("<select />",{id:"financialyear",name:"financialyear"}).addClass("financial_year"))
								)
							)
							.append($("<tr />")
								.append($("<th />",{html:"Filter by Legislation:",width:options.filterWidth}).addClass("left"))
								.append($("<td />",{})
									.append($("<select />",{id:"legislationlist", name:"legislationlist"}))
								)
							)
							.append($("<tr />")
								.append($("<th />",{html:"Filter by Action Owner:",width:options.filterWidth}).addClass("left"))
								.append($("<td />",{})
									.append($("<select />",{id:"user", name:"user"}))
								)
							)
							.append($("<tr />")
								.append($("<th />",{html:"Where I am the...:",width:options.filterWidth}).addClass("left"))
								.append($("<td />",{})
									.append($("<select />",{id:"delperson", name:"delperson"})
										.append($("<option />",{value:"R",text:"Deliverable Responsible Person"}))
										.append($("<option />",{value:"A",text:"Deliverable Accountable Person"}))
										.append($("<option />",{value:"RA",text:"Either Deliverable Responsible Person or Deliverable Accountable Person"}))
									)
								)
							)
							.append($("<tr />")
								.append($("<th />",{width:options.filterWidth}).addClass("left"))
								.append($("<td />",{})
									.append($("<input />",{id:"filter", name:"filter",value:"Go",type:"button",class:"isubmit"})
										.click(function(){ 
											self.options.financialyear = $("#financialyear").val();
											self.options.legislationId = $("#legislationlist").val();
											self.options.user = $("#user").val();
											self.options.delPerson = $("#delperson").val();
											switch(self.options.delPerson) {
												case "R":	self.options.delResponsible = true; self.options.delAccountable = false; break;
												case "A":	self.options.delResponsible = false; self.options.delAccountable = true; break;
												case "RA":	self.options.delResponsible = true; self.options.delAccountable = true; break;
											}
											self._getActions();
										})
									)
								)
							)
						)
					)
				)
				.append($("<tr />")
					.append($("<td />").addClass("noborder")
						.append($("<table />",{id:options.tableId, width:"100%"})
							.append($("<tr />")
								.append($("<td />",{html:"Please select a Financial Year or Action Owner above before clicking the 'Go' button in order to display a list of Actions waiting approval."}))
							)
						)
					)
				)
			)
       )	
		$("#delperson").val("R");
       self._financialYears();
       self._loadLegislations(0);
       self._loadUsers();
	   self._getActions();

       $("#financialyear").live("change", function(e){
           self.options.financialyear = $(this).val();
           $("#message").hide();
              $("#legislationlist").empty();
              self._loadLegislations($(this).val());
              e.preventDefault();           
       });           
       
/*       $("#user").live("change", function(){
          options.user = $(this).val();
          options.start = 0;
          options.limit = 10;
          options.current = 1;
          options.total = 0;
          $("#message").hide();   
		  self._getActions();
       });
            
	  $("#legislationlist").live("change", function(){
          options.legislationId = $(this).val();
          options.start = 0;
          options.limit = 10;
          options.current = 1;
          options.total = 0;
          $("#message").hide();
		  self._getActions();
	  }); */
     } , 

     _loadUsers          : function()
     {
        var self = this,
            options = self.options;
        if($.isEmptyObject(options.users))
        {
             $("select#user").append($("<option />",{value:"", text:"--please select--"}))   
             $.getJSON("../class/request.php?action=User.getAll",function(users){
               if(!$.isEmptyObject(users))
               {
                  options.users = users;
                  $.each(users, function(index, user){
                    $("select#user").append($("<option />",{value:index, text:user.user})) 
                  })
               }       
             },"json");          
        } else {
           
        }
     },
     
     _financialYears     : function()
     {
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $(".financial_year").append($("<option />",{value:"", text:"--please select--"}));
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAllSorted",function(financialYears) {
	          if($.isEmptyObject(financialYears))
	          {
	              $(".financial_year").append($("<option />",{value:"", text:"--please select--"}))
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear) {
	               if(self.options.financialyear == financialYear.id)
	               {
                        $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date, selected:"selected"}))	                    
	               } else {
                      $(".financial_year").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date}))	                    
	               }
	             });
	          }	   
	        });
	   } 
     } ,
     
     _loadLegislations    : function(financialyear)
     {
		//alert(financialyear);
        var self = this; 
            options = self.options;
        if(financialyear!=0 && financialyear!="0") { 
			$("#legislationlist").empty();    
			$("select#legislationlist").append($("<option />",{value:"", text:"--please select--"}))   
			var tmpPage = "";
			if(self.options.page == "edit")
			{
			  tmpPage = "update";
			} else {
			   tmpPage = self.options.page; 
			}
			$.getJSON("../class/request.php?action=Legislation.getLegislations",
			   {data:{financial_year:financialyear, section:self.options.section, page:tmpPage}}, function(legislations){
			  $.each(legislations, function(index, legislation){
				 $("#legislationlist").append($("<option />",{text:"[L"+legislation.id+"] "+legislation.name, value:legislation.id}))
			  });        
			});
		  //self._getActions();
	  } else {
			$("#legislationlist").empty();    
			$("select#legislationlist").append($("<option />",{value:"", text:"--please select financial year--"}))   
		}
     } ,
	
	_getActions			: function()
	{
		var self = this;
		$("#"+self.options.tableId).empty();
		self._disableFilters(true);
	     $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"270px", left:"550px", border:"0px solid #FFFFF", padding:"5px"})
	     );		
		$.getJSON("../class/request.php?action=Action.getApprovalActions", {
			options : {
                    start			: self.options.start,
                    limit			: self.options.limit,
                    deliverableId	: self.options.id,
                    section        : self.options.section,
                    page           : self.options.page,
					display			: self.options.display,
					financialyear	: self.options.financialyear,
					legislationId	: self.options.legislationId,
					user			: self.options.user,
					responsible		: (self.options.delResponsible==true ? "1" : "0"),
					accountable		: (self.options.delAccountable==true ? "1" : "0")
			}
		}, function( responseData ){
               $(".approval").remove();               
               $("#loadingDiv").remove();
				//alert(responseData.awaiting.actions.size);
			if($.isEmptyObject(responseData.awaiting.actions)) {
				//if($.isEmptyObject(responseData.approved.actions) )
				//{
					self._displayHeaders(responseData.approved.headers, self.options.tableId, false );
				//}
				//responseData.awaiting.columns+=1;
				$("#"+self.options.tableId).append($("<tr />",{id:"tr_noawaiting"})
					.append($("<td />",{html:"There are no actions awaiting approval"}))
				)		
			} else {
				self._displayHeaders(responseData.awaiting.headers, self.options.tableId, true);
				self._display(responseData.awaiting.actions, self.options.tableId);
			}
			self._disableFilters(false);
			
		})
	} , 

	_disableFilters : function(act) {
		//alert(act);
		if(act==true) {
			$("#filter").removeClass("isubmit");
			$("#filter").attr("disabled","disabled");
		} else {
			$("#filter").addClass("isubmit");
			$("#filter").attr("disabled",false);
		}
		
	} ,

	
	_display			: function( actions, table )
	{  
		var self = this;
		$.each( actions , function( index, action){
			var tr = $("<tr />",{id:"tr_"+index}).addClass("approval")
			$.each( action, function( key, val){
				tr.append($("<td />",{html:val}))
			});
			if(table=="approved_actions") {
				//tr.append("<td><input type=button value='Unlock' class=unlock_action_approval /></td>")
				//tr.append("<td>abc</td>");
                tr.append($("<td />").append($("<input />",{type:"button", name:"unlock_"+index, id:"unlock_"+index, value:"Unlock"})
                  .click(function(e){
                    self._unlockAction(index);                   
					//alert(index);
                  })
                )
				)
				
			}
			tr.append($("<td />").css({display:(table == "awaiting_approval"  ? "table-cell" : "none")}).addClass("center")
                .append($("<input />",{type:"button", name:"view_logs_"+index, id:"view_logs_"+index, value:"View Logs"}).css({"margin-bottom":"10px"})
                  .click(function(e){
                    self._viewActionLog(index);                   
                  })
                )
				.append("<br />")
			    .append($("<input />",{type : "button", name : "approval_"+index, id : "approval_"+index, value : "Approve"})
			       .click(function(e){
				     if($("#approval_decline_"+index).length > 0)
				     {
				         $("#approval_decline_"+index).remove();
				     }
				     $("<div />",{id:"approval_decline_"+index})
				      .append($("<table />", {width:"100%"})
				        .append($("<tr />")
					      .append($("<th />",{html:"Response:"}))
					      .append($("<td />")
					        .append($("<textarea />",{cols:"30", rows:"10", name:"response", id:"response"}))		 
					      )
				        )
				        .append($("<tr />")
					      .append($("<td />",{colspan:"2", id:"response_message"}))
				        )
				      )
				      .dialog({
			           	  autoOpen	: true,
			           	  modal		: true, 
			           	  title		: "Approve/Decline Action A"+index,
			           	  width        : 500,				
			           	  position     : "top",	 	  
			           	  buttons		: {
           							"Approve"	: function()
           							{
           								jsDisplayResult("info", "info", "Approving action . . . <img src='../images/loaderA32.gif' />");
           								var data  	= {};
           								data.id   	= index;
           								data.response	= $("#response").val();
           								data.signoff	= $("#signoff :selected").val()
           								$.post("../class/request.php?action=ClientAction.approveAction",
           								{ data : data },function( response ){
           										if( response.error )
           										{						 										
           											jsDisplayResult("error", "error", response.text);
           										} else {
           											jsDisplayResult("ok", "ok", response.text);
           								               self._getActions();
           										}					 										
           								},"json")
           								$("#approval_decline_"+index).dialog("destroy")
						           	    $("#approval_decline_"+index).remove();
           							} , 
           							
           							"Decline"	: function()
           							{
           								if($("#response").val() == ""){
           									dialogResult("response_message", "Please enter the response", "error" , "error");
           									return false;
           								} else {
           									jsDisplayResult("info", "info", "Declining action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
           									data.id   		= index;
           									data.response	= $("#response").val();
           									data.signoff	= 1
           									$.post("../class/request.php?action=ClientAction.declineAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json")
           									$("#approval_decline_"+index).dialog("destroy")
						           	          $("#approval_decline_"+index).remove();
           								}
           							}	
		           		} ,
		           		open           : function(event, ui)
		           		{
                               var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                               var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                               var approveBtn = buttons[0];
                               var declineBtn = buttons[1];
                               $(approveBtn).css({"color":"#090"});
                               $(declineBtn).css({"color":"red"});
                               $(this).css("height", "auto");
		           		},
		           	     close       :  function(event, ui)
		           	     {
						     $("#approval_decline_"+index).dialog("destroy")
		           	          $("#approval_decline_"+index).remove();	      	          
		           	     }					  
		           	
				      }) 
				      e.preventDefault();
			       })
			    )		
			)
			$("#"+table).append( tr );
		})
		
	} , 
	
	
	_displayHeaders	: function( headers, table, showTr )
	{
		var self = this;
		var tr   = $("<tr />").addClass("approval")
		$.each( headers, function( key, head){
			tr.append($("<th />",{html:head}))
		});	
		if(table=="approved_actions") {
			tr.append("<th></th>");
		}
		tr.append($("<th />").css({display:((table == "awaiting_approval" && showTr)   ? "table-cell" : "none")}))		
		$("#"+table).append( tr )
	} , 
	
	_viewActionLog			: function(actionId)
	{
       var self = this;
       if($("#viewlogdialog").length > 0)
       {
         $("#viewlogdialog").remove(); 
       }
       
       $("<div />",{id:"viewlogdialog"}).css({height:"100%"})
       .append($("<table />",{width:"100%", id:"action_log_table"})
         .append($("<tr />")
           .append($("<th />",{html:"Date/Time"}))
           .append($("<th />",{html:"User"}))
           .append($("<th />",{html:"Change Log"}))
           .append($("<th />",{html:"Status"}))
         )        
       ).dialog({
                autoOpen    : true,
                modal       : true,
                width       : (self.options.screen_width*.50)+"px",
                position    : { my: "center top", at: "center top", of: window },
                title       : "Action A"+actionId+" Activity Log ",
                buttons     : {
                                "Ok"    : function()
                                {
                                    $("#viewlogdialog").remove();                               
                                }
                } ,
                close       : function()
                {
                   $("#viewlogdialog").remove();                
                } ,
                open        : function()
                {
                   $.get("../class/request.php?action=ClientAction.getActionUpdateLog", {
                    action_id : actionId
                   }, function(actionUpdates){
                      $("#action_log_table").append(actionUpdates);
                   },"html");           
                   $(this).css("height", (self.options.screen_height*0.9)+"px");  
				   //alert(self.options.screen_height);
                }
                      
       })
	} ,
		
	_unlockAction : function(index) {
		var self = this;
		/*if(confirm("Are you sure you wish to remove the Approval status of Action A"+index+"?")==true) {
           									jsDisplayResult("info", "info", "Unlocking action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
											data.id = index;
           									data.response	= "testing unlock";
           									data.signoff	= 1;
           									$.post("../class/request.php?action=ClientAction.unlockAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json");
		}*/
		if($("#approval_decline_"+index).length > 0)
				     {
				         $("#approval_decline_"+index).remove();
				     }
				     $("<div />",{id:"approval_decline_"+index})
					 .append("<p>Are you sure you wish to reverse the Approval of Action A"+index+"?</p>")
				      .append($("<table />", {width:"100%"})
				        .append($("<tr />")
					      .append($("<th />",{html:"Reason:"}))
					      .append($("<td />")
					        .append($("<textarea />",{cols:"30", rows:"10", name:"response", id:"response"}))		 
					      )
				        )
				        .append($("<tr />")
					      .append($("<td />",{colspan:"2", id:"response_message"}))
				        )
				      )
				      .dialog({
			           	  autoOpen	: true,
			           	  modal		: true, 
			           	  title		: "Unlock Action A"+index,
			           	  width        : 500,				
			           	  position     : "top",	 	  
			           	  buttons		: {
           							"Yes"	: function()
           							{
           								if($("#response").val() == ""){
           									dialogResult("response_message", "Please enter a reason for the reversal.", "error" , "error");
           									return false;
           								} else {
           									jsDisplayResult("info", "info", "Unlocking action . . . <img src='../images/loaderA32.gif' />");
           									var data  		= {};
           									data.id   		= index;
           									data.response	= $("#response").val();
           									data.signoff	= 1
           									$.post("../class/request.php?action=ClientAction.unlockAction",
           									{ data : data },function( response ){
           									  if( response.error )
           									  {
           										jsDisplayResult("error", "error", response.text);
           									  } else {
           										self._getActions();
           									     jsDisplayResult("ok", "ok", response.text);
           									  }					 										
           									},"json")
           									$("#approval_decline_"+index).dialog("destroy")
						           	          $("#approval_decline_"+index).remove();
           								}
           							}	,
           							"No"	: function()
           							{
           								$("#approval_decline_"+index).dialog("destroy")
						           	    $("#approval_decline_"+index).remove();
           							} 
           							
		           		} ,
		           		open           : function(event, ui)
		           		{
                               var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                               var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                               var approveBtn = buttons[0];
                               var declineBtn = buttons[1];
                               $(approveBtn).css({"color":"#090"});
                               $(declineBtn).css({"color":"red"});
                               $(this).css("height", "auto");
		           		},
		           	     close       :  function(event, ui)
		           	     {
						     $("#approval_decline_"+index).dialog("destroy")
		           	          $("#approval_decline_"+index).remove();	      	          
		           	     }					  
		           	
				      }) 
	  }
});
