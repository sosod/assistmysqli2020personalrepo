// JavaScript Document
$(function(){
	if( $("#deliverableid").val() === undefined)
	{
	    //Deliverable.getUsers();
	    //Deliverable.getOrganisationSizes();
		Deliverable.getOrganisationTypes();
		Deliverable.getFunctionalService();
		Deliverable.getMainEvents();
		Deliverable.getDepartments();
		Deliverable.getComplianceFrequency();
		Deliverable.getResponsibleOwner();		
		Deliverable.getAccountablePerson();		
		Deliverable.getComplianceTo();
		Deliverable.getReportingCategory();	
		Deliverable.getSubEvents(1);						
    }


	$("table th").attr("valign", "top");
	
	$("#has_actions").change(function() {
		
		if( $(this).val() == 0)
		{
		var deadlinedate = $("#legislation_deadline").val();
		$("<div />",{id:"hasactions_dialog"})
		.append($("<p />",{html:"To enable the system to track and report on the compliance process , please complete the fields below"}).addClass("ui-state").addClass("ui-state-highlight"))
		.append($("<table />",{id:"table_actioninfo"})
			.append($("<tr />")
			  .append($("<th />",{html:"Action Deadline Date:"}))
			  .append($("<td />")
				.append($("<input />",{type:"text", name:"action_deadlinedate", id:"action_deadlinedate", value:deadlinedate, readonly:"readonly"}))
			  )
			)	
			.append($("<tr />")
			  .append($("<th />",{html:"Action Owner:"}))
			  .append($("<td />")
				 .append($("<select />",{id:"action_owner", name:"action_owner"})
				   .append($("<option />",{id:"", text:"action owner"}))		 
				 )	  
			  )
			)			
			).dialog({
				autoOpen	: true, 
				modal		: true,
				width		: "500px",
				title		: "Add Action",
				buttons		: {
								"Ok"	: function()
								{
									$("#actionowner").val( $("#action_owner :selected").val() )
									$("#actiondeadlinedate").val( $("#action_deadlinedate").val() )
									$("#hasactions_dialog").dialog("destroy");
									$("#hasactions_dialog").remove();
								} , 
								"Cancel"	: function()
								{
									$("#hasactions_dialog").dialog("destroy");
									$("#hasactions_dialog").remove();
								}
							 } , 
			     open           : function(event, ui)
			     {				         		
	                    Deliverable.getActionOwner();
			     }
			});
			
			$("#action_deadlinedate").datepicker({
				showOn		: "both",
				buttonImage 	: "/library/jquery/css/calendar.gif",
				buttonImageOnly: true,
				changeMonth	: true,
				changeYear	: true,
				dateFormat 	: "dd-M-yy"
			});	
		}

		return false;
	});	
	
	$("#_savedeliverable").click(function(e){
	    if($("#adddeliverableDialog").length > 0)
	    {
	       $("#adddeliverableDialog").remove();
	    }
	    
	    $("<div />",{id:"adddeliverableDialog", html:"Confirm adding a deliverable . . . "})
	     .append($("<ul />")
	       
	     ).dialog({
	               autoOpen  : true,
	               modal     : true,
	               title     : "Add New Deliverable",
	               width     : 300,
	               height    : 300,
	               position  : "top",
	               buttons   : {
	                              "Ok"      : function()
	                              {
	                                Deliverable.save("manage");
	                                $("#adddeliverableDialog").dialog("destroy");
	                                $("#adddeliverableDialog").remove();	                                                 
	                              } ,
	                              "Cancel"  : function()
	                              {
	                                $("#adddeliverableDialog").dialog("destroy");
	                                $("#adddeliverableDialog").remove();	                              
	                              }
	              },
	              close       : function(event, ui)
	              {
	                 $("#adddeliverableDialog").dialog("destroy");
	                 $("#adddeliverableDialog").remove();
	              }, 
	              open        : function(event, ui)
	              {
	              
	              }     
	     })
	
	   e.preventDefault();
	});
	
	$("#recurring_type").live("click", function(){
		if( $(this).val() == "fixed"){
			$(".fixed").show();			
			$("#days").hide()
		} else {
			$(".fixed").hide();
			$("#days").show();
		}		
		return false;
	});
	
	$("#recurring").change(function() {
		if( $(this).val() == "") {  
			$("#more_details").hide();
			$(".recurr").hide();
		} else if( $(this).val() == 0) {
			$(".recurr").hide();
			$(".fixed").show();
		} else {
			$(".fixed").hide();
			$(".nonfixed").show();
			$(".recurr").show();
		}
		return false;
	});
	
	$("#save_deliverable").click(function(){
		Deliverable.save();
		return false;					  
	});

	$("#edit").click(function(){
		Deliverable.edit();
		return false;
	});

	$("#update").click(function(){
		Deliverable.update();
		return false;
	});	
	
    $("#main_event").live("change", function(){
	    Deliverable.getSubEvents( $(this).val() );
        return false;
    });

    $("#main_event_").live("change", function(){
	    Deliverable.getSubEvent( $(this).val() );
        return false;
    });    
    
    $("#view_edit_log").live("click",function(){
       Deliverable.viewEditLog();
       return false;
    });

    $("#import").live("click",function(){
       document.location.href = "import_deliverable.php?legislationid="+$("#legislationid").val();;
       return false;
    });
    
    $("#delete").click(function(){
    	Deliverable.deleteDeliverable();
    	return false;
    });
    
	$("#compliance_date").live("change", function() {
		$("#legislation_deadline").val( $(this).val() );
		return false;
	});
    
	$("#legislation_deadline").live("change", function(){
		var thisdate = $(this).val();
		if( $("#compliance_date").val() != "")
		{
			if( $("#compliance_date").val() < thisdate  )
			{
				$("<div />",{id:"correctdate_dialog"}).append($("<p />",{html:"The deadline date should not be greater / after the compliance date "+$("#compliance_date").val()}).addClass("ui-ste"))
				.dialog({
						autoOpen	: true,
						modal		: true,
						title		: "Correct Date", 
						buttons		: {
										"Ok"	: function()
										{
											$("#legislation_deadline").val( $("#compliance_date").val() )
											$("#correctdate_dialog").dialog("'destroy");
											$("#correctdate_dialog").remove();
										} 
									  }					
				});				
			}			
		}
		return false;
	});
	
    
});

function showDatePicker(input)
{
	$("#legislation_deadline").datepicker({
		showOn		: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly: true,
		changeMonth	: true,
		changeYear	: true,
		dateFormat 	: "dd-M-yy"
	});	
}

Deliverable			= {
	
	getActionOwner           : function()
	{
	     var users = $("body").data("users");
          $.each(users, function(index, user){
               $("#action_owner").append($("<option />",{text:user.user, value:index}))
          });
	} ,
		
     getReportingCategory     : function()
     {
		$.getJSON("../class/request.php?action=ReportingCategories.getReportingCategories",{data:{status:1}}, function( reportingCategories ){
		$.each( reportingCategories, function( index, reportingCategory){
				$("#reporting_category").append($("<option />",{value:reportingCategory.id, text:reportingCategory.name}))
			});
			sortSelectOptions($("#reporting_category"));			
		});
     } ,		
	
	getComplianceFrequency	: function(){
		$.post("../class/request.php?action=ComplianceFrequency.getAll",{data:{status:1}},function( compfrequencies ){
            $.each(compfrequencies, function( index, complianceFrequency){
               $("#compliance_frequency").append($("<option />",{text:complianceFrequency.name, value:complianceFrequency.id }))
            });
        },"json");
	} ,
	
	
	getComplianceTo		: function(){
		$.post("../class/request.php?action=ComplianceToManager.getAll",{data:{status:1}}, function( data ){
            $.each( data, function( index, compliance){
				var displayText = "";
				if(compliance.title == compliance.user) { displayText = compliance.title; } else { displayText = compliance.title+" ("+compliance.user+")"; }
             $("#compliance_to").append($("<option />",{text:displayText, value:compliance.id }));
           });
        },"json");
		sortSelectOptions($("#compliance_to"));
	} ,	
	
     getUsers                 : function()
     {
         $.ajaxSetup({async:false});
	    $.getJSON("../class/request.php?action=User.getAll", function(users){
           $("body").data("users", users);
        });

     } ,
	
	getResponsibleOwner				: function()
	{     
	     Deliverable.getUsers();
          var users = $("body").data("users");
          $.each(users, function(index, user){
               $("#responsibility_owner").append($("<option />",{text:user.user, value:index}));
          });
	    /*$.post("../class/request.php?action=User.getAll", function(users){
        },"json");*/
		sortSelectOptions($("#responsibility_owner"));
	} ,
	
	getAccountablePerson				: function(){
		$.post("../class/request.php?action=AccountablePersonManager.getAll",{data:{status:1}},function( data ){
         $.each( data, function( index, accPerson){
			var displayText = "";
			if(accPerson.title==accPerson.user) { displayText = accPerson.title; } else { displayText = accPerson.role; }
            $("#accountable_person").append($("<option />",{text:displayText, value:index}))
         });
        },"json");
		sortSelectOptions($("#accountable_person"));
	} ,	
	
    getDepartments:function()
    {
        $.get("../class/request.php?action=ClientDepartment.getAllSorted", function( direData ){
          $.each( direData ,function( index, directorate ) {
				if(directorate.subyn=="Y" && directorate.diryn=="Y") {
					$("#responsible").append($("<option />",{text:directorate.name, value:index}));
				}
           })
        }, "json");

    },
	getOrganisationTypes	: function()
	{
		$.post("../class/request.php?action=OrganisationTypesManager.getAll",{data:{status:1}},function( orgtypes ){
			$.each( orgtypes, function( index, orgtype ){
				$("#applicable_org_type").append($("<option />",{text:orgtype.name, value:orgtype.id}))
			});
		}, "json");	
	} ,
	
	getFunctionalService	: function()
	{
		$.post("../class/request.php?action=FunctionalServiceManager.getClientFunctionalServices",
			 { data:{active:1} }, function( functional_services ){
			$.each( functional_services, function( index, functional_service ){
				$("#functional_service").append($("<option />",{text:functional_service, value:index}));
			});
			sortSelectOptions($("#functional_service"));
		}, "json");	
	} ,

	getMainEvents	: function()
	{
		$.post("../class/request.php?action=Event.getAll",{data:{status:1}}, function( main_events ){
			$.each( main_events, function( index, main_event ){
				$("#main_event").append($("<option />",{text:main_event.name, value:main_event.id}))
			});
			sortSelectOptions($("#main_event"));
		}, "json");
	} ,

	getSubEvents	: function( id )
	{
	   $("#sub_event").empty();
        var data	= {};
        data.eventid = id;
        data.status  = 1;
		$.post("../class/request.php?action=SubEvent.getAll",{data:data}, function( sub_events ){
			$.each( sub_events, function( index, sub_event ){
				$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
			});
			sortSelectOptions($("#sub_event"));
			if(id == 1)
			{
				$("#sub_event option[value=1]").attr("selected", "selected")
			}
		}, "json");
	} ,
		
	save					: function(type)
	{
		var data 				     = {};
		data.legislation           = $("#legislationid").val();
		data.description 		  = $("#description").val();
		data.short_description	  = $("#short_description").val();
		data.legislation_section   = $("#legislation_section").val();
		data.compliance_frequency  = $("#compliance_frequency  :selected").val();
		data.applicable_org_type   = $("#applicable_org_type :selected").val();
		data.compliance_date  	  = $("#compliance_date").val();
		data.functional_service    = $("#functional_service :selected").val();
		data.accountable_person    = $("#accountable_person").val();
		data.responsibility_owner  = $("#responsibility_owner :selected").val();
		data.responsible           = $("#responsible :selected").val();
		data.legislation_deadline  = $("#legislation_deadline").val();
		data.reminder 	            = $("#reminder").val();
		data.sanction              = $("#sanction").val();
		data.reference_link        = $("#reference_link").val();
		data.assurance             = $("#assurance").val();
		data.compliance_to         = $("#compliance_to").val();
		data.main_event            = $("#main_event :selected").val();
		data.sub_event             = $("#sub_event").val();
		data.guidance              = $("#guidance").val();
		data.hasActions 		      = $("#has_actions :selected").val();
		data.recurring			      = $("#recurring :selected").val();
		data.actionowner    	      = $("#actionowner").val();
		data.actiondeadlinedate    = $("#actiondeadlinedate").val();
		data.recurring_type		   = $("#recurring_type :selected").val(); 
		data.recurring_period	   = $("#recurring_period").val() +" "+$("#period :selected").val()
		data.deliverabletype       = type;
		data.reporting_category    = $("#reporting_category").val()
		/*		
		if( $("#fixed").is(":checked")){
			data.recurring_type		   = "fixed";
		} else if( $("#start").is(":checked")){
			data.recurring_type		   = "start";
		} else if($("#end").is(":checked")){
			data.recurring_type		   = "end";
		}
		*/
		var valid = Deliverable.isValid( data );
		if( valid == true)
		{
			jsDisplayResult("info", "info", "Saving   . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientDeliverable.saveDeliverable", { data: data}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					document.location.href = "?saved=1&text="+response.text+"&id="+$("#legislationid").val();
					//$("#display_deliverable").html("");	
					//document.location.href = "?saved=1&text="+response.text+"&id="+$("#legislationid").val();
					//$("#display_deliverable").deliverable({editDeliverable:true, page:"add_deliverable",legislationId:$("#legislationid").val(), reference : $("#legislation_reference").val() , headers:["reference_link", "guidance", "sanction"]});
					//$("#display_deliverable").deliverable({editDeleverable:true, legislationId:$("#legislationid").val(), headers:["reference_link", "guidance", "sanction"]});
	                $.each( data, function( index, value){
	                    $("#"+index).val("");
	                });					
	                jsDisplayResult("ok", "ok", response.text);
				}
			} ,"json");		
		} else {
			jsDisplayResult("error", "error", valid);
		}
		
	} , 
	
	edit 				: function()
	{
	    var self                   = this;
		var data 				   = {};
		data.legislation           = $("#legislationid").val();
		data.description 		   = $("#description").val();
		data.short_description	   = $("#short_description").val();
		data.legislation_section   = $("#legislation_section").val();
		data.compliance_frequency  = $("#compliance_frequency  :selected").val();
		data.applicable_org_type   = $("#applicable_org_type :selected").val();
		data.compliance_date  	   = $("#compliance_date").val();
		data.functional_service    = $("#functional_service :selected").val();
		data.accountable_person    = $("#accountable_person").val();
		data.responsibility_owner  = $("#responsibility_owner :selected").val();
		data.responsible           = $("#responsible :selected").val();
		data.legislation_deadline  = $("#legislation_deadline").val();
		data.reminder 	           = $("#reminder").val();
		data.sanction              = $("#sanction").val();
		data.reference_link        = $("#reference_link").val();
		data.assurance             = $("#assurance").val();
		data.compliance_to         = $("#compliance_to").val();
		data.main_event            = $("#main_event :selected").val();
		data.sub_event             = $("#sub_event").val();
		data.guidance              = $("#guidance").val();
		data.recurring			   = $("#recurring :selected").val();
		data.actionowner    	   = $("#actionowner").val();
		data.recurring_type		   = $("#recurring_type :selected").val();  
		data.id 				   = $("#deliverableid").val();
		data.reporting_category    = $("#reporting_category").val();
		if(($("#period :selected").val() !== undefined) || ($("#recurring_period").val() !== undefined))
		{
		  data.recurring_period = $("#recurring_period").val() +" "+$("#period :selected").val()       
		}
			
        /*if( $("#fixed").is(":checked")){
        	data.recurring_type		   = "fixed";
        } else if( $("#start").is(":checked")){
        	data.recurring_type		   = "start";
        } else if($("#end").is(":checked")){
        	data.recurring_type		   = "end";
        }
        */
		if( data.short_description === ""){
		   jsDisplayResult("error", "error", "Please enter the short description" )
		   return false;
		} else if(data.legislation_section === "") {
		   jsDisplayResult("error", "error", "Please select the legislation section" )
		   return false
		} else if(data.compliance_frequency === "") {
		   jsDisplayResult("error", "error", "Please enter the deliverable compliance frequency" )
		   return false		
		} else if($("#recurring :selected").val() === "") {
		   jsDisplayResult("error", "error", "Please enter the legislative compliance date");
		   return false				
		} else if(data.accountable_person === null){
		   jsDisplayResult("error", "error", "Please select the accountable person");
		   return false
		} else if(data.responsibility_owner === ""){
		   jsDisplayResult("error", "error", "Please select the responsible owner");
		   return false
		} else if(data.compliance_to === null){
		   jsDisplayResult("error", "error", "Please select compliance given to");
		   return false		
		}  else {
        	 if($("#mainevent").val() != $("#main_event").val())
        	 {
        	    if($("#main_event").val() !== 1 && $("#mainevent").val() == 1)
        	    {
	                var self = this;
	                var html = [];
                    html.push("<table width='100%' class='noborder'>");
                      html.push("<tr>");
                        html.push("<td class='noborder'>");
                          html.push("<p class='ui-state ui-state-info' style='padding:5px;'>");
                            html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span><span style='margin-left:3px;'>Warning! Adding a link between an event and the deliverable will deactivate the deliverable and any associated actions until the event is triggered and activated. The deliverable and associated actions will no longer be available for updating and editing. Are you sure you want to continue</span>")
                          html.push("</p>");
                        html.push("</td>");
                      html.push("</tr>");       
	                html.push("</table>");
	                
	                $("<div />",{id:"notification_"+data.id+"_deliverable"}).append(html.join(' '))
	                  .dialog({
	                        autoOpen : true,
	                        modal    : true,
	                        title    : " Edit deliverable D"+data.id,
	                        position : "top",
	                        buttons  : { 
	                                     "Yes"       : function()
	                                     {
	                                        $("#notification_"+data.id+"_deliverable").dialog("destroy").remove();
	                                        self._saveDeliverableEdit(data);
	                                     } ,
	                                     "No"       : function()
	                                     {
	                                        $("#notification_"+data.id+"_deliverable").dialog("destroy").remove();
	                                     }
	                        } ,
	                        open        : function(event, ui)
	                        {
		             			var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		             			var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");	
		             			var saveButton = buttons[0];
		             			$(saveButton).css({"color":"#090"});
		             		    $(this).css("height", "auto");
	                        } ,
	                        
	                        close       : function(event, ui)
	                        {
	                           $("#notification_"+data.id+"_deliverable").dialog("destroy").remove();
	                        }	            
	                  }); 
        	    } else {
        	       self._saveDeliverableEdit(data);
        	    }
        	 } else {
        	    self._saveDeliverableEdit(data);
        	 }
        	 /*

            */
		}  
	} , 
	
	
	_saveDeliverableEdit    : function(data)
	{
    	 jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
         $.post("../class/request.php?action=ClientDeliverable.editDeliverable", { data: data }, function( response ){
	        if(response.updated)
	        {
	          if( response.error)
	          {
		        jsDisplayResult("info", "info", response.text);
	          } else {
		        jsDisplayResult("ok", "ok", response.text);						
	          }	
	        } else {					
		        jsDisplayResult("error", "error", response.text);
	        }
         } ,"json");
    },

	update 			: function()
	{
		var data 	  = {};
		var response  = $("#response").val();
		var status 	  = $("#deliverablestatus :selected").val();
		var reminder  = $("#remindon").val();
        if(response === "")
        {
        	jsDisplayResult("error", "error", "Please eneter the response");
        	return false;
        } else if( status === "") {
        	jsDisplayResult("error", "error", "Please select the status");
        	return false;
        } else {
		
        	data.response 			= response;
        	data.deliverable_status = status;
        	data.reminder			= reminder
        	data.id 	  			= $("#deliverableId").val(); 
        	data.status	  			= $("#delstatus").val(); 
        	jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
            $.post("../class/request.php?action=ClientDeliverable.updateDeliverable", { data: data }, function( response ){
			   if(response.updated )
			   {
                  if(response.error)
                  {
                    jsDisplayResult("info", "info", response.text);
                  } else {
                    jsDisplayResult("ok", "ok", response.text);						
                  }	
                  $("#response").val("");
			   } else {					
				  jsDisplayResult("error", "error", response.text);
			   }
            } ,"json");

        } 
	} ,
	
	deleteDeliverable			: function()
	{
		if( confirm("Are you sure you want to delete this deliverable"))
		{
			jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' />");
            $("#deliverable_message").show().html( "deleting deliverable . . . . <img src='../images/loaderA32.gif' />" );
              $.post("../class/request.php?action=ClientDeliverable.deleteDeliverable", {
               	id  : $("#deliverableid").val()
               }, function( response ){
  				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
            } ,"json");	
		}
	} , 
	
	isValid			: function( data )
	{

		/*if( data.description == ""){
			return "Please enter the description ";
		} else */
		if( data.short_description == ""){
			return "Please enter the short description ";
		} else if( data.legislation_section == "") {
			return "Please select the legislation section";
		} else if( data.compliance_frequency == "") {
			return "Please enter the legislative compliance frequency";
		} else if( $("#recurring :selected").val() == "") {
			return "Please enter the legislative compliance date";
		} else if( data.accountable_person == null ){
			return "Please select the accountable person";
		} else if( data.compliance_to == null){
			return "Please select compliance given to";
		} else if( data.main_event == null ){
			return "Please select main event";
		} else if( data.sub_event == null){
			return "Please select sub event";
		}  else {
			return true;	
		}  		
		 /*
		 else if( data.responsibility_owner == ""){
			return "Please select the responsibility owner";
		} else if( data.sanction == ""){
			return "Please enter the sanction";
		} else if( data.assurance == ""){
			return "Please enter assurance"
		} else if( data.applicable_org_type == ""){
			return "Please select the applicable organisation type";
		} else if( data.guidance == ""){
			return "Please enter the guidance";
		}*/ 
		
	}

}
