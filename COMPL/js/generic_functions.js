//generic functions
$(function(){
	
	$("th").css({"text-align":"left"})	
	
	var count = 0;
	var max   = 100;
	$(".textcounter").html( "<br />"+ max +" allowed ")
	$(".checkcounter").keyup(function(){
		
		var id   = this.id
		var text = $(this).val();
		count    = text.length
		if( count > max ){
			var new_text = text.substr(0, max);
			$(this).val( new_text );
			return false;
		} else {
			var left = max - count;
			$("#"+id+"_textcounter").html( "<br />"+left+" characters left"  )
			return true;
		}
		
	});	
	
	$(".datepicker").datepicker({
        showOn          : 'both',
        buttonImage     : '/library/jquery/css/calendar.gif',
        buttonImageOnly : true,
        dateFormat      : 'dd-M-yy',
        changeMonth     :true,
        changeYear      :true		
    });

	$(".historydate").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		maxDate             : 0
	});	
	
	$(".futuredate").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		minDate         : 0
	});	
	
	$("#clear_remind").click(function(e){
		$("#remindon").val("");
		$("#reminder").val("");
		e.preventDefault();
	});	
    
    $(".header").click(function(){
       if($(".rulesbox").length > 0)
       {
          $(".rulesbox").remove();
       }
       var headerId = this.id.substr(6);
       var offset = $(this).offset();
       var leftOffset = (offset.left + 210)+"px";
       var topOffset  = (offset.top - 140)+"px";
       var fieldName  = $(this).text(); 
       $("body").append($("<div />", {id:"rules_"+headerId}).addClass("rulesbox")
         .append($("<h3 />",{html:fieldName+" must follow these rules"})
            .css({"background":"#009", color:"#FFFFFF", "margin-top":"2px", "padding":"5px", "font-size":"12px"})
         )
         .append($("<div />",{html:"Loading rules . . .", id:"main_content"}).addClass("ui-state-info").css({"padding":"3px", "margin:bottom":"5px"}))
         .css({position:"absolute", "z-index":"9999", top:topOffset, left:leftOffset, border:"1px solid #000", padding:"5px", "background":"#FFF", width:"400px", height:"auto", "border-radius":"3px", "margin-bottom":"10px"})
       ).click(function(){
          if($(".rulesbox").length > 0)
          {
             $(".rulesbox").remove();
          }
       })
       $.get("../class/request.php?action=Glossary.getRules",{
          field : headerId        
       }, function(response){
          if(response === "")
          {
             $("#main_content").html("No rules found for this field");       
          } else {
             $("#main_content").html(response);       
          }
       },"html");
	  return false;
    });
    
    $(".viewmore").live("click", function(e){
       var id = this.id;
       var parentSpan = $(this).parent().attr("id");
       var longText = $(this).attr("title");
       var shortText = $("#"+parentSpan).text();
       var positionOfMore = shortText.lastIndexOf("more..."); 
       shortText = shortText.substr(0, positionOfMore);
       $("#"+parentSpan).html("");
       $("#"+parentSpan).html(longText+" <a href='#' class='less' id='less_"+id+"' style='color:orange;'>less...</a>");
         $("#less_"+id).bind("click", function(e){
           $("#"+parentSpan).html("");
           $("#"+parentSpan).html(shortText+" <a href='#' class='viewmore' id='"+id+"' title='"+longText+"' style='color:orange;'>more...</a>");
           e.preventDefault();
         })
       
       return false;
    });

	
	
	$("table.list th").css("text-align","center");
	
	
	
});



function dialogResult(id, text, result, icon)
{
	$("#display_result_"+id).remove();
	$("#"+id).append($("<p />",{id:"display_result_"+id})
	  .css({'margin':'5px 0px 10px 0px','padding':'0.5em', 'font-size':'0.8em', 'width':'350px'})
	  .addClass("result").addClass("ui-state").addClass("ui-widget").addClass("ui-state-"+result)
	  .append($("<span />", {id:"display_icon_"+id}).addClass("ui-icon").css({'float':'left', 'margin-right':'0.3em'}))
	  .append($("<span />",{id:"display_text_"+id, html:text}))
	  .append($("<span />").css({'float':'right', 'margin-right':'0.3em'})
	    .append($("<a />",{text:"Close", href:"#", id:"close"}).css({"color":"black"}))
	  )
	)
	
	$("#close").live("click", function(){
		$("#display_result_"+id).remove();
		return false;
	});
	
	switch( icon )
	{
		case "ok":
			$("#display_icon_"+id).addClass("ui-icon-check");
		break;
		case "info":
			$("#display_icon_"+id).addClass("ui-icon-info");	
		break;
		case "error":
			$("#display_icon_"+id).addClass("ui-icon-closethick");
		break;
		default:
			$("#display_icon_"+id).addClass("ui-icon-"+icon);
		break;
	
	}              
}

function getWindowSize() {
		var viewportwidth;  
		var viewportheight;     // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight     
		if (typeof window.innerWidth != 'undefined')  {       
			viewportwidth = window.innerWidth,       
			viewportheight = window.innerHeight  
		}    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)    
		else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)  {
			viewportwidth = document.documentElement.clientWidth,
			viewportheight = document.documentElement.clientHeight  
		}     // older versions of IE     
		else {        
			viewportwidth = document.getElementsByTagName('body')[0].clientWidth,        
			viewportheight = document.getElementsByTagName('body')[0].clientHeight  
		} 
	var ret = new Array();
	ret['width'] = viewportwidth;
	ret['height'] = viewportheight-120; //footer + header
	return ret;
}


function sortSelectOptions($dd) {
	if ($dd.length > 0) { // make sure we found the select we were looking for

		// save the selected value
		var selectedVal = $dd.val();

		// get the options and loop through them
		var $options = $('option', $dd);
		var arrVals = [];
		$options.each(function(){
			// push each option value and text into an array
			arrVals.push({
				val: $(this).val(),
				text: $(this).text()
			});
		});

		// sort the array by the value (change val to text to sort by text instead)
		arrVals.sort(function(a, b){
			if(a.text=="Unspecified") { 
				var ret = -1;
			} else if(b.text=="Unspecified") {
				var ret = 1;
			} else {
				var ret = a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
			}
			 return ret;
		});

		// loop through the sorted array and set the text/values to the options
		for (var i = 0, l = arrVals.length; i < l; i++) {
			$($options[i]).val(arrVals[i].val).text(arrVals[i].text);
		}

		// set the selected value back
		$dd.val(selectedVal);
	}

}

function redirectAfterActivity(t) {
								var url = document.location.href;
								var a = url.split("&");
								var b = new Array();
								for(x in a) {
									if(a[x]=="" || (a[x].charAt(0)=="r" && a[x].charAt(1)=="[") ) {
										//do nothing
									} else {
										b[x] = a[x];
									}
								}
								url = b.join("&");
								document.location.href = url+"&r[]=ok&r[]="+t;
}