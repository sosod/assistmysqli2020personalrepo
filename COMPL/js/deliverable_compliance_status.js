$(function(){
     
     DeliverableComplianceStatus.getReport();          

});


var DeliverableComplianceStatus    = {

     getReport           : function()
     {
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/ajax-loader.gif' />"})
		     .css({position:"absolute", "z-index":"9999", top:"250px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
	     )               
		$.getJSON("../class/request.php?action=ReportManager.getDeliverableComplianceStatus", function(responseData) {
		    $("#loadingDiv").remove();
			var chart;
			var chartData = [
			 				{country:"USA", year2004:3.5, year2005:4.2},
			 				{country:"UK", year2004:1.7, year2005:3.1},
			 				{country:"Canada", year2004:2.8, year2005:2.9},
			 				{country:"Japan", year2004:2.6, year2005:2.3},
			 				{country:"France", year2004:1.4, year2005:2.1},
			 				{country:"Brazil", year2004:2.6, year2005:4.9},
			 				{country:"Russia", year2004:6.4, year2005:7.2},
			 				{country:"India", year2004:8.0, year2005:7.1},
			 				{country:"China", year2004:9.9, year2005:10.1}
			 			  ];
               chart = new AmCharts.AmSerialChart();
               chart.dataProvider = responseData;
               chart.categoryField = "name";
               chart.marginLeft = 47;
               chart.marginTop = 30;
		     chart.plotAreaBorderAlpha = 0.2;
		     chart.rotate = false;


		     var graph = new AmCharts.AmGraph();
		     graph.title = "Completed Before Deadline";
		     //graph.labelText="[[value]]";
		     graph.balloonText = "[[category]]: [[value]]";
		     graph.valueField = "completedBeforeDeadline";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#000077";
		     graph.stackable = true;
		     chart.addGraph(graph);

		     var graph = new AmCharts.AmGraph();
		     graph.title = "Completed After Deadline";
		     //graph.labelText="[[value]]";
		     graph.balloonText = "[[category]]: [[value]]";
		     graph.valueField = "completedAfterDeadline";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#FE9900";
		     chart.addGraph(graph);

		     var graph = new AmCharts.AmGraph();
		     graph.title = "Not Completed And Overdue";
		     //graph.labelText="[[value]]";
		     graph.balloonText = "[[category]]: [[value]]";
		     graph.valueField = "notCompletedAndOverdue";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#000001";
		     chart.addGraph(graph);

		     var graph = new AmCharts.AmGraph();
		     graph.title = "Not Completed And Not Overdue";
		     //graph.labelText="[[value]]";
		     graph.balloonText = "[[category]]: [[value]]";
		     graph.valueField = "notCompletedAndNotOverdue";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#999999";
		     chart.addGraph(graph);

		     var graph = new AmCharts.AmGraph();
		     graph.title = "Completed On Deadline Date";
		     //graph.labelText="[[value]]";
		     graph.balloonText = "[[category]]: [[value]]";
		     graph.valueField = "completedOnDeadlineDate";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#009900";
		     chart.addGraph(graph);

		     /*var graph = new AmCharts.AmGraph();
		     graph.title = "Africa";
		     graph.labelText="[[value]]";
		     graph.valueField = "africa";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#F4E23B";
		     chart.addGraph(graph);
               */
		     var valAxis = new AmCharts.ValueAxis();
		     valAxis.stackType = "regular";
		     valAxis.gridAlpha = 0.1;
		     valAxis.axisAlpha = 0;
		     chart.addValueAxis(valAxis);

		     var catAxis = chart.categoryAxis;
		     catAxis.gridAlpha = 0.1;
		     catAxis.axisAlpha = 0;
		     catAxis.gridPosition = "middle";
		     catAxis.startOnAxis = false;

		     var legend = new AmCharts.AmLegend();
		     legend.position = "right";
		     legend.borderAlpha = 0.2;
		     legend.horizontalGap = 10;
		     legend.switchType = "v";
		     chart.addLegend(legend);

		     chart.write("chartdiv");
			
		});
     }, 
     


}
