$(function(){
	LegislationNaming.get();
});

LegislationNaming		= {
		
		get			: function()
		{
			$.getJSON("../class/request.php?action=LegislationNaming.getNaming", function( responseData ){
				$.each( responseData, function( index, naming){
					$("#legislation_naming").append($("<tr />")
					    .bind("mouseenter mouseleave", function(){
					       $(this).toggleClass("tdhover");
					    })
					    .append($("<td />",{html:naming.id}))
					    .append($("<td />",{html:naming.ignite_terminology}))
					    .append($("<td />")
					      .append($("<input >",{type:"text", name:"client_"+naming.id, id:"client_"+naming.id, value:naming.client_terminology, size:"50"}))		
					    )
					    .append($("<td />")
					       .append($("<input />",{type:"button", name:"update_"+naming.id, id:"update_"+naming.id, value:"Update"}))  		
					   )
					)
					
					$("#update_"+naming.id).live("click", function(){
						var data 				= {};
						data.id  				= naming.id;
						data.client_terminology = $("#client_"+naming.id).val();
						jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
						$.post("../class/request.php?action=BaseNaming.updateLegislationNaming",{
							data 	: data	
						}, function( response ){
							if( response.error ){
								jsDisplayResult("error", "error", response.text);
							} else {
								jsDisplayResult("ok", "ok", response.text );
							}						
						},"json")
						return false;
					});
				});
			});
		}
		
};
