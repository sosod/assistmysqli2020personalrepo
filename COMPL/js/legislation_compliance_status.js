$(function(){
              
    LegislationComplianceStatus.getFinancialYear(); 
    $("#financialyear").change(function(){
        LegislationComplianceStatus.getReport($(this).val()); 
    }); 
});


var LegislationComplianceStatus    = {

     getFinancialYear      : function()
     {
       $("#financialyear").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financialyear").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 

     getReport           : function(financialyear)
     {                
        document.location.href = "legislation_compliance_status_report.php?financialyear="+financialyear;  
          /*$.get("",{financialyear:financialyear}, function(reportData){
              $("#report").html( reportData );
          },"html");*/
     }, 
     


}

