$.widget("ui.quick", {
	
	options		: {
		start			: 0,
		limit			: 10,
		current			: 1,
		total			: 0,
		userLogged		: "",
		tableId			: "quick_"+(Math.floor(Math.round(45)+45))
	} ,
	
	_init		: function()
	{
		this._getQuickReport();
	} , 
	
	_create		: function()
	{
		$(this.element).append($("<table />",{id:this.options.tableId, width:'100%'}))
	} , 
	
	_getQuickReport			: function()
	{
		var self = this;
		$.getJSON("../class/request.php?action=Report.getQuickReport",{
			start		: self.options.start,			
			limit		: self.options.limit
		}, function( responseData ){
			$("#"+self.options.tableId).html("");
			self._displayPaging( responseData.total )
			self._displayHeaders()			
			if( $.isEmptyObject(responseData.reports ))
			{
				$("#"+self.options.tableId).append($("<tr />")
				   .append($("<td />",{colspan:"8", html:"There are no quick reports"}))		
				)			
			} else {
				self._display( responseData.reports )				
			}
		})
		
		
	} , 
	
	_displayHeaders		: function()
	{
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"Name"}))
			.append($("<th />",{html:"Description"}))
			.append($("<th />",{html:"Display"}))
			.append($("<th />",{html:"Report Type"}))
			.append($("<th />",{html:""}))
			.append($("<th />",{html:""}))
			.append($("<th />",{html:""}))
		)
		
	} ,
	
	_display		: function( data )
	{
		var self = this;
		
		$.each( data , function( index, quick){
     		var reporttype = "";
		     if((quick.status & 4) == 4)
		     {
		        reporttype = "Legislation";
		     } else if((quick.status & 8) == 8){
		        reporttype = "Deliverable";
		     } else if((quick.status & 16) == 16){
		        reporttype = "Action";
		     }		
			var tr = $("<tr />",{id:"tr_"+quick.id});
			tr.append($("<td />",{html:quick.id}))
			tr.append($("<td />",{html:quick.name}))
			tr.append($("<td />",{html:quick.description}))
			tr.append($("<td />",{html:"On Screen"}))
			tr.append($("<td />",{html:"<b>"+reporttype+"<b>"}))
			tr.append($("<td />")
				 .append($("<input />",{type:"button", name:"generate_"+quick.id, id:"generate_"+quick.id, value:"Generate"}))	
			)
			tr.append($("<td />")
		       .append($("<input />",{type:"button", name:"edit_"+quick.id, id:"edit_"+quick.id,
				                        disabled: ($("#userid").val() != quick.insertuser ? "disabled" : ""), value:"Edit"}))	
			)
			tr.append($("<td />")
				.append($("<input />",{type:"button", name:"del_"+quick.id, id:"del_"+quick.id,	value:"Del",
										disabled	: ($("#userid").val() != quick.insertuser ? "disabled" : "")
				}))
			)
			
			$("#generate_"+quick.id).live("click", function(){
			     var reporttype = "";
			     if((quick.status & 4) == 4)
			     {
			       reporttype = "legislation";
			     } else if((quick.status & 8) == 8){
			       reporttype = "deliverable";
			     } else if((quick.status & 16) == 16){
			       reporttype = "action";
			     }
				document.location.href = "generate_report.php?id="+quick.id+"&reporttype="+reporttype;
				return false;
			});
			
			$("#edit_"+quick.id).live("click", function(){
			     var reporttype = "";
			     if((quick.status & 4) == 4)
			     {
			       reporttype = "legislation";
			     } else if((quick.status & 8) == 8){
			       reporttype = "deliverable";
			     } else if((quick.status & 16) == 16){
			       reporttype = "action";
			     }			     
				document.location.href = "edit_"+reporttype+"_report.php?id="+quick.id;
				return false;
			});
			
			$("#del_"+quick.id).live("click", function(){
				if( confirm("Are you sure you want to delete this report"))
				{
					var data    = {};
					data.id     = quick.id;
					data.status = 2;
					$.post("../class/request.php?action=Report.deleteQuickReport",{ data : data }, function(response){
					   if(response.error)
					   {
					      jsDisplayResult("error", "error", response.text);
					   } else {
				           $("#tr_"+quick.id).fadeOut();		
					      jsDisplayResult("ok", "ok", response.text);
					      self._getQuickReport();
					   }   
					},"json")					
				}
				return false
			})
			
			$("#"+self.options.tableId).append( tr );
		});
	} ,
	
	
	_displayPaging 		: function( total ) {
		var self = this;

		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		//$("#"+self.options.tableId)
		$("#"+self.options.tableId)
		  .append($("<tr />")
			.append($("<td />",{colspan:8})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))			   
		   )	
		  )

		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});			
	} ,		
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getQuickReport();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getQuickReport();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getQuickReport();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getQuickReport();				
	} 
});
