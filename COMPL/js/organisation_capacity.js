// JavaScript Document
$(function(){

	OrganisationCapacity.get();
	
	$("#save").click(function(){
		OrganisationCapacity.save();							  
		return false;							  
	});	   
		   
	$("#edit_orgcapacity").click(function(){
		OrganisationCapacity.edit();							  
		return false;							  
	});	   
	
	$("#update_orgcapacity").click(function(){
		OrganisationCapacity.update();							  
		return false;							  
	});	   
	
});

var OrganisationCapacity  	= {
	
	get			: function()
	{
		$.post("../class/request.php?action=OrganisationCapacity.getAll", function( response ){
			$.each( response, function( index, orgcapacity){
				OrganisationCapacity.display( orgcapacity )					   
			})												  											  
		},"json");
	} , 
	
	save		: function()
	{
		var data 		 = {};
		data.name 		 = $("#name").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the organization capacity name");
			return false;
		}  else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientOrganisationCapacity.saveOrganisationCapacity", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					data.id  	= response.id; 	
					data.status = 1; 	
					OrganisationCapacity.display( data )																   
					OrganisationCapacity.empty( data );					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	update		: function()
	{
		var data    = {};  
		data.status = $("#status :selected").val();
		data.id 	= $("#orgcapacityid").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientOrganisationCapacity.updateOrganisationCapacity", { data : data }, function( response ){																																																									 					
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {	
				jsDisplayResult("ok", "ok", response.text);
			}	
		}, "json");
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		data.name 		 = $("#name").val();		
		data.description = $("#description").val();
		data.id			 = $("#orgcapacityid").val();
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the organization capacity name");
			return false;
		}  else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientOrganisationCapacity.updateOrganisationCapacity", { data : data },function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	display		: function( orgcapacity )
	{
		$("#organisation_capacity_table")
		 .append($("<tr />",{id:"tr_"+orgcapacity.id})
		   .append($("<td />",{html:orgcapacity.id}))	   
		   .append($("<td />",{html:orgcapacity.name}))
		   .append($("<td />",{html:orgcapacity.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "edit_"+orgcapacity.id,
				  					id			: (orgcapacity.imported == true ? "edit" : "edit_"+orgcapacity.id),
				  					value		: "Edit"
				  					}))
			  .append($("<input />",{
				  					type		: "button",
				  					name		: "del_"+orgcapacity.id,
				  					id			: (orgcapacity.imported == true ? "del" : "del_"+orgcapacity.id),
				  					value		: "Del"
				  					}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((orgcapacity.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					 type		: "button",
				  					 name		: "change_"+orgcapacity.id,
				  					 id			: (orgcapacity.imported == true ? "change" : "change_"+orgcapacity.id),
				  					 value		: "Change Status"
			  }))			  					
		    )		   
		 )
		 if(orgcapacity.imported == true)
		 {
		     $("input[name='change_"+orgcapacity.id+"']").attr('disabled', 'disabled')
		     $("input[name='edit_"+orgcapacity.id+"']").attr('disabled', 'disabled')
		     $("input[name='del_"+orgcapacity.id+"']").attr('disabled', 'disabled')
		 }	 
		 $("#edit_"+orgcapacity.id).live("click", function(){
			document.location.href = "edit_organisation_capacity.php?id="+orgcapacity.id;								   
			return false;								   
		  });
		 
		 $("#del_"+orgcapacity.id).live("click", function(){
			var data 	= {};
			data.id  	= orgcapacity.id;
			data.status = 2
			if( confirm("Are you sure you want to delete this organisation capacity")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("../class/request.php?action=ClientOrganisationCapacity.updateOrganisationCapacity", { data : data }, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {
						$("#tr_"+orgcapacity.id).fadeOut();	
						jsDisplayResult("ok", "ok", response.text);
					}		
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+orgcapacity.id).live("click", function(){
			document.location.href = "change_organisation_capacity_status.php?id="+orgcapacity.id;										 
		 });	
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	}
	
}
