// JavaScript Document
$(function(){
	if( $("#accountable_id").val() == undefined){
		AccountablePerson.get();	
		//AccountablePerson.getAccountables();	
	}
	
	$("#assign_user").click(function(){
		AccountablePerson.save();
		return false;						  
	});
	
	$("#edit").click(function(){
		AccountablePerson.edit();
		return false;
	});
	
	$("#update").click(function(){
		AccountablePerson.update();
		return false;
	});
	
	$("#add").click(function(){
		
		$("<div />",{id:"newaccountableperson"})
		 .append($("<table />",{width:"100%"})
		   .append($("<tr />")
		     .append($("<th />",{html:"Accountable person name"}))				   
			 .append($("<td />")
			   .append($("<input />",{type:"text", id:"name", name:"name", value:""}))		 
			 )	   
		   )
		  .append($("<tr />")
			.append($("<td />",{colspan:"2"})
			  .append($("<p />")
				.append($("<span />",{id:"message"}))	  
			  )		
			)	  
		  )
		 ).dialog({
			 		autoOpen	: true,
			 		modal		: true,
			 		title		: "Add new accountable person title",
			 		position	: "top",
			 		width		: "auto",
			 		buttons		: {
			 						"Save"	: function()
			 						{
			 							if( $("#name").val() == "")
			 							{
			 								$("#message").html("Please enter the accountable person name");
			 								return false;
			 							} else {
			 								AccountablePerson.savenew();
			 							}
			 						} , 
			 						"Cancel"	: function()
			 						{
			 							$("#newaccountableperson").dialog("destroy");
			 							$("#newaccountableperson").remove();
			 						}
		 						 } ,
		 						 open   : function()
		 						 {
		 						     $(this).css("height", "auto")
		 						 }
		 						 
		 });			
		return false;
	});
	
	$("#accountable_person").change(function(){
		
		$("<div />",{html:"Is this title associated with a user in the list?", id:"hasusers"}).dialog({
			autoOpen	: true, 
			modal		: true,
			position : "top",
			width		: "auto",
			buttons  : {
							"Yes"		: function()
							{
								$("#hasusers").dialog("destroy");
								$("#hasusers").remove();
							} , 
							"No"		: function()
							{
								$("#users").empty()
								$("#users").append($("<option />",{text:$("#accountable_person :selected").text(), value:$("#accountable_person :selected").val()}))
								$("#usernotinlist").val( $("#accountable_person :selected").val() );
								$("#hasusers").dialog("destroy");
								$("#hasusers").remove();							
							}
			} ,
			
			open        : function()
			{
			     $(this).css("height", "auto")
			}
		})
		
	});
	
});

AccountablePerson 		= {
		
	get 				: function()
	{
		$("#accountable_person").empty();
		$("#accountable_person").append($("<option />",{text:"--please select--", value:""}));
		$.post("../class/request.php?action=AccountablePersonManager.getAccPersonsTitles", function( response ){
			$.each(response, function( index, accountable){
			  if(accountable.used)
			  {
			    $("#accountable_person").append($("<option />",{text:accountable.name, value:index, disabled:"disabled"}))
			  } else {
			    $("#accountable_person").append($("<option />",{text:accountable.name, value:index}))
			  }
			})												  											  
		},"json");
		
		$("#users").empty();
		$("#users").append($("<option />",{text:"--please select--", value:""}));		
		$.post("../class/request.php?action=User.getAll", function( response ){
			$.each( response, function( index, owner){
			    $("#users").append($("<option />",{text:owner.user, value:index}))	
			})												  											  
		},"json");		
		
		$(".accountables").remove();
		$.post("../class/request.php?action=AccountablePersonManager.getAll", function(response){
		   if($.isEmptyObject(response))
		   {
		      $("#accountable_table").append($("<tr />").addClass("accountables")
		        .append($("<td />",{colspan:"6", html:"There are no matches between master accountable person title and client accountable persons"}))
		      )
		   } else {
			$.each( response, function( index, accountable){
				AccountablePerson.display( accountable )
			})	
	       }											  											  
		},"json");
	} , 
	
	display		: function( accountable )
	{
		$("#accountable_table")
		 .append($("<tr />",{id:"tr_"+accountable.id}).addClass("accountables")
		   .append($("<td />",{html:accountable.id}))
		   .append($("<td />",{html:accountable.title}))
		   .append($("<td />",{html:accountable.user}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+accountable.id, id:"edit_"+accountable.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+accountable.id, id:"del_"+accountable.id, value:"Del"}))
			)
		   .append($("<td />")
			  .append($("<span />",{html:((accountable.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+accountable.id, id:"change_"+accountable.id, value:"Change Status"}))
		    )		   
		 )
		 		 
		 $("#edit_"+accountable.id).live("click", function(){
			document.location.href = "edit_accountable.php?id="+accountable.id;
			return false;								   
		  });
		 
		 $("#del_"+accountable.id).live("click", function(){
			var data 	= {}
			data.id		= accountable.id;
			data.status = 2;
			if( confirm("Are you sure you want to delete this accountable person")){
				jsDisplayResult("info", "info", "Deleting accountable person . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=AccountablePersonMatch.deleteAccountablePersonMatch", {  data : data }, function( response ){
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else{
						$("#tr_"+accountable.id).fadeOut();	
						jsDisplayResult("ok", "ok", response.text);
					}    
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+accountable.id).live("click", function(){
			document.location.href = "change_accountable_status.php?id="+accountable.id;
		 });
		
	} , 
	
	savenew 		: function()
	{
		$.post("../class/request.php?action=ClientAccountablePerson.savenewAccountablePerson",{
			data : {name:$("#name").val()}
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text );
			} else {
				AccountablePerson.get();
				jsDisplayResult("ok", "ok", response.text);
				$("#newaccountableperson").dialog("destroy");
				$("#newaccountableperson").remove();
			}
			
		},"json")
		
	},
	
	save		: function()
	{
		var data 	  = {};
		data.ref 	  = $("#accountable_person :selected").val();
		data.user_id  = $("#users :selected").val();
		data.usernotinlist = $("#usernotinlist").val();
		jsDisplayResult("info", "info", "Assigning  . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=AccountablePersonMatch.saveAccountablePerson", { data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					AccountablePerson.get();
					jsDisplayResult("ok", "ok", response.text);
				} 															   
			},"json");	
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		data.ref 	  = $("#accountable_person :selected").val();
		data.user_id  = $("#users :selected").val();
		data.id 	  =  $("#accountable_id").val()
		
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please enter the accountable person title . . . ");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please enter the user user . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=AccountablePersonMatch.updateAccountable", { data : data }, function( response ){	
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}  
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var data	= {};
		data.status	= $("#status :selected").val();
		data.id 	= $("#accountable_id").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=AccountablePersonMatch.updateAccountable", {  data : data },
		 function( response ) { 					
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else{
				jsDisplayResult("ok", "ok", response.text);
			} 
		}, "json");
	}
	
	
}
