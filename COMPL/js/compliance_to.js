/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/16/11
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
// JavaScript Document
$(function(){
	
	if( $("#complianceid").val() == undefined){
		//ComplianceTo.getComplianceTo();
		ComplianceTo.get()
	}

	$("#compliance_title").live("change", function(){
		var selectedId = $(this).val();
		var selectedText = $("#compliance_title :selected").text();
		if( selectedId != "")
		{
		     if($("#titlehasUser_"+selectedId).length > 0)
		     {
		       $("#titlehasUser_"+selectedId).remove();
		     }
		
			$("<div />",{id:"titlehasUser_"+selectedId, html:"Is this title associated with a user in the list?"})
			 .append($("<table />",{id:"nouserTable_"+selectedId, width:"100%"}).hide()
			   .append($("<tr />")
				 .append($("<th />",{html:"Please the name for the selected title"}))
				 .append($("<td />")
				   .append($("<textarea />",{cols:"30", rows:"2", name:"titlename", id:"titlename"}))		 
				 )
				 .append($("<td />")
				   .append($("<input />",{type:"button",name:"addnewname", id:"addnewname", value:"Save Name"}))		 
				 )				 
			   )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"3"})
				   .append($("<p />")
					  .append($("<span />",{id:"message"}))	   
				   )		 
				 )	   
			   )
			 ).dialog({
		 	   autoOpen	: true,
		 	   modal	     : true,
		 	   title 	     : "User availability",
		 	   position	: "top",
		 	   width	     : "auto",
		 	   buttons	: {
		 					"Yes"	: function()
		 					{
		 						$("#titlehasUser_"+selectedId).dialog("destroy");
		 						$("#titlehasUser_"+selectedId).remove();
		 					} ,
		 					"No"	: function()
		 					{
		 						$("#users").empty();
		 						$("#users").append($("<option />",{text:$("#compliance_title :selected").text(), value:$("#compliance_title :selected").val()}))
		 						$("#usernotinlist").val( $("#compliance_title :selected").val() )
		 						$("#titlehasUser_"+selectedId).dialog("destroy");
		 						$("#titlehasUser_"+selectedId).remove();
		 					}
	 					  },
			 close              : function(event, ui)
			 {
				$("#titlehasUser_"+selectedId).dialog("destroy");
				$("#titlehasUser_"+selectedId).remove();
			 } ,
			 
			 open               : function()
			 {
			     $(this).css("height", "auto")
			 }					  				 	   
	        });
		}
		
		return false;
	});
	
	$("#assign_user").click(function(){
		ComplianceTo.save();
		return false;
	});

	$("#edit").click(function(){
		ComplianceTo.edit();
		return false;
	});

	$("#update").click(function(){
		ComplianceTo.update();
		return false;
	});
	
	$("#add").click(function(){
		
		$("<div />",{id:"newcomplianceto"})
		.append($("<table />")
		 .append($("<tr />")
		   .append($("<th />",{html:"New compliance to title:"}))
		   .append($("<td />")
			 .append($("<input />",{type:"text", name:"name", id:"name", value:""}))	   
		   )
		 )
		 .append($("<tr />")
		   .append($("<td />",{colspan:"2"})
			 .append($("<p />")
			   .append($("<span />",{id:"message"}))		 
			 )	   
		   )		 
		 )
		).dialog({
				  autoOpen	: true,
				  modal		: true,
				  title 	: "Add new compliance to title",
				  position  : "top",
				  width		: "auto",
				  buttons	: {
								"Save"	: function()
								{
									if( $("#name").val() == "")
									{
										$("#message").html("Please enter the title of compliance to ")
										return false;
									} else {
										ComplianceTo.savenew();										
									}
								} , 
								"Cancel"	: function()
								{
									$("#newcomplianceto").dialog("destroy");
									$("#newcomplianceto").remove();
								}
							} ,
							
							open        : function()
							{
							     $(this).css("height", "auto")
							}
		})
		
		return false;
	});
	
});

var ComplianceTo 	= {
		
	get 		: function()
	{
		$("#compliance_title").empty()
		$("#compliance_title").append($("<option />",{text:"--please select--", value:""}))
		$.post("../class/request.php?action=ComplianceToManager.getComplianceToTitles", function( response ){
			$.each( response, function( index, owner){
			   if(owner.used)
			   {
			     $("#compliance_title").append($("<option />",{text:owner.name, value:index, disabled:"disabled"}))	
			   } else {
			     $("#compliance_title").append($("<option />",{text:owner.name, value:index}))	
			   }
			})												  											  
		},"json");
		
		$("#users").empty()
		$("#users").append($("<option />",{text:"--please select--", value:""}))
		$.post("../class/request.php?action=User.getAll", function( response ){
			$.each( response, function( index, owner){
				$("#users").append($("<option />",{text:owner.user, value:index}))	
			})												  											  
		},"json");
		
		$(".complianceto").remove();
		$.post("../class/request.php?action=ComplianceToManager.getAll", function(response){
		   if($.isEmptyObject(response))
		   {
		     $("#compliance_table").append($("<tr />").addClass("complianceto")
		       .append($("<td />",{colspan:"6", html:"There are no matches found "}))
		     )
		   } else {
			$.each( response, function( index, freq){
				ComplianceTo.display( freq )
			})
	       }
		},"json");		
	} , 
	
	getComplianceTo 		: function()
	{
	}  ,

	savenew				: function()
	{
		$.post("../class/request.php?action=ClientComplianceTo.savenewComplianceTo",{
			data : {name:$("#name").val()}
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text );
			} else {
				ComplianceTo.get();
				$("#newcomplianceto").dialog("destroy");
				$("#newcomplianceto").remove();
				jsDisplayResult("ok", "ok", response.text);
			}
		},"json")
		
	} ,
	
	save		: function()
	{
		var data 	 = {};
		data.ref 	 = $("#compliance_title :selected").val();
		data.user_id = $("#users :selected").val();
		data.usernotinlist  = $("#usernotinlist").val();
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please select the compliance to title");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ComplianceToMatch.saveComplianceTo", { data : data }, function( response ){
  				if( response.error ) {
					jsDisplayResult("error", "error", response.text);		
				} else {
					ComplianceTo.get();					
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}
	} ,

	edit		: function()
	{
		var data 		 = {};
		data.ref 	 = $("#compliance_title :selected").val();
		data.user_id = $("#users :selected").val();
		data.id		 = $("#complianceid").val();

		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please select the compliance to title");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please select the user")
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ComplianceToMatch.updateComplianceTo", { data : data }, function( response ){
  				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}

	} ,

	update		: function()
	{
		var data	= {};
		data.status	= $("#status :selected").val()
		data.id		= $("#complianceid").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ComplianceToMatch.updateComplianceTo", { data : data }, function( response ) {					
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");
	} ,

	display		: function( compliance )
	{
		$("#compliance_table")
		 .append($("<tr />",{id:"tr_"+compliance.id}).addClass("complianceto")
		   .append($("<td />",{html:compliance.id}))
		   .append($("<td />",{html:compliance.title}))
		   .append($("<td />",{html:compliance.user}))
		   .append($("<td />")
			  .append($("<input />",{type : "button", name : "edit_"+compliance.id, id:"edit_"+compliance.id, value : "Edit"}))
			  .append($("<input />",{type : "button", name : "del_"+compliance.id, id : "del_"+compliance.id, value : "Del"}))
			)
		   .append($("<td />")
			  .append($("<span />",{html:((compliance.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			)
		   .append($("<td />")
			  .append($("<input />",{type	: "button", name : "change_"+compliance.id, id : "change_"+compliance.id, value	: "Change Status" }))
		    )
		 )

		 $("#edit_"+compliance.id).live("click", function(){
			document.location.href = "edit_compliance_to.php?id="+compliance.id;
			return false;
		  });

		 $("#del_"+compliance.id).live("click", function(){
			var data    = {};
			data.id     = compliance.id;
			data.status = 2;
			if( confirm("Are you sure you want to delete this compliance to")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ComplianceToMatch.deleteDeliverableComplianceTo",{ data : data }, function( response ){	
      				if( response.error ){
    					jsDisplayResult("error", "error", response.text);		
    				} else {
    					$("#tr_"+compliance.id).fadeOut();
    					jsDisplayResult("ok", "ok", response.text);
    				}
				}, "json");
			}
			return false;
		  });

		 $("#change_"+compliance.id).live("click", function(){
			document.location.href = "change_compliance_to.php?id="+compliance.id;
		 });
	} ,


	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");
		});
	}



}
