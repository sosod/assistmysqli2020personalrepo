$(function(){

     ReportingCategories.get();

    
    
     $("#addnew").click(function(e){
         if($("#addreportingcategory").length)
         {
            $("#addreportingcategory").remove();   
         } 
           
         $("<div />",{id:"addreportingcategory"}) 
          .append($("<table />", {width:"100%"})
            .append($("<tr />")
              .append($("<th />",{html:"Reporting Category Name:"}))
              .append($("<td />")
                 .append($("<input />",{type:"text", name:"name", id:"name", value:"", size:"50"}))
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Reporting Category Description:"}))
              .append($("<td />")
                 .append($("<textarea />",{name:"description", id:"description", cols:"35", rows:"7"}))
              )
            )
            .append($("<tr />")
              .append($("<td />",{colspan:"2"})
                .append($("<span />",{id:"message"}).css({"padding":"5px"}))
              )
            ) 
          ).dialog({
                    autoOpen	  : true, 
                    modal	  : true, 
                    width	  : "auto",
                    position	  : "top",
                    title       : "Add New Reporting Category",
                    buttons        : {
                                        "Save"    : function()
                                        {
                                             if($("#name").val() == "")
                                             {
                                                $("#message").addClass("ui-state-error").html("Please enter the name of the reporting category");
                                                return false;    
                                             } else if($("#description").val() == ""){
                                                $("#message").addClass("ui-state-error").html("Please enter the description of the reporting category");
                                                return false;
                                             } else {
                                                  ReportingCategories.save();
                                             }
                                        } , 
                                        
                                        "Cancel"  : function()
                                        {
                                              $("#addreportingcategory").remove(); 
                                              $("#addreportingcategory").dialog("destroy");
                                        }
                     } ,
                    open           : function(event, ui)
                    {
	     				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	     				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	     				var saveButton = buttons[0];
	     				$(saveButton).css({"color":"#090"});
	     				 $(this).css("height", "auto")
                    } ,
                    close          : function(event, ui)
                    {
                          $("#addreportingcategory").remove(); 
                          $("#addreportingcategory").dialog("destroy");
                    }          
          })
        e.preventDefault();
     });
     
});

var ReportingCategories  = {

     get       : function()
     {
          $(".categories").remove();
          $.getJSON("../class/request.php?action=ReportingCategories.getReportingCategories", function(categories) {
               if(!$.isEmptyObject(categories))
               {
                    $.each(categories, function(index , category){
                         ReportingCategories.display(category);
                    });
               }          
          });
     
     } , 
     
     display        :   function(category)
     {
         $("#categories_table").append($("<tr />").addClass("categories")
            .append($("<td />",{html:category.id}))
            .append($("<td />",{html:category.name}))
            .append($("<td />",{html:category.description}))
            .append($("<td />",{html:((category.status & 1) == 1 ? "<b>Active</b>" : "<b>InActive</b>")}))
            .append($("<td />")
              .append($("<input />",{type:"button", name:"edit_"+category.id, id:"edit_"+category.id, value:"Edit"}))
            )
         )    
         
         $("#edit_"+category.id).live("click", function(e){
           
                if($("#edit_"+category.id+"_dialog").length > 0)
                {
                   $("#edit_"+category.id+"_dialog").remove(); 
                }
                
              $("<div />",{id:"edit_"+category.id+"_dialog"}) 
               .append($("<table />")
                 .append($("<tr />")
                   .append($("<th />",{html:"Reporting Category Name:"}))
                   .append($("<td />")
                      .append($("<input />",{type:"text", name:"name", id:"name", value:category.name, size:"50"}))
                   )
                 )
                 .append($("<tr />")
                   .append($("<th />",{html:"Reporting Category Description:"}))
                   .append($("<td />")
                      .append($("<textarea />",{name:"description", id:"description", cols:"50", rows:"7", text:category.description}))
                   )
                 )
                 .append($("<tr />")
                   .append($("<td />",{colspan:"2"})
                     .append($("<span />",{id:"message"}).css({"padding":"5px"}))
                   )
                 ) 
               ).dialog({
                         autoOpen	  : true, 
                         modal	  : true, 
                         width	  : "auto",
                         position	  : "top",
                         buttons        : {
                                             "Save Changes"    : function()
                                             {
                                                  if($("#name").val() == "")
                                                  {
                                                     $("#message").addClass("ui-state-error").html("Please enter the name of the reporting category");
                                                     return false;    
                                                  } else if($("#description").val() == ""){
                                                     $("#message").addClass("ui-state-error").html("Please enter the description of the reporting category");
                                                     return false;
                                                  } else {
                                                       ReportingCategories.update(category.id);
                                                  }
                                             } , 
                                             
                                             "Cancel"             : function()
                                             {
                                                  $("#edit_"+category.id+"_dialog").remove(); 
                                                  $("#edit_"+category.id+"_dialog").dialog("destroy");
                                             } ,
                                             
                                             "Deactivate"        : function()
                                             {
                                                ReportingCategories.updateStatus(category.id, 0);                                
                                             } , 
                                             "Activate"          : function()
                                             {
                                                ReportingCategories.updateStatus(category.id, 1);                                
                                             } , 
                                             
                                             "Delete"            : function()
                                             {
                                                ReportingCategories.updateStatus(category.id, 2);
                                             }
                          } ,
                         open           : function(event, ui)
                         {
            		     				var displayDelete     = true;
            		     				var displayDeactivate = false;
            		     				var displayActivate   = false; 
            		     				//if the status is for the default , then dont display delete
            		     				if( (category.status & 1) == 1)
            		     				{
            		     					displayDeactivate = false;
            		     				} else {
            		     					displayActivate = false;
            		     				}
            		     				
            		     				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
            		     				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
            		     				var saveButton = buttons[0];
            		     				var deactivateButton = buttons[2];
            		     				var activateButton   = buttons[3];
            		     				var deleteButton     = buttons[4];
            		     				$(saveButton).css({"color":"#090"});
            		     				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
            		     				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
            		     				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")});   
            		     				 $(this).css("height", "auto")
                         } ,
                         close          : function(event, ui)
                         {
                              $("#edit_"+category.id+"_dialog").remove(); 
                              $("#edit_"+category.id+"_dialog").dialog("destroy");
                         }          
               })
           })
     
     },
     
     save           : function(categoryData)
     {
        $.post("../class/request.php?action=ReportingCategories.saveCategory",{
          data  : {
                    name      : $("#name").val(),
                    description : $("#description").val()
          }
        }, function(response){
               if(response.error)
               {
                    $("#message").addClass("ui-state-error").html( response.text );
               } else {
                    jsDisplayResult("ok", "ok", response.text );			
                    ReportingCategories.get();
                    $("#addreportingcategory").remove(); 
                    $("#addreportingcategory").dialog("destroy");               
               }
        
        },"json");
     
     } ,
     
     update         : function(id)
     {
           $.post("../class/request.php?action=ReportingCategories.updateCategory",{
               data  :{
                         name        : $("#name").val(),
                         description : $("#description").val(),
                         id : id 
               }
           },function(response){
               if(response.error)
               {
                   $("#message").addClass("ui-state-error").html( response.text );        
               } else {
                  if(response.updated)
                  {
                    jsDisplayResult("info", "info", response.text );
                  } else {
                    jsDisplayResult("ok", "ok", response.text );
                  }
                  ReportingCategories.get();
                  $("#edit_"+id+"_dialog").remove(); 
                  $("#edit_"+id+"_dialog").dialog("destroy");                    
               }
           },"json");
     } ,
     
     updateStatus        : function(id, status)
     {
           $.post("../class/request.php?action=ReportingCategories.updateCategory",{
               data : {status : status, id : id }
           },function(response){
               if(response.error)
               {
                   $("#message").addClass("ui-state-error").html( response.text );        
               } else {
                  if(response.updated)
                  {
                    jsDisplayResult("info", "info", response.text );
                  } else {
                    jsDisplayResult("ok", "ok", response.text );
                  }                  
                  ReportingCategories.get();
                  $("#edit_"+id+"_dialog").remove(); 
                  $("#edit_"+id+"_dialog").dialog("destroy");                    
               }
           },"json");          
     }
     
     
}
