/**
 * Javascript
 */
$(function(){
	if( $("#ownerid").val() == undefined){
		ActionOwner.get();	
		//ActionOwner.getActionOwners();
	}
	
	$("#assign_user").click(function(){
		ActionOwner.save();
		return false;						  
	});
	
	$("#edit").click(function(){
		ActionOwner.edit();
		return false;
	});
	
	$("#update").click(function(){
		ActionOwner.update();
		return false;
	});
	
	$("#add").click(function(){
		
		$("<div />",{id:"newactionowner"})
		 .append($("<table />")
		  .append($("<tr />")
			.append($("<th />",{html:"Actio Owner Title name:"}))
			.append($("<td />")
			  .append($("<input />",{type:"text", name:"name", id:"name", value:""}))		
			)
		  )
		  .append($("<tr />")
			.append($("<td />",{colspan:"2"})
			  .append($("<p />")
				.append($("<span />",{id:"message"}))	  
			  )		
			)	  
		  )
		).dialog({
				 autoOpen	: true, 
				 modal		: true,
				 titile 		: "Add new action owner title",
				 position 	: "top",
				 width		: "auto",
				 buttons 	: {
								 "Save"	: function()
								 {
									if( $("#name").val() == "")
									{
										$("#message").html("Please enter the action owner title name")
										return false;
									} else {
										ActionOwner.savenew();										
									}
								 } , 
								 "Cancel"	: function()
								 {
									 $("#newactionowner").dialog("destroy");
									 $("#newactionowner").remove();
								 }
							  } ,
							  
			    open           : function()
			    {
			         $(this).css("height", "auto")
			    }			
		})
		
		return false;
	});
	
	$("#owners").change(function(){
		$("<div />",{html:"Is this title associated with a user in the list?", id:"hasusers"}).dialog({
			autoOpen : true, 
			modal 	: true, 
			position : "top",
			width		: "auto",
			buttons 	: {
							"Yes"	: function()
							{
								$("#hasusers").dialog("destroy");
								$("#hasusers").remove();
							} , 
							"No"	: function()
							{
								$("#users").empty();
								$("#users").append($("<option />",{text:$("#owners :selected").text(), value:$("#owners :selected").val()}))
								$("#usernotinlist").val( $("#owners :selected").val() );								
								$("#hasusers").dialog("destroy");
								$("#hasusers").remove();							
							}	
			}, 
			open            : function()
			{
			     $(this).css("height", "auto")
			}	
		})
		return false;
	});
	
});

ActionOwner 		= {
	get 		: function()
	{
		$("#owners").empty();
		$("#owners").append($("<option />",{text:"--please select--", value:""}));
		$.post("../class/request.php?action=ActionOwnerManager.getActionOwnerTitles", function(response) {
			$.each( response, function( index, owner){
			     if(owner.used)
			     {  
			        $("#owners").append($("<option />",{text:owner.name, value:index, disabled:"disabled"}))
			     } else {
			         $("#owners").append($("<option />",{text:owner.name, value:index}))	
			     }
			})												  											  
		},"json");
		
		$("#users").empty();		
		$("#users").append($("<option />",{text:"--please select--", value:""}));
		$.post("../class/request.php?action=User.getAll", function( response ){
			$.each( response, function( index, owner){
				$("#users").append($("<option />",{text:owner.user, value:index}))	
			})												  											  
		},"json");	
		
		$(".actionowners").remove();
		$.post("../class/request.php?action=ActionOwnerManager.getAll", function(response){
		  if($.isEmptyObject(response))
		  {
		     $("#action_owner_table").append($("<tr />").addClass("actionowners")
		       .append($("<td />",{html:"There are no action owner matches", colspan:"6"}))
		     ) 
		  } else {
			$.each( response, function( index, actionowner){
				ActionOwner.display( actionowner )
			})	
	       }											  											  
		},"json");		
	} , 
	
	getActionOwners 		: function()
	{
	} , 
	
	display		: function( actionowner )
	{
		$("#action_owner_table")
		 .append($("<tr />",{id:"tr_"+actionowner.id}).addClass("actionowners")
		   .append($("<td />",{html:actionowner.id}))
		   .append($("<td />",{html:actionowner.title}))
		   .append($("<td />",{html:actionowner.user}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+actionowner.id, id:"edit_"+actionowner.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+actionowner.id, id:"del_"+actionowner.id, value:"Del"}))
			)
		   .append($("<td />")
			   .append($("<span />",{html:((actionowner.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+actionowner.id, id:"change_"+actionowner.id, value:"Change Status"}))
		    )		   
		 )
		 		 
		 $("#edit_"+actionowner.id).live("click", function(){
			document.location.href = "edit_action_owner.php?id="+actionowner.id;
			return false;								   
		  });
		 
		 $("#del_"+actionowner.id).live("click", function(){
			 var data 	 = {};
			 data.status = 2;
			 data.id     = actionowner.id;
			if( confirm("Are you sure you want to delete this action owner")){
				jsDisplayResult("info", "info", "Deleting action owner . . . <img src='../images/loaderA32.gif' />");				
				$.post("../class/request.php?action=ActionOwnerMatch.deleteActionOwner", { data : data }, function( response ){
					if( response.error )
					{
					   jsDisplayResult("error", "error", response.text);
					} else {
					   ActionOwner.get();
				        $("#tr_"+actionowner.id).fadeOut();		
					   jsDisplayResult("ok", "ok", "Action owner deleted . . .");
					}    
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+actionowner.id).live("click", function(){
			document.location.href = "change_action_owner_status.php?id="+actionowner.id;
		 });
		
	} , 
	
	savenew		: function()
	{
		$.post("../class/request.php?action=ClientActionOwner.savenewActionOwner",{
			data : { name : $("#name").val() }
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				ActionOwner.get();
				jsDisplayResult("ok", "ok", response.text );
				 $("#newactionowner").dialog("destroy");
				 $("#newactionowner").remove();
			}	 		
		},"json")
		
	} ,
	
	save		: function()
	{
		var data 	  = {};
		data.ref 	  = $("#owners :selected").val();
		data.user_id  = $("#users :selected").val();
		data.usernotinlist = $("#usernotinlist").val();
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please select the owner");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please select the user");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ActionOwnerMatch.saveActionOwner", { data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{																	   
					ActionOwner.get();
					jsDisplayResult("ok", "ok", response.text);
				} 																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		data.ref 	  = $("#owner :selected").val();
		data.user_id  = $("#users :selected").val();
		data.id		  = $("#ownerid").val();
		
		if( data.ref == ""){
			jsDisplayResult("error", "error", "Please select the owner");
			return false;
		} else if( data.user_id == ""){
			jsDisplayResult("error", "error", "Please select the user");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ActionOwnerMatch.updateActionOwner", { data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{		
					jsDisplayResult("ok", "ok", response.text);
				} 																   
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var data	 = {};
		data.status	 = $("#status :selected").val();
		data.id 	 = $("#ownerid").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ActionOwnerMatch.updateActionOwner", { data : data }, function( response ) {
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{		
					jsDisplayResult("ok", "ok", response.text);
				} 
		}, "json");
	}
	
	
}
