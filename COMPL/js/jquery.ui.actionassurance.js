$.widget("ui.actionassurance",{
	
	options		: {
		start		: 0,
		limit		: 10,
		current		: 1,
		total		: 0,
		toDay		: "",
		id			: 0
	} ,
	
	_init		: function()
	{
		this._getActionAssurance();
	} , 
	
	_create			: function()
	{
		var month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
				   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
				   11 : "Nov", 12 : "Dec"
				};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
			min = "0"+min;				
		} else{
			min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min;	 
		
	} , 
	
	_getActionAssurance		: function()
	{
		var self = this;
		$.getJSON("../class/request.php?action=AssuranceAction.getActionAssurance",{
			start		: self.options.start,
			limit		: self.options.limit,
			actionId	: self.options.id
		}, function( responseData ){
			$(self.element).html("");
			$(self.element).parent().children("h2").remove();
			//$(self.element).append($("<tr />")
			//    .append($("<td />",{colspan:"8", html:"<h4>Action Assurance</h4>"}))		
			//)
			$(self.element).parent().prepend($("<h2 />",{html:"Action Assurance"}));
			self._displayPaging( responseData.total );
			self._displayHeaders();
			//alert(responseData.total);
			if( $.isEmptyObject(responseData.data))
			{
				$(self.element).append($("<tr />")
				   .append($("<td />",{colspan:"8", html:"There are no action assurances"}))		
				)				
			} else {
				self._display( responseData.data )
			}
			self._addNew()
		})
		
		
	} ,
	
	_display		: function( actions )
	{
		var self = this;
		$.each( actions, function( index , action){
			var tr = $("<tr />",{id:"tr_assurance_"+action.id})
			$.each( action, function( key , val){
				if( key == "insertdate"){
					tr.append($("<td />",{html:self.options.toDay}))
				} else {
					tr.append($("<td />",{html:(key == "sign_off" ? (val == 1 ? "Yes" : "No") : val )}))
				}
			});
			
			tr.append($("<td />")
			     .append($("<input />",{type:"button", name:"edit_"+index, id:"edit_"+index, value:"Edit"}))
			     .append("&nbsp;&nbsp;")
			     .append($("<input />",{type:"button", name:"del_"+index, id:"del_"+index, value:"Del"}))
			)
		
			$("#del_"+index).live("click", function() {
				if(confirm("Are you sure you want to delete this action assurance?"))
				{
					jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
					/*$.post("../class/request.php?action=AssuranceAction.updateActionAssurance", { data : { id : action.id, status : 2, action_id: action.action_id} }, function( response ){
						if(response.error) {
							jsDisplayResult("error", "error", response.text);
						} else {
							$("#tr_assurance_"+action.id).fadeOut();
							jsDisplayResult("ok", "ok", response.text);
						}						
					},"json")					*/
					var result = AssistHelper.doAjax("../common/common_controller.php?action=updateActionAssurance",{ id : action.id, status : 2, action_id : action.action_id});
						if( result.error ){
							jsDisplayResult("info", "info", result.text);
						} else {
							$("#tr_assurance_"+deliverable.id).fadeOut();
							jsDisplayResult("ok", "ok", result.text);
						}						
					
				}
				return false;
			});
			
			$("#edit_"+index).live("click", function(){
				if($("#edit_new_assurance_"+index).length > 0)
				{
					$("#edit_new_assurance_"+index).remove();
				}	
					
				$("<div />",{id:"edit_new_assurance_"+index})
					 .append($("<table />",{class:"form"})
					    .append($("<tr />")
					       .append($("<th />",{html:"Response:"}))
					       .append($("<td />")
					    	  .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7", text:action.response}))	   
					       )
					    )
					   .append($("<tr />")
						  .append($("<th />",{html:"Sign Off:"}))
						  .append($("<td />")
							 .append($("<select />",{id:"signoff", name:"signoff"})
							   .append($("<option />",{text:"No", value:"0", selected:(action.sign_off == 0 ?  "selected" : "")}))
							   .append($("<option />",{text:"Yes", value:"1", selected:(action.sign_off == 1 ?  "selected" : "")}))
							 )	  
						  )
					   )
					   .append($("<tr />")
						 .append($("<th />",{html:"Date Tested:"}))
						 .append($("<td />")
							.append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:action.date_tested, readonly:"readonly"}).addClass("datepicker"))	 
						  )
					   )
					 )
					 .dialog({
						 	  modal		: true, 
						 	  autoOpen	: true, 
						 	  title		: "Edit Action Assurance",
						 	  position	: "top",
							  width		: "500px",
						 	  buttons	: {
						 					"Save Changes" 	: function() {
						 						if( $("#response").val() == "") {
						 							jsDisplayResult("error", "error", "Please enter the response.");
						 							return false;
						 						} else {
						 							jsDisplayResult("info", "info", "Updating... <img src='../images/loaderA32.gif' />");
						 							$.post("../class/request.php?action=AssuranceAction.updateActionAssurance",{
							 							data : { id : action.id, action_id: self.options.id, response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val() } },
						 							function(response) {
							 							if(response.error) {
							 								jsDisplayResult("error", "error", response.text);
							 							} else {
							 								self._getActionAssurance()
							 								jsDisplayResult("ok", "ok", response.text);
							 							}
						 							},"json")
						 							$("#edit_new_assurance_"+index).dialog("destroy")
													$("#edit_new_assurance_"+index).remove()
						 						}
						 					}, 
						 					"Cancel"		: function() {
						 						$("#edit_new_assurance_"+index).dialog("destroy");
												$("#edit_new_assurance_"+index).remove();
						 					}
					 					}
					 });
					$(".datepicker").datepicker({
				        showOn: 'both',
				        buttonImage: '/library/jquery/css/calendar.gif',
				        buttonImageOnly: true,
				        dateFormat: 'dd-M-yy',
				        changeMonth:true,
				        changeYear:true		
				    });
													
					return false;			
				
			});
			
			$(self.element).append( tr )
		});
		 
	} , 
	
	_addNew				: function()
	{
		var self = this;
		$(self.element).append($("<tr />")
		   .append($("<td />",{colspan:"8"})
			  .append($("<input />",{type:"button", name:"add_new", id:"add_new", value:"Add New"}))	   
		   )		
		)
		
		$("#add_new").live("click", function(){
			if($("#add_new_assurance").length > 0)
			{
				$("#add_new_assurance").remove();
			}
						
			$("<div />",{id:"add_new_assurance"})
			 .append($("<table />",{class:"form"})
			    .append($("<tr />")
			       .append($("<th />",{html:"Response:"}))
			       .append($("<td />")
			    	  .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7"}))	   
			       )
			    )
			   .append($("<tr />")
				  .append($("<th />",{html:"Sign Off:"}))
				  .append($("<td />")
					 .append($("<select />",{id:"signoff", name:"signoff"})
					   .append($("<option />",{text:"No", value:"0"}))
					   .append($("<option />",{text:"Yes", value:"1"}))
					 )	  
				  )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Date Tested:"}))
				 .append($("<td />")
					.append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly"}).addClass("datepicker"))	 
				  )
			   )
			 )
			 .dialog({
				 	  modal		: true, 
				 	  autoOpen	: true, 
				 	  title		: "Add Action Assurance",
				 	  position 	: "top",
					  width		: "500px",
				 	  buttons	: {
				 					"Save" 			: function()
				 					{
				 						
				 						if( $("#response").val() == "")
				 						{
				 							jsDisplayResult("error", "error", "Plese enter the response");
				 							return false;
				 						} else {
				 							jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
				 							$.post("../class/request.php?action=AssuranceAction.saveActionAssurance",{
					 							data : {response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val(),action_id : self.options.id } },
				 							function( response ){
					 							if(response.error){
					 								jsDisplayResult("error", "error", response.text);
					 							} else {
					 								self._getActionAssurance()
					 								jsDisplayResult("ok", "ok", response.text);
					 							}				 								
				 							},"json")
				 							$("#add_new_assurance").dialog("destroy")
											$("#add_new_assurance").remove()
				 						}
				 						
				 					} , 
				 					
				 					"Cancel"		: function()
				 					{
				 						$("#add_new_assurance").dialog("destroy")
										$("#add_new_assurance").remove()
				 					}
			 					  }
			 });
			$(".datepicker").datepicker({
		        showOn: 'both',
		        buttonImage: '/library/jquery/css/calendar.gif',
		        buttonImageOnly: true,
		        dateFormat: 'dd-M-yy',
		        changeMonth:true,
		        changeYear:true		
		    });
							
			return false;
		});				
	} ,
	
	_displayHeaders		: function()
	{
		var self = this;
		$(self.element).append($("<tr />")
		   .append($("<th />",{html:"Ref"}))
		   .append($("<th />",{html:"System date and time"}))
		   .append($("<th />",{html:"Date Tested"}))
		   .append($("<th />",{html:"Response"}))
		   .append($("<th />",{html:"Sign Off"}))
		   .append($("<th />",{html:"Assurance By"}))
		   .append($("<th />",{html:"&nbsp;"}))
		)		
	} ,
	
	_displayPaging 		: function( total ) {
		var self = this;
		
		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		//$("#"+self.options.tableId)
		$(self.element)
		  .append($("<tr />")
			.append($("<td />",{colspan:8})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < ", disabled:(self.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| ", disabled:((self.options.current==pages || pages == 0) ? 'disabled' : '' )}))			   
		   )
		  )

		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});			
	} ,		
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getActionAssurance();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getActionAssurance();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getActionAssurance();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getActionAssurance();				
	}	
	
	
});
