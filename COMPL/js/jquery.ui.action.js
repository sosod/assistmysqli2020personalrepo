$.widget("ui.action",{
	
	options		: {
		lowerLimit            : 0,
		upperLimit	          : 10,
		currentPage           : 1,
		totalActions	      : 0,
		editAction	          : false,
		updateAction	      : false,
		reassignAction        : false,
		assuranceAction       : false,
		viewAction			  : false,
		tableId		          : "action_"+(Math.floor(Math.random(78) * 45)),
		deliverableId	      : 0,
		page			      : "",
		actionStatus	      : [], 
		section		          : "",
		thClass		          : "",
		eventActivation       : false, 
		occuranceid	          : 0,
		copyAction	          : false, 
		url			          : "../class/request.php?action=Action.getActions",
		autoLoad              : true,
		user                  : "",  
		filterByFinancialYear : false,
		filterByLegislation   : false,
		filterByDeliverable   : false,
		legislationId         : 0,
		financialyear		  : 0,
		searchtext            : "",
		searchAction          : false,
		view                  : "",
		actionOwners          : {},
		user_logged           : {},
		delTotalActionsDisplayID:"",
		page_src			  : new Array()
	} ,
	
	_init		: function()
	{
	  if(this.options.autoLoad)
	  {
	    this._getAction();
	  }
	} , 
	
	_writeArray : function() {
		var self = this;
		a = self.options.page_src;
		var st = "";
		for(x in a) {
			st+=";"+x+"-"+a[x];
		}
		$("#page_src").val(st);
		return st;
	},
	
	_create		: function()
	{
       var self    = this,
           options = self.options,
           tableId = options.tableId,
           element = self.element;
           $(element).append($("<table />",{width:"100%"}).addClass("noborder")
              .append($("<tr />").css({"display":(options.filterByFinancialYear ? "table-row" : "none"), "background-color":"#EEEEEE"})
                .append($("<th />",{html:"Filter by financial year :"}).css({"width":"250px", "text-align":"left"}))
                .append($("<td />").addClass("noborder")
                  .append($("<select />",{id:"financialyear", name:"financialyear"}).addClass('financialyear'))
                )
              )    
              .append($("<tr />").css({"display":(options.filterByLegislation ? "table-row" : "none"), "background-color":"#EEEEEE"})
                .append($("<th />",{html:"Filter by legislation :"}).css({"width":"250px", "text-align":"left"}))
                .append($("<td />").addClass("noborder")
                  .append($("<select />",{id:"legislation_filter", name:"legislation_filter"}))
                )
              )       
              .append($("<tr />").css({"display":(options.filterByDeliverable ? "table-row" : "none"), "background-color":"#EEEEEE"})
                .append($("<th />",{html:"Filter by deliverable :"}).css({"width":"250px", "text-align":"left"}))
                .append($("<td />").addClass("noborder")
                  .append($("<select />",{id:"deliverable_filter", name:"deliverable_filter"}))
                )
              )
              .append($("<tr />").css({"display":"none"}).addClass("descriptions")
                .append($("<td />",{id:"deliverableDescription", colspan:"2"}))
              )       
              .append($("<tr />").css({"display":(options.reassignAction ? "table-row" : "none"), "background-color":"#EEEEEE"})
                .append($("<th />",{html:"Select user"}).css({"width":"250px", "text-align":"left"}))
                .append($("<td />").addClass("noborder")
                  .append($("<select />",{id:"user", name:"user"}))
                )
              )       
              .append($("<tr />")
                .append($("<td />",{colspan:"2"}).addClass("noborder")
                  .append($("<table />",{id:tableId, width:"100%"}))
                )
              )                                  
            )           

       self._financialYears();     
		if(self.options.financialyear>0) {
			self._loadLegislations(self.options.financialyear);
		}
		if(self.options.legislationId>0) {
			self._loadDeliverables(self.options.legislationId);
		}
		if(self.options.financialyear>0) {
			self._getAction();
		}
       
       if(options.reassignAction)
       {
          self._loadUsers();
       }
       
	  $("#financialyear").live("change", function(e){
          $("#message").hide();   	     
	     self.options.financialyear = $(this).val();
		 self.options.page_src['finYear'] = self.options.financialyear;
	     if(options.reassignAction)
	     {
	        if(options.user !== "")
	        {
		     options.lowerLimit  = 0;
		     options.upperLimit	= 10;
		     options.currentPage = 1;
		     options.total       = 0;              
	        }
	     } else {
	       $("#legislation_filter").empty();
	       $("#deliverable_filter").empty();
            self._loadLegislations($(this).val());
	       e.preventDefault();	     
	     }
          self._getAction();	
	  });       
       
       $("#legislation_filter").live("change", function(e){
          self._loadDeliverables($(this).val());      
          self.options.legislationId = $(this).val();
		 self.options.page_src['legId'] = self.options.legislationId;
          self.options.deliverableId = 0; 
          $("#deliverableDescription").html("").hide();
          self._getAction();	   
          e.preventDefault();
       });
       
       $("#deliverable_filter").live("change", function(){
		options.lowerLimit  = 0;
		options.upperLimit	= 10;
		options.currentPage = 1;       
		options.total       = 0;
          self.options.deliverableId = $(this).val();
		 self.options.page_src['delId'] = self.options.deliveraleId;
          $("#message").hide();
          var longDescription = $("select#deliverable_filter :selected").attr("title");
          $(".descriptions").show();
          $("#deliverableDescription").addClass("ui-state-highlight").show().html(longDescription);
         self._getAction();       
       });
       
       $("#user").live("change",function(){
		options.lowerLimit  = 0;
		options.upperLimit	= 10;
		options.currentPage = 1;
		options.total       = 0;       
          self.options.user = $(this).val();
          $("#message").hide();
          self._getAction();
       });
	} ,
	
     
     _financialYears     : function()
     {
	   var self = this;
	   if($.isEmptyObject(self.options.financialYearsList))
	   {
	        $(".financialyear").append($("<option />",{value:"", text:"--please select--"}));
	        $.getJSON("../class/request.php?action=FinancialYear.fetchAllSorted",function(financialYears) {
	          if($.isEmptyObject(financialYears))
	          {
	              $(".financialyear").append($("<option />",{value:"", text:"--please select--"}))
	          } else {
	             self.options.financialYearsList  = financialYears;
	             $.each(financialYears, function(index, financialYear){
					if(self.options.financialyear == financialYear.id) {
						$(".financialyear").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date,selected:"selected"}))
					} else {
						$(".financialyear").append($("<option />",{value:financialYear.id, text:" ("+financialYear.value+") "+financialYear.start_date+" - "+financialYear.end_date}))
					}
	               //$(".financialyear").attr('selected', (self.options.financialyear == financialYear.id ? "selected" : ""))
	             });
	          }	   
	        });
	   } 
     } ,	
	
	_loadLegislations   : function(financialyear)
	{
	  var self = this;
	  var legislations = $("body").data("legislations");
	  //if(!$.isEmptyObject(legislations))
	  //{
	     //self._loadSelect("legislation_filter", legislations, "name");
	  //} else {
	     $.getJSON("../class/request.php?action=Legislation.getLegislations",{
	        data  : { financial_year:financialyear, section:self.options.section, page:self.options.page, type:"action"}
	      }, 
	      function(legislations){  
	        if(!$.isEmptyObject(legislations))
	        {
	           $("body").data("legislations", legislations);
                self._loadSelect("legislation_filter", legislations, "name");
	        }
	     });
	  //} 
	}, 	
	
	_loadDeliverables    : function(legislationId)
	{
	   var self = this;
	   //var legDeliverable = $("body").data("deliverables");
	   $("#deliverable_filter").empty();
        $.getJSON("../class/request.php?action=Deliverable.getDeliverablesList", 
           {data : {legislation_id:legislationId}}, function(deliverables){
             if(!$.isEmptyObject(deliverables))
             {
               //legislationDeliverables = {legislationId : deliverables}; 
               //console.log( deliverables );
               //$("body").data("deliverables", legislationDeliverables);
               self._loadSelect("deliverable_filter", deliverables, "shortdescription", {title:"short_description"});
             }
        });
	},
	
	_loadUsers           : function()
	{
	   var self  = this;
	   var users = $("body").data("users");
	   if(!$.isEmptyObject(users))
	   {
	     self._loadSelect("user", users, "user");
	   } else {
	     $.getJSON("../class/request.php?action=User.getAll", function(users){
	       if(!$.isEmptyObject(users))
	       {
	          $("body").data("users", users);
	          self._loadSelect("user", users, "user");
	       }
	     });	     
	   }
	} ,
	
	_loadSelect          : function(selectBox, data, key, options)
	{
	   var self = this;
	   $("#"+selectBox).append($("<option />",{value:"", text:"--please select--"}))
	   $.each(data, function(index, val){
	     var optionKeyVal = {};
		 var sel = false;
	     if(!$.isEmptyObject(options))
	     {
	        $.each(options, function(optionIndex, optionKey){
	          if(val.hasOwnProperty(optionKey))
	          {
	             optionKeyVal[optionIndex] = val[optionKey];
	          }
	        });   
	     }	   
	     var text_str = val[key]; 
	     if(selectBox == "deliverable_filter")
	     {
	        text_str = "[D"+val.id+"] "+text_str;
			if(self.options.deliverableId==val.id) {
				sel = true;
			}
	     }
	     if(selectBox == "legislation_filter")
	     {
	        text_str = "[L"+val.id+"] "+text_str;
			if(self.options.legislationId==val.id) {
				sel = true;
			}
	     }
		 if(sel) {
			$("#"+selectBox).append($("<option />",{value:val.id, text:text_str,selected:"selected"}).attr(optionKeyVal))	     
		 } else {
			$("#"+selectBox).append($("<option />",{value:val.id, text:text_str}).attr(optionKeyVal))	     
		 }
	   });
	},
	
	_getAction		: function()
	{
		var self = this;
					self.options.page_src['finYear'] = self.options.financialyear;
					self.options.page_src['legId'] = self.options.legislationId;
					self.options.page_src['delId'] = self.options.deliverableId;
					self.options.page_src['page'] = self.options.page;
					self.options.page_src['currentPage'] = self.options.currentPage;
					self.options.page_src['user'] = self.options.user;
					self.options.page_src['searchtext'] = self.options.searchtext;
					self.options.page_src['view'] = self.options.view;
				 self._writeArray();
			
	     $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );		
		$.getJSON(self.options.url,{
		  options :{ start	: self.options.lowerLimit,
			        limit	: self.options.upperLimit,
                       deliverable_id : self.options.deliverableId,
                       legislation_id : self.options.legislationId,
                       section        : self.options.section,
                       page           : self.options.page,
                       user           : self.options.user,
                       financial_year : self.options.financialyear, 
                       searchtext     :self.options.searchtext,
                       view           : self.options.view
             }
		}, function( response ) {
		     $("#actionLoadingDiv").remove();
		     if(response.hasOwnProperty('totalActions'))
		     {
                self.options.totalActions = response.totalActions;	
				var dtadi = self.options.delTotalActionsDisplayID;
				var ta = response.totalActions;
	
				if(dtadi.length>0) {
					var v = ta>1 ? ta+" actions" : ta+" action";
					$(dtadi).html(v);
					if(ta==0 && self.options.section=="new") {
						$(dtadi).css({"font-weight":"bold","color":"#cc0001"});
					}
				}
		     }
		     if(response.hasOwnProperty('user'))
		     {
		        self.options.user_logged = response.user;
		     }
		     
	         self.options.actionStatus = response.actionStatus;
	         $("#"+self.options.tableId).html("");
	         if( $("#"+self.options.tableId).length < 1)
	         {
	              self._createTable()
	         }
	         if(response.hasOwnProperty('actionOwner'))
	         {
	            self.options.actionOwners = response.actionOwner;
	         }
	         if($.isEmptyObject(response.actions))
	         {
	              self._displayActionHeaders(response.headers, false);		          
		          $("#tr_header").hide();
		          $("#"+self.options.tableId).append($("<tr />")
			        .append($("<td />",{colspan: response.columns+1, html:"There are no actions allocated to you for the selected financial year or legislations or deliverable"}))
		          );
	         } else {
				self._displayActionPaging( response.totalActions, response.columns );
				self._displayActionHeaders(response.headers, true);		               
				self._displayActions(response.actions, response.average, response.columns, response.deliverable );	
	         }
		});
		
	} ,
     
     _createTable             : function()
     {
        var self = this;
        $(self.element).append($("<table />",{id:self.options.tableId}))
     } ,	

	_displayActions		: function( actions, average, cols, actionDeliverable)
	{   //alert(actionDeliverable.length());
		//for(x in actionDeliverable) { alert(x+" :: "+actionDeliverable[x]); }
		var self     = this;	
		var userList = $("body").data("users");
		var html     = [];
		$.each(actions, function(index, action){
			var btnOptions = self._displayButtonOptions(index);		
		    if(self.options.actionOwners[index] == self.options.user_logged['tid'])
		    {
		       html.push("<tr id='tr_"+index+"'>");
		    } else {
		       html.push("<tr id='tr_"+index+"' class='view-notowner'>");
		    }
		      $.each(action, function(i, val){
		         if(self.options.searchAction)
		         {
			        if((val !== null || val !== "") && typeof val == "string")
			        {
		               if(val.indexOf(self.options.searchtext) >= 0)
		               {
		                  html.push("<td><span style='background-color:yellow'>"+val+"</span></td>");
		               } else {
		                  html.push("<td>"+(i == "action_progress" ? val+"<em>%</em>" : val)+"</td>");
		               }			          
			        } else {
			           html.push("<td>"+(i == "action_progress" ? val+"<em>%</em>" : val)+"</td>");
			        }  
		         } else {
                    html.push("<td>"+(i == "action_progress" ? val+"<em>%</em>" : val)+"</td>");
		         }
		      });			
		   
			if(self.options.viewAction)
			{
			   html.push("<td>");
               if(btnOptions['disableButton'])
               {
                  html.push("<input type='button' name='view_action__"+index+"' id='view_action__"+index+"' value='"+btnOptions['disableText']+"' disabled='disabled' />")
               } else {
                html.push("<input type='button' name='view_action_"+index+"' id='view_action_"+index+"' value='View' />")
			        $("#view_action_"+index).live("click", function(){
				        document.location.href = "view_action.php?id="+index;
				        return false;
			        });
               }
			   html.push("</td>"); 
			}

			if(self.options.editAction)
			{
			   html.push("<td class=center>");
			     if(btnOptions['disableButton'])
			     {
			        html.push("<input type='button' name='edit_action__"+index+"' id='edit_action__"+index+"' value='Edit' disabled='disabled' />")
			     } else {
			        html.push("<input type='button' name='edit_action_"+index+"' id='edit_action_"+index+"' value='Edit' />")
                    $("#edit_action_"+index).live("click", function(){
                        if(self.options.page == "create_action" )
                        {
                          document.location.href = "edit_action.php?id="+index+(self.options.eventActivation ? "&eventoccuranceid="+self.options.occuranceid : "");
                        } else if(self.options.page == "edit"){
                           //alert("sssssssssssssssss");
                           document.location.href = "edit_action.php?id="+index+(self.options.eventActivation ? "&eventoccuranceid="+self.options.occuranceid : "");                            
                        } else {
                           self._editDeliverableAction(index, 0);		
                           $("#clear_remind").val("");		
                        }
                        return false;
                    });
			     }
				 //alert(self.options.section);
			     if(self.options.section == "admin" || self.options.section=="new")
			     { //alert(index);
			         var allowDelete = false; 
			         if(actionDeliverable.hasOwnProperty(index))
			         { //alert(index+" ::> "+actionDeliverable[index]);
			            if(actionDeliverable[index])
			            {
			               allowDelete = true 
			            } else {
			               allowDelete = false;
			            }
			         }
			         if(allowDelete) {
			            html.push("<br /><input type='button' name='delete_action_"+index+"' id='delete_action_"+index+"' class=delete_button index="+index+" value='Delete' />") 
			         } 
                     

			        $("#delete_action_"+index).live("click", function(){
				       /* if(confirm("Are you sure you want to delete Action A"+index+"?")) 
				        {
				           jsDisplayResult("info", "info", "Deleting ...  <img src='../images/loaderA32.gif' />");
						   Action.delAction(index);
				        }*/
						deleteButton(index);
				        return false;
			        });
					 
					 
					 
					 
			     }
			     if(self.options.copyAction)
			     {
			       if(btnOptions['disableButton'])
			       {
			         html.push("<br /><input type='button' name='copy_action_"+index+"' id='copy_action_"+index+"' value='Copy' disabled='disabled' />") 
			       } else {
			         html.push("<br /><input type='button' name='copy_action_"+index+"' id='copy_action_"+index+"' value='Copy' />") 
                     $("#copy_action_"+index).live("click", function(){
                          document.location.href = "copy_action.php?id="+index+"&deliverableid="+self.options.deliverableId;
                          return false;
                     });	
			       }
			     }
			   html.push("</td>");
			}
			if(self.options.updateAction)
			{
			   html.push("<td>");
               if(btnOptions['disableButton'])
               {
                  html.push("<input type='button' name='update_action__"+index+"' id='update_action__"+index+"' value='"+btnOptions['disableText']+"' disabled='disabled' />")
               } else {
                html.push("<input type='button' name='update_action_"+index+"' id='update_action_"+index+"' value='Update Action' />")
			        $("#update_action_"+index).live("click", function(){
						var st = self._writeArray();
				        document.location.href = "update_action.php?id="+index+"&page_src="+st;
				        return false;
			        });
               }
			   html.push("</td>"); 
			}
			
			if(self.options.assuranceAction)
			{
			   html.push("<td>");
               html.push("<input type='button' name='assurance_action_"+index+"' id='assurance_action_"+index+"' value='Assurance' />");
                $("#assurance_action_"+index).live("click", function(){
                  document.location.href = "assurance_action.php?id="+index;
                  return false;
                });	
			   html.push("</td>"); 
			}
			
			if(self.options.reassignAction)
			{
			   html.push("<td>");
                 html.push("<select name='userlist_"+index+"' id='userlist_"+index+"' class='userslist'>");
                 html.push("</select>");
			   html.push("</td>"); 
			   $("#reassign_action_"+index).live("click", function(e){
                    self._editDeliverableAction(index, 1);
				    e.preventDefault();
			   });			
			}			
		    html.push("</tr>");	
		});
		$("#"+self.options.tableId).append(html.join(' '));
		
		if(!$.isEmptyObject(userList) && self.options.reassignAction)
		{
		   $.each(self.options.actionStatus, function(actionId, actionStatus){
		       var actionOwner = self.options.actionOwners[actionId];
		       $.each(userList, function(uIndex, tkUser){
                  if(actionOwner === tkUser['id'])
                  {
                     $("#userlist_"+actionId).append($("<option />",{text:tkUser['user'], value:tkUser['id'], selected:"selected"}).addClass("userslist"))
                   } else {
                     $("#userlist_"+actionId).append($("<option />",{text:tkUser['user'], value:tkUser['id']}).addClass("userslist"))
                   }
		       });
		   })
		}
			
		if(self.options.page !== "create_action"  )
		{
			//$("#"+self.options.tableId).append($("<tr />")
			$("#"+self.options.tableId).append($("<tr />")
				.append($("<td />",{colspan:cols - 1, html:"Overall Actions Progress"}).css({"text-align":"right"}))
				.append($("<td />",{html:"<b>"+average+" % </b>"}))
				.append($("<td />",{html:""}).css({"display" : ( self.options.assuranceAction ? "table-cell" : "none") }))
				.append($("<td />",{html:"&nbsp;", colspan:"2"}).css({"display" : ( self.options.editAction ? "table-cell" : "none") }))
				.append($("<td />",{html:""}).css({"display" : ( self.options.viewAction ? "table-cell" : "none") }))
				.append($("<td />",{html:""}).css({"display" : ( self.options.updateAction ? "table-cell" : "none") }))
				.append($("<td />",{html:""}).css({"display" : ( self.options.reassignAction ? "table-cell" : "none") }))
			)			
		}
          $("#"+self.options.tableId).append($("<tr />").css({display:(self.options.reassignAction ? "table-row" : "none")})
            .append($("<td />",{colspan:(cols)+1}).css({"text-align":"right"})
             .append($("<input />",{type:"button", name:"save_changes_"+self.options.user, id:"save_changes_"+self.options.user, value:"Save Changes"})
              .addClass("isubmit")
              .click(function(e){
                 jsDisplayResult("info", "info", "Re-assingning actions . . . .<img src='../images/loaderA32.gif' />" );
                 $.post("../class/request.php?action=ClientAction.reAssign", {
                    data 	 : $(".userslist").serializeArray(),
                    statuses : self.options.actionStatus
                 }, function(response){
                       if(response.error)
                       {
                          jsDisplayResult("error","error",response.text );                            
                       } else {
                       	 if(response.updated)
                       	 {
                           jsDisplayResult("info","info",response.text);                                       
                       	 } else {
                           jsDisplayResult("ok","ok",response.text);                                       	                          
                           self._getAction();
                       	 }
                       }
                 },"json"); 
                 e.preventDefault();      
              })
             )
            )
          )
	} ,
	
	_displayActionHeaders		: function(headers, extraHeader)
	{
		var self = this;
		var html = [];
		html.push("<tr>");
		$.each(headers, function( index, value) {
		    html.push("<th class='"+self.options.thClass+"'>"+value+"</th>");
			//tr.append($("<th />",{html:value}).addClass(self.options.thClass))
		});
		if(self.options.assuranceAction && extraHeader)
		{
		  html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
		}
		if(self.options.editAction && extraHeader)
		{
		  html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
		}
		if(self.options.viewAction && extraHeader)
		{
		  html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
		}
		if(self.options.updateAction && extraHeader)
		{
		  html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
		}
		if(self.options.reassignAction && extraHeader)
		{
		  html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
		}		
		html.push("</tr>");
		$("#"+self.options.tableId).append( html.join(' ') );
	} , 
	
	_displayActionPaging 		: function( total, columns ) {
		var self = this;
		var html = [];
		var pages;
		if( total%self.options.upperLimit > 0){
			pages   = Math.ceil(total/self.options.upperLimit); 				
		} else {
			pages   = Math.floor(total/self.options.upperLimit); 				
		}
		var delId = self.options.deliverableId
		
		html.push("<tr>");
		  html.push("<td colspan='"+columns+"'>");
		    html.push("<input type='button' name='actionfirst_"+delId+"' id='actionfirst_"+delId+"' value=' |< ' />");
		    html.push("&nbsp;&nbsp;")
		    html.push("<input type='button' name='actionprevious_"+delId+"' id='actionprevious_"+delId+"' value=' < ' />");
		    html.push("&nbsp;&nbsp;")
		    html.push("Page "+self.options.currentPage+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
		    html.push("&nbsp;&nbsp;")
		    html.push("<input type='button' name='actionnext_"+delId+"' id='actionnext_"+delId+"' value=' > ' />");
		    html.push("&nbsp;&nbsp;")
		    html.push("<input type='button' name='actionlast_"+delId+"' id='actionlast_"+delId+"' value=' >| ' />");
		  html.push("</td>");
		  if(self.options.assuranceAction)
		  {
		     html.push("<td>&nbsp;</td>");
		  }
		  if(self.options.editAction)
		  {
		     html.push("<td>&nbsp;</td>");
		  }
		  if(self.options.viewAction)
		  {
		     html.push("<td>&nbsp;</td>");
		  }
		  if(self.options.updateAction)
		  {
		     html.push("<td>&nbsp;</td>");
		  }
		  if(self.options.reassignAction)
		  {
		     html.push("<td>&nbsp;</td>");
		  }		
		html.push("<tr>");
		$("#"+self.options.tableId).append(html.join(' '));

		if(self.options.currentPage < 2)
		{
              $("#actionfirst_"+delId).attr('disabled', 'disabled');
              $("#actionprevious_"+delId).attr('disabled', 'disabled');		     
		}
		if((self.options.currentPage == pages || pages == 0))
		{
              $("#actionnext_"+delId).attr('disabled', 'disabled');
              $("#actionlast_"+delId).attr('disabled', 'disabled');		     
		}		
		$("#actionnext_"+delId).live("click", function(evt){
			self._getNextAction(self);
			evt.stopImmediatePropagation();
		});
		$("#actionlast_"+delId).live("click",  function(evt){
			self._getLastAction(self);
               evt.stopImmediatePropagation();
		});
		$("#actionprevious_"+delId).live("click",  function(evt){
			self._getPreviousAction(self);
			evt.stopImmediatePropagation();
		});
		$("#actionfirst_"+delId).live("click",  function(evt){
			self._getFirstAction(self);
			evt.stopImmediatePropagation();
		});			
	} ,		
	
	_getNextAction  			: function(rk) 
	{
		rk.options.currentPage = rk.options.currentPage+1;
		rk.options.lowerLimit = parseFloat(rk.options.currentPage - 1) * parseFloat( rk.options.upperLimit);
	     this._getAction();
	},	
	_getLastAction  			: function(rk) 
	{    
		rk.options.currentPage =  Math.ceil( parseFloat( rk.options.totalActions )/ parseFloat( rk.options.upperLimit ));
		rk.options.lowerLimit   = parseFloat(rk.options.currentPage-1) * parseFloat( rk.options.upperLimit );			
		this._getAction();
	},	
	_getPreviousAction    	: function(rk)
	{
		rk.options.currentPage  = parseFloat( rk.options.currentPage ) - 1;
		rk.options.lowerLimit 	= (rk.options.currentPage-1)*rk.options.upperLimit;		
		this._getAction();			
	},	
	_getFirstAction  			: function(rk) 
	{
		rk.options.currentPage  = 1;
		rk.options.lowerLimit 	= 0;
		this._getAction();				
	}	,
	
	_editDeliverableAction		: function(index, reAssignAction)
	{
		var self = this;
		///if($("#editactiondialog_"+index).length > 0)
		//{
		$("#editactiondialog_"+index).remove();
		
		//}
		
		$.get("../class/request.php?action=Action.getEditAction",{
			data : { actionid : index , reAssignAction : reAssignAction}
		},function( response ){
			$("#editactiondialog_"+index).html("");
			$("#editactiondialog_"+index).remove();
			$("<div />",{id:"editactiondialog_"+index, html:response})
			.dialog({
				autoOpen 	: true,
				modal 	: true,
				title 	: "Edit action ref "+index,
				position 	: "top",
				width 	: 750,
			     height    : "auto",
				buttons 	: {	
							"Save Changes"		: function()
							{
								var data		          = {};
								data.action		     = $("#_actionname").val();
								data.owner  	          = $("#_actionowner :selected").val();
								data.action_deliverable  = $("#_deliverable").val();
								data.deadline	          = $("#_deadline").val();
								data.reminder	          = $("#_reminder").val();
								data.deliverable_id	     = $("#deliverable_id").val();
								data.id	   		     = index;
								if(data.action === "") {
									$("#actionmessage").addClass("ui-state-error").html("Action name is required");	
								} else if( data.owner === "") {
									$("#actionmessage").addClass("ui-state-error").html("Please select the action owner");	
								} else if( data.action_deliverable === "") {
									$("#actionmessage").addClass("ui-state-error").html("Please select the deliverable");	
								} else if( data.deadline === "") {
									$("#actionmessage").addClass("ui-state-error").html("Please enter the deadline");	
								} else {
									$("#actionmessage").addClass("ui-state-error").html("Updating Action . . . .<img src='../images/loaderA32.gif' />");
									$.post("../class/request.php?action=ClientAction.editAction",
										{ data : data }, function( response ){
										if( response.error )
										{
											$("#actionmessage").addClass("ui-state-error").html(response.text);
										} else{
											jsDisplayResult("ok", "ok", response.text);
											self._getAction();
											$("#editactiondialog_"+index).dialog("destroy");
											$("#editactiondialog_"+index).remove();				
										}    
									},"json")												
								}
							} ,
							"Cancel"				: function()
							{
								$("#editactiondialog_"+index).dialog("destroy");
								$("#editactiondialog_"+index).remove();
							}
				},
				open 				: function(event, ui)
				{
					var dialog = $(event.target).parents(".ui-dialog.ui-widget");
					var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
					var saveButton = buttons[0];
					$(saveButton).css({"color":"#090"});
					
					$(".datepicker").datepicker({
						  showOn: 'both',
						  buttonImage: '/library/jquery/css/calendar.gif',
						  buttonImageOnly: true,
						  dateFormat: 'dd-M-yy',
						  changeMonth:true,
						  changeYear:true		
					 });
					
				}
			})
			$("#clear_remind").live("click", function(e){
				$("#_reminder").val("");
				e.preventDefault();
			});	

		},"html")
		
	} ,
	
	_displayButtonOptions    : function(actionId)
	{
	     var self = this;
	     var btnDisplayOptions                 = {};
	     btnDisplayOptions['disableButton']    = false;
	     btnDisplayOptions['disableText']	   = false;
	     btnDisplayOptions['disableUpdateBtn'] = false;
	     if(((self.options.actionStatus[actionId] & 16) == 16))
	     {
		     btnDisplayOptions['disableUpdateBtn'] = true
		     btnDisplayOptions['disableText']   = "Awaiting approval";				
	     } else if( ((self.options.actionStatus[actionId] & 32) == 32) ) {
		     btnDisplayOptions['disableUpdateBtn'] = true
		     btnDisplayOptions['disableText']   = "Locked";				
	     } 
         return btnDisplayOptions
	} 

	
});


/*			        $("#delete_action_"+index).live("click", function(){
				        if(confirm("Are you sure you want to delete Action A"+index+"?")) 
				        {
				           jsDisplayResult("info", "info", "Deleting ...  <img src='../images/loaderA32.gif' />");
				           $.post("../class/request.php?action=ClientAction.deleteAction", {
				             data : { id : index, actionstatus : 2 }
				            }, function( response ){
					          if( response.error )
					          {
						        jsDisplayResult("error", "error", response.text);
					          } else {
						        jsDisplayResult("ok", "ok", response.text);
								$("#delete_action_"+index).removeClass("delete_button");
								if($("input:button.delete_button").size()==1) {
									$("input:button.delete_button").hide()
								}
						        $("#tr_"+index).fadeOut();
					          }
					        },"json");
				        }
				        return false;
			        });	*/
	function deleteButton(i) {
		if(confirm("Are you sure you want to delete Action A"+i+"?")) {
			jsDisplayResult("info", "info", "Deleting ...  <img src='../images/loaderA32.gif' />");
				           $.post("../class/request.php?action=ClientAction.deleteAction", {
				             data : { id : i, actionstatus : 2 }
				            }, function( response ){
					          if( response.error ) {
						        jsDisplayResult("error", "error", response.text);
					          } else {
//						        jsDisplayResult("ok", "ok", response.text);
								redirectAfterActivity(response.text);
					          }
					        },"json");
		}
	}
