$(function(){
	Search.getLegislativeTypes();
	Search.getLegislationCategories();
	Search.getOrganisationSize();
	Search.getOrganisationType();
	Search.getOrganisationCapacity();
	Search.getResponsibleOrganisations();		
	
	$("#go").click(function(){
		Search.go();
	});
	
	$("#advancedsearch").click(function(){
		Search.advancedSearch();
		return false;
	});
	
	$("#searchagain").click(function(){
		$("#tablelegislation").show();
		$("#tr_searchagain").hide();
		$("#search_result").html("");
		$("#display_objresult").hide();
		return false;
	})
	
});

Search		= {
		
		go			: function()
		{
			jsDisplayResult("info", "info", "Searching . . . .<img src='../images/loaderA32.gif' />" );
			$.getScript("../js/jquery.ui.legislation.js", function(){
				$("#search_result").legislation({url:"../class/request.php?action=LegislationManager.searchLegislation", legoption:$("#search_text").val(), searchLegislation:true});				
			});
	
		} , 
		
		advancedSearch			: function()
		{
			jsDisplayResult("info", "info", "Searching . . . .<img src='../images/loaderA32.gif' />" );
			$.getScript("../js/jquery.ui.legislation.js", function(){
				$("#search_result").legislation({url:"../class/request.php?action=LegislationManager.advancedSearch", legoption:$("#advanced_search_form").serializeArray(), advancedSearchLegislation:true});				
			});
				
		}, 
		
		getLegislativeTypes		: function()
		{
			$.getJSON("../class/request.php?action=SetupManager.getLegislativeTypes", function( legTypes ){
				$.each( legTypes, function( index, legtype ){
					$("#type").append($("<option />",{text:legtype.name, value:legtype.id}))
				});		
			});
	
		},
		
		getLegislationCategories : function()
		{
			$.post("../class/request.php?action=SetupManager.getLegislativeCategories", function( response ){
				$.each( response, function( index, legcat){
					$("#category").append($("<option />",{text:legcat.name, value:legcat.id}))				   
				})												  											  
			},"json");
		}  ,
				
		getOrganisationSize : function()
		{
			$.post("../class/request.php?action=SetupManager.getOrganisationSizes", function( response ){
				$.each( response, function( index, orgsize){
					$("#organisation_size").append($("<option />",{text:orgsize.name, value:orgsize.id}))				   
				})												  											  
			},"json");
		}  ,	
		
		getOrganisationType : function()
		{
			$.post("../class/request.php?action=SetupManager.getLegOrganisationTypes", function( response ){
				$.each( response, function( index, orgtype){
					$("#organisation_type").append($("<option />",{text:orgtype.name, value:orgtype.id}))				   
				})												  											  
			},"json");
		}  ,
		
		getOrganisationCapacity			: function()
		{
			$.post("../class/request.php?action=SetupManager.getOrganisationCapacity", function( response ){
				$.each( response, function( index, orgcapacity){
					$("#organisation_capacity").append($("<option />",{text:orgcapacity.name, value:orgcapacity.id}))				   
				})											  											  
			},"json");
		} , 	
		
		getResponsibleOrganisations			: function()
		{
			$.post("../class/request.php?action=SetupManager.getResponsibleOrganisations",{ active : 1}, function( response ){
				$.each( response, function( index, orgcapacity){
					$("#responsible_organisation").append($("<option />",{text:orgcapacity.name, value:orgcapacity.id}))				   
				})											  											  
			},"json");
		} 				
};