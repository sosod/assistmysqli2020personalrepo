$(function(){
	
	if($("#refid").val() == undefined)
	{
		Directorate.get();		
	}
	
	$("#changestatus").click(function(){
		Directorate.changestatus();
		return false;
	});
	
	$("#edit").click(function(){
		Directorate.editMatch();		
		return false;
	});
	
	$("#cancel").click(function(){
		history.back();
		return false;
	});
	
	$("#assign").click(function(){
		Directorate.matchDepartments();
		return false;
	});
	
	$("#assignsub").click(function(){
		Directorate.matchSubDepartments();
		return false;
	});
	
	$("#sub_directorate").change(function(){
		$("#action_owner_title").attr("selected", "").attr("disabled", "");
		Directorate.getActionOwners( $(this).val() ); 		
		return false;
	});
	
});


var Directorate		= {

		get		: function()
		{	
			$("#department").empty();
			$("#department").append($("<option />",{text:"--please select--", value:""}))			
			$.getJSON("../class/request.php?action=DepartmentManager.getMasterDepartments", function( departments ){
		       if(!$.isEmptyObject(departments))
			  {
				$.each( departments, function( index, department){
				  if(department.used === true)
				  {
				   $("#department").append($("<option />",{text:department.name, value:department.id, disabled:"disabled"}))
				  } else {
				   $("#department").append($("<option />",{text:department.name, value:department.id}))
				  }
				});
			   }
			});
			
			$("#masterdepartment").empty();
			$("#masterdepartment").append($("<option />",{text:"--please select--", value:""}))			
			$.getJSON("../class/request.php?action=ClientDepartment.getDepartments", function( departments ){
				if(!$.isEmptyObject(departments))
				{
					$.each( departments, function(index, department){
						$("#masterdepartment").append($("<option />",{text:department.name, value:department.id}))
					});
				}
			});			
			
			$(".departments").remove();
			$.getJSON("../class/request.php?action=DepartmentManager.getAll", function( responseList ){
				if(!$.isEmptyObject(responseList))
				{
					$.each( responseList, function(index , deptList){
						$("#table_directorates").append($("<tr />",{id:"tr_"+index}).addClass("departments")
						  .append($("<td />",{html:index}))		
						  .append($("<td />",{html:deptList.masterDept}))
						  .append($("<td />",{html:deptList.clientDept}))
						  .append($("<td />",{html:"<b>"+((deptList.status & 1) == 1 ? "Active" : "Inactive")}))						  
						  .append($("<td />")
							.append($("<input />",{type:"button" ,name:"edit_"+index, id:"edit_"+index, value:"Edit"}))
							.append($("<input />",{type:"button" ,name:"delete_"+index, id:"delete_"+index, value:"Del"}).click(function(e){
							   if(confirm("Are you sure you want to delete this department match"))
							   { 
							      Directorate._deleteMatch(index)					   
							   }
							   e.preventDefault();
							 })  
							)  
						  )
						  .append($("<td />")
							.append($("<input />",{type:"button", name:"changestatus_"+index, id:"changestatus_"+index, value:"Change Status"}))
						  )
						)	
						
						$("#edit_"+index).live("click", function(){
							document.location.href = "edit_department.php?id="+index;						
							return false;
						});
	
						
						$("#changestatus_"+index).live("click", function(){
							document.location.href = "change_department_status.php?id="+index;						
							return false;
						});						
					});		
				} else {
				     $("#table_directorates").append($("<tr />")
				       .append($("<td />",{colspan:"7", html:"No matches between master department and client departments"})
				         .addClass("departments")
				       )
				     )
				}									
			});
		} , 
		
		matchDepartments	: function()
		{
			var refid   = $("#department :selected").val()
			var dept_id = $("#masterdepartment :selected").val()
			
			if( refid == ""){
				jsDisplayResult("error", "error", "Please select the compliance master department");
				return false;
			} else if( dept_id == ""){
				jsDisplayResult("error", "error", "Please select the client department");
				return false;				
			} else {
				jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");								
				$.post("../class/request.php?action=DepartmentMatch.saveDeptMatch",{
					data : { ref_id : refid, dept_id : dept_id}
				}, function( response  ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {			
						Directorate.get();
						jsDisplayResult("ok", "ok", response.text);
					}					
				},"json");
			}
		} ,
		
		editMatch	: function()
		{
			var refid   = $("#department :selected").val();
			var dept_id = $("#masterdepartment :selected").val();
			var id      = $("#refid").val();  
			
			if( refid == ""){
				jsDisplayResult("error", "error", "Please select the department");
				return false;
			} else if( dept_id == ""){
				jsDisplayResult("error", "error", "Please select the master department");
				return false;				
			} else {
				jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");								
				$.post("../class/request.php?action=DepartmentMatch.editMatch",{
					data : { ref_id : refid, dept_id : dept_id, id : id}
				}, function( response  ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {			
						jsDisplayResult("ok", "ok", response.text);
						//document.location.href = "match_sub_departments.php?ref_id="+refid+"&deptid="+dept_id+"&dirid="+response.id;
					}					
				},"json");
			}
		} ,
		
		_deleteMatch        : function(id)
		{
		   jsDisplayResult("info", "info", "deleteing . . . <img src='../images/loaderA32.gif' />");  
		   $.post("../class/request.php?action=DepartmentMatch.deleteMatch",{
		     matchid   : id 
		   } ,function(response){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {			
				jsDisplayResult("ok", "ok", response.text);
				$("#tr_"+id).fadeOut();
				Directorate.get();
			}	
		   },"json");
		} ,
		
		changestatus		: function()
		{
			var id  = $("#refid").val();
			var status = $("#status").val();  
			
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");								
			$.post("../class/request.php?action=DepartmentMatch.changeMatch",{
				data : { status : status, id : id}
			}, function( response  ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {			
					jsDisplayResult("ok", "ok", response.text);
				}					
			},"json");
		} 
		
		
} 
