$(function(){
  Notification.get();
  $("#add").click(function(){
    Notification.addNotification();
  });

});

var Notification = {

   get                   : function()
   {
     var self = this;
     $(".notifications").remove();
     $.getJSON("../class/request.php?action=Notification.getAll", function(notifications){
        if(!$.isEmptyObject(notifications))
        {
          $.each(notifications, function(index, notification){
            Notification._display(notification);
          });
        } else {
          $("#notifications_table").append($("<tr />").addClass("notifications")
            .append($("<td />",{html:"There are no notifications set", colspan:"6"}))
          )
        }     
     })
   
   },
  
  _display               : function(notification)
  {
     var self = this;
     $("#notifications_table").append($("<tr />",{id:"tr_"+notification.id}).addClass("notifications")
       .append($("<td />",{html:notification.id}))
       .append($("<td />",{html:notification.frequency}))
       .append($("<td />",{html:notification.when}))
       .append($("<td />",{html:notification.what}))
       .append($("<td />",{html:(notification.status & 1) ? "<b>Active</b>" : "<b>InActive</b>"}))
       .append($("<td />")
         .append($("<input />",{type:"button", name:"edit_"+notification.id, id:"edit_"+notification.id, value:"Edit"}))
         //.append($("<input />",{type:"button", name:"del_"+notification.id, id:"del_"+notification.id, value:"Del"}))
       )     
     )
     
     $("#edit_"+notification.id).live("click", function(e){
        self._editNotification(notification);     
        e.preventDefault();
     });
     
  },
     
  addNotification        : function()
  {
    var self = this;
    if($("#notificationDialog").length > 0)
    {
      $("#notificationDialog").remove();
    }
    
    $("<div />",{id:"notificationDialog"}).append($("<table />", {width:"100%"})
      .append($("<tr />")
        .append($("<th />",{html:"Receive When"}))
        .append($("<td />")
		  .append($("<select />",{name:"frequency", id:"frequency"})
			 .append($("<option />",{text:"Daily", value:"daily"}))
			 .append($("<option />",{text:"Weekly", value:"weekly"}))
		  )	 
        )
      )
      .append($("<tr />",{id:"daily_tr"})
        .append($("<th />",{html:"Receive Day"}))
        .append($("<td />")
		  .append($("<select />",{id:"when", name:"when"})
			 .append($("<option />",{value:"1", text:"Monday"}))
			 .append($("<option />",{value:"2", text:"Tuesday"}))
			 .append($("<option />",{value:"3", text:"Wednesday"}))
			 .append($("<option />",{value:"4", text:"Thursday"}))
			 .append($("<option />",{value:"5", text:"Friday"}))
			 .append($("<option />",{value:"6", text:"Saturday"}))
			 .append($("<option />",{value:"7", text:"Sunday"}))
		  )	          
        )
      )
      .append($("<tr />")
        .append($("<th />",{html:"What To Receive:"}))
        .append($("<td />")
		  .append($("<select />",{id:"what", name:"what"})
			 .append($("<option />",{text:"Actions due this week", value:"due_this_week"}))
			 .append($("<option />",{text:"Actions due on or before this week", value:"due_on_or_before_week"}))
			 .append($("<option />",{text:"Actions due today", value:"due_today"}))
			 .append($("<option />",{text:"Actions due on or before today", value:"due_on_or_before_today"}))
			 .append($("<option />",{text:"All Incomplete actions", value:"all_incomplete_actions"}))
		  )	        
        )
      )
	.append($("<tr />")
	   .append($("<td />",{id:"message", colspan:"2"}))			   
	)                      
    ).dialog({
            autoOpen     : true,
            modal        : true,
            position     : "top",
            width        : 500,
            title        : "User Notifications - Reminder Emails",
            buttons      : 
            {
               "Save"    : function()
               {
                 Notification.save();    
               } ,
               
               "Cancel"  : function()
               {
                 $("#notificationDialog").dialog("destroy");
                 $("#notificationDialog").remove();
               }
            } ,
            
            close        : function(event, ui)
            {
              $("#notificationDialog").dialog("destroy");
              $("#notificationDialog").remove();            
            } ,
            
            open         : function(event, ui)
            {
			var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			var saveButton = buttons[0];
			$(saveButton).css({"color":"#090"});    
			
			$("#frequency").bind("change", function(e){
                  $("#daily_tr").toggle();
			   e.preventDefault();
			});   
            }
    });
  } ,
  
  _editNotification       : function(notification)
  {
    var self = this;
    if($("#editNotificationDialog").length > 0)
    {
      $("#editNotificationDialog").remove();
    }
    
    $("<div />",{id:"editNotificationDialog"}).append($("<table />", {width:"100%"})
      .append($("<tr />")
        .append($("<th />",{html:"Receive When"}))
        .append($("<td />")
		.append($("<select />",{name:"frequency", id:"frequency"})
	      .append($("<option />",{text:"Daily", value:"daily", selected:(notification.freq === "daily" ? "selected" : "")}))
	      .append($("<option />",{text:"Weekly", value:"weekly", selected:(notification.freq === "weekly" ? "selected" : "")}))
		)	 
        )
      )
      .append($("<tr />",{id:"daily_tr"}).css({display:(notification.freq === "daily" ? "none" : "table-row")})
        .append($("<th />",{html:"Receive Day"}))
        .append($("<td />")
		.append($("<select />",{id:"when", name:"when"})
		  .append($("<option />",{value:"1", text:"Monday", selected:(notification.recieve_when === "1" ? "selected" : "")}))
		  .append($("<option />",{value:"2", text:"Tuesday", selected:(notification.recieve_when === "2" ? "selected" : "")}))
		  .append($("<option />",{value:"3", text:"Wednesday", selected:(notification.recieve_when === "3" ? "selected" : "")}))
		  .append($("<option />",{value:"4", text:"Thursday", selected:(notification.recieve_when === "4" ? "selected" : "")}))
		  .append($("<option />",{value:"5", text:"Friday", selected:(notification.recieve_when === "5" ? "selected" : "")}))
		  .append($("<option />",{value:"6", text:"Saturday", selected:(notification.recieve_when === "6" ? "selected" : "")}))
		  .append($("<option />",{value:"7", text:"Sunday", selected:(notification.recieve_when === "7" ? "selected" : "")}))
		)	          
        )
      )
      .append($("<tr />")
        .append($("<th />",{html:"What To Receive:"}))
        .append($("<td />")
		  .append($("<select />",{id:"what", name:"what"})
			 .append($("<option />",{text:"Actions due this week", value:"due_this_week", selected:(notification.recieve_what === "due_this_week" ? "selected" : "")}))
			 .append($("<option />",{text:"Actions due on or before this week", value:"due_on_or_before_week", selected:(notification.recieve_what === "due_on_or_before_week" ? "selected" : "")}))
			 .append($("<option />",{text:"Actions due today", value:"due_today", selected:(notification.recieve_what === "due_today" ? "selected" : "")}))
			 .append($("<option />",{text:"Actions due on or before today", value:"due_on_or_before_today", selected:(notification.recieve_what === "due_on_or_before_today" ? "selected" : "")}))
			 .append($("<option />",{text:"All Incomplete actions", value:"all_incomplete_actions", selected:(notification.recieve_what === "all_incomplete_actions" ? "selected" : "")}))
		  )	        
        )
      )
	.append($("<tr />")
	   .append($("<td />",{id:"message", colspan:"2"}))			   
	)                      
    ).dialog({
            autoOpen     : true,
            modal        : true,
            position     : "top",
            width        : 500,
            title        : "Edit User Notifications - Reminder Emails",
            buttons      : 
            {
               "Save Changes"    : function()
               {
                 self._update(notification.id);    
               } ,
               
               "Cancel"  : function()
               {
                 $("#editNotificationDialog").dialog("destroy");
                 $("#editNotificationDialog").remove();
               } ,
               
               "Activate"     : function()
               {
                  self._updateStatus(notification.id, 1);               
               } ,
               
               "De-Activate"   : function()
               {
                  self._updateStatus(notification.id, 0);
               } ,
               
               "Delete"        : function()
               {
                  self._updateStatus(notification.id, 2);  
               }
            } ,
            
            close        : function(event, ui)
            {
              $("#editNotificationDialog").dialog("destroy");
              $("#editNotificationDialog").remove();            
            } ,
            
            open         : function(event, ui)
            {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var saveBtn       = btns[0];
                var cancelBtn     = btns[1];
                var activateBtn = btns[2];
                var deactivateBtn   = btns[3];
                var deleteBtn     = btns[4];
                    	                
                if((notification.status & 1) == 0)
                {
                  $(deactivateBtn).css({"display":"none"});
                } else {
                  $(activateBtn).css({"display":"none"});
                }
                
                $(saveBtn).css({"color":"#090"});
                $(deactivateBtn).css({"color":"red"});
                $(activateBtn).css({"color":"#090"});
                $(deleteBtn).css({"color":"red"}); 
			
			$("#frequency").bind("change", function(e){
                  $("#daily_tr").toggle();
			   e.preventDefault();
			});   
            }
    }); 
  } ,
  
  save                   : function()
  {
    var self = this;
    $("#message").html("Saving   . . . <img src='../images/loaderA32.gif' />");
    $.post("../class/request.php?action=Notification.saveNotification",{
      data : { user_id       : $("#userid").val(),
               frequency     : $("#frequency :selected").val(),
               recieve_when  : $("#when :selected").val(),
               recieve_what  : $("#what :selected").val()
             }
    }, function(response){
     if(response.error)
     {
        $("#message").addClass("ui-state-error").html(response.text);
     } else {
        jsDisplayResult("ok", "ok", response.text);
        $("#notificationDialog").dialog("destroy");
        $("#notificationDialog").remove();         
        self.get();
     }
    },"json");
  
  } ,
  
  _update      : function(id)
  {
    var self = this;
    $("#message").html("Updating . . . <img src='../images/loaderA32.gif' />");
    $.post("../class/request.php?action=Notification.updateNotification",{
      data : { id            : id,
               user_id       : $("#userid").val(),
               frequency     : $("#frequency :selected").val(),
               recieve_when  : $("#when :selected").val(),
               recieve_what  : $("#what :selected").val()
             }
    }, function(response){
     if(response.error)
     {
        $("#message").addClass("ui-state-error").html(response.text);
     } else {
        jsDisplayResult("ok", "ok", response.text);
        $("#editNotificationDialog").dialog("destroy");
        $("#editNotificationDialog").remove(); 
        self.get();        
     }
    },"json");
  } ,
  
  _updateStatus                 : function(id, status)
  {
    var self = this;
    $("#message").html("Updating . . . <img src='../images/loaderA32.gif' />");
    $.post("../class/request.php?action=Notification.deleteNotification",{
      data : { id            : id,
               status        : status
             }
    }, function(response){
     if(response.error)
     {
        $("#message").addClass("ui-state-error").html(response.text);
     } else {
        jsDisplayResult("ok", "ok", response.text);
        $("#editNotificationDialog").dialog("destroy");
        $("#editNotificationDialog").remove();       
        self.get();  
     }
    },"json");
  }
     
};
