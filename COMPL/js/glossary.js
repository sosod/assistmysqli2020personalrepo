// JavaScript Document
$(function(){
     $("#add_glossary").hide();

     if($("#show").val() !== undefined)
     {
        Glossary.getGlossary();
     }
     var _category = $("#_category").val();
     if( _category !== "")
     {
        Glossary.get(_category);
        $("#add_glossary").show();     
     }

     var category = $("#category");
     category.change(function(e){
        Glossary.get(category.val());
        $("#add_glossary").show();
        e.preventDefault();
     });   
   
	$("#add").click(function(e){
	   document.location.href = "add_glossary.php?category="+category.val();
	   //Glossary.addGlossary(category.val());
	   e.preventDefault()				 
	});

	$("#edit").click(function(){
		Glossary.edit();
		return false;					 
	});


	$("#save").click(function(){
		Glossary.save();
		return false;					 
	});

    /* $("#add_rule").click(function(e){
       var kname = $(".rules").last().attr("name");
       var _id = parseInt(kname.substr(5)) + 1; 
       $("#addRule").before($("<div />")
          .append($("<label />",{html:"Rule"}))
          .append($("<textarea />",{cols:"80", rows:"4", name:"rule_"+_id, id:"_rule", value:""}).addClass("rules"))
        )                 
        e.preventDefault()          
     });
     */
});

var Glossary  = {
     	
	getGlossary			: function()
	{
		//$(".glossary").remove();
		$.getJSON("../class/request.php?action=Glossary.getCategoryGlossary", function( response ){
			$.each(response, function(index, glossary){
		          $("#table_glossary").append($("<tr />",{id:"tr_"+glossary.id}).addClass("glossary")
		            .append($("<td />",{html:glossary.id}))		   		   
		            .append($("<td />",{html:(glossary.client_terminology === "" ? glossary.ignite_terminology : glossary.client_terminology)}))
		            .append($("<td />",{html:glossary.purpose}))		   
		            .append($("<td />",{html:glossary.rules}))		   	    
		           )						   
			})														 
		});	
	} , 

	get			: function(category)
	{
		$(".glossary").remove();
		$.get("../class/request.php?action=Glossary.getCategoryGlossary",{category:category}, function(response){
               $("#table_glossary").html(response);
               
		 
		      $(".edit").live("click", function(e){
		          var glossaryid = this.id.substr(5);
		          //self._editGlossary(glossary, category);
			     document.location.href = "edit_glossary.php?id="+glossaryid;
			    e.preventDefault();
		     });
		      
		      $(".delete").live("click", function(){
		           var glossaryid = this.id.substr(7);
			      jsDisplayResult("info", "info", "Delete  . . . <img src='../images/loaderA32.gif' />");
			      if(confirm("Are you sure you want to delete this glossary term"))
			      {
				     var data 	= {};
			      	data.status = 2;
			      	data.id		= glossaryid
				      $.post("../class/request.php?action=Glossary.updateGlossary", { data : data }, function( response ){
					     if( response.error )
					     {
						     jsDisplayResult("error", "error", response.text);		
					     } else {
						     $("#tr_"+glossaryid).fadeOut();
						     jsDisplayResult("ok", "ok", response.text);
					     }							
				     },"json")	
			     }
			     return false;											
		     });
		      	
               
		}, "html");		
		/*$.getJSON("../class/request.php?action=Glossary.getCategoryGlossary",{category:category},function( response ){
			$.each(response, function(index, glossary){
				Glossary.display(glossary, category);					   
			})														 
		});*/	
	} , 
	
	display		: function(glossary, category)
	{	
	    var self = this;
		$("#table_glossary").append($("<tr />",{id:"tr_"+glossary.id}).addClass("glossary")
			  .append($("<td />",{html:glossary.id}))		   		   
			  .append($("<td />",{html:(glossary.client_terminology === "" ? glossary.ignite_terminology : glossary.client_terminology)}))		   
			  .append($("<td />",{html:glossary.purpose}))		   
			  .append($("<td />",{html:glossary.rules}))		   	   
			  .append($("<td />")
				.append($("<input />",{type:"submit", name:"edit_"+glossary.id, id:"edit_"+glossary.id, value:"Edit"}))		
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", name:"del_"+glossary.id, id:"del_"+glossary.id, value:"Delete"}))		
			  ) 
		 )
		 
		 $("#edit_"+glossary.id).live("click", function(){
		     //self._editGlossary(glossary, category);
			document.location.href = "edit_glossary.php?id="+glossary.id;
			return false;											
		});
		 
		 $("#del_"+glossary.id).live("click", function(){
			 jsDisplayResult("info", "info", "Delete  . . . <img src='../images/loaderA32.gif' />");
			 if(confirm("Are you sure you want to delete this glossary term"))
			 {
				var data 	= {};
			 	data.status = 2;
			 	data.id		= glossary.id
				 $.post("../class/request.php?action=Glossary.updateGlossary", { data : data }, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {
						$("#tr_"+glossary.id).fadeOut();
						jsDisplayResult("ok", "ok", response.text);
					}							
				},"json")	
			}
			return false;											
		});
		 		 
	} ,
	
	save 		: function(category)
	{
		var data	 	= {};		
		data.category 	 = $("#category").val();
		data.rules      = $("#txtDefaultHtmlArea").val();
		data.purpose    = $("#purpose").val();
		data.field     = $("#field").val();
		jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
		 $.post("../class/request.php?action=Glossary.saveGlossary", {data : data}, function( response ){
			if( response.error )
			{
			  jsDisplayResult("error", "error", response.text);
			} else {
			   //Glossary.get(category)
			   jsDisplayResult("ok", "ok", response.text);	   
			}
		},"json");		
	},
	
	edit 		: function()
	{
	     var data	 	 = {};		
		data.category 	 = $("#category").val();
		data.rules      = $("#txtDefaultHtmlArea").val();
		data.purpose    = $("#purpose").val();
		data.field      = $("#field").val();
		data.id 		 = $("#id").val();
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");		
		$.post("../class/request.php?action=Glossary.updateGlossary", {data :data }, function( response ){	
		   if(response.error)
		   {
			jsDisplayResult("error", "error", response.text);		
		   } else {					
			jsDisplayResult("ok", "ok", response.text);		
		   }	
		},"json");
	},
	
	addGlossary         : function(category)
	{
	   var self = this;
	   if($("#addglossary").length)
	   {
	     $("#addglossary").remove();
	   }
	   
	   $("<div />",{id:"addglossary"}).append($("<table />",{width:"100%"})
	     .append($("<tr />")
	       .append($("<th />",{html:"Field : "}))
	       .append($("<td />")
	         .append($("<select />",{id:"field", name:"field"}))
	       )
	      )
	     .append($("<tr />")
	       .append($("<th />",{html:"Purpose : "}))
	       .append($("<td />")
	         .append($("<textarea />",{id:"purpose", name:"purpose", cols:80, rows:7}))
	       )
	     )
	     .append($("<tr />")
	       .append($("<th />",{html:"Rules : "}))
	       .append($("<td />")
	         .append($("<textarea />",{id:"rules", name:"rules", cols:80, rows:7}))
	       )	      
	     )
	     .append($("<tr />")
	       .append($("<td />",{colspan:"2"})
	         .append($("<div />",{id:"message"}))
	       )      
	     )	     	   
	   ).dialog({
	      autoOpen : true, 
	      modal    : true,
	      position : "top",
	      title    : "Add Glossary Term",
	      width    : 700,
	      buttons  : {
	                   "Save"     : function()
	                   {
	                      if($("#purpose").val() === "")
	                      {
	                         $("#message").addClass("ui-state-error").html("Purpose is required ");
	                         return false;
	                      } else if($("#rules").val() === ""){
	                         $("#message").addClass("ui-state-error").html("Rules are required");
	                         return false;
	                      } else {
	                         Glossary.save(category);
	                      }
	                   } ,
	                   
	                   "Cancel"    : function()
	                   {
	                      $("#addglossary").remove();
	                      $("#addglossary").dialog("destroy");
	                   }
	      } ,
	      close    : function(event, ui)
	      {
                 $("#addglossary").remove();
                 $("#addglossary").dialog("destroy");	      
	      } ,
	      open     : function(event, ui)
	      {
			var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			var saveButton = buttons[0];
			$(saveButton).css({"color":"#090"});
			var fields = $("body").data("fields");
			if($.isEmptyObject(fields))
			{
			   Glossary.loadFields(category);
			} else {
			    if(fields[category] !== undefined)
			    {
			      Glossary.loadFields(category);
			    } else {
			         $.each(fields[category], function(index, value){
			            $("#field").append($("<option />",{text:value.name, value:value.id}))
			         });     
			    }			     
			}			
	      }
	   });
	},
	
	loadFields               : function(category)
	{
	   var self = this;
	   $("body").data("fields", {});
	   //console.log("Coming here now with data");
	   //console.log($("body").data() );
	   $("#field").empty();
	   $.getJSON("../class/request.php?action=Naming.getNaming",{data:{type:category}}, function(fields){
	     if(!$.isEmptyObject(fields))
	     {
	        var categoryFields = {};  
	        //console.log("category "+category);
	        categoryFields[category] = fields;
	        $("body").data("fields", categoryFields );
	        //console.log("Coming here now with data");
	        //console.log(category);
	        //console.log($("body").data() );
	        $.each(fields, function(index, val){
	          var textVal = (val.client_terminology === "" ? val.ignite_terminology : val.client_terminology);
	          $("#field").append($("<option />",{text:textVal, value:val.id}));
	        })
	     }
	   });
	},
	
	_editGlossary            : function(glossary, category)
	{
	   var self = this;
	   if($("#editglossary").length)
	   {
	     $("#editglossary").remove();
	   }
	   $("<div />",{id:"editglossary"}).append($("<table />",{width:"100%"})
	     .append($("<tr />")
	       .append($("<th />",{html:"Field : "}))
	       .append($("<td />")
	         .append($("<select />",{id:"field", name:"field"}))
	       )
	      )	   
	     .append($("<tr />")
	       .append($("<th />",{html:"Purpose"}))
	       .append($("<td />")
	         .append($("<textarea />",{id:"purpose", name:"purpose", cols:80, rows:7, text:glossary.purpose}))
	       )
	     )
	     .append($("<tr />")
	       .append($("<th />",{html:"Rules"}))
	       .append($("<td />")
	         .append($("<textarea />",{id:"rules", name:"rules", cols:80, rows:7, text:glossary.rules}))
	       )	      
	     )
	     .append($("<tr />")
	       .append($("<td />",{colspan:"2"})
	         .append($("<div />",{id:"message"}))
	       )      
	     )	     	   
	   ).dialog({
	      autoOpen : true, 
	      modal    : true,
	      position : "top",
	      title    : "Update Glossary Terms",
	      width    : 700,
	      buttons  : {
	                   "Save Change"     : function()
	                   {
	                      if($("#purpose").val() === "")
	                      {
	                         $("#message").addClass("ui-state-error").html("Purpose is required ");
	                         return false;
	                      } else if($("#rules").val() === ""){
	                         $("#message").addClass("ui-state-error").html("Rules are required");
	                         return false;
	                      } else {
	                         Glossary.update(glossary.id, category);
	                      }
	                   } ,
	                   
	                   "Cancel"    : function()
	                   {
	                      $("#editglossary").remove();
	                      $("#editglossary").dialog("destroy");
	                   }
	      } ,
	      close    : function(event, ui)
	      {
                 $("#editglossary").remove();
                 $("#editglossary").dialog("destroy");	      
	      } ,
	      open     : function(event, ui)
	      {
			var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			var saveButton = buttons[0];
			$(saveButton).css({"color":"#090"});
			var fields = $("body").data("fields");
			if($.isEmptyObject(fields))
			{
			   Glossary.loadFields(category);
			} else {
			    //console.log("There is somethid there so it doesn't go back to load new");
			    //console.log(category);
			    //console.log( fields );
			    if(fields.hasOwnProperty(category))
			    {
			         $("#field").empty();
			         $.each(fields[category], function(index, value){
			            var textVal = (value.client_terminology === "" ? value.ignite_terminology : value.client_terminology);
			            var selectedVal = (value.id === glossary.field ? "selected" : "");
			            $("#field").append($("<option />",{text:textVal, value:value.id, selected:selectedVal}))
			         });
			    } else {
			      Glossary.loadFields(category);
			    }
			}				
			
	      }
	   })
	}
	
	
	
}
