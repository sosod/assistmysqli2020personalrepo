$(function(){
	
	Menu.get();

});

Menu		= { 
		
	get		: function()
	{
		$.getJSON("../class/request.php?action=Menu.getMenu", function( responseData ){
			Menu.display( responseData );
		});
	} ,
	
	display	: function( data )
	{
		$.each( data, function( ref , menu ){
			$("#menutable")
			.append($("<tr />",{id:"tr_"+menu.id})
                  .bind('mouseenter mouseleave', function() {
                      $(this).toggleClass('tdhover');
                  })		     
			  .append($("<td />",{html:menu.id}))
			  .append($("<td />",{html:menu.ignite_terminology}))
			  .append($("<td />")
				.append($("<input />",{type:"text", id:"menu_"+menu.id, name:"menu_"+menu.id, value:menu.client_terminology, size:50}))	  
			  )
			  .append($("<td />")
				.append($("<input />",{type:"button", id:"update_"+menu.id, name:"update_"+menu.id, value:"Update"}))	  
			  )							  
			)

			
			$("#update_"+menu.id).live("click", function(){
				jsDisplayResult("info", "info", "Updating menu ... <img src='../images/loaderA32.gif' >");
				
				$.post("../class/request.php?action=Menu.updateMenu", { 
						data	: {
									client_terminology	: $("#menu_"+menu.id).val(),
									id 					: menu.id
						}
					}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text );
					} else{
						jsDisplayResult("ok", "ok", response.text );
					}				
				}, "json");
				
				
			    return false;	
			});
			
		});
			
	} ,
	
	update		: function()
	{
		
		
		
	}		
}; 
