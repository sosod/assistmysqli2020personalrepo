$(function(){

    LegislationComplianceStatusPerDeliverable.getFinancialYear(); 
    $("#financialyear").change(function(){
        LegislationComplianceStatusPerDeliverable.get($(this).val());
    }); 



});

var LegislationComplianceStatusPerDeliverable     = 
{    

     getFinancialYear      : function()
     {
       $("#financialyear").append($("<option />",{text:"--please select--", value:""}))   
       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financialyear").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });
     } , 

     get                 : function(financialyear)
     {               
          document.location.href = "compliance_per_deliverable.php?financialyear="+financialyear;
          /*$.get("compliance_per_deliverable.php",{financialyear:financialyear}, function(reportData){
              $("#report").html(reportData);
          },"html");*/
     }



}
