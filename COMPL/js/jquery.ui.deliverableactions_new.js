$.widget("ui.deliverableactions",{
	
	options		: {
		tableId		    : "deliverable_"+(Math.floor(Math.random(55) * 98)), 
		start		       : 0,
		current		    : 1,
		total		       : 0,
		limit 		    : 10,
		legislationId	    : 0,
		usedIds 	 	    : [],
		reference 	    :  0,
		page			    : "",
		section            :  "",
		actionProgress	    : [], 
		details		    :	 false, 
		confirmation	    : false,
		editAction         : false,
		editDeliverable : false, 
		eventActivation : false,
		view			    : "", 
		url				 : "../class/request.php?action=Deliverable.getDeliverables",
		actionUrl           : "../class/request.php?action=Action.getActions",
		eventid			 : 0, 
		triggerBy		 : false, 
		occuranceid		 : 0,
		deleverableActivated : [],
		deleverableDeactivated : [],
		autoLoad               : true,
		checkedDeliverables	   : [],
		uncheckedDeliverables  : []
		
	}	, 
	
	_init		: function()
	{
	   if(this.options.autoLoad)
	   {
	     this._getDeliverable();
	   }
	} , 
	
	_create		: function()
	{
	   var self = this,
	      options = self.options,
	      element = self.element;
	   $(element).append($("<table />", {width:"100%"})
	    .append($("<tr />").css({"display":(options.eventActivation ? "table-row" : "none")})
	      .append($("<th />",{html:"Select event"}).css({"text-align":"left"}))
	      .append($("<td />")
	        .append($("<select />", {id:"events", name:"events"}))
	      )
	    )
	    .append($("<tr />")
	      .append($("<td />",{colspan:"2"})
	        .append($("<table />",{id:options.tableId, width:"100%"}))
	      )
	    )	  
	  )
	  
	  if(options.eventActivation)
	  {
	     self._loadEventOccured();	  
	  }
	  
	  $("#events").live("change", function(e){
	     self.options.occuranceid = $(this).val();
	     self._getDeliverable();
	     $("#message").hide();
	     e.preventDefault();
	  });
	
	} , 
	
	_loadEventOccured   : function()
	{
	   var self = this;
	   var events = $("body").data("events");
	   
	   if(!$.isEmptyObject(events))
	   {
          self._loadSelect("events", events, "name"); 
	   } else {
	      
	      $.getJSON("../class/request.php?action=EventOccuranceManager.getSubEventOccurance",{data : { status : 1 } }, function(events){
              if(!$.isEmptyObject(events))
              {
                $("body").data("events", events);
                self._loadSelect("events", events, "name"); 
              }  	     
	      });
	   }
	},	
	
	_loadSelect          : function(selectBox, data, key, options)
	{
	   var self = this;
	   $("#"+selectBox).append($("<option />",{value:"", text:"--please select--"}))
	   $.each(data, function(index, val){
	     var optionKeyVal = {};
	     if(!$.isEmptyObject(options))
	     {
	        $.each(options, function(optionIndex, optionKey){
	          if(val.hasOwnProperty(optionKey))
	          {
	             optionKeyVal[optionIndex] = val[optionKey];
	          }
	        });   
	     }	   
	     $("#"+selectBox).append($("<option />",{value:val.id, text:val[key]}).attr(optionKeyVal))	     
	   });
	},	
	
	_getDeliverable	: function()
	{
		var self = this,
		    options = self.options;
		$(self.element).append($("<div />",{id:"loadingDiv", html:"Loading deliverable actions... <img src='../images/loaderA32.gif' />"})
			.css({position:"absolute", "z-index":"9999",	top:"250px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
		)		
		$.getJSON(self.options.url ,{
		 options 	:{
			start     		: options.start,
			limit 			: options.limit,
			legislation_id  : options.legislationId,
			reference       : options.reference,
			page            : options.page,
			event     		: options.eventid, 
			view 			: options.view,
			occuranceid		: options.occuranceid,
			section         : options.section
		   }
		}, function( deliverableResponse ) {	
			//console.log("Checked deliveliverable that were once unchecked");
			//console.log( self.options.checkedDeliverables );
			//console.log("Unchecked deliverables that were once checked");
			//console.log( self.options.uncheckedDeliverables );
		     $("#loadingDiv").remove();
               if($("#"+options.tableId).length > 0)
               {
                  $("#"+options.tableId).html(""); 
               } else {
	             self._createTable();                     
               }		     	
		     options.actionProgress = deliverableResponse.actionProgress;
			self._displayPaging( deliverableResponse.total, deliverableResponse.columns +1 )
			self._displayHeaders( deliverableResponse.headers )
			options.total   = deliverableResponse.total;
			if($.isEmptyObject(deliverableResponse.deliverable ))
			{
			     var cols = deliverableResponse.columns;
			     if(options.eventActivation)
			     {
			       cols = cols + 3;
			     } else if(options.confirmation){
			       cols = cols + 4;
			     }
				$("#"+options.tableId).append($("<tr />")
				  .append($("<td />",{colspan:cols, html:"There are no deliverables"}))
				);
			} else {
				options.usedIds = deliverableResponse.usedId; 
				self._display(deliverableResponse.deliverable, deliverableResponse.columns, deliverableResponse.hasActions, deliverableResponse.deliverablesId, deliverableResponse.events);
			}				
		});				
	} , 
	
	_createTable   : function()
	{
	   var self = this;
	   $(self.element).append($("<table />",{id:self.options.tableId}))
	} ,
	
	_display		: function( deliverables, cols, hasActions, deliverablesId, events )
	{	
		var self = this;		
		$.each( deliverables , function( index, del ){
			var tr = $("<tr />").hover(function(){
				$(this).addClass("tdhover")
			}, function(){
				$(this).removeClass("tdhover")				
			})
						
			tr.append($("<td />")
				.append($("<a />",{href:"#", title:"View Actions", id:"less_"+index})
				  .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus"))		
				).click(function(e) {
				     if( $("#childtr_"+index).is(":hidden") )
				     {
					      $("#less_"+index+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");					
				     } else {
					     $("#less_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");					
				     }
				     $("#childtr_"+index).toggle();
				   //e.stopImmediatePropagation();
				   e.preventDefault();
				})
			)
			
			//condition to disable the  make delieverable active checkbox
			var deliverableActivate = false;
			if(deliverablesId.hasOwnProperty(index))
			{
				if((deliverablesId[index]['status'] & 512) == 512)
				{
					 deliverableActivate = true;
				}
			
				if((deliverablesId[index]['status'] & 2048) == 2048)
				{
					 deliverableActivate = true;
				}
				if(deliverableActivate == true)
				{
					tr.append($("<td />",{id:"appendcheckbox_"+index}).css({display:(self.options.confirmation ? "table-cell" : "none")})
						.append($("<input />",{type: "checkbox", name:"deliverable_activate_"+index, id: "deliverable_activate_"+index,	value:index, checked:"checked", disabled:"disabled"})
						  .addClass("deliverableactivation")
							  .change(function(e){
									if($("#deliverable_activate_"+index).is(":checked"))
									{
									   if(self._indexOf(self.options.uncheckedDeliverables, index) > -1)
									   {
										  self.options.uncheckedDeliverables.splice(self._indexOf(self.options.uncheckedDeliverables, index), 1)
									   }
									   self.options.checkedDeliverables.push(index);
									   self.options.deleverableActivated.push(index); 
									} else {
										if(self.options.checkedDeliverables.indexOf(index) > -1)
										{
											self.options.checkedDeliverables.splice(self._indexOf(self.options.checkedDeliverables, index), 1)
										}	
										self.options.uncheckedDeliverables.push(index);
										self.options.deleverableDeactivated.push(index);
									}
									e.preventDefault();
							  })							  
						)
					)	
				} else {
					//console.log( typeof self.options.uncheckedDeliverables );
					//console.log(index)
					//console.log( self._indexOf(self.options.uncheckedDeliverables, index) );
					//console.log("\r\n");
					if(self._indexOf(self.options.uncheckedDeliverables, index) > -1)
					{
						tr.append($("<td />",{id:"appendcheckbox_"+index}).css({display:(self.options.confirmation ? "table-cell" : "none")})
							.append($("<input />",{type:"checkbox", name:"deliverable_activate_"+index, id: "deliverable_activate_"+index, value:index})
							  .addClass("deliverableactivation")
							  .change(function(e){
									if($("#deliverable_activate_"+index).is(":checked"))
									{
									   if(self._indexOf(self.options.uncheckedDeliverables, index) > -1)
									   {
											self.options.uncheckedDeliverables.splice(self._indexOf(self.options.uncheckedDeliverables, index), 1)
									   }
									   self.options.checkedDeliverables.push(index);
									   self.options.deleverableActivated.push(index); 
									} else {
										if($.inArray(index, self.options.checkedDeliverables) > -1)
										{
											self.options.checkedDeliverables.splice(self._indexOf(self.options.checkedDeliverables, index), 1);
										}	
										self.options.uncheckedDeliverables.push(index);
										self.options.deleverableDeactivated.push(index);
									}
									e.preventDefault()
							  })						  
							)
						)
					} else {
						tr.append($("<td />",{id:"appendcheckbox_"+index}).css({display:(self.options.confirmation ? "table-cell" : "none")})
							.append($("<input />",{type:"checkbox", name:"deliverable_activate_"+index, id: "deliverable_activate_"+index, value:index, checked:"checked"})
							  .addClass("deliverableactivation")
							  .change(function(e){
									if($("#deliverable_activate_"+index).is(":checked"))
									{
									   if($.inArray(index, self.options.uncheckedDeliverables) > -1)
									   {
											self.options.uncheckedDeliverables.splice(self.options.uncheckedDeliverables.indexOf(index), 1)
									   }
									   self.options.checkedDeliverables.push(index);
									   self.options.deleverableActivated.push(index); 
									} else {
										if($.inArray(index, self.options.checkedDeliverables) > -1)
										{
											self.options.checkedDeliverables.splice(self.options.checkedDeliverables.indexOf(index), 1)
										}	
										self.options.uncheckedDeliverables.push(index);
										self.options.deleverableDeactivated.push(index);
									}
									e.preventDefault()
							  })							  
							)
						)							
					}					
				}
			}
			
			tr.append($("<td />").css({display:(self.options.eventActivation ? "table-cell" : "none")})
				.append($("<input />",{
									type	   : "checkbox",
									name	   : "event_activation_"+index,
									id	      : "event_activation_"+index,
									checked  : "checked",
									value    : index 
									}).addClass("eventactivation"))
			)
			var url = "";
			if(self.options.confirmation)
			{
			   self.options.actionUrl = "../class/request.php?action=Action.getActions";
			} else {
			  self.options.actionUrl = self.options.actionUrl;
			}			
			var childtr = $("<tr />",{id:"childtr_"+index}).css({"display":(self.options.details ? "table-row" : "none")})
			
			childtr.append($("<td />",{html:"&nbsp;"}).addClass("noborder").append($("<span />").addClass("ui-icon").addClass("ui-icon-carat-1-sw")))		
			childtr.append($("<td />",{colspan : cols + 1, id:"childtable_"+index}))
			
			$.each(del, function(key, val){
				tr.append($("<td />",{html:(key == "action_progress" ? val+"%" : " "+val)}));
			});
			tr.append($("<td />",{html:(events[index] == undefined ? "" : events[index])}).css({"display" : ( self.options.triggerBy ? "table-cell" : "none") }))
			tr.append($("<td />").css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") })
			    .append($("<input />",{type:"button", id:"editdeliverable_"+index, name:"editdeliverable_"+index, value:"Edit Deliverable "})
			              .addClass("imports")
			    		)	
			    .append($("<br />"))
			    .append($("<button />",{
			                            text: "Quick Edit",
			                            id  : "quickeditdeliverable_"+index
			                            }).button({icons:{primary:"ui-icon-pencil"}})
			                            .click(function(e) {
                                              self._editDeliverable(index, 1); 
                                              e.preventDefault();
                                            })
			    )
			)					
			$.getScript("../js/jquery.ui.action.js", function(){
				$("#childtable_"+index).action({deliverableId:index, section:self.options.section, thClass:"th2", editAction:self.options.editAction, eventActivation:self.options.eventActivation, occuranceid:self.options.occuranceid, url:self.options.actionUrl, lowerLimit:0, upperLimit:10, currentPage:1, view:self.options.view});					
			});
			
			$("#editdeliverable_"+index).live("click", function(){
			
				//self._editDeliverable(index, 0);
				if(self.options.eventActivation)
				{
				  document.location.href = "edit_deliverable.php?id="+index+"&eventoccuranceid="+self.options.occuranceid;								
				} else {
					document.location.href = "edit_deliverable.php?id="+index;				
				}
				return false;
			});

			$("#"+self.options.tableId).append( tr );
			$("#"+self.options.tableId).append( childtr );

			
			/*
			$(".deliverableactivation").each(function( index, element ){
				//$(this).attr("checked", "checked");		
			});
			$(".eventactivation").each(function( index, element ){
				//$(this).attr("checked", "checked");		
			});			
			*/
		});
		
		//$(".eventactivation").remove();
		$("#"+self.options.tableId).append($("<tr />").css({display:(self.options.eventActivation ? "table-row" : "none")})
	        .append($("<td />",{colspan:parseInt(cols)+1}).css({"text-align":"right"}).addClass("noborder")
	        .append($("<input />",{type:"button", name:"activate", id:"activate", value:"Confirm and Activate"}).addClass("isubmit"))
	       )
	     )	
	
		
		//.append($("<table />").addClass("noborder").addClass("eventactivation")
	
		//)
      
		$("#"+self.options.tableId).append($("<tr />").css({"display" : (self.options.confirmation ? "table-row" : "none")})
		   .append($("<td >",{colspan:cols + 4}).css({"text-align":"left"})
			 .append($("<input />",{
				type	: "button",
				name	: "confirm_activation_"+self.options.legislationId,
				id		: (hasActions ? "confirm_activation_"+self.options.legislationId : "confirm_activation"),
				value	: "Confirm and Request Activation"
				}))	   
		   )	
		)      
		
      $("#activate").click(function(){
		    $("<div />",{id:"activate_eventdeliverable"})
		    .append($("<p />")
		      .append($("<span />",{html:"You are now activating the deliverables checked for this event and deliverable unchecked will be inactive"}))
		    ).dialog({
		              autoOpen  : true, 
		              modal     : true, 
		              title     : "Activate Deliverables",
		              position  : "top",
		              buttons   : {
		                            "Activate"   : function()
		                            {
		                               var _eventDeliverable =  [];
		                               var eventDeliverable =  [];
		                               var eDel  = [];
		                               var _eDel = [];
		                               $(".eventactivation").each(function( index, deliverable){
		                                  if($(this).is(":checked"))
		                                  {
		                                     eDel.push( this.id );
		                                  } else {
		                                     _eDel.push( this.id );
		                                  }
		                               });
		                               jsDisplayResult("info", "info", "Activating deliverables . . . <img src='../images/loaderA32.gif' />");
		                               $.post("../class/request.php?action=AdminDeliverable.activateDeliverable", 
		                               {
		                                   data: {checked:eDel, unchecked:_eDel, eventoccuranceid:self.options.occuranceid}
		                               },function( response ){
		                                 if( response.error)
		                                 {
		                                   jsDisplayResult("error", "error", response.text);
		                                 } else {
		                                   jsDisplayResult("ok", "ok", response.text);
		                                   $("#activate").attr("disabled", "disabled");
		                                 } 
		                               },"json")                      
		                                  $("#activate_eventdeliverable").dialog("destroy")             
		                                  $("#activate_eventdeliverable").remove();                                                   
		                            } , 
		                            "Cancel"       : function()
		                            {
		                               $("#activate_eventdeliverable").dialog("destroy")             
		                               $("#activate_eventdeliverable").remove();               
		                            }
		              }
		    });		
		})
	
		if(!hasActions)
		{
			$("input[name='confirm_activation_"+self.options.legislationId+"']").attr("disabled", "disabled")
		}
		
		$("#confirm_activation_"+self.options.legislationId).live("click", function(){
			
			$("<div />",{html:"Confirmation of legislation", id:"confirmationdialog_"+self.options.legislationId})
			 .dialog({
			 		autoOpen	: true,
			 		modal		: true,
			 		position	: "top",
			 		width 	: "500px",
			 		title		: "Confirmation of legislation",
			 		buttons		: {
			 						"Confirm"	: function()
			 						{
				 						var data = {};
										data.id  = self.options.legislationId;
										data.activated   = self.options.deleverableActivated; 
										data.deactivated = self.options.deleverableDeactivated
										data.ids 		  = deliverablesId;											

										jsDisplayResult("info", "info", "Confirming  . . . .<img src='../images/loaderA32.gif' />");
										$.post("../class/request.php?action=Legislation.confirmLegislation",
										{ data : data }, function( response) {
											if( response.error )
											{
												jsDisplayResult("error", "error", response.text);
											} else{
												alert( response.text )
												document.location.href = "confirmation.php";
												jsDisplayResult("ok", "ok", response.text);				
											}											
				 							$("#confirmationdialog_"+self.options.legislationId).dialog("destroy")
				 							$("#confirmationdialog_"+self.options.legislationId).remove()
										}, "json") 
			 						} ,
			 						
			 						"Cancel"	: function()
			 						{
		 							$("#confirmationdialog_"+self.options.legislationId).dialog("destroy")
		 							$("#confirmationdialog_"+self.options.legislationId).remove()			
			 						}
			 
		 						}	
			 })
			
			return false;
		});		
		
	} , 
	
	_displayHeaders	: function( headers )
	{
		var self = this;
		var tr   = $("<tr />");
		tr.append($("<th />",{html:"", width:"5px"}))
		tr.append($("<th />",{html:"Make Deliverable Active"}).css({display:(self.options.confirmation ? "table-cell" : "none")}))		
		tr.append($("<th />",{html:"Make Deliverable Active"}).css({display:(self.options.eventActivation ? "table-cell" : "none")}))
		$.each( headers, function( index, head ){
			tr.append($("<th />",{html:head}))			
		});		
		tr.append($("<th />",{html:"Triggered By"}).css({"display" : (self.options.triggerBy ? "table-cell" : "none") }))
		tr.append($("<th />").css({"display" : ( self.options.editDeliverable ? "table-cell" : "none") }))
		$("#"+self.options.tableId).append( tr );
	} , 
	
	_displayPaging 		: function( total,columns ) {
		var self = this;
		
		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		$("#"+self.options.tableId)
		  .append($("<tr />")
			.append($("<td />",{colspan:columns})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < "}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| "}))			   
		   )
			.append($("<td />",{html:"&nbsp;"}).css({display:(self.options.confirmation ? "table-cell" : "none")}))				   
		    .append($("<td />",{html:"&nbsp;"}).css({"display" : ( self.options.triggerBy ? "table-cell" : "none") }))
		    .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.eventActivation ? "table-cell" : "none")}))
	   	 .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.editActions ? "table-cell" : "none")}))
			 .append($("<td />",{html:"&nbsp;"}).css({display:(self.options.addActions ? "table-cell" : "none")}))
			 .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({display:(self.options.assuranceDeliverable ? "table-cell" : "none")}))
			 .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({display:(self.options.editDeliverable ? "table-cell" : "none")}))
			 .append($("<td />",{html:"&nbsp;", colspan:"2"}).css({display:(self.options.updateDeliverable ? "table-cell" : "none")}))
			 .append($("<td />",{html:"&nbsp;", colspan:"1"}).css({display:(self.options.approval ? "table-cell" : "none")}))
		  )
		if(self.options.current < 2)
		{
              $("#first").attr('disabled', 'disabled');
              $("#previous").attr('disabled', 'disabled');		     
		}
		if((self.options.current == pages || pages == 0))
		{
              $("#next").attr('disabled', 'disabled');
              $("#last").attr('disabled', 'disabled');		     
		}		
		$("#next").bind("click", function(evt){
			self._getNext( self );
			evt.stopImmediatePropagation();
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
			evt.stopImmediatePropagation();
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
			evt.stopImmediatePropagation();
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
			evt.stopImmediatePropagation();
		});			
	} ,		
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getDeliverable();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getDeliverable();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getDeliverable();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getDeliverable();				
	} , 
	
	_editDeliverable 		: function(index, quickEdit)
	{
		var self = this;
		if($("#editdeliverabledialog_"+index).length > 0)
		{
			$("#editdeliverabledialog_"+index).remove();
		}
				
		$.get("../class/request.php?action=Deliverable.getEditDeliverableForm",{
			data : {deliverableid : index, quickEdit:quickEdit}
		}, function( response ) {
			$("#editdeliverabledialog_"+index).html("");
			$("#editdeliverabledialog_"+index).remove();
			$("<div />",{id:"editdeliverabledialog_"+index, html:response})
				.dialog({
					autoOpen		: true, 
					modal 		: true,
					width 		: "auto",
					position		: "top", 
					title 		: "Edit Deliverable Ref : "+index,
					buttons 		: {
								"Save Changes"		: function()
								{
								    
									var data 				   = {};
									data.description 		   	= $("#description").val();
									data.short_description	   = $("#short_description").val();
									data.legislation_section   = $("#legislation_section").val();
									data.compliance_frequency  = $("#compliance_frequency  :selected").val();
									data.applicable_org_type   = $("#applicable_org_type :selected").val();
									data.compliance_date  	   = $("#compliance_date").val();
									data.functional_service    = $("#functional_service :selected").val();
									data.accountable_person    = $("#accountable_person").val();
									data.responsibility_owner  = $("#responsibility_owner :selected").val();
									data.responsible           = $("#responsible :selected").val();
									data.legislation_deadline  = $("#legislation_deadline").val();
									data.reminder 	            = $("#reminder").val();
									data.sanction              = $("#sanction").val();
									data.reference_link        = $("#reference_link").val();
									data.assurance             = $("#assurance").val();
									data.compliance_to         = $("#compliance_to").val();
									data.main_event            = $("#main_event :selected").val();
									data.sub_event             = $("#sub_event").val();
									data.guidance              = $("#guidance").val();
									data.recurring			      = $("#recurring :selected").val();
									data.actionowner    	      = $("#actionowner").val();
									data.recurring_type		   = $("#recurring_type :selected").val();         
									data.id 				   		= index;		
									data.reporting_category    = $("#reporting_category :selected").val();
									if($("#confirmavailable").is(":checked"))
									{
										data.confirmavailable 		= 1;
									}
									if(data.short_description === "") {
									   $("#message").addClass("ui-state-error").html("Please enter the short description")
									   return false;
									} else if( data.legislation_section === "") {
									   $("#message").addClass("ui-state-error").html("Please select the legislation section" )
									   return false
									} else if( data.compliance_frequency === "") {
									   $("#message").addClass("ui-state-error").html("Please enter the deliverable compliance frequency")
									   return false		
									} else if( $("#recurring :selected").val() === "") {
									   $("#message").addClass("ui-state-error").html("Please enter the legislative compliance date");
									   return false				
									} else if( data.accountable_person === null) {
									   $("#message").addClass("ui-state-error").html("Please select the accountable person");
									   return false
									} else if( data.responsibility_owner === "" ) {
									   $("#message").addClass("ui-state-error").html("Please select the responsible person");
									   return false
									} else if( data.compliance_to === null) {
									   $("#message").addClass("ui-state-error").html("Please select compliance given to");
									   return false		
									} else {
										$("#message").addClass("ui-state-error").html("Updating  . . . <img src='../images/loaderA32.gif' />");
										$.post("../class/request.php?action=ClientDeliverable.editDeliverable",
									 		{ data: data }, function( response ){
												if( response.updated ){
													if( response.error)
													{
                                                                      $("#message").addClass("ui-state-error").html(response.text);
													} else {
														jsDisplayResult("ok", "ok", response.text);
													}	
													self._getDeliverable();
												} else {					
													$("#message").addClass("ui-state-error").html(response.text);
												}
											} ,"json");
									}
									$("#editdeliverabledialog_"+index).dialog("destroy");
									$("#editdeliverabledialog_"+index).remove();  												
								} ,
								"Cancel"				: function()
								{
									$("#editdeliverabledialog_"+index).dialog("destroy");
									$("#editdeliverabledialog_"+index).remove();
								}						
					}	,
					open 			: function( event , ui)
					{
						var dialog = $(event.target).parents(".ui-dialog.ui-widget");
						var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
						var saveButton = buttons[0];
						$(saveButton).css({"color":"#090"});
						
						//$("#confrimavailable").bind("change", function(){
							//console.log( $("#confrimavailable").is("checked") );							
						//});
			
			               $(".datepicker").datepicker({
				                 showOn: 'both',
				                 buttonImage: '/library/jquery/css/calendar.gif',
				                 buttonImageOnly: true,
				                 dateFormat: 'dd-M-yy',
				                 changeMonth:true,
				                 changeYear:true		
			                });								
						
						
						$("#recurring_type").bind("click", function(){
							if( $(this).val() == "fixed"){
								$(".fixed").show();			
								$("#days").hide()
							} else {
								$(".fixed").hide();
								$("#days").show();
							}		
							return false;
						});

						$("#recurring").bind("change",function() {
							if( $(this).val() == "") {  
								$("#more_details").hide();
								$(".recurr").hide();
							} else if( $(this).val() == 0) {
								$(".recurr").hide();
								$(".fixed").show();
							} else {
								$(".fixed").hide();
								$(".nonfixed").show();
								$(".recurr").show();
							}
							return false;
						});
					
					    $("#main_event").bind("change", function(){
					    		var id = $(this).val()
								$("#sub_event").empty();
								  var data	= {};
								  data.eventid = id;
								  data.status  = 1;
								$.post("../class/request.php?action=SubeventManager.getAll",
									{data:data}, function( sub_events ){
									$.each( sub_events, function( index, sub_event ){
										$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
									});
									if(id == 1)
									{
										$("#sub_event option[value=1]").attr("selected", "selected")
									}
								}, "json");
							  return false;
						 });
					
					}	
				});
		},"html");
	},
	
	_indexOf				: function(arrayData, obj)
	{
        for(var i = 0; i < arrayData.length; i++)
        {
            if(arrayData[i] == obj)
            {
               return i;     
            }
        }
       return -1;
	} 
});
