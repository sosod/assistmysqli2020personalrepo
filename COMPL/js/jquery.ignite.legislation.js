$.widget("ui.legislation",{

	options		: {
	     tableRef 			     : "legislation_"+Math.floor(Math.random(57)*98),
	     url 				     : "../class/request.php?action=Legislation.getClientLegislations",
	     deliverableUrl 	     : "../class/request.php?action=Deliverable.getDeliverables",
	     start 				     : 0,
	     limit				     : 10,
	     total 				     : 0,
	     current				 : 1,
	     page				     : "",
	     section                 : "",
	     legoption   		     : "",
	     legStatus		 	     : [],
 	     usedIds 			     : [],
	     importDeliverable	     : false,
	     importLegislation	     : false,
	     createDeliverable	     : false,
	     addDeliverable          : false,
	     addAction			     : false,
	     editLegislation	     : false,
	     editDeliverable	     : false,
	     updateLegislation	     : false,
	     updateDeliverable	     : false,
	     assuranceLegislation    : false,
	     assuranceDeliverable    : false,
	     confirmLegislation	     : false,
	     setupActivate		     : false,
	     setupLegislation	     : false,
	     setupEdit		   	     : false,
	     viewDetails			 : false,
	     approvalUpdate		     : false,
	     reassignDeliverable	 : false,
	     reassignActions         : false,
	     adminPage               : false,
	     user				     : "",
	     approval			     : false,
	     viewType                : "mine",
	     displayBtn              : {},
	     statusDescription       : {},
	     financialyear           : 0,
	     financialYearsList      : [],
	     legislationId           : 0,
	     showFinancialYear       : true,
	     copyFinancialYear       : false,
	     tofinancialyear         : 0,
	     autoLoad                : true
	} , 
	
	_init               : function()
	{
	    if(this.options.autoLoad)
	    {
	        this._get();
	    }
	    
	} ,
	
	_create             : function()
	{
	    var self = this;
	    var html = [];
	    
	    
	
	} ,
	
	_get                : function()
    {
    
    
    } ,
    
    _display            : function()
    {
    
    
    }
  

});
