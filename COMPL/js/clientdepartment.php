<?php
class Clientdepartments
{
     
     function getClientDepartments()
     {
	    //main departments
	    $deptObj = new MasterMasterDepartment();
	    $mainDepartments = $deptObj ->fetchAll();
	    //sub departments 
	    $subdeptObj = new MasterSubDepartment();
	    $subdepartments = $subdeptObj -> fetchAll();

	    $departments = array();
	    foreach($mainDepartments as $cIndex => $mDept)
	    {
            foreach($subdepartments as $sIndex => $sDept)
	       {	
		      if($sDept['dirid'] === $mDept['id'])
		      {
		         $departments[$sDept['id']] = array("name" => $mDept['name']." - ".$sDept['name'] );
		      }
	        }
	    }    
	    return $departments; 
     }


}
?>
