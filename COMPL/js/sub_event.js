// JavaScript Document
$(function(){
	 if($("#eventid").val() == undefined){
		 SubEvents.get()  
	 	 SubEvents.getEvents()  
	 }
	 
	$("#save").click(function(){
		SubEvents.save();
		return false;
	});
	
	$("#edit").click(function(){
		SubEvents.edit();	
		return false;
	});
	
	$("#update").click(function(){
		SubEvents.update();	
		return false;
	});	
});

var SubEvents 	= {
		
	getEvents 		: function()
	{
		$.post("../class/request.php?action=EventManager.getAll", { active : 1},function( response ){
			$.each( response, function( index, events){
				$("#main_event").append($("<option />",{text:events.name, value:events.id}))
			})												  											  
		},"json");
	}  ,
		
	get 		: function()
	{
		$.post("../class/request.php?action=SubEvent.getAll", function( response ){
			$.each( response, function( index, subevent ){
				SubEvents.display( subevent )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.main_event	 = $("#main_event :selected").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the sub event name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else if( data.main_event == ""){
			jsDisplayResult("error", "error", "Please select the main event");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientSubEvent.saveSubEvent", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					data.id  	= response.id; 	
					data.status = 1; 	
					data.main_event = $("#main_event :selected").text();; 	
					SubEvents.display( data )																   
					SubEvents.empty( data );
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {}; 
		data.name 		 = $("#name").val();
		data.main_event	 = $("#main_event :selected").val();		
		data.description = $("#description").val();
		data.id 		 = $("#eventid").val()
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the sub event name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else if( data.main_event == ""){
			jsDisplayResult("error", "error", "Please enter the main event");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ClientSubEvent.updateSubEvent", { data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var data	 = {}
		data.status	 = $("#status :selected").val()
		data.id 	 = $("#eventid").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("../class/request.php?action=ClientSubEvent.updateSubEvent", {  data: data }, function( response ){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");
	} , 
	
	display		: function( subevent )
	{
		$("#event_table")
		 .append($("<tr />",{id:"tr_"+subevent.id})
		   .append($("<td />",{html:subevent.id}))
		   .append($("<td />",{html:subevent.name}))
		   .append($("<td />",{html:subevent.description}))
		   .append($("<td />",{html:subevent.event}))
		   .append($("<td />")
			  .append($("<input />",{
				  					 type		: "button",
				  					 name		: "edit_"+subevent.id,
				  					 id			: (subevent.imported == true ? "edit" : "edit_"+subevent.id),
				  					 value		: "Edit"
				  					}))
			  .append($("<input />",{
				  					  type		: "button",
				  					  name		: "del_"+subevent.id,
				  					  id		     : ((subevent.imported || subevent.used) == true ? "del" : "del_"+subevent.id),
				  					  value		: "Del"
				  					}))			  
			)
		   .append($("<td />")
			  .append($("<span />",{html:((subevent.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					 type		: "button", 
				  					 name		: "change_"+subevent.id,
				  					 id			: (subevent.imported == true ? "change" : "change_"+subevent.id),
				  					 value		: "Change Status"
				  					}))			  					
		    )		   
		 )
		
		 if(subevent.imported == true)
		 {
		     $("input[name='change_"+subevent.id+"']").attr('disabled', 'disabled')
		     $("input[name='edit_"+subevent.id+"']").attr('disabled', 'disabled')
		     $("input[name='del_"+subevent.id+"']").attr('disabled', 'disabled')
		 }
		
		 		 
		 $("#edit_"+subevent.id).live("click", function(){
			document.location.href = "edit_subevent.php?id="+subevent.id;								   
			return false;								   
		  });
		 
		 $("#del_"+subevent.id).live("click", function(){
			var data 	= {};
			data.id  	= subevent.id ;
			data.status = 2;
			if( confirm("Are you sure you want to delete this event")){
			     jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
			     $.post("../class/request.php?action=ClientSubEvent.updateSubEvent",{ data : data }, function( response ){
				     if( response.error ){
					     jsDisplayResult("error", "error", response.text);		
				     } else {
					     $("#tr_"+subevent.id).fadeOut();
					     jsDisplayResult("ok", "ok", response.text);
				     }
			     }, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+subevent.id).live("click", function(){
			document.location.href = "change_subevent.php?id="+subevent.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
