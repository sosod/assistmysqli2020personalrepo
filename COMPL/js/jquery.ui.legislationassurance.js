$.widget("ui.legislationassurance", {
	
	options		: {
			start		: 0,
			limit		: 10, 
			current		: 1,
			total		: 0,
			id			: 0,
			toDay		: ""
	} , 
	
	_init			: function()
	{
		this._getLegislationAssurance();		
	} , 
	
	_create			: function()
	{

		
		var month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
			   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
			   11 : "Nov", 12 : "Dec"
			};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
			min = "0"+min;				
		} else{
			min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min;	 
			
					
	}, 
	
	_getLegislationAssurance		: function()
	{
		var self = this;
		$.getJSON("../class/request.php?action=AssuranceLegislation.getLegislationAssurance", {
			start		  : self.options.start,
			limit		  : self.options.limit,
			legislationId : self.options.id
		}, function(responseData) {
			$(self.element).html("");
			$(self.element).parent().children("h2").remove();
			$(self.element).parent().prepend($("<h2 />",{html:"Legislation Assurance"}));
			self._displayPaging( responseData.total );
			self._displayHeaders( );	
			if( $.isEmptyObject(responseData.data))
			{			
			     $(self.element).append($("<tr />")
			       .append($("<td />",{colspan:"7", html:"There are no legislation assurances"}))
			     )	
			} else {
				self._display(responseData.data)
			}	
			self._displayAddNew()
		});
	} , 
	
	_display						: function( legislations )
	{
		var self = this;
		$.each( legislations, function(index, legislation) {
			var tr = $("<tr />",{id:"tr_assurance_"+legislation.id})
			$.each(legislation, function(key, val) {
				if(key == "insertdate"){
					tr.append($("<td />",{html:self.options.toDay}))
				} else if(key == "attachment") {
					if($.isEmptyObject(val))
					{
						
					}
				} else  {
				  tr.append($("<td />",{html:(key == "sign_off" ? (val == 1 ? "Yes" : "No") : val)}))
				}
			})
			tr.append($("<td />")
				.append($("<input />",{type:"button", name:"edit_"+index, id:"edit_"+index, value:"Edit"}))
				.append("&nbsp;&nbsp;&nbsp;")
				.append($("<input />",{type:"button", name:"del_"+index, id:"del_"+index, value:"Del"}))
			)
			
			$("#del_"+index).live("click", function(){
				if(confirm("Are you sure you want to delete this Legislation assurance?"))
				{
					jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
					//$.post("../class/request.php?action=AssuranceLegislation.updateLegislationAssurance", { data : { id : legislation.id, legislation_id: legislation.self.options.id, status : 2} }, function( response ){
					var result = AssistHelper.doAjax("../common/common_controller.php?action=updateLegislationAssurance",{ id : legislation.id, status : 2, legislation_id : self.options.id});
						if( result.error ){
							jsDisplayResult("info", "info", result.text);
						} else {
							$("#tr_assurance_"+legislation.id).fadeOut();
							jsDisplayResult("ok", "ok", result.text);
						}						
					//},"json")					
				}
				return false;
			});
			
			
			
			$("#edit_"+index).live("click", function(){
				//alert((legislation.sign_off==0)+" => "+(legislation.sign_off=="0")+" versus "+(legislation.sign_off==1)+" => "+(legislation.sign_off=="1"));
				if($("#edit_new_assurance").length > 0)
				{
				  $("#edit_new_assurance").remove();
				}
				
				$("<div />",{id:"edit_new_assurance"})
				 .append($("<table />",{class:"form"})
				    .append($("<tr />")
				       .append($("<th />",{html:"Response:"}))
				       .append($("<td />")
				    	  .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7", text:legislation.response}))	   
				       )
				    )
				   .append($("<tr />")
					  .append($("<th />",{html:"Sign Off:"}))
					  .append($("<td />")
						 .append($("<select />",{id:"signoff", name:"signoff"})
						   .append($("<option />",{text:"No", value:"0"}))
						   .append($("<option />",{text:"Yes", value:"1"}))
						 )	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<th />",{html:"Date Tested:"}))
					 .append($("<td />")
						.append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:legislation.date_tested, readonly:"readonly"}).addClass("datepicker"))	 
					  )
				   )
				 )
				 .dialog({
				 	  modal		: true, 
				 	  autoOpen	: true, 
				 	  title		: "Edit Legislation Assurance",
				 	  position	: "top",
					  width		: "500px",
				 	  buttons	     : {
		 					"Save Changes" 			: function()
		 					{
		 						
		 						if( $("#response").val() == "")
		 						{
		 							jsDisplayResult("error", "error", "Plese enter the response");
		 							return false;
		 						} else {
		 							jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
		 							$.post("../class/request.php?action=AssuranceLegislation.updateLegislationAssurance",{
			 							data : { id : legislation.id, response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val(),legislation_id : self.options.id } },
		 							function( response ){
			 							if(response.error){
			 								jsDisplayResult("error", "error", response.text);
			 							} else {
			 								self._getLegislationAssurance()
			 								jsDisplayResult("ok", "ok", response.text);
			 							}				 								
		 							},"json")
		 							$("#edit_new_assurance").dialog("destroy")
									$("#edit_new_assurance").remove()
		 						}
		 						
		 					} , 
		 					
		 					"Cancel"		: function()
		 					{
		 						$("#edit_new_assurance").dialog("destroy")
								$("#edit_new_assurance").remove()
		 					}
	 					  } ,
	 			     close          : function(event, ui)
	 			     {
 						$("#edit_new_assurance").dialog("destroy")
						$("#edit_new_assurance").remove()		 			      
	 			     } ,
	 			     open           : function(event, ui)
	 			     {
                              var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                              var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                              var saveBtn = buttons[0];
                              $(saveBtn).css({"color":"#090"});
	 			     }
				 });
				$(".datepicker").datepicker({
			        showOn: 'both',
			        buttonImage: '/library/jquery/css/calendar.gif',
			        buttonImageOnly: true,
			        dateFormat: 'dd-M-yy',
			        changeMonth:true,
			        changeYear:true		
			    });
				$("#signoff").val(legislation.sign_off);
				return false;
			});
			$(self.element).append( tr );
		});
	
		
	} , 
	
	_displayHeaders					: function()
	{
		var self = this;
		/*$.each( headers , function( key, head){
			tr.append($("<th />",{html:head}))			
		});*/
		$(self.element).append($("<tr />")
		    .append($("<th />",{html:"Ref"}))
		    .append($("<th />",{html:"System Date and time"}))
		    .append($("<th />",{html:"Date Tested"}))
		    .append($("<th />",{html:"Response"}))
		    .append($("<th />",{html:"Sign Off"}))
		    .append($("<th />",{html:"Assurance By"}))
		    .append($("<th />",{html:"&nbsp;"}))	    
		);		
	} ,
	
	_displayPaging 		: function( total ) {
		var self = this;

		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		//$("#"+self.options.tableId)
		$(self.element)
		  .append($("<tr />")
			.append($("<td />",{colspan:7})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < "}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| "}))			   
		   )	
		  )
		if(self.options.current < 2)
		{
              $("#first").attr('disabled', 'disabled');
              $("#previous").attr('disabled', 'disabled');		     
		}
		if((self.options.current == pages || pages == 0))
		{
              $("#next").attr('disabled', 'disabled');
              $("#last").attr('disabled', 'disabled');		     
		}		
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});			
	} ,		
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getLegislationAssurance();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getLegislationAssurance();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getLegislationAssurance();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getLegislationAssurance();				
	} ,
	
	_displayAddNew					: function()
	{
		var self = this;
		$(self.element).append($("<tr />",{id:"tr_add_new"})
			    .append($("<td />",{colspan:"7"})
			       .append($("<input />",{
			    	   						type	: "button",
			    	   						name	: "add_new",
			    	   						id		: "add_new",
			    	   						value	: "Add New"
			       }))		
			    )		
			)
			
			$("#add_new").live("click",function(){
				
				if($("#add_new_assurance").length > 0)
				{
				   $("#add_new_assurance").remove();
				}
				
				$("<div />",{id:"add_new_assurance"})
				 .append($("<table />",{class:"form"})
				    .append($("<tr />")
				       .append($("<th />",{html:"Response:"}))
				       .append($("<td />")
				    	  .append($("<textarea />",{name:"response", id:"response", cols:"30", rows:"7"}))	   
				       )
				    )
				   .append($("<tr />")
					  .append($("<th />",{html:"Sign Off:"}))
					  .append($("<td />")
						 .append($("<select />",{id:"signoff", name:"signoff"})
						   .append($("<option />",{text:"No", value:"0"}))
						   .append($("<option />",{text:"Yes", value:"1"}))
						 )	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<th />",{html:"Date Tested:"}))
					 .append($("<td />")
						.append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:"", readonly:"readonly"}).addClass("datepicker"))	 
					  )
				   )
				 )
				 .dialog({
				 	  modal		: true, 
				 	  autoOpen	: true, 
				 	  title		: "Legislation Assurance",
				 	  position	: "top",
					  width		: "500px",
				 	  buttons	     : {
			 					"Save" 			: function()
			 					{
			 						if( $("#response").val() == "")
			 						{
			 							jsDisplayResult("error", "error", "Plese enter the response");
			 							return false;
			 						} else {
			 							jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
			 							$.post("../class/request.php?action=AssuranceLegislation.saveLegislationAssurance",{
				 							data : { response : $("#response").val(),sign_off : $("#signoff :selected").val(),date_tested : $("#date_tested").val(),legislation_id : self.options.id } },
			 							function( response ){
				 							if(response.error){
				 								jsDisplayResult("error", "error", response.text );
				 							} else {
				 								jsDisplayResult("ok", "ok", response.text);
				 								self._getLegislationAssurance();
				 								//self._display( response.data )
				 							}				 								
			 							},"json")
			 							$("#add_new_assurance").dialog("destroy")
										$("#add_new_assurance").remove()
			 						}
			 					} , 
			 					"Cancel"		: function()
			 					{
			 						$("#add_new_assurance").dialog("destroy")
									$("#add_new_assurance").remove()
			 					}
		 					  } ,
		 		     close          : function(event, ui)
		 		     {
 						$("#add_new_assurance").dialog("destroy")
						$("#add_new_assurance").remove()
		 		     } ,
      			     open           : function(event, ui)
      			     {
                              var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                              var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
                              var saveBtn = buttons[0];
                              $(saveBtn).css({"color":"#090"});
      			     }			 		     
				 });
				$(".datepicker").datepicker({
			        showOn: 'both',
			        buttonImage: '/library/jquery/css/calendar.gif',
			        buttonImageOnly: true,
			        dateFormat: 'dd-M-yy',
			        changeMonth:true,
			        changeYear:true		
			    });
				
			
				$("#attachDoc").live("change", function(){
				
					$.ajaxFileUpload(
					{	
						url				: "../class/request.php?action=AssuranceLegislation.upload",
						dataType			: "json",
						secureuri		: true,	
						fileElementId 	: "attachDoc",
						success 			: function(data, status)
						{
							$("#fileuploads").empty();
							if(data.error)
							{
								$("#fileuploads").append($("<span />",{id:"responsetext", html:data.text})).append($("<br />"))
							} else {
							
								$("#fileuploads").append($("<span />",{id:"responsetext", html:data.text})).append($("<br />"))
								$("#fileuploads").append($("<ul />",{id:"filelist"}))
								if(!$.isEmptyObject(data.files))
								{
								     console.log(data.files)
									$.each( data.files, function( index, file){
										$("#filelist").append($("<li />",{id:"li_"+index})
										  .append($("<span />")
											 .append($("<a />",{href:"../class/download.php?file="+index+"&name="+file.ref+"&company=assist1", text:file.name}))
										  )
										  .append($("<span />",{id:index}).css({"margin-left":"5px"})
											 .append($("<a />",{href:"#", id:index, title:file, text:"Remove"}).addClass("deletefile")
											  .click(function(e){
											     $.post("../class/request.php?action=AssuranceLegislation.removeFile",
											     {file:file.key, ref:file.ref}, function(response){
						                                   if( response.error )
						                                   {
							                                   $("#responsetext").html( response.text )		
						                                   } else {
							                                   $("#li_"+index).remove();
							                                   $("#responsetext").html( response.text )
						                                   }										     
											     },"json");
											     e.preventDefault();
											  })
											 )
										  )
										)								
									});
								}					
							
							}
						} ,
						error 			: function(data, status, e)
						{
							$("#fileuploads").html( e );
						}					
					});
					return false;
				});
				/*
				$(".deletefile").live("click", function(){
					var file = this.id
					var filename = $(this).attr("title");					
					var listid 	= $(this).parent().attr("id");
					$.post("../class/request.php?action=AssuranceLegislation.removeFile",{
						data 	: { file : file, filename : filename }
					}, function( response ){
						if( response.error )
						{
							$("#responsetext").html( response.text )		
						} else {
							$("#li_"+listid).remove();
							$("#responsetext").html( response.text )
						}					
					},"json");
					return false;
				});
				*/
			});		
		
	}
	
	
	
});
