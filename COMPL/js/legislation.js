$(function(){

	if( $("#legislationId").val() == undefined )
	{
		Legislation.getLegislativeTypes();
		Legislation.getLegislationCategories();
		Legislation.getOrganisationSize();
		Legislation.getOrganisationType();
		Legislation.getOrganisationCapacity();
		//Legislation.getResponsibleOrganisations();
	}
	$("#save").click(function(){
		Legislation.save();		
		return false;
	});
	
	$("#delete").click(function() {
		Legislation.deleteLegislation();
	});
	
	$("#financial_year").change(function(){
	   var selectedval =  $("#financial_year :selected").val();
	   $(".extra").hide();
	   $("#extra_"+selectedval).show();	
	   return false;
	});
	
	
	$("#edit").click(function(){
		Legislation.edit();
		return false;
	});
	
	$("#update").click(function(){
		Legislation.update();
		return false;
	})
	
	$("#type").change(function(){

		if($(this).val() != 3)
		{
			$("<div />",{id:"legislation_dialog"})
			 .append($("<table />",{width:"100%"})
			   .append($("<tr />")
			     .append($("<th />",{html:"Select the main legislation"}))
				 .append($("<td />")
				   .append($("<select />",{id:"parentLegislation", name:"parentLegislation"}))		 
				 )	   
			   )
			 )
			 .dialog({
			 		autoOpen	  : true, 
			 		modal	  : true, 
			 		title	  : "Legislation",
			 		width	  : 600,
			 		position    : "top",
			 		buttons	  : {
			 						"Ok"	: function()
			 						{	
										$("#legislation_dialog").dialog("destroy");
			 							$("#legislation_dialog").remove();			 							
			 						} , 
			 						"Cancel" : function()
			 						{
										$("#legislation_dialog").dialog("destroy");
			 							$("#legislation_dialog").remove();
			 						}
						 		} ,
				    close         : function(event, ui)
				    {
					 $("#legislation_dialog").dialog("destroy");
					 $("#legislation_dialog").remove();   
				    }, 
				    open          : function(event, ui)
				    {
	     			 Legislation.getLegislations()
				      //var dialogBtn = $(event.target).find("ui-")
				    }
			 })
			 return false;
		}
	});
	
});

Legislation		= {

		getLegislations		: function()
		{
			$.getJSON("../class/request.php?action=Legislation.getLegislations",{
				data : { type 	: 3 , section:"new"}
			}, function( legislations ){
				if( $.isEmptyObject(legislations ))
				{
					$("#legislation_dialog").append($("<p />").addClass("ui-state").addClass("ui-state-highlight").css({width:"300px", padding:"7px"})
							.append($("<span />").addClass("ui-icon").addClass("ui-icon-alert").css({"float":"left"}))
							.append($("<span />",{html:"There are no parent legislation"}))
					)
				} else {
					$.each(legislations, function(index, leg ){
						$("#parentLegislation").append($("<option />",{text:leg.name, value:leg.id}))
					});					
				}		
			});
	
		},
		
		getLegislativeTypes		: function()
		{
			$.getJSON("../class/request.php?action=LegislationType.getAll", function( legTypes ){
				$.each( legTypes, function( index, legtype){
					$("#type").append($("<option />",{text:legtype.name, value:legtype.id}))
				});		
			});
	
		},
		
		getLegislationCategories : function()
		{
		   $.post("../class/request.php?action=LegislationCategory.getAll", function( legcategories ){
			 $.each( legcategories, function( index, legcat){
			  $("#category").append($("<option />",{text:legcat.name, value:legcat.id, selected:(legcat.id == 1 ? "selected" : "")}))	   
			 })												  											  
		   },"json");
		}  ,
				
		getOrganisationSize : function()
		{
		  $.post("../class/request.php?action=OrganisationSize.getAll",function( sizes ){
		     $.each( sizes, function( index, orgsize){
			   $("#organisation_size").append($("<option />",{text:orgsize.name, value:orgsize.id, selected:(orgsize.id == 1 ? "selected" : "")}))
			})												  											  
		  },"json");
		}  ,	
		
		getOrganisationType : function()
		{
			$.post("../class/request.php?action=LegislationOrganisationType.getAll", function(orgtypes){			
				$.each( orgtypes, function( index, orgtype){
				  $("#organisation_type").append($("<option />",{text:orgtype.name, value:orgtype.id, selected:(orgtype.id == 1 ? "selected" : "") }))
				})												  											  
			},"json");
		}  ,
		
		getOrganisationCapacity			: function()
		{
			$.post("../class/request.php?action=OrganisationCapacity.getAll", function( capacities ){
				$.each( capacities, function( index, orgcapacity){
				  $("#organisation_capacity").append($("<option />",{text:orgcapacity.name, value:orgcapacity.id, selected:(orgcapacity.id == 1 ? "selected" : "")}))
				})											  											  
			},"json");
		},
		
		save					: function()
		{
			var data 				   = {};
			data.name 				   = $("#name").val();
			data.reference			   = $("#reference").val();
			data.type				   = $("#type :selected").val();
			data.category			   = $("#category").val();
			data.organisation_size 	   = $("#organisation_size").val();
			data.legislation_date  	   = $("#legislation_date").val();
			data.legislation_number	   = $("#legislation_number").val();
			data.organisation_type 	   = $("#organisation_type").val();
			data.organisation_capacity = $("#organisation_capacity").val();
			data.responsible_organisation = $("#responsible_organisation :selected").val();
			data.hyperlink_to_act	   = $("#hyperlink_to_act").val();
			data.available_to_organisation = $("#legislation_available :selected").val()
			data.financial_year 		   = $("#financial_year :selected").val()
			var valid = Legislation.isValid( data );
			
			if( valid == true)
			{
				jsDisplayResult("info", "info", "Saving legislation. . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ClientLegislation.saveLegislation", { data: data}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
					}
	                $.each( data, function( index, value){
	                    $("#"+index).val("");
	                });
				} ,"json");		

			} else {
				jsDisplayResult("error", "error", valid);
			}
			
		}  , 
		
		edit 				: function()
		{
			var data 				   = {};
			data.name 				   = $("#name").val();
			data.reference			   = $("#reference").val();
			data.type				   = $("#type :selected").val();
			data.category			   = $("#category").val();
			data.organisation_size 	   = $("#organisation_size").val();
			data.legislation_date  	   = $("#legislation_date").val();
			data.legislation_number	   = $("#legislation_number").val();
			data.organisation_type 	   = $("#organisation_type").val();
			data.organisation_capacity = $("#organisation_capacity").val();
			data.hyperlink_to_act	   = $("#hyperlink_to_act").val();
			data.available_to_organisation = $("#legislation_available :selected").val()
			data.id 				   = $("#legislationId").val();
			
			var valid = Legislation.isValid( data );
			if(valid == true)
			{
				jsDisplayResult("info", "info", "Updating legislation. . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ClientLegislation.editLegislation", {data:data}, function(response){
				    if(response.error)
				    {
				      jsDisplayResult("error", "error", response.text);
				    } else {
				       if(response.updated)
				       {
				         jsDisplayResult("ok", "ok", response.text);
				       } else {
				         jsDisplayResult("info", "info", response.text);
				       }
				    }
				} ,"json");

			} else {
				jsDisplayResult("error", "error", valid);
			}
		} , 
		
		update 				: function()
		{
			var data 	  = {};
			var response = $("#response").val();
			var status   = $("#status :selected").val();
			var reminder   = $("#reminder").val();
			if(response == "")
			{
			   jsDisplayResult("error", "error", "Please enter the response");
			   return false;
			} else if(reminder === "") {
			   jsDisplayResult("error", "error", "Please enter the reminder date");
			   return false;
			} else if(status === "") {
			   jsDisplayResult("error", "error", "Please select the status");
			   return false;
			} else {
			   data.response = response;
		        data.status   = status;
			   data.reminder   = reminder;
			   data.id 	  = $("#legislationId").val();
			   jsDisplayResult("info", "info", "Updating legislation ... <img src='../images/loaderA32.gif' />");
			   $.post("../class/request.php?action=ClientLegislation.updateLegislation", { data: data }, function(response ){
				 if(response.updated )
				 {
					if(response.error)
					{
					   jsDisplayResult("info", "info", response.text);
					} else {
					   jsDisplayResult("ok", "ok", response.text);
					}
				  } else {					
					jsDisplayResult("error", "error", response.text);
				  }
			    } ,"json");
			}

		} , 		
		
		deleteLegislation	: function()
		{
			if( confirm("Are you sure you want to deactivate legislation L"+$("#legislationId").val()+"?"))
			{
				jsDisplayResult("info", "info", "Deactivating legislation. . . <img src='../images/loaderA32.gif' />");
				$.post("../class/request.php?action=ClientLegislation.deactivate", { legislation_id:$("#legislationId").val()}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						//jsDisplayResult("ok", "ok", response.text);
						document.location.href = 'edit_legislations.php?r[]=ok&r[]='+response.text;
					}
				} ,"json");
			}
		} ,
	
		isValid			: function( data )
		{
			if( data.financial_year == ""){
				return "Please select the financial year";
			} else if( data.name == ""){
				return "Please enter the legislation name";
			} else if( data.type == ""){
				return "Please select the legislative type";
			} else if( data.category == ""){
				return "Please select the legislative category";
			} else if( data.responsible_organisation == ""){
				return "Please select the responsible organisation";
			} else if( data.legislation_date == ""){
				return "Please enter the legislation date";
			} else if( data.legislation_number == ""){
				return "Please enter the legislation number";
			} else if( data.organisatio_type == ""){
				return "Please select the organisation type";
			} else if( data.responsible_organisation == ""){
				return "Please select the responsible organisation";
			}  else {
				return true;	
			} 
			
		}		
				
};
