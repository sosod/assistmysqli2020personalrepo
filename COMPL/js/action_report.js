$(function(){

       $.getJSON("../class/request.php?action=FinancialYear.fetchAll", function(responseList){
         if(!$.isEmptyObject(responseList))
         {
            $.each( responseList, function(index, financialyear){
              $("#financial_year").append($("<option />",{text:"("+financialyear.value+")"+financialyear.start_date+" - "+financialyear.end_date, value:financialyear.id}))
            })
         }      
       });

     $.getJSON("../class/request.php?action=User.getAll", function( users ){
	     $.each( users, function( index, user){
		     $("#owner").append($("<option />",{value:index  , text:user.user}))									
	     })													 
     });
     
     $.getJSON("../class/request.php?action=ActionStatus.getActionStatuses", function(statuses ){
	     $.each(statuses, function( index, status){
		     $("#status").append($("<option />",{value:status.id, text:status.name}))									
	     })													 
     });     
     
	
    
    $("#sortable").sortable().disableSelection();  		
	if($("#quickid").val() === undefined)
	{
	   $(".action").attr("checked","checked");
	} else {
	     $.getJSON("../class/request.php?action=Report.getAQuickReport", {quickid : $("#quickid").val()}, function(response){
		     $("#report_description").text( response.description )
		     $("#report_name").val( response.name )
		     $("#report_title").val( response.data.report_title )
		     //check the legislatin fields that have been selected
		     $(".action").each(function(){
			   var id = $(this).attr("id")
			   $.each(response.data.actions, function(key  ,val){
				if("action_"+key === id)
				{ 
				   $("#"+id).attr("checked", "checked");					
				}
			   });
		     });
		
		     $.each(response.data.value, function(index, val){
			    if(index == "deadline_date")
			    {
				  $("#from_deadline").val(val.from )
				  $("#to_deadline").val(val.to )
			    } else {				
				  $("#"+index).val(val);
				  $("#"+index).text(val);
				  $("#"+index+" option[value='"+val+"']").attr("selected", "selected")								
			    }
		     });
		     $.each(response.data['match'], function(index , key){
			     $("#match_"+index+" option[value='"+key+"']").attr("selected", "selected")
		     });		
		
		     $("#group option[value='"+response.data.group_by+"']").attr("selected", "selected")
		
		     $.each(response['sort'], function(index , val){
			     $("#sortable").append($("<li />").addClass("ui-state-default")
							     .append($("<span />",{id:"__"+index}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort"))
							     .append($("<input />",{type:"hidden", name:"sort[]", value:"__"+index}))
							     .append( val )
			     );			
		     });
		
	     }); 
	}	
	$("#check_action").click(function(e){
		$(".action").each(function(){
			if( !$(this).is(":checked")){
			  $(this).attr("checked", "checked")
			}
		});
		e.preventDefault();
	});
	
	$("#invert_action").click(function(e){
		$(".action").each(function(){
			if( $(this).is(":checked")){
				$(this).attr("checked", "")
			} else {
				$(this).attr("checked", "checked")				
			}		
		});
		e.preventDefault();
	});
	
});
