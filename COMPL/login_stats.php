<?php
/* requires $sdb as object of ASSIST_DB class => created in main/stats.php */
require_once("class/model.php");
require_once("class/action.php");
require_once("class/deliverable.php");
require_once("class/legislation.php");

$sqlStatus = Action::getStatusSQLForWhere("A")." AND ".Deliverable::getStatusSQLForWhere("D")." AND ".Legislation::getStatusSqlForWhere("L");

$now    = strtotime(date("d F Y"));
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));
$sql    = "SELECT
           A.id AS ref,
		   A.deadline AS taskdeadline,
		   A.status,
		   count(A.id) as c 
		   FROM assist_".$cmpcode."_".$modref."_action A 
		   INNER JOIN assist_".$cmpcode."_".$modref."_deliverable D ON D.id = A.deliverable_id 
		   INNER JOIN assist_".$cmpcode."_".$modref."_legislation L ON D.legislation = L.id
		   WHERE A.owner = '$tkid'
		   AND STR_TO_DATE(A.deadline,'%d-%M-%Y') <= ADDDATE(now(), ".$next_due.") 
		   AND A.status <> 3 
		   AND ".$sqlStatus."
		   GROUP BY A.deadline";
		   //echo $sql;
$rows = $sdb->mysql_fetch_all($sql);
foreach($rows as $row) 
{
	$d         = strtotime($row['taskdeadline']);
	$next7days = strtotime(date("d-M-Y", strtotime("+".$next_due." days")) );
	if($d < $now) 
	{
		$count['past'] +=$row['c'];
	} elseif($d==$now) {
		$count['present'] +=$row['c'];
	} else {
	    //if($d <= $next7days) 
	    //{
		    $count['future'] +=$row['c'];
		//}
	}
}
?>
