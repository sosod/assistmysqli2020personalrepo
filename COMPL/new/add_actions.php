<?php
$scripts = array("actions.js","jquery.ui.deliverable.js");
include("../header.php");
$nObj = new LegislationNaming();
$headers = $nObj -> getNaming();

$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById($_GET['id']);

$categories	 = LegislativeCategoriesManager::getLegislationCategory($_GET['id']);

$orgSizes	 = OrganisationSizeManager::getLegislationOrganisationSizes($_GET['id']); 

$orgTypes    = LegislationOrganisationTypeManager::getLegislationOrganisationType($_GET['id']);

$orgCaps 	 = OrganisationCapacityManager::getLegislationOrganisationCapacity($_GET['id']);

$legtype = LegislativetypeManager::getLegislationType($legislation['type']);	

$financialYearObj = new FinancialYear();
$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder">
 <tr>
    <td class="noborder" width="50%">
		<table width='100%'>
       	<tr>
       		<td colspan="2"><h4>Legislation Details</h4></td>
       	</tr>
           <tr>
               <th><?php echo $nObj->getHeader("id"); ?>: </th>    
               <td><?php echo $legislation['id']; ?></td>
           </tr>
           <tr>
               <th><?php echo $nObj->getHeader("name"); ?>: </th>    
               <td><?php echo $legislation['name']; ?></td>
           </tr>
           <tr>
               <th><?php echo $nObj->getHeader("reference"); ?>:</th>    
               <td><?php echo $legislation['legislation_reference']; ?></td>
           </tr>    
           <tr>
               <th><?php echo $nObj->getHeader("legislation_number"); ?>:</th>    
               <td><?php echo $legislation['legislation_number']; ?></td>
           </tr>                    
          <tr>
               <th><?php echo $nObj->getHeader("type"); ?>:</th>    
               <td><?php echo $legtype; ?></td>
           </tr>    
           <tr>
               <th><?php echo $nObj->getHeader("category"); ?>:</th>    
               <td><?php echo $categories; ?></td>
           </tr>    
           <tr>
               <th><?php echo $nObj->getHeader("organisation_size"); ?>:</th>    
               <td><?php echo $orgSizes; ?></td>
           </tr>         
           <tr>
               <th><?php echo $nObj->getHeader("organisation_type"); ?>:</th>    
               <td><?php echo $orgTypes; ?></td>
           </tr>   
           <tr>
               <th><?php echo $nObj->getHeader("organisation_capacity"); ?>:</th>    
               <td><?php echo $orgCaps; ?></td>
           </tr>                
           <!--<tr>
               <th><?php echo $nObj->getHeader("responsible_business_patner"); ?>:</th>    
               <td><?php echo $legislation['responsible_organisation']; ?></td>
           </tr> -->                 
           <tr>
               <th><?php echo $nObj->getHeader("legislation_date"); ?>:</th>    
               <td><?php echo $legislation['legislation_date']; ?></td>
           </tr>                            
           <tr>
               <th><?php echo $nObj->getHeader("hyperlink_act"); ?>:</th>    
               <td><?php echo $legislation['hyperlink_to_act']; ?></td>
           </tr> 
           <tr>
               <th><?php echo $nObj->getHeader("financial_year"); ?>:</th>    
               <td><?php  
                    if(!empty($financialyear))
                    {
                      echo $financialyear['start_date']." to ".$financialyear['end_date']." (".$financialyear['value'].")";
                    }
                    ?></td>
           </tr>            
           <tr>
               <td class="noborder"><?php displayGoBack("", ""); ?></td>    
               <td class="noborder"></td>
           </tr>                 
       </table>
    </td>
    <td class="noborder" width="50%">
		<script language="javascript">
		$(function(){
			$("#display_deliverable").deliverable({addActions:true, editActions:true, legislationId:$("#legislationid").val(), page:"new", section:"new"});
		})
		</script>
		<div id="display_deliverable"></div>
	</td>
  </tr>
</table>  
<input type="hidden" value="<?php echo $legislation['id']; ?>" id="legislationid" name="legislationid" />
