<?php
$scripts = array('jquery.ui.legislation.js', 'jquery.ui.deliverableactions.js', 'jquery.ui.action.js');
include("../header.php");
?>
<script>
$(function(){

  $("#legislation_table").legislation({confirmLegislation : true,
                                       page               : "confirmation",
                                       section            : "new",
                                       deliverableUrl     : "../class/request.php?action=Deliverable.getDeliverableConfimation",
                                       autoLoad           : false
                                      });
});	
</script>
<?php JSdisplayResultObj(""); ?>  
<div id="legislation_table"></div>
