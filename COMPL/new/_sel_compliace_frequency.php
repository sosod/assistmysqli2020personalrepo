<?php
$list = $_REQUEST['l'];
$page_title = "Deliverable >> Import >> ";
switch($list) {
	case "compliance_frequency": $page_title.="Compliance Frequency"; break;
}
$page_title.=" List";

$scripts = array();
include("../header.php");

$values = array();

switch($list) {
case "compliance_frequency":
	$object = new ComplianceFrequency();
	$source_values = $object -> getAll(); 
	foreach($source_values as $key => $arr) {
		$i = $arr['id'];
		$v = $arr['name'];
		if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
		$values[] = $v;
	}
	break;
}

sort($values,SORT_STRING);
?>
<table>
    <tr>
        <th>Allowable Values</th>
    </tr>
	<?php
	echo "<tr><td>".implode("</td></tr><tr><td>",$values)."</td></tr>";
?>
</table>
<div><?php displayGoBack("", ""); ?></div>

