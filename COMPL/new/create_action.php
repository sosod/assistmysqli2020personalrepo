<?php
$scripts = array("guidance.js", "action.js", "jquery.ui.action.js");
include("../header.php");
$nObj = new ActionNaming();
$headers = $nObj-> getNaming();

$dOb = new ClientDeliverable();
$deliverable = $dOb -> fetch($_GET['id']);

$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById($deliverable['legislation']);

?>
<?php JSdisplayResultObj(""); ?>   
<script language="javascript">
$(function(){
	$("#display_action").action({editAction:true, deleteActions:true, deliverableId:$("#deliverableid").val(), page : "create_action", section:"new", copyAction:true});
})
</script>
<table width="100%" class='noborder'>
	<tr>
    	<td valign="top" width="50%" class='noborder'>
    	     <?php  	          
               $deliverableFormObj = new DeliverableDisplay($_GET['id']);
               $deliverableFormObj -> display(new DeliverableNaming());    	   
                 
    	     ?>
        </td>
        <td valign="top" class="noborder" width="50%">
            <table width="100%">
				<tr>
					<td class='noborder'><h4>Add New Action</h4></td>
					 <td style="text-align:right;"><?php 
            				if((Legislation::IMPORTED & $legislation['legislation_status']) == Legislation::IMPORTED)
            				{
							 $controller->printRequestGuidance("");  	
						} 
            			?>
            		</td>
            	</tr>
                <tr>
                    <th><?php                 
                     echo $nObj -> getHeader("action"); ?>:</th>    
                    <td>
                    	<textarea rows="8" cols="30" id="action_name" name="action_name"></textarea>
                    </td>
                </tr>
   
                <tr>
                    <th><?php echo $nObj -> getHeader("owner"); ?>:</th>    
                    <td>
                        <select name="action_owner" id="action_owner">
                            <option value="">--action owner--</option>
                        </select>        
                    </td>
                </tr>    
                <tr>
                    <th><?php echo $nObj -> getHeader("action_deliverable"); ?>:</th>    
                    <td>
                    	<textarea rows="5" cols="30" id="deliverable" name="deliverable"></textarea>
                    </td>
                </tr>   
                <tr>
                    <th><?php echo $nObj -> getHeader("deadline"); ?>:</th>    
                    <td><input type="text" name="deadline" id="deadline" value="" class="datepicker"/></td>
                </tr>    
                <tr>
                    <th><?php echo $nObj -> getHeader("reminder"); ?>:</th>    
                    <td><input type="text" name="reminder" id="reminder" value="" class="datepicker"/></td>
                </tr>     
               <tr>
                    <th>Recurring:</th>    
                    <td>
                      <input type="checkbox" name="recurring" id="recurring" value="1" />
                    </td>
                </tr>                             
                <tr>
                    <td></td>    
                    <td>
                      <input type="button" name="save" id="save" value=" Save " class='isubmit' />
                      <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>">
                      <input type="hidden" name="deliverableId" id="deliverableId" value="<?php echo $_GET['id']; ?>" />
                      <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $deliverable['legislation']; ?>" />
                    </td>
                </tr>
                <tr>
                	<td class="noborder" colspan="2">
                		<table id="display_action" width="100%"></table>
                	</td>
                </tr>
            </table>        
        </td>
    </tr>
  <tr>
     <td class="noborder"><?php displayGoBack("", ""); ?></td>    
     <td class="noborder"><?php displayAuditLogLink("action_edit", false); ?></td>
 </tr>     
</table>		
