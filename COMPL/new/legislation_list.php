<?php
$scripts = array('jquery.ui.legislation.js');
include("../header.php");
?>
<script>
$(function(){

  $("#legislation_table").legislation({deactivateLegislation : true,
                                       page               : "list",
                                       section            : "new",
                                       autoLoad           : true,
									   actionProgress		: false
                                      });
});	
</script>
<?php JSdisplayResultObj(""); ?>  
<div id="legislation_table"></div>
