<?php
$scripts = array("activation.js","jquery.ui.activatelegislation.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>   
<table class="noborder" width="100%">
	<tr>
		<td colspan="2" class="noborder">
			<script>
				$(function(){
					$("#legislation").activatelegislation({activateAction:true, legoption : "active", section:"activate"});
				});
			</script>
			<div id="legislation"></div>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php //displayAuditLogLink("legislation_edit", false); ?></td>
	</tr>
</table>
