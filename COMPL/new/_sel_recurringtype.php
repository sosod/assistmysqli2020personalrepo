<?php
$scripts = array();
include("../header.php");
?>
<table>
    <tr>
        <th>Ref</th>
        <th>Name</th>
    </tr>
    <tr>
      <td>1</td>
      <td>Fixed</td>
    </tr>
    <tr>
      <td>2</td>
      <td> Start Of Financial Year</td>
    </tr>
    <tr>
      <td>3</td>
      <td>End Of Financial Year</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Start Of Month</td>
    </tr>
    <tr>
      <td>5</td>
      <td>End Of Month</td>
    </tr>
    <tr>
      <td>6</td>
      <td>Start Of Quarter</td>
    </tr>
    <tr>
      <td>7</td>
      <td>End Of Quarter</td>
    </tr>
    <tr>
      <td>8</td>
      <td>Start Of Semester</td>
    </tr>                   
    <tr>
      <td>9</td>
      <td>End Of Semester</td>
    </tr>                    
</table>
<div><?php displayGoBack("", ""); ?></div>