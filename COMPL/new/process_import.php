<?php
//error_reporting(-1);
$scripts = array("deliverable.js");
include("../header.php");
$legislationId = $_REQUEST['legislationid'];
$legislation_id = $legislationId;
$nObj          = new Naming();
//$nObj -> setThColuomns();
$importObj     = new ImportFile();
$filesAllowed  = array("csv");  
$file          = "";
?>
<table class=noborder width=100%>
	<tr>
		<td class=noborder width=50%>
			<h2>Legislation Details</h2>
			<?php include("../common/legislation.inc.php"); ?>
<script type=text/javascript>
$(function() {
		$("#legislation_details_table tr").each(function() {
			if($(this).hasClass("min_display")) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		$("#legislation_details_table th").css("width","200px");
});
</script>
<?php //$helper->displayGoBack(); ?>			
		</td>
		<td class=noborder>

<table class="noborder" style='margin: 0 auto'>
	<tr>
		<td class="noborder">
			<?php 
			$results =  $importObj -> upload('import_file', 'deliverable');

			if(($results['error']) || $results['error'] == TRUE)
			{
				echo '<p class="ui-widget ui-state ui-state-error" style="padding:5px; width:450px;">';
				echo '<span class="ui-icon ui-icon-closethick" style="float:left;"></span>';
				echo '<span>';
					echo '<span>'.$results['text'].'</span><br />';
				echo '</span>';			 			 
				echo '</p>';	
				displayGoBack("", "");
				return false;	
			}
			if(!($results['error']) || $results['error'] == FALSE)
			{
				//echo '<p class="ui-widget ui-state ui-state-ok" style="padding:5px; width:430px;">';
				//echo '<span class="ui-icon ui-icon-check" style="float:left;"></span>';
				//echo '<span>';
				//	echo '<span>'.$results['text'].'</span><br />';
				//echo '</span>';			
				$importObj -> writeImported("deleverable");
                $file  =  $results['file'];
				//echo '</p>';	
			}
			?>		
		</td>
	</tr>
	<tr>
		<td class="noborder">
  		<p class="ui-widget ui-state ui-state-info" style='padding: 7px; ' ><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			Aftering clicking the Import button, please review the information and then either click the Accept or Reject button.  <br />No information will be imported until the Accept button has been clicked.
  		</p>
			<p id="output2"></p>			
<?php 
$fileContents = $importObj -> readfileContents("deleverable", $file);
$columnCount  = 0;
$tableData    = array();
$error        = "";
$hasActions   = false;
$acceptFile   = true;

if(isset($fileContents) && !empty($fileContents))
{
	$complianceFreqObj = new ComplianceFrequency();
	$complianceFreq    = $complianceFreqObj -> getList();
	
	$orgTypeObj        = new OrganisationTypesManager();
	$orgTypes          = $orgTypeObj -> getAllList();
	
    /*$responsibleOrgObj = new DepartmentManager();
    $departments       = $responsibleOrgObj -> getAll();*/

    $functionalServObj = new FunctionalServiceManager();
    $functionalService = $functionalServObj -> getClientFunctionalServices(); //ASSIST_HELPER::arrPrint($functionalService);

    $directorateObj    = new ClientDepartment();
    $directorates      = $directorateObj -> getList();
    
    $userObj           = new User();
    $users             = $userObj -> getList();
    
    $eventObj          = new Event();
    $events            = $eventObj -> getList(array());
  
    $subeventObj       = new SubEvent();
    $subevents         = $subeventObj -> getList(array());    
    
    $reportingCatObj   = new ReportingCategories();
    $reportingcategory = $reportingCatObj -> getList(array());  

	$accountablePersonObj = new AccountablePersonManager();
	$accountablePerson = $accountablePersonObj->getImportList();

	$complianceToObj = new ComplianceToManager();
	$complianceTo = $complianceToObj->getImportList();
	
	$actionObj 			= new Action();
	$action_deadline 	= $actionObj->getActionDeadlineTypes();
$action_deadline2 = array();
foreach($action_deadline as $key=>$v) {
	$v = str_replace(" ","",strtolower($v));
	$action_deadline2[$v] = $key;
}
    //get the file headers     
    $headers     = $fileContents[0];
    $columnCount = count($headers); 
    foreach($headers as $key => $value) {
        $tableData['headers'][] = $value; 
    }
	//unset($tableData['headers'][count($tableData['headers'])]);
    $datatypes = $fileContents[1];
    unset($fileContents[0]);
    unset($fileContents[1]);
    //debug($tableData['headers']);
    //debug($subevents);
    //separating common data types 
    $dates   	= array(6 => "compliance_date", 14 => "legislation_deadline", 24=>"action_deadline");
    $textFields = array( 1   => "description",
    					 15  => "sanction",
    					 16  => "reference_link",
    					 17  => "assurance",
    					 22  => "guidance"
    					);
    $requiredText = array(0 => "short_description", 2 => "legislation_section");     
    $owners       = array( 13 => "responsibility_owner", 23=>"action_owner");     
    $data         = array();
    if(!empty($fileContents))
    {
       foreach($fileContents as $key => $deliverable_row)
       {
	  // echo "<h2>".$key."</h2>";
          $data[$key]['hasError'] 			   = 1; //holds the error state of a data-row
          $tableData['data'][$key]['hasError'] = 1; //holds the error state of a specific cell/or data item
          $countTest[$key]  				   = 0; //holds the coloum count of a row
          if(count($deliverable_row) < $columnCount)
          {
            $tableData['data'][$key] = "The column count does not match , there a fewer columns";
			$acceptFile 			 = false;
          } elseif(count($deliverable_row) > $columnCount) {
            $tableData['data'][$key] = "The column count does not match , there a more columns";
			$acceptFile 			 = false;            
          } else {
           		$isRecurring               = false;
             foreach($deliverable_row as $i => $value)
             {
                $val 		               = trim($value);
				//echo "<P>".$i."=>".$val."</p>";
				$data[$key]['legislation'] = $_REQUEST['legislationid'];
				$data[$key]['insertuser']  = $_SESSION['tid'];  
                if($i == 3)
                {
                   //check the legislation compliance frequency validity
                    if(!in_array($val, $complianceFreq)) 
                    {
                        $data[$key]['hasError']                          = ($data[$key]['hasError'] & 0);
                        $tableData['data'][$key]['hasError']             = ($tableData['data'][$key]['hasError'] & 0);
                        $tableData['data'][$key]['compliance_frequency'] = "<p class='ui-state ui-state-error'  style='padding:5px;'>Legislation compliance frequency is not valid  <i>".$val."</i><br /></p>";
                    } else {
                        $countTest[$key]                                = $countTest[$key] + 1;
                        $_compliances                                   = array_flip($complianceFreq);
                        $data[$key]['compliance_frequency']             = $_compliances[$val];
                        $data[$key]['hasError']                         = ($data[$key]['hasError'] & 1);
                        $tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 1);
                        $tableData['data'][$key]['compliance_frequency']= $val;
                    }
                } elseif($i == 4) {
                   //check the validity of the deliverable applicable organisational type
                    if(!empty($val))
                    {
                        if(!in_array($val, $orgTypes))
                        {
                            $tableData['data'][$key]['applicable_org_type'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Organistion type ".$oValue." does not exist</p>"; 
                            $tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 0);
                            $data[$key]['hasError']                         = ($data[$key]['hasError'] & 0); 
                        } else {
                            $countTest[$key]                                = $countTest[$key] + 1;
                            $_orgtypes                                      = array_flip($orgTypes);
                            $data[$key]['applicable_org_type']              = $_orgtypes[$val];
                            $data[$key]['hasError']                         = ($data[$key]['hasError'] & 1);
                            $tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 1);
                            $tableData['data'][$key]['applicable_org_type'] = $val;
                        }				  
                    } else {
                        $tableData['data'][$key]['applicable_org_type'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Organistion type is required</p>"; 
                        $tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 0);
                        $data[$key]['hasError']                         = ($data[$key]['hasError'] & 0); 
                    }
                } elseif($i == 5) {
                  //check the recuring types
					//recurance type
					if(!empty($val))
					{
						if($val == "Yes")
						{
							$isRecurring             = true;
							$data[$key]['recurring'] = 1;
						} else {
							$isRecurring             = false;
							$data[$key]['recurring'] = 0;
						}
						$tableData['data'][$key]['recurring'] = $val;												
					} else {
						$data[$key]['recurring']              = 0;
						$isRecurring 			              = false;
						$tableData['data'][$key]['recurring'] = "No";
					}	
                } elseif($i == 7) {
                   //check the validity of the number of days entered 
					if($isRecurring===true) {
						if(empty($val) || !is_numeric($val)) {
							$data[$key]['hasError']                      = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError']         = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['recurring_period'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Please enter valid number of days  <i>".$val."</i><br /></p>";	
						} else {
							$data[$key]['recurring_period']              = $val;
							$tableData['data'][$key]['recurring_period'] = $val;
						}												
					} else {
						$data[$key]['recurring_period']              = "";
						$tableData['data'][$key]['recurring_period'] = "";
					}                   
                } elseif($i == 8) {
                   //check the validity of the days-options 
                    $daysOptions = array("days" => "Days", "working_days" => "Working Days");
					if($isRecurring)
					{
						if(!in_array($val, $daysOptions))
						{
							$data[$key]['hasError']                  = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError']     = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['days_options'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid option  <i>".$val."</i><br /></p>";	
						} else {
							$data[$key]['days_options']              = $val;
							$tableData['data'][$key]['days_options'] = $val;
						}												
					} else {
						$data[$key]['days_options']              = "";
						$tableData['data'][$key]['days_options'] = "";						
					}                   
                } else if($i == 9) {
                  //check the validity of the recurrance types if the recurring is yes
					if($isRecurring)
					{
						$recurranceTypes = array("fixed" 		   => "Fixed",
												 "start" 		   => "Start Of Financial Year", 
												 "end" 	 		   => "End Of Financial Year",
						 						 "startofmonth"    => "Start Of Month",
						 						 "endofmonth" 	   => "End Of Month",
												 "startofquarter"  => "Start Of Quarter",
												 "endofquarter"	   => "End Of Quarter",
												 "startofsemester" => "Start Of Semester",
												 "endofsemester"   => "End Of Semester", 
												 "startofweek"	   => "Start Of Week", 
												 "endofweek"	   => "End Of Week"
												); 
						$rTypes = array();
						foreach($recurranceTypes as $r => $t) {
							$x = str_replace(" ","",strtolower($t));
							$rTypes[$x] = $r;
						} 
						$val2 = str_replace(" ","",strtolower($val));
						if( isset($rTypes[$val2]) )
						{
							$countTest[$key]                            = $countTest[$key] + 1;
							//$rTypes                                     = array_flip($recurranceTypes);
							$data[$key]['recurring_type']               = $rTypes[$val2];
							$data[$key]['hasError']                     = ($data[$key]['hasError'] & 1);
							$tableData['data'][$key]['hasError']        = ($tableData['data'][$key]['hasError'] & 1);
							$tableData['data'][$key]['recurring_type']  = $val;//." :: ".$val2." :: ".$data[$key]['recurring_type'];	
						} else {
							$data[$key]['hasError']                    = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError']       = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['recurring_type'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid recurring type  <i>".$val."</i><br /></p>";
						}				
					} else {
                        $data[$key]['recurring_type']              = "";
                        $tableData['data'][$key]['recurring_type'] = "";
					}                  
                } elseif($i == 10) {
				   //check the validity of the deliverable responsible department if not empty			
					if(!empty($val))
					{
					    if( !in_array($val, $directorates)){
	                       $tableData['data'][$key]['responsible'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid department  <i>".$val."</i><br /></p>";
	                       $data[$key]['hasError']                 = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError']    = ($tableData['data'][$key]['hasError'] & 0);
	                    } else {
						   $countTest[$key]                             = $countTest[$key] + 1;
	                       $_directorates                               = array_flip($directorates);
	                       $data[$key]['responsible']                   = $_directorates[$val];
	                       $data[$key]['hasError']                      = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['responsibleError'] = $val;
	                       $tableData['data'][$key]['hasError']         = ($tableData['data'][$key]['hasError'] & 1);
	                    }
					} else {
					   $countTest[$key]                        = $countTest[$key] + 1;
                       $data[$key]['responsible']              = 1;
                       $data[$key]['hasError']                 = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['responsible'] = "Unspecified";
                       $tableData['data'][$key]['hasError']    = ($tableData['data'][$key]['hasError'] & 1);
					}
				} elseif($i == 11) {
				    //check the validity of the deliverable functional services if not empty
				   if(!empty($val))
				   {
				   		if( !in_array($val, $functionalService)){
	                       //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>Invalid functional service<br />".$val."</p></td>";
	                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 0);
	                       $tableData['data'][$key]['functional_service'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid functional service  <i>".$val."</i><br /></p>";
	                    } else {
	                       $countTest[$key]                               = $countTest[$key] + 1;
	                       $_functional_service                           = array_flip($functionalService);
	                       $data[$key]['functional_service']              = $_functional_service[$val];
	                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['functional_service'] = $val;
	                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 1);
	                    }
				   } else {
                       $countTest[$key]                               = $countTest[$key] + 1;
                       $data[$key]['functional_service']              = 1;
                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['functional_service'] = "Unspecified";
                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 1);
				   }
				} elseif($i==12) {
				//accountable person
					$fld = "accountable_person";
					$tableData['data'][$key][$fld] = array();
					$_accountable = array_flip($accountablePerson);
					$err = false;
					if(strlen($val)==0) {
						$err = true;
					} else {
						$values = explode(";",$val);
						foreach($values as $v) {
							$v = trim($v);
							if(!in_array($v, $accountablePerson)) {
								$err = true;
							} else {
								$data[$key][$fld]				            = $_accountable[$v];
								$tableData['data'][$key][$fld][]			= $v;
							}				
						}
					}
					if($err) {
						$data[$key]['hasError']                          = ($data[$key]['hasError'] & 0);
						$tableData['data'][$key]['hasError']             = ($tableData['data'][$key]['hasError'] & 0);
						$tableData['data'][$key][$fld][]				 = "<p class='ui-state ui-state-error'  style='padding:5px;'>Accountable Person is not valid  <i>".$val."</i><br /></p>";
					} else {
						$countTest[$key]                                = $countTest[$key] + 1;
						$data[$key]['hasError']                         = ($data[$key]['hasError'] & 1);
						$tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 1);
					}
				} elseif($i==18) {
				//compliance to
					$fld = "compliance_to";
					$tableData['data'][$key][$fld] = array();
					$_complianceto = array_flip($complianceTo);
					$err = false;
					if(strlen($val)==0) {
						$err = true;
					} else {
						$values = explode(";",$val);
						foreach($values as $v) {
							$v = trim($v);
							if(!in_array($v, $complianceTo)) {
								$err = true;
							} else {
								$data[$key][$fld]				            = $_complianceto[$v];
								$tableData['data'][$key][$fld][]			= $v;
							}				
						}
					}
					if($err) {
						$data[$key]['hasError']                          = ($data[$key]['hasError'] & 0);
						$tableData['data'][$key]['hasError']             = ($tableData['data'][$key]['hasError'] & 0);
						$tableData['data'][$key][$fld][]				 = "<p class='ui-state ui-state-error'  style='padding:5px;'>Compliance To value is not valid  <i>".$val."</i><br /></p>";
					} else {
						$countTest[$key]                                = $countTest[$key] + 1;
						$data[$key]['hasError']                         = ($data[$key]['hasError'] & 1);
						$tableData['data'][$key]['hasError']            = ($tableData['data'][$key]['hasError'] & 1);
					}
				} elseif(array_key_exists($i, $owners)) {
				  //check the validity of the accountable person if not empty
				  //use users list 
				  //check the validity of the responsible person if not empty -- when $i = 13
				  //check the validity of the deliverable compliance to if not empty when $i = 18	
					if(!empty($val))
					{
					    if( !in_array($val, $users))
					    {
	                       $data[$key]['hasError']               = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError']  = ($tableData['data'][$key]['hasError'] & 0);
	                       $tableData['data'][$key][$owners[$i]] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid  ".ucwords(str_replace("_", " ", $owners[$i]))."  <i>".$val."</i><br /><p>";
	                    } else {
						   $countTest[$key]                        = $countTest[$key] + 1;
	                       $_owners                                = array_flip($users);
	                       $data[$key][$owners[$i]]                = $_owners[$val];
	                       $data[$key]['hasError']                 = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key][$owners[$i]]   = $val;
	                       $tableData['data'][$key]['hasError']    = ($tableData['data'][$key]['hasError'] & 1);
	                      // $tr .= "<td>".$val."</td>";
	                    }
					} else {
					   $countTest[$key]                                 = $countTest[$key] + 1;
                       $data[$key][$owners[$i]]                         = 1;
                       $data[$key]['hasError']                          = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key][$owners[$i]]            = "Unspecified";
                       $tableData['data'][$key]['hasError']             = ($tableData['data'][$key]['hasError'] & 1);
					}	  
				} elseif($i == 19) {
				  //check the validity of the deliverable main event if not empty
                  if(!empty($val))
                  {
                     if(!in_array($val, $events))
                     {
                        $tableData['data'][$key]['main_event'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Main event ".$val." does not exist</p>";
                        $data[$key]['hasError']                = ($data[$key]['hasError'] & 0);
                        $tableData['data'][$key]['hasError']   = ($tableData['data'][$key]['hasError'] & 0);	
                     } else {
	                    $countTest[$key]                        = $countTest[$key] + 1;
	                    $_events                                = array_flip($events);
	                    $data[$key]['main_event']               = $_events[$val];
	                    $data[$key]['hasError']                 = ($data[$key]['hasError'] & 1);
	                    $tableData['data'][$key]['main_event']  = $val;
	                    $tableData['data'][$key]['hasError']    = ($tableData['data'][$key]['hasError'] & 1);
                     }	
                  } else {
                    $tableData['data'][$key]['main_event'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Main event is required</p>";
                    $data[$key]['hasError']                = ($data[$key]['hasError'] & 0);
                    $tableData['data'][$key]['hasError']   = ($tableData['data'][$key]['hasError'] & 0);
                  }
				} elseif($i == 20) {
				  //check the validity of the deliverable sub-events if not empty
					$subArr = array_diff(explode(";", $val), array(''));
					$subStr = "";
					$subErr = "";
					$subDat = "";					
					if(!empty($subArr))
					{
						foreach($subArr as $subIndex => $subValue) {
							$subValue = trim($subValue);
						    if(!in_array($subValue, $subevents))
						    {
							   $subErr                              = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid sub events ".$subValue."<br /></p>";
		                       $data[$key]['hasError']              = ($data[$key]['hasError'] & 0);
		                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
		                    } else {
							   $countTest[$key]                     = $countTest[$key] + 1;
		                       $_subevents                          = array_flip($subevents);
		                       $subDat                             .= $subValue.",";
		                       $subStr                             .= $_subevents[$subValue].",";
		                       $data[$key]['hasError']              = ($data[$key]['hasError'] & 1);
		                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);               
		                    }	
						}										
					} else {
					   $subErr = "<p class='ui-state ui-state-error' style='padding:5px;'>Sub events are required</p>";
					}
					
					if(!empty($subErr))
					{
                       $data[$key]['hasError']               = ($data[$key]['hasError'] & 0);
                       $tableData['data'][$key]['hasError']  = ($tableData['data'][$key]['hasError'] & 0);	
                       $tableData['data'][$key]['sub_event'] = $subErr;						                       
					} else {
                       $data[$key]['sub_event']              = rtrim($subStr,",");
                       $data[$key]['hasError']               = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['sub_event'] = rtrim($subDat, ",");
                       $tableData['data'][$key]['hasError']  = ($tableData['data'][$key]['hasError'] & 1);					
					}
				} elseif($i == 21) {
				  //check the validity of the reporting categories
				   if(!empty($val))
				   {
				   		if( !in_array($val, $reportingcategory)){
	                       //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>Invalid functional service<br />".$val."</p></td>";
	                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 0);
	                       $tableData['data'][$key]['reporting_category'] = "<p class='ui-state ui-state-error' style='padding:5px;'>Invalid reporting category <i>".$val."</i><br /></p>";
	                    } else {
	                       $countTest[$key]                               = $countTest[$key] + 1;
	                       $_reporting_category                           = array_flip($reportingcategory);
	                       $data[$key]['reporting_category']              = $_reporting_category[$val];
	                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['reporting_category'] = $val;
	                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 1);
	                    }
				   } else {
                       $countTest[$key]                               = $countTest[$key] + 1;
                       $data[$key]['reporting_category']              = 1;
                       $data[$key]['hasError']                        = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['reporting_category'] = "Unspecified";
                       $tableData['data'][$key]['hasError']           = ($tableData['data'][$key]['hasError'] & 1);
				   }
				//} elseif($i == 23) {
				  //check if the deliverable has actions 
					/*if(!empty($val))
					{
					   $hasActions                              = false;
					   $data[$key]['has_actions']               = false;
                       $tableData['data'][$key]["has_actions"]  = "No";						
					} else {
					   $hasActions                             = true;
					   $data[$key]['has_actions']              = true;
                       $tableData['data'][$key]["has_actions"] = "Yes";		
					}*/
				} elseif(array_key_exists($i, $dates)) {
				  //check the validity of the dates fields
					$validate_date = true;
					$val2 = str_replace(" ","",strtolower($val));
					if($dates[$i]=="action_deadline" && isset($action_deadline2[$val2])) { //in_array($val,$action_deadline)) {
						$vkey = $action_deadline2[$val2];
						$validate_date = false;
	                       $_action_deadline                      	    = array_flip($action_deadline);  //echo $vkey;
	                       $data[$key]['action_deadline']              	= $vkey;
	                       $data[$key]['hasError']                      = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['action_deadline'] 	= $val;
	                       $tableData['data'][$key]['hasError']         = ($tableData['data'][$key]['hasError'] & 1);
					}
					if($validate_date) {
						if(preg_match("/[2][0-9][0-9][1-9]\/[0-1][0-9]\/[0-3][0-9]/",$val) == FALSE) {
							if((isset($value[5]) && $value[5] === "Yes") && $i == 6)
							{
								$tableData['data'][$key][$dates[$i]] = "";
								$data[$key]['hasError']              = ($data[$key]['hasError'] & 1);
								$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
							} else {
								$tableData['data'][$key][$dates[$i]] = "<p class='ui-state ui-state-error'  style='padding:5px;'>Pattern error(YYYY/MM/DD) <br />".$val."</p>";
								$data[$key]['hasError']              = ($data[$key]['hasError'] & 0);
								$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							}
						} else {
							$newdate         = date("d-M-Y", strtotime($val));	
							$countTest[$key] = $countTest[$key] + 1;
							if($i == 6) {
								$newdate = date("d-M",strtotime($val));
//							   $data[$key][$dates[$i]] = date("d-M", strtotime($val));
							
							} elseif($dates[$i]=="action_deadline" && (strtotime($val)>strtotime($data[$key]['legislation_deadline']))) {
								$validate_date = false;
								$tableData['data'][$key][$dates[$i]] = "<p class='ui-state ui-state-error'  style='padding:5px;'>Invalid Deadline Date - exceeds Legislation Deadline <br />".$val."</p>";
								$data[$key]['hasError']              = ($data[$key]['hasError'] & 0);
								$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							} else {
							}
							if($validate_date) {
							   $data[$key][$dates[$i]] = $newdate;
								$data[$key]['hasError']              = ($data[$key]['hasError'] & 1);
								$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
								$tableData['data'][$key][$dates[$i]] = $val."<br />(".$newdate.")";
							}
						}				    
					}
				} elseif(array_key_exists($i, $requiredText)) {
				  //check the validity of the required text fields
					if(empty($val))
					{
						$tableData['data'][$key][$requiredText[$i]] = "<p class='ui-state ui-state-error' style='padding:5px;'>".ucwords(str_replace("_", " ", $requiredText[$i]))." is required and cannot be empty</p>";
						$data[$key]['hasError']                     = ($data[$key]['hasError'] & 0);
						$tableData['data'][$key]['hasError']        = ($tableData['data'][$key]['hasError'] & 0);
					} else {
						$countTest[$key]                            = $countTest[$key] + 1;
						$data[$key][$requiredText[$i]]              = $val;
						$data[$key]['hasError']                     = ($data[$key]['hasError'] & 1);
						$tableData['data'][$key]['hasError']        = ($tableData['data'][$key]['hasError'] & 1);
						$tableData['data'][$key][$requiredText[$i]] = $val;
					}				  
			    } else {
					$countTest[$key]                          = $countTest[$key] + 1;
					$data[$key][$textFields[$i]]              = $val;
					$data[$key]['hasError']                   = ($data[$key]['hasError'] & 1);
					$tableData['data'][$key]['hasError']      = ($tableData['data'][$key]['hasError'] & 1);
					$tableData['data'][$key][$textFields[$i]] = $val;
				}
             }  //end foreach deliverable_row
          }
       } //end of foreach contents
    } else {
      $tableData['data'] = "Deliverabe data is empty";
      $accept            = false;
    } //if fileContents are not emty
    
} //end if filecontents isset
$delTable  = "<table>";
$delTable .= "<tr>";
$delTable .= "<th>&nbsp;&nbsp;</th>";
foreach($tableData['headers'] as $hIndex => $header)
{	
	$delTable .= "<th>".$header."</th>";	
}
$delTable .= "</tr>";

foreach($tableData['data'] as $dIndex => $dArr)
{
	$delTable .= "<tr>";
	if($dArr['hasError'])
	{
		//if there is no error in this column
		$delTable .= "<td class='ui-state-ok'><span class='ui-icon ui-icon-check' style='margin-right:0.3em;'></span></td>";
	} else {
		$acceptFile = false;
		$delTable  .= "<td class='ui-state-error'><span class='ui-icon ui-icon-closethick' style='margin-right:0.3em;'></span></td>";
	}	
	unset($dArr['hasError']);	
	foreach($dArr as $key => $val) {
		$val = is_array($val) ? implode(";<br />",$val) : $val;
		$delTable .= "<td>".$val."</td>";		
	}	
	$delTable .= "</tr>";
}
$delTable .= "</table>";
?>
<form action="save_import.php" method="post" id="saveImport">
	<table class="noborder" style='margin: 0 auto;'>
	  <tr class="noborder">
	    <td class="noborder">
	    	<button id="accept" <?php echo ($acceptFile ? "" : "disabled='disabled'" ); ?> class="isubmit">
	    		<span class="ui-icon ui-icon-check" style="float:left;"></span>
	    		<span>Accept</span>
	    	</button>
	    	<input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_REQUEST['legislationid']; ?>"/>
	    	<input type="hidden" name="columnCount" id="columnCount" value="<?php echo $columnCount; ?>"/>

	    </td>
		<td class="noborder" >
			 <button id="reject" <?php echo ($acceptFile ? "" : "disabled='disabled'" ); ?> class="idelete">
	    		<span class="ui-icon ui-icon-closethick" style="float:left;"></span>
	    		<span>Reject</span>
	    	</button>
		</td>    
	  </tr>
	</table>
</form>


		</td>
	</tr>
</table>

</td></tr></table>
<?php  
if($acceptFile)
{
	$_SESSION[$_SESSION['modref']]['importdata'] = $data;
	$_SESSION[$_SESSION['modref']]['importtable'] = $delTable;
}
echo $delTable;
//echo "<pre>"; print_r($_SESSION[$_SESSION['modref']]['importdata']);
displayGoBack("", "");
?>
<style type=text/css>
//.noborder { border: 1px dashed #009900; }
</style>