<?php
$scripts = array("action.js", "jquery.ui.action.js");
include("../header.php");
$nObj = new DeliverableNaming();
$dOb = new Deliverable();
$deliverable = $dOb -> fetch( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>    
<script language="javascript">
$(function(){
	$("#display_action").action({editAction:true,deleteActions:true, deliverableId:$("#deliverableId").val(), controller : "newcontroller", page:"manage", section:"manage"});
})
</script>
<table class="noborder" style="table-layout:fixed;">
  <tr>
    <td class="noborder" width="50%">
		<table>
            	<tr>
            		<td colspan="2"><h4>Deliverable Details</h4></td>
            	</tr>
                <tr>
                  <th>Ref #:</th>
                  <td><?php echo $deliverable['ref']; ?></td>
                </tr>
               <tr>
                    <th><?php echo $nObj->getHeader("short_description"); ?>: </th>
                    <td><?php echo $deliverable['short_description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("description"); ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("legislation_section"); ?>: </th>
                    <td><?php echo $deliverable['legislation_section']; ?></td>
                </tr>				
               <!--  <tr>
                    <th><?php echo $nObj->getHeader("frequency_of_compliance"); ?>:</th>
                    <td><?php echo $deliverable['compliance_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("applicable_organisation_type"); ?>:</th>
                    <td><?php echo $deliverable['applicable_org_type']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("legislation_compliance_date"); ?>:</th>
                    <td><?php echo $deliverable['compliance_date']; ?></td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->getHeader("responsible_department"); ?>:</th>
                    <td><?php echo $deliverable['responsible']; ?></td>
                </tr>                
                <tr>
                    <th><?php echo $nObj->getHeader("functional_service"); ?>:</th>
                    <td><?php echo $deliverable['functional_service']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("accountable_person"); ?>:</th>
                    <td><?php echo $deliverable['accountable_person']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("responsibility_ower"); ?>:</th>
                    <td><?php echo $deliverable['responsiblity_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("legislative_deadline_date"); ?>:</th>
                    <td><?php echo $deliverable['legistation_deadline']; ?></td>
                </tr>
                <!-- <tr>
                    <th>Organisation KPI Ref</th>
                    <td><?php echo $deliverable['org_kpi_ref']; ?>:</td>
                </tr> --> 
                <tr>
                    <th><?php echo $nObj->getHeader("sanction"); ?>:</th>    
                    <td>
                      <?php echo $deliverable['sanction']; ?>
                    </td>
                </tr>     
                <tr>
                    <th><?php echo $nObj->getHeader("reference_link"); ?>:</th>    
                    <td><?php echo $deliverable['reference_link']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->getHeader("assurance"); ?>:</th>    
                    <td><?php echo $deliverable['assurance']; ?></td>
                </tr>                                             
                <tr>
                    <th><?php echo $nObj->getHeader("guidance"); ?>:</th>
                    <td><?php echo $deliverable['guidance']; ?></td>
                </tr> 
				<tr>
					<td class="noborder"><?php displayGoBack("", ""); ?></td>
					<td class="noborder"></td>
				</tr>
            </table>    
    </td>
    <td class="noborder" width="50%">
       <table id="display_action"></table>
    </td>
  </tr>
</table>
<input type="hidden" name="deliverableId" id="deliverableId" value="<?php echo $_GET['id']; ?>" />
