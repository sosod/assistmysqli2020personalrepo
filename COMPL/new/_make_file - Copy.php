<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/19/11
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */
include_once("../../library/dbconnect/dbconnect.php");
include_once("../class/model.php");
include_once("../class/basenaming.php");
include_once("../class/naming.php");
$deliverablenameObj = new Naming();
$headers            = $deliverablenameObj -> getHeaderList();
$fieldsList = array(
                array(
                           	$headers['short_description'],
                            $headers['description'],
                            $headers['legislation_section'],
                            $headers['compliance_frequency'],
                            $headers['applicable_org_type'],
                            'Is date reccuring',
                            $headers['compliance_date'],
                            'Number of days',
                            'Days option',                       
                            'Days from',         
                            $headers['responsible'],
                            $headers['functional_service'],
                            $headers['accountable_person'],
                            $headers['responsibility_owner'],
                            $headers['legislation_deadline'],
                            $headers['sanction'],
                            $headers['reference_link'],
                            $headers['assurance'],
                            $headers['compliance_to'],
                            $headers['main_event'],
                            $headers['sub_event'],
                            $headers['reporting_category'],
                            $headers['guidance'],
                            'Does this deliverable has actions'
                       ),
                  array(
                            "text",
                            "text",
                            "text",
                            "text - select options",
                            "text - select options",
                            "text(Yes/No)",
                            "date YYYY/MM/DD eg. (2012/06/17)",
                  			"number - days",                      
                  			"text - (Days/Working Days)",
                  			"text - select options",
                            "text - select options",
                            "text - select options",
                            "text - select options",
                            "text - select options",
                            "date YYYY/MM/DD eg. (2012/06/17)",
                            "text",
                            "text",
                            "text",
                            "text - select options",
                            "text - select options",
                            "text - select options",
                            "text - select options",
                            "text",
                  			"text(Yes/No)"
                       )

                );
$filename =  "deliverable.csv";
$fp       =  fopen($filename, "w+");
foreach($fieldsList as $fields)
{
    fputcsv($fp, $fields);
}
fclose($fp);
header('Content-Type: application/csv'); 
header("Content-length: " . filesize($filename)); 
header('Content-Disposition: attachment; filename="' . $filename . '"'); 
readfile($filename);
unlink($filename);
exit();
