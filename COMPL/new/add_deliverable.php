<?php 
error_reporting(-1);
$scripts = array("guidance.js","deliverable.js", "jquery.ui.deliverable.js");
include("../header.php");
$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById($_GET['id']);

$dnObj 	 	 = new DeliverableNaming();
$deliverableHeaders = $dnObj -> getNaming();
//$legislationNaming = new LegislationNaming();
//$legislationHeaders = $legislationNaming -> getNaming();
//$setupObj    = new SetupManager();

$categories	 = LegislativeCategoriesManager::getLegislationCategory($_GET['id']);

$orgSizes	 = OrganisationSizeManager::getLegislationOrganisationSizes($_GET['id']); 

$orgTypes    = LegislationOrganisationTypeManager::getLegislationOrganisationType($_GET['id']);

$orgCaps 	 = OrganisationCapacityManager::getLegislationOrganisationCapacity($_GET['id']);

$legtype   = LegislativetypeManager::getLegislationType($legislation['type']);

//$financialYearObj = new FinancialYear();
//$financialyear    = $financialYearObj -> fetch($legislation['financial_year']);

?>
<?php 
$text = "";
if(isset($_SESSION['deliverblesavedtext']) && (isset($_GET['saved'])  && $_GET['saved'] == 1))
{
	$text = '<p class="ui-widget ui-state ui-state-ok" style="padding:5px;">
		 	 <span class="ui-icon ui-icon-check" style="float:left;"></span>
		 	 <span style="margin-left:5px;">'.$_SESSION['deliverblesavedtext'].'</span></p>';
	echo $text;
	unset($_SESSION['deliverblesavedtext']);
	unset($_GET['saved']);
	$text = "";
} else {
	JSdisplayResultObj("");
}
JSdisplayResultObj("");
?>
<table class=noborder width=100%>
	<tr>
		<td class=noborder>
			<table class=noborder>
				<tr>
					<td class=noborder width=50%>
						<h2>Legislation Details</h2>
						<?php include("../common/legislation.inc.php"); ?>
<script type=text/javascript>
$(function() {
		$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
});
</script>
						<p><span class='float currentd'><img src="../../pics/tri_down.gif" style='vertical-align: middle;'> <span class='u blue'>Current Deliverables</span></span><?php displayGoBack("", ""); ?></p>
					</td>
					<td class=noborder>
						<h2>Deliverable Details</h2>
						<table width="100%" class=form>
							<tr>
								<td colspan="2"><div class='ui-widget ui-state-info' style="padding: 3px; font-size: 7pt">All fields marked with * are required fields</div></td>
							</tr>
							<tr>
								<td style="text-align:right;" colspan=2><?php 
										if((Legislation::IMPORTED & $legislation['legislation_status']) == Legislation::IMPORTED){
										 $controller->printRequestGuidance("");  	
										} 
									?>
								</td>
							</tr>
							<tr>
								<th><?php echo $dnObj->getHeader("short_description"); ?><em>*</em>:</th>    
								<td>
									<textarea name="short_description" id="short_description" cols="50" rows="7" placeholder="Deliverable Short Description"></textarea>
								</td>
							</tr>                  
							<tr>
								<th><?php echo $dnObj->getHeader("description"); ?>:</th>    
								<td>
									<textarea name="description" id="description" cols="50" rows='7' placeholder="Deliverable Description"></textarea>
								</td>
							</tr>  
							<tr>
								<th><?php echo $dnObj->getHeader("legislation_section"); ?><em>*</em>:</th>    
								<td><input type="text" name="legislation_section" id="legislation_section" placeholder="Deliverable Legislation Section" size="50" /></td>
							</tr>    
							<tr>
								<th><?php echo $dnObj->getHeader("compliance_frequency"); ?><em>*</em>:</th>    
								<td>
									<select name="compliance_frequency" id="compliance_frequency">
										<option value="">--compliance frequency--</option>
									</select>
								</td>
							</tr>    
							<tr>
								<th><?php echo $dnObj->getHeader("applicable_org_type"); ?>:</th>    
								<td>
									<select name="applicable_org_type" id="applicable_org_type">
									</select>        
								</td>
							</tr>    
							<tr>
								<th><?php echo $dnObj->getHeader("compliance_date"); ?><em>*</em>:</th>    
								<td>
								  <!--  <input type="text" name="compliance_date" id="compliance_date" value="" class="datepicker" /> -->
								  <div style="padding-top:10px;">
									  <table width="100%">
										<tr>
										  <td>Is the deadline date recurring within the financial year:</td>
										  <td>
											<select id="recurring" name="recurring">
												<option value="">--recurring--</option>
												<option value="1">Yes</option>
												<option value="0">No</option>
											</select>
										</td>
										<tr id="days" class="recurr nonfixed" style="display:none;">
											<td colspan="2"><input type="text" name="recurring_period" id="recurring_period" value="" placeholder="Days" />
												<select name="period" id="period">
												<option value="days">Days</option>
												<option value="weekly">Working Days</option>
												</select>
											</td>
										</tr>	                      	
										<tr class="recurr" style="display:none; text-align:center;">
											<td colspan="2" align="center">
												Days From : 
												<select name="recurring_type" id="recurring_type">
													<option value="">-- select -- </option>
													<option value="fixed" class="fixedoption">Fixed</option>
													<option value="start" class="nonfixed">Start Of Financial Year</option>
													<option value="end" class="nonfixed">End Of Financial Year</option>
													<option value="startofmonth" class="nonfixed">Start Of Month</option>
													<option value="endofmonth" class="nonfixed">End Of Month</option>
													<option value="startofquarter" class="nonfixed">Start Of Quarter</option>
													<option value="endofquarter" class="nonfixed">End Of Quarter</option>
													<option value="startofsemester" class="nonfixed">Start Of Semester</option>
													<option value="endofsemester" class="nonfixed">End Of Semester</option>
													<option value="startofweek" class="nonfixed">Start Of Week</option>
													<option value="endofweek" class="nonfixed">End Of Week</option>
												</select>
											</td>
										</tr>
										 <tr class="fixed" style="display:none;">
											<td colspan="2">
											Fixed <input type="text" name="compliance_date" id="compliance_date" value="" class="datepicker" readonly="readonly" placeholder="12-Jun-1970" />
											</td>
										</tr> 
									  </table>
								  </div>       
								</td>
							</tr>        
							<tr>
								<th><?php echo $dnObj->getHeader("responsible"); ?>:</th>    
								<td>
									<select name="responsible" id="responsible">
										<option value="">--responsible department--</option>
									</select>
								</td>
							</tr>            
							<tr>
								<th><?php echo $dnObj->getHeader("functional_service"); ?>:</th>    
								<td>
									<select name="functional_service" id="functional_service">
										<option  value="">--functional service--</option>
									</select>        
								</td>
							</tr>   
							<tr>
								<th><?php echo $dnObj->getHeader("accountable_person"); ?><em>*</em>:</th>    
								<td>
									<select name="accountable_person" id="accountable_person" multiple="multiple">
									</select>        
								</td>
							</tr>            
							<tr>
								<th><?php echo $dnObj->getHeader("responsibility_owner"); ?>:</th>    
								<td>
									<select name="responsibility_owner" id="responsibility_owner">
										<option value="">--responsibility person--</option>
									</select>        
								</td>
							</tr> 
							<tr>
								<th><?php echo $dnObj->getHeader("legislation_deadline"); ?>:</th>    
								<td>
								  <input type="text" name="legislation_deadline" id="legislation_deadline" value="<?php echo $legislation['legislation_date']; ?>" 
									  class="datepicker" readonly="readonly" onClick='showDatePicker(this); return false;' />
								</td>
							</tr>                 
							<!-- <tr>
								<th><?php echo $dnObj->getHeader("action_deadline_date"); ?>:</th>    
								<td>
								  <input type="text" name="action_deadline" id="action_deadline" value="" class="datepicker" />
								</td>
							</tr>                                  
							<tr>
								<th><?php echo $dnObj->getHeader("reminder_date"); ?>:</th>    
								<td>
								  <input type="text" name="reminder" id="reminder" value="" class="datepicker" />
								</td>
							</tr> -->
							<tr>
								<th><?php echo $dnObj->getHeader("sanction"); ?>:</th>    
								<td>
									<textarea name="sanction" id="sanction" cols="50" rows='7' placeholder="Deliverable sanction"></textarea>
								</td>
							</tr>                                                           
							<tr>
								<th><?php echo $dnObj->getHeader("reference_link"); ?>:</th>    
								<td>
								  <input type="text" name="reference_link" id="reference_link" placeholder="Deliverable Reference link" value="" size="50" />
								</td>
							</tr> 
						   <!-- <tr>
								<th><?php echo $dnObj->getHeader("org_kpi_ref"); ?>:</th>
								<td>
								  <input type="text" name="org_kpi_ref" id="org_kpi_ref" value="" />
								</td>
							</tr>                                                           
							<tr>
								<th><?php echo $dnObj->getHeader("individual_ref"); ?>:</th>    
								<td>
								  <input type="text" name="individual_kpi_ref" id="individual_kpi_ref" value="" />
								</td>
							</tr> -->   
							<tr>
								<th><?php echo $dnObj->getHeader("assurance"); ?>:</th>    
								<td>
								   <textarea name="assurance" id="assurance" cols="50" rows="7" placeholder="Deliverable Proof of evidence"></textarea>
								</td>
							</tr> 
							<tr>
								<th><?php echo $dnObj->getHeader("compliance_to"); ?><em>*</em>:</th>
								<td>
									<select name="compliance_to" id="compliance_to" multiple="multiple" placeholder="Deliverable Compliance To">
									</select>        
								</td>
							</tr>                
							<tr>
								<th><?php echo $dnObj->getHeader("main_event"); ?><em>*</em>:</th>    
								<td>
									<select name="main_event" id="main_event">
									</select>        
								</td>
							</tr>              
							<tr>
								<th><?php echo $dnObj->getHeader("sub_event"); ?><em>*</em>:</th>    
								<td>
									<select name="sub_event" id="sub_event" multiple="multiple">
									</select>        
								</td>
							</tr>
							<tr>
								<th><?php echo $dnObj->getHeader("guidance"); ?>:</th>
								<td>
								   <textarea name="guidance" id="guidance" cols="50" rows="7" placeholder="Deliverable Guidance"></textarea>
								</td>
							</tr>
							<tr>
								<th><?php echo $dnObj->getHeader("reporting_category"); ?><em>*</em>:</th>    
								<td>
								   <select name="reporting_category" id="reporting_category">
									  <option value="">--reporting category--</option>
									</select>         
								</td>
							</tr>                                
							<tr>
								<th><?php echo $dnObj->getHeader("does_this_deliverable_have_actions"); ?>:</th>
								<td>
								  <select id="has_actions" name="has_actions">
									<option value="1">Yes</option>
									<option value="0">No</option>
								  </select>
								</td>
							</tr>                 
							<tr>
								<th><?php echo $dnObj->getHeader("import"); ?>:</th>
								<td>
								  <input type="submit" name="import" id="import" value="Import" class="isubmit"/>
								</td>
							</tr>                 
							<tr>
								<td></td>    
								<td>
								  <input type="submit" name="save_deliverable" id="save_deliverable" value="Save" class="isubmit" />
								  <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_GET['id']; ?>" />
								  <input type="hidden" name="legislation_reference" id="legislation_reference" value="<?php echo $legislation['legislation_reference']; ?>" />                      
							<input type="hidden" name="actionowner" id="actionowner" value="" />
							<input type="hidden" name="actiondeadlinedate" id="actiondeadlinedate" value="" />
							<input type="button" name="cancel" id="cancel" value=" Cancel " class="idelete " />	
								</td>
							</tr>                    
						</table>  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class=noborder>
			<h2 id=existing>Existing Deliverables</h2>
			<div id="display_deliverable"></div>  
		</td>
	</tr>
</table>
<p><span class='gotop'><img src="../../pics/tri_up.gif" style='vertical-align: middle;'> <span class='u blue'>Back to Top</span></span></p>
			<script language="javascript">
			$(function(){
				$("#display_deliverable").deliverable({editDeliverable:true, legislationId:$("#legislationid").val(), page:"new", section:"new"});
				$(".currentd").css("cursor","pointer").click(function(){
				    $('html, body').animate({
						scrollTop: $("#existing").offset().top
					}, 1000);
				});
				$(".gotop").css("cursor","pointer").click(function(){
				    $('html, body').animate({
						scrollTop: 0
					}, 1000);
				});
			 // $("table.form th").css("width","25%");
			  //$(".noborder").css("border","dashed 2px #009900");
			  
			  //development purposes
			  $("select").each(function() {
					if($(this).prop("multiple")===true) {
						$(this).prop("size",10);
					}
					/*$sel = $(this);
					alert($sel.prop("id"));
					$options = $("option",$sel);
					$options.each(function() {
						//if($(this).val()=="") { 
						//	$(this).text("#"+$sel.prop("id"));
						//} else {
							$(this).text($(this).text()+" ["+$(this).val()+"]");
						//}
					});
					//$sel.prepend($("<option />",{text:"#"+$sel.prop("id"),value:"0"}));
					//$sel.prop("size",$("option",$sel).length);
					//$(".fixed").show();			
					//$("#days").show();*/
			  });
			});
			</script>