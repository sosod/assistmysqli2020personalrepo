<?php
$scripts = array("legislation.js");
include("../header.php");
$finObj = new FinancialYear();
$financialYears = $finObj -> fetchAll();
$namingObj = new LegislationNaming();
$headers   = $namingObj -> getNaming();
?>
<?php JSdisplayResultObj(""); ?>
<p>*All fileds marked with * are required fields</p>
<table width="50%">
	<tr>
		<th><?php echo $namingObj -> getHeader('financial_year'); ?>*</th>
		<td>
			<select name="financial_year" id="financial_year">
				<option value="">--financial years--</option>
				<?php
					foreach($financialYears as $fIndex => $fValues)
					{
					?>
						<option value="<?php echo $fValues['id']; ?>"><?php echo $fValues['start_date']." - ".$fValues['end_date']; ?></option>
					<?php
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
    	<th><?php echo $namingObj -> getHeader('name'); ?>*:</th>    
    	<td>
    		<textarea rows="5" cols="50" id="name" name="name" placeholder="legislation name"></textarea>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $namingObj -> getHeader('reference'); ?>*:</th>    
    	<td><input type="text" name="reference" id="reference" value="" placeholder="legislation reference number" size="50"/></td>
    </tr>
	<tr>
    	<th><?php echo $namingObj -> getHeader('legislation_number'); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="50" name="legislation_number" id="legislation_number" placeholder="legislation additional information"></textarea>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $namingObj -> getHeader('type'); ?>*:</th>    
    	<td>
        	<select name="type" id="type">
        		<option>--type--</option>
            </select>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $namingObj -> getHeader('category'); ?>:</th>    
    	<td>
        	<select name="category" id="category" multiple="multiple">
            </select>        
        </td>
    </tr>    
	<tr>
    	<th><?php echo $namingObj -> getHeader('organisation_size'); ?>:</th>    
    	<td>
        	<select name="organisation_size" id="organisation_size" multiple="multiple">
            </select>        
        </td>
    </tr>    
    <tr>
    	<th><?php echo $namingObj -> getHeader('organisation_type'); ?>:</th>    
    	<td>
        	<select name="organisation_type" id="organisation_type" multiple="multiple">
            </select>        
        </td>
    </tr>   
	<tr>
    	<th><?php echo $namingObj -> getHeader('organisation_capacity'); ?>:</th>    
    	<td>
        	<select name="organisation_capacity" id="organisation_capacity" multiple="multiple">
            </select>        
        </td>
    </tr>
	<!-- <tr>
    	<th>Responsible Business Patner:</th>
    	<td>
        	<select name="responsible_organisation" id="responsible_organisation">
            </select>
        </td>
    </tr> 
	<tr>
    	<th>Is the legislation available to non-business partner clients?:</th>
    	<td>
        	<select name="legislation_available" id="legislation_available">
        		<option value="0">No</option>
        		<option value="1">Yes</option>
            </select>
        </td>
    </tr> -->   		         
	<tr>
    	<th><?php echo $namingObj -> getHeader('legislation_date'); ?>*:</th>    
    	<td> 
          <input type="text" name="legislation_date" id="legislation_date" class="datepicker" value="" readonly="readonly" placeholder="01-Jan-1970"/>
        </td>
    </tr>                       
	<tr>
    	<th><?php echo $namingObj -> getHeader('hyperlink_to_act'); ?>:</th>    
    	<td>
          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="" placeholder="www.example.com/" size="50"/>
        </td>
    </tr> 
	<tr>
    	<th></th>    
    	<td>
          <input type="button" name="save" id="save" value=" Save " class="isubmit" />
		  <input type="hidden" name="main_legislation" id="main_legislation" value="" />
        </td>
    </tr>                    
</table>
<script type=text/javascript>
$(function() {
	$("#legislation_date").datepicker( "option", "yearRange", "1900:c+1" );
});
</script>