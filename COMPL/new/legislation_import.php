<?php
$scripts = array("legislation.js", "jquery.ui.legislation.js");
include("../header.php");
//$finObj         = new FinancialYear();
//$financialYears = $finObj -> fetchAll();
?>
<?php JSdisplayResultObj(""); ?>   

<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<script>
				$(function(){
					$("#legislation").legislation({importLegislation : true,
					                               legoption         : "available",
					                               page              : "new",
					                               section           : "import",
					                               url               :"../class/request.php?action=Legislation.legislationImport",
					                               autoLoad          : true
					                              });
				});
			</script>
			<div id="legislation"></div>			
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
