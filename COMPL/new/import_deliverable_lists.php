<?php error_reporting(-1);
$list = $_REQUEST['l'];
$page_title = "Deliverable >> Import >> ";
switch($list) {
	case "compliance_frequency": $page_title.="Compliance Frequency"; break;
	case "org_type": $page_title.="Organisational Type"; break;
	case "respdept": $page_title.="Responsible Department"; break;
	case "functional_service": $page_title.="Functional Service"; break;
	case "accountable_person": $page_title.="Accountable Person"; break;
	case "action_deadline": $page_title.="Action Deadline"; break;
	case "action_owner": $page_title.="Action Owner"; break;
		case "responsible_person": $page_title.="Responsible Person"; break;
	case "compliance_to": $page_title.="Compliance To"; break;
	case "events": $page_title.="Main Event"; break;
	case "sub_event": $page_title.="Sub Event"; break;
	case "reporting_category": $page_title.="Reporting Category"; break;
	case "recurringtype": $page_title.="Recurring Type"; break;
}
$page_title.=" List";

$scripts = array("deliverable.js");
include("../header.php");

$values = array();
$sort = true;
$multiple = false;

$headings = array("Allowable Values");

switch($list) {
	case "reporting_category":
		$object = new ReportingCategories();
		$source_values = $object -> getReportingCategories(); 
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$values[] = $v;
		}
		break;
	case "compliance_frequency":
		$object = new ComplianceFrequency();
		$source_values = $object -> getAll(); 
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$values[] = $v;
		}
		break;
	case "org_type":
		$object  = new OrganisationType();
		$source_values = $object -> getAll( );
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$values[] = $v;
		}
		break;
	case "respdept":
		$sort = false;
		$object = new ClientDepartment();
		$source_values = $object -> getAll( );
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$values[] = $v;
		}
		break;
	case "functional_service":
		$object = new FunctionalServiceManager();
		$source_values = $object -> getClientFunctionalServices();
		foreach($source_values as $i => $v) {
			//$i = $arr['id'];
			//$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$values[] = $v;
		}
		break;
	case "accountable_person":
		$multiple = true;
		$object = new AccountablePersonManager();
		$source_values = $object->getAll();
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['title'];
			$u = $arr['user'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; } elseif($v!=$u) { $v.="</td><td>".$u; } else { $v.="</td><td>"; }
			$values[] = $v;
		}
		$headings[] = "Associated Users";
		break;
	case "compliance_to":
		$multiple = true;
		$object = new ComplianceToManager();
		$source_values = $object->getAll();
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['title'];
			$u = $arr['user'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; } elseif($v!=$u) { $v.="</td><td>".$u; } else { $v.="</td><td>"; }
			$values[] = $v;
		}
		$headings[] = "Associated Users";
		break;
	case "action_owner":
	case "responsible_person":
		$object = new User();
		$source_values = $object->getAll();
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['user'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; } 
			$values[] = $v;
		}
		break;
	case "events":
		$object = new Event();
		$source_values = $object -> getAll();
		//ASSIST_HELPER::arrPrint($source_values);
		$h = array("Ref");
		foreach($source_values as $key => $arr) {
			$i = $arr['id'];
			$v = $arr['name'];
			if(strlen($v)==0 && $i==1) { $v = $unspecified_text; }
			$v = $i."</td><td>".$v;
			$values[] = $v;
		}
		break;
	case "sub_event":
		$sort = false;
		$multiple=true;
		$object = new SubEvent();
		$source_values = $object -> getAll();
		//ASSIST_HELPER::arrPrint($source_values);
		$h = array("Main Event Ref","Main Event","Sub Event Ref");
		$events = array();
		foreach($source_values as $key => $arr) {
			$eid = $arr['eventid'];
			if(!isset($events[$eid])) {
				$events[$eid] = array(); 
			}
			$events[$eid][] = $arr;
		}
		foreach($events as $e => $source_values) {
			foreach($source_values as $key => $arr) {
				$i = $arr['id'];
				$v = $arr['name'];
				if(strlen($v)==0 && $i==1) { $v = $unspecified_text; } 
				$v = $arr['eventid']."</td><td>".$arr['event']."</td><td>".$arr['id']."</td><td>".$v;
				$values[] = $v;
			}
		}
		$hint = " within the same Main Event group (as defined by the Main Event Reference)";
		break;
	case "recurringtype":
		$values = array();
		$values[] = "Fixed";
		$values[] = "Start Of Financial Year";
		$values[] = "End Of Financial Year";
		$values[] = "Start Of Month";
		$values[] = "End Of Month";
		$values[] = "Start Of Quarter";
		$values[] = "End Of Quarter";
		$values[] = "Start Of Semester";
		$values[] = "End Of Semester";
		$sort = false;
		break;
	case "recurringtype":
		$values = array();
		$values[] = "Fixed";
		$values[] = "Start Of Financial Year";
		$values[] = "End Of Financial Year";
		$values[] = "Start Of Month";
		$values[] = "End Of Month";
		$values[] = "Start Of Quarter";
		$values[] = "End Of Quarter";
		$values[] = "Start Of Semester";
		$values[] = "End Of Semester";
		$sort = false;
		break;
	case "action_deadline":
		$headings[] = "Comment";
		$values = array();
		$sort = false;
		$extra_info = "Actions will only be created with deadline dates which fall within the Financial Year AND which fall on or before the Legislation Compliance Date";
		$values[] = "Date as YYYY/MM/DD</td><td>Will result in 1 action, please indicate the date in format YYYY/MM/DD";
		$values[] = "Start Of Financial Year</td><td>Will result in 1 action with the deadline set to the first day of the Financial Year.";
		$values[] = "End Of Financial Year</td><td>Will result in 1 action with the deadline set to the last day of the Financial Year.";
		$values[] = "Start Of Month</td><td>Will result in 1 action for each month in the Financial Year with the deadline set to the first day of each month.";
		$values[] = "End Of Month</td><td>Will result in 1 action for each month in the Financial Year with the deadline set to the last day of each month.";
		$values[] = "Start Of Quarter</td><td>Will result in 1 action for each 3 month period during the Financial Year with the deadline set to the first day of each period.";
		$values[] = "End Of Quarter</td><td>Will result in 1 action for each 3 month period during the Financial Year with the deadline set to the last day of each period.";
		$values[] = "Start Of Semester</td><td>Will result in 1 action for each 4 month period during the Financial Year with the deadline set to the first day of each period.";
		$values[] = "End Of Semester</td><td>Will result in 1 action for each 4 month period during the Financial Year with the deadline set to the last day of each period.";
		$values[] = "Start Of 6-month Period</td><td>Will result in 1 action for each 6 month period during the Financial Year with the deadline set to the first day of each period.";
		$values[] = "End Of 6-month Period</td><td>Will result in 1 action for each 6 month period during the Financial Year with the deadline set to the last day of each period.";
		break;
}
if($sort) {
	sort($values,SORT_STRING);
}
ASSIST_HELPER::displayResult(array("error","Values are CaSe-SeNsItIvE.  Please capture the values in the template exactly as they are found in the 'Allowable Values' column."));
if($multiple) {
	ASSIST_HELPER::displayResult(array("info","Multiple values are permitted".(isset($hint) ? $hint : "").".  Please separate each value with a semi-colon (;) when completing the import template."));
}
if(isset($extra_info)) {
	ASSIST_HELPER::displayResult(array("info",$extra_info));
}

if(isset($h) && count($h)>0) {
		$hm = $headings;
		$headings = $h;
		$headings = array_merge($headings,$hm);
}

?>
<table>
	<?php
	echo "<tr><th>".implode("</th><th>",$headings)."</th></tr>";
	echo "<tr><td>".implode("</td></tr><tr><td>",$values)."</td></tr>";
?>
</table>
<div><?php displayGoBack("", ""); ?></div>

<script type=text/javascript>
$(function() {
	$("span.ui-icon-closethick").addClass("ui-icon-alert").removeClass("ui-icon-closethick");
});
</script>