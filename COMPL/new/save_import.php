<?php
//error_reporting(-1);

$scripts = array("deliverable.js");
include("../header.php");
$legislationId = $_REQUEST['legislationid'];
$legislation_id = $legislationId;
$saving        = "<p>Saving the imported data . . . <img src='../images/loaderA32.gif' /></p>"; 
$dsaved        = 0;
$dunsaved      = 0;
$del           = new ClientDeliverable();
$errorSave     = "";
$hasActions    = true;
$multiple      = array(
						"sub_event" 		=> array("t" => "subevent_deliverable", "key" => "subevent_id"),
						"compliance_to" 	=> array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
						"accountable_person"  => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
	  		);
	
foreach($_SESSION[$_SESSION['modref']]['importdata'] as $sIndex =>  $dataArr)
{
	unset($dataArr['hasError']);
	$dataCount = count($dataArr); 	
	if($dataCount == ($_REQUEST['columnCount']+2))
	{
		//$hasActions                    = $dataArr['has_actions'];
		//$dataArr['hasActions']         = $dataArr['has_actions'];
        //$dataArr['actiondeadlinedate'] = $dataArr['legislation_deadline'];
        //$dataArr['actionowner']        = $dataArr['responsibility_owner'];		
		//unset($dataArr['has_actions']);
		$insertdata     = array();
		$multipleInsert = array();
		foreach($dataArr as $dIndex => $dVal)
		{			  
		    $insertdata[$dIndex]      = $dVal;		
		}
		$response =  $del -> saveClientImportDeliverable($insertdata);
		if(!$response['error'])
		{
			$dsaved++;  	
		} else {
			$dunsaved++;
		}
	} else {
		$dunsaved++;		
	}	
}
?>
<table class=noborder width=100% style='margin-bottom: 15px'>
	<tr>
		<td class=noborder width=50%>
			<h2>Legislation Details</h2>
			<?php include("../common/legislation.inc.php"); ?>
<script type=text/javascript>
$(function() {
		$("#legislation_details_table tr").each(function() {
			if($(this).hasClass("min_display")) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		$("#legislation_details_table th").css("width","200px");
});
</script>
			
		</td>
		<td class=noborder style='vertical-align: bottom'>

			<table class="noborder" style='margin: 0 auto'>

				<tr>
					<td class="noborder">
						<?php 
							if($errorSave == "") {
						?>
						<p class="ui-widget ui-state ui-state-ok" style="padding:5px; clear:both; width:450px;">
						  <span class="ui-icon ui-icon-check" style="float:left;"></span>
						  <span>
							<?php echo $dsaved." deliverables successfully saved"; ?>
						  </span>
						</p>
						<?php 
							} else {
						?>
						<p class="ui-widget ui-state ui-state-error" style="padding:5px; clear:both; width:450px;">
						  <span class="ui-icon ui-icon-info" style="float:left;"></span>
						  <span>
							<?php echo $errorSave; ?>
						  </span>
						</p>		
						<?php 
							}
						?>
					</td>
				</tr><tr>
					<td class="noborder">
						<?php 
						
							echo '<p class="ui-widget ui-state ui-state-error" style="padding:5px; width:450px;">';
							echo '<span class="ui-icon ui-icon-closethick" style="float:left;"></span>';
							echo '<span>';
							  echo '<span>'.$dunsaved.'  deliverables were not saved.</span><br />';
							echo '</span>';			 			 
							echo '</p>';			
						?> 
					</td>
				</tr>
			</table>
</td></tr></table>
<?php 
$tableImported = $_SESSION[$_SESSION['modref']]['importtable'];
//unset($_SESSION[$_SESSION['modref']]['importtable']);
//unset($_SESSION[$_SESSION['modref']]['importdata']);
unset($data);
echo $tableImported;
unset($saving);
?>
