<?php
$scripts = array("deliverable.js");
include("../header.php");
//$data = array();
$legislationId = $_REQUEST['legislationid'];
$legislation_id = $legislationId;
$dnObj         = new Naming();
$dnObj -> getNaming(); 



$legislationObj = new ClientLegislation();
$legislation    = $legislationObj -> findById($legislation_id);




?>
<table class=noborder>
	<tr>
		<td class=noborder width=50%>
			<h2>Legislation Details</h2>
			<?php include("../common/legislation.inc.php"); ?>
<script type=text/javascript>
$(function() {
		$("#legislation_details_table tr:last, #legislation_details_table tr:first").hide();
});
</script>
<?php $helper->displayGoBack(); ?>			
		</td>
		<td class=noborder>
			<h2>Deliverable Import</h2>
			 <table width=450>
			  <tr>
				<th>Generate CSV:</th>
				<td>
					<form action="make_file.php" method="post" enctype="multipart/form-data">
					 <input type="submit" name="generate_csv" id="generate_csv" value="Generate" />
					</form>
				</td>
			  </tr>  
			  <tr>
				<th width=100><span class="ui-icon ui-icon-info" style="background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); float: right;">&nbsp;</span>Import File:</th>
				<td>
					<form action="process_import.php" method="post" enctype="multipart/form-data">  
						<table width="100%" class="noborder">
							<tr>
								<td class="noborder">
									<input type="file" name="import_file" id="import_file" />
								</td>
								<td class="noborder"> 
									<input type="submit" name="save_import" id="save_import" value="Import" class="isubmit" />
									<input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_REQUEST['legislationid']; ?>" />
									
								</td>
							</tr>    		
						</table>
					</form>
				</td>
			  </tr> 
			  </table>    
			<span class=float>
  		<p class="ui-widget ui-state ui-state-info" style='padding: 7px; width:300px' ><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			Aftering clicking the Import button, please review the information and then either click the Accept or Reject button.  <br />No information will be imported until the Accept button has been clicked.
  		</p>
			</span>
<table class=noborder><tr><td class=noborder>
<h4>Option Lists</h4>
<table class=form width=450>
    <tr>
        <th><?php echo $dnObj->getHeader("compliance_frequency"); ?>*</th>
        <td><a href="import_deliverable_lists.php?l=compliance_frequency">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("applicable_org_type"); ?></th>
        <td><a href="import_deliverable_lists.php?l=org_type">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("legislation_compliance_date_recurring_types"); ?>*</th>
        <td><a href="import_deliverable_lists.php?l=recurringtype">Select Options</a></td>
    </tr>      
    <tr>
        <th><?php echo $dnObj->getHeader("responsible"); ?></th>
        <td><a href="import_deliverable_lists.php?l=respdept">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("functional_service"); ?></th>
        <td><a href="import_deliverable_lists.php?l=functional_service">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("accountable_person"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=accountable_person">Select Options</a> </td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("responsibility_owner"); ?></th>
        <td><a href="import_deliverable_lists.php?l=responsible_person">Select Options</a> </td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("compliance_to"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=compliance_to">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("main_event"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=events">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("sub_event"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=sub_event">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("reporting_category"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=reporting_category">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("action_owner"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=action_owner">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $dnObj->getHeader("action_deadline"); ?><em>*</em></th>
        <td><a href="import_deliverable_lists.php?l=action_deadline">Select Options</a></td>
    </tr>
</table>
</td></tr></table>
<input type="hidden" name="legislationid" id="legislationid" value="<?php $_GET['legislationid']?>" class="idelete" />
		</td>
	</tr>
</table>