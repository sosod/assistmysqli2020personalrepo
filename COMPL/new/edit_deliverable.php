<?php 
$scripts = array("guidance.js","deliverable.js");
include("../header.php");
JSdisplayResultObj(""); 
$deliverableFormObj = new DeliverableEditDisplay($_GET['id']);
$deliverableFormObj->displayForm(new Form(), new DeliverableNaming());
?>
<table class="noborder" width="80%">    
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>    
        <td class="noborder"><?php displayAuditLogLink("deliverable_edit", false); ?></td>
    </tr>                                  
</table>  
