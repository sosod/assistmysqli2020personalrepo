<?php 
    //require 'inc_session.php';
//require_once("module/autoloader.php");
$_REQUEST['jquery_version'] = "1.10.0";
//$my_assist->displayPageHeader();




//$my_assist->arrPrint($menu);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>assist.Action4u.co.za</title>
<base target="main">
	<link href="/library/jquery-ui-1.10.0/css/jquery-ui.css?1456142536" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery.min.js?1456142536"></script>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery-ui.min.js?1456142536"></script>

	<link rel="stylesheet" href="/assist.css?1456142536" type="text/css">
	<script type ="text/javascript" src="/assist.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?1456142536"></script>

</head>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<style type="text/css" >
	table {
		border: 2px solid #374EA2;
	}
	table td {
		border: 1px solid #374EA2;
		padding: 10px;
		line-height: 125%;
	}
	button { margin-top: 10px; }
</style>
<table style=' margin: 10px; width: 200px'>
	<tr><td class=center>
		Your session will expire in <label id=lbl_timeout>30</label> seconds.
	</td></tr>
	<tr><td class=center>
		Do you wish to extend your session?<br/>
		<button id=btn_yes>Yes</button>  <button id=btn_no>No</button>
	</td></tr>
</table>

<script type="text/javascript" >
	var rem = 30;
	var my_timeout = setInterval(timeOut,1000);
		
	function timeOut() {
		$(function() {
			rem--;
			console.log(rem);
			if(rem<0) {
				window.opener.document.location.href = "logout.php?action=TIME";
				window.close();
				//alert("timed out");
				clearInterval(my_timeout);
			} else {
				$("#lbl_timeout").html(rem);
				//setTimeout(timeOut,1000);
			}
		});
	}
	$(function() {
		$("#btn_yes").button({
			icons:{primary:"ui-icon-check"}
		}).click(function() {
			var r = AssistHelper.doAjax("inc_timeout.php","action=extend");
			window.opener.document.location.href = window.opener.document.location.href;
			window.close();
		}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"});
		
		$("#btn_no").button({
			icons:{primary:"ui-icon-closethick"}
		}).click(function() {
			rem = -1;
			timeOut();
		}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #CC0001"});
	});
</script>

</body>

</html>
