<?php
include("inc_ignite.php");
$bankid = $_GET['b'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delDep(id) {
    if(confirm("Are you sure you want to delete deposit "+id+"?"))
    {
        document.location.href = "admin_del.php?i="+id;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_bank WHERE bankid = ".$bankid;
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$banktext = $row['banktext']." - ".$row['banktype'];
mysql_close();
?>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Unclaimed Deposits - Admin: Delete deposits for <?php echo($banktext); ?></b></h1>
<p>&nbsp;</p>
<?php
    $u = 0;
    $totamt = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_deposit, assist_".$cmpcode."_".$modref."_list_bank WHERE udbankid = bankid AND bankid = ".$bankid." AND udclaimyn = 'N' ORDER BY udsort, udreceipt ASC";
    include("inc_db_con.php");
    $b = mysql_num_rows($rs);
    if($b == 0)
    {
        echo("<p>There are no unclaimed deposits for this bank.</p>");
    }
    else
    {
?>
<table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader>Date Deposited</td>
        <td class=tdheader>Bank</td>
        <td class=tdheader>Receipt</td>
        <td class=tdheader>Bank<br>Statement</td>
        <td class=tdheader>Narration</td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        $u++;
        $totamt = $totamt+$row['udamount'];
        ?>
        <tr>
            <td class=tdheader><?php echo($row['udid']);?></td>
            <td class=tdgeneral><?php echo(date("d M Y",$row['uddepdate'])); ?>&nbsp;</td>
            <td class=tdgeneral><?php echo($banktext); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udreceipt']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udbankstat']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udnarration']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral align=right>&nbsp;&nbsp;R <?php echo(number_format($row['udamount'],2)); ?>&nbsp;</td>
            <td class=tdheader>&nbsp;<input type=button value="Del " onclick="delDep(<?php echo($row['udid']); ?>)">&nbsp;</td>
        </tr>
        <?php
    }
    ?>
    <tr>
        <td class=tdheader style="text-align:right" colspan=5>Total amount unclaimed in <?php echo($u); ?> deposits:</td>
        <td class=tdheader align=right>&nbsp;R&nbsp;<?php echo(number_format($totamt,2)); ?>&nbsp;</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
</table>
<?php
}
mysql_close();
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>

</html>
