<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Unclaimed Deposits Journal</title>
</head>
<style type=text/css>
<!--
.fc {color: #006600}
p {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
	color: #000000;
	margin: 10px 0px 10px;
	padding: 0px 0px 0px 5px;
}
h1 {
	font-size: 17pt;
	margin: 0px 0px 10px;
	padding: 0px 0px 2px 2px;
	font-family: "Comic Sans MS", Arial, Helvetica, sans-serif;
	color: #cc0001;
	letter-spacing: 0.07em;
	line-height: 22px;
	font-weight: bold;
	text-decoration: underline;
}
h2 {
	font-size: 14pt;
	margin: 20px 0px 10px;
	padding: 0px;
	font-family: "Comic Sans MS", Arial, Helvetica, sans-serif;
	color: #dd1a20;
	letter-spacing: 0.07em;
    font-weight: bold;
	line-height: 22px;
}
.TDgeneral {
	color: #000000;
	background-color: #FFFFFF;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}

-->
</style>

<body topmargin=0>
<h1 class=fc style="margin-top: 5px">Unclaimed Deposits Journal</h1>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Unclaimed Deposit Detail</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral"  width="150"><b>Date Deposited:</b></td>
		<td colspan="2" class="tdgeneral" width=400>1 January 1970</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Date Claimed:</b></td>
		<td colspan="2" class="tdgeneral">1 January 1970</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Bank:</b></td>
		<td colspan="2" class="tdgeneral">Nedbank - Type</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Receipt Number:</b></td>
		<td colspan="2" class="tdgeneral">T11111</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Bank Statement Num.:</b></td>
		<td colspan="2" class="tdgeneral">B/S888</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Narration:</b></td>
		<td colspan="2" class="tdgeneral">NARRATION TEXT HERE</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Amount:</b></td>
		<td colspan="2" class="tdgeneral">&nbsp;R 1,000.00</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Instruction to the Finance Department</h2>
		<p style="margin-top: 0px; margin-bottom: 0px"><i>Please allocate the funds identified to my file with the following
		details:</i></td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Client Number:</b></td>
		<td colspan="2" class="tdgeneral">Client number as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Client Name:</b></td>
		<td colspan="2" class="tdgeneral">Client name as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>File / Matter Number:</b></td>
		<td colspan="2" class="tdgeneral">Matter number as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>File / Matter Name:</b></td>
		<td colspan="2" class="tdgeneral">Matter descrip as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Narration / Comment:</b></td>
		<td colspan="2" class="tdgeneral">Comment as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Type:</b></td>
		<td colspan="2" class="tdgeneral">Retainer&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Document Authorisation by the Professional</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Requested by:</td>
		<td colspan="2" class="tdgeneral">User name of user who claimed UD&nbsp;</td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Authorised signature:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Authorised by:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Date:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Accounting by Finance Department</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150">&nbsp;</td>
		<td align="center" class="tdgeneral" width="200"><b>Debit</b></td>
		<td align="center" class=tdgeneral width="200"><b>Credit</b></td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Account number:</td>
		<td class="tdgeneral">F02306&nbsp;</td>
		<td class="tdgeneral">Matter number as provided by user&nbsp;</td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Processed by:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Date:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Notes:</h2>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">&nbsp;<p>&nbsp;</p>
		<p>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral"><i>Please ensure that the documentary proof of the
		claim of the unclaimed deposit is attached.</td>
	</tr>
</table>

</body>

</html>
