<?php
$get_rtk = true; $get_co = true;
require_once 'inc_header.php';

//arrPrint($_REQUEST);
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	if(isset($_REQUEST['call_id']) && is_numeric($_REQUEST['call_id']) && isset($_REQUEST['call_admin_tkid']) && strlen($_REQUEST['call_admin_tkid'])>3) {
		$ci = $_REQUEST['call_id'];
		$cat = $_REQUEST['call_admin_tkid'];
		$sql = "UPDATE help_client_call SET call_admin_tkid = '$cat' WHERE call_id = ".$ci;
		$mar = db_sql("H",$sql);
		if($mar>0) {
			$result = array("ok","Client Request assigned successfully.");
		} else {
			$result = array("error","An error occurred while trying to assign the client request.  Please try again.");
		}
	} else {
		$result = array("error","An error occurred while trying to assign the client request.  Please try again.");
	}
}

if(isset($result)) { displayResult($result); }

$sql = "SELECT * FROM help_client_call WHERE call_admin = '".strtoupper($cmpcode)."' AND call_statusid = 1 AND call_admin_tkid = '' AND call_active = true";
$objects = mysql_fetch_all1("H",$sql,"call_id");

echo "<table>";
	echo "<tr>";
		foreach($headings['client'] as $fld => $h) {
			echo "<th>".$h['text']."</th>";
		}
		echo "<th></th>";
	echo "</tr>";
	foreach($objects as $obj_id => $obj) {
		echo "<tr>";
			foreach($headings['client'] as $fld => $h) {
				echo "<td>";
				switch($h['type']) {
					case "LIST":
						$l = $lists[$h['table']]['data'][$obj[$fld]];
						if(isset($l['color']) && strlen($l['color'])>0) {
							echo "<div style=\"background-color: #".$l['color']."; color: #FFFFFF;\" class=center>";
						}
						echo $l['value'];
						echo "</div>";
						break;
					case "DATE":
						echo date("d M Y H:i:s",strtotime($obj[$fld]));
						break;
					case "RTK":
						echo "<select id=\"rtk-".$obj_id."\"><option selected value=X>--- SELECT ---</option>";
							foreach($rtk as $tki => $tk) {
								echo "<option value=\"$tki\">".$tk['tkn']."</option>";
							}
						echo "</select>";
						break;
					case "TEXT":
						echo str_replace(chr(10),"<br />",$obj[$fld]);
						break;
					case "MOD":
						$m = explode("_",$obj[$fld]);
						echo $m[1];
						break;
					case "CMP":
						if(isset($cmp_objects[$obj[$fld]])) {
							echo $cmp_objects[$obj[$fld]]['cmpname']." [".$obj[$fld]."]";
						} else {
							echo $obj[$fld];
						}
						break;
					default:
						echo $obj[$fld];
						break;
				}
				echo "</td>";
			}
			echo "<td><input type=button value=Assign id=\"".$obj_id."\" /> <input type=button value=\"Assign to Me\" id=\"".$obj_id."\" /></td>";
		echo "</tr>";
	}

echo "</table>";
?>
<form method=POST action=<?php echo $self; ?> id=ass>
	<input type=hidden name=act value=SAVE />
	<input type=hidden id=i name=call_id value="" />
	<input type=hidden id=cat name=call_admin_tkid value="" />
</form>
<script type=text/javascript>
	$(function() {
		$("input:button").click(function() {
			i = $(this).attr("id");
			v = $(this).val();
			if(v=="Assign to Me") {
					$("#ass #i").val(i);
					$("#ass #cat").val("<?php echo $tkid; ?>");
					$("#ass").submit();
			} else {
				tk = $("#rtk-"+i).attr("value");
				if(tk=="X") {
					alert("Please select a respondant from the drop down list.");
				} else {
					$("#ass #i").val(i);
					$("#ass #cat").val(tk);
					$("#ass").submit();
				}
			}
		});
	});
</script>
</body>
</html>