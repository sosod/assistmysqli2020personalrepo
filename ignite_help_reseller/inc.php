<?php
function breadCrumb($bc) {
	global $self;
	if($self!=$bc)
		return "<a href=".$bc." class=breadcrumb>";
	else
		return false;
}


function displayDetail($h,$val) {
	global $lists;
	global $rtk;
	global $cmp_objects;
	global $tkname, $tkid;
	$display = "";
	switch($h['type']) {
		case "LIST":
			$l = $lists[$h['table']]['data'][$val];
			if(isset($l['color']) && strlen($l['color'])>0) {
				$display = "<div style=\"background-color: #".$l['color']."; color: #FFFFFF;\" class=center>";
			}
			$display.= $l['value'];
			$display.= "</div>";
			break;
		case "DATE":
			$display = date("d M Y H:i:s",strtotime($val));
			break;
		case "RTK":
			if($val==$tkid) {
				$display = $tkname;
			} elseif(isset($rtk[$val])) {
				$display = $rtk[$val]['tkn'];
			} else {
				$display = $val;
			}
			break;
		case "MOD":
			$m = explode("_",$val);
			$display = $m[1];
			break;
		case "CMP":
			if(isset($cmp_objects[$val])) {
				$display = $cmp_objects[$val]['cmpname']." [".$val."]";
			} else {
				$display = $val;
			}
			break;
		case "TEXT":
			$display = str_replace(chr(10),"<br />",$val);
			break;
		default:
			$display = $val;
			break;
	}
	return $display;
}

function displayFormField($h,$fld) {
	global $lists;
	global $rtk;
	global $cmp_objects;
	global $tkname, $tkid;
	$display = "";
	switch($h['type']) {
		case "LIST":
			$lt = $lists[$h['table']]['data'];
			$display = "<select name=$fld><option value=0 selected>--- SELECT ---</option>";
			foreach($lt as $i => $l) {
				$display.="<option value=$i>".$l['value']."</option>";
			}
			$display.="</select>";
			break;
		case "DATE":
			if($fld=="log_date") {
				$display = date("d M Y H:i:s");
			} else {
				$display = "<input type=text class=datepicker name=$fld />";
			}
			break;
		case "RTK":
			if($val==$tkid) {
				$display = $tkname;
			} elseif(isset($rtk[$val])) {
				$display = $rtk[$val]['tkn'];
			} else {
				$display = $val;
			}
			break;
		case "MOD":
			$m = explode("_",$val);
			$display = $m[1];
			break;
		case "CMP":
			if(isset($cmp_objects[$val])) {
				$display = $cmp_objects[$val]['cmpname']." [".$val."]";
			} else {
				$display = $val;
			}
			break;
		case "TEXT":
			$display = "<textarea rows=5 cols=50 name=$fld></textarea>";
			break;
		default:
			$display = $val;
			break;
	}
	return $display;
}
?>