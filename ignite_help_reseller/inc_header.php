<?php
require_once '../inc_session.php';
require_once '../inc_db_help.php';
require_once '../ignite_help_assist/inc_all.php';
require_once 'inc.php';
require_once '../inc_sms.php';
//error_reporting(-1);

$ignite_help_mobile = "27797120000";
$ignite_help_email = "helpdesk@actionassist.co.za";

$bpa = $_SESSION['ia_cmp_reseller'];
if(!isset($_SESSION['bpa_details'])) {
	$sql = "SELECT r.*, c.cmpname FROM assist_reseller r 
			INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
			WHERE r.cmpcode = '".strtoupper($bpa)."'";
	$rs = db_query("A",$sql);
		$bpa_details = mysql_fetch_assoc($rs);
		$_SESSION['bpa_details'] = $bpa_details;
	mysql_close();
} else {
	$bpa_details = $_SESSION['bpa_details'];
}

/*
if(isset($get_co) && $get_co == true) {
	$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpreseller = '".strtoupper($cmpcode)."' ORDER BY cmpcode, cmpname";
	$cmp_objects = mysql_fetch_all1("A",$sql,"cmpcode");
} else {
	$cmp_objects = array();
}

if(isset($get_rtk) && $get_rtk == true) {
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as tkn FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 ORDER BY tkname, tksurname";
	$rtk = mysql_fetch_all1("C",$sql,"tkid");
} else {
	$rtk = array();
}
*/
$dbheadings = getHeadings(array("R_CALL","R_LOG"),"R");

$lists = getLists((isset($list_active) ? $list_active : false));

$headings = array(
	'client' => array(
		'call_ref' => array(
			'field'=> "call_ref",
			'text'=>"Request Ref",
			'display' => "N",
			'type'=>"VC",
			'table'=>"",
			'maxlen'=>50,
		),
		'call_date'		=> array(
			'field'		=>	"call_date",
			'text'		=>	"Date Logged",
			'display' 	=>	"Y",
			'type'		=>	"DATE",
			'table'		=>	"",
			'maxlen'	=>	0,
		),
		'call_cmpcode' => array(
			'field'=> "call_cmpcode",
			'text'=>"Client Company",
			'display' => "Y",
			'type'=>"CMP",
			'table'=>"cmp_objects",
			'maxlen'=>50,
		),
		'call_tkname' => array(
			'field'=> "call_tkname",
			'text'=>"User",
			'display' => "Y",
			'type'=>"VC",
			'table'=>"",
			'maxlen'=>250,
		),
		'call_modref' => array(
			'field'=> "call_modref",
			'text'=>"Module",
			'display' => "Y",
			'type'=>"MOD",
			'table'=>"",
			'maxlen'=>250,
		),
		'call_message' => array(
			'field'=> "call_message",
			'text'=>"Message",
			'display' => "Y",
			'type'=>"TEXT",
			'table'=>"",
			'maxlen'=>1000,
		),
		'call_statusid'	=> array(
			'field'		=>	"call_statusid",
			'text'		=>	"Status",
			'display' 	=>	"Y",
			'type'		=>	"LIST",
			'table'		=>	"help_list_status",
			'maxlen'	=>	0,
		),
		'call_admin_tkid'	=> array(
			'field'		=>	"call_admin_tkid",
			'text'		=>	"Respondant",
			'display'	=>	"Y",
			'type'		=>	"RTK",
			'table'		=>	"timekeep",
			'maxlen'	=>	0,
		),
	),
	//'reseller'=> array(
	//),
	'update'=>array(
		'log_date'		=>	array(
			'field'		=>	"log_date",
			'text'		=>	"Date Updated",
			'display'	=>	"Y",
			'type'		=>	"DATE",
			'table'		=>	"",
			'maxlen'	=>	0,
		),
		'log_message'	=>	array(
			'field'		=>	"log_message",
			'text'		=>	"Update",
			'display'	=>	"Y",
			'type'		=>	"TEXT",
			'table'		=>	"",
			'maxlen'	=>	0,
		),
		'log_statusid'	=>	array(
			'field'		=>	"log_statusid",
			'text'		=>	"Status",
			'display'	=>	"Y",
			'type'		=>	"LIST",
			'table'		=>	"help_list_status",
			'maxlen'	=>	0,
		),
	),
);

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css" />
<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
<script type ="text/javascript" src="/library/jquery/js/jquery-ui-timepicker-addon.js"></script>
<script src="/library/amcharts/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/amcharts/javascript/raphael.js" type="text/javascript"></script>        
<script type ="text/javascript" src="main.js"></script>
<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
<script type ="text/javascript" src="/library/js/assiststring.js"></script>
<script type ="text/javascript" src="/library/js/assistform.js"></script>
<style type=text/css>
.noborder { border: 1px dashed #fe9900; }
h2.notop { margin-top: 0px; }
table th.left { vertical-align: top; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
$menu = array(
	'new' => array(
		//'client'=>"New Client Requests Received",
		'reseller'=>"Log a New Reseller Request"
	),
	'manage' => array(
		//'client'=>"Client Requests",
		'reseller'=>"Reseller Requests",
	)
);
$redirect = array(
	'main'			=> "new_reseller.php",
	'new'	 		=> "new_reseller.php",
	'manage' 		=> "manage_reseller.php",
);

$self = $_SERVER['PHP_SELF'];
$page = explode("/",$self);
$self = $page[count($page)-1];
$page = substr($self,0,-4);
$base = explode("_",$page);
if(!isset($src)) {
	$src = isset($base[1]) ? $base[1] : "";
}


if(isset($redirect[$page])) {
	echo "<script type=text/javascript>document.location.href = '".$redirect[$page]."';</script>";
}

if(isset($menu[$base[0]])) {
	$submenu = $menu[$base[0]];
	$nav = array();
	foreach($submenu as $id => $display) {
		$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$id.".php",'active'=>(($src==$id) ? "Y" : "N"),'display'=>$display);
	}
	echoNavigation(1,$nav);
}

switch($base[0]) {
	case "manage":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "view";
			$menu2 = array(
				'view' => "View",
				//'update' => "Update",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
		
		break;
}

if($base[0]=="main") {
	echo "<h1>Ignite Assist Help for Resellers</h1>";
} else {
	echo "<h1>".breadCrumb($base[0].".php").(isset($menu['base_pages'][$base[0]]) ? $menu['base_pages'][$base[0]] : ucfirst($base[0]));
	if(isset($base[1])) {
		$breadcrumb = $base[0]."_".$base[1].".php";
		echo "</a> >> ".breadCrumb($breadcrumb).(isset($menu[$base[0]][$base[1]]) ? $menu[$base[0]][$base[1]] : ucfirst($base[1]));
	}
	if(isset($page_title)) {
		echo "</a> >> ".$page_title;
	}
	echo "</a></h1>";
}
?>