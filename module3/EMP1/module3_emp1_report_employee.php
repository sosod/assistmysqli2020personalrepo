<?php

class MODULE3_EMP1_REPORT_EMPLOYEE extends MODULE3_EMP1_REPORT {

    //CONSTRUCT
    private $date_format = "DATETIME";
    private $my_class_name = "MODULE3_EMP1_REPORT_EMPLOYEE";
    private $my_quick_class_name = "MODULE3_EMP1_QUICK_REPORT";

    protected $me;
    protected $s=1;

    protected $object_type = "EMPLOYEE";
    protected $table_name = "kpi";
    protected $id_field = "kpi_id";
    protected $name_field = "kpi_name";
    protected $reftag = "D";
    protected $deadline_field;
    protected $date_completed_field;
    protected $action_date_completed_field;



    protected $result_categories = array(
        'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
        'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
        'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
        'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
        'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
    );

    //OUTPUT OPTIONS
    private $act ;
    private $groups = array();
    private $group_rows = array();
    private $default_report_title = "EMPLOYEE Report";





    public function __construct($p) {
        parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
        $this->folder = "report";
        $this->me = new EMP1_EMPLOYEE();
        $this->object_type = $this->me->getMyObjectType();
        $this->table_name = $this->me->getTableName();
        $this->reftag = $this->me->getRefTag();
        $this->deadline_field = false;//$this->me->getDeadlineFieldName();
        $this->date_completed_field = $this->me->getDateCompletedFieldName();
        $this->name_field = $this->me->getNameFieldName();
        $this->default_report_title = $this->me->replaceAllNames("|".$this->me->getMyObjectName()."|")." Report";
        if($p=="fixed") {
            $this->titles = array('financial_year'=>"Financial Year");
            $this->allowfilter['financial_year']=true;
        }
        $resultSetupObject = new SDBP6_SETUP_RESULTS();
        $temp_result_categories = $resultSetupObject->getResultOptions();
        $this->result_categories = array();
        foreach($temp_result_categories as $i => $rc) {
            $key = $rc['code'];
            $this->result_categories[$key] = array(
                'text'=>$rc['value'],
                'code'=>$rc['code'],
                'color'=>$rc['color']
            );
        }
//		ASSIST_HELPER::arrPrint($this->result_categories);
    }










    /*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/

    protected function prepareGenerator() {
        parent::prepareGenerator();
    }



    protected function getFieldDetails() {
        $this->allowchoose = array();
        $this->default_selected = array();
        $this->allowfilter = array();
        $this->types = array(

        );
        $this->default_data = array();
        $this->data = array();
        $this->allowgroupby = array();
        $this->allowsortby = array(

        );
        $this->default_sort = 100;
        $this->sortposition = array();
        $this->s = 1;

        $listObject = new EMP1_LIST();
        $headObject = new EMP1_HEADINGS();
        $headings = $headObject->getReportObjectHeadings($this->object_type);
        foreach($headings as $id => $head) {
            $fld = $head['field'];
            if($head['type']!="HEADING") {

                //echo '<pre style="font-size: 18px">';
                //echo '<p>HEAD</p>';
                //print_r($headings);
                //echo '</pre>';

                if($headObject->isListField($head['type'])) {
                    $items = array();
                    if($head['type']=="LIST" || $head['type']=="MULTILIST") {
                        $listObject->changeListType($head['list_table']);
                        $items = $listObject->getItemsForReport();
                    } elseif($head['type']=="MASTER") {
                        $tbl = $head['list_table'];
                        $masterObject = new EMP1_MASTER($fld);
                        $items = $masterObject->getItemsForReport();

                    } elseif($head['type']=="USER") {
                        $userObject = new ASSIST_MODULE_USER();
                        $items = $userObject->getItemsForReport($this->table_name, $fld);
//					} elseif($head['type']=="OWNER") {
//						$ownerObject = new SDBP6_CONTRACT_OWNER();
//						$items = $ownerObject->getItemsForReport();
//						$tbl = $head['list_table'];
                    }elseif($head['type']=="OBJECT" || $head['type']=="MULTIOBJECT") {
                        if($fld == 'kpi_sub_id' || strpos($fld, '_sub_id') !== false){
                            $object = new SDBP6_SETUP_ORGSTRUCTURE();
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_proj_id' || strpos($fld, '_proj_id') !== false){
                            $object = new SDBP6_PROJECT();
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_top_id' || strpos($fld, '_top_id') !== false){
                            $object = new SDBP6_TOPKPI();
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_unit_id' || strpos($fld, '_unit_id') !== false){
                            $object = new SDBP6_SETUP_TARGETTYPE();
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_calctype_id' || strpos($fld, '_calctype_id') !== false){
                            $object = new SDBP6_SETUP_CALCTYPE();
                            $items = $object->getAllListItemsFormattedForSelect();
                        }
                    }elseif($head['type']=="SEGMENT" || $head['type']=="MULTISEGMENT") {
                        $company_code = strtolower($_SESSION['cc']);
                        $company_db_object = new ASSIST_MODULE_HELPER('client', $company_code);

                        $table_name = $head['list_table'];
                        $segmentObject = new SDBP6_SEGMENTS($table_name);
                        $scoa_segment_items = $segmentObject->getAllListItemsFormattedForSelect();
                        /******
                         * REMOVED 26 March 2019 by JC - Replaced with correct call to SEGMENTS class
                         * $segment_table_name = 'assist_' . $company_code . '_mscoa_segment_' . $table_name;
                         * $sql = "SELECT id as id
                         * , ref
                         * , name as name
                         * , description as description
                         * , parent_id as parent
                         * , has_child as has_children
                         * , can_post as can_assign
                         * , IF(status = 2,1,0) as active
                         * FROM $segment_table_name S
                         * WHERE  (( S.status & 8) <> 8 OR ( S.status & 2) = 2 OR ( S.status & 4) = 4)
                         *
                         * ORDER BY parent_id, name";
                         * $scoa_segment = $company_db_object->mysql_fetch_all_by_id($sql, 'id');
                         *
                         * $scoa_segment_items = array();
                         * foreach($scoa_segment as $key => $val){
                         * $scoa_segment_items[$key] = $val['name'] . ' (' . $val['ref'] . ')';
                         * }*/

                        $items = $scoa_segment_items;
                    }

                    $this->data[$fld] = $items;
                }
                $this->processHeadings($headObject, $head, $headings);

            }

        }

//		echo '<pre style="font-size: 18px">';
//		echo '<p>TITLES</p>';
//		print_r($this->titles);
//		echo '</pre>';
//
//		echo '<pre style="font-size: 18px">';
//		echo '<p>DATA</p>';
//		print_r($this->data);
//		echo '</pre>';




        //$this->arrPrint($this->types);
    }

    private function processHeadings($headObject,$head,$headings) {
        $fld = $head['field'];
        $this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
        if($headObject->isListField($head['type'])) {
            $this->types[$fld] = "LIST";
        } elseif($headObject->isTextField($head['type'])) {
            $this->types[$fld] = "TEXT";
        } else {
            $this->types[$fld] = $head['type'];
        }
        $this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
        $this->allowchoose[$fld] = true;
        $this->sortposition[$fld] = $this->s; $this->s++;
        $this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
        if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
            $this->allowgroupby[$fld] = true;
        } else {
            $this->allowgroupby[$fld] = false;
        }
    }

    protected function getFieldData() {
        $this->data['result'] = $this->getResultOptions();
    }

    private function processResultFieldInformation($headObject) {
        $this->setResultFieldColumnSelectionTitle($headObject);
        $this->setResultFieldHeadersThatDisplayUnderTheTimePeriodHeaders($headObject);

        $filter_by = $this->me->getFilterByOptions('MANAGE','VIEW');
        $this->all_result_time_periods = $filter_by['when'];
        $display_time_period_header = true;
        $processTimePeriod = true;
        if($this->displayResultFields()){
            $display_time_period_header = false;
            $processTimePeriod = false;
        }

        foreach($filter_by['when'] as $key => $time) {
            $this->addTimePeriodToResultFieldDateRangeSelectBoxArray($key, $time);

            // This all goes into a function called 'applyTimePeriodFilters($key, $time, $display_time_period_header)'
            if($this->displayResultFields() && $this->isTimePeriodToDisplayFrom($key)){
                $display_time_period_header = true;
            }

            if($display_time_period_header === true){
                $this->result_fields['top'][$key] = $time;
                $this->setTopNameByValueType($key);
            }

            if($this->displayResultFields() && $this->isTimePeriodToDisplayTo($key)){
                $display_time_period_header = false;
            }


            //Time Periods To Process By value_type and dates from & to
            if($this->displayResultFields()
                && ($_REQUEST['value_type'] == 'year_to_date' || $this->isTimePeriodToDisplayFrom($key))
                //&& !$this->isTimePeriodToDisplayTo($key)
            ){
                $processTimePeriod = true;
            }

            if($processTimePeriod === true){
                $this->result_field_time_periods_to_process[$key] = $time;
            }

            if($this->displayResultFields() && $this->isTimePeriodToDisplayTo($key)){
                $processTimePeriod = false;
            }
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RESULTS TOP</p>';
//        print_r($this->result_fields['top']);
//        echo '</pre>';
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>RESULT TIME FIELDS TO PROCESS</p>';
//        print_r($this->result_field_time_periods_to_process);
//        echo '</pre>';

        /**************************************************/


        if(isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0 && $_REQUEST['value_type'] == 'quarterly_values') {
            $quarter_index = 1;
            $loop_counter = 1;
            unset($this->result_fields['top']);
            $time_period_data_structure = array();
            $result_top = array();
            foreach($filter_by['when'] as $key => $time) {
                $time_period_data_structure[$quarter_index][$key] = $time;

                if($loop_counter % 3 == 0){
                    $this->result_fields['top'][$key] = $time;
                    $this->result_fields['top'][$key]['name'] = 'Quarter ending ' . $time['name'];
                    $quarter_index++;
                }
                $loop_counter++;
            }

//            echo '<pre style="font-size: 18px">';
//            echo '<p>$time_period_data_structure</p>';
//            print_r($time_period_data_structure);
//            echo '</pre>';
//
//            echo '<pre style="font-size: 18px">';
//            echo '<p>$result_top</p>';
//            print_r($result_top);
//            echo '</pre>';
        }

    }

    private function setResultFieldColumnSelectionTitle($headObject) {
        $helper = new EMP1();
        $object_name = $this->me->getMyObjectName();
        $this->result_fields_selector_title = $headObject->getObjectName($object_name) . ' ' . $helper->getActivityName("update") . ' Columns';
    }

    private function setResultFieldHeadersThatDisplayUnderTheTimePeriodHeaders($headObject) {
        $class_name = get_class($this->me);
        $results_headings = $headObject->getAllHeadingsForRenaming($class_name::CHILD_OBJECT_TYPE);

        foreach($results_headings as $key => $r) {
            $results_headings[$key]['name'] = $r['client'];
            if($r['type']=="STATUS" || $r['type']=="COMMENT") {
                unset($results_headings[$key]);
            }
        }
        $this->result_fields['bottom'] = $results_headings;
    }

    private function setTopNameByValueType($time_id){
        $top_name_pre_title = "";
        if($this->displayResultFields() && ($_REQUEST['value_type'] == 'year_to_date' || $_REQUEST['value_type'] == 'period_to_date')) {
            $top_name_pre_title = $this->result_field_value_types[$_REQUEST['value_type']] . ' ending ';
        }
        $this->result_fields['top'][$time_id]['name'] = $top_name_pre_title . $this->result_fields['top'][$time_id]['name'];
    }

    private function displayResultFields() {
        $displayResultFields = false;
        if(isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0) {
            $displayResultFields = true;
        }
        return $displayResultFields;
    }

    private function addTimePeriodToResultFieldDateRangeSelectBoxArray($key, $time) {
        $this->result_field_time_periods[$key]['name'] = $time['name'];
        $this->result_field_time_periods[$key]['is_current'] = ($time['is_current']===true ? true : false );
    }

    private function isTimePeriodToDisplayFrom($time_id) {
        $result_field_time_period = $_REQUEST['result_field_time_period_start'];
        $isTimePeriodToDisplayFrom = $this->resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id);
        return $isTimePeriodToDisplayFrom;
    }

    private function isTimePeriodToDisplayTo($time_id) {
        $result_field_time_period = $_REQUEST['result_field_time_period_end'];
        $isTimePeriodToDisplayTo = $this->resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id);
        return $isTimePeriodToDisplayTo;
    }

    private function resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id) {
        $resultFieldTimePeriodMatchesTimeId = false;
        if((int)$result_field_time_period == (int)$time_id) {
            $resultFieldTimePeriodMatchesTimeId = true;
        }
        return $resultFieldTimePeriodMatchesTimeId;
    }


    /***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
    protected function prepareOutput() {
        $this->getFieldDetails();
        $this->getFieldData();
        $this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
        $this->setGroups();
        $rows = $this->getRows();

//		echo '<pre style="font-size: 18px">';
//		echo '<p>$rows</p>';
//		print_r($rows);
//		echo '</pre>';

//		echo '<pre style="font-size: 18px">';
//		echo '<p>RESULT FIELDS TOP</p>';
//		print_r($this->result_fields['top']);
//		echo '</pre>';
//		echo '<pre style="font-size: 18px">';
//		echo '<p>RESULT FIELDS TOP</p>';
//		print_r($this->result_fields['bottom']);
//		echo '</pre>';

//        $results_rows = $this->me->getResultsForListPages(
//        	$rows,
//			$this->result_field_time_periods_to_process,
//			$this->result_fields['bottom'],
//			(isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0) ? $_REQUEST['value_type'] : false,
//			false
//		);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RESULTS ROWS</p>';
//        print_r($results_rows);
//        echo '</pre>';

        /**********************************************************************************************************/
        if($this->displayResultFields() && $_REQUEST['value_type'] == 'quarterly_values') {
            $filter_by = $this->me->getFilterByOptions('MANAGE','VIEW');

            $quarter_index = 1;
            $loop_counter = 1;
            $time_period_data_structure = array();

            foreach($filter_by['when'] as $key => $time) {
                $time_period_data_structure[$quarter_index][$key] = $time;
                if($loop_counter % 3 == 0){
                    $quarter_index++;
                }
                $loop_counter++;
            }

            $results_rows_array = array();
            foreach($time_period_data_structure as $quarter_index => $result_field_time_periods_to_process){
                $results_rows_array[$quarter_index] = $this->me->getResultsForListPages($rows,$result_field_time_periods_to_process,$this->result_fields['bottom'], 'period_to_date',false);
            }

            $results_rows = array();
            $period_index = 3;
            $comment_fields = array();
            foreach($this->result_fields['bottom'] as $fld => $head) {
                if($head['type']=="TEXT") {
                    $comment_fields[$fld] = array();
                }
            }
            foreach($results_rows_array as $quarter_index => $results){
                foreach($results as $row_id => $period_values){
                    $loop_counter = 1;
                    $quarter_comments = $comment_fields;
                    foreach($time_period_data_structure[$quarter_index] as $time_period_id => $time_period_data){
                        foreach($comment_fields as $fld => $x) {
                            if(strlen($results_rows_array[$quarter_index][$row_id][$time_period_id][$fld])>0) {
                                $quarter_comments[$fld][] =  '+ ' . $results_rows_array[$quarter_index][$row_id][$time_period_id][$fld] . ' (' . $time_period_data['name'] . ')';
                            }
                        }
                        $loop_counter++;
                    }
                    $results_period_keys = array_keys($period_values);
                    $final_time_period_key = $results_period_keys[count($results_period_keys)-1];
                    $results_rows[$row_id][$final_time_period_key] = $period_values[$final_time_period_key];
                    if($_REQUEST['output']=="csv") {
                        $implode = ". ";
                    } else {
                        $implode = "<br />";
                    }
                    foreach($comment_fields as $fld => $x) {
                        $results_rows[$row_id][$final_time_period_key][$fld] = implode($implode,$quarter_comments[$fld]);
                    }
                }
                $period_index = $period_index + 3;
            }
        }else{
            $results_rows = $this->me->getResultsForListPages($rows,$this->result_field_time_periods_to_process,$this->result_fields['bottom'], (isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0) ? $_REQUEST['value_type'] : false,false);
        }
        $all_result_rows = $this->me->getResultsForListPages($rows,$this->all_result_time_periods,$this->result_fields['bottom'], "year_to_date",false);
        $all_keys = array_keys($this->all_result_time_periods);
        $final_time_id = $all_keys[count($all_keys)-1];

        $final_overall_time_id = 0;
        $overall_result_rows = array();
        if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {/*if "Values as Captured" or "Group by Quarter" has been selected & Original or Revised Targets, Actual & R*/
            $this->overall_result_fields['top'][$_REQUEST['result_field_time_period_end']] = 'Overall Performance for ' . $this->result_field_time_periods_to_process[$_REQUEST['result_field_time_period_start']]['name'] . ' to ' . $this->result_field_time_periods_to_process[$_REQUEST['result_field_time_period_end']]['name'];

            foreach($this->result_fields['bottom'] as $field_name => $field_data){
                if((isset($_REQUEST['result_columns'][$field_name]) && $_REQUEST['result_columns'][$field_name] == 'on')
                    &&
                    (strpos($field_name, '_original') !== false || strpos($field_name, '_result') !== false || strpos($field_name, '_revised') !== false || strpos($field_name, '_actual') !== false)
                ){
                    $this->overall_result_fields['bottom'][$field_name] = $field_data;
                    $_REQUEST['overall_result_columns'][$field_name] = 'on';
                }

            }

            $overall_result_rows = $this->me->getResultsForListPages($rows,$this->result_field_time_periods_to_process,$this->result_fields['bottom'], "period_to_date",false);
            $final_overall_time_id = $_REQUEST['result_field_time_period_end'];
        }

        /**********************************************************************************************************/

        //Targets & Actuals (including baseline & original/revised annual target & ytd actual) FORMAT... UGLY HACK
        $idpObject = $this->me;
        $id_fld = $idpObject->getIDFieldName();
        $sdbp_id = 0;
        foreach($rows as $id => $r) {
            $i = $id;
            $targetTypeObject = new SDBP6_SETUP_TARGETTYPE("",$r['target_type_id']);
            foreach($r as $key => $val){
                if(strpos($key, '_baseline') !== false){
                    $rows[$i][$key] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val,false);
                }elseif(strpos($key, '_sdbip_id') !== false){
                    $sdbp_id = $rows[$i][$key];
                }
            }


            $final = $all_result_rows[$id][$final_time_id];
            foreach($final as $fld => $val) {
                if(strpos($fld,"_original")!==false) {
                    $rows[$id]['original_total'] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val,false);
                } elseif(strpos($fld,"_revised")!==false) {
                    $rows[$id]['revised_total'] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val,false);
                } elseif(strpos($fld,"_actual")!==false) {
                    $rows[$id]['actual_total'] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val,false);
                }
            }

            if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {
                $overall_final = $overall_result_rows[$id][$final_overall_time_id];
                foreach($overall_final as $fld => $val) {
                    if(strpos($fld, '_original') !== false || strpos($fld, '_adjustments') !== false || strpos($fld, '_revised') !== false || strpos($fld, '_actual') !== false){
                        $overall_result_rows[$id][$final_overall_time_id][$fld] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val,false);
                    }
                }
            }

            if(isset($results_rows[$i]) && is_array($results_rows[$i])){
                foreach($results_rows[$i] as $time_id => $time_result_data){
                    foreach($time_result_data as $field_name => $field_data){
                        if(strpos($field_name, '_original') !== false || strpos($field_name, '_adjustments') !== false || strpos($field_name, '_revised') !== false || strpos($field_name, '_actual') !== false){
                            $results_rows[$i][$time_id][$field_name] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($field_data,false);
                        }
                    }
                }
            }
        }


        $resultObject = new SDBP6_SETUP_RESULTS();
        $result_settings = $resultObject->getCustomResultSettingsDirectlyFromDatabase();
        if(isset($_REQUEST['filter']['result_setting']) && !in_array("X", $_REQUEST['filter']['result_setting']) && !in_array("XX", $_REQUEST['filter']['result_setting'])){
            unset($this->result_categories);
            foreach($_REQUEST['filter']['result_setting'] as $key => $val){
                $index = $result_settings[$val]['code'];
                $kpi_text = 'KPI ' . $result_settings[$val]['value'];

                $this->result_categories[$index]['text'] = $kpi_text;
                $this->result_categories[$index]['code'] = $index;
                $this->result_categories[$index]['color'] = $result_settings[$val]['color'];
                $this->result_categories[$index]['description'] = $result_settings[$val]['glossary'];
            }
        }

        $foreach_rows = $rows;
        foreach($foreach_rows as $id => $r){
            if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {//Use overall result to analyze
                $result_row_to_analyze = $overall_result_rows[$id][$final_overall_time_id];
            }else{//Use the last time period to analyze
                $result_row_to_analyze = $results_rows[$id][$_REQUEST['result_field_time_period_end']];
            }

            foreach($result_row_to_analyze as $field_name => $field_data){
                if(strpos($field_name, '_result') !== false){
                    //set the result of the row as this one
                    $rows[$id]['result'] = $result_row_to_analyze[$field_name];
                }
            }


            if(!array_key_exists($rows[$id]['result'], $this->result_categories)){
                unset($rows[$id]);
                unset($results_rows[$id]);
                unset($overall_result_rows[$id]);

                foreach($this->groups as $key=>$g) {
                    $group_rows_array = (isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
                    if(count($group_rows_array)>0){
                        $group_rows_array_flipped = array_flip($group_rows_array);
                        if(array_key_exists($id, $group_rows_array_flipped)){
                            unset($this->group_rows[$key][$group_rows_array_flipped[$id]]);
                        }
                    }
                }
            }
        }

        $sdbp_odject = new EMP1_EMPLOYEE();
        $sdbp_name = $sdbp_odject->getASDBIPName($sdbp_id);
        $report_title = $sdbp_name . ': ' . $this->default_report_title;

        $this->report->setReportTitle($report_title);
        $this->report->setReportFileName(strtolower($this->getMyObjectType()));
        foreach($this->result_categories as $key=>$r) {
            $this->report->setResultCategoryWithDescription($key,$r['text'],$r['color'], $r['description']);
        }
        $this->report->setRows($rows);

        $this->report->setResultRows($results_rows);
        $this->report->setOverallResultRows($overall_result_rows);

        foreach($this->groups as $key=>$g) {
            $this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
        }
        $this->report->prepareSettings($this->result_fields['bottom']);
    }

    protected function prepareDraw() {
        $this->report->setReportFileName(strtolower($this->getMyObjectType()));
    }


    private function setGroups() {
        $groupby = $this->groupby;
        if($groupby!="X" && isset($this->data[$groupby])) {
            foreach($this->data[$groupby] as $key => $t) {
                $this->groups[$key] = $this->blankGroup($key,$t);
            }
        } else {
            $this->groupby = "X";
        }
        $this->groups['X'] = $this->blankGroup("X","Unknown Group");
    }

    private function blankGroup($id,$t) {
        return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
    }

    private function allocateToAGroup($r) {
        $i = $r[$this->me->getIDFieldName()];
        $groupby = $this->groupby;
        if($groupby=="X") {
            $this->group_rows['X'][] = $i;
        } else {
            $g = explode(",",$r[$groupby]);
            if(count($g)==0) {
                $this->group_rows['X'][] = $i;
            } else {
                foreach($g as $k) {
                    $this->group_rows[$k][] = $i;
                }
            }
        }
    }






    private function getRows() {
        $final_rows = array();
        $db = new ASSIST_DB();
        $sql = $this->setSQL($db);

        $id_field = $this->me->getIDFieldName();
        if(strlen($sql)>0) {
            $rows = $db->mysql_fetch_all_fld($sql,$id_field);

            /***************************************************************
             *** ADDITIONAL PROCESSING CODE FROM EMP1 getList() - START ***
             ***************************************************************/
            $obj_type = 'EMPLOYEE';
            $section = 'MANAGE';
            $headObject = new EMP1_HEADINGS();
            $idp_headings = $headObject->getListPageHeadings($obj_type, $section);
            $all_headings = array_merge($idp_headings);
            $idpObject = $this->me;
            //REPLACE LIST, SEGMENT, OBJECT values with names
            $all_my_lists = array();
            //get replacement values
            foreach($all_headings as $fld => $head) {
                switch($head['type']) {
                    case "MULTILIST":
                    case "LIST":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $listObject = new EMP1_LIST($head['list_table']);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
                            unset($listObject);
                        }
                        break;
                    case "MULTIOBJECT":
                    case "OBJECT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_name = $head['list_table'];
                            $extra_info = array();
                            if(strpos($list_object_name,"|")!==false) {
                                $lon = explode("|",$list_object_name);
                                $list_object_name = $lon[0];
                                $extra_info = $lon[1];
                            }
                            $listObject = new $list_object_name();
                            $all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect($extra_info);
                            unset($listObject);
                        }
                        break;
                    case "MULTISEGMENT":
                    case "SEGMENT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_type = $head['list_table'];
                            $listObject = new SDBP6_SEGMENTS($list_object_type);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect(false);
                            unset($listObject);
                        }
                        break;
                } //end switch by type
            }//end foreach heading
            /***************************************************************
             **** ADDITIONAL PROCESSING CODE FROM EMP1 getList() - END ****
             ***************************************************************/
            //now loop through rows and replace values as needed

            foreach($rows as $key => $r) {
                if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
                    $final_rows[$r[$id_field]] = $r;
                    foreach($this->titles as $i => $t) {
                        if(isset($r[$i]) && isset($this->types[$i])) {
                            $d = $r[$i];
                            $type = $this->types[$i];
                            switch($type) {
                                case "REF":
                                    $d = $this->getRefTag().$d;
                                    break;
                                case "DATE":
                                    if(strtotime($d) != 0) {
                                        $d = date("d-M-Y",strtotime($d));
                                    } else {
                                        $d = "";
                                    }
                                    break;
                                case "BOOL":
                                    if($d==="1") { $d = "Yes"; } elseif($d==="0") { $d = "No"; }
                                    break;
                                case "LIST":
                                case "TEXTLIST":
                                    $d2 = $d;
                                    $x = explode(",",$d);
                                    $d = "";
                                    $z = array();
                                    if($i=="status") {
                                        $z[] = (!isset($this->data[$i][$d2])) ? $this->data[$i][1] : $this->data[$i][$d2];	//default to new for unknown status
                                    } else {
                                        foreach($x as $a) {
                                            if(isset($this->data[$i][$a])) {
                                                $z[] = $this->data[$i][$a];
                                            }
                                        }
                                    }
                                    $d = count($z)>0 ? implode(", ",$z) : "Unspecified";
                                    break;
                                case "PERC":
                                case "PERCENTAGE":
                                    $d = number_format($d,2)."%";
                                    break;
                                case "LINK":
                                    $d = "<a href=".$d.">".$d."</a>";
                                    break;
                                case "ATTACH":
                                    if(strlen($d) > 0){
                                        $f = unserialize($d);
                                        $d = "";
                                        if(isset($f) && is_array($f) && count($f) > 0){
                                            foreach($f as $key => $val){
                                                $d .= "+" . $val['original_filename'] . "\n";
                                            }
                                        }
                                    }else{
                                        $d = "";
                                    }
                                    break;
                                case "TEXT":
                                default:
                                    $d = $d;
                                    break;
                            }
                            $final_rows[$r[$id_field]][$i] = stripslashes($d);
                        }
                    }
                    $this->allocateToAGroup($r);
                }

                /***************************************************************
                 *** ADDITIONAL PROCESSING CODE FROM EMP1 getList() - START ***
                 ***************************************************************/
                $row = $r;
                foreach($row as $fld => $x) {
                    //only treat as list if previously handled in check above && only act on field if required for list page i.e. set in headings list
                    if(isset($all_my_lists[$fld]) && isset($all_headings[$fld])) {
                        $head = $all_headings[$fld];
                        //store display data in list_table element to preserve raw data in original fld element
                        //formatting function (below) looks for display data in list_Table element for all list heading types (set in _HEADINGS->list_heading_types)
                        $save_field = $head['list_table'];
                        //if heading type contains MULTI - assume it is multi LIST SEGMENT or OBJECT
                        if(strpos($head['type'],"MULTI")!==false) {
                            if(strpos($head['type'],"OBJECT")!==false) {
                                $blank_value = "";
                            } else {
                                $blank_value = $idpObject->getUnspecified();
                            }
                            $x = explode(";",$x);
                            $x = $idpObject->removeBlanksFromArray($x);
                            if(count($x)>0) {
                                $rows[$key][$save_field] = array();
                                foreach($x as $y) {
                                    if(isset($all_my_lists[$fld][$y])) {
                                        $rows[$key][$save_field][] = $all_my_lists[$fld][$y];
                                    }
                                }
                                if(count($rows[$key][$save_field])>0) {
                                    $rows[$key][$save_field] = implode("; ",$rows[$key][$save_field]);
                                } else {
                                    $rows[$key][$save_field] = $blank_value;
                                }
                            } else {
                                $rows[$key][$save_field] = $blank_value;
                            }
                        } else {
                            if($head['type']=="OBJECT") {
                                $blank_value = "";
                            } else {
                                $blank_value = $idpObject->getUnspecified();
                            }
                            if(isset($all_my_lists[$fld][$x])) {
                                $rows[$key][$save_field] = $all_my_lists[$fld][$x];
                            } else {
                                $rows[$key][$save_field] = $blank_value;
                            }
                        }//end if multi
                    }//end if all_my_lists[fld] isset
                }//end foreach fld

                /***************************************************************
                 **** ADDITIONAL PROCESSING CODE FROM EMP1 getList() - END ****
                 ***************************************************************/
            }
            /************************************************************************
             *** ADDITIONAL PROCESSING CODE FROM EMP1 formatRowDisplay() - START ***
             ************************************************************************/
            $id_fld = $idpObject->getIDFieldName();
            $ref_tag = $idpObject->getRefTag();
            $displayObject = new EMP1_DISPLAY();
            $final_data['head'] = $idp_headings;

            foreach($rows as $r) {
                $row = array();
                $i = $r[$id_fld];
                if(isset($r['target_type_id'])) { $final_rows[$i]['target_type_id'] = $r['target_type_id']; }
                if(isset($r['calc_type_id'])) { $final_rows[$i]['calc_type_id'] = $r['calc_type_id']; }
                foreach($final_data['head'] as $fld=> $head) {
                    if($head['parent_id']==0){
                        if($headObject->isListField($head['type'])) {
                            $final_rows[$i][$fld] = (isset($r[$head['list_table']]) ? $r[$head['list_table']] : 'N/A');
                        } elseif($idpObject->isDateField($fld)) {
                            $field_data = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        } elseif($head['type']=="NUM" && $head['apply_formatting']==true) {
                            $field_data = $displayObject->getDataField("TEXT", $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        } else {
                            $field_data = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        }
                    }
                }
            }
            /************************************************************************
             **** ADDITIONAL PROCESSING CODE FROM EMP1 formatRowDisplay() - END ****
             ************************************************************************/
        }

//		echo '<pre style="font-size: 18px">';
//		echo '<p>FINAL ROWS</p>';
//		print_r($final_rows);
//		echo '</pre>';

        return $final_rows;
    }


    protected function setSQL($db,$filter=array()) {
        $filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
        $group_by = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";

//		echo '<pre style="font-size: 18px">';
//		echo '<p>THE REQUEST</p>';
//		var_dump($_REQUEST);
//		echo '</pre>';

        $obj_type = 'EMPLOYEE';
        $section = 'MANAGE';
        $dept_obj = new EMP1_EMPLOYEE();
        $has_results = $dept_obj->hasResults();

        $left_joins = array();

        //comment out for development purposes to register changes in heading class
        if(!isset($_SESSION[$this->getModRef()]['headingObject'])) {
            $headObject = new EMP1_HEADINGS();
        } else {
            $headObject = unserialize($_SESSION[$this->getModRef()]['headingObject']);
        }

        //set up variables
//        $idp_headings = $headObject->getListPageHeadings($obj_type, $section);
        $idp_headings = $headObject->getReportObjectHeadings($obj_type);

        $class_name = "EMP1_".$obj_type;
        $idpObject = new $class_name();
        $idp_tblref = $idpObject->getMyObjectType();
        $idp_table = $idpObject->getTableName();
        $idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();

        $tblref = $idp_tblref;
        $where_tblref = $idp_tblref;

        $where = $idpObject->getActiveStatusSQL($idp_tblref);
        $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
        //extra field in sql to get RAW TARGET TYPE ID - required for results formatting
        if($idpObject->getTargetTypeTableField()!==false) {
            $sql.= ", ".$idpObject->getTargetTypeTableField()." as target_type_id";
        }
        //extra field in sql to get RAW CALC TYPE ID - required for results calculations
        if($idpObject->getCalcTypeTableField()!==false) {
            $sql.= ", ".$idpObject->getCalcTypeTableField()." as calc_type_id";
        }
        $from = " $idp_table $idp_tblref
					";
        $sort_by = array();

        $all_headings = array_merge($idp_headings);

        $listObject = new EMP1_LIST('');

        foreach($all_headings as $id => $head) {
            $fld = $head['field'];
            $lj_tblref = $head['section'];
            if($head['type']=="MASTER") {
                $tbl = $head['list_table'];
                $masterObject = new EMP1_MASTER($fld);
                $fy = $masterObject->getFields();
                $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
                $sb = $tbl;
            } elseif($head['type']=="USER") {
                $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                $left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
                $sb = $head['list_table'];
            }elseif($head['type']=="LIST") {
                $tbl = $head['list_table'];
                $listObject->changeListType($tbl);
                $sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($tbl)." AS ".$tbl." 
										ON ".$tbl.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl.".status & ".SDBP6::DELETED.") <> ".SDBP6::DELETED;
                $sb = $tbl.".".implode(", ".$tbl.".",str_ireplace("|X|", $tbl, $listObject->getSortBy(true)));
                $sb = str_ireplace($tbl.".if", "if", $sb);
            }else {
                $sb = $where_tblref.".".$fld;
            }
            $sort_by[$fld] = $sb;
        }

        //Link the Top Layer Org Structure to the rows
        if(/*$_REQUEST['group_by'] == 'top_sub_id'*/true){
            $object = new SDBP6_SETUP_ORGSTRUCTURE();
            $tbl = $object->getTableName();
            $sql.= ", ".$tbl.".org_parent_id as top_sub_id";
            $left_joins[] = "LEFT OUTER JOIN ".$tbl/*." AS ".$tbl*/." 
										ON ".$tbl.".org_id = ".$idp_tblref.".kpi_sub_id";

            $sort_by['top_sub_id'] = 'top_sub_id';
        }

        $sql.= " FROM ".$from.implode(" ",$left_joins);
        $sql.= " WHERE ".$where;

        $s = array();
        if(count($this->titles)>0) {
            foreach($this->titles as $fld => $t) {
                //echo "<P>".$fld;
                if( (!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld]===true) && isset($filters[$fld]) && ($fld != 'result_setting')) {
                    $t = $this->types[$fld];
                    $f = $filters[$fld];
                    $ft = isset($this->filter_types[$fld]) ? $this->filter_types[$fld] : "";
                    $a = "";
                    switch($fld) {
                        case "action_progress":
                            //do nothing - filtering applied in row processing
                            break;
                        case "result":
                            //do nothing - filtering applied in row processing
                            break;
                        default:
                            $a =  $this->report->getFilterSql($tblref,$t,$f,$ft,$fld);
                            break;
                    }
                    if(strlen($a)>0) { $s[] = $a; }
                }
            }
        }
        if(count($s)>0) {
            $sql.= " AND ".implode(" AND ",$s);
        }

        $sql.=$this->getSortBySql($sort_by);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>$sql</p>';
//        print_r($sql);
//        echo '</pre>';

        return $sql;
    }


}

?>