<?php
error_reporting(0);
class MODULE3_EMP1_QUICK_REPORT extends MODULE3_GENERIC_QUICK_REPORT {

    protected $page;
    protected $folder;
    protected $my_class_name = "MODULE3_EMP1_QUICK_REPORT";
    private $db;

    public function __construct($p) {
        parent::__construct();
        $this->page = $p;
        $this->folder = "report";
        $this->db = new ASSIST_DB("client");
    }

    public function deleteReport($id) {
        $sql = "UPDATE ".$this->db->getDBRef()."_quick_report SET status = 0 WHERE id = $id";
        $mar = $this->db->db_update($sql);
        return array("ok","Quick Report ".$id." has been deleted successfully.");
    }

    public function updateReport($id,$name,$descrip,$report) {
        $sql = "UPDATE ".$this->db->getDBRef()."_quick_report SET
					name = '$name',
					description = '$descrip',
					report = '$report'
				WHERE 
					id = $id";
        $mar = $this->db->db_update($sql);
        return array("ok","Quick Report ".$id." has been updated successfully.");
    }

    public function createReport($name,$descrip,$report) {
        $sql = "INSERT INTO ".$this->db->getDBRef()."_quick_report (name,description,report,status,insertuser,insertdate) VALUES ('$name','$descrip','$report',1,'".$this->getUserID()."',now())";
        $id = $this->db->db_insert($sql);
        return array("ok","New Quick Report ".$id." has been created successfully.", $report);
    }

    public function getList() {
        $results = array();
        $sql = "SELECT id, name, description, report, insertdate as moddate FROM ".$this->db->getDBRef()."_quick_report WHERE status = 1 AND insertuser = '".$this->getUserID()."' ORDER BY name";
        $rows = $this->db->mysql_fetch_all_fld($sql,"id");
        foreach($rows as $key => $row) {
            $report = unserialize($row['report']);
            $results[$key] = array(
                'id'=>$row['id'],
                'name'=>$row['name'],
                'description'=>$row['description'],
                'moddate'=>$row['moddate'],
                'source_class'=>$report['source_class'],
                'page'=>$report['page'],
            );
        }

        return $results;
    }

    public function preparePage() {
        $page = $_REQUEST['page'];
        $menuObject = new EMP1_MENU();
        $menu = $menuObject->getPageMenu($page,"/EMP1/");
        $menuObject->drawPageTop($menu);
    }

}



?>