<?php



class MODULE3_RGSTR2_REPORT_ACTION extends MODULE3_RGSTR2_REPORT {

	//CONSTRUCT
	private $date_format = "DATETIME";
	protected $my_class_name = "MODULE3_RGSTR2_REPORT_ACTION";
	private $my_quick_class_name = "MODULE3_RGSTR2_QUICK_REPORT";
	
	protected $me;
	protected $s=1;
	
	protected $object_type = "ACTION";
	protected $table_name = "action";
	protected $id_field = "action_id";
	protected $reftag = "RA";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;
	protected $owner_field;
	

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Risk Assist: Action Report";
	
	
	


	public function __construct($p) {	
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		$this->me = new RGSTR2_ACTION();
		$this->object_type = $this->me->getMyObjectType();
		$this->table_name = $this->me->getTableName();
		$this->reftag = $this->me->getRefTag();
		$this->deadline_field = $this->me->getDeadlineFieldName();
		$this->date_completed_field = $this->me->getDateCompletedFieldName();
		$this->owner_field = $this->me->getOwnerFieldName();
		
		if($p=="fixed") {
			$this->titles = array('financial_year'=>"Financial Year");
			$this->allowfilter['financial_year']=true;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {
		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array();
		$this->types = array(
			'result'				=> "RESULT",
		);
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array();
		$this->allowsortby = array(
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

        $this->titles['register_id'] = "Register";
        $this->types['register_id'] = "LIST";

        $this->titles['rsk_id'] = "Risk";
        $this->types['rsk_id'] = "LIST";

		$listObject = new RGSTR2_LIST();
		
		$headObject = new RGSTR2_HEADINGS();
		$headings = $headObject->getReportObjectHeadings($this->object_type);
		//$deliverable_status = $headings['deliverable_status'];
		unset($headings['deliverable_status']);
		foreach($headings as $id => $head) {
			$fld = $head['field'];
			if($head['type']!="HEADING") {
				if($headObject->isListField($head['type'])) {
					$items = array();
					if($head['type']=="LIST") {
						$listObject->changeListType($head['list_table']);
						$items = $listObject->getItemsForReport();
					} elseif($head['type']=="MASTER") {
						$tbl = $head['list_table'];
						$masterObject = new RGSTR2_MASTER($fld);
						$items = $masterObject->getItemsForReport();
					} elseif($head['type']=="USER") {
						$userObject = new ASSIST_MODULE_USER();
						$items = $userObject->getItemsForReport($this->table_name, $fld);
					} elseif($head['type']=="OWNER") {
						$ownerObject = new RGSTR2_CONTRACT_OWNER();
						$items = $ownerObject->getItemsForReport();
						$tbl = $head['list_table'];
					} elseif($head['type']=="DEL_TYPE") {
						$items = $this->me->getAllDeliverableTypes();
						$tbl = $head['list_table'];
					}
					$this->data[$fld] = $items;
				}
				$this->processHeadings($headObject, $head, $headings);
			} elseif($head['field']=="contract_assess_status") {
				$fld = $head['field'];
				foreach($deliverable_status as $id=>$item) {
					$head['name'] = $item['name'];
					$head['field'] = $fld."_".$id;
					$head['type'] = "BOOL";
					$head['parent_id'] = $head['id'];
					$this->processHeadings($headObject, $head, $headings);
				}
			}
		}

		$this->titles['result'] = "Compliance Status";// On ".$this->titles['action_progress'];
		//[SD] 31-07-21 AA-658 Risk 2 - Register/Risk/Action Report Generator - add missing field option to include update log
		$this->titles['update_log'] = "Response";
	//$this->arrPrint($this->types);
	}
	
	private function processHeadings($headObject,$head,$headings) {
				$fld = $head['field'];
				$this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
				if($headObject->isListField($head['type'])) {
					$this->types[$fld] = "LIST";
				} elseif($headObject->isTextField($head['type'])) {
					$this->types[$fld] = "TEXT";
				} else {
					$this->types[$fld] = $head['type'];
				}
				$this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
				$this->allowchoose[$fld] = true;
				$this->sortposition[$fld] = $this->s; $this->s++;
				$this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
				if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
					$this->allowgroupby[$fld] = true;
				} else {
					$this->allowgroupby[$fld] = false;
				}
	}
	
	protected function getFieldData() {
		$this->data['result'] = $this->getResultOptions();
        $this->data['register_id'] = $this->getRegisterObjects();
        $this->data['rsk_id'] = $this->getRiskObjects();
	}
		
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			if($groupby == "register_id"){$groupby = "grand_parent_id";}//spotted while doing [AA-658]SD. Allow group by register.
			if($groupby == "rsk_id"){$groupby = "action_deliverable_id";}//spotted while doing [AA-658]SD. Allow group by risk.
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		$id_field = $this->me->getIDFieldName();
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,$id_field);

			//[SD] 31-07-21 AA-658 Risk 2 - Register/Risk/Action Report Generator - add missing field option to include update log
			if(isset($_REQUEST['columns']['update_log'])) {
				$response = $this->getResponses("action","al_action_id",array_keys($rows));
			}

			foreach($rows as $key => $r) {
				$date_completed = $r[$this->date_completed_field];
				if($r['action_progress']==100 && strtotime($date_completed)==0 && !empty($this->action_date_completed_field)) {
					$date_completed = $r[$this->action_date_completed_field];
					$r[$this->date_completed_field] = $date_completed;
				}
				$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$this->deadline_field],'date_completed'=>$date_completed);
				$r['result'] = $this->getCompliance($my_action_progress);

				//[SD] 31-07-21 AA-658 Risk 2 - Register/Risk/Action Report Generator - add missing field option to include update log
				$r['update_log'] = isset($response[$key]) ? $response[$key] : array();
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r[$id_field]] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = $this->getRefTag().$d;
								break;
							case "DATE":
								if(strtotime($d) != 0) {
									$d = date("d-M-Y",strtotime($d));
								} else {
									$d = "";
								}
								break;
							case "BOOL":
								if($d==="1") { $d = "Yes"; } elseif($d==="0") { $d = "No"; }
								break;
							case "LIST":
							case "TEXTLIST":
								$d2 = $d;
								$x = explode(",",$d);
								$d = "";
								$z = array();
								if($i=="status") {
									$z[] = (!isset($this->data[$i][$a])) ? $this->data[$i][1] : $this->data[$i][$a];	//default to new for unknown status
								} else {
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										}
									}
								}
								$d = count($z)>0 ? implode(", ",$z) : "Unspecified";
								break;
							case "PERC":
							case "PERCENTAGE":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "ATTACH":
                                if(strlen($d) > 0){
                                    $f = unserialize($d);
                                    $d = "";
                                    if(isset($f) && is_array($f) && count($f) > 0){
                                        foreach($f as $key => $val){
                                            $d .= "+" . $val['original_filename'] . "\n";
                                        }
                                    }
                                }else{
                                    $d = "";
                                }
								break; 
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r[$id_field]][$i] = stripslashes($d);
						}
					}

                    if(isset($final_rows) && is_array($final_rows) && count($final_rows) > 0){
                        foreach($final_rows as $key => $val){
                            if(array_key_exists('parent_id', $val)){
                                $final_rows[$key]['rsk_id'] = $val['parent_name'] . '[RR' . $val['parent_id'] . ']';
                            }
                            if(array_key_exists('grand_parent_id', $val)){
                                $final_rows[$key]['register_id'] = $val['grand_parent_name'] . '[R' . $val['grand_parent_id'] . ']';
                            }
                        }
                    }

					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	

	
	
	protected function setSQL($db,$filter=array()) {
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$sql = "";
		$left_joins = array();
		
		$listObject = new RGSTR2_LIST("deliverable_status");
		$deliverable_status_items = $listObject->getAllListItems();
		
		$headObject = new RGSTR2_HEADINGS();
		
		$d_tblref = "RISK";
		$dObject = new RGSTR2_RISK();
		$d_table = $dObject->getTableName();
		$d_id = $d_tblref.".".$dObject->getIDFieldName();
        $d_name = $d_tblref.".".$dObject->getNameFieldName();
		$d_status = $d_tblref.".".$dObject->getStatusFieldName();
		$d_parent = $d_tblref.".".$dObject->getParentFieldName();
		$d_headings = $headObject->getReportObjectHeadings($dObject->getMyObjectType());

		$c_tblref = "REGISTER";
		$cObject = new RGSTR2_REGISTER();
		//$this->action_date_completed_field = $cObject->getDateCompletedFieldName();
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_name = $c_tblref.".".$cObject->getNameFieldName();
		$c_headings = $headObject->getReportObjectHeadings($cObject->getMyObjectType());
		unset($c_headings['deliverable_status']);
		//$c_status = $a_tblref.".".$aObject->getStatusFieldName();
		//$c_parent = $a_tblref.".".$aObject->getParentFieldName();
		//$c_progress = $a_tblref.".".$aObject->getProgressFieldName();
		//$c_progress2 = $aObject->getProgressFieldName();
		
		//set up contract variables
		$my_headings = $headObject->getReportObjectHeadings($this->getMyObjectType()); 
		$myObject = $this->getMyObject(); 
		$my_tblref = "ACTION";
		$tblref = $my_tblref;
		$my_table = $myObject->getTableName();
		$my_id = $my_tblref.".".$myObject->getIDFieldName();
		$my_status = $my_tblref.".".$myObject->getStatusFieldName();
		$my_status_fld = $my_tblref.".".$myObject->getProgressStatusFieldName();
		$my_name = $myObject->getNameFieldName();
		$my_deadline = $myObject->getDeadlineFieldName();
		$my_field = $my_tblref.".".$myObject->getTableField();
		$my_parent = $myObject->getParentFieldName();
		
		$where = $myObject->getActiveStatusSQL($my_tblref);
		
			$sql = "SELECT DISTINCT $my_status as my_status
					, ".$my_tblref.".*
					, ".$c_name."
					, $c_id as grand_parent_id
					, $c_name as grand_parent_name
					, $d_id as parent_id
					, $d_name as parent_name
					";
			$from = " $my_table $my_tblref 
					INNER JOIN $d_table $d_tblref
					  ON $my_parent = $d_id
					INNER JOIN $c_table $c_tblref
					  ON $d_parent = $c_id
			";
			$final_data['head'] = $my_headings;
			$id_fld = $myObject->getIDFieldName();
			$where_tblref = $my_tblref;
			$where_deadline = $my_deadline;
			$ref_tag = $myObject->getRefTag();
			$where_status_id_fld = $my_tblref.".".$myObject->getProgressStatusFieldName();
			$status_id_fld = $myObject->getProgressStatusFieldName();
			$where_status_fld = $my_tblref.".".$myObject->getStatusFieldName();
		

			$sort_by = array();
			$all_headings = array_merge($my_headings,$d_headings,$c_headings);
			$listObject = new RGSTR2_LIST();

            //The following code removes duplicate Risk Fields that have been added to the update section
            foreach($all_headings as $id => $head){
                if($head['section'] == 'RISK_UPDATE'){
                    $head_field = $head['field'];
                    if(
                        $head_field == 'rsk_impact'
                        ||
                        $head_field == 'rsk_impact_rating'
                        ||
                        $head_field == 'rsk_likelihood'
                        ||
                        $head_field == 'rsk_likelihood_rating'
                        ||
                        $head_field == 'rsk_control_effectiveness'
                        ||
                        $head_field == 'rsk_control_effectiveness_rating'
                        ||
                        $head_field == 'rsk_current_controls'
                    ){
                        unset($all_headings[$id]);
                    }
                }
            }
			
//$this->arrPrint($all_headings);
			foreach($all_headings as $id => $head) {   
				$fld = $head['field'];
				$lj_tblref = substr($head['section'],0,1);

                $lj_tblref = substr($head['section'],0,1);

                //Register/Contract hack
                $lj_tblref = ($lj_tblref == 'C' ? 'REGISTER' : $lj_tblref); //Tshego Hack
                $lj_tblref = ($lj_tblref == 'R' ? 'RISK' : $lj_tblref); //Tshego Hack

                //Risk/Deliverable hack
                $lj_tblref = ($lj_tblref == 'D' ? 'RISK' : $lj_tblref); //Tshego Hack
                $lj_tblref = ($lj_tblref == 'R' ? 'RISK' : $lj_tblref); //Tshego Hack

                //Action Hack
                $lj_tblref = ($lj_tblref == 'A' ? 'ACTION' : $lj_tblref); //Tshego Hack

                //One more Hack
                if(substr($head['section'],0,1) == 'R'){
                    if(substr($head['section'],0,3) == 'REG'){
                        $lj_tblref = 'REGISTER';
                    }else{
                        $lj_tblref = 'RISK';
                    }
                }
                
                
				if($head['type']=="LIST") {
					$tbl = $head['list_table'];
					$listObject->changeListType($tbl);
					$sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($tbl)." AS ".$tbl." 
										ON ".$tbl.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl.".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
					$sb = $tbl.".".implode(", ".$tbl.".",str_ireplace("|X|", $tbl, $listObject->getSortBy(true)));
					$sb = str_ireplace($tbl.".if", "if", $sb);
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
					$sb = $fld.".tkname, ".$fld.".tksurname";
				} else {
					if($fld=="action_progress") {
						$sb = "AVG(ACTION.action_progress)";
					} else {
						$sb = $where_tblref.".".$fld;
					}
				}
				$sort_by[$fld] = $sb;
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where;		
			$sql.=" AND ".$myObject->getReportingStatusSQL($my_tblref);
			$sql.=" AND ".$dObject->getReportingStatusSQL($d_tblref);
			$sql.=" AND ".$cObject->getReportingStatusSQL($c_tblref);
			
			$s = array();
			foreach($this->titles as $fld => $t) {
				//echo "<P>".$fld;
				if( (!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld]===true) && isset($filters[$fld])) {
					if($fld == "update_log"){ //[SD] 31-07-21 AA-658 Risk 2 - Register/Risk/Action Report Generator - add missing field option to include update log
						$t = "TEXT";
					}else {
						$t = $this->types[$fld];
					}
					$f = $filters[$fld];
					$ft = isset($filter_types[$fld]) ? $filter_types[$fld] : "";
					$a = "";
					switch($fld) {
						case "action_progress": 
							//do nothing - filtering applied in row processing
							break;
						case "result":
							//do nothing - filtering applied in row processing
							break;
						default:
                            if($fld == 'rsk_id'){
                                $table_reference = $d_tblref;
                            }elseif($fld == 'register_id'){
                                $table_reference = $c_tblref;
                            }else{
                                $table_reference = $tblref;
                            }

                            $a =  $this->report->getFilterSql($table_reference,$t,$f,$ft,$fld);
							break;
					}
					if(strlen($a)>0) { $s[] = $a; }
				}
			}
			if(count($s)>0) {
				$sql.= " AND ".implode(" AND ",$s);
			}
			$sql.="GROUP BY ".$my_tblref.".".$myObject->getIDFieldName();

            if(isset($sort_by) && is_array($sort_by) && array_key_exists('rsk_id', $sort_by)){
                $sort_by['rsk_id'] = ($sort_by['rsk_id'] == 'ACTION.rsk_id' ? 'RISK.rsk_id' : $sort_by['rsk_id']);
            }

            if(isset($sort_by) && is_array($sort_by) && array_key_exists('register_id', $sort_by)){
                $sort_by['register_id'] = ($sort_by['register_id'] == 'ACTION.register_id' ? 'REGISTER.register_id' : $sort_by['register_id']);
            }

			$sql.=$this->getSortBySql($sort_by);
		return $sql;
	}
	

}