<?php

class MODULE3_RGSTR2_REPORT_FIXED extends MODULE3_GENERIC_REPORT_FIXED {

	//CONSTRUCT
	private $page;
	private $folder = "report";
	private $date_format = "DATETIME";
	protected $my_class_name = "MODULE3_RGSTR2_REPORT_FIXED";

	private $my_reports;
	private $my_filters;
	private $use_a_filter = false;

	private $cntrct_report;

	private $contract_name = "REGISTER";
	private $deliverable_name = "RISK";
	private $action_name = "ACTION";


	private $active_report_title;

	public function __construct($p) {
		$this->page = $p;
		parent::__construct($p,$this->date_format,$this->my_class_name);

		$me = new RGSTR2_HELPER();

        $headObject = new RGSTR2_HEADINGS();
        $risk_headings = $headObject->getMainObjectHeadings("RISK","LIST",'MANAGE');
        $action_headings = $headObject->getMainObjectHeadings("ACTION","LIST",'MANAGE');

		$this->contract_name = $me->getObjectName("REGISTER");
		$this->deliverable_name = $me->getObjectName("RISK");
		$this->action_name = $me->getObjectName("ACTION");

		$contract_name = $this->contract_name;
		$deliverable_name = $this->deliverable_name;
		$action_name = $this->action_name;

/*			3=> array(
                'id'=>3,
                'name'=>"Analysis Dashboard per " . $headObject->getAHeadingNameByField("rsk_owner_id"),
                'descrip'=>"Overall  $deliverable_name analysis per " . $headObject->getAHeadingNameByField("rsk_owner_id") . " broken down by $action_name Progress, " . $risk_headings['rsk_status_id']['name'] . ", " . $risk_headings['rsk_impact']['name'] . ", " . $risk_headings['rsk_likelihood']['name'] . ", " . $risk_headings['rsk_control_effectiveness']['name'] . ", " . $risk_headings['rsk_inherent_exposure']['name'] . ", and " . $risk_headings['rsk_residual_exposure']['name'],
                'format'=>"Pie & Bar",
                'code'=>"risk_compliance_per_department",
            ),*/
            /*5=>array(
                'id'=>5,
                'name'=>"$action_name Analysis Dashboard per Person",
                'descrip'=>"Overall $action_name analysis per Person broken down by $action_name Progress, " . $action_headings['action_status_id']['name'] . ", " . $risk_headings['rsk_impact']['name'] . ", " . $risk_headings['rsk_likelihood']['name'] . ", " . $risk_headings['rsk_control_effectiveness']['name'] . ", " . $risk_headings['rsk_inherent_exposure']['name'] . ", and " . $risk_headings['rsk_residual_exposure']['name'],
                'format'=>"Pie & Bar",
                'code'=>"action_progress_per_person",
            ),*/
		$this->my_reports = array(
			1=>array(
                'id'=>1,
                'name'=>"$contract_name Report",
                'descrip'=>"$deliverable_name $contract_name Details",
                'format'=>"Table",
                'code'=>"register_report",
            ),
            2=>array(
                'id'=>2,
                'name'=>"Analysis Dashboard",
                'descrip'=>"Overall  $deliverable_name analysis broken down by $action_name Progress, " . $risk_headings['rsk_status_id']['name'] . ", " . $risk_headings['rsk_impact']['name'] . ", " . $risk_headings['rsk_likelihood']['name'] . ", " . $risk_headings['rsk_control_effectiveness']['name'] . ", " . $risk_headings['rsk_inherent_exposure']['name'] . ", and " . $risk_headings['rsk_residual_exposure']['name'],
                'format'=>"Pie & Bar",
                'code'=>"risk_compliance_overall",
            ),
			4=>array(
                'id'=>4,
                'name'=>"$action_name Dashboard",
                'descrip'=>"Overall $action_name analysis broken down by $action_name Progress, " . $action_headings['action_status_id']['name'] . ", " . $risk_headings['rsk_impact']['name'] . ", " . $risk_headings['rsk_likelihood']['name'] . ", " . $risk_headings['rsk_control_effectiveness']['name'] . ", " . $risk_headings['rsk_inherent_exposure']['name'] . ", and " . $risk_headings['rsk_residual_exposure']['name'],
                'format'=>"Pie & Bar",
                'code'=>"action_progress_dashboard",
				),
            6=>array(
                'id'=>6,
                'name'=>"Top Ten $deliverable_name's Analysis Dashboard",
                'descrip'=>"Top Ten $deliverable_name's ordered by inherent risk exposure and residual risk exposure",
                'format'=>"Table",
                'code'=>"top_ten_risks",
            ),
		);

		$this->cntrct_report = new MODULE3_RGSTR2_REPORT($this->page,$this->date_format,$this->my_class_name,"");
		$this->cntrct_report->setFolder($this->folder);
//		$this->my_filters = $this->cntrct_report->getFinancialYears();

	}


	protected function prepareGenerator() {
		$this->drawPageHeader();

		foreach($this->my_reports as $r) {
			$this->setReport($r['name'],$r['descrip'],$r['format'],$r['code']);
		}

//		foreach($this->my_filters as $key=>$f) {
//			$this->setFilter($key,$f);
//		}

		$this->setUseFilter($this->use_a_filter);


	}

	protected function drawPageHeader() {
		$this->cntrct_report->displayPageHeading();
	}

	protected function prepareReport() {
		//$this->drawPageHeader();

		$report = $_REQUEST['code'];
		if($report=="risk_compliance_overall" || $report=="action_progress_dashboard") {
			header("Location:/RGSTR2/report_dashboard.php?report=".$report);
		}
		$rprt = explode("_",$report);
		$report = "";
		foreach($rprt as $r) { $report.=ucwords($r); }

		$f = explode("_",$_REQUEST['filter']);
		$financial_year = $f[0];

		foreach($this->my_reports as $r) {
			if($r['code']==$_REQUEST['code']) {
				$this->active_report_title = $r['name'];
				break;
			}
		}

		$my_function = "self::get".$report;
		$this->reporting_data = call_user_func($my_function,$financial_year);

	}

	protected function getReportTitle() { return $this->active_report_title; }


	private function getDashboard($f) {
		$data = array(
					'settings'=>array(
							'layout'=>"P",
							'hasMain'=>true,
							'hasSub'=>false,
							'manualPageTitle'=>true,
						),
					'contract'=>array(),'deliverable'=>array(),'action'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "contract";
		$topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
		$topObject->fixedReport();
		$data[$graph]['graph_title'] = $this->contract_name." Compliance Summary (".$this->action_name." Progress)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $topObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$top_sql = $topObject->getSQL($db,array('financial_year'=>array($f)));
		$top_rows = $db->mysql_fetch_all_fld($top_sql,$topObject->getIDField());
		foreach($top_rows as $key => $r) {
			$date_completed = $r[$topObject->getDateCompletedField()];
			if($r['action_progress']==100 && strtotime($date_completed)==0) {
				$date_completed = $r[$topObject->getActionDateCompletedField()];
				$r[$topObject->getDateCompletedField()] = $date_completed;
			}
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$topObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $topObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}


		//RISKS
		$graph = "deliverable";
		$midObject = new MODULE3_RGSTR2_REPORT_RISK($_REQUEST['page']);
		$midObject->fixedReport();
		$data[$graph]['graph_title'] = $this->deliverable_name." Compliance Summary (".$this->action_name." Progress)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $midObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$mid_sql = $midObject->getSQL($db,array('financial_year'=>array($f)));
		$mid_rows = $db->mysql_fetch_all_fld($mid_sql,$midObject->getIDField());
		foreach($mid_rows as $key => $r) {
			$date_completed = $r[$midObject->getDateCompletedField()];
			if($r['action_progress']==100 && strtotime($date_completed)==0) {
				$date_completed = $r[$midObject->getActionDateCompletedField()];
				$r[$midObject->getDateCompletedField()] = $date_completed;
			}
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$midObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $midObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}

		//ACTIONS
		$graph = "action";
		$childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
		$childObject->fixedReport();
		$data[$graph]['graph_title'] = $this->action_name." Compliance Summary (".$this->action_name." Status)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $childObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));
		$child_rows = $db->mysql_fetch_all_fld($child_sql,$childObject->getIDField());
		foreach($child_rows as $key => $r) {
			$date_completed = $r[$childObject->getDateCompletedField()];
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $childObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}
		return $data;
	}

	private function getContractComplianceStatus($f) {
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'contract'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "contract";
		$topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
		$childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
		$topObject->fixedReport();
		$childObject->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $childObject->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));
		$child_rows = $db->mysql_fetch_all_fld($child_sql,$childObject->getIDField());
		foreach($child_rows as $key => $r) {
			$parent_id = $r['grand_parent_id'];
			if(!isset($data[$graph]['subs'][$parent_id])) {
				$data[$graph]['subs'][$parent_id] = $r[$topObject->getNameField()];
				foreach($data[$graph]['result_options'] as $rkey => $rr) {
					$data[$graph]['data'][$parent_id][$rkey] = 0;
				}
			}
			$date_completed = $r[$childObject->getDateCompletedField()];
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $childObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$parent_id][$res]++;
		}
		return $data;
	}


    private function getRiskComplianceOverall($f) {
        $data = array(
            'settings'=>array(
                'manualPageTitle'=>false,
                'layout'=>"L",
                'hasMain'=>true,
                'hasSub'=>true,
                'bar'=>array(
                    'format'=>"100",
                    'grid'=>false,
                ),
            ),
            'register_by_completion_status'=>array(),
        );
        $db = new ASSIST_DB("client");
        //ACTION BY COMPLETION STATUS
        $graph = "register_by_completion_status";
        $topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
        $childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
        $topObject->fixedReport();
        $childObject->fixedReport();
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by Completion Status';
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['result_options'] = $childObject->getResultSettings();
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();

        $child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));

        $child_rows = $db->mysql_fetch_all_by_id($child_sql,$childObject->getIDField());

        $risk_object = new MODULE3_RGSTR2_REPORT_RISK($_REQUEST['page']);
        $client_db = new ASSIST_MODULE_HELPER('client', $_SESSION['cc']);
        $risk_sql = $risk_object->getSQL($client_db,array('financial_year'=>array($f)));
        $risk_rows = $client_db->mysql_fetch_all_by_id($risk_sql,'rsk_id');

        $owner_object = new RGSTR2_CONTRACT_OWNER();
        $active_owners = $owner_object->getActiveOwners();

        $owners_data_structure = array();
        foreach($active_owners as $key => $val){
            $owners_data_structure[$val['id']]['name'] = $val['name'];
        }

        foreach($risk_rows as $key => $val){
            $owners_data_structure[$val['rsk_owner_id']]['risks'][$val['rsk_id']] = $val;
        }

        foreach($child_rows as $key => $val){
            $owners_data_structure[$risk_rows[$val['parent_id']]['rsk_owner_id']]['risks'][$val['parent_id']]['actions'][$val['action_id']] = $val;
        }


        $risks_by_action_progress_per_department = array();

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }


            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    if(isset($val2['actions']) && is_array($val2['actions']) && count($val2['actions']) > 0){
                        foreach($val2['actions'] as $key3 => $val3){
                            $date_completed = $val3[$childObject->getDateCompletedField()];
                            $my_action_progress = array('action_progress'=>$val3['action_progress'],'deadline'=>$val3[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
                            $res = $childObject->getCompliance($my_action_progress);
                            $risks_by_action_progress_per_department[$key][$res]++;
                            $data[$graph]['data']['ALL'][$res]++;
                        }
                    }
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);
//temporarily hide this graph [JC - 2018-05-23]
unset($data[$graph]);

        //HEADINGS ----
        $headObject = new RGSTR2_HEADINGS();
        $risk_headings = $headObject->getMainObjectHeadings("RISK","LIST",'MANAGE');

        //RISK BY RISK STATUS
        $graph = "risk_by_risk_status";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_status_id']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_status');
        $risk_status_list = $list_object->getListItems();

        $risk_status_result_options = array();
        foreach($risk_status_list as $key => $val){
            $risk_status_result_options[$val['id']]['text'] = $val['name'];
            $risk_status_result_options[$val['id']]['code'] = $val['id'];
            $risk_status_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_status_result_options;


        foreach($owners_data_structure as $key => $val){

            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_status_id']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);


        //============================
        //RISK BY RISK CLASSIFICATIONS
        //============================

        //RISK BY IMPACT
        $graph = "risk_by_risk_impact";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_impact']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_impact');
        $risk_impact_list = $list_object->getListItems();

        $risk_impact_result_options = array();
        foreach($risk_impact_list as $key => $val){
            $risk_impact_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_impact_result_options[$val['id']]['code'] = $val['id'];
            $risk_impact_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_impact_result_options;

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_impact']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY LIKELIHOOD
        $graph = "risk_by_risk_likelihood";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_likelihood']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_likelihood');
        $risk_likelihood_list = $list_object->getListItems();

        $risk_likelihood_result_options = array();
        foreach($risk_likelihood_list as $key => $val){
            $risk_likelihood_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_likelihood_result_options[$val['id']]['code'] = $val['id'];
            $risk_likelihood_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_likelihood_result_options;

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_likelihood']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY CONTROL EFFECTIVENESS
        $graph = "risk_by_risk_control_effectiveness";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_control_effectiveness']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_control_effectiveness');
        $risk_control_effectiveness_list = $list_object->getListItems();

        $risk_control_effectiveness_result_options = array();
        foreach($risk_control_effectiveness_list as $key => $val){
            $risk_control_effectiveness_result_options[$val['id']]['text'] = $val['name'];
            $risk_control_effectiveness_result_options[$val['id']]['code'] = $val['id'];
            $risk_control_effectiveness_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_control_effectiveness_result_options;

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_control_effectiveness']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY INHERENT EXPOSURE
        $graph = "risk_by_risk_inherent_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_inherent_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_inherent_exposure');
        $risk_inherent_exposure_list = $list_object->getListItems();

        $risk_inherent_exposure_result_options = array();
        foreach($risk_inherent_exposure_list as $key => $val){
            $risk_inherent_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_inherent_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_inherent_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_inherent_exposure_result_options;

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_inherent_exposure']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY RESIDUAL EXPOSURE
        $graph = "risk_by_risk_residual_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_residual_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_residual_exposure');
        $risk_residual_exposure_list = $list_object->getListItems();

        $risk_residual_exposure_result_options = array();
        foreach($risk_residual_exposure_list as $key => $val){
            $risk_residual_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_residual_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_residual_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_residual_exposure_result_options;

        foreach($owners_data_structure as $key => $val){
            if(!isset($data[$graph]['subs']['ALL'])){
                $data[$graph]['subs']['ALL'] = 'ALL';
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $risks_by_action_progress_per_department[$key][$rkey] = 0;
                    $data[$graph]['data']['ALL'][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
                }
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data']['ALL'][$val2['rsk_residual_exposure']]++;
                }
            }

        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        return $data;
    }


    private function getRiskCompliancePerDepartment($f) {
        $data = array(
            'settings'=>array(
                'manualPageTitle'=>false,
                'layout'=>"L",
                'hasMain'=>true,
                'hasSub'=>true,
                'bar'=>array(
                    'format'=>"100",
                    'grid'=>false,
                ),
            ),
            'register_by_completion_status'=>array(),
        );
        $db = new ASSIST_DB("client");
        //ACTION BY COMPLETION STATUS
        $graph = "register_by_completion_status";
        $topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
        $childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
        $topObject->fixedReport();
        $childObject->fixedReport();
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by Completion Status';
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['result_options'] = $childObject->getResultSettings();
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();

        $child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));

//        echo '<pre style="font-size: 18px">';
//        echo '<p>The SQL</p>';
//        print_r($child_sql);
//        echo '</pre>';

        $child_rows = $db->mysql_fetch_all_by_id($child_sql,$childObject->getIDField());

//        echo '<pre style="font-size: 18px">';
//        echo '<p>CHILD ROWS</p>';
//        print_r($child_rows);
//        echo '</pre>';
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>CHILD ROWS COUNT</p>';
//        print_r(count($child_rows));
//        echo '</pre>';

        $risk_object = new MODULE3_RGSTR2_REPORT_RISK($_REQUEST['page']);
        $client_db = new ASSIST_MODULE_HELPER('client', $_SESSION['cc']);
        $risk_sql = $risk_object->getSQL($client_db,array('financial_year'=>array($f)));
        $risk_rows = $client_db->mysql_fetch_all_by_id($risk_sql,'rsk_id');

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RISK SQL</p>';
//        print_r($risk_sql);
//        echo '</pre>';


//        echo '<pre style="font-size: 18px">';
//        echo '<p>RISK OBJECtS</p>';
//        print_r($risk_rows);
//        echo '</pre>';

        $rsk_owner_field = 'rsk_owner_id';

        $owner_object = new RGSTR2_CONTRACT_OWNER();
        $active_owners = $owner_object->getActiveOwners();

//        echo '<pre style="font-size: 18px">';
//        echo '<p>OWNERS</p>';
//        print_r($active_owners);
//        echo '</pre>';

        $owners_data_structure = array();
        foreach($active_owners as $key => $val){
            $owners_data_structure[$val['id']]['name'] = $val['name'];
        }

        foreach($risk_rows as $key => $val){
            $owners_data_structure[$val['rsk_owner_id']]['risks'][$val['rsk_id']] = $val;
        }

        foreach($child_rows as $key => $val){
            $owners_data_structure[$risk_rows[$val['parent_id']]['rsk_owner_id']]['risks'][$val['parent_id']]['actions'][$val['action_id']] = $val;
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>OWNER DATA STRUCTURE</p>';
//        print_r($owners_data_structure);
//        echo '</pre>';

        $risks_by_action_progress_per_department = array();

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    if(isset($val2['actions']) && is_array($val2['actions']) && count($val2['actions']) > 0){
                        foreach($val2['actions'] as $key3 => $val3){
                            $date_completed = $val3[$childObject->getDateCompletedField()];
                            $my_action_progress = array('action_progress'=>$val3['action_progress'],'deadline'=>$val3[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
                            $res = $childObject->getCompliance($my_action_progress);
                            $risks_by_action_progress_per_department[$key][$res]++;
                            $data[$graph]['data'][$key][$res]++;
                        }
                    }
                }
            }

        }

        //HEADINGS ----
        $headObject = new RGSTR2_HEADINGS();
        $risk_headings = $headObject->getMainObjectHeadings("RISK","LIST",'MANAGE');

//temporarily hide this graph [JC - 2018-05-23]
unset($data[$graph]);


        //RISK BY RISK STATUS
        $graph = "risk_by_risk_status";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_status_id']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_status');
        $risk_status_list = $list_object->getListItems();

        $risk_status_result_options = array();
        foreach($risk_status_list as $key => $val){
            $risk_status_result_options[$val['id']]['text'] = $val['name'];
            $risk_status_result_options[$val['id']]['code'] = $val['id'];
            $risk_status_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_status_result_options;


        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_status_id']]++;
                }
            }

        }


        //============================
        //RISK BY RISK CLASSIFICATIONS
        //============================

        //RISK BY IMPACT
        $graph = "risk_by_risk_impact";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_impact']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_impact');
        $risk_impact_list = $list_object->getListItems();

        $risk_impact_result_options = array();
        foreach($risk_impact_list as $key => $val){
            $risk_impact_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_impact_result_options[$val['id']]['code'] = $val['id'];
            $risk_impact_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_impact_result_options;

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_impact']]++;
                }
            }

        }

        //RISK BY LIKELIHOOD
        $graph = "risk_by_risk_likelihood";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_likelihood']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_likelihood');
        $risk_likelihood_list = $list_object->getListItems();

        $risk_likelihood_result_options = array();
        foreach($risk_likelihood_list as $key => $val){
            $risk_likelihood_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_likelihood_result_options[$val['id']]['code'] = $val['id'];
            $risk_likelihood_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_likelihood_result_options;

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_likelihood']]++;
                }
            }

        }

        //RISK BY CONTROL EFFECTIVENESS
        $graph = "risk_by_risk_control_effectiveness";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_control_effectiveness']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_control_effectiveness');
        $risk_control_effectiveness_list = $list_object->getListItems();

        $risk_control_effectiveness_result_options = array();
        foreach($risk_control_effectiveness_list as $key => $val){
            $risk_control_effectiveness_result_options[$val['id']]['text'] = $val['name'];
            $risk_control_effectiveness_result_options[$val['id']]['code'] = $val['id'];
            $risk_control_effectiveness_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_control_effectiveness_result_options;

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_control_effectiveness']]++;
                }
            }

        }

        //RISK BY INHERENT EXPOSURE
        $graph = "risk_by_risk_inherent_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_inherent_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_inherent_exposure');
        $risk_inherent_exposure_list = $list_object->getListItems();

        $risk_inherent_exposure_result_options = array();
        foreach($risk_inherent_exposure_list as $key => $val){
            $risk_inherent_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_inherent_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_inherent_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_inherent_exposure_result_options;

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_inherent_exposure']]++;
                }
            }

        }

        //RISK BY RESIDUAL EXPOSURE
        $graph = "risk_by_risk_residual_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_residual_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_residual_exposure');
        $risk_residual_exposure_list = $list_object->getListItems();

        $risk_residual_exposure_result_options = array();
        foreach($risk_residual_exposure_list as $key => $val){
            $risk_residual_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_residual_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_residual_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_residual_exposure_result_options;

        foreach($owners_data_structure as $key => $val){
            $data[$graph]['subs'][$key] = $val['name'];
            foreach($data[$graph]['result_options'] as $rkey => $rr) {
                $risks_by_action_progress_per_department[$key][$rkey] = 0;
                $data[$graph]['data'][$key][$rkey] = $risks_by_action_progress_per_department[$key][$rkey];
            }

            if(isset($val['risks']) && is_array($val['risks']) && count($val['risks']) > 0){
                foreach($val['risks'] as $key2 => $val2){
                    $data[$graph]['data'][$key][$val2['rsk_residual_exposure']]++;
                }
            }

        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>DATA</p>';
//        print_r($data);
//        echo '</pre>';


        return $data;
    }



    private function getActionProgressDashboard($f) {
        $data = array(
            'settings'=>array(
                'manualPageTitle'=>false,
                'layout'=>"L",
                'hasMain'=>true,
                'hasSub'=>true,
                'bar'=>array(
                    'format'=>"100",
                    'grid'=>false,
                ),
            ),
            'action_by_completion_status'=>array(),
        );
        $db = new ASSIST_DB("client");
        //ACTION BY COMPLETION STATUS
        $graph = "action_by_completion_status";
        $topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
        $childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
        $topObject->fixedReport();
        $childObject->fixedReport();
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by Completion Status';
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['result_options'] = $childObject->getResultSettings();
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();

        $child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));

        $child_rows = $db->mysql_fetch_all_by_id($child_sql,$childObject->getIDField());

        foreach($child_rows as $key => $r) {
            $owner = 'ALL';
            if(!isset($data[$graph]['subs'][$owner])) {
                $data[$graph]['subs'][$owner] = $owner;
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $data[$graph]['data'][$owner][$rkey] = 0;
                }
            }
            $date_completed = $r[$childObject->getDateCompletedField()];
            $my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
            $res = $childObject->getCompliance($my_action_progress);
            $data[$graph]['data'][$owner][$res]++;
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']);
        $data[$graph]['subs'] = array();

        //HEADINGS ----
        $headObject = new RGSTR2_HEADINGS();
        $risk_headings = $headObject->getMainObjectHeadings("RISK","LIST",'MANAGE');
        $action_headings = $headObject->getMainObjectHeadings("ACTION","LIST",'MANAGE');

        //ACTION BY ACTION STATUS
        $graph = "action_by_action_status";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $action_headings['action_status_id']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('action_status');
        $action_status_list = $list_object->getListItems();

        $action_status_result_options = array();
        foreach($action_status_list as $key => $val){
            $action_status_result_options[$val['id']]['text'] = (isset($val['client_name']) && is_string($val['client_name']) && strlen($val['client_name']) > 0 ? $val['client_name'] : $val['default_name'] );
            $action_status_result_options[$val['id']]['code'] = $val['id'];
            $action_status_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $action_status_result_options;


        $owner_field = $childObject->getOwnerField();
        foreach($child_rows as $key => $r) {
            $owner = $r[$owner_field];
            if(!isset($data[$graph]['subs'][$owner])) {
                $data[$graph]['subs'][$owner] = $owner;
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $data[$graph]['data'][$owner][$rkey] = 0;
                }
            }

            $data[$graph]['data'][$owner][$r['action_status_id']]++;
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']);
        $data[$graph]['subs'] = array();

        //=============================
        //ACTION BY RISK CLASSIFICATION
        //=============================

        $risk_object = new MODULE3_RGSTR2_REPORT_RISK($_REQUEST['page']);
        $client_db = new ASSIST_MODULE_HELPER('client', $_SESSION['cc']);
        $risk_sql = $risk_object->getSQL($client_db,array('financial_year'=>array($f)));
        $risk_rows = $client_db->mysql_fetch_all_by_id($risk_sql,'rsk_id');

        $risk_data_structure = array();
        foreach($risk_rows as $key => $val){
            $risk_data_structure[$val['rsk_id']] = $val;
        }

        foreach($child_rows as $key => $val){
            $risk_data_structure[$val['parent_id']]['actions'][$val['action_id']] = $val;
        }

        //============================
        //RISK BY RISK CLASSIFICATIONS
        //============================

        //RISK BY IMPACT
        $graph = "by_risk_impact";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_impact']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_impact');
        $risk_impact_list = $list_object->getListItems();

        $risk_impact_result_options = array();
        foreach($risk_impact_list as $key => $val){
            $risk_impact_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_impact_result_options[$val['id']]['code'] = $val['id'];
            $risk_impact_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_impact_result_options;


        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = 'ALL';
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $owner;
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_impact']]++;
                }
            }
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY LIKELIHOOD
        $graph = "risk_by_risk_likelihood";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_likelihood']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_likelihood');
        $risk_likelihood_list = $list_object->getListItems();

        $risk_likelihood_result_options = array();
        foreach($risk_likelihood_list as $key => $val){
            $risk_likelihood_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_likelihood_result_options[$val['id']]['code'] = $val['id'];
            $risk_likelihood_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_likelihood_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = 'ALL';
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $owner;
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_likelihood']]++;
                }
            }
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY CONTROL EFFECTIVENESS
        $graph = "risk_by_risk_control_effectiveness";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_control_effectiveness']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_control_effectiveness');
        $risk_control_effectiveness_list = $list_object->getListItems();

        $risk_control_effectiveness_result_options = array();
        foreach($risk_control_effectiveness_list as $key => $val){
            $risk_control_effectiveness_result_options[$val['id']]['text'] = $val['name'];
            $risk_control_effectiveness_result_options[$val['id']]['code'] = $val['id'];
            $risk_control_effectiveness_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_control_effectiveness_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = 'ALL';
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $owner;
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_control_effectiveness']]++;
                }
            }
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY INHERENT EXPOSURE
        $graph = "risk_by_risk_inherent_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_inherent_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_inherent_exposure');
        $risk_inherent_exposure_list = $list_object->getListItems();

        $risk_inherent_exposure_result_options = array();
        foreach($risk_inherent_exposure_list as $key => $val){
            $risk_inherent_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_inherent_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_inherent_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_inherent_exposure_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = 'ALL';
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $owner;
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_inherent_exposure']]++;
                }
            }
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);

        //RISK BY RESIDUAL EXPOSURE
        $graph = "risk_by_risk_residual_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_residual_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_residual_exposure');
        $risk_residual_exposure_list = $list_object->getListItems();

        $risk_residual_exposure_result_options = array();
        foreach($risk_residual_exposure_list as $key => $val){
            $risk_residual_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_residual_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_residual_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_residual_exposure_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = 'ALL';
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $owner;
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_residual_exposure']]++;
                }
            }
        }

        //Get rid of Bar Graph
        unset($data[$graph]['subs']['ALL']);


        return $data;
    }


	private function getActionProgressPerPerson($f) {
        $data = array(
            'settings'=>array(
                'manualPageTitle'=>false,
                'layout'=>"L",
                'hasMain'=>true,
                'hasSub'=>true,
                'bar'=>array(
                    'format'=>"100",
                    'grid'=>false,
                ),
            ),
            'action_by_completion_status'=>array(),
        );
        $db = new ASSIST_DB("client");
        //ACTION BY COMPLETION STATUS
        $graph = "action_by_completion_status";
        $topObject = new MODULE3_RGSTR2_REPORT_REGISTER($_REQUEST['page']);
        $childObject = new MODULE3_RGSTR2_REPORT_ACTION($_REQUEST['page']);
        $topObject->fixedReport();
        $childObject->fixedReport();
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by Completion Status';
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['result_options'] = $childObject->getResultSettings();
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();

        $child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));

//        echo '<pre style="font-size: 18px">';
//        echo '<p>The SQL</p>';
//        print_r($child_sql);
//        echo '</pre>';

        $child_rows = $db->mysql_fetch_all_by_id($child_sql,$childObject->getIDField());

//        echo '<pre style="font-size: 18px">';
//        echo '<p>CHILD ROWS</p>';
//        print_r($child_rows);
//        echo '</pre>';
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>CHILD ROWS COUNT</p>';
//        print_r(count($child_rows));
//        echo '</pre>';

        $owner_field = $childObject->getOwnerField();
        foreach($child_rows as $key => $r) {
            $owner = $r[$owner_field];
            if(!isset($data[$graph]['subs'][$owner])) {
                $data[$graph]['subs'][$owner] = $r[$owner_field."_tkid"];
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $data[$graph]['data'][$owner][$rkey] = 0;
                }
            }
            $date_completed = $r[$childObject->getDateCompletedField()];
            $my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
            $res = $childObject->getCompliance($my_action_progress);
            $data[$graph]['data'][$owner][$res]++;
        }

        //HEADINGS ----
        $headObject = new RGSTR2_HEADINGS();
        $risk_headings = $headObject->getMainObjectHeadings("RISK","LIST",'MANAGE');
        $action_headings = $headObject->getMainObjectHeadings("ACTION","LIST",'MANAGE');

        //ACTION BY ACTION STATUS
        $graph = "action_by_action_status";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $action_headings['action_status_id']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('action_status');
        $action_status_list = $list_object->getListItems();

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINTHERE</p>';
//        print_r($action_status_list);
//        echo '</pre>';

        $action_status_result_options = array();
        foreach($action_status_list as $key => $val){
            $action_status_result_options[$val['id']]['text'] = (isset($val['client_name']) && is_string($val['client_name']) && strlen($val['client_name']) > 0 ? $val['client_name'] : $val['default_name'] );
            $action_status_result_options[$val['id']]['code'] = $val['id'];
            $action_status_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $action_status_result_options;


        $owner_field = $childObject->getOwnerField();
        foreach($child_rows as $key => $r) {
            $owner = $r[$owner_field];
            if(!isset($data[$graph]['subs'][$owner])) {
                $data[$graph]['subs'][$owner] = $r[$owner_field."_tkid"];
                foreach($data[$graph]['result_options'] as $rkey => $rr) {
                    $data[$graph]['data'][$owner][$rkey] = 0;
                }
            }

            $data[$graph]['data'][$owner][$r['action_status_id']]++;
        }

        //=============================
        //ACTION BY RISK CLASSIFICATION
        //=============================

        $risk_object = new MODULE3_RGSTR2_REPORT_RISK($_REQUEST['page']);
        $client_db = new ASSIST_MODULE_HELPER('client', $_SESSION['cc']);
        $risk_sql = $risk_object->getSQL($client_db,array('financial_year'=>array($f)));
        $risk_rows = $client_db->mysql_fetch_all_by_id($risk_sql,'rsk_id');

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RISK SQL</p>';
//        print_r($risk_sql);
//        echo '</pre>';


//        echo '<pre style="font-size: 18px">';
//        echo '<p>RISK OBJECtS</p>';
//        print_r($risk_rows);
//        echo '</pre>';


        $risk_data_structure = array();
        foreach($risk_rows as $key => $val){
            $risk_data_structure[$val['rsk_id']] = $val;
        }

        foreach($child_rows as $key => $val){
            $risk_data_structure[$val['parent_id']]['actions'][$val['action_id']] = $val;
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RISK DATA STRUCTURE</p>';
//        print_r($risk_data_structure);
//        echo '</pre>';


        //============================
        //RISK BY RISK CLASSIFICATIONS
        //============================

        //RISK BY IMPACT
        $graph = "by_risk_impact";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_impact']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_impact');
        $risk_impact_list = $list_object->getListItems();

        $risk_impact_result_options = array();
        foreach($risk_impact_list as $key => $val){
            $risk_impact_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_impact_result_options[$val['id']]['code'] = $val['id'];
            $risk_impact_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_impact_result_options;


        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = $val2[$owner_field];
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $val2[$owner_field."_tkid"];
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_impact']]++;
                }
            }
        }

        //RISK BY LIKELIHOOD
        $graph = "risk_by_risk_likelihood";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_likelihood']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_likelihood');
        $risk_likelihood_list = $list_object->getListItems();

        $risk_likelihood_result_options = array();
        foreach($risk_likelihood_list as $key => $val){
            $risk_likelihood_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_likelihood_result_options[$val['id']]['code'] = $val['id'];
            $risk_likelihood_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_likelihood_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = $val2[$owner_field];
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $val2[$owner_field."_tkid"];
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_likelihood']]++;
                }
            }
        }

        //RISK BY CONTROL EFFECTIVENESS
        $graph = "risk_by_risk_control_effectiveness";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_control_effectiveness']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_control_effectiveness');
        $risk_control_effectiveness_list = $list_object->getListItems();

        $risk_control_effectiveness_result_options = array();
        foreach($risk_control_effectiveness_list as $key => $val){
            $risk_control_effectiveness_result_options[$val['id']]['text'] = $val['name'];
            $risk_control_effectiveness_result_options[$val['id']]['code'] = $val['id'];
            $risk_control_effectiveness_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_control_effectiveness_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = $val2[$owner_field];
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $val2[$owner_field."_tkid"];
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_control_effectiveness']]++;
                }
            }
        }

        //RISK BY INHERENT EXPOSURE
        $graph = "risk_by_risk_inherent_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_inherent_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_inherent_exposure');
        $risk_inherent_exposure_list = $list_object->getListItems();

        $risk_inherent_exposure_result_options = array();
        foreach($risk_inherent_exposure_list as $key => $val){
            $risk_inherent_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_inherent_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_inherent_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_inherent_exposure_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = $val2[$owner_field];
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $val2[$owner_field."_tkid"];
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_inherent_exposure']]++;
                }
            }
        }

        //RISK BY RESIDUAL EXPOSURE
        $graph = "risk_by_risk_residual_exposure";
        $data[$graph]['page_title'] = $this->getReportTitle() . ' by ' . $risk_headings['rsk_residual_exposure']['name'];
        $data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
        $data[$graph]['type'] = "BAR";
        $data[$graph]['data'] = array();
        $data[$graph]['subs'] = array();


        $list_object = new RGSTR2_LIST('risk_residual_exposure');
        $risk_residual_exposure_list = $list_object->getListItems();

        $risk_residual_exposure_result_options = array();
        foreach($risk_residual_exposure_list as $key => $val){
            $risk_residual_exposure_result_options[$val['id']]['text'] = $val['name'] . ' [' . $val['rating_from'] . ' - ' .  $val['rating_to'] . ']';
            $risk_residual_exposure_result_options[$val['id']]['code'] = $val['id'];
            $risk_residual_exposure_result_options[$val['id']]['color'] = $val['colour'];
        }

        $data[$graph]['result_options'] = $risk_residual_exposure_result_options;

        foreach($risk_data_structure as $key => $val){
            if(isset($val['actions']) && is_array($val['actions']) && count($val['actions']) > 0){
                foreach($val['actions'] as $key2 => $val2){
                    $owner = $val2[$owner_field];
                    if(!isset($data[$graph]['subs'][$owner])) {
                        $data[$graph]['subs'][$owner] = $val2[$owner_field."_tkid"];
                        foreach($data[$graph]['result_options'] as $rkey => $rr) {
                            $data[$graph]['data'][$owner][$rkey] = 0;
                        }
                    }
                    $data[$graph]['data'][$owner][$val['rsk_residual_exposure']]++;
                }
            }
        }


        return $data;
	}

}

?>