<?php

include_once("../library3/autoloader.php");

//TABLE FIXED REPORT HACK
if(isset($_REQUEST['code']) && ($_REQUEST['code'] == 'register_report' || $_REQUEST['code'] == 'top_ten_risks')){
    $headObject = new RGSTR2_HEADINGS();
    if($_REQUEST['code'] == 'register_report'){
        $_REQUEST['class'] = 'MODULE3_RGSTR2_REPORT_REGISTER';
        $_REQUEST['page'] = 'report_generate_contract';
        $_REQUEST['act'] = 'GENERATE';

        $object_type = 'REGISTER';
    }else{
        $_REQUEST['class'] = 'MODULE3_RGSTR2_REPORT_RISK';
        $_REQUEST['page'] = 'report_generate_deliverable';
        $_REQUEST['act'] = 'GENERATE';

        $_REQUEST['rhead'] = 'Top Ten Risks';

        $object_type = 'RISK';

        $_REQUEST['columns']['register_id'] = 'on';
        $_REQUEST['filtertype']['register_id'] = 'ANY';

        $_REQUEST['sort'][] = 'rsk_inherent_exposure_rating';
        $_REQUEST['sort'][] = 'rsk_residual_exposure_rating';


    }
    $headings = $headObject->getReportObjectHeadings($object_type);
    foreach($headings as $id => $head) {
        $_REQUEST['columns'][$head['field']] = 'on';
        $_REQUEST['filtertype'][$head['field']] = 'ANY';

        if($_REQUEST['code'] == 'register_report'){
            $_REQUEST['sort'][] = $head['field'];
        }
    }
    $_REQUEST['group_by'] = 'X';
    $_REQUEST['filter'] = array();
}

$source_class = $_REQUEST['class'];

//echo $source_class;

$page = $_REQUEST['page'];


$me = new $source_class($_REQUEST['page']);

//$me->arrPrint($_REQUEST);

$_REQUEST['jquery_version'] = "1.10.0";

$me->displayPageHeader();

$me->displayPage();
?>

<script>
    //Filter by Risk || Register Hack
    $(function() {
        var source_class = $("#source_class").val();

        if(source_class == 'MODULE3_RGSTR2_REPORT_RISK'){

        }

        if(source_class == 'MODULE3_RGSTR2_REPORT_REGISTER'){

        }
    });
</script>

</body></html>