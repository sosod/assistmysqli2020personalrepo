<?php

class MODULE3_CNTRCT_REPORT_FIXED extends MODULE3_GENERIC_REPORT_FIXED {

	//CONSTRUCT
	private $page;
	private $folder = "report";
	private $date_format = "DATETIME";
	protected $my_class_name = "MODULE3_CNTRCT_REPORT_FIXED";
	
	private $my_reports;
	private $my_filters;
	private $use_a_filter = true;
	
	private $cntrct_report;
	
	private $contract_name = "CONTRACT";
	private $deliverable_name = "DELIVERABLE";
	private $action_name = "ACTION";
	
	
	private $active_report_title;

	public function __construct($p) {	
		$this->page = $p;
		parent::__construct($p,$this->date_format,$this->my_class_name);
		
		$me = new CNTRCT_HELPER();
		
		$this->contract_name = $me->getObjectName("CONTRACT");
		$this->deliverable_name = $me->getObjectName("DELIVERABLE");
		$this->action_name = $me->getObjectName("ACTION");
		
		$contract_name = $this->contract_name;
		$deliverable_name = $this->deliverable_name;
		$action_name = $this->action_name;
		
		$this->my_reports = array(
			1=>array(
					'id'=>1,
					'name'=>"Compliance Analysis Dashboard",
					'descrip'=>"Overview of $contract_name and $deliverable_name Compliance measured by $action_name Progress and $action_name Compliance measured by $action_name Status",
					'format'=>"Pie",
					'code'=>"dashboard",
				),
			2=> array(
					'id'=>2,
					'name'=>"$contract_name Compliance Status ($action_name Progress)",
					'descrip'=>"$contract_name Compliance per $action_name measured by $action_name Progress",
					'format'=>"Bar",
					'code'=>"contract_compliance_status",
				),
/*			3=>array(
					'id'=>3,
					'name'=>"$contract_name Compliance Status ($deliverable_name Status)",
					'descrip'=>"$contract_name Compliance per $deliverable_name measured by $deliverable_name Status",
					'format'=>"Bar",
					'code'=>"contract_compliance_status_per_deliverable",
				),*/
			3=>array(
					'id'=>4,
					'name'=>"$action_name Compliance per Person",
					'descrip'=>"$action_name Compliance per Person measured by $action_name Status",
					'format'=>"Bar",
					'code'=>"action_progress_per_person",
				),
		);
		
		$this->cntrct_report = new MODULE3_CNTRCT_REPORT($this->page,$this->date_format,$this->my_class_name,"");
		$this->cntrct_report->setFolder($this->folder);
		$this->my_filters = $this->cntrct_report->getFinancialYears();
		
	}
	
	
	protected function prepareGenerator() {
		$this->drawPageHeader();

		foreach($this->my_reports as $r) {
			$this->setReport($r['name'],$r['descrip'],$r['format'],$r['code']);
		}
		
		foreach($this->my_filters as $key=>$f) {
			$this->setFilter($key,$f);
		}
		
		$this->setUseFilter($this->use_a_filter);
		
		
	}
	
	protected function drawPageHeader() {
		$this->cntrct_report->displayPageHeading();
	}
	
	protected function prepareReport() {
		//$this->drawPageHeader();
		
		$report = $_REQUEST['code'];
		$rprt = explode("_",$report);
		$report = "";
		foreach($rprt as $r) { $report.=ucwords($r); }
		
		$f = explode("_",$_REQUEST['filter']);
		$financial_year = $f[0];
		
		foreach($this->my_reports as $r) {
			if($r['code']==$_REQUEST['code']) {
				$this->active_report_title = $r['name'];
				break;
			}
		}
		
		$my_function = "self::get".$report;
		$this->reporting_data = call_user_func($my_function,$financial_year);
		
	}
	
	protected function getReportTitle() { return $this->active_report_title; }
	
	
	private function getDashboard($f) { 
		$data = array(
					'settings'=>array(
							'layout'=>"P",
							'hasMain'=>true,
							'hasSub'=>false,
							'manualPageTitle'=>true,
						),
					'contract'=>array(),'deliverable'=>array(),'action'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "contract";
		$topObject = new MODULE3_CNTRCT_REPORT_CONTRACT($_REQUEST['page']);
		$topObject->fixedReport();
		$data[$graph]['graph_title'] = $this->contract_name." Compliance Summary (".$this->action_name." Progress)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $topObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$top_sql = $topObject->getSQL($db,array('financial_year'=>array($f))); 
		$top_rows = $db->mysql_fetch_all_fld($top_sql,$topObject->getIDField());
		foreach($top_rows as $key => $r) {
			$date_completed = $r[$topObject->getDateCompletedField()];
			if($r['action_progress']==100 && strtotime($date_completed)==0) {
				$date_completed = $r[$topObject->getActionDateCompletedField()];
				$r[$topObject->getDateCompletedField()] = $date_completed;
			}
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$topObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $topObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}


		//DELIVERABLES
		$graph = "deliverable";
		$midObject = new MODULE3_CNTRCT_REPORT_DELIVERABLE($_REQUEST['page']);
		$midObject->fixedReport();
		$data[$graph]['graph_title'] = $this->deliverable_name." Compliance Summary (".$this->action_name." Progress)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $midObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$mid_sql = $midObject->getSQL($db,array('financial_year'=>array($f)));
		$mid_rows = $db->mysql_fetch_all_fld($mid_sql,$midObject->getIDField());
		foreach($mid_rows as $key => $r) {
			$date_completed = $r[$midObject->getDateCompletedField()];
			if($r['action_progress']==100 && strtotime($date_completed)==0) {
				$date_completed = $r[$midObject->getActionDateCompletedField()];
				$r[$midObject->getDateCompletedField()] = $date_completed;
			}
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$midObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $midObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}

		//ACTIONS
		$graph = "action";
		$childObject = new MODULE3_CNTRCT_REPORT_ACTION($_REQUEST['page']);
		$childObject->fixedReport();
		$data[$graph]['graph_title'] = $this->action_name." Compliance Summary (".$this->action_name." Status)";
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $childObject->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));
		$child_rows = $db->mysql_fetch_all_fld($child_sql,$childObject->getIDField());
		foreach($child_rows as $key => $r) {
			$date_completed = $r[$childObject->getDateCompletedField()];
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $childObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}
		return $data;
	}
	
	private function getContractComplianceStatus($f) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'contract'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "contract";
		$topObject = new MODULE3_CNTRCT_REPORT_CONTRACT($_REQUEST['page']);
		$childObject = new MODULE3_CNTRCT_REPORT_ACTION($_REQUEST['page']);
		$topObject->fixedReport();
		$childObject->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $childObject->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$child_sql = $childObject->getSQL($db,array('financial_year'=>array($f)));
		$child_rows = $db->mysql_fetch_all_fld($child_sql,$childObject->getIDField());
		foreach($child_rows as $key => $r) {
			$parent_id = $r['grand_parent_id'];
			if(!isset($data[$graph]['subs'][$parent_id])) {
				$data[$graph]['subs'][$parent_id] = $r[$topObject->getNameField()];
				foreach($data[$graph]['result_options'] as $rkey => $rr) {
					$data[$graph]['data'][$parent_id][$rkey] = 0;
				}
			}
			$date_completed = $r[$childObject->getDateCompletedField()];
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $childObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$parent_id][$res]++;
		}
		return $data;
	}
	
/*	private function getContractComplianceStatusPerDeliverable($f) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'contract'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "legislation";
		$topObject = new MODULE3_CNTRCT_REPORT_CONTRACT($_REQUEST['page']);
		$midObject = new MODULE3_CNTRCT_REPORT_DELIVERABLE($_REQUEST['page']);
		$topObject->fixedReport();
		$midObject->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $midObject->getDeliverableStatusOptions();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$lsql = $leg->getSQL($db,array('financial_year'=>array($f),'filter_legislation_type'=>$t));
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		//set default results
		foreach($lkeys as $k) {
			$data[$graph]['subs'][$k] = $lrows[$k]['name'];
			foreach($data[$graph]['result_options'] as $key => $r) {
				$data[$graph]['data'][$k][$key] = 0;
			}
		}
		//get associated deliverables and status => returns d.id as id, l.id as leg, d.deliverable_status as status
		$ldel = $leg->getLegislationDeliverables($db,$lkeys);
		foreach($ldel as $key => $r) {
			$data[$graph]['data'][$r['leg']][$r['status']]++;
		}
		return $data;	
	}
	*/
	private function getActionProgressPerPerson($f) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'action'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "action";
		$topObject = new MODULE3_CNTRCT_REPORT_CONTRACT($_REQUEST['page']);
		$childObject = new MODULE3_CNTRCT_REPORT_ACTION($_REQUEST['page']);
		$topObject->fixedReport();
		$childObject->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$data[$graph]['blurb'] = $f!="ALL" && isset($this->my_filters[$f]) ? "Financial Year: ".$this->my_filters[$f] : "";
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $childObject->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get actions for given financial year
		//$top_sql = $leg->getSQL($db,array('financial_year'=>array($f),'filter_legislation_type'=>$t));
		//$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		//$lkeys = array_keys($lrows);
		//get associated action ids => returns a.id, l.id as leg, a.deadline, a.status
		//$lact = $leg->getLegislationActions($db,$lkeys,"a.owner");
		//$subs = $act->getActionOwners();
		//set default results
		//foreach($subs as $k=>$a) {
		//	$data[$graph]['subs'][$k] = $a;
		//	foreach($data[$graph]['result_options'] as $key => $r) {
		//		$data[$graph]['data'][$k][$key] = 0;
		//	}
		//}
		//get action progress & date completed for associated actions
		//$aap = $act->getActionProgress($db,array_keys($lact));
		//foreach($lact as $key => $r) {
		//	if(isset($data[$graph]['data'][$r['owner']])) {
		//		$my_result = isset($aap[$key]) ?  ($aap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
		//		$res = $act->getCompliance($my_result,$r['deadline'],$r['status']);
		//		$data[$graph]['data'][$r['owner']][$res]++;
		//	}
		//}
//		$this->arrPrint($data);
		$child_sql = $childObject->getSQL($db,array('financial_year'=>array($f))); 
		$child_rows = $db->mysql_fetch_all_by_id($child_sql,$childObject->getIDField()); 
		$owner_field = $childObject->getOwnerField(); 
		foreach($child_rows as $key => $r) {
			$owner = $r[$owner_field];
			if(!isset($data[$graph]['subs'][$owner])) {
				$data[$graph]['subs'][$owner] = $r[$owner_field."_tkid"];
				foreach($data[$graph]['result_options'] as $rkey => $rr) {
					$data[$graph]['data'][$owner][$rkey] = 0;
				}
			}
			$date_completed = $r[$childObject->getDateCompletedField()];
			$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$childObject->getDeadlineField()],'date_completed'=>$date_completed);
			$res = $childObject->getCompliance($my_action_progress);
			$data[$graph]['data'][$owner][$res]++;
		}
		return $data;
	}


	

}

?>