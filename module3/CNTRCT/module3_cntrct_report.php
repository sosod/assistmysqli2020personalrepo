<?php

class MODULE3_CNTRCT_REPORT extends MODULE3_GENERIC_REPORT {

	protected $folder;
	//GET FIELD DETAILS
	protected $titles;
	protected $allowchoose;
	protected $default_selected;
	protected $allowfilter;
	protected $types;
	protected $default_data;
	protected $data;
	protected $allowgroupby;
	protected $allowsortby;
	protected $default_sort;
	protected $sortposition;
	protected $db;

	public function __construct($p,$d,$c,$q) {
		parent::__construct($p,$d,$c,$q);
		$this->db = new ASSIST_DB("client");
	}


	public function getTableName() { return $this->table_name; }
	public function getRefTag() { return $this->reftag; }
	public function getMyObject() { return $this->me; }
	public function getMyObjectType() { return $this->object_type; }
	public function getIDField() { return $this->id_field; }
	public function getNameField() { return $this->name_field; }
	public function getDeadlineField() { return $this->deadline_field; }
	public function getDateCompletedField() { return $this->date_completed_field; }
	public function getActionDateCompletedField() { return $this->action_date_completed_field; }
	public function getOwnerField() { return $this->owner_field; }
	
	protected function prepareGenerator() {
		$this->report->disableUpdateLogOption("AUDIT");
		if(isset($_REQUEST['quick_id'])) {
			//$db = new ASSIST_DB("client");
			$quick = $this->db->mysql_fetch_one("SELECT * FROM ".$this->db->getDBRef()."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'],$quick['name'],$quick['description'],$quick['insertuser'],$quick['insertdate'],unserialize($quick['report']));
		}
		$this->getFieldDetails();
		$this->getFieldData();
		$this->displayPageHeading();
	}
	
	public function displayPageHeading() {
		//Navigation buttons & page title
		$page = $_REQUEST['page'];
		$menuObject = new CNTRCT_MENU();
		$menu = $menuObject->getPageMenu($page,"/CNTRCT/"); 
		$menuObject->drawPageTop($menu);
	}
	
	public function setFolder($f) {
		$this->folder = $f;
	}
	protected function getSortBySql($sort_fields) {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$s = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $st) {
				if(isset($sort_fields[$st])) {
					$s[] = $sort_fields[$st];
				}
			}
		}
		return (count($s)>0 ? " ORDER BY ".implode(", ",$s) : "");
	}
	
	
	public function getCompliance($ap) { 
		$result = "notCompletedAndNotOverdue";
		
		$p = isset($ap['action_progress']) ? $ap['action_progress'] : 0;
		$d = isset($ap['deadline']) ? strtotime($ap['deadline']) : 0;
		$dc = isset($ap['date_completed']) ? strtotime(date("d F Y",strtotime($ap['date_completed']))) : 1;
		$now = strtotime(date("d F Y"));
		
		if($p==100) {
			if($dc < $d) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$d) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		} else {
			if($d < $now) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		}
		return $result;
	}
		
	public function getFinancialYears() {
		$data = array();
		$fld = "financial_year";
		$masterObject = new CNTRCT_MASTER($fld);
		$data = $masterObject->getItemsForReport();
/*		$data = array();
		$me = new FinancialYear();
		$d = $me->fetchAll();
		$d2 = array();
		foreach($d as $e) {
			$e['name'] = $e['value'];
			$d2[] = $e;
		}
		$data = $this->listSort($d2);
		return $data;*/
		/*
		$me = new Assist_Master_FinancialYears();
		$table = $this->db->getDBRef()."_legislation";
		$fld = "financial_year";
		$d = $me->getLinked($table,$fld,"#.id as id, #.value as name");
		$data = $this->listSort($d);
		 */ 
		return $data;
	}
	public function getLegislations() {
		$data = array();
/*
		if(!isset($this->data['financial_year']) || count($this->data['financial_year'])==0) { $this->data['financial_year'] = $this->getFinancialYears(); }
		//$db = new ASSIST_DB("client");
		$sql = "SELECT L.id, L.name, L.financial_year 
				FROM ".$this->db->getDBRef()."_legislation L 
				INNER JOIN assist_".$this->getCmpCode()."_".ASSIST_MASTER_FINANCIALYEARS::TABLE." FY
				ON L.financial_year = FY.".ASSIST_MASTER_FINANCIALYEARS::ID_FLD."
				WHERE ".Legislation::getStatusSQLForWhere("L")."
				ORDER BY L.name, FY.".ASSIST_MASTER_FINANCIALYEARS::SORT_FLD;
		$legislations = $this->db->mysql_fetch_all_fld($sql,"id");
		
		foreach($legislations as $lid => $l) {
			$txt = "";
			if(!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR") {
				$txt = "[".MODULE_COMPL_REPORT_LEGISLATION::REFTAG.$lid."] ";
				$txt.= stripslashes($l['name']);
				$txt.= " (".$this->data['financial_year'][$l['financial_year']].")";
			} else {
				$txt = stripslashes($l['name']);
			//	$txt.= " [".MODULE_COMPL_REPORT_LEGISLATION::REFTAG.$lid."]";
			}
			$data[$lid] = $txt;
		}
 */
		return $data;
	}
	public function getDeliverables() {
		$data = array();
/*
		if(!isset($this->data['financial_year']) || count($this->data['financial_year'])==0) { $this->data['financial_year'] = $this->getFinancialYears(); }
		//$db = new ASSIST_DB("client");
		$sql = "SELECT L.id as legislation_id, L.name, L.financial_year , D.id as id, D.short_description, D.legislation_section as section
				FROM ".$this->db->getDBRef()."_deliverable D
				INNER JOIN ".$this->db->getDBRef()."_legislation L ON L.id = D.legislation
				INNER JOIN assist_".$this->getCmpCode()."_".ASSIST_MASTER_FINANCIALYEARS::TABLE." FY
				ON L.financial_year = FY.".ASSIST_MASTER_FINANCIALYEARS::ID_FLD."
				WHERE ".Legislation::getStatusSQLForWhere("L")."
				AND ".Deliverable::getStatusSQLForWhere("D")."
				ORDER BY L.name, FY.".ASSIST_MASTER_FINANCIALYEARS::SORT_FLD.", D.short_description, D.id";
		$deliverables = $this->db->mysql_fetch_all_fld($sql,"id");
		
		foreach($deliverables as $id => $d) {
			$txt = "";
			if(!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR") {
				$txt = "[".MODULE_COMPL_REPORT_LEGISLATION::REFTAG.$d['legislation_id']."-".MODULE_COMPL_REPORT_DELIVERABLE::REFTAG.$id."] ";
				$txt.=substr(stripslashes($d['short_description']),0,100).(strlen(stripslashes($d['short_description']))>75 ? "..." : "");
			} else {
				$txt.=$d['short_description'];
			}
			$txt.=strlen($d['section'])>0 ? " [".$d['section']."]" : "";
			$data[$id] = $txt;
		}
 */
		return $data;
	}
	
	
	
	
	protected function listSort($d) {
		$data = array();
		$d2 = array();
		foreach($d as $e) {
			if($e['id']==1 && (strtoupper($e['name'])=="UNSPECIFIED" || strlen($e['name'])==0) ) {
				$data[$e['id']] = "[Unspecified]";
			} else {
				$d2[$e['id']] = $e['name'];
			}
		}
		natcasesort($d2);
		foreach($d2 as $e=>$f) { $data[$e] = $f; }	
		return $data;
	}
	
	public function getResultOptions() {
		$data = array();
		foreach($this->result_categories as $key => $r) {
			$data[$key] = $r['text'];
		} 
		return $data;
	}

	public function getResultSettings() {
		return $this->result_categories;
	}
	
	protected function setFields() {
		foreach($this->titles as $i => $t) {	
			$ac = isset($this->allowchoose[$i]) ? $this->allowchoose[$i] : $this->defaults['allowchoose'];	
			$df = isset($this->default_selected[$i]) ? $this->default_selected[$i] : $this->defaults['default_selected'];
			$af = isset($this->allowfilter[$i]) ? $this->allowfilter[$i] : $this->defaults['allowfilter'];
			$dt = isset($this->types[$i]) ? $this->types[$i] : $this->defaults['type'];
			$do = isset($this->data[$i]) ? $this->data[$i] : $this->defaults['data'];
			$dd = isset($this->default_data[$i]) ? $this->default_data[$i] : $this->defaults['default_data'];
			$ag = isset($this->allowgroupby[$i]) ? $this->allowgroupby[$i] : $this->defaults['allowgroupby'];
			$as = isset($this->allowsortby[$i]) ? $this->allowsortby[$i] : $this->defaults['allowsortby'];
			$sp = isset($this->sortposition[$i]) ? $this->sortposition[$i] : $this->default_sort;
			$this->report->addField($i,$t,$ac,$df,$af,$dt,$do,$dd,$ag,$as,$sp);
			$this->default_sort++;
		}
	}
	
	
	public function getSQL($db,$filter) {
		return $this->setSQL($db,$filter);
	}
	
	protected function sourceLegislation($l,$filter) {
		$sql = "";
/*
		if(isset($filter['filter_legislation_type'])) {
			switch($filter['filter_legislation_type']) {
				case "CLIENT":
					$sql=" AND ".$l.".legislation_status & ".LEGISLATION::CREATED." = ".LEGISLATION::CREATED." ";
					break;
				case "MASTER":
					$sql=" AND ".$l.".legislation_status & ".LEGISLATION::CREATED." <> ".LEGISLATION::CREATED." ";
					break;
				case "ALL":
				default:
					$sql="";
					break;
			}
		}
 * 
 */
		return $sql;
	}

	
	
	protected function getResponses($tbl,$id,$db,$objects) {
		$result = array();
		/*
		$filter = strtoupper($_REQUEST['update_filter']); 
		if(count($objects)>0) {
			$sql = "SELECT * FROM ".$this->db->getDBRef()."_".$tbl."_update WHERE $id IN (".implode(",",$objects).") ORDER BY $id ASC, insertdate DESC";
			$a = $this->db->mysql_fetch_all($sql);
			$logs = array();
			foreach($a as $b) {
				$logs[$b[$id]][] = $b;
			}
			foreach($logs as $i => $x) {
				foreach($x as $l) {
					if( ($filter=="LAST" && !isset($result[$i])) || $filter!="LAST") {
						$l['changes'] = base64_decode($l['changes']);
						$c = unserialize($l['changes']);
						if( (isset($c['response']) && strlen($c['response'])>0) || (isset($c['attachments']) && count($c['attachments'])>0) || (isset($c['action_status']) && strlen($c['action_status'])>0) ) {
							$txt = "";
							$d = strtotime($l['insertdate']);
							if(isset($c['response']) && strlen($c['response'])>0) {
								$txt = stripslashes(" - ".$c['response']);
							}
							if(isset($c['attachments']) && count($c['attachments'])>0) {
								$at = array();
								foreach($c['attachments'] as $a) {
									if(strtoupper(substr($a,0,11))=="ATTACHMENT ") {
										$a = substr($a,11,strlen($a));
									}
									if(strtoupper(substr($a,-15))==" HAS BEEN ADDED") {
										$a = substr($a,0,-15);
									}
									$at[] = stripslashes($a);
								}
								if(count($at)>0) {
									$attach = true;
									$txt.= (strlen($txt)>0 ? " " : " - ")."[Attachment".(count($at)>0 ? "s" : "").": ".implode(", ",$at)."]";
								} else {
									$attach = false;
								}
							} else {
								$attach = false;
							}
							if(isset($c['action_status']) && trim(strtoupper($c['action_status']))=="ACTION HAS BEEN APPROVED") {
								if(strlen($txt)==0 && $filter!="LAST") {
									$txt.= (strlen($txt)>0 ? " " : " - ").(strlen($txt)>0 && !$attach ? "[" : "").strip_tags($c['action_status']).(strlen($txt)>0 && !$attach ? "]" : "");
								}
							}
							//$txt.="::::".$c['action_status'].":::";
							if(strlen($txt)>0) {
								$result[$i][] = $txt.stripslashes(" (Logged by ".$c['user']." on ".date("d-M-Y",$d)." at ".date("H:i",$d).")");
							}
						}
					}
				}
			}
		}
		 */
		return $result;
	}
		
	
	
}

?>