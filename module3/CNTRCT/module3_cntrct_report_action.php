<?php



class MODULE3_CNTRCT_REPORT_ACTION extends MODULE3_CNTRCT_REPORT {

	//CONSTRUCT
	private $date_format = "DATETIME";
	protected $my_class_name = "MODULE3_CNTRCT_REPORT_ACTION";
	private $my_quick_class_name = "MODULE3_CNTRCT_QUICK_REPORT";
	
	protected $me;
	protected $s=1;
	
	protected $object_type = "ACTION";
	protected $table_name = "action";
	protected $id_field = "action_id";
	protected $reftag = "CA";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;
	protected $owner_field;
	

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Contract Assist: Action Report";
	
	
	


	public function __construct($p) {	
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		$this->me = new CNTRCT_ACTION();
		$this->object_type = $this->me->getMyObjectType();
		$this->table_name = $this->me->getTableName();
		$this->reftag = $this->me->getRefTag();
		$this->deadline_field = $this->me->getDeadlineFieldName();
		$this->date_completed_field = $this->me->getDateCompletedFieldName();
		$this->owner_field = $this->me->getOwnerFieldName();
		
		if($p=="fixed") {
			$this->titles = array('financial_year'=>"Financial Year");
			$this->allowfilter['financial_year']=true;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {
		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array();
		$this->types = array(
			'result'				=> "RESULT",
		);
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array();
		$this->allowsortby = array(
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

		$listObject = new CNTRCT_LIST();
		
		$headObject = new CNTRCT_HEADINGS();
		$headings = $headObject->getReportObjectHeadings($this->object_type);
		//$deliverable_status = $headings['deliverable_status'];
		unset($headings['deliverable_status']);
		foreach($headings as $id => $head) {
			$fld = $head['field'];
			if($head['type']!="HEADING") {
				if($headObject->isListField($head['type'])) {
					$items = array();
					if($head['type']=="LIST") {
						$listObject->changeListType($head['list_table']);
						$items = $listObject->getItemsForReport();
					} elseif($head['type']=="MASTER") {
						$tbl = $head['list_table'];
						$masterObject = new CNTRCT_MASTER($fld);
						$items = $masterObject->getItemsForReport();
					} elseif($head['type']=="USER") {
						$userObject = new ASSIST_MODULE_USER();
						$items = $userObject->getItemsForReport($this->table_name, $fld);
					} elseif($head['type']=="OWNER") {
						$ownerObject = new CNTRCT_CONTRACT_OWNER();
						$items = $ownerObject->getItemsForReport();
						$tbl = $head['list_table'];
					} elseif($head['type']=="DEL_TYPE") {
						$items = $this->me->getAllDeliverableTypes();
						$tbl = $head['list_table'];
					}
					$this->data[$fld] = $items;
				}
				$this->processHeadings($headObject, $head, $headings);
			} elseif($head['field']=="contract_assess_status") {
				$fld = $head['field'];
				foreach($deliverable_status as $id=>$item) {
					$head['name'] = $item['name'];
					$head['field'] = $fld."_".$id;
					$head['type'] = "BOOL";
					$head['parent_id'] = $head['id'];
					$this->processHeadings($headObject, $head, $headings);
				}
			}
		}
		$this->titles['result'] = "Compliance Status";// On ".$this->titles['action_progress'];
	//$this->arrPrint($this->types);
	}
	
	private function processHeadings($headObject,$head,$headings) {
				$fld = $head['field'];
				$this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
				if($headObject->isListField($head['type'])) {
					$this->types[$fld] = "LIST";
				} elseif($headObject->isTextField($head['type'])) {
					$this->types[$fld] = "TEXT";
				} else {
					$this->types[$fld] = $head['type'];
				}
				$this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
				$this->allowchoose[$fld] = true;
				$this->sortposition[$fld] = $this->s; $this->s++;
				$this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
				if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
					$this->allowgroupby[$fld] = true;
				} else {
					$this->allowgroupby[$fld] = false;
				}
	}
	
	protected function getFieldData() {
		$this->data['result'] = $this->getResultOptions();
	}
		
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		$id_field = $this->me->getIDFieldName();
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,$id_field);
			foreach($rows as $key => $r) {
				$date_completed = $r[$this->date_completed_field];
				if($r['action_progress']==100 && strtotime($date_completed)==0) {
					$date_completed = $r[$this->action_date_completed_field];
					$r[$this->date_completed_field] = $date_completed;
				}
				$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$this->deadline_field],'date_completed'=>$date_completed);
				$r['result'] = $this->getCompliance($my_action_progress);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r[$id_field]] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = $this->getRefTag().$d;
								break;
							case "DATE":
								if(strtotime($d) != 0) {
									$d = date("d-M-Y",strtotime($d));
								} else {
									$d = "";
								}
								break;
							case "BOOL":
								if($d==="1") { $d = "Yes"; } elseif($d==="0") { $d = "No"; }
								break;
							case "LIST":
							case "TEXTLIST":
								$d2 = $d;
								$x = explode(",",$d);
								$d = "";
								$z = array();
								if($i=="status") {
									$z[] = (!isset($this->data[$i][$a])) ? $this->data[$i][1] : $this->data[$i][$a];	//default to new for unknown status
								} else {
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										}
									}
								}
								$d = count($z)>0 ? implode(", ",$z) : "Unspecified";
								break;
							case "PERC":
							case "PERCENTAGE":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "ATTACH":
								$d = "";
								break; 
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r[$id_field]][$i] = stripslashes($d);
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	

	
	
	protected function setSQL($db,$filter=array()) {
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$sql = "";
		$left_joins = array();
		
		$listObject = new CNTRCT_LIST("deliverable_status");
		$deliverable_status_items = $listObject->getAllListItems();
		
		$headObject = new CNTRCT_HEADINGS();
		
		$d_tblref = "D";
		$dObject = new CNTRCT_DELIVERABLE(); 
		$d_table = $dObject->getTableName();
		$d_id = $d_tblref.".".$dObject->getIDFieldName();
		$d_status = $d_tblref.".".$dObject->getStatusFieldName();
		$d_parent = $d_tblref.".".$dObject->getParentFieldName();
		$d_headings = $headObject->getReportObjectHeadings($dObject->getMyObjectType());

		$c_tblref = "C";
		$cObject = new CNTRCT_CONTRACT(); 
		//$this->action_date_completed_field = $cObject->getDateCompletedFieldName();
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_name = $c_tblref.".".$cObject->getNameFieldName();
		$c_headings = $headObject->getReportObjectHeadings($cObject->getMyObjectType());
		unset($c_headings['deliverable_status']);
		//$c_status = $a_tblref.".".$aObject->getStatusFieldName();
		//$c_parent = $a_tblref.".".$aObject->getParentFieldName();
		//$c_progress = $a_tblref.".".$aObject->getProgressFieldName();
		//$c_progress2 = $aObject->getProgressFieldName();
		
		//set up contract variables
		$my_headings = $headObject->getReportObjectHeadings($this->getMyObjectType()); 
		$myObject = $this->getMyObject(); 
		$my_tblref = "A";
		$tblref = $my_tblref;
		$my_table = $myObject->getTableName();
		$my_id = $my_tblref.".".$myObject->getIDFieldName();
		$my_status = $my_tblref.".".$myObject->getStatusFieldName();
		$my_status_fld = $my_tblref.".".$myObject->getProgressStatusFieldName();
		$my_name = $myObject->getNameFieldName();
		$my_deadline = $myObject->getDeadlineFieldName();
		$my_field = $my_tblref.".".$myObject->getTableField();
		$my_parent = $myObject->getParentFieldName();
		
		$where = $myObject->getActiveStatusSQL($my_tblref);
		
			$sql = "SELECT DISTINCT $my_status as my_status
					, ".$my_tblref.".*
					, ".$c_name."
					, $c_id as grand_parent_id
					, $d_id as parent_id
					";
			$from = " $my_table $my_tblref 
					INNER JOIN $d_table $d_tblref
					  ON $my_parent = $d_id
					INNER JOIN $c_table $c_tblref
					  ON $d_parent = $c_id
			";
			$final_data['head'] = $my_headings;
			$id_fld = $myObject->getIDFieldName();
			$where_tblref = $my_tblref;
			$where_deadline = $my_deadline;
			$ref_tag = $myObject->getRefTag();
			$where_status_id_fld = $my_tblref.".".$myObject->getProgressStatusFieldName();
			$status_id_fld = $myObject->getProgressStatusFieldName();
			$where_status_fld = $my_tblref.".".$myObject->getStatusFieldName();
		

			$sort_by = array();
			$all_headings = array_merge($my_headings,$d_headings,$c_headings);
			$listObject = new CNTRCT_LIST();
			
//$this->arrPrint($all_headings);
			foreach($all_headings as $id => $head) {   
				$fld = $head['field'];
				$lj_tblref = substr($head['section'],0,1);
				if($head['type']=="LIST") {
					$tbl = $head['list_table'];
					$listObject->changeListType($tbl);
					$sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($tbl)." AS ".$tbl." 
										ON ".$tbl.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl.".status & ".CNTRCT::DELETED.") <> ".CNTRCT::DELETED;
					$sb = $tbl.".".implode(", ".$tbl.".",str_ireplace("|X|", $tbl, $listObject->getSortBy(true)));
					$sb = str_ireplace($tbl.".if", "if", $sb);
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
					$sb = $fld.".tkname, ".$fld.".tksurname";
				} else {
					if($fld=="action_progress") {
						$sb = "AVG(A.action_progress)";
					} else {
						$sb = $where_tblref.".".$fld;
					}
				}
				$sort_by[$fld] = $sb;
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where;		
			$sql.=" AND ".$myObject->getReportingStatusSQL($my_tblref);
			$sql.=" AND ".$dObject->getReportingStatusSQL($d_tblref);
			$sql.=" AND ".$cObject->getReportingStatusSQL($c_tblref);
			
			$s = array();
			foreach($this->titles as $fld => $t) {
				//echo "<P>".$fld;
				if( (!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld]===true) && isset($filters[$fld])) {
					$t = $this->types[$fld];
					$f = $filters[$fld];
					$ft = isset($filter_types[$fld]) ? $filter_types[$fld] : "";
					$a = "";
					switch($fld) {
						case "action_progress": 
							//do nothing - filtering applied in row processing
							break;
						case "result":
							//do nothing - filtering applied in row processing
							break;
						default:
							$a =  $this->report->getFilterSql($tblref,$t,$f,$ft,$fld);
							break;
					}
					if(strlen($a)>0) { $s[] = $a; }
				}
			}
			if(count($s)>0) {
				$sql.= " AND ".implode(" AND ",$s);
			}
			$sql.="GROUP BY ".$my_tblref.".".$myObject->getIDFieldName();
			$sql.=$this->getSortBySql($sort_by);
		return $sql;
	}
	

}










/*
class MODULE3_CNTRCT_REPORT_DELIVERABLE extends MODULE3_CNTRCT_REPORT {

	//CONSTRUCT
	private $date_format = "DATETIME";
	private $my_class_name = "MODULE3_CNTRCT_REPORT_DELIVERABLE";
	private $my_quick_class_name = "MODULE3_CNTRCT_QUICK_REPORT";
	const REFTAG = "CD";

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Contract Assist: Deliverable Report";
	private $multilist_results;
	
	private $backupdata = array();
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	/*
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {

		$headers = new DeliverableNaming();
		$this->titles['id'] = "Ref";
		$this->titles['financial_year'] = "Financial Year";
		$this->titles['legislation_id'] = "Legislation";
		$x = $headers->getHeaderList();
		unset($x['id']);
		$this->titles += $x;
		//$this->arrPrint($this->titles);
		//$this->titles['update_log'] = "Response";
		$this->titles['date_completed'] = "Action Completed Date";
		$this->titles['result'] = "Compliance Status By Action Progress";
		$this->titles['filter_legislation_type'] = "Legislation Source";
		$this->allowchoose = array(
			'compliance_date'=>false,
			'deliverable_import'=>false,
			'filter_legislation_type'=>false,
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id'=>false,
			'action_progress'		=> false,
			'date_completed'		=> false,
			'compliance_date'		=> false,
			'deliverable_import'	=> false,
			'update_log'			=> false,
		);
		$this->types = array(
			'financial_year'		=> "LIST",
			'legislation_id'		=> "LIST",
			'id' 					=> "REF",
			'action_progress'		=> "PERC",
			'result'				=> "RESULT",
			'date_completed'		=> "DATE",
			'description'			=> "TEXT",
			'short_description'		=> "TEXT",
			'legislation_section'	=> "TEXT",
			'compliance_frequency'	=> "LIST",
			'applicable_org_type'	=> "LIST",
			'compliance_date'		=> "",
			'responsible'			=> "LIST",
			'functional_service'	=> "LIST",
			'accountable_person'	=> "MULTILIST",
			'responsibility_owner'	=> "LIST",
			'legislation_deadline'	=> "DATE",
			'sanction'				=> "TEXT",
			'reference_link'		=> "TEXT",
			'assurance'				=> "TEXT",
			'compliance_to'			=> "MULTILIST",
			'main_event'			=> "LIST",
			'sub_event'				=> "MULTILIST",
			'guidance'				=> "TEXT",
			'deliverable_import'	=> "",
			'reporting_category'	=> "LIST",
			'deliverable_status'	=> "LIST",
			'filter_legislation_type'=>"SMLLIST",
			'update_log'			=> "LOG",
		);
		
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array(
			'id'					=> false,
			'action_progress'		=> false,
			'date_completed'		=> false,
			'description'			=> false,
			'short_description'		=> false,
			'legislation_section'	=> false,
			'sanction'				=> false,
			'reference_link'		=> false,
			'assurance'				=> false,
			'guidance'				=> false,
			'deliverable_import'	=> false,
			'filter_legislation_type'=>false,
			'update_log'			=> false,
		);
		$this->allowsortby = array(
			'financial_year'		=> false,
			'action_progress'		=> false,
			'date_completed'		=> false,
			'result'				=> false,
			'compliance_frequency'	=> false,
			'applicable_org_type'	=> false,
			'compliance_date'		=> false,
			'responsible'			=> false,
			'functional_service'	=> false,
			'accountable_person'	=> false,
			'responsibility_owner'	=> false,
			'compliance_to'			=> false,
			'main_event'			=> false,
			'sub_event'				=> false,
			'deliverable_import'	=> false,
			'filter_legislation_type'=>false,
			'update_log'			=> false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'short_description'		=> 1, 
			'description'			=> 2,
			'legislation_section'	=> 3,
			'id' 					=> 4,
			'compliance_frequency'	=> 5,
			'applicable_org_type'	=> 6,
			'responsible'			=> 7,
			'functional_service'	=> 8,
			'accountable_person'	=> 9,
			'responsibility_owner'	=> 10,
			'legislation_deadline'	=> 11,
			'sanction'				=> 12,
			'reference_link'		=> 13,
			'assurance'				=> 14,
			'compliance_to'			=> 15,
			'main_event'			=> 16,
			'sub_event'				=> 17,
			'guidance'				=> 18,
			'reporting_category'	=> 19,
			'action_progress'		=> 20,
			'deliverable_status'	=> 21,
			'result'				=> 22,
			'compliance_date'		=> 99,
			'deliverable_import'	=> 99,
		);
	
	}
	
	protected function getFieldData() {
		$this->data = array(
			'financial_year'		=> $this->getFinancialYears(),
			'legislation_id'		=> $this->getLegislations(),
			'compliance_frequency'	=> $this->getComplianceFrequency(),
			'applicable_org_type'	=> $this->getOrgType(),
			'responsible'			=> $this->getResponsibleDept(),
			'functional_service'	=> $this->getFunctionalService(),
			'accountable_person'	=> $this->getAccountablePerson(),
			'responsibility_owner'	=> $this->getResponsiblePerson(),
			'compliance_to'			=> $this->getComplianceTo(),
			'main_event'			=> $this->getMainEvent(),
			'sub_event'				=> $this->getSubEvent(),
			'reporting_category'	=> $this->getReportingCategory(),
			'deliverable_status'	=> $this->getDelStatus(),
			'result'				=> $this->getResultOptions(),
			'filter_legislation_type'=> array('ALL'=>"All Legislations",'CLIENT'=>"Client Created Legislations",'MASTER'=>"Imported Master Legislations"),
		);
		$this->default_data = array('filter_legislation_type'=>"ALL");
		//$this->arrPrint($this->data);
	}


	private function getComplianceFrequency() {
		$data = array();
		$me = new ComplianceFrequency();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}
	private function getOrgType() {
		$data = array();
		$me = new OrganisationTypesManager();
		$d2 = $me->getAll();
		$data = $this->listSort($d2);
		return $data;
	}
	private function getResponsibleDept() {
		$data = array();
		$me = new ClientDepartment();
		$d2 = $me->getAll();
		$data = $this->listSort($d2);
		return $data;
	}
	private function getFunctionalService() {
		$data = array();
		$me = new FunctionalServiceManager();
		$d2 = $me->getClientFunctionalServices();
		$d = array();
		foreach($d2 as $i => $e) {
			$d[$i] = array('id'=>$i,'name'=>$e);
		}
		$data = $this->listSort($d);
		return $data;
	}
	private function getAccountablePerson() {
		$data = array();
		$me = new AccountablePersonManager();
		$d2 = $me->getAll();
		//$this->arrPrint($d2);
		$users = array();
		$master = array();
		foreach($d2 as $i => $e) {
			if($e['title']!=$e['user']) {
				$users[$i] = array(		'id'=>$i,	'name'=>$e['role'],		);
			} else {
				$master[$i] = array(	'id'=>$i,	'name'=>$e['title'],	);
			}
		}
		$data = $this->listSort($users)+$this->listSort($master);
		//$this->arrPrint($data);
		return $data;
	}
	private function getResponsiblePerson() {
		$data = array();
		$me = new User();
		$d2 = $me->getAll();
		$d = array();
		foreach($d2 as $i => $e) { 
			$d[$i] = array(		'id'=>$i,	'name'=>$e['user'],		);
		}
		$data = $this->listSort($d);
		
		
		return $data;
	}
	private function getComplianceTo() {
		$data = array();
		$me = new ComplianceToManager();
		$d2 = $me->getAll();
		$backup = $me->getComplianceToTitles();
		foreach($backup as $key=>$b) {
			$backup[$key] = $b['name'];
		}
		$this->backupdata['compliance_to'] = $backup;
		$users = array();
		$master = array();
		foreach($d2 as $i => $e) {
			if($e['title']!=$e['user']) {
				$users[$i] = array(		'id'=>$i,	'name'=>$e['user']." (".$e['title'].")",		);
			} else {
				$master[$i] = array(	'id'=>$i,	'name'=>$e['title'],	);
			}
		}
		$data = $this->listSort($users)+$this->listSort($master);
		return $data;
	}
	private function getMainEvent() {
		$data = array();
		$me = new Event();
		$d2 = $me->getAll();
		$data = $this->listSort($d2);
		return $data;
	}
	private function getSubEvent() {
		$data = array();
		$me = new SubEvent();
		$d2 = $me->getAll();
		foreach($d2 as $e) { 
			$d[] = array(
				'id'=>$e['id'],
				'name'=>($e['id']==1 ? "" : $e['name']." (".$e['event'].")")
			);
		}
		//$this->arrPrint($d2);
		$data = $this->listSort($d);
		return $data;
	}
	public function getReportingCategory() {
		$data = array();
		$me = new ReportingCategories();
		$d2 = $me->getReportingCategories();
		$data = array(0=>"[Unspecified]")+$this->listSort($d2);
		
		return $data;
	}
	
	private function getDelStatus() {
		$data = array();
		$me = new Deliverablestatus();
		$d = $me->getAll();
		foreach($d as $e) {
			$data[$e['id']] = $e['name'];
		}
		return $data;
	}
	
	public function getDeliverableStatusOptions() {
		$data = array();
		$me = new Deliverablestatus();
		$d = $me->getAll();
		foreach($d as $e) {
			$data[$e['id']] = array(
				'code'=>$e['id'],
				'color'=>"#".$e['color'],
				'text'=>strlen($e['client_terminology'])>0 ? $e['client_terminology'] : $e['name'],
			);
		}
		if(strlen($data[0]['color'])==1 || $data[1]['color']=="#FFFFFF") { $data[0]['color'] = "charcoal"; }
		if(strlen($data[1]['color'])==1 || $data[1]['color']=="#FFFFFF") { $data[1]['color'] = "red"; }
		if(strlen($data[2]['color'])==1 || $data[2]['color']=="#FFFFFF") { $data[2]['color'] = "orange"; }
		if(strlen($data[3]['color'])==1 || $data[3]['color']=="#FFFFFF") { $data[3]['color'] = "green"; }
		
		return $data;
	}


	
	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	/*protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName("deliverable");
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName("deliverable");
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,"id");
			//echo "<P>".implode(",",array_keys($rows))."</p>";
			if( (isset($_REQUEST['columns']['action_progress']) && strtoupper($_REQUEST['columns']['action_progress'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$action_progress = $this->getActionProgress($db,array_keys($rows));
			}
			if(isset($_REQUEST['columns']['update_log'])) {
				$response = $this->getResponses("deliverable","deliverable_id",$db,array_keys($rows));
			}
			foreach($rows as $key => $r) {
				$my_result = isset($action_progress[$key]) ?  ($action_progress[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
				$my_result['deliverable_status'] = $r['deliverable_status'];
				$r['action_progress'] = $my_result['action_progress'];
				$r['date_completed'] = ($my_result['action_progress']<100 ? "" : $my_result['date_completed']);
				$r['update_log'] = isset($response[$key]) ? $response[$key] : array();
				$mystatus = $this->activeDeliverable($r['status']);
				if(!$mystatus) {
					$r['result'] = "inactive";
				} else {
					$r['result'] = $this->getCompliance($my_result,$r['legislation_deadline']);
				}
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
								if($i=="deliverable_status" && !($mystatus)) {
									$d = "Inactive";
								} else {
									$d2 = $d;
									if(strlen($d)==0) {
										$d = "Unspecified";
									} else {
										$x = explode(",",$d);
										$d = "";
										$z = array();
										foreach($x as $a) {
											if(isset($this->data[$i][$a])) {
												$z[] = $this->data[$i][$a];
											} elseif(isset($this->backupdata[$i][$a])) {
												$z[] = $this->backupdata[$i][$a];
											} 
										}
										$d = count($z)>0 ? implode(", ",$z) : "Unspecified";//$d2;//"Unknown";
									}
									//$d.=" [".$d2."]";
								}
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
//							$final_rows[$r['id']][$i] = stripslashes($d);
							if($type!="LOG") {
								$final_rows[$r['id']][$i] = stripslashes($d);
							} else {
								$final_rows[$r['id']][$i] = $d;
							}
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d) {
		$result = "notCompletedAndOverdue";
		$deadline = strtotime($d);
		$p = $ap['action_progress'];
		$dc = strtotime($ap['date_completed']);
		if($ap['action_status']==0) {
			$result = "unableToComply";
		} elseif($p<100) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		return $result;
		
	}
	
	public function getActionProgress($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT U.*, A.deliverable_id 
			FROM ".$db->getDBRef()."_action_update U 
			INNER JOIN ".$db->getDBRef()."_action A 
				ON U.action_id = A.id 
			WHERE ".Action::getStatusSQLForWhere("A")." 
			AND A.deliverable_id IN (".implode(",",$objects).") 
			AND (
			  str_to_date(date_completed, '%d-%b-%Y') is null
			OR
			  str_to_date(date_completed, '%d-%b-%Y') = 0
			)
			AND A.progress > 0 
			ORDER BY A.deliverable_id ASC
			, U.action_id ASC
			, U.insertdate DESC";
			$rows = $db->mysql_fetch_all($sql); //echo $sql;
			$updates = array();
			foreach($rows as $r) {
				$a = $r['deliverable_id'];
				$c = unserialize(base64_decode($r['changes']));
				if(isset($c['progress'])) {
					if(isset($c['date_completed']['to']) && strlen($c['date_completed']['to'])>0 && strtotime($c['date_completed']['to'])>86401) {
						$dc = date("d F Y H:i:s",strtotime($c['date_completed']['to']));
					} else {
						$dc = date("d F Y H:i:s",strtotime($r['insertdate']));
					}
					if(!isset($updates[$a])) {
						$updates[$a] = $dc;
					} elseif(strtotime($updates[$a])<strtotime($dc)) {
						$updates[$a] = $dc;
					}
				}
			}

			$sql = "SELECT
			A.deliverable_id as deliverable
			, D.legislation_deadline
			, count(A.id) as count
			, sum(A.progress) as total
			, avg(A.progress) as average
			, max(STR_TO_DATE(A.date_completed,'%d-%b-%Y')) as last_date
			, min(A.status) as lowest_status
			FROM `".$db->getDBRef()."_action` A
			INNER JOIN ".$db->getDBRef()."_deliverable D
			ON A.deliverable_id = D.id
			WHERE A.deliverable_id IN (".implode(",",$objects).")
			group by A.deliverable_id";
			$res = $db->mysql_fetch_all_fld($sql,"deliverable");		
			
			foreach($res as $d => $rs) {
				$r = "N/A";
				if(isset($updates[$d])) { 
					$r = $updates[$d];
				}
				if($r!="N/A" && strtotime($r)>strtotime($rs['last_date'])) {
					$dc = strtotime($r);
				} else {
					$dc = strtotime($rs['last_date']);
				}
				$result[$d] = array(
					'action_progress'=>$rs['average'],
					'date_completed'=>date("d M Y",$dc),
					'action_status'=>$rs['lowest_status'],
				);
			}
			
			
		}
		return $result;
	}
	
	
/**************
SELECT D . * , GROUP_CONCAT( DISTINCT CD.complianceto_id
SEPARATOR  ',' ) AS compliance_to
FROM assist_kny0009_compl_deliverable D
LEFT OUTER JOIN  `assist_kny0009_compl_complianceto_deliverable` CD ON D.id = CD.deliverable_id
WHERE 1 
GROUP BY CD.deliverable_id
HAVING CONCAT(  ',', GROUP_CONCAT( DISTINCT CD.complianceto_id
SEPARATOR  ',' ) ,  ',' ) LIKE  '%,114,%'
OR CONCAT(  ',', GROUP_CONCAT( DISTINCT CD.complianceto_id
SEPARATOR  ',' ) ,  ',' ) LIKE  '%,112,%'
ORDER BY D.description
*****************/
	/*protected function setSQL($db,$filter=array()) {
		$sql = "";
		$sql = "
		SELECT D.*
		, GROUP_CONCAT( DISTINCT CD.complianceto_id SEPARATOR  ',' ) AS compliance_to 
		, GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) AS accountable_person 
		, GROUP_CONCAT( DISTINCT SE.subevent_id SEPARATOR  ',' ) AS sub_event 
		, L.financial_year
		, L.id as legislation_id
		FROM ".$db->getDBRef()."_deliverable D 
		INNER JOIN ".$db->getDBRef()."_legislation L
		ON L.id = D.legislation
		LEFT OUTER JOIN  `".$db->getDBRef()."_complianceto_deliverable` CD ON D.id = CD.deliverable_id
		LEFT OUTER JOIN  `".$db->getDBRef()."_accountableperson_deliverable` AD ON D.id = AD.deliverable_id
		LEFT OUTER JOIN  `".$db->getDBRef()."_subevent_deliverable` SE ON D.id = SE.deliverable_id
		WHERE ".Deliverable::getStatusSQLForWhere("D")." AND ".Legislation::getStatusSQLForWhere("L"); //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); }
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY D.id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;
						case "compliance_to":
							if(count($f)>0 && $f[0]!="X" &&  $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT CD.complianceto_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;
						case "sub_event":
							if(count($f)>0 && $f[0]!="X" &&  $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT SE.subevent_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
						case 'sub_event': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql("D",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;
						case 'legislation_id':
							$a = $this->report->getFilterSql("L",$t,$f,$ft,"id");
							break;
						case 'action_progress':	break;	//do nothing; filter applied in row processing
						case 'result':			break;	//do nothing; filter applied in row processing
						case 'filter_legislation_type':			break;	//do nothing; filter applied later
						default:				
							if($i=="financial_year") { $x = "L"; } else { $x = "D"; }
							$a = $this->report->getFilterSql($x,$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		$sql.=$this->sourceLegislation("L",$filters);
		$sql.= $sql_group;
		if(count($having)>0) {
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		$sql.=$this->getSortBySql();
		
		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				switch($s) {
					case "legislation_deadline":
						$sort_by[] = "STR_TO_DATE(legislation_deadline,'%d-%b-%Y')";
						break;
					case "legislation_id":
						$sort_by[] = "L.name";
						break;
					default:
						$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	
	protected function activeDeliverable($mystatus) {
		if( ( ($mystatus & Deliverable::ACTIVATED_BYSUBEVENT) == Deliverable::ACTIVATED_BYSUBEVENT) 
				||
			( ($mystatus & Deliverable::MANAGE_CREATED) == Deliverable::MANAGE_CREATED) 
				||
			( ($mystatus & Deliverable::ACTIVATED) == Deliverable::ACTIVATED) 
				||
			( ($mystatus & Deliverable::CONFIRMED) == Deliverable::CONFIRMED)
		) {
			return true;
		} 
		return false;
	
	}

}
*/
?>