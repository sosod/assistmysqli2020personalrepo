<?php

class MODULE3_CNTRCT_REPORT_CONTRACT extends MODULE3_CNTRCT_REPORT {

	//CONSTRUCT
	private $date_format = "DATETIME";
	private $my_class_name = "MODULE3_CNTRCT_REPORT_CONTRACT";
	private $my_quick_class_name = "MODULE3_CNTRCT_QUICK_REPORT";
	
	protected $me;
	protected $s=1;
	
	protected $object_type = "CONTRACT";
	protected $table_name = "contract";
	protected $id_field = "contract_id";
	protected $name_field = "contract_name";
	protected $reftag = "C";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;
	

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Contract Assist: Contract Report";
	
	
	


	public function __construct($p) {	
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		$this->me = new CNTRCT_CONTRACT();
		$this->object_type = $this->me->getMyObjectType();
		$this->table_name = $this->me->getTableName();
		$this->reftag = $this->me->getRefTag();
		$this->deadline_field = $this->me->getDeadlineFieldName();
		$this->date_completed_field = $this->me->getDateCompletedFieldName();
		$this->name_field = $this->me->getNameFieldName();
		
		if($p=="fixed") {
			$this->titles = array('financial_year'=>"Financial Year");
			$this->allowfilter['financial_year']=true;
		}
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {
		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array();
		$this->types = array(
			'result'				=> "RESULT",
		);
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array();
		$this->allowsortby = array(
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

		$listObject = new CNTRCT_LIST();
		
		$headObject = new CNTRCT_HEADINGS();
		$headings = $headObject->getReportObjectHeadings($this->object_type);
		$deliverable_status = $headings['deliverable_status'];
		unset($headings['deliverable_status']);
		//$this->arrPrint($headings);
		foreach($headings as $id => $head) {
			$fld = $head['field'];
			if($head['type']!="HEADING") {
				if($headObject->isListField($head['type'])) {
					$items = array();
					if($head['type']=="LIST") {
						$listObject->changeListType($head['list_table']);
						$items = $listObject->getItemsForReport();
					} elseif($head['type']=="MASTER") {
						$tbl = $head['list_table'];
						$masterObject = new CNTRCT_MASTER($fld);
						$items = $masterObject->getItemsForReport();
					} elseif($head['type']=="USER") {
						$userObject = new ASSIST_MODULE_USER();
						$items = $userObject->getItemsForReport($this->table_name, $fld);
					} elseif($head['type']=="OWNER") {
						$ownerObject = new CNTRCT_CONTRACT_OWNER();
						$items = $ownerObject->getItemsForReport();
						$tbl = $head['list_table'];
					}
					$this->data[$fld] = $items;
				}
				$this->processHeadings($headObject, $head, $headings);
			} elseif($head['field']=="contract_assess_status") {
				$fld = $head['field'];
				foreach($deliverable_status as $id=>$item) {
					$head['name'] = $item['name'];
					$head['field'] = $fld."_".$id;
					$head['type'] = "BOOL";
					$head['parent_id'] = $head['id'];
					$this->processHeadings($headObject, $head, $headings);
				}
			}
		}
		$this->titles['result'] = "Compliance Status On ".$this->titles['action_progress'];
	//$this->arrPrint($this->types);
	}
	
	private function processHeadings($headObject,$head,$headings) {
				$fld = $head['field'];
				$this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
				if($headObject->isListField($head['type'])) {
					$this->types[$fld] = "LIST";
				} elseif($headObject->isTextField($head['type'])) {
					$this->types[$fld] = "TEXT";
				} else {
					$this->types[$fld] = $head['type'];
				}
				$this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
				$this->allowchoose[$fld] = true;
				$this->sortposition[$fld] = $this->s; $this->s++;
				$this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
				if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
					$this->allowgroupby[$fld] = true;
				} else {
					$this->allowgroupby[$fld] = false;
				}
	}
	
	protected function getFieldData() {
		$this->data['result'] = $this->getResultOptions();
	}
		
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		$id_field = $this->me->getIDFieldName();
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,$id_field);
			foreach($rows as $key => $r) {
				$date_completed = $r[$this->date_completed_field];
				if($r['action_progress']==100 && strtotime($date_completed)==0) {
					$date_completed = $r[$this->action_date_completed_field];
					$r[$this->date_completed_field] = $date_completed;
				}
				$my_action_progress = array('action_progress'=>$r['action_progress'],'deadline'=>$r[$this->deadline_field],'date_completed'=>$date_completed);
				$r['result'] = $this->getCompliance($my_action_progress);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r[$id_field]] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = $this->getRefTag().$d;
								break;
							case "DATE":
								if(strtotime($d) != 0) {
									$d = date("d-M-Y",strtotime($d));
								} else {
									$d = "";
								}
								break;
							case "BOOL":
								if($d==="1") { $d = "Yes"; } elseif($d==="0") { $d = "No"; }
								break;
							case "LIST":
							case "TEXTLIST":
								$d2 = $d;
								$x = explode(",",$d);
								$d = "";
								$z = array();
								if($i=="status") {
									$z[] = (!isset($this->data[$i][$a])) ? $this->data[$i][1] : $this->data[$i][$a];	//default to new for unknown status
								} else {
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										}
									}
								}
								$d = count($z)>0 ? implode(", ",$z) : "Unspecified";
								break;
							case "PERC":
							case "PERCENTAGE":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "ATTACH":
								$d = "";
								break; 
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r[$id_field]][$i] = stripslashes($d);
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
/*	public function getCompliance($ap) { 
		$result = "notCompletedAndNotOverdue";
		
		$p = isset($ap['action_progress']) ? $ap['action_progress'] : 0;
		$d = isset($ap['deadline']) ? strtotime($ap['deadline']) : 0;
		$dc = isset($ap['date_completed']) ? strtotime(date("d F Y",strtotime($ap['date_completed']))) : 1;
		$now = strtotime(date("d F Y"));
		
		if($p==100) {
			if($dc < $d) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$d) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		} else {
			if($d < $now) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		}
		
		return $result;
		
	}*/
	
	
	protected function setSQL($db,$filter=array()) {
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$sql = "";
		$left_joins = array();
		
		$listObject = new CNTRCT_LIST("deliverable_status");
		$deliverable_status_items = $listObject->getAllListItems();
		
		$headObject = new CNTRCT_HEADINGS();
		
		$d_tblref = "D";
		$dObject = new CNTRCT_DELIVERABLE(); 
		$d_table = $dObject->getTableName();
		$d_id = $d_tblref.".".$dObject->getIDFieldName();
		$d_status = $d_tblref.".".$dObject->getStatusFieldName();
		$d_parent = $d_tblref.".".$dObject->getParentFieldName();

		$a_tblref = "A";
		$aObject = new CNTRCT_ACTION(); 
		$this->action_date_completed_field = $aObject->getDateCompletedFieldName();
		$a_table = $aObject->getTableName();
		$a_id = $a_tblref.".".$aObject->getIDFieldName();
		$a_status = $a_tblref.".".$aObject->getStatusFieldName();
		$a_parent = $a_tblref.".".$aObject->getParentFieldName();
		$a_progress = $a_tblref.".".$aObject->getProgressFieldName();
		$a_progress2 = $aObject->getProgressFieldName();
		
		//set up contract variables
		$c_headings = $headObject->getReportObjectHeadings($this->getMyObjectType()); 
		$cObject = $this->getMyObject(); 
		$c_tblref = "C";
		$tblref = $c_tblref;
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_status_fld = $c_tblref.".".$cObject->getProgressStatusFieldName();
		$c_name = $cObject->getNameFieldName();
		$c_deadline = $cObject->getDeadlineFieldName();
		$c_field = $c_tblref.".".$cObject->getTableField();
		
		$where = $cObject->getActiveStatusSQL($c_tblref);
		
			$sql = "SELECT DISTINCT $c_status as my_status
					, ".$c_tblref.".*
					, AVG(".$a_progress.") as ".$a_progress2."
					, MAX(".$a_tblref.".".$this->action_date_completed_field.") as ".$this->action_date_completed_field."
					, IF( ((".$c_tblref.".contract_assess_type & ".CNTRCT_CONTRACT::QUALITATIVE.") = ".CNTRCT_CONTRACT::QUALITATIVE."),1,0) as contract_assess_qual 
					, IF( ((".$c_tblref.".contract_assess_type & ".CNTRCT_CONTRACT::QUANTITATIVE.") = ".CNTRCT_CONTRACT::QUANTITATIVE."),1,0) as contract_assess_quan 
					, IF( ((".$c_tblref.".contract_assess_type & ".CNTRCT_CONTRACT::OTHER.") = ".CNTRCT_CONTRACT::OTHER."),1,0) as contract_assess_other 
					";
			foreach($deliverable_status_items as $ds) {
				$dsi = $ds['id'];
				$sql.= "
				, IF( ((".$c_tblref.".contract_assess_status & ".(pow(2,$dsi)).") = ".(pow(2,$dsi))."),1,0) as contract_assess_status_".$dsi."
				";
			}
			$d_headings = array();
			$a_headings = array();
			$from = " $c_table $c_tblref 
						INNER JOIN $d_table $d_tblref
						  ON $d_parent = $c_id AND ( $d_status & ".CNTRCT::ACTIVE." ) = ".CNTRCT::ACTIVE."
						INNER JOIN $a_table $a_tblref
			  			ON $a_parent = $d_id AND ( $a_status & ".CNTRCT::ACTIVE." ) = ".CNTRCT::ACTIVE."
			";
			$final_data['head'] = $c_headings;
			$id_fld = $cObject->getIDFieldName();
			$where_tblref = $c_tblref;
			$where_deadline = $c_deadline;
			$ref_tag = $cObject->getRefTag();
			$where_status_id_fld = $c_tblref.".".$cObject->getProgressStatusFieldName();
			$status_id_fld = $cObject->getProgressStatusFieldName();
			$where_status_fld = $c_tblref.".".$cObject->getStatusFieldName();
		
			$sort_by = array();
			$all_headings = array_merge($c_headings,$d_headings,$a_headings);

			$listObject = new CNTRCT_LIST();
			$deliverable_status = $all_headings['deliverable_status'];
			unset($all_headings['deliverable_status']);
			
			foreach($all_headings as $id => $head) {   
				$fld = $head['field'];
				$lj_tblref = substr($head['section'],0,1);
				if($head['type']=="LIST") {
					$tbl = $head['list_table'];
					$listObject->changeListType($tbl);
					$sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($tbl)." AS ".$tbl." 
										ON ".$tbl.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl.".status & ".CNTRCT::DELETED.") <> ".CNTRCT::DELETED;
					$sb = $tbl.".".implode(", ".$tbl.".",str_ireplace("|X|", $tbl, $listObject->getSortBy(true)));
					$sb = str_ireplace($tbl.".if", "if", $sb);
				} elseif($head['type']=="MASTER") {
					$tbl = $head['list_table'];
					$masterObject = new CNTRCT_MASTER($fld);
					$sb = $fld.".".implode(", ".$fld.".",$masterObject->getSortBy());
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
					$sb = $fld.".tkname, ".$fld.".tksurname";
				} elseif($head['type']=="OWNER") {
					$ownerObject = new CNTRCT_CONTRACT_OWNER();
					$tbl = $head['list_table'];
					$dir_tbl = $fld."_parent";
					$sub_tbl = $fld."_child";
					$own_tbl = $fld;
					$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
					$sb = "".$dir_tbl.".dirsort, ".$sub_tbl.".subsort";
					$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$lj_tblref.".".$fld." = ".$sub_tbl.".subid";
					$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
					$left_joins[] = "LEFT OUTER JOIN ".$db->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
				} else {
					if($fld=="action_progress") {
						$sb = "AVG(A.action_progress)";
					} else {
						$sb = $where_tblref.".".$fld;
					}
				}
				$sort_by[$fld] = $sb;
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where;		
			$sql.=" AND ".$cObject->getReportingStatusSQL($c_tblref);
			
			$s = array();
			if(count($this->titles)>0) {
				foreach($this->titles as $fld => $t) {
					//echo "<P>".$fld;
					if( (!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld]===true) && isset($filters[$fld])) {
						$t = $this->types[$fld];
						$f = $filters[$fld];
						$ft = isset($this->filter_types[$fld]) ? $this->filter_types[$fld] : "";
						$a = "";
						switch($fld) {
							case "action_progress": 
								//do nothing - filtering applied in row processing
								break;
							case "result":
								//do nothing - filtering applied in row processing
								break;
							default:
								$a =  $this->report->getFilterSql($tblref,$t,$f,$ft,$fld);
								break;
						}
						if(strlen($a)>0) { $s[] = $a; }
					}
				}
			}
			if(count($s)>0) {
				$sql.= " AND ".implode(" AND ",$s);
			}
			$sql.="GROUP BY ".$c_tblref.".".$cObject->getIDFieldName();
			$sql.=$this->getSortBySql($sort_by);
		return $sql;
	}
	

}

?>