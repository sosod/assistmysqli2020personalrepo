<?php

class MODULE3_TK_REPORT_USER extends MODULE3_TK_REPORT {
	private $start_time_of_script_running; //used to measure time taken for script to run for debugging
	//CONSTRUCT
	private $date_format = "DATETIME";
	private $my_class_name = "MODULE3_TK_REPORT_USER";
	private $my_quick_class_name = "MODULE3_TK_QUICK_REPORT";

	protected $me;
	protected $s = 1;

	protected $object_type = "USER";

	protected $id_field = "tkid";
	protected $name_field = "tkname";
	protected $reftag = "TK";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;

	protected $headings;

	protected $result_categories = array();

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "User Report";


	public function __construct($p) {
		parent::__construct($p, $this->date_format, $this->my_class_name, $this->my_quick_class_name);
		$this->start_time_of_script_running = time();
		$this->folder = "report";
		$this->me = new TK_REPORT();
		$this->object_type = "USER";

		$this->reftag = "";
		$this->deadline_field = false;//$this->me->getDeadlineFieldName();
		$this->date_completed_field = false;
		$this->name_field = "tkname";
	}


	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/

	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {
		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array('module_access' => false,
								   'tkuser'        => false,
								   'tklogindate'   => false,
								   'tkadddate'     => false,
								   'emp_date'      => false,
								   'tktitle'       => false,
								   'tkidnum'       => false,
								   'tkid'       => false,
			);
		$this->types = array();
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array('module_access' => false,
									'tkuser'        => false,
									'tklogindate'   => false,
									'tkadddate'     => false,
									'emp_date'      => false,
									'tktitle'       => false,
									'tkidnum'       => false,
									'tkname'        => false,
									'tksurname'     => false,
									'tktel'         => false,
									'tkfax'         => false,
									'tkmobile'      => false,
									'tkemail'       => false,
									'emp_num'       => false,
									'emp_date'      => false,
									'tklogin'       => false,
									'tklogin_days'  => false,
									'tkid'  => false,
			);
		$this->allowsortby = array('module_access' => false,
								   'tkuser'        => false,
								   'tklogindate'   => false,
								   'tkadddate'     => false,
								   'emp_date'      => false,
								   'tktitle'       => false,
								   'tkidnum'       => false,
								   'tktel'         => false,
								   'tkfax'         => false,
								   'tkmobile'      => false,
								   'tkemail'       => false,
								   'tklogin_days'  => false,
								   'tkid'  => false,
			);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

		//hard code getting headings using fixed mod refs
		$headings = $this->me->getHeadings();
		$this->headings = $headings;
		foreach($headings as $fld => $head) {
			if($head['type'] != "HEAD") {
				if($this->me->isListField($head['type'])) {
					$items = array();
					if($head['type'] == "LIST" || $head['type'] == "MULTILIST") {
						$items = $this->me->getListItemsForReport($head['table']);
					} elseif($head['type'] == "MASTER") {
						$items = $this->me->getMasterItemsForReport($head['table']);
					} elseif($head['type'] == "USER_STATUS") {
						$items = $this->me->getUserStatusOptionsFormattedForSelect();
					} elseif($head['type'] == "EMP_STATUS") {
						$items = $this->me->getEmployeeStatusOptionsFormattedForSelect();
					}
					$this->data[$fld] = $items;
				}
				$this->processHeadings($head, $headings);
			}
		}
	}

	private function processHeadings($head, $headings) {
		$fld = $head['field'];
		$this->titles[$fld] = (isset($head['client']) && strlen($head['client']) > 0 ? $head['client'] : $head['default']);
		if($this->me->isListField($head['type'])) {
			$this->types[$fld] = "LIST";
		} elseif($this->me->isTextField($head['type'])) {
			$this->types[$fld] = "TEXT";
		} else {
			$this->types[$fld] = $head['type'];
		}
		if(!isset($this->allowfilter[$fld])) {
			$this->allowfilter[$fld] = !in_array($head['type'], $this->bad_filter_types);
		}
		if(!isset($this->allowchoose[$fld])) {
			$this->allowchoose[$fld] = true;
		}
		$this->sortposition[$fld] = $this->s;
		$this->s++;
		if(!isset($this->allowsortby[$fld])) {
			$this->allowsortby[$fld] = (!in_array($head['type'], $this->bad_sort_types));
		}
		if(!isset($this->allowgroupby[$fld])) {
			if(!in_array($head['type'], $this->bad_graph_types) && (!in_array($head['type'], $this->bad_sort_types))) {
				$this->allowgroupby[$fld] = true;
			} else {
				$this->allowgroupby[$fld] = false;
			}
		}
	}

	protected function getFieldData() {
		//empty - handled by getFieldDetails & processHeadings
	}


	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
//echo "<hr /><h1 class='green'>START OF SCRIPT @ ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";


		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by']) > 0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		//test - do we need to save data to file to free up memory or is it small enough to handle as is? [JC] #AA-380
		if(count($rows) > $this->max_objects_to_handle) {
			$this->save_data_to_file_to_free_memory = true;
		}


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";


//save Rows data to text file to clear memory
		if($this->save_data_to_file_to_free_memory === true) {
			$rows_data_file_name = "../files/temp/".$this->me->getCmpCode()."_".$this->me->getModRef()."_".$this->me->getUserID()."_report_objects_".date('Ymd_His').".txt";
			$fh = fopen($rows_data_file_name, 'ab');
			foreach($rows as $r_key => $row) {
				fwrite($fh, serialize($row).chr(10).chr(10));
			}
			fclose($fh);
			unset($rows);
		} else {
			$rows_data_file_name = "";
		}


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />Finished prepping for getting results => save data to file = :".$this->save_data_to_file_to_free_memory.":");


		//get rows data back from file if needed
		//remember that double blank line was used to save the data in case of unexpected line breaks in the data from the data so check for blank line
		if($this->save_data_to_file_to_free_memory) {
			// [JC] AA-380
			$fh = fopen($rows_data_file_name, 'rb');
			$rows = array();
			$save_line = "";
			while($line = fgets($fh)) {
				//if line is not blank then add it to any previous line - using 2 to catch odd eol characters
				if(strlen($line) > 2) {
					$save_line .= $line;
				} elseif(strlen($save_line) > 0) {
					//otherwise process the previous line and then start from scratch with current line
					$r = unserialize($save_line);
					$obj_id = $r['raw_id'];
					$rows[$obj_id] = $r;
					$save_line = "";
				} else {
					//do nothing to extra blank lines - it means the user was an idiot and submitted multiple line breaks in a text box which isn't needed for display in the report and just takes up space/memory
//					echo "<hr />How did I end up here? ROWS :".$line.":";
				}
			}
			fclose($fh);
			unset($save_line);
			unset($line);
			unset($r);
		} //endif save_data_to_file = true


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />fetched data from files ");


		/*****************************************************
		 * FINAL PROCESSING AND SEND TO OUTPUT
		 */
		$report_title = isset($_REQUEST['rhead']) && strlen($_REQUEST['rhead']) > 0 ? $_REQUEST['rhead'] : $this->default_report_title;
		$this->report->setReportTitle($report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
		$this->report->setRows($rows);
		unset($rows); //free up memory

		//allocate any items not in the "group" list to unspecified group
		if(!isset($this->group_rows['X'])) {
			$this->group_rows['X'] = array();
		}

		foreach($this->group_rows as $group_key => $row_keys) {
			if($group_key!="X") {
				if($group_key == "" || is_null($group_key) || $group_key == "0" || $group_key == 0) {
					$this->group_rows['X'] = $this->group_rows['X'] + $row_keys;
					unset($this->group_rows[$group_key]);
				} elseif(!isset($this->groups[$group_key])) {
					$this->group_rows['X'] = array_merge($this->group_rows['X'], $row_keys);
					unset($this->group_rows[$group_key]);
				}
			}
		}



		foreach($this->groups as $key => $g) {
			$this->report->setGroup($key, $g['text'], isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings($this->result_fields['bottom']);
	}


	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}


	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby != "X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key, $t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X", "Unknown Group ".$this->getUnspecified());
	}

	private function blankGroup($id, $t) {
		return array('id' => $id, 'text' => stripslashes($t), 'rows' => array());
	}

	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby == "X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",", $r[$groupby]);
			if(count($g) == 0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $group_key) {
					if($group_key == "" || is_null($group_key) || $group_key == "0" || $group_key == 0 || !isset($this->groups[$group_key])) {
						$group_key = "X";
					}
					$this->group_rows[$group_key][] = $i;
				}
			}
		}
	}


	private function cleanUpSDBP6Text($text) {
		if(!is_array($text)) {
			$text = str_replace('\r\n', '', $text);
			$text = str_replace('\n', '', $text);
			$text = str_replace('\r', '', $text);
			$text = str_replace(chr(10), '', $text);
		}
		return $text;
	}

	private function getModuleAccessPerUser($users = array()) {
		$sql = "SELECT DISTINCT usrtkid, modref, modtext 
				FROM assist_".$this->getCmpCode()."_menu_modules_users mmu
				INNER JOIN assist_menu_modules mm
				ON mmu.usrmodref = mm.modref
				AND mm.modyn = 'Y'
				WHERE mmu.usrtkid IN ('".implode("','", $users)."')
				ORDER BY usrtkid, modtext,modref";
		$rows = $this->me->mysql_fetch_all($sql);
		$data = array();
		$bullet = "&bull;&nbsp;";
		if($_REQUEST['output'] == "csv") {
			$bullet = "";
		}
		foreach($rows as $row) {
			$data[$row['usrtkid']][] = $bullet.$row['modtext'];
		}
		return $data;
	}

	private function getCountOfLoginsInPast30Days($users = array()) {
		if(count($users) > 0) {
			$sql = "SELECT tkid, count(id) as c 
					FROM ".$this->table_name."_activity 
					WHERE date > DATE_SUB(NOW(), INTERVAL 30 DAY)
					AND action = 'IN'
					AND tkid IN ('".implode("','", $users)."')
					GROUP BY tkid";
			$counts = $this->me->mysql_fetch_value_by_id($sql, "tkid", "c");
			return $counts;
		} else {
			return array();
		}
	}


	private function getRows() {
		$final_rows = array();
		$sql = $this->setSQL(false, array());
		$id_field = "tkid";
		if(strlen($sql) > 0) {
			$rows = $this->db->mysql_fetch_all_fld($sql, $id_field);

			/***********************************
			 * ADDITIONAL DATA REQUIRED HERE
			 */
			if(count($rows) > 0) {
				$users_to_check = array_keys($rows);
				if(isset($_REQUEST['columns']['module_access']) && $_REQUEST['columns']['module_access'] == "on") {
					$module_access = $this->getModuleAccessPerUser($users_to_check);
				}
				if(isset($_REQUEST['columns']['tklogin_days']) && $_REQUEST['columns']['tklogin_days'] == "on") {
					$login_past_30_days = $this->getCountOfLoginsInPast30Days($users_to_check);
				}
			}
			/***************************************************************
			 *** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - START ***
			 ***************************************************************/ //            $obj_type = 'DEPTKPI';
//            $section = 'MANAGE';
//            $headObject = new SDBP6_HEADINGS();
//            $idp_headings = $headObject->getListPageHeadings($obj_type, $section);
//            $all_headings = array_merge($idp_headings);
//            $idpObject = $this->me;
			$all_headings = $this->headings;
			//REPLACE LIST, SEGMENT, OBJECT values with names
			$all_my_lists = array();
			//get replacement values
			foreach($all_headings as $fld => $head) {
				switch($head['type']) {
					case "MULTILIST":
					case "LIST":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$all_my_lists[$fld] = $this->me->getListItemsForReport($head['table']);
						}
						if(!isset($all_my_lists[$fld][0])) {
							$all_my_lists[$fld][0] = $this->me->getUnspecified();
						}
						break;
					case "MASTER":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$all_my_lists[$fld] = $this->me->getMasterItemsForReport($head['table']);
						}
						break;
				} //end switch by type
			}//end foreach heading
			//IAS terminated non-active user setting
			$this->data['tkstatus'][4] = $this->data['tkstatus'][2];
			/***************************************************************
			 **** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - END ****
			 ***************************************************************/
			//now loop through rows and replace values as needed

			foreach($rows as $key => $r) {
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0] == "X" || in_array($r['result'], $_REQUEST['filter']['result']))) {
					$final_rows[$r[$id_field]] = $r;
					foreach($this->titles as $i => $t) {
						if($i == "module_access") {
							$r[$i] = isset($module_access[$key]) ? $module_access[$key] : array();
							$rows[$key][$i] = $r[$i];
							$final_rows[$key][$i] = $r[$i];
						} elseif($i == "tklogin_days") {
							$r[$i] = isset($login_past_30_days[$key]) ? $login_past_30_days[$key] : 0;
							$rows[$key][$i] = $r[$i];
							$final_rows[$key][$i] = $r[$i];
						}
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
								case "REF":
									$d = $this->getRefTag().$d;
									break;
								case "DATE":
									if(is_numeric($d) || strpos($d, "-") == false) {
										$d = date("Y-m-d H:i:s", $d * 1);
									}
									if(strtotime($d) != 0) {
										$d = date("d-M-Y", strtotime($d));
									} else {
										$d = "";
									}
									break;
								case "DATETIME":
									if(is_numeric($d) || strpos($d, "-") == false) {
										$d = date("Y-m-d H:i:s", $d * 1);
									}
									if(strtotime($d) != 0) {
										$d = date("d-M-Y H:i:s", strtotime($d));
									} else {
										$d = "";
									}
									$rows[$key][$i] = $d;
									break;
								case "BOOL":
									if($d === "1") {
										$d = "Yes";
									} elseif($d === "0") {
										$d = "No";
									}
									break;
								case "LIST":
								case "MASTER":
								case "TEXTLIST":
									if($all_headings[$i]['type'] == "EMP_STATUS") {
										if(($d & EMP1::ACTIVE) == EMP1::ACTIVE) {
											$d = $this->data['emp_status'][EMP1::ACTIVE];
										} elseif(($d & EMP1::INACTIVE) == EMP1::INACTIVE) {
											$d = $this->data['emp_status'][EMP1::INACTIVE];
										} else {
											$d = $this->getUnspecified();
										}
									} elseif($i == "tktitle") {
										$d = $d;
									} else {
										$d2 = $d;
										$x = explode(",", $d);
										$d = "";
										$z = array();
										if($i == "status") {
											$z[] = (!isset($this->data[$i][$d2])) ? $this->data[$i][1] : $this->data[$i][$d2];    //default to new for unknown status
										} else {
											foreach($x as $a) {
												if(isset($this->data[$i][$a])) {
													$z[] = $this->data[$i][$a];
												}
											}
										}
										$d = count($z) > 0 ? implode(", ", $z) : $this->getUnspecified();
									}
									break;
								case "PERC":
								case "PERCENTAGE":
									$d = number_format($d, 2)."%";
									break;
								case "LINK":
									$d = "<a href=".$d.">".$d."</a>";
									break;
								case "ATTACH":
									if(strlen($d) > 0) {
										$f = unserialize($d);
										$d = "";
										if(isset($f) && is_array($f) && count($f) > 0) {
											foreach($f as $key => $val) {
												$d .= "+".$val['original_filename']."\n";
											}
										}
									} else {
										$d = "";
									}
									break;
								case "TEXT":
								default:
									$d = $d;
									break;
							}
							if(is_array($d)) {
								$final_rows[$r[$id_field]][$i] = $this->cleanUpSDBP6Text(($d));
							} else {
								$final_rows[$r[$id_field]][$i] = $this->cleanUpSDBP6Text(stripslashes($d));
							}
						}
					}
					$this->allocateToAGroup($r);
				}

				/***************************************************************
				 *** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - START ***
				 ***************************************************************/
				$row = $r;
				foreach($row as $fld => $x) {
					//only treat as list if previously handled in check above && only act on field if required for list page i.e. set in headings list
					if(isset($all_my_lists[$fld]) && isset($all_headings[$fld])) {
						$head = $all_headings[$fld];
						//store display data in table element to preserve raw data in original fld element
						//formatting function (below) looks for display data in table element for all list heading types (set in _HEADINGS->list_heading_types)
						$save_field = $head['table'];
						//if heading type contains MULTI - assume it is multi LIST SEGMENT or OBJECT
						if(strpos($head['type'], "MULTI") !== false) {
							if(strpos($head['type'], "OBJECT") !== false) {
								$blank_value = "";
							} else {
								$blank_value = $this->me->getUnspecified();
							}
							$x = explode(";", $x);
							$x = $this->me->removeBlanksFromArray($x);
							if(count($x) > 0) {
								$rows[$key][$save_field] = array();
								foreach($x as $y) {
									if(isset($all_my_lists[$fld][$y])) {
										$rows[$key][$save_field][] = $all_my_lists[$fld][$y];
									}
								}
								if(count($rows[$key][$save_field]) > 0) {
									$rows[$key][$save_field] = implode("; ", $rows[$key][$save_field]);
								} else {
									$rows[$key][$save_field] = $blank_value;
								}
							} else {
								$rows[$key][$save_field] = $blank_value;
							}
						} else {
							if($head['type'] == "OBJECT") {
								$blank_value = "";
							} else {
								$blank_value = $this->me->getUnspecified();
							}
							if(isset($all_my_lists[$fld][$x])) {
								$rows[$key][$save_field] = $all_my_lists[$fld][$x];
							} else {
								$rows[$key][$save_field] = $blank_value;
							}
						}//end if multi
					}//end if all_my_lists[fld] isset
				}//end foreach fld

				/***************************************************************
				 **** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - END ****
				 ***************************************************************/
			}
			/************************************************************************
			 *** ADDITIONAL PROCESSING CODE FROM SDBP6 formatRowDisplay() - START ***
			 ************************************************************************/
			$id_fld = $this->me->getIDFieldName();
			$ref_tag = $this->me->getRefTag();
			$displayObject = new SDBP6_DISPLAY("emp", true);
			$final_data['head'] = $all_headings;

			foreach($rows as $r) {
				$row = array();
				$i = $r[$id_fld];
				$final_rows[$i]['raw_id'] = $i;
				foreach($final_data['head'] as $fld => $head) {
					if($head['type'] == "NUM") {
						$head['type'] = "TEXT";    //display # of logins as is without formatting
					}
					if($head['parent_id'] == 0) {
						if($this->me->isListField($head['type'])) {
							$final_rows[$i][$fld] = (isset($r[$head['table']]) ? $r[$head['table']] : 'N/A');
						} elseif($this->me->isDateField($fld)) {
							$field_data = $displayObject->getDataField("DATE", $r[$fld], array('include_time' => ($head['type'] == "DATETIME")));
							$final_rows[$i][$fld] = (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
						} else {
							$field_data = $displayObject->getDataField($head['type'], $r[$fld], array('right' => true, 'html' => true, 'reftag' => $ref_tag));
							$final_rows[$i][$fld] = (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
						}
					}
				}
			}
			/************************************************************************
			 **** ADDITIONAL PROCESSING CODE FROM SDBP6 formatRowDisplay() - END ****
			 ************************************************************************/
		}
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($this->group_rows);
//echo "<hr />";

		return $final_rows;
	}

	//Changed $db variable passing to reference to stop unnecessary  duplication of object & spare memory
	protected function setSQL($db, $filter = array(), $sdbip_id = false) {
		$filters = count($filter) > 0 ? $filter : $_REQUEST['filter'];
		$group_by = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by']) > 0 ? $_REQUEST['group_by'] : "X";

		$left_joins = array();

		//set up variables
		$all_headings = $this->headings;

		$sql = "SELECT USER.*, EMPLOYEE.*, JOB.* ";
		$from = $this->table_name." USER";
		$left_joins[] = "LEFT OUTER JOIN ".$this->employee_table_name." EMPLOYEE
				ON USER.tkid = EMPLOYEE.emp_tkid";
		$left_joins[] = "LEFT OUTER JOIN ".$this->job_table_name." JOB
				ON EMPLOYEE.emp_id = JOB.job_empid AND (JOB.job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE." AND (JOB.job_status & ".EMP1::JOB_CURRENT_CONTRACT.") = ".EMP1::JOB_CURRENT_CONTRACT;


		foreach($all_headings as $fld => $head) {
			$lj_tblref = $head['section'];
			if($head['type'] == "MASTER") {
				$tbl = $head['table'];
				$tbl_id = $fld."_".$tbl;
				switch($tbl) {
					case "ppltitle":
						$masterObject = new ASSIST_MASTER_PPLTITLE();
						break;
				}
				$fy = $masterObject->getFields();
				$sql .= ", ".$fld.".".(isset($fy['name']) ? $fy['name']['field'] : $fy['value']['field'])." as ".$tbl_id;
				$left_joins[] = " LEFT OUTER JOIN ".$masterObject->getTable()." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id']['field'];
				$sb = $tbl_id;
			} elseif($head['type'] == "LIST") {
				$tbl = $head['table'];
				$tbl_id = $fld."_".$tbl;
				$sql .= ", ".$tbl_id.".name as ".$tbl_id;
				$left_joins[] = "LEFT OUTER JOIN ".$this->employee_dbref."_list_".$tbl." AS ".$tbl_id."
										ON ".$tbl_id.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl_id.".status & ".EMP1::DELETED.") <> ".EMP1::DELETED;
				$sb = $tbl_id.".name";
			} else {
				$sb = $lj_tblref.".".$fld;
			}
			$sort_by[$fld] = $sb;
		}


		$sql .= " FROM ".$from." ".implode(" ", $left_joins);
		$sql .= " WHERE USER.tkstatus >=0 AND USER.tkuser <> 'support' AND USER.tkid <> '0000' ";    //placeholder to catch items that follow

		$s = array();
		if(count($this->titles) > 0) {
			foreach($this->titles as $fld => $t) {
				//echo "<P>".$fld;
				if((!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld] === true) && isset($filters[$fld]) && ($fld != 'result_setting')) {
					$t = $this->types[$fld];
					$f = $filters[$fld];
					$ft = isset($this->filter_types[$fld]) ? $this->filter_types[$fld] : "";
					$a = "";
					switch($fld) {
						case "emp_status":
							$e_filter = array();
							foreach($f as $k) {
								if($k!=="X") {
								if($k == 0) {
									$e_filter[] = "EMPLOYEE.emp_status IS NULL";
								} else {
									$emp_status_filter = ($k == EMP1::ACTIVE) ? EMP1::ACTIVE : EMP1::INACTIVE;
									$e_filter[] = "(EMPLOYEE.emp_status & ".($emp_status_filter).") = ".$emp_status_filter;
								}
							}
							}
							if(count($e_filter)>0) {
							$a = "(".implode(") OR (", $e_filter).")";
							}
							break;
						default:
							$a = $this->report->getFilterSql($all_headings[$fld]['section'], $t, $f, $ft, $fld);
							break;
					}
					if(strlen($a) > 0) {
						$s[] = $a;
					}
				}
			}
		}
		if(count($s) > 0) {
			$sql .= " AND ".implode(" AND ", $s);
		}

		$sql .= $this->getSortBySql($sort_by);

		return $sql;
	}


}

?>