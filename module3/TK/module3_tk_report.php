<?php

class MODULE3_TK_REPORT extends MODULE3_GENERIC_REPORT {

	protected $folder;
	//GET FIELD DETAILS
	protected $titles;
	protected $allowchoose;
	protected $default_selected;
	protected $allowfilter;
	protected $types;
	protected $default_data;
	protected $data;
	protected $allowgroupby;
	protected $allowsortby;
	protected $default_sort;
	protected $sortposition;
	protected $db;

	protected $result_field_time_periods;
	protected $result_field_time_periods_to_process;
	protected $all_result_time_periods;
	protected $result_field_value_types;
	protected $result_fields;
	protected $overall_result_fields;
	protected $result_fields_selector_title;

	protected $max_objects_to_handle = 10000;
	protected $save_data_to_file_to_free_memory = false;

	protected $table_name = "timekeep";
	protected $employee_table_name = "employee";
	protected $employee_dbref = "emp";
	protected $employee_modref = "emp";
	protected $job_table_name = "job";

	public function __construct($p, $d, $c, $q) {
		parent::__construct($p, $d, $c, $q);
		$this->db = new TK_REPORT();
		$this->table_name = "assist_".$this->db->getCmpCode()."_timekeep";
		$this->employee_dbref = "assist_".$this->db->getCmpCode()."_".$this->employee_modref;
		$this->employee_table_name = $this->employee_dbref."_employee";
		$this->job_table_name = $this->employee_dbref."_job";
	}


	public function getTableName() {
		return $this->table_name;
	}

	public function getRefTag() {
		return $this->reftag;
	}

	public function getMyObject() {
		return $this->me;
	}

	public function getMyObjectType() {
		return $this->object_type;
	}

	public function getIDField() {
		return $this->id_field;
	}

	public function getNameField() {
		return $this->name_field;
	}

	public function getDeadlineField() {
		return $this->deadline_field;
	}

	public function getDateCompletedField() {
		return $this->date_completed_field;
	}

	public function getActionDateCompletedField() {
		return $this->action_date_completed_field;
	}

	public function getOwnerField() {
		return $this->owner_field;
	}

	public function getSDBIPId() {
		return $this->current_sdbip_details['id'];
	}  //Added for #AA-381 [JC]

	public function getSDBIPName() {
		return $this->current_sdbip_details['name'];
	}  //Added for #AA-381 [JC]

	public function getSDBIPmSCOAVersion() {
		return $this->current_sdbip_details['mscoa_version'];
	}  //Added for #AA-381 [JC]

	protected function prepareGenerator() {
		$this->report->disableUpdateLogOption("AUDIT");
		if(isset($_REQUEST['quick_id'])) {
			//$db = new ASSIST_DB("client");
			$quick = $this->db->mysql_fetch_one("SELECT * FROM ".$this->table_name."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'], $quick['name'], $quick['description'], $quick['insertuser'], $quick['insertdate'], unserialize($quick['report']));
		}
		$this->getFieldDetails();
		$this->getFieldData();
		$this->displayPageHeading();
	}

	public function displayPageHeading() {
		//Navigation buttons & page title
		$page = $_REQUEST['page'];
		$this->db->drawPageTop($page);
	}

	public function setFolder($f) {
		$this->folder = $f;
	}

	protected function getSortBySql($sort_fields) {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$s = array();
		if(is_array($sort) && count($sort) > 0) {
			foreach($sort as $st) {
				if(isset($sort_fields[$st])) {
					$s[] = $sort_fields[$st];
				}
			}
		}
		return (count($s) > 0 ? " ORDER BY ".implode(", ", $s) : "");
	}


	public function getCompliance($ap) {
		$result = "notCompletedAndNotOverdue";

		$p = isset($ap['action_progress']) ? $ap['action_progress'] : 0;
		$d = isset($ap['deadline']) ? strtotime($ap['deadline']) : 0;
		$dc = isset($ap['date_completed']) ? strtotime(date("d F Y", strtotime($ap['date_completed']))) : 1;
		$now = strtotime(date("d F Y"));

		if($p == 100) {
			if($dc < $d) {
				$result = "completedBeforeDeadline";
			} elseif($dc > $d) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		} else {
			if($d < $now) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		}
		return $result;
	}

	public function getFinancialYears() {
		$data = array();
		$fld = "financial_year";
		$masterObject = new SDBP6_MASTER($fld);
		$data = $masterObject->getItemsForReport();
		return $data;
	}

	public function getLegislations() {
		$data = array();
		return $data;
	}

	public function getDeliverables() {
		$data = array();
		return $data;
	}


	protected function listSort($d) {
		$data = array();
		$d2 = array();
		foreach($d as $e) {
			if($e['id'] == 1 && (strtoupper($e['name']) == "UNSPECIFIED" || strlen($e['name']) == 0)) {
				$data[$e['id']] = "[Unspecified]";
			} else {
				$d2[$e['id']] = $e['name'];
			}
		}
		natcasesort($d2);
		foreach($d2 as $e => $f) {
			$data[$e] = $f;
		}
		return $data;
	}

	public function getResultOptions() {
		$data = array();
		foreach($this->result_categories as $key => $r) {
			$data[$key] = $r['text'];
		}
		return $data;
	}

	public function getResultSettings() {
		return $this->result_categories;
	}

	protected function setFields() {


		foreach($this->titles as $i => $t) {
			$allow_choose = isset($this->allowchoose[$i]) ? $this->allowchoose[$i] : $this->defaults['allowchoose'];
			$default_selected = isset($this->default_selected[$i]) ? $this->default_selected[$i] : $this->defaults['default_selected'];
			$allow_filter = isset($this->allowfilter[$i]) ? $this->allowfilter[$i] : $this->defaults['allowfilter'];
			$types = isset($this->types[$i]) ? $this->types[$i] : $this->defaults['type'];
			$data = isset($this->data[$i]) ? $this->data[$i] : $this->defaults['data'];
			$default_data = isset($this->default_data[$i]) ? $this->default_data[$i] : $this->defaults['default_data'];
			$allow_group_by = isset($this->allowgroupby[$i]) ? $this->allowgroupby[$i] : $this->defaults['allowgroupby'];
			$allow_sort_by = isset($this->allowsortby[$i]) ? $this->allowsortby[$i] : $this->defaults['allowsortby'];
			$sort_position = isset($this->sortposition[$i]) ? $this->sortposition[$i] : $this->default_sort;
			$this->report->addField($i, $t, $allow_choose, $default_selected, $allow_filter, $types, $data, $default_data, $allow_group_by, $allow_sort_by, $sort_position);
			$this->default_sort++;
		}


	}


	public function getSQL($db, $filter) {
		return $this->setSQL($db, $filter);
	}

	protected function sourceLegislation($l, $filter) {
		$sql = "";
		return $sql;
	}


	protected function getResponses($tbl, $id, $db, $objects) {
		$result = array();
		return $result;
	}

	public function getRegisterObjects() {
		$data = array();

		$register_Object = new SDBP6_REGISTER();
		$register_id = $register_Object->getIDFieldName();
		$register_name = $register_Object->getNameFieldName();

		$register_list = $register_Object->getMyList('REGISTER', 'ADMIN');
		$register_list_rows = $register_list['rows'];

		foreach($register_list_rows as $reg_id => $l) {
			$data[$reg_id] = $l[$register_name]['display'].' ['.$l[$register_id]['display'].']';
		}
		return $data;
	}

	public function getRiskObjects() {
		$data = array();

		$risk_Object = new SDBP6_RISK();
		$risk_id = $risk_Object->getIDFieldName();
		$risk_name = $risk_Object->getNameFieldName();

		$risk_list = $risk_Object->getMyList('RISK', 'ADMIN');
		$risk_list_rows = $risk_list['rows'];

		foreach($risk_list_rows as $rsk_id => $l) {
			$data[$rsk_id] = $l[$risk_name]['display'].' ['.$l[$risk_id]['display'].']';
		}
		return $data;
	}


}

?>