<?php

class MODULE3_SDBP6_REPORT_DEPTKPI extends MODULE3_SDBP6_REPORT {
	private $start_time_of_script_running; //used to measure time taken for script to run for debugging
	//CONSTRUCT
	private $date_format = "DATETIME";
	private $my_class_name = "MODULE3_SDBP6_REPORT_DEPTKPI";
	private $my_quick_class_name = "MODULE3_SDBP6_QUICK_REPORT";
	
	protected $me;
	protected $s=1;
	
	protected $object_type = "DEPTKPI";
	protected $table_name = "kpi";
	protected $id_field = "kpi_id";
	protected $name_field = "kpi_name";
	protected $reftag = "D";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;
	

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Departmental KPI Report";
	
	
	


	public function __construct($p) {	
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->start_time_of_script_running = time();
//		echo "<h1>Script started at ".date("H:i:s",$this->start_time_of_script_running)."</h1>";
		$this->folder = "report";
		$this->me = new SDBP6_DEPTKPI();
		$this->object_type = $this->me->getMyObjectType();
		$this->table_name = $this->me->getTableName();
		$this->reftag = $this->me->getRefTag();
		$this->deadline_field = false;//$this->me->getDeadlineFieldName();
		$this->date_completed_field = $this->me->getDateCompletedFieldName();
		$this->name_field = $this->me->getNameFieldName();
		$this->default_report_title = $this->me->replaceAllNames("|".$this->me->getMyObjectName()."|")." Report";
		if($p=="fixed") {
			$this->titles = array('financial_year'=>"Financial Year");
			$this->allowfilter['financial_year']=true;
		}
		$resultSetupObject = new SDBP6_SETUP_RESULTS();
		$temp_result_categories = $resultSetupObject->getResultOptions();
		$this->result_categories = array();
		foreach($temp_result_categories as $i => $rc) {
			$key = $rc['code'];
			$this->result_categories[$key] = array(
				'text'=>$rc['value'],
				'code'=>$rc['code'],
				'color'=>$rc['color']
			);
		}
//		ASSIST_HELPER::arrPrint($this->result_categories);
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {

		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array();
		$this->types = array(
			'result'				=> "RESULT",
		);
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array();
		$this->allowsortby = array(
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

		$listObject = new SDBP6_LIST();
		$listObject->setSDBIPID($this->sdbip_id);
		
		$headObject = new SDBP6_HEADINGS();
		$headings = $headObject->getReportObjectHeadings($this->object_type);
		foreach($headings as $id => $head) {
			$fld = $head['field'];
			if($head['type']!="HEADING") {

//			    echo '<pre style="font-size: 18px">';
//			    echo '<p>HEAD</p>';
//			    print_r($head);
//			    echo '</pre>';

				if($headObject->isListField($head['type'])) {
					$items = array();
					if($head['type']=="LIST" || $head['type']=="MULTILIST") {
						$listObject->changeListType($head['list_table']);
						$items = $listObject->getItemsForReport();
					} elseif($head['type']=="MASTER") {
						$tbl = $head['list_table'];
						$masterObject = new SDBP6_MASTER($fld);
						$items = $masterObject->getItemsForReport();
					} elseif($head['type']=="USER") {
						$userObject = new ASSIST_MODULE_USER();
						$items = $userObject->getItemsForReport($this->table_name, $fld);
//					} elseif($head['type']=="OWNER") {
//						$ownerObject = new SDBP6_CONTRACT_OWNER();
//						$items = $ownerObject->getItemsForReport();
//						$tbl = $head['list_table'];
					}elseif($head['type']=="OBJECT" || $head['type']=="MULTIOBJECT") {
                        if($fld == 'kpi_sub_id' || strpos($fld, '_sub_id') !== false){
                            $object = new SDBP6_SETUP_ORGSTRUCTURE();
                            $object->setSDBIPID($this->sdbip_id);
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_proj_id' || strpos($fld, '_proj_id') !== false){
                            $object = new SDBP6_PROJECT();
                            $object->setSDBIPID($this->sdbip_id);
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_top_id' || strpos($fld, '_top_id') !== false){
                            $object = new SDBP6_TOPKPI();
                            $object->setSDBIPID($this->sdbip_id);
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_unit_id' || strpos($fld, '_unit_id') !== false){
                            $object = new SDBP6_SETUP_TARGETTYPE();
                            $object->setSDBIPID($this->sdbip_id);
                            $items = $object->getAllListItemsFormattedForSelect();
                        }elseif($fld == 'kpi_calctype_id' || strpos($fld, '_calctype_id') !== false){
                            $object = new SDBP6_SETUP_CALCTYPE();
                            $object->setSDBIPID($this->sdbip_id);
                            $items = $object->getAllListItemsFormattedForSelect();
                        }
                    }elseif($head['type']=="SEGMENT" || $head['type']=="MULTISEGMENT"){
//                        $company_code = strtolower($_SESSION['cc']);
//                        $company_db_object = new ASSIST_MODULE_HELPER('client', $company_code);

                        $table_name = $head['list_table'];
                        $mscoa_version = $this->getSDBIPmSCOAVersion();	// #AA-381 [JC]
                        $segmentObject = new SDBP6_SEGMENTS($table_name);
                        $scoa_segment_items = $segmentObject->getAllListItemsFormattedForSelect(array('version'=>$mscoa_version)); //Added mscoa version for #AA-381 [JC]
                        /******
						 * REMOVED 26 March 2019 by JC - Replaced with correct call to SEGMENTS class
                         $segment_table_name = 'assist_' . $company_code . '_mscoa_segment_' . $table_name;
                        $sql = "SELECT id as id
                                , ref
                                , name as name
                                , description as description
                                , parent_id as parent
                                , has_child as has_children
                                , can_post as can_assign
                                , IF(status = 2,1,0) as active
                                FROM $segment_table_name S
                                WHERE  (( S.status & 8) <> 8 OR ( S.status & 2) = 2 OR ( S.status & 4) = 4)

                                ORDER BY parent_id, name";
                        $scoa_segment = $company_db_object->mysql_fetch_all_by_id($sql, 'id');

                        $scoa_segment_items = array();
                        foreach($scoa_segment as $key => $val){
                            $scoa_segment_items[$key] = $val['name'] . ' (' . $val['ref'] . ')';
                        }*/

                        $items = $scoa_segment_items;
                    }
					$this->data[$fld] = $items;
				}
				$this->processHeadings($headObject, $head, $headings);
			}
		}

//		echo '<pre style="font-size: 18px">';
//		echo '<p>TITLES</p>';
//		print_r($this->titles);
//		echo '</pre>';
//
//		echo '<pre style="font-size: 18px">';
//		echo '<p>DATA</p>';
//		print_r($this->data);
//		echo '</pre>';

        $this->result_field_value_types['value_as_captured'] = 'Value as captured';
        $this->result_field_value_types['year_to_date'] = 'Year to Date Values';
        $this->result_field_value_types['period_to_date'] = 'Period to Date Values';
        $this->result_field_value_types['quarterly_values'] = 'Group results by Quarter';

        $this->processResultFieldInformation($headObject);

        $this->allowchoose['original_total'] = true;
        $this->allowfilter['original_total'] = false;
        $this->allowgroupby['original_total'] = false;
        $this->allowsortby['original_total'] = false;

        $this->allowchoose['revised_total'] = true;
        $this->allowfilter['revised_total'] = false;
        $this->allowgroupby['revised_total'] = false;
        $this->allowsortby['revised_total'] = false;

        $this->allowchoose['actual_total'] = true;
        $this->allowfilter['actual_total'] = false;
        $this->allowgroupby['actual_total'] = false;
        $this->allowsortby['actual_total'] = false;

        //Top Level Orgstructure
        $field_name = 'top_sub_id';
        $title_text = 'Top Level Organisational Structure';
        $this->titles[$field_name] = $title_text;
        $this->types[$field_name] = "LIST";
        $this->allowchoose[$field_name] = false;
        $this->allowfilter[$field_name] = false;
        $this->allowgroupby[$field_name] = true;
        $this->allowsortby[$field_name] = true;
        $object = new SDBP6_SETUP_ORGSTRUCTURE();
        $items = $object->getAllListItemsFormattedForSelect("TOP");
        $this->data[$field_name] = $items;

        //Results Setting Filter
        $field_name = 'result_setting';
        $title_text = 'Results Setting';
        $this->titles[$field_name] = $title_text;
        $this->types[$field_name] = "LIST";
        $this->allowchoose[$field_name] = false;
        $this->allowfilter[$field_name] = true;
        $this->allowgroupby[$field_name] = false;
        $this->allowsortby[$field_name] = false;

        $resultObject = new SDBP6_SETUP_RESULTS();
        $result_settings = $resultObject->getCustomResultSettingsDirectlyFromDatabase();
        unset($this->result_categories);
        $this->result_categories = array();
        $items = array();
        $items['XX'] = 'All';
        foreach($result_settings as $i => $result){
            $index = $result['code'];
            $kpi_text = 'KPI ' . $result['value'];
            $this->result_categories[$index]['text'] = $kpi_text;
            $this->result_categories[$index]['code'] = $index;
            $this->result_categories[$index]['color'] = $result['color'];
            $this->result_categories[$index]['description'] = $result['glossary'];

            $items[$i] = $kpi_text;
        }
        $this->data[$field_name] = $items;

	//$this->arrPrint($this->types);
	}
	
	private function processHeadings($headObject,$head,$headings) {
				$fld = $head['field'];
				$this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
				if($headObject->isListField($head['type'])) {
					$this->types[$fld] = "LIST";
				} elseif($headObject->isTextField($head['type'])) {
					$this->types[$fld] = "TEXT";
				} else {
					$this->types[$fld] = $head['type'];
				}
				$this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
				$this->allowchoose[$fld] = true;
				$this->sortposition[$fld] = $this->s; $this->s++;
				$this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
				if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
					$this->allowgroupby[$fld] = true;
				} else {
					$this->allowgroupby[$fld] = false;
				}
	}
	
	protected function getFieldData() {
		$this->data['result'] = $this->getResultOptions();
	}

    private function processResultFieldInformation($headObject) {
        $this->setResultFieldColumnSelectionTitle($headObject);
        $this->setResultFieldHeadersThatDisplayUnderTheTimePeriodHeaders($headObject);

        $filter_by = $this->me->getFilterByOptions('MANAGE','VIEW');
		$this->all_result_time_periods = $filter_by['when'];
        $display_time_period_header = true;
        $processTimePeriod = true;
        if($this->displayResultFields()){
            $display_time_period_header = false;
            $processTimePeriod = false;
        }

        foreach($filter_by['when'] as $key => $time) {
            $this->addTimePeriodToResultFieldDateRangeSelectBoxArray($key, $time);

            // This all goes into a function called 'applyTimePeriodFilters($key, $time, $display_time_period_header)'
            if($this->displayResultFields() && $this->isTimePeriodToDisplayFrom($key)){
                $display_time_period_header = true;
            }

            if($display_time_period_header === true){
                $this->result_fields['top'][$key] = $time;
                $this->setTopNameByValueType($key);
            }

            if($this->displayResultFields() && $this->isTimePeriodToDisplayTo($key)){
                $display_time_period_header = false;
            }


            //Time Periods To Process By value_type and dates from & to
            if($this->displayResultFields()
                && ($_REQUEST['value_type'] == 'year_to_date' || $this->isTimePeriodToDisplayFrom($key))
                //&& !$this->isTimePeriodToDisplayTo($key)
            ){
                $processTimePeriod = true;
            }

            if($processTimePeriod === true){
                $this->result_field_time_periods_to_process[$key] = $time;
            }

            if($this->displayResultFields() && $this->isTimePeriodToDisplayTo($key)){
                $processTimePeriod = false;
            }
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>RESULTS TOP</p>';
//        print_r($this->result_fields['top']);
//        echo '</pre>';
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>RESULT TIME FIELDS TO PROCESS</p>';
//        print_r($this->result_field_time_periods_to_process);
//        echo '</pre>';

        /**************************************************/


        if(isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0 && $_REQUEST['value_type'] == 'quarterly_values') {
            $quarter_index = 1;
            $loop_counter = 1;
            unset($this->result_fields['top']);
            $time_period_data_structure = array();
            $result_top = array();
            foreach($filter_by['when'] as $key => $time) {
                $time_period_data_structure[$quarter_index][$key] = $time;

                if($loop_counter % 3 == 0){
                    $this->result_fields['top'][$key] = $time;
                    $this->result_fields['top'][$key]['name'] = 'Quarter ending ' . $time['name'];
                    $quarter_index++;
                }
                $loop_counter++;
            }

//            echo '<pre style="font-size: 18px">';
//            echo '<p>$time_period_data_structure</p>';
//            print_r($time_period_data_structure);
//            echo '</pre>';
//
//            echo '<pre style="font-size: 18px">';
//            echo '<p>$result_top</p>';
//            print_r($result_top);
//            echo '</pre>';
        }

    }

    private function setResultFieldColumnSelectionTitle($headObject) {
        $helper = new SDBP6();
        $object_name = $this->me->getMyObjectName();
        $this->result_fields_selector_title = $headObject->getObjectName($object_name) . ' ' . $helper->getActivityName("update") . ' Columns';
    }

    private function setResultFieldHeadersThatDisplayUnderTheTimePeriodHeaders($headObject) {
	    $class_name = get_class($this->me);
        $results_headings = $headObject->getAllHeadingsForRenaming($class_name::CHILD_OBJECT_TYPE);

        foreach($results_headings as $key => $r) {
            $results_headings[$key]['name'] = $r['client'];
        	if($r['type']=="STATUS" || $r['type']=="COMMENT") {
        		unset($results_headings[$key]);
			}
		}
        $this->result_fields['bottom'] = $results_headings;
    }

    private function setTopNameByValueType($time_id){
        $top_name_pre_title = "";
        if($this->displayResultFields() && ($_REQUEST['value_type'] == 'year_to_date' || $_REQUEST['value_type'] == 'period_to_date')) {
            $top_name_pre_title = $this->result_field_value_types[$_REQUEST['value_type']] . ' ending ';
        }
        $this->result_fields['top'][$time_id]['name'] = $top_name_pre_title . $this->result_fields['top'][$time_id]['name'];
    }

    private function displayResultFields() {
        $displayResultFields = false;
        if(isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0) {
            $displayResultFields = true;
        }
        return $displayResultFields;
    }

    private function addTimePeriodToResultFieldDateRangeSelectBoxArray($key, $time) {
        $this->result_field_time_periods[$key]['name'] = $time['name'];
        $this->result_field_time_periods[$key]['is_current'] = ($time['is_current']===true ? true : false );
    }

    private function isTimePeriodToDisplayFrom($time_id) {
        $result_field_time_period = $_REQUEST['result_field_time_period_start'];
        $isTimePeriodToDisplayFrom = $this->resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id);
        return $isTimePeriodToDisplayFrom;
    }

    private function isTimePeriodToDisplayTo($time_id) {
        $result_field_time_period = $_REQUEST['result_field_time_period_end'];
        $isTimePeriodToDisplayTo = $this->resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id);
        return $isTimePeriodToDisplayTo;
    }

    private function resultFieldTimePeriodMatchesTimeId($result_field_time_period, $time_id) {
        $resultFieldTimePeriodMatchesTimeId = false;
        if((int)$result_field_time_period == (int)$time_id) {
            $resultFieldTimePeriodMatchesTimeId = true;
        }
        return $resultFieldTimePeriodMatchesTimeId;
    }
		
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {

		$sdbip_id = $this->getSDBIPId(); // #AA-381 [JC]
//echo "<hr /><h1 class='green'>START OF SCRIPT @ ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";


		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by']) > 0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows($sdbip_id);
		//test - do we need to save data to file to free up memory or is it small enough to handle as is? [JC] #AA-380
		if(count($rows) > $this->max_objects_to_handle) {
			$this->save_data_to_file_to_free_memory = true;
		}


		//update list of result_fields to remove unnecessary text / attach columns depending on what has been selected by user
		//this is to save time & reduce memory load
		//but keep the target/actual columns in case they are needed for calculations
		// [JC] #AA-380
		$result_columns_requested = $_REQUEST['result_columns'];
		$result_headings_for_processing = array();
		foreach($this->result_fields['bottom'] as $fld => $rh) {
			if(!in_array($rh['type'], array('TEXT', 'ATTACH')) || (isset($result_columns_requested[$fld]) && $result_columns_requested[$fld] == "on")) {
				$result_headings_for_processing[$fld] = $rh;
			}
		}
		unset($result_columns_requested);


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";


//save Rows data to text file to clear memory
//Prep only necessary data for passing on to Results functions - to reduce memory usage [JC] #AA-380 26 April 2020
		if($this->save_data_to_file_to_free_memory === true) {
			$rows_data_for_getting_results = array();
			$rows_data_file_name = "../files/temp/".$this->me->getCmpCode()."_".$this->me->getModRef()."_".$this->me->getUserID()."_report_objects_".date('Ymd_His').".txt";
			$fh = fopen($rows_data_file_name, 'w');
			/*echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
			ASSIST_HELPER::arrPrint($rows);
			echo "<hr />";
			$alt_rows = $rows;//
			$raw_file_data = (json_encode($alt_rows));
			fwrite($fh,$raw_file_data);
			*/
//			echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
			foreach($rows as $r_key => $row) {
				$rows_data_for_getting_results[$r_key] = array('calc_type_id' => $row['calc_type_id']);
				$alt_row = json_encode($row);
				if(json_last_error()>0) {
					$a_row = array();
					$r_key.="+";
					foreach($row as $f => $v) {
						$a_row[$f] = mb_convert_encoding($v,'UTF-8');
					}
					$alt_row = json_encode($a_row);
				}
//				$alt_row = $row;//mb_convert_encoding($row, 'UTF-8');
				fwrite($fh, ($alt_row).chr(10).chr(10));
//				echo "<h2 class=orange>:".$r_key.": ".json_last_error()."</h2>";
			}
			fclose($fh);
			//echo "<h2 class=red>:".$raw_file_data."</h2>";
//			die();
			unset($rows);
		} else {
			$rows_data_file_name = "";
			$rows_data_for_getting_results = array();
			foreach($rows as $r_key => $row) {
				$rows_data_for_getting_results[$r_key] = array('calc_type_id' => $row['calc_type_id']);
			}
		}


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />Finished prepping for getting results => save data to file = :".$this->save_data_to_file_to_free_memory.":");


		/**********************************************************************************************************/
		if($this->displayResultFields() && $_REQUEST['value_type'] == 'quarterly_values') {
			$filter_by = $this->me->getFilterByOptions('REPORT', 'VIEW');
			$quarter_index = 1;
			$loop_counter = 1;
			$time_period_data_structure = array();

			foreach($filter_by['when'] as $key => $time) {
				$time_period_data_structure[$quarter_index][$key] = $time;
				if($loop_counter % 3 == 0) {
					$quarter_index++;
				}
				$loop_counter++;
			}

			$results_rows_array = array();
			foreach($time_period_data_structure as $quarter_index => $result_field_time_periods_to_process) {
				$results_rows_array[$quarter_index] = $this->me->getResultsForListPages($rows_data_for_getting_results, $result_field_time_periods_to_process, $this->result_fields['bottom'], 'period_to_date', false);
			}

			$results_rows = array();
			$period_index = 3;
			$comment_fields = array();
			foreach($this->result_fields['bottom'] as $fld => $head) {
				if($head['type'] == "TEXT") {
					$comment_fields[$fld] = array();
				}
			}
			foreach($results_rows_array as $quarter_index => $results) {
				foreach($results as $row_id => $period_values) {
					$loop_counter = 1;
					$quarter_comments = $comment_fields;
					foreach($time_period_data_structure[$quarter_index] as $time_period_id => $time_period_data) {
						foreach($comment_fields as $fld => $x) {
							if(strlen($results_rows_array[$quarter_index][$row_id][$time_period_id][$fld]) > 0) {
								$quarter_comments[$fld][] = '+ '.$results_rows_array[$quarter_index][$row_id][$time_period_id][$fld].' ('.$time_period_data['name'].')';
							}
						}
						$loop_counter++;
					}
					$results_period_keys = array_keys($period_values);
					$final_time_period_key = $results_period_keys[count($results_period_keys) - 1];
					$results_rows[$row_id][$final_time_period_key] = $period_values[$final_time_period_key];
					if($_REQUEST['output'] == "csv") {
						$implode = ". ";
					} else {
						$implode = "<br />";
					}
					foreach($comment_fields as $fld => $x) {
						$results_rows[$row_id][$final_time_period_key][$fld] = implode($implode, $quarter_comments[$fld]);
					}
//					$results_rows[$row_id][$final_time_period_key][$this->me->getParentFieldName()] = $row_id;
				}
				$period_index = $period_index + 3;
			}
//save Rows data to text file to clear memory - quarterly_values doesn't do it in the results function like others as there is other processing that happens after so it must be done here [JC] #AA-387 19 May 2020
			if($this->save_data_to_file_to_free_memory === true) {
				$results_rows_file_name = "../files/temp/".$this->getCmpCode()."_".$this->getModRef()."_".$this->getUserID()."_report_results_group_".date("Ymd_His").".txt";
				$fh = fopen($results_rows_file_name, 'w');
				$alt_result_rows = array();
				foreach($results_rows as $i => $time_rows) {
					foreach($time_rows as $ti => $row) {
						foreach($row as $f => $r) {
							$alt_result_rows[$i][$ti][$f] = mb_convert_encoding($r, 'UTF-8');
						}
					}
				}
				$raw_file_data = base64_encode(json_encode($alt_result_rows));
				fwrite($fh,$raw_file_data);
				/*foreach($results_rows as $r_key => $row) {
					fwrite($fh, json_encode($row).chr(10).chr(10));
				}*/
				fclose($fh);
				unset($results_rows);
				unset($raw_file_data);
				$results_rows = $results_rows_file_name;
			}
		} else {
			//replaced first variable $rows with $rows_data_for_getting_results which contains only the data needed for the function - to reduce memory usage [JC] #AA-380
			//replaced $this->result_fields['bottom'] with $result_headings_for_processing so that only selected columns are processed & formatted as ATTACH processing sucks up a lot of time [JC] #AA-380
			$results_rows = $this->me->getResultsForListPages($rows_data_for_getting_results, $this->result_field_time_periods_to_process, $result_headings_for_processing, (isset($_REQUEST['value_type']) && is_string($_REQUEST['value_type']) && strlen($_REQUEST['value_type']) > 0) ? $_REQUEST['value_type'] : false, false, "ALL", false, $this->save_data_to_file_to_free_memory);
		}
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />Got basic results");


		//Check if any of the total columns have been selected - otherwise we can skip the $all_results_rows functions [JC] #AA-380
		$require_all_results_calculations = false;
		$total_column_names = array();
		foreach($_REQUEST['columns'] as $fld => $on) {
			if(in_array($fld, array("original_total", "revised_total", "actual_total"))) {
				$total_column_names[] = $fld;
				$require_all_results_calculations = true;
				//break; - can't break here even though the boolean check has been done, as need the full set of columns in the total_column_names array [JC] #AA-380
			}
		}

		//if any of the _total fields are needed - get the total for the whole year
		if($require_all_results_calculations) {
			//send full list of headings - process = false & save = true will then strip out TEXT/ATTACH headings
			//cost of sending full headings variable deemed minimal in comparison to the processing required to filter out unnecessary headings
			$all_result_rows = $this->me->getResultsForListPages($rows_data_for_getting_results, $this->all_result_time_periods, $result_headings_for_processing, "year_to_date", false, "ALL", false, $this->save_data_to_file_to_free_memory, false, true);
		}
		$all_keys = array_keys($this->all_result_time_periods);
		$final_time_id = $all_keys[count($all_keys) - 1];
		$final_overall_time_id = 0;


//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />Got all time results for TOTAL");


		//if "Values as Captured" or "Group by Quarter" has been selected & Original or Revised Targets, Actual & R
		$overall_result_rows = array();
		if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {
			$this->overall_result_fields['top'][$_REQUEST['result_field_time_period_end']] = 'Overall Performance for '.$this->result_field_time_periods_to_process[$_REQUEST['result_field_time_period_start']]['name'].' to '.$this->result_field_time_periods_to_process[$_REQUEST['result_field_time_period_end']]['name'];

			foreach($this->result_fields['bottom'] as $field_name => $field_data) {
				if((isset($_REQUEST['result_columns'][$field_name]) && $_REQUEST['result_columns'][$field_name] == 'on')
					&&
					(strpos($field_name, '_original') !== false || strpos($field_name, '_result') !== false || strpos($field_name, '_revised') !== false || strpos($field_name, '_actual') !== false)
				) {
					$this->overall_result_fields['bottom'][$field_name] = $field_data;
					$_REQUEST['overall_result_columns'][$field_name] = 'on';
				}

			}
			//no need to save this to file as it will just get read back out again immediately
			$overall_result_rows = $this->me->getResultsForListPages($rows_data_for_getting_results, $this->result_field_time_periods_to_process, $result_headings_for_processing, "period_to_date", false, "ALL", false, false, false, true);
			$final_overall_time_id = $_REQUEST['result_field_time_period_end'];
		}

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />Got final PTD overall results");


		//get rows data back from file if needed
		//remember that double blank line was used to save the data in case of unexpected line breaks in the data from the data so check for blank line
		if($this->save_data_to_file_to_free_memory) {
//			$raw_file_data = file_get_contents($rows_data_file_name);
//			$rows = json_decode($raw_file_data,true);
			// [JC] AA-380
			$fh = fopen($rows_data_file_name, 'rb');
			$rows = array();
			$save_line = "";
			while($line = fgets($fh)) {
				//if line is not blank then add it to any previous line - using 2 to catch odd eol characters
				if(strlen($line) > 2) {
					$save_line .= $line;
				} elseif(strlen($save_line) > 0) {
					//otherwise process the previous line and then start from scratch with current line
					$r = json_decode($save_line,true);
					$obj_id = $r['raw_id'];
					$rows[$obj_id] = $r;
					$save_line = "";
				} else {
					//do nothing to extra blank lines - it means the user was an idiot and submitted multiple line breaks in a text box which isn't needed for display in the report and just takes up space/memory
//					echo "<hr />How did I end up here? ROWS :".$line.":";
				}
			}
			fclose($fh);

			//if(!is_array($results_rows)) {
				//$fh = fopen($results_rows, 'rb');
				$raw_file_data = file_get_contents($results_rows);
				$results_rows = json_decode(base64_decode($raw_file_data),true);
				/*$results_rows = array();
				$save_line = "";
				$c = 1;
				while($line = fgets($fh)) {
					//if line is not blank then add it to any previous line - using 2 to catch odd eol characters
					if(strlen($line) > 2) {
						$save_line .= $line;
					} elseif(strlen($save_line) > 0) {
						//otherwise process the previous line and then start from scratch with current line
						$r = json_decode(str_replace("||", chr(10), $save_line),true);
						$keys = array_keys($r);
						$obj_id = $r[$keys[0]][$this->me->getResultsParentFieldName()];
						$results_rows[$obj_id] = $r;
						$save_line = "";
					} else {
						//do nothing to extra blank lines - it means the user was an idiot and submitted multiple line breaks in a text box which isn't needed for display in the report and just takes up space/memory
			//					echo "<hr />How did I end up here? RESULTS ".$c.":".ASSIST_HELPER::code(ASSIST_HELPER::code($line)).":";
					}
					$c++;
				}
				fclose($fh);*/
			//}
//ASSIST_HELPER::arrPrint($results_rows);die(__LINE__);
			//get rows data back from file
			//remember that double blank line was used to save the data in case of unexpected line breaks in the data from the data so check for blank line
			// [JC] AA-380
			/** @var STRING $all_result_rows - string if save_data_to_file is true, otherwise array but this won't trigger if save_data_to_file = false */
			$raw_file_data = file_get_contents($all_result_rows);
			$all_result_rows = json_decode(base64_decode($raw_file_data),true);
			/*$fh = fopen($all_result_rows, 'rb');
			$all_result_rows = array();
			$save_line = "";
			while($line = fgets($fh)) {
				//if line is not blank then add it to any previous line - using 2 to catch odd eol characters
				if(strlen($line) > 2) {
					$save_line .= $line;
				} elseif(strlen($save_line) > 0) {
					//otherwise process the previous line and then start from scratch with current line
					$r = json_decode($save_line,true);
					$keys = array_keys($r);
					$obj_id = $r[$keys[0]][$this->me->getResultsParentFieldName()];
					$all_result_rows[$obj_id] = $r;
					$save_line = "";
				} else {
					//do nothing to extra blank lines - it means the user was an idiot and submitted multiple line breaks in a text box which isn't needed for display in the report and just takes up space/memory
//					echo "<hr />How did I end up here? ALL :".$line.":";
				}
			}
			fclose($fh);*/
			unset($save_line);
			unset($line);
			unset($r);
		} //endif save_data_to_file = true
/*else {
					echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
				echo base64_encode(json_encode($results_rows));
				ASSIST_HELPER::arrPrint($results_rows);
				echo "<hr />";
				die();
}*/

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />fetched data from files ");


		/**********************************************************************************************************/

		//Targets & Actuals (including baseline & original/revised annual target & ytd actual) FORMAT... UGLY HACK
//        $sdbp_id = 0; //voided by #AA-381
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("");
		foreach($rows as $i => $r) {

			//get sdbip_id if needed - voided by #AA-381
//			if($sdbp_id==0) {
//				$fld = $this->me->getParentFieldName();
//				$sdbp_id = $r[$fld];
//			}

			//format baseline
			$fld = $this->me->getTableField()."_baseline";
			$val = $r[$fld];
			$rows[$i][$fld] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val, false, $r['target_type_id']);


			//format & set _total columns
			if($require_all_results_calculations) {
				$final = isset($all_result_rows[$i][$final_time_id])?$all_result_rows[$i][$final_time_id]:array();
				foreach($total_column_names as $fld) {
					if($fld == "original_total") {
						$val = isset($final[$this->me->getResultsOriginalFieldName()])?$final[$this->me->getResultsOriginalFieldName()]:0;
					} elseif($fld == "revised_total") {
						$val = isset($final[$this->me->getResultsRevisedFieldName()])?$final[$this->me->getResultsRevisedFieldName()]:0;
					} elseif($fld == "actual_total") {
						$val = isset($final[$this->me->getResultsActualFieldName()])?$final[$this->me->getResultsActualFieldName()]:0;
					}
					$rows[$i][$fld] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($val, false);
				}
			}


			//if overall PTD column is needed
			if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {
				foreach($this->overall_result_fields['bottom'] as $fld => $orf) {
					if(in_array($orf['type'], array("NUM", "CALC"))) {
						$overall_result_rows[$i][$final_overall_time_id][$fld] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($overall_result_rows[$i][$final_overall_time_id][$fld], false);
					}
				}
			}
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($results_rows[$i]);
//echo "<hr />";
//die();
			//format numbers in results
			if($this->displayResultFields() && isset($results_rows[$i]) && is_array($results_rows[$i])) {
				foreach($results_rows[$i] as $time_id => $time_result_data) {
					foreach($this->result_fields['bottom'] as $fld => $rh) {
						if(in_array($rh['type'], array("NUM", "CALC"))) {
							$results_rows[$i][$time_id][$fld] = $targetTypeObject->formatNumberBasedOnTargetTypeForReporting($results_rows[$i][$time_id][$fld], false);
						}
					}
				}
			}

		}
		unset($targetTypeObject);

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo("<hr />formatted everything");

		/*******************************************************************************
		 * Work out which result categories must be displayed & filter the rows for those that don't match
		 */
		$resultObject = new SDBP6_SETUP_RESULTS();
		$result_settings = $resultObject->getCustomResultSettingsDirectlyFromDatabase();
		if(isset($_REQUEST['filter']['result_setting']) && !in_array("X", $_REQUEST['filter']['result_setting']) && !in_array("XX", $_REQUEST['filter']['result_setting'])) {
			unset($this->result_categories);
			foreach($_REQUEST['filter']['result_setting'] as $key => $val) {
				$index = $result_settings[$val]['code'];
				$kpi_text = 'KPI '.$result_settings[$val]['value'];

				$this->result_categories[$index]['text'] = $kpi_text;
				$this->result_categories[$index]['code'] = $index;
				$this->result_categories[$index]['color'] = $result_settings[$val]['color'];
				$this->result_categories[$index]['description'] = $result_settings[$val]['glossary'];
			}
		}

		$foreach_rows = array_keys($rows);
		foreach($foreach_rows as $id) {
			if($this->displayResultFields() && ($_REQUEST['value_type'] == 'value_as_captured' || $_REQUEST['value_type'] == 'quarterly_values')) {//Use overall result to analyze
				$result_row_to_analyze = $overall_result_rows[$id][$final_overall_time_id];
			} else {//Use the last time period to analyze
				$result_row_to_analyze = $results_rows[$id][$_REQUEST['result_field_time_period_end']];
			}

			//set the result of the row as this one
			$field_name = $this->me->getResultsTableField()."_result";
			$rows[$id]['result'] = $result_row_to_analyze[$field_name];


			if(!array_key_exists($rows[$id]['result'], $this->result_categories)) {
				unset($rows[$id]);
				unset($results_rows[$id]);
				unset($overall_result_rows[$id]);

				foreach($this->groups as $key => $g) {
					$group_rows_array = (isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
					if(count($group_rows_array) > 0) {
						$group_rows_array_flipped = array_flip($group_rows_array);
						if(array_key_exists($id, $group_rows_array_flipped)) {
							unset($this->group_rows[$key][$group_rows_array_flipped[$id]]);
						}
					}
				}
			}
		}
		unset($result_row_to_analyze);

		/****************************************************
		 * END OF RESULTS CATEGORIES FILTER
		 */


		/*****************************************************
		 * FINAL PROCESSING AND SEND TO OUTPUT
		 */
//voided by #AA-381
//        $sdbp_odject = new SDBP6_SDBIP();
//        $sdbp_name = $sdbp_odject->getASDBIPName($sdbp_id);
//        unset($sdbp_odject);
		$sdbp_name = $this->getSDBIPName();
		$report_title = $sdbp_name.': '.$this->default_report_title;

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$this->me->print_mem();
//echo "<P>".date("H:i:s")." = ".(time()-$this->start_time_of_script_running)." seconds";
//echo "<hr />";
//echo "<P>".$sdbp_id;
//echo "<P>".$report_title;
//die("<hr />ready to output @ ".date("H:i:s"));


		$this->report->setReportTitle($report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
		foreach($this->result_categories as $key => $r) {
			$this->report->setResultCategoryWithDescription($key, $r['text'], $r['color'], $r['description']);
		}
		$this->report->setRows($rows);
		unset($rows); //free up memory
		$this->report->setResultRows($results_rows);
		unset($results_rows); //free up memory
		$this->report->setOverallResultRows($overall_result_rows);
		unset($overall_result_rows); //free up memory

		foreach($this->groups as $key => $g) {
			$this->report->setGroup($key, $g['text'], isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings($this->result_fields['bottom']);
	}






	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	private function cleanUpSDBP6Text($text) {
		$text = str_replace('\r\n','',$text);
		$text = str_replace('\n','',$text);
		$text = str_replace('\r','',$text);
		$text = str_replace(chr(10),'',$text);
		return $text;
	}
	
	
	
	private function getRows($sdbip_id=false) {
		$final_rows = array();
		$mscoa_version = $this->getSDBIPmSCOAVersion();
		$sql = $this->setSQL(false,array(),$sdbip_id);

		$id_field = $this->me->getIDFieldName();
		if(strlen($sql)>0) {
			$rows = $this->db->mysql_fetch_all_fld($sql,$id_field);
            /***************************************************************
             *** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - START ***
             ***************************************************************/
            $obj_type = 'DEPTKPI';
            $section = 'MANAGE';
            $headObject = new SDBP6_HEADINGS();
            $idp_headings = $headObject->getListPageHeadings($obj_type, $section);
            $all_headings = array_merge($idp_headings);
            $idpObject = $this->me;
            //REPLACE LIST, SEGMENT, OBJECT values with names
            $all_my_lists = array();
            //get replacement values
            foreach($all_headings as $fld => $head) {
                switch($head['type']) {
                    case "MULTILIST":
                    case "LIST":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $listObject = new SDBP6_LIST($head['list_table']);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
                            unset($listObject);
                        }
                        break;
                    case "MULTIOBJECT":
                    case "OBJECT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_name = $head['list_table'];
                            $extra_info = array();
                            if(strpos($list_object_name,"|")!==false) {
                                $lon = explode("|",$list_object_name);
                                $list_object_name = $lon[0];
                                $extra_info = $lon[1];
                            }
                            $listObject = new $list_object_name();
                            $all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect($extra_info);
                            unset($listObject);
                        }
                        break;
                    case "MULTISEGMENT":
                    case "SEGMENT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_type = $head['list_table'];
                            $listObject = new SDBP6_SEGMENTS($list_object_type);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect(array('version'=>$mscoa_version));
                            unset($listObject);
                        }
                        break;
                } //end switch by type
            }//end foreach heading
            /***************************************************************
             **** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - END ****
             ***************************************************************/
            //now loop through rows and replace values as needed

			foreach($rows as $key => $r) {
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r[$id_field]] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = $this->getRefTag().$d;
								break;
							case "DATE":
								if(strtotime($d) != 0) {
									$d = date("d-M-Y",strtotime($d));
								} else {
									$d = "";
								}
								break;
							case "BOOL":
								if($d==="1") { $d = "Yes"; } elseif($d==="0") { $d = "No"; }
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
								$d2 = $d;
								$x = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$d);
								$d = "";
								$z = array();
								if($i=="status") {
									$z[] = (!isset($this->data[$i][$d2])) ? $this->data[$i][1] : $this->data[$i][$d2];	//default to new for unknown status
								} else {
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										}
									}
								}
								$d = count($z)>0 ? implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS." ",$z) : "Unspecified";
								break;
							case "PERC":
							case "PERCENTAGE":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "ATTACH":
                                if(strlen($d) > 0){
                                    $f = unserialize($d);
                                    $d = "";
                                    if(isset($f) && is_array($f) && count($f) > 0){
                                        foreach($f as $key => $val){
                                            $d .= "+" . $val['original_filename'] . "\n";
                                        }
                                    }
                                }else{
                                    $d = "";
                                }
								break; 
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r[$id_field]][$i] = $this->cleanUpSDBP6Text(stripslashes($d));
						}
					}
					$this->allocateToAGroup($r);
				}

                /***************************************************************
                 *** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - START ***
                 ***************************************************************/
                $row = $r;
                foreach($row as $fld => $x) {
                    //only treat as list if previously handled in check above && only act on field if required for list page i.e. set in headings list
                    if(isset($all_my_lists[$fld]) && isset($all_headings[$fld])) {
                        $head = $all_headings[$fld];
                        //store display data in list_table element to preserve raw data in original fld element
                        //formatting function (below) looks for display data in list_Table element for all list heading types (set in _HEADINGS->list_heading_types)
                        $save_field = $head['list_table'];
                        //if heading type contains MULTI - assume it is multi LIST SEGMENT or OBJECT
                        if(strpos($head['type'],"MULTI")!==false) {
                            if(strpos($head['type'],"OBJECT")!==false) {
                                $blank_value = "";
                            } else {
                                $blank_value = $idpObject->getUnspecified();
                            }
                            $x = explode(";",$x);
                            $x = $idpObject->removeBlanksFromArray($x);
                            if(count($x)>0) {
                                $rows[$key][$save_field] = array();
                                foreach($x as $y) {
                                    if(isset($all_my_lists[$fld][$y])) {
                                        $rows[$key][$save_field][] = $all_my_lists[$fld][$y];
                                    }
                                }
                                if(count($rows[$key][$save_field])>0) {
                                    $rows[$key][$save_field] = implode("; ",$rows[$key][$save_field]);
                                } else {
                                    $rows[$key][$save_field] = $blank_value;
                                }
                            } else {
                                $rows[$key][$save_field] = $blank_value;
                            }
                        } else {
                            if($head['type']=="OBJECT") {
                                $blank_value = "";
                            } else {
                                $blank_value = $idpObject->getUnspecified();
                            }
                            if(isset($all_my_lists[$fld][$x])) {
                                $rows[$key][$save_field] = $all_my_lists[$fld][$x];
                            } else {
                                $rows[$key][$save_field] = $blank_value;
                            }
                        }//end if multi
                    }//end if all_my_lists[fld] isset
                }//end foreach fld

                /***************************************************************
                 **** ADDITIONAL PROCESSING CODE FROM SDBP6 getList() - END ****
                 ***************************************************************/
			}
            /************************************************************************
             *** ADDITIONAL PROCESSING CODE FROM SDBP6 formatRowDisplay() - START ***
             ************************************************************************/
            $id_fld = $idpObject->getIDFieldName();
            $ref_tag = $idpObject->getRefTag();
            $displayObject = new SDBP6_DISPLAY();
            $final_data['head'] = $idp_headings;

            foreach($rows as $r) {
                $row = array();
                $i = $r[$id_fld];
                $final_rows[$i]['raw_id'] = $i;
                if(isset($r['target_type_id'])) { $final_rows[$i]['target_type_id'] = $r['target_type_id']; }
                if(isset($r['calc_type_id'])) { $final_rows[$i]['calc_type_id'] = $r['calc_type_id']; }
                foreach($final_data['head'] as $fld=> $head) {
                    if($head['parent_id']==0){
                        if($headObject->isListField($head['type'])) {
                            $final_rows[$i][$fld] = (isset($r[$head['list_table']]) ? $r[$head['list_table']] : 'N/A');
                        } elseif($idpObject->isDateField($fld)) {
                            $field_data = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        } elseif($head['type']=="NUM" && $head['apply_formatting']==true) {
                            $field_data = $displayObject->getDataField("TEXT", (isset($r[$fld])?$r[$fld]:""),array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        } else {
                            $field_data = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                            $final_rows[$i][$fld] =  (is_array($field_data) && array_key_exists('display', $field_data) ? $field_data['display'] : $field_data);
                        }
                    }
                }
            }
            /************************************************************************
             **** ADDITIONAL PROCESSING CODE FROM SDBP6 formatRowDisplay() - END ****
             ************************************************************************/
		}
//		echo '<pre style="font-size: 18px">';
//		echo '<p>FINAL ROWS</p>';
//		print_r($final_rows);
//		echo '</pre>';

		return $final_rows;
	}
	
	//Changed $db variable passing to reference to stop unnecessary  duplication of object & spare memory
	protected function setSQL($db,$filter=array(),$sdbip_id=false) {
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
        $group_by = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";

//		echo '<pre style="font-size: 18px">';
//		echo '<p>THE REQUEST</p>';
//		var_dump($_REQUEST);
//		echo '</pre>';

        $obj_type = 'DEPTKPI';
        $section = 'MANAGE';
        $dept_obj = new SDBP6_DEPTKPI();
        $has_results = $dept_obj->hasResults();

        $left_joins = array();

        //comment out for development purposes to register changes in heading class
        if(!isset($_SESSION[$this->getModRef()]['headingObject'])) {
            $headObject = new SDBP6_HEADINGS();
        } else {
            $headObject = unserialize($_SESSION[$this->getModRef()]['headingObject']);
        }

        //set up variables
//        $idp_headings = $headObject->getListPageHeadings($obj_type, $section);
		$all_headings = $headObject->getReportObjectHeadings($obj_type);

//        $class_name = "SDBP6_".$obj_type;
//        $idpObject = new $class_name();
        $idp_tblref = $this->me->getMyObjectType();
        $idp_table = $this->me->getTableName();
        $idp_status = $idp_tblref.".".$this->me->getStatusFieldName();

        $tblref = $idp_tblref;
        $where_tblref = $idp_tblref;

        $where = $this->me->getActiveStatusSQL($idp_tblref);
        // #AA-381 - limit to given SDBIP
        if($sdbip_id!==false) {
        	$where.= " AND ".$this->me->getParentFieldName()." = ".$sdbip_id;
		}
        $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
        //extra field in sql to get RAW TARGET TYPE ID - required for results formatting
        if($this->me->getTargetTypeTableField()!==false) {
            $sql.= ", ".$this->me->getTargetTypeTableField()." as target_type_id";
        }
        //extra field in sql to get RAW CALC TYPE ID - required for results calculations
        if($this->me->getCalcTypeTableField()!==false) {
            $sql.= ", ".$this->me->getCalcTypeTableField()." as calc_type_id";
        }
        $from = " ".$idp_table." ".$idp_tblref."";
        $sort_by = array();

//        $all_headings = array_merge($idp_headings);

        $listObject = new SDBP6_LIST('');

        foreach($all_headings as $id => $head) {
        	$fld = $head['field'];
            $lj_tblref = $head['section'];
            if($head['type']=="MASTER") {
                $tbl = $head['list_table'];
                $masterObject = new SDBP6_MASTER($fld);
                $fy = $masterObject->getFields();
                $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
                $left_joins[] = " LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
                $sb = $tbl;
            } elseif($head['type']=="USER") {
                $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                $left_joins[] = " INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
                $sb = $head['list_table'];
            }elseif($head['type']=="LIST") {
                $tbl = $head['list_table'];
                $listObject->changeListType($tbl);
                $sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($tbl)." AS ".$tbl."
										ON ".$tbl.".id = ".$lj_tblref.".".$fld." 
										AND (".$tbl.".status & ".SDBP6::DELETED.") <> ".SDBP6::DELETED;
                $sb = $tbl.".".implode(", ".$tbl.".",str_ireplace("|X|", $tbl, $listObject->getSortBy(true)));
                $sb = str_ireplace($tbl.".if", "if", $sb);
            }else {
                $sb = $where_tblref.".".$fld;
            }
            $sort_by[$fld] = $sb;
        }

        //Link the Top Layer Org Structure to the rows
        if(/*$_REQUEST['group_by'] == 'top_sub_id'*/true){
            $object = new SDBP6_SETUP_ORGSTRUCTURE();
            $tbl = $object->getTableName();
            $sql.= ", ".$tbl.".org_parent_id as top_sub_id";
            $left_joins[] = " LEFT OUTER JOIN ".$tbl/*." AS ".$tbl*/." 
										ON ".$tbl.".org_id = ".$idp_tblref.".kpi_sub_id";

            $sort_by['top_sub_id'] = 'top_sub_id';
        }

        $sql.= " FROM ".$from." ".implode(" ",$left_joins);
        $sql.= " WHERE ".$where;

        $s = array();
        if(count($this->titles)>0) {
            foreach($this->titles as $fld => $t) {
                //echo "<P>".$fld;
                if( (!isset($this->allowfilter[$fld]) || $this->allowfilter[$fld]===true) && isset($filters[$fld]) && ($fld != 'result_setting')) {
                    $t = $this->types[$fld];
                    $f = $filters[$fld];
                    $ft = isset($this->filter_types[$fld]) ? $this->filter_types[$fld] : "";
                    $a = "";
                    switch($fld) {
                        case "action_progress":
                            //do nothing - filtering applied in row processing
                            break;
                        case "result":
                            //do nothing - filtering applied in row processing
                            break;
                        default:
                            $a =  $this->report->getFilterSql($tblref,$t,$f,$ft,$fld);
                            break;
                    }
                    if(strlen($a)>0) { $s[] = $a; }
                }
            }
        }
        if(count($s)>0) {
            $sql.= " AND ".implode(" AND ",$s);
        }

        $sql.=$this->getSortBySql($sort_by);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>$sql</p>';
//        print_r($sql);
//        echo '</pre>';

		return $sql;
	}
	

}

?>