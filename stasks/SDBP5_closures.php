<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP5_closures not executed.");
}

/** Generic Assist Classes **/
include("../library/class/assist_helper.php");
$me = new ASSIST_HELPER();
include("../library/class/assist_dbconn.php");
include("../library/class/assist_db.php");
include("../library/class/assist_email.php");
include("../library/class/assist.php");
include("../library/class/assist_user.php");
include("../library/class/assist_root.php");
include("../library/class/assist_module_helper.php");

/** SDBP5 Classes **/
include("../SDBP5/class/sdbp5_helper.php");
include("../SDBP5/class/sdbp5_time.php");

	$support_email = "helpdesk@ignite4u.co.za";
	//$support_email = "igniteassist_dev@gmail.com";
	$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
	//$IAS_helpdesk = "igniteassistdev@gmail.com";
	$IRS_helpdesk = "helpdesk@igniteassist.co.za";
	$from = "no-reply@ignite4u.co.za";
	//$header1 = "From: ".$from."\r\nReply-to: ".$from."\r\n";
$today = $_SERVER["REQUEST_TIME"];


//error_reporting(-1);
$arr_modref = array("sdp13","sdp14","sdp15","sdp15i","sdp16","sdp16i","sdp17","sdp17i","sdp18","sdp18i");
$mod_table = "list_time";
$doctitle = "SDBIP v5 & v5B & IKPI1";
$mod_titles = array(
'sdp13'=>"SDBIP 2013/2014",'sdp14'=>"SDBIP 2014/2015",'sdp15'=>"SDBIP 2015/2016",'sdp15i'=>"Individual KPIs 2015/2016",
	'sdp16'=>"SDBIP 2016/2017",
	'sdp16i'=>"Individual KPIs 2016/2017",
	'sdp17'=>"SDBIP 2017/2018",
	'sdp17i'=>"Individual KPIs 2017/2018",
	'sdp18'=>"SDBIP 2018/2019",
	'sdp18i'=>"Individual KPIs 2018/2019",

);
$totalactions = 0;

$time_categories = array("primary","secondary","finance");

$tkid = "IA";
$_SESSION['tkn'] = "Assist Automated Tasks";
$_SESSION['tid'] = "IA";

$result1 = "<style type=text/css>";
$result1.= "</style>";
$result2 = "<h1>$doctitle ~ Scheduled Tasks: Closures</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th>CMPCODE</th><th>Action</th></tr>";


$cmpcode = "";
//GET ARRAY OF CMPCODE
$db = new ASSIST_DB("master");

$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y'";
$company = $db->mysql_fetch_all_by_value($sql,"cmpcode");


$dbs = array();
$db->db_connect();
$link = $db->getConn();
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}


//FOREACH COMPANY
foreach($company as $cmpcode) {
	set_time_limit(1800);
	$cmpcode = strtolower($cmpcode);
	$db_name = $db->getOtherDBName($cmpcode);
	//IF DATABASE EXISTS
	$result2.= "<tr><td>".strtoupper($cmpcode)."</td><td><ul>";
	if (in_array($db_name, $dbs, true)) {
		$result2.= "<li>Database found.</li>";
		foreach($arr_modref as $modref) {
			$log_change = array();
			$_SESSION['cc'] = $cmpcode;
			$_SESSION['modref'] = $modref;
			$modtitle = $mod_titles[$modref];
			//CHECK IF MODULE/TABLE EXISTS
			$dbObj = new ASSIST_DB("client",$cmpcode);
			$module_exists = $dbObj->checkModuleExists($modref,$mod_table);
			if($module_exists) {
				$result2.="<li>".strtoupper($modref)." exists</li>";
				$dbObj->setDBRef($modref);
				$dbref = $dbObj->getDBRef();
				$reseller = $me->getReseller($cmpcode);
				$reseller_info = $me->getBPADetails($reseller);
				//Get time with reminders
				$timeObj = new SDBP5_TIME($cmpcode,true);
				$timeObj->setDBRef($modref,$cmpcode);
				$time = array();
				$tcate = array();
				foreach($time_categories as $cate) {
					$tcate[$cate] = $timeObj->getOpenTime($cate," start_date < '".date("Y-m-d H:i:s")."' AND close_".$cate." > 86400 AND close_".$cate." <= ".$today);
					foreach($tcate[$cate] as $ti=>$t) {
						if(!isset($time[$ti])) { $time[$ti] = $t; }
					}
				}
				if(count($time)>0) {
					$result2.="<li>".count($time)." open time periods with scheduled closures found.</li>";
					foreach($time as $ti=>$t) {
						$resets = array();
						$reset_fld = array();
						foreach($time_categories as $cate) {
							if(isset($tcate[$cate]) && count($tcate[$cate])>0) {
								switch($cate) {
									case "primary":
										$section_head = "Departmental SDBIP";
										break;
									case "secondary":
										$section_head = "Departmental / Top Layer SDBIP";
										break;
									case "finance":
										$section_head = "Financials";
										break;
								}
								if($t['close_'.$cate]<=$today && $t['active_'.$cate]) {
									$reset_fld[] = $cate;
									$resets[] = "active_".$cate;
									$resets[] = "rem_".$cate;
									$resets[] = "close_".$cate;
									$log_change[] = " - $section_head - ".date("F Y",$t['end'])." for $cate users.";
								}
							}
						}
						if(count($resets)>0) {
								$lsql = $timeObj->resetTime($resets,$ti);
								if($lsql!==false) {
												$v_log = array(
													'section'=>"TIME",
													'ref'=>$ti,
													'field'=>implode(", ",$resets),
													'text'=>"Performed Automatic Closures:<br />".implode("<br />",$log_change),
													'old'=>"",
													'new'=>"0",
													'YN'=>"Y"
												);
												$timeObj->logChanges('SETUP',0,$v_log,$me->code($lsql));
								}
								$totalactions+=count($log_change);
						}
						$result2.="<li class=isubmit>Automatic Closures performed for time period $ti:</li><li>".implode("</li><li>",$log_change)."</li>";
						$log_change = array();
					}
				} else {
					$result2.="<li class=iinform>No open time periods with scheduled closures found.</li>";
				}
			} else {
				$result2.="<li class=iinform>".strtoupper($modref)." not found</li>";
			}
		}
    } else {
        $result2.= "<li class=idelete>No database found.</li>";
    }
	$result2.="</ul></td></tr>";
}
$result2.="</table>";

//WRITE RESULTS TO FILE
$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Action4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"https://assist.action4u.co.za/assist.css\" type=\"text/css\">";
$echo.= $result1;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result2."</body></html>";
		//WRITE DATA TO FILE
		$filename = "Results/SDBP5_ACLOSE_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
$_SESSION['modref'] = 'SDBP5_CLOSE';
//$to = "janet@actionassist.co.za";
//$from = "no-reply@ignite4u.co.za";
$subject = "$doctitle Auto Closures";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
//$header = "Content-type: text/html; charset=us-ascii";
//mail($to,$subject,$message,$header);
/*$email = new ASSIST_EMAIL($to,$subject,$message,"HTML");
	$email->setSenderUserID("AUTO");
	$email->setSenderCmpCode("IASSIST");
	$email->setRecipientUserID("0001");
$email->sendEmail();
*/
include("cron_job_send_email.php");

echo "<p>Closures performed: ".$totalactions."</p>";

?>