<?php
require_once("../module/autoloader.php");

//error_reporting(-1);
$db = new ASSIST_DB("master");

$sql = "SELECT * FROM assist_email_log_sendgrid_processed WHERE final_date > '2017-07-01 00:00:00' AND final_date < '2018-01-10 12:00:00' AND email LIKE 'velly%' ORDER BY final_date DESC";
$rows = $db->mysql_fetch_all($sql);

$me = new ASSIST_HELPER();
$me->echoPageHeader();


$h = $rows[0];
$headings = array_keys($h);

$headings = array(
	'id'=>"Log ID",
	'smtp_id'=>"SMTP Ref",
	'email'=>"Recipient",
	'final_result'=>"Email Result",
	'final_date'=>"Email Result Date",
	
);
//'log_data'=>"Complete Log Data",

?>
<h1>Assist Email Log</h1>
<p>All emails sent to velly@gteda.co.za between 1 July 2017 00:00 and 10 Jan 2018 12:00.</p>
<table>
	<thead>
		<tr>
<?php
foreach($headings as $fld => $head) {
echo "<th>".$head."</th>";
}
?>
			<!-- <th>ID</th>
			<th>Date</th><!--
			<th>POST Data</th>
			--><!--<th>Recipient</th>
			<th>Raw Timestamp</th>
			<th>Event Date</th>
			<th>Event</th>
			<th>Response</th> -->
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($rows as $row) {
echo "<tr>";
foreach($headings as $fld=>$head) {
	if($fld=="log_data") {
		echo "<td><pre>";
		print_r(unserialize(base64_decode($row[$fld])));
		echo "</pre></td>";
	} else {
		echo "<td>".$row[$fld]."</td>";
	}
}
echo "</tr>";
/*
$var = unserialize($row['log_data']);
foreach($var as $v) {
if(strpos($v['email'],'draken')!==false && strpos($v['subject'],'Password')!==false && $v['event']!='processed') { 
			echo "
			<tr>
				<td>".$row['id']."</td>
				<td>".$row['log_date']."</td>
				<!--<td><pre>"; //print_r($v); 
				echo "</pre></td> -->
				<td>".$v['email']."</td>
				<td>".$v['timestamp']."</td>
				<td>".date('Y-m-d H:i:s',$v['timestamp'])."</td>
				<td>".$v['event']."</td>
				<td>".$v['response']."</td>
			</tr>";
}
}
*/
		}
		?>
	</tbody>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>