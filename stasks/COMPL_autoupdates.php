<?php
//error_reporting(-1);

$update_log_insert_date = date("Y-m-d H:i:s");

function saveResult($buffer) {
 global $ccount;
$buffer.="<p>".$ccount." updates completed.</p>";
	$filename = "Results/COMPL_AUTO_".date("Ymd_His").".html";
       $file = fopen($filename,"w");
       fwrite($file,$buffer."\n");
       fclose($file);

	//NOTIFY ME OF CLOSURES
//	$to = "janet@actionassist.co.za";
//	$from = "no-reply@ignite4u.co.za";
	$subject = "Auto Compliance Updates";
	$message = "<p>".$ccount." updates completed.</p><p>Results: <a href=http://".$_SERVER["HTTP_HOST"]."/stasks/".$filename.">".$filename."</a></p>";
//	$header = "Content-type: text/html; charset=us-ascii";
//	mail($to,$subject,$message,$header);
	unset($to);
	include("cron_job_send_email.php");
	return $buffer."<hr>".$message."<p>Done!</p>" ;

}

ob_start("saveResult");


//include_once("../library/class/assist_email.php");
//include_once("../library/class/assist_email_summary.php");

$ccount = 0;

$get_session = false;
require_once '../header.php';
$_SESSION['cc'] = ""; $_SESSION['modref'] = "";
set_time_limit(1800);
?>
<style type=text/css>

table.mods th { background-color: #999999; }
</style>
<?php
$ml = "";
$_SESSION['modlocation'] = "COMPL";
$_SESSION['tid'] = "0000";
$_SESSION['tkname'] = "Assist";
$_SESSION['modref'] = "COMPL";
$_SESSION['ref'] = "COMPL";
include_once("../module/loader.php");
spl_autoload_register("Loader::autoload");

echo "<h1>Compliance Assist Automatic Updates</h1>
<p class=i>Task run at ".date("d F Y H:i:s")."</p>";

$mdb = new ASSIST_DB("master");

$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmp_is_demo = 0 AND cmpcode NOT IN ('BLANK','CASSIST')";
$company = $mdb->mysql_fetch_all_fld($sql,"cmpcode");

//echo "<pre>"; print_r($company); echo "</pre>";

$mdb->db_connect();
$db_list = mysql_list_dbs($mdb->getConn());
$bds = array();
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
//echo "<pre>"; print_r($dbs); echo "</pre>";

echo "<table width=100%><tr><th colspan=2>Company</th><th>Result</th></tr>";
foreach($company as $cmpcode => $cmp) {
set_time_limit(1800);
	echo "<tr><td class=b>$cmpcode</td><td class=b>".$cmp['cmpname']."</td><td>";
	$cmpcode = strtolower($cmpcode);
	//Check that database exists
	if(in_array($mdb->getDBPrefix().$cmpcode,$dbs)) {
		$_SESSION['cc'] = $cmpcode; //$_SESSION['modref'] = "";
		$cdb = new ASSIST_DB("client",$cmpcode);
		//Get active modules
		$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' AND modref = 'COMPL' AND modref IN (SELECT DISTINCT usrmodref FROM assist_".$cmpcode."_menu_modules_users)";
		$mods = $cdb->mysql_fetch_all($sql);
		if(count($mods)>0) {
			$mod_def = new COMPL_MODULE_DEFAULTS(true);
			echo "<P class=isubmit>Compliance found!</p>";
			echo "<ol>";
			//check for deliverables to be updated to In Progress if action progress > 0
			$updateDelToIP = $mod_def->updateDeliverableToIP();
			$updateLegToIP = $mod_def->updateLegislationToIP();
			$updateDelToUC = $mod_def->updateDeliverableToUC();
			$updateLegToUC = $mod_def->updateLegislationToUC();
			$completeDelOnApp = $mod_def->updateDeliverableToCompletedOnApproval();
			$completeDelWOutApp = $mod_def->updateDeliverableToCompletedWithoutApproval();
			$completeLeg = $mod_def->updateLegislationToCompleted();
			$del_status = array_merge(array(0=>"Unable to Comply"),$mod_def->mysql_fetch_value_by_id("SELECT id, client_terminology as name FROM ".$mod_def->getDBRef()."_deliverable_status","id","name"));
			if(count($del_status)==0) {
				$sql = "INSERT INTO `".$mod_def->getDBRef()."_deliverable_status` (`id`, `name`, `client_terminology`, `color`, `status`, `insertdate`, `insertuser`) VALUES
				(1, 'New', 'New', '', 1, '2011-09-11 07:19:06', '0001'),
				(2, 'In Progress', 'In Progress', '', 1, '2011-09-11 07:19:06', '0001'),
				(3, 'Completed', 'Completed', '', 1, '2011-09-11 07:19:06', '0001')";
				$mod_def->db_insert($sql);
				$del_status = array_merge(array(0=>"Unable to Comply"),$mod_def->mysql_fetch_value_by_id("SELECT id, client_terminology as name FROM ".$mod_def->getDBRef()."_deliverable_status","id","name"));
			}
			$leg_status = array_merge(array(0=>"Unable to Comply"),$mod_def->mysql_fetch_value_by_id("SELECT id, client_terminology as name FROM ".$mod_def->getDBRef()."_legislation_status","id","name"));
			if(count($leg_status)==0) {
				$sql = "INSERT INTO `".$mod_def->getDBRef()."_legislation_status` (`id`, `name`, `client_terminology`, `color`, `status`, `insertdate`, `insertuser`) VALUES
				(1, 'New', 'New', '', 1, '2011-09-11 07:19:06', '0001'),
				(2, 'In Progress', 'In Progress', '', 1, '2011-09-11 07:19:06', '0001'),
				(3, 'Completed', 'Completed', '', 1, '2011-09-11 07:19:06', '0001')";
				$mod_def->db_insert($sql);
				$leg_status = array_merge(array(0=>"Unable to Comply"),$mod_def->mysql_fetch_value_by_id("SELECT id, client_terminology as name FROM ".$mod_def->getDBRef()."_legislation_status","id","name"));
			}
/**** 
	UPDATE DELIVERABLE TO COMPLETE IF ACTIONS ARE ALL COMPLETED:
		Deliverable: status != Completed (3)
		Action: average progress = 100
		Action: all status = completed (3)
		Action: all bitwise = Approved (32) - if $completeDelOnApp = true
*****/
			echo "<li>Complete deliverables: ";
			if($completeDelOnApp===true || $completeDelWOutApp===true) {
				echo "Yes".($completeDelOnApp===true ? " - on Approval" : " - ignore Approval status");
				$completedDeliverables = array();
				$base_sql = "SELECT DISTINCT D.id, count(A.id) as acount, D.deliverable_status as old
							FROM `".$mod_def->getDBRef()."_deliverable` D
							INNER JOIN `".$mod_def->getDBRef()."_legislation` L
							ON L.id = D.legislation
							INNER JOIN `".$mod_def->getDBRef()."_action` A
							ON A.deliverable_id = D.id
							WHERE D.deliverable_status <> 3
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L")."
							AND ".Action::getStatusSQLForWhere("A");
				//Get list of Deliverables with average action progress = 100 and deliverable status != Completed
				$sql = $base_sql." GROUP BY D.id HAVING avg(A.progress) = 100";
				//echo "<p>Source: ".$sql;
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$srcKeys = array_keys($sourceList);
				//Get elimination list of Deliverables with an Action status != completed or Action bitwise status != approval
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (D.id IN (".implode(", ",$srcKeys)."))" : "")." AND (".($completeDelOnApp===true ? "A.actionstatus & ".Action::APPROVED." <> ".Action::APPROVED." OR " : "")."A.status <> 3) GROUP BY D.id";
				//echo "<p>Elimination: ".$sql;
				$eliminationList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Are there any deliverables to update?
				//$completeDel = array_diff_key($sourceList,$eliminationList);
				foreach($eliminationList as $i => $r) { unset($sourceList[$i]); } 
				if(count($sourceList)>0) {
					echo " - Deliverables marked as completed: <ul>";
					$currentstatus = 3;
					//foreach deliverable from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						//if(!isset($eliminationList[$d]) || $eliminationList[$d]['acount']==0) {
							//set variables
							$completedDeliverables[] = $d;
							$old = $r['old'];
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>"Ignite Assist",
								'currentstatus'=>$del_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$del_status[$old],
									'to'=>$del_status[$currentstatus],
								),
							);
							//update deliverable status
							$sql = "UPDATE ".$mod_def->getDBRef()."_deliverable SET deliverable_status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_deliverable_update SET 
									deliverable_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]." :: ".serialize($changes)."</li>";
						//}
					}
					echo "</ul>";
				} else {
					echo " - No deliverable meet the criteria to be closed.";
				}
			} else {
				echo "No";
			}

/**** 
	UPDATE DELIVERABLE TO Unable to Comply IF An Action has been updated to UtC:
		Deliverable: ID not in $completedDeliverables updated above
		Deliverable: status != Unable to Comply (0)
		Action: status = Unable to Comply (0)
*****/
			echo "<li>Update deliverables to Unable to Comply: ";
			$unableToComplyDeliverables = array();
			if($updateDelToUC===true) {
				echo "Yes";
				$base_sql = "SELECT DISTINCT D.id, count(A.id) as acount, D.deliverable_status as old
							FROM `".$mod_def->getDBRef()."_deliverable` D
							INNER JOIN `".$mod_def->getDBRef()."_legislation` L
							ON L.id = D.legislation
							INNER JOIN `".$mod_def->getDBRef()."_action` A
							ON A.deliverable_id = D.id
							WHERE (D.deliverable_status <> 0)
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L")."
							AND ".Action::getStatusSQLForWhere("A")."
							".(count($completedDeliverables)>0 ? "AND D.id NOT IN (".implode(", ",$completedDeliverables).")" : "");
				//Get list of Deliverables with action progress > 0 with Deliverable status either New or Completed
				$sql = $base_sql." AND (A.status = 0) GROUP BY D.id";
				//echo "<P>Source: ".$sql;
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$unableToComplyDeliverables = array_keys($sourceList);
				if(count($sourceList)>0) {
					echo " - Deliverables updated: <ul>";
					$currentstatus = 0;
					//foreach deliverable from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						//if(!isset($eliminationList[$d])) {
							//set variables
							$old = $r['old'];
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>$mod_def->getSiteName(),
								'currentstatus'=>$del_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$del_status[$old],
									'to'=>$del_status[$currentstatus],
								),
							);
							//update deliverable status
							$sql = "UPDATE ".$mod_def->getDBRef()."_deliverable SET deliverable_status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_deliverable_update SET 
									deliverable_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]." :: ".serialize($changes)."</li>";
						//}
					}
					echo "</ul>";
				} else {
					echo " - No deliverable meet the criteria to be updated.";
				}
			} else {
				echo "No";
			}
/**** 
	UPDATE DELIVERABLE TO In Progress IF An Action has started and not ACTIONS ARE ALL COMPLETED:
		Deliverable: ID not in $completedDeliverables || $unableToComplyDeliverables updated above
		Deliverable: status = New (1) OR status = Completed (3)
		Action: sum progress > 0
		Action: not all status = completed (3)
		Action: not all bitwise = Approved (32) - if $completeDelOnApp = true
*****/
$updatedDeliverables = array_merge($completedDeliverables, $unableToComplyDeliverables);
			echo "<li>Update deliverables to In Progress: ";
			if($updateDelToIP===true) {
				echo "Yes";
				$base_sql = "SELECT DISTINCT D.id, count(A.id) as acount, D.deliverable_status as old
							FROM `".$mod_def->getDBRef()."_deliverable` D
							INNER JOIN `".$mod_def->getDBRef()."_legislation` L
							ON L.id = D.legislation
							INNER JOIN `".$mod_def->getDBRef()."_action` A
							ON A.deliverable_id = D.id
							WHERE (D.deliverable_status = 3 OR D.deliverable_status = 1)
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L")."
							AND ".Action::getStatusSQLForWhere("A")."
							".(count($updatedDeliverables)>0 ? "AND D.id NOT IN (".implode(", ",$updatedDeliverables).")" : "");
				//Get list of Deliverables with action progress > 0 with Deliverable status either New or Completed
				$sql = $base_sql." AND (A.status <> 0 AND (A.progress > 0 OR A.status <> 1)) GROUP BY D.id";
				//echo "<P>Source: ".$sql;
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$srcKeys = array_keys($sourceList);
				//Get first elimination list of Deliverables with an average Action progress = 100
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (D.id IN (".implode(", ",$srcKeys)."))" : "")." GROUP BY D.id HAVING avg(A.progress) = 100";
				//echo "<P>Elimination1: ".$sql;
				$eliminationList1 = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Get second elimination list of Deliverables with an Action status != completed or Action bitwise status != approval
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (D.id IN (".implode(", ",$srcKeys)."))" : "")." AND (".($completeDelOnApp===true ? "A.actionstatus & ".Action::APPROVED." <> ".Action::APPROVED." AND " : "")."A.status <> 3) GROUP BY D.id";
				//echo "<P>Elimination2: ".$sql;
				$eliminationList2 = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Are there any deliverables to update?
				//$srcKeys = array_keys($sourceList);
				//$elimKeys = array_keys($eliminationList);
				$eliminationList = array_diff_key($eliminationList1,$eliminationList2);
				/*foreach($eliminationList1 as $i => $r) {
					if(!isset($eliminationList2[$i])) {
						//$eliminationList[$i] = $r;
						unset($sourceList[$i]);
					}
				}*/
				$updateDel = array_diff_key($sourceList,$eliminationList);
				if(count($updateDel)>0) {
					echo " - Deliverables updated: <ul>";
					$currentstatus = 2;
					//foreach deliverable from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						if(!isset($eliminationList[$d])) {
							//set variables
							$old = $r['old'];
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>"Ignite Assist",
								'currentstatus'=>$del_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$del_status[$old],
									'to'=>$del_status[$currentstatus],
								),
							);
							//update deliverable status
							$sql = "UPDATE ".$mod_def->getDBRef()."_deliverable SET deliverable_status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_deliverable_update SET 
									deliverable_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>D".$d.": ".$del_status[$old]." => ".$del_status[$currentstatus]." :: ".serialize($changes)."</li>";
						}
					}
					echo "</ul>";
				} else {
					echo " - No deliverable meet the criteria to be updated.";
				}
			} else {
				echo "No";
			}
/**** 
	UPDATE LEGISLATION TO COMPLETE IF DELIVERABLES ARE ALL COMPLETED:
		Legislation: status != Completed (3)
		Deliverable: all status = completed (3)
*****/
			echo "<li>Complete legislations: ";
			$completedLegislations = array();
			if($completeLeg===true) {
				echo "Yes";
				$base_sql = "SELECT DISTINCT L.id, count(D.id) as acount, L.status as old
							FROM `".$mod_def->getDBRef()."_legislation` L
							INNER JOIN `".$mod_def->getDBRef()."_deliverable` D
							ON L.id = D.legislation
							WHERE L.status <> 3
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L");
				//Get list of legislations with status != completed
				$sql = $base_sql." GROUP BY L.id";
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$srcKeys = array_keys($sourceList);
				//Get elimination list of Legislations with Deliverable status != completed 
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (L.id IN (".implode(", ",$srcKeys)."))" : "")." AND (D.deliverable_status <> 3) GROUP BY L.id";
				$eliminationList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Are there any deliverables to update?
				$completeLeg = array_diff_key($sourceList,$eliminationList);
				if(count($completeLeg)>0) {
					echo " - Legislations marked as completed: <ul>";
					$currentstatus = 3;
					//foreach legislation from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						if(!isset($eliminationList[$d]) || $eliminationList[$d]['acount']==0) {
							//set variables
							$completedLegislations[] = $d;
							$old = $r['old'];
							$oldstatus = isset($leg_status[$old]) ? $leg_status[$old] : "New";
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>"Ignite Assist",
								'currentstatus'=>$leg_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$oldstatus,
									'to'=>$leg_status[$currentstatus],
								),
							);
							//update legislation status
							$sql = "UPDATE ".$mod_def->getDBRef()."_legislation SET status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_legislation_update SET 
									legislation_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>L".$d.": ".$oldstatus." => ".$leg_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>L".$d.": ".$leg_status[$old]." => ".$leg_status[$currentstatus]." :: ".serialize($changes)."</li>";
						}
					}
					echo "</ul>";
				} else {
					echo " - No legislations meet the criteria to be marked as completed.";
				}
			} else {
				echo "No";
			}			
/**** 
	UPDATE LEGISLATION TO UNABLE TO COMPLY IF ANY DELIVERABLE IS UNABLE TO COMPLY:
		Legislation: status != Unable to Comply (0)
		Deliverable: status = Unable to Comply (0)
*****/
			echo "<li>Legislations - Unable to Comply: ";
			$unableToComplyLegislations = array();
			if($updateLegToUC===true) {
				echo "Yes";
				$base_sql = "SELECT DISTINCT L.id, count(D.id) as acount, L.status as old
							FROM `".$mod_def->getDBRef()."_legislation` L
							INNER JOIN `".$mod_def->getDBRef()."_deliverable` D
							ON L.id = D.legislation
							WHERE L.status <> 0
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L");
				//Get list of legislations with status != completed
				$sql = $base_sql." AND D.deliverable_status = 0 GROUP BY L.id";
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$unableToComplyLegislations = array_keys($sourceList);
				//Are there any deliverables to update?
				if(count($unableToComplyLegislations)>0) {
					echo " - Legislations marked as Unable to Comply: <ul>";
					$currentstatus = 0;
					//foreach legislation from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						//if(!isset($eliminationList[$d]) || $eliminationList[$d]['acount']==0) {
							//set variables
							//$completedLegislations[] = $d;
							$old = $r['old'];
							$oldstatus = isset($leg_status[$old]) ? $leg_status[$old] : "New";
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>$mod_def->getSiteName(),
								'currentstatus'=>$leg_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$oldstatus,
									'to'=>$leg_status[$currentstatus],
								),
							);
							//update legislation status
							$sql = "UPDATE ".$mod_def->getDBRef()."_legislation SET status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_legislation_update SET 
									legislation_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>L".$d.": ".$oldstatus." => ".$leg_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>L".$d.": ".$leg_status[$old]." => ".$leg_status[$currentstatus]." :: ".serialize($changes)."</li>";
						//}
					}
					echo "</ul>";
				} else {
					echo " - No legislations meet the criteria to be marked as Unable to Comply.";
				}
			} else {
				echo "No";
			}	
/**** 
	UPDATE LEGISLATION TO In Progress IF A DELIVERABLE is started:
		Legislation: status = Completed (3) OR status = New (1)
		Deliverable: not (all status = completed (3)) and at least 1 deliverable status != 1
*****/
$updatedLegislations = array_merge($completedLegislations, $unableToComplyLegislations);
			echo "<li>Update legislations to In Progress: ";
			if($updateLegToIP===true) {
				echo "Yes";
				$base_sql = "SELECT DISTINCT L.id, count(D.id) as acount, L.status as old
							FROM `".$mod_def->getDBRef()."_legislation` L
							INNER JOIN `".$mod_def->getDBRef()."_deliverable` D
							ON L.id = D.legislation
							LEFT OUTER JOIN ".$mod_def->getDBRef()."_legislation_status S
							ON S.id = L.status
							WHERE (L.status = 1 OR L.status = 3 OR S.id IS NULL)
							".(count($updatedLegislations)>0 ? "AND L.id NOT IN (".(implode(", ",$updatedLegislations)).")" : "")."
							AND ".Deliverable::getStatusSQLForWhere("D")."
							AND ".Legislation::getStatusSQLForWhere("L");
				//Get list of legislations with status = new or completed
				$sql = $base_sql." GROUP BY L.id HAVING avg(D.deliverable_status) > 1 ";
				$sourceList = $mod_def->mysql_fetch_all_by_id($sql,"id");
				$srcKeys = array_keys($sourceList);
				//Get elimination list of Legislations with Deliverable status = completed 
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (L.id IN (".implode(", ",$srcKeys)."))" : "")." AND (D.deliverable_status = 3) GROUP BY L.id";
				$eliminationList3 = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Get elimination list of Legislations with Deliverable status = new
				$sql = $base_sql.(count($srcKeys)>0 ? " AND (L.id IN (".implode(", ",$srcKeys)."))" : "")." AND (D.deliverable_status = 1) GROUP BY L.id";
				$eliminationList1 = $mod_def->mysql_fetch_all_by_id($sql,"id");
				//Are there any deliverables to update?
				//$upLeg = array_intersect_key($sourceList,$eliminationList);
				foreach($sourceList as $i => $r) {
					if(isset($eliminationList3[$i]) && $eliminationList3[$i]['acount']==$r['acount']) {
						unset($sourceList[$i]);
					}
					if(isset($eliminationList1[$i]) && $eliminationList1[$i]['acount']==$r['acount']) {
						unset($sourceList[$i]);
					}
				}
				if(count($sourceList)>0) {
					echo " - Legislations updated: <ul>";
					$currentstatus = 2;
					//foreach legislation from source list
					foreach($sourceList as $d => $r) {
						//if not in elimination list
						//if(!isset($eliminationList[$d]) || $eliminationList[$d]['acount']==0) {
							//set variables
							//$completedLegislations[] = $d;
							$old = $r['old'];
							$oldstatus = isset($leg_status[$old]) ? $leg_status[$old] : "New";
							$response = "Status changed by automatic update.";
							$changes = array(
								'user'=>"Ignite Assist",
								'currentstatus'=>$leg_status[$currentstatus],
								'response'=>$response,
								'deliverable_status'=>array(
									'from'=>$oldstatus,
									'to'=>$leg_status[$currentstatus],
								),
							);
							//update legislation status
							$sql = "UPDATE ".$mod_def->getDBRef()."_legislation SET status = $currentstatus WHERE id = ".$d;
							$mod_def->db_update($sql);
							//add activity log record
							$sql = "INSERT INTO ".$mod_def->getDBRef()."_legislation_update SET 
									legislation_id = ".$d.", 
									response = '$response',
									changes = '".base64_encode(serialize($changes))."',
									insertdate = '$update_log_insert_date',
									insertuser = '0000'";
							$mod_def->db_insert($sql); $ccount++;
							echo "<li>L".$d.": ".$oldstatus." => ".$leg_status[$currentstatus]."</li>";
							//testing comment
							//echo "<li>L".$d.": ".$leg_status[$old]." => ".$leg_status[$currentstatus]." :: ".serialize($changes)."</li>";
						//}
					}
					echo "</ul>";
				} else {
					echo " - No legislations meet the criteria to be marked as in progress.";
				}
			} else {
				echo "No";
			}					
			echo "</li></ol>";
			
		} else {
			echo "<span class=iinform>No Compliance module found.</span>";
		}
	} else {
		echo "<span class=idelete>No database found.</span>";
	}
	echo "</td></tr>";
}
echo "</table>

<p> $ccount compliance auto updates completed.</p>
";



ob_end_flush();


?>