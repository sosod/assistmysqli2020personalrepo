<?php
require_once("../module/autoloader.php");
error_reporting(E_ALL && ~E_DEPRECATED);

$db = new ASSIST_DB("master");


//Add record to get max log_id from assist_email_log_sendgrid_processed
$sql = "SELECT MAX(final_event_log_id) as last_log_id FROM assist_email_log_sendgrid_processed";
$last_log_id = $db->mysql_fetch_one_fld($sql, "last_log_id");

if(is_null($last_log_id)) { $last_log_id = 0; }

$sql = "SELECT * FROM assist_email_log_sendgrid WHERE id > $last_log_id LIMIT 3500"; // WHERE id > max_log_id
$rows = $db->mysql_fetch_all($sql);
$total_rows = count($rows);
$me = new ASSIST_HELPER();
$me->echoPageHeader();


//$me->arrPrint($rows);

$total_events = array();
$final_events = array();

$db_data = array();
$data = array();
$smtp_ids = array();
$drop_keys = array();//array("timestamp","email","sg_message_id");
foreach($rows as $row) {
	$src_id = $row['id'];
	$log_data = unserialize($row['log_data']);
	if(isset($log_data['ID'])) {
			if(isset($d['timestamp']) && isset($d['email']) && isset($d['smtp-id'])){
				$ts = $d['timestamp'];   //unset($d['timestamp']);
				$e = $d['email'];		//unset($d['email']);
				$si = $d['sg_message_id'];	//unset($d['smtp-id']);
				$smtp = $d['smtp-id'];
				
				$data[$smtp][$e][$ts] = array();
				foreach($d as $key => $i) {
					if(!in_array($key,$drop_keys)) {
						$data[$smtp][$e][$ts][$key] = $i;
					}
					if($key=="event") {
						if(!isset($total_events[$i])) { $total_events[$i] = 0; }
						$total_events[$i]++;
					}
				}
				$data[$smtp][$e][$ts]['src_db_id'] = $src_id;
			}
	} else {
		foreach($log_data as $d) {
			if(isset($d['timestamp']) && isset($d['email']) && isset($d['smtp-id'])){
				$ts = $d['timestamp'];   //unset($d['timestamp']);
				$e = $d['email'];		//unset($d['email']);
				$si = $d['sg_message_id'];	//unset($d['smtp-id']);
				$smtp = $d['smtp-id'];
				
				$data[$smtp][$e][$ts] = array();
				foreach($d as $key => $i) {
					if(!in_array($key,$drop_keys)) {
						$data[$smtp][$e][$ts][$key] = $i;
					}
					if($key=="event") {
						if(!isset($total_events[$i])) { $total_events[$i] = 0; }
						$total_events[$i]++;
					}
				}
				$data[$smtp][$e][$ts]['src_db_id'] = $src_id;
			}
		}
	}
}

//ksort($data,SORT_STRING);

//ASSIST_HELPER::arrPrint($data);
/*****
 * sg_message_id
 * smtp-id
 * email
 * first_date
 * last_date
 * final result
 * num_tries
 * log_data
 */

$echo = "
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>Log</th>
		</tr>
	</thead>
	<tbody>";
		foreach($data as $si=>$data1) {
			$echo .="<tr>
					<td>$si</td>
				</tr>";
			foreach($data1 as $e=>$data2) {
				$array_key = $si."_".$e;
				krsort($data2);
				if(!isset($db_data[$array_key])) {
					$db_data[$array_key] = array(
						'sg_message_id' => "",
						'smtp_id' => $si,
						'email' => $e,
						'processed_date' => "",
						'final_event_log_id' => "",
						'final_date' => "",
						'final_result' => "",
						'num_tries' => count($data2),
						'log_data' => $data2,
					);
				}
				$echo .="
				<tr>
					<td></td>
					<td>$e</td>
					<td>
						<table><tr>
							<th>Date</th>
							<th>Event</th>
							<th>Response</th>
							<th>Response Text</th>
							<th>Log data</th>
						</tr>";
					foreach($data2 as $ts => $d) {
						if(strlen($db_data[$array_key]['sg_message_id'])==0) { $db_data[$array_key]['sg_message_id'] = $d['sg_message_id']; }
						if(strlen($db_data[$array_key]['final_date'])==0) {
							$db_data[$array_key]['final_date'] = date("Y-m-d H:i:s",$ts);
							$db_data[$array_key]['final_event_log_id'] = $d['src_db_id'];
						}
						if(strlen($db_data[$array_key]['final_result'])==0) { $db_data[$array_key]['final_result'] = $d['event']; }
						if($d['event']=="processed") {
							$db_data[$array_key]['num_tries']--;
							$db_data[$array_key]['processed_date'] = date("Y-m-d H:i:s",$ts); 
						}
						$end_loop = false;
						if(($d['event']=="processed" && count($data2)==1) || $d['event']!="processed") {
							switch($d['event']) {
								case "processed":
									$d['event']="<span class=iinform>".$d['event']."</span>"; 
									break;
								case "bounce":
									$d['event']="<span class=idelete>".$d['event']."</span>";
									$end_loop = true;
									break;
							}
							if(isset($d['response'])) {
								$rx = explode(" ",$d['response']);
								$text = $d['response'];
								if($rx[0]=="250"){
									$response = "250 OK";
									$end_loop = true;
								} elseif(!is_numeric($rx[0])) {
									$response = "<span class=required>ERROR</span>";
								} else {
									$response = "<span class=required>".$rx[0]."</span>";
								}
							} elseif(isset($d['reason'])) {
								$rx = explode(" ",$d['reason']);
								$text = $d['reason'];
								if($rx[0]=="250"){
									$response = "250 OK";
									$end_loop = true;
								} elseif(!is_numeric($rx[0])) {
									$response = "<span class=required>ERROR</span>";
								} else {
									$response = "<span class=required>".$rx[0]."</span>";
								}
							} else {
								$response = "";
								$text = "";
							}
							$echo .="
							<tr>
								<td>".date("d M Y H:i:s",$ts)."</td>
							<td>".(isset($d['event']) ? $d['event'] : "")."</td>
							<td>".$response."</td>
							<td>".$text."</td>
							<td><ul>"; 
							foreach($d as $k=>$v){
								$echo.= "<li>".$k." => ".$v."</li>";
							} 
							$echo.="</ul></td>
							</tr>";
						}
						if($end_loop) {
							//break;
						}
					}
					
				$echo.= "	
					</table></td>
				";
			}
		}
		
		
		/*foreach($data as $si => $data1) {
			foreach($data1 as $ts => $data2) {
				foreach($data2 as $e => $d) {
					
					echo "
					<tr>
						<td>".$si."</td>
						<td>".date("d M Y H:i:s",$ts)."</td>
						<td>".$e."</td>
						<td>".(isset($d['event']) ? $d['event'] : "")."</td>
						<td>".(isset($d['response']) ? $d['response'] : "")."</td>
						<td><pre>"; print_r($d); echo "</pre></td>
					</tr>";
				}
			}
		}*/
$echo.="	</tbody>
</table>";
echo $echo;

//ASSIST_HELPER::arrPrint($db_data);

/*
 * */
//echo "<h1>Insert</h1>";
$sql = "SELECT DISTINCT CONCAT(smtp_id,'_',email) as array_key FROM assist_email_log_sendgrid_processed";
$already_processed = $db->mysql_fetch_all_by_value($sql,"array_key"); 

$update = array();

$results = array('added'=>0,'updated'=>0);

foreach($db_data as $key => $d) {
	if(in_array($key,$already_processed)) {
		$update[$key] = $d;
	} else {
		$sql = "INSERT INTO assist_email_log_sendgrid_processed SET ";
		$insert_data = array();
		foreach($d as $key => $x) {
			if($key == "log_data") { $x = base64_encode(serialize($x)); }
			$insert_data[] = "$key = '$x'";
			if($key=="final_result") {
				if(!isset($final_events[$x])) { $final_events[$x] = 0; }
				$final_events[$x]++;
			}
		}
		$sql.= implode(", ",$insert_data);
		//echo "<p>".$sql;
		$db->db_insert($sql); 
		$results['added']++;
	}
}
//echo "<h1>Update</h1>";
if(count($update)>0) {
	$update_keys = array_keys($update);
	$sql = "SELECT * FROM assist_email_log_sendgrid_processed WHERE CONCAT(smtp_id,'_',email) IN ('".implode("','",$update_keys)."')";
	$existing_data = $db->mysql_fetch_all_by_id($sql,"id");
	foreach($existing_data as $id => $ed) {
		$smi = $ed['smtp_id']."_".$ed['email'];
		$nd = $update[$smi];
		$elog = unserialize(base64_decode($ed['log_data']));
		$new_log = array_merge($nd['log_data'],$ed);
		ksort($new_log);
		$update_data = $ed;
		$update_data['log_data'] = $new_log;
		$update_data['final_date'] = $nd['final_date'];
		$update_data['final_result'] = $nd['final_result'];
		$update_data['final_event_log_id'] = $nd['final_event_log_id'];
		$update_data['num_tries'] = $ed['num_tries']+$nd['num_tries'];

		$sql = "UPDATE assist_email_log_sendgrid_processed SET ";
		$insert_data = array();
		foreach($d as $key => $x) {
			if($key == "log_data") { $x = base64_encode(serialize($x)); }
			$insert_data[] = "$key = '$x'";
		}
		$sql.= implode(", ",$insert_data);
		$sql.= " WHERE id = $id ";
		//echo "<p>".$sql;
		$db->db_update($sql);
		$results['updated']++;
	}
}


echo "<h2>Last Processed Log ID</h2>";
echo "<p>".$last_log_id."</p>";

echo "<h2>All Events</h2>";
ASSIST_HELPER::arrPrint($total_events);
echo "<h2>Final Events</h2>";
ASSIST_HELPER::arrPrint($final_events);
echo "<h2>DB Activities</h2>";
ASSIST_HELPER::arrPrint($results);
echo "<h2>Total Rows Processed</h2>";
echo "<p>".$total_rows."</p>";
















/* */
?>