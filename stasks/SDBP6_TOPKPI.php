<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
include("../inc_status.php");
//$assiststatus comes from inc_status included above - it sets the status of the servers for maintenance purposes
/** @var CHAR $assiststatus - comes from /inc_status.php */
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP6_TOPKPI not executed.");
}
require_once("../module/autoloader.php");
include_once("../SDBP6/class/sdbp6_topkpi.php");
$helper = new ASSIST_HELPER();
/**********************
 * Setup variables
 */
//Who am I for logging
$_SESSION['tid'] = "IA";
$_SESSION['tkn'] = "Assist Automated Tasks";

//Which server am I operating on (for DB Connections)
$server = "LOCAL";

//Support user emails
$support_email = "helpdesk@ignite4u.co.za";
$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
$IRS_helpdesk = "helpdesk@igniteassist.co.za";
$AIT_helpdesk = "helpdesk@actionassist.co.za";
$helpdesk_emails = array($support_email,$IAS_helpdesk,$IRS_helpdesk,$AIT_helpdesk);
$today = $_SERVER["REQUEST_TIME"];

//Module variables
$source_modules = array("SDBP6");
$mod_table = array('SDBP6'=>"top_results");	//even though the time table is the same for each module, set it up for future modules where it might be different
$class_names = array('SDBP6'=>"SDBP6_TOPKPI");
$doctitle = "SDBIP v6 Automated Top Layer Updates";
$totalactions = array();
foreach($source_modules as $modloc) {
	$totalactions[$modloc] = array(
			'updates'=>0
	);
}

$result1 = $helper->getFixedPageHeader();
$result2 = "<h1>".$doctitle."</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th colspan='2'>Company</th><th>Action</th></tr>";


$cmpcode = "";
$db = new ASSIST_DB("master");
$exclusion_databases = $db->getExclusionDatabases();
$support_databases = $db->getAITSupportDatabases();
foreach($support_databases as $sc => $s) {
	$exclusion_databases[] = $sc;
}

error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);

//GET ARRAY OF Companies
$company = array();
$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y'";
$company = $db->mysql_fetch_value_by_id($sql,"cmpcode","cmpname");

//get array of databases
$dbs = array();
$db_prefix = $db->getDBPrefix();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $db->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}


//ASSIST_HELPER::arrPrint($company);
//ASSIST_HELPER::arrPrint($dbs);

//$company = array("MOP0001"=>"MOP0001");

foreach($company as $cmpcode => $cmpname) {
//	echo "<hr /><h1 class='green'>Helllloooo $cmpcode from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

	//don't act on the "BLANK" module - that is purely for holding empty modules for creating modules on client databases
	if(!in_array(strtoupper($cmpcode),$exclusion_databases)) {
		$result2 .= "
<tr><td class='b'>$cmpcode</td><td class='b'>$cmpname</td>
<td>";
		//check that client exists in database list & change the DB type to client
		$db_name = strtolower($db_prefix.$cmpcode);
		if(in_array($db_name, $dbs)) {
			$client_db = new ASSIST_DB("client", $cmpcode);
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);	//reset error_reporting
			foreach($source_modules as $modloc) {
				$result2 .= "
<p class=b>".$modloc."</p><ul>";
				//check if module exists
				$time_table = $mod_table[$modloc];
				$client_modules = $client_db->checkModuleExistsByModuleLocation($modloc, $time_table);
				$client_modules2 = $client_db->checkModuleExistsByModuleLocation($modloc, "setup_target_types");

				//if exists then
				if($client_modules !== false && is_array($client_modules) && count($client_modules) > 0 && $client_modules2 !== false && is_array($client_modules2) && count($client_modules2) > 0) {
					$result2 .= "<li class='green b'>Module found ".count($client_modules)." time(s) on the DB</li>";
					foreach($client_modules as $modref => $mod) {
						if(isset($mod['table_found']) && $mod['table_found']===true) {
//	echo "<hr /><h1 class='red'>Helllloooo $modref from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
							$_SESSION['modref'] = $modref;
							$_SESSION['cc'] = strtolower($cmpcode);
							$result2.="<li class='u'>Acting on ".$mod['modtext']." [$modref]</li>";
							$_REQUEST['autojob'] = true;
							$_REQUEST['action'] = "GO";
							
							$sql = "DESCRIBE assist_".strtolower($cmpcode)."_".strtolower($modref)."_kpi_results";
							$cols = $client_db->mysql_fetch_all($sql);
							$found = false;
							foreach($cols as $c => $col) {
								if($col['Field']=="kpir_kpi_id") {
									$found = true;
								}
							}
							if($found) {
								$sdbipObject = new SDBP6_SDBIP($modref,true,$cmpcode);
								$all_sdbip_details = $sdbipObject->getAllActivatedSDBIPDetails(true);
								$autojob = true;
								//$myObject = new SDBP6_TOPKPI($modref,0,true);
								if(count($all_sdbip_details)>0) {
									foreach($all_sdbip_details as $sdbip_id => $sdbip_details) {
//										echo "<hr /><h1 class='orange'>Helllloooo $sdbip_id from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
										include("../SDBP6/admin_top_auto_force.php");
//										echo "<hr /><h2 class='orange'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h2><hr />";
//										echo "SUCCESS"; ASSIST_HELPER::arrPrint($kpi_processed_successfully);
//										echo "<hr />TARGET ERRORS";
//										ASSIST_HELPER::arrPrint($target_errors_found);
//										echo "<hr />ACTUAL ERRORS";
//										ASSIST_HELPER::arrPrint($actual_errors_found);
//										echo "<hr /><h3 class='orange'>TOP KEYS from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h3><hr />";
//										ASSIST_HELPER::arrPrint($top_keys);
//										echo "<hr />";

										$result2.="<li><span class='u'>".$sdbip_details['name'].":</span> ".$count." change(s) made.</li>";
									}
								}
							} else {
								$result2.="<li class='u'>Error!  Bad table structure found.</li>";
							}
							$count = 0;
						} else {
							$result2.="<li class='red'>ALERT! ".$mod['modtext']." [$modref] table $time_table <span class='b'>NOT</span> found.</li>";
						}
					}
				} else {
					$result2 .= "<li class='orange'>Module NOT found on the DB</li>";
				}
				$result2 .= "</ul>";
			}
			unset($client_db);
		} else {
			$result2 .= "<p class=red>Client database not found.</p>";
		}
		$result2 .= "</td></tr>";
	}
}

$result2.="</table>";


$result2.= "<h3 class=green>Done!</h3>";
$result2.= "<p>".date("d F Y H:i:s")."</p>";


//SAVE RESULTS

//NOTIFY JANET [AIT]


echo $result1;
echo $result2;

//WRITE RESULTS TO FILE
$echo = "";
$echo.= $result1;
$echo.= $result2."</body></html>";
//WRITE DATA TO FILE
$filename = "Results/SDBP6_TOPKPI_".date("Ymd_His").".html";
$file = fopen($filename,"w");
fwrite($file,$echo."\n");
fclose($file);

//NOTIFY ME OF CLOSURES
$_SESSION['modref'] = 'SDBP6_TOPKPI';
//$to = "janet@actionassist.co.za";
unset($to);
$subject = "SDBP6 Automatic Top Layer KPI Functions";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
/*$em = new ASSIST_EMAIL($to,$subject,$message,"HTML");
	$em->setSenderUserID("AUTO");
	$em->setSenderCmpCode("IASSIST");
	$em->setRecipientUserID("0001");

$em->sendEmail();
*/
include("cron_job_send_email.php");


?>