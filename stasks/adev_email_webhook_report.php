<?php
require_once("../module/autoloader.php");

//error_reporting(-1);
$db = new ASSIST_DB("master");

$sql = "
SELECT p.*, i.subject FROM assist_email_log_sendgrid_processed p
LEFT OUTER JOIN assist_email_log_internal i
ON i.smtp_id = p.smtp_id
WHERE p.final_date > '2017-07-01 00:00:00' 
AND p.final_date < '2018-01-10 12:00:00' 
AND p.email LIKE 'velly%' 
ORDER BY p.final_date DESC";
$rows = $db->mysql_fetch_all($sql);

$me = new ASSIST_HELPER();
$me->echoPageHeader();


$h = $rows[0];
$headings = array_keys($h);

$headings = array(
	'id'=>"Log ID",
	'smtp_id'=>"Server SMTP Ref",
	'email'=>"Recipient",
	'subject'=>"Email Subject",
	'final_result'=>"Email Result",
	'final_date'=>"Email Result Date",
	
);
//'log_data'=>"Complete Log Data",

?>
<style type=text/css>
thead th {
	background-color: #FFFFFF;
	border: 1px solid #ababab;
	color: #000099;
}
</style>
<h1>Assist Email Log Report</h1>
<p>All emails sent to velly@gteda.co.za between 1 July 2017 00:00 and 10 Jan 2018 12:00.</p>
<table>
	<thead>
		<tr>
<?php
foreach($headings as $fld => $head) {
echo "<th>".$head."</th>";
}
?>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($rows as $row) {
echo "<tr>";
foreach($headings as $fld=>$head) {
	if($fld=="log_data") {
		echo "<td><pre>";
		print_r(unserialize(base64_decode($row[$fld])));
		echo "</pre></td>";
	} elseif($fld=="final_result") {
		echo "<td>";
		switch($row[$fld]) {
			case "delivered": echo "<span class=green>Delivered Successfully</p>"; break;
			case "bounce": echo "<span class=orange>Bounced</p>"; break;
			default: echo "<span class=red>".$row[$fld]."</p>"; break;
		}
		echo "</td>";
	} elseif($fld=="smtp_id") {
		$v = explode("@",$row[$fld]);
		$v[0] = str_replace(".","<br />.",$v[0]);
		echo "<td>".implode("<br />@",$v)."</td>";
	} elseif($fld=="subject") {
		echo "<td>".base64_decode($row[$fld])."</td>";
	} elseif($fld=="final_date") {
		echo "<td>".date("d M Y H:i:s",strtotime($row[$fld]))."</td>";
	} else {
		echo "<td>".$row[$fld]."</td>";
	}
}
echo "</tr>";
		}
		?>
	</tbody>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>