<?php
require_once("../module/autoloader.php");


$json = file_get_contents('php://input');
$req = json_decode($json, true);

//$req = $_REQUEST;

$db = new ASSIST_DB("master");
$db_remote = new ASSIST_DB_REMOTE("master");

//$sql = "INSERT INTO assist_email_log_inbound_sendgrid VALUES (null, now(), '".serialize($action)."','')";
//$db->db_insert($sql);

$sql = "INSERT INTO assist_slack_inbound VALUES (null, now(), '".serialize($req)."')";
$db->db_insert($sql);

$text = $req['text'];
$text = explode(" ",$text);
$device = $text[1];
$type = substr($text[3],0,-1);

$sql = "INSERT INTO assist_alertra_log (log_date,log_data,log_type,log_device) VALUES (now(),'".serialize($req)."','$type','$device')";
$db->db_insert($sql);
$db_remote->db_insert($sql);

//$sql = "INSERT INTO assist_email_log_inbound_sendgrid_raw VALUES (null, now(), '".base64_encode($json)."')";
//$db->db_insert($sql);


$emailObject = new ASSIST_EMAIL();
$emailObject->setContentType("HTML");
$emailObject->setRecipient("janet@actionassist.co.za");
$emailObject->setSubject("New slack message recorded by Zapier");
$message = date("d F Y H:i:s").": $device is $type <br /><br /><ul>";
foreach($req as $fld => $r) {
	$message.= "<li>[".$fld."] => ".$r.";</li>";
}
$message.="</ul>";
$emailObject->setBody($message);
$emailObject->sendEmail();

?>