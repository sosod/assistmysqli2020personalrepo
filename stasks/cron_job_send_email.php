<?php

/** Generic Assist Classes **/
include_once("../library/class/assist_helper.php");
include_once("../library/class/assist_dbconn.php");
include_once("../library/class/assist_db.php");
include_once("../library/class/assist_email.php");
include_once("../library/class/assist.php");
include_once("../library/class/assist_user.php");
include_once("../library/class/assist_root.php");
include_once("../library/class/assist_module_helper.php");



//CALLED AS https://..../stasks/cron_job_send_email.php?mr=MODREF&message_type=AS_IS&fn=MESSAGE&sub=SUBJECT
if(!isset($subject)) {
	$_SESSION['modref'] = $_REQUEST['mr'];
	$message_type = $_REQUEST['message_type'];
	$filename = $_REQUEST['fn'];
	$subject = $_REQUEST['sub'];

	if($message_type=="link") {
		$message = "<p>Results: <a href=http://".$_SERVER["HTTP_HOST"]."/stasks/".$filename.">".$filename."</a></p>";
	} else {
		$message = $filename;
	}
}

if(!isset($to) || $to == "janet@actionassist.co.za" || $to == "janet@igniteassist.co.za") {
	$to = "janet@actionassist.co.za";
	$cc = "faranath@gmail.com";
} else {
	$cc = "janet@actionassist.co.za";
	$bcc = "faranath@gmail.com";
}

$em = new ASSIST_EMAIL($to,$subject,$message,"HTML",$cc);
if(isset($bcc) && strlen($bcc)>0) {
	$em->setBCC($bcc);
}
	$em->setSenderUserID("AUTO");
	$em->setSenderCmpCode("IASSIST");
	$em->setRecipientUserID("0001");
	$em->sendEmail();
unset($to);
unset($cc);

?>