<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP5_reminder not executed.");
}


/** Generic Assist Classes **/
include("../library/class/assist_helper.php");
$me = new ASSIST_HELPER();
include("../library/class/assist_dbconn.php");
include("../library/class/assist_db.php");
include("../library/class/assist_email.php");
include("../library/class/assist.php");
include("../library/class/assist_user.php");
include("../library/class/assist_root.php");
include("../library/class/assist_module_helper.php");

/** SDBP5 Classes **/
include("../SDBP5/class/sdbp5_helper.php");
include("../SDBP5/class/sdbp5_time.php");

	$support_email = "helpdesk@ignite4u.co.za";
	//$support_email = "igniteassist_dev@gmail.com";
	$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
	//$IAS_helpdesk = "igniteassistdev@gmail.com";
	$IRS_helpdesk = "helpdesk@igniteassist.co.za";
$helpdesk_emails = array($support_email,$IAS_helpdesk,$IRS_helpdesk,"helpdesk@actionassist.co.za");
	$from = "no-reply@ignite4u.co.za";
	$header1 = "From: ".$from."\r\nReply-to: ".$from."\r\n";
$today = $_SERVER["REQUEST_TIME"];//+(31*24*60*60);//extra time for development purposes


//error_reporting(-1);
$arr_modref = array("sdp13","sdp14","sdp15","sdp15i","sdp16","sdp16i","sdp17","sdp17i","sdp18","sdp18i");
$mod_table = "list_time";
$doctitle = "SDBIP v5 & v5B & IKPI1";
$mod_titles = array('sdp13'=>"SDBIP 2013/2014",'sdp14'=>"SDBIP 2014/2015",'sdp15'=>"SDBIP 2015/2016",'sdp15i'=>"Individual KPIs 2015/2016",
	'sdp16'=>"SDBIP 2016/2017",
	'sdp16i'=>"Individual KPIs 2016/2017",
	'sdp17'=>"SDBIP 2017/2018",
	'sdp17i'=>"Individual KPIs 2017/2018",
	'sdp18'=>"SDBIP 2018/2019",
	'sdp18i'=>"Individual KPIs 2018/2019",
);
$totalactions = 0;

$time_categories = array("primary","secondary","finance");

$tkid = "IA";
$_SESSION['tkn'] = "Assist Automated Tasks";
$_SESSION['tid'] = "IA";

$result1 = "<style type=text/css>";
$result1.= "</style>";
$result2 = "<h1>$doctitle ~ Scheduled Tasks: Reminders</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th>CMPCODE</th><th>Action</th></tr>";


$cmpcode = "";
//GET ARRAY OF CMPCODE
$db = new ASSIST_DB("master");

$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y' AND cmp_is_demo = 0";
$company = $db->mysql_fetch_all_by_value($sql,"cmpcode");


$dbs = array();
$db->db_connect();
$link = $db->getConn();
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}


//FOREACH COMPANY
foreach($company as $cmpcode) {
	set_time_limit(1800);
	$cmpcode = strtolower($cmpcode);
	$db_name = $db->getOtherDBName($cmpcode);
	//IF DATABASE EXISTS
	$result2.= "<tr><td>".strtoupper($cmpcode)."</td><td><ul>";
	if (in_array($db_name, $dbs, true)) {
		$result2.= "<li>Database found.</li>";
		foreach($arr_modref as $modref) {
			$_SESSION['cc'] = $cmpcode;
			$_SESSION['modref'] = $modref;
			$modtitle = $mod_titles[$modref];
			//CHECK IF MODULE/TABLE EXISTS
			$dbObj = new ASSIST_DB("client",$cmpcode);
			$module_exists = $dbObj->checkModuleExists($modref,$mod_table);
			if($module_exists) {
				$result2.="<li>".strtoupper($modref)." exists</li>";
				$dbObj->setDBRef($modref);
				$dbref = $dbObj->getDBRef();
				$reseller = $me->getReseller($cmpcode);
				$reseller_info = $me->getBPADetails($reseller);
				//Get time with reminders
				$timeObj = new SDBP5_TIME($cmpcode,true);
				$timeObj->setDBRef($modref,$cmpcode);
				$time = array();
				$tcate = array();
				foreach($time_categories as $cate) {
					$tcate[$cate] = $timeObj->getOpenTime($cate," start_date < '".date("Y-m-d H:i:s")."' AND rem_".$cate." > 86400 AND rem_".$cate." <= ".$today);
					foreach($tcate[$cate] as $ti=>$t) {
						if(!isset($time[$ti])) { $time[$ti] = $t; }
					}
				}
				if(count($time)>0) {
					$result2.="<li>".count($time)." open time periods with current reminders found.</li>";
					$uc = 0;
					//CHECK FOR USERS FOR EACH TIME & Access SECTION - primary, secondary, toplayer, finance
					$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_user_admins a
							INNER JOIN assist_".$cmpcode."_timekeep t
							   ON t.tkid = a.tkid AND t.tkstatus = 1
							INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
							   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '".strtoupper($modref)."'
							WHERE a.active = true
							AND a.type IN ('DIR','SUB','OWN')
							AND a.tkid NOT IN (
							   SELECT tkid FROM ".$dbref."_user_access
							   WHERE active = true
							   AND second = true
							)";
					$users['primary'] = $dbObj->mysql_fetch_all($sql);
					$uc+=count($users['primary']);
					$sql = "SELECT DISTINCT t.tkemail, t.tkid FROM ".$dbref."_user_admins a
							INNER JOIN assist_".$cmpcode."_timekeep t
							   ON t.tkid = a.tkid AND t.tkstatus = 1
							INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
							   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '".strtoupper($modref)."'
							WHERE a.active = true
							AND a.type IN ('DIR','SUB','OWN')
							AND a.tkid IN (
							   SELECT tkid FROM ".$dbref."_user_access
							   WHERE active = true
							   AND second = true
							)";
					$users['secondary'] = $dbObj->mysql_fetch_value_by_id($sql,"tkid","tkemail");
					$uc+=count($users['secondary']);
					$sql = "SELECT DISTINCT t.tkemail, t.tkid FROM ".$dbref."_user_admins a
							INNER JOIN assist_".$cmpcode."_timekeep t
							   ON t.tkid = a.tkid AND t.tkstatus = 1
							INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
							   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '".strtoupper($modref)."'
							WHERE a.active = true
							AND a.type IN ('TOP')";
					$users['toplayer'] = $dbObj->mysql_fetch_value_by_id($sql,"tkid","tkemail");
					$uc+=count($users['toplayer']);
					$sql = "SELECT DISTINCT t.tkemail, t.tkid FROM ".$dbref."_user_access a
							INNER JOIN assist_".$cmpcode."_timekeep t
							   ON t.tkid = a.tkid AND t.tkstatus = 1
							INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
							   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '".strtoupper($modref)."'
							WHERE a.active = true
							AND a.finance = true";
					$users['finance'] = $dbObj->mysql_fetch_value_by_id($sql,"tkid","tkemail");
					$uc+=count($users['finance']);
					if($uc>0) {
						$result2.="<li>".$uc." email recipients found.</li>";
						foreach($time as $ti=>$t) {
							$actions = array();
							$log_change = array();
							$resets = array();
							foreach($users as $key=>$emails) {
								$time_fld = $key;
								switch($key) {
									case "primary":
									case "secondary":
										$section_head = "Departmental SDBIP";
										break;
									case "toplayer":
										$section_head = "Top Layer SDBIP";
										$time_fld = "secondary";
										break;
									case "finance":
										$section_head = "Financials";
										break;
								}
								if(isset($tcate[$time_fld][$ti])) {
									$cc = "";
									if(count($emails)>0 && $t['rem_'.$time_fld]>0 && $t['rem_'.$time_fld]<=$today && $t['close_'.$time_fld]>$today && $t['active_'.$time_fld]) {
										//$emails[] = $IRS_helpdesk;
										foreach($emails as $ri=> $to) {
											$subject = "$modtitle - $section_head Closure - ".date("M Y",$t['end'])."";
											$message = "<font face=arial>Kindly note that $modtitle - $section_head - <span style=\"font-weight: bold;text-decoration: underline;\">".date("F Y",$t['end'])."</span> will close on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y",$t['close_'.$time_fld])." at 23h59</span>.";
											$message.= "<br>Please ensure that you have completed all necessary updates before this time.";
											$message.= "<br>&nbsp;<br>Kind regards<br>".(isset($reseller_info['site_name']) ? $reseller_info['site_name'] : "Ignite Assist")."</font>";
											if($to == $support_email) {
												$to = $IAS_helpdesk;
											}
											if(in_array($to,$helpdesk_emails)) {
												$message.="<font face=arial><br>&nbsp;<br>Municipality: ".strtoupper($cmpcode)."</font>";
											} else {
												$cc = "";
											}
											$email = new ASSIST_EMAIL($to,$subject,$message,"HTML", $cc);
$email->setSenderUserID("AUTO");
$email->setSenderCmpCode($cmpcode);
$email->setRecipientUserID($ri);
											$email->sendEmail();
										}
										$actions[] = "Sent reminder '".$subject."' for ".$key." users [Closure: ".date("d F Y",$t['close_'.$time_fld])."].";
										if(!in_array("rem_".$time_fld,$resets)) { $resets[] = "rem_".$time_fld; }
										$log_change[] = " - ".$subject." ($time_fld) [".date("d F Y",$t['close_'.$time_fld])."]";
									} else {
										if(!$t['active_'.$time_fld]) {
											$actions[] = "No reminder sent due to: Inactive time period for $key users.";
										} elseif(count($emails)==0) {
											$actions[] = "No reminder sent due to: Lack of $key users.";
										} elseif($t['rem_'.$time_fld]>0 && $t['rem_'.$time_fld]<=$today) {
											$actions[] = "No reminder sent due to: No $key reminder for today.";
										} elseif($t['close_'.$time_fld]>$today) {
											$actions[] = "No reminder sent due to: No $key closure in the future.";
										}
									}
								}//if reminder is set for time period for user group
							}//foreach users
							if(count($resets)>0) {
								$lsql = $timeObj->resetTime($resets,$ti);
								if($lsql!==false) {
												$v_log = array(
													'section'=>"TIME",
													'ref'=>$ti,
													'field'=>implode(", ",$resets),
													'text'=>"Sent reminders:<br />".implode("<br />",$log_change),
													'old'=>"",
													'new'=>"0",
													'YN'=>"Y"
												);
												$timeObj->logChanges('SETUP',0,$v_log,$me->code($lsql));
								}

								$totalactions+=count($log_change);
							}
							$result2.="<li class=isubmit>Actions completed for time period $ti:</li><li>".implode("</li><li>",$actions)."</li>";
						}//foreach time
					} else {
						$result2.="<li class=iinform>No email recipients found.</li>";
					}
				} else {
					$result2.="<li class=iinform>No open time periods with current reminders found.</li>";
				}
			} else {
				$result2.="<li class=iinform>".strtoupper($modref)." not found</li>";
			}
		}
    } else {
        $result2.= "<li class=idelete>No database found.</li>";
    }
	$result2.="</ul></td></tr>";
}
$result2.="</table>";

//WRITE RESULTS TO FILE
$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Action4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"https://assist.action4u.co.za/assist.css\" type=\"text/css\">";
$echo.= $result1;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result2."</body></html>";
		//WRITE DATA TO FILE
		$filename = "Results/SDBP5_AREM_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
$_SESSION['modref'] = 'SDBP5_REM';
//$to = "janet@actionassist.co.za";
unset($to);
$subject = "$doctitle Auto reminders";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
/*$em = new ASSIST_EMAIL($to,$subject,$message,"HTML");
	$em->setSenderUserID("AUTO");
	$em->setSenderCmpCode("IASSIST");
	$em->setRecipientUserID("0001");

$em->sendEmail();
*/
include("cron_job_send_email.php");

echo "<p>Notifications performed: ".$totalactions."</p>";

?>