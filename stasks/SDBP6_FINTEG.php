<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 14400);
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
include("../inc_status.php");
//$assiststatus comes from inc_status included above - it sets the status of the servers for maintenance purposes
/** @var CHAR $assiststatus - comes from /inc_status.php */
if($assiststatus == "U") {
	exit("Assist Status set to Maintenance.  SDBP6_FINTEG not executed.");
}
require_once("../module/autoloader.php");
$helper = new ASSIST_HELPER();
/**********************
 * Setup variables
 */
//Who am I for logging
$_SESSION['tid'] = "IA";
$_SESSION['tkn'] = "Assist Automated Tasks";

//NOTIFY ME OF start
$_SESSION['modref'] = 'SDBP6_FINTEG';
$subject = "SDBP6 / FINTEG Automatic Functions";
$message = "<p>Process started at ".date("d F Y H:i:s")."</p>";
include("cron_job_send_email.php");
//unset email variables to stop database conn from trying to connect to email CC
unset($to);
unset($cc);

//variables
$remote_lines_table = "finteg_all_lines";
$remote_data_table = "finteg_all_financials";
$local_lines_table = "_financials_lines";
$local_data_table = "_financials_lines_results";
$record_limit = 1000;


//Which server am I operating on (for DB Connections) - if localhost or action4udev then use local conn else use remote conn to .125 server
//Not working on live so killed for now AA-539
//if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
//	$server = "LOCAL_FINTEG";
//} elseif(strpos($_SERVER['HTTP_HOST'], "action4udev") !== false) {
//	$server = "LOCAL_FINTEG";
//} else {
	$server = "FINTEG";
//}
$module = "SDBP6";
$company = array("POL0001", "MOP0001", "ELM0001", "NLM0001","MLM0001","GLM0001");
$dbs = array("ignittps_ipol0001", "ignittps_imop0001", "ignittps_ielm0001", "ignittps_inlm0001", "ignittps_imlm0001", "ignittps_iglm0001");
//single DB option for specific client testing or quick dev testing that using less data AA-539
//$company = array("GLM0001");
//$dbs = array("ignittps_iglm0001");
//blank DB option for testing everything but getting of client data AA-539
//$company = array();
//$dbs = array();
$doctitle = "SDBP6 reading from Integration server";

$result1 = $helper->getFixedPageHeader();
$result2 = "<h1>".$doctitle."</h1>";
$result2 .= "<p class='b i green'>Full process started @ ".date("d F Y H:i:s")."</p>";
$result2 .= "<table cellpadding=3 cellspacing=0>";
$result2 .= "<tr><th colspan='1'>Company</th><th>Action</th></tr>";

$raw_logs = array();

//for each client
foreach($company as $cmpcode) {
	$financial_tables_have_been_truncated = false;
	$result2 .= "<tr><td class='b'>".$cmpcode."</td><td>";
	$result2 .= "<p class='b u i green'>START: ".date("d F Y H:i:s")."</p>";
	$cmpcode = strtolower($cmpcode);
	$_SESSION['cc'] = $cmpcode;
	$me = new ASSIST_MODULE_HELPER("client", $cc);

	$sql = "SELECT * FROM assist_menu_modules WHERE modlocation = '".$module."' AND modyn = 'Y'";
	$mods = $me->mysql_fetch_all_by_id($sql, "modref");

	$my_log = array();
	foreach($mods as $modref => $mod) {
		$_SESSION['ref'] = strtolower($modref);
		$_SESSION['modref'] = strtolower($modref);
		$my_log[$modref] = array('name'=>$mod['modtext']);
		$me->setDBRef($modref);
		$dbref = "assist_".strtolower($cmpcode)."_".strtolower($modref);
		$result2 .= "<p>Checking: $modref </p>";
		//Triggering SDBP6_SDBIP object redirects back to SDBP6_TOPKPI - don't know why this is happening.  Short term solution is to bypass the class and hard code the necessary SQL statement - JC 2019-11-11 AA-287
//		$sdbipObject = new SDBP6_SDBIP($modref,true,$cmpcode);
//		$does_activated_exist = $sdbipObject->doesActivatedSDBIPExist(true);
		$sql = "SELECT * 
				FROM ".$dbref."_sdbip
				INNER JOIN assist_".$cmpcode."_master_financialyears ON sdbip_fin_year_id = id 
				WHERE ( sdbip_status & ".SDBP6::ACTIVE." ) = ".SDBP6::ACTIVE." 
				AND ( sdbip_status & ".SDBP6::SDBIP_ACTIVATED." ) = ".SDBP6::SDBIP_ACTIVATED." 
				ORDER BY sdbip_id DESC";
		$sdbip_rows = $me->mysql_fetch_all($sql);
//echo $sql;
//ASSIST_HELPER::arrPrint($sdbip_rows);
		$does_activated_exist = is_array($sdbip_rows) && (count($sdbip_rows) > 0);
		if($does_activated_exist) {
			foreach($sdbip_rows as $row) {
				$sdbip = $row;//$sdbipObject->getCurrentActivatedSDBIPDetails();
				$sdbip_id = $sdbip['sdbip_id'];
				$my_log[$modref][$sdbip_id] = array();
				if(isset($sdbip['sdbip_fin_year_id'])) {
					$fin_code = $sdbip['fin_ref'];
					$fin_year_id = $sdbip['sdbip_fin_year_id'];
					$fin_ref = $fin_year_id;//which field to use for search
					$result2 .= "<p>".$sdbip['sdbip_name']." (SDP".$sdbip_id.") [FY".$fin_ref."]</p>";
					$my_log[$modref][$sdbip_id]['name'] = $sdbip['sdbip_name']." (SDP".$sdbip_id.") [FY".$fin_ref."]";
					//connect to finteg for client
					$fintegDB = new ASSIST_DB_REMOTE("client", $cmpcode, $server);

					//get finteg_all_lines
					$sql = "SELECT COUNT(line_id) as m FROM ".$remote_lines_table." WHERE line_fin_year = '".$fin_ref."'"; //echo $sql;
					$total_rows = $fintegDB->mysql_fetch_one_value($sql, "m"); //echo $total_rows;
					//if number of rows exists then continue
					if($total_rows > 0) {
						$my_log[$modref][$sdbip_id]['results'] = array(
							'lines'=>array(
								'found'=>$total_rows,
								'processed'=>0,
							),
							'financials'=>array(
								'found'=>0,
								'processed'=>0,
							),
							'projects'=>array(
								'financials'=>0,
								'sdbip'=>0,
								'processed'=>0,
								'unspecified'=>0,
							)
						);
						//truncate existing - only if data exists & not already done!!!!!!
						$local_table = $me->getDBRef().$local_lines_table;
						$local_fin_table = $me->getDBRef().$local_data_table;
						if(!$financial_tables_have_been_truncated) {
							$financial_tables_have_been_truncated = true;
							$sql = "TRUNCATE ".$local_table;
							$me->db_update($sql);
							$sql = "TRUNCATE ".$local_fin_table;
							$me->db_update($sql);
						}
						//get data & save
						$rows_processed = 0;
						$continue = true;
						while($continue) { //echo "<P>".$rows_processed." : ".$total_rows." :: ";
							$sql = "SELECT * FROM ".$remote_lines_table." WHERE line_fin_year = '".$fin_ref."' LIMIT $rows_processed , $record_limit "; //echo $sql;
							$lines = $fintegDB->mysql_fetch_all($sql); //ASSIST_HELPER::arrPrint($lines);
							if(count($lines) > 0) {
								//save into db
								$insert_data = array();
								foreach($lines as $line) {
									foreach($line as $fld => $l) {
									//quick fix until raw_fin_year can be added to all tables
										if($fld=="raw_fin_year") {
											unset($line[$fld]);
										} else {
											if(substr($l, 0, 1) == '"') {
												$l = substr($l, 1, strlen($l));
											}
											if(substr($l, -1) == '"') {
												$l = substr($l, 0, -1);
											}
											$line[$fld] = ASSIST_HELPER::code($l);
										}
									}
									$insert_data[] = "('".implode("','", $line)."')";
								}
								$sql = "INSERT INTO ".$local_table." VALUES ".implode(",", $insert_data);
								$me->db_insert($sql);
								$rows_processed += count($lines);
							} else {
								$continue = false;//$rows_processed = $total_rows + 1;
							}
						}
						$result2 .= "<p class=green>".$total_rows." lines found; ".$rows_processed." lines processed;</p>";
						$my_log[$modref][$sdbip_id]['results']['lines']['processed'] = $rows_processed;
						//get finteg_all_financials
						//$sql = "SELECT MAX(fin_id) as m FROM ".$remote_data_table;
						$sql = "SELECT count(fin.fin_id) as m FROM ".$remote_data_table." fin INNER JOIN ".$remote_lines_table." ON fin_line_id = line_id AND line_fin_year = '".$fin_ref."'";
						$total_financials = $fintegDB->mysql_fetch_one_value($sql, "m");
						$my_log[$modref][$sdbip_id]['results']['financials']['found'] = $total_financials;
						if($total_financials > 0) {
							$rows_processed = 0;
							$continue = true;
							//save into db
							while($continue) {
								$sql = "SELECT fin.* FROM ".$remote_data_table." fin INNER JOIN ".$remote_lines_table." ON fin_line_id = line_id AND line_fin_year = '".$fin_ref."' LIMIT $rows_processed , $record_limit ";
								$rows = $fintegDB->mysql_fetch_all($sql);
								if(count($rows) > 0) {
									//save into db
									$insert_data = array();
									foreach($rows as $row) {
										$insert_data[] = "('".implode("','", $row)."')";
									}
									$sql = "INSERT INTO ".$local_fin_table." VALUES ".implode(",", $insert_data);
									$me->db_insert($sql);
									$rows_processed += count($rows);
								} else {
									$continue = false; //$rows_processed = $total_financials + 1;
								}
							}
							//convert time_id to local
							//get correct time id for given fin year id
							$sql = "SELECT stime.id as time_id
							FROM ".$dbref."_setup_time stime
							INNER JOIN ".$dbref."_sdbip S
							ON S.sdbip_id = stime.sdbip_id AND S.sdbip_id = ".$sdbip_id;
							$time_ids = $me->mysql_fetch_all_by_value($sql, "time_id");
							//update db
							$t = 1;
							foreach($time_ids as $time_id) {
								$sql = "UPDATE ".$local_fin_table." SET fin_time_id = ".$time_id." WHERE fin_time_id = ".$t;
								$me->db_update($sql);
								$t++;
							}
						}
						$my_log[$modref][$sdbip_id]['results']['financials']['processed'] = $rows_processed;
						$result2 .= "<p class=green>".$total_financials." financials rows found; ".$rows_processed." financial rows processed;</p>";
						//update projects
						//get list of project_id & fin_ref
						$sql = "SELECT proj_id as id, proj_finref as fin_ref FROM ".$dbref."_project WHERE (proj_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE;
						$projects = $me->mysql_fetch_value_by_id($sql, "fin_ref", "id");
						//get sum of financials per month per project - excluding revenue, assets & liabilities (but including asset acquisitions)
						$sql = "SELECT pc_client_guid as project , fin_time_id as time_id , SUM(fin_budget) as budget , SUM(fin_actual) as actual
								FROM ".$dbref."_financials_lines 
								INNER JOIN ".$dbref."_financials_lines_results
								ON line_id = fin_line_id
								WHERE line_fin_year = '".$fin_ref."'
								AND item_prefix NOT IN ('IR','IA','IL')
								GROUP BY pc_client_guid, fin_time_id";
						$financials = $me->mysql_fetch_all_by_id2($sql, "project", "time_id");
//					ASSIST_HELPER::arrPrint($projects);
//					ASSIST_HELPER::arrPrint($financials);
						//update projects
						$projects_processed = 0;
						$total_financials = count($financials);
						$unspecified_projects = $total_financials;
						foreach($projects as $fin_ref => $project_id) {
							if(isset($financials[$fin_ref])) {
								$project_financials = $financials[$fin_ref];
								foreach($project_financials as $time_id => $fin) {
									$variance = $fin['budget'] - $fin['actual'];
									$perc_spent = $fin['budget'] > 0 ? ($fin['actual'] / $fin['budget']) * 100 : 100;
									$sql = "UPDATE ".$dbref."_project_finances 
										SET pf_revised = '".$fin['budget']."'
											, pf_actual = '".$fin['actual']."'
											, pf_variance = ".$variance."
											, pf_perc_spent = ".$perc_spent."
										WHERE pf_proj_id = ".$project_id." AND pf_time_id = ".$time_id;
									$me->db_update($sql);
								}
								unset($financials[$fin_ref]);
								$projects_processed++;
								$unspecified_projects--;
							}
						}
						$my_log[$modref][$sdbip_id]['results']['projects']['financials'] = $total_financials;
						$my_log[$modref][$sdbip_id]['results']['projects']['sdbip'] = count($projects);
						$my_log[$modref][$sdbip_id]['results']['projects']['processed'] = $projects_processed;
						$result2 .= "<p class=green>".$total_financials." projects found in financials; ".count($projects)." projects found in SDBIP; ".$projects_processed." projects updated;</p>";
						//record "unspecified" data
						if($unspecified_projects > 0) {
							$my_log[$modref][$sdbip_id]['results']['projects']['unspecified'] = $unspecified_projects;
							$result2 .= "<p class=red>".$unspecified_projects." projects not found in local SDBIP - recorded as Unspecified Project;</p>";
							$sql = "DELETE FROM ".$dbref."_project_finances WHERE pf_proj_id = 0";
							$me->db_update($sql);
							//sum
							$insert_data = array();
							foreach($financials as $fin) {
								foreach($fin as $time_id => $f) {
									if(!isset($insert_data[$time_id]['budget'])) {
										$insert_data[$time_id]['budget'] = 0;
									}
									$insert_data[$time_id]['budget'] += $f['budget'];
									if(!isset($insert_data[$time_id]['actual'])) {
										$insert_data[$time_id]['actual'] = 0;
									}
									$insert_data[$time_id]['actual'] += $f['actual'];
								}
							}
							//insert
							foreach($insert_data as $time_id => $f) {
								$b = $f['budget'];
								$a = $f['actual'];
								$v = $b - $a;
								$ps = $b > 0 ? ($a / $b) * 100 : 100;
								$sql = "INSERT INTO ".$dbref."_project_finances 
									SET pf_proj_id = 0, pf_time_id = $time_id 
									, pf_original = $b , pf_revised = $b , pf_actual = $a 
									, pf_variance = $v , pf_perc_spent = $ps 
									, pf_status = ".SDBP6::ACTIVE." , pf_insertuser = 'AUTO' , pf_insertdate = now()";
								$me->db_update($sql);
							}
						}
						/** END update projects - manual route **/
					} else {
						$my_log[$modref][$sdbip_id]['comment'] = "No financial records found for financial year FY".$fin_ref.".";
						$result2 .= "<p class=red>No financial records found for financial year with code ".$fin_ref."</p>";
					} //end if financials found
					unset($fintegDB);
				} else {
					$my_log[$modref][$sdbip_id]['comment'] = "Invalid financial year information found (SDP".$sdbip_id.").";
					$result2 .= "<p class=red>Invalid financial year information found (SDP".$sdbip_id.").</p>"; //fin_year_id test
				}
			} //foreach sdbip
		} else {
			$my_log[$modref]['comment'] = "No activated SDBIP found.";
			$result2 .= "<p class=red>No activated SDBIP found.</p>";
		}
	}
	unset($me);
	$result2 .= "<p class='b u i'>Done : ".date("d F Y H:i:s")."</p></td></tr>";
	$raw_logs[$cmpcode] = $my_log;
}

$result2 .= "</table><p class='b i green'>Full process completed @ ".date("d F Y H:i:s")."</p>";
$result2 .= "</body></html>";



/** LOGGING */
$mdb = new ASSIST_MODULE_HELPER("master","",true);
//Get the last remote log id so you know which logs to fetch & process
$sql = "SELECT max(log_remote_log_id) as max_id FROM finteg_logs";
$row = $mdb->mysql_fetch_one($sql);
$max_id = isset($row['max_id']) ? $row['max_id'] : 0;

//get the logs ordered in reverse - assume the latest records relate to the current script run & match up with the logs from above, any left just get processed separately
$fintegDB = new ASSIST_DB_REMOTE("other", "finteg_financial_integration", $server);
$sql = "SELECT ftpl_id as log_id, LOWER(ftc_cc) as cmpcode, ftpl_vpn_connection as remote_connection_result, ftpl_insertdate as insert_date, ftpl_activities as result FROM finteg_process_log INNER JOIN finteg_companies ON ftpl_sgc_id = ftc_id WHERE ftpl_id > $max_id ORDER BY ftpl_id DESC ";
$logs = $fintegDB->mysql_fetch_all($sql);

foreach($logs as $log) {
	$cc = $log['cmpcode'];
	$insert_data = array(
		'log_remote_log_id'=>$log['log_id'],
		'log_cmpcode'=>strtoupper($cc),
		'log_connection_result'=>$log['remote_connection_result'],
		'log_insertdate'=>$log['insert_date'],
		'log_remote_activity_result'=>$log['result']
	);
	if(isset($raw_logs[$cc])) {
		$insert_data['log_local_activity_result'] = serialize($raw_logs[$cc]);
		$insert_data['log_insertdate'] = date("Y-m-d H:i:s");
		unset($raw_logs[$cc]);
	} else {
		$insert_data['log_local_activity_result'] = "";
	}
	$sql = "INSERT INTO finteg_logs SET ".$mdb->convertArrayToSQLForSave($insert_data);
	$mdb->db_insert($sql);
}

//Now double check that there aren't any local logs that must still be saved
if(count($raw_logs)>0) {
	foreach($raw_logs as $cc => $log) {
		$insert_data = array(
			'log_remote_log_id'=>0,
			'log_cmpcode'=>strtoupper($cc),
			'log_connection_result'=>2,
			'log_insertdate'=>date("Y-m-d H:i:s"),
			'log_remote_activity_result'=>""
		);
		$insert_data['log_local_activity_result'] = serialize($raw_logs[$cc]);
		$sql = "INSERT INTO finteg_logs SET ".$mdb->convertArrayToSQLForSave($insert_data);
		$mdb->db_insert($sql);
	}
}


/*
$log = $logs[0];
$result = unserialize($log['result']);
echo $result;

ASSIST_HELPER::arrPrint($logs);

die();*/










echo $result1;
echo $result2;


//WRITE RESULTS TO FILE
$echo = "";
$echo .= $result1;
$echo .= $result2."</body></html>";
//WRITE DATA TO FILE
$filename = "Results/SDBP6_FINTEG_".date("Ymd_His").".html";
$file = fopen($filename, "w");
fwrite($file, $echo."\n");
fclose($file);

//NOTIFY ME OF CLOSURES
$_SESSION['modref'] = 'SDBP6_FINTEG';
$subject = "SDBP6 / FINTEG Automatic Functions";
$message = "<p>Process ended at ".date("d F Y H:i:s").".</p><p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
include("cron_job_send_email.php");
?>