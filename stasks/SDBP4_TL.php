<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP4_TL not executed.");
}

include("../inc_assist.php");
include("../inc_codehelper.php");
include("../inc_db_conn.php");
include("../SDBP4/inc/inc.php");
include("../SDBP4/inc/inc_cron.php");

$all_total_records_changed = 0;

$today = $_SERVER["REQUEST_TIME"];
error_reporting(-1);
$arr_modref = array("sdp11","sdp12","sp12b");
$mod_table = "top_results";
$modtitle = "SDBIP v4";
$totalactions = 0;

$result_settings = array(
	0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."),
	1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"R"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% >= Actual/Target < 75%"),
	2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"O"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75% >= Actual/Target < 100%"),
	3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"G"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual/Target = 100%"),
	4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"G2"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100% > Actual/Target < 150%"),
	5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"B"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "Actual/Target >= 150%"),
);


$tkid = "IA";
$_SESSION['tkn'] = "Ignite Assist";

$result1 = "<style type=text/css>";
$result1.= "table {";
$result1.= "    border-collapse: collapse;";
$result1.= "}";
$result1.= "td {";
$result1.= "	color: #000000;";
$result1.= "	text-align: left;";
$result1.= "	background-color: #ffffff;";
$result1.= "	font-weight: normal;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "th {";
$result1.= "	color: #ffffff;";
$result1.= "	text-align: center;";
$result1.= "	background-color: #cc0001;";
$result1.= "	font-weight: bold;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "</style>";
$result2 = "<h1>$modtitle ~ Scheduled Tasks: Top Layer SDBIP Auto Update</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th>CMPCODE</th><th>Action</th></tr>";


$cmpcode = "";
//GET ARRAY OF CMPCODE
require_once("../inc_db.php");
$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y'";
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[] = $row['cmpcode'];
    }

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close();

//FOREACH COMPANY
foreach($company as $cmpcode)
{
	$total_records_changed = 0;
	set_time_limit(1800);
	$cmpcode = strtolower($cmpcode);
	//include("../inc_db.php");
	$db_name = $db_other.$cmpcode;
	//IF DATABASE EXISTS
	if (in_array($db_other.$cmpcode, $dbs, true)) {
		$result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>";
		foreach($arr_modref as $modref) {
			//CHECK IF MODULE/TABLE EXISTS
			$module_exists = getModuleRefExistance($modref,$mod_table);
			//IF EXISTS
			if($module_exists) {
				$dbref = "assist_".$cmpcode."_".$modref;
	//echo "<P>".$cmpcode." :: ";
				$section = "TOP";
				$get_lists = true;
				$get_active_lists = false;
				$get_open_time = false;
				$get_headings = array("KPI","TOP");
				$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
				include("../SDBP4/inc/inc_module.php");
				$total_records_changed = returnTLAutoUpdate($cmpcode);
	//echo $total_records_changed;
				$all_total_records_changed+=$total_records_changed;
				$result2.= strtoupper($modref)." auto update run. ".$total_records_changed." change(s) were made.";
			} else {
				$result2.= "Module $modref doesn't exist.<br />";
			}
		}
		$result2.= "</td></tr>";
    } else {
        $result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>No database found.</td></tr>";
    }
}
$result2.="</table>";


//WRITE RESULTS TO FILE
$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"http://assist.ignite4u.co.za/default.css\" type=\"text/css\">";
$echo.= $result1;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result2."<p>Update complete at ".date("d M Y H:i:s")." - ".$all_total_records_changed." change(s) were made.</p></body></html>";
		//WRITE DATA TO FILE
		$filename = "Results/SDBP4_TOP_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
$to = "janet@igniteassist.co.za";
$from = "no-reply@ignite4u.co.za";
$subject = "$modtitle Top Layer Auto update";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
$header = "Content-type: text/html; charset=us-ascii";
mail($to,$subject,$message,$header);

		
echo "Updates performed: ".$all_total_records_changed;



?>