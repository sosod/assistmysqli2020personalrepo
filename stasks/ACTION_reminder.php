<?php
require_once("../module/autoloader.php");
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  ACTION_reminder not executed.");
}

include("../inc_db.php");
include("../inc_db_conn.php");
include("../inc_assist.php");
include("../inc_codehelper.php");
//error_reporting(-1);

$style = "<style type=text/css>";
$style.= "
hr {
	color: #000099;
}
h1 {
	text-decoration: none;
	font-size: 16pt;
	line-height: 20pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
	font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
	color: #000099;
	letter-spacing: 0.07em;
}
h2 {
	text-decoration: none;
	font-size: 14pt;
	line-height: 17pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
	font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
	color: #000099;
	letter-spacing: 0.07em;
}
p {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
	color: #000000;
	margin: 10px 0px 10px;
	padding: 0px 0px 0px 5px;
}
table {
	border-collapse: collapse;
	border: solid 1px #ababab;
}
table td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
	color: #000000;
	border: solid 1px #ababab;
	vertical-align: top;
	padding: 5px 5px 5px 5px;
	text-align: left;
}
table th {
	background-color: #000099;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	text-align: center;
	font-weight: bold;
	line-height: 12pt;
	color: #ffffff;
	padding: 5px 5px 5px 5px;
	border: solid 1px #ffffff;
}
.overdue { color: #cc0001; }
.today { color: #009900; }
.soon { color: #FE9900; }
";
$style.= "</style>";
$result = "<h1>Action Reminders</h1><table cellpadding=3 cellspacing=0>";
$result.= "<tr><th>CMPCODE</th><th>Action</th></tr>";
$email1 = $style."<h1>Action Assist Reminder Email</h1>";
$email1.= "<table cellpadding=3 cellspacing=0>";
$email1.= "<tr><th>Ref</th><th>Task&nbsp;Owner</th><th>Topic</th><th>Priority</th><th>Deadline</th><th>Status</th><th>Instructions</th></tr>";
$email9 = "</table><p>To see all task details and to update your tasks please log onto Ignite Assist.</p>";


$html_start = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\">";
$html_start.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$html_start.= $style;
$html_start.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$html_end = "</body></html>";

$mailObj = new ASSIST_EMAIL();
$mailObj->setSenderUserID("AUTO");
$mailObj->setSenderCmpCode("");
$mailObj->setContentType("HTML");
$mailObj->setSender("");

$email_sent = 0;

//	$gdate = getdate();
//	$today = $gdate[0];
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
//	$tkid = "IA";
//	$tref = "TA";
    $weekday = date("N",$today);
    $today0 = mktime(23,59,59,date("m"),date("d"),date("Y"));
    $today1 = mktime(1,0,0,date("m"),date("d")+1,date("Y"));
    $today7 = mktime(1,0,0,date("m"),date("d")+8,date("Y"));
//GET ARRAY OF CMPCODE
$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y' AND cmp_is_demo = 0";
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[] = $row['cmpcode'];
    }
mysql_close();

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


//FOREACH CMPCODE
foreach($company as $cmpcode) {
    set_time_limit(1800);
	$result_text = array();
    $cmpcode = strtolower($cmpcode);
    if (in_array($db_other.$cmpcode, $dbs, true)) {
		$db_name = $db_other.strtolower($cmpcode);
        $mnr = 0;
        //CHECK MENU FOR TASK
        $sql = "SELECT * FROM assist_menu_modules WHERE modlocation LIKE 'ACTION%' AND modyn = 'Y'";
        $modules = mysql_fetch_all($sql);
        $mnr = count($modules);
        //IF ACTION MODULE EXISTS
        if($mnr > 0) {
			foreach($modules as $mod) {
				$modref = strtolower($mod['modref']);
				$_SESSION['modref'] = strtoupper($modref);
				$dbref = "assist_".$cmpcode."_".$modref;
				//CHECK TABLES FOR PROFILE TABLE
				$sql = "SHOW TABLES LIKE '".$dbref."_profile'";
				$rs = getRS($sql);
					$mnr2 = mysql_num_rows($rs);
				mysql_close();
				if($mnr2>0) {
					//CHECK FOR ACTIVE PROFILES
					$sql = "SELECT p.*, tk.tkemail, CONCAT_WS(' ',tk.tkname,tk.tksurname) as tkname FROM ".$dbref."_profile p
							INNER JOIN assist_".$cmpcode."_timekeep tk
								ON tk.tkid = p.tkid AND tk.tkstatus = 1 AND tk.tkemail <> ''
							INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
								ON tk.tkid = mmu.usrtkid AND mmu.usrmodref = '".$modref."'
							WHERE field1 = 'Y'";
					$profiles = mysql_fetch_all_fld($sql,"tkid");
					if(count($profiles)>0) {
						//GET FIELD HEADINGS AND DISPLAY ORDER
						$sql = "SELECT field, headingfull FROM ".$dbref."_list_display WHERE yn = 'Y' AND type = 'S' AND mydisp = 'Y' ORDER BY mysort";
						$head = mysql_fetch_all_fld($sql,"field");
						//FOR EACH PROFILE CHECK FOR EMAIL SENDING FOR TODAY
						foreach($profiles as $p) {
							$tkid = $p['tkid'];
							$tkname = $p['tkname'];
							$when = $p['field2'];
							$on = $p['field3'];
							$what = $p['field4'];
							if($when=="daily" || ($when=="weekly" && $on == $weekday)) {
								$sql = "SELECT t.taskid, t.taskadddate,
											CONCAT_WS(' ',totk.tkname, totk.tksurname) as taskadduser,
											top.value as tasktopicid,
											urg.value as taskurgencyid,
											t.taskdeadline,
											st.value as taskstatusid,
											t.taskaction, t.taskdeliver
										FROM ".$dbref."_task t
										INNER JOIN ".$dbref."_task_recipients tr
											ON t.taskid = tr.taskid
											AND tr.tasktkid = '$tkid'
										INNER JOIN ".$dbref."_list_topic top
											ON t.tasktopicid = top.id
										INNER JOIN ".$dbref."_list_urgency urg
											ON t.taskurgencyid = urg.id
										INNER JOIN ".$dbref."_list_status st
											ON t.taskstatusid = st.pkey
										INNER JOIN assist_".$cmpcode."_timekeep totk
											ON totk.tkid = t.taskadduser
										WHERE t.taskstatusid > 2 ";
								switch($what) {
								case "week":
									$sql.= "AND t.taskdeadline <= $today7
											AND t.taskdeadline >= $today";
									$email_title = "Actions Due Within the Next 7 Days";
									break;
								case "week2":
									$sql.= "AND t.taskdeadline <= $today7";
									$email_title = "Overdue Actions and Actions Due Within the Next 7 Days";
									break;
								case "today":
									$sql.= "AND t.taskdeadline >= $today
											AND t.taskdeadline <= $today0";
									$email_title = "Actions Due Today";
									break;
								case "today2":
									$sql.= "AND t.taskdeadline <= $today0";
									$email_title = "Overdue Actions and Actions Due Today";
									break;
								case "all":
								default:
									$email_title = "All Incomplete Actions";
									break;
								}
								$sql.= " ORDER BY t.taskdeadline DESC";
								$tasks = mysql_fetch_all($sql);
								if(count($tasks)>0) {
									$result_text[] = "- <span class=isubmit>".$mod['modtext']." profile found for ".$p['tkname']." - email to be sent with ".count($tasks)." actions to ".$p['tkemail']."</span>";
									$email = $html_start;
									$email.= "<h1>".$mod['modtext']."</h1><h2>".$email_title."</h2>";
									$email.= "<table><tr><th>Ref</th>";
									foreach($head as $fld => $h) {
										$email.="<th>".$h['headingfull']."</th>";
									}
									$email.="</tr>";
									foreach($tasks as $t) {
										$email.="<tr><td>".$t['taskid']."</td>";
										foreach($head as $fld => $h) {
											$email.="<td>";
											switch($fld) {
											case "taskdateadded":
											case "taskadddate":
												$email.=date("d M Y",$t['taskadddate']);
												break;
											case "taskdeadline":
												if(checkIntRef($t[$fld])) {
													if($t[$fld]<$today) {
														$email.="<span class=overdue>";
													} elseif($t[$fld]<$today0) {
														$email.="<span class=today>";
													} elseif($t[$fld]<$today7) {
														$email.="<span class=soon>";
													}
													$email.= date("d M Y",$t[$fld])."</span>";
												} else {
													$email.="N/A";
												}
												break;
											default:
												$email.=str_replace(chr(10),"<br />",decode($t[$fld]));
											}
											$email.="</td>";
										}
										$email.="</tr>";
									}
									$email.="</table>
									<p>To view all the details or to update an action please log onto Ignite Assist.</p>";
									//SEND EMAIL
									$to = $p['tkemail'];
									$from = "no-reply@ignite4u.co.za";
									$subject = "".$mod['modtext']." Reminder";
									$header = "From:".$from."\r\nContent-type: text/html; charset=us-ascii";
		$mailObj->setRecipient($to);
		$mailObj->setSubject($subject);
		$mailObj->setBody($email);
		$mailObj->setSenderCmpCode($cmpcode);
		$mailObj->setRecipientUserID($tkid);
		$mailObj->sendEmail();
									//mail($to,$subject,$email,$header);
									$email_sent++;
									//$result_text[] = $email;
								} else {
									$result_text[] = "- <span class=iinform>Profile found for ".$p['tkname']." but no tasks found to be sent</span>";
								}
							} else {
								$result_text[] = "- Profile found for ".$p['tkname']." - NO email to be sent today";
							}
						}
					} else {
						$result_text[] = "<span class=idelete>- No active profiles found for ".$mod['modtext']." [".$mod['modref']."]</span>";
					}
				} else {
					$result_text[] = "<span class=required>- No profile table found for ".$mod['modtext']." [".$mod['modref']."]</span>";
				}
				//mysql_close();
			}
		} else {
			$result_text[] = "No Action modules found.";
        }
    } else {
		$result_text[] = "No database found.";
    }
	$result.= "<tr><td>".strtoupper($cmpcode)."</td><td>".implode("<br />",$result_text)."</td></tr>";
}
$result.="</table>";



$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
//$echo.= "<link rel=\"stylesheet\" href=\"http://assist.ignite4u.co.za/assist.css\" type=\"text/css\">";
$echo.= $style;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result."
<P>".$email_sent." ACTION reminder emails sent.</p>
</body></html>";
		//WRITE DATA TO FILE
		$filename = "Results/ACTION_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
//$me = "janet@actionassist.co.za";
//$from = "";
unset($to);
$_SESSION['modref'] = "ACTION_REM";
$subject = "Action Auto reminders";
$message = $email_sent." ACTION reminder emails sent.";
$message = "<p>".$email_sent." ACTION reminder emails sent.</p><p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
//$header = "Content-type: text/html; charset=us-ascii";
//mail($me,$subject,$message,$header);
include("cron_job_send_email.php");


echo "<P>".$email_sent." ACTION reminder emails sent.</p>";
//echo $echo;

?>
