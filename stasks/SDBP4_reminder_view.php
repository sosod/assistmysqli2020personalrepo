<?php
include("../inc_assist.php");
include("../inc_codehelper.php");
include("../inc_db_conn.php");
include("../SDBP4/inc/inc.php");
//phpinfo();
	$support_email = "helpdesk@ignite4u.co.za";
	//$support_email = "igniteassist_dev@gmail.com";
	$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
	//$IAS_helpdesk = "igniteassistdev@gmail.com";
	$IRS_helpdesk = "helpdesk@igniteassist.co.za";
	$from = "no-reply@ignite4u.co.za";
	$header1 = "From: ".$from."\r\nReply-to: ".$from."\r\n";
$today = $_SERVER["REQUEST_TIME"];
//error_reporting(-1);
$modref = "sdp11";
$mod_table = "list_time";
$modtitle = "SDBIP 2011/2012";
$totalactions = 0;

$tkid = "IA";
$_SESSION['tkn'] = "Ignite Assist";

$result1 = "<style type=text/css>";
$result1.= "table {";
$result1.= "    border-collapse: collapse;";
$result1.= "}";
$result1.= "td {";
$result1.= "	color: #000000;";
$result1.= "	text-align: left;";
$result1.= "	background-color: #ffffff;";
$result1.= "	font-weight: normal;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "th {";
$result1.= "	color: #ffffff;";
$result1.= "	text-align: center;";
$result1.= "	background-color: #cc0001;";
$result1.= "	font-weight: bold;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "</style>";
$result2 = "<h1>$modtitle ~ Scheduled Tasks: Reminders</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th>CMPCODE</th><th>Action</th></tr>";


$cmpcode = "";
//GET ARRAY OF CMPCODE
include("../inc_db.php");
$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y' AND cmp_is_demo = 0";
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[] = $row['cmpcode'];
    }

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close();

//FOREACH COMPANY
foreach($company as $cmpcode)
{
	set_time_limit(1800);
	$cmpcode = strtolower($cmpcode);
	//include("../inc_db.php");
	$db_name = $db_other.$cmpcode;
	//IF DATABASE EXISTS
	if (in_array($db_other.$cmpcode, $dbs, true)) {
		//CHECK IF MODULE/TABLE EXISTS
		$module_exists = getModuleRefExistance($modref,$mod_table);
        //IF EXISTS
        if($module_exists) {
			$dbref = "assist_".$cmpcode."_".$modref;
		//	include("SDBP4/inc/inc_cron.php");
			//GET OPEN TIME
				$sql = "SELECT * FROM ".$dbref."_list_time 
						WHERE active = true
						AND start_date < '".date("Y-m-d H:i:s")."'
						AND ( 
								(rem_primary > 0 AND rem_primary<=$today AND active_primary = true)
							OR 
								(rem_secondary > 0  AND rem_secondary <=$today AND active_secondary = true)
							OR 
								(rem_finance > 0  AND rem_finance <=$today AND active_finance = true) 
							)";
				$time = Omysql_fetch_all_fld($sql,$cmpcode,"id");
				//arrPrint($time);
			if(!(count($time)>0)) {
                $result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>No active time periods with auto reminders set.</td></tr>";
			} else {
				$result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>";
				//CHECK FOR USERS FOR EACH TIME&Access SECTION - primary, secondary, toplayer, finance
				$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_user_admins a
						INNER JOIN assist_".$cmpcode."_timekeep t 
						   ON t.tkid = a.tkid AND t.tkstatus = 1
						INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
						   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = 'SDP11'
						WHERE a.active = true
						AND a.tkid NOT IN (
						   SELECT tkid FROM ".$dbref."_user_access
						   WHERE active = true
						   AND second = true
						)";
				$users['primary'] = Omysql_fetch_all($sql,$cmpcode);
				$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_user_access a
						INNER JOIN assist_".$cmpcode."_timekeep t 
						   ON t.tkid = a.tkid AND t.tkstatus = 1
						INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
						   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = 'SDP11'
						WHERE a.active = true
						AND a.second = true";
				$users['secondary'] = Omysql_fetch_all($sql,$cmpcode);
				$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_user_access a
						INNER JOIN assist_".$cmpcode."_timekeep t 
						   ON t.tkid = a.tkid AND t.tkstatus = 1
						INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
						   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = 'SDP11'
						WHERE a.active = true
						AND a.toplayer = true";
				$users['toplayer'] = Omysql_fetch_all($sql,$cmpcode);
				$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_user_access a
						INNER JOIN assist_".$cmpcode."_timekeep t 
						   ON t.tkid = a.tkid AND t.tkstatus = 1
						INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
						   ON mmu.usrtkid = t.tkid AND mmu.usrmodref = 'SDP11'
						WHERE a.active = true
						AND a.finance = true";
				$users['finance'] = Omysql_fetch_all($sql,$cmpcode);
				//arrPrint($users);
				$actions = array();
				foreach($time as $ti => $t) {
					$resets = array();
					$log_change = array();
					//primary
					$fld = "primary"; $section = "Departmental SDBIP";
					if(isset($users[$fld]) && count($users[$fld])>0 && $t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today && $t['close_'.$fld]>$today && $t['active_'.$fld]) {
						foreach($users[$fld] as $tofld => $toarr) {
							$to = $toarr['tkemail'];
							$header = $header1;
                            $subject = "$modtitle - $section Closure - ".date("M Y",strtotime($t['end_date']))."";
                            $message = "<font face=arial>Kindly note that $modtitle - $section - <span style=\"font-weight: bold;text-decoration: underline;\">".date("F Y",strtotime($t['end_date']))."</span> will close on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y",$t['close_'.$fld])." at 23h59</span>.";
                            $message.= "<br>Please ensure that you have completed all necessary updates before this time.";
                            $message.= "<br>&nbsp;<br>Kind regards<br>Ignite Assist</font>";
                            if($to == $support_email) { 
								$to = $IAS_helpdesk; 
							}
                            if($to == $IAS_helpdesk) { 
								$message.="<font face=arial><br>&nbsp;<br>Municipality: ".strtoupper($cmpcode)."</font>"; 
								$header.="CC:".$IRS_helpdesk."\r\n";
							}
                            $header.= "Content-type: text/html; charset=us-ascii";
                            //mail($to,$subject,$message,$header);
						}
						$actions[] = "Sent reminder '".$subject."' for ".$fld." users [Closure: ".date("d F Y",$t['close_'.$fld])."].";
						$resets[] = "rem_".$fld;
						$log_change[] = " - ".$subject." ($fld) [".date("d F Y",$t['close_'.$fld])."]";
					} else {
						if(!$t['active_'.$fld]) {
							$actions[] = "No reminder sent due to: Inactive time period for $fld users.";
						} elseif(isset($users[$fld]) && count($users[$fld])>0) {
							$actions[] = "No reminder sent due to: Lack of $fld users.";
						} elseif($t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today) { 
							$actions[] = "No reminder sent due to: No $fld reminder for today.";
						} elseif($t['close_'.$fld]>$today) {
							$actions[] = "No reminder sent due to: No $fld closure in the future.";
						}
					}
					//secondary
					$fld = "secondary"; $section = "Departmental SDBIP";
					if(isset($users[$fld]) && count($users[$fld])>0 && $t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today && $t['close_'.$fld]>$today && $t['active_'.$fld]) {
						foreach($users[$fld] as $tofld => $toarr) {
							$to = $toarr['tkemail'];
							$header = $header1;
                            $subject = "$modtitle - $section Closure - ".date("M Y",strtotime($t['end_date']))."";
                            $message = "<font face=arial>Kindly note that $modtitle - $section - <span style=\"font-weight: bold;text-decoration: underline;\">".date("F Y",strtotime($t['end_date']))."</span> will close on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y",$t['close_'.$fld])." at 23h59</span>
										".($t['close_primary']>0 ? "<br /><i><small>(Following primary closure on ".date("d M F",$t['close_primary']).")</small></i>" : "").".";
                            $message.= "<br>Please ensure that you have completed all necessary updates before this time.";
                            $message.= "<br>&nbsp;<br>Kind regards<br>Ignite Assist</font>";
                            if($to == $support_email) { 
								$to = $IAS_helpdesk; 
							}
                            if($to == $IAS_helpdesk) { 
								$message.="<font face=arial><br>&nbsp;<br>Municipality: ".strtoupper($cmpcode)."</font>"; 
								$header.="CC:".$IRS_helpdesk."\r\n";
							}
                            $header.= "Content-type: text/html; charset=us-ascii";
                            //mail($to,$subject,$message,$header);
						}
						$actions[] = "Sent reminder '".$subject."' for ".$fld." users [Closure: ".date("d F Y",$t['close_'.$fld])."].";
						$log_change[] = " - ".$subject." ($fld) [".date("d F Y",$t['close_'.$fld])."]";
					} else {
						if(!$t['active_'.$fld]) {
							$actions[] = "No reminder sent due to: Inactive time period for $fld users.";
						} elseif(isset($users[$fld]) && count($users[$fld])>0) {
							$actions[] = "No reminder sent due to: Lack of $fld users.";
						} elseif($t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today) { 
							$actions[] = "No reminder sent due to: No $fld reminder for today.";
						} elseif($t['close_'.$fld]>$today) {
							$actions[] = "No reminder sent due to: No $fld closure in the future.";
						}
					}
					//toplayer
					$fld = "secondary"; $section = "Top Layer SDBIP";
					if(isset($users[$fld]) && count($users[$fld])>0 && $t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today && $t['close_'.$fld]>$today && $t['active_'.$fld]) {
						foreach($users[$fld] as $tofld => $toarr) {
							$to = $toarr['tkemail'];
							$header = $header1;
                            $subject = "$modtitle - $section Closure - ".date("M Y",strtotime($t['end_date']))."";
                            $message = "<font face=arial>Kindly note that $modtitle - $section - <span style=\"font-weight: bold;text-decoration: underline;\">".date("F Y",strtotime($t['end_date']))."</span> will close on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y",$t['close_'.$fld])." at 23h59</span>.";
                            $message.= "<br>Please ensure that you have completed all necessary updates before this time.";
                            $message.= "<br>&nbsp;<br>Kind regards<br>Ignite Assist</font>";
                            if($to == $support_email) { 
								$to = $IAS_helpdesk; 
							}
                            if($to == $IAS_helpdesk) { 
								$message.="<font face=arial><br>&nbsp;<br>Municipality: ".strtoupper($cmpcode)."</font>"; 
								$header.="CC:".$IRS_helpdesk."\r\n";
							}
                            $header.= "Content-type: text/html; charset=us-ascii";
                            //mail($to,$subject,$message,$header);
						}
						$actions[] = "Sent reminder '".$subject."' for ".$fld." users [Closure: ".date("d F Y",$t['close_'.$fld])."].";
						$resets[] = "rem_".$fld;
						$log_change[] = " - ".$subject." [".date("d F Y",$t['close_'.$fld])."]";
					} else {
						if(!$t['active_'.$fld]) {
							$actions[] = "No reminder sent due to: Inactive time period for $fld users.";
						} elseif(isset($users[$fld]) && count($users[$fld])>0) {
							$actions[] = "No reminder sent due to: Lack of $fld users.";
						} elseif($t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today) { 
							$actions[] = "No reminder sent due to: No $fld reminder for today.";
						} elseif($t['close_'.$fld]>$today) {
							$actions[] = "No reminder sent due to: No $fld closure in the future.";
						}
					}
					//finance
					$fld = "finance"; $section = "Financials";
					if(isset($users[$fld]) && count($users[$fld])>0 && $t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today && $t['close_'.$fld]>$today && $t['active_'.$fld]) {
						foreach($users[$fld] as $tofld => $toarr) {
							$to = $toarr['tkemail'];
							$header = $header1;
                            $subject = "$modtitle - $section Closure - ".date("M Y",strtotime($t['end_date']))."";
                            $message = "<font face=arial>Kindly note that $modtitle - $section - <span style=\"font-weight: bold;text-decoration: underline;\">".date("F Y",strtotime($t['end_date']))."</span> will close on <span style=\"font-weight: bold;text-decoration: underline;color: #cc0001;\">".date("d F Y",$t['close_'.$fld])." at 23h59</span>.";
                            $message.= "<br>Please ensure that you have completed all necessary updates before this time.";
                            $message.= "<br>&nbsp;<br>Kind regards<br>Ignite Assist</font>";
                            if($to == $support_email) { 
								$to = $IAS_helpdesk; 
							}
                            if($to == $IAS_helpdesk) { 
								$message.="<font face=arial><br>&nbsp;<br>Municipality: ".strtoupper($cmpcode)."</font>"; 
								$header.="CC:".$IRS_helpdesk."\r\n";
							}
                            $header.= "Content-type: text/html; charset=us-ascii";
                            //mail($to,$subject,$message,$header);
						}
						$actions[] = "Sent reminder '".$subject."' for ".$fld." users [Closure: ".date("d F Y",$t['close_'.$fld])."].";
						$resets[] = "rem_".$fld;
						$log_change[] = " - ".$subject." [".date("d F Y",$t['close_'.$fld])."]";
					} else {
						if(!$t['active_'.$fld]) {
							$actions[] = "No reminder sent due to: Inactive time period for $fld users.";
						} elseif(isset($users[$fld]) && count($users[$fld])>0) {
							$actions[] = "No reminder sent due to: Lack of $fld users.";
						} elseif($t['rem_'.$fld]>0 && $t['rem_'.$fld]<=$today) { 
							$actions[] = "No reminder sent due to: No $fld reminder for today.";
						} elseif($t['close_'.$fld]>$today) {
							$actions[] = "No reminder sent due to: No $fld closure in the future.";
						}
					}
					if(count($resets)>0) {
						$sql = "UPDATE ".$dbref."_list_time SET ".implode("=0, ",$resets)."=0 WHERE id = ".$ti;
						//$rs = OgetRS($sql,$cmpcode);
										$v_log = array(
											'section'=>"TIME",
											'ref'=>$ti,
											'field'=>implode(", ",$resets),
											'text'=>"Sent reminders:<br />".implode("<br />",$log_change),
											'old'=>"",
											'new'=>"0",
											'YN'=>"Y"
										);
										//logChanges('SETUP',0,$v_log,code($sql));
										
						$totalactions+=count($log_change);
					}
				}	//foreach time
				$result2.=implode("<br />",$actions)."</td>";
			}//if count time > 0
		} else {
            $result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>Module $modref doesn't exist.</td></tr>";
        }
    } else {
        $result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>No database found.</td></tr>";
    }
}
$result2.="</table>";


//WRITE RESULTS TO FILE
$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"http://assist.ignite4u.co.za/default.css\" type=\"text/css\">";
$echo.= $result1;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result2."</body></html>";
/*		//WRITE DATA TO FILE
		$filename = "Results/".strtoupper($modref)."_AREM_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
$to = "janet@igniteassist.co.za";
$from = "no-reply@ignite4u.co.za";
$subject = "$modtitle Auto reminders";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
$header = "Content-type: text/html; charset=us-ascii";
//mail($to,$subject,$message,$header);

		
//echo "Notifications performed: ".$totalactions;
*/
echo $echo;

?>
