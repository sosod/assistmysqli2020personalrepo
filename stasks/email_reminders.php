<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  email_reminders not executed.");
}

error_reporting(E_ALL & ~E_DEPRECATED);
//error_reporting(-1);
//error_reporting(0);


require_once("../module/autoloader.php");
$mailObj = new ASSIST_EMAIL();
$mailObj->setSenderUserID("AUTO");
$mailObj->setSenderCmpCode("");
$mailObj->setContentType("HTML");
$mailObj->setSender("");

function saveResult($buffer) {

	$filename = "Results/EMAIL_".date("Ymd_His").".html";
       $file = fopen($filename,"w");
       fwrite($file,$buffer."\n");
       fclose($file);
$_SESSION['modref'] = 'EMAIL_REM';
	//NOTIFY ME OF CLOSURES
	//$to = "janet@actionassist.co.za";
	//$from = "no-reply@ignite4u.co.za";
unset($to);
	$subject = "Auto email reminders";
	$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
	//$header = "Content-type: text/html; charset=us-ascii";
	//mail($to,$subject,$message,$header);
	include("cron_job_send_email.php");
	return $buffer."<hr>".$message."<p>Done!</p>" ;

}

ob_start("saveResult");


//include_once("../library/class/assist_email.php");
//include_once("../library/class/assist_email_summary.php");



$get_session = false;
require_once '../header.php';
$_SESSION['cc'] = ""; $_SESSION['modref'] = "";
set_time_limit(1800);
?>
<style type=text/css>

table.mods th { background-color: #999999; }
</style>
<?php
$ml = "";
//include_once("../module/loader.php");
//spl_autoload_register("Loader::autoload");

echo "<h1>Email Reminders</h1>
<p class=i>Emails sent at ".date("d F Y H:i:s")."</p>";

$mdb = new ASSIST_DB("master");

$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmp_is_demo = 0 AND cmpcode NOT IN ('BLANK','CASSIST')";
$company = $mdb->mysql_fetch_all_fld($sql,"cmpcode");

//echo "<pre>"; print_r($company); echo "</pre>";
$mdb->db_connect();
$db_list = mysql_list_dbs($mdb->getConn());
$bds = array();
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
//echo "<pre>"; print_r($dbs); echo "</pre>";

echo "<table width=100%><tr><th colspan=2>Company</th><th>Result</th></tr>";
foreach($company as $cmpcode => $cmp) {
set_time_limit(1800);
	echo "<tr><td class=b>$cmpcode</td><td class=b>".$cmp['cmpname']."</td><td>";
	$cmpcode = strtolower($cmpcode);
	//Check that database exists
	if(in_array($mdb->getDBPrefix().$cmpcode,$dbs)) {
		$_SESSION['cc'] = $cmpcode; $_SESSION['modref'] = "";
		$cdb = new ASSIST_DB("",$cmpcode);
		//Get active modules
		$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' AND modref IN (SELECT DISTINCT usrmodref FROM assist_".$cmpcode."_menu_modules_users)";
		$mods = $cdb->mysql_fetch_all($sql);
		$sql = "SELECT DISTINCT usrmodref as modref, usrtkid as id, tkemail as email, CONCAT(tkname,' ',tksurname) as name
				FROM assist_".$cmpcode."_menu_modules_users
				INNER JOIN assist_".$cmpcode."_timekeep
				  ON tkid = usrtkid AND tkstatus = 1 AND tkemail <> ''
				INNER JOIN assist_menu_modules
				  ON usrmodref = modref AND modyn = 'Y'
				ORDER BY usrmodref, usrtkid";
		$u = $cdb->mysql_fetch_all($sql);
		$users = array();
		foreach($u as $s) {
			if(!isset($users[$s['modref']])) { $users[$s['modref']] = array(); }
			$users[$s['modref']][$s['id']] = $s;
		}
		if(count($mods)>0) {
			echo "<table width=100% class=mods><tr><th width=60>Ref</th><th width=80>Loc</th><th width=190>Title</th><th>Result</th></tr>";
			foreach($mods as $m) {
				$_SESSION['cc'] = $cmpcode;
				$_SESSION['modref'] = strtolower($m['modref']);
				$_SESSION['modlocation'] = $m['modlocation'];
				$_SESSION['dbref'] = "assist_".$cmpcode."_".$_SESSION['modref'];
				set_time_limit(1800);
				echo "<tr><td class=b>".$m['modref']."</td><td>".$m['modlocation']."</td><td>".$m['modtext']."</td><td>";
				$mr = strtolower($m['modref']);
				$ml = $m['modlocation'];
				//check
				if(file_exists("../".$ml."/login_reminders.php")) {
					require_once("../".$ml."/login_reminders.php");
					$cdb->setDBRef($mr);
					if(isset($users[$m['modref']])) {
						$result = call_user_func_array("email".$ml, array($cdb,$users[$m['modref']]));
						//$result = array("0");
						//$result = array(
						//	0=>"email".$ml."<br />".serialize(array($cdb,$users[$m['modref']]))
						//);
						echo "<b>".$result[0]."</b>";
						unset($result[0]);
						if(count($result)>0) {
							echo "<ol><li>".implode("</li><li>",$result)."</li></ol>";
						}
					} else {
						echo "No active users with access to the module";
					}
				} else {
					echo "No reminder email file found";
				}
			}
			echo "</table>";
		} else {
			echo "<span class=iinform>No active modules</span>";
		}
	} else {
		echo "<span class=idelete>No database found.</span>";
	}
	echo "</td></tr>";
}
echo "</table>";



ob_end_flush();


?>