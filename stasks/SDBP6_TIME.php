<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
include("../inc_status.php");
//$assiststatus comes from inc_status included above - it sets the status of the servers for maintenance purposes
/** @var CHAR $assiststatus - comes from /inc_status.php */
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP6_TIME not executed.");
}
require_once("../module/autoloader.php");
$helper = new ASSIST_HELPER();
/**********************
 * Setup variables
 */
//Who am I for logging
$_SESSION['tid'] = "IA";
$_SESSION['tkn'] = "Assist Automated Tasks";

//Which server am I operating on (for DB Connections)
$server = "LOCAL";

//Support user emails
$support_email = "helpdesk@ignite4u.co.za";
$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
$IRS_helpdesk = "helpdesk@igniteassist.co.za";
$AIT_helpdesk = "helpdesk@actionassist.co.za";
$helpdesk_emails = array($support_email,$IAS_helpdesk,$IRS_helpdesk,$AIT_helpdesk);
$today = $_SERVER["REQUEST_TIME"];
//$today = strtotime("24 February 2020 01:30:00");
$date_of_last_successful_run = false;//strtotime("23 February 2020 23:59:59");

//Module variables
$source_modules = array("SDBP6","IKPI2");
$mod_table = array('SDBP6'=>"setup_time",'IKPI2'=>"setup_time");	//even though the time table is the same for each module, set it up for future modules where it might be different
$class_names = array('SDBP6'=>"SDBP6_SETUP_TIME",'IKPI2'=>"SDBP6_SETUP_TIME");
$doctitle = "SDBIP v6 & IKPI v2 Automated Time Management";
$totalactions = array();
foreach($source_modules as $modloc) {
	$totalactions[$modloc] = array(
			'rem_emails_sent' =>0,
			'time_periods_closed'=>0,
	);
}

$result1 = $helper->getFixedPageHeader();
$result2 = "<h1>".$doctitle."</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th colspan='2'>Company</th><th>Action</th></tr>";


$cmpcode = "";
$db = new ASSIST_DB("master");
$exclusion_databases = $db->getExclusionDatabases();
$support_databases = $db->getAITSupportDatabases();
foreach($support_databases as $sc => $s) {
	$exclusion_databases[] = $sc;
}

//GET ARRAY OF Companies
$company = array();
$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y'";
$company = $db->mysql_fetch_value_by_id($sql,"cmpcode","cmpname");

//get array of databases
$dbs = array();
$db_prefix = $db->getDBPrefix();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $db->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}


//ASSIST_HELPER::arrPrint($company);
//ASSIST_HELPER::arrPrint($dbs);

//$company = array("KNYDEMO"=>"KNYDEMO");

foreach($company as $cmpcode => $cmpname) {
	//don't act on the "BLANK" module - that is purely for holding empty modules for creating modules on client databases
	if(!in_array(strtoupper($cmpcode),$exclusion_databases)) {
		$result2 .= "
<tr><td class='b'>$cmpcode</td><td class='b'>$cmpname</td>
<td>";
		//check that client exists in database list & change the DB type to client
		$db_name = strtolower($db_prefix.$cmpcode);
		if(in_array($db_name, $dbs)) {
			$client_db = new ASSIST_DB("client", $cmpcode);
			foreach($source_modules as $modloc) {
				$result2 .= "
<p class=b>".$modloc."</p><ul>";
				//check if module exists
				$time_table = $mod_table[$modloc];
				$client_modules = $client_db->checkModuleExistsByModuleLocation($modloc, $time_table);
				//if exists then
				if($client_modules !== false && is_array($client_modules) && count($client_modules) > 0) {
					$result2 .= "<li class='green b'>Module found ".count($client_modules)." time(s) on the DB</li>";
					foreach($client_modules as $modref => $mod) {
						if(isset($mod['table_found']) && $mod['table_found']===true) {
							$_SESSION['modref'] = $modref;
							$result2.="<li class='u'>Acting on ".$mod['modtext']." [$modref]</li>";
							//create class object
							$class_name = $class_names[$modloc];
							/** @var SDBP6_SETUP_TIME $timeObject */
							$timeObject = new $class_name($modref,true,strtolower($cmpcode));
							// check for closures
							$close_results = $timeObject->runAutomatedClosures($today,$date_of_last_successful_run);
							if($close_results===false) {
								$result2.="<li>No time periods closures found for today.</li>";
							} else {
								$result2.="<li class=green><span class='b'>Automatic Closures performed:</span><ul>";
								foreach($close_results as $cr) {
									$result2.="<li class='black'>".$cr['time_name']." for ".$cr['tu'].".</li>";
								}
								$result2.="</ul></li>";
							}
							// check for reminder emails
							$rem_results = $timeObject->generateAutomatedReminders($today,$mod['modtext'],$helpdesk_emails); //ASSIST_HELPER::arrPrint($rem_results);
							if($rem_results===false) {
								$result2.="<li>No time periods reminders set for today.</li>";
							} else {
								$result2.="<li class=green><span class='b'>Automatic reminders sent:</span><ul>";
								foreach($rem_results as $cr) {
									$result2.="<li class='black'>".$cr['time_name']." for ".$cr['tu'].": ".$cr['count']." emails sent</li>";
								}
								$result2.="</ul></li>";
							}
						} else {
							$result2.="<li class='red'>ALERT! ".$mod['modtext']." [$modref] table $time_table <span class='b'>NOT</span> found.</li>";
						}
					}
				} else {
					$result2 .= "<li class='orange'>Module NOT found on the DB</li>";
				}
				$result2 .= "</ul>";
			}
			unset($client_db);
		} else {
			$result2 .= "<p class=red>Client database not found.</p>";
		}
		$result2 .= "</td></tr>";
	}
}

$result2.="</table>";


$result2.= "<h3 class=green>Done!</h3>";
$result2.= "<p>".date("d F Y H:i:s")."</p>";


//SAVE RESULTS

//NOTIFY JANET [AIT]


echo $result1;
echo $result2;


//WRITE RESULTS TO FILE
$echo = "";
$echo.= $result1;
$echo.= $result2."</body></html>";
//WRITE DATA TO FILE
$filename = "Results/SDBP6_TIME_".date("Ymd_His").".html";
$file = fopen($filename,"w");
fwrite($file,$echo."\n");
fclose($file);

//NOTIFY ME OF CLOSURES
$_SESSION['modref'] = 'SDBP6_TIME';
//$to = "janet@actionassist.co.za";
unset($to);
$subject = "SDBP6 / IKPI2 Automatic Time Functions";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
/*$em = new ASSIST_EMAIL($to,$subject,$message,"HTML");
	$em->setSenderUserID("AUTO");
	$em->setSenderCmpCode("IASSIST");
	$em->setRecipientUserID("0001");

$em->sendEmail();
*/
include("cron_job_send_email.php");


?>