<?php
require_once("../module/autoloader.php");


$json = file_get_contents('php://input');
$action = json_decode($json, true);

$req = $_REQUEST;


$ignore_charsets = array("iso-2022-jp","big5","euc-kr","shift-jis","x-euc-jp","euc-cn","iso-2022-kr");
$has_bad_charset = false;
if(isset($req['email'])) {
	foreach($ignore_charset as $charset) {
		if(stripos($req['email'],$charset)!==false) {
			$has_bad_charset = true;
		}
	}
}


//ignore spam emails from asia
if(!$has_bad_charset) {


$db = new ASSIST_DB("master");

$sql = "INSERT INTO assist_email_log_inbound_sendgrid VALUES (null, now(), '".serialize($action)."','')";
//$db->db_insert($sql);

$sql = "INSERT INTO assist_email_log_inbound_sendgrid VALUES (null, now(), '".serialize($req)."','".$req['to']."','".$req['subject']."')";
$db->db_insert($sql);


//$sql = "INSERT INTO assist_email_log_inbound_sendgrid_raw VALUES (null, now(), '".base64_encode($json)."')";
//$db->db_insert($sql);


$emailObject = new ASSIST_EMAIL();
$emailObject->setContentType("HTML");
$emailObject->setRecipient("janet@actionassist.co.za");
$emailObject->setSubject("New email received via SendGrid: ".$req['to']);
$message = date("d F Y H:i:s")."<br /><br /><ul>";
foreach($req as $fld => $r) {
	$message.= "<li>[".$fld."] => ".$r.";</li>";
}
$message.="</ul>";
$emailObject->setBody($message);
$emailObject->sendEmail();


}


?>