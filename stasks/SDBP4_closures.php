<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  SDBP4_closures not executed.");
}

include("../inc_assist.php");
include("../inc_codehelper.php");
include("../inc_db_conn.php");
include("../SDBP4/inc/inc.php");
//phpinfo();
	$support_email = "helpdesk@ignite4u.co.za";
	//$support_email = "igniteassist_dev@gmail.com";
	$IAS_helpdesk = "helpdesk@igniteconsult.co.za";
	//$IAS_helpdesk = "igniteassistdev@gmail.com";
	$IRS_helpdesk = "helpdesk@igniteassist.co.za";
	$from = "no-reply@ignite4u.co.za";
	$header1 = "From: ".$from."\r\nReply-to: ".$from."\r\n";
$today = $_SERVER["REQUEST_TIME"];
//error_reporting(-1);
$arr_modref = array("sdp11","sdp12","sp12b");
$mod_table = "list_time";
$modtitle = "SDBIP v4";
$totalactions = 0;

$tkid = "IA";
$_SESSION['tkn'] = "Ignite Assist";

$result1 = "<style type=text/css>";
$result1.= "table {";
$result1.= "    border-collapse: collapse;";
$result1.= "}";
$result1.= "td {";
$result1.= "	color: #000000;";
$result1.= "	text-align: left;";
$result1.= "	background-color: #ffffff;";
$result1.= "	font-weight: normal;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "th {";
$result1.= "	color: #ffffff;";
$result1.= "	text-align: center;";
$result1.= "	background-color: #cc0001;";
$result1.= "	font-weight: bold;";
$result1.= "	text-decoration: none;";
$result1.= "	font-family: Verdana, Arial, Helvetica, sans-serif;";
$result1.= "	font-size: 10pt;";
$result1.= "	line-height: 12pt;";
$result1.= "}";
$result1.= "</style>";
$result2 = "<h1>$modtitle ~ Scheduled Tasks: Closures</h1>";
$result2.= "<table cellpadding=3 cellspacing=0>";
$result2.= "<tr><th>CMPCODE</th><th>Action</th></tr>";


$cmpcode = "";
//GET ARRAY OF CMPCODE
include("../inc_db.php");
$company = array();
$sql = "SELECT cmpcode FROM assist_company WHERE cmpstatus = 'Y'";
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[] = $row['cmpcode'];
    }

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close();

//FOREACH COMPANY
foreach($company as $cmpcode)
{
	set_time_limit(1800);
	$cmpcode = strtolower($cmpcode);
	//include("../inc_db.php");
	$db_name = $db_other.$cmpcode;
	//IF DATABASE EXISTS
	if (in_array($db_other.$cmpcode, $dbs, true)) {
		$result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>";
		foreach($arr_modref as $modref) {
			$result2.=strtoupper($modref).": ";
			//CHECK IF MODULE/TABLE EXISTS
			$module_exists = getModuleRefExistance($modref,$mod_table);
			//IF EXISTS
			if($module_exists) {
				$dbref = "assist_".$cmpcode."_".$modref;
			//	include("SDBP4/inc/inc_cron.php");
				//GET OPEN TIME
					$sql = "SELECT * FROM ".$dbref."_list_time 
							WHERE active = true
							AND start_date < '".date("Y-m-d H:i:s")."'
							AND ( 
									(close_primary > 0 AND close_primary<=$today AND active_primary = true)
								OR 
									(close_secondary > 0  AND close_secondary <=$today AND active_secondary = true)
								OR 
									(close_finance > 0  AND close_finance <=$today AND active_finance = true) 
								)";
					$time = Omysql_fetch_all_fld($sql,$cmpcode,"id");
					//arrPrint($time);
				if(!(count($time)>0)) {
					$result2.= "No active time periods with auto closures set.";
				} else {
					$actions = array();
					foreach($time as $ti => $t) {
						$resets = array();
						$reset_fld = array();
						$log_change = array();
						//primary
						$fld = "primary"; $section = "Departmental SDBIP";
						if($t['close_'.$fld]<=$today && $t['active_'.$fld]) {
							$reset_fld[] = $fld;
							$resets[] = "active_".$fld." = false, rem_".$fld." = 0, close_".$fld." = 0";
							$log_change[] = " - $section - ".date("F Y",strtotime($t['end_date']))." for $fld users.";
						}
						//secondary
						$fld = "secondary"; $section = "Departmental SDBIP";
						if($t['close_'.$fld]<=$today && $t['active_'.$fld]) {
							$reset_fld[] = $fld;
							$resets[] = "active_".$fld." = false, rem_".$fld." = 0, close_".$fld." = 0";
							$log_change[] = " - $section - ".date("F Y",strtotime($t['end_date']))." for $fld users.";
						}
						//toplayer
						$fld = "secondary"; $section = "Top Layer SDBIP";
						if($t['close_'.$fld]<=$today && $t['active_'.$fld]) {
							$log_change[] = " - $section - ".date("F Y",strtotime($t['end_date'])).".";
						}
						//finance
						$fld = "finance"; $section = "Financials";
						if($t['close_'.$fld]<=$today && $t['active_'.$fld]) {
							$reset_fld[] = $fld;
							$resets[] = "active_".$fld." = false, rem_".$fld." = 0, close_".$fld." = 0";
							$log_change[] = " - $section - ".date("F Y",strtotime($t['end_date'])).".";
						}
						if(count($resets)>0) {
							$sql = "UPDATE ".$dbref."_list_time SET ".implode(", ",$resets)." WHERE id = ".$ti;
							$rs = OgetRS($sql,$cmpcode);
											$v_log = array(
												'section'=>"TIME",
												'ref'=>$ti,
												'field'=>implode(", ",$reset_fld),
												'text'=>"Performed Automatic Closures:<br />".implode("<br />",$log_change),
												'old'=>"",
												'new'=>"",
												'YN'=>"Y"
											);
											logChanges('SETUP',0,$v_log,code($sql));
											
							$totalactions+=count($log_change);
						}
					}	//foreach time
					$result2.=$v_log['text']."";
				}//if count time > 0
			} else {
				$result2.= "Module $modref doesn't exist.<br />";
			}
		}
		$result2.= "</td></tr>";
    } else {
        $result2.= "<tr><td>".strtoupper($cmpcode)."</td><td>No database found.</td></tr>";
    }
}
$result2.="</table>";


//WRITE RESULTS TO FILE
$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"http://assist.ignite4u.co.za/default.css\" type=\"text/css\">";
$echo.= $result1;
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
$echo.= $result2."</body></html>";
		//WRITE DATA TO FILE
		$filename = "Results/SDBP4_ACLOSE_".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

//NOTIFY ME OF CLOSURES
$to = "janet@igniteassist.co.za";
$from = "no-reply@ignite4u.co.za";
$subject = "$modtitle Auto closures";
$message = "<p>Results: <a href=http://".$_SERVER["SERVER_NAME"]."/stasks/".$filename.">".$filename."</a></p>";
$header = "Content-type: text/html; charset=us-ascii";
mail($to,$subject,$message,$header);

		
echo "Closures performed: ".$totalactions;


?>
