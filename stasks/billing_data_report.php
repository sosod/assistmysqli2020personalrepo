<?php
include("../inc_status.php");
if($assiststatus=="U") {
	exit("Assist Status set to Maintenance.  Billing_data_report not executed.");
}

//error_reporting(-1);
$_SESSION['cc'] = "IASSIST";
$_SESSION['tid'] = "0000";
$_REQUEST['cc'] = "IASSIST";
$_REQUEST['tki'] = "0000";
$_REQUEST['cmpcode'] = "ALL_ALLCMP";
$_REQUEST['src'] = "AUTO";

$cmpcode = "";
require_once '../inc_db.php';
require_once '../inc_db_conn.php';

$get_session = false;

$today = strtotime(date("d F Y H:i:s"));

//$tkadddate = strtotime("25 August 2012 00:00:00");
$report_folder = "../reports/files/";

require_once('../reports/data_summary.php');

$data_sum_id = $hist_id;

require_once('../reports/data_report.php');

$data_detail_id = $hist_id;

if(isset($tkadddate)) { $today = $tkadddate; }

$_SESSION['modref'] = "BILLING_DATA";
//$to = "igniteassistdev@gmail.com";
$to = "accounts@actionassist.co.za";
//$to = "janet@actionassist.co.za";
//$from = "no-reply@ignite4u.co.za";
$subject = "Data Report (".date("d F Y",$today).")";
$message = "<p>Data Report drawn as at ".date("d F Y H:i:s",$today)."</p>
<p>Summary Report: <a href='http://".$_SERVER['HTTP_HOST']."/billing_report_history.php?h=".$data_sum_id."'>Click here</a></p>
<p>Detail Report: <a href='http://".$_SERVER['HTTP_HOST']."/billing_report_history.php?h=".$data_detail_id."'>Click here</a></p>";
//$header = "From: $from \r\nBCC: janet@actionassist.co.za\r\nContent-type: text/html; charset=us-ascii";
//$header = "From: $from \r\nContent-type: text/html; charset=us-ascii";

//mail($to,$subject,$message,$header);
include("cron_job_send_email.php");



//phpinfo();
?>