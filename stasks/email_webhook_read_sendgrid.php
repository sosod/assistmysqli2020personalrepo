<?php
require_once("../module/autoloader.php");
//error_reporting(-1);

$db = new ASSIST_DB("master");
$recipient = "kaylenet@beaufort";
$start = "1 September 2017 00:00";
$sql = "SELECT * FROM assist_email_log_sendgrid_processed WHERE final_date > '".date("Y-m-d H:i:s",strtotime($start))."' AND email LIKE '".$recipient."%'";
$rows = $db->mysql_fetch_all($sql);

$me = new ASSIST_HELPER();
$me->echoPageHeader();

echo "
<style type=text/css>
thead th { background-color:#ffffff;
color:#000099;
border: 1px solid #ababab;
</style>
<h1>Assist Email Log Report</h1>
<p class=i>Report drawn on ".date("d F Y H:i")." showing all emails sent to ".$recipient."* since ".$start.".</p>
<table>
	<thead>
		<tr>
			<th>Assist Email Log ID</th>
			<th>Date</th>
			<th>Assist SMTP ID</th>
			<th>Sendgrid SMTP Server ID</th>
			<th>Recipient</th>
			<th>Subject</th>
			<th>Result</th>
		</tr>
	</thead>
	<tbody>";
		
		foreach($rows as $row) {
$log_data = unserialize(base64_decode($row['log_data']));
//echo "<pre>";print_r($log_data); echo "</pre>";
$log_keys = array_keys($log_data);
$log_data = $log_data[$log_keys[0]];
$subject = str_replace('+',' ',$log_data['subject']);
$response = ucfirst($log_data['event']).": ".$log_data['response'];
$smtp_id = explode("@",$row['smtp_id']);
$smtp_id[0] = str_replace(".","<br />.",$smtp_id[0]);
$smtp_id = implode("<br />@",$smtp_id);
			echo "
			<tr>
				<td>".$row['id']."</td>
				<td>".$row['final_date']."</td>
				<td>".$smtp_id."</td>
				<td>".$row['sg_message_id']."</td>
				<td>".$row['email']."</td>
				<td>".$subject."</td>
				<td>".$response."</td>
			</tr>";
		}
				//<td><pre>"; print_r(unserialize($row['log_data'])); echo "</pre></td>
		?>
	</tbody>
</table>
</body></html>