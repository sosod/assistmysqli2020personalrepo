<?php
require_once("inc_header.php");

$emailObject = new ASSIST_EMAIL();
$emailObject->setRecipient("janet@actionassist.co.za");
$emailObject->setCC("actionit.actionassist@gmail.com");
$emailObject->setSubject("Alert: IDP3 - Admin - Fin section accessed");
$body = "ALERT: IDP3/admin_fin_view.php has been accessed by ".$helper->getUserName()." (".$helper->getUserID().") of ".strtoupper($helper->getCmpCode())." in module '".$helper->getModRef()."'";
$emailObject->setBody($body);
$emailObject->sendEmail();

ASSIST_HELPER::displayResult(array("info","This section is no longer valid and has been removed."));
die();


//ASSIST_HELPER::arrPrint($_REQUEST);
$reportObject = new IDP3_FIXED_REPORT();
$data = $reportObject->getReport("admin_fin_".$_REQUEST['ref'], $_REQUEST['object_id']);

//ASSIST_HELPER::arrPrint($data);

echo "
	<table class=list>
		<tr>
";

$head = $data['head'];
	foreach($head as $fld => $h) {
		$rowspan = 1;
		$colspan = 1;
		echo "<th rowspan=".$rowspan." colspan=".$colspan.">".$h."</th>";
	}

echo "</tr>";

$rows = $data['rows'];

foreach($rows as $i => $r) {
	echo "
	<tr>";
	foreach($head as $fld => $h) {
		echo "<td >".(isset($r[$fld]) ? $r[$fld] : "")."</td>";
	}
	echo "
	</tr>";
}

echo "</table>";
?>