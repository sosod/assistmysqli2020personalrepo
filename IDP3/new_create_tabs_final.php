<?php
$echo_detail = false;
$page_activity = "CONFIRM";




echo "<div style='width:48%' class=float>";
include("common/idp_object.php");
echo "</div>";

$idp_details = $idpObject->getRawObject($idp_object_id);
$idp_status = $idp_details['idp_status'];
$has_been_requested = ($idp_status&IDP3_IDP::CONFIRMATION_REQUESTED)==IDP3_IDP::CONFIRMATION_REQUESTED;


echo "<div style='width:48%'>
<div style='width:500px;margin:0 auto; padding:10px; border-radius: 8px;border-style:solid; border-width:1px' class='blue-border'>
";
if($can_confirm) {

	$userObject = new IDP3_USERACCESS();
	$confirm_users = $userObject->getUsersWithConfirmationAccess();

	if(count($confirm_users)>0) {
		echo "
		<form name=frm_request>
		<input type=hidden name=object_id value='$idp_object_id' />";
		if($has_been_requested===true) {
			echo "<p>Confirmation has been previously requested but users can be notified again if required.</p>";
		}
		echo "
		<P class=center><button id=btn_request_confirmation>Request Confirmation</button></p>
		<p>The following user(s) have access to Confirmation and will be notified:</p>
		<ul>
		";
		foreach($confirm_users as $i => $t) {
			echo "<li><input type=hidden name=req_notify[] value='$i' />$t</li>";
		}
		echo "</ul>";
	} else {
		echo "
		<P class=center><button id=btn_request_confirmation disabled=disabled>Request Confirmation</button></p>

		";
		ASSIST_HELPER::displayResult(array("error","No users have access to Confirmation so Confirmation is not possible."));
	}
} else {
	echo "<P class=center>Confirmation not possible at this time.<br />&nbsp;<br />Please review the error message on the right.</p>";
}
echo "
</div>
</div>";
?>
<script type="text/javascript">
$(function() {
	$("button").button();
	$("#btn_request_confirmation").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var dta = AssistForm.serialize($("form[name=frm_request]"));
		var result = AssistHelper.doAjax("inc_controller.php?action=IDP.requestConfirmation",dta);
		if(result[0]=="ok") {
			AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"new_create.php");
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
});
</script>