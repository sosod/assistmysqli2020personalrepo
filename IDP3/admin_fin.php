<?php
require_once("inc_header.php");

$emailObject = new ASSIST_EMAIL();
$emailObject->setRecipient("janet@actionassist.co.za");
$emailObject->setCC("actionit.actionassist@gmail.com");
$emailObject->setSubject("Alert: IDP3 - Admin - Fin section accessed");
$body = "ALERT: IDP3/admin_fin.php has been accessed by ".$helper->getUserName()." (".$helper->getUserID().") of ".strtoupper($helper->getCmpCode())." in module '".$helper->getModRef()."'";
$emailObject->setBody($body);
$emailObject->sendEmail();

ASSIST_HELPER::displayResult(array("info","This section is no longer valid and has been removed."));
die();




//ASSIST_HELPER::arrPrint(explode("_",$_SERVER['PHP_SELF']));
ASSIST_HELPER::displayResult(array("error","Warning: This section is still under development & waiting on confirmation of mSCOA & Financial Service Provider requirements"));
ASSIST_HELPER::displayResult(array("info","DEVELOPER NOTES: SDBIP reports included for testing purposes until SDBIP module development complete."));
ASSIST_HELPER::displayResult(array("info","DEVELOPER NOTES 2: Financial integration STILL PENDING - need the FSP details"));


$idpObject = new IDP3_IDP();
$objects = $idpObject->getObject(array('type'=>"LIST",'section'=>"REPORT",'page'=>"VIEW"));
$filter = array();
foreach($objects['rows'] as $idp_id => $idp) {
	$filter[$idp_id] = $idp[$idpObject->getNameFieldName()]['display']."&nbsp;(".$idp[$idpObject->getTableField()."_ref"]['display'].")&nbsp;&nbsp;<span class=float>[".$idp[$idpObject->getIDFieldName()]['display']."]</span>";
}

function echoSelectFilter($id) {
	global $idpObject;
	global $filter;
	$select_filter = "<select name=".$id." id=".$id."><option selected value=X>--- SELECT ".$idpObject->getObjectName($idpObject->getMyObjectName())." ---</option>";
				foreach($filter as $idp_id => $idp) {
					$select_filter.= "<option value=".$idp_id.">".$idp."</option>";
				}
	$select_filter.= "</select>";
	echo $select_filter;
}

?>
<h2>Output</h2>
<table class=tbl_list>
	<tr>
		<th>Module</th>
		<th>Output</th>
		<th>Description</th>
		<th>Filter</th>
		<th>Preview</th>
		<th>CSV Export</th>
		<th>Financial<Br />System</th>
	</tr>
	<tr><?php $id = "indicator_push"; ?>
		<td class=b>IDP / SDBIP</td>
		<td class=b>Indicator List</td>
		<td >List of KPIs & Projects for allocation of expenses</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_view' ref="<?php echo $id; ?>">View</button></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_push' ref="<?php echo $id; ?>">Push</button></td>
	</tr>
	<tr><?php $id = "annual_push"; ?>
		<td class=b>IDP</td>
		<td class=b>Annual Budget - PUSH</td>
		<td >Send Annual Budget per mSCOA segment per Financial Year to Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_view' ref="<?php echo $id; ?>">View</button></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_push' ref="<?php echo $id; ?>">Push</button></td>
	</tr>
	<tr><?php $id = "sdbip_budget_push"; ?>
		<td class=b>SDBIP</td>
		<td class=b>Monthly Budget - PUSH</td>
		<td >Send Monthly Budget per mSCOA segment to Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_view' ref="<?php echo $id; ?>">View</button></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_push' ref="<?php echo $id; ?>">Push</button></td>
	</tr>
	<tr><?php $id = "mscoa_push"; ?>
		<td class=b>IDP</td>
		<td class=b>mSCOA Segments - PUSH</td>
		<td >Send mSCOA segments to Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_view' ref="<?php echo $id; ?>">View</button></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_push' ref="<?php echo $id; ?>">Push</button></td>
	</tr>
</table>
<h2>Input</h2>
<table class=tbl_list>
	<tr>
		<th>Module</th>
		<th>Input</th>
		<th>Description</th>
		<th>Filter</th>
		<th>Template</th>
		<th>CSV Import</th>
		<th>Financial<br />System</th>
	</tr>
	<tr><?php $id = "annual_pull"; ?>
		<td class=b>IDP</td>
		<td class=b>Annual Budget - PULL</td>
		<td >Read Annual Budget per mSCOA segment per Financial Year from Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_import' ref="<?php echo $id; ?>">Start Import</button></td>
		<td ><button class='abutton btn_pull' ref="<?php echo $id; ?>">Pull</button></td>
	</tr>
	<tr><?php $id = "mscoa_pull"; ?>
		<td class=b>IDP</td>
		<td class=b>mSCOA Segments - PULL</td>
		<td >Read mSCOA segments from Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_import' ref="<?php echo $id; ?>">Start Import</button></td>
		<td ><button class='abutton btn_pull' ref="<?php echo $id; ?>">Pull</button></td>
	</tr>
	<tr><?php $id = "sdbip_budget_pull"; ?>
		<td class=b>SDBIP</td>
		<td class=b>Monthly Budget - PULL</td>
		<td >Read Monthly Budget per mSCOA segment from Financial System</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_import' ref="<?php echo $id; ?>">Start Import</button></td>
		<td ><button class='abutton btn_pull' ref="<?php echo $id; ?>">Pull</button></td>
	</tr>
	<tr><?php $id = "sdbup_actual_pull"; ?>
		<td class=b>SDBIP</td>
		<td class=b>Actual Expenditure</td>
		<td >Actual expenditure per Indicator per month</td>
		<td ><?php echoSelectFilter($id); ?></td>
		<td ><button class='abutton btn_generate' ref="<?php echo $id; ?>">Generate</button></td>
		<td ><button class='abutton btn_import' ref="<?php echo $id; ?>">Start Import</button></td>
		<td ><button class='abutton btn_pull'  ref="<?php echo $id; ?>">Pull</button></td>
	</tr>
</table>
<div id=dlg_display>
	<iframe id=ifr_display width=500 height=350 src=""></iframe>
</div>
<script type="text/javascript">
$(function() {
	$("#dlg_display").dialog({
		autoOpen: false,
		modal: true,
		width: 600,
		height: 400
	});
	$("button.btn_view").button({
		icons: { primary:"ui-icon-newwin" }
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var r = $(this).attr("ref");
		var v = $("#"+r).val();
		if(v=="X") {
			AssistHelper.finishedProcessing("error","Error: Please select a <?php echo $idpObject->getObjectName($idpObject->getMyObjectName()); ?>.");
		} else {
			var url = "admin_fin_view.php?ref="+r+"&object_id="+v;
			$("#ifr_display").prop("src",url);
			$("#dlg_display").dialog("open");
			AssistHelper.closeProcessing();
		}
	});
	$("button.btn_generate").button({
		icons: { primary:"ui-icon-arrowrefresh-1-n" }
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		AssistHelper.finishedProcessing("info","Under development");
	});
	$("button.btn_push").button({
		icons: { primary:"ui-icon-arrow-1-n" }
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		AssistHelper.finishedProcessing("info","Under development");
	});
	$("button.btn_pull").button({
		icons: { primary:"ui-icon-arrow-1-s" }
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		AssistHelper.finishedProcessing("info","Under development");
	});
	$("button.btn_import").button({
		icons: { primary:"ui-icon-arrowrefresh-1-s" }
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		AssistHelper.finishedProcessing("info","Under development");
	}).css("margin-left","5px");
	$("button.abutton").css("font-weight","normal").css("background","url()");
	$("table.tbl_list tr").each(function() {
		$(this).find("td:gt(2)").addClass("center");
	})
});
</script>