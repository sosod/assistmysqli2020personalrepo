<?php
require_once("inc_header.php");


$emailObject = new ASSIST_EMAIL();
$emailObject->setRecipient("janet@actionassist.co.za");
$emailObject->setCC("actionit.actionassist@gmail.com");
$emailObject->setSubject("Alert: IDP3 - Admin - Fin section accessed");
$body = "ALERT: IDP3/admin_fin_setup.php has been accessed by ".$helper->getUserName()." (".$helper->getUserID().") of ".strtoupper($helper->getCmpCode())." in module '".$helper->getModRef()."'";
$emailObject->setBody($body);
$emailObject->sendEmail();

ASSIST_HELPER::displayResult(array("info","This section is no longer valid and has been removed."));
die();


?>
<p>Here is where the user will specify any necessary settings to permit the module to connect to the financial system.</p>