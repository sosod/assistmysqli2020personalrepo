<?php
require_once("inc_header.php");
//ASSIST_HELPER::arrPrint($_REQUEST);

if(isset($_REQUEST['tab_act'])) {
	$object_id = $_REQUEST['idp_object_id'];
} else {
	$object_id = $_REQUEST['object_id'];
}
$idp_object_id = $object_id;
$page_redirect_path = "new_create_idp.php?object_id=".$object_id;

if(!isset($_REQUEST['tab'])) {
	$_REQUEST['tab'] = "strategy";
}


$idpObject = new IDP3_IDP();
$idp_heading = $idpObject->getHeading($idp_object_id);
//Catch funny whitespace characters & line breaks in the IDP name
$ix = explode(chr(10),$idp_heading);
foreach($ix as $i => $x) {
	$ix[$i] = trim($x);
}
$second_heading = " ".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER." ".implode(" / ",$ix);



$tabs = array(
	"strategy",
	"stratplan",
	"stratprojects",
	"stratperfmeasures",
	"final",
);

$tab_keys = array_flip($tabs);

?>
<button style='position:absolute;top:10px;right:10px;z-index:999999999;' id=btn_hide_headings state=show>Hide Headings</button>
<div id="tabs">
	<ul>
		<?php
		foreach($tabs as $t) {
			if($t=="final") {
				echo "<li><a href='#".$t."'>Finalise</a></li>";
			} else {
				echo "<li><a href='#".$t."'>".$helper->getObjectName($t)."</a></li>";
			}
		}
		?>
	</ul>

<?php
foreach($tabs as $t) {
	echo "<div id=".$t.">";
	if($_REQUEST['tab']==$t) {
		echo "<div id=div_placeholder >";
			include("new_create_tabs_".$t.".php");
		echo "</div>";
	} else {
		echo "
		<div class=center>
			<p>Loading... please wait</p>
		</div>
		";
	}
	echo "</div>";
			//<p><img src=/pics/ajax_loader_v2.gif /></p>
}
?>
</div>


<script type=text/javascript>
$(function() {

	<?php //echo $js; ?>


	$("#h1_page_heading").append(AssistString.code("<?php echo $second_heading; ?>"));
	$("#btn_hide_headings").button({
		icons: { primary:"ui-icon-circle-arrow-n" }
	}).click(function(e) {
		e.preventDefault();
		var s = $(this).attr("state");
		if(s=="show") {
			$("div#m1").hide();
			$("#h1_page_heading").hide();
			$(this).attr("state","hide");
			$(this).button( "option", "icons", { primary: "ui-icon-circle-arrow-s" } );
			$("span", this).text("Show Headings");
			$(this).blur();
		} else {
			$("div#m1").show();
			$("#h1_page_heading").show();
			$(this).attr("state","show");
			$(this).button( "option", "icons", { primary: "ui-icon-circle-arrow-n" } );
			$("span", this).text("Hide Headings");
			$(this).blur();
		}
		$(window).trigger("resize");
	});

	$("#tabs").tabs({
		active: <?php echo $tab_keys[$_REQUEST['tab']]; ?>,
		activate: function( event, ui ) {
			AssistHelper.processing();
			var tab = AssistString.substr(ui.newPanel.selector,1);
			document.location.href = "new_create_idp.php?object_id=<?php echo $idp_object_id; ?>&tab="+tab;
		}
	}).css("margin","0px");
	$(".supplier-tab").css("float","right");



	var offset = $("#tabs").offset();
	$(window).resize(function() {
		var state = $("#btn_hide_headings").attr("state");
		var scr = AssistHelper.getWindowSize();
		if(state=="hide") {
			var space_available_for_tabs = scr.height-5;
			var tab_height = space_available_for_tabs<350 ? 350 : (space_available_for_tabs-10);
		} else {
			var space_available_for_tabs = scr.height - offset.top;
			var tab_height = space_available_for_tabs<350 ? 350+Math.floor(offset.top) : (space_available_for_tabs-10);
		}
		$("#tabs").css("height",tab_height+"px");
		$("#div_placeholder").css({"height":(tab_height-50)+"px","overflow":"auto"});
	});
	$(window).trigger("resize");
});
</script>
<?php //ASSIST_HELPER::arrPrint($_REQUEST);
?>