<?php
$my_tab = "stratperfmeasures";
$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "new_create_".$my_tab.".php?object_id=".$parent_object_id."&";
}
include("inc_header.php");
//ASSIST_HELPER::arrPrint($_REQUEST);

$object_type = $_REQUEST['object_type'];
//$page_action = $_REQUEST['page_action'];
$action = $_REQUEST['page_action']; 
if(strpos($action,".")!==false) {
	$act = explode(".",$action);
	$page_action = $act[1];
	$section = $act[0];
} else {
	$section = "MANAGE";
	$page_action = $action;
}
//Changed edit option to include IDP ID - need IDP ID for edit/view as well in order to get the mSCOA version id - JC AA-407 1 June 2020
if($page_action=="add") {
	$parent_object_id = $_REQUEST['object_id']."_".$_REQUEST['idp_object_id'];
	$child_object_id = 0;
} else {
	$child_object_id = $_REQUEST['object_id'];
	$parent_object_id = "0_".$_REQUEST['idp_object_id'];
}
//echo $object_type;
switch($object_type) {
	case "PMKPA":
		$parent_object_type = "IDP";
		$childObject = new IDP3_PMKPA();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = new IDP3_IDP();
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
	case "PMPROG":
		$parent_object_type = "PMKPA";
		$childObject = new IDP3_PMPROG();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = new IDP3_PMKPA();
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
	case "PMKPI":
		$parent_object_type = "PMPROG";
		$childObject = new IDP3_PMKPI();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id)); 
		}
		$parentObject = new IDP3_PMPROG();
		$child_object_type = $object_type;
		$parent_id_name = $childObject->getParentFieldName();
		break;
}

$child_redirect = "new_create_".$my_tab.".php?object_type=".$object_type."&object_id=";
$child_name = $helper->getObjectName($childObject->getMyObjectName());



//ASSIST_HELPER::arrPrint($_REQUEST);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php 
	if($display_type=="default") {
		$td2_width = "48%"; 
	?>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="IDP"));
			//$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>><h2>New <?php echo $child_name; ?></h2>
	<?php 
	} else {
		echo "<td>";
	}
	
	//echo $childObject->getMyObjectType();
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
			
			$js.= $displayObject->drawPageFooter("",strtolower($child_object_type),"object_type=".$child_object_type."&object_id=".$child_object_id."&log_type=".IDP3_LOG::EDIT);
			
		?></td>
	</tr>
	<?php if(isset($child_objects) && count($child_objects)>0 && $display_type=="default") { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	?>
	$("input:button.btn_edit").click(function() {
		var i = $(this).attr("ref");
		document.location.href = '<?php echo $child_redirect; ?>'+i;
	});
	
	
	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST,true,true);
	}
	?>
	
});
</script>