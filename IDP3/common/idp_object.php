<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
$cc = $_SESSION['cc'];
function markTime($s) {
	global $start;
	global $cc;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	//if($cc=="ait555d") {
//		echo '<p>'.$s.' => '.$total_time.' seconds.';
	//}
}


/**
 * REQUIRED variable:
 * 		$page_activity
 * 			CONFIRM
 * 			ACTIVATE_WAITING
 * 			ACTIVATE_DONE
 * 			VIEW
 *
 */

if(!isset($echo_detail)) { $echo_detail = true; }
//error_reporting(-1);
if(!isset($page_activity)) { $page_activity = "VIEW"; }
$view_final_section = array("CONFIRM","ACTIVATE_WAITING","ACTIVATE_DONE");



function linebreak() {
	return "<p style='font-size:50%; line-height: 50%'>&nbsp;</p><hr  width=50% /><p style='font-size:50%; line-height: 50%'>&nbsp;</p>";
}

$page_action = "VIEW";

require_once("inc_header.php");

$idp_object_id = $_REQUEST['object_id'];

$helperObject = new IDP3();
$idpObject = new IDP3_IDP();
	$raw_idp = $idpObject->getRawObject($idp_object_id);
	$mscoa_version_id = $raw_idp['idp_mscoa_version_id'];


//GET ALL LISTS & THEIR VALUES
$lists = array();
$listObject = new IDP3_LIST();
$list_headings = $headingObject->getStandardListHeadings();
foreach($list_headings as $fld => $l) {
	$listObject->changeListType($fld);
	$lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
	if($fld=="measurement_unit") {
		$lists['TARGET_TYPE_CODES'] = $listObject->getTargetTypeCodes();
	}
}
$listObject = new IDP3_SEGMENTS();
$list_headings = $listObject->getSegmentDetails();
foreach($list_headings as $fld => $l) {
	$return_no_posting = array('return_no_posting'=>false,'version'=>$mscoa_version_id);
	$listObject->setSection($fld);
	$lists[$fld] = $listObject->getActiveListItemsFormattedForSelect($return_no_posting);
}

$total_object_count = 0;
$can_confirm = true;
$warning = array();
$fatal = array();

ob_start();

//IDP DETAILS
	$object_type = $idpObject->getMyObjectType();
	$object_id = $idp_object_id;
	$idp_form = $displayObject->getDetailedView($object_type,$object_id,false,false,false,array(),false,false);
	echo "<div style='width:500px'>".$idp_form['display']."</div>";
	$js.= $idp_form['js'];



/************************************
 * STRATEGY
 */
$visionObject = new IDP3_VISION();
$missionObject = new IDP3_MISSION();
$valuesObject = new IDP3_VALUES();
echo "<h2>".$helperObject->getObjectName("STRATEGY")."</h2>";
//VISION
$activeObject = $visionObject;
$object_type = $activeObject->getMyObjectType();
$latest = $activeObject->getMostRecentObject($idp_object_id,true);
if(!isset($latest['id']) || is_null($latest['id'])) {
	$latest=array(
		'name'=>"<span class='iinform i'>No final available.</span>",
	);
	$warning[$object_type] = "No final ".$helperObject->getObjectName($activeObject->getMyObjectName());
}
echo "
<h3>".$activeObject->getObjectName($activeObject->getMyObjectName())."</h3>
<div style='width:500px'>
<p>".ASSIST_MODULE_DISPLAY::convertStringToHTML($latest['name'])."</p>
</div>

";


//MISSION
$activeObject = $missionObject;
$object_type = $activeObject->getMyObjectType();
$latest = $activeObject->getMostRecentObject($idp_object_id,true);
if(!isset($latest['id']) || is_null($latest['id'])) {
	$latest=array(
		'name'=>"<span class='iinform i'>No final available.</span>",
	);
	$warning[$object_type] = "No final ".$helperObject->getObjectName($activeObject->getMyObjectName());
}
echo "
<h3>".$activeObject->getObjectName($activeObject->getMyObjectName())."</h3>
<div style='width:500px'>
<p>".ASSIST_MODULE_DISPLAY::convertStringToHTML($latest['name'])."</p>
</div>

";


//VALUES
$activeObject = $valuesObject;
$object_type = $activeObject->getMyObjectType();
$latest = $activeObject->getMostRecentObject($idp_object_id,true);
if(!isset($latest['id']) || is_null($latest['id'])) {
	$latest=array(
		'name'=>"<span class='iinform i'>No final available.</span>",
	);
	$warning[$object_type] = "No final ".$helperObject->getObjectName($activeObject->getMyObjectName());
}
echo "
<h3>".$activeObject->getObjectName($activeObject->getMyObjectName())."</h3>
<div style='width:500px'>
<p>".ASSIST_MODULE_DISPLAY::convertStringToHTML($latest['name'])."</p>
</div>

";












/************************************
 * STRATEGIC PLAN
 */
echo linebreak()."
<h2>".$helperObject->getObjectName("STRATPLAN")."</h2>";

$stratobjObject = new IDP3_STRATOBJ();
$so_object_type = $stratobjObject->getMyObjectType();
$so_object_name = $stratobjObject->getMyObjectName();
$stratfocusObject = new IDP3_STRATFOCUS();
$sf_object_type = $stratfocusObject->getMyObjectType();
$sf_object_name = $stratfocusObject->getMyObjectName();
$stratgoalObject = new IDP3_STRATGOAL();
$sg_object_type = $stratgoalObject->getMyObjectType();
$sg_object_name = $stratgoalObject->getMyObjectName();
$stratresultObject = new IDP3_STRATRESULT();
$sr_object_type = $stratresultObject->getMyObjectType();
$sr_object_name = $stratresultObject->getMyObjectName();
$sr_targettype_field = $stratresultObject->getTargetTypeField();

$stratplan_records = $stratobjObject->getOrderedObjectsWithChildren($idp_object_id);


$headings = $headingObject->getMainObjectHeadings($sr_object_type,"FORM",		"NEW",			"",			true);
$headings = $headings['rows'];
$lists['IDP3_STRATRESULT'] = array();
$sr_name_field = $stratresultObject->getNameFieldName();

$count = array();
$count_sg = array();
$count_sf = array();
$count_so = array();

$total_object_count+=count($stratplan_records[$so_object_type]);
$total_object_count+=count($stratplan_records[$sf_object_type]);
$total_object_count+=count($stratplan_records[$sg_object_type]);
$total_object_count+=count($stratplan_records[$sr_object_type]);

foreach($stratplan_records[$so_object_type] as $so_id => $so) {
	$count["SO".$so_id] = array();
	$count_so[$so_id] = 0;
	if(isset($stratplan_records[$sf_object_type][$so_id])) {
		foreach($stratplan_records[$sf_object_type][$so_id] as $sf_id => $sf) {
			$count["SO".$so_id]["SF".$sf_id] = array();
			$count_sf[$sf_id] = 0;
			if(isset($stratplan_records[$sg_object_type][$sf_id])) {
				foreach($stratplan_records[$sg_object_type][$sf_id] as $sg_id => $sg) {
					$count["SO".$so_id]["SF".$sf_id]["SG".$sg_id] = isset($stratplan_records[$stratresultObject->getMyObjectType()][$sg_id]) ? count($stratplan_records[$stratresultObject->getMyObjectType()][$sg_id]) : 0;
					$c = isset($count["SO".$so_id]["SF".$sf_id]["SG".$sg_id]) && $count["SO".$so_id]["SF".$sf_id]["SG".$sg_id]>0 ? $count["SO".$so_id]["SF".$sf_id]["SG".$sg_id] : 1;
					$count_sg[$sg_id] = $count["SO".$so_id]["SF".$sf_id]["SG".$sg_id];
					$count_sf[$sf_id]+= $c;
					$count_so[$so_id]+= $c;
				}
			} else {
				$stratplan_records[$sg_object_type][$sf_id] = array();
				//$count_sf[$sf_id]+= 1;
				$count_so[$so_id]+= 1;
			}
		}
	} else {
		$stratplan_records[$sf_object_type][$so_id] = array();
	}
}
echo "
<table id=tbl_stratplan>
	<thead>
		<tr>
			<th rowspan=2>".$stratobjObject->getObjectName($stratobjObject->getMyObjectName())."</th>
			<th rowspan=2>".$stratfocusObject->getObjectName($stratfocusObject->getMyObjectName())."</th>
			<th rowspan=2>".$stratgoalObject->getObjectName($stratgoalObject->getMyObjectName())."</th>
			<th colspan=".count($headings).">".$stratresultObject->getObjectName($stratresultObject->getMyObjectName())."</th>
		</tr><tr>";
			foreach($headings as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
echo "
		</tr>
	</thead>
	<tbody>
	";
	foreach($stratplan_records[$so_object_type] as $so_id => $so) {
		echo "
		<tr>
			<td rowspan=".($count_so[$so_id]>0?$count_so[$so_id]:1).">".$so['name']."</td>
		";
		if($count_so[$so_id]>0) {
			foreach($stratplan_records[$sf_object_type][$so_id] as $sf_id => $sf) {
				echo "
				<td rowspan=".($count_sf[$sf_id]>0?$count_sf[$sf_id]:1).">".$sf['name']."</td>";
				if($count_sf[$sf_id]>0) {
					foreach($stratplan_records[$sg_object_type][$sf_id] as $sg_id => $sg) {
						echo "
						<td rowspan=".($count_sg[$sg_id]>0?$count_sg[$sg_id]:1).">".$sg['name']."</td>";
						if($count_sg[$sg_id]>0){
							foreach($stratplan_records[$sr_object_type][$sg_id] as $sr_id => $sr) {
								$sr_ref = strlen($sr[$stratresultObject->getTableField()."_ref"])>0 ? $sr[$stratresultObject->getTableField()."_ref"] : $stratresultObject->getRefTag().$sr_id;
								$lists['IDP3_STRATRESULT'][$sr_id] = $sr[$sr_name_field]." (".$sr_ref.")";
								foreach($headings as $fld => $head) {
									$v = $sr[$fld];
									$cell_class = "";
									switch($head['type']) {
										case "REF":
											$v = $stratresultObject->getRefTag().$v;
											break;
										case "LIST":
											$table = $head['list_table'];
											$v = isset($lists[$table][$v]) ? $lists[$table][$v] : $helperObject->getUnspecified();
											break;
										case "NUM":
											if($stratresultObject->isTargetNumberField($fld)) {
												$format_key = $sr[$sr_targettype_field];
												$format = isset($lists['TARGET_TYPE_CODES'][$format_key]) ? $lists['TARGET_TYPE_CODES'][$format_key] :"";
												$v = $displayObject->formatTarget($v, $format, $format_key);
											} else {
												$v = $displayObject->getNumberForDisplay($v,2);
											}
											$cell_class = "right";
											break;
										default:
											$e = $displayObject->getDataField($head['type'], $v);
											$v = $e['display'];
											break;
									}
									echo "<td class=".$cell_class.">".$v."</td>";
								}
								echo "</tr><tr>";
							}
						} else {
							echo "<td colspan=".(count($headings))." class=idelete>Error.  Incomplete ".$helperObject->getObjectName($sg_object_name)."</td>";
								echo "</tr><tr>";
							$can_confirm = false;
							$fatal[$sg_object_type] = "Incomplete ".$helperObject->getObjectName($sg_object_name);
						}
					}
				} else {
					echo "<td colspan=".(1+count($headings))." class=idelete>Error.  Incomplete ".$helperObject->getObjectName($sf_object_name)."</td>";
					$can_confirm = false;
					$fatal[$sf_object_type] = "Incomplete ".$helperObject->getObjectName($sf_object_name);
				}
			}
		} else {
			echo "<td colspan=".(1+1+count($headings))." class=idelete>Error.  Incomplete ".$helperObject->getObjectName($so_object_name)."</td>";
			$can_confirm = false;
			$fatal[$so_object_type] = "Incomplete ".$helperObject->getObjectName($so_object_name);
		}
		echo "
		</tr>";
	}
echo "
	</tbody>
</table>";











/************************************
 * PROJECTS
 */

echo linebreak()."<h2>".$helperObject->getObjectName("STRATPROJECTS")."</h2>";



$stratobjObject = new IDP3_PROJECT();
$so_object_type = $stratobjObject->getMyObjectType();
$so_object_name = $stratobjObject->getMyObjectName();

$stratplan_records = $stratobjObject->getOrderedObjectsWithChildren($idp_object_id);


$headings = $headingObject->getMainObjectHeadings($so_object_type,"FORM","NEW");
$headings = $stratobjObject->replaceAllNames($headings['rows']);
$project_headings = $headings;
$projectObject = $stratobjObject;
$project_list = array();



//TIER2 = INCOME
$t2Object = new IDP3_PROJECTINCOME();
$t2_object_type = $t2Object->getMyObjectType();
$t2_object_name = $t2Object->getMyObjectName();
$t2_objects = $stratplan_records[$t2_object_type];

//TIER3 = COST
$t3Object = new IDP3_PROJECTCOST();
$t3_object_type = $t3Object->getMyObjectType();
$t3_object_name = $t3Object->getMyObjectName();
$t3_objects = $stratplan_records[$t3_object_type];



$total_object_count+=count($stratplan_records[$so_object_type]);
$total_object_count+=count($stratplan_records[$t2_object_type]);
$total_object_count+=count($stratplan_records[$t3_object_type]);



					$t2_objects_full = array();
					$t3_objects_full = array();

echo "
<table id=tbl_projects3>
		<tr>";
			foreach($headings as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
echo "
		</tr>
				";
	foreach($stratplan_records[$so_object_type] as $sr_id => $sr) {
		$project_list[$sr_id] = $sr[$stratobjObject->getNameFieldName()];
		echo "<tr>";
		foreach($headings as $fld => $head) {
			$v = $sr[$fld];
			switch($head['type']) {
				case "REF":
					$v = $stratobjObject->getRefTag().$v;
					break;
				case "LIST":
				case "OBJECT":
				case "SEGMENT":
					$table = $head['list_table'];
					$v = isset($lists[$table][$v]) ? $lists[$table][$v] : $helperObject->getUnspecified();
					break;
				case "MULTILIST":
					$table = $head['list_table'];
					$v2 = explode(";",$v);
					$x = array();
					foreach($v2 as $y) {
						if(isset($lists[$table][$y])) {
							$x[] = $lists[$table][$y];
						}
					}
					if(count($x)>0) {
						$v= implode(";<br />",$x);
					} else {
						$v= $helperObject->getUnspecified();
					}
					break;
				case "NUM":
					$v = $displayObject->getNumberForDisplay($v,2);
					break;
				default:
					if($fld=="proj_actions" && strpos($v,chr(10))!==false) {
						$x = explode(chr(10),$v);
						$v = "<ul><li>".implode("</li><li>",$x)."</li></ul>";
					}
					$e = $displayObject->getDataField($head['type'], $v);
					$v = $e['display'];
					break;
			}
			echo "<td>".$v."</td>";
		}
		echo "</tr>";
					if(isset($t2_objects[$sr_id]) && count($t2_objects[$sr_id])>0) {
						foreach($t2_objects[$sr_id] as $tid => $tobj) {
							$tobj[$t2Object->getParentFieldName()] = $sr['proj_name'];
							$t2_objects_full[$tid] = $tobj;
						}
					}
					if(isset($t3_objects[$sr_id]) && count($t3_objects[$sr_id])>0) {
						foreach($t3_objects[$sr_id] as $tid => $tobj) {
							$tobj[$t3Object->getParentFieldName()] = $sr['proj_name'];
							$t3_objects_full[$tid] = $tobj;
						}
					}

	}
echo "
</table>";

					echo "<h3>".$helper->getObjectName($t2_object_name)."</h3>";
					$tier = "t2";
					$js.=$displayObject->drawProjectDetailTable($tier, $t2_object_type, null,		false,			  false,		true,			$t2_objects_full,$idp_object_id, array(),		  true);

					echo "<h3>".$helper->getObjectName($t3_object_name)."</h3>";
					$tier = "t3";
					$js.=$displayObject->drawProjectDetailTable($tier, $t3_object_type, null,		false,			  false,		true,			$t3_objects_full,$idp_object_id, array(),		  true);


























/************************************
 * PERFORMANCE MEASURES
 */

echo linebreak()."<h2>".$helperObject->getObjectName("STRATPERFMEASURES")."</h2>";

markTime("perfmeasures start");


$stratobjObject = new IDP3_PMKPA();
$so_object_type = $stratobjObject->getMyObjectType();
$so_object_name = $stratobjObject->getMyObjectName();

markTime("pmkpa end pmprog start");

$stratgoalObject = new IDP3_PMPROG();
$sg_object_type = $stratgoalObject->getMyObjectType();
$sg_object_name = $stratgoalObject->getMyObjectName();

markTime("end pmprog start pmkpi");

$stratresultObject = new IDP3_PMKPI();
$sr_object_type = $stratresultObject->getMyObjectType();
$sr_object_name = $stratresultObject->getMyObjectName();
$sr_targettype_field = $stratresultObject->getTargetTypeField();

markTime("pmkpi end");

$stratplan_records = $stratobjObject->getOrderedObjectsWithChildren($idp_object_id);

markTime("got records");

//ASSIST_HELPER::arrPrint($stratplan_records);

$headings = $headingObject->getMainObjectHeadings($sr_object_type,"FORM","NEW");
$headings = $stratobjObject->replaceAllNames($headings['rows']);
//$headings = $headings['rows'];
//ASSIST_HELPER::arrPrint($headings);

$target_heading = $headings['pky_kpi_id'];
unset($headings['pky_kpi_id']);
	//'kpi_id'=>$headings['kpi_id'],
$indicator_headings = array(
	'kpi_name'=>$headings['kpi_name'],
	'kp_indicator'=>$headings['kp_indicator_code'],
	'kp_proj_id'=>$headings['kp_proj_id'],
);
unset($headings['kp_proj_id']);
unset($headings['kp_indicator_code']);

$total_object_count+=count($stratplan_records[$so_object_type]);
$total_object_count+=count($stratplan_records[$sg_object_type]);
$total_object_count+=count($stratplan_records[$sr_object_type]);

$count = array();
$count_sg = array();
//$count_sf = array();
$count_so = array();
foreach($stratplan_records[$so_object_type] as $so_id => $so) {
	$count[$so_id] = array();
	$count_so[$so_id] = 0;
			if(isset($stratplan_records[$sg_object_type][$so_id])) {
				foreach($stratplan_records[$sg_object_type][$so_id] as $sg_id => $sg) {
					$count[$so_id][$sg_id] = isset($stratplan_records[$stratresultObject->getMyObjectType()][$sg_id]) ? count($stratplan_records[$stratresultObject->getMyObjectType()][$sg_id]) : 0;
					$count_sg[$sg_id] = $count[$so_id][$sg_id];
					$count_so[$so_id]+= $count[$so_id][$sg_id];
				}
			} else {
				$stratplan_records[$sg_object_type][$so_id] = array();
			}
}

$kpi_ids = array();
$kpi_target_types = array();

echo "
<table id=tbl_perfmeasures>
	<thead>
		<tr>
			<th rowspan=2>".$stratobjObject->getObjectName($stratobjObject->getMyObjectName())."</th>
			<th rowspan=2>".$stratgoalObject->getObjectName($stratgoalObject->getMyObjectName())."</th>
			<th colspan=".count($headings).">".$stratresultObject->getObjectName($stratresultObject->getMyObjectName())."</th>
		</tr><tr>";
			foreach($headings as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
echo "
		</tr>
	</thead>
	<tbody>
	";

	foreach($stratplan_records[$so_object_type] as $so_id => $so) {
		echo "
		<tr>
			<td rowspan=".($count_so[$so_id]>0?$count_so[$so_id]:1).">".$so['name']."</td>
		";
		if($count_so[$so_id]>0) {
					foreach($stratplan_records[$sg_object_type][$so_id] as $sg_id => $sg) {
						echo "
						<td rowspan=".($count_sg[$sg_id]>0?$count_sg[$sg_id]:1).">".$sg['name']."</td>";
						if($count_sg[$sg_id]>0){
							foreach($stratplan_records[$sr_object_type][$sg_id] as $sr_id => $sr) {
								$format_key = $sr[$sr_targettype_field];
								$format = $lists['TARGET_TYPE_CODES'][$format_key];
								$kpi_target_types[$sr_id] = $format_key;
								$kpi_ids[$sr_id] = array('id'=>$sr_id);
								foreach($headings as $fld => $head) {
									$v = $sr[$fld];
									$cell_class = "";
									switch($head['type']) {
										case "REF":
											$v = $stratresultObject->getRefTag().$v;
											$kpi_ids[$sr_id][$fld] = $v;
											break;
										case "OBJECT":
											$table = $head['list_table'];
											$v = isset($lists[$table][$v]) ? $lists[$table][$v] : $helperObject->getUnspecified();
											break;
										case "LIST":
										case "SEGMENT":
											$table = $head['list_table'];
											$v = isset($lists[$table][$v]) ? $lists[$table][$v] : $helperObject->getUnspecified();
											break;
										case "MULTISEGMENT":
											$table = $head['list_table'];
											$x = explode(";",$v);
											$y = array();
											if(in_array("X",$x) || in_array("0",$x)) {
												$v = $helperObject->getUnspecified();
											} else {
												foreach($x as $z) {
													if(isset($lists[$table][$z])) {
														$y[] = $lists[$table][$z];
													}
												}
												if(count($y)>0) {
													$v = implode("; ",$y);
												} else {
													$v = $helperObject->getUnspecified();
												}
											}
											break;
										case "NUM":
											if($stratresultObject->isTargetNumberField($fld)) {
												$v = $displayObject->formatTarget($v, $format, $format_key);
											} else {
												$v = $displayObject->getNumberForDisplay($v,2);
											}
											$cell_class = "right";
											break;
										case "PROJECT":
											if(count($v)>1) {
												$v = "<ul><li>".implode(";</li><li>",$v)."</li><ul>";
											} else {
												$v = implode("",$v);
											}
											break;
										default:
											$e = $displayObject->getDataField($head['type'], $v);
											$v = $e['display'];
											break;
									}
									echo "<td class='".$cell_class."'>".$v."</td>";
									//if(in_array($fld,array($stratresultObject->getNameFieldName(),$stratresultObject->getTableField()."_ref",$stratresultObject->getTableField()."_unit_id"))) {
										$kpi_ids[$sr_id][$fld] = $v;
									//}
								}
								echo "</tr><tr>";
							}
						} else {
							echo "<td colspan=".(count($headings))." class=idelete>Error.  Incomplete ".$helperObject->getObjectName($sg_object_name)."</td>";
							$can_confirm = false;
							$fatal[$sg_object_type] = "Incomplete ".$helperObject->getObjectName($sg_object_name);
						}
						//echo "</tr><tr>";
					}
		} else {
			echo "<td colspan=".(1+count($headings))." class=idelete>Error.  Incomplete ".$helperObject->getObjectName($so_object_name)."</td>";
			$can_confirm = false;
			$fatal[$so_object_type] = "Incomplete ".$helperObject->getObjectName($so_object_name);
		}
		echo "
		</tr>";
	}
echo "
	</tbody>
</table>";


//GET INDICATOR INFORMATION
$kpiprojObject = new IDP3_PMKPI_PROJECT();
$indicators = $kpiprojObject->getRecordsForListDisplay(array_keys($kpi_ids),array());//$kpi_ids,array(),false,true);
//ASSIST_HELPER::arrPrint($indicators);
//ASSIST_HELPER::arrPrint($project_list);
echo "
<h3>".$helper->getObjectName("STRATPERFMEASURES")." - ".$helper->getObjectName("INDICATORCODE")."</h3>

<table>
	<tr>";
foreach($indicator_headings as $fld => $h) {
	echo "<th>".$h['name']."</th>";
}
	echo "
	</tr>";
				//<td>".$k[$stratresultObject->getIDFieldName()]."</td>
foreach($kpi_ids as $ki => $k) {
	if(isset($indicators[$ki]) && count($indicators[$ki])>0) {
		foreach($indicators[$ki] as $pi => $i) {
			echo "
			<tr>
				<td>".$k[$stratresultObject->getNameFieldName()]."</td>
				<td>".$i['indicator']."</td>
				<td>".$project_list[$i['project']]."</td>
			</tr>";
		}
	}
}
	echo "
</table>
";


echo "<h3>".$helper->getObjectName("STRATPERFMEASURES")." - ".$target_heading['name']."</h3>";
$tier = "t3";
//$js.=$displayObject->drawProjectDetailTable($tier, $t3_object_type, null,		false,			  false,		true,			$t3_objects_full,$idp_object_id, array(),		  true);

$d = $displayObject->getKPITargetTable($idp_object_id,$kpi_ids,false,array(),$lists['TARGET_TYPE_CODES'],$kpi_target_types);

$js.=$d['js'];
echo $d['display'];

//ASSIST_HELPER::displayResult(array("info","Under development"));




$output = ob_get_contents();

ob_end_clean();

if($echo_detail) {
	echo $output;
	echo linebreak();
}















if(in_array($page_activity,$view_final_section)) {




	switch($page_activity) {
		case "CONFIRM":
			if($total_object_count==0) {
				$fatal[] = "Blank ".$idpObject->getObjectName($idpObject->getMyObjectName());
			}
			echo "
			<div style='width:500px;margin:0 auto; padding:10px; border-radius: 8px;border-style:solid; border-width:1px' class='blue-border'>
				";
				if($can_confirm && $total_object_count>0) {
					if($echo_detail) {
						echo "
						<h3>Confirmation</h3>
						<p>I, ".$idpObject->getUserName().", hereby confirm that this ".$idpObject->getObjectName($idpObject->getMyObjectName())." is complete and correct and that I have authority to confirm this ".$idpObject->getObjectName($idpObject->getMyObjectName()).".</p>
						<p class=center>
							<button id=btn_confirm class='green-button abutton' >Confirm ".$idpObject->getObjectName($idpObject->getMyObjectName())."</button>
							&nbsp;<button id=btn_delete class='red-button abutton' >Delete ".$idpObject->getObjectName($idpObject->getMyObjectName())."</button>
						</p>";
					} else {
						echo "
						<h3>Confirmation Status</h3>
						<p class='green b'>Success</p>
						<p>This ".$idpObject->getObjectName($idpObject->getMyObjectName())." can be confirmed. Please use the 'Request Confirmation' button on the left to start the Confirmation process.";
					}
				} else {
					echo "
					<p class='red b'>Error</p>
					<p>This ".$idpObject->getObjectName($idpObject->getMyObjectName())." can not be confirmed due to the following fatal errors:<ul><li>"
					.implode("</li><li>",$fatal)
					."</li></ul></p>";
				}
				if(count($warning)>0) {
					echo "
					<p class='orange b'>Warning</p>
					<p>Please note the following non-fatal errors:<ul><li>"
					.implode("</li><li>",$warning)
					."</li></ul></p>
					<p>These non-fatal errors do not prevent you from confirming this ".$idpObject->getObjectName($idpObject->getMyObjectName()).".</p>";
				}
			echo "
			</div>";
		break;
		case "ACTIVATE_WAITING":
			echo "
			<div style='width:500px;margin:0 auto; padding:10px; border-radius: 8px;border-style:solid; border-width:1px' class='blue-border'>
				<h3>Activate</h3>";
					echo "
					<p>I, ".$idpObject->getUserName().", hereby confirm that this ".$idpObject->getObjectName($idpObject->getMyObjectName())." is complete and correct and that I have authority to activate this ".$idpObject->getObjectName($idpObject->getMyObjectName()).".</p>
					<p class=center>
						<button id=btn_activate class='green-button abutton' >Activate ".$idpObject->getObjectName($idpObject->getMyObjectName())."</button>
						&nbsp;<button id=btn_cancel_confirmation class='red-button abutton' >Cancel Prior Confirmation</button>
					</p>";
				if(count($warning)>0) {
					echo "
					<p class='orange b'>Warning</p>
					<p>Please note the following non-fatal errors:<ul><li>"
					.implode("</li><li>",$warning)
					."</li></ul></p>
					<p>These non-fatal errors do not prevent you from activating this ".$idpObject->getObjectName($idpObject->getMyObjectName()).".</p>";
				}
					echo "
			</div>";
		break;
		case"ACTIVATE_DONE":
			echo "
			<div style='width:500px;margin:0 auto; padding:10px; border-radius: 8px;border-style:solid; border-width:1px' class='blue-border'>
				<h3>Undo Activation</h3>";
					echo "
					<p>I, ".$idpObject->getUserName().", hereby confirm that I have authority to reverse the prior activation of this ".$idpObject->getObjectName($idpObject->getMyObjectName()).".</p>
					<p class=center>
						<button id=btn_activate_undo class='green-button abutton' >Reverse Activation ".$idpObject->getObjectName($idpObject->getMyObjectName())."</button>
					</p>";
						//&nbsp;<button id=btn_cancel_confirmation class='red-button abutton' >Cancel Prior Confirmation</button>
				if(count($warning)>0) {
					echo "
					<p class='orange b'>Warning</p>
					<p>Please note the following non-fatal errors:<ul><li>"
					.implode("</li><li>",$warning)
					."</li></ul></p>";
				}
					echo "
			</div>";
		break;
	}
}
?>
<p>&nbsp;</p>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>

	<?php if($page_activity=="CONFIRM") { ?>

		$("#btn_confirm").button({
			icons:{primary:"ui-icon-check"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=IDP.CONFIRM","object_id=<?php echo $idp_object_id; ?>");
			if(r[0]=="ok") {
				document.location.href = "new_confirm.php";
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-green");


		$("#btn_delete").button({
			icons:{primary:"ui-icon-trash"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=IDP.DELETE","object_id=<?php echo $idp_object_id; ?>");
			if(r[0]=="ok") {
				document.location.href = "new_confirm.php";
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-red");

	<?php } elseif($page_activity=="ACTIVATE_WAITING") { ?>

		$("#btn_activate").button({
			icons:{primary:"ui-icon-check"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=IDP.ACTIVATE","object_id=<?php echo $idp_object_id; ?>");
			if(r[0]=="ok") {
				document.location.href = "new_activate.php";
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-green");


		$("#btn_cancel_confirmation").button({
			icons:{primary:"ui-icon-closethick"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=IDP.UNDOCONFIRM","object_id=<?php echo $idp_object_id; ?>");
			if(r[0]=="ok") {
				document.location.href = "new_activate.php";
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-red");

	<?php } elseif($page_activity=="ACTIVATE_DONE") { ?>

		$("#btn_activate_undo").button({
			icons:{primary:"ui-icon-closethick"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var r = AssistHelper.doAjax("inc_controller.php?action=IDP.UNDOACTIVATE","object_id=<?php echo $idp_object_id; ?>");
			if(r[0]=="ok") {
				document.location.href = "new_activate_done.php";
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}).removeClass("ui-state-default").addClass("ui-button-state-red");


	<?php } ?>
});

</script>