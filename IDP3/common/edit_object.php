<?php
include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type; 

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$page_action = (isset($page_section) && strlen($page_section)>0 ? $page_section."." : "")."Edit";


switch($object_type) {
	case "CONTRACT":
		$myObject = new IDP3_CONTRACT($object_id);
		
		break;
	case "DELIVERABLE":
		$childObject = new IDP3_DELIVERABLE($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new IDP3_CONTRACT($parent_id);
		$parent_object_type = "CONTRACT";
		break;
	case "ACTION":
		$childObject = new IDP3_ACTION($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new IDP3_DELIVERABLE($parent_id);
		$parent_object_type = "DELIVERABLE";
		break;
}
?>
<table class=tbl-container>
	<tr>
	<?php if($display_type!= "dialog" && $object_type!="CONTRACT") { ?>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($parent_object_type, $parent_id, true); ?></td>
		<td width=5%>&nbsp;</td>
	<?php } ?>
		<td width=48%>
		<?php if($display_type!="dialog") { ?>
			<h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php
		} 
			//include("common/generic_object_form.php");
			$data = $displayObject->getObjectForm($object_type, $childObject, $object_id, $parent_object_type, $myObject, $parent_id, $page_action, $page_redirect_path);
echo $data['display'];
$js.=$data['js'];
if(!isset($page_section) || $page_section!="NEW") {
			$js.= $displayObject->drawPageFooter($helper->getGoBack($page_redirect_path),strtolower($object_type),"object_id=".$object_id."&log_type=".IDP3_LOG::EDIT);
} elseif($page_redirect_path!="dialog") {
			$js.= $displayObject->drawPageFooter($helper->getGoBack($page_redirect_path));
}
		?></td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	if($object_type=="DELIVERABLE") {
		echo "$(\"#del_legislation_section_id\").val(del_legislation_section_value).trigger('change');";
	}
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($object_type,"dlg_child",$_REQUEST);
	}
	?>
});
</script>