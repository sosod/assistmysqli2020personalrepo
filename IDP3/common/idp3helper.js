var IDP3Helper = {

	processObjectForm : function($form,page_action,page_direct) {
//console.log("pOF"+AssistForm.serialize($form));
		//alert(page_action);
		if(page_action!="DELETE") {
			var valid = IDP3Helper.validateForm($form);
		} else {
			var valid = true;
		}
		//var valid=false;
		if(valid) {
			AssistHelper.processing();
			var dta = AssistForm.serialize($form); //alert(dta);
			if(page_action == "serialize") {
				return serial;
			} else {
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished, and uncomment redirects!
//console.log(result);
	//console.log(result.responseText);
				if(result[0]=="ok") {
					if(page_direct=="home") {
//console.log("home :: "+result[1]);
						IDP3Helper.routeHome();
					} else if(page_direct=="dialog") {
//console.log("dialog :: "+result[1]);
						//alert(result[1]);
						//console.log(window.parent.location.href);
						window.parent.dialogFinished(result[0],result[1]);
					} else {
//console.log("document redirect :: "+page_direct[1]);
						document.location.href = page_direct+'r[]='+result[0]+'&r[]='+result[1];
					}
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		}
	},
	processObjectFormWithAttachment : function($form,page_action,page_direct) {
		//console.log("pOFwA"+AssistForm.serialize($form));
		var valid = IDP3Helper.validateForm($form);
		//var valid=false;
		if(valid) {
			AssistHelper.processing();
			$form.prop("target","file_upload_target");
			$form.prop("action","inc_controller.php");
			//$form.append("<input type=hidden name=page_direct value='"+page_direct+"' />");
			//$form.append("<input type=hidden name=action value='"+page_action+"' />");
			//alert(AssistForm.serialize($form));
			$form.submit();
			/*var dta = AssistForm.serialize($form); //alert(page_action+"\n\n"+dta);
			if(page_action == "serialize") {
				return serial;
			} else {
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished!
				console.log(result);
				if(result[0]=="ok") {
					if(page_direct=="home") {
						IDP3Helper.routeHome();
					} else {
						document.location.href = page_direct+'r[]='+result[0]+'&r[]='+result[1];
					}
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}*/
			AssistHelper.closeProcessing();
		}
	},
	routeHome : function() {
		parent.header.$("#backHome").trigger("click");
	},
	validateForm : function($form) {
	/**
	 * 1. loop through $form input fields and check for any with attr("req")=="required" where no value has been input
	 * 2. if previous loop found missing required field(s)
	 * 		2.a display error message listing missing fields
	 * 		2.b reject form
	 * 	 else
	 *		serialize $form
	 * 		send to inc_controller with action=page_action
	 *  	if ajax response[0] = "ok"
	 * 			redirect to page_direct+r[]=result[0]&r[]=result[1]
	 * 		else
	 * 			display error message
	 */
		$formchild = $form.find('button, input:visible, select, textarea');
		var fields = [];
		var unique_fields = [];
		var result = [];
		var serial = [];
		var err_list = "<h2 class=idelete>Error</h2><p>The form you've filled in is incomplete.</p><p>The following fields are required but have not been captured/selected:</p><ul>";
		var num_arr = ['0','1','2','3','4','5','6','7','8','9','.','-'];
		$formchild.each(function(){
			if(!$(this).hasClass('i-am-the-submit-button') && $(this).prop("id")!="btn_save" && !$(this).is(":checkbox") && $(this).prop("name").length>0) {
				$(this).removeClass("required");
				var val = $(this).val(); //alert($(this).prop("id"));
				var type = $(this).prop("type");
				var req = $(this).attr("req");
				var req_error = false;	//needed to verify if unique test must be done
				var role = $(this).prop("role");
				if($(this).parents("tr").children("th").length > 0) {
					var id_raw = $(this).parents("tr").children("th")[0].innerHTML;
					var id_arr = id_raw.split(":");
					var id = id_arr[0];
					var my_id = $(this).prop("id");
				} else {
					var id = "Unknown Field ("+$(this).prop("id")+")";
				}
				if(req == 1){
					if(type != "submit" && type != "button"){
						if(type == "select-one"){
							if(val == "X" || val=="0" || val==0 || val==undefined || val==null){
								fields.push(id);
								$(this).addClass("required");
							}
						} else if(type == "select-multiple") {
							if($(this).find("option:selected").length==0) {
								fields.push(id);
								$(this).addClass("required");
								val = $(this).find("option:selected:first").val();
							}
						} else {
							if(val.length == 0 || val == undefined || val == null){
								fields.push(id);
								$(this).addClass("required");
							}else if($(this).hasClass('number-only')) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
										req_error = true;
									}
								}
							}
						}
					}
				} else if($(this).hasClass("number-only")) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
									}
								}
				}
				if(req_error==false && $(this)[0].hasAttribute('is_unique') && $(this).attr('is_unique')*1==1) {
					//check for fields which must be unique - only kick in if required field has been completed

					var ot = $(this).attr("object_type");
					var pi = $(this).attr("parent_id");
					var tv = $(this).val();

					var dta = "field="+my_id+"&parent_id="+pi+"&test_value="+tv;

					var unique_test = AssistHelper.doAjax("inc_controller.php?action="+ot+".UniqueTest",dta);
					//console.log(id+" = "+unique_test);
					if(unique_test=="error") {
						unique_fields.push(id);
						$(this).addClass("required");
						//alert(id+" must be unique");
					}

				}
				//console.log($(this).prop("id")+" = "+val+" # "+type);
				//val = AssistString.decode(val);
				//serial.push(val);
			}
		});
			//console.log(unique_fields);
		if(fields.length != 0){
			for(var i in fields){
				err_list += "<li>" + fields[i] + "</li>";
			}
			err_list += "</ul><p>Please complete the required fields and try again.</p><br>";
			$("#div_error").html(err_list);
			$("#div_error").dialog({
				modal: true,
				resizable: false,
				buttons: {
					"Okay":function(){
						$(this).dialog("close");
					}
				}
			});
			AssistHelper.hideDialogTitlebar("id","div_error");
			return false;
		} else if(unique_fields.length!=0) {
			err_list = "<h2 class=idelete>Error</h2><p>The form you've filled in is incomplete.</p><p>The following fields must be <span class=b>unique</span> and duplicate values have been detected:</p><ul>";
			for(var i in unique_fields){
				err_list += "<li>" + unique_fields[i] + "</li>";
			}
			err_list += "</ul><p>Please correct the duplicate values and try again.</p><br>";
			$("#div_error").html(err_list);
			$("#div_error").dialog({
				modal: true,
				resizable: false,
				buttons: {
					"Okay":function(){
						$(this).dialog("close");
					}
				}
			});
			AssistHelper.hideDialogTitlebar("id","div_error");
			return false;
		} else {
			return true;
		}
	}

};