<?php
include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$page_action = "UPDATE";

switch($object_type) {
	case "CONTRACT":
		$myObject = new IDP3_CONTRACT();
		$page_redirect = $page_section."_update_contract.php?";
		$parent_object_type = null;
		break;
	case "DELIVERABLE":
		$myObject = new IDP3_DELIVERABLE();
		$page_redirect = $page_section."_update_deliverable.php?";
		$parent_object_type = "CONTRACT";
		break;
	case "ACTION":
		$myObject = new IDP3_ACTION();
		$parent_object_type = "DELIVERABLE";
		if(isset($_REQUEST['page_redirect'])) {
			$page_redirect = $_REQUEST['page_redirect'];
		} else {
			$page_redirect = $page_section."_update_action.php?";
		}
		break;
}

$object = $myObject->getObjectForUpdate($object_id);
//ASSIST_HELPER::arrPrint($myObject->getRecipients($object_id));
?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Update</h2><?php 
			//include("common/generic_object_update_form.php");
			$data = $displayObject->getObjectForm($object_type, $myObject, $object_id, $parent_object_type, null, 0, $page_action, $page_redirect);
echo $data['display'];
$js.=$data['js'];
			$js.= $displayObject->drawPageFooter("",strtolower($object_type),"object_id=".$object_id."&log_type=".IDP3_LOG::UPDATE);
		?></td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td colspan=1>
			<?php  ?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>

});
</script>