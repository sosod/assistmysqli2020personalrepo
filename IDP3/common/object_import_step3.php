<?php
//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($_FILES);

//error_reporting(-1);


//record output of import page
/**
 * NOT WORKING - JC 2018-07-15
 *
function recordOutput($buffer) {
	global $object_type;
	$helper = new ASSIST_MODULE_HELPER();
	$output = $helper->getPageHeader("1.10.0",$scripts,array("/assist_jquery.css","/assist3.css"));
	$output.=$buffer;

	$cmpcode = $helper->getCmpCode();
	$modref = $helper->getModRef();

	//WRITE DATA TO FILE
	$filename = "../files/".$cmpcode."/".$modref."/import/".strtolower($object_type)."_step3_".date("Ymd_His").".html";
	$file = fopen($filename,"w");
	fwrite($file,$output."\n");
	fclose($file);


	return $buffer;
}

/* Verify IDP selected */
/* ----------------------------------------------------------------------------------------------------------------------- */
$valid_file = true;
if(!isset($_REQUEST['idp_id']) || ASSIST_HELPER::checkIntRef($_REQUEST['idp_id'])===false) {
	$valid_file = false;
	$error = "No IDP selected.  Please go back and try again.";
} else {
	$idp_object_id = $_REQUEST['idp_id'];
}


//if error then display message and die
if($valid_file!==true) {
	ASSIST_HELPER::displayResult(array("error",$error));
	die();
} else {
	//$idpObject = new IDP3_IDP();
	//$second_heading = " ".IDP3_HELPER::BREADCRUMB_DIVIDER." ".$idpObject->getHeading($idp_object_id);
}

/* Handle the uploading of the file */
/* ----------------------------------------------------------------------------------------------------------------------- */

    //NEED TO GET mSCOA VERSION ID FROM parent IDP - JC AA-407 1 June 2020
    $mscoa_version_id = 1;	//default to 1 = Non-compliant
    if($object_type!="IDP") {
        $idpObject = new IDP3_IDP();
        $raw_idp_object = $idpObject->getRawObject($idp_object_id);
        $mscoa_version_id = isset($raw_idp_object['idp_mscoa_version_id']) ? $raw_idp_object['idp_mscoa_version_id'] : 1;
    }



//Test that a valid file has been imported
$valid_file = true;
if(!isset($_FILES['import_file']['name']) || strlen($_FILES['import_file']['name'])==0) {
	$valid_file = false;
	$error = "No import file found.  Please go back and try again.";
} else {
	$name = $_FILES['import_file']['name'];
	$test = explode(".",$name);
	if(count($test)<2) {
		$error = "Invalid file name - Assist could not detect a valid file extension.  Please go back and try again.";
		$valid_file = false;
	} else {
		$ext = array_pop($test);
		if(strtoupper($ext)!="CSV") {
			$error = "Invalid file type - only Comma Separated/Delimited Values (CSV) files permitted.  Please go back and try again.";
			$valid_file = false;
		}
	}
}
//test file size before trying to upload
if($valid_file===true) {
	$max_file_size_allowed = $myObject->getMaxUploadFileSize();
	if($_FILES['import_file']['size']*1>$max_file_size_allowed*1) {
			$error = "Invalid file size - the file you have tried to upload is too big.  Please try again with a smaller file - only files up to ".$myObject->formatBytes($max_file_size_allowed,null,0)." are permitted.";
			$valid_file = false;
	}
}









//if error then display message and die
if($valid_file!==true) {
	ASSIST_HELPER::displayResult(array("error",$error));
	die();
}



//Upload the import file
$folder = $myObject->getAttachmentImportFolder();
$save_folder = $myObject->getSaveFolder();
$full_folder = $myObject->checkFolder($folder,1,true);
$file_name = $object_type."_".date("YmdHis");
$ext = ".".strtolower($ext);

//test file name is unique
$c = 1;
$test_name = $file_name.$ext;
while(file_exists($full_folder."/".$test_name)) {
	$file_name.="_".$c;
	$c++;
	$test_name = $file_name.$ext;
}
$file_name.=$ext;


//if you've gotten this far, then upload the file
if (move_uploaded_file($_FILES['import_file']['tmp_name'], $full_folder."/".$file_name)) {
	/*echo "
	<p><span class='b green'>Success!</span>  The file ". basename( $_FILES['mscoa_import_file']['name']). " has been uploaded.</p>
	<p>Assist is now processing the import file. Please do not close the window while this is happening...</p>";*/
} else {
	ASSIST_HELPER::displayResult(array("error","There was an unexpected error while trying to upload your file.  Please try again."));
	die();
}



/* ----------------------------------------------------------------------------------------------------------------------- */
/* now to process the import file into the list */
/* ----------------------------------------------------------------------------------------------------------------------- */

//read the uploaded file
$import = fopen($full_folder."/".$file_name,"r");
$data = array();
while(!feof($import)) {
	$tmpdata = fgetcsv($import);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($import);

//test that data exists in the file - check for more than 2 lines (2 lines = heading and sub-heading)
if(count($data)<3) {
	ASSIST_HELPER::displayResult(array("error","No data was found to be imported.  Please check that your file is not empty."));
	die();
}

//remove heading & definition rows
unset($data[0]);
unset($data[1]);

//check for too many lines (limit set in parent page)
if(count($data)>$max_rows_to_import) {
	ASSIST_HELPER::displayResult(array("error","Your file has too much data in it to be processed in a single upload.  Please break up your file into multiple smaller files of no more than $max_rows_to_import rows and try again."));
	die();
}

//prepare variables before going through document
//$headings = $myObject->getHeadingsForImport(); ---- provided by parent page
$has_data = false;
$processed_data = array();
$errors = array();
$status_count = array(
	'rows'=>array('ok'=>0,'warn'=>0,'error'=>0),
	'fields'=>array('ok'=>0,'warn'=>0,'error'=>0),
);

$import_data = array();
$import_results_data = array();
$import_ref = $file_name.".".$myObject->getUserID();



$all_list_items = array();






//process the import data
foreach($data as $key => $row) {
	$row = $myObject->removeBlanksFromArray($row,false,true);
	if(count($row)>0) {
		$has_data = true;
		$cell = 0;
		$processed_data[$key] = array(
			'row'=>$key+1,
			'import_status' => array(
				'final_icon'=>"ok"
			),
		);
		$has_error = false;
		$row_error = false;
		//process headings for main object import
		foreach($headings['rows'] as $fld => $head) {
			$is_required = $head['required']==true;
			$max_characters = $head['type']=="TEXT"?"65000":$head['max'];
			$fld_type = $head['type'];
			$fld_name = $head['name'];
			$value = isset($row[$cell]) ? $row[$cell] : "";
			$value = ASSIST_HELPER::code($value,"ISO-8859-1");
			$processed_data[$key][$fld] = $value;
			$has_error = false;
			if($is_required && strlen($value)==0) {
				$has_error = true;
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - $fld_name is a required field.";
			} elseif($fld_type=="BOOL" || $fld_type=="BOOL_BUTTON") {
				if(strlen($value)==0) {
					$value="No";
				}
				$value = trim(strtoupper($value));
				if($value!="YES" && $value!="NO") {
					$has_error = true;
					$errors[$key][$fld] = "error";
					$status_count['fields']['error']++;
					$processed_data[$key]['import_status']['final_icon'] = "error";
					$processed_data[$key]['import_status'][$fld] = "Error - Invalid Yes/No answer.";
				} elseif($value=="YES") {
					$value = 1;
				} else {
					$value = 0;
				}
			} elseif($headingObject->isDateField($fld_type)) {
				if(strlen($value)>0) {
					$value = strtotime($value);
					$value = date("Y-m-d",$value);
				}
			} elseif($headingObject->isTextField($fld_type) && strlen($value)>$max_characters) {
				$has_error = true;
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - Text exceeds maximum allowed length.  Text length is ".strlen($value)." while the maximum allowed is ".$max_characters.".";
			} elseif($headingObject->isListField($fld_type) || $fld_type=="OBJECT" || $fld_type=="MULTIOBJECT") {
				if(strlen($value)>0) {
					$is_multi = false;
					$code_me = false;
					$extra_info = array();
					$list_table = $head['list_table'];
if(!isset($all_list_items[$list_table])) {
					switch($fld_type) {
							case "MULTILIST":
								$is_multi = true;
						case "LIST":
							$listObject = new IDP3_LIST($head['list_table']);
							break;
							case "MULTISEGMENT":
								$is_multi = true;
							case "SEGMENT":
								$code_me = true;
							$listObject = new IDP3_SEGMENTS($head['list_table']);
								$extra_info['version'] = $mscoa_version_id;
							break;
							case "MULTIOBJECT":
								$is_multi = true;
						case "OBJECT":
							$list_table = $head['list_table'];
							/*if(strpos($list_table,"|")!==false) {
								$lon = explode("|",$list_table);
								$list_table = $lon[0];
								$extra_info = $lon[1];
							}*/
							$extra_info = $idp_object_id;
							$listObject = new $list_table();
							break;
					}
					$list_items = $listObject->getActiveListItemsFormattedForImportProcessing($extra_info);
$all_list_items[$list_table] = $list_items;
} else {
$list_items = $all_list_items[$list_table];
}
					if($is_multi) {
						$test_values = explode(";",$value);
						$result = array();
						foreach($test_values as $test) {
							if(strlen($test)>0) {
								//$test = trim(strtolower(str_replace(" ","",($code_me?ASSIST_HELPER::code($test):$test))));
$test = $myObject->formatTextForComparison($test);
								if(!isset($list_items[$test])) {
									$has_error = true;
									break;
								} else {
									$result[] = $list_items[$test];
								}
							}
						}
						if(!$has_error) {
							$value = implode(";",$result);
						}
					} else {
						//$test_value = strtolower(str_replace(" ","",(($code_me?ASSIST_HELPER::code($value):$value))));
$test_value = $myObject->formatTextForComparison($value);						
						if(!isset($list_items[$test_value])) {
							$has_error = true;
						} else {
							$value = $list_items[$test_value];
						}
					}
					if($has_error) {
						$errors[$key][$fld] = "error";
						$status_count['fields']['error']++;
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - List item \"".$value."\" not found.";
					}
				}

			} elseif($head['is_unique']*1==1) {
				$var = array(
					'test_value' => $value,
					'parent_id' => $idp_object_id,
					'field' => $fld
				);
				$unique_test = $myObject->uniqueTest($var);
				if($unique_test=="error") {
					$has_error = true;
					$errors[$key][$fld] = "error";
					$status_count['fields']['error']++;
					$processed_data[$key]['import_status']['final_icon'] = "error";
					$processed_data[$key]['import_status'][$fld] = "Error - $fld_name must be unique - \"$value\" has been detected as a duplicate value.";
				}
			}
			//} else {
			if(!$has_error) {
				$errors[$key][$fld] = "";
				$status_count['fields']['ok']++;
				$import_data[$key][$fld] = $value; //ASSIST_HELPER::code($value);
			} else {
				$row_error = true;
			}
			$cell++;
		}
		unset($listObject);
		if(!$row_error) {
			$status_count['rows']['ok']++;
			$import_data[$key]['import_key']=$key;
			$import_data[$key]['import_ref']=$import_ref;
			$import_data[$key]['import_status']=0;
			$import_data[$key]['import_user']=$myObject->getUserID();
			$import_data[$key]['import_date']=date("Y-m-d H:i:s");
			$processed_data[$key]['import_status']['final_icon'] = "ok";
			$processed_data[$key]['import_status']['final_result'] = "Ok to import";
		} else {
			$status_count['rows']['error']++;
		}

	}
}
if($has_data!==true) {
	ASSIST_HELPER::displayResult(array("error","The file appears to be empty.  Please try again.  Remember that the import process ignores the first 2 rows as they are assumed to be headings."));
	die();
}


/* If all okay then load to temp table to facilitate quick accept import step 4 */
if($status_count['rows']['error']==0) {
	foreach($import_data as $key => $row) {
		$row['results'] = base64_encode(serialize($row['results']));
		$row[$myObject->getParentFieldName()] = $idp_object_id;
		$sql = "INSERT INTO ".$myObject->getTableName()."_temp SET ".$myObject->convertArrayToSQLForSave($row);
		//echo "<P>".$sql."</p>";
		$myObject->db_insert($sql);
	}
}




/*
 * DATA VALIDATION during loop (DEVELOPMENT)

echo "<hr />";
 echo "end";
echo "<hr />";
ASSIST_HELPER::arrPrint($import_data);
echo "<hr />";
ASSIST_HELPER::arrPrint($processed_data);
echo "<hr />";
ASSIST_HELPER::arrPrint($status_count);
echo "<hr />";

 */
$colspan = isset($results_headings['rows']) ? count($results_headings['rows']) : 1;

$rowspan = (isset($time_periods) && count($time_periods)>0) ? 2 : 1;


?>
<table id=tbl_import style=''>
	<tr>
		<th rowspan=<?php echo $rowspan; ?>>&nbsp;</th>
		<th rowspan=<?php echo $rowspan; ?>>Row</th>
		<?php
		foreach($headings['rows'] as $fld => $head) {
			echo "<th rowspan=".$rowspan.">".$head['name']."</th>";
		}
if(isset($time_periods) && count($time_periods)>0) {

		foreach($time_periods as $fld => $head) {
			echo "<th colspan=".$colspan.">".$head."</th>";
		}
}
				?>
		<th rowspan=<?php echo $rowspan; ?>>Import Status</th>
	</tr>
		<?php
if(isset($time_periods) && count($time_periods)>0) {
echo "
	<tr>";
		
		foreach($time_periods as $ti => $time) {
			foreach($results_headings['rows'] as $fld => $head) {
				echo "<th >".$head['name']."</th>";
			}
		}
		
	echo "</tr>";
}


foreach($processed_data as $key => $row) {
	$final_icon = $row['import_status']['final_icon'];
	unset($row['import_status']['final_icon']);
		echo "
		<tr>
			<td><div class='div_message final_".$final_icon."'>".ASSIST_HELPER::getDisplayIconAsDiv($final_icon)."</div></td>
			<td class=center>".$row['row']."</td>";
	foreach($headings['rows'] as $fld => $head) {
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			";
	}
if(isset($time_periods) && count($time_periods)>0) {
	foreach($time_periods as $time_id => $head) {
		foreach($results_headings['rows'] as $fld => $head) {
			echo "
				<td><div class='div_message ".(isset($errors[$key]['results'][$time_id][$fld])?"".$errors[$key]['results'][$time_id][$fld]:"")."'>".$row['results'][$time_id][$fld]."</div></td>
				";
		}
	}
}
		echo "
			<td><div class='div_message final_".$final_icon."'>".implode("<br />",$row['import_status'])."</div></td>
		</tr>
		";
}
	?>
</table>
<div id=div_result style='width:500px;margin:10px auto;padding:10px;'>
	<p class='center b'>Import Result</p>
	<p>
		<?php
		if($status_count['rows']['error']>0) {
			echo "<p>There are ".$status_count['rows']['error']." rows in an error state".($status_count['rows']['warn']>0?" and ".$status_count['rows']['warn']." rows with warnings":"").".  The Import cannot be finalised.  Please correct the errors and try again.</p>
			<p class='b center'>WARNING: NO LINE ITEMS HAVE BEEN IMPORTED.";
		} else {
			if($status_count['rows']['warn']>0) {
				echo "<p>There are ".$status_count['rows']['warn']." rows with warnings.  The Import can be finalised however the warnings should be reviewed.</p>";
			} else {
				echo "<p class='center'>All ".$status_count['rows']['ok']." rows are ok to Import.</p>";
			}
			echo "
			<form name=frm_save method=post action=".$my_url.">
			<input type=hidden name=action value='STEP4' />
			<input type=hidden name=import_ref value='$import_ref' />
			<p class=center>
			<button id=btn_save>Accept Import</button>
			</p>
			<p class=center style='margin-top:10px'>
			<button id=btn_cancel>Cancel Import</button>
			</p>
						</form>
			";
		}
		?>
	</p>

</div>
<script type="text/javascript">
	$(function() {

		<?php
		if($status_count['rows']['error']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-error\");";
		} elseif($status_count['rows']['warn']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-info\");";
		} else {
			echo "$(\"#div_result\").addClass(\"ui-state-ok\").css(\"background\",\"#ffffff\");";
		}
		?>

		$("#tbl_import").css({"height":"100%"});
		$("#tbl_import td").css({"padding":"0px"});
		$("#tbl_import div.div_message").css({"margin":"1px","padding":"2px","height":"87%"});
		$("#tbl_import div.error").addClass("ui-state-error");
		$("#tbl_import div.warn").addClass("ui-state-info");
		$("#tbl_import div.final_ok").addClass("ui-state-ok");
		$("#tbl_import div.final_warn").addClass("ui-state-info");
		$("#tbl_import div.final_error").addClass("ui-state-error");

		$("#btn_save").button({
			icons:{primary:"ui-icon-check"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green")
		.css("margin","0 auto")
		.hover(function(){
			$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
		},function() {
			$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
		})
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			$("form[name=frm_save]").submit();
		});
		$("#btn_cancel").button({
			icons:{primary:"ui-icon-closethick"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.css("margin","0 auto")
		.hover(function(){
			$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
		},function() {
			$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
		})
		.click(function(e) {
			e.preventDefault();
			history.back();
		});
	});
</script>
<?php





//$all_vars = get_defined_vars();
//unset($all_vars['_SESSION']);
//unset($all_vars['_SERVER']);
//ASSIST_HELPER::arrPrint($all_list_items);
?>