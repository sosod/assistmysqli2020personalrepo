<?php
/************
 * REQUIRED:
 * include module/autoloader.php
 * $headings = array('rows'=>array(),'subs'=>array())
 * $results_headings = array('rows'=>array(),'subs'=>array())
 * $object_type = PROJECT || TOPKPI || DEPTKPI
 */

$me = new IDP3_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$today = time();
$has_time = isset($has_time) ? $has_time : false;
$time_setting = isset($time_setting) ? $time_setting : "MONTH";

$data = array(
	1=>array(),
	2=>array(),
);
foreach($headings['rows'] as $fld => $head) {
	$data[1][]=$head['name'].($head['required']==true?"*":"");
	$row_2 = "";
	switch($head['type']) {
		case "DATE":
			$row_2 = "Date: YYYY-MM-DD";
			break;
		case "REF":
			$row_2 = "System Reference";
			break;
			//$row_2 = "Small text";
			//break;
		case "SMLVC":
		case "MEDVC":
		case "LRGVC":
			$row_2 = "Text: Max ".$head['max']." characters";
			break;
		case "TEXT":
			$row_2 = "Text: Max 65000 characters";
			break;
		case "BOOL":
			$row_2 = "Yes / No";
			break;
		case "SEGMENT":
			$row_2 = "Single ".$me->getObjectName("MSCOA")." list item";
			break;
		case "MULTISEGMENT":
			$row_2 = "Multiple ".$me->getObjectName("MSCOA")." list items (separated by ;)";
			break;
		case "MULTIOBJECT":
		case "MULTILIST":
			$row_2 = "Multiple List items (separated by ;)";
			break;
		case "OBJECT":
		case "LIST":
			$row_2 = "Single List item";
			break;
		case "NUM":
			$row_2 = "Numbers only (no formatting)";
			break;
		default:
			$row_2 = ucfirst($head['type']);
			break;
	}
	$data[2][] = $row_2;
}



$fdata = "";
$fdata = "\"".implode("\",\"",$data[1])."\"\r\n\"".implode("\",\"",$data[2])."\"";




//WRITE DATA TO FILE
$filename = "../files/".$cmpcode."/".$modref."_".strtolower($object_type)."_template_".date("Ymd_Hi",$today).".csv";
$newfilename = strtolower($object_type)."_import_template_".date("YmdHis",$today).".csv";
$file = fopen($filename,"w");
fwrite($file,$fdata."\n");
fclose($file);
//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$newfilename.'"');
readfile($filename);


/*
$fdata = "\"Parent GUID\",\"Line Item GUID\",\"Line Item Name\",\"Description\",\"Has sub-levels?\",\"Can be assigned?\"\r\n\"\",\"Required & Unique\",\"Required\",\"\",\"Yes / No\",\"Yes / No\"\r\n";
*/
?>