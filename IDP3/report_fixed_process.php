<?php
$no_page_heading = true;
require_once("inc_header.php");

$report_ref = $_REQUEST['report'];
$idp_id = $_REQUEST['object_id'];

$reportObject = new IDP3_FIXED_REPORT();
$reports = $reportObject->getReportDetails($report_ref);
echo "<h1 class=center>".$reportObject->getCmpName()."</h1>";
echo "<h2 class=center>Report: ".$reports['name']."</h2>";


$data = $reportObject->getReport($report_ref,$idp_id);

$groups = $data['group'];
$head = $data['head'];
$multiple_rows = isset($head[0]) ? $head[0] : false;
if(isset($head[0])) { unset($head[0]); }
$rows = $data['rows'];
$count = $data['count'];
foreach($groups as $g => $grp) {
	echo "
	<h3>".$grp."</h3>
	<table class=list>
		<tr>
			";
	$cols=0;
	foreach($head[1] as $fld => $h) {
		$rowspan = 1;
		$colspan = 1;
		if($multiple_rows) {
			$rowspan=2;
			if(isset($head[2][$fld])) {
				$colspan = count($head[2][$fld]);
				$rowspan=1; 
			}
		}
		$cols+=$colspan;
		echo "<th rowspan=".$rowspan." colspan=".$colspan.">".$h."</th>";
	}
	echo "
		</tr>";
	if($multiple_rows) {
		echo "<tr>";
		foreach($head[2] as $fld => $h) {
			foreach($h as $h2) {
				echo "<th>".$h2."</th>";
			}
		}
		echo "</tr>";
	}
	if(isset($rows[$g]) && count($rows[$g])>0) {
		foreach($rows[$g] as $i => $r) {
			echo "
			<tr>";
			foreach($head[1] as $fld => $h) {
				if(isset($head[2][$fld])) {
					if(!isset($count[$g][$i][$fld]) || $count[$g][$i][$fld]>0) {
						if(!isset($count[$g][$i][$fld])) { $c = 1; } else { $c = $count[$g][$i][$fld]; }
						foreach($head[2][$fld] as $fld2 => $h2) {
							echo "<td rowspan=".$c.">".(isset($r[$fld][$fld2]) ? $r[$fld][$fld2] : "")."</td>";
						}
					}
				} else {
					if(!isset($count[$g][$i][$fld]) || $count[$g][$i][$fld]>0) {
						if(!isset($count[$g][$i][$fld])) { $c = 1; } else { $c = $count[$g][$i][$fld]; }
						echo "<td rowspan=".$c.">".(isset($r[$fld]) ? $r[$fld] : "")."</td>";
					}
				}
			}
			echo "
			</tr>";
		}
	} else {
		echo "<tr><td class=i colspan=".$cols.">No records to display.</td></tr>";
	}
	echo "
	</table>";
}


//ASSIST_HELPER::arrPrint($data);

echo "<p class=i>Report drawn on ".date("d F Y H:i:s")."</p>";
?>