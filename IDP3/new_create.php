<?php
//require_once("inc_header.php");
//ASSIST_HELPER::arrPrint(explode("_",$_SERVER['PHP_SELF']));


$_REQUEST['object_type'] = "IDP";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "IDP";
$button_label = "|build|";
$button_icon = "circlesmall-plus";



$add_button = true;
$add_button_label = "|add| |idp|";
$add_button_function = "showAddDialog();";

$page_direct = "new_create_idp.php";


$button = array(
	array(
		'value' => "|build|",
		'class' => "btn_list",
		'type' => "button",
		'icon' => "circlesmall-plus"
	),
	array(
		'value' => "|edit|",
		'class' => "btn_edit",
		'type' => "button",
		'icon' => "pencil"
	),
);


include("common/generic_list_page.php");


?>


<div id=dlg_add_idp title="<?php echo $helper->replaceAllNames("|add| |idp|"); ?>" div_action="ADD">
	<?php

	$section = "NEW";
	$page_redirect_path = "new_create.php?";
	$page_action = "Add";

	$uaObject = new IDP3_USERACCESS();

	$child_object_type = "IDP";
	$child_object_id = 0;
	$childObject = new IDP3_IDP();

	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	echo $data['display'];

?>
</div>


<div id=dlg_edit_idp title="<?php echo $helper->replaceAllNames("|edit| |idp|"); ?>" div_action="EDIT">
	<?php

	$page_action = "EDIT";

	$data2 = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	echo $data2['display'];
	$data2['js'].=$displayObject->drawPageFooter("",$childObject->getMyLogTable(),"object_type=".$child_object_type,0);

?>
</div>
<script type="text/javascript">
$(function () {
	$("#dlg_add_idp").dialog({
		autoOpen: false,
		modal: true
	});
	$("#dlg_edit_idp").dialog({
		autoOpen: false,
		modal: true
	});
	<?php echo $data['js']; ?>
	<?php echo $data2['js']; ?>



	var add_title = "<?php echo addslashes($helper->replaceAllNames("|add| |idp|")); ?>";
	var add_ref = $("#dlg_add_idp table tr:first td:last").html();
	var edit_title = "<?php echo addslashes($helper->replaceAllNames("|edit| |idp|")); ?>";
	var edit_ref = "<?php echo $childObject->getRefTag(); ?>";


	$(".btn_edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var ref = $(this).attr("ref");
		var idp = AssistHelper.doAjax("inc_controller.php?action=IDP.getARawObjectFromController","object_id="+ref);
		var fy = 0;
		$("#dlg_edit_idp").find("select.forecast_iy_finyear_id:gt(0)").remove();
		$("#dlg_edit_idp").find(".forecast_line_break").remove();
		ifyear_count=1;
		for(x in idp) {
			if(x=="idp_finyear_id") {
				fy = idp[x];
			}
			if(x=="iy_finyear_id") {
				var c = 0;
				for(i in idp[x]) {
					var my_fy_val = idp[x][i]*1;
					if(idp[x][i]!=fy) {
						if(c==0) {
							$("#dlg_edit_idp #iy_finyear_id").val(idp[x][i]);
						} else {
							$("#dlg_edit_idp #btn_forecast_iy_finyear_id_EDIT").trigger("click");
							$("#dlg_edit_idp #iy_finyear_id"+c).val(my_fy_val).change(function() {
								checkForDisabledItems($(this));
							});
						}
						c++;
					}
				}
			} else {
				$("#dlg_edit_idp #"+x).val(idp[x]);
			}
		}
		$("#dlg_edit_idp #iy_finyear_id").trigger("change");
		$("#dlg_edit_idp table tr:first td:last").html(edit_ref+ref);
		$("#dlg_edit_idp #object_id").val(ref);//.prop("type","text");
		$("#dlg_edit_idp #log_pic").attr("object_id",ref);
		showEditDialog();
		//frm_object_IDP_0_add_page_action = "IDP.EDIT";
		AssistHelper.closeProcessing();
	});


	$("#dlg_edit_idp select.forecast_iy_finyear_id").change(function() {
		checkForDisabledItems($(this));
	});


	//get column width for last column
	var w = $(".btn_edit:first").width() + $(".btn_list:first").width() + 30;
	$(".btn_edit:first").closest("td").parent().parent().find("tr").find("td:last").width(w);

	//function to adjust disabled options in financial year dropdowns
	function checkForDisabledItems($sel) {
		var used_items = [];
		$sel.closest("form").find("select.forecast_iy_finyear_id, select#idp_finyear_id").each(function() {
			if($(this).val()!="X") {
				used_items.push($(this).val());
			}
		});

		$sel.closest("form").find("select.forecast_iy_finyear_id, select#idp_finyear_id").find('option:not(:first):not(:selected)').prop("disabled","");
		$sel.closest("form").find("select.forecast_iy_finyear_id, select#idp_finyear_id").find('option:not(:first):not(:selected)').filter(function() {
			return $.inArray($.trim($(this).prop('value')), used_items) != -1;
		}).prop('disabled','disabled');

	}
});
function showAddDialog() {
	$(function() {
		$("#dlg_add_idp").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("#dlg_add_idp form[name=frm_object] table.form").css("width","800px");
			$("#dlg_add_idp").dialog("option","width",850);
		} else {
			$("#dlg_add_idp form[name=frm_object] table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_add_idp").dialog("option","width",(my_window['width']-50));
		}
		//$("#dlg_add_idp").dialog("option","height",my_window['height']-50);
		var h = $("#dlg_add_idp").height()+75;
		if(h > my_window['height']-50) {
			h = my_window['height']-50;
		}
		$("#dlg_add_idp").dialog("option","height",h);
	});
}
function showEditDialog() {
	$(function() {
		var audit_log_state = $("#dlg_edit_idp #0_disp_audit_log").attr("state");
		if(audit_log_state=="show") {
			$("#dlg_edit_idp #0_disp_audit_log").trigger("click");
		}
		$("#dlg_edit_idp").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("#dlg_edit_idp form[name=frm_object] table.form").css("width","800px");
			$("#dlg_edit_idp").dialog("option","width",850);
		} else {
			$("#dlg_edit_idp form[name=frm_object] table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_edit_idp").dialog("option","width",(my_window['width']-50));
		}
		//$("#dlg_add_idp").dialog("option","height",my_window['height']-50);
		var h = $("#dlg_edit_idp").height()+75;
		if(h > my_window['height']-50) {
			h = my_window['height']-50;
		}
		$("#dlg_edit_idp").dialog("option","height",h);
	});
}

</script>
