<?php
require_once("inc_header.php");

$me = new IDP3_SETUP_INDICATOR();
$object_name = $me->getObjectName($me->getMyObjectType());

$indicator = $me->getLatestIndicatorStructure();

$divider = $indicator['divider'];
$structure = explode("||",$indicator['structure']);
$fields = explode("||",$indicator['fields']);
$min_length = $indicator['min_length'];

$example_code = array();
$e = 1; 
foreach($structure as $key => $s) {
	$c = "IDP3_".$s;
	$o = new $c();
	$structure[$key] = $o->getObjectName($o->getMyObjectName());
	if(substr($fields[$key],-2)=="id") {
		$example_code[$fields[$key]] = $e;
		$e++;
	} else {
		$example_code[$fields[$key]] = strtoupper($o->getMyObjectName());
	}
}

$headingObject = new IDP3_HEADINGS();
$headings = $headingObject->getAHeadingNameByField($fields);

foreach($fields as $key => $f) {
	if(substr($f,-2)=="id") {
		$fields[$key] = "System Ref";
	} elseif(isset($headings[$f])) {
		$fields[$key] = $headings[$f];
	}
}

$example = $me->generateIndicatorCode($example_code);

?>
<h2><?php echo $object_name; ?></h2>
<?php
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
$warning = array("info","Please contact your Assist Reseller in order to make any changes to the Indicator Code structure.  Before doing so, please ensure that any changes to the IDP structure (Setup / Naming section) have been completed.");
ASSIST_HELPER::displayResult($warning);
?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_code>
<table id=tbl_code class=form>
	
	<tr>
		<th>Divider:</th>
		<td><?php echo $divider; ?></td>
	</tr>
	
	<tr>
		<th>Structure:</th>
		<td><ul>
			<?php foreach($structure as $key => $s) {
				echo "<li>".$s." -> ".$fields[$key]."</li>";
			} ?>
		</ul></td>
	</tr>
	
	<tr>
		<th>Minimum Length:</th>
		<td><?php echo $min_length; ?> <span class='i float'>(Where System Ref is used)</span></td>
	</tr>

	<tr>
		<th>Example:</th>
		<td><?php echo $example; ?></td>
	</tr>

</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php')); ?></td>
	</tr>
</table>

<style type="text/css" >
	.saveform {
		font-size: 90%;
	}
	.saveform-active .ui-icon { background-image: url(/library/images/ui-icons_009900_256x240.png); }
	.saveform-active {
		border: 1px solid #009900;
		color: #009900;
		background-color: #ffffff;
		background-image: url();
	}
</style>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>

});
</script>