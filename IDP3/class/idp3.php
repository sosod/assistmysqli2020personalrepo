<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3 extends IDP3_HELPER {
    /***
     * Module Wide variables
     */
	protected $object_form_extra_js = "";

	protected $deliverable_types = array(
		'DEL'=>"Deliverable With Actions",
		'MAIN'=>"Deliverable With Sub-Deliverables",
		'SUB'=>"Sub-Deliverable With Actions",
	);

	protected $copy_function_protected_fields = array("contract_have_subdeliverables","del_type","del_parent_id");
	protected $copy_function_protected_heading_types = array("ATTACH","DEL_TYPE");

	protected $has_target = false;

	protected $has_secondary_parent = false;

	protected $local_modref;
	protected $called_from_external_module = false;


    /**
     * Module Wide Constants
     * */
    const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;

	const AWAITING_APPROVAL = 16;
	const APPROVED = 128;

	const STRATEGY_DRAFT = 2048;
	const STRATEGY_FINAL = 4096;


	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref="",$external=false,$set_names_for_external=false) {
		parent::__construct($modref,$external,$set_names_for_external);
		$this->called_from_external_module = $external;
		if(strlen($modref)==0) {
			$this->local_modref = $this->getModRef();
		} else {
			$this->local_modref = $modref;
		}
	}

	public function getBlankIndicatorCodeArray() {
		return array(
						'KPA'=>0,
						'STRATOBJ'=>0,
						'KPI'=>"",
						'PROJ'=>"",
					);
	}


	public function getMySecondaryParentObjectType() {
		if($this->has_secondary_parent) {
			return self::SECONDARY_PARENT_OBJECT_TYPE;
		}
		return false;
	}

	public function getSpecificForecastYearName($id) {
		$iyObject = new IDP3_IDPYEARS();
		return $iyObject->getSpecificYear($id);
	}


/*********************
 * MODULE OBJECT functions
 * for CONTRACT, DELIVERABLE AND ACTION
 */
	public function getObject($var) { //$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'],$options);
				if($var['type']=="LIST" || $var['type']=="ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data,$this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details)>0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'],$var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'],$var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}
	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}
	public function getProgressFieldName() {
		return $this->progress_field;
	}
	public function getIDFieldName() {
		return $this->id_field;
	}
	public function getParentFieldName() {
		return $this->parent_field;
	}
	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}
	public function getNameFieldName() {
		return $this->name_field;
	}
	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}
	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}
	public function getOwnerFieldName() {
		return $this->owner_field;
	}
	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}
	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}
	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}
	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}

	public function getPageLimit() {
		//$profileObject = new IDP3_PROFILE();
        //Profile function needs to be restored - With JC & TM [Paired] 17 July 2019
		return /*$profileObject->getProfileTableLength()*/15;
	}

	public function getDateOptions($fld) {
		if(stripos($fld,"action_on")!==FALSE) { $fld = "action_on"; }
		switch($fld) {
			case "action_on":
				return array('maxDate'=>"'+0D'");
				break;
			default:
				return array();
		}
	}

	public function getExtraObjectFormJS() { return $this->object_form_extra_js; }

	public function getAllDeliverableTypes() { return $this->deliverable_types; }

	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key=>$t){
			$arr[]=$t;
		}
		return $arr;
	}

	public function getDeliverableTypes($contract_id) {
		$contractObject = new IDP3_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables()==FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types;
	}
	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt,$ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE",$options);
		//$this->arrPrint($data);
		$result = array(
			'past'=>0,
			'present'=>0,
			'future'=>0,
		);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z<$now) {
				$result['past']++; //echo "past";
			} elseif($z>$now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE",$options);
		return $data;
	}


	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($options=array()) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." ";
		if(count($options)>0) {
			foreach($options as $key => $val) {
				switch($key) {
					case "id":
						$sql.= " AND ".$this->getIDFieldName()." = ".$val;
						break;
					case "parent":
						$sql.= " AND ".$this->getParentFieldName()." = ".$val;
						break;
				}
			}
		} //echo $sql;
		$sql.= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getAllObjectsFormattedForSelect() {
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED."
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}
















	public function getMyList($obj_type,$section,$options=array()) {
//		echo "<P>object: ".$obj_type;
//		echo "<P>section: ".$section;
//		echo "<P>options: "; $this->arrPrint($options);
		if(isset($options['page'])) {
			$page = strtoupper($options['page']);
			unset($options['page']);
		} else {
			$page = "DEFAULT";
		}
		if(isset($options['mscoa_version'])) {
			$mscoa_version_id = $options['mscoa_version'];
			unset($options['mscoa_version']);
		} else {
			$mscoa_version_id = 1;
		}
		$trigger_rows = true;
		if(isset($options['trigger'])) {
			$trigger_rows = $options['trigger'];
			unset($options['trigger']);
		}
		//echo $page;
		if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
			$future_deadline = $options['deadline'];
			unset($options['deadline']);
			if(!isset($options['limit'])) { $options['limit'] = false; }
		}
		$compact_details = ($page == "CONFIRM" || $page == "ALL");
		if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
			//$pagelimit = false;
			$pagelimit = 15;
			$start = 0;
			$limit = false;
			$limit = $pagelimit;
			unset($options['start']);
			unset($options['limit']);
		}else{
			$pagelimit = $this->getPageLimit();
			//$pagelimit = 15;
			$start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
			$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
			$limit = $pagelimit;
			if($start<0) {
				$start = 0;
				$limit = $limit;
			}
		}
		$order_by = ""; //still to be processed
		$left_joins = array();

		$headObject = new IDP3_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());

		//set up variables
		$idp_headings = $headObject->getMainObjectHeadings($obj_type,($obj_type=="IDP"?"LIST":"FULL"),$section);
		switch($obj_type) {

			case "IDP":
				$idpObject = new IDP3_IDP();
				break;
			case "STRATOBJ":
				$idpObject = new IDP3_STRATOBJ();
				break;
			case "PMKPA":
				$idpObject = new IDP3_PMKPA();
				break;
			case "PROJECT":
				$idpObject = new IDP3_PROJECT();
				break;
		}
				$idp_tblref = $idpObject->getMyObjectType();
				$idp_table = $idpObject->getTableName();
				$idp_id = $idp_tblref.".".$idpObject->getIDFieldName();
				$idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
				$idp_status_fld = false;//$idp_tblref.".".$idpObject->getProgressStatusFieldName();
				$idp_name = $idp_tblref.".".$idpObject->getNameFieldName();
				$idp_deadline = false;//$idpObject->getDeadlineFieldName();
				$idp_field = $idp_tblref.".".$idpObject->getTableField();

				$where = $idpObject->getActiveStatusSQL($idp_tblref);
		//if type is something other than contract
		if($obj_type!="IDP") {
			switch($obj_type) {

				case "STRATOBJ":
				case "PMKPA":
				case "PROJECT":
					$id_fld = $idpObject->getIDFieldName();
					$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
					$from = " $idp_table $idp_tblref
					";
					$final_data['head'] = $idp_headings;
					$where_tblref = $idp_tblref;
					$where_deadline = $idp_deadline;
					$where_order = $idp_name;
					$ref_tag = $idpObject->getRefTag();
					if($page=="LOGIN_TABLE") {
						$order_by = $idp_name;
					}
					$where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
					$group_by = "";
					break;

			}
		} else {
			$id_fld = $idpObject->getIDFieldName();
			$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
			$from = " $idp_table $idp_tblref
			";
			$final_data['head'] = $idp_headings;
			$where_tblref = $idp_tblref;
			$where_deadline = $idp_deadline;
			$where_order = $idp_name;
			$ref_tag = $idpObject->getRefTag();
			if($page=="LOGIN_TABLE") {
				$order_by = $idp_name;
			}
			$where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
			$group_by = "";
		}
//ASSIST_HELPER::arrPrint($final_data);
		if(count($options)>0) {
			foreach($options as $key => $filter) {
				if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
					$key = $this->getTableField()."_".$key;
				}
				$where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
			}
		}


			$all_headings = array_merge($idp_headings);

			$listObject = new IDP3_LIST();

			//$list_headings = $headObject->getAllListHeadings();
			//$list_tables = $listObject->getAllListTables(array_keys($list_headings));


			foreach($all_headings as $fld => $head) {
				$lj_tblref = $head['section'];
				if($head['type']=="LIST") {
					$listObject->changeListType($head['list_table']);
					$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])."
										AS ".$head['list_table']."
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
										AND (".$head['list_table'].".status & ".IDP3::DELETED.") <> ".IDP3::DELETED;
				} elseif($head['type']=="SEGMENT") {
					/*$segObject = new IDP3_SEGMENTS($head['list_table']);
					$segObject = new IDP3_SEGMENTS($head['list_table']);
					//$listObject->changeListType($head['list_table']);
					$sql.= ", ".$head['list_table'].".".$segObject->getNameFieldName()." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$segObject->getTableName()."
										AS ".$head['list_table']."
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
										AND (".$head['list_table'].".status & ".IDP3::DELETED.") <> ".IDP3::DELETED;*/
					/*$left_joins[] = "LEFT OUTER JOIN ".$segObject->getTableName()."
										AS ".$head['list_table']."
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
										AND (".$head['list_table'].".status & ".IDP3::DELETED.") <> ".IDP3::DELETED;*/
				} elseif($head['type']=="MASTER") {
					$tbl = $head['list_table'];
					$masterObject = new IDP3_MASTER($fld);
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				}
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where;
			switch($section) {
				case "ACTIVE":
					$sql.=" AND ".$idpObject->getActiveStatusSQL($idp_tblref);
					break;
				case "NEW":
					if($page=="ACTIVATE_WAITING") {
						$sql.=" AND ".$idpObject->getActivationStatusSQL($idp_tblref);
					} elseif($page=="ACTIVATE_DONE") {
						$sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
					} else {
						$sql.=" AND ".$idpObject->getNewStatusSQL($idp_tblref);
					}
					break;
				case "MANAGE":
					$sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
					break;
				case "REPORT":
				case "SEARCH":
					$sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
					break;
			}
			//$sql.= isset($parent_id) ? " AND ".$this->getParentFieldName()." = ".$parent_id : "";
			$sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";
			$mnr = $this->db_get_num_rows($sql);
		if($trigger_rows==true) {

//		    echo '<pre style="font-size: 18px">';
//		    echo '<p>LIMIT</p>';
//		    print_r($limit);
//            echo '<p>START</p>';
//            print_r($start);
//		    echo '</pre>';

			$start = ($start!=false && is_numeric($start) ? $start : "0");
			$sql.=(strlen($order_by)>0 ? " ORDER BY ".$order_by : " ORDER BY ".$where_order).($limit !== false?" LIMIT ".$start." , $limit ":"");
			//echo $sql;
			//return array($sql);
			$rows = $this->mysql_fetch_all_by_id($sql,$id_fld);
			if($obj_type=="IDP") {
				$keys = array_keys($rows);
				$iyObject = new IDP3_IDPYEARS();
				$forecast_years = $iyObject->getRowsForParentListTable($keys,true);
				foreach($rows as $key => $r) {
					$rows[$key][$iyObject->getNameFieldName()] = isset($forecast_years[$key]) ? $forecast_years[$key] : $this->getUnspecified();
					$rows[$key]['financial_years_id'] = $r[$idpObject->getFinYearField()];
					$rows[$key][$idpObject->getFinYearField()] = $r['financial_years'];
					$rows[$key][$idpObject->getTableField()."_mscoa_version_id"] = $r['mscoa_version'];
					$fld = "idp_status";
					$val = $r[$fld];
					if($val==self::ACTIVE) {
						$val = "New";
					} elseif($val==(IDP3_IDP::CONFIRMATION_REQUESTED + self::ACTIVE)) {
						$val = "Confirmation requested";
					} elseif($val & IDP3_IDP::ACTIVATED == IDP3_IDP::ACTIVATED) {
						$val = "Activated";
					} elseif($val & IDP3_IDP::CONFIRMED == IDP3_IDP::CONFIRMED) {
						$val = "Confirmed";
					} else {
						$val = "Unknown Status [$val]";
					}
					$rows[$key][$fld] = $val;
				}
			} else {
				$all_my_lists = array();
				foreach($all_headings as $fld => $head) {
					if($fld=="idp_status") {
					} else {
						switch($head['type']) {
							case "MULTISEGMENT":
							case "SEGMENT":
								//get list items if not already available
								if(!isset($all_my_lists[$fld])) {
									$listObject = new IDP3_SEGMENTS($head['list_table']);
									$extra_info = array('return_no_posting'=>false,'version'=>$mscoa_version_id);
									$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect($extra_info);
									unset($listObject);
								}
								//check each row and replace as needed
								foreach($rows as $key => $r) {
									if($head['type']=="SEGMENT") {
										$val = array($r[$fld]);
									} else {
										$val = explode(";",$r[$fld]);
									}
									$rows[$key][$fld] = array();
									foreach($val as $x) {
										if(isset($all_my_lists[$fld][$x])) {
											$rows[$key][$fld][] = $all_my_lists[$fld][$x];
										}
									}
									if(count($rows[$key][$fld])>0) {
										$rows[$key][$fld] = implode("; ",$rows[$key][$fld]);
									} else {
										$rows[$key][$fld] = $this->getUnspecified();
									}
								}
								break;
							case "MULTILIST":
								//get list items if not already available
								if(!isset($all_my_lists[$fld])) {
									$listObject = new IDP3_LIST($head['list_table']);
									$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
									unset($listObject);
								}
								//check each row and replace as needed
								foreach($rows as $key => $r) {
									$val = explode(";",$r[$fld]);
									$rows[$key][$fld] = array();
									foreach($val as $x) {
										if(isset($all_my_lists[$fld][$x])) {
											$rows[$key][$fld][] = $all_my_lists[$fld][$x];
										}
									}
									if(count($rows[$key][$fld])>0) {
										$rows[$key][$fld] = implode("; ",$rows[$key][$fld]);
									} else {
										$rows[$key][$fld] = $this->getUnspecified();
									}
								}
								break;
							case "OBJECT":
								//get list items if not already available
								if(!isset($all_my_lists[$fld])) {
									$list_object_name = $head['list_table'];
									$listObject = new $list_object_name();
									$all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect();
									unset($listObject);
								}
								//check each row and replace as needed
								foreach($rows as $key => $r) {
									$x = $r[$fld];
									if(isset($all_my_lists[$fld][$x])) {
										$rows[$key][$fld] = $all_my_lists[$fld][$x];
									} else {
										$rows[$key][$fld] = $this->getUnspecified();
									}
								}
								break;
						}
					}
				}
			}
			$final_data = $this->formatRowDisplay($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,false,array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
		} else {
			$final_data['rows'] = array();
		}
		$final_data['head'] = $this->replaceObjectNames($final_data['head']);

        $final_data['the_sql_that_generated_this_data'] = $sql;

		return $final_data;
	}
	function formatRowDisplay($mnr,$rows,$final_data,$id_fld,$headObject,$ref_tag,$status_id_fld,$paging) {
		$limit = $paging['limit'];
		$pagelimit = $paging['pagelimit'];
		$start = $paging['start'];
			//ASSIST_HELPER::arrPrint($rows);
			$keys = array_keys($rows);
			$displayObject = new IDP3_DISPLAY();
			foreach($rows as $r) {
				$row = array();
				$i = $r[$id_fld];
				foreach($final_data['head'] as $fld=> $head) {
					if($head['parent_id']==0){
						if($headObject->isListField($head['type']) && $head['type']!="DELIVERABLE") {
							$row[$fld] = (isset($r[$fld]) && strlen($r[$fld])>0) ? $r[$fld] : $this->getUnspecified();
							if($status_id_fld!==false && $fld==$status_id_fld) {
								if(($r['my_status'] & IDP3::AWAITING_APPROVAL) == IDP3::AWAITING_APPROVAL) {
									$row[$fld].=" (Awaiting approval)";
								} elseif(($r['my_status'] & IDP3::APPROVED) == IDP3::APPROVED) {
									$row[$fld].= " (Approved)";
								}
							}
						} elseif($this->isDateField($fld)) {
							$row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
						} else {
							//$row[$fld] = $r[$fld];
							$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
						}
					}
				}
				$final_data['rows'][$i] = $row;
				//$mnr++;
			}

			if($mnr==0 || $limit === false) {
				$totalpages = 1;
				$currentpage = 1;
			} else {
				$totalpages = round(($mnr/$pagelimit),0);
				$totalpages += (($totalpages*$pagelimit)>=$mnr ? 0 : 1);
				if($start==0) {
					$currentpage = 1;
				} else {
					$currentpage = round($start/$pagelimit,0);
					$currentpage += (($currentpage*$pagelimit)>$start ? 0 : 1);
				}
			}
			$final_data['paging'] = array(
				'totalrows'=>$mnr,
				'totalpages'=>$totalpages,
				'currentpage'=>$currentpage,
				'pagelimit'=>$pagelimit,
				'first'=>($start==0 ? false : 0),
				'prev'=>($start==0 ? false : ($start-$pagelimit)),
				'next'=>($totalpages==$currentpage ? false : ($start+$pagelimit)),
				'last'=>($currentpage==$totalpages ? false : ($totalpages-1)*$pagelimit),
			);
		return $final_data;
	}


	//public function getDetailedObject($obj_type,$id,$options=array()) {
		//die("getDetailedObject error");
	//}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}



    /***********************************
     * GET functions
     */

    /**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
echo "WRONG PAGE FOOTER";
return "";
    }
    /**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="") {
/*    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#div_audit_log\").html(\"\");
					$(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					$(\"#div_audit_log\").html(result[0]);
					$(\"#log_txt\").html('Hide Activity Log');
				}

			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
 **/
		return array('display'=>"WRONG PAGE FOOTER",'js'=>"");
    }

	public function drawActivityLog() {

	}




    /*********************************************
     * SET/UPDATE functions
     */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new IDP3_LOG($log_table);
	//	$logObject->addObject($var);
	//}


    public function notify($data,$type,$object){
    	//$noteObj = new IDP3_NOTIFICATION($object);
		//$result = $noteObj->prepareNote($data, $type, $object);
		//return $result;
		return false;
    }




	public function editMyObject($var,$attach=array(),$extra_logs=array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);

		$is_segment = isset($var['is_segment']) && $var['is_segment']===true ? true : false;

		$headObject = new IDP3_HEADINGS();


		if($is_segment) {
			$headings = $headObject->getMainObjectHeadings("SEGMENT","FORM");
		} else {
			$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"FORM");
		}

		//return array("error",serialize($headings));

		$mar=0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld=>$v) {
			if(isset($headings['rows'][$fld])) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type']=="DATE") {
					if(strlen($v)>0){
						$insert_data[$fld] = date("Y-m-d",strtotime($v));
					}
				} elseif(is_array($v)) {
					$insert_data[$fld] = implode(";",$v);
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}


		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

		//if($mar>0 || count($extra_logs)>0 || count($attach)>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			foreach($insert_data as $fld => $v) {
				$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type'=>"TEXT");
				if($old[$fld]!=$v || $h['type']=="HEADING") {
					if(in_array($h['type'],array("LIST","MASTER","USER","OBJECT","PROJECT","FUTURE","SEGMENT","MULTILIST","MULTISEGMENT"))) {
						$list_items = array();
						if(in_array($h['type'],array("MULTILIST","MULTISEGMENT"))) {
							$old[$fld] = explode(";",$old[$fld]);
							$v = explode(";",$v);
							$ids = array_merge($v,$old[$fld]);
						} else {
							$ids = array($v,$old[$fld]);
						}
						switch($h['type']) {
							case "LIST":
							case "MULTILIST":
								$listObject = new IDP3_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "SEGMENT":
							case "MULTISEGMENT":
								$listObject = new IDP3_SEGMENTS($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$listObject = new IDP3_USERACCESS();
								$list_items = $listObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "MASTER":
							case "FUTURE":
								$listObject = new IDP3_MASTER($h['list_table']);
								$list_items = $listObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "OBJECT":
								$table = $h['list_table'];
								$listObject = new $table();
								$list_items = $listObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "PROJECT":
								$table = $h['list_table'];
								$listObject = new $table();
								$list_items = $listObject->getActiveItemsFormattedForSelect($ids);
								break;
						}

						unset($listObject);
						if(in_array($h['type'],array("MULTILIST","MULTISEGMENT"))) {
							$to = array();
							foreach($v as $x) {
								if($this->checkIntRef($x) && isset($list_items[$x])) {
									$to[] = $list_items[$x];
								}
							}
							if(count($to) > 0) {
								$to = implode("; ",$to);
							} else {
								$to = $this->getUnspecified();
							}
							$from = array();
							foreach($old[$fld] as $x) {
								if($this->checkIntRef($x) && isset($list_items[$x])) {
									$from[] = $list_items[$x];
								}
							}
							if(count($from) > 0) {
								$from = implode("; ",$from);
							} else {
								$from = $this->getUnspecified();
							}
							$v = implode(";",$v);
							$old[$fld] = implode(";",$old[$fld]);
						} else {
							$to = (($v==0 || !isset($list_items[$v])) ? $this->getUnspecified() : $list_items[$v]);
							$from = (($v==0 || !isset($list_items[$old[$fld]])) ? $this->getUnspecified() : $list_items[$old[$fld]]);
						}

							$changes[$fld] = array(
								'to'=>$to,
								'from'=>$from,
								'raw'=>array(
									'to'=>$v,
									'from'=>$old[$fld])
								);
					} elseif($h['type']=="HEADING") {
					} else {
						$changes[$fld] = array('to'=>$v,'from'=>$old[$fld]);
					}
				}
			}

			if(count($attach)>0) {
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getAttachmentFieldName()] = $x;
			}
			if(count($extra_logs)>0) {
			$changes = array_merge($changes,$extra_logs);
			}

		if(count($changes)>0) {
			$log_var = array(
				'object_id'		=> $object_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::EDIT,
			);
			if($is_segment) {
				$log_var['object_type'] = $this->getMyObjectType();
			}
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." has been updated successfully.",array($log_var,$this->getMyObjectType()));
		}
		return array("info","No changes were found to be saved.  Please try again.");
	}










    /********************************************
     * PROTECTED functions
     */









	/*******************************************
     * PRIVATE functions
     */
















     public function __destruct() {
     	parent::__destruct();
     }




}






?>