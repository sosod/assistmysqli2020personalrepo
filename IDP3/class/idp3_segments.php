<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3_SEGMENTS extends IDP3 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = "parent_id";
	protected $secondary_parent_field = "idp_id";
	protected $name_field = "name";
	protected $description_field = "description";
	protected $has_children_field = "has_child";
	protected $can_post_field = "can_post";
	protected $attachment_field = "attachment";

	protected $has_attachment = false;

    protected $ref_tag = "";

	protected $segments;
	protected $section;
	protected $section_set = false;

	protected $dynamic_object_type;
	protected $dynamic_parent_object_type;
	protected $dynamic_object_name;
	protected $dynamic_table;

	protected $segmentObject;
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = false;
    const OBJECT_NAME = false;
	const PARENT_OBJECT_TYPE = false;

    const TABLE = false;
    const TABLE_FLD = "";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "segment";

	/**
	 * Status Constants
	 */
    //const CONFIRMED = 32;
	//const ACTIVATED = 64;

    public function __construct($section="",$modref="") {
        parent::__construct($modref);

		$nameObj = new IDP3_NAMES($modref);
		$this->segments = $nameObj->fetchSegmentNames(true);

		$this->object_form_extra_js = "";

		if(strlen($section)>0 && $section!==false) {
			$this->setSection($section);
		}

    }

	public function setSection($section) {
		$this->segmentObject = new MSCOA1_SEGMENTS($section,$this->mscoa_mod_ref);
			$this->dynamic_table = $this->segmentObject->getTableName();//self::LOG_TABLE."_".strtolower($section);
			$this->dynamic_parent_object_type = strtolower($section);
			$this->dynamic_object_type = strtolower($section);
			$this->dynamic_object_name = strtolower($section);
			$this->section = strtolower($section);
			$this->ref_tag = $this->segmentObject->getRefTag();//"SEG/".strtoupper($section);
			$this->section_set = true;
	}
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		return array("error","Old mSCOA Segment add function");
	}






	public function editObject($var,$attach=array()) {
		return array("error","Old mSCOA Segment edit function");
	}

	/**
	 * Check whether an object is in use
	 */
	public function checkUsage($var) {
		//TO BE PROGRAMMED - 17 APRIL 2017 [JC]
		return false;
	}
	/**
	 * Check whether an object can be deleted
	 * 	calls this->checkUsage
	 */
	public function canIBeDeleted($var) {
		return false;
	}

	public function deleteObject($var) {
			$result = array(
				0=>"error",
				1=>"Old mSCOA segment delete function",
			);
			return $result;

	}

	public function deactivateObject($var) {
			$result = array(
				0=>"error",
				1=>"Old mSCOA segment deactivate function",
			);
			return $result;
	}

	public function restoreObject($var) {
			$result = array(
				0=>"error",
				1=>"Old mSCOA segment restore function",
			);
			return $result;
	}

	public function confirmObject($var) {
		$result = array("error","Old mSCOA Segment confirm function");
		return $result;
	}



	public function undoConfirmObject($var) {
		$result = array("error","Old mSCOA Segment undo confirm function");
		return $result;
	}


	public function activateObject($var) {
		$result = array("error","Old mSCOA Segment activate function");
		return $result;
	}


	public function undoActivateObject($var) {
		$result = array("error","Old mSCOA Segment undo activate function");
		return $result;
	}
    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->dynamic_table; }
    public function getDescriptionFieldName() { return $this->description_field; }
    public function getHasChildrenFieldName() { return $this->has_children_field; }
    public function getCanPostFieldName() { return $this->can_post_field; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return (self::OBJECT_TYPE!==false?self::OBJECT_TYPE:$this->dynamic_object_type); }
    public function getMyObjectName() { return (self::OBJECT_NAME!==false?self::OBJECT_NAME:$this->dynamic_object_name); }
    public function getMyParentObjectType() { return (self::PARENT_OBJECT_TYPE!==false?self::PARENT_OBJECT_TYPE:$this->dynamic_parent_object_type); }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}

	public function getSegmentDetails($i="") {
		if($i===true && strlen($this->section)>0) {
			return $this->segments[$this->section];
		} elseif($i!==true && strlen($i)>0) {
			return $this->segments[$i];
		} elseif($i!==true) {
			return $this->segments;
		} else {
			return false;
		}
	}

	public function getList($parent=0,$active_only=true,$idp_id=0) {
		$data = $this->segmentObject->getList($parent,$active_only,$idp_id,false);
		return $data;
	}

	public function getAllListItemsFormattedForSelect($return_no_posting=false) {
		$data = $this->segmentObject->getAllListItemsFormattedForSelect($return_no_posting);
		return $data;
	}


	/**
	 * needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($extra_info=false) {
		$items = $this->getActiveListItemsFormattedForSelect($extra_info);
		$data = array();
		foreach($items as $id => $name) {
			$name = $this->formatTextForComparison($name);//trim(strtolower(str_replace(" ","",$name)));
			$data[$name] = $id;
		}
		return $data;
	}



	public function getActiveListItemsFormattedForSelect($return_no_posting=false,$idp_id=0) { //echo $idp_id;
		$data = $this->segmentObject->getActiveListItemsFormattedForSelect($return_no_posting,$idp_id);
		return $data;
	}

	private function processList($data,$key,$d,$title,$items,$no_can_post) {
		$title[] = $d['name'];
		if($d['can_assign']==false) { $no_can_post[] = $key; }
		if($d['can_assign']==true) {
			$items[$key] = implode(": ",$title);
		}
		if(isset($data[$key]) && count($data[$key])>0) {
			foreach($data[$key] as $k => $e) {
				$ret = $this->processList($data, $k, $e, $title, $items, $no_can_post);
				//$title = $ret[0];
				$items = $ret[1];
				$no_can_post = $ret[2];
			}
		}
		return array($title,$items,$no_can_post);
	}

	public function getNameForAnObject($id) {
		$row = $this->getRawObject($id);
		if(is_array($id)) {
			$d = array();
			foreach($row as $k => $r) {
				$d[] = $r[$this->getNameFieldName()];
			}
			$data = implode(chr(10),$d);
		} else {
			$data = $row[$this->getNameFieldName()];
		}
		return $data;
	}
	public function getAObject($id=0,$options=array()) {
		$row = $this->getRawObject($id);
		return $row;
	}

	public function getAListItemName($id,$name_only = true) {
		$list = $this->getAllListItemsFormattedForSelect();
		$data = array();
		if(is_array($id)) {
			foreach($id as $i) {
				$data[$i] = isset($list[$i]) ? $list[$i] : "Not available";
			}
			return $data;
		} elseif($name_only) {
			return isset($list[$i]) ? $list[$i] : $this->getUnspecified();
		} else {
			$data[$i] = isset($list[$i]) ? $list[$i] : "Not available";
			return $data;
		}
	}

	public function getHeading($id) {
		$row = $this->getRawObject($id);
		return $row[$this->getNameFieldName()].(strlen($row[$this->getTableField()."_ref"])>0 ? " (".$row[$this->getTableField()."_ref"].")":"");
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		if(is_array($obj_id)) {
			$where = $this->getIDFieldName()." IN (".implode(",",$obj_id).")";
			$multi = true;
		} else {
			$where = $this->getIDFieldName()." = ".$obj_id;
			$multi = false;
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$where;
		if($multi) {
			$data = $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
		} else {
			$data = $this->mysql_fetch_one($sql);
		}
		return $data;
	}



	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		$res = array();
	/*	if($parent_id==0) {
			$parentObject = new IDP3_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");*/

		return $res2;
	}



	public function getListOfObjectsFormattedForSelect($active=true,$activated=true) {
		//$sql = "SELECT * FROM ".$this->getTableName()." WHERE "
	}


	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false,false,false);
	}
	/**
	 * Returns status check for Actions are fully activated & available outside of NEW
	 */
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW / ACTIVATE / WAITING
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATE",$t,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>