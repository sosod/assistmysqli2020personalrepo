<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3_PROJECT extends IDP3 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_idp_id";
	protected $secondary_parent_field = "_expresult_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";

	protected $has_attachment = false;

    protected $ref_tag = "IDP/P";

	protected $mscoa_ref_field_name = "proj_mscoa_ref";
	protected $int_ref_field_name = "proj_ref";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PROJECT";
    const OBJECT_NAME = "project";
	const PARENT_OBJECT_TYPE = "IDP";
	const SECONDARY_PARENT_OBJECT_TYPE = "STRATRESULT";

    const TABLE = "project";
    const TABLE_FLD = "proj";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

    public function __construct($obj_id=0,$modref="",$external=false) {
        parent::__construct($modref,$external);
		$this->has_secondary_parent = true;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }


	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		//return array("error","normal project");
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			} elseif(is_array($v)) {
				$var[$key] = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$v);
			}
			//check for unnecessary auto-complete fields
			if($this->isThisAnExtraAutoCompleteFieldName($key)==true) {
				unset($var[$key]);
			}
		}

		$var[$this->getTableField().'_status'] = IDP3::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		//return array("error",$sql);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);

			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}



	/**
	 * Function to test for fields that must be unique
	 */
	public function uniqueTest($var) {
		$parent_id = $var['parent_id'];
		$field = $var['field'];
		$raw_test_value = ($var['test_value']);
		$test_value = ASSIST_HELPER::stripStringForComparison($raw_test_value);
		$sql = "SELECT $field FROM ".$this->getTableName()." P WHERE ".$this->getParentFieldName()." = ".$parent_id." AND (".$this->getActiveStatusSQL("P").")";
		$rows = $this->mysql_fetch_all_by_value($sql, $field);

		foreach($rows as $i => $r) {
			$rows[$i] = ASSIST_HELPER::stripStringForComparison($r);
		}

		if(in_array($test_value,$rows)) {
			return "error";
		} else {
			return "ok";
		}


	}



	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach);
		return $result;
	}



	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
			);
			return $result;
	}



    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName($plural=false) { return $plural==true?self::OBJECT_NAME."s":self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getMscoaRefField() { return $this->mscoa_ref_field_name; }
	public function getInternalRefFieldName() { return $this->int_ref_field_name; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}
	public function getIDPidFromMyID($obj_id) {
		return $this->getParentID($obj_id);
	}


	public function getList($section,$options) {
    	$idpObject = new IDP3_IDP();
    	$raw_idp = $idpObject->getRawObject($options[$this->getParentFieldName()]);
    	$options['mscoa_version'] = $raw_idp['idp_mscoa_version_id'];
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
	public function getAMscoaRefFieldFromObjectID($obj_id,$int_ref) {
		//"OBJ_FOCUS_GOAL_RESULT_".internal_reference
		return "OBJ_FOCUS_GOAL_RESULT_".$int_ref;
	}
	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ";
		if(is_array($obj_id)) {
			if(count($obj_id)>0) {
				$sql.=$this->getIDFieldName()." IN (".implode(",",$obj_id).")";
				$data = $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
			} else {
				$data = array();
			}
		} else {
			$sql.=$this->getIDFieldName()." = ".$obj_id;
			$data = $this->mysql_fetch_one($sql);
		}
		return $data;
	}

	public function getRawObjectsForExternalModule($idp_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." P WHERE ".$this->getActiveStatusSQL("P")." AND ".$this->getParentFieldName()." = ".$idp_id;
		return $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
	}

	public function getObjectForExpectedResult($var){
		$expected_result_id = $var['expresult_id'];
		$is_edit_page = $var['is_edit'];
		$kpi_id = $var['kpi_id'];
		/**
		 * JC - 2017-05-05
		 * Added to take into account new Capital/Opex Yes/No field
		 * IF capital_or_opex = TRUE then only return CAPITAL projects that have not been assigned to KPI already
		 * IF capital_or_opex = FALSE then only return OPERATIONAL projects & display form must only allow for 1 project to be added
		 */

		$capital_or_opex = $var['cap_op'];
		$capital_or_opex_filter = array($this->getTableField().'_cap_op'=>$capital_or_opex);
		$result0 = $this->getOrderedObjects(0,$expected_result_id,$capital_or_opex_filter);
		$result = $result0[0];
		$list = array();
		$id_array = array();
		foreach($result as $parent_id => $res) {
			foreach($res as $id => $r) {
				$list[$id] = $r['name'];
				$id_array[] = $id;
			}
		}
		if(count($id_array)>0) {
			$kpObject = new IDP3_PMKPI_PROJECT();
			$kpi_count = $kpObject->getCountOfKPIsPerProject($id_array);
			/**
			 * Added to take into account Capital/Opex Yes/No field
			 */
			if($capital_or_opex==true) {
				foreach($id_array as $i) {
					if(isset($kpi_count[$i]) && count($kpi_count[$i])>0) {
						if(!$is_edit_page || !in_array($kpi_id,$kpi_count[$i])) {
							unset($list[$i]);
						}
					}
				}
			}
			/**
			 * Replaced by Capital/Opex Yes/No field
			 */
			//$proj_count = $kpObject->getCountOfProjectsPerKPI($id_array);
		}
		asort($list);
		$list_invert = array();
		$list2 = array();
		foreach($list as $i => $l) {
			$list_invert[$l] = $i;
			$list2[] = array('id'=>$i,'txt'=>$l);
		}
		/**
		 * $list = list of projects associated with the selected Expected Result
		 * $kpi_count = number of KPIs each project has been linked to = if > 0 then project can't share KPI with other projects
		 * $proj_count = does a PROJECT share it's KPI with other projects? = if TRUE then can't link to more than 1 KPI
		$return = array(
			'list'=>$list,
			'kpi_count'=>$kpi_count,
			'other_projects'=>$proj_count,
		);
		 */
		return array($list,$list_invert,$list2,$result0[1]);
	}

	public function getObjectsForKPIs($project_ids = array()) {
		$result = array();

		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				FROM ".$this->getTableName()." A
				WHERE ".$this->getActiveStatusSQL("A");
		if(is_array($project_ids) && count($project_ids)>0) {
			$sql.= " AND ".$this->getIDFieldName()." IN (".implode(",",$project_ids).")";
		} elseif(!is_array($project_ids) && $this->checkIntRef($project_ids)) {
			$sql.= " AND ".$this->getIDFieldName()." = ".$project_ids."";
		}
		$sql.= " ORDER BY ".$this->getNameFieldName().", ".$this->getTableField()."_ref, ".$this->getIDFieldName();
		$result = $this->mysql_fetch_all_by_id($sql, "id");
		return $result;
	}

	public function getActiveItemsFormattedForSelect($ids=array()) {
		$rows = $this->getObjectsForKPIs($ids);
		$data = array();
		if(count($rows)>0) {
			foreach($rows as $i => $r) {
				$data[$i] = $r['name'];
			}
		}
		return $data;
	}

	public function getOrderedObjects($parent_parent_id=0,$parent_id=0,$options=array()){
		if(ASSIST_HELPER::checkIntRef($parent_parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_parent_id." AND ";
		} elseif(ASSIST_HELPER::checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getSecondaryParentFieldName()." = ".$parent_id." AND ";
		} else {
			$sql_from = "WHERE ";
		}
		if(count($options)>0) {
			$sql_from.=$this->convertArrayToSQL($options)." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A")."
				ORDER BY ".$this->getNameFieldName();
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		//send sql back for debugging
		return array($res2,$sql);
	}

	//parent_parent = IDP, parent = expected result
	public function getOrderedObjectsWithChildren($parent_parent_id=0,$parent_id=0){
		/*if($parent_id==0) {
			$parentParentObject = new IDP3_IDP();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getIDFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}*/

		$parentObject = new IDP3_STRATRESULT();
		$parent2Object = new IDP3_STRATGOAL();
		$parent3Object = new IDP3_STRATFOCUS();
		$parent4Object = new IDP3_STRATOBJ();

		$sql_from = "
		INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getSecondaryParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getStatusFieldName()." = ".self::ACTIVE."";
		if($parent_id>0) {
			$sql_from.= " AND B.".$parentObject->getIDFieldName()." = $parent_id";
		}
		$sql_from.= "
		INNER JOIN ".$parent2Object->getTableName()." C ON B.".$parentObject->getParentFieldName()." = C.".$parent2Object->getIDFieldName()."
		INNER JOIN ".$parent3Object->getTableName()." D ON C.".$parent2Object->getParentFieldName()." = D.".$parent3Object->getIDFieldName()."
		INNER JOIN ".$parent4Object->getTableName()." E ON D.".$parent3Object->getParentFieldName()." = E.".$parent4Object->getIDFieldName()." ";
		if($parent_parent_id>0) {
			$sql_from.=" AND E.".$parent4Object->getParentFieldName()." = $parent_parent_id ";
		}

		$sql = "SELECT A.*
				, A.".$this->getIDFieldName()." as id
				, CONCAT('".$this->getRefTag()."',A.".$this->getIDFieldName().") as reftag
				, A.".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from." WHERE
				".$this->getActiveStatusSQL("A")."
				AND (".$parentObject->getActiveStatusSQL("B").")
				AND (".$parent2Object->getActiveStatusSQL("C").")
				AND (".$parent3Object->getActiveStatusSQL("D").")
				AND (".$parent4Object->getActiveStatusSQL("E").")";
		$res2 = $this->mysql_fetch_all_by_id($sql, "id"); //echo $sql;

		$results = array();
		$results[$this->getMyObjectType()] = $res2;

		$keys = count($res2) > 0 ? array_keys($res2) : array(0);

		$incomeObject = new IDP3_PROJECTINCOME();
		$results[$incomeObject->getMyObjectType()] = $incomeObject->getChildObjectsForTable($keys);
		$costsObject = new IDP3_PROJECTCOST();
		$results[$costsObject->getMyObjectType()] = $costsObject->getChildObjectsForTable($keys);
		//$kpiObject = new IDP3_PROJECTKPI();
		//$results[$kpiObject->getMyObjectType()] = $kpiObject->getChildObjectsForTable($keys);

		return $results;
	}

	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'parent_id'=>$obj[$this->getParentFieldName()],
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}

	public function getSegmentDetails($id=0) {
		$obj = $this->getRawObject($id);
		$headObject = new IDP3_HEADINGS();
		$head = $headObject->getMainObjectHeadingsByType($this->getMyObjectType(),array("SEGMENT","MULTISEGMENT"),true);
		$head = $head['rows'];
		$data = array();
		$segObject = new IDP3_SEGMENTS();
		foreach($head as $fld => $h) {
			$tbl = $h['list_table'];
			$segObject->setSection($tbl);
			if($h['type']=="MULTISEGMENT") {
				$obj[$fld] = explode(";",$obj[$fld]);
				if(count($obj[$fld])==0 || in_array(0,$obj[$fld])) {
					$obj[$fld] = 0;
				}
			}
			if($obj[$fld]==0) {
				$data[$h['name']] = $this->getUnspecified();
			} else {
				$data[$h['name']] = $segObject->getNameForAnObject($obj[$fld]);
			}
		}
		return $data;
	}

	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}

	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>