<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3_IDP extends IDP3 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = false;
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $finyear_field = "_finyear_id";

	protected $has_attachment = false;

	protected $financial_year_fields = array();

    protected $ref_tag = "IDP";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "IDP";
    const OBJECT_NAME = "idp";
	const PARENT_OBJECT_TYPE = false;

    const TABLE = "idp";
    const TABLE_FLD = "idp";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

	/**
	 * Status Constants
	 */
    const CONFIRMED = 32;
	const ACTIVATED = 64;

	const CONFIRMATION_REQUESTED = 256;

    public function __construct($obj_id=0,$modref="",$external=false,$set_object_names_for_external=false) {
        parent::__construct($modref,$external,$set_object_names_for_external);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		//$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->financial_year_fields[] = IDP3_IDPYEARS::TABLE_FLD.$this->finyear_field;
		$this->finyear_field = self::TABLE_FLD.$this->finyear_field;
		$this->financial_year_fields[] = $this->finyear_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }


	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			//check for unnecessary auto-complete fields
				if($this->isThisAnExtraAutoCompleteFieldName($key)==true) {
					unset($var[$key]);
				}
		}
		$var[$this->getTableField().'_status'] = IDP3::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		//remove FORECAST YEARs & prep for adding later
		$financial_year = $var['idp_finyear_id'];
		$forecast_years = $var['iy_finyear_id'];
		unset($var['iy_finyear_id']);

		//[JC] removed - not sure if required... do we need to force more than the primary financial year?
		//Validate that forecast years have been set
		/*$all_years = array($financial_year);
		$add = false;
		$id = 0;
		foreach($forecast_years as $fy) {
			if(!in_array($fy,$all_years)) {
				$all_years[] = $fy;
			}
		}
		if(count($all_years)>0) {
			$add = true;
		}*/


		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			//Add FORECAST YEARS
			$iyObject = new IDP3_IDPYEARS();
			//Add primary financial year
			$order = 0;
			$iyObject->addObjectById($id,$financial_year,$order);
			$order++;
			$done_years = array($financial_year);
			//add any additional forecast years that aren't the financial year
			foreach($forecast_years as $fy) {
				if($this->checkIntRef($fy) && !in_array($fy,$done_years)) {
					$iyObject->addObjectByID($id,$fy,$order);
					$order++;
				}
			}
			unset($iyObject);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				//1=>"all ok",
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
			);
			//return array("error","nothing done".serialize($result));
				//'log_var'=>$log_var
			return $result;
		}
		return array("error","Testing: ".$sql);
	}






	public function editObject($var,$attach=array()) {
		$forecast = $var['iy_finyear_id'];
		$object_id = $var['object_id'];
		unset($var['iy_finyear_id']);

		$finyear_id = $var[$this->getFinYearField()];
		$iyObject = new IDP3_IDPYEARS();
		$old_iy_years = $iyObject->getRawYearsForSpecificIDP($object_id,false,false);

		$new_iy_years = array_merge(array($finyear_id),$forecast);
		//test for dupplicates - assume first selection is correct
		foreach($new_iy_years as $key => $value) {
			if(!$this->checkIntRef($value)) {
				//auto unset any value that is 0/X - to take into account any forecast years that have been set to "Unspecfied" / "--- SELECT ---"
				unset($new_iy_years[$key]);
			} else {
				$test = $new_iy_years;
				unset($test[$key]);
				//can't use array_flip here because it will only pick up 1 duplicate and we need to check for multiple to be thorough so do it long-hand
				if(in_array($value, $test)) {
					foreach($test as $t => $tval) {
						if($tval==$value) {
							unset($new_iy_years[$t]);
						}
					}
				}
			}
		}
		//rebase the index of the new_iy_years array so that it can be used as sort
		$new_iy_years = array_values($new_iy_years);
		$new_iy_years_flipped = array_flip($new_iy_years);

		$old_iy_years_flipped = array();
		//evaluate old iy_years to check for changes
		$changes_found = 0;
		$changes = array();
		foreach($old_iy_years as $iy_id => $old_row) {
			$test_fy_id = $old_row['fy_id'];
			$old_status = $old_row['status'];
			$old_iy_years_flipped[$test_fy_id] = $iy_id;
			//not present in new array - set record to delete if not deleted
			if(!in_array($test_fy_id,$new_iy_years)) {
				if($old_status == self::ACTIVE) {
					//call iyObject to delete
					$iyObject->deactivateObject(array('object_id'=>$iy_id),false);
					$changes_found++;
					$changes[$test_fy_id] = "DELETED@".$test_fy_id;
				}
			} else {
				$old_order = $old_row['display_order'];
				$new_order = $new_iy_years_flipped[$test_fy_id];
				if($old_order!=$new_order) {
					if($old_status != self::ACTIVE) {
						//call iyObject to change order & reactivate
						$iyObject->restoreAndRearrangeObject(array('object_id'=>$iy_id,'display_order'=>$new_order),false);
						$changes[$test_fy_id] = "R&R@".$old_status;
						$changes_found++;
					} else {
						//call iyObject to change order
						$iyObject->rearrangeObject(array('object_id'=>$iy_id,'display_order'=>$new_order),false);
						$changes[$test_fy_id] = "MOVED-".$new_order;
						$changes_found++;
					}
				} elseif($old_status != self::ACTIVE) {
					//call iyObject to reactivate
					$iyObject->restoreObject(array('object_id'=>$iy_id),false);
					$changes[$test_fy_id] = "RESTORED";
					$changes_found++;
				} else {
					//do nothing, nothing has changed
				}
			}
		}
		//evaluate new_iy_years for additions
		foreach($new_iy_years as $sort => $new_fy_id) {
			//if new fy_id is not in the existing iy_years evaluated above, then add
			if(!isset($old_iy_years_flipped[$new_fy_id])) {
				//call iyObject to add new object
				$iyObject->addObjectByID($object_id, $new_fy_id, $sort);
				$changes[$test_fy_id] = "ADDED";
				$changes_found++;
			}
		}
		//if changes were made, create log
		$extra_logs = array();
		if($changes_found>0) {
			$ids = $new_iy_years+array_keys($old_iy_years_flipped);
			$listObject = new IDP3_MASTER('financial_years');
			$list_items = $listObject->getActiveItemsFormattedForSelect($ids);
			$new = array();

			foreach($new_iy_years as $n) {
				$new[] = $list_items[$n];
			}
			unset($new[0]);	//for purposes of logging, remove financial year duplicate
			$old = array();
			foreach($old_iy_years_flipped as $o => $y) {
				$old[] = $list_items[$o];
			}
			unset($old[0]);	//for purposes of logging, remove financial year duplicate
			$extra_logs = array(
				'iy_finyear_id'=>array(
					'to'=>implode("; ",$new),
					'from'=>implode("; ",$old),
					'raw'=>array(
						'to'=>$new_iy_years,
						'from'=>array_flip($old_iy_years_flipped)
					),
				)
			);
		}



		$result = $this->editMyObject($var,$attach,$extra_logs);
//		$result = array("info","Oops, I don't know what you want me to do with this information..?");



//$result[1].="<br />".serialize($changes);

		return $result;
	}

	public function requestConfirmation($var) {
		$object_id = $var['object_id'];
		$notify_users = $var['req_notify'];

		$old = $this->getRawObject($object_id);

		if($old!==false && is_array($old) && count($old)>0) {
			if(($old[$this->getStatusFieldName()] & self::CONFIRMATION_REQUESTED)!=self::CONFIRMATION_REQUESTED) {
				$sql = "UPDATE ".$this->getTableName()."
						SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()."+".self::CONFIRMATION_REQUESTED.")
						WHERE ".$this->getIDFieldName()." = ".$object_id."
						";
				$mnr = $this->db_update($sql);
			}
				$result = array(
					0=>"ok",
					1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." has been sent for Confirmation.",
					'object_id'=>$object_id,
				);
				if(count($notify_users)>0) {
					$recipients = $this->getUserNameWithEmail($notify_users);
					$subject = $this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." awaiting Confirmation.";
$message = "Good day

".$this->getUserName()." has submitted ".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." for your review and confirmation.

Please log onto ".$this->getSiteName()." to view this ".$this->getObjectName($this->getMyObjectName())."

Kind regards
".$this->getSiteName()."

This is an automated email.  Please do not reply to this email.
";
					$emailObject = new ASSIST_EMAIL();
					$emailObject->setSubject($subject);
					$emailObject->setBody($message);
					$emailObject->setModRef($this->getModRef());
					$emailObject->setSender(array('email'=>"",'name'=>$this->getSiteName()));
					$log_recipients = array();
					foreach($recipients as $tki => $tk) {
						$emailObject->setRecipientUserID($tki);
						$to = array('name'=>$tk['tkn'], 'email'=>$tk['tkemail']);
						$log_recipients[] = $tk['tkn'];
						$emailObject->setRecipient($to);
						$emailObject->sendEmail();
					}
					if(count($log_recipients)>0) {
						$changes = array(
							'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." has been sent for Confirmation.  The following users were notifed: ".implode(", ",$log_recipients),
							'user'=>$this->getUserName(),
						);
					} else {
						$changes = array(
							'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." has been sent for Confirmation.  No users were found to be notified. ",
							'user'=>$this->getUserName(),
						);
					}
					$log_var = array(
						'object_id'		=> $object_id,
						'object_type'	=> $this->getMyObjectType(),
						'changes'		=> $changes,
						'log_type'		=> IDP3_LOG::REQUESTCONFIRM,
					);
					$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				} else {
					$changes = array(
						'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." has been sent for Confirmation.  No users were found to be notified. ",
						'user'=>$this->getUserName(),
					);
					$log_var = array(
						'object_id'		=> $object_id,
						'object_type'	=> $this->getMyObjectType(),
						'changes'		=> $changes,
						'log_type'		=> IDP3_LOG::REQUESTCONFIRM,
					);
					$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				}
				return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." has been sent for Confirmation.");
		//	} else {
		//		return array("error",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." has already been sent for Confirmation.");
		//	}
		} else {
			return array("error",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$object_id." could not be found.  Please reload the page and try again.");
		}
		return array("error","Sorry, I couldn't complete the requested action.  Please try again or contact your Administrator.");
	}


	public function confirmObject($var) {
		//$result = array("error","Confirmation functionality pending");

		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE + self::CONFIRMED;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$obj_id." confirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::CONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully confirmed.",
				'object_id'=>$obj_id,
			);


		return $result;
	}



	public function undoConfirmObject($var) {
//		$result = array("error","Confirmation functionality pending");
		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$obj_id." unconfirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::UNDOCONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully unconfirmed.",
				'object_id'=>$obj_id,
			);
		/*
		*/

		return $result;
	}


	public function activateObject($var) {
		//$result = array("error","Confirmation functionality pending");

		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status + self::ACTIVATED;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$obj_id." activated.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::ACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully activated.",
				'object_id'=>$obj_id,
			);


		return $result;
	}


	public function undoActivateObject($var) {
		//$result = array("error","Confirmation functionality pending");

		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status - self::ACTIVATED;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$obj_id." activation reversed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::UNDOACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"Reversal of prior activation of ".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." successfull.",
				'object_id'=>$obj_id,
			);


		return $result;
	}
    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
	public function getFinYearField() { return $this->finyear_field; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getFinYearFields() { return $this->financial_year_fields; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}


	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		//get headings
		$headingObject = new IDP3_HEADINGS();
		$headings = $headingObject->getMainObjectHeadings($this->getMyObjectType(),"DETAILS","NEW");
		//get raw idp record from database
		$raw = $this->getRawObject($id);
		$raw[$this->getIDFieldName()] = $this->getRefTag().$raw[$this->getIDFieldName()];
		//get financial years
		$finyearObject = new IDP3_MASTER("financial_year");
		$finyears = $finyearObject->getAllItemsFormattedForSelect();
		//replace all financial years with text
		$finyear_id = $raw[$this->getFinYearField()];
		$raw[$this->getFinYearField()] = $finyears[$raw[$this->getFinYearField()]];
		//get idp forecast years
		$iyObject = new IDP3_IDPYEARS();
		$iy_years = $iyObject->getYearsForSpecificIDP($id);
		unset($iy_years[$finyear_id]);
		$raw['iy_finyear_id'] = implode("<br />",$iy_years);
		//get financial systems
		$listObject = new IDP3_LIST("finsys");
		if(!isset($raw[$this->getTableField().'_finsys_id']) || !$this->checkIntRef($raw[$this->getTableField().'_finsys_id'])) {
			$raw[$this->getTableField().'_finsys_id'] = $this->getUnspecified();
		} else {
			$fs_id = $raw[$this->getTableField().'_finsys_id'];
			$fs = $listObject->getAListItem($fs_id);
			$raw[$this->getTableField().'_finsys_id'] = $fs['name'];
		}
		//format into expected return format
		$data = array(
			'head'=>$headings['rows'],
			'rows'=>$raw,
		);
		return $data;
	}

	public function getHeading($id) {
		$row = $this->getRawObject($id);
		return $row[$this->getNameFieldName()].(strlen($row[$this->getTableField()."_ref"])>0 ? " (".$row[$this->getTableField()."_ref"].")":"");
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	public function getARawObjectFromController($var) {
		$obj_id = $var['object_id'];
		$data = $this->getRawObject($obj_id);
		$finyear_id = $data[$this->getFinYearField()];
		$iyObject = new IDP3_IDPYEARS();
		$iy_years = $iyObject->getRawYearsForSpecificIDP($obj_id);
		//unset($iy_years[$finyear_id]);
		$data['iy_finyear_id'] = $iy_years;

		return $data;
	}



	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		if($parent_id==0) {
			$parentObject = new IDP3_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}


//called by IDP3_EXTERNAL for use in SDBP6 module
	public function getListOfObjectsFormattedForSelect($active=true,$activated=true,$include_financial_year=false,$include_mscoa_version_in_name=false) {
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getTableField()."_ref as internal_ref
				, ".$this->getTableField()."_finyear_id as financial_year
				, ".$this->getTableField()."_mscoa_version_id as mscoa_version
				FROM ".$this->getTableName()." A
				WHERE ";
				if($activated) {
					$sql.=$this->getReportingStatusSQL("A");
				} elseif($active) {
					$sql.=$this->getActiveStatusSQL("A");
				}
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		$data = array();
		foreach($rows as $id => $row) {
			if($include_financial_year) {
				$data[$id] = $row;
			} else {
				$data[$id] = $row['name'];
			}
		}
		return $data;
	}


	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Actions are fully activated & available outside of NEW
	 */
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW / ACTIVATE / WAITING
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATE",$t,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>