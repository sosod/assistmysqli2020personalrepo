<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3_PMKPI extends IDP3 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_prog_id";
	protected $secondary_parent_field = "_expresult_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	protected $target_fields = array(
		"_baseline",
	);

	protected $has_attachment = false;

    protected $ref_tag = "IDP/KPI";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PMKPI";
    const OBJECT_NAME = "kpi";
	const PARENT_OBJECT_TYPE = "PMPROG";

    const TABLE = "pm_kpi";
    const TABLE_FLD = "kpi";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

    public function __construct($obj_id=0,$modref="",$external=false) {
        parent::__construct($modref,$external);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		foreach($this->target_fields as $key => $fld) {
			$this->target_fields[$key] = self::TABLE_FLD.$fld;
		}
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }


	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['object_id']); //remove incorrect field value from add form

		$projects = $var['kp_proj_id'];
		unset($var['kp_proj_id']);
		$targets = array();
		$targets['original'] = $var['pky_original'];
		unset($var['pky_original']);
		$targets['adjustment'] = $var['pky_adjustment'];
		unset($var['pky_adjustment']);

		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
			//check for unnecessary auto-complete fields
			if($this->isThisAnExtraAutoCompleteFieldName($key)==true) {
				unset($var[$key]);
			}
		}
		$var[$this->getTableField().'_status'] = IDP3::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			/**
			 * Make Indicator Code
			 */
			//Get Indicator Object
			$iObject = new IDP3_SETUP_INDICATOR();
			//Get STRATEGY Tree Details
				$resultObject = new IDP3_STRATRESULT();
				$result = $resultObject->getRawObject($var[$this->getSecondaryParentFieldName()]);
				$goalObject = new IDP3_STRATGOAL();
				$goal = $goalObject->getRawObject($result[$resultObject->getParentFieldName()]);
				$focusObject = new IDP3_STRATFOCUS();
				$focus = $focusObject->getRawObject($goal[$goalObject->getParentFieldName()]);
				$objObject = new IDP3_STRATOBJ();
				$obj = $objObject->getRawObject($focus[$focusObject->getParentFieldName()]);
				$idpObject = new IDP3_IDP();
				$idp = $idpObject->getRawObject($obj[$objObject->getParentFieldName()]);
				$strategy = $idp+$obj+$focus+$goal+$result;
			//Get PM Tree Details
				$progObject = new IDP3_PMPROG();
				$prog = $progObject->getRawObject($var[$this->getParentFieldName()]);
				$kpaObject = new IDP3_PMKPA();
				$kpa = $kpaObject->getRawObject($prog[$progObject->getParentFieldName()]);
				$perfmeasures = $kpa+$prog;
			//Get KPI details
				$kpi = $var;
				$kpi['kpi_id'] = $id;
			$projects = ASSIST_HELPER::removeBlanksFromArray($projects);
			if(count($projects)>0) {
				$pObject = new IDP3_PMKPI_PROJECT();
				$projObject = new IDP3_PROJECT();
				//Get project details
				$projs = $projObject->getRawObject($projects);
				foreach($projects as $pi) {
					//Make Indicator Code
					$proj = array();
					$proj['proj_id'] = $pi;
					$proj['proj_ref'] = $projs[$pi]['proj_ref'];
					$indi = $iObject->generateIndicatorCode($strategy,$perfmeasures,$kpi,$proj);
					$pObject->addObject(array(),$id,$pi,$indi);
				}
			}

			$tObject = new IDP3_PMKPI_TARGET();
			foreach($targets['original'] as $yi => $original) {
				$adjustment = $targets['adjustment'][$yi];
				$tObject->addObject(array(),$id,$yi,$original,$adjustment);
			}

			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}



	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		$extra_logs = array();
		if(isset($var['kp_proj_id'])) {
			$projects = $var['kp_proj_id'];
			unset($var['kp_proj_id']);
		} else {
			$projects = array();
		}
		$targets = array();
		$targets['original'] = $var['pky_original'];
		unset($var['pky_original']);
		$targets['adjustment'] = $var['pky_adjustment'];
		unset($var['pky_adjustment']);

		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}

		$old = $this->getRawObject($object_id);

			/**
			 * Make Indicator Code
			 */
			//Get Indicator Object
			$iObject = new IDP3_SETUP_INDICATOR();
			//Get STRATEGY Tree Details
				$resultObject = new IDP3_STRATRESULT();
				$result = $resultObject->getRawObject($var[$this->getSecondaryParentFieldName()]);
				$goalObject = new IDP3_STRATGOAL();
				$goal = $goalObject->getRawObject($result[$resultObject->getParentFieldName()]);
				$focusObject = new IDP3_STRATFOCUS();
				$focus = $focusObject->getRawObject($goal[$goalObject->getParentFieldName()]);
				$objObject = new IDP3_STRATOBJ();
				$obj = $objObject->getRawObject($focus[$focusObject->getParentFieldName()]);
				$idpObject = new IDP3_IDP();
				$idp = $idpObject->getRawObject($obj[$objObject->getParentFieldName()]);
				$strategy = $idp+$obj+$focus+$goal+$result;
			//Get PM Tree Details
				$progObject = new IDP3_PMPROG();
				$prog = $progObject->getRawObject($old[$this->getParentFieldName()]);
				$kpaObject = new IDP3_PMKPA();
				$kpa = $kpaObject->getRawObject($prog[$progObject->getParentFieldName()]);
				$perfmeasures = $kpa+$prog;
			//Get KPI details
				$kpi = $var;
				$kpi['kpi_id'] = $object_id;


		$projects = ASSIST_HELPER::removeBlanksFromArray($projects);
		$pObject = new IDP3_PMKPI_PROJECT();
		$p_old = $pObject->getRawObjectGroupedByParent($object_id);
		$p_old = isset($p_old[$object_id]) ? $p_old[$object_id] : array();

		$same = array();
		$new = array();
		$removed = array();
		foreach($projects as $pi) {
			if(isset($p_old[$pi])) {
				$same[$pi] = $pi;
				unset($p_old[$pi]);
			} else {
				$new[$pi] = $pi;
			}
		}
		if(count($p_old)>0) { $removed = $p_old; }
		$projObject = new IDP3_PROJECT();
		$projs = $projObject->getRawObject($new);
		foreach($new as $pi) {
			//Make Indicator Code
			$proj = array();
			$proj['proj_id'] = $pi;
			$proj['proj_ref'] = $projs[$pi]['proj_ref'];
			$indi = $iObject->generateIndicatorCode($strategy,$perfmeasures,$kpi,$proj);
			$pObject->addObject(array(),$object_id,$pi,$indi);
			$extra_logs['kp_proj_id'][$pi] = "Added link to |".$pObject->getMyObjectName()."| ".$pObject->getRefTag().$pi;
		}
		foreach($removed as $pi) {
			$pObject->deleteObject(array(),$object_id,$pi);
			$extra_logs['kp_proj_id'][$pi] = "Removed link to |".$pObject->getMyObjectName()."| ".$pObject->getRefTag().$pi;
		}


		$tObject = new IDP3_PMKPI_TARGET();
		$t_old = $tObject->getRawObjectGroupedByParent($object_id);
		$t_old = $t_old[$object_id];
		foreach($targets['original'] as $yi => $original) {
			$adjustment = $targets['adjustment'][$yi];
			if(!isset($t_old[$yi])) {
				//If new forecast year then add
				$tObject->addObject(array(),$object_id,$yi,$original,$adjustment);
				$extra_logs['pky_kpi_id'][$yi] = "Added target for |future| ".$this->getSpecificForecastYearName($yi);
			} else {
				$old = $t_old[$yi];
				$old_original = $old[$tObject->getNameFieldName()];
				$old_adjustment = $old[$tObject->getSecondaryNameFieldName()];
				//If original or adjustment has changed then edit
				if(($old_original*1)!=($original*1) || (($old_adjustment*1)!=($adjustment*1))) {
					$tObject->editObject(array(),$object_id,$yi,$original,$adjustment);
					$extra_logs['pky_kpi_id'][$yi][$tObject->getNameFieldName()] = array('to'=>$original,'from'=>$old_original);
					$extra_logs['pky_kpi_id'][$yi][$tObject->getSecondaryNameFieldName()] = array('to'=>$adjustment,'from'=>$old_adjustment);
				}
			}
		}


/*		$at_old = $at_old[$object_id];
		foreach($activity_target as $ti => $at) {
			$old = $at_old[$ti];
			if($at!=$old) {
				$edit_var = array(
					'parent'=>$object_id,
					'secondary'=> $ti,
					'value'=>$at,
				);
				$atObject->editObject($edit_var);
				$extra_logs['at_id'][$ti] = array('to'=>$at,'from'=>$old);
			}
		}

*/



		$result = $this->editMyObject($var,$attach,$extra_logs);
		return $result;
	}



	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP3_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
			);
			return $result;
	}



    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getTargetTypeField() { return $this->getTableField()."_unit_id"; }
	public function isTargetNumberField($fld) {
		return in_array($fld,$this->target_fields);
	}

    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}


	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);

		$pObject = new IDP3_PMKPI_PROJECT();
		$p_rows = $pObject->getRawObjectGroupedByParent($obj_id,true);
		//$this->arrPrint($p_rows);
		$data[$pObject->getSecondaryParentFieldName()] = isset($p_rows['proj_id'][$obj_id]) ? $p_rows['proj_id'][$obj_id] : array();
		$data[$pObject->getTableField()."_indicator"] = isset($p_rows['indicator'][$obj_id]) ? $p_rows['indicator'][$obj_id] : array();

		$tObject = new IDP3_PMKPI_TARGET();
		$t_rows = $tObject->getRawObjectGroupedByParent($obj_id);
		$data[$tObject->getParentFieldName()] = isset($t_rows[$obj_id]) ? $t_rows[$obj_id] : array();
		//$this->arrPrint($data);
		return $data;
	}

	public function getRawObjectsForExternalModule($idp_id) {
		$my_table = $this->getTableName();
		$my_id_field = $this->getIDFieldName();
		$my_parent_field = $this->getParentFieldName();
		$progObject = new IDP3_PMPROG(0,$this->local_modref,$this->called_from_external_module);
		$parent_table = $progObject->getTableName();
		$parent_id_field = $progObject->getIDFieldName();
		$parent_parent_field = $progObject->getParentFieldName();
		$kpaObject = new IDP3_PMKPA(0,$this->local_modref,$this->called_from_external_module);
		$grand_table = $kpaObject->getTableName();
		$grand_id_field = $kpaObject->getIDFieldName();
		$grand_parent_field = $kpaObject->getParentFieldName();

		$sql = "SELECT ME.* FROM ".$my_table." ME 
				INNER JOIN ".$parent_table." P
				ON P.".$parent_id_field." = ME.".$my_parent_field."
				INNER JOIN ".$grand_table." G
				ON G.".$grand_id_field." = P.".$parent_parent_field."
				WHERE (".$this->getActiveStatusSQL("ME").") 
				AND (".$progObject->getActiveStatusSQL("P").") 
				AND (".$kpaObject->getActiveStatusSQL("G").") 
				AND ".$grand_parent_field." = ".$idp_id;
		$rows = $this->mysql_fetch_all_by_id($sql,$my_id_field);

		//kp_proj_id
		$kpObject = new IDP3_PMKPI_PROJECT(0,$this->local_modref,$this->called_from_external_module);
		$kp_proj = $kpObject->getRawObjectGroupedByParent(array_keys($rows));

		foreach($rows as $key => $r) {
			$rows[$key]['kp_proj_id'] = "";
			if(isset($kp_proj[$key])) {
				$rows[$key]['kp_proj_id'] = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$kp_proj[$key]);
			}
		}

		return $rows;

	}

//parent3 = IDP; parent_parent = PMKPA; parent = PMPROG
/*	public function getOrderedObjects($parent3 = 0, $parent_parent_id=0, $parent_id=0){
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new IDP3_PMPROG();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$parent2Object = new IDP3_PMKPA();
				$sql_from.= "INNER JOIN ".$parent2Object->getTableName()." C
							  ON B.".$parentObject->getParentFieldName()." = C.".$parent2Object->getIDFieldName()." ";
				if($this->checkIntRef($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." = ".$parent3." WHERE ";
				}
			}
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");

		return $res2;
	}
*/





	/**
	 * Get list of active objects limited to specific IDP ready to populate a SELECT element
	 */
	public function getLimitedActiveObjectsFormattedForSelect($options=array()) { //$this->arrPrint($options);
		$rows = $this->getOrderedObjects($options['IDP']);
		//$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($rows);
		$items = array();
		foreach($rows as $i => $r) {
			foreach($r as $i2 => $r2) {
				$items[$r2['id']] = $r2['name'];
			}
		}
		asort($items);
		return $items;
		//return $items;
	}

//parent3 = IDP; parent_parent = PMKPA; parent = PMPROG
	public function getOrderedObjects($parent3 = 0, $parent_parent_id=0, $parent_id=0){
		$sql_status = $this->getActiveStatusSQL("A");
		$err = false;
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if(is_array($parent_id)) {
			if(count($parent_id)>0) {
				$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id).") AND ";
			} else {
				$err = true;
			}
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new IDP3_PMPROG();
			$sql_status.=" AND ".$parentObject->getActiveStatusSQL("B");
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if(is_array($parent_parent_id)) {
				if(count($parent_parent_id)>0) {
					$sql_from.=" AND B.".$parentObject->getParentFieldName()." IN (".implode(",",$parent_parent_id).") WHERE ";
				} else {
					$err = true;
				}
			} elseif($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$parent2Object = new IDP3_PMKPA();
				$sql_status.=" AND ".$parent2Object->getActiveStatusSQL("C");
				$sql_from.= "INNER JOIN ".$parent2Object->getTableName()." C
							  ON B.".$parentObject->getParentFieldName()." = C.".$parent2Object->getIDFieldName()." ";
				if(is_array($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." IN (".implode(",",$parent3).") WHERE ";
				} elseif($this->checkIntRef($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." = ".$parent3." WHERE ";
				}
			}
		}
		if(!$err) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$sql_status;
			$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		} else {
			$res2 = array();
		}
		//echo $sql;
		return $res2;
	}




//parent3 = IDP; parent_parent = PMKPA; parent = PMPROG
	public function getRawOrderedObjects($parent3 = 0, $parent_parent_id=0, $parent_id=0){
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if(is_array($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id).") AND ";
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new IDP3_PMPROG();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if(is_array($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." IN (".implode(",",$parent_parent_id).") WHERE ";
			} elseif($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$parent2Object = new IDP3_PMKPA();
				$sql_from.= "INNER JOIN ".$parent2Object->getTableName()." C
							  ON B.".$parentObject->getParentFieldName()." = C.".$parent2Object->getIDFieldName()." ";
				if(is_array($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." IN (".implode(",",$parent3).") WHERE ";
				} elseif($this->checkIntRef($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." = ".$parent3." WHERE ";
				}
			}
		}
		$sql = "SELECT *
				, ".$this->getIDFieldName()." as id
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");

		$sql = "SELECT ".$this->getIDFieldName()." as id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$ids = $this->mysql_fetch_all_by_value($sql,"id");

		if(count($ids)>0) {
			$sql = "SELECT kp_kpi_id as kpi_id, kp_proj_id as proj_id, kp_indicator as indicator_code FROM ".$this->getTableName()."_project WHERE kp_kpi_id IN (".implode(",",$ids).") AND kp_status = ".self::ACTIVE;
			$all_ids = $this->mysql_fetch_all_by_id2($sql, "kpi_id","proj_id");
			$proj_ids = array();
			foreach($all_ids as $kpi_id => $p_ids) {
				foreach($p_ids as $i => $p) {
					if(!in_array($i,$proj_ids)) {
						$proj_ids[] = $i;
					}
				}
			}

			$projObject = new IDP3_PROJECT();
			$proj = $projObject->getObjectsForKPIs($proj_ids);

			foreach($res2 as $parent_id => $kpis_array) {
				foreach($kpis_array as $kpi_id => $row) {
					$res2[$parent_id][$kpi_id]['kp_proj_id'] = array();
					$kp_proj_ids = isset($all_ids[$kpi_id]) ? $all_ids[$kpi_id] : array();
					foreach($kp_proj_ids as $p => $r) {
						$res2[$parent_id][$kpi_id]['kp_proj_id'][$p] = $proj[$p]['name']." [".$proj[$p]['reftag']."]";
						$res2[$parent_id][$kpi_id]['kp_indicator_code'][$p] = $r['indicator_code'];
					}
				}
			}
		}




		return $res2;
	}




	public function getIDPidFromParentID($obj_id) {


		$parentObject = new IDP3_PMPROG();
		$grandParentObject = new IDP3_PMKPA();
		$sql_from = "INNER JOIN ".$grandParentObject->getTableName()." B
					  ON A.".$parentObject->getParentFieldName()." = B.".$grandParentObject->getIDFieldName()."
					  WHERE ".$parentObject->getIDFieldName()." = ".$obj_id." AND ";
		$sql_status = $grandParentObject->getActiveStatusSQL("B")." AND ".$parentObject->getActiveStatusSQL("A");

		$sql = "SELECT ".$grandParentObject->getParentFieldName()." as parent_id
				FROM ".$parentObject->getTableName()." A
				".$sql_from."  ".$sql_status;
		$res2 = $this->mysql_fetch_one($sql);
		return $res2['parent_id'];

	}

	public function getIDPidFromMyID($obj_id) {
		$row = $this->getRawObject($obj_id);
		$parent_id = $row[$this->getParentFieldName()];
		$idp_id = $this->getIDPidFromParentID($parent_id);
		return $idp_id;
	}

	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}

	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>