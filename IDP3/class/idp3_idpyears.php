<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class IDP3_IDPYEARS extends IDP3 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_idp_id";
	protected $name_field = "_finyear_id";
	protected $attachment_field = "_attachment";

	protected $has_attachment = false;

	private $parentObject;

    protected $ref_tag = "IDP/IY";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "IDPYEAR";
    const OBJECT_NAME = "forecastyear";
	const PARENT_OBJECT_TYPE = "IDP";

    const TABLE = "idp_year";
    const TABLE_FLD = "iy";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

	/**
	 * Status Constants
	 */
    const CONFIRMED = 32;
	const ACTIVATED = 64;

    public function __construct($obj_id=0) {
        parent::__construct();
		$this->parentObject = new IDP3_IDP();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }


	/********************************
	 * CONTROLLER functions
	 */
	/**
	 * Add new financial / forecast year to an IDP
	 */
	public function addObject($var,$add_log=true) {

		$parent_id = $var[$this->getParentFieldName()];

		$var[$this->getTableField().'_status'] = IDP3::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			//log not required for IDP EDIT function
			if($add_log) {
				$changes = array(
					'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." added to |".$this->parentObject->getMyObjectName()."| ".$this->parentObject->getRefTag().$parent_id.".",
					'user'=>$this->getUserName(),
				);
				$log_var = array(
					'object_id'		=> $id,
					'object_type'	=> $this->getMyObjectType(),
					'changes'		=> $changes,
					'log_type'		=> IDP3_LOG::CREATE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				/*$result = array(
					0=>"ok",
					1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
					'object_id'=>$id,
					'log_var'=>$log_var
				);*/
				//return $result;
			}
			return true;
		}
		//return array("error","Testing: ".$sql);
		return false;
	}

	/**
	 * Deactivate financial / forecast year to an IDP
	 */
	public function deactivateObject($var,$add_log=true) {

		$object_id = $var['object_id'];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($id>0) {
			//log not required for IDP EDIT function
			if($add_log) {
				$changes = array(
					'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deactivated.",
					'user'=>$this->getUserName(),
				);
				$log_var = array(
					'object_id'		=> $id,
					'object_type'	=> $this->getMyObjectType(),
					'changes'		=> $changes,
					'log_type'		=> IDP3_LOG::DEACTIVATE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				/*$result = array(
					0=>"ok",
					1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
					'object_id'=>$id,
					'log_var'=>$log_var
				);*/
				//return $result;
			}
			return true;
		}
		//return array("error","Testing: ".$sql);
		return false;
	}


	/**
	 * Deactivate financial / forecast year to an IDP
	 */
	public function restoreObject($var,$add_log=true) {

		$object_id = $var['object_id'];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($id>0) {
			//log not required for IDP EDIT function
			if($add_log) {
				$changes = array(
					'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." restored.",
					'user'=>$this->getUserName(),
				);
				$log_var = array(
					'object_id'		=> $id,
					'object_type'	=> $this->getMyObjectType(),
					'changes'		=> $changes,
					'log_type'		=> IDP3_LOG::RESTORE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
				/*$result = array(
					0=>"ok",
					1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
					'object_id'=>$id,
					'log_var'=>$log_var
				);*/
				//return $result;
			}
			return true;
		}
		//return array("error","Testing: ".$sql);
		return false;
	}

	public function restoreAndRearrangeObject($var,$add_log=true) {
		$this->restoreObject($var,$add_log);
		$this->rearrangeObject($var,$add_log);
	}

	/**
	 * Rearrange financial / forecast year to an IDP
	 */
	public function rearrangeObject($var,$add_log=true) {

		$object_id = $var['object_id'];
		$display_order = $var['display_order'];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getTableField()."_order = ".$display_order." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($id>0) {
			//log not required for IDP EDIT function
			if($add_log) {
			}
			return true;
		}
		//return array("error","Testing: ".$sql);
		return false;
	}




	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach);
		return $result;
	}



	/***********************
	 * Management functions from IDP class
	 */
	public function addObjectByID($idp_id,$finyear_id,$order,$add_log=true) {
		$var = array(
			$this->getNameFieldName()=>$finyear_id,
			$this->getParentFieldName()=>$idp_id,
			$this->getTableField().'_order'=>$order,
		);
		$this->addObject($var);
	}


    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}

	public function getYearsForSpecificIDP($id) {
			$fld = "FY";
			$masterObject = new IDP3_MASTER("financial_years");
			$fy = $masterObject->getFields();
			$sql_select= "".$fld.".".$fy['name']." as name";
			$table_join = "INNER JOIN ".$fy['table']." AS ".$fld." ON IY.".$this->getNameFieldName()." = ".$fld.".".$fy['id'];

			$sql = "SELECT ".$this->getIDFieldName()." as id
					,  $sql_select
					FROM ".$this->getTableName()." IY
					$table_join
					WHERE ".$this->getParentFieldName()." = ".$id."
					AND ".$this->getActiveStatusSQL("IY")."
					ORDER BY ".$this->getParentFieldName().", ".$fy['sort'];
			$rows = $this->mysql_fetch_value_by_id($sql,"id","name");
			/*$data = array();
			foreach($rows as $key => $years) {
				if($html) { $glue = "<br />"; } else { $glue = ", "; }
				$data[$key] = implode($glue,$years);
			}
			return $data;*/
			return $rows;

	}

	public function getRawYearsForSpecificIDP($id,$active_only=true,$value_only=true) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					,  ".$this->getNameFieldName()." as fy_id
					,  ".$this->getStatusFieldName()." as status
					,  ".$this->getTableField()."_order as display_order
					FROM ".$this->getTableName()." IY
					WHERE ".$this->getParentFieldName()." = ".$id."
					".($active_only?"
					AND ".$this->getActiveStatusSQL("IY"):"");
			if($value_only) {
				$rows = $this->mysql_fetch_value_by_id($sql,"id","fy_id");
			} else {
				$rows = $this->mysql_fetch_all_by_id($sql,"id");
			}
			return $rows;
	}


	public function getSpecificYear($id) {
			$fld = "FY";
			$masterObject = new IDP3_MASTER("financial_years");
			$fy = $masterObject->getFields();
			$sql_select= "".$fld.".".$fy['name']." as name";
			$table_join = "INNER JOIN ".$fy['table']." AS ".$fld." ON IY.".$this->getNameFieldName()." = ".$fld.".".$fy['id'];

			$sql = "SELECT ".$this->getIDFieldName()." as id
					,  $sql_select
					FROM ".$this->getTableName()." IY
					$table_join
					WHERE ".$this->getIDFieldName()." = ".$id."
					AND ".$this->getActiveStatusSQL("IY");
			$rows = $this->mysql_fetch_one($sql);
			/*$data = array();
			foreach($rows as $key => $years) {
				if($html) { $glue = "<br />"; } else { $glue = ", "; }
				$data[$key] = implode($glue,$years);
			}
			return $data;*/
			return $rows['name'];

	}

	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}


	public function getRowsForParentListTable($keys=array(),$html = true) {
		if(count($keys)>0) {
			$fld = "FY";
			$masterObject = new IDP3_MASTER("financial_years");
			$fy = $masterObject->getFields();
			$sql_select= "".$fld.".".$fy['name']." as name";
			$table_join = "INNER JOIN ".$fy['table']." AS ".$fld." ON IY.".$this->getNameFieldName()." = ".$fld.".".$fy['id'];

			$sql = "SELECT ".$this->getParentFieldName()." as parent
					,  $sql_select
					FROM ".$this->getTableName()." IY
					$table_join
					WHERE ".$this->getParentFieldName()." IN (".implode(",",$keys).")
					AND ".$this->getTableField()."_order > 0
					AND ".$this->getActiveStatusSQL("IY")."
					ORDER BY ".$this->getParentFieldName().", ".$fy['sort'];
			$rows = $this->mysql_fetch_array_by_id($sql,"parent","name");
			$data = array();
			foreach($rows as $key => $years) {
				if($html) { $glue = "<br />"; } else { $glue = ", "; }
				$data[$key] = implode($glue,$years);
			}
			return $data;
		} else {
			return array();
		}
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}



	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		if($parent_id==0) {
			$parentObject = new IDP3_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}


	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}

	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>