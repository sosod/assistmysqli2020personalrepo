<?php
/**
 * To manage the functions required by external modules
 *
 * Created on: 10 June 2018
 * Authors: Janet Currie
 *
 */

class IDP3_EXTERNAL extends ASSIST_MODULE_HELPER {

	private $local_modref;
	private $sourceObject;
	private $object_type;

    public function __construct($modref="",$object_type="",$create_source_object=false) {
        parent::__construct("client","",false,$modref);
		$this->local_modref = $modref;
		$this->object_type = $object_type;
		if(strlen($object_type)>0 && $create_source_object===true) {
			switch($object_type) {
				case IDP3_PROJECT::OBJECT_TYPE:
					$this->sourceObject = new IDP3_PROJECT(0,$this->local_modref,true);
					break;
				case IDP3_PMKPI::OBJECT_TYPE:
					$this->sourceObject = new IDP3_PMKPI(0,$this->local_modref,true);
					break;
			}
		}
    }


//get list of activated idps - needed by SDBP6:new_create_step1 to confirm if SDBIP can be linked to IDP
	public function getActivatedIDPs($idp_id=0,$include_mscoa_version_in_name=false) {
		$idpObject = new IDP3_IDP(0,$this->local_modref,true);
		$rows = $idpObject->getListOfObjectsFormattedForSelect(true,true,true,$include_mscoa_version_in_name);
		if(ASSIST_HELPER::checkIntRef($idp_id)) {
			return $rows[$idp_id];
		} else {
			return $rows;
		}
	}


	public function replaceExternalNames($a) {
		$idpObject = new IDP3_IDP(0,$this->local_modref,true,true);
		$a = $idpObject->replaceAllNames($a);
		return $a;
	}





	public function getSourceRecords($object_type,$idp_id) {
		$records = $this->sourceObject->getRawObjectsForExternalModule($idp_id);
		return $records;
	}


	public function getRefTag() {
    	return $this->sourceObject->getRefTag();
	}



























	public function __destruct() {
		parent::__destruct();
	}
}


?>