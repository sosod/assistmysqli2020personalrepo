<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Delete a Category</b></h1>
<?php
$variables = $_REQUEST;
$cateid = $variables['c'];

if(checkIntRef($cateid))
{

    $sql = "SELECT * FROM ".$dbref."_content WHERE doccateid = $cateid ";
    include("inc_db_con.php");
    $dc = array();
        while($doc = mysql_fetch_array($rs))
        {
            $dc[] = $doc['docid'];
            $old = $doc['doclocation'];
            $loc = strFn("explode",$old,"/","");
            $new = "/files/".$cmpcode."/deleted_".date("YmdHis")."_".$loc[count($loc)-1];
            rename("..".$old,"..".$new);
        }
    mysql_close($con);
    $d = strFn("implode",$dc,",","");
    $sql = "UPDATE ".$dbref."_content SET docyn = 'N' WHERE doccateid = $cateid ";
    include("inc_db_con.php");
                logAct("Deleted documents (".$d.") belonging to category $cateid due to category deletion",$sql,"DOC",0);

    $sql = "UPDATE ".$dbref."_categories SET cateyn = 'N' WHERE cateid = $cateid ";
    include("inc_db_con.php");
    if(mysql_affected_rows($con)==1)
    {
                logAct("Deleted category $cateid",$sql,"CATE",$cateid);
        echo("<h3>Success!</h3>");
        echo("<p>Category $cateid has been successfully deleted.</p>");
        $urlback = "admin_maintain.php";
        include("inc_goback.php");
    } else {
        die("<p>An error has occurred.  Please go back and try again.</p>");
    }
} else {
    die("An error has occurred.  Please go back and try again.");
}
?>
</body>
</html>
