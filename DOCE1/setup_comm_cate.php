<?php
include("inc_head.php");
include("inc_setup.php");

function drawTD($cateid,$catetxt) {
global $cmpcode;
			$comm = getComment($cateid);
			echo("<th>$cateid</th><td>$catetxt</td>");
			if(count($comm)>0) { $but = "Edit"; } else { $but = "Add"; }
				$val = strFn('str_replace',decode($comm['value']),chr(10),"<br>");
				if(strlen($comm['img'])>0) {
					$img = "<img src=\"".$comm['img']."\" width=50%>";
				} else {
					$img = "";
				}
				$contxt = $comm['contacttxt'];
				$conlink = $comm['contactlink'];
				echo("<td>$val</td><td>$img</td><td>$contxt");
				if(strlen($conlink)>0) { echo("<br>($conlink)"); }
				echo("</td><td>");
				if(count($comm)>0) {
					echo(getTK($comm['tkid'],$cmpcode,'tkn'));
					echo(" on ".$comm['dt']);
				} else {
					echo("N/A");
				}
				echo("</td></tr>");

}
?>
<style type=text/css>
table {
	border: 1px solid #dedede;
}
table th {
	border: 1px solid #dedede;
}
table td {
	border: 1px solid #dedede;
}
</style>
<h1><b><?php echo($moduletitle); ?>: Setup - Category Comments</b></h1>
<h2>Category Pages</h2>
<?php displayResult($result); ?>
<table cellpadding=3 cellspacing=0>
	<tr>
		<th>Ref</th>
		<th>Category</th>
		<th>Blurb</th>
		<th>Image</th>
		<th>Contact text</th>
		<th>Created</th>
	</tr>
	<?php
	$sql = "SELECT * FROM ".$dbref."_categories WHERE catesubid = 0 AND cateyn = 'Y' ORDER BY catesort, catetitle";
	include("inc_db_con.php");
		while($cate = mysql_fetch_array($rs))
		{
			$cateid = $cate['cateid'];
			$catetxt = $cate['catetitle'];
			include("inc_tr.php");
			drawTD($cateid,$catetxt);
		}
	mysql_close($con);
	?>
</table>
</form>
<?php
$urlback = "setup_comm.php";
include("inc_goback.php");
?>
</body>

</html>
