<?php
function displayResult($result) {
    if(count($result)>0) {
        echo("<div class=\"ui-widget\">");
        if($result[0]=="check")
        {
            echo("<div class=\"ui-state-highlight ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        else
        {
            echo("<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        echo($result[1]."</p></div></div>");
    }
}

function logAct($laction,$lsql,$ltype,$ref) {
	global $cmpcode;
	global $tkid;
	global $moduledb;
	global $today;

	$laction = code($laction);
	$lsql = code($lsql);

	$sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_log VALUES ";
	$sql.= "(null,'$laction','$lsql',$today,'$tkid','$type',$ref)";
	include("inc_db_con.php");

}

function getCategory($cateid) {
    global $cmpcode;
    global $dbref;
    $cate = array();
    if(checkIntRef($cateid)) {
        $sql = "SELECT * FROM ".$dbref."_categories WHERE cateid = $cateid ";
        include("inc_db_con.php");
            $cate = mysql_fetch_array($rs);
        mysql_close($con);
    }
    return $cate;
}

function getComment($cateid) {
	global $cmpcode;
	global $dbref;
	$comm = array();
	if(checkIntRef($cateid) || $cateid == 0) {
        $sql = "SELECT * FROM ".$dbref."_setup WHERE cateid = $cateid AND yn = 'Y' ORDER BY id DESC LIMIT 1";
        include("inc_db_con.php");
			if(mysql_num_rows($rs)>0) {
				$comm = mysql_fetch_array($rs);
			}
        mysql_close($con);
		$sql = "SELECT * FROM ".$dbref."_setup_img WHERE cateid = $cateid AND yn = 'Y' ORDER BY id DESC LIMIT 1";
		include("inc_db_con.php");
			if(mysql_num_rows($rs)>0) {
				$img = mysql_fetch_array($rs);
				$comm['img'] = $img['img'];
			}
		mysql_close($con);
	}
	return $comm;
}

function displayComment($comm,$width) {
	if(count($comm)>0) {
		echo("<table cellpadding=5 cellspacing=0 width=$width style=\"margin-bottom: 10px;\" style=\"border: 1px solid #ababab;\"><tr><td>");
		echo("<p style=\"text-align:justify\">");
		if(strlen($comm['img'])>0) {
			echo("<img src=\"".$comm['img']."\" style=\"float:right;\">");
		}
		echo(strFn("str_replace",$comm['value'],chr(10),"<br>")); 
		echo("</p>");
		if(strlen($comm['contacttxt'])>0) {
			echo("<p>");
			if(strlen($comm['contactlink'])>0) {
				echo("<a href=\"".$comm['contactlink']."\" target=_blank>");
			}
			echo(strFn("str_replace",$comm['contacttxt'],chr(10),"<br>")."</a></p>"); 
		}
		echo("</td></tr></table>");
	}
}

function deleteCommentImage($cateid) {
	global $cmpcode;
	global $dbref;
	global $moduledb;
	
				$chkloc = "../files/".$cmpcode."/deleted";
				if(!is_dir($chkloc)) { mkdir($chkloc); }
				$sql = "SELECT img FROM ".$dbref."_setup_img WHERE yn = 'N'";
				include("inc_db_con.php");
				while($row = mysql_fetch_array($rs))
				{
					$old = $row['img'];
					if(file_exists("..".$old)) {
						$loc = strFn("explode",$old,"/","");
						$new = $chkloc."/deleted_".date("YmdHis")."_".$moduledb."_".$loc[count($loc)-1];
						rename("..".$old,$new);
					}
				}
				mysql_close($con);
}
?>
