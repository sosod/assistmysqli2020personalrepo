<?php
    require 'inc_head.php';
//    print_r($access);
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
<?php if($access['addcate']=="Y" || $access['docadmin']=="Y") { ?>
    <tr height=30>
        <th style="text-align:left;">Create</th>
        <td>Create a new category.</td>
        <td align=center><input type=button value=Create id=3 onclick="adminGoTo('create');"></td>
    </tr>
<?php } ?>
<?php if($r['cc']>0 || $access['docadmin']=="Y") { ?>
    <tr height=30>
        <th style="text-align:left;">Maintain</th>
        <td>Edit categories, sub-categories and documents.&nbsp;</td>
        <td align=center><input type=button value=Maintain id=4 onclick="adminGoTo('maintain');"></td>
    </tr>
<?php } ?>
<?php if($r['cc']>0 || $access['docadmin']=="Y" || $up['cc'] > 0) { ?>
    <tr height=30>
        <th style="text-align:left;">Upload</th>
        <td>Add a new document to a category.</td>
        <td align=center><input type=button value=Upload id=2 onclick="adminGoTo('upload');"></td>
    </tr>
<?php } ?>
</table>
</body>
</html>
