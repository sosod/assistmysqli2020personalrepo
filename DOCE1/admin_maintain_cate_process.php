<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Edit a Category</b></h1>
<?php
$variables = $_REQUEST;
$cateid = $variables['cateid'];
$catetitle = code($variables['catetitle']);
$cateyn = "Y";
$catesubid = $variables['catesubid'];
$cateowner = $variables['cateowner'];
$view = $variables['cateview'];
$up = $variables['cateupload'];

if(strlen($catetitle)>0 && strlen($cateowner)>0 && $cateowner!="X" && is_numeric($catesubid))
{
    $sql = "UPDATE ".$dbref."_categories SET catetitle = '$catetitle', catesubid = $catesubid, cateowner = '$cateowner' WHERE cateid = $cateid ";
    include("inc_db_con.php");
    if(mysql_affected_rows($con)==1)
    {
        logAct("Edited category $cateid",$sql,"CATE",$cateid);
        echo("<h3>Success!</h3>");
        echo("<p>Category $cateid has been successfully updated.</p>");
		if($catesubid>0) {
			$urlback = "admin_maintain_doc.php?c=".$catesubid;
		} else {
			$urlback = "admin_maintain.php";
		}
        include("inc_goback.php");
    } else {
        die("<p>An error has occurred.  Please go back and try again.</p>");
    }
} else {
    die("<p>Please complete all the required fields.</p>");
}
?>
</body>
</html>
