<?php
    require 'inc_head.php';

//print_r($access);
$docid = $_REQUEST['d'];
    if(!checkIntRef($docid)) { die("An error has occurred.  Please go back and try again."); }

$sql = "SELECT * FROM ".$dbref."_content WHERE docid = $docid ";
include("inc_db_con.php");
    if(mysql_num_rows($rs)<1) { die("An error has occurred.  Please go back and try again."); }
    $doc = mysql_fetch_array($rs);
mysql_close($con);

$cates = array();
/*if($access['docadmin']=="Y") {
    $sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' AND catesubid = 0 ORDER BY catetitle";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $cates[] = $row;
        }
    mysql_close($con);
    $sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' AND catesubid > 0 ORDER BY catetitle";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $subcates[$row['catesubid']][] = $row;
        }
    mysql_close($con);
} else {
    $sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' AND catesubid = 0 ";
    $sql.= "AND (cateowner = '$tkid' OR ()) ";
    $sql.= "ORDER BY catetitle";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $cates[] = $row;
        }
    mysql_close($con);
    $sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' AND catesubid > 0 ORDER BY catetitle";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $subcates[$row['catesubid']][] = $row;
        }
    mysql_close($con);
}
*/
$mycates = array();
$mysubs = array();

$sql = "SELECT *, cateyn as yn FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $cates[] = $row;
        if($access['docadmin']=="Y") {
            $subid = $row['catesubid'];
            $cateid = $row['cateid'];
            if($subid!=0) {
                $mysubs[$subid]['yn'] = "Y";
                $mysubs[$subid][$cateid] = $row;
            } else {
                $cateid = $row['cateid'];
                $mycates[$cateid] = $row;
            }
        }
    }
mysql_close($con);
if($access['docadmin']!="Y") {
$sql = "SELECT DISTINCT cateid, catetitle, catesubid, cateyn as yn FROM ".$dbref."_categories, ".$dbref."_categories_users ";
$sql.= "WHERE cateyn = 'Y' AND cucateid = cateid AND catesubid = 0 ";
$sql.= "AND ((cutkid = '$tkid' AND cuyn = 'Y' AND cutype = 'UP') OR (cateowner = '$tkid')) ";
$sql.= "ORDER BY catetitle";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $cateid = $row['cateid'];
        $mycates[$cateid] = $row;
    }
mysql_close($con);
$sql = "SELECT DISTINCT cateid, catetitle, catesubid, cateyn as yn FROM ".$dbref."_categories, ".$dbref."_categories_users ";
$sql.= "WHERE cateyn = 'Y' AND cucateid = cateid AND catesubid > 0 ";
$sql.= "AND ((cutkid = '$tkid' AND cuyn = 'Y' AND cutype = 'UP') OR (cateowner = '$tkid')) ";
$sql.= "ORDER BY catetitle";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $subid = $row['catesubid'];
        $cateid = $row['cateid'];
        $mysubs[$subid]['yn'] = "Y";
        $mysubs[$subid][$cateid] = $row;
    }
mysql_close($con);
}

//echo("<h1>CATES:</h1><p> "); print_r($cates);
//echo("<h2>MY:</h2><p> "); print_r($mycates);
//echo("<h3>SUBS:</h3><p> "); print_r($mysubs);

?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'yy/mm/dd',
                    changeMonth:true,
                    changeYear:true
                });

			});

    function Validate() {
        var dt = document.upload.doctitle.value;
        var dc = document.upload.doccateid.value;
        var dd = document.upload.docdate.value;
        var di = document.upload.doctypeid.value;
        var err = "Please complete the following missing details:";
        var erryn = "N";
        if(dt.length==0) {
            err = err+"/nDocument Title";
            erryn = "Y";
        }
        if(dc.length==0 || dc=="X") {
            err+= "/nCategory";
            erryn = "Y";
        }
        if(dd.length!=10) {
            err+= "/nDocument Date";
            erryn = "Y";
        }
        if(di.length==0 || di == "X") {
            err+= "/nDocument Type";
            erryn = "Y";
        }
        if(erryn == 'Y') {
            alert(err);
            return false;
        }
        else {
            return true;
        }
    }
		</script>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Edit a document</b></h1>
<p>Fields marked with a * are required.</p>
<form name=upload method=post action=admin_maintain_doc_edit_process.php onsubmit="return Validate();" language=jscript>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th style="text-align:left;">Document Ref:</th>
        <td><?php echo($docid); ?> <input type=hidden name=docid value="<?php echo($docid); ?>"></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document Title:*</th>
        <td><input type=text name=doctitle size=50 maxlength=200 value="<?php echo(decode($doc['doctitle'])); ?>"></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document Contents:</th>
        <td><textarea name=doccontents rows=3 cols=40><?php echo(decode($doc['doccontent'])); ?></textarea></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category:*</th>
        <td><select name=doccateid>
            <option value=X>--- SELECT ---</option>
<?php
foreach($cates as $c)
{
    $ci = $c['cateid'];
    if($mycates[$ci]['yn'] == "Y" || $mysubs[$ci]['yn'] == "Y")
    {
        $spc = "";
        if($mycates[$ci]['yn']== "Y") {
            echo("<option ");
            if($ci == $doc['doccateid']) { echo("selected "); }
            echo("value=$ci>".$c['catetitle']."</option>");
            $spc = "&nbsp;&nbsp;";
        }
        $subs = $mysubs[$ci];
        $ctit = strFn("substr",$c['catetitle'],0,$catedisplay);
        foreach($subs as $s)
        {
            $si = $s['cateid'];
            if(checkIntRef($si)) {
                echo("<option ");
                if($si == $doc['doccateid']) { echo("selected "); }
                echo("value=$si>".$spc.$ctit." - ".$s['catetitle']."</option>");
            }
        }
    }
}
?>
        </select></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document Date:*</th>
        <td><input type=text name=docdate readonly=readonly id=datepicker1 value="<?php echo(date("Y/m/d",$doc['docdate'])); ?>"></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document:</th>
        <td><?php echo($doc['docfilename']); ?></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Document Type:*</th>
        <td><select name=doctypeid>
            <option value=X>--- SELECT ---</option>
            <?php
            $sql = "SELECT * FROM ".$dbref."_list_doctype WHERE yn = 'Y'";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option ");
                    if($id == $doc['doctypeid']) { echo("selected "); }
                    echo("value=$id> $val </option>");
                }
            mysql_close($con);
            ?>
        </select></td>
    </tr>
    <tr height=30>
        <td colspan=2><input type=submit value="Save Changes"> <input type=reset></th>
    </tr>
</table>
</form>
<?php
$urlback = "admin_maintain_doc.php?c=".$doc['doccateid'];
include("inc_goback.php");
?>
</body>
</html>
