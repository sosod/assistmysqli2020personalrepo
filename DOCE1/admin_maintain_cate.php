<?php
    require 'inc_head.php';

$cateid = $_REQUEST['c'];
if(!checkIntRef($cateid)) { die("An error has occurred.  Please go back and try again."); }

$sql = "SELECT * FROM ".$dbref."_categories WHERE cateid = $cateid ";
include("inc_db_con.php");
//echo(mysql_num_rows($rs));
if(mysql_num_rows($rs)<1) { die("An error has occurred.  Please go back and try again."); }
    $cate = mysql_fetch_array($rs);
mysql_close($con);

$users = array();
$viewers = array();
$nonview = array();
$sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname, l.view FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u, ".$dbref."_list_users l ";
$sql.= "WHERE t.tkid = u.usrtkid AND u.usrmodref = '".$tref."' AND t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid = l.tkid AND l.yn = 'Y' ";
$sql.= " ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    $u = 0;
    $v = 0;
    $n = 0;
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            $users[$u] = array($id,$val);
            $u++;
            if($row['view']=="Y") {
                $viewers[$v] = array($id,$val);
                $v++;
            } else {
                $nonview[$n] = array($id,$val);
                $n++;
            }
        }
    mysql_close($con);
//    print_r($users);
if(count($users)>10) { $sc = 10; } else { $sc = count($users); }
if(count($nonview)>10) { $nv = 10; } else { $nv = count($nonview); }
?>
<script type=text/javascript>
function delcate(c) {
    if(confirm("Are you sure you wish to delete this category?\n\nThis will delete all sub-categories and documents.")==true)
    {
        document.location.href = "admin_maintain_cate_delete.php?c="+c;
    }
}
function cateBlurb(c) {
	if(!isNaN(parseInt(c)))
	{
		document.location.href = 'admin_comm_cate.php?c='+c;
	}
	else
	{
		alert("An error has occured.  Please try again.");
	}
}
</script>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Edit a Category</b></h1>
<form name=addcate method=post action=admin_maintain_cate_process.php><input type=hidden name=cateid value="<?php echo($cateid); ?>">
<?php
foreach($users as $vr)
{
    echo("<input type=hidden name=cateview[] value='".$vr[0]."'>");
    echo("<input type=hidden name=cateupload[] value='".$vr[0]."'>");
}

?>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th style="text-align:left;">Category Ref:</th>
        <td><?php echo($cateid); ?></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category Name:</th>
        <td><input type=text name=catetitle size=50 maxlength=200 value="<?php echo(decode($cate['catetitle'])); ?>"></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Parent Category:</th>
        <td><select name=catesubid>
            <option <?php if($cate['catesubid']==0) { echo("selected"); } ?> value=0>None</option>
<?php
    $sql = "SELECT * FROM ".$dbref."_categories WHERE catesubid = 0 AND cateid <> $cateid AND cateyn = 'Y' ORDER BY catetitle";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['cateid'];
            $val = $row['catetitle'];
            echo("<option ");
            if($id==$cate['catesubid']) { echo("selected "); }
            echo("value='$id'>$val</option>");
        }
    mysql_close($con);
?>
        </select></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category owner:</th>
        <td><?php //print_r($users); echo(count($users)); ?><select name=cateowner>
<?php
for($u=0;$u<count($users);$u++)
        {
            $id = $users[$u][0];
            $val = $users[$u][1];
            if($id == $cate['cateowner']) { $sel = "selected"; } else { $sel = ""; }
            echo("<option $sel value='$id'>$val</option>");
        }
?>
        </select></td>
    </tr>
    <tr height=30>
        <td colspan=2><input type=submit value="Save Changes"> <input type=reset> <?php if($access['delcate']=="A" || $access['docadmin']=="Y" || ($access['delcate']=="O" && $cate['cateowner']==$tkid)) { ?><input type=button value="Delete Category" onclick="delcate(<?php echo($cateid); ?>)"><?php } ?> <input type=button value="Blurb" onclick="cateBlurb(<?php echo($cateid); ?>);"></th>
    </tr>
</table>
</form>
<?php
if($cate['catesubid']>0) {
	$urlback = "admin_maintain_doc.php?c=".$cate['catesubid'];
} else {
	$urlback = "admin_maintain.php";
}
include("inc_goback.php");

?>
</body>
</html>
