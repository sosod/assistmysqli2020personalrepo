<?php
include("inc_head.php");
include("inc_setup.php");
?>
<script type=text/javascript>
function saveBlurb(act) {
	var fld = "0a";
	if(act=="cate") { fld = "1a"; }
	var sel = document.getElementById(fld).value;

	if(sel=="N" || sel == "Y") {
		document.location.href = 'setup_comm.php?a=save_comm&act='+act+'&val='+sel;
	} else {
		alert("An error has occured.  Please try again.");
	}
}
</script>
<h1><b><?php echo($moduletitle); ?>: Setup - Category Comments</b></h1>
<?php displayResult($result); ?>
<table cellpadding=3 cellspacing=0 width=650>
    <tr height=30>
        <th style="text-align:left;">Main Page</th>
        <td>Display blurb on Main Page? <select id=0a><?php echo(selectYesNo($blurbs[0])); ?></select> <input type=button value=Save onclick="saveBlurb('main');"></td>
        <td align=center><input type=button value=Configure id=0 onclick="setupGoTo('comm_main');"></td>
    </tr> 
    <tr height=30>
        <th style="text-align:left;">Categories</th>
        <td>Display blurbs for each Category? <select id=1a><?php echo(selectYesNo($blurbs[1])); ?></select> <input type=button value=Save onclick="saveBlurb('cate');"></td>
        <td align=center><input type=button value=View id=1 onclick="setupGoTo('comm_cate');"></td>
    </tr> 
</table>
<script type=text/javascript>
var a1 = document.getElementById('1a').value;
if(a1!="Y")
	document.getElementById('1').disabled = true;
else 
	document.getElementById('1').disabled = false;

var a0 = document.getElementById('0a').value;
if(a0!="Y")
	document.getElementById('0').disabled = true;
else 
	document.getElementById('0').disabled = false;

</script>
<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
</body>

</html>
