<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Delete a Document</b></h1>
<?php
$variables = $_REQUEST;
$docid = $variables['d'];

if(checkIntRef($docid))
{
    $sql = "UPDATE ".$dbref."_content SET docyn = 'N' WHERE docid = $docid ";
    include("inc_db_con.php");
    if(mysql_affected_rows($con)==1)
    {
        logAct("Deleted document $docid",$sql,"DOC",$docid);
        $sql = "SELECT * FROM ".$dbref."_content WHERE docid = $docid";
        include("inc_db_con.php");
            $doc = mysql_fetch_array($rs);
        mysql_close($con);

        $chkloc = "../files/".$cmpcode."/deleted";
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        
        $old = $doc['doclocation'];
        $loc = strFn("explode",$old,"/","");
        $new = $chkloc."/deleted_".date("YmdHis")."_".$moduledb."_".$loc[count($loc)-1];
        rename("..".$old,$new);
        
        echo("<h3>Success!</h3>");
        echo("<p>Document $docid has been successfully deleted.</p>");
        $urlback = "admin_maintain_doc.php?c=".$doc['doccateid'];
        include("inc_goback.php");
    } else {
        die("<p>An error has occurred.  Please go back and try again.</p>");
    }
} else {
    die("An error has occurred.  Please go back and try again.");
}
?>
</body>
</html>
