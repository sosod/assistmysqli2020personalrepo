<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Upload a document</b></h1>
<?php
set_time_limit(1800);
$step = 1;
echo("<P>$step. Getting document details...");  $step++;
if($_FILES["docfile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
{
    if($_FILES["docfile"]["error"]==2)
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["docfile"]["error"]." - File size exceeds max file size limit.</p>");
    }
    else
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["docfile"]["error"]."</p>");
    }
}
else    //IF ERROR WITH UPLOAD FILE
{
    $variables = $_REQUEST;
    $doctitle = code($variables['doctitle']);
    $doccontents = code($variables['doccontents']);
    $docdate = $variables['docdate'];
    $docyn = "T";
    $doccateid = $variables['doccateid'];
    $doctypeid = $variables['doctypeid'];
    $docadddate = $today;
    $docadduser = $tkid;
    $docfile = $_FILES["docfile"]["name"];

    //PHP Validation check
    if(strlen($doctitle)>0 && checkIntRef($doccateid) && checkIntRef($doctypeid) && strlen($docdate)>0 && strlen($docfile) > 0) {
echo("Done.</p><p>$step. Creating temporary document..."); $step++;
        //format docdate
        $docdt1 = strFn("explode",$docdate,"/","");
        $docdt = mktime(12,0,0,$docdt1[1],$docdt1[2],$docdt1[0]);
        //first sql load
        $sql = "INSERT INTO ".$dbref."_content (doctitle, doccontent, doccateid, docdate, doclocation, docfilename, doctypeid, docyn, docadduser, docadddate, docmoduser, docmoddate) VALUES ";
        $sql.= "('$doctitle','$doccontents',$doccateid,$docdt,'','$docfile',$doctypeid,'$docyn','$docadduser',$docadddate,'$docadduser',$docadddate)";
        include("inc_db_con.php");
        $lsql = $sql;
        //get docid
        $docid = mysql_insert_id($con);
        //validate docid
        if(!checkIntRef($docid)) { killMe("An error has occurred.  Please go back and try again."); }
        //format filelocation /files/$cmpcode/$moduledb/docdate(yyyy)/docdate(mm)/docid_docdate(yyyymmdd)_today(yyyymmddhhiiss).ext
        $doc = strFn("explode",$docfile,".","");
        $docc = count($doc)-1;
        $docext = $doc[$docc];
        $doclocation = "/files/".$cmpcode."/".$moduledb."/".date("Y",$docdt)."/".date("m",$docdt)."/".$docid."_".date("Ymd",$docdt)."_".date("YmdHis",$docadddate).".".$docext;
echo("Done.</p> <p>$step. Uploading document..."); $step++;
        //upload document
        $chkloc = "../files/".$cmpcode;
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb;
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb."/".date("Y",$docdt);
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        $chkloc = "../files/".$cmpcode."/".$moduledb."/".date("Y",$docdt)."/".date("m",$docdt);
        if(!is_dir($chkloc)) { mkdir($chkloc); }
        copy($_FILES["docfile"]["tmp_name"], "..".$doclocation);
echo("Done.</p> <p>$step. Updating document..."); $step++;
        //update sql - docfilename + docyn = Y
        $sql = "UPDATE ".$dbref."_content SET doclocation = '$doclocation', docyn = 'Y' WHERE docid = $docid";
        include("inc_db_con.php");
        $lsql.=chr(10).$sql;
echo("Done.</p>");
        logAct("Uploaded document $docid",$lsql,"DOC",$docid);

        //display result
        $result = array("check","Success!  Your document has been successfully uploaded.");
displayResult($result);
    } else {
        die("<h2>Error</h2><p>An error has occurred - some form data appears to be missing.  Please go back and try again.</p>");
    }
}

$ucate = getCategory($doccateid);
if(strlen($ucate['catetitle'])==0) { $ucate['catetitle'] = "Category"; }
?>
<table class=noborder cellpadding=5><tr><td class=noborder><img src="/pics/tri_left.gif"></td><td class=noborder><a href="admin.php">Back to Admin</a>
<?php
if(checkIntRef($doccateid)) 
{ 
	echo("</td><td class=noborder><a href=admin_upload.php?c=".$doccateid.">Upload another document</a>"); 
	if(strlen($ucate['catetitle'])>0)
	{
		echo("</td><td class=noborder><a href=admin_maintain_doc.php?c=".$doccateid.">Go to ".$ucate['catetitle']."</a>"); 
	}
}
?>
</td><td class=noborder><img src="/pics/tri_right.gif"></td></tr></table>
</body>
</html>
