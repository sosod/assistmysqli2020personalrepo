<?php
include("inc_head.php");
include("inc_setup.php");
$comm = getComment(0);
?>
<style type=text/css>
table th {
	border: 1px solid #ababab;
	vertical-align:top;
}
.b-b {
	border-bottom-color: #ffffff;
}
.b-t {
	border-top-color: #ffffff;
}
</style>
<h1><b><?php echo($moduletitle); ?>: Setup - Category Comments</b></h1>
<h2>Main Page</h2>
<?php displayResult($result); ?>
<form name=upload method=post action=setup_comm_main.php enctype="multipart/form-data">
<input type=hidden name=a value=comm_main>
<input type=hidden name=act value=save>
<input type=hidden name=cateid value=0>
<table cellpadding=3 cellspacing=0 width=650>
	<tr>
		<th class="b-b" style="text-align:left;">Blurb text:</th>
		<td><textarea cols=60 rows=8 name=value><?php echo($comm['value']); ?></textarea></td>
	</tr>
	<tr>
		<th class="b-b b-t" style="text-align:left;border-top-color: #ffffff;border-bottom-color: #ffffff;vertical-align: top">Image:</th>
		<td><input type=file size=30 name=fimg>
		<?php
			if(strlen($comm['img'])>0) {
				echo("<span style=\"color: red;\">*</span><br><img src=\"".$comm['img']."\"><br><input type=button value=\"Remove this image\">");
			}
		?>
		</td>
	</tr>
	<tr>
		<th class="b-b b-t" style="text-align:left;">Contact Us text:</th>
		<td><textarea cols=30 rows=3 name=contacttxt><?php echo($comm['contacttxt']); ?></textarea></td>
	</tr>
	<tr>
		<th class="b-t" style="text-align:left;">Contact Us link:</th>
		<td><input type=text size=30 name=contactlink value="<?php echo($comm['contactlink']); ?>"></td>
	</tr>
<?php if(count($comm)>0) { ?>
	<tr>
		<th class="b-t" style="text-align:left;">Blurb updated:</th>
		<td>By <?php echo(getTK($tkid,$cmpcode,'tkn'));?> on <?php echo($comm['dt']); ?></td>
	</tr>
<?php } ?>
	<tr>
		<td colspan=2><input type=submit value="Save Changes"> <input type=reset> <input type=button value="Clear blurb" onclick="document.location.href='setup_comm_main.php?a=comm_main&cateid=0&act=clear';"></td>
	</tr>
</table>
<p style="color: red">*If you load a new image the existing image will be removed.<br>To remove the existing image without loading a new image, use the "Remove this image" button below the image.
</form>
<?php
$urlback = "setup_comm_main.php";
include("inc_goback.php");
?>
</body>

</html>
