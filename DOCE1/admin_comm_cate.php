<?php
include("inc_head.php");
include("inc_setup.php");
$cateid = $_REQUEST['c'];
if(!checkIntRef($cateid)) {
	$cateid = $_REQUEST['cateid'];
}
if(!checkIntRef($cateid)) {
	die("An error has occured.  Please go back and try again.");
}
$cate = getCategory($cateid);
$comm = getComment($cateid);
?>
<script type=text/javascript>
function delImage(c) {
	if(!isNaN(parseInt(c))) {
		document.location.href = 'admin_comm_cate.php?a=comm_img&c='+c;
	} else {
		alert("An error has occured.  Please try again.");
	}
}
</script>
<style type=text/css>
table th {
	border: 1px solid #ababab;
	vertical-align:top;
}
.b-b {
	border-bottom-color: #ffffff;
}
.b-t {
	border-top-color: #ffffff;
}
</style>
<h1><b><?php echo($moduletitle); ?>: Admin - Category Blurb</b></h1>
<?php displayResult($result); ?>
<form name=upload method=post action=admin_comm_cate.php enctype="multipart/form-data">
<input type=hidden name=a value=comm_cate>
<input type=hidden name=act value=save>
<input type=hidden name=cateid value=<?php echo($cateid); ?>>
<table cellpadding=3 cellspacing=0 width=650>
	<tr>
		<th class="b-b" style="text-align:left;">Category:</th>
		<td><?php echo($cate['catetitle']." (Ref: $cateid)"); ?></td>
	</tr>
	<tr>
		<th class="b-b" style="text-align:left;">Blurb text:</th>
		<td><textarea cols=60 rows=8 name=value><?php echo($comm['value']); ?></textarea></td>
	</tr>
	<tr>
		<th class="b-b b-t" style="text-align:left;border-top-color: #ffffff;border-bottom-color: #ffffff;vertical-align: top">Image:</th>
		<td><input type=file size=30 name=fimg>
		<?php
			if(strlen($comm['img'])>0) {
				echo("<span style=\"color: red;\">*</span><br><img src=\"".$comm['img']."\"><br><input type=button value=\"Remove this image\" onclick=\"delImage($cateid);\">");
			}
		?>
		</td>
	</tr>
	<tr>
		<th class="b-b b-t" style="text-align:left;">Contact Us text:</th>
		<td><textarea cols=30 rows=3 name=contacttxt><?php echo($comm['contacttxt']); ?></textarea></td>
	</tr>
	<tr>
		<th class="b-t" style="text-align:left;">Contact Us link:</th>
		<td><input type=text size=30 name=contactlink value="<?php echo($comm['contactlink']); ?>"></td>
	</tr>
<?php if(count($comm)>0) { ?>
	<tr>
		<th class="b-t" style="text-align:left;">Blurb updated:</th>
		<td>By <?php echo(getTK($tkid,$cmpcode,'tkn'));?> on <?php echo($comm['dt']); ?></td>
	</tr>
<?php } ?>
	<tr>
		<td colspan=2><input type=submit value="Save Changes"> <input type=reset> <?php if(count($comm)>0) { ?><input type=button value="Clear blurb" onclick="document.location.href='admin_comm_cate.php?a=comm_cate&cateid=<?php echo($cateid); ?>&act=clear';"><?php } ?></td>
	</tr>
</table>
<?php if(strlen($comm['img'])>0) { ?>
<p style="color: red">*If you load a new image the existing image will be removed.<br>To remove the existing image without loading a new image, use the "Remove this image" button below the image.
<?php } ?>
</form>
<?php
$urlback = "setup_comm_cate.php";
include("inc_goback.php");
?>
</body>

</html>
