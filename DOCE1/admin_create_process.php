<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Create a Category</b></h1>
<?php
$variables = $_REQUEST;
$catetitle = code($variables['catetitle']);
$cateyn = "Y";
$catesubid = $variables['catesubid'];
$cateowner = $variables['cateowner'];
$view = $variables['cateview'];
$up = $variables['cateupload'];

if(strlen($catetitle)>0 && strlen($cateowner)>0 && $cateowner!="X" && is_numeric($catesubid))
{
	$sql = "SELECT max(catesort) as cs FROM ".$dbref."_categories WHERE catesubid = $catesubid AND cateyn = 'Y'";
	include("inc_db_con.php");
		$cs = mysql_fetch_array($rs);
	mysql_close($con);
	$catesort = $cs['cs']+1;
    $sql = "INSERT INTO ".$dbref."_categories VALUES (null,'$catetitle','Y',$catesubid,'$cateowner',$catesort)";
    include("inc_db_con.php");
    $cateid = mysql_insert_id($con);
    $lsql = $sql;
    if(checkIntRef($cateid)) {
        $sql = "INSERT INTO ".$dbref."_categories_users VALUES (null,$cateid,'$cateowner','VIEW','Y')";
        include("inc_db_con.php");
        foreach($view as $v)
        {
            if($v!=$cateowner) {
                $sql = "INSERT INTO ".$dbref."_categories_users VALUES (null,$cateid,'$v','VIEW','Y')";
                include("inc_db_con.php");
            }
        }
        $sql = "INSERT INTO ".$dbref."_categories_users VALUES (null,$cateid,'$cateowner','UP','Y')";
        include("inc_db_con.php");
        foreach($up as $u)
        {
            if($u!=$cateowner) {
                $sql = "INSERT INTO ".$dbref."_categories_users VALUES (null,$cateid,'$u','UP','Y')";
                include("inc_db_con.php");
            }
        }
        logAct("Created category $cateid",$lsql,"CATE",$cateid);
        echo("<h3>Success!</h3>");
        echo("<p>Category '$catetitle' ($cateid) has been successfully created.</p>");
    } else {
        die("<p>An error has occurred.  Please go back and try again.</p>");
    }
} else {
    die("<p>Please complete all the required fields.</p>");
}
?>
</body>
</html>
