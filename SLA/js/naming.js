// JavaScript Document
$(function(){
	Naming.getNaming();
});

var Naming = {
	
	getNaming 		: function()
	{
		$.getJSON("controller.php?action=getNaming", function( data ) {
			Naming.displayNaming( data );								 
		});
		
	}, 
	updateNaming 	: function( id, value )
	{
		
		$.post("controller.php?action=updateNaming", { id : id, name : value }, function( response ){
			$("#update_"+id).html("");																		 
			if( !response.error )
			{
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );
			}																			 
		},"json");		
	} ,
	displayNaming  : function( data )
	{
		$(".displayview").append($("<table />",{id:"naming_table", width:"100%"})
		   .append($("<tr />")
			 .append($("<th />"))
			 .append($("<th />"))
			 .append($("<th />"))			 
			 .append($("<th />"))			 
			)						   
		)
		
	$.each( data , function( index, naming){
		$("#naming_table")
		 .append($("<tr />")
		   .append($("<td />",{ html: naming.id}))
		   .append($("<td />",{ html: naming.sla_terminology}))
		   .append($("<td />")
			  .append($("<input />", { value:naming.client_terminology, id:"val_"+naming.id, name:"val_"+naming.id, size:"60"}))		 
		   )
		   .append($("<td />")
			  .append($("<span />",{ id: "update_"+naming.id}))
			  .append($("<input />",{id:"save_"+naming.id,name:"save_"+naming.id, type:"submit", value:"Update"}))
		   )			   
		 )
		 
		 $("#save_"+naming.id).live("click", function(){
			jsDisplayResult("info", "info", "Updating naming . . . .<img src='../images/loaderA32.gif' />" );
			Naming.updateNaming( naming.id, $("#val_"+naming.id).val() )
			return false;
		});
	});
		
	}
	
}