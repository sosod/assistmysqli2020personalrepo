// JavaScript Document
$(function(){
	var catid = $("#catid").val();    
	if(catid == undefined){
		ContractCatergory.get();		 
	}
	
	$("#savecat").click(function(){
		ContractCatergory.save();
		return false;						  
	});
	
	$("#updatecat").click(function(){
		ContractCatergory.update();	
		return false;
	});
	
	$("#updatecatstatus").click(function(){
		ContractCatergory.changeStatus( catid, $("#status").val() );							
		return false;
	})
	
});

var ContractCatergory = {
	
	name 		: "",
	shortcode   : "",
	description : "",
	
	get 		   : function()
	{
		$.getJSON("controller.php?action=getContractCategories", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					ContractCatergory.display( status );	
			   });	
			}														  
		});			
	} , 

	update		    : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();	
		var valid   	 = ContractCatergory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, id:$("#catid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating contract category . . . .<img src='../images/loaderA32.gif' />" );	
			$.post("controller.php?action=updateContractCategory", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}														 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", response.text );
		}		
	} , 
	
	save 		    : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		var valid   	 = ContractCatergory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving contract category . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveContractCategory", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.type = $("#type :selected").text();
					ContractCatergory.emptyFields( data );		
					data.id 	= response.id;
					data.active = 1;		
					ContractCatergory.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );		
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}		
	} , 
	
	changeStatus    : function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating contract category . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeCategoryStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");			
	}, 
	
	isValid 		: function(that)
	{
		if( that.name == "" ){
			return "Category name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the category short code";
		} else if( that.description == ""){
			return "Please enter the category description";
		} 
		return true;		
	} , 
	
	emptyFields 	: function( fields )
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});			
	},
	
	display			: function( category )
	{
		$("#table_contractcategory")
		  .append($("<tr />",{id:"tr_"+category.id})
			 .append($("<td />",{html:category.id}))
			 .append($("<td />",{html:category.shortcode}))			 
			 .append($("<td />",{html:category.name}))
			 .append($("<td />",{html:category.description}))			 
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+category.id, id:"edit_"+category.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:((category.status & 4) == 4 ? "disabled" : ""), name:"delete_"+category.id, id:"delete_"+category.id, type:"submit", value:"Delete"}))				
			  )
			 .append($("<td />",{html:((category.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((category.status & 4) == 4 ? "disabled" : ""), id:"change_"+category.id, name:"change_"+category.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+category.id).live("click", function(){
			document.location.href = "edit_category.php?id="+category.id+"&";
			return false;
		  });
		  
		  $("#delete_"+category.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				ContractCatergory.changeStatus( category.id, parseInt(category.status) + 2);
				$("#tr_"+category.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#change_"+category.id).live("click", function(){
			document.location.href = "change_categorystatus.php?id="+category.id+"&";
			return false;
		  });				
	}


}