// JavaScript Document
$(function(){
	$(".submenu>li").buttonset();	

    $("th").css({"text-align":"left"});
    $("table.list th").css("text-align","center");
    $("th").attr("valign","top");
	
	$(".goback").prepend("<img src='../../pics/tri_left.gif' />").css({"padding-top":"10px"});
	$(".logs").prepend("<img src='../../pics/tri_up.gif' />").css({"padding-top":"10px"});
	$(".auditlog").prepend("<img src='../../pics/tri_up.gif' />").css({"padding-top":"10px"});
	
	$(".auditlog").click(function(){
		$(this).find("img").remove();
		$(this).prepend("<img src='../../pics/tri_right.gif' />").css({"padding-top":"10px"});
	});
	
	 $(".logs").click(function(){
		   $(this).find("img").remove();
		   $(this).prepend("<img src='../../pics/tri_right.gif' />").css({"padding-top":"10px"});
		   //$(this).prepend("<img src='../../pics/tri_down.gif' />").css({"padding-top":"10px"});
	       var id = $(this).attr("id");
	       var table = id.replace("view_", "");
	       $.post("controller.php?action=view_log", { table_name : table}, function( data ){
	            $("#log_div").html("");
	            if( $.isEmptyObject(data)) {
	            	$("#logs").html("");
	                $("<div />",{id:"logs",html:"There are no audit logs "}).insertAfter("#log_div")
	            } else {

	               $("#log_div").append($("<div />",{id:"logs"}).append($("<table />",{id:"log_table"})))

	                $("#log_table").append($("<tr />")
	                    .append($("<th />",{html:"Date"}).addClass("log"))
	                    .append($("<th />",{html:"Ref"}).addClass("log"))
	                    .append($("<th />",{html:"Audit log"}).addClass("log"))
	                )
	                $.each( data, function( index, log){
	                    $("#log_table").append($("<tr />")
	                        .append($("<td />",{html:log.date}))
	                        .append($("<td />",{html:log.ref}))
	                        .append($("<td />",{html:log.changes}))
	                    )
	                });
	            }

	       },"json");

	   });	
	
});

function loadser( id )
{
	/*	
	$("#loading").ajaxStart(function(){
		$(this).html("");
		$(this).append($("<div />",{id:id+"_save", html:"Saving . . .<img src='../images/loaderA32.gif' />"}))
			   .dialog({autoOpen:true, modal:true, buttons:{"Close" : function(){
																				$(this).dialog("destroy");
																				$(this).html("");
																			   }}})								
	})
	*/
}(jQuery)
