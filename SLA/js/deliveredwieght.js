//Javascript
$(function(){
	var delweightid = $("#delweightid").val();
	if( typeof delweightid == "undefined" ){
		DeliveredWieght.get();		
	}
	
	$("#add").click(function(){
		DeliveredWieght.save();
		return false;					 
	});
	
	$("#update").click(function(){
		DeliveredWieght.update();
		return false;							
	});
	
	$("#change_status").click(function(){
		DeliveredWieght.changeStatus(delweightid, $("#status :selected").val() );									   
		return false;							   
	});
});

var DeliveredWieght	= {
	
	dweight		 : "", 
	description  : "",
	definition	 : "",
	
	get				: function()
	{

		$.getJSON("controller.php?action=getDeliveredWieghts", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, deliveredWeight ) {
					DeliveredWieght.display( deliveredWeight )	
				});
			}																 
		})
	} ,
	display 	: function( dweight )
	{
		$("#table_deliveredwieght")
		  .append($("<tr />",{id:"tr_"+dweight.id})
			.append($("<td />",{ html:dweight.id}))
			.append($("<td />",{ html:dweight.dweight}))
			.append($("<td />",{ html:dweight.description}))			
			.append($("<td />",{ html:dweight.definition}))
			.append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":dweight.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+dweight.id, name:"edit_"+dweight.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{type:"submit", id:"del_"+dweight.id, name:"del_"+dweight.id, disabled:((dweight.status & 4) == 4 ? "disabled" : ""), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((dweight.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{type:"submit", id:"change_"+dweight.id, name:"change_"+dweight.id, disabled:((dweight.status & 4) == 4 ? "disabled" : ""), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+dweight.id).live("click",function(){
			document.location.href = "edit_dweight.php?id="+dweight.id+"&";
			return false;
		});
		
		$("#del_"+dweight.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				DeliveredWieght.changeStatus( dweight.id , parseInt(dweight.status) + 2 );
				$("#tr_"+dweight.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+dweight.id).live("click", function(){
			document.location.href = "change_dweight.php?id="+dweight.id+"&";														
			return false;												
		});
		
	} ,
	save		: function()
	{
		var that 		 = this;
		that.dweight 	 = $("#dweight").val();		
		that.description = $("#description").val();	
		that.definition	 = $("#definition").val();
		that.color		 = $("#color").val();
		var valid 		 = DeliveredWieght.isValid( that );
		var data  		 = { dweight:that.dweight, description:that.description, definition:that.definition, color:that.color};
		if( valid == true ){
			$.post("controller.php?action=saveDeliveredWieght", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
					DeliveredWieght.emptyFields( that );
					data.id 	= response.id;
					data.active = 1
					DeliveredWieght.display( data )
				} else {
					jsDisplayResult("error", "error", response.text );					
				}																		 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}
	} ,
	
	isValid 	: function( that )
	{
		if( that.dweight == ""){
			return "Please enter the delivered weight"; 	
		} else if( isNaN( that.dweight ) ){
			return "Please enter valid delivered weight"; 	
		}  else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		} else if( that.color == ""){
			return "Please enter the color";	
		}
		return true;
	} , 
	update 		: function()
	{
		var that 		 = this;
		that.dweight = $("#dweight").val();
		that.description = $("#description").val();	
		that.definition	 = $("#definition").val();
		that.color		 = $("#color").val();
		var valid 		 = DeliveredWieght.isValid( that );
		var data  		 = { dweight:that.dweight, description:that.description, definition:that.definition, color:that.color, id:$("#delweightid").val() };		
		if( valid == true ){
			$.post("controller.php?action=updateDeliveredWieght", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																		 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}
		
	}  ,
	
	changeStatus : function( id, tostatus )
	{
		$.post("controller.php?action=changeDeliveredWieght", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																									 
		}, "json");
	} ,
	emptyFields : function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}			  
		}); 
	}
	
}