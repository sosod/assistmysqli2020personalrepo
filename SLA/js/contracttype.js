// JavaScript Document
$(function(){
	var typeid = $("#typeid").val();    
	if(typeid == undefined){
		ContractType.get();		 
	}
	
	$("#savetype").click(function(){
		ContractType.save();
		return false;						  
	});
	
	$("#update").click(function(){
		ContractType.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		ContractType.changeStatus( typeid, $("#status").val() );							
		return false;
	})
		   
});

var ContractType = {
	
	name 		: "",
	description : "",
	shortcode   : "", 
	
	get 		 : function()
	{
		$.getJSON("controller.php?action=getContractTypes", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					ContractType.display( status );					   
			   });	
			}														  
		});		
		
	} , 
	
	save		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		var valid   	 = ContractType.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving contract type . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveContractType", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					ContractType.emptyFields( that );		
					data.id 	= response.id;
					data.active = 1;					
					ContractType.display( data );					
				} else{
					jsDisplayResult("error", "error", response.text );
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}		
	} , 
	
	update		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		
		var valid   	 = ContractType.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, id:$("#typeid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating contract type . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateContractType", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}															 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );
		}		
	} ,
	
	changeStatus  : function( id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating contract type . . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeContractStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");		
	} , 
	
	display		  : function( type )
	{
		$("#type").append($("<option />",{value:type.id, text:type.name}));
		
		$("#table_contracttype")
		  .append($("<tr />",{id:"tr_"+type.id})
			 .append($("<td />",{html:type.id}))
			 .append($("<td />",{html:type.shortcode}))			 
			 .append($("<td />",{html:type.name}))
			 .append($("<td />",{html:type.description}))
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+type.id, id:"edit_"+type.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:((type.status & 4) == 4 ? "disabled" : ""), name:"del_"+type.id, id:"del_"+type.id, type:"submit",value:"Delete"}))				
			  )
			 .append($("<td />",{html:((type.status & 1) == 1 ? "Active" : "Inactive")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((type.status & 4) == 4 ? "disabled" : ""), id:"change_"+type.id, name:"change_"+type.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+type.id).live("click", function(){
			document.location.href = "edit_ctype.php?id="+type.id+"&";
			return false;
		  });
		  
		  $("#del_"+type.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				ContractType.changeStatus( type.id, parseInt(type.status) + 2 );
				$("#tr_"+type.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#change_"+type.id).live("click", function(){
			document.location.href = "change_ctype.php?id="+type.id+"&";
			return false;
		  });		
	} , 
	
	isValid		  : function(that)
	{
		if( that.name == "" ){
			return "Contract name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the contract short code";
		} else if( that.description == ""){
			return "Please enter the contract type description";
		}
		return true;				
	} , 
	
	emptyFields   : function(fields)
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});		
	}
		
}