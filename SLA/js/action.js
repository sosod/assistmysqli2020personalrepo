// JavaScript Document
$(function(){
  var actionid = $("#actionid").val();

  if( actionid == undefined){
	  Action.getOwner();
	  Action.getDeliverable();
  }
  
 
  $("#authorize_action").click(function(){
	Action.authorize();					  
	return false;						  
 })
  
  $(".saveaction").click(function(){
	Action.save( $(this).attr("id") );					  
	return false;						  
 })
  
$(".attach").live("change", function(){
	Action.uploadFile( this );		
	return false;
});
	
  $("#viewEditLog").click(function(){
	Action.viewEditLog();
	return false;
  });

  $("#viewUpdateLogs").click(function(){
	Action.viewUpdateLog() 							   
	return false;
  });
  
  $("#edit").click(function(){
	Action.edit() 
	return false;
  });
  
  $("#update").click(function(){
	Action.update();
	return false;
  });
 
  $("#auth_log").click(function(){
	Action.authorizationLog();
	return false;
  }); 
 
  
  $("#status").change(function(){
	if($(this).val() == "3"){
		$("#request_approval").attr("disabled", "") 
		$("#progress").val("100")
	} else{
		$("#request_approval").attr("disabled", "disabled") 		
	}
  });
  
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-mm-yy"
		});						   
	});	 
});



var Action 	= {
	
	getDeliverable	 : function()
	{
		$.post("controller.php?action=getDeliverable", {id:$("#deliverableid").val()}, function( deliverable ){
			
			$("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Ref #:"}))	   				   
					 .append($("<td />",{html:deliverable.id}))	   
				  )		
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable:"}))	   				   
					 .append($("<td />",{html:deliverable.deliverable}))	   
				  )
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Description:"}))	   				   
					 .append($("<td />",{html:deliverable.deliverable_description}))	   
				  )
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Category:"}))	   				   
					 .append($("<td />",{html:deliverable.category}))	   
				  )							  
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Type:"}))	   				   
					 .append($("<td />",{html:(deliverable.type == 1 ? "Sub Deliverable" : "Main Deliverable")}))	   
				  )			
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Main Deliverable:"}))	   				   
					 .append($("<td />",{html:deliverable.main_deliverable}))	   
				  )		
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Owner:"}))	   				   
					 .append($("<td />",{html:deliverable.deliverable_owner}))	   
				  )			
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Status:"}))	   				   
					 .append($("<td />",{html:deliverable.status}))	   
				  )	
				 $("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Deliverable Deadline:"}))	   				   
					 .append($("<td />",{html:deliverable.deadline}))	   
				  )			
				  
				 if( ( deliverable.contractdefaults & 8)  == 8){
					 $("#goback").before($("<tr />")	   
						 .append($("<th />",{html:"Deliverable Weight:"}))	   				   
						 .append($("<td />",{html:deliverable.dweight}))	   
					 )						 
				 }
			
				 if( ( deliverable.contractdefaults & 16)  == 16){			 
					 $("#goback").before($("<tr />")	   
						 .append($("<th />",{html:"Quality Weight:"}))	   				   
						 .append($("<td />",{html:deliverable.qweight}))	   
					 )					 	
				 }
					 
				 if( ( deliverable.contractdefaults & 32)  == 32){			 
					$("#goback").before($("<tr />")	   
					 .append($("<th />",{html:"Other Weight:"}))	   				   
					 .append($("<td />",{html:deliverable.oweight}))	   
					)								 			 			 
				 }

		},"json");	
		
	} ,
	
	getOwner 		: function (){
		$.getJSON("controller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
				$("#owner").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))									
			})													 
		});
	} ,
	
	save 			: function( clickedid )
	{
		that 			= this;
		that.action 	= $("#action_name").val();
		that.owner 		= $("#owner :selected").val();
		that.measurable	= $("#measurable").val();
		that.status		= $("#status :selected").val();
		that.deadline	= $("#deadline").val();
		that.remindon	= $("#remindon").val();
		valid 		= Action.isValid( that );
		var data = {name:that.action, owner:that.owner, measurable:that.measurable, status:that.status, progress:that.progress, deadline:that.deadline, remind_on:that.remindon, deliverable_id:$("#deliverableid").val()};
		
		if( valid == true ) {
		/*			 
	      $("#display_objresult")		
		  .dialog({	autoOpen:true, 
					modal	 : true,					
					buttons  : {"Close" : function(){
								$(this).dialog("destroy");
								$("#save_action").remove();
								$("#loading").html("");									
								$(this).html("");
						   }
					}
			})*/
			jsDisplayResult("info", "info", "Saving Action . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveAction", data, function( response ){
				$("#save_action").html( response.text )
				
				if( !response.error ){
					Action.emptyField( that );					 
					if( clickedid == "addnext" ){
						document.location.href = "confirmation.php?id="+$("#contractid").val();
					} else {
						  $("#displayaction").html("");	
						  $("#displayaction").action({delId:$("#deliverableid").val(), editAction:true, deleteAction:true})
						  jsDisplayResult("ok", "ok", response.text );		
					}
				} else {	
					jsDisplayResult("error", "error", response.text );		
				}
			},"json")
		} else{
			jsDisplayResult("error", "error", valid );
		}
		
	} , 
	
	edit		: function() {

		that 			= this;
		that.action_name= $("#action_name").val();
		that.owner 		= $("#owner :selected").val();
		that.measurable	= $("#measurable").val();
		that.status		= $("#status :selected").val();
		that.deadline	= $("#deadline").val();
		that.remindon	= $("#remindon").val();
		
		valid 		= Action.isValid( that );
		var data = {
					action_name	  : that.action_name,
					action_owner  : that.owner,
					measurable	  : that.measurable,
					action_status : that.status,
					progress	  : that.progress,
					deadline	  : that.deadline,
					remind_on	  : that.remindon,
					action_id	  : $("#actionid").val(),
					contractid	  : $("#contractid").val()
					};
		if( valid == true ) {

			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=editAction", data, function( response ){		  
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text  );
				}		
			},"json")
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} , 
	
	update		: function(){	
	
		that 			= this;
		that.response 	= $("#response").val();
		that.status		= $("#status :selected").val();
		that.progress	= $("#progress").val();
		that.remind_on  = $("#remind_on").val();
		
		valid 		= Action.isValid( that );
		var data = {
					response	  	 : that.response, 
					action_status 	 : that.status,
					progress	  	 : that.progress,
					remindon	  	 : that.remindon,
					request_approval : $("#request_approval").val(),
					action_id	  	 : $("#actionid").val(),
					contractid	  	 : $("#contractid").val()					
				};
		
		if( valid == true ) {

			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateAction", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text  );
				}	
			},"json")
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	authorize			: function()
	{
		that 			= {};
		that.response 	= $("#response").val();
		that.approval	= $("#approval :selected").val();
		that.progress	= $("#progress").val();
		that.remind_on  = $("#remind_on").val();
		
		valid 		= Action.isValid( that );
		var data = {
					response	  	 : that.response, 
					approval	 	 : that.approval,
					request_approval : $("#request_approval").val(),
					action_id	  	 : $("#actionid").val()		
				};
		
		if( valid == true ) {
			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' />" );  
			$.post("controller.php?action=authorizeAction", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text  );
				}	
			},"json")
		} else{
			jsDisplayResult("error", "error", valid );	
		}
	} ,
	
	authorizationLog	: function()
	{	
		
		$.post("controller.php?action=getActionAuthorizations", {id : $("#actionid").val()}, function( response ){																			
			$("#display_authLogs").html("");																																					
			if( $.isEmptyObject(response)){
				$("#display_authLogs").html("There are no update logs yet")																																																										} else {
				$("#display_authLogs").append($("<table />",{id:"table_updateloga"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, updateLog ){
						
						$("#table_updateloga")
							.append($("<tr />")
								.append($("<td />",{html:updateLog.date}))
								.append($("<td />",{html:updateLog.changes}))
								.append($("<td />",{html:updateLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
	} ,
	
	
	viewUpdateLog	: function(){
		
		$.post("controller.php?action=getActionUpdates", {id : $("#actionid").val()}, function( response ){																			   
			$("#actionUpdateLogs").html("");																																					
			if( $.isEmptyObject(response)){
				$("#actionUpdateLogs").html("There are no update logs yet")																																																								} else {
				$("#actionUpdateLogs").append($("<table />",{id:"table_updateloga"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, updateLog ){
						
						$("#table_updateloga")
							.append($("<tr />")
								.append($("<td />",{html:updateLog.date}))
								.append($("<td />",{html:updateLog.changes}))
								.append($("<td />",{html:updateLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
		
	} ,
	
	viewEditLog	: function(){
	$.post("controller.php?action=getActionEdits", {id : $("#actionid").val()}, function( response ){					
				$("#editLog").html("");																																					
				if( $.isEmptyObject(response)){
					$("#editLog").html("There are no edit logs yet")																																																								} else {
					$("#editLog").append($("<table />",{id:"table_editlog"})
								.append($("<tr />")
									.append($("<th />",{html:"Date"}))
									.append($("<th />",{html:"Audit Log"}))
									.append($("<th />",{html:"Status"}))								
								)		
							  )
					$.each( response, function( index, editLog ){
							
							$("#table_editlog")
								.append($("<tr />")
									.append($("<td />",{html:editLog.date}))
									.append($("<td />",{html:editLog.changes}))
									.append($("<td />",{html:editLog.status}))								
								)					   
					})																																																						}
																																																				
			}, "json");
	} ,
	
	isValid 	: function( fields ){
		if( fields.action == ""){
			return "Action name is required";	
		} else if( fields.owner == ""){
			return "Please select the action owner";	
		} else if( fields.measurable == ""){
			return "Please enter the measurable";	
		} else if( fields.status == ""){
			return "Please select the status";	
		} else if( fields.deadline == ""){
			return "Please enter the deadline";	
		}
		return true;		
	} ,
	
	emptyField		: function(  fields )
	{
		$.each( fields, function( index, field){
		   $("#"+index).val("");
		});		
		$("#action_name").val("");
	} , 
	
	uploadFile		: function( $this )
	{
		//console.log( $this.id )
		$("#uploading")
		 .ajaxStart(function(){
			$(this).append($("<img />",{src:"../images/arrows16.gif"}))				 
		 })
		 .ajaxComplete(function(){
			//$(this).html("");				
		});
		
		$.ajaxFileUpload({
			url	 	      : "controller.php?action=uploadAction",
			secureuri     : false,
			fileElementId : $this.id,
			dataType	  : "json",
			success		  : function( response , status){
				//console.log( response );
				if( typeof(response.error) != 'undefined'){
					
					if( response.error){
						$("#uploading").append( response.text+"<br />" )
					} else {
						//$("#uploading").append( data.text+"<br />" );
						$("#uploading").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									   .css({"color":"green"});
						if( $.isEmptyObject( response.filesuploaded ) ){
							$("#uploading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#uploading").append( file+"<br />" )											 
							});							
						}
													
					}
				}
				
			} , 
			error		: function(response, status, e){
				//console.log( "Ajax error "+e)
			}
		});
	}
 	
	
	
}