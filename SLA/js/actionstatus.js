// JavaScript Document
$(function(){
	var statusid = $("#statusid").val();    
	if(statusid == undefined){
		ActionStatus.get();		 
	}
	
	$("#save").click(function(){
		ActionStatus.save();
		return false;						  
	});
	
	$("#update").click(function(){
		ActionStatus.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		ActionStatus.changeStatus( statusid, $("#status").val() );							
		return false;
	})

});

var ActionStatus = {
	
	get 		: function()
	{
		$.getJSON("controller.php?action=getActionStatuses", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					ActionStatus.display( status );					   
			   });	
			}														  
		});		
	} , 
	
	save 		: function()
	{
		var that 				= this;
		that.name 				= $("#name").val();
		that.client_terminology = $("#client_terminology").val();
		that.color  			= $("#color").val();
		var valid   			= ActionStatus.isValid(that);
		var data    			= {name:that.name, client_terminology:that.client_terminology, color:that.color};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving action status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveActionStatus", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					ActionStatus.emptyFields( data );		
					data.id 	= response.id;
					data.status = 1;					
					ActionStatus.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );					
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} , 
	
	changeStatus 	: function( id, tostatus )
	{
		jsDisplayResult("info", "info", "Updating action status . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeAStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");
	} , 
	
	update 		: function( )
	{
		var that 				= this;
		that.name 				= $("#name").val();
		that.client_terminology = $("#client_terminology").val();
		that.color  			= $("#color").val();
		
		var valid   = ActionStatus.isValid(that);
		var data    = {name:that.name, client_terminology:that.client_terminology, color:that.color, id:$("#statusid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating action status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateAStstus", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}															 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );
		}
	} , 
	
	display		: function( astatus )
	{
		$("#table_actionstatus")
		  .append($("<tr />",{id:"tr_"+astatus.id})
			 .append($("<td />",{html:astatus.id}))
			 .append($("<td />",{html:astatus.name}))
			 .append($("<td />",{html:astatus.client_terminology}))
			 .append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":astatus.color}))	   
			  )
			 .append($("<td />")
				.append($("<input />",{type:"submit", name:"edit_"+astatus.id, id:"edit_"+astatus.id, value:"Edit"}))
				.append($("<input />",{type:"submit", disabled:((astatus.status & 4) == 4 ? "disabled" : ""), name:"del_"+astatus.id, id:"del_"+astatus.id, value:"Delete"}))				
			  )
			 .append($("<td />",{html:((astatus.status & 1)== 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((astatus.status & 4) == 4 ? "disabled" : ""), id:"change_"+astatus.id, name:"change_"+astatus.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+astatus.id).live("click", function(){
			document.location.href = "edit_astatus.php?id="+astatus.id+"&";
			return false;
		  });
		  
		  $("#del_"+astatus.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				ActionStatus.changeStatus( astatus.id, parseInt(astatus.status)+2 );
				$("#tr_"+astatus.id).fadeOut();
				
			}
			return false;										  
		  });
		  
		  $("#change_"+astatus.id).live("click", function(){
			document.location.href = "change_astatus.php?id="+astatus.id+"&";
			return false;
		  });		  
	} , 
	
	emptyFields : function( fields )
	{
		$.each(fields, function( index, field){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}
		});
	} , 
	
	isValid 	: function( that )
	{
		if( that.name == "" ){
			return "Status name is required";		
		} else if( that.client == ""){
			return "Please enter the client terminology";
		}
		return true;
	}
	
	
}