// JavaScript Document
$(function(){
  var contractid = $("#contractid").val(); 

  
  if( typeof(contractid) == "undefined"){
	Contract.getActiveType();
	Contract.getSupplier();
	Contract.getUsers();
	Contract.getTemplates();	
	Contract.getAssessmentFrequency();
	Contract.getNotificationFrequency();	
	Contract.getDeliverableStatus();
	Contract.getActiveCategory();
	Contract.getContractDetail();	
  }

	
	$(".weight_dependent").hide();
	$(".other_dependent").hide();
	$(".closed_contract").hide();	
	
	$("#contract_status").change(function(){								  
		if($(this).val() == 3){
			$(".closed_contract").show();	
			$("#requestapproval").attr("disabled", "");
			$("#tr_remind").hide();
		} else {
			$(".closed_contract").hide();				
			$("#requestapproval").attr("disabled", "disabled")			
			$("#tr_remind").show();
		}								  
		return false;								  
	})
	
	$(".attach").live("change", function(){
		Contract.uploadFile( this );		
		return false;
	});
	
	$("#weightsAssigned").change(function(){
		if( $(this).val() == 1){
			$(".weight_dependent").show();	
		} else{
			$(".weight_dependent").hide();
		}
		return false;								  
	});
	
	$("#other").live("change", function(){
		if( $(this).val() == 1 ) {
			$(".other_dependent").show();	
		} else{
			$(".other_dependent").hide();
		}								
	});
	
	
	$("#template_").live("change", function(){
		if($(this).val() == ""){
			$(".delStatus").show();
		} else{
			$(".delStatus").hide();	
		}									
	})
	
	$("#authorize_contract").click(function(){
		Contract.authorize();
		return false;											
	})
	
	$(".savecontract").click(function(){
		Contract.save( $(this).attr('id') );
		return false;
	});
	
	$("#addanother").click(function(){
		Contract.addItem();
		return false;
	});
	
	$("#savedefaults").click(function(){
		Contract.saveContractDefaults();									  
		return false;								  
	})
	
	$("#update_contract").click(function(){
		Contract.update();
		return false;						
	});
	
	$("#editcontract").click(function(){
		Contract.edit();									  
		return false;							  
	});
	
	$("#edit_details").click(function(){
		Contract.editContractDetails();		
		return false;
	});
	
	$("#vieweditlog").click(function(){
		Contract.viewEditLog();	
		return false;
	});
	
	$("#viewupdatelog").click(function(){
		Contract.viewUpdateLog();	
		return false;
	});
	
	$("#auth_log").live("click", function(){
		Contract.viewAuthorizationLog();	
		return false;
	});	
	
	$("#save_request").click(function(){
		document.location.href = "save_template.php?id="+contractid;
		return false;							  
	});
	
	$("#edit_contract").live("click", function(){
		document.location.href = "edit_contract_detail.php?id="+$("#contractid").val();		
		return false
	});
	
	
	$("#contract_manager").live("change", function(){
		if( $(this).val() == "" || $(this).val() == null ) {}
		else{
			$("#contract_authorisor_1").html( $("#contract_manager :selected").text() );					
		}
		
	});
	
	$("#assessment_frequency").live("change", function(){											   
		// alert( $("#assessment_frequency :selected").text() )
		$(".assfreq").hide();
		var text = $("#assessment_frequency :selected").text();
		if( text == "Weekly") {
			$("#weekly").show();
		} else if( text == "Monthly" ) {
			$("#monthly").show()
		} else if( text == "Quaterly" ) {
			$("#quaterly").show();			
		}									   
		return false;												   
	});
	
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-mm-yy"
		});						   
	});	   

});


var Contract = {
	
	getContractDetail		: function()
	{
		$.post("controller.php?action=getContract", { id : $("#contractid").val()} , function( contract ){
																							  
			if( $.isEmptyObject(contract) )	{
				$("#table_contract").append($("<tr />")
									  .append($("<td />",{colspan:"2", html:"Contract information could not be obtained"}))
				)
			} else {																			  
				$("#table_contract")	
				 .append($("<tr />")
					.append($("<td/>",{colspan:"2", html:"<h3>Step 5 : Confrimation Of SLA</h3>"}))				
				  )
				 .append($("<tr />")
					.append($("<th />",{html:"Ref :"}))
					.append($("<td />",{html:$("#contractid").val()}))					
				  )
				$.each( contract, function( index, contr ){
					$("#table_contract")	
					 .append($("<tr />")
						.append($("<th />",{html:contr.colName+" :"}))
						.append($("<td />",{html:contr.value}))					
					  )
				});
			}
			
		},"json")	
		
	} , 
	
	getActiveType 	   :    function()
	{
		Contract.getContract();
		$.getJSON("controller.php?action=getActiveType", function( types ){
			$.each( types, function( index, type ){
				$("#type").append($("<option />",{value:type.id, text:type.name}))					
			});									 
		})	
	} , 
	getActiveCategory :    function( id )
	{
		$.post("controller.php?action=getActiveCategory", function( categories ){
			$.each( categories, function( index, category ){
				$("#category").append($("<option />",{value:category.id, text:category.name}))					
			});									 
		}, "json")	
	}, 
	getSupplier 	:    function()
	{
		$.getJSON("controller.php?action=getSupplier", function( suppliers ){
			$.each( suppliers, function( index, supplier){
				$("#supplier").append($("<option />",{value:supplier.id, text:supplier.name}))					
			});									 
		})	
	} , 	

	getUsers	   : function()
	{
		$.getJSON("controller.php?action=getUsers", function( users ){											 
			$.each( users, function( index, user){
					$("#contract_owner").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))	
					
					$("#contract_authorisor").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))
					
				if( (user.status & 2) == 2 ){
					$("#contract_manager").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))
				}
			});														 
		});	
	}, 
	
	getTemplates	 : function()
	{
		$.getJSON("controller.php?action=getTemplates", function( templates ){
			$.each( templates, function( index, template){
				$("#template_").append($("<option />",{text:template.name, value:template.id}))					
			});														 
		});				
	} , 
	
	getAssessmentFrequency 	: function(){
		
		$.getJSON("controller.php?action=getAssessmentFrequency", function( assessmentFrequency ){
			$.each( assessmentFrequency, function( index, value){
				$("#assessment_frequency").append($("<option />", { text:value.name, value:value.id}))								  
			});
		})
		
	} , 
	
	getNotificationFrequency		: function(){
		
		$.getJSON("controller.php?action=getNotificationFrequency", function( notificationFrequency ){
			$.each( notificationFrequency, function( index, value){
				$("#notification_frequency").append($("<option />", { text:value.shortcode, value:value.id}))								
			});
		})
	} ,
	
	getDeliverableStatus	: function(){
		$.getJSON("controller.php?action=getDeliverableStatus", function( deliverableStatuses ){
			if( !$.isEmptyObject(deliverableStatuses) ){
				$.each( deliverableStatuses, function( index, delstatus ){
					
					$("#deliverable_status").append($("<table />",{width:"100%"})
						 .append($("<tr />")
						   .append($("<th />",{html:delstatus.client_terminology+":", width:"58%"}))	   
						   .append($("<td />")
							  .append($("<select />",{name:delstatus.client_terminology, id:delstatus.id})
								 .addClass("deliverable_status")
								 .append($("<option />",{value:"1", text:"Yes"}))
								 .append($("<option />",{value:"0", text:"No",selected:(delstatus.id != 4 ? "selected" : "")}))						 
							   )		 
							)	   				   
						 )							   
					 )						 
				});
			}
			
		});		
	},
	
	save 				: function( clickeid )
	{
		that 						= {};
		that.name   				= $("#name").val();
		that.budget 				= $("#budget").val();
		that.comments				= $("#comments").val();
		that.type					= $("#type :selected").val();
		that.category  				= $("#category :selected").val();
		that.supplier				= $("#supplier :selected").val();
		that.signnature_of_tender   = $("#signnature_of_tender").val();
		that.signature_of_sla 		= $("#signature_of_sla").val();
		that.completion_date		= $("#completion_date").val();
		that.contract_manager		= $("#contract_manager :selected").val();
		that.contract_owner			= $("#contract_owner :selected").val();		
		that.template				= $("#template_ :selected").val();
		that.assessment_frequency   = $("#assessment_frequency :selected").val();
		that.notification_frequency = $("#notification_frequency :selected").val();
		that.contract_authorisor	= $("#contract_authorisor :selected").val();		
		that.deliverable_status		= {};

		$(".deliverable_status").each(function( index, selectBox){
			that.deliverable_status[selectBox.id] = $(this).val();									   
		});
		
		var valid 					= Contract.isValid( that ); 
		that.assessment_frequency_date  = $(".assessment_frequency_date").val();
		that.attach					= $(".attach").val();
		data = {
				name					: that.name,
				budget  				: that.budget,
				comments				: that.comments,
				type		    		: that.type,
				category				: that.category,
				supplier				: that.supplier,
				signnature_of_tender	: that.signnature_of_tender,
				signature_of_sla		: that.signature_of_sla,
				completion_date			: that.completion_date,
				contract_manager		: that.contract_manager,
				contract_owner			: that.contract_owner,
				template				: that.template	,		
				contractdefaults		: (that.template == "" ? 0 : 64),
				assessment_frequency	: that.assessment_frequency,
				notification_frequency	: that.notification_frequency,				
				contract_authorisor		: that.contract_authorisor, 
				deliverable_status		: that.deliverable_status
				};
		if( valid == true){
			
			  jsDisplayResult("info", "info", "Saving Contract . . . .<img src='../images/loaderA32.gif' />" );
			
			 $("#display_objresult")	
			 .dialog({	autoOpen:true, 
						modal	 : true,	
						position : 'top',
						buttons  : {
								"Close" : function(){
									$(this).dialog("destroy");
									$("#save_contract").remove();									
									$(this).html("");
									var response = $("body").data("response");						
									if( response == false ){
										var contract_id = $("body").data("contract_id"); 								
										if( clickeid == "savenext"){
											if( that.template == ""){
												document.location.href = "contract_defaults.php?id="+contract_id;
											} else{
												document.location.href = "confirmation.php?contract_id="+contract_id;
											}
										} else {
	
										}
										if( that.template == ""){
											//document.location.href = "contract_defaults.php?id="+response.id;
										} else{
											document.location.href = "confirmation.php?contractid="+contract_id;
										}	
									}
							   }
					}
				  });
		  $.post("controller.php?action=saveContract", data, function( response ){
			
			if( !response.error ){
				//$("#save_contract").html( response.text )
				$("body").data("contract_id", response.id )		
				$("body").data("response", response.error );
		

					$("#attachments").html("");
					Contract.emptyFields( that );
					$("#attachments").html("");
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );
					//$("#save_contract").html(  )
				}										
			}, "json");				
		} else{
			jsDisplayResult("error", "error", valid );
		}
		
	} , 
	
	update			: function()
	{

		that 						= this;
		that.project_value   		= $("#project_value").val();
		that.saving   				= $("#saving").val();		
		that.date_completed 		= $("#date_completed").val();
		//that.comments				= $("#comments").val();
		//that.type					= $("#type :selected").val();
		//that.category  				= $("#category :selected").val();
		//that.supplier				= $("#supplier :selected").val();
		//that.signnature_of_tender   = $("#signnature_of_tender").val();
		//that.signature_of_sla 		= $("#signature_of_sla").val();
		//that.completion_date		= $("#completion_date").val();
		//that.contract_manager		= $("#contract_manager :selected").val();
		//that.contract_owner			= $("#contract_owner :selected").val();		
		//that.template				= $("#template :selected").val();
		//that.assessment_frequency   = $("#assessment_frequency :selected").val();
		//that.notification_frequency = $("#notification_frequency :selected").val();	
		//that.contract_authorisor	= $("#contract_authorisor :selected").val();			
		that.contract_status		= $("#contract_status :selected").val();		
		that.response				= $("#response").val();			
		that.remindon				= $("#remindon").val();			
		
		var valid 					= false;
		if( that.contract_status == ""){
		   jsDisplayResult("error", "error", "Please select the status" );
			return false;
		} else if( that.response == ""){
			jsDisplayResult("error", "error", "Please enter the response" );
			return false;
		} else if( that.remindon == ""){
			jsDisplayResult("error", "error", "Please enter remind on date" );
			return false
		} else if( that.project_value !=  "" && isNaN(that.project_value)){
			jsDisplayResult("error", "error", "Please enter valid contract value" );
			return false;
		} else {
			valid = true;	
		}
		that.assessment_frequency_date  = $(".assessment_frequency_date").val();
		that.attach					= $(".attach").val();
		data = {
				contract_id				: $("#contractid").val(),
				project_value   		: that.project_value,
				saving   				: that.saving,	
				date_completed 			: that.date_completed,
				//name					: that.name,
				//budget  				: that.budget,
				//comments				: that.comments,
				//type		    		: that.type,
				//category				: that.category,
				//supplier				: that.supplier,
				//signnature_of_tender	: that.signnature_of_tender,
				//signature_of_sla		: that.signature_of_sla,
				//completion_date			: that.completion_date,
				//contract_manager		: that.contract_manager,				
				//contract_owner			: that.contract_owner,
				//template				: that.template,
				//assessment_frequency	: that.assessment_frequency,
				//assessment_frequency_date : that.assessment_frequency_date,
				//notification_frequency	: that.notification_frequency,
				//contract_authorisor		: that.contract_authorisor,
				status					: that.contract_status,	
				response				: that.response	,
				remindon				: that.remindon	
				};
		if( valid == true){
			
			 jsDisplayResult("info", "info", "updating  Contract . . . .<img src='../images/loaderA32.gif' />" );
			 
			  $("#display_objresult")
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#updatecontract").remove();									
									$(this).html("");
							   }
						}
			})			
			  
		  $.post("controller.php?action=updateContract", data, function( response ){
			  if( response.error )
			  {
				  jsDisplayResult("error", "error", response.text );
			  } else {
				  $("#response").val("");
				  jsDisplayResult("ok", "ok", response.text );
			  }																								
		  }, "json");				
		} else{
			jsDisplayResult("error", "error", valid );	
		}
		
	} ,
	
	edit			: function()
	{

		that 						= this;
		that.name   				= $("#name").val();
		that.budget 				= $("#budget").val();
		that.comments				= $("#comments").val();
		that.type					= $("#type :selected").val();
		that.category  				= $("#category :selected").val();
		that.supplier				= $("#supplier :selected").val();
		that.signnature_of_tender   = $("#signnature_of_tender").val();
		that.signature_of_sla 		= $("#signature_of_sla").val();
		that.completion_date		= $("#completion_date").val();
		that.contract_manager		= $("#contract_manager :selected").val();		
		that.contract_owner			= $("#contract_owner :selected").val();
		that.template				= $("#template :selected").val();
		that.assessment_frequency   = $("#assessment_frequency :selected").val();
		that.notification_frequency = $("#notification_frequency :selected").val();
		that.contract_authorisor	= $("#contract_authorisor :selected").val();
		
		var valid 					= Contract.isValid( that ); 
		that.assessment_frequency_date  = $(".assessment_frequency_date").val();
		that.attach					= $(".attach").val();
		data = {
				contract_id				: $("#contractid").val(),	
				contract				: that.name,
				budget  				: that.budget,
				project_comments		: that.comments,
				type		    		: that.type,
				category				: that.category,
				supplier				: that.supplier,
				signnature_of_tender	: that.signnature_of_tender,
				signature_of_sla		: that.signature_of_sla,
				completion_date			: that.completion_date,
				contract_manager		: that.contract_manager,
				owner			: that.contract_owner,
				template				: that.template,
				assessment_frequency	: that.assessment_frequency,
				assessment_frequency_date : that.assessment_frequency_date,
				notification_frequency	: that.notification_frequency,	
				contract_authorisor		: that.contract_authorisor
				};
		if( valid == true){
			 jsDisplayResult("info", "info", "Updating Contract . . . .<img src='../images/loaderA32.gif' />" );
			 
			  $("#display_objresult")
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#edit_contract").remove();									
									$(this).html("");
							   }
						}
			})						
		  $.post("controller.php?action=editContract", data, function( response ){
			  	if( response.error )
			  	{
			  		 jsDisplayResult("error", "error", response.text );	
			  	} else {
			  		 jsDisplayResult("ok", "ok", response.text );
			  	}									
			}, "json");				
		} else{
	  		 jsDisplayResult("error", "error", valid );
		}
	} ,
	
	editContractDetails		: function()
	{

		that 						= this;
		that.name   				= $("#name").val();
		that.budget 				= $("#budget").val();
		that.comments				= $("#comments").val();
		that.type					= $("#type :selected").val();
		that.category  				= $("#category :selected").val();
		that.supplier				= $("#supplier :selected").val();
		that.signnature_of_tender   = $("#signnature_of_tender").val();
		that.signature_of_sla 		= $("#signature_of_sla").val();
		that.completion_date		= $("#completion_date").val();
		that.contract_manager		= $("#contract_manager :selected").val();		
		that.contract_owner			= $("#contract_owner :selected").val();
		that.template				= $("#template :selected").val();
		that.assessment_frequency   = $("#assessment_frequency :selected").val();
		that.notification_frequency = $("#notification_frequency :selected").val();
		that.contract_authorisor	= $("#contract_authorisor :selected").val();
		
		var valid 					= Contract.isValid( that ); 
		that.assessment_frequency_date  = $(".assessment_frequency_date").val();
		that.attach					= $(".attach").val();
		data = {
				contract_id				: $("#contractid").val(),	
				name					: that.name,
				budget  				: that.budget,
				comments				: that.comments,
				type		    		: that.type,
				category				: that.category,
				supplier				: that.supplier,
				signnature_of_tender	: that.signnature_of_tender,
				signature_of_sla		: that.signature_of_sla,
				completion_date			: that.completion_date,
				contract_manager		: that.contract_manager,
				contract_owner			: that.contract_owner,
				template				: that.template,
				assessment_frequency	: that.assessment_frequency,
				assessment_frequency_date : that.assessment_frequency_date,
				notification_frequency	: that.notification_frequency,	
				contract_authorisor		: that.contract_authorisor
				};
		if( valid == true){
			 jsDisplayResult("info", "info", "Updating Contract Defaults . . . .<img src='../images/loaderA32.gif' />" );
			 
			  $("#display_objresult")			
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#editdefault_contract").remove();									
									$(this).html("");
							   }
						}
			})				
			
		  $.post("controller.php?action=editContractDetails", data, function( response ){
			    if( response.error )
			    {
					jsDisplayResult("error", "error", response.text );
			    } else {
			    	jsDisplayResult("ok", "ok", response.text );
			    }																																
			}, "json");				
		} else{
			jsDisplayResult("error", "error", valid );	
		}
	} , 
	
	authorize		: function()
	{

		that 						= {};
		that.response  				= $("#response").val();
		that.approval				= $("#approval :selected").val();
		that.requestapproval		= $("#requestapproval").val();
		var valid 					= Contract.isValid( that ); 

		data = {
				contract_id				: $("#contractid").val(),	
				response				: that.response,
				approval  				: that.approval,
				requestapproval			: that.requestapproval
				};
		if( valid == true){
		jsDisplayResult("info", "info", "Authorizing Contract . . . .<img src='../images/loaderA32.gif' />" );
		
			  $("#display_objresult")
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#authorizecontract").remove();									
									$(this).html("");
							   }
						}
			})				
			
		  $.post("controller.php?action=authorizeContract", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );					
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}																																
			}, "json");				
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	viewEditLog	: function()
	{
		$.post("controller.php?action=getContractEdits", {id:$("#contractid").val()}, function( data ){
			$("#editlog").html("");
			$("#editlog").append($("<table />", {id:"editlog_table"})
							  .append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							   )		 
							)
			$.each( data, function(index, contract){
				$("#editlog_table")
				  .append($("<tr />")
					.append($("<td />",{html:contract.date}))
					.append($("<td />",{html:contract.changes}))
					.append($("<td />",{html:contract.status}))								
				   )		 
			})																			 
		},"json");
			
	} , 
	
	viewUpdateLog	: function()
	{
		$.post("controller.php?action=getContractUpdates", {id:$("#contractid").val()}, function( data ){
			$("#updatelog").html("");
			$("#updatelog").append($("<table />", {id:"updatelog_table", width:"60%"})
							  .append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))
							   )		 
							)
			$.each( data, function(index, contract){
				$("#updatelog_table")
				  .append($("<tr />")
					.append($("<td />",{html:contract.date}))
					.append($("<td />",{html:contract.changes}))
					.append($("<td />",{html:contract.status}))
				   )		 
			})																			 
		},"json");
			
	} ,
	
	viewAuthorizationLog		: function()
	{
		$.post("controller.php?action=getContractAuthorizations", {id:$("#contractid").val()}, function( data ){																					
			$("#updatelog").html("");
			$("#updatelog").append($("<table />", {id:"updatelog_table", width:"60%"})
							  .append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))
							   )		 
							)
			$.each( data, function(index, contract){
				$("#updatelog_table")
				  .append($("<tr />")
					.append($("<td />",{html:contract.date}))
					.append($("<td />",{html:contract.changes}))
					.append($("<td />",{html:contract.status}))
				   )		 
			})																			 
		},"json");
			
	} ,
	
	addItem 		:	function()
	{
	//var date = new Date();
	//var id = date.getDay()+""+date.getMonth()+""+date.getDate()+""+date.getMinutes()+""+date.getSeconds()+""+date.getMilliseconds();
		//$("#attachment").append($("<input />",{type:"file", id:"attach_"+id,  name:"attach_"+id }).addClass("attach"))	
	} , 
	
	isValid			: function( that )
	{

		if( that.name == "" ) {
			return "Please enter the project name";	
		} else if( that.budget == ""){
			return "Please enter the budget";			
		} else if( that.budget != undefined &&  isNaN(that.budget) ){
			return "Please enter valid budget";			
		} else if( that.type == ""){
			return "Please select the sla type";	
		} else if( that.category == ""){
			return "Please enter the sla category";	
		} else if( that.supplier == ""){
			return "Please select the supplier";	
		} else if( that.signnature_of_tender == ""){
			return "Please enter the signnature of tender";	
		} else if( that.signature_of_sla == "" ){
			return "Please enter the signature of sla";	
		} else if( that.completion_date == ""){
			return "Please enter the completion date";	
		} else if( that.contract_manager == ""){
			return "Please select the contract manager";	
		} else if( that.contract_owner == "" ){
			return "Please select the contract owner";			
		} 
		
		return true;
	},
	
	emptyFields					: function( that ){
			$.each( that, function( index, val){
				$("#"+index).val("");	
				$(".assfreq").hide();
				$("#contract_authorisor_1").html("");
			});		
	} ,
	
	saveContractDefaults		: function()
	{
		var that 				= this;
		that.hasDeliverables	= $("#hasDeliverables :selected").val();
		that.hasSubDeliverables = $("#hasSubDeliverables :selected").val();
		that.weightsAssigned	= $("#weightsAssigned :selected").val();
		that.delivered			= $("#delivered :selected").val();
		that.quality			= $("#quality :selected").val();
		that.other				= $("#other :selected").val();		
		that.othercategory		= $("#othercategory").val();
			
		var data = {
					 hasDeliverables	: that.hasDeliverables,
					 hasSubDeliverables	: that.hasSubDeliverables,
					 weightsAssigned	: that.weightsAssigned,
					 delivered			: that.delivered,
					 quality			: that.quality,
					 othercategory		: that.othercategory,
					 other				: that.other,
					 id					: $("#contractid").val()
				  };

		jsDisplayResult("info", "info", "Updating Contract Defaults . . . .<img src='../images/loaderA32.gif' />" );
		  
		$.post("controller.php?action=saveContractDefault", data, function( response ){
			$("#savedefault_contract").html( response.text );																		   
			if( !response.error ){	
				if( that.hasDeliverables == 1){
					$("#display_objresult").animate({opacity:1}, 400000 ).delay(5000);
					jsDisplayResult("ok", "ok", response.text );
					document.location.href = "add_deliverable.php?id="+$("#contractid").val();
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}
			} else {
				jsDisplayResult("error", "error", response.text );	 	
			}
		}, "json");		  
		
	}, 
	
	getContract		: function()
	{
		$.post("controller.php?action=getContract", {id: $("#contractid").val() }, function( data ){																			
		}, "json")
	} , 
	
	uploadFile		: function( $this )
	{
		$("#uploading")
		 .ajaxStart(function(){
			$(this).append($("<img />",{src:"../images/arrows16.gif"}))				 
		 })
		 .ajaxComplete(function(){
			$(this).html("");				
		});
		
		$.ajaxFileUpload({
			url	 	      : "controller.php?action=uploadFile",
			secureuri     : false,
			fileElementId : $this.id,
			dataType	  : "json",
			success		  : function( response , status){
				if( typeof(response.error) != 'undefined'){
					
					if( response.error){
						$("#attachments").append( response.text+"<br />" )
					} else {
						//$("#uploading").append( data.text+"<br />" );
						$("#attachments").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									   .css({"color":"green"});
						if( $.isEmptyObject( response.filesuploaded ) ){
							$("#attachments").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#attachments").append( file+"<br />" )											 
							});							
						}
													
					}
				}
				
			} , 
			error		: function(response, status, e){
				//console.log( "Ajax error "+e)
			}
		});
	}
}
