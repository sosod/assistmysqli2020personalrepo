/**
 * Displays the actions awaiting and those approved
 */
$.widget("ui.actionapprove", {
	options			: {
		start		: 0, 
		limit		: 10, 
		widgetRef 	: Math.floor( Math.random()*1000) + Math.floor( Math.random()*10)
	} , 
	
	_init			: function()
	{
		this._getActions();
		
	} , 
	
	_getActions	 	: function()
	{
		$this = this;
		var element = $this.element;
		$.post("controller.php?action=approvalData", { id : $("#deliverableid").val() }, function( responseData ){
			
			var ref 	= $this.options.widgetRef;
			var columns = responseData.total;
			
			$(element).append($("<table />",{id:"approval_"+ref}).addClass("noborder")
						.append($("<tr />")
							.append($("<td />",{html:"<h4>Actions Awaiting to be approved</h4>"}).addClass("noborder"))
							.append($("<td />",{html:"<h4>Approved actions</h4>"}).addClass("noborder"))
						)
						.append($("<tr />")
							.append($("<td />").addClass("noborder")
								.append($("<table />",{id:"awaiting_"+ref}))
							)
							.append($("<td />").addClass("noborder")
								.append($("<table />",{id:"approved_"+ref}))	
							)
						)
			)
			
			$this._displayHeaders( responseData.headers, "awaiting_"+ref,  ($.isEmptyObject(responseData.awaiting) ? "awaiting" : "awaiting_"+ref) )
			$this._displayHeaders( responseData.headers,  "approved_"+ref, "" )
			//actions approved
			if( $.isEmptyObject(responseData.approved)){
				$("#approved_"+ref).append($("<tr />",{id:"tr_noapproved"})
										 .append($("<td />",{colspan:columns, html:"No actions approved"}))		
									   )
			} else {
				$this._displayActions( responseData.approved, "approved_"+ref , responseData.actionStatues)					
			}
			
			//actions awaiting approval	
			if( $.isEmptyObject(responseData.awaiting) ){
				$("#awaiting_"+ref).append($("<tr />",{id:"tr_noawaiting"})
										 .append($("<td />",{colspan:columns,html:"No actions awaiting approval"}))		
									   )				
			} else {
				$this._displayActions( responseData.awaiting, ($.isEmptyObject(responseData.awaiting) ? "awaiting" : "awaiting_"+ref)  , responseData.actionStatues )
			}
			
		}, "json");
		
	} , 
	
	_displayHeaders		: function( headers, table, extra )
	{
		var $this = this;
		var reference   = $this.options.widgetRef		
		var tr    = $("<tr />");
		
		$.each( headers, function( index, head){			
			tr.append($("<th />",{html:head}))
		});
		var displayStyle = "";
		if( table == "awaiting_"+reference && extra == "awaiting_"+reference )
		{
			displayStyle = "table-cell";
		} else {
			displayStyle = "none";
		}		
		tr.append($("<th />",{html:"&nbsp;"}).css({display:displayStyle}))
		
		$("#"+table).append( tr );
	} , 
	
	_displayActions		: function( actions, table , status)
	{
		var $this 		= this;
		var reference   = $this.options.widgetRef
		
		$.each( actions, function( index, action){
			var tr = $("<tr />",{id:"tr_"+index})
			$.each( action, function( ref, val){
				tr.append($("<td />",{html:val}))
			})
			var displayStyle = "";
			if( table == "awaiting_"+reference )
			{
				displayStyle = "table-cell";
			} else {
				displayStyle = "none";
			}
			
			tr.append($("<td />").css({display:displayStyle})
								 .append($("<input />",{type:"button", name:"approveaction_"+index, id:"approveaction_"+index, value:"Approve"}))	
			)
			
			$("#approveaction_"+index).live("click", function(){

				$("#action_approval").dialog({
						autoOpen	: true,
						modal		: true, 
						width		: "350px",
						height		: "auto",
						title		: "Approve Action", 
						buttons		: {
									 "Approve" : function(){
										 $this._approveAction( action )													 
									  },
									 "Decline" : function(){
											 $this._declineAction( action )													  
											// $(this).dialog().dialog("destroy")																				
										} 	 				
									 } 								 
					  })										 
				return false;
			})
			
			$("#"+table).append( tr )
		})	
	} ,
	
	_approveAction	: function( action )
	{
		var $this 		= this;
		var reference   = $this.options.widgetRef		
		if( $("#response").val() == ""){
			jsDisplayResult("error", "error", "Please enter the response" );
			return false;	
		} else{
			jsDisplayResult("info", "info", "Approving action . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=approveAction", { id : action.ref,response : $("#response").val(), contractid:$this.options.contractId }, function( response ){																		
					if( !response.error){
						jsDisplayResult("ok", "ok", response.text );
						$("#awaiting_"+reference).find("#tr_"+action.ref).remove();
						$this.options.approveAction	= false;
						var actions = {};
						actions = { 1 : action };
						$("#approved_"+reference).find("#tr_noapproved").remove();
						$this._displayActions( actions, "approved_"+reference)
						
						$("#action_approval").dialog("destroy")
					} else {
						jsDisplayResult("error", "error", response.text );
					}																			
			},"json");
		}
		
	} ,
	
	_declineAction	: function( action )
	{
		if( $("#response").val() == ""){
			jsDisplayResult("error", "error", "Please enter the response" );
			return false;	
		} else{
			jsDisplayResult("info", "info", "Declining  action . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=declineAction", { id : action.ref, response : $("#response").val(), contractid  :$this.options.contractId },
				function( response ){																		
					if( !response.error){
						jsDisplayResult("ok", "ok", response.text );
						$("#action_approval").dialog("destroy")
					} else {
						jsDisplayResult("error", "error", response.text );				
					}																		
				},"json");
		}
		
	} 
	
	
});