/**
 * jquery widget to show deliverables waiting and those approved 
 */
$.widget("ui.activate",{
	options	:{
		limit	 		: 10,
		start	 		: 0,		
		activate 		: false,
		idVariable		: Math.floor( Math.random()*11 ) + Math.floor( Math.random()* 3 ),
	} ,
	
	_init	: function()
	{
		this._getDeliverables();
	} , 
	
	_getDeliverables	: function()
	{
		$this 	= this;
		var ref = $this.options.idVariable;
		$.post("controller.php?action=getApprovalData",
				{
					start	 : $this.options.start, 
					limit	 : $this.options.limit
				}, function( responseData ){
					
					var columns = responseData.total					
					$($this.element).append($("<table />", {id:"activate_"+ref}).addClass("noborder")
						 .append($("<tr />")
						    .append($("<td />",{html:"<h4>Contracts awaiting activation</h4>"}).addClass("noborder"))
						    .append($("<td />",{html:"<h4>Contracts activated</h4>"}).addClass("noborder"))
						 )		
						.append($("<tr />")
							.append($("<td />").addClass("noborder")
							  .append($("<table />",{id:"awaiting_"+ref}))
							)
							.append($("<td />").addClass("noborder")
							  .append($("<table />",{id:"approve_"+ref}))		
							)
						)	
					);
					
					$this._displayHeaders( responseData.headers ,"awaiting_"+ref, ($.isEmptyObject( responseData.awaiting ) ? "" : "awaiting") );
					$this._displayHeaders( responseData.headers ,"approve_"+ref,"");
					
					if( $.isEmptyObject( responseData.approved ) ){
						$("#approve_"+ref).append($("<tr />")
							.append($("<td />",{colspan:columns, html:"There are no activated contracts"}))
						)
					} else {
						$this._displayContract( responseData.approved,"approve_"+ref, "approved" )					
					}
					
					if( $.isEmptyObject( responseData.awaiting )){	
						$("#awaiting_"+ref).append($("<tr />")
							.append($("<td />",{id:"no_activated", colspan:columns, html:"There are contract to activate"}))
						)
					} else {
						$this._displayContract( responseData.awaiting,"awaiting_"+ref, "awaiting" )
					}
					
				},"json");		
	} ,
	
	_displayHeaders		: function( headers, table, reference  )
	{		
		var tr = $("<tr />");
		
		$.each( headers , function( index, head){
			tr.append($("<th />",{html:head}))			
		});
		$(tr).append($("<th />").css({display:(reference == "awaiting" ? "table-cell" : "none")}))		
		$("#"+table).append( tr );
	} , 
	
	_displayContract		: function( contracts, table , reference )
	{	
		var tr;
		$.each( contracts, function( index, contract){
			tr = $("<tr />",{id:"tr_"+index})
			$.each( contract, function( ref, del ){
				tr.append($("<td />",{html: del }))
		})
		$(tr).append($("<td />").css({display:(reference == "awaiting" ? "table-cell" : "none")})
				.append($("<input />",{type:"submit", name:"activate_"+index, id:"activate_"+index, value:"Activate"}))
		)
		
		$("#activate_"+index).live("click", function(){
			$this._approveContract( index, contract );
			return false;
		});
					
		$("#"+table).append( tr );
	  });
	},	
		
		
	_approveContract	: function( contractId, contactObj)
	{
		var $this = this;
		jsDisplayResult("info", "info", "Activating . . . <img src='../images/loaderA32.gif' >");
		$.post("controller.php?action=approveContract", { id : contactObj.ref }, function( response ){
			if( !response.error ){
				$("#tr_"+contractId).fadeOut();	
				$("#no_activated").empty().remove();
				$("#approve_"+$this.options.idVariable)
				  .append($("<tr />",{id:"tr__"+contractId})
					 .append($("<td />",{html:contactObj.ref}))
					 .append($("<td />",{html:contactObj.contract}))					 
					 .append($("<td />",{html:contactObj.contract_type}))
					 .append($("<td />",{html:contactObj.project_comments}))
					 .append($("<td />",{html:contactObj.contract_completion_date}))
					 .append($("<td />",{html:contactObj.contract_owner}))
					 .append($("<td />",{html:contactObj.contract_status}))
					 .append($("<td />",{html:contactObj.project_value}))					 					 
				   )
				jsDisplayResult("ok", "ok",response.text);					
			} else {
				jsDisplayResult("error", "error", response.text);	
			}
		},"json");
	} 
	
	
	
});