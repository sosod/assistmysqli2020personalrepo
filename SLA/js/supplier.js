// JavaScript Document
$(function(){
	//alert()		   
	var supplierid = $("#supplierid").val();    
	if(supplierid == undefined){
		Supplier.get();		 
	}
	
	$("#savesupplier").click(function(){
		Supplier.save();
		return false;						  
	});
	
	$("#updatesupplier").click(function(){
		Supplier.update();	
		return false;
	});
	
	$("#updatesupplierstatus").click(function(){
		Supplier.changeStatus( supplierid, $("#status").val() );							
		return false;
	})
	
});

var Supplier = {
	
	name		  : "",
	shortcode     : "",
	supplier_code : "",
	category	  : "",

	get 		   : function()
	{
		$.getJSON("controller.php?action=getSuppliers", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					Supplier.display( status );	
					
			   });	
			}														  
		});			
	} , 

	update		    : function()
	{
		var that 		   = this;
		that.name 		   = $("#name").val();
		that.supplier_code = $("#supplier_code").val();
		that.shortcode     = $("#shortcode").val();
		that.category      = $("#category :selected").val();		
		var valid   	   = Supplier.isValid(that);
		var data    	   = {name:that.name, supplier_code:that.supplier_code, shortcode:that.shortcode, category:that.category, id:$("#supplierid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating supplier. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateSupply", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}														 
			}, "json")	
		} else {
			$("#loading").html(response.text)
		}		
	} , 
	
	save 		    : function()
	{
		var that 		   = this;
		that.name 		   = $("#name").val();
		that.supplier_code = $("#supplier_code").val();
		that.shortcode     = $("#shortcode").val();
		that.category      = $("#category :selected").val();
		var valid   	   = Supplier.isValid(that);
		var data    	   = {name:that.name, supplier_code:that.supplier_code, shortcode:that.shortcode, category:that.category};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving supplier. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveSupply", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.category = $("#category :selected").text();					
					Supplier.emptyFields( that );	
					data.id 	= response.id;
					data.active = 1;					
					Supplier.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}		
	} , 
	
	changeStatus    : function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating supplier. . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeSupplyStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");			
	}, 
	
	isValid 		: function(that)
	{
		if( that.name == "" ){
			return "Category name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the category short code";
		} else if( that.description == ""){
			return "Please enter the category description";
		} else if( that.type == ""){
			return "Please enter the contract type ";
		}
		return true;		
	} , 
	
	emptyFields 	: function( fields )
	{
		$.each(fields, function( index, field){
								
			$("#"+index).val("");					
		});			
	},
	
	display			: function( supplier )
	{
		$("#table_supplier")
		  .append($("<tr />",{id:"tr_"+supplier.id})
			 .append($("<td />",{html:supplier.id}))
			 .append($("<td />",{html:supplier.shortcode}))			 
			 .append($("<td />",{html:supplier.supplier_code}))
			 .append($("<td />",{html:supplier.name}))
			 .append($("<td />",{html:supplier.category}))			 
			 .append($("<td />")
				.append($("<input />",{name:"editsup_"+supplier.id, id:"editsup_"+supplier.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:((supplier.status & 4) == 4 ? "disabled" : ""), name:"deletesup_"+supplier.id, id:"deletesup_"+supplier.id, type:"submit",value:"Delete"}))				
			  )
			 .append($("<td />",{html:((supplier.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((supplier.status & 4) == 4 ? "disabled" : ""), id:"changesup_"+supplier.id, name:"changesup_"+supplier.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#editsup_"+supplier.id).live("click", function(){
			document.location.href = "edit_supplier.php?id="+supplier.id+"&";
			return false;
		  });
		  
		  $("#deletesup_"+supplier.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				Supplier.changeStatus( supplier.id, parseInt(supplier.status) + 2);
				$("#tr_"+supplier.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#changesup_"+supplier.id).live("click", function(){
			document.location.href = "change_supplier.php?id="+supplier.id+"&";
			return false;
		  });				
	}


}