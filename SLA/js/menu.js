// JavaScript Document
$(function(){
	Menu.getMenu();
});

var Menu = {
	
	updateMenu : function( id, menuname){
		$.post("controller.php?action=updateMenu", { id : id, clientname : menuname}, function( response ){
			$("#update_"+id).html("")
			if( !response.error )
			{
				jsDisplayResult("ok", "ok", response.text );	
			} else{
				jsDisplayResult("error", "error", response.text );
			}																		   
		},"json");		
	}, 
	getMenu  : function(){
		
		$.getJSON("controller.php?action=getMenu", function( response ){
			Menu.displayMenu( response );						   
		});		
	}, 
	displayMenu : function( menuitems )
	{
		$(".displayview").append($("<table />", {id : "table_menu", width:"100%"})
			.append($("<tr />")
			  .append($("<th />",{ html: "#"}))					  
			  .append($("<th />",{ html: "SLA Terminology"}))
			  .append($("<th />",{ html: "Client Terminology"}))
			  .append($("<th />",{ html: ""}))			  
			)						   
		 )
		$.each( menuitems, function( index, menu) {
			$("#table_menu")
			 .append($("<tr />", {id:"tr_"+menu.id})
			   .append($("<td />",{html:"#"+menu.id}))					   
			   .append($("<td />",{html:menu.description}))
			   .append($("<td />")
				 .append($("<input />",{value:menu.client_name, id:"val_"+menu.id, name:"val_"+menu.id, size:"50"}))		 
				)
			   .append($("<td />")
				  .append($("<span />",{id:"update_"+menu.id}))
				  .append($("<input />",{type:"submit", name:"update_"+menu.id, id:"update_"+menu.id, value:"Update"}))		 
				)			   
			 )	
			 
			 $("#update_"+menu.id).live("click", function(){	
			    jsDisplayResult("info", "info", "Updating menu . . . .<img src='../images/loaderA32.gif' />" );									 
				Menu.updateMenu( menu.id, $("#val_"+menu.id).val())
				return false;
			});
			 
		});
	}
}