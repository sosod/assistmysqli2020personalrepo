// JavaScript Document
$(function(){
var qualityweightid = $("#qualityweightid").val()
	if(typeof qualityweightid == "undefined"){
		QualityWeight.get();		
	}
	
	$("#updatestatus").click(function(){
		QualityWeight.changeStatus( qualityweightid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		QualityWeight.update();							
		return false;
	});
	
	$("#save").click(function(){
		QualityWeight.save();
		return false;
	})
			   
})

var QualityWeight	= {
	
	qweight		 : "", 
	description  : "",
	definition	 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getQualityWeights", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, qualityWeight ) {
					QualityWeight.display( qualityWeight )				   
				});
			}																 
		})
	} , 
	display 	: function( qweight )
	{
		$("#table_qualityweight")
		  .append($("<tr />",{id:"tr_"+qweight.id})
			.append($("<td />",{ html:qweight.id}))
			.append($("<td />",{ html:qweight.qweight}))
			.append($("<td />",{ html:qweight.description}))			
			.append($("<td />",{ html:qweight.definition}))
			.append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":qweight.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+qweight.id, name:"edit_"+qweight.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{id:"del_"+qweight.id, name:"del_"+qweight.id, type:((qweight.status & 4)== 4 ? "hidden" : "submit"), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((qweight.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{id:"change_"+qweight.id, name:"change_"+qweight.id, type:((qweight.status & 4) == 4 ? "hidden" : "submit"), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+qweight.id).live("click",function(){
			document.location.href = "edit_qweight.php?id="+qweight.id+"&";
			return false;
		});
		
		$("#del_"+qweight.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				QualityWeight.changeStatus( qweight.id , parseInt(qweight.status) + 2);
				$("#tr_"+qweight.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+qweight.id).live("click", function(){
			document.location.href = "change_qweight.php?id="+qweight.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.qweight  	  =	$("#qweight").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val();
		
		var valid = QualityWeight.isValid( that );
		var data  = { qweight : that.qweight, description : that.description,definition : that.definition,color:that.color  }; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving quality weight . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveQualityWeight", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id 	= response.id;
					data.status = 1
					QualityWeight.display( data );
					QualityWeight.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.qweight  =	$("#qweight").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val(); 
		
		var valid = QualityWeight.isValid( that );
		var data  = { qweight:that.qweight, description : that.description,definition : that.definition,color:that.color , id:$("#qualityweightid").val() };   		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating quality weight . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateQualityWeight", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																	 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating quality weight . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeQualityWeight", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																										 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}				  
		}); 
	} ,
	
	isValid			:function( that)
	{
		if( that.qweight == ""){
			return "Please enter the quality weight"; 	
		} else if( isNaN(that.qweight) ){
			return "Please enter valid  weight"; 	
		}  else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		}
		return true;
	}
	
	
}