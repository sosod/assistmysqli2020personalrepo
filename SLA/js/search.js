/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/3/11
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

	Search.getActiveType();
	Search.getSupplier();
	Search.getUsers();
	Search.getTemplates();
	Search.getAssessmentFrequency();
	Search.getNotificationFrequency();
	Search.getActiveCategory();

   $("#go").click(function(){
	jsDisplayResult("info", "info", "Searching . . .  . . . .<img src='../images/loaderA32.gif' />" );
    if($("#search_text").val() != ""){

     $.post("controller.php?action=searchAll", {searchText : $("#search_text").val() },function( data ){
    	 $("#result").html("");
    	 if( $.isEmptyObject( data))
         {
            jsDisplayResult("info", "info", "No results we found" );
         }  else {
    		jsDisplayResult("ok", "ok", data.total+" results found . . ." );
             $("#result").append($("<table />",{id:"contract_table"}))
            trh = $("<tr />");
            $.each( data.headers , function( index, head ){
                trh.append($("<th />",{html:head}))
            })
            $("#contract_table").append( trh )

            $.each( data.contracts , function( inx, contract ){
                tr = $("<tr />");
                $.each(contract, function( i, val){
                    if( val != null){
                        if( val.indexOf($("#search_text").val()) >= 0)
                        {
                            tr.append($("<td />",{html:"<span style='background-color: yellow;'>"+val+"</span>"}));
                        } else {
                            tr.append($("<td />",{html:val}));
                        }
                    } else {
                        tr.append($("<td />",{html:val}));
                    }
                })
                $("#contract_table").append( tr )
            })
         }


     },"json");
    } else {
    	jsDisplayResult("error", "error", "Please enter the text to search" );
    }
       return false;
   });
   
   
   $("#advancedsearch").click(function(){
	   Search.advancesSearch();
	  return false; 
   });
   
});


Search  =  {

		getActiveType 	   :    function()
	{
		$.getJSON("controller.php?action=getActiveType", function( types ){
			$.each( types, function( index, type ){
				$("#type").append($("<option />",{value:type.id, text:type.name}))
			});
		})
	} ,
	getActiveCategory :    function( id )
	{
		$.post("controller.php?action=getActiveCategory", function( categories ){
			$.each( categories, function( index, category ){
				$("#category").append($("<option />",{value:category.id, text:category.name}))
			});
		}, "json")
	},
	getSupplier 	:    function()
	{
		$.getJSON("controller.php?action=getSupplier", function( suppliers ){
			$.each( suppliers, function( index, supplier){
				$("#supplier").append($("<option />",{value:supplier.id, text:supplier.name}))
			});
		})
	} ,

	getUsers	   : function()
	{
		$.getJSON("controller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
					$("#contract_owner").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))

					$("#contract_authorisor").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))

				if( (user.status & 2) == 2 ){
					$("#contract_manager").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))
				}
			});
		});
	},

	getTemplates	 : function()
	{
		$.getJSON("controller.php?action=getTemplates", function( templates ){
			$.each( templates, function( index, template){
				$("#template_").append($("<option />",{text:template.name, value:template.id}))
			});
		});
	} ,

	getAssessmentFrequency 	: function(){

		$.getJSON("controller.php?action=getAssessmentFrequency", function( assessmentFrequency ){
			$.each( assessmentFrequency, function( index, value){
				$("#assessment_frequency").append($("<option />", { text:value.name, value:value.id}))
			});
		})

	} ,

	getNotificationFrequency		: function(){

		$.getJSON("controller.php?action=getNotificationFrequency", function( notificationFrequency ){
			$.each( notificationFrequency, function( index, value){
				$("#notification_frequency").append($("<option />", { text:value.shortcode, value:value.id}))
			});
		})
	} , 
	
	advancesSearch					: function(){
		jsDisplayResult("info", "info", "Searching . . .  . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=advancedSearch", { data : $("#advanced-search").serializeArray() }, function( responseData ){
			console.log("Coming here ")
			console.log( responseData );
			if( $.isEmptyObject( responseData ) ){
				jsDisplayResult("info", "info", "No results we found ...." );				
			} else {
				$("#contract_table").hide();
				jsDisplayResult("ok", "ok", responseData.total+" results found . . ." );
				Search.display( responseData );			
			}
		},"json")
		
		
	} , 

	display 					: function(  data ){
		
        $("#result").append($("<table />",{id:"_contract_table"}))
        var trh = $("<tr />");
        $.each( data.headers , function( index, head ){
            trh.append($("<th />",{html:head}))
        })
        $("#_contract_table").append( trh )

        $.each( data.contracts , function( inx, contract ){
            var tr = $("<tr />");
            $.each(contract, function( i, val){
               tr.append($("<td />",{html:val}));
            })
            $("#_contract_table").append( tr )
        })
	}
	



}