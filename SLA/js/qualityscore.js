// JavaScript Document
$(function(){
	var qualityscoreid = $("#qualityscoreid").val()
	if(typeof qualityscoreid == "undefined"){
		QualityScore.get();		
	}
	
	$("#updatestatus").click(function(){
		QualityScore.changeStatus( qualityscoreid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		QualityScore.update();							
		return false;
	});
	
	$("#save").click(function(){
		QualityScore.save();
		return false;
	})
			   
})

var QualityScore	= {
	
	qscore		 : "",
	description  : "",
	definition	 : "",
	color		 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getQualityScores", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, qualityScore ) {
					QualityScore.display( qualityScore )				   
				});
			}																 
		})
	} , 
	display 	: function( qscore )
	{
		$("#table_qualityscore")
		 .append($("<tr />",{ id:"tr_"+qscore.id })
			.append($("<td />",{ html:qscore.id}))
			.append($("<td />",{ html:qscore.qscore}))
			.append($("<td />",{ html:qscore.description}))
			.append($("<td />",{ html:qscore.definition}))
			.append($("<td />")
			   .append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":qscore.color}))					  
			 )
			.append($("<td />")
			 .append($("<input />",{id:"edit_"+qscore.id, name:"edit_"+qscore.id, type:"submit", value:"Edit"}))
			 .append($("<input />",{id:"del_"+qscore.id, name:"del_"+qscore.id, type:((qscore.status & 4) == 4 ? "hidden" : "submit"), value:"Delete"}))
			)
			.append($("<td />",{ html:((qscore.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		
			.append($("<td />")
			 .append($("<input />",{id:"change_"+qscore.id, name:"change_"+qscore.id, type:((qscore.status & 4) == 4 ? "hidden" : "submit"), value:"Change Status"}))		  
			)
		 )



		$("#edit_"+qscore.id).live("click",function(){
			document.location.href = "edit_qscore.php?id="+qscore.id+"&";
			return false;
		});
		
		$("#del_"+qscore.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				QualityScore.changeStatus( qscore.id , parseInt(qscore.status) + 2);
				$("#tr_"+qscore.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+qscore.id).live("click", function(){
			document.location.href = "change_qscore.php?id="+qscore.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.qscore	  	  = $("#qscore").val(); 
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val();
		
		var valid = QualityScore.isValid( that );
		var data  = { qscore : that.qscore, description : that.description,definition : that.definition,color:that.color  }; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving quality score. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveQualityScore", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id 	= response.id;
					data.status = 1
					QualityScore.display( data );
					QualityScore.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.qscore       =	$("#qscore").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val(); 
		
		var valid = QualityScore.isValid( that );
		var data  = { qscore:that.qscore, description : that.description,definition : that.definition,color:that.color , id:$("#qualityscoreid").val() };   		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating quality score. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateQualityScore", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																		 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating quality score. . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeQualityScore", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																										 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}				  
		}); 
	} ,
	
	isValid			:function( that)
	{
		if( that.qscore == ""){
			return "Please enter the quality score"; 	
		} else if( isNaN(that.qscore) ){
			return "Please enter valid quality score"; 	
		} else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		}
		return true;
	}
	
	
}