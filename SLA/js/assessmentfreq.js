// JavaScript Document
$(function(){
	var assessid = $("#assessid").val();    
	
	$("#date").live("focus", function(){
		$(this).timepicker();							  
	})
	
	if(assessid == undefined){
		AssessmentFrequency.get();		 
	}
	
	$("#save").click(function(){
		AssessmentFrequency.save();
		return false;						  
	});
	
	$("#updateassessfreq").click(function(){
		AssessmentFrequency.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		AssessmentFrequency.changeStatus( assessid, $("#status").val() );							
		return false;
	})
		   
});

var AssessmentFrequency = {
	
	name 		: "",
	description : "",
	shortcode   : "", 
	date		: "",
	
	get 		 : function()
	{
		$.getJSON("controller.php?action=getAssessmentFrequencies", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					AssessmentFrequency.display( status );					   
			   });	
			}														  
		});		
		
	} , 
	
	save		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		that.date		 = $("#date").val();
		var valid   	 = AssessmentFrequency.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, date: that.date};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving assessment frequency. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveAssessmentFrequency", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					AssessmentFrequency.emptyFields( data );		
					data.id 	= response.id;
					data.active = 1;					
					AssessmentFrequency.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );	
		}		
	} , 
	
	update		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		that.date		 = $("#date").val();
		var valid   	 = AssessmentFrequency.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, date:that.date, id:$("#assessid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating assessment frequency. . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateAssessmentFrequency", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}													 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}		
	} ,
	
	changeStatus  : function( id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating assessment frequency. . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeAssessmentFrequency", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );	
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");		
	} , 
	
	display		  : function( assessfreq )
	{	
		$("#table_assessmentfreq")
		  .append($("<tr />", {id:"tr_"+assessfreq.id})
			 .append($("<td />",{html:assessfreq.id}))
			 .append($("<td />",{html:assessfreq.shortcode}))			 
			 .append($("<td />",{html:assessfreq.name}))
			 .append($("<td />",{html:assessfreq.description}))
			 .append($("<td />",{html:assessfreq.date}))			 
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+assessfreq.id, id:"edit_"+assessfreq.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:(assessfreq.defaults == 1 ? "disabled" : ""), name:"del_"+assessfreq.id, id:"del_"+assessfreq.id, type:"submit",value:"Delete"}))				
			  )
			 .append($("<td />",{html:(assessfreq.active == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:(assessfreq.defaults == 1 ? "disabled" : ""), id:"change_"+assessfreq.id, name:"change_"+assessfreq.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+assessfreq.id).live("click", function(){
			document.location.href = "edit_assfreq.php?id="+assessfreq.id+"&";
			return false;
		  });
		  
		  $("#del_"+assessfreq.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				AssessmentFrequency.changeStatus( assessfreq.id, 2);
				$("#tr_"+assessfreq.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#change_"+assessfreq.id).live("click", function(){
			document.location.href = "change_assfreq.php?id="+assessfreq.id+"&";
			return false;
		  });		
	} , 
	
	isValid		  : function(that)
	{
		if( that.name == "" ){
			return "Contract name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the contract short code";
		} else if( that.description == ""){
			return "Please enter the contract type description";
		}
		return true;				
	} , 
	
	emptyFields   : function(fields)
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});		
	}
		
}