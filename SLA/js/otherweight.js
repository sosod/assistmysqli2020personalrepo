// JavaScript Document
$(function(){
var otherweightid = $("#otherweightid").val()
	if(typeof otherweightid == "undefined"){
		OtherWeight.get();		
	}
	
	$("#updatestatus").click(function(){
		OtherWeight.changeStatus( otherweightid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		OtherWeight.update();							
		return false;
	});
	
	$("#save").click(function(){
		OtherWeight.save();
		return false;
	})
			   
})

var OtherWeight	= {
	
	oweight		 : "",
	description  : "",
	definition	 : "",
	color		 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getOtherWeights", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, otherWeight ) {
					OtherWeight.display( otherWeight )				   
				});
			}																 
		})
	} , 
	display 	: function( oweight )
	{
		$("#table_otherweight")
		  .append($("<tr />",{id:"tr_"+oweight.id})
			.append($("<td />",{ html:oweight.id}))
			.append($("<td />",{html:oweight.oweight}) )
			.append($("<td />",{ html:oweight.description}))			
			.append($("<td />",{ html:oweight.definition}))
			.append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":oweight.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+oweight.id, name:"edit_"+oweight.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{id:"del_"+oweight.id, name:"del_"+oweight.id, type:((oweight.status & 4) == 4 ? "hidden" : "submit"), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((oweight.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{id:"change_"+oweight.id, name:"change_"+oweight.id, type:((oweight.status & 4)== 4 ? "hidden" : "submit"), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+oweight.id).live("click",function(){
			document.location.href = "edit_oweight.php?id="+oweight.id+"&";
			return false;
		});
		
		$("#del_"+oweight.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				OtherWeight.changeStatus( oweight.id , parseInt(oweight.status) + 2);
				$("#tr_"+oweight.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+oweight.id).live("click", function(){
			document.location.href = "change_oweight.php?id="+oweight.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.oweight  	  =	$("#oweight").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val();
		
		var valid = OtherWeight.isValid( that );
		var data  = { oweight : that.oweight, description : that.description,definition : that.definition,color:that.color  }; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving other weight . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveOtherWeight", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id 	= response.id;
					data.status = 1
					OtherWeight.display( data );
					OtherWeight.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.oweight  	  =	$("#oweight").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val(); 
		
		var valid = OtherWeight.isValid( that );
		var data  = { oweight:that.oweight, description : that.description,definition : that.definition,color:that.color , id:$("#otherweightid").val() };   		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating other weight . . . .<img src='../images/loaderA32.gif' />" );			
			$.post("controller.php?action=updateOtherWeight", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																	 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating other weight . . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeOtherWeight", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																									 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}				  
		}); 
	} ,
	
	isValid			:function( that)
	{
		if( that.oweight == ""){
			return "Please enter the other weight "; 	
		} else if( isNaN(that.oweight) ){
			return "Please enter valid other weight"; 	
		} else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		}
		return true;
	}
	
	
}

