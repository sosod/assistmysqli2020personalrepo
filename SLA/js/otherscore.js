// JavaScript Document
$(function(){
	var otherscoreid = $("#otherscoreid").val()
	if(typeof otherscoreid == "undefined"){
		OtherScore.get();		
	}
	
	$("#updatestatus").click(function(){
		OtherScore.changeStatus( otherscoreid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		OtherScore.update();							
		return false;
	});
	
	$("#save").click(function(){
		OtherScore.save();
		return false;
	})
			   
})

var OtherScore	= {
	
	oscore		 : "",
	description  : "",
	definition	 : "",
	color		 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getOtherScores", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html();
			    jsDisplayResult("info", "info", "No data returned");
			} else {
				$.each( data, function( index, otherScore ) {
					OtherScore.display( otherScore )				   
				});
			}																 
		})
	} , 
	display 	: function( oscore )
	{
		$("#table_otherscore")
		  .append($("<tr />",{id: "tr_"+oscore.id})
			.append($("<td />",{ html:oscore.id}))
			.append($("<td />",{html:oscore.oscore}))
			.append($("<td />",{ html:oscore.description}))			
			.append($("<td />",{ html:oscore.definition}))
			.append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":oscore.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+oscore.id, name:"edit_"+oscore.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{id:"del_"+oscore.id, name:"del_"+oscore.id, type:((oscore.status & 4) == 4 ? "hidden" : "submit"), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((oscore.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{id:"change_"+oscore.id, name:"change_"+oscore.id, type:((oscore.status & 4) == 4 ? "hidden" : "submit"), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+oscore.id).live("click",function(){
			document.location.href = "edit_oscore.php?id="+oscore.id+"&";
			return false;
		});
		
		$("#del_"+oscore.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				OtherScore.changeStatus( oscore.id , parseInt(oscore.status) + 2);
				$("#tr_"+oscore.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+oscore.id).live("click", function(){
			document.location.href = "change_oscore.php?id="+oscore.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.oscore  	  =	$("#oscore").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val();
		
		var valid = OtherScore.isValid( that );
		var data  = { oscore : that.oscore, description : that.description,definition : that.definition,color:that.color  }; 
		if( valid == true ){
			$.post("controller.php?action=saveOtherScore", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id 	= response.id;
					data.status = 1
					OtherScore.display( data );
					OtherScore.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.oscore       =	$("#oscore").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val(); 
		
		var valid = OtherScore.isValid( that );
		var data  = { oscore:that.oscore, description : that.description,definition : that.definition,color:that.color , id:$("#otherscoreid").val() };   		
		if( valid == true ){
			$.post("controller.php?action=updateOtherScore", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																	 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating other score . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeOtherScore", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																										 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}				  
		}); 
	} ,
	
	isValid			:function( that)
	{
		if( that.oscore == ""){
			return "Please enter the other score"; 	
		} else if( isNaN(that.oscore) ){
			return "Please enter valid other score"; 	
		} else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		}
		return true;
	}

}