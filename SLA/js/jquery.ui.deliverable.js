// JavaScript Document
$.widget("ui.deliverable", {
		 
	options : {
		addAction			: false,
		editAction			: false,
		updateAction		: false,
		approveAction		: false,		
		editDeliverable 	: false,
		updateDeliverable 	: false,
		approveDeliverable	: false,
		deleteDeliverable	: false,
		approveAction		: false,	
		authorizeDeliverable: false,
		authorizeActions	: false,	
		assesment			: false,		
		required			: false,
		editable			: false,		
		start				: 0,
		limit				: 10,
		current				: 1,
		total 				: 0 , 
		element				: "",
		contractId 			: "",
		urlLocation			: "",
		url 				: "controller.php?action=getDeliverables",
		page				: ""
	},
	
	_init  		 : function(){

		
		if(this.options.assessment)
		{
			this._displayCategorised();			
		} else{
			this._getDeliverable();			
		}
	},
	
	_create 		 : function(){
	} ,

	_getDeliverable : function()
	{
		$this = this;
		$this.element.append($("<div />",{id:"loader"}));
		$("#loader").ajaxStart(function(){
			$(this).html("Loading . . <img src='../images/loaderA32.gif' />")
					.css({	
							"background-color":"", 
							"position"  :"absolute",
							"z-index"	:"9999",
							"top"		: "200px",
							"left"		: "500px"
						 })				 
		});
		$("#loader").ajaxComplete(function(){
			$(this).empty();					 
		});	
		$.post( $this.options.urlLocation+""+$this.options.url, { 
			   	start 	: $this.options.start,
				limit 	: $this.options.limit,
				id	  	: $this.options.contractId,
				request : $this.options.page
				},
			   function( data ){		 

				   if( $.isEmptyObject( data.deliverables )){
					 $this.element.html("There are no deliverable added yet");   
				   } else {
					  $("#project_name").html( "Project Name : "+data.contract );
					 $this.options.total = data.total
					 $this.element.html("");
					 $this.element.append($("<table />",{ id:"deliverable_table" , width:"100%", border:"6"}))	
					 $this._createPager( data.total, data.columns, data.contractDefs  )		
					 $this._displayHeaders( data.headers, data.columns )			
					 $this._displayDeliverable( data.deliverables, data.columns, data.contractDefs )
				  }
		}, "json");	
	} , 
	
	_displayDeliverable	: function( deliverables, columns, contractDefs)
	{
		$this = this;
		var colspan = 6;
		var cols = 0;

		var dontDisplay = ["hasAccess", "hasActionAccess"];
		if( $this.options.page == "update"){
			dontDisplay.push( "deliverable_category" );
			colspan = colspan + 1;
		} else if( $this.options.page == "approve" ){
			dontDisplay.push( "deliverable_category" );
			colspan 	= colspan + 1;		
		} else if($this.options.page == "adddeliverable") {
			dontDisplay.push( "deliverable_status" );
			colspan = colspan ;			
		} else if($this.options.page == "addaction") {
			colspan = colspan + 1;			
		} else if( $this.options.page == "template"){
			dontDisplay.push( "deliverable_status" );
			colspan = colspan ;		
		} else if( $this.options.page == "view_contract"){
			colspan = colspan + 1;		
		} else if( $this.options.page == "editdeliverable"){
			dontDisplay.push( "deliverable_status" );			
			colspan = colspan ;		
		} else if( $this.options.updateDeliverable == true && $this.options.page == "updatedeliverables"){			
			colspan = colspan + 1;			
		} else if( $this.options.page == "authorization"){			
			colspan = colspan + 1;			
		}  else {
			dontDisplay.push( "deliverable_status" );	
		}

		var tr_subdev;
		//var delivs =  JSON.parse( deliverables );
		//$this.element.append( deliverables )
		var totalDW = 0;
		var totalQW = 0;
		var totalOW = 0;
		var totalA = 0; // total number of actions
		var totalP = 0; //totla progressi
		var i = 0;

		$.each( deliverables, function( index, deliverable){		

			if(deliverable.subd == "yes" ){		
				tr = $("<tr />")		
				$.each( deliverable.subarray, function( inx, subDel){
					
					var tr_sub = "";
					tr_sub = $("<tr />",{id:"tr_"+inx}).css({"background-color":"#CCCCCC", "text-align":"right"})
						$.each( subDel , function(i, val){	  
							if( $.inArray(i, dontDisplay) >= 0)
							{
							
							} else {
								
								if( i == "number_of_actions"){
										
										//display actions for this deliverable on hover
										tr_sub.append($("<td />",{html:val})
											.hover( function(){
											$("#display_actions_"+subDel['ref']).remove();
											$(this).append($("<div />",{id:"display_actions_"+subDel['ref']})
															 .addClass("displayActions")
															 .css({top:$(this).position().top+20, left:$(this).position().left-20})
															 )
												   
												$.getScript("../js/jquery.ui.action.js", function(){
													$("#display_actions_"+subDel['ref']).action({delId:subDel['ref'], urlLocation: "../new/"}).show();	
												});					 
											}, function(){
												$("#display_actions_"+subDel['ref']).hide();
											})
											.css({"text-align":"right"})
										)
		
									} else {					  
										tr_sub.append($("<td />",{html:val}))
									}
							}

						});	
					$("#deliverable_table").append( tr_sub )
					$this._createButtons( tr_sub, subDel, false)					
				});

			} else {
				i++;
				numberOfAction = deliverable.number_of_actions.replace("<span class='semi'>", "");
				numberOfAction = numberOfAction.replace("</span>", "");
				numberOfAction = numberOfAction.replace("&nbsp;&nbsp;&nbsp;&nbsp;", "");				
				numberOfAction = numberOfAction.replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "");	
				numberOfAction = numberOfAction.replace("<span class='success'>", "");					
				
				
				totalDW = parseInt(totalDW) + parseInt( deliverable.delivered_weight ); 
				totalQW = parseInt(totalQW) + parseInt( deliverable.quality_weight );
				totalOW = parseInt(totalOW) + parseInt( deliverable.other_weight );
				totalA	= parseInt(totalA) + parseInt( numberOfAction );

				totalP	= parseFloat(totalP) + parseFloat( deliverable.action_progress );				
				tr = $("<tr />",{id:"tr_"+index})
				$.each( deliverable , function( key, value){
					if( key == "main_with_sub" || key == "subd"){
						//continue;
					} else {
						if( $.inArray(key, dontDisplay) >= 0)
						{
							
						} else {
							if( key == "number_of_actions"){
									//display actions for this deliverable on hover
									tr.append($("<td />",{html:value})
										.hover( function(){
										$("#display_actions_"+deliverable['ref']).remove();
										$(this).append($("<div />",{id:"display_actions_"+deliverable['ref']})
														 .addClass("displayActions")
														 .css({top:$(this).position().top+20, left:$(this).position().left-20})
														 )
											   
											$.getScript("../js/jquery.ui.action.js", function(){
												$("#display_actions_"+deliverable['ref']).action({delId:deliverable['ref'], urlLocation: "../new/"}).show();	
											});					 
										}, function(){
											$("#display_actions_"+deliverable['ref']).hide();
										})
									)
	
							} else {					  
								tr.append($("<td />",{html:value}))	
							}
						}
						//tr.append($("<td />",{html:value}))					 										 					
					}
				});
				$("#deliverable_table").append( tr )
				$this._createButtons( tr, deliverable, deliverable.main_with_sub)				
			}	
		});
		/*
		if( $this.options.page == "update"){
			colspan	= colspan + 1
		} else if( $this.options.page == "approve" ){
			colspan = colspan + 3 
		} else if( $this.options.page == "adddeliverable") {
			colspan = colspan+2; 
		}	else if( $this.options.page == "addaction" ) {
			colspan = colspan+2; 
		}*/	 		
		cols  = colspan;
		if((contractDefs & 8) == 8 ){
			colspan = parseInt( colspan ) + 1;
		}
		if((contractDefs & 16) == 16 ){
			colspan = parseInt( colspan ) + 1;
		}		
		if((contractDefs & 32) == 32 ){
			colspan = parseInt( colspan ) + 1;
		}		
		$("#deliverable_table")
			.append($("<tr />")
			   .append($("<td />",{colspan:cols}))	   
			   .append($("<td />",{html:"<b>"+($this.options.page == "confirmation" ? "" : Math.round((isNaN(totalA) ? "0" : totalA)))+"</b>"})
				  .css("display" , ($this.options.page !== "confirmation"  ? "block" : "none"))
				)				   
			   .append($("<td />",{html:"<b>"+($this.options.page == "confirmation" ? "" : Math.round(parseFloat(totalP/i)))+"</b>"})
				  .css("display" , ($this.options.page !== "confirmation"  ? "block" : "none"))
				)			   
			   .append($("<td />",{html:""}).css("display" , ($this.options.page == "approve"  ? "none" : "block")))	
			   .append($("<td />",{html:"<b>"+($this.options.page == "update" ? "" : totalDW)+"</b>"})
				  .css("display" , ((contractDefs & 8) == 8 ? "block" : "none"))
				)
			   .append($("<td />",{html:"<b>"+totalQW+"</b>"})				
				  .css({"display":($this.options.page == "update" ? "none" : ((contractDefs & 16) == 16 ? "table-cell" : "none"))})
				)
			   .append($("<td />",{html:"<b>"+totalOW+"</b>"})						 
				  .css({"display":($this.options.page == "update" ? "none" : ((contractDefs & 32) == 32 ? "table-cell" : "none"))})
				)
			   .append($("<td />",{colspan:"2"}).css({"display":($this.options.page == "update" ? "none" : "table-cell")}))		   
			)

	} , 
	
	_displayCategorised 		: function()
	{
		$this = this;
		$this.element.append($("<div />",{id:"loader"}));
		$("#loader").ajaxStart(function(){
			$(this).html("Loading . . <img src='../images/loaderA32.gif' />")
					.css({	
							"background-color":"", 
							"position"  :"absolute",
							"z-index"	:"9999",
							"top"		: "200px",
							"left"		: "500px"
						 })				 
		});
		$("#loader").ajaxComplete(function(){
			$(this).empty();					 
		});	
		
		$.post("controller.php?action=getContractDetail", {
			   	id : $this.options.contractId 	
			   } , function( data ) {				 
			   	  $this.options.total = data.total;
				
				  if( $.isEmptyObject( data.contract ) )
				  {
					$("#table_contract")	
					 .append($("<tr />")
						.append($("<td/>",{colspan:"2", html:"An error occured getting contract details"}))				
					  )	
				  } else {
					$this._displayContract( data.contract );  
				  }
				  
				  if( $.isEmptyObject( data.deiverable ) )
				  {
					$("#view_delivereable").append($("<span/>",{html:"There are no deliverables for this contract"}))				
				  } else {
					$this._displayCategorisedDetail( data.deiverable );  
				  }
				  
				}
		, "json")
		
	} ,
	
	 _displayContract		: function( contract , id)
	{
		$("#table_contract")	
		 .append($("<tr />")
			.append($("<td/>",{colspan:"2", html:"<h3>"+($this.options.template ? " Edit Template" : "Step 5 : Confrimation Of SLA")+"</h3>" }))				
		  )
		 .append($("<tr />")
			.append($("<th />",{html:"Ref :"}))
			.append($("<td />",{html:id}))					
		  )
		$.each( contract, function( index, contr ){
			
			if( contr.colName == "Attachment"){					   
				//do nothing
			} else{
				$("#table_contract")	
				 .append($("<tr />")
					.append($("<th />",{html:contr.colName+" :"}))
					.append($("<td />",{html:contr.value}))					
				  )				
			}
		});
	} , 
	
	_displayCategorisedDetail		: function( deliverables )
	{
		var $this = this;
		$("#view_delivereable").append($("<table />",{id:"deliverable_data"}))
		
		var tr 		 ;	
		var table_assess = $("<table />",{border:"2",id:"table_assessment"});
		var rowspan = 1;
		$.each( deliverables, function( category, catDeliverable){
			var tr_asses= "";
			$("#deliverable_data").append($("<tr />")
									.append($("<td />",{html:category}))									
			)	
			$.each( catDeliverable, function( ref, mainDeliverable){	
			tr_asses = $("<tr />");											 
			tr 		= $("<tr />")				
			tr = tr.append($("<td />",{html:mainDeliverable['ref']}))
				   .append($("<td />",{html:mainDeliverable['deliverable']}))
				   .append($("<td />",{html:mainDeliverable['category']}))
				   .append($("<td />",{html:mainDeliverable['type']}))
				   .append($("<td />",{html:mainDeliverable['owner']}))					   
			
				if( !$.isEmptyObject(mainDeliverable['assessment'])){
					$("#table_assessment").append($("<tr />")
													.append($("<td />",{html:mainDeliverable['assessment']}))
																									  
					)
					tr = tr.append($("<td />",{id:"td_assessment", rowspan:$this.options.total})
													 .append( table_assess )				 
									)	
				}
				
				
				$("#deliverable_data").append( tr )
			});
	
	    });

	} , 
	
	_outputdeliverable		:function( deliverableObj, type ){
		/*var tr = $("<tr />")
			.append($("<td />",{html:deliverableObj['ref']}))				  
			.append($("<td />",{html:deliverableObj['deliverable']}))		  
			.append($("<td />",{html:deliverableObj['owner']}))		  					
			.append($("<td />",{html:deliverableObj['deadline']}))		  					
			.append($("<td />",{html:""}))	
			.append($("<td />"))
			//.append($("<td />",{html:deliverableObj['status']}))
			//.append($("<td />",{html:deliverableObj['progress']}))					
		*/
		$("#asses_"+deliverableObj['ref']).live("click", function(){
			var dialog = $("<div />",{id:"div_"+deliverableObj['ref']})
						  .append($("<form />",{id:"form_"+deliverableObj['ref']})
							.append($("<table />",{border:"0"}).css({"border-collapse":"collapse", "border-color":"#FFFFFF"})								
								.append($("<tr />")		  										  
								  .append($("<td />",{html:"Delivered Comment :"}))		  
								  .append($("<td />")
									 .append($("<tr />")
										.append($("<td />",{html:"Total :"})
										   .append($("<select />",{name:"delivered_total"}).addClass("selectless")
											  .append($("<option />",{value:"1", text:"1"}))
											  .append($("<option />",{value:"2", text:"2"}))													  											  .append($("<option />",{value:"3", text:"3"}))
											  .append($("<option />",{value:"4", text:"4"}))											  											  .append($("<option />",{value:"5", text:"5"}))
										   )		  
										 )      
									  )	
									 .append($("<tr />")
										.append($("<td />")
											.append($("<textarea />",{name:"delivered_comment", id:"delivered_comment"}))  
										)      
									 )											 
								  )		  
								)						
								.append($("<tr />") 										  
								  .append($("<td />",{html:"Quality Comment : "}))		  
								  .append($("<td />")
									 .append($("<tr />")
										.append($("<td />",{html:"Total :"})
										   .append($("<select />",{name:"quality_total"}).addClass("selectless")
											  .append($("<option />",{value:"1", text:"1"}))
											  .append($("<option />",{value:"2", text:"2"}))													  											  .append($("<option />",{value:"3", text:"3"}))
											  .append($("<option />",{value:"4", text:"4"}))											  											  .append($("<option />",{value:"5", text:"5"}))
										   )		  
										 )      
									  )	
									 .append($("<tr />")
										.append($("<td />")
											.append($("<textarea />",{name:"quality_comment", id:"quality_comment"}))  
										)      
									 )											 
								  )		  
								)
								.append($("<tr />")	 										  
								  .append($("<td />",{html:"Other Comment : "}))		  
								  .append($("<td />")
									 .append($("<tr />")
										.append($("<td />",{html:"Total :"})
										   .append($("<select />",{name:"other_total"}).addClass("selectless")
											  .append($("<option />",{value:"1", text:"1"}))
											  .append($("<option />",{value:"2", text:"2"}))													  											  .append($("<option />",{value:"3", text:"3"}))
											  .append($("<option />",{value:"4", text:"4"}))											  											  .append($("<option />",{value:"5", text:"5"}))
										   )		  
										 )      
									  )	
									 .append($("<tr />")
										.append($("<td />")
											.append($("<textarea />",{name:"other_comment", id:"other_comment"}))  
										)      
									 )											 
								  )		  
								)
							  .append($("<tr />",{id:"tr_"+deliverableObj['ref']}).css({"display":"none"})
								.append($("<td />",{colspan:"2"})
									.append($("<div />",{id:"response_"+deliverableObj['ref']}))
								)		
							  )								   
							 )		
							)
							.dialog(
								  {
								   autoOpen	: true,	  
								   modal	: true,
								   width	: "auto",
								   hieght	: "500px",
								   title	: "Assessment", 
								   buttons	: {
									   		"Save" 		: function()
											{
												$("#tr_"+deliverableObj['ref']).show()
												$("#response_"+deliverableObj['ref'])
												 .html(" Saving Assessement . . <img src='../images/loaderA32.gif' /> ")														
												$.post("controller.php?action=saveAssesment",
												{
													id 		 : deliverableObj['ref'],
													formData : $("#form_"+deliverableObj['ref']).serializeArray()
												}, function( response ){	
											$("#response_"+deliverableObj['ref']).html( response.text )
											
											$("#td_"+deliverableObj['ref']).empty()
												},"json")														
											} , 
											"Close"	: function()
											{
												$(this).dialog("destroy");													
											}
									   }
								   })		
			
		});
		
	} ,
	_outputactions		:function( action, type ){
	$("#deliverable_data")
		.append($("<tr />")	
			.append($("<td />",{html:action['action']}))		  
			.append($("<td />",{html:action['action_owner']}))		  					
			.append($("<td />",{html:action['deadline']}))		  					
			.append($("<td />",{html:""}))		  					
			.append($("<td />",{html:action['status']}))
			//.append($("<td />",{html:action['total']}))					
			//.append($("<td />",{html:action['total']}))										
			.css({"background-color":(type == "shade" ? "#CCCCCC" : ""), "text-align":"right"})	
	  )
	} ,
	
	_createButtons		: function( tableRow, fieldObj , type ){
		var fieldId = fieldObj.ref;
		$this = this;
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "add_"+fieldId, id:"add_"+fieldId, value:"Add Action"}))
			 .css({display:(($this.options.addAction == true &&  type == false) ? "block" : "none")})
			)
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "edit_"+fieldId, id:"edit_"+fieldId, value:"Edit Deliverable"}))
			 .css({display:($this.options.editDeliverable ? "block" : "none")})		   
			)
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "edit_actions_"+fieldId, id:"edit_actions_"+fieldId, value:"Edit Actions"}))
			 .css({display:(($this.options.editAction == true &&  type == false) ? "block" : "none")})		   
			)
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "delete_"+fieldId, id:"delete_"+fieldId, value:"Delete"}))
			 .css({display:($this.options.deleteDeliverable ? "block" : "none")})		   
			)			
			$(tableRow).append($("<td />")
			 .append($("<input />",{ type 	  : "submit",
					   				 name 	  : "update_"+fieldId,
									 id	  	  : "update_"+fieldId,
									 value	  : "Update Deliverable", 
									 disabled :(fieldObj.hasAccess == 1  ? "" : "disabled")
									 }))
			 .css({display:($this.options.updateDeliverable ? "block" : "none")})		   
			)
			$(tableRow).append($("<td />")
			 .append($("<input />",{ type : "submit",
					   				 name : "update_actions_"+fieldId,
									 id	  : "update_actions_"+fieldId, 
									 value: "Update Action",
									 disabled:(fieldObj.hasActionAccess == 1  ? "" : "disabled")								 
									 }))
			 .css({display:(($this.options.updateAction &&  type == false) ? "block" : "none")})		   
			)			
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"button", name: "approve_"+fieldId, id:"approve_"+fieldId, value:"Approve Deliverable"}))
			 .css({display:($this.options.approveDeliverable ? "block" : "none")})			
			)	
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "approve_actions_"+fieldId, id:(fieldObj.action_progress < 100 ? "no_action" : "approve_actions_"+fieldId), value:(fieldObj.action_progress < 100 ? "No Actions due to approve"  : "Approve Actions"), disabled :(fieldObj.action_progress < 100 ? "disabled"  : "") }))
			  .css({display:(($this.options.approveAction &&  type == false) ? "block" : "none")})		
			)	
		
			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "authorize_deliverable_"+fieldId, id:"authorize_deliverable_"+fieldId, value:"Authorize Deliverable"}))
			  .css({display:(($this.options.authorizeDeliverable &&  type == false) ? "block" : "none")})		
			)	

			$(tableRow).append($("<td />")
			 .append($("<input />",{type:"submit", name: "authorize_actions_"+fieldId, id:"authorize_actions_"+fieldId, value:"Authorize Actions"}))
			  .css({display:(($this.options.authorizeActions &&  type == false) ? "block" : "none")})		
			)	
				
			$(tableRow).append($("<td />")
			 .append($("<select />",{name:"required_"+fieldId})
				.append($("<option />",{text:"Yes", value:"1", selected:"selected"}))
				.append($("<option />",{text:"No", value:"0"}))				
			 )
			 .css({display:($this.options.required ? "block" : "none")})		   
			)			
			$(tableRow).append($("<td />")
			 .append($("<select />",{name:"editable_"+fieldId})
				.append($("<option />",{text:"Yes", value:"1"}))
				.append($("<option />",{text:"No", value:"0", selected:"selected"}))				
			  )
			 .css({display:($this.options.editable ? "block" : "none")})			
			)	

			$("#authorize_deliverable_"+fieldId).live("click", function(){
				document.location.href = "authorize_deliverable.php?id="+fieldId+"&";
				return false;
			});		

			$("#authorize_actions_"+fieldId).live("click", function(){
				document.location.href = "authorize_actions.php?id="+fieldId+"&";
				return false;
			});		
			
			$("#approve_actions_"+fieldId).live("click", function(){
				document.location.href = "approve_actions.php?id="+fieldId+"&";
				return false;
			});			
			
			$("#add_"+fieldId).live("click", function(){
				document.location.href = "add_action.php?id="+fieldId+"&";
				return false;
			});
			
			$("#edit_"+fieldId).live("click", function(){
				document.location.href = "../manage/edit_deliverable.php?id="+fieldId+"&contractid="+$this.options.contractId+"&page="+$this.options.page
				return false;
			});
			
			$("#edit_actions_"+fieldId).live("click", function(){
				document.location.href = "../manage/edit_actions.php?id="+fieldId+"&contractid="+$this.options.contractId+"&page="+$this.options.page;
				return false;
			});			
			
			$("#update_actions_"+fieldId).live("click", function(){
				document.location.href = "../manage/update_actions.php?id="+fieldId+"&contractid="+$this.options.contractId;
				return false;
			});			
						
			$("#delete_"+fieldId).live("click", function(){
				if(confirm("Are you sure you want to delete this deliverable")){
					$.post("controller.php?action=deleteDeliverable", {
						   id 		: fieldId   }, function( response ){
						if( !response.error ){
							$("#tr_"+fieldId).fadeOut();
							$(tableRow).remove()
							$("#loading").html( response.text );
						} else {
							$("#loading").html( response.text );	
						}																					
					},"json");
				}													 
			})
			
			$("#approve_"+fieldId).live("click", function(){
				document.location.href = "approve_deliverable.php?id="+fieldId+"&";
				return false;
			});
			
			$("#update_"+fieldId).live("click", function(){
				document.location.href = "update_deliverable.php?id="+fieldId+"&";
				return false;
			});	
	} ,
	
	_displayHeaders		: function( headers , columns)
	{
		var dontDisplay = [];
		var pages		= ["update", "approve"]

		if( $this.options.page == "update"){
			dontDisplay.push( "deliverable_category" );
		} else if( $this.options.page == "approve" ){
			dontDisplay.push( "deliverable_category" );
		} else if( $this.options.page == "adddeliverable"){
			dontDisplay.push( "deliverable_status" );			
		} else if( $this.options.page == "template"){
			dontDisplay.push( "deliverable_status" );		
		} else if( $this.options.page == "editdeliverable"){
			dontDisplay.push( "deliverable_status" );			
		} else {
			
		}
		
		tr = $("<tr />")
		$.each( headers, function( index, header){
								  
			if( $.inArray(index, dontDisplay) >= 0 || header == "" ){
			
			} else {
				tr = tr.append($("<th />",{html:header}));				  
			}					  
		});
		if( $this.options.page == "template")
		{
			tr = tr.append($("<th />",{html:"Required"}));
			tr = tr.append($("<th />",{html:"Editable"}));
		}
		tr = tr.append($("<th />",{colspan:"2", html:"&nbsp;"}).css({"display":($this.options.page == "editdeliverable" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{colspan:"2", html:"&nbsp;"}).css({"display":($this.options.page == "update" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{colspan:"2", html:"&nbsp;"}).css({"display":($this.options.page == "approve" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{colspan:"2", html:"&nbsp;"}).css({"display":($this.options.page == "adddeliverable" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "addaction" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.authorizeDeliverable ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.authorizeActions ? "table-cell" : "none")}))		
		$("#deliverable_table").append( tr );
	} ,
	
	_createPager 		: function(total, columns , contractDefs) {
		var rk 		= this;
		
		if((contractDefs & 8) == 8 ){
			columns = parseInt( columns ) + 1;
		}
		if((contractDefs & 16) == 16 ){
			columns = parseInt( columns ) + 1;
		}		
		if((contractDefs & 32) == 32 ){
			columns = parseInt( columns ) + 1;
		}	
		
		var pages;
		if( total%rk.options.limit > 0){
			pages   = Math.ceil(total/rk.options.limit); 				
		} else {
			pages   = Math.floor(total/rk.options.limit); 				
		}
		$("#deliverable_table")
		.append($("<tr/>")
			.append($("<td />",{colspan:columns})
			  .append($("<input />",{type:"button", value:" |< ", id:"first", name:"first", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
			  .append("&nbsp;")
			  .append($("<input />",{type:"button", value:" < ", id:"previous", name:"previous", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))				  
			  .append("&nbsp;&nbsp;&nbsp;")
			  .append($("<span />", {colspan:"2",html:"Page "+rk.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages), align:"center"}))	
			  .append("&nbsp;&nbsp;&nbsp;")
			  .append($("<input />",{type:"button", value:" > ", id:"next", name:"next", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))
			  .append("&nbsp;")				  
			  .append($("<input />",{type:"button", value:" >| ", id:"last", name:"last", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))				  
			 )											
			//.append($("<td />",{colspan:columns-4}))		   
			)
		
		$("#next").bind("click", function(evt){
			rk._getNext( rk );
		});
		$("#last").bind("click",  function(evt){
			rk._getLast( rk );
		});
		$("#previous").bind("click",  function(evt){
			rk._getPrevious( rk );
		});
		$("#first").bind("click",  function(evt){
			rk._getFirst( rk );
		});			

	} ,		
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getDeliverable();
	},	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getDeliverable();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getDeliverable();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getDeliverable();				
	} 
		 
});