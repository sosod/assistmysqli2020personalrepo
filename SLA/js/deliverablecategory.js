// JavaScript Document
$(function(){
	var catid = $("#catid").val();    
	if(catid == undefined){
		DeliverableCatergory.get();		 
	}
	
	$("#savecat").click(function(){
		DeliverableCatergory.save();
		return false;						  
	});
	
	$("#updatecat").click(function(){
		DeliverableCatergory.update();	
		return false;
	});
	
	$("#updatecatstatus").click(function(){
		DeliverableCatergory.changeStatus( catid, $("#status").val() );							
		return false;
	})
	
});

var DeliverableCatergory = {
	
	name 		: "",
	shortcode   : "",
	description : "",
	
	get 		   : function()
	{
		$.getJSON("controller.php?action=getDeliverableCatergories", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					DeliverableCatergory.display( status );	
			   });	
			}														  
		});			
	} , 

	update		    : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();	
		var valid   	 = DeliverableCatergory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, id:$("#catid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating deliverable category . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateDeliverableCategory", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}														 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid);
		}		
	} , 
	
	save 		    : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		var valid   	 = DeliverableCatergory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving deliverable category . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveDeliverableCategory", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.type = $("#type :selected").text();
					DeliverableCatergory.emptyFields( data );		
					data.id 	= response.id;
					data.status = 1;		
					DeliverableCatergory.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );					
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}		
	} , 
	
	changeStatus    : function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating deliverable category . . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeDeliverableCategoryStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																						
		}, "json");			
	}, 
	
	isValid 		: function(that)
	{
		if( that.name == "" ){
			return "Category name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the category short code";
		} else if( that.description == ""){
			return "Please enter the category description";
		} 
		return true;		
	} , 
	
	emptyFields 	: function( fields )
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});			
	},
	
	display			: function( category )
	{
		$("#table_deliverablecatergory")
		  .append($("<tr />",{id:"tr_"+category.id})
			 .append($("<td />",{html:category.id}))
			 .append($("<td />",{html:category.shortcode}))			 
			 .append($("<td />",{html:category.name}))
			 .append($("<td />",{html:category.description}))			 
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+category.id, id:"edit_"+category.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:((category.status & 4) == 4 ? "disabled" : ""), name:"delete_"+category.id, id:"delete_"+category.id, type:"submit", value:"Delete"}))				
			  )
			 .append($("<td />",{html:((category.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((category.status & 4) == 4 ? "disabled" : ""), id:"change_"+category.id, name:"change_"+category.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+category.id).live("click", function(){
			document.location.href = "edit_deliverable.php?id="+category.id+"&";
			return false;
		  });
		  
		  $("#delete_"+category.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				DeliverableCatergory.changeStatus( category.id, parseInt(category.status) + 2);
				$("#tr_"+category.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#change_"+category.id).live("click", function(){
			document.location.href = "change_deliverablecategorystatus.php?id="+category.id+"&";
			return false;
		  });				
	}


}