// JavaScript Document
$.widget("ui.contract", {
	options : {
		headers 			: [] ,
		limit 				: 10, 
		start				: 0,
		current				: 1,
		addDeliverable		: false,
		editContract		: false,
		updateContract		: false,
		approveContract		: false,
		activateContract	: false,
		assessmentContract	: false,		
		editDeliverable		: false,
		editAction			: false,	
		addAction			: false,
		updateDeliverable	: false,
		contractTemplate	: false,
		contractAuthorization : false,	
		deliverableAuthorization : false,	
		view				: false,
		contractId			: "",
		page				: "",
		goto				: "",
		urlLocation			: "",		
		template 			: false
	}, 
	
	_init 			: function(){
		var $this = this;
		if( $this.options.goto == "categorized"){
			$this._getContractDetail( $this.options.contractId );
		} else{
			$this._getContract();			
		}
	}, 
	
	_create 		: function(){
		
	} ,
	
	
	_getContract    : function(){
		$this = this;
		//var loader = $(this.element).append($("<div />",{id:"loading"}));
		//$("#loading").ajaxStart(function(){
		$("<div />",{id:"init_loading"}).html("Loading . . <img src='../images/loaderA32.gif' />")
				.css({	
						"background-color":"", 
						"position"  :"absolute",
						"z-index"	:"9999",
						"top"		: "200px",
						"left"		: "500px"
					 })				 

		$.post("controller.php?action=getContracts", {
			   		start	 : $this.options.start, 
					limit	 : $this.options.limit,
					template : $this.options.template,
					page	 : $this.options.page
				}, function( data ){
			$("#init_loading").empty();			
			if( $.isEmptyObject( data.contractArr ) ){
				if( $this.options.page == "assessment"){
					$this.element.html("There are no assessments due");
				} else {
					$this.element.html("There are no contracts yet");
				}
			} else{
				data.headers[''] = "";
			    $this.options.total = data.total
				$("#contractview").html("");
				$("#contractview").append($("<table />",{ id:"contract_table" , width:"100%"}))			
				$this._diplaySelect( data.headCount );
				$this._populateContract( data.contractArr );
				$this._createPager( data.total, data.headCount, "contract_table");
				$this._diplayHeaders( data.headers , data.headCount, "contract_table" );
				$this._diplayContract( data.contractArr,data.headCount , "contract_table" );
			}
		}, "json");
	
	} , 

	
	_populateContract   : function( contract )
	{
		$.each( contract, function( index, ctr){
			$("#contracts").append($("<option />",{text:ctr.contract, value:ctr.ref}))										
		})
	} ,
	_getUsers	   		: function()
	{
		$.getJSON("controller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
				$("#users").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))					
			});														 
		});	
	}, 
	
	_diplaySelect		: function( headerCount )
	{		 
	 $("#display_select").append($("<table />")
							 .append($("<tr />")
										.append($("<th />",{html:"Select Contract : ", colspan:"2"}))
										.append($("<td />", {colspan:"2"})
											.append($("<select />",{id:"contracts"})
											  .append($("<option />",{value:"", text:"--select contract--"}))		  
											)		  
										).css({"display":(($this.options.page != "confirmation") ? "none" : "inline")})
									  )
									.append($("<tr />")
									  .append($("<th />",{colspan:"2", html:"Select User"}))	
									  .append($("<td />")
										 .append($("<select />",{id:"users"})
											.append($("<option />",{text:"Select User"}))	   
										  )		
									  ).css({"display":"none"})
									) 
	 							)
		$this._getUsers();
		
		$("#contracts").live("change", function(){
			$("#contractDetail").show();
			$("#display_select").hide();

			$this.options.contractId	=  $(this).val();
			$this.options.page			= "displayless";
			$this._getContractDetail( $(this).val() );
			return false;
		});
		 
		 $("#users").live("change", function(){
			$this._getUserContracts( $(this).val() );
			return false;
		});			 
	} ,
	
	_getContractDetail  : function( id )
	{	
		$this = this;
		$.post($this.options.urlLocation+"controller.php?action=getDeliverables",{
			  	 id 	   : id ,
			   	 getDetail : true,
				 request   : "" 
		}, function( data ){
			$("#contractview").html("");
			$("#contract_summary").html("");
			$("#table_contract").html("");
			$this.options.contractId	=  id;
			
			$("#contractDetail").css({"display":"block"})
			$("#contractview").append($("<table />",{ id:"contract_table" , width:"100%"}))		
			if( $.isEmptyObject(data.contractData)){
				$("#table_contract")	
				 .append($("<tr />")
					.append($("<td/>",{colspan:"2", html:"An error occured getting contract details"}))				
				  )				
			} else {
				$this._displayContract( data.contractData, id );	
			}
			
			if( $.isEmptyObject(data.categorized)){
				$("#contract_summary").append($("<tr />")
										  .append($("<td/>",{colspna:"2", html:"There are no deliverables for this contract"}))
									   )	
			} else {
				$this._displayCategorized( data.categorized );
			}
			
			if( $.isEmptyObject(data.headers) ){				
			} else {
				$this._diplayHeaders( data.headers , data.headCount , "contract_table");
			}			
			
			if( $.isEmptyObject(data.allCategory) ){
				$("#contract_table").append($("<tr />")
										  .append($("<td/>",{colspna:"2", html:"There are no deliverables for this contract"}))
									   )					
			} else {
				$this._displayCategorisedDetail( data.allCategory, id , data.columns, data.contractDefs);
			}			
				
		},"json");
	},
	
	_displayCategorisedDetail	: function( deliverables, id, columns, contractDefs ){
		$this = this;
		var totalDW = 0;
		var totalOW = 0;
		var totalQW = 0;
		
		$.each( deliverables, function( index, catDels ){					   
			if( typeof( catDels['delivered_weight'] ) != 'undefined') {
				totalDW =  parseInt( catDels['delivered_weight'] ) + parseFloat( totalDW );
			}
			if( typeof(catDels['quality_weight']) != 'undefined'){
				totalQW =  parseInt( catDels['quality_weight'] ) + parseFloat( totalQW );
			}
			if(typeof(catDels['other_weight']) != 'undefined'){
				totalOW =  parseInt(catDels['other_weight'])	+ parseFloat( totalOW );			
			}
			
				$("#contract_table").append($("<tr />").append($("<td />",{colspan:"2", html:"<b>"+index+"</b>"})))
				$this._displayContractDetail( index, catDels , columns, contractDefs, id )

				totalS = $("body").data();
				totalDW = parseFloat( totalS["totalDW_"+index] ) + totalDW;
				totalQW = parseFloat( totalS["totalQW_"+index] ) + totalQW;
				totalOW = parseFloat( totalS["totalOW_"+index] ) + totalOW;
			});		
		//display total of the weights\
		
		var colspan = 1;
		if((contractDefs & 8) == 8 ){
			colspan = parseInt( colspan ) + 1;
		}
		if((contractDefs & 16) == 16 ){
			colspan = parseInt( colspan ) + 1;
		}		
		if((contractDefs & 32) == 32 ){
			colspan = parseInt( colspan ) + 1;
		}			
		colspan = 6;
		$("#contract_table")
			.append($("<tr />")
			   .append($("<td />",{colspan:colspan}))	
			   .append($("<td />",{html:""}))				   
			   .append($("<td />",{html:"<b>"+totalDW+"</b>"})
				  .css("display" , ((contractDefs & 8) == 8 ? "block" : "none"))
				)
			   .append($("<td />",{html:"<b>"+totalQW+"</b>"})
				  .css("display" , ((contractDefs & 16) == 16 ? "block" : "none"))						 
				)
			   .append($("<td />",{html:"<b>"+totalOW+"</b>"})
				  .css("display" , ((contractDefs & 32) == 32 ? "block" : "none"))						 
				)
			   .append($("<td />",{colspan:"2"}))		
			   .css({display:(isNaN(totalDW) || isNaN(totalOW) || isNaN(totalQW)) ? "none" : ""})
			)

			// display save and request approval buttons
			$("#contract_table").append($("<tr />")
			   .append($("<td />")
				 .append($("<input />",{type:"button", name:"step3_"+id, id:"step3_"+id, value:"Step 3"}).click(function(){
								document.location.href = "add_deliverable.php?id="+id;
				}))
				 .css({display:($this.options.template ? "none" : "block")})
				)
			   .append($("<td />")
				 .append($("<input />",{type:"button", name:"saveonly_"+id, id:"saveonly_"+id, value:"Save"}).click(function(){
			$("<div />", {id:"savecontractTemplate", html:"Saving contract details . .  . .<img src='../images/loaderA32.gif' /> "})
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#savecontractTemplate").remove();									
									$(this).html("");
							   }
						}
			 })												  
			$.post("controller.php?action=lockContract", 
				   { id 		: id ,
					 saveonly   : "yes" 
				   }, function( response ){	
				$("#savecontractTemplate").html( response.text );		
			},"json");																										 
				}))
				 .css({display:($this.options.template ? "none" : "block")})
				)			   
			   .append($("<td />")
				 .append($("<input />",{type:"button", name:"savecontract_"+id, id:"savecontract_"+id, value:"Save and Request Activation"}).click(function(){
					//$("<div />", {id:"lockcontract_dialog", html:"Updating contract details . .  . .<img src='../images/loaderA32.gif' /> "})
					 /* .dialog({	autoOpen:true, 
								modal	 : true,					
								buttons  : {"Close" : function(){
											$(this).dialog("destroy");
											$("#lockcontract_dialog").remove();									
											$(this).html("");
									   }
								}
					})*/
					jsDisplayResult("info", "info", "Updating contract . . . .<img src='../images/loaderA32.gif' />" );
					$.post("controller.php?action=lockContract", { id : id }, function( response ){	
						if( response.error ){
							jsDisplayResult("error", "error", response.text );
							//$("#lockcontract_dialog").html( response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );
						}
					},"json");																																}))		 										
					 .css({display:($this.options.template ? "none" : "block")})				
					)	
				   .append($("<td />")
					 .append($("<input />",{type:"button", name:"delete_"+id, id:"delete_"+id, value:"Delete"}).click(function(){
						$("<div />", {id:"deleteContract", html:"Deleting contract. .  . .<img src='../images/loaderA32.gif' /> "})
						  .dialog({	autoOpen:true, 
									modal	 : true,					
									buttons  : {"Close" : function(){
												$(this).dialog("destroy");
												$("#deleteContract").remove();									
												$(this).html("");
										   }
									}
						 })												  
						$.post("controller.php?action=deleteContract", { id : id }, function( response ){	
							$("#deleteContract").html( response.text );		
						},"json");																											   
					}))		 										
				 .css({display:($this.options.template ? "none" : "block")})				
				)	
			   .append($("<td />")
				 .append($("<input />",{type:"button", name:"savechanges_"+id, id:"savechanges_"+id, value:"Save Changes"}))		 										
				 .css({display:($this.options.template ? "block" : "none")})				
				)				   
			   .append($("<td />",{colspan:parseInt(columns) }))
			 )	

		//$("#savecontract_"+id).live("click", function( e ){	
			//e.preventDefault();
							  									  
		//});

		//$("#step3_"+id).live("click", function(){
			//e.preventDefault();											   
			//document.location.href = "add_deliverable.php?id="+id;
			//return false;								  									  
		//});	
		
		//$("#saveonly_"+id).live("click", function( e ){
			//e.preventDefault();												  
							  
			//return false;									  									  
		//});	
		
		//$("#delete_"+id).live("click", function( e ){
		//	e.preventDefault();																					  
			//return false;							  									  
		//});	
		
		$("#savechanges_"+id).live("click", function( e ){
			e.preventDefault();															 
			$("<div />", {id:"saveTemplateChanges", html:"Saving contract changes . .  . .<img src='../images/loaderA32.gif' /> "})
			  .dialog({	autoOpen:true, 
						modal	 : true,					
						buttons  : {"Close" : function(){
									$(this).dialog("destroy");
									$("#saveTemplateChanges").remove();									
									$(this).html("");
							   }
						}
			 })												  
			$.post("controller.php?action=saveTemplateChanges", { id : id }, function( response ){	
				$("#saveTemplateChanges").html( response.text );		
			},"json");								  
			return false;								  									  
		});			
	} , 
	
	_displayContract		: function( contract , id)
	{
		
		$("#table_contract")	
		 .append($("<tr />")
			.append($("<td/>",{colspan:"2", html:"<h3>"+($this.options.template ? " Edit Template" : "Step 5 : Confrimation Of SLA")+"</h3>" }))				
		  )
		 .append($("<tr />")
			.append($("<th />",{html:"Ref:"}))
			.append($("<td />",{html:id}))					
		  )
		$.each( contract, function( index, contr ){
			
			if( contr.colName == "Attachment" || contr.colName == "Deliverable Status"){					   
				//do nothing
			} else{
				$("#table_contract")	
				 .append($("<tr />")
					.append($("<th />",{html:contr.colName+":"}))
					.append($("<td />",{html:contr.value}))					
				  )				
			}
		});
	} , 
	
	_displayCategorized		: function( contractSummary )
	{
		$("#contract_summary").append($("<tr />")
								  .append($("<th />",{html:"Key Measures"}))
								  .append($("<th />",{html:"Weights"}))								  
							   )	
		var  total = 0;
		$.each( contractSummary, function( index, catDels ){
			$("#contract_summary").append($("<tr />").append($("<td />",{colspan:"2", html:"<b>"+index+"</b>"})))
			var totalCat = 0;
			$.each( catDels, function( key, value){						
			var totalW = 0								  
			//totalW	  = $this._calculteTotals( value['delivered_weight'], value['quality_weight'], value['other_weight']);
			if( typeof( value['delivered_weight'] ) != 'undefined') {
				totalW =  parseInt( value['delivered_weight'] ) + parseFloat( totalW );
			}
			if( typeof(value['quality_weight']) != 'undefined'){
				totalW =  parseInt( value['quality_weight'] ) + parseFloat( totalW );
			}
			if(typeof(value['other_weight']) != 'undefined'){
				totalW =  parseInt(value['other_weight'])	+ parseFloat( totalW );			
			}			
			totalCat  = parseFloat( totalCat ) + parseFloat( totalW );
	
				$("#contract_summary").append($("<tr />")
										.append($("<td />",{html:value['deliverable']}))		
										.append($("<td />",{html: totalW }))												
									  )						  
			})
			total = parseFloat( totalCat ) + parseFloat( total )
			$("#contract_summary").append($("<tr />")
									.append($("<td />"))		
									.append($("<td />",{html:totalCat}))											
								)
		})
		$("#contract_summary").append($("<tr />")
								.append($("<td />",{html:"<b>Score </b>"}))		
								.append($("<td />",{html:total}))											
							)		
	} ,

	_displayContractDetail		: function(category, contract , columns,  contractDefs, contractId)
	{
		var totalDW = 0;
		var totalOW = 0;
		var totalQW = 0;	
		var totalA	= 0;
		$this = this;

		var dontDisplay = ['main_with_sub', 'subd', 'deliverable_category', 'contract', 'deliverable_status', 'action_progress', "hasAccess", "hasActionAccess"];
	
		var colspan 	= 6 ;
		var cols		= 0;
		$.each( contract, function( index, deliverable){						   
			if(deliverable.subd == "yes" ){		
				tr = $("<tr />")		
				$.each( deliverable.subarray, function( inx, subDel){
					
					var tr_sub = "";
					tr_sub = $("<tr />",{id:"tr_"+inx}).css({"background-color":"#CCCCCC", "text-align":"right"})
						$.each( subDel , function(i, val){
							if( $.inArray(i, dontDisplay) >= 0){
							//continue;
							} else if( i == "number_of_actions"){
								tr_sub.append($("<td />",{html:val})
									.hover(function(){
										$("#display_actions_"+inx).remove();
										$(this).append($("<div />",{id:"display_actions_"+inx})
														 .addClass("displayActions")
														 .css({top:$(this).position().top+20, left:$(this).position().left-20})																										
														)
											   //.css({top:$(this).postion.top()+10, left:$(this).postion.top()-10})										
											$.getScript("../js/jquery.ui.action.js", function(){
												$("#display_actions_"+inx).action({delId:inx}).show();	
										    });					 
									}, function(){
										$("#display_actions_"+inx).hide();
									})
									.css({"text-align":"right"})
								)
							} else {
								tr_sub.append($("<td />",{html:val}))			 										 					
							}
				  
						});	
					$("#contract_table").append( tr_sub )

					$this._createButtons( tr_sub, subDel.ref, false, subDel.number_of_actions, contractId)					
				});

			} else {
				
				numberOfAction = deliverable.number_of_actions.replace("<span class='semi'>", "");
				numberOfAction = numberOfAction.replace("</span>", "");
				numberOfAction = numberOfAction.replace("&nbsp;&nbsp;&nbsp;&nbsp;", "");				
				numberOfAction = numberOfAction.replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "");	
				numberOfAction = numberOfAction.replace("<span class='success'>", "");	
				
				totalA	= parseInt(totalA) + parseInt( numberOfAction );
				totalDW = parseFloat( deliverable['delivered_weight'] ) + parseFloat( totalDW );
				totalQW = parseFloat( deliverable['quality_weight'] )+  parseFloat( totalQW );
				totalOW	= parseFloat( deliverable['other_weight'] ) +  parseFloat( totalOW );		
				
				tr = $("<tr />",{id:"tr_"+index})
				$.each( deliverable , function( key, value){
					if( $.inArray(key, dontDisplay) >= 0){
						//continue;
					}  else if( key == "number_of_actions"){
						tr.append($("<td />",{html:value})
								.hover( function(){
								$("#display_actions_"+deliverable['ref']).remove();
								$(this).append($("<div />",{id:"display_actions_"+deliverable['ref']})
												 .addClass("displayActions")
												 .css({top:$(this).position().top+20, left:$(this).position().left-20})
												 )
									   
									$.getScript("../js/jquery.ui.action.js", function(){
										$("#display_actions_"+deliverable['ref']).action({delId:deliverable['ref']}).show();	
									});					 
								}, function(){
									$("#display_actions_"+deliverable['ref']).hide();
								})
							)
					} else{
						tr.append($("<td />",{html:value}))					 										 					
					}
				});
				$("#contract_table").append( tr )
				$this._createButtons( tr, deliverable.ref, deliverable.main_with_sub, deliverable.number_of_actions, contractId)				
			}	
			
			
		});	
		cols = colspan ;
		if((contractDefs & 8) == 8 ){
			colspan = parseInt( colspan ) + 1;
		}
		if((contractDefs & 16) == 16 ){
			colspan = parseInt( colspan ) + 1;
		}		
		if((contractDefs & 32) == 32 ){
			colspan = parseInt( colspan ) + 1;
		}		

		var total = {};
		$("#contract_table")
			.append($("<tr />")					  
			   .append($("<td />",{colspan:cols}))	   
			   .append($("<td />",{html:"<b>"+($this.options.page == "confirmation" ? "" : Math.round((isNaN(totalA) ? "0" : totalA)))+"</b>"})
				  .css("display" , ($this.options.page !== "confirmation"  ? "block" : "none"))
				)				
			   .append($("<td />",{html:"<b>"+totalDW+"</b>"})
				  .css("display" , ((contractDefs & 8) == 8 ? "block" : "none"))
				)
			   .append($("<td />",{html:"<b>"+totalQW+"</b>"})
				  .css("display" , ((contractDefs & 16) == 16 ? "block" : "none"))						 
				)
			   .append($("<td />",{html:"<b>"+totalOW+"</b>"})
				  .css("display" , ((contractDefs & 32) == 32 ? "block" : "none"))						 
				)
			   .append($("<td />",{colspan:"2"}))		   
			)
		total['totalDW_'+category] = totalDW
		total['totalQW_'+category] = totalQW
		total['totalOW_'+category] = totalOW		
		$("body").data(total);
	} ,
	
	_displayActions		 : function( id )
	{
		$(this).append($("<div />",{id:"display_actions_"+id}).addClass("displayActions"))
			$.getScript("../js/jquery.ui.action.js", function(){
				$("#display_actions_"+id).action({delId:id}).show();	
			});										
	} , 
	
	_hideActions		:  function( id ){
		$("#display_actions_"+id).hide();
	},
	
	_createButtons		: function( tableRow, fieldId, showButton , actions, number_of_actions, contractId)
	{	
		$this = this;

		$(tableRow).append($("<td />")
		 .append($("<input />",{type:"submit", name: "edit_del_"+fieldId, id:"edit_del_"+fieldId, value:"Edit Deliverable"}))
		 .css({display:(($this.options.editDeliverable == true ) ? "block" : "none")})
		)
		
		$(tableRow).append($("<td />")
		 .append($("<input />",{type:"submit", name: "edit_action_"+fieldId, id:"edit_action_"+fieldId, value:"Edit Action"}))
		 .css({display:(($this.options.editAction == true &&  showButton == false) ? "block" : "none")})
		)		
		
		$("#edit_del_"+fieldId).live("click", function(){								   
			document.location.href = "../manage/edit_deliverable.php?id="+fieldId+"&contractid="+$this.options.contractId+"&page="+$this.options.page;
			return false;
		});
		
		$("#edit_action_"+fieldId).live("click", function(){
			document.location.href = "../manage/edit_actions.php?id="+fieldId+"&contractid="+$this.options.contractId+"&page="+$this.options.page;
			return false;
		});							
	} ,
	
	_diplayContract		: function( contract, headerCount, table ){

		$this = this;
		var dontDisplay = ["deliverable_status", "hasAccess", "hasDeliverableAccess"];
		$.each( contract, function( index, ctr){
			tr = $("<tr />",{id:"tr_"+ctr.ref})
			$.each( ctr, function( i, val){
				if($.inArray( i, dontDisplay) < 0 ){
					tr = tr.append($("<td />", { html : val }))
				}
			});
			
			$(tr).append($("<td />")
		   	 .append($("<input />",{type:"submit", name: "add_"+ctr.ref, id:"add_"+ctr.ref, value:"Add Deliverable"})
			  ).css({display:($this.options.addDeliverable ? "block" : "none")})
			)
			$(tr).append($("<td />")
		   	 .append($("<input />",{type:"submit", name: "add_action_"+ctr.ref, id:"add_action_"+ctr.ref, value:"Add Action"})
			  ).css({display:($this.options.addAction ? "block" : "none")})
			)			
			$(tr).append($("<td />")
		   	 .append($("<input />",{type:"submit" , name: "edit_"+ctr.ref, id:"edit_"+ctr.ref, value:"Edit Contract"})		 							
			 ).css({display:($this.options.editContract ? "block" : "none")})		   
			)
			$(tr).append($("<td />")
		   	 .append($("<input />",{type:"submit" , name: "edit_deliverables_"+ctr.ref, id:"edit_deliverables_"+ctr.ref, value:"Edit Deliverable"})		 							
			 ).css({display:(($this.options.editDeliverable && $this.options.page != "confirmation" )? "block" : "none")})		   
			)
			$(tr).append($("<td />")
		   	 .append($("<input />",{ type     : "submit", 
					   				 name     : "update_"+ctr.ref, 
									 id	      :	"update_"+ctr.ref, 
									 value    : "Update Contract",
									 disabled : (ctr.hasAccess == 0 ? "disabled" : "" )
									 })						
			 ).css({display:($this.options.updateContract ? "block" : "none")})			   
			)
			$(tr).append($("<td />")
		   	 .append($("<input />",{ type     : "submit", 
					   				 name     : "update_deliverables_"+ctr.ref, 
									 id	      : "update_deliverables_"+ctr.ref, 
									 value    : "Update Deliverable",
									 disabled : (ctr.hasDeliverableAccess == 0 ? "disabled" : "" )
									})						
			 ).css({display:($this.options.updateDeliverable ? "block" : "none")})			   
			)
			
			$(tr).append($("<td />")
		   		 .append($("<input />",{type:"submit" , name: "activate_"+ctr.ref, id:"activate_"+ctr.ref, value:"Activate"})			
				).css({display:($this.options.activateContract ? "block" : "none")})				
			)				
			$(tr).append($("<td />")
		   		 .append($("<input />",{type:"submit" , name: "approve_"+ctr.ref, id:"approve_"+ctr.ref, value:"Approve"})			
				).css({display:($this.options.approveContract ? "block" : "none")})				
			)							
			$(tr).append($("<td />")
		   	 	.append($("<input />",{type:"submit" , name: "view_"+ctr.ref, id:"view_"+ctr.ref, value:"View Contract"})								
				).css({display:($this.options.view ? "block" : "none")})			
			)	
			$(tr).append($("<td />")
		   	 	.append($("<input />",{type:"submit" , name: "assessment_"+ctr.ref, id:"assessment_"+ctr.ref, value:"Assesement"})								
				).css({display:($this.options.assessmentContract ? "block" : "none")})			
			)	
			$(tr).append($("<td />")
			 .append($("<input />",{type:"submit", name: "edit_template_"+ctr.ref, id:"edit_template_"+ctr.ref, value:"Manage Template "}))
			  .css({display:(($this.options.contractTemplate) ? "block" : "none")})		
			)				
			$(tr).append($("<td />")
			 .append($("<input />",{type:"submit", name: "contract_authorization_"+ctr.ref, id:"contract_authorization_"+ctr.ref, value:"Authorization"}))
			  .css({display:(($this.options.contractAuthorization) ? "block" : "none")})		
			)				
			$(tr).append($("<td />")
			 .append($("<input />",{type:"submit", name: "deliverable_authorization_"+ctr.ref, id:"deliverable_authorization_"+ctr.ref, value:"Deliverable Authorization"}))
			  .css({display:(($this.options.deliverableAuthorization) ? "block" : "none")})		
			)
			
			
			$("#contract_authorization_"+ctr.ref).live("click", function(){
				document.location.href = "contract_authorization.php?id="+ctr.ref+"&";																						
				return false;																
			})
			
			$("#deliverable_authorization_"+ctr.ref).live("click", function(){
				document.location.href = "deliverable_authorization.php?id="+ctr.ref+"&";																						
				return false;																
			})
			
			$("#assessment_"+ctr.ref).live("click", function(){												 
				document.location.href = "contract_assessment.php?id="+ctr.ref+"&";			
				return false;
			});				
			$("#view_"+ctr.ref).live("click", function(){
				document.location.href = "contract_view.php?id="+ctr.ref+"&";			
				return false;
			});	

			$("#add_"+ctr.ref).live("click", function(){
				document.location.href = "add_deliverable.php?id="+ctr.ref+"&";
				return false;
			});
			
			$("#add_action_"+ctr.ref).live("click", function(){
				document.location.href = "actions.php?id="+ctr.ref+"&";
				return false;
			});			

			$("#edit_"+ctr.ref).live("click", function(){
				document.location.href = "edit_contract.php?id="+ctr.ref+"&";
				return false;
			});
			
			$("#edit_deliverables_"+ctr.ref).live("click", function(){
				document.location.href = "edit_deliverables.php?id="+ctr.ref+"&";
				return false;
			});	
			
			$("#update_deliverables_"+ctr.ref).live("click", function(){
				document.location.href = "update_deliverables.php?id="+ctr.ref+"&";
				return false;
			});				

			$("#approve_"+ctr.ref).live("click", function(){
				document.location.href = "approve_contract.php?id="+ctr.ref+"&";
				return false;
			});
			
			$("#activate_"+ctr.ref).live("click", function(){
				$this._approveContract( ctr.ref, ctr );
				return false;
			});
			
			$("#update_"+ctr.ref).live("click", function(){
				document.location.href = "update_contract.php?id="+ctr.ref+"&";
				return false;
			});		
			$("#edit_template_"+ctr.ref).live("click", function(){
				document.location.href = "manage_template.php?contractid="+ctr.ref;
				return false;
			});					
			
			
			$("#"+table).append( tr );
		});
			$("#"+table+" tr").hover(function(){
				$(this).addClass("hovering")							  
			}, function(){
				$(this).removeClass("hovering")							  		  
			});		
	} ,
	
	_diplayHeaders		: function( headers ,headerCount, table )
	{
		var tr = $("<tr />");
		var dontDisplayHeader = [];
		if( $this.options.page == "displayless")
		{
			dontDisplayHeader = ['deliverable_category', "contract", "deliverable_status", "action_progress"];			
		} else if($this.options.page == "update"){
				if( $this.options.updateDeliverable == false){
					dontDisplayHeader = ['deliverable_category', "contract"];		
				} else {
					dontDisplayHeader = ['deliverable_category'];	
				}
		} else {
			dontDisplayHeader = ['deliverable_category'];				
		}

		if( $this.options.addDeliverable == true || $this.options.addAction == true || $this.options.page == "confirmation"){
			dontDisplayHeader.push("deliverable_status");	
		}

		$.each( headers, function( index, header){
			if( ($.inArray(index, dontDisplayHeader) >= 0) || header==""){
				
			} else {
				tr = tr.append($("<th />",{html:header}))					  
			}
		})
		
		if( $this.options.page !== "confirmation"){
			tr = tr.append($("<th />",{html:"&nbsp;"}))
		}
			
		
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "update" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "editContract" ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.deliverableAuthorization ? "table-cell" : "none")}))
				
		if( this.options.page == "displayless"){
			tr = tr.append($("<th />",{html:"&nbsp;"}))		
		}
		$("#"+table).append( tr )
	} , 
	
	_createPager 		: function(total, columns, table ) {
		var rk 		= this;
		
		var pages;
		if( total%rk.options.limit > 0){
			pages   = Math.ceil(total/rk.options.limit); 				
		} else {
			pages   = Math.floor(total/rk.options.limit); 				
		}

		$("#"+table)
		  .append($("<tr />")
			.append($("<td />",{colspan:columns})
				.append($("<input />",{type:"button", name:"first", value:" |< ", id:"first", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
				.append("&nbsp;")
				.append($("<input />",{type:"button", name:"previous", value:" < ", id:"previous", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))				
				.append("&nbsp;&nbsp;&nbsp;")
				.append($("<span />", {colspan:"1",html:"Page "+rk.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages), align:"center"}))	
				.append("&nbsp;&nbsp;&nbsp;")
				.append($("<input />",{type:"button", name:"next", value:" > ", id:"next", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))
				.append("&nbsp;")
				.append($("<input />",{type:"button", name:"last", value:" >| ", id:"last", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))				
			 )		
			 .append($("<td />",{colspan:2, html:"&nbsp;"}).css({"display":($this.options.page == "update" ? "table-cell" : "none")}))
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.page == "editContract" ? "table-cell" : "none")}))
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.page == "assessment" ? "table-cell" : "none")}))
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.page == "authorization" ? "table-cell" : "none")}))			 
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.page == "view" ? "table-cell" : "none")}))
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.approveContract == true ? "table-cell" : "none")}))
			 .append($("<td />",{colspan:2,html:"&nbsp;"}).css({"display":($this.options.template == true ? "table-cell" : "none")}))
			 
			//.append($("<td />",{colspan:columns}))
		  )

		$("#next").bind("click", function(evt){
			rk._getNext( rk );
		});
		$("#last").bind("click",  function(evt){
			rk._getLast( rk );
		});
		$("#previous").bind("click",  function(evt){
			rk._getPrevious( rk );
		});
		$("#first").bind("click",  function(evt){
			rk._getFirst( rk );
		});			

	} ,		
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getContract();
	},	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getContract();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getContract();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getContract();				
	}
});