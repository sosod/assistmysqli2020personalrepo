// JavaScript Document
$(function(){
		   //alert("Coming in her")
	var notificationid = $("#notificationid").val();
	if(typeof notificationid == "undefined"){
		$("#date").timepicker();
		Notification.get();		
	}
	
	$("#updatenotstatus").click(function(){
		Notification.changeStatus( notificationid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		Notification.update();							
		return false;
	});
	
	$("#save").click(function(){
		Notification.save();
		return false;
	})
			   
})

var Notification	= {
	
	shortcode	 : "",
	type	 	 : "", 
	description  : "",
	date		 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getNotifications", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, notification ) {
					Notification.display( notification )				   
				});
			}																 
		})
	} , 
	display 	: function( notification )
	{
		$("#table_notifcations")
		  .append($("<tr />",{ id:"tr_"+notification.id})
			.append($("<td />",{ html:notification.id}))
			.append($("<td />",{html:notification.shortcode}))
			.append($("<td />",{ html:notification.type}))			
			.append($("<td />",{ html:notification.description}))
			.append($("<td />",{ html:notification.date}))
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+notification.id, name:"edit_"+notification.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{id:"del_"+notification.id, name:"del_"+notification.id, type:"submit", value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((notification.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{id:"change_"+notification.id, name:"change_"+notification.id, type:"submit", value:"Change Status"}))		
		   )
		  )
		
		$("#edit_"+notification.id).live("click",function(){
			document.location.href = "edit_notifcation.php?id="+notification.id+"&";
			return false;
		});
		
		$("#del_"+notification.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				Notification.changeStatus( notification.id , parseInt(notification.status) + 2);
				$("#tr_"+notification.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+notification.id).live("click", function(){
			document.location.href = "change_notifcation.php?id="+notification.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.shortcode 	  =	$("#shortcode").val();
		that.type		  = $("#type").val(); 
		that.description  = $("#description").val();
		that.date	  	  = $("#date").val();	
		
		var valid = Notification.isValid( that );
		var data  = { shortcode:that.shortcode, type:that.type, description:that.description,date:that.date}; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving notifications . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveNotification", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id 	= response.id;
					data.active = 1
					Notification.display( data );
					Notification.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("ok", "ok", valid);
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.shortcode 	  =	$("#shortcode").val();
		that.type		  = $("#type").val(); 
		that.description  = $("#description").val();
		that.date	  	  = $("#date").val();	
		
		var valid = Notification.isValid( that );
		var data  = { shortcode:that.shortcode, type:that.type, description:that.description, date:that.date,id:$("#notificationid").val() };   		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating notifications . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateNotification", data , function( response ){
				if( !response.error )
				{
					jsDisplayResult("ok", "ok", response.text );	
				} else{
					jsDisplayResult("error", "error", response.text );
				}																		 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", response.text );	
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating notifications . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeNotification", {id:id, tostatus : tostatus}, function( response ) {
			if( !response.error )
			{
				jsDisplayResult("ok", "ok", response.text );	
			} else{
				jsDisplayResult("error", "error", response.text );
			}																										 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			$("#"+index).val("");					  
		}); 
	} ,
	
	isValid			:function( that )
	{
		if( that.shortcode == ""){
			return "Please enter the short code"; 	
		} else if( that.type == ""){
			return "Please enter the notifcation type";	
		} else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.date == ""){
			return "Please enter the day time";	
		}
		return true;
	}

}