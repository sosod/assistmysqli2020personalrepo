// JavaScript Document
$(function(){
	/*		   
	$(".datepicker").datepicker({	
		showOnFocus   : true,
		showOn		  : "both",
		buttonImageOnly: true,
		buttonImage: "../images/calendar.png",
		changeMonth :true,
		changeYear	: true,
		dateFormat : "d-MM-yy"
	});	*/	   
	 $(".datepicker").live("focus",function(){
      $(this).datepicker({ changeMonth : true, changeYear : true, dateFormat : "d-MM-yy"  });
    });	 		   
		   
	if( $("#finid").val() == undefined){
		FinancialYear.getFinancialYear();		
	} else{
		FinancialYear.getFinYear();
	}
	
	$("#save").click(function(){
		FinancialYear.saveFinancialYear();	
		return false;
	});
	

	$("#edit").click(function(){
		FinancialYear.updateFinancialYear();						  
		return false;
	});

});

var FinancialYear = {

	lastday   : "",
	startdate : "",
	enddate   : "",
	message   : undefined, 
	getFinYear				: function(){
		
	}, 
	getFinancialYear 	    : function()
	{
		$.getJSON("controller.php?action=getFinancialYear", function( data ){
			if( $.isEmptyObject(data) )
			{
				jsDisplayResult("info", "info", "No results obtained" );
			}else{
				FinancialYear.displayFinYear(data);						
			}								
		});	
	},
	display 			   : function( finyear )
	{		
		$("#table_financialyear")
		  .append($("<tr />", { id:"tr_"+finyear.id})
			.append($("<td />", {align:"center", html:finyear.id}))								
			.append($("<td />", {align:"center", html:finyear.lastday }))		  
			.append($("<td />", {align:"center", html:finyear.startdate }))		  
			.append($("<td />", {align:"center", html:finyear.enddate }))	
			.append($("<td />")
			  .append($("<input />",{ type:"submit", id:"edit_"+finyear.id, name:"edit_"+finyear.id, value:"Edit"}))
			  .append($("<input />",{ type:"submit", id:"del_"+finyear.id, name:"del_"+finyear.id, value:"Delete"}))				  
			)
	   )
		  
		$("#edit_"+finyear.id).live("click", function(){
			document.location.href = "edit_finyear.php?id="+finyear.id+"&";
			return false;										  
		});  
		
		$("#del_"+finyear.id).live("click", function(){									 
			var message   = $("#message") 													 
			if(confirm("Are you sure you want to delete this finacial year"))
			{
				jsDisplayResult("info", "info", "Updating financial year . . . .<img src='../images/loaderA32.gif' />" );
				$.post("controller.php?action=deleteFinYear", { id :finyear.id }, function( response ){
					if( !response.error  ){
						jsDisplayResult("ok", "ok", response.text );
						$("#tr_"+finyear.id).fadeOut();
					} else {
						jsDisplayResult("error", "error", response.text );
					}																	
				},"json");	
			}
			return false;										  
		});  		
		  
	} , 
	
	displayFinYear 	       : function( data )
	{
		$.each( data, function( index, finyear ){
		  FinancialYear.display( finyear );					   			   
		})
	},
	
	showEditFinYear       : function( id )
	{
		document.location.href	= "editfinyear.php";
	},
	
	isValid 			  : function()
	{
		var message  = $("#message") 
		var $this    = this;
		if( $this.lastday == "" ) {
			return "Please select the last day of the year ";
		} else if( $this.startdate == "" ){
			return "Please select the start date of the financial year "			
		} else if( $this.enddate == ""){
			return "Please select the end date of the financial year ";			  
		}
		return true;
	},
	
	emptyFields 		  : function( fields )
	{
		$.each( fields , function(index, value){
			$("#"+index).val("");					  
		}); 		
	}, 
	
	saveFinancialYear     : function()
	{
		var that = this;
		that.lastday  = $("#lastday").val()
		that.startdate= $("#startdate").val()
		that.enddate  = $("#enddate").val()
		var message   = $("#message") 
		var data  = { lastday:that.lastday, startdate:that.startdate, enddate:that.enddate }
		//checks validation
		valid = FinancialYear.isValid(); 
		if( valid == true ) {
			jsDisplayResult("info", "info", "Saving financial year . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveFinYear", data ,
				function( response ){
					if( !response.error  ){
						jsDisplayResult("ok", "ok", response.text );
						data.id = response.id;					
						FinancialYear.display( data );	
						FinancialYear.emptyFields( data );
					} else {
						jsDisplayResult("error", "error", response.text );
					}
			}, "json")
		} else {
			jsDisplayResult("error", "error", valid );			
			return false;	
		}
	
	},
	
	updateFinancialYear   : function()
	{
		var that = this;
		that.lastday  = $("#lastday").val()
		that.startdate= $("#startdate").val()
		that.enddate  = $("#enddate").val()
		var message   = $("#message") 
		var valid 	  =  FinancialYear.isValid();
		if( valid == true ){
			data = { lastday:that.lastday, startdate:that.startdate, enddate:that.enddate , id : $("#finid").val()};
			jsDisplayResult("info", "info", "Upating financial year . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateFinancialYear", data , function( response ){
				if( !response.error  ){
					jsDisplayResult("ok", "ok", response.text);												 					
				} else {
					jsDisplayResult("error", "error", response.text);
				}																	
			},"json");		
		} else{
			jsDisplayResult("error", "error", valid);			
			return false;
		}
	}  
	
}