// JavaScript Document
$(function(){
		   
	var delid = $("#delid").val();
	if( delid == undefined ){
		DeliverableStatus.get();		
	}
	
	$("#change_status").click(function(){
		DeliverableStatus.changeStatus( delid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		DeliverableStatus.update();							
		return false;
	});
	
	$("#save").click(function(){
		DeliverableStatus.save();
		return false;
	})
	
});

var DeliverableStatus = {
	
	name 	: "",
	color 	: "", 
	client  : "",
	
	get 		: function()
	{
		$.getJSON("controller.php?action=getDeliverableStatus", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, delStatus) {
					DeliverableStatus.display( delStatus )				   
				});
			}																 
		})
	} , 
	
	save 		: function()
	{
		var that    = this;
		that.name   = $("#name").val();
		that.client = $("#client_terminology").val();		
		that.color  = $("#color").val();		
			
			var valid = DeliverableStatus.isValid( that );
			var data  = { name : that.name, client_terminology : that.client, color : that.color }; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving deliverable status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveDeliverable", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					data.id = response.id
					data.status = 1;
					DeliverableStatus.display( data );
					DeliverableStatus.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	update 		: function()
	{
		var that    = this;
		that.name   = $("#name").val();
		that.client = $("#client_terminology").val();		
		that.color  = $("#color").val();		
		var valid = DeliverableStatus.isValid( that );
		var data  = { name : that.name, client_terminology : that.client, color : that.color , id : $("#delid").val() }; 		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating deliverable status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateDeleiverableStatus", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																		 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid );	
		}
		
	} ,
	
	changeStatus : function( id, tostatus )
	{
		jsDisplayResult("info", "info", "Updating deliverable status . . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeDeliverableStatus", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																									 
		}, "json");
	} , 
	
	display 	: function( dstatus )
	{
		$("#table_deliverablestatus")
		  .append($("<tr />",{id:"tr_"+dstatus.id})
			.append($("<td />",{ html:dstatus.id}))
			.append($("<td />",{ html:dstatus.name}))
			.append($("<td />",{ html:dstatus.client_terminology}))
			.append($("<td />")
			  .append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":dstatus.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+dstatus.id, name:"edit_"+dstatus.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{type:"submit", id:"del_"+dstatus.id, name:"del_"+dstatus.id, disabled:((dstatus.status & 4 ) == 4 ? "disabled" : ""), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((dstatus.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{type:"submit", id:"change_"+dstatus.id, name:"change_"+dstatus.id, disabled:((dstatus.status & 4) == 4 ? "disbled" : ""), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+dstatus.id).live("click",function(){
			document.location.href = "edit_dstatus.php?id="+dstatus.id+"&";
			return false;
		});
		
		$("#del_"+dstatus.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				DeliverableStatus.changeStatus( dstatus.id , parseInt(dstatus.status)+2 );
				$("#tr_"+dstatus.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+dstatus.id).live("click", function(){
			document.location.href = "change_dstatus.php?id="+dstatus.id+"&";														
			return false;												
		});
		
	} ,
	
	isValid 	: function( that )
	{
		if( that.name == ""){
			return "Please enter the status name"; 	
		} else if( that.client == ""){
			return "Please enter the client terminology";	
		}
		return true;
	} , 
	
	emptyFields : function( fields )
	{
		$.each( fields , function(index, value){
			$("#"+index).val("");					  
		}); 
	}
	
	
	



}
