// JavaScript Document
$(function(){
	var deliverableid = $("#deliverableid").val();
	
	if( typeof($("#contractid").val()) != 'undefined' ){
 		$("#viewdelivereable").deliverable({editDeliverable:true, deleteDeliverable:true, contractId:$("#contractid").val(), page:$("#page").val()});
	}
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-mm-yy"
		});						   
	});	 
	

 if( typeof($("#deliverableid").val()) == 'undefined'){
	 Deliverable.getDeliverables();
	 Deliverable.getDeliverableCategories(); 
	 Deliverable.getOwners();
	 Deliverable.getDeliveredWeights();
	 Deliverable.getQualityWeights();
	 Deliverable.getOtherWeights();
	 Deliverable.getContract();
	 Deliverable.getDeliverableStatus();
 }
 $(".savedeliverable").click(function(){
	Deliverable.save( $(this).attr('id') );	
	return false;
 }); 
 
 $(".attach").live("change", function(){
	Deliverable.uploadFile( this );		
	return false;
 });
 
 $("#viewEditLog").click(function(){
	Deliverable.viewEditLog();	
	return false;								  
 })
 
 $("#viewupdatelog").click(function(){
	Deliverable.viewUpdateLog();	
	return false;								  									
 })
 
 $("#next").click(function(){
	document.location.href = "action.php?id="+$("#contractid").val();
	return false;
 }); 
 
$("#edit").click(function(){
	Deliverable.edit();
	return false;							
 });

$("#update_deliverable").click(function(){
	Deliverable.update();
	return false;																	
});

$("#type").change(function(){
	var id = $(this).attr("id")
	Deliverable.loadDeliverables( id );
	return false;							
 });
 
 $("#authorize_deliverable").click(function(){
	Deliverable.authorize();										
	return false;										
});

 $("#auth_log").click(function(){
	Deliverable.viewAuthorization();										
	return false;										
});
 
 
});

var Deliverable = {
	getContract		: function()
	{
		$.post("controller.php?action=getContract", { id : $("#contractid").val()} , function( contract ){								  
				//$("#table_contract")	
			
			
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Ref #:"}))
				.append($("<td />",{html:$("#contractid").val()}))					
			  )
			$.each( contract, function( index, contr ){
				//$("#table_contract")	
				 $("#goback").before($("<tr />")
					.append($("<th />",{html:contr.colName+":"}))
					.append($("<td />",{html:contr.value}))					
				  )
			});
		},"json")	
	} ,
	
	loadDeliverables	: function( clickfrom ){
		if( $("#type :selected").val() == 1){
			 Deliverable.getDeliverables();
			 $("body").data("type", "sub");	
			 if( $(".delweights").is(":hidden")){
				$(".delweights").show();	 
			 }
			 $("#main_deliverable").html("");
			 $("#main_deliverable").append($("<option />",{value:"", text:"-- select main deliverable --"}));
		} else {
		
			if( clickfrom !== "fromsave"){
				$("#deliverableDialog").html("")
				$("#deliverableDialog").append($("<span />",{html:"Does deliverable have sub-deliverable"})).dialog({
					autoOpen 	: true,
					buttons		: {
							"Yes"	: function(){
								$("body").data("type", "main");
								$(".delweights").hide();	
								$(this).dialog("close")								
							} ,
							"No"	: function(){
								$("body").data("type", "mainSub");					
								$(".delweights").show();
								$(this).dialog("close")
							}
					}
				})
				/*if(confirm("Does this deliverable have sub deliverables")){
					$("body").data("type", "main");
					$(".delweights").hide();	
				} else {
					$("body").data("type", "mainSub");					
					$(".delweights").show();
				} */
			}
			$("#main_deliverable").html("<option value=''>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-</option>");
		}
	} ,
	
	getDeliverables	: function(){
		$.post("controller.php?action=getDeliverablesList",{id:$('#contractid').val()}, function( deliverables ){
			if( $.isEmptyObject(deliverables)){
				$("#main_deliverable").append($("<option />", {text:"--no deliverables yet--", value:"", disabled:"disabled"}))
			} else {
				$.each( deliverables  , function( index, del){
					if( del.main_deliverable == null){										  
						$("#main_deliverable").append($("<option />", {text:del.deliverable, value:del.ref}))
					}
				});		
			}
		},"json");
	} ,
	
	getDeliverableStatus	: function(){
		$.post("controller.php?action=getDeliverableStatus", function( deliverableStatus ){																
			$.each( deliverableStatus  , function( index, status){								  
					$("#status").append($("<option />", {text:status.name, value:status.id}))
			});																	
		},"json");
	} , 
		
	getOwners		: function(){
		$.getJSON("controller.php?action=getUsers", function( users ){
			$.each( users  , function( index, user ){
				$("#owner").append($("<option />", {text:user.tkname+" "+user.tksurname, value:user.tkid}))
			});																																  
		});	
	} , 
	
	getDeliveredWeights		: function(){
		$.getJSON("controller.php?action=getDeliveredWeights", function( deliveredWeights ){
			$.each( deliveredWeights  , function( index, deliveredWeight ){
				$("#weight").append($("<option />", {text:deliveredWeight.description, value:deliveredWeight.id}))
			});																																  
		});	
	} , 
	getQualityWeights 		: function(){
		$.getJSON("controller.php?action=getQualityWeights", function( qualityWeights ){
			$.each( qualityWeights  , function( index, qualityWeight ){
			
				$("#quality_weight").append($("<option />", {text:qualityWeight.description, value:qualityWeight.id}))
			});																																  
		});				
	} , 
	getOtherWeights 	: function(){
		$.getJSON("controller.php?action=getOtherWeights", function( otherWeights ){
			$.each( otherWeights   , function( index, otherWeight ){
				$("#other_weight").append($("<option />", {text:otherWeight.description, value:otherWeight.id}))
			});																																  
		});		
	} , 
	
	getDeliverableCategories	: function(){
		$.getJSON("controller.php?action=getDeliverableCatergories", function( data ){
			if( $.isEmptyObject( data ) ){

			} else{
				$.each( data, function(index, category){
					$("#category").append($("<option />", {text:category.name, value:category.id}))
			   });	
			}														  
		});
		
	} , 
	
	save 				: function ( clickedid ) {
		
		var that 		 		= this;
		that.name 		 		= $("#name").val();
		that.description 		= $("#description").val();
		that.category		 	= $("#category :selected").val();		
		that.type		 	    = $("#type :selected").val();
		that.main_deliverable   = $("#main_deliverable :selected").val();
		that.owner				= $("#owner :selected").val();
		that.weight		    	= $("#weight :selected").val();
		that.quality_weight	    = $("#quality_weight :selected").val();
		that.other_weight 		= $("#other_weight :selected").val();
		that.deadline			= $("#deadline").val();
		that.remind_on			= $("#remind_on").val(); 		

		var typeSelected = $('body').data('type');
		var valid  = Deliverable.isValid( that );
		var data = {
					name			: that.name, 
					description		: that.description,
					category		: that.category,					
					type 			: that.type,
					main_deliverable: that.main_deliverable,
					owner			: that.owner,
					weight			: that.weight, 
					quality_weight	: that.quality_weight,
					other_weight	: that.other_weight,
					deadline		: that.deadline,
					remind_on		: that.remind_on,
					deliverableStatus : typeSelected,
					contract_id 	: $("#contractid").val()
				}
		if( valid == true){		
			
		jsDisplayResult("info", "info", "Saving Deliverable . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=saveDeliverable", data, function( response ){
																						   
				if( !response.error ){
					jsDisplayResult("ok", "ok", response.text );
					Deliverable.emptyFields( that )
					if( clickedid == "addnext"){
						jsDisplayResult("ok", "ok", "Redirecting to add actions . . . ");
						//$('#display_objresult').stop().animate({opacity: 1},1000).fadeOut('slow');												
						document.location.href = "add_action.php?id="+response.id+"&contractid="+$("#contractid").val();	
					} else {
						$("#viewdelivereable").html("");
						$("#viewdelivereable").deliverable({
														   editDeliverable	 : true, 
														   deleteDeliverable : true,
														   contractId		 : $("#contractid").val()
														  });
						Deliverable.loadDeliverables( "fromsave" )		
					}				
				} else {
					 jsDisplayResult("error", "error", response.text );
				}
																		   
			},"json");
		} else{
			 jsDisplayResult("error", "error", valid );
		}
	} , 
	
	edit				: function()
	{

		var that 		 		= this;
		that.name 		 		= $("#name").val();
		that.description 		= $("#description").val();
		that.category		 	= $("#category :selected").val();		
		that.type		 	    = $("#type :selected").val();
		that.main_deliverable   = $("#main_deliverable :selected").val();
		that.owner				= $("#owner :selected").val();
		that.weight		    	= $("#weight :selected").val();
		that.quality_weight	    = $("#quality_weight :selected").val();
		that.other_weight 		= $("#other_weight :selected").val();
		that.deadline			= $("#deadline").val();
		that.remind_on			= $("#remind_on").val(); 		

		var valid  = Deliverable.isValid( that );
		var data = {
					id						: $("#deliverableid").val(),
					contractid				: $("#contid").val(),	
					is_template				: $("#is_template").val(),
					deliverable				: that.name, 
					deliverable_description : that.description,
					deliverable_category	: that.category,					
					type 					: that.type,
					maindeliverable			: that.main_deliverable,
					owner					: that.owner,
					delivered_weight		: that.weight, 
					quality_weight			: that.quality_weight,
					other_weight			: that.other_weight,
					deadline				: that.deadline,
					remind_on				: that.remind_on
				}
		if( valid == true){		
			jsDisplayResult("info", "info", "Updating Deliverable . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=editDeliverable", data, function( response ){
				if(response.error)
				{				
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}																																			
			},"json");
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} ,
	
	update						: function(){
		var that 		 		= this;
		that.response	 		= $("#response").val();
		that.status 			= $("#status :selected").val();
		that.remind_on		 	= $("#remindon").val();		

		var valid  = Deliverable.isValid( that );
		var data = {
					id						: $("#deliverableid").val(),
					response				: that.response, 
					deliverable_status		: that.status,
					remind_on				: that.remind_on
				}
		if( valid == true){	
			jsDisplayResult("info", "info", "Updating Deliverable . . . .<img src='../images/loaderA32.gif' />" );

			$.post("controller.php?action=updateDeliverable", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}													   
			},"json");
		} else{
			jsDisplayResult("error", "error", valid );	
		}
	} ,

	
	authorize						: function(){
		var that 		 		= this;
		that.response	 		= $("#response").val();
		that.approve 			= $("#approval :selected").val();	
		
		var valid  = Deliverable.isValid( that );
		var data = {
					id				: $("#deliverableid").val(),
					response		: that.response, 
					approve			: that.approve,
					requestapproval	: $("#requestapproval").val()
				}
		if( valid == true){	
			jsDisplayResult("info", "info", "Authorizing deliverable . . . .<img src='../images/loaderA32.gif' />" );		
			$.post("controller.php?action=authorizeDeliverable", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}												   
			},"json");
		} else{
			jsDisplayResult("error", "error", valid );		
		}
	} ,
	

	viewUpdateLog		: function(){
		$("#loading").html("")
		$.post("controller.php?action=getDelUpdateLogs", {id : $("#deliverableid").val()}, function( response ){																			
				$("#updatelog").html("");																					
				if( $.isEmptyObject(response)){
						$("#updatelog").html("There are no update logs yet")																																																								} else {
						$("#updatelog").append($("<table />",{id:"table_updatelog"})
									.append($("<tr />")
										.append($("<th />",{html:"Date"}))
										.append($("<th />",{html:"Audit Log"}))
										.append($("<th />",{html:"Status"}))								
									)		
								  )
						$.each( response, function( index, editLog ){
								
								$("#table_updatelog")
									.append($("<tr />")
										.append($("<td />",{html:editLog.date}))
										.append($("<td />",{html:editLog.changes}))
										.append($("<td />",{html:editLog.status}))								
									)					   
						})																																																						}
																																																					
				}, "json");
		
		
	} ,
	
	viewEditLog			: function(){
		$("#loading").html("")		
		$.post("controller.php?action=getEditLogs", {id : $("#deliverableid").val()}, function( response ){					
			$("#editLog").html("");																																					
			if( $.isEmptyObject(response)){
				$("#editLog").html("There are no edit logs yet")																																																								} else {
				$("#editLog").append($("<table />",{id:"table_editlog"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, editLog ){
						
						$("#table_editlog")
							.append($("<tr />")
								.append($("<td />",{html:editLog.date}))
								.append($("<td />",{html:editLog.changes}))
								.append($("<td />",{html:editLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
	} ,
	
	viewAuthorization	: function()
	{
		$("#loading").html("")		
		$.post("controller.php?action=getDeliverebleAuthorizations",
			   {id : $("#deliverableid").val()}, function( response ){											
			$("#editLog").html("");																																					
			if( $.isEmptyObject(response)){
				$("#editLog").html("There are no edit logs yet")																																																								} else {
				$("#editLog").append($("<table />",{id:"table_editlog"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, editLog ){
						
						$("#table_editlog")
							.append($("<tr />")
								.append($("<td />",{html:editLog.date}))
								.append($("<td />",{html:editLog.changes}))
								.append($("<td />",{html:editLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
	 } ,
	 
	isValid 			: function( field )
	{	
		if( field.name == ""){
			return "Enter deliverable name  "
		} else if( field.description == ""){
			return "Enter the deliverable description";
		} else if( field.category == ""){
			return "Select deliverable category";
		}  else if( field.type == ""){
			return "Select deliverable type";
		} else if( field.type == 1 && field.main_deliverable ==  ""){
				return "Please select the main deliverable";	
		} else if( field.owner == "") {
			return "Please select the owner";
		} else if( field.weight == "" && !$(".delweights").is(":hidden")) {
			return "Select the delivered weight";
		} else if( field.quality_weight == "" && !$(".delweights").is(":hidden")){
			return "Select the quality weight";
		} else if( field.other_weight == "" && !$(".delweights").is(":hidden")){
			return "Select the other weight";
		} else if( field.deadline == "") {
			return "Select the dealine date";
		}
		return true;
	} ,
	
	emptyFields 		: function( that )
	{	
		$.each( that, function( index, field){
		   $("#"+index).val("");
		});			
	} , 
	
	uploadFile		: function( $this )
	{
		$("#uploading")
		 .ajaxStart(function(){
			$(this).append($("<img />",{src:"../images/arrows16.gif"}))				 
		 })
		 .ajaxComplete(function(){
			//$(this).html("");				
		});
		
		$.ajaxFileUpload({
			url	 	      : "controller.php?action=uploadDeliverable",
			secureuri     : false,
			fileElementId : $this.id,
			dataType	  : "json",
			success		  : function( response , status){
				//console.log( response );
				if( typeof(response.error) != 'undefined'){
					
					if( response.error){
						$("#uploading").append( response.text+"<br />" )
					} else {
						//$("#uploading").append( data.text+"<br />" );
						$("#uploading").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									   .css({"color":"green"});
						if( $.isEmptyObject( response.filesuploaded ) ){
							$("#uploading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#uploading").append( file+"<br />" )											 
							});							
						}
													
					}
				}
				
			} , 
			error		: function(response, status, e){
				//console.log( "Ajax error "+e)
			}
		});
	}

	
	
};