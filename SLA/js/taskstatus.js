// JavaScript Document
$(function(){
	var statusid = $("#statusid").val();    
	if(statusid == undefined){
		TaskStatus.get();		 
	}
	
	$("#save").click(function(){
		TaskStatus.save();
		return false;						  
	});
	
	$("#update").click(function(){
		TaskStatus.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		TaskStatus.changeStatus( statusid, $("#status").val() );							
		return false;
	})
});

var TaskStatus = {
	name 		: "",
	client		: "",
	color 		: "",
	get 		: function()
	{
		$.getJSON("controller.php?action=getTaskStatuses", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					TaskStatus.display( status );					   
			   });	
			}														  
		});		
	} , 
	
	save 		: function()
	{
		var that 	= this;
		that.name 	= $("#name").val();
		that.color  = $("#color").val();
		var valid   = TaskStatus.isValid(that);
		var data    = {name:that.name, client_terminology:that.client, color:that.color};
		
		if(valid == true){
			$.post("controller.php?action=saveTaskStatus", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					TaskStatus.emptyFields( data );						
					data.id 	= response.id;
					data.active = 1;					
					TaskStatus.display( data );							
				} else{
					jsDisplayResult("error", "error", response.text );
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid );
		}
	} , 
	
	changeStatus 	: function( id, tostatus )
	{
		$.post("controller.php?action=changeTaskStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");
		
	} , 
	
	update 		: function( )
	{
		var that 	= this;
		that.name 	= $("#name").val();
		that.color  = $("#color").val();
		
		var valid   = TaskStatus.isValid(that);
		var data    = {name:that.name, color:that.color, id:$("#statusid").val()};		
		if( valid == true ){
			$.post("controller.php?action=updateTaskStstus", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}													 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", response.text );
		}
		
	} , 
	
	display		: function( tstatus )
	{
		$("#table_taskstatus")
		  .append($("<tr />",{id:"tr_"+tstatus.id})
			 .append($("<td />",{html:tstatus.id}))
			 .append($("<td />",{html:tstatus.name}))
			 .append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":tstatus.color}))	   
			  )
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+tstatus.id, id:"edit_"+tstatus.id, type:"submit", value:"Edit"}))			
			  )
		   )
		  
		  $("#edit_"+tstatus.id).live("click", function(){
			document.location.href = "edit_tstatus.php?id="+tstatus.id+"&";
			return false;
		  });
		  
		  $("#del_"+tstatus.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				TaskStatus.changeStatus( tstatus.id, parseInt(tstatus.status) + 2 );
				$("#tr_"+tstatus.id).fadeOut()
			}
			return false;										  
		  });
		  
		  $("#change_"+tstatus.id).live("click", function(){
			document.location.href = "change_tstatus.php?id="+tstatus.id+"&";
			return false;
		  });		  
	} , 
	
	emptyFields : function( fields )
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});
	} , 
	
	isValid 	: function( that )
	{
		if( that.name == "" ){
			return "Status name is required";		
		} 
		return true;
	}
	
	
}