// JavaScript Document
$(function(){

	var statusid = $("#statusid").val();
	if( statusid == undefined){
		ContractStatus.get();	
	}
	
	$("#edit").click(function(){
		ContractStatus.update();						  
		return false;					  
	});

	$("#updatestatus").click(function(){
		ContractStatus.changestatus();						  
		return false;									  
	});
	
	$("#save").click(function(){
		ContractStatus.save();	
		return false;					  
	});
	
});


var ContractStatus = {
	name       : "",
	client     : "",
	color 	   : "", 
	that       : this,
	get   : function()
	{
		$.getJSON("controller.php?action=getContractStatus", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else {
				$.each( data, function( index, status){
					ContractStatus.display( status );							   
				})			
			}													  
		});
	} ,	
	
	isValid 	: function ()
	{
		var that = this;		
		if(that.name == "")
		{
			return "Please enter the name";	
		} else if( that.client == ""){
			return "Please enter the client terminology";	
		} else if( that.color == ""){
			return "Please select the color corresponding to this status"	
		}
		return true;
		
	} , 
	
	save: function()
	{
		var that 	 =  this;
		that.name    = $("#name").val();
		that.client  = $("#client_terminology").val();
		that.color   = $("#color").val() 

		var valid 	 = ContractStatus.isValid();
		var data     = { name:that.name, client_terminology:that.client, color:that.color  }; 
	
		if( valid ==  true )
		{
			jsDisplayResult("info", "info", "Saving contract status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveStatus", data , function( response ){
				if( !response.error )
				{
					jsDisplayResult("ok", "ok", response.text );	
					data.id 	= response.id;
					data.status = 1; 
					ContractStatus.display( data );
					ContractStatus.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text );
				}															 
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid );		
		}
		
	} ,
	
	update: function()
	{
		var that 	 =  this;
		that.name    = $("#name").val();
		that.client  = $("#client_terminology").val();
		that.color   = $("#color").val() 

		var valid 	 = ContractStatus.isValid();
		var data     = { name:that.name, client_terminology:that.client, color:that.color, id:$("#statusid").val()  }; 
		if( valid == true){	
			jsDisplayResult("info", "info", "Updating contract status . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateStatus", data, function( response ){
				ContractStatus.displayResponse( response );									
			},"json");	
		} else{
			
		}
	} ,
	
	_delete: function()
	{
		
	}, 
	display : function( cstatus )
	{
		$("#table_contractstatus")
		  .append($("<tr />", { id:"tr_"+cstatus.id})
			.append($("<td />",{html:cstatus.id}))
			.append($("<td />",{html:cstatus.name}))
			.append($("<td />",{html:cstatus.client_terminology}))
			.append($("<td />")
			  .append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":cstatus.color, "width":"100px"}))		  
			)			
			.append($("<td />")
				.append($("<input />",{id:"edit_"+cstatus.id, type:"submit", name:"edit_"+cstatus.id,value:"Edit"}))	
				.append($("<input />",{type:"submit", id:"del_"+cstatus.id, disabled:((cstatus.status & 4) == 4 ? "disabled" : ""), name:"del_"+cstatus.id,value:"Delete"}))				
			 )
			.append($("<td />",{html:((cstatus.status & 1) == 1   ? "<b>Active</b>" : "<b>Inactive</b>")}))
			.append($("<td />")
			   .append($("<input />",{type:"submit", disabled:((cstatus.status & 4) == 4 ? "disabled" : ""), id:"changestatus_"+cstatus.id, name:"changestatus_"+cstatus.id, value:"Change Status"}))
			 )			
		  )
		  
		  $("#edit_"+cstatus.id).live("click", function(){
			document.location.href = "edit_cstatus.php?id="+cstatus.id+"&";											
			return false;											
		 });
		  
		  $("#del_"+cstatus.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status "))
			{
				jsDisplayResult("info", "info", "Updating contract status . . . .<img src='../images/loaderA32.gif' />" );
				$.post("controller.php?action=changeCStatus", { 
					   	id 				: cstatus.id,
						tostatus 		: parseInt(cstatus.status)+ 2
						
					}
				, function( response ){
					if(!response.error){
						$("#tr_"+cstatus.id).fadeOut();
						jsDisplayResult("ok", "ok", response.text );
					} else{
						jsDisplayResult("error", "error", response.text );	
					}	
				},"json");	
			}
			return false;											
		 });
		  
		 $("#changestatus_"+cstatus.id).live("click", function(){
			document.location.href = "change_cstatus.php?id="+cstatus.id+"&";															
			return false;											
		 });		  
		  
	}, 
	
	changestatus 	: function()
	{

		$.post("controller.php?action=changeCStatus", { id : $("#statusid").val(), tostatus: $("#status").val()}, function( response ){
				ContractStatus.displayResponse( response );
			},"json");	
	}, 
	emptyFields 		  : function( fields )
	{
		$.each( fields , function(index, value){
			$("#"+index).val("");					  
		}); 		
	}, 	
	
	displayResponse 	: function( response )
	{
		if(!response.error){
			jsDisplayResult("ok", "ok", response.text );
		} else{
			jsDisplayResult("error", "error", response.text );	
		}	
	}
	
}
