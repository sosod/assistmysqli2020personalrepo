/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/1/11
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

    $(".datepicker").live("focus", function(){
        $(this).datepicker({changeMonth:true, changeYear:true, dateFormat:"dd-MM-yy"});
    });

    $(".contractf").attr("checked", "checked");
    $(".actionf").attr("checked", "checked");
    $(".deliverablef").attr("checked", "checked");
    
    $("#quick_report").live("change", function(){
        document.location.href = "process_report.php?generate_quick_report=1&id="+$(this).val();
    });

    $("#r_checkAll").click(function(){
        $(".contractf").attr("checked", "checked");
        return false;
    });

    $("#r_uncheckAll").click(function(){
        $(".contractf").attr("checked", "");
        return false;
    });

    $("#r_invert").click(function(){
        $(".contractf").each(function(index, field){
            if($(this).is(":checked")){
                $(this).attr("checked", "")
            } else {
                $(this).attr("checked", "checked");
            }
        });
        return false;
    });

    $("#d_checkAll").click(function(){
        $(".deliverablef").attr("checked", "checked");
        return false;
    });

    $("#d_uncheckAll").click(function(){
        $(".deliverablef").attr("checked", "");
        return false;
    });

    $("#d_invert").click(function(){
        $(".deliverablef").each(function(index, field){
            if($(this).is(":checked")){
                $(this).attr("checked", "")
            } else {
                $(this).attr("checked", "checked");
            }
        });
        return false;
    });

    $("#a_checkAll").click(function(){
        $(".actionf").attr("checked", "checked");
        return false;
    });

    $("#a_uncheckAll").click(function(){
        $(".actionf").attr("checked", "");
        return false;
    });

    $("#a_invert").click(function(){
        $(".actionf").each(function(index, field){
            if($(this).is(":checked")){
                $(this).attr("checked", "")
            } else {
                $(this).attr("checked", "checked");
            }
        });
        return false;
    });

    Generate.getActiveType();
    Generate.getSupplier();
    Generate.getUsers();
    Generate.getTemplates();
    Generate.getAssessmentFrequency();
    Generate.getNotificationFrequency();
	Generate.getActiveCategory();
});

Generate    = {



	getActiveType 	   :    function()
	{
		$.getJSON("controller.php?action=getActiveType", function( types ){
			$.each( types, function( index, type ){
				$("#type").append($("<option />",{value:type.id, text:type.name}))
			});
		})
	} ,
	getActiveCategory :    function( id )
	{
		$.post("controller.php?action=getActiveCategory", function( categories ){
			$.each( categories, function( index, category ){
				$("#category").append($("<option />",{value:category.id, text:category.name}))
			});
		}, "json")
	},
	getSupplier 	:    function()
	{
		$.getJSON("controller.php?action=getSupplier", function( suppliers ){
			$.each( suppliers, function( index, supplier){
				$("#supplier").append($("<option />",{value:supplier.id, text:supplier.name}))
			});
		})
	} ,

	getUsers	   : function()
	{
		$.getJSON("controller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
					$("#contract_owner_").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))

					$("#contract_authorisor").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))

				if( (user.status & 2) == 2 ){
					$("#contract_manager").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid}))
				}
			});
		});
	},

	getTemplates	 : function()
	{
		$.getJSON("controller.php?action=getTemplates", function( templates ){
			$.each( templates, function( index, template){
				$("#template_").append($("<option />",{text:template.name, value:template.id}))
			});
		});
	} ,

	getAssessmentFrequency 	: function(){

		$.getJSON("controller.php?action=getAssessmentFrequency", function( assessmentFrequency ){
			$.each( assessmentFrequency, function( index, value){
				$("#assessment_frequency").append($("<option />", { text:value.name, value:value.id}))
			});
		})

	} ,

	getNotificationFrequency		: function(){

		$.getJSON("controller.php?action=getNotificationFrequency", function( notificationFrequency ){
			$.each( notificationFrequency, function( index, value){
				$("#notification_frequency").append($("<option />", { text:value.shortcode, value:value.id}))
			});
		})
	}



}