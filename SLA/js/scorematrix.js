// JavaScript Document
$(function(){
	var scorematrixid = $("#scorematrixid").val()
	if(typeof scorematrixid == "undefined"){
		ScoreMatrix.get();		
	}
	
	$("#updatestatus").click(function(){
		ScoreMatrix.changeStatus( scorematrixid, $("#status").val());							
		return false;
	});	
	
	$("#update").click(function(){
		ScoreMatrix.update();							
		return false;
	});
	
	$("#save").click(function(){
		ScoreMatrix.save();
		return false;
	})
			   
})

var ScoreMatrix	= {
	
	dscore		 : "",
	description  : "",
	definition	 : "",
	color		 : "",
	
	get				: function()
	{
		$.getJSON("controller.php?action=getScoreMatrixs", function( data ){
			if( $.isEmptyObject( data ) )
			{
			    $("#loading").html("No data returned");
			} else {
				$.each( data, function( index, scoreMatrix ) {
					ScoreMatrix.display( scoreMatrix )				   
				});
			}																 
		})
	} , 
	display 	: function( smatrix )
	{
		$("#table_scorematrix")
		  .append($("<tr />",{id:"tr_"+smatrix.id})
			.append($("<td />",{ html:smatrix.id}))
			.append($("<td />",{ html:smatrix.dscore}) )
			.append($("<td />",{ html:smatrix.description}))			
			.append($("<td />",{ html:smatrix.definition}))
			.append($("<td />")
				.append($("<span />",{html:"&nbsp;&nbsp;&nbsp;"}).css({"background-color":smatrix.color}))		  
			)
			.append($("<td />")
			  .append($("<input />",{id:"edit_"+smatrix.id, name:"edit_"+smatrix.id, type:"submit", value:"Edit"}))
			  .append($("<input />",{id:"del_"+smatrix.id, name:"del_"+smatrix.id, type:((smatrix.status & 4) == 4 ? "hidden" : "submit"), value:"Delete"}))			  
			)
		   .append($("<td />",{ html:((smatrix.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
		   .append($("<td />")
			 .append($("<input />",{id:"change_"+smatrix.id, name:"change_"+smatrix.id, type:((smatrix.status & 4) == 4 ? "hidden" : "submit"), value:"Change Status"}))		 
		   )
		  )
		
		$("#edit_"+smatrix.id).live("click",function(){
			document.location.href = "edit_smatrix.php?id="+smatrix.id+"&";
			return false;
		});
		
		$("#del_"+smatrix.id).live("click",function(){
			if(confirm("Are you sure you want to delete this deliverable status")){
				ScoreMatrix.changeStatus( smatrix.id , parseInt(smatrix.status) + 2);
				$("#tr_"+smatrix.id).fadeOut();
			}
			return false;
		});
		
		$("#change_"+smatrix.id).live("click", function(){
			document.location.href = "change_smatrix.php?id="+smatrix.id+"&";														
			return false;												
		});
		
	} ,
	save 	 		: function()
	{
		var that          = this;
		that.dscore  	  =	$("#dscore").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val();
		
		var valid = ScoreMatrix.isValid( that );
		var data  = { dscore : that.dscore, description : that.description,definition : that.definition,color:that.color  }; 
		if( valid == true ){
			jsDisplayResult("info", "info", "Saving score matrix . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveScoreMatrix", data, function( response ){
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text);
					data.id 	= response.id;
					data.status = 1
					ScoreMatrix.display( data );
					ScoreMatrix.emptyFields( data );
				} else{
					jsDisplayResult("error", "error", response.text);
				}				
			}, "json");	
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} ,
	
	update 			: function()
	{
		var that    	  = this;
		that.dscore  	  =	$("#dscore").val();
		that.description  = $("#description").val();
		that.definition	  = $("#definition").val();	
		that.color		  = $("#color").val(); 
		
		var valid = ScoreMatrix.isValid( that );
		var data  = { dscore:that.dscore, description : that.description,definition : that.definition,color:that.color , id:$("#scorematrixid").val() };   		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating score matrix . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateScoreMatrix", data , function( response ){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}																	 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	changeStatus	: function(id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating score matrix . . . .<img src='../images/loaderA32.gif' />" );		
		$.post("controller.php?action=changeScoreMatrix", {id:id, tostatus : tostatus}, function( response ) {
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																											 
		}, "json");
	} ,
	
	emptyFields		: function( fields )
	{
		$.each( fields , function(index, value){
			if( index == "color"){	
				$("#"+index).css("background-color","#FFFFFF").val("");
			} else {
				$("#"+index).val("");
			}				  
		}); 
	} ,
	
	isValid			:function( that)
	{
		if( that.dscore == ""){
			return "Please enter the delieverable score"; 	
		} else if( isNaN(that.dscore) ){
			return "Please enter valid deliverable score"; 	
		} else if( that.description == ""){
			return "Please enter the description";	
		} else if( that.definition == ""){
			return "Please enter the definition";	
		}
		return true;
	}
	
	
}