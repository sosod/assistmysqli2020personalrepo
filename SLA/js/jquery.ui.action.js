// JavaScript Document
$.widget("ui.action", {
		 
	options :	{
		delId			: "",
		contractId 		: "",
		editAction		: false,
		deleteAction	: false,
		approveAction	: false,
		updateAction	: false,	
		authorizeAction	: false,
		urlLocation		: "",
		url				: "controller.php?action=getActions",
		id				: Math.round((8*8))+1,
		start			: 0, 
		limit			: 10,
		current			: 1,
		total			: 0,
		page			: ""
	} , 

	_init	: function(){	
	
		if(this.options.page == "approval"){
			this._actionsApproval();
		} else{
			this._getActions();
		}
	} , 
	
	_create	: function(){
		
	} , 
	
	_getActions		: function (){
		$this = this;
		
		$this.element.append($("<div />",{id:"loader"}));
		$("#loader").ajaxStart(function(){
			$(this).html("Loading . . <img src='../images/loaderA32.gif' />")
					.css({	
							"background-color":"", 
							"position"  :"absolute",
							"z-index"	:"9999",
							"top"		: "200px",
							"left"		: "500px"
						 })				 
		});
		$("#loader").ajaxComplete(function(){
			$(this).empty();					 
		});
		
		$.post($this.options.urlLocation+""+$this.options.url,{
			   	start	: $this.options.start,
				limit 	: $this.options.limit,
				id		: $this.options.delId
		} , function( data ){			
			if( $.isEmptyObject( data.actions ) ){
				$this.element.html("No actions ")
			} else {
				$this.options.total = data.total;
				$this.element.append($("<table />",{id:"tableactions_"+$this.options.delId}));
				$this._createPager( data.total, data.columns, "tableactions_"+$this.options.delId);
				$this._displayHeaders( data.headers , "tableactions_"+$this.options.delId);
				$this._displayActions( data.actions, "tableactions_"+$this.options.delId, data.actionstatus );				
			}
			//$this._displayDeliverable( data.deliverable );
		},"json");
	} , 
	
	
	_actionsApproval	: function(){
		$this = this;

		$.post("controller.php?action=approvalData", { id : $("#deliverableid").val() }, function( data ){
																								  
			$this._displayHeaders( data.headers,  "actions_awaiting" )
			$this._displayHeaders( data.headers,  "actions_approved" )
			
			if( $.isEmptyObject(data.approved)){
				$("#actions_approved").append($("<tr />",{id:"tr_noapproved"})
										 .append($("<td />",{colspan:"8", html:"No actions approved"}))		
									   )
			} else {
				$this._displayActions( data.approved, "actions_approved" , data.actionStatues)					
			}

			if( $.isEmptyObject(data.awaiting) ){
				$("#actions_awaiting").append($("<tr />",{id:"tr_noawaiting"})
										 .append($("<td />",{colspan:"8",html:"No actions awaiting approval"}))		
									   )				
			} else {
				$this.options.approveAction = true;
				$this._displayActions( data.awaiting, "actions_awaiting"  , data.actionStatues )
			}
			
		}, "json");
	} , 		
	
	_displayHeaders	: function( heading , table ){
		var $this = this;
		
		var tr = $("<tr />")
		$.each( heading, function( index, head){
			tr = tr.append($("<th />",{html:head}))					  
		});
		
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.editAction ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.updateAction ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.approveAction ? "table-cell" : "none")}))
		tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.authorizeAction ? "table-cell" : "none")}))		
		$("#"+table).append( tr )
	} ,
 	
	_displayActions	: function( actions , table, status ){
		
		$.each( actions , function( ref, action){	
			var tr = $("<tr />", {id:"tr_"+action.ref})
			$.each( action, function( index, val){
				if( index !== "userAccess"){
					tr.append($("<td />",{html:val}))	
				}								
			});
			var updateState = false;
			var editState   = false;
			var deleteState = false;
			var lockedState = false;
			
			if( (status[action.ref] & 16) == 16  ){
				lockedState = true;
				editState   = true;
				deleteState = true;
				updateState = true;
			} 
			
			if( (status[action.ref] & 2) == 2 ){
				editState   = true;
				deleteState = true;
				updateState = true;
			}
			
			if( action.userAccess  == false ){
				updateState = true;
			}
			
			if( (status[action.ref] & 4) == 4  ){
				updateState = true; 	
			}
			
			
			
			tr = tr.append($("<td />")
				   	 .append($("<input />",{
   		 					type 	: "submit" ,
   		 					name 	: "edit_"+action.ref,
   		 					id	 	: "edit_"+action.ref,
   		 					value	: (lockedState ? "Locked" : "Edit"),
			   				disabled: (editState ? "disabled" : "")//(((status[action.ref] & 2) == 2 || (status[action.ref] & 16) == 16)  ? "diabled" : "")
			   				}))	
			   		.append($("<input />",{
	  					  type	   :"submit",
	  					  name	   :"delete_"+action.ref,
	  					  id	   :"delete_"+action.ref,
	  					  value    :(lockedState  ? "Locked" : "Delete"), 
	  					  disabled :(deleteState ? "disabled" : "")//(((status[action.ref] & 2) == 2 || (status[action.ref] & 16) == 16)  ? "diabled" : "")
	  					  }))					   				
					.css({"display":($this.options.editAction ? "table-cell" : "none")})
			)
				
			tr = tr.append($("<td />")
			   	 .append($("<input />",{
	 					type	:"submit", 
	 					name	:"update_"+action.ref,
	 					id		:"update_"+action.ref,
	 					value	:(lockedState  ? "Locked" : "Update"),
		   				disabled:(updateState ? "disabled" : "")//((((status[action.ref] & 2) == 2 || action.userAccess  == false) || (status[action.ref] & 16) == 16)  ? "diabled" : "")
		   				}))		   
					.css({"display":($this.options.updateAction ? "table-cell" : "none")})
			)
				
			tr = tr.append($("<td />")
					 .append($("<input />",{
						 					type	: "submit",
						 					name 	: "approve_"+action.ref,
						 					id		: "approve_"+action.ref,
						 					value	: "Approve",
						 					disabled:((status[action.ref] & 2) == 2 ? "diabled" : "")
					 						}))
				.css({"display":($this.options.approveAction ? "table-cell" : "none")})
			)
			tr = tr.append($("<td />")
			   	 .append($("<input />",{
			   		 						type	: "submit", 
			   		 						name	: "authorize_"+action.ref,
			   		 						id		: "authorize_"+action.ref,
			   		 						value	: "Authorize",
			   		 						disabled:((status[action.ref] & 2) == 2 ? "diabled" : "")
			   		 					}))						
					.css({"display":($this.options.authorizeAction ? "table-cell" : "none")})
			)	
						
						
			$("#edit_"+action.ref).live("click", function(){
				document.location.href = "../manage/edit_action.php?id="+action.ref+"&contractid="+$this.options.contractId;
				return false;
			});
			
			$("#authorize_"+action.ref).live("click", function(){
				document.location.href = "authorize_action.php?id="+action.ref+"&contractid="+$this.options.contractId;
				return false;
			});
			
			$("#delete_"+action.ref).live("click", function(){
				if(confirm("Are you sure you want to delete this deliverable")){
					jsDisplayResult("info", "info", "Deleting action . . . .<img src='../images/loaderA32.gif' />" );					
					$.post("controller.php?action=deleteAction", {
						   id 		: action.ref 
						   }, function( response ){
						if( !response.error ){
							jsDisplayResult("ok", "ok", response.text );
							$("#tr_"+action.ref).fadeOut()
						} else {
							jsDisplayResult("error", "error", response.text );
						}																					
					},"json");
				}
				return false;
			})
			
			$("#approve_"+action.ref).live("click", function(){
				$("#action_approval").dialog({
								autoOpen	: true,
								modal		: true, 
								width		: "350px",
								height		: "auto",
								title		: "Approve Action", 
								buttons		: {

											"Decline" : function(){
													 $this._declineAction( action )													  
													// $(this).dialog().dialog("destroy")																				
												} ,
											 "Approve" : function(){
												 $this._approveAction( action )													 
											  } 				
											 }
							  })										 
				return false;
			});
			
			$("#update_"+action.ref).live("click", function(){
				document.location.href = "update_action.php?id="+action.ref+"&contractid="+$this.options.contractId;;
				return false;
			});							
			$("#"+table).append( tr );
		});
		
	} ,
		
	_approveAction	: function( action )
	{
		$this = this;
		if( $("#response").val() == ""){
			jsDisplayResult("error", "error", "Please enter the response" );
			return false;	
		} else{
			$("#approve_loader").ajaxStart(function(){
				$(this).html("Approving action . . . <img src='../images/loaderA32.gif' />");									
			});
			
			$("#approve_loader").ajaxComplete(function(){
				//$(this).html("");									
			});			
			
			$.post("controller.php?action=approveAction", { id : action.ref,response : $("#response").val(), contractid:$this.options.contractId }, function( response ){																		
					if( !response.error){
						jsDisplayResult("ok", "ok", response.text );
						$("#actions_awaiting").find("#tr_"+action.ref).remove();
						$this.options.approveAction	= false;
						var actions = {};
						actions = { 1 : action };
						$("#actions_approved").find("#tr_noapproved").remove();
						$this._displayActions( actions, "actions_approved")
						
						$("#action_approval").dialog("destroy")
					} else {
						jsDisplayResult("error", "error", response.text );
					}																			
			},"json");
		}
		
	} ,
	
	_declineAction	: function( action )
	{
		if( $("#response").val() == ""){
			jsDisplayResult("error", "error", "Please enter the response" );
			return false;	
		} else{
			$("#approve_loader").ajaxStart(function(){
				$(this).html("Approving action . . . <img src='../images/loaderA32.gif' />");									
			});
			
			$("#approve_loader").ajaxComplete(function(){
				$(this).html("");									
			});			
			
			$.post("controller.php?action=declineAction", { id : action.ref, response : $("#response").val(), contractid  :$this.options.contractId },
				function( response ){																		
					if( !response.error){
						jsDisplayResult("ok", "ok", response.text );
						$("#action_approval").dialog("destroy")
					} else {
						jsDisplayResult("error", "error", response.text );				
					}																		
				},"json");
		}
		
	} ,
	
	_createPager 		: function(total, columns , table) {
		var rk 		= this;
		
		var pages;
		if( total%rk.options.limit > 0){
			pages   = Math.ceil(total/rk.options.limit); 				
		} else {
			pages   = Math.floor(total/rk.options.limit); 				
		}
		$("#"+table)
		  .append($("<tr />")
			.append($("<td />",{colspan:columns})
			   .append($("<input />",{type:"button", name:"first", id:"first", value:" |< ", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"previous", id:"previous", value:" < ", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{html:"Page "+rk.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", name:"next", id:"next", value:" > ", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", name:"last", id:"last", value:" >| ", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))			   
		   )
		  .append($("<td />",{colspan:"2", html:"&nbsp;"}))
		  )

		$("#next").bind("click", function(evt){
			rk._getNext( rk );
		});
		$("#last").bind("click",  function(evt){
			rk._getLast( rk );
		});
		$("#previous").bind("click",  function(evt){
			rk._getPrevious( rk );
		});
		$("#first").bind("click",  function(evt){
			rk._getFirst( rk );
		});			

	} ,		
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getActions();
	},	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getActions();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getActions();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getActions();				
	}	
		
		 
})