// JavaScript Document
$(function(){
	var suppcatid = $("#suppcatid").val();    

	if(suppcatid == undefined){
		SupplierCategory.get();		 
	}
	
	$("#save").click(function(){
		SupplierCategory.save();
		return false;						  
	});
	
	$("#update").click(function(){
		SupplierCategory.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		SupplierCategory.changeStatus( suppcatid, $("#status").val() );							
		return false;
	})
		   
});

var SupplierCategory = {
	
	name 		: "",
	description : "",
	shortcode   : "", 
	notes		: "",
	
	get 		 : function()
	{
		$.getJSON("controller.php?action=getSupplierCategories", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					SupplierCategory.display( status );					   
			   });	
			}														  
		});		
		
	} , 
	
	save		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		that.notes		 = $("#notes").val();
		var valid   	 = SupplierCategory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, notes:that.notes};
		
		if(valid == true){
			jsDisplayResult("info", "info", "Saving supplier category . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=saveSupplierCategory", data, function(response){				
				if( !response.error ) {
					jsDisplayResult("ok", "ok", response.text );
					SupplierCategory.emptyFields( data );		
					data.id 	= response.id;
					data.status = 1;					
					SupplierCategory.display( data );					
				
				} else{
					jsDisplayResult("error", "error", response.text );
				}																
			}, "json");	
		} else{
			jsDisplayResult("error", "error", valid  );
		}		
	} , 
	
	update		 : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		that.notes		 = $("#notes").val();
		var valid   	 = SupplierCategory.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, notes:that.notes, id:$("#suppcatid").val()};		
		if( valid == true ){
			jsDisplayResult("info", "info", "Updating supplier category . . . .<img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=updateSupplierCategory", data, function(response){
				if(!response.error){
					jsDisplayResult("ok", "ok", response.text );
				} else{
					jsDisplayResult("error", "error", response.text );	
				}													 
			}, "json")	
		} else {
			jsDisplayResult("error", "error", valid  );
		}		
	} ,
	
	changeStatus  : function( id, tostatus)
	{
		jsDisplayResult("info", "info", "Updating supplier category . . . .<img src='../images/loaderA32.gif' />" );
		$.post("controller.php?action=changeSupplierCategory", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				jsDisplayResult("ok", "ok", response.text );
			} else{
				jsDisplayResult("error", "error", response.text );	
			}																																					
		}, "json");		
	} , 
	
	display		  : function( suppcat )
	{	
		$("#category").append($("<option />",{value:suppcat.id, text:suppcat.name}))
		
		$("#table_suppliercategory")
		  .append($("<tr />",{id:"tr_"+suppcat.id})
			 .append($("<td />",{html:suppcat.id}))
			 .append($("<td />",{html:suppcat.shortcode}))			 
			 .append($("<td />",{html:suppcat.name}))
			 .append($("<td />",{html:suppcat.description}))
			 .append($("<td />",{html:suppcat.notes}))			 
			 .append($("<td />")
				.append($("<input />",{name:"edit_"+suppcat.id, id:"edit_"+suppcat.id, type:"submit", value:"Edit"}))
				.append($("<input />",{disabled:((suppcat.status & 4) == 4 ? "disabled" : ""), name:"del_"+suppcat.id, id:"del_"+suppcat.id, type:"submit",value:"Delete"}))				
			  )
			 .append($("<td />",{html:((suppcat.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:((suppcat.status & 4) == 4 ? "disabled" : ""), id:"change_"+suppcat.id, name:"change_"+suppcat.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+suppcat.id).live("click", function(){
			document.location.href = "edit_suppliercategory.php?id="+suppcat.id+"&";
			return false;
		  });
		  
		  $("#del_"+suppcat.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				SupplierCategory.changeStatus( suppcat.id, parseInt(suppcat.status) + 2);
				$("#tr_"+suppcat.id).fadeOut();
			}
			return false;										  
		  });
		  
		  $("#change_"+suppcat.id).live("click", function(){
			document.location.href = "change_suppliercategory.php?id="+suppcat.id+"&";
			return false;
		  });		
	} , 
	
	isValid		  : function(that)
	{
		if( that.name == "" ){
			return "Contract name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the contract short code";
		} else if( that.description == ""){
			return "Please enter the contract type description";
		}
		return true;				
	} , 
	
	emptyFields   : function(fields)
	{
		$.each(fields, function( index, field){
			$("#"+index).val("");					
		});		
	}
		
}