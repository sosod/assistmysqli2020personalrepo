<?php
	$scripts = array( 'generate.js','menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../header.php");
?>
<script>
$(function(){
    document.location.href = "generate.php";
});
</script>
<!--
<div id="searchBox">
<div id="generate_report_messsage" class="message"></div>
<form action="process_report.php"name="report-header-form" id="report-header-form" method="post">
<table id="query_field_tobeincluded">
	<tr>
		<th colspan="3" align="left">Select the Query information to be displayed on the report</th>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[query_category]" value="1" id="risk_category" class="queryf" />Query Category</td>
		<td><input type="checkbox" name="header[query_reference]" value="1" id="query_reference" class="queryf" />Query Reference</td>
		<td><input type="checkbox" name="header[query_type]" value="1" id="query_type" class="queryf" />Query Type</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[query_background]" value="1" id="risk_background" class="queryf" />Background of Query</td>
		<td><input type="checkbox" name="header[query_date]" value="1" id="query_date" class="queryf" />Query Date</td>
		<td><input type="checkbox" name="header[query_description]" value="1" id="risk_description" class="queryf" >Query Description</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[monetary_implication]" value="1" id="monetary_implication" class="queryf" />Monetary Implication</td>
		<td><input type="checkbox" name="header[query_owner]" id="query_owner" value="1" class="queryf" />Directorate</td>
        <td><input type="checkbox" name="header[financial_exposure]" id="financial_exposure" value="1" class="queryf" />Financial Exposure</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[risk_detail]" value="1" id="risk_detail" class="queryf" />Risk Detail</td>
		<td><input type="checkbox" name="header[risk_level]" value="1" id="risk_level" class="queryf" />Risk Level</td>
		<td><input type="checkbox" name="header[risk_type]" value="1" id="risk_type" class="queryf" />Risk Type</td>
	</tr>
    <tr>
        <td><input type="checkbox" name="header[client_response]" value="1" id="client_response" class="queryf" />Client Response</td>
        <td><input type="checkbox" name="header[finding]" value="1" id="finding" class="queryf" />Finding</td>
        <td><input type="checkbox" name="header[recommendation]" value="1" id="recommendation" class="queryf" />Recommendation</td>
    </tr>
    <tr>
        <td><input type="checkbox" name="header[query_status]" value="1" id="query_status" class="queryf" />Query Status</td>
        <td></td>
        <td></td>
    </tr>
	<tr>
	<td colspan="3">
		<input type="button" value="Check All" id="r_checkAll" name="r_checkAll" />
		<input type="button" value="UnCheck All" id="r_uncheckAll" name="r_uncheckAll" />
		<input type="button" value="Invert" id="r_invert" name="r_invert" />
	</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_item]" value="1" id="action_item" class="actionf">Action Item</td>
		<td><input type="checkbox" name="aheader[action_description]" value="1" id="action_description" class="actionf">Action Description</td>
		<td><input type="checkbox" name="aheader[deliverable]" value="1" id="deliverable" class="actionf" >Action Deliverable</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_owner]" value="1" id="action_owner" class="actionf">Action Owner</td>
		<td><input type="checkbox" name="aheader[timescale]" value="1" id="timescale" class="actionf"/>Action Time Scale</td>
		<td><input type="checkbox" name="aheader[deadline]" value="1" id="deadline" class="actionf" />Action Deadline</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_reminder]" value="1" id="action_reminder" class="actionf" />Action Reminder Date</td>
		<td><input type="checkbox" name="aheader[action_status]" id="action_status" value="1" class="actionf" />Action Status</td>
		<td><input type="checkbox" name="aheader[progress]" value="1" id="progress" class="actionf" />Action Progress</td>
	</tr>
	<tr>
	<td colspan="3">
		<input type="button" value="Check All" id="a_checkAll" name="a_checkAll" />
		<input type="button" value="UnCheck All" id="a_uncheckAll" name="a_uncheckAll" />
		<input type="button" value="Invert" id="a_invert" name="a_invert" />
	</td>
	</tr>
</table>

	</form>
	<form id="report-values-form" name="report-values-form" method="post">

<table border="1" id="report_values_table" >
	<tr>
		<td colspan="3"><h3>Select the filter you wish to apply</h3></th>
	</tr>
	<tr>
		<th>Query Reference:</th>
		<td>
			<input type="text" name="values[query_reference]" id="_query_reference" value="" />
		</td>
		<td>
			<select name="match[query_reference]" id="match_query_reference" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Query Type:</th>
		<td>
			<select name="values[type]" id="_type"  multiple="multiple">
			   <option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Query Category:</th>
		<td>
			<select id="_category" name="values[category]" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Query Description:</th>
		<td>
			<input type="text" name="values[description]" id="_description" value="" />
		</td>
		<td>
			<select name="match[description]" id="match_description" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Background of the Query:</th>
		<td>
			<input type="text" name="values[background]" id="_background" value=""  />
		</td>
		<td>
			<select name="match[background]" id="match_background" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
   <tr>
		<th>Financial Exposure:</th>
		<td>
			<select id="_financial_exposure" name="values[financial_exposure]" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Monetary Implication:</th>
		<td>
			<input type="text" name="values[monetary_implication]" id="_monetary_implication" value=""  />
		</td>
		<td>
			<select id="_monetary_implication]" name="match[monetary_implication]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Risk Level:</th>
		<td>
			<select name="values[risk_level]" id="_risk_level" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Risk Type:</th>
		<td>
			<select name="values[risk_type]" id="_risk_type" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Risk Detail:</th>
		<td><input type="text" name="values[risk_detail]" id="_risk_detail" value=""  /></td>
		<td>
			<select id="match_risk_detail" name="match[risk_detail]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Finding:</th>
		<td><input type="text" name="values[finding]" id="_finding" value=""  /></td>
		<td>
			<select id="match_finding" name="match[finding]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Recommendation:</th>
		<td><input type="text" name="values[recommendation]" id="_recommendation" value=""  /></td>
		<td>
			<select id="match_recommendation" name="match[recommendation]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Client Response:</th>
		<td><input type="text" name="values[client_response]" id="_client_response" value=""  /></td>
		<td>
			<select id="match_client_response" name="match[client_response]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Query Status:</th>
		<td>
			<select name="values[status]" id="_status" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
		</tr>
	<tr>
		<th>Query Date:</th>
		<td width="300px;">
			From : <input type="text" name="values[from_query_date]" id="_from_query_date" value="" class="datepicker" style="width:20px;" />
		</td>
		<td>
         to  <input type="text" name="values[to_query_date]" id="_to_query_date" value="" class="datepicker" style="width:20px;" />
		</td>
	</tr>
</table>
</form>
<form id="grouping-options-form" name="grouping-options-form" method="post">
<table id="grouping_table" border="1" width="65%"> -
	<tr>
		<td colspan="3"><h3>Choose your group and sort options</h3></th>
	</tr>
	<tr>
		<th width="65">Group By</th>
		<td width="371" colspan="2">
			<select name="group_by" id="group_by">
				<option value="">No grouping</option>
				<option value="type_">Query Type</option>
				<option value="category_">Query Category</option>
				<option value="financial_exposure_">Financial Exposure</option>
				<option value="risk_level_">Risk Level</option>
				<option value="risk_type_">Risk Type</option>
			</select>
	  </td>
  </tr>
	<tr>
		<th>Sort by</td>
		<td colspan="2">
			<ul id="sortable" style="list-style:none;">
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__id"></span><input type=hidden name=sort[] value="__id">Query Item</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__type"></span><input type=hidden name=sort[] value="__type">Query Type</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__category"></span><input type=hidden name=sort[] value="__category">Query Category</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__description"></span><input type=hidden name=sort[] value="__description">Query Description</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__background"></span><input type=hidden name=sort[] value="__background">Query Background</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__financial_exposure"></span><input type=hidden name=sort[] value="__financial_exposure">Financial Exposure</li>
                <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__monetary_implication"></span><input type=hidden name=sort[] value="__monetary_implication">Monetary Implication</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_level"></span><input type=hidden name=sort[] value="__risk_level">Risk Level</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_type"></span><input type=hidden name=sort[] value="__risk_type">Risk Type</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_detail"></span><input type=hidden name=sort[] value="__risk_detail">Risk Detail</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__finding"></span><input type=hidden name=sort[] value="__finding">Finding</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__recommendation"></span><input type=hidden name=sort[] value="__recommendation">Recommendation</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__client_response"></span><input type=hidden name=sort[] value="__client_response">Client Response</li>
			</ul>
		</td>
	</tr>
</table>
</form>
<form id="document-format-form" name="document-format-form" method="post">
<table id="document_format_table" border="1" width="65%">
	<tr>
		<td colspan="3"><h3>Choose the document format of your report</h3></th>
	</tr>
	<tr>
		<td colspan="3"><input type="radio" id="onscreen" name="display" value="screen" checked="checked" />OnScreen Display</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="excell" name="display" value="excell" />Microsoft Excel( Plain Text)</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="excell_formated" name="display" value="excell_formated" />Microsft Excel(Formatted)</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="pdf" name="display" value="pdf" />Save to Pdf</td>
	</tr>
 </table>
</form>
<form id="generate-report-form" name="generate-report-form"  method="post">
<table id="generate_report_table" border="1" width="65%">
	<tr>
		<td colspan="3"><h3>Generate the report</h3></td>
	</tr>
	<tr>
		<th>Report Title</th>
		<td><input type="text" id="report_title" name="report_title" value="" /></td>
        <td></td>
	<tr>
	<tr>
		<th></th>
		<td>
		<input type="submit" name="generate_report" id="generate_report" value="Generate Report"  />
		<input type="reset" id="reset" name="reset" value="Reset" />
		</td>
        <td></td>
	<tr>
	<tr>
		<th>Report Name</th>
		<td><input type="text" id="report_name" name="report_name" value="" /></td>
        <td></td>
	<tr>
	<tr>
		<th></th>
		<td><input type="submit" id="save_quick_report" name="save_quick_report" value="Save as Quick Report"  /></td>
        <td></td>
        </tr>
</table>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
</form>
</div>
<div id="showResults"></div> -->


