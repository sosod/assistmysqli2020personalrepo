<link rel="stylesheet" href="/assist.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/styles.css" />
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/2/11
 * Time: 12:14 AM
 * To change this template use File | Settings | File Templates.
 */
@session_start();
require_once("../../library/dbconnect/dbconnect.php");
include_once("../class/report.php");
include_once("../class/naming.php");
//print "<pre>";
    //print_r($_REQUEST);
//print "</pre>";
$rep    = new Report();
if(isset($_REQUEST['save_quick_report']))
{
    if($_REQUEST['report_name'] !== ""){
         $rep -> saveQuickReport();
    }
} else {

    $report = array();
    if(isset($_REQUEST['generate_quick_report']))
    {
        $_REQUEST = unserialize($rep->getQuickData($_REQUEST['id']));
        $report = $rep -> generateReport();
    } else {
       $report = $rep -> generateReport();
    }
        $report = $rep -> generateReport();
        //print "<pre>";
          //print_r($report);
        //print "</pre>";

        switch($_REQUEST['display'])
        {
            case "screen":
                $contractHeaders    = $report['contractheaders'];
                $cHeaders           = count($contractHeaders);
                $deliverablesheades = $report['delheaders'];
                $dHeaders           = count($deliverablesheades);
                $actionheaders      = $report['actionheaders'];
                 echo "<table width='100%'>";
                    echo "<tr>";
                    foreach($contractHeaders as $key => $c_head){
                        echo "<th>".$c_head."</th>";
                    }
                    foreach($deliverablesheades as $ky => $d_head){
                            echo "<th>".$d_head."</th>";
                    }
                    foreach($actionheaders as $k => $a_head){
                            echo "<th>".$a_head."</th>";
                    }
                    foreach( $report['data'] as $key => $data)
                    {
                        echo "<tr>";
                            foreach($data as $cKey => $cValue)
                            {
                                if($cKey=="deliverables"){} else{
                                    echo "<td>".$cValue."</td>";
                                }

                            }

                        if(isset($data['deliverables']) && !empty($data['deliverables']))
                        {
                            foreach($data['deliverables'] as $mdKey => $dData)
                            {
                                echo "<tr>";
                                echo "<td colspan='".$cHeaders."'></td>";
                                foreach($dData as $dKey => $dValue){
                                    if($dKey == "sub_deliverable" || $dKey=="actions")
                                    { } else {
                                        if($dKey !== "delstatus"){
                                            echo "<td>".$dValue."</td>";
                                        }
                                    }
                                }
                                echo "</tr>";
                                if(isset($dData['sub_deliverable']) && !empty($dData['sub_deliverable']))
                                {
                                   foreach($dData['sub_deliverable'] as $sbKey => $sbData)
                                   {
                                         echo "<tr>";
                                         echo "<td colspan='".$cHeaders."'></td>";
                                        foreach($sbData as $sKey => $sValue){
                                            if($sKey=="actions")
                                            { } else {
                                                if($sKey !== "delstatus"){
                                                    echo "<td  bgcolor='gray' align='right'>".$sValue."</td>";
                                                }
                                            }
                                        }//end sub deliverable
                                       echo "</tr>";
                                       if(isset($sbData['actions'])  && !empty($sbData['actions']))
                                        {
                                           foreach($sbData['actions'] as $acKey => $sacData)
                                           {
                                                 echo "<tr>";
                                                 echo "<td colspan='".($cHeaders+$dHeaders)."'></td>";
                                                foreach($sacData as $saKey => $saValue){
                                                    echo "<td>".$saValue."</td>";
                                                }//end action
                                               echo "</tr>";
                                          }
                                       }
                                   }
                                }

                                if(isset($dData['actions']) && !empty($dData['actions']))
                                {
                                   foreach($dData['actions'] as $acKey => $acData)
                                   {
                                         echo "<tr>";
                                         echo "<td colspan='".($cHeaders+$dHeaders)."'></td>";
                                        foreach($acData as $aKey => $aValue){
                                            echo "<td>".$aValue."</td>";
                                        }//end action
                                       echo "</tr>";
                                   }
                                }

                            }
                        }
                        echo "</tr>";
                    }
                echo "</table>";
                break;
            case "excell":
                displayExcell( $report );
                break;
            case "excell_formated":
                displayExcellFormatted( $report );
                break;
            case "pdf":
                displayPdf( $report );
                break;
            default:
                displayScreen( $report );
                break;
        }


        function displayScreen( $report )
            {
                $contractHeaders    = $report['contractheaders'];
                $cHeaders           = count($contractHeaders);
                $deliverablesheades = $report['delheaders'];
                $dHeaders           = count($deliverablesheades);
                $actionheaders      = $report['actionheaders'];
                 echo "<table width='100%'>";
                    echo "<tr>";
                    foreach($contractHeaders as $key => $c_head){
                        echo "<th>".$c_head."</th>";
                    }
                    foreach($deliverablesheades as $ky => $d_head){
                            echo "<th>".$d_head."</th>";
                    }
                    foreach($actionheaders as $k => $a_head){
                            echo "<th>".$a_head."</th>";
                    }
                    foreach( $report['data'] as $key => $data)
                    {
                        echo "<tr>";
                            foreach($data as $cKey => $cValue)
                            {
                                if($cKey=="deliverables"){} else{
                                    echo "<td>".$cValue."</td>";
                                }

                            }

                        if(isset($data['deliverables']) && !empty($data['deliverables']))
                        {
                            foreach($data['deliverables'] as $mdKey => $dData)
                            {
                                echo "<tr>";
                                echo "<td colspan='".$cHeaders."'></td>";
                                foreach($dData as $dKey => $dValue){
                                    if($dKey == "sub_deliverable" || $dKey=="actions")
                                    { } else {
                                        if($dKey !== "delstatus"){
                                            echo "<td>".$dValue."</td>";
                                        }
                                    }
                                }
                                echo "</tr>";
                                if(isset($dData['sub_deliverable']) && !empty($dData['sub_deliverable']))
                                {
                                   foreach($dData['sub_deliverable'] as $sbKey => $sbData)
                                   {
                                         echo "<tr>";
                                         echo "<td colspan='".$cHeaders."'></td>";
                                        foreach($sbData as $sKey => $sValue){
                                            if($sKey=="actions")
                                            { } else {
                                                if($sKey !== "delstatus"){
                                                    echo "<td  bgcolor='gray' align='right'>".$sValue."</td>";
                                                }
                                            }
                                        }//end sub deliverable
                                       echo "</tr>";
                                       if(isset($sbData['actions'])  && !empty($sbData['actions']))
                                        {
                                           foreach($sbData['actions'] as $acKey => $sacData)
                                           {
                                                 echo "<tr>";
                                                 echo "<td colspan='".($cHeaders+$dHeaders)."'></td>";
                                                foreach($sacData as $saKey => $saValue){
                                                    echo "<td>".$saValue."</td>";
                                                }//end action
                                               echo "</tr>";
                                          }
                                       }
                                   }
                                }

                                if(isset($dData['actions']) && !empty($dData['actions']))
                                {
                                   foreach($dData['actions'] as $acKey => $acData)
                                   {
                                         echo "<tr>";
                                         echo "<td colspan='".($cHeaders+$dHeaders)."'></td>";
                                        foreach($acData as $aKey => $aValue){
                                            echo "<td>".$aValue."</td>";
                                        }//end action
                                       echo "</tr>";
                                   }
                                }

                            }
                        }
                        echo "</tr>";
                    }
                echo "</table>";
            }
}