<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/2/11
 * Time: 12:09 AM
 * To change this template use File | Settings | File Templates.
 */
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );
	}
}
class GenerateController extends Controller{

	function __construct()
	{

	}

    function getActiveType()
	{
		$obj = $this -> loadModel("ContractType");
		echo json_encode( $obj -> getActiveContractType() );
	}

	function getActiveCategory()
	{
		$obj = $this -> loadModel("ContractCategory");
		echo json_encode( $obj -> getCategories( ) );
	}

	function getSupplier()
	{
		$obj = $this -> loadModel("Supplier");
		echo json_encode( $obj -> getActiveSupplier() );
	}

	function getUsers()
	{
		$obj = $this -> loadModel("UserAccess");
		echo json_encode( $obj -> getUsers() );
	}

	function getTemplates()
	{
		$obj = $this -> loadModel("Contract");
		echo json_encode( $obj -> getTemplates() );
	}

	function getAssessmentFrequency()
	{
		$obj = $this -> loadModel("AssessmentFrequency");
		echo json_encode( $obj -> getActiveAssessmentFrequencies() );
	}

	function getNotificationFrequency()
	{
		$obj = $this -> loadModel("Notification");
		echo json_encode( $obj -> getNotifications() );
	}

	function getDeliverableCatergories()
	{
		$obj = $this -> loadModel("DeliverableCategory");
		echo json_encode( $obj -> getCategories() );
	}
}
$method 	=  $_GET['action'];
$controller = new GenerateController();
if(method_exists($controller, $method)){
	$controller -> $method();
}
