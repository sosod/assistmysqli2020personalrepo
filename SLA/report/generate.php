<?php
	$scripts = array( 'generate.js','menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../header.php");
?>
<script>
$(function(){
	$("table#query_field_tobeincluded").find("th").css({"text-align":"left", "vertical-align":"top"});
	$("table#query_field_tobeincluded td").css({"border-color":"#FFFFFF"});
	$("table#query_field_tobeincluded td").css({"border-width":"0px"});	
	$("table#query_field_tobeincluded td").css({"border-style":"none"});
	$("table#query_field_tobeincluded td").css({"vertical-align":"top"});
});
</script>
<?php
$naming = new Naming();
$headernames = $naming ->getHeader();
$qrep     = new Report();
$qreports = $qrep -> getQuickReports();
?>
<table class="noborder">
    <tr>
        <td class="noborder">Quick report</td>
        <td class="noborder">
            <select name="quick_report" id="quick_report">
            <option value="">--select--</option>
            <?php
            foreach($qreports as $k => $value){
            ?>
             <option value='<?php echo $value['id']?>'><?php echo $value['name']?></option>
            <?php
            }
            ?>
            </select>
        </td>
    </tr>
</table>
<form action="process_report.php"name="report-header-form" id="report-header-form" method="post">
<table id="query_field_tobeincluded" class="noborder">
	<tr>
		<td colspan="3" align="left"><h3>1. Select the Contract Information to be displayed on the report</h3></th>
	</tr>
	<tr>
	<td colspan="3">
	  <table class="noborder">
				<tr>
					<td><input type="checkbox" name="header[ref]" value="1" id="ref" class="contractf" /><?php echo $headernames['ref']; ?></td>
					<td><input type="checkbox" name="header[contract]" value="1" id="contract" class="contractf" /><?php echo $headernames['contract']; ?></td>
					<td><input type="checkbox" name="header[contract_type]" value="1" id="contract_type" class="contractf" /><?php echo $headernames['contract_type']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="header[contract_category]" value="1" id="contract_category" class="contractf" /><?php echo $headernames['contract_type']; ?></td>
					<td><input type="checkbox" name="header[project_comments]" value="1" id="project_comments" class="contractf" /><?php echo $headernames['project_comments']; ?></td>
					<td><input type="checkbox" name="header[contract_completion_date]" value="1" id="contract_completion_date" class="contractf" ><?php echo $headernames['contract_completion_date']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="header[contract_supplier]" value="1" id="contract_supplier" class="contractf" /><?php echo $headernames['contract_supplier']; ?></td>
					<td><input type="checkbox" name="header[contract_owner]" id="contract_owner" value="1" class="contractf" /><?php echo $headernames['contract_owner']; ?></td>
					<td><input type="checkbox" name="header[contract_status]" id="contract_status" value="1" class="contractf" /><?php echo $headernames['contract_status']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="header[project_value]" value="1" id="project_value" class="contractf" /><?php echo $headernames['project_value']; ?></td>
					<td><input type="checkbox" name="header[signature_of_tender]" value="1" id="signature_of_tender" class="contractf" /><?php echo $headernames['signature_of_tender']; ?></td>
					<td><input type="checkbox" name="header[signature_of_sla]" value="1" id="signature_of_sla" class="contractf" /><?php echo $headernames['signature_of_sla']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="header[contract_assessment_frequency]" value="1" id="contract_assessment_frequency" class="contractf" /><?php echo $headernames['contract_assessment_frequency']; ?></td>
					<td><!--<input type="checkbox" name="header[finding]" value="1" id="finding" class="queryf" /><?php //echo $headernames['contract_type']; ?> --></td>
					<td><!--<input type="checkbox" name="header[recommendation]" value="1" id="recommendation" class="queryf" /><?php //echo $headernames['contract_type']; ?> --></td>
				</tr>
				<!--<tr>
					<td><input type="checkbox" name="header[query_status]" value="1" id="query_status" class="queryf" /><?php //echo $headernames['contract_type']; ?></td>
					<td><input type="checkbox" name="header[query_status]" value="1" id="query_status" class="queryf" /><?php // echo $headernames['contract_type']; ?></td>
					<td><<input type="checkbox" name="header[query_status]" value="1" id="query_status" class="queryf" /><?php //echo $headernames['contract_type']; ?></td>
				</tr> -->
				<tr>
				<td colspan="3">
					<input type="button" value="Check All" id="r_checkAll" name="r_checkAll" />
					<input type="button" value="UnCheck All" id="r_uncheckAll" name="r_uncheckAll" />
					<input type="button" value="Invert" id="r_invert" name="r_invert" />
				</td>
				</tr>			
			</table>
		</td>
	</tr>
    <td colspan="3" align="left"><h3>2. Select the Deliverable Information to be displayed on the report</h3></td>
		<tr>
		   <td colspan="3">		
			 <table class="noborder">
				<tr>
					<td><input type="checkbox" name="dheader[ref]" value="1" id="dref" class="deliverablef"><?php echo $headernames['ref']; ?></td>
					<td><input type="checkbox" name="dheader[deliverable]" value="1" id="deliverable" class="deliverablef"><?php echo $headernames['deliverable']; ?></td>
					<td><input type="checkbox" name="dheader[deliverable_description]" value="1" id="deliverable_description" class="deliverablef" ><?php echo $headernames['deliverable_description']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="dheader[main_deliverable]" value="1" id="main_deliverable" class="deliverablef"><?php echo $headernames['main_deliverable']; ?></td>
					<td><input type="checkbox" name="dheader[deliverable_category]" value="1" id="deliverable_category" class="deliverablef" /><?php echo $headernames['deliverable_category']; ?></td>
					<td><input type="checkbox" name="dheader[deliverable_owner]" id="deliverable_owner" value="1" class="deliverablef" /><?php echo $headernames['deliverable_owner']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="dheader[delivered_weight]" value="1" id="delivered_weight" class="deliverablef" /><?php echo $headernames['delivered_weight']; ?></td>
					<td><input type="checkbox" name="dheader[quality_weight]" value="1" id="quality_weight" class="deliverablef" /><?php echo $headernames['quality_weight']; ?></td>
					<td><input type="checkbox" name="dheader[other_weight]" value="1" id="other_weight" class="deliverablef"/><?php echo $headernames['other_weight']; ?></td>
				</tr>
				<tr>
				<tr>
					<td><input type="checkbox" name="dheader[deadline]" value="1" id="deadline" class="deliverablef" /><?php echo $headernames['deadline']; ?></td>
					<td><!--<input type="checkbox" name="dheader[quality_weight]" value="1" id="quality_weight" class="deliverablef" /><?php echo $headernames['quality_weight']; ?> --></td>
					<td><!-- <input type="checkbox" name="dheader[other_weight]" value="1" id="other_weight" class="deliverablef"/><?php echo $headernames['other_weight']; ?> --></td>
				</tr>
				<tr>
				<td colspan="3">
					<input type="button" value="Check All" id="d_checkAll" name="d_checkAll" />
					<input type="button" value="UnCheck All" id="d_uncheckAll" name="d_uncheckAll" />
					<input type="button" value="Invert" id="d_invert" name="d_invert" />
				</td>
				</tr>			
		  </table>
		</td>		  
		</tr>
    <td colspan="3" align="left"><h3>3. Select the Action Information to be displayed on the report</h3></td>
	<tr>
	 <td colspan='3'>
	  <table class="noborder">		    
				<tr>
					<td><input type="checkbox" name="aheader[ref]" value="1" id="aref" class="actionf"><?php echo $headernames['ref']; ?></td>
					<td><input type="checkbox" name="aheader[action]" value="1" id="action" class="actionf"><?php echo $headernames['action']; ?></td>
					<td><input type="checkbox" name="aheader[measurable]" value="1" id="measurable" class="actionf" ><?php echo $headernames['measurable']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="aheader[action_owner]" value="1" id="action_owner" class="actionf"><?php echo $headernames['action_owner']; ?></td>
					<td><input type="checkbox" name="aheader[deadline]" value="1" id="deadline" class="actionf" /><?php echo $headernames['deadline']; ?></td>
					<td><input type="checkbox" name="aheader[action_status]" id="action_status" value="1" class="actionf" /><?php echo $headernames['action_status']; ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="aheader[progress]" value="1" id="progress" class="actionf" /><?php echo $headernames['progress']; ?></td>
					<td><!-- <input type="checkbox" name="aheader[action_reminder]" value="1" id="action_reminder" class="actionf" /><?php echo $headernames['contract_type']; ?> --></td>
					<td><!-- <input type="checkbox" name="aheader[timescale]" value="1" id="timescale" class="actionf"/><?php echo $headernames['contract_type']; ?> --></td>
				</tr>
				<tr>
				<td colspan="3">
					<input type="button" value="Check All" id="a_checkAll" name="a_checkAll" />
					<input type="button" value="UnCheck All" id="a_uncheckAll" name="a_uncheckAll" />
					<input type="button" value="Invert" id="a_invert" name="a_invert" />
				</td>
				</tr>
		 </table>
	   </td>
	</tr>
<!-- </table>

	</form>
	<form id="report-values-form" name="report-values-form" method="post">

<table border="1" id="report_values_table" >-->
	<tr>
		<td colspan="3"><h3>4. Select the filters you wish to apply</h3></th>
	</tr>
    <tr>
    	<th><?php echo $headernames['contract']; ?> :</th>
        <td><input type="text" name="values[name]" id="name" placeholder="enter project name" value="" /></td>
        <td>
            <select name="match[name]" id="match_name">
                <option value="all">All Words</option>
                <option value="any">Any Words</option>
                <option value="exact">exact Phrase</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th><?php echo $headernames['project_value']; ?> :</th>
        <td><input type="text" name="values[budget]" id="budget" placeholder="enter project budget" value="" /></td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['project_comments']; ?> :</th>
        <td><textarea name="values[comments]" id="comments" placeholder="enter project comments"></textarea></td>
        <td>
            <select name="match[comments]" id="match_comments">
                <option value="all">All Words</option>
                <option value="any">Any Words</option>
                <option value="exact">exact Phrase</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th><?php echo $headernames['contract_type']; ?> :</th>
        <td>
        	<select name="values[type]" id="type" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['contract_category']; ?> :</th>
        <td>
        	<select name="values[category]" id="category" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['contract_supplier']; ?> :</th>
        <td>
        	<select name="values[supplier]" id="supplier" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['signature_of_tender']; ?> :</th>
        <td>
         <input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signnature_of_tender" name="values[signnature_of_tender]" class="datepicker" />
        </td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['signature_of_sla']; ?> :</th>
        <td>
         <input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signature_of_sla_" name="values[signature_of_sla]" class="datepicker" />
        </td>
        <td></td>
    </tr>
    <tr>
    	<th>Contract Manager :</th>
        <td>
        	<select name="values[contract_manager]" id="contract_manager" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th><?php echo $headernames['contract_owner']; ?> :</th>
        <td>
        	<select name="values[contract_owner]" id="contract_owner_" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th>Contract Authorisor 2 :</th>
        <td>
        <select name="values[contract_authorisor]" id="contract_authorisor" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
        </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th>Template used :</th>
        <td>
        	<select name="values[template]" id="template_" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr>
    	<th>Assessment Frequency : <br />
            <!--<em>
            If weekly , select day of week and time to be send<br />
            If monthly , select day of month and time to be send<br />
            If Quartely , select day and time to be send etc.
            </em> -->
        </th>
        <td>
        	<select name="values[assessment_frequency]" id="assessment_frequency" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    <tr id="weekly" style="display:none;" class="assfreq">
    	<th>Start Date :</th>
        <td><input type="text" name="weeklydate" id="weeklydate"  class="datepicker"/></td>
    </tr>
    <tr id="monthly" style="display:none;" class="assfreq">
    	<th>Start Date: </th>
        <td><input placeholder="<?php echo date("d-F-Y"); ?>" type="text" name="monthlydate" id="monthlydate" class="datepicker" /></td>
    </tr>
    <tr id="quarterly" style="display:none;" class="assfreq">
    	<th>Start Date :</th>
        <td><input type="text" placeholder="<?php echo date("d-F-Y"); ?>" name="quarterlydate" id="quarterlydate" class="datepicker" /></td>
    </tr>
    <tr>
    	<th>
        	Notification Frequency:
        </th>
        <td>
        	<select name="values[notification_frequency]" id="notification_frequency" multiple="multiple">
            	<option value="" selected="selected">ALL</option>
            </select>
        </td>
        <td></td>
    </tr>
    </tr>
    <th>Contract Creation Date:</th>
		<td width="300px;">
			From : <input type="text" placeholder="<?php echo date("d-F-Y"); ?>"  name="values[from_date]" id="_from_query_date" value="" class="datepicker"  />
		</td>
		<td>
         to  <input type="text" placeholder="<?php echo date("d-F-Y"); ?>" name="values[to_date]" id="_to_query_date" value="" class="datepicker" />
		</td>
	</tr>
<!-- </table>
</form>
<form id="grouping-options-form" name="grouping-options-form" method="post">
<table id="grouping_table" border="1" width="65%"> -->
	<tr>
		<td colspan="3"><h3>5. Choose your group and sort options</h3></th>
	</tr>
	<tr>
		<th width="65">Group By :</th>
		<td width="371" colspan="2">
			<select name="group_by" id="group_by">
				<option value="">No grouping</option>
				<option value="type_">Contract Type</option>
				<option value="category_">Contract Category</option>
				<option value="supplier_">Contract Supplier</option>
				<option value="contract_owner_">Contract Owner</option>
                <option value="contract_owner_">Contract Manager</option>
				<option value="assessment_frequency_">Assessment Frequency</option>
                <option value="notification_frequency_">Notification Frequency</option>
			</select>
	  </td>
  </tr>
	<tr>
		<th>Sort By :</td>
		<td colspan="2">
			<ul id="sortable" style="list-style:none;">
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__id"></span><input type=hidden name=sort[] value="__id">Ref</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__type"></span><input type=hidden name=sort[] value="__type">Contract Type</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__category"></span><input type=hidden name=sort[] value="__category">Category</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__comments"></span><input type=hidden name=sort[] value="__comments">Comments</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__supplier"></span><input type=hidden name=sort[] value="__supplier">Supplier</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__signnature_of_tender"></span><input type=hidden name=sort[] value="__signnature_of_tender">Signnature Of Tender</li>
                <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__signature_of_sla"></span><input type=hidden name=sort[] value="__signature_of_sla">Signature Of Sla</li>
			</ul>
		</td>
	</tr>
<!-- </table>
</form>
<form id="document-format-form" name="document-format-form" method="post">
<table id="document_format_table" border="1" width="65%"> -->
	<tr>
		<td colspan="3"><h3>6. Choose the document format of your report</h3></th>
	</tr>
	<tr>
		<td colspan="3"><input type="radio" id="onscreen" name="display" value="screen" checked="checked" />OnScreen Display</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="excell" name="display" value="excell" />Microsoft Excel( Plain Text)</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="excell_formated" name="display" value="excell_formated" />Microsft Excel(Formatted)</td>
	<tr>
	<tr>
		<td colspan="3"><input type="radio" id="pdf" name="display" value="pdf" />Save to Pdf</td>
	</tr>
<!-- </table>
</form>
<form id="generate-report-form" name="generate-report-form"  method="post">
<table id="generate_report_table" border="1" width="65%"> -->
	<tr>
		<td colspan="3"><h3>7. Generate the report</h3></td>
	</tr>
	<tr>
		<th>Report Title : </th>
		<td><input type="text" id="report_title" name="report_title" value="" /></td>
        <td></td>
	<tr>
	<tr>
		<th></th>
		<td>
		<input type="submit" name="generate_report" id="generate_report" value="Generate Report"  />
		<input type="reset" id="reset" name="reset" value="Reset" />
		</td>
        <td></td>
	<tr>
	<tr>
		<th>Report Name :</th>
		<td><input type="text" id="report_name" name="report_name" value="" /></td>
        <td></td>
	<tr>
	<tr>
		<th></th>
		<td><input type="submit" id="save_quick_report" name="save_quick_report" value="Save as Quick Report"  /></td>
        <td></td>
        </tr>
</table>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
</form>



