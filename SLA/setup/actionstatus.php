<?php
$scripts = array("actionstatus.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<span id="loading"></span>
<form method="post" id="form-actionstatus">
<table id="table_actionstatus">
	<tr>
    	<th>Ref</th>
    	<th>Action Status</th>
    	<th>&lt;Client&gt; Teminology</th>                
    	<th>Colour</th>
    	<th></th>                        
    	<th>Status</th>                
    	<th></th>                        
    </tr>
	<tr>
    	<td>#</td>
    	<td><input type="text" name="name" id="name" value="" /></td>
    	<td><input type="text" name="client_terminology" id="client_terminology" value="" /></td>                
    	<td><input type="text" name="color" id="color" class="color" value="e82929" /></td>
    	<td><input type="submit" name="save" id="save" value="Add"  /></td>                        
    	<td></td>                
    	<td></td>                        
    </tr>    
</table>
</form>
</div>
<ul class="list_link">
	<li><?php displayGoBack("", ""); ?></li>
	<li><a href="#" id="view_action_status_logs" class="logs">View Audit Log</a></li>
	</ul>
<div id="log_div"></div>