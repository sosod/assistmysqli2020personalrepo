<?php
$scripts = array("contracttype.js", "contractcatergory.js");
require_once("../header.php");
$cat 	  = new ContractCategory();
$category = $cat -> getCategory( $_GET['id']);

$type     = new ContractType();
$types 	  = $type -> getContractTypes(); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-contractcategory">
<table id="table_contractcategory">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>     
    <tr>
        <th>Category Status:</th>            
    	<td>
        <select id="status" name="status">
        	<option value="1" <?php if(($category['status'] & 1) == 1) {?> selected <?php } ?>>Active</option>    
        	<option value="0" <?php if(($category['status'] & 1) == 0) {?> selected <?php } ?>>Inactive</option>                    
        </select>
       </td>   
    </tr>     	
	<tr>     
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>              
    	<td class="noborder">
        <input type="submit" name="updatecatstatus" id="updatecatstatus" value="Update Status"  />
        <input type="hidden" name="catid" id="catid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>    
</table>
</form>
</div>