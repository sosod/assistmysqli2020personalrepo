<?php
$scripts = array("otherscore.js");
require_once("../header.php");
$q 		= new OtherScore();
$osocre	= $q -> getOtherScore( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="otherscore" name="otherscore">
<table id="table_otherscore">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
     </tr>
     <tr>
		<th>Other Score Status:</th>	               
    	<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($osocre['status'] & 1)==1) {?> selected<?php } ?>>Active</option>
            	<option value="0" <?php if(($osocre['status'] & 1)==0) {?> selected<?php } ?>>InActive</option>                
            </select>
        </td>    
    </tr>
    <tr>
    	<th></th>
    	<td>
		    	<input type="submit" name="updatestatus" id="updatestatus" value="Update" />
		        <input type="hidden" id="otherscoreid" name="otherscoreid" value="<?php echo $_GET['id']; ?>" />        
        </td>                             
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>