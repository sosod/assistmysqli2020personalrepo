<?php
$scripts = array("otherweight.js", "jscolor.js");
require_once("../header.php");
$o 		 = new OtherWeight();
$oweight = $o -> getOtherWeight( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="otherweight" name="otherweight">
<table id="table_otherweight">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
		<th>Other Weight:</th>	    
		<td><input type="text" name="oweight" id="oweight" value="<?php echo $oweight['oweight']; ?>" /> </td>    
    </tr>
    <tr>
		<th>Other Weight Description:</th>
    	<td><input type="text" name="description" id="description" value="<?php echo $oweight['description']; ?>" /></td>                
    </tr>
    <tr>        
		<th>Other Weight Definition:</th>
		<td><textarea name="definition" id="definition"><?php echo $oweight['definition']; ?></textarea></td>                      
    </tr>
    <tr>
		<th>Color assigned to other weight:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $oweight['color']; ?>" /></td> 
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="update" id="update"  value="Update" />
        	<input type="hidden" id="otherweightid" name="otherweightid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>