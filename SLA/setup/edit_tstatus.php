<?php
$scripts = array("taskstatus.js", "jscolor.js");
require_once("../header.php");
$tstatus = new TaskStatus();
$status  = $tstatus -> getTaskStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-taskstatus">
<table id="table_taskstatus">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>           
    	<th>Task Status:</th>
		<td><input type="text" name="name" id="name" value="<?php echo $status['name']; ?>"  <?php echo (($status['status'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?> /></td>        
   </tr>                      
   <tr> 
    	<th>Colour:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $status['color']; ?>" /></td>        
   </tr>
	<tr>    	
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>                
    	<td class="noborder">
        	<input type="submit" name="update" id="update" value="Update"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />            
        </td>                        
    </tr>    
</table>
</form>
</div>