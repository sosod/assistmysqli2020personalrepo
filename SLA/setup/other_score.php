<?php
$scripts = array("otherscore.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="otherscore" name="otherscore">
			<table id="table_otherscore">
				<tr>
					<th>Ref</th>	
					<th>Other Score</th>	    
					<th>Other Score Description</th>
					<th>Other Score definition</th>
					<th>Color assigned to other score</th>
					<th></th>                        	           
					<th></th>             
					<th></th>                  
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="oscore" id="oscore" value="" /></td> 
			    	<td><input type="text" name="description" id="description" value="" /></td>                
			    	<td><textarea name="definition" id="definition"></textarea></td>              
			    	<td><input type="text" name="color" id="color" class="color" value="EEEFFF" /></td>    
			    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("other_score_logs", true)?></td>
	</tr>
</table>
</div>
