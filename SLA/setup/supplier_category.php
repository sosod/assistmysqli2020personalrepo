<?php
$scripts = array("suppliercategory.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="form-suppliercategory">
			<table id="table_suppliercategory">
				<tr>
			    	<th>Ref</th>
			    	<th>Short Code</th>
			    	<th>Supplier Category Code</th>        
			    	<th>Suplier Code name</th>    
			        <th>Notes</th>            
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td>
			    	<td><input type="text" name="name" id="name" value="" /></td>                
			    	<td><textarea id="description" name="description"></textarea></td>
			    	<td><textarea id="notes" name="notes"></textarea></td>        
			    	<td><input type="submit" name="save" id="save" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("supplier_category_logs", true)?></td>
	</tr>
</table>
</div>
