<?php
$scripts = array("taskstatus.js");
require_once("../header.php");
$tstatus = new TaskStatus();
$status  = $tstatus ->  getTaskStatus( $_GET['id'] );
?>
<div class="displayview">
<span id="loading"></span>
<form method="post" id="form-taskstatus">
<table id="table_taskstatus">
	<tr>
    	<th>Ref</th>
    	<td>#<?php echo $_GET['id']; ?></td>
     </tr>
     <tr>        
    	<th>Action Status</th>
		<td>
        	<select name="status" id="status">
            	<option value="1" <?php if($status['active'] == 1 ){?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if($status['active'] == 0 ){?> selected="selected" <?php } ?>>Inactive</option>                
            </select>
        </td>
	</tr>        
   		<td><a href="javascript:history.back()">Go Back</a></td>                        
    	<td>
        	<input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
</table>
</form>
</div>