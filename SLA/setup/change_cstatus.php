<?php
$scripts = array("jscolor.js","contractstatus.js");
require_once("../header.php");
$cstatus   = new ContractStatus();
$cstatuses = $cstatus -> getCStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table id="table_contractstatus">
	<tr>
    	<th>Ref  #:</th>
    	<td><?php echo $_GET['id']; ?></td>
     </tr>
     <tr>        
    	<th>Contract Status:</th>
		<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($cstatuses['status'] & 1) == 1 ){?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if(($cstatuses['status'] & 1) == 0 ){?> selected="selected" <?php } ?>>Inactive</option>                
            </select>
        </td>
	</tr> 
	<tr>       
   		<td class="noborder"><?php displayGoBack("", ""); ?></td>                        
    	<td class="noborder">
        	<input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
</table>
</form>
</div>