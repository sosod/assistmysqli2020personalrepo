<?php
$scripts = array("otherweight.js", "jscolor.js");
require_once("../header.php");
$o 		 = new OtherWeight();
$oweight = $o -> getOtherWeight( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="otherweight" name="otherweight">
<table id="table_otherweight">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
		<th>Other Weight Status:</th>	    
		<td>
        	<select id="status" name="status">
            	<option value="1" <?php if($oweight['active']==1) {?> selected<?php } ?>>Active</option>
            	<option value="0" <?php if($oweight['active']==0) {?> selected<?php } ?>>InActive</option>   
            </select>
        </td>    
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="updatestatus" id="updatestatus"  value="Update" />
        	<input type="hidden" id="otherweightid" name="otherweightid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>