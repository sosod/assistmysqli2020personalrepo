<?php
$scripts = array("actionstatus.js", "jscolor.js");
require_once("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form method="post" id="form-actionstatus">
			<table id="table_actionstatus">
				<tr>
			    	<th>Ref</th>
			    	<th>Action Status</th>
			    	<th>&lt;Client&gt; Teminology</th>                
			    	<th>Colour</th>
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>
			    	<td><input type="text" name="client_terminology" id="client_terminology" value="" /></td>                
			    	<td><input type="text" name="color" id="color" class="color" value="e82929" /></td>
			    	<td><input type="submit" name="save" id="save" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("action_status_logs", true)?></td>
	</tr>
</table>
