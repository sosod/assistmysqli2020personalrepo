<?php
$scripts = array("assessmentfreq.js", "jquery-ui-timepicker-addon.js");
require_once("../header.php");
$assess	  = new AssessmentFrequency();
$assfreq  = $assess -> getAssessmentFrequency( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-assessmentfreq">
<table id="table_assessmentfreq">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr> 
    	<th>Short Code:</th>
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $assfreq['shortcode']; ?>" <?php echo (($assfreq['active'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?> /></td>        
    </tr>
    <tr>
    	<th>Assessment Type:</th>    
    	<td><input type="text" name="name" id="name" value="<?php echo $assfreq['name']; ?>" /></td>              
    </tr>
    <tr>
    	<th>Desciption of Assessment Type:</th>    
    	<td><textarea id="description" name="description"><?php echo $assfreq['description']; ?></textarea></td>
    </tr>        
    <tr>
        <th>Date Time:</th>            
    	<td>
       <input type="text" name="date" id="date" value="<?php echo $assfreq['date']; ?>" />
       </td>   
    </tr>     	
	<tr>     
		<th></th>             
    	<td>
	       <input type="submit" name="updateassessfreq" id="updateassessfreq" value="Update"  />
	       <input type="hidden" name="assessid" id="assessid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>   
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td> 
    </tr> 
</table>
</form>
</div>