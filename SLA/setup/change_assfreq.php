<?php
$scripts = array("assessmentfreq.js", "jquery-ui-timepicker-addon.js");
require_once("../header.php");
$assess	  = new AssessmentFrequency();
$assfreq  = $assess -> getAssessmentFrequency( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-assessmentfreq">
<table id="table_assessmentfreq">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>     
    <tr>
        <th>Assessment Frequency Status:</th>            
    	<td>
        <select id="status" name="status">
        	<option value="1" <?php if($assfreq['active'] == 1) {?> selected <?php } ?>>Active</option>    
        	<option value="0" <?php if($assfreq['active'] == 0) {?> selected <?php } ?>>Inactive</option>                    
        </select>
       </td>   
    </tr>     	
	<tr> 
    	<th></th>                
    	<td>
	        <input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
	        <input type="hidden" name="assessid" id="assessid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>    
    <tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>  
    </tr>
</table>
</form>
</div>