<?php
$scripts = array("supplier.js", "suppliercategory.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="form-supplier">
			<table id="table_supplier">
				<tr>
			    	<th>Ref</th>
			    	<th>Short Code</th>        
			    	<th>Supplier Code</th>    
			        <th>Name of Supplier</th>            
			    	<th>Supplier Category</th>        
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td>
			   		<td><input type="text" name="supplier_code" id="supplier_code" value="" /></td>        
			    	<td><input type="text" name="name" id="name" value="" /></td>                
			    	<td><select id="category" name="category"><option value="">--supplier category--</option></select></td>        
			    	<td><input type="submit" name="savesupplier" id="savesupplier" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("supplier_logs", true)?></td>
	</tr>
</table>
</div>
