<?php
$scripts = array("deliverablecategory.js");
require_once("../header.php");
$cat 	  = new DeliverableCategory();
$category = $cat -> getCategory( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-deliverablecategory">
<table id="table_deliverablecategory">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr> 
    	<th>Short Code:</th>
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $category['shortcode']; ?>" <?php echo (($category['status'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?>/></td>        
    </tr>
    <tr>
    	<th>Deliverable Category:</th>    
    	<td><input type="text" name="name" id="name" value="<?php echo $category['name']; ?>" /></td>              
    </tr>
    <tr>
    	<th>Desciption of Deliverable Category:</th>    
    	<td><textarea id="description" name="description"><?php echo $category['description']; ?></textarea></td>
    </tr>             	
	<tr>  
    	<th></th>                
    	<td>
	        <input type="submit" name="updatecat" id="updatecat" value="Update"  />
	        <input type="hidden" name="catid" id="catid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>    
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td> 
    	<td class="noborder"></td>
    </tr>
</table>
</form>
</div>