<?php
$scripts = array( 'columns.js' );
$styles = array();
$page_title = "ListColums";
require_once("../header.php");
?>
<style>
	#activeColumns, #inactiveColumns
	{
		list-style-type:none;
		margin:0;
		padding:0;
		float:left;
		margin-right:10px;
	}
	#activeColumns li, #inactiveColumns	li
	{
		margin:0 5px 5px 5px;
		padding:5px;
		font-size:1.2em;
		width:120px;
	}
	#columnsContent
	{
		border:1px solid #000000;
		width:500px;
		clear:both;
		position:static;
	}
</style>
<?php JSdisplayResultObj(""); ?>
<div>
<table>
	<tr>
    	<th>Active Colums</th>
        <th>InActive Colums</th>
    </tr>
    <tr>
    	<td valign="top">
        <ul style="float:left; clear:left; width:250px;" id="activeColumns" class="connectedSortable">       
    	</ul>
    </td>
        <td valign="top">
        <ul style="float:left; clear:right; width:250px;" id="inactiveColumns" class="connectedSortable"> 
         <li class="ui-state-highlight" id="column_default">Default Column<br /><em><small>(Do not move)</small></em></li>            
        </ul>        
        </td>    
    </tr>
</table>
</div>
