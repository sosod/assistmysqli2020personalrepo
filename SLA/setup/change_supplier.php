<?php
$scripts = array("supplier.js");
require_once("../header.php");
$supp     = new Supplier();
$supplier = $supp -> getSupply( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-supplier">
<table id="table_supplier">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>     
    <tr>
        <th>Supplier Status:</th>            
    	<td>
        <select id="status" name="status">
        	<option value="1" <?php if(($supplier['status'] & 1) == 1) {?> selected <?php } ?>>Active</option>    
        	<option value="0" <?php if(($supplier['status'] & 1) == 0) {?> selected <?php } ?>>Inactive</option>                    
        </select>
       </td>   
    </tr>     	
	<tr>    
    	<th></th>               
    	<td>
		        <input type="submit" name="updatesupplierstatus" id="updatesupplierstatus" value="Update Status"  />
		        <input type="hidden" name="supplierid" id="supplierid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr> 
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td>
    </tr>   
</table>
</form>
</div>