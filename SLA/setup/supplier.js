// JavaScript Document
$(function(){
	//alert()		   
	var supplierid = $("#supplierid").val();    
	if(supplierid == undefined){
		Supplier.get();		 
	}
	
	$("#savesupplier").click(function(){
		alert("Coming here");
		//Supplier.save();
		return false;						  
	});
	
	$("#updatecat").click(function(){
		Supplier.update();	
		return false;
	});
	
	$("#updatestatus").click(function(){
		Supplier.changeStatus( supplierid, $("#status").val() );							
		return false;
	})
	
});

var Supplier = {
	
	name 		  : "",
	shortcode     : "",
	description   : "",
	category	  : "",
	
	get 		   : function()
	{
		$.getJSON("controller.php?action=getSuppliers", function( data ){
			if( $.isEmptyObject( data ) ){
				
			} else{
				$.each( data, function(index, status){
					Supplier.display( status );	
					
			   });	
			}														  
		});			
	} , 

	update		    : function()
	{
		var that 		   = this;
		that.name 		   = $("#name").val();
		that.supplier_code = $("#supplier_code").val();
		that.description   = $("#description").val();
		that.shortcode     = $("#shortcode").val();
		that.type		   = $("#type :selected").val();		
		var valid   	   = Supplier.isValid(that);
		var data    	   = {name:that.name, description:that.description, shortcode:that.shortcode, id:$("#supplierid").val()};		
		if( valid == true ){
			$.post("controller.php?action=updateSupply", data, function(response){
				if(!response.error){
					$("#loading").html(response.text);	
				} else{
					$("#loading").html(response.text)
				}														 
			}, "json")	
		} else {
			$("#loading").html(response.text)
		}		
	} , 
	
	save 		    : function()
	{
		var that 		 = this;
		that.name 		 = $("#name").val();
		that.description = $("#description").val();
		that.shortcode   = $("#shortcode").val();
		that.category    = $("#category :selected").val();
		var valid   	 = Supplier.isValid(that);
		var data    	 = {name:that.name, description:that.description, shortcode:that.shortcode, category:that.category};
		
		if(valid == true){
			$.post("controller.php?action=saveSupply", data, function(response){				
				if( !response.error ) {
					$("#loading").html(response.text)
					Supplier.emptyFields( data );	
					data.type = $("#type :selected").text();	
					data.id 	= response.id;
					data.active = 1;					
					Supplier.display( data );					
				
				} else{
					$("#loading").html(response.text)					
				}																
			}, "json");	
		} else{
			$("#loading").html(valid);
		}		
	} , 
	
	changeStatus    : function(id, tostatus)
	{
		$.post("controller.php?action=changeSupplyStatus", {id:id, tostatus:tostatus }, function(response){
			if(!response.error){
				$("#loading").html(response.text);	
			} else{
				$("#loading").html(response.text);	
			}																																					
		}, "json");			
	}, 
	
	isValid 		: function(that)
	{
		if( that.name == "" ){
			return "Category name is required";		
		} else if( that.shortcode == ""){
			return "Please enter the category short code";
		} else if( that.description == ""){
			return "Please enter the category description";
		} else if( that.type == ""){
			return "Please enter the contract type ";
		}
		return true;		
	} , 
	
	emptyFields 	: function( fields )
	{
		$.each(fields, function( index, field){
			$("#"+field).val("");					
		});			
	},
	
	display			: function( category )
	{
		$("#table_supplier")
		  .append($("<tr />")
			 .append($("<td />",{html:category.id}))
			 .append($("<td />",{html:category.shortcode}))			 
			 .append($("<td />",{html:category.name}))
			 .append($("<td />",{html:category.description}))
			 .append($("<td />",{html:category.type}))			 
			 .append($("<td />")
				.append($("<input />",{value:"Edit", name:"edit_"+category.id, id:"edit_"+category.id, type:"submit"}))
				.append($("<input />",{disabled:(category.defaults == 1 ? "disabled" : ""),value:"Delete", name:"delete_"+category.id, id:"delete_"+category.id, type:"submit"}))				
			  )
			 .append($("<td />",{html:(category.active == 1 ? "Active" : "Inactive")}))
			 .append($("<td />")
				.append($("<input />",{type:"submit", disabled:(category.defaults == 1 ? "disabled" : ""), id:"change_"+category.id, name:"change_"+category.id, value:"Change Status"}))	   
			  )			 
		   )
		  
		  $("#edit_"+category.id).live("click", function(){
			document.location.href = "edit_category.php?id="+category.id;
			return false;
		  });
		  
		  $("#delete_"+category.id).live("click", function(){
			if(confirm("Are you sure you want to delete this status")){
				Supplier.changeStatus( category.id, 0);
			}
			return false;										  
		  });
		  
		  $("#change_"+category.id).live("click", function(){
			document.location.href = "change_categorystatus.php?id="+category.id;
			return false;
		  });				
	}


}