<?php
$scripts = array("deliveredwieght.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="form-deliveredwieght" name="form-deliveredwieght">
			<table id="table_deliveredwieght">
				<tr>
					<th>Id</th>	
					<th>Delivered Weight</th>	    
					<th>Delivered Weight Description</th>
					<th>Delivered Weight definition</th>
					<th>Color assigned to delivered</th>
					<th></th>                        	           
					<th></th> 
					<th></th>                 
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="dweight" id="dweight" size="5" value="" /></td> 
			    	<td><input type="text" name="description" id="description" value="" /></td>                
			    	<td><textarea name="definition" id="definition"></textarea></td>              
			    	<td><input type="text" name="color" id="color" class="color" value="" /></td>    
			    	<td><input type="submit" name="add" id="add"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("delivered_weight_logs", true)?></td>		
	</tr>
</table>
</div>
