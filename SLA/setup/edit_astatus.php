<?php
$scripts = array("actionstatus.js", "jscolor.js");
require_once("../header.php");
$astatus = new ActionStatus();
$status  = $astatus ->  getActionStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table id="table_contractstatus">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>           
    	<th>Name:</th>
		<td><input type="text" name="name" id="name" value="<?php echo $status['name'].""; ?>" <?php echo (($status['status'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?> /></td>        
   </tr>
   <tr>
    	<th>&lt;Client&gt; Teminology:</th>                
    	<td><input type="text" name="client_terminology" id="client_terminology" value="<?php echo $status['client_terminology']; ?>" /></td>                        
   </tr>
   <tr> 
    	<th>Colour:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $status['color']; ?>" /></td>        
   </tr>
	<tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder">
        	<input type="submit" name="update" id="update" value="Update"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />            
        </td>                                       
    </tr>    
</table>
</form>
</div>