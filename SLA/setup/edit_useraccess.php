<?php
$scripts = array("useraccess.js");
require_once("../header.php");
$userAccess = new UserAccess();
$user 		= $userAccess->getUserAccessSetting($_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form>
<table id="table_useraccess" width="50%">
		<tr>
            <th>Ref #</th>
            <td><?php echo $_GET['id']; ?></td>
        </tr>
        <tr>
            <th>User:</th>
            <td>
            	<?php echo $user['user']; ?>
            </td>
        </tr>   
        <tr>     
            <th>Directorate:</th>
            <td>
            <select id="directorate" name="directorate">
                <option value="">-directorates-</option>
            </select>        
            </td>        
        </tr>
        <tr>
            <th>Sub-directorate:</th>
            <td>
            <select id="sub_directorate" name="sub_directorate">
                <option value="">-subdirectorate-</option>
            </select>        
            </td>        
        </tr>
        <tr>
            <th>Module Admin:</th>       
            <td>
            <select id="module_admin" name="module_admin">
                <option value="1" <?php if(($user['status'] & 1) == 1 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 1) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>                
            </td>                                   
        </tr>
        <tr>
            <th>Contract Manager:</th>     
            <td>
            <select id="contract_manager" name="contract_manager">
                <option value="1" <?php if(($user['status'] & 2) == 2 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 2) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>                      
            </td>                               
        </tr>
        <tr>
            <th>Create Contracts:</th>  
            <td>
            <select id="create_contract" name="create_contract">
                <option value="1" <?php if(($user['status'] & 4) == 4 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 4) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>                    
            </td>                                   
        </tr>
        <tr>
            <th>Create Deliverable:</th> 
            <td>
            <select id="create_deliverable" name="create_deliverable">
                <option value="1" <?php if(($user['status'] & 8) == 8 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 8) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>          
            </td>                        
        </tr>
        <tr>
            <th>Approval:</th>
            <td>
            <select id="approval" name="approval">
                <option value="1" <?php if(($user['status'] & 16) == 16 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 16) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>                  
            </td>        
        </tr>
        <tr>
            <th>View All:</th> 
            <td>
            <select id="viewall" name="viewall">
                <option value="1" <?php if(($user['status'] & 32) == 32 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 32) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>          
            </td>                                
        </tr>
        <tr>
            <th>Edit All:</th>   
            <td>
            <select id="editall" name="editall">
                <option value="1" <?php if(($user['status'] & 64) == 64 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 64) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>         
            </td>             
        </tr>
        <tr>
            <th>Reports:</th>
            <td>
            <select id="reports" name="reports">
                <option value="1" <?php if(($user['status'] & 128) == 128 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 128) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>            
            </td>              
        </tr>
        <tr>
            <th>Assurance:</th>  
            <td>
            <select id="assurance" name="assurance">
                <option value="1" <?php if(($user['status'] & 256) == 256 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 256) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>         
            </td>                  
        </tr>
        <tr>
            <th>Setup:</th>  
            <td>
            <select id="setup" name="setup">
                <option value="1" <?php if(($user['status'] & 512) == 512 ){?> selected <?php } ?>>Yes</option>
                <option value="0" <?php if(($user['status'] & 512) == 0 ){?> selected <?php } ?>>No</option>                        
            </select>          
            </td>               
        </tr>
		<tr>    
        	<th></th>       
    		<td>
                <input type="submit" name="edit" id="edit" value="Edit" />
                <input type="hidden" id="useraccessid" name="useraccessid" value="<?php echo $_GET['id']; ?>" /> 
            </td>        
   		 </tr>    
   		<tr>
   			<td class="noborder"><?php displayGoBack("", ""); ?></td>
   			<td class="noborder"></td>
   		</tr>
</table>
</form>
</div>
