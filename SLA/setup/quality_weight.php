<?php
$scripts = array("qualityweight.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="qualityweight" name="qualityweight">
			<table id="table_qualityweight">
				<tr>
					<th>Ref</th>	
					<th>Quality Weight</th>	    
					<th>Quality Weight Description</th>
					<th>Quality Weight Definition</th>
					<th>Color assigned to quality</th>
					<th></th>                        	           
					<th></th>  
			        <th></th>       
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="qweight" id="qweight" value="" /> </td> 
			    	<td><input type="text" name="description" id="description" value="" /></td>                
			    	<td><textarea name="definition" id="definition"></textarea></td>              
			    	<td><input type="text" name="color" id="color" class="color" value="CCCCDD" /></td>    
			    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("quality_weight_logs", true)?></td>
	</tr>
</table>
</div>
