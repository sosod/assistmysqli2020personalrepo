<?php
$scripts = array("otherweight.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="otherweight" name="otherweight">
			<table id="table_otherweight">
				<tr>
					<th>Ref</th>	
					<th>Other Weight</th>	    
					<th>Other Weight Description</th>
					<th>Other Weight definition</th>
					<th>Color assigned to other weight</th>
					<th></th>                        	           
					<th></th> 
					<th></th>        
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="oweight" id="oweight" value="" /></td> 
			    	<td><input type="text" name="description" id="description" value="" /></td>                
			    	<td><textarea name="definition" id="definition"></textarea></td>              
			    	<td><input type="text" name="color" id="color" class="color" value="" /></td>    
			    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("other_weight_logs", true)?></td>
	</tr>
</table>
</div>
