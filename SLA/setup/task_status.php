<?php
$scripts = array("taskstatus.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
	<table class="noborder" width="50%">
		<tr>
			<td class="noborder" colspan="2">
				<form method="post" id="form-taskstatus">
				<table id="table_taskstatus" width="100%">
					<tr>
				    	<th>Ref</th>
				    	<th>Task Status</th>             
				    	<th>Colour</th>
				    	<th></th>                                                             
				    </tr>
				</table>
				</form>		
			</td>
		</tr>
		<tr>
			<td class="noborder"><?php displayGoBack("", ""); ?></td>
			<td class="noborder"><?php displayAuditLogLink("task_status_logs", true)?></td>
		</tr>
	</table>
</div>
