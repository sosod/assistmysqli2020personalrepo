<?php
$scripts = array("contracttype.js", "contractcatergory.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="form-contractcategory">
			<table id="table_contractcategory">
				<tr>
			    	<th>Ref</th>
			    	<th>Short Code</th>
			    	<th>Contract Category</th>        
			    	<th>Desciption of Contract Category</th>             
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td>
			    	<td><input type="text" name="name" id="name" value="" /></td>                
			    	<td><textarea id="description" name="description"></textarea></td>        
			    	<td><input type="submit" name="savecat" id="savecat" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("contract_categories_logs", true)?></td>		
	</tr>
</table>
</div>
