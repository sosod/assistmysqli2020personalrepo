<?php
$scripts = array("assessmentfreq.js", "jquery-ui-timepicker-addon.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form method="post" id="form-assessmentfreq">
			<table id="table_assessmentfreq">
				<tr>
			    	<th>Ref</th>
			    	<th>Short Code</th>
			    	<th>Assessment Type</th>        
			    	<th>Desciption of Assessment Type</th>    
			        <th>Date Time</th>            
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td>
			    	<td><input type="text" name="name" id="name" value="" /></td>                
			    	<td><textarea id="description" name="description"></textarea></td>
			    	<td><input type="text" name="date" id="date" value="" /></td>        
			    	<td><input type="submit" name="save" id="save" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("assessment_frequency_logs", true)?></td>
	</tr>
</table>
</div>
