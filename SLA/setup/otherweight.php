<?php
$scripts = array("otherweight.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<span id="loading"></span>
<form id="otherweight" name="otherweight">
<table id="table_otherweight">
	<tr>
		<th>Id</th>	
		<th>Other Weight</th>	    
		<th>Other Weight Description</th>
		<th>Other Weight definition</th>
		<th>Color assigned to other weight</th>
		<th></th>                        	           
		<th></th>         
    </tr>
    <tr>
    	<td>#</td>
    	<td><input type="text" name="oweight" id="oweight" value="" /></td> 
    	<td><input type="text" name="description" id="description" value="" /></td>                
    	<td><textarea name="definition" id="definition" value=""></textarea></td>              
    	<td><input type="text" name="color" id="color" class="color" value="" /></td>    
    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
    	<td></td> 
    	<td></td>                       
    </tr>
</table>
</form>
</div>