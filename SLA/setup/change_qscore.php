<?php
$scripts = array("qualityscore.js");
require_once("../header.php");
$q 		= new QualityScore();
$qscore	= $q -> getQualityScore( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="qualityscore" name="qualityscore">
<table id="table_qualityscore">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
     </tr>
    <tr>
    	<th>Quality Score Status:</th>
    	<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($qscore['status'] & 1)==1) {?> selected<?php } ?>>Active</option>
            	<option value="0" <?php if(($qscore['status'] & 1)==0) {?> selected<?php } ?>>InActive</option>                
            </select>
        </td>    
    </tr>
    <tr>
    	<th></th>
    	<td><input type="submit" name="updatestatus" id="updatestatus" value="Update" />
        <input type="hidden" id="qualityscoreid" name="qualityscoreid" value="<?php echo $_GET['id']; ?>" />        
        </td>                             
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>