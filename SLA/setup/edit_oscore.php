<?php
$scripts = array("otherscore.js", "jscolor.js");
require_once("../header.php");
$q 	    = new OtherScore();
$osocre = $q -> getOtherScore( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="otherscore" name="otherscore">
<table id="table_otherscore">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
		<th>Other Score:</th>	    
		<td><input type="text" name="oscore" id="oscore" value="<?php echo $osocre['oscore']; ?>" size="3"/></td>    
    </tr>
    <tr>
		<th>Other Score Description:</th>
    	<td><input type="text" name="description" id="description" value="<?php echo $osocre['description']; ?>" /></td>                
    </tr>
    <tr>        
		<th>Other Score Definition:</th>
		<td><textarea name="definition" id="definition"><?php echo $osocre['definition']; ?></textarea></td>                      
    </tr>
    <tr>
		<th>Color assigned to other score:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $osocre['color']; ?>" /></td> 
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="update" id="update"  value="Update" />
        	<input type="hidden" id="otherscoreid" name="otherscoreid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>