<?php
$scripts = array("suppliercategory.js");
require_once("../header.php");
$suppcat    = new SupplierCategory();
$spcategory = $suppcat -> getSupplierCategory( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-suppliercategory">
<table id="table_suppliercategory">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>     
    <tr>
        <th>Supplier Category Status:</th>            
    	<td>
        <select id="status" name="status">
        	<option value="1" <?php if(($spcategory['status'] & 1) == 1) {?> selected <?php } ?>>Active</option>    
        	<option value="0" <?php if(($spcategory['status'] & 1) == 0) {?> selected <?php } ?>>Inactive</option>                    
        </select>
       </td>   
    </tr>     	
	<tr>     	
    	<th></th>              
    	<td>
	        <input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
	        <input type="hidden" name="suppcatid" id="suppcatid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>  
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder"></td>
    </tr>  
</table>
</form>
</div>