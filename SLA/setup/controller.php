<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} 
}
class SetupController extends Controller
{
	//
	function __construct()
	{
		
	}
	
	function getMenu()
	{
		$obj = $this -> loadModel("Menu");
		$data = $obj -> getMenuItems();
		echo json_encode( $data );
	}

	function updateMenu()
	{
		$id      = $_POST['id'];
		$name    = $_POST['clientname'];
		$obj     = $this -> loadModel("Menu");
		$result  = $obj -> saveMenu( $id, $name);
		echo $this -> _updateCheck( $result, "menu name");
	}
	
	function getNaming()
	{
		$obj  = $this -> loadModel("Naming");
		$data = $obj -> getNaming(); 
		echo json_encode( $data );
	}
	
	function getColumns(){
		$obj  = $this -> loadModel("Naming");
		$data = $obj -> getNaming(); 
		$columns = array();
		foreach( $data as $key => $value ){
			if($value['active'] == 1){
				$columns['active'][$key] = $value;
			} else {
				$columns['inactive'][$key] = $value;	
			}
		}		
		echo json_encode($columns);
	}
	
	function saveColumnOrder(){
		$obj  = $this -> loadModel("Naming");
		parse_str( $_POST['active'], $active ); 
		parse_str( $_POST['inactive'], $inactive ); 						
		$obj -> saveColumnsOrder( $active['column'], $inactive['column']);
	}
	
	function updateNaming()
	{
		$id     = $_POST['id'];
		$name   = $_POST['name'];
		$obj    = $this -> loadModel("Naming");
		$res    = $obj -> saveNaming( $id, $name); 		
		$response = array();
		echo $this -> _updateCheck( $res, "naming");

	}

	function _isValidStartEndDate( $start, $end)
	{
		$startstamp = strtotime($start);
		$endstamp   = strtotime($end);
		
		if( $endstamp > $startstamp )
		{
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

	function saveFinYear()
	{
		$lastday 	= $_POST['lastday'];
		$startdate  = $_POST['startdate'];
		$enddate    = $_POST['enddate'];
		$response	= array();
		if( $this -> _isValidStartEndDate( $startdate, $enddate) ){
			$obj 		= $this -> loadModel("FinancialYear");
			$insertdata =  array( "lastday" => $lastday, "enddate" => $enddate , "startdate" => $startdate );
			$res 		= $obj -> save( $insertdata );
			if( $res > 0 ) 
			{
				$response = array("error" => false, "text" => "Financial year saved successifully", "id" => $res);	
			} else {
				$response = array("error" => true, "text" => "Error saving financial year");				
			}
		} else {
			$response = array( "error" => true, "text" => "Start date cannot be bigger than start date ");
		}
		echo json_encode( $response );
	}

	function getFinancialYear()
	{
		$obj 	  = $this -> loadModel("FinancialYear");
		$response = $obj -> getFinYears();
		echo json_encode( $response ); 
	}
	
	function updateFinancialYear()
	{
		$lastday 	= $_POST['lastday'];
		$startdate  = $_POST['startdate'];
		$enddate    = $_POST['enddate'];
		$id    		= $_POST['id'];
		$response   = array(); 
		
		if( $this -> _isValidStartEndDate($startdate, $enddate)){
			$obj		= $this -> loadModel("FinancialYear");
			$updatedata =  array( "lastday" => $lastday, "enddate" => $enddate , "startdate" => $startdate );
			$res 		= $obj -> updateFinyear($id, $updatedata);
			echo $this -> _updateCheck( $res, "financial year");
		}
	}
	
	function deleteFinYear()
	{
		$id  	  = $_POST['id'];
		$obj 	  = $this -> loadModel("FinancialYear");
		$res	 = $obj -> updateStatus($id, 0);
		echo $this -> _updateCheck( $res, "financial year");
	}
	
	function saveStatus()
	{
		$insertdata = array();
		foreach($_POST as $field => $value)
		{
			$insertdata[$field] = $value;
		} 
		$obj 	   = $this -> loadModel("ContractStatus");
		$res  	   = $obj -> save($insertdata);
		$response  = array();
		if( $res > 0 )
		{
			$response = array("error" => false, "text" => "Save was successfully", "id" => $res);
		} else {
			$response = array("error" => true, "text" => "Error Saving contract status");
		}
		echo json_encode($response); 
	}

	function getContractStatus()
	{
		$obj 	   = $this -> loadModel("ContractStatus");
		$response  = $obj -> getStatuses();
		echo json_encode( $response );
	}
	
	function updateStatus()
	{
		$obj 	    = $this -> loadModel("ContractStatus");
		$updatedata = array(); 
		$id 		= "";
		foreach($_POST as $field => $value)
		{
			if($field == "id"){
				$id = $value;
			} else{
				$updatedata[$field] = $value;
			}
		}
		$res  =	$obj -> updateStatus( $id , $updatedata);
		echo $this -> _updateCheck( $res, "contract status");
	}
	
	function changeCStatus()
	{
		$obj  = $this -> loadModel("ContractStatus");
		$res = $obj -> updateCStatus( $_POST['id'], $_POST['tostatus'] ); 
		echo $this -> _updateCheck( $res, "contract status");
	}
	
	// =======================================================================================================

	function getDeliverableStatus()
	{
		$obj = $this -> loadModel("DeliverableStatus");
		$res = $obj -> getDeliverableStatuses();
		echo json_encode( $res );
	}
	
	function saveDeliverable()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value ){
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("DeliverableStatus");
		$result   = $obj -> save($insertdata);
		$response = array(); 
		if( $result > 0 ){
			$response = array("error" => false, "text" =>"Saved successfully", "id" => $result);
		} else {
			$response = array("error" => true, "text" => "Error saving");
		}
		echo json_encode( $response );
	}
	
	function updateDeleiverableStatus()
	{
		$updatedata = array();
		$id 		= "";
		foreach( $_POST as $field => $value ){
			if( $field == "id" ){
				$id = $value;
			} else{
				$updatedata[$field] = $value;
			}
		}
		$obj 	=  $this -> loadModel("DeliverableStatus");
		$result = $obj  -> updateDeliverableStatus( $id, $updatedata);
		echo $this -> _updateCheck($result, "deliverable status");
	}
	
	function changeDeliverableStatus()
	{
		$obj 	=  $this -> loadModel("DeliverableStatus");
		$result = $obj  -> changeDStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck($result, "deliverable status");		
	}
	// ========================================================================================================
	
	function getActionStatuses()
	{
		$obj = $this -> loadModel("ActionStatus");
		echo json_encode( $obj -> getActionStatuses() );
	}
	
	function saveActionStatus()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value ){
			$insertdata[$field] = $value;
		}	
		$obj 	  = $this -> loadModel("ActionStatus");
		$result   = $obj -> save($insertdata);
		$response = array(); 
		if( $result > 0 ){
			$response = array("error" => false, "text" =>"Saved successfully", "id" => $result);
		} else {
			$response = array("error" => true, "text" => "Error saving");
		}
		echo json_encode( $response );		
	}
	
	 function changeAStatus()
	 {
		$obj 	=  $this -> loadModel("ActionStatus");
		$result = $obj  -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck($result, "action status");				 	
	 }
	
	function updateAStstus()
	{
		$updatedata = array();
		$id 		= "";
		foreach( $_POST as $field => $value ){
			if( $field == "id" ){
				$id = $value;
			} else{
				$updatedata[$field] = $value;
			}
		}
		$obj 	=  $this -> loadModel("ActionStatus");
		$result = $obj  -> updateActionStatus( $id, $updatedata);
		echo $this -> _updateCheck($result, "action status");
	}
	
	// ===============================================================================================
	
	function getTaskStatuses()
	{
		$obj = $this -> loadModel("TaskStatus");
		echo json_encode( $obj -> getTaskStatuses() );
	}
	
	function saveTaskStatus()
	{
		$insertdata = array();
		foreach($_POST as $field => $value ){
			$insertdata[$field] =  $value;
		}
		$obj 	  = $this -> loadModel("TaskStatus");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 )
		{
			$response = array("error" => false, "text" => "Saved Sucessfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the task status"); 	
		}
		echo json_encode( $response );
	}
	
	function updateTaskStstus()
	{
		$updatedata  = array();
		$id 		 = "";
		foreach($_POST as $field => $value ) {
			if($field == "id"){
				$id = $value;
			} else{
				$updatedata[$field] = $value;
			}
		} 
		$object = $this -> loadModel("TaskStatus");
		$res 	= $object -> updateStatus( $id, $updatedata );
		echo $this -> _updateCheck( $res, "task status"); 
		
	}
	
	function changeTaskStatus()
	{
		$obj = $this -> loadModel("TaskStatus");
		$res = $obj -> changeStatus($_POST['id'], $_POST['tostatus']);
		echo $this -> _updateCheck( $res, "task status");
	}
	// =================================================================================================
	
	function getContractTypes()
	{
		$obj = $this -> loadModel("ContractType");
		echo json_encode( $obj -> getContractTypes() ); 
	}
	
	function saveContractType()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("ContractType");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the contract type");	
		}
		echo json_encode($response);
	}
	
	function updateContractType()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("ContractType");
		$res 	  = $obj -> updateContractType( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "contract type" );
	}
	
	function changeContractStatus()
	{
		$obj 	  = $this -> loadModel("ContractType");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "contract type" );		
	}
	
	// =================================================================================================
	
	function getContractCategories()
	{
		$obj = $this -> loadModel("ContractCategory");
		echo json_encode( $obj -> getCategories() );		
	}
	
	function updateContractCategory()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("ContractCategory");
		$res 	  = $obj -> updateCategory( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "contract category" );	
	}
	
	function saveContractCategory()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("ContractCategory");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the contract category");	
		}
		echo json_encode($response);
	}
	
	function changeCategoryStatus()
	{
		$obj 	  = $this -> loadModel("ContractCategory");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "contract type" );				
	}
// ==============================================================================================
	
	function getDeliverableCatergories()
	{
		$obj = $this -> loadModel("DeliverableCategory");
		echo json_encode( $obj -> getCategories() );	
	}
	function updateDeliverableCategory()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("DeliverableCategory");
		$res 	  = $obj -> updateCategory( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "deliverable category" );	
	}
	
	function saveDeliverableCategory()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("DeliverableCategory");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the contract category");	
		}
		echo json_encode($response);
	}
	
	function changeDeliverableCategoryStatus()
	{
		$obj 	  = $this -> loadModel("DeliverableCategory");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "contract type" );				
	}
	
	// ========================================================================================================

	function getAssessmentFrequencies()
	{
		$obj = $this -> loadModel("AssessmentFrequency");
		echo json_encode( $obj -> getAssessmentFrequencies() );		
	}
	
	function updateAssessmentFrequency()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("AssessmentFrequency");
		$res 	  = $obj -> updateAssessmentFrequency( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "assessment frequency" );	
	}
	
	function saveAssessmentFrequency()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("AssessmentFrequency");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the assessment frequency");	
		}
		echo json_encode($response);
	}
	
	function changeAssessmentFrequency()
	{
		$obj 	  = $this -> loadModel("AssessmentFrequency");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "assessment frequency" );				
	}
	//=========================================================================================
		
	function getSupplierCategories()
	{
		$obj = $this -> loadModel("SupplierCategory");
		echo json_encode( $obj -> getSupplierCategories() );		
	}
	
	function updateSupplierCategory()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("SupplierCategory");
		$res 	  = $obj -> updateSupplierCategory( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "supplier category" );	
	}
	
	function saveSupplierCategory()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("SupplierCategory");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the supplier category");	
		}
		echo json_encode($response);
	}
	
	function changeSupplierCategory()
	{
		$obj 	  = $this -> loadModel("SupplierCategory");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "supplier category" );				
	}
	
	// ========================================================================================
	
	function getSuppliers()
	{
		$obj = $this -> loadModel("Supplier");
		echo json_encode( $obj -> getSuppliers() );		
	}
	
	function updateSupply()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("Supplier");
		$res 	  = $obj -> updateSupplier( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "supplier" );	
	}
	
	function saveSupply()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("Supplier");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the supplier");	
		}
		echo json_encode($response);
	}
	
	function changeSupplyStatus()
	{
		$obj 	  = $this -> loadModel("Supplier");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "supplier" );				
	}	
	
		// ========================================================================================
	
	function getScoreMatrixs()
	{
		$obj = $this -> loadModel("ScoreMatrix");
		echo json_encode( $obj -> getScoreMatrixs() );		
	}
	
	function updateScoreMatrix()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("ScoreMatrix");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "score matrix" );	
	}
	
	function saveScoreMatrix()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("ScoreMatrix");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the score matrix");	
		}
		echo json_encode($response);
	}
	
	function changeScoreMatrix()
	{
		$obj 	  = $this -> loadModel("ScoreMatrix");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "supplier" );				
	}	
	//============================================================================================
	
	function getDeliveredWieghts()
	{
		$obj = $this -> loadModel("DeliveredWieght");
		echo json_encode( $obj -> getDeliveredWieghts() );		
	}
	
	function updateDeliveredWieght()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("DeliveredWieght");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "score matrix" );	
	}
	
	function saveDeliveredWieght()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("DeliveredWieght");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the score matrix");	
		}
		echo json_encode($response);
	}
	
	function changeDeliveredWieght()
	{
		$obj 	  = $this -> loadModel("DeliveredWieght");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "supplier" );				
	}		
	
		//============================================================================================
	
	function getQualityWeights()
	{
		$obj = $this -> loadModel("QualityWeight");
		echo json_encode( $obj -> getQualityWeights() );		
	}
	
	function updateQualityWeight()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("QualityWeight");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "quality weight" );	
	}
	
	function saveQualityWeight()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("QualityWeight");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the quality weight");	
		}
		echo json_encode($response);
	}
	
	function changeQualityWeight()
	{
		$obj 	  = $this -> loadModel("QualityWeight");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "quality weight" );				
	}	
	// ============================================================================================	
	function getOtherWeights()
	{
		$obj = $this -> loadModel("OtherWeight");
		echo json_encode( $obj -> getOtherWeights() );		
	}
	
	function updateOtherWeight()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("OtherWeight");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "quality weight" );	
	}
	
	function saveOtherWeight()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("OtherWeight");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the other weight");	
		}
		echo json_encode($response);
	}
	
	function changeOtherWeight()
	{
		$obj 	  = $this -> loadModel("OtherWeight");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "other weight" );				
	}	
	// =========================================================================================

	function getQualityScores()
	{
		$obj = $this -> loadModel("QualityScore");
		echo json_encode( $obj -> getQualityscores() );		
	}
	
	function updateQualityScore()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("QualityScore");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "quality score" );	
	}
	
	function saveQualityScore()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("QualityScore");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the quality score");	
		}
		echo json_encode($response);
	}
	
	function changeQualityScore()
	{
		$obj 	  = $this -> loadModel("QualityScore");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "quality score" );				
	}	
	// =========================================================================================

	function getOtherScores()
	{
		$obj = $this -> loadModel("OtherScore");
		echo json_encode( $obj -> getOtherScores() );		
	}
	
	function updateOtherScore()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("OtherScore");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "quality score" );	
	}
	
	function saveOtherScore()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("OtherScore");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the other score");	
		}
		echo json_encode($response);
	}
	
	function changeOtherScore()
	{
		$obj 	  = $this -> loadModel("OtherScore");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "other score" );				
	}	
	// =========================================================================================

	function getNotifications()
	{
		$obj = $this -> loadModel("Notification");
		echo json_encode( $obj -> getNotifications() );		
	}
	
	function updateNotification()
	{
		$insertdata = array();
		$id 		= "";
		foreach( $_POST as $field => $value )
		{
			if( $field == "id" ){
				$id = $value;
			}else{
				$insertdata[$field] = $value;
			}
		}
		$obj 	  = $this -> loadModel("Notification");
		$res 	  = $obj -> updateData( $id, $insertdata );		
		echo $this -> _updateCheck( $res, "notification" );	
	}
	
	function saveNotification()
	{
		$insertdata = array();
		foreach( $_POST as $field => $value )
		{
			$insertdata[$field] = $value;
		}
		$obj 	  = $this -> loadModel("Notification");
		$res 	  = $obj -> save( $insertdata );
		$response = array();
		if( $res > 0 ) {
			$response = array("error" => false, "text" => "Saved successfully", "id" => $res);
		} else{
			$response = array("error" => true, "text" => "Error saving the notification");	
		}
		echo json_encode($response);
	}
	
	function changeNotification()
	{
		$obj 	  = $this -> loadModel("Notification");
		$res 	  = $obj -> changeStatus( $_POST['id'], $_POST['tostatus'] );
		echo $this -> _updateCheck( $res, "notification" );				
	}	
	// =========================================================================================
	
	function getGlossaries()
	{
		$obj = $this->loadModel("Glossary");
		echo json_encode( $obj -> getGlossaries() );
	}
	
	function saveGlossary()
	{
		$obj = $this->loadModel("Glossary");
		$insertdata = array();
		foreach( $_POST as $key => $value )
		{
			$insertdata[$key] = mysql_real_escape_string( $value );
		}
		
		echo json_encode( $obj -> save( $insertdata ) );	
	}

	function updateGlossary()
	{
		$obj 		= $this->loadModel("Glossary");
		$insertdata = array();
		$id 		= 0;
		foreach( $_POST as $key => $value )
		{
			if( $key == "id"){
				$id = $value;
			} else {
				$updatedata[$key] = mysql_real_escape_string( $value );
			}
		}
		$res = $obj -> updateGlossary( $id, $updatedata  );
		$response = array();
		if( $res == 1)
		{
			$response = array("error" => false, "text" => "Glossary term updated");
		} else if( $res == 0){
			$response = array("error" => true, "text" => "No change was made");						
		} else {
			$response = array("error" => true, "text" => "Error updating glossary");
		}
		echo json_encode( $response );	
	}	
	// =========================================================================================
	
	function getUsers()
	{
		$obj = $this ->loadModel("UserAccess");
		echo json_encode( $obj -> getUsers() );
	}
	
	function getUAssignedUsers()
	{
		$obj = $this ->loadModel("UserAccess");
		$users 	= $obj -> unsedUserAccess();
		echo json_encode( $users );
	}
	
	function saveUserAccess()
	{
		$obj 		 = $this ->loadModel("UserAccess");
		$user_status = 0;
		if($_POST['module_admin'] == 1 ){
			$user_status += UserAccess::MODULE_ADMIN;
		} 				
		if($_POST['contract_manager'] == 1 ){
			$user_status += UserAccess::CONTRACT_MANAGER;
		} 
		 if($_POST['create_contract'] == 1 ){
			$user_status += UserAccess::CREATE_CONTRACT;
		} 
		if($_POST['create_deliverable'] == 1){
			$user_status += UserAccess::CREATE_DELIVERABLE;			
		} 
		 if($_POST['approval'] == 1){
			$user_status += UserAccess::APPROVAL;		
		} 
		 if($_POST['viewall'] == 1){
			$user_status += UserAccess::VIEW_ALL;		
		} 
		if($_POST['editall'] == 1){
			$user_status += UserAccess::EDIT_ALL;		
		} 
		if($_POST['reports'] == 1) {
			$user_status += UserAccess::REPORTS;		
		} 
		if($_POST['assurance'] == 1) {
			$user_status += UserAccess::ASSURANCE;			
		} 
		 if($_POST['setup'] == 1){
			$user_status += UserAccess::SETUP;
		}
		
		$insertdata = array("status" => $user_status, "user_id" => $_POST['user'], "insert_user" => $_SESSION['tid'] );
		
		$res 		= $obj->save( $insertdata );
		$response 	= array(); 
		if( $res > 0){
			$response = array("text" => "User settings saved", "error" => false, "id" => $res);
		} else{
			$response = array("text" => "User settings saved", "error" => false);			
		}
		echo json_encode( $response );
	}
	
	function getUserAccess()
	{
			$obj 		 = $this ->loadModel("UserAccess");
			echo json_encode( $obj->getUserAccessSettings() );	
	}

	function editUserSettings()
	{
		$obj 		 = $this ->loadModel("UserAccess");
		$user_status = 0;
		if($_POST['module_admin'] == 1 ){
			$user_status += UserAccess::MODULE_ADMIN;
		} 		
		if($_POST['contract_manager'] == 1 ){
			$user_status += UserAccess::CONTRACT_MANAGER;
		} 
		 if($_POST['create_contract'] == 1 ){
			$user_status += UserAccess::CREATE_CONTRACT;
		} 
		if($_POST['create_deliverable'] == 1){
			$user_status += UserAccess::CREATE_DELIVERABLE;			
		} 
		 if($_POST['approval'] == 1){
			$user_status += UserAccess::APPROVAL;		
		} 
		 if($_POST['viewall'] == 1){
			$user_status += UserAccess::VIEW_ALL;		
		} 
		if($_POST['editall'] == 1){
			$user_status += UserAccess::EDIT_ALL;		
		} 
		if($_POST['reports'] == 1) {
			$user_status += UserAccess::REPORTS;		
		} 
		if($_POST['assurance'] == 1) {
			$user_status += UserAccess::ASSURANCE;			
		} 
		 if($_POST['setup'] == 1){
			$user_status += UserAccess::SETUP;
		}
		
		$insertdata = array("status" => $user_status, "insert_user" => $_SESSION['tid'] );
		
		$res 		= $obj->updateUserAccess( $_POST['id'], $insertdata );
		$response 	= array(); 
		if( $res == 1){
				$response = array("text" => "Update successfull", "error" => false);
		} else{
		   $response = array("text" => "Error updating", "error" => true);			
		}
		echo json_encode( $response );
	}
	
	//get action columns 
	function getActionColumns()
	{
		$actCols = $this->loadModel("ActionColumns");
		$columns = $actCols -> getNaming();
		echo json_encode( $columns );
	}
	
	//get deliverable columns
	function getDeliverableColumns()
	{			
		$delCols = $this->loadModel("DeliverableColumns");
		$columns = $delCols ->  getNaming();
		echo json_encode( $columns );
	}
	
	//	get contact columns
	function getContractColumns()
	{
		$colsObu = $this->loadModel("ContractColumns");
		$columns = $colsObu -> getNaming();	
		echo json_encode( $columns );
	}
	
	//update columns
	function updateColumns()
	{
		$namObj	  = $this->loadModel("Naming"); 
		$position = array();	
		$manage	  = array();
		$new	  = array();
		$res 	  = "";
		foreach( $_POST['data'] as $index => $valArr)
		{
			if( $valArr['name'] == "position")
			{
				array_push($position, $valArr['value'] ); 
			}
			
			if( $valArr['name'] == "manage")
			{
				array_push($manage, $valArr['value'] );				
			}

			if( $valArr['name'] == "new")
			{
				array_push($new, $valArr['value'] );				
			} 
		}
		
		foreach( $position as $index => $id)
		{
			$updateArr = array();
			if( in_array($id, $manage))
			{
				$updateArr  = array("status" => 4, "position" => $index);				
			} else {
				$updateArr  = array("status" => 0, "position" => $index);
			}
			
			if( in_array($id, $new))
			{
				$updateArr = array("status" => $updateArr['status'] + 8, "position" => $index);				
			} else {
				$updateArr = array("status" => $updateArr['status'] + 0, "position" => $index);
			}	
			$res += $namObj -> update("naming", $updateArr, "id=$id");
		}
		echo $this->_updateCheck( $res , "columns");		
	}
	
	
	function deleteUserAccess()
	{
			$obj  = $this ->loadModel("UserAccess");
			$res  = $obj->updateUserAccess( $id, array("status" => $_POST['status']));
			$response = array();
			if( $res == 1 ){
				$response = array("text" => "Update successfull", "error" => false);
			} else {
			   $response = array("text" => "Error updating", "error" => true);
			}
			echo json_encode( $response );	
	}	
	//checks the staus of the update made to the database and returns the appropriate message
	//@return json encoded string 
	function _updateCheck( $res , $context)
	{
		if( $res == 1 || $res > 0){
			$response = array("error" => false, "text" => "Update successfull");
		} else if( $res == 0){
			$response = array("error" => true, "text" => "No change was made");
		} else{
			$response = array("error" => true, "text" => "Error updating ".$context." ");
		}
		return json_encode( $response );
	}
	
	function view_log()
	{
        $logObj  = new Logs("","","");
        $logs    = $logObj -> viewLogs( $_POST['table_name'] );
        echo json_encode($logs);	
	}
}
$method 	=  $_GET['action'];
$controller = new SetupController();
if(method_exists($controller, $method)){
	$controller -> $method();
}
?>
