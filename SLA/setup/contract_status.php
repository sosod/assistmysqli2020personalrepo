<?php
$scripts = array("jscolor.js","contractstatus.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post">
			<table id="table_contractstatus">
				<tr>
			    	<th>Ref</th>
			    	<th>Contract Status</th>
			    	<th>&lt;Client&gt; Teminology</th>                
			    	<th>Color</th>
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>
			    	<td><input type="text" name="client_terminology" id="client_terminology" value="" /></td>                
			    	<td><input type="text" name="color" id="color" class="color" value="e82929" /></td>
			    	<td><input type="submit" name="save" id="save" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("contract_status_logs", true)?></td>		
	</tr>
</table>
</div>
