<?php
$scripts = array("financialyear.js");
require_once("../header.php");
?>
<div class="displayview">
<span id="message" class="displaymsg"></span>
<form method="post" id="financialyear">
<span id="loading"></span>
<table id="table_financialyear">
	<tr>
    	<th>#</th>
    	<th>Last Day</th>
    	<th>Start Day</th>
    	<th>End Day</th>                        
    	<th></th>        
    </tr>
    <tr>
    	<td></td>
    	<td><input type="text" name="lastday" id="lastday" class="datepicker" value="" /></td>
    	<td><input type="text" name="startdate" id="startdate" class="datepicker" value="" /></td>
    	<td><input type="text" name="enddate" id="enddate" class="datepicker" value="" /></td>
    	<td><input type="submit" name="save" id="save" value="Add" /></td>                                
    </tr>
</table>
</form>
</div>