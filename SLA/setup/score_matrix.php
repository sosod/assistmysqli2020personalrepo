<?php
$scripts = array("scorematrix.js","jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form id="scorematrix" name="scorematrix">
			<table id="table_scorematrix">
				<tr>
					<th>Ref</th>	
					<th>Delivered Score Matrix</th>	    
					<th>Delivered Score Matrix Description</th>
					<th>Delivered Score Matrix definition</th>
					<th>Color assigned to <br />Delivered Score Matrix</th>
					<th></th>                        	           
					<th></th>
			        <th></th>     
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="dscore" id="dscore" value="" /> </td> 
			    	<td><input type="text" name="description" id="description" value="" /></td>                
			    	<td><textarea name="definition" id="definition"></textarea></td>              
			    	<td><input type="text" name="color" id="color" class="color" value="" /></td>    
			    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("delivered_score_matrix_logs", true)?></td>		
	</tr>
</table>
</div>
