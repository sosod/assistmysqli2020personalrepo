<?php
$scripts = array("contracttype.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="form-contracttype">
			<table id="table_contracttype">
				<tr>
			    	<th>Ref</th>
			    	<th>Short Code</th>
			    	<th>Contract Type</th>        
			    	<th>Desciption of Contract Type</th>                
			    	<th></th>                        
			    	<th>Status</th>                
			    	<th></th>                        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td>
			    	<td><input type="text" name="name" id="name" value="" /></td>                
			    	<td><textarea id="description" name="description"></textarea></td>
			    	<td><input type="submit" name="savetype" id="savetype" value="Add"  /></td>                        
			    	<td></td>                
			    	<td></td>                        
			    </tr>    
			</table>
			</form>				
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("contract_type_logs", true)?></td>			
	</tr>
</table>
</div>

