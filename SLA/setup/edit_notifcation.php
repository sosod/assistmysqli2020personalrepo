<?php
$scripts = array("notifcationrules.js", "jquery-ui-timepicker-addon.js");
require_once("../header.php");
$not 		  = new Notification();
$notification = $not -> getNotification( $_GET['id'] ); 
?>
<script language="javascript">
$(function(){
	$("#date").timepicker();
});
</script>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="notifcationrules" name="notifcationrules">
<table id="table_notifcations">
	<tr>
		<th>Ref #:</th>	
    	<td><?php echo $_GET['id'] ?></td>
     </tr>
     <tr>        
		<th>Short Code:</th>	    
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $notification['shortcode']; ?>" /></td> 
     </tr>
     <tr>     
		<th>Notification Type:</th>
    	<td><input type="text" name="type" id="type" value="<?php echo $notification['type']; ?>" /></td>            
     </tr>
     <tr>
		<th>Description of notification:</th>
    	<td><textarea name="description" id="description"><?php echo $notification['description']; ?></textarea></td>         
     </tr>   
     <tr>
		<th>Day Time:</th>
    	<td><input type="text" name="date" id="date" value="<?php echo $notification['date']; ?>" /></td>        
     </tr>
    <tr> 
    	<th></th>
    	<td>
	      <input type="submit" name="update" id="update"  value="Update" />
	      <input type="hidden" name="notificationid" id="notificationid" value="<?php echo $_GET['id']; ?>" />
      </td>                                                
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>