<?php
class DeliverableStatus extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}

	function save( $data )
	{
		$response = $this -> insert("deliverable_status", $data);
		return $this->insertedId();
	}

	function getDeliverableStatuses()
	{
		$response = $this -> get("SELECT * FROM #_deliverable_status WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."");
		return $response; 
	}
	
	function getActiveDeliverableStatuses()
	{
		$response = $this -> get("SELECT * FROM #_deliverable_status 
								  WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE." 
								  AND status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." 
								 ");
		return $response; 	
	}
	
	function getDelStatus( $id )
	{
		$response  = $this -> getRow("SELECT * FROM #_deliverable_status WHERE id = $id");
		return $response;
	}
	
	function updateDeliverableStatus( $id, $data )
	{
		$dat    = $this->getDelStatus( $id );
        $logsObj = new Logs($data , $dat, "deliverable_status");
        $logsObj -> createLog();		
		$response = $this -> update("deliverable_status", $data, "id=$id");
		return $response;
	}
	
	function changeDStatus($id, $tostatus)
	{
		$dat    = $this->getDelStatus( $id );
        $logsObj = new Logs(array("status" => $tostatus ), $dat, "deliverable_status");
        $logsObj -> createLog();		
		$response = $this -> update("deliverable_status", array("status" => $tostatus ), "id=$id");
		return $response;
	} 
	
}
?>