<?php
class ContractType extends DBConnect
{

	function __construct()
	{
		parent::__construct();
	}

	function getContractTypes()
	{
		$response = $this -> get("SELECT * FROM #_contract_type WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." ");
		return $response; 
	}
	
	function getContractType($id)
	{
		$response = $this -> getRow("SELECT * FROM #_contract_type WHERE id = $id");
		return $response;
	}
	
	function getActiveContractType()
	{
		$response = $this -> get("SELECT * FROM #_contract_type WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
		return $response;	
	}
	
	function save( $data )
	{
		$res = $this -> insert( "contract_type",$data );
		return $this -> insertedId();
	}
	
	function updateContractType( $id, $data)
	{
		$dat    = $this->getContractType( $id );
        $logsObj = new Logs($data , $dat, "contract_type");
        $logsObj -> createLog();		
		$res = $this -> update("contract_type", $data, "id=$id");
		return $res;
	}
	
	function changeStatus($id, $tostatus)
	{
		$dat    = $this->getContractType( $id );
        $logsObj = new Logs(array("status" => $tostatus) , $dat, "contract_type");
        $logsObj -> createLog();			
		$res = $this -> update("contract_type", array("status" => $tostatus), "id=$id");
		return $res;
	}

}

?>