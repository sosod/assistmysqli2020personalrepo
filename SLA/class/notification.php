<?php
class Notification extends DBConnect
{
	
	protected $table = "notification_rules";
		
	function __construct()
	{
		parent::__construct();
	}
	
	function save( $data ) 
	{
		$this -> insert( $this -> table , $data);
		return $this -> insertedId();
	}	
	
	function updateData( $id, $data)
	{
    	$dat    = $this->getNotification( $id );
        $logsObj = new Logs($_POST , $dat, $this -> table);
        $logsObj -> createLog();			
		$res = $this -> update( $this -> table, $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus)
	{
    	$data    = $this->getNotification( $id );
        $logsObj = new Logs( array("status" => $tostatus) , $data, $this -> table);
        $logsObj -> createLog();			
		$res = $this -> update( $this -> table, array("status" => $tostatus), "id=$id");
		return $res;
	}
	
	function getNotifications()
	{
		$response = $this -> get("SELECT * FROM #_".$this -> table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."");
		return $response;
	}
	
	function getNotification( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_".$this -> table." WHERE id = $id");
		return $response;
	}
}
?>