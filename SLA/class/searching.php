<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/10/11
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
 
class Searching extends DBConnect{

    protected  $actionHeaders      = array();

    protected $deliverableHeaders  = array();

    protected $contractHeaders     = array();

    function __construct()
    {
        parent::__construct();
        $this->searchText = $_REQUEST['searchText'];
    }

    function search()
    {
         $actions      = $this->_searchActions();
         $deliverables = $this->_searchDeliverables();
         $contracts    = $this->_searchContract();
         return array("headers" => $this->contractHeaders, "contracts" => $contracts, "total" => count($contracts));
    }

    function advancedSearch( $data )
    {
    	$query = "";
		foreach( $data as $index => $nameVal){
			if($nameVal['value'] !== ""){
				$query .= "C.".$nameVal['name']." LIKE '".$nameVal['value']."' AND ";								
			}			
		}
		$query = ($query == "" ? "" : " AND ".$query);
		$query = rtrim($query, "AND ");
			
        $sql = "SELECT
                DISTINCT(C.id) AS ref ,
                C.contractdefaults,
                C.name AS contract ,
                C.budget ,
                C.comments AS project_comments,
                C.signnature_of_tender ,
                C.signature_of_sla,
                C.completion_date,
                C.assessment_frequency,
                C.notification_frequency,
                C.status,
                C.type,
                C.supplier,
                C.category,
                C.contractdefaults,
                C.contract_owner AS owner,
                C.contract_manager,
                C.completion_date AS deadline,
                CT.name AS contract_type,
                CC.name AS contract_category,
                S.name AS contract_supplier,
                CONCAT(TK.tkname, ' ', TK.tksurname) AS contract_owner,
                CS.name AS contract_status,
                AF.name AS contract_assessment_frequency,
                NR.description AS contract_notification_rules
                FROM #_contract C
                INNER JOIN #_contract_type CT ON CT.id = C.type
                INNER JOIN #_contract_categories CC ON CC.id = C.category
                INNER JOIN #_supplier S ON S.id = C.supplier
                INNER JOIN #_contract_status CS ON CS.id = C.status
                LEFT JOIN #_notification_rules NR ON NR.id = C.notification_frequency
                LEFT JOIN #_assessment_frequency AF ON AF.id = C.assessment_frequency
                INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner
                WHERE C.status & 1 = 1 $query
                ";
        $result 	= $this->get( $sql );
        $response   = $this->prepareContractHeaders($result);	
        return array("headers" => $this->contractHeaders, "contracts" => $response, "total" => count($response));
    }
    
    
    function _searchActions()
    {

        $sql = "SELECT 
                DISTINCT(A.id) AS ref,
                A.name AS action,
                A.measurable,
                A.progress,
                A.deadline,
                A.status,
                A.owner,
                A.remind_on,
                AST.name AS action_status,
                A.actionstatus,
                CONCAT(TK.tkname,' ', TK.tksurname) AS action_owner
                FROM #_action A
                INNER JOIN #_action_status AST ON AST.id = A.status
                LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.owner
                WHERE A.name LIKE '%".$this->searchText."%'
                OR A.measurable	 LIKE '%".$this->searchText."%'
                OR A.progress LIKE '%".$this->searchText."%'
                OR AST.client_terminology LIKE '%".$this->searchText."%'
                ";
        $result = $this->get( $sql );
        return $result;
    }

    function _searchDeliverables()
    {
        $sql = "SELECT
                DISTINCT(D.id) AS ref,
                D.name AS deliverable ,
                C.name AS project_name,
                D.description AS deliverable_description,
                D.type ,
                D.category,
                D.owner,
                D.remind_on,
                D.status AS status,
                DS.name AS deliverable_status,
                CT.name AS contract_types,
                DD.name as main_deliverable,
                DD.id AS maindeliverableid,
                DC.name AS deliverable_category,
                CONCAT(TK.tkname, ' ', TK.tksurname) as deliverable_owner	,
                DW.description as descr_weight,
                DQ.description as descr_quality_weight,
                OW.description as descr_other_weight ,
                DW.dweight as delivered_weight,
                DQ.qweight as quality_weight,
                OW.oweight as other_weight,
                D.weight AS delivered_weightId,
                D.quality_weight AS quality_weightId,
                D.other_weight AS other_weightId,
                D.deadline,
                D.delstatus
                FROM #_deliverable D
                INNER JOIN #_contract C ON C.id = D.contract_id
                INNER JOIN #_contract_type CT ON CT.id = C.type
                INNER JOIN #_deliverable_categories DC ON DC.id = D.category
                LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
                LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
                LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
                LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
                LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable
                LEFT JOIN #_deliverable_status DS ON DS.id = D.status
                WHERE D.name LIKE '%".$this->searchText."%'
                OR D.description LIKE '%".$this->searchText."%'
                OR DC.name LIKE '%".$this->searchText."%'
                OR DC.description LIKE '%".$this->searchText."%'
                OR DW.description LIKE '%".$this->searchText."%'
                OR DW.definition LIKE '%".$this->searchText."%'
                OR OW.description LIKE '%".$this->searchText."%'
                OR OW.definition LIKE '%".$this->searchText."%'
                OR DQ.description LIKE '%".$this->searchText."%'
                OR DQ.definition LIKE '%".$this->searchText."%'

              ";
        $result = $this->get( $sql );
        return $result;
    }

    function _searchContract()
    {
        $sql = "SELECT
                DISTINCT(C.id) AS ref ,
                C.contractdefaults,
                C.name AS contract ,
                C.budget ,
                C.comments AS project_comments,
                C.signnature_of_tender ,
                C.signature_of_sla,
                C.completion_date,
                C.assessment_frequency,
                C.notification_frequency,
                C.status,
                C.type,
                C.supplier,
                C.category,
                C.contractdefaults,
                C.contract_owner AS owner,
                C.contract_manager,
                C.completion_date AS deadline,
                CT.name AS contract_type,
                CC.name AS contract_category,
                S.name AS contract_supplier,
                CONCAT(TK.tkname, ' ', TK.tksurname) AS contract_owner,
                CS.name AS contract_status,
                AF.name AS contract_assessment_frequency,
                NR.description AS contract_notification_rules
                FROM #_contract C
                INNER JOIN #_contract_type CT ON CT.id = C.type
                INNER JOIN #_contract_categories CC ON CC.id = C.category
                INNER JOIN #_supplier S ON S.id = C.supplier
                INNER JOIN #_contract_status CS ON CS.id = C.status
                LEFT JOIN #_notification_rules NR ON NR.id = C.notification_frequency
                LEFT JOIN #_assessment_frequency AF ON AF.id = C.assessment_frequency
                INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner
                WHERE C.name LIKE '%".$this->searchText."%'
                OR C.budget LIKE '%".$this->searchText."%'
                OR C.comments LIKE '%".$this->searchText."%'
                OR CT.shortcode LIKE '%".$this->searchText."%'
                OR CT.name LIKE '%".$this->searchText."%'
                OR CT.description LIKE '%".$this->searchText."%'
                OR CC.name LIKE '%".$this->searchText."%'
                OR CC.description LIKE '%".$this->searchText."%'
                OR CC.shortcode LIKE '%".$this->searchText."%'
                OR CS.name LIKE '%".$this->searchText."%'
                OR CS.client_terminology LIKE '%".$this->searchText."%'
                OR S.shortcode LIKE '%".$this->searchText."%'
                OR S.name LIKE '%".$this->searchText."%'
                OR S.supplier_code LIKE '%".$this->searchText."%'
                ";
        $result = $this->get( $sql );
        return $this->prepareContractHeaders($result);
    }

    function prepareContractHeaders( $contr )
    {
        $naming  = new Naming();
        $headers = $naming -> getHeader();
        $contractArray = array();
		foreach( $contr as $key => $ctrl )
		{
		  foreach( $headers as $field => $val)
			{
				if( $field == "attachments"){
					$contractArray[$field]	                     = $val;
					$this->contractHeaders[$ctrl['ref']][$field] = $attachements;
				}  else if( isset( $ctrl[$field] ) ) {
					$this->contractHeaders[$field]	     = $val;
					$contractArray[$ctrl['ref']][$field] = $ctrl[$field];
				}
			}
		}
        return $contractArray;
    }

   function prepareDeliverables()
   {
        $naming      = new Naming();
		$headers	 = $naming -> getHeader();
		$array       = array();
		$delArray 	 = array();
		$headArray   = array();
		foreach( $headers as $field => $val)
		{
			 if($field == "delivered_weight"){
					if(($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED){
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;
					}
			} else if($field == "quality_weight"){
					if(($contract['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY){
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;
					}
			} else if($field == "other_weight"){
					if(($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER){
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;
					}
			} else if($field == "main_deliverable"){

				$delArray[$value['ref']]["deliverable_type"] = ($value[$field] == "" ? "Main" : "Sub")." deliverable";
				$this->delHeaderArray["deliverable_type"]    = "Deliverable Type";

				$delArray[$value['ref']]['number_of_actions'] = ($value['number_of_actions'] == "" ? "<span class='error'>&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;</span>" : "<span class='success'>&nbsp;&nbsp;&nbsp;&nbsp;".$value['number_of_actions']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");

				$this->delHeaderArray['action_progress']	 = "Action Progress";
				$delArray[$value['ref']]['action_progress']  = ($value['action_progress'] == ""  ? "0" : $value['action_progress']);

				$this->delHeaderArray["number_of_actions"]	 = "Number of actions";
			}  else if( isset($value[$field]) || array_key_exists($field, $value)){
				$delArray[$value['ref']][$field] = ($value[$field] == "" ? "" : $value[$field]);
				$this->delHeaderArray[$field]	 = $val;
			}
		}

		$this->delHeaderArray = $this->sortHeadeDisplay( $this->delHeaderArray ,  $requestPage);
		$delArray = $this->sortDisplay( $delArray , $requestPage);
		return $delArray;
   }

    function prepareAction( $contr )
    {
        $naming      = new Naming();
		$headers	 = $naming -> getHeader();
		$orderedHeaders = array();
		foreach( $contr as $key => $ctrl )
		{
		  foreach( $headers as $field => $val)
			{
				if( $field == "attachments"){
					$this->actionHeaders[$field]	   = $val;
				} else if( isset( $ctrl[$field] ) ) {
					$this->actionHeaders[$field]	   = $val;
					$orderedHeaders[$ctrl['ref']][$field] = $ctrl[$field];
				}
			}
		}
		return $orderedHeaders;
    }

}
