<?php
class ActionStatus extends DBConnect
{
	
	
	function __construct()
	{
		parent::__construct();
	}

	function getActionStatuses()
	{
		$response = $this -> get("SELECT * FROM #_action_status WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."");
		return $response;
	}

	function save( $data )
	{
		$response = $this -> insert("action_status", $data );
		return $this -> insertedId() ;
	}

	function updateActionStatus( $id, $data)
	{
    	//$data    = $this->getActionStatus( $id );
        $logsObj = new Logs( $_POST , $data, "action_status");
        $logsObj -> createLog();			
		$response = $this -> update("action_status", $data, "id=$id");
		return $response; 
	}
	
	function getActionStatus( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_action_status WHERE id = $id");
		return $response;
	}
	
	function changeStatus( $id, $tostatus )
	{
    	$data    = $this->getActionStatus( $id );
        $logsObj = new Logs( array("status" => $tostatus) , $data, "action_status");
        $logsObj -> createLog();			
		$response = $this -> update("action_status", array("status" => $tostatus), "id=$id");
		return $response; 		
	}

}
?>