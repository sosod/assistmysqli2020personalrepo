<?php
/**
	@author : admire
**/
class Naming extends DBConnect
{
	/**
	 @var int
	**/
	protected $id;
	/**
	 @var char
	**/
	protected $name;
	/**
	 @var char
	**/
	protected $sla_terminology;
	/**
	 @var char
	**/
	protected $client_terminology;
	/**
	 @var int
	**/
	protected $insertuser;
	/**
	 @var int
	**/
	protected $position;
	/**
	 @var int
	**/
	protected $insertdate;
	
	protected $headerArray;
	
	
	function __construct()
	{
		parent::__construct();
	}

	function getNaming()
	{
		$response = $this -> get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
		$this->headerArray = $response;
		return $response;
	}
	
	function getHeaderNames()
	{
		$response = $this -> get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
		$this->headerArray = $response;
		return $response;
	}	

	function getName( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_naming WHERE id = $id");
		return $response;
	}
	
	
	function saveNaming( $id, $name)
	{
 		$data    = $this->getName( $id );
        $logsObj = new Logs( array( "client_terminology" => $name ) , $data, "naming");
        $logsObj -> createLog();			
		$response = $this -> update("naming", array( "client_terminology" => $name ), "id=$id");
		return $response;
	}
	
	function getHeader()
	{
		$response = $this -> get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
		$headers  = array(); 
		foreach( $response as $key => $val)
		{
			$headers[ $val['name'] ] = ($val['client_terminology'] == "" ? $val['sla_terminology'] : $val['client_terminology'] ); 
		}
		return $headers;	
	}
	
	
	
	function setHeader( $key )
	{
		$headerName = "";
		foreach($this->headerArray as $index => $value){
			if( $key == $value['name'] ){
				$headerName = (isset($value['client_terminology']) ? $value['sla_terminology'] : "");
			} else {
				$headerName = ucwords( str_replace("_", " ", $key));
			}
		}
		return $headerName;
	}	
	
	function saveColumnsOrder( $activeColumns, $inactiveColumns) {	
		$activeArray = array();
		$resInAct	 = 0;
		$resAct		 = 0;		
		foreach( $activeColumns as $key => $id ) {
			$resAct = $this -> update( "naming", array( 
															"active"	 	=> 1 ,
															 "insertuser"   => $_SESSION['tid'],
															) ,"id = $id"); 
		}
		
		foreach( $inactiveColumns as $key => $id ) {
			$resInAct = $this -> update( "naming", array( 
																"active" 		=> 0,
																"insertuser"    => $_SESSION['tid'], 
																) ,"id = $id"); 
		}
		
	}

	public function setName( $key )
	{
		$header = $this->getHeader();
		if( isset($header[$key])){
			echo $header[$key];
		} else {
			echo ucwords( str_replace("_", " ", $key));
		}
	}
	
	
}
?>