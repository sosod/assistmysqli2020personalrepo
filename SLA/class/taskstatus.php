<?php
class TaskStatus extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getTaskStatuses()
	{
		$response = $this -> get("SELECT * FROM #_task_status WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." ");
		return $response;
	}
	
	function getTaskStatus( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_task_status WHERE id = $id");
		return $response;
	}

	function save( $data )
	{
		$response = $this -> insert( "task_status", $data );
		return $response;
		
	}
	
	function updateStatus(  $id, $data)
	{
		$dat    = $this->getTaskStatus( $id );
        $logsObj = new Logs($data , $dat, "task_status");
        $logsObj -> createLog();			
		$response = $this -> update("task_status", $data, "id=$id");
		return $response;
	}
	
	function changeStatus( $id, $tostatus)
	{
		$dat     = $this->getTaskStatus( $id );
        $logsObj = new Logs(array("status" => $tostatus) , $dat, "task_status");
        $logsObj -> createLog();			
		$response = $this -> update("task_status", array("status" => $tostatus), "id=$id");
		return $response;
	}
	
	
}
?>