<?php
/**
	This file updates and creates menu items 
	* @uthor : admire
**/
class Menu extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $parent_id;
	/**
		@var char
	**/
	protected $name;
	/**
		@var char
	**/
	protected $client_name;
	/**
		@var char
	**/
	protected $description;
	/**
		@var char
	**/
	protected $folder;
	/**
		@var int
	**/
	protected $position;
	/**
		@var date
	**/
	protected $insertdate;
	/**
		@var char
	**/
	protected $insertuser;
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function getSubMenu( $name )
	{
		$response = $this -> get("SELECT * FROM assist_assist1_sla_menu WHERE folder = '".$name."' AND parent_id <> 0 ");
		return $response;
	}
	
	function getMenuItems()
	{
		$response = $this -> get("SELECT * FROM #_menu ");
		return $response;
	}

	function getMenu( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_menu WHERE id = $id ");
		return $response;
	}
	

	function saveMenu( $id, $name)
	{
    	$data    = $this->getMenu( $id );
        $logsObj = new Logs( array("client_name" => $name ) , $data, "menu");
        $logsObj -> createLog();			
		$response = $this -> update( "menu" , array("client_name" => $name ), "id=$id");
		return $response;
	}
	
}
?>