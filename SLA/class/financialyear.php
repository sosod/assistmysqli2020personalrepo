<?php
class FinancialYear extends DBConnect
{
	/*
		@var int
	*/
	protected $id;
	/*
		@var char
	*/
	protected $lastday;
	/*
		@var date
	*/
	protected $startdate;
	/*
		@var char
	*/
	protected $enddate;
	/*
		@var int
	*/
	protected $active;
	/*
		@var int
	*/
	protected $insertdate;
	/*
		@var int
	*/
	protected $insertuser;
	
	function __construct()
	{
		parent::__construct();
	}
	//get all the financial years
	function getFinYears()
	{
		$response = $this -> get("SELECT * FROM #_financial_years WHERE active = 1");
		return $response;
	}
	//get a single financial year
	function getFinYear( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_financial_years WHERE id = $id");
		
		return $response;
	}
	//save a new financial yeat
	function save( $data )
	{
		$res = $this -> insert("financial_years", $data );
		return $this -> insertedId();
	}
	//update the financial year
	function updateFinyear( $id, $data)
	{
		$dat    = $this->getFinYear( $id );
        $logsObj = new Logs( $_POST , $dat, "financial_years");
        $logsObj -> createLog();		
		$response = $this -> update("financial_years", $data, "id=$id");
		return $response;
	}
	//activate or delete or deactivate a financial year
	function updateStatus( $id, $status )
	{
		$dat    = $this->getFinYear( $id );
        $logsObj = new Logs(array("active" => $status) , $dat, "financial_years");
        $logsObj -> createLog();			
		$response = $this -> update("financial_years", array("active" => $status), "id=$id");
		return $response;
	}
	
}
?>