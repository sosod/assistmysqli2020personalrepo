<?php
/**
	// This class defines the mothod to smanage the score and weight , it can be extended
**/
class ScoreWeight extends DBConnect
{
	
	protected $id;
	protected $from;
	protected $to;
	protected $description;
	protected $definition;
	protected $color;
	protected $active;
	protected $insertdate;
	protected $insertuser;
	protected $table;
	
	function __construct()
	{
		parent::__construct();
	}
	
	function save( $data ) 
	{
		$this -> insert( $this -> table , $data);
		return $this -> insertedId();
	}	
	
	function getAScoreWeight( $id )
	{
		return  $this->getRow("SELECT * FROM #_".$this->table." WHERE id = $id");
				
	}
	
	function updateData( $id, $data)
	{
		$dat    = $this->getAScoreWeight( $id );
        $logsObj = new Logs( $_POST , $dat, $this -> table);
        $logsObj -> createLog();		
		$res = $this -> update( $this -> table, $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus)
	{
		$dat    = $this->getAScoreWeight( $id );
        $logsObj = new Logs( array("status" => $tostatus) , $dat, $this -> table);
        $logsObj -> createLog();		
		$res = $this -> update( $this -> table, array("status" => $tostatus), "id=$id");
		return $res;
	}
	
	
}
?>