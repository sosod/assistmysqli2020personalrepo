<?php
class ContractCategory extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getCategories()
	{
		$response = $this -> get("SELECT CC.id, CC.name, CC.description, CC.status, CC.defaults, CT.name as type
								  FROM #_contract_categories  CC 
								  INNER JOIN #_contract_type CT ON CC.type = CT.id 
								  ");
		return $response;
	}
	
	function getCategory( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_contract_categories WHERE id = $id");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("contract_categories", $data);
		return $this -> insertedId();
	}
	
	function updateCategory( $id, $data )
	{
		$res = $this -> update("contract_categories", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$res = $this -> update("contract_categories", array("active" => $tostatus), "id=$id");
		return $res;
	}
	
}
?>