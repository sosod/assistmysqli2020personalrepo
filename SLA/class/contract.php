<?php
/**
	This file manages , creates the contracts
	* @author 	: admire
	
**/
class Contract extends DBConnect
{
	/*
		@var int
	*/	
	protected $id;
	/*
		@var char
	*/	
	protected $name;	
	/*
		@var double
	*/	
	protected $budget;
	/*
		@var char
	*/	
	protected $comments;
	/*
		@var int
	*/	
	protected $type;
	/*
		@var int
	*/	
	protected $category;
	/*
		@var int
	*/	
	protected $supplier;
	/*
		@var date
	*/	
	protected $signnature_of_tender;
	/*
		@var date
	*/	
	protected $signature_of_sla;
	/*
		@var date
	*/	
	protected $completion_date;
	/*
		@var char
	*/	
	protected $responsible_person;
	/*
		@var char
	*/	
	protected $template;
	/*
		@var int
	*/	
	protected $is_template;
	/*
		@var int
	*/	
	protected $assessment_frequency;
	/*
		@var int
	*/	
	protected $assessment_frequency_date;
	/*
		@var date
	*/	
	protected $assessment_frequency_time;
	/*
		@var int
	*/	
	protected $notification_frequency;
	/*
		@var char
	*/	
	protected $attachments;
	/*
		@var char
	*/	
	protected $insert_user;
	/*
		@var date
	*/	
	protected $insertdate;

	protected $contractdefaults;
	
	const HAS_DELIVERABLES 		= 1;
	const HAS_SUBDELIVERABLES   = 2;
	const WEIGHTS_ASSIGNED		= 4;
	const DELIVERED				= 8;
	const QUALITY 				= 16;
	const OTHER 				= 32;
	const TEMPLATE				= 64;
	const LOCKED 				= 128;
	const APPROVED				= 256;
	const CONTRACT_DELETED		= 512;

	function __construct()
	{
		parent::__construct();
	}
	
	function getTemplates()
	{
		$response = $this -> get("SELECT 
								  C.id,
								  CT.name 
								  FROM #_contract C
								  INNER JOIN #_contract_template CT ON CT.contract_id = C.id
								  WHERE contractdefaults & ".Contract::TEMPLATE." = ".Contract::TEMPLATE."
								  ");
		return $response;
	}
	
	function save( $insertdata )
	{
		$response = $this -> insert("contract", $insertdata );
		return $this -> insertedId();
	}
	
	function updateContractDefaults(  $id, $updatedata)
	{
		$result = $this->update("contract", $updatedata , "id=$id");
		return $result;	
	}	
	
	function updateContract( $id, $updatedata, $insertdata )
	{	
		$res 	= "";
		$result = $this -> insert("contract_update", $insertdata );
		if(isset($updatedata) && !empty($updatedata)){
			$res    = $this -> update("contract", $updatedata  ,"id=$id");
		}
		if($result > 0 && $res == 1){
			return 1;	
		} else if( $result > 0 && $res != 1){
			return 2;
		} else {
			return -1;
		}
	}
	
	function editContract( $id ,$insertdata , $updatedata)
	{
		$result = $this -> insert("contract_edit", $insertdata );
		$res 	= "";
		if(isset($updatedata)){
			$res = $this -> update("contract", $updatedata ,"id=$id");
		}
		if($result > 0 && $res == 1){
			return 1;	
		} else if( $result > 0 && $res != 1){
			return 2;
		} else {
			return -1;
		}
	}	
	
	function getAContract( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_contract WHERE id=$id ");
		return $response;	
	}
	
	function getContract( $id )
	{					
		$result = $this -> getRow("SELECT 
									C.id ,
									C.contractdefaults,
									C.name AS contract , 
									C.budget ,
									C.comments AS project_comments,
									C.signnature_of_tender ,
									C.signature_of_sla,
									C.completion_date,
									C.assessment_frequency,
									C.notification_frequency,	
									C.status,								
									C.type,
									C.supplier,	
									C.category,	
									C.contractdefaults,			
									C.contract_owner AS owner,
									C.contract_manager,
									C.template,
									C.attachments,
									C.deliverable_status,
									C.completion_date AS deadline,
									(SELECT CC.name FROM #_contract CC WHERE CC.id = C.template) AS template_name,
									CT.name AS contract_type, 
									CC.name AS contract_category,
									S.name AS contract_supplier,					
									CONCAT(TK.tkname, ' ', TK.tksurname) AS contract_owner,
									CS.name AS contract_status,
									AF.name AS contract_assessment_frequency,
									NR.description AS contract_notification_rules,
									CU.remindon
									FROM #_contract C  
									INNER JOIN #_contract_type CT ON CT.id = C.type 
									LEFT JOIN #_contract_categories CC ON CC.id = C.category
									INNER JOIN #_supplier S ON S.id = C.supplier
									LEFT JOIN #_contract_update CU ON CU.contract_id = C.id	
									INNER JOIN #_contract_status CS ON CS.id = C.status	
									LEFT JOIN #_notification_rules NR ON NR.id = C.notification_frequency
									LEFT JOIN #_assessment_frequency AF ON AF.id = C.assessment_frequency	
									LEFT JOIN #_contract CCT ON  C.id = CCT.template
									INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner 
									WHERE C.id = $id
				");
		return $result;	
	}

	function getContracts( $start, $limit,  $conditions)
	{
		$query = "";
		if( $conditions == ""){
			$query = "AND contractdefaults & ".Contract::LOCKED." <> ".Contract::LOCKED."";			
		} else {
			$query = $conditions;			
		}
		//create the limit and start 
		$pager = "";
		if( $start == "" || $limit == ""){
			$pager = "";
		} else {
			$pager =  "LIMIT $start, $limit";
		}
		
		$query .= " AND C.contractdefaults & ".Contract::CONTRACT_DELETED." <> ".Contract::CONTRACT_DELETED."";
		
		$sql = "SELECT 
				C.id AS ref,
				C.name AS contract , 
				C.budget AS project_value ,
				C.comments AS project_comments ,
				C.signnature_of_tender AS tender_doc_signature_date,
				C.signature_of_sla AS sla_doc_signature_date,
				C.completion_date AS contract_completion_date,
				C.attachments,
				C.deliverable_status,	
				C.contract_owner AS owner,	
				C.contract_manager,		
				CS.name AS status,
				S.name AS supplier,
				CT.name AS contract_type, 
				CC.name AS category,
				CONCAT(TK.tkname, ' ', TK.tksurname) AS contract_owner 
				FROM #_contract C 
				INNER JOIN #_contract_type CT ON CT.id = C.type 
				LEFT JOIN #_contract_categories CC ON CC.id = C.category
				LEFT JOIN #_supplier S	ON  S.id = C.supplier
				INNER JOIN #_contract_status CS ON CS.id = C.status
				INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner
				$query
				$pager
				";
		$response = $this -> get( $sql );
		return $response;
	}
	
	function getContractUpdates( $id )
	{
		$sql = "SELECT 
				C.changes,								
				C.insertdate AS date,
				CS.name AS status 
				FROM #_contract_update C 
				INNER JOIN #_contract CC ON CC.id = C.contract_id 
				INNER JOIN #_contract_status CS ON CS.id = CC.status		
				WHERE C.contract_id = $id
				ORDER BY C.insertdate DESC
				";
		$response = $this -> get( $sql );
		return $response;
	}
	
	function getContractDelAction( $id )
	{
		$sql = "SELECT 
				C.id as contract_id ,
				D.name AS deliverable,
				CONCAT(TK.tkname, ' ', TK.tksurname) AS owner,
				D.deadline,
				D.weight,
				D.quality_weight,
				D.other_weight,	
				A.name AS action
			 	FROM  #_contract C 
				LEFT JOIN #_deliverable D ON C.id = D.contract_id
				LEFT JOIN #_action A ON D.id = A.deliverable_id 
				INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
				WHERE C.id = $id
				";
		$result = $this -> get($sql);
		return $result;
	}
	
	function getContractTemplate( $id )
	{
		$result = $this->getRow("SELECT 
								  C.id,
								  C.name,
								  C.data
								  FROM #_contract_template C
								  WHERE C.contract_id = $id
								  ");
		return $result;
	}
	
	function getContractEdits( $id )
	{
		$sql = "SELECT 
				C.changes,								
				C.insertdate AS date,
				CS.name AS status 
				FROM #_contract_edit C 
				INNER JOIN #_contract CC ON CC.id = C.contract_id 
				INNER JOIN #_contract_status CS ON CS.id = CC.status		
				WHERE C.contract_id = $id
				ORDER BY C.insertdate DESC
				";
		$response = $this -> get( $sql );
		return $response;
	}	
	
	function getAuthorization( $id )
	{
		$response = $this -> get("SELECT 
								  CA.description,
								  CA.approval,
								  CA.insertdate,
								  CONCAT(TK.tkname,' ',TK.tksurname) AS changeBy,
								  CS.name AS status
								  FROM #_contract_authorization CA
								  INNER JOIN #_contract C ON C.id = CA.contract_id
								  INNER JOIN #_contract_status CS ON CS.id = C.status
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = CA.insertuser	
								  WHERE CA.contract_id = $id							  
								  ORDER BY CA.insertdate DESC								  
								  ");
		return $response;
	}
	
	function getLastAssesedDate( $delIds )
	{
		$response = $this->getRow("SELECT insertdate 
								   FROM #_assessment 
								   WHERE deliverable_id 
								   IN(".implode(",", $delIds).") 
								   ORDER BY insertdate DESC"
								   );
		return $response;
	}

	function getTotalContract( $conditions  )
	{
		$query = "";
		if( $conditions == ""){
			$query = "AND contractdefaults & ".Contract::LOCKED." <> ".Contract::LOCKED."";			
		} else {
			$query = $conditions;			
		}
		$sql = "SELECT COUNT(*) AS total
				FROM #_contract C 
				INNER JOIN #_contract_type CT ON CT.id = C.type 
				LEFT JOIN #_contract_categories CC ON CC.id = C.category
				LEFT JOIN #_supplier S	ON  S.id = C.supplier
				INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner
				WHERE 1 $query 
				";	
		$total = $this -> getRow( $sql );
		return $total['total'];		
	}
		
	function saveTemplate( $insertdata ){
		$response = $this -> insert("contract_template", $insertdata );
		return $this -> insertedId();
	}
	
	function contractAuthorization( $insertdata ){
		$response = $this -> insert("contract_authorization", $insertdata );
		return $this -> insertedId();
	}	

}
?>