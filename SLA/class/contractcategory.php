<?php
class ContractCategory extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getCategories()
	{
		$response = $this -> get("SELECT CC.id, CC.name,  CC.shortcode, CC.description, CC.status
								  FROM #_contract_categories  CC 
								  WHERE CC.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
								   ");
		return $response;
	}
	
	function getCategory( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_contract_categories WHERE id = $id");
		return $response;
	}
	
	function getActiveCategory( $type )
	{
		$response = $this -> get("SELECT * FROM #_contract_categories WHERE type = $type AND active =1");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("contract_categories", $data);
		return $this -> insertedId();
	}
	
	function updateCategory( $id, $data )
	{
		$dat    = $this->getCategory( $id );
        $logsObj = new Logs($_POST , $dat, "contract_categories");
        $logsObj -> createLog();		
		$res = $this -> update("contract_categories", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$dat    = $this->getCategory( $id );
        $logsObj = new Logs(array("status" => $tostatus) , $dat, "contract_categories");
        $logsObj -> createLog();			
		$res = $this -> update("contract_categories", array("status" => $tostatus), "id=$id");
		return $res;
	}
	
}
?>