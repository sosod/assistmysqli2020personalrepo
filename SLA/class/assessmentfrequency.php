<?php
class AssessmentFrequency extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getAssessmentFrequencies()
	{
		$response = $this -> get("SELECT * FROM #_assessment_frequency WHERE active <> 2 ");
		return $response;
	}
	
	function getAssessmentFrequency( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_assessment_frequency WHERE id = $id");
		return $response;
	}
	
	function getActiveAssessmentFrequencies()
	{
		$response = $this -> get("SELECT * FROM #_assessment_frequency WHERE active = 1 ");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("assessment_frequency", $data);
		return $this -> insertedId();
	}
	
	function updateAssessmentFrequency( $id, $data )
	{
		$dat    = $this->getAssessmentFrequency( $id );
        $logsObj = new Logs( $_POST , $dat, "assessment_frequency");
        $logsObj -> createLog();				
		$res = $this -> update("assessment_frequency", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$dat    = $this->getAssessmentFrequency( $id );
        $logsObj = new Logs(  array("active" => $tostatus), $dat, "assessment_frequency");
        $logsObj -> createLog();		
		$res = $this -> update("assessment_frequency", array("active" => $tostatus), "id=$id");
		return $res;
	}
	
}
?>