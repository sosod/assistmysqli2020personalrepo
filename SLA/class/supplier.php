<?php
class Supplier extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getSuppliers()
	{
		$response = $this -> get("SELECT S.id, S.name , S.shortcode, S.status, S.supplier_code, SC.name as category 
								  FROM #_supplier S INNER JOIN #_supplier_category SC ON S.category = SC.id
								  WHERE S.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." ");
		return $response;
	}
	
	function getSupply( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_supplier WHERE id = $id");
		return $response;
	}
	
	function getActiveSupplier()
	{
		$response = $this -> get("SELECT * FROM #_supplier WHERE status = 1");
		return $response;
		
	}
	
	function save( $data )
	{
		$response = $this -> insert("supplier", $data);
		return $this -> insertedId();
	}
	
	function updateSupplier( $id, $data )
	{
		$dat    = $this->getSupply( $id );
        $logsObj = new Logs( $_POST , $dat, "supplier");
        $logsObj -> createLog();		
		$res = $this -> update("supplier", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$dat    = $this->getSupply( $id );
        $logsObj = new Logs(  array("status" => $tostatus) , $dat, "supplier");
        $logsObj -> createLog();			
		$res = $this -> update("supplier", array("status" => $tostatus), "id=$id");
		return $res;
	}
	
}
?>