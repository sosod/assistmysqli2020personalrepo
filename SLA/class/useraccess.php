 <?php
class UserAccess extends DBConnect
{
	//defining user access constants
	const MODULE_ADMIN 		 = 1;//1000000000	
	const CONTRACT_MANAGER   = 2;   //00000001
	const CREATE_CONTRACT    = 4;   //00000010
	const CREATE_DELIVERABLE = 8;   //00000100
	const APPROVAL			 = 16;  //00001000
	const VIEW_ALL		     = 32;	//00010000		 
	const EDIT_ALL			 = 64;  //00100000
	const REPORTS			 = 128; //01000000
	const ASSURANCE 		 = 256; //10000000
	const SETUP		 		 = 512; //100000000	
	const DELETED 		 	 = 2048;//1000000000	
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getUsers()
	{

		$response = $this -> get("SELECT 
								   TK.tkid, 
								   TK.tkname,
								   TK.tksurname,
								   TK.tkstatus ,
								   TK.tkemail ,
								   UA.status
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   LEFT JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user_id = TK.tkid
								   WHERE tkid <> 0000 AND tkstatus = 1 
								   AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname , TK.tksurname
								   ");
		return $response;
	}
		
	function getUserAccessSettings()
	{
		$response = $this -> get("SELECT UA.id,
								  UA.status, 
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								  FROM #_useraccess UA 
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								  WHERE status & ".UserAccess::DELETED." <> ".UserAccess::DELETED."");
		return $response;
	}
	
	function getUser( $userId ){
		$response = $this -> getRow("SELECT 
									  CONCAT(TK.tkname, ' ', TK.tksurname) AS user,
									  TK.tkemail as email
									  FROM 
									  assist_".$_SESSION['cc']."_timekeep TK 
									  WHERE tkid  = '".$userId."' ");	
		return $response;	
	}
	
	function getUserAccessSetting( $id )
	{
		$response = $this -> getRow("SELECT 
								  UA.id,
								  UA.status, 
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								  FROM #_useraccess UA 
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								  WHERE id = $id ");	
		return $response;	
	}
	
	function getUserAccess()
	{
		$response = $this -> get("SELECT * FROM #_useraccess WHERE user_id = '".$_SESSION['tid']."' ");
		return $response;
	}
	
	function getByUserId()
	{
		$response = $this -> getRow("SELECT * FROM #_useraccess WHERE user_id = '".$_SESSION['tid']."'");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("useraccess", $data);
		return $this -> insertedId();
	}
	
	function updateUserAccess( $id, $data)
	{
		$response = $this -> update("useraccess", $data, "id=$id");
		return $response;
	}
	
	function unsedUserAccess()
	{

		$users = array();
		$users = $this->getUsers();
		$usedUsers = $this -> get("SELECT * FROM #_useraccess");
		$usedList = array();
        foreach($usedUsers as $k => $usedU)
        {
            $usedList[$usedU['user_id']] = $usedU['user_id'];
        }
		$unUsedUsers = array();
        foreach($users as $index => $user)
        {
            if(!isset($usedList[$user['tkid']]))
            {
              $unUsedUsers[$user['tkid']] = $user;
            }
        }        
		return $unUsedUsers;	
	}
}
?>
