<?php
/**
	@author 	: admire
**/
class Deliverable extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $contract_id;
	/**
		@var int
	**/
	protected $name;
	/**
		@var char
	**/
	protected $description;
	/**
		@var int
	**/
	protected $type;
	/**
		@var int
	**/
	protected $main_deliverable;
	/**
		@var char
	**/
	protected $owner;
	/**
		@var int
	**/
	protected $weight;
	/**
		@var int
	**/
	protected $quality_weight;
	/**
		@var int
	**/
	protected $other_weight;
	/**
		@var date
	**/
	protected $deadline;
	/**
		@var date
	**/
	protected $remind_on;
	/**
		@var char
	**/
	protected $insertuser;
	/**
		@var date
	**/
	protected $insertdate;
	/**
		@var int
	**/
	protected $active;
	
	const MAIN_DELIVERABLE	= 2;
	const MAIN_NO_SUB 		= 4;
	const MAIN_WITH_SUB 	= 8;
	
	function __constrcuct()
	{
		parent::__construct();
	}

	function getDeliverables( $id, $type = "" )
	{	
		
		$and = "";
		if( $id == ""){
			$and = "";
		} else {
			$and = "AND  D.contract_id = $id";
		}

		$deliverableType = "";
		if( $type == "main"){
			$deliverableType = "AND D.type = 0 ";		
		} else if($type == "sub") {
			$and = "";
			$deliverableType = "AND D.main_deliverable = $id AND D.type = 1";		
		} else if( $type == "mainSub") {
			$deliverableType = "AND D.delstatus & ".Deliverable::MAIN_WITH_SUB." = ".Deliverable::MAIN_WITH_SUB." AND D.type = 0";
		} else {
			$deliverableType = "";
		}
		$response = $this -> get("SELECT 
								  D.id AS ref,
								  D.name AS deliverable , 
								  C.name AS project_name,
								  D.description AS deliverable_description,
								  D.type ,
								  D.category,
								  D.owner,
								  D.remind_on,
								  D.status AS status,
								  DS.name AS deliverable_status,	
								  CT.name AS contract_types,
								  DD.name as main_deliverable,								  
								  DD.id AS maindeliverableid,
								  DC.name AS deliverable_category,
								  CONCAT(TK.tkname, ' ', TK.tksurname) as deliverable_owner	,
							  	  DW.description as descr_weight,
								  DQ.description as descr_quality_weight,
								  OW.description as descr_other_weight ,
								  DW.dweight as delivered_weight,
								  DQ.qweight as quality_weight,
								  OW.oweight as other_weight,
								  D.weight AS delivered_weightId,
								  D.quality_weight AS quality_weightId,								  
								  D.other_weight AS other_weightId,								  								  
								  D.deadline,
								  D.delstatus,
								  A.assessment
								  FROM #_deliverable D
								  INNER JOIN #_contract C ON C.id = D.contract_id
								  INNER JOIN #_contract_type CT ON CT.id = C.type
								  LEFT JOIN #_deliverable_categories DC ON DC.id = D.category
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
								  LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
								  LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
								  LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
								  LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable
								  LEFT JOIN #_assessment A ON A.deliverable_id = D.id
								  LEFT JOIN #_deliverable_status DS ON DS.id = D.status
								  WHERE D.delstatus & ".DBConnect::DELETE." <> ".DBConnect::DELETE."  
								  $and
								  $deliverableType
								  ");	
					  
		return array( "deliverable" => $response, "total" => $this->getTotalDeliverables( $and, $deliverableType) );
	}
	
	function getTotalDeliverables( $and , $deliverableType )
	{
		$result = $this->getRow("SELECT COUNT(*) AS total				  
								  FROM #_deliverable D
								  INNER JOIN #_contract C ON C.id = D.contract_id
								  INNER JOIN #_contract_type CT ON CT.id = C.type
								  LEFT JOIN #_deliverable_categories DC ON DC.id = D.category
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
								  LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
								  LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
								  LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
								  LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable
								  WHERE D.delstatus & ".DBConnect::DELETE." <> ".DBConnect::DELETE."  
								  $and
								  $deliverableType
								  ");	
			return $result['total'];	
	}
	
	function getDeliverable( $id )
	{
		$response = $this -> getRow("SELECT 
								  D.id,
								  D.contract_id,
								  D.name AS deliverable, 
								  DD.name as main_deliverable,
								  D.type,
								  D.category AS deliverable_category,								  
								  DD.id AS maindeliverableid,
								  D.description AS deliverable_description,
								  DW.description as dweight,
								  DQ.description as qweight,
								  OW.description as oweight ,
								  D.weight as delivered_weight,
								  D.quality_weight as quality_weight,
								  D.other_weight as other_weight ,	
								  D.owner AS owner,	
								  DC.name AS category,
								  D.status AS deliverable_status,
								  DS.name AS status,						  
								  CONCAT(TK.tkname , ' ', TK.tksurname) AS deliverable_owner,
								  D.delstatus,
								  D.deadline,						
								  D.remind_on,
								  C.contractdefaults							  							  
								  FROM #_deliverable D
								  INNER JOIN #_contract C ON C.id = D.contract_id 
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
								  LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
								  LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
								  LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
								  LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable
								  LEFT JOIN #_deliverable_categories DC ON DC.id = D.category
								  LEFT JOIN #_deliverable_status DS ON DS.id = D.status
								  WHERE D.delstatus & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
								  AND D.id = $id ");						  
		return $response;
	}

	function getADeliverable( $id )
	{
		$response = $this -> getRow("SELECT 
								  D.id AS ref,
								  D.contract_id,
								  D.name AS deliverable, 
								  DD.name as main_deliverable,
								  D.type,
								  D.category AS category,								  
								  DD.id AS maindeliverableid,
								  D.description AS deliverable_description,
								  DW.description as dweight,
								  DQ.description as qweight,
								  OW.description as oweight ,
								  D.weight as delivered_weight,
								  D.quality_weight as quality_weight,
								  D.other_weight as other_weight ,	
								  D.owner AS owner,	
								  DC.name AS deliverable_category,
								  DS.name AS deliverable_status,						  
								  CONCAT(TK.tkname , ' ', TK.tksurname) AS deliverable_owner,
								  D.delstatus,
								  D.deadline,						
								  D.remind_on,
								  C.contractdefaults							  							  
								  FROM #_deliverable D
								  INNER JOIN #_contract C ON C.id = D.contract_id 
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
								  LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
								  LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
								  LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
								  LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable
								  LEFT JOIN #_deliverable_categories DC ON DC.id = D.category
								  LEFT JOIN #_deliverable_status DS ON DS.id = D.status
								  WHERE D.delstatus & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
								  AND D.id = $id ");						  
		return $response;
	}	
	function save( $insertdata )
	{
		$res = $this -> insert("deliverable", $insertdata);
		return $this -> insertedId();
	}
	
	function getContractDeliverables( $contractid , $conditions = "")
	{
		$response = $this -> get("SELECT 
								  D.contract_id,
								  D.id AS ref,
								  D.name AS deliverable , 
								  D.description ,
								  D.owner,
								  D.status AS deliverable_status,									  
								  DW.description as descr_weight,
								  DQ.description as descr_quality_weight,
								  OW.description as descr_other_weight ,
								  DW.dweight as weight,
								  DQ.qweight as quality_weight,
								  OW.oweight as other_weight ,								  
								  CONCAT(TK.tkname, ' ', TK.tksurname) as del_owner								  
								  FROM #_deliverable D
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner
								  LEFT JOIN #_delivered_weight DW ON DW.id = D.weight
								  LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight
								  LEFT JOIN #_other_weight OW ON OW.id = D.other_weight
								  WHERE D.delstatus & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
								  AND  D.contract_id = $contractid 
								  $conditions
								  ");								  
		return $response;		
	}
	
	function getEditLogs( $id ){
		$result = $this->get("SELECT 
							  DE.changes,
							  DE.insertuser, 
							  DE.insertdate,
							  DS.name AS status  
							  FROM #_deliverable_edit DE 
							  INNER JOIN #_deliverable D ON DE.deliverable_id = D.id
							  LEFT JOIN #_deliverable_status DS ON D.delstatus = DS.id 
							  WHERE deliverable_id = $id
							  ORDER BY DE.insertdate DESC
							  ");

		return $result;	
	}
	
	function getUpdateLogs( $id ){
		$result = $this->get("SELECT 
							  DU.changes,
							  DU.insertuser, 
							  DU.insertdate,
							  DS.name AS status  
							  FROM #_deliverable_update DU 
							  INNER JOIN #_deliverable D ON DU.deliverable_id = D.id
							  LEFT JOIN #_deliverable_status DS ON D.delstatus = DS.id 
							  WHERE deliverable_id = $id
							  ORDER BY DU.insertdate DESC
							  ");

		return $result;	
	}
	
	function getAuthorization( $id )
	{
		$response = $this -> get("SELECT 
								  DA.description,
								  DA.approval,
								  DA.insertdate,
								  CONCAT(TK.tkname,' ',TK.tksurname) AS changeBy,
								  DS.name AS status
								  FROM #_deliverable_authorization DA
								  INNER JOIN #_deliverable D ON D.id = DA.deliverable_id
								  LEFT JOIN #_deliverable_status DS ON DS.id = D.status
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = DA.insertuser	
								  WHERE DA.deliverable_id = $id							  
								  ORDER BY DA.insertdate DESC								  
								  ");				  
		return $response;
	}
	
	function updateDeliverable( $id, $insertdata, $updatedata){
		$result	= "";
		$res	= "";
		if(isset($updatedata) && !empty($updatedata) ){
			$result = $this->update("deliverable", $updatedata, "id=$id");
		} else{
			$result = 1;
		}
		if(isset($insertdata) && !empty($insertdata) ){
			$res	= $this->insert("deliverable_update", $insertdata);					
		} else{
			$res = 1;
		}			
		if( $result > 0 && $res > 0){
			return 1;
		} else if( $result > 0 && $res < 0){
			return 2;
		} else {
			return -1;
		}
	}
	
	function editDeliverable( $id, $data, $insertdata){
		$result = "";
		$res 	= "";

		if(isset($data) && !empty($data)){
			$result = $this->insert("deliverable_edit", $data );
		} else{
			$result = 1;
		}
		if(isset($insertdata) && !empty($insertdata)){
			$res  = $this->update("deliverable", $insertdata, "id=$id");		
		} else {
			$res = 1;
		}

		if( $result > 0 && $res > 0){
			return 1;
		} else if( $result > 0 && $res < 0){
			return 2;
		} else {
			return -1;
		}
	}	
	
	function authorizaDeliverable( $insertdata ){
		$res	= $this->insert("deliverable_authorization", $insertdata);		
		return $res;
	}
	
	function assessment( $insertdata ){
		$res	= $this->insert("assessment", $insertdata);		
		return $this->insertedId();
	}	
}
?>