<?php
class DeliverableCategory extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getCategories()
	{
		$response = $this -> get("SELECT CC.id, CC.name,  CC.shortcode, CC.description, CC.status
								  FROM #_deliverable_categories  CC 
								  WHERE CC.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
								   ");
		return $response;
	}
	
	function getCategory( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_deliverable_categories WHERE id = $id");
		return $response;
	}
	
	function getActiveCategory( $type )
	{
		$response = $this -> get("SELECT * FROM #_deliverable_categories WHERE type = $type AND active =1");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("deliverable_categories", $data);
		return $this -> insertedId();
	}
	
	function updateCategory( $id, $data )
	{
		$dat    = $this->getCategory( $id );
        $logsObj = new Logs($_POST , $dat, "deliverable_categories");
        $logsObj -> createLog();			
		$res = $this -> update("deliverable_categories", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$dat    = $this->getCategory( $id );
        $logsObj = new Logs( array("status" => $tostatus) , $dat, "deliverable_categories");
        $logsObj -> createLog();		
		$res = $this -> update("deliverable_categories", array("status" => $tostatus), "id=$id");
		return $res;
	}
	
}
?>