<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/2/11
 * Time: 12:28 AM
 * To change this template use File | Settings | File Templates.
 */
 
class Report extends DBConnect {

    protected $contractHeaders    = array();

    protected $deliverableHeaders = array();

    protected $actionHeaders      = array();

    protected $headersArr         = array();

    protected $cHeads             = array();

    protected $aHeads             = array();

    protected $dHeads             = array();

    function generateReport(){
        $namingObj                = new Naming();
        $this->headersArr         = $namingObj->getHeader();
        $this->contractHeaders    = $this -> getContractFields( $_REQUEST['header'] );
        $this->deliverableHeaders = $this -> getDeliverableFields( $_REQUEST['dheader'] );
        $this->actionHeaders      = $this -> getActionFields( $_REQUEST['aheader'] );
        $group                    = rtrim($_REQUEST['group_by'],"_");
        $where                    = $this -> createWhere( $_REQUEST['values'] );
        $sort                     = $this -> createSort( $_REQUEST['sort'] );
        $sql                      = $where." ".($group == "" ? "" : " GROUP BY C.".$group)." ORDER BY ".$sort;
        $results = $this->get( $sql );
        $data    = $this->getDeliverableAction( $results );
        $slaData = array(
                        "data"            => $data,
                        "contractheaders" => $this->cHeads,
                        "delheaders"      => $this->dHeads,
                        "actionheaders"   => $this->aHeads
                    );
        return $slaData;
    }

    function getContractFields( $headers )
    {

        $c_fields = "";
        $db_fields = array(
                            "ref"                      => " C.id",
                            "contract"                 => " C.name AS contract",
                            "contract_type"            => " CT.name AS contract_type",
                            "contract_category"        => " CC.name AS contract_category",
                            "project_comments"         => " C.comments AS project_comments",
                            "contract_completion_date" => " C.completion_date AS contract_completion_date",
                            "contract_supplier"        =>  "S.name AS contract_supplier",
                            "contract_owner"           => " C.contract_owner",
                            "contract_status"          => " CS.name AS contract_status",
                            "project_value"            => " C.budget AS project_value",
                            "signature_of_tender"      => " C.signnature_of_tender AS signature_of_tender",
                            "signature_of_sla"         => " C.signature_of_sla",
                            "contract_assessment_frequency" => "AF.name AS contract_assessment_frequency"
        );
        foreach( $this->headersArr as $key => $value )
        {
            if(isset($headers[$key]) || array_key_exists($key, $headers))
            {
                   $c_fields  .= (isset($db_fields[$key]) ? $db_fields[$key] : " C.".$key).",";
                   $this->cHeads[$key] = $value;
            }
        }
       return rtrim($c_fields, ",");
    }

    function getDeliverableFields( $headers )
    {
        $d_fields   = "";
        $del_fields = array(
                            "ref"                   => " D.id",
                            "deliverable"           => "D.name AS deliverable",
                            "deliverable_description" => "D.description AS deliverable_description",
                            "deliverable_category"  => " DC.name AS deliverable_category",
                            "deliverable_owner"     => " TK.tkname AS deliverable_owner",
                            "delivered_weight"      => " DW.description AS delivered_weight",
                            "quality_weight"        => " DQ.description AS quality_weight",
                            "other_weight"          => " OW.description AS other_weight"
                           );
        foreach($this->headersArr as $field => $value)
        {
            if(isset($headers[$field]) || array_key_exists($field, $headers))
            {
                $d_fields .= (isset($del_fields[$field]) ? $del_fields[$field] : " D.".$field ).",";
                $this->dHeads[$field] = $value;
            }
        }
        return rtrim($d_fields,",");
    }

    function getActionFields( $headers )
    {
        $a_fields   = "";
        $ac_fields = array(
                            "ref"                   => " A.id",
                            "action"                => " A.name AS action",
                            "action_owner"          => " TK.tkname AS action_owner",
                            "action_status"         => " AST.name AS action_status"
                           );
        foreach($this->headersArr as $field => $value)
        {
            if(isset($headers[$field]) || array_key_exists($field, $headers))
            {
                $a_fields .= (isset($ac_fields[$field]) ? $ac_fields[$field] : " A.".$field ).",";
                $this->aHeads[$field] = $value;
            }
        }
       return rtrim($a_fields,",");
    }

    function createWhere( $fieldValues )
    {
       $matches = $_REQUEST['match'];
       $sql_    = "";
       $sql = "SELECT ".$this->contractHeaders." FROM #_contract C ";
        $join = "";
        $join  .= " INNER JOIN #_contract_type CT ON CT.id = C.type";
        $join  .= " LEFT JOIN #_contract_categories CC ON CC.id = C.category";
        $join  .= " INNER JOIN #_supplier S ON S.id = C.supplier";
        $join  .= " LEFT JOIN #_contract_update CU ON CU.contract_id = C.id";
        $join  .= " INNER JOIN #_contract_status CS ON CS.id = C.status";
        $join  .= " LEFT JOIN #_notification_rules NR ON NR.id = C.notification_frequency";
        $join  .= " LEFT JOIN #_assessment_frequency AF ON AF.id = C.assessment_frequency";
        $join  .= " LEFT JOIN #_contract CCT ON  C.id = CCT.template";
        $join  .= " INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = C.contract_owner ";
       $sql     =  $sql." ".$join." WHERE 1  ";
       $dateRange = "";
       $from     = "";
       $to       = "";
       foreach($fieldValues as $index => $value)
       {
           //echo $index."<br />";
           if($value != ""){
               if($index == "from_date")
               {
                   $from = date("Y-m-d H:i:s",strtotime($value));
               } else if($index == "to_date"){
                    $to = date("Y-m-d H:i:s",strtotime($value));
               }else  if(in_array($index, $matches) || array_key_exists($index, $matches)) {
                    if($matches[$index] == "all"){
                        $sql .= " AND C.".$index." LIKE '%".$value."%'";
                    } else if($matches[$index] == "any"){
                        $sql .= " AND  C.".$index." LIKE '".$value."'";
                    } else {
                        $sql .= " AND C.".$index." = '".$value."'";
                    }
                } else {
                    $sql .= " AND  C.".$index." = '".$value."'";
                }
           }
       }

        if($to == "" && $from !="")
        {
            $to = date("Y-m-d H:i:s");
        }
        if($to != "" && $from =="")
        {
            $from = date("Y-m-d H:i:s", strtotime("-365 days"));
        }
        if($from != "" && $to != "")
        {
            $dateRange = " AND C.insertdate BETWEEN '".$from."' AND '".$to."'";
        }
        $sql .= $dateRange;
        return $sql;
    }

    function createSort( $sortFields )
    {
        $sortBy = "";
        if(!empty($sortFields))
        {
            foreach($sortFields as $i => $field)
            {
                $sortBy .= " C.".ltrim($field,"__").",";
            }
        }
        return rtrim($sortBy, ",");
    }

    function getDeliverableAction( $contracts )
    {
        $deliverables = array();
        foreach( $contracts as $index => $contract)
        {
            $deliverables[$contract['id']]                 = $contract;
            $deliverables[$contract['id']]['deliverables'] = $this->getDeliverables( $contract['id'] );
        }
        return $deliverables;
    }

    function getDeliverables( $ref )
    {
       $sql = "SELECT ".$this->deliverableHeaders.", D.delstatus FROM #_deliverable D ";
        $sql .= " INNER JOIN #_contract C ON C.id = D.contract_id";
        $sql .= " INNER JOIN #_contract_type CT ON CT.id = C.type";
        $sql .= " LEFT JOIN #_deliverable_categories DC ON DC.id = D.category";
        $sql .= " LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner";
        $sql .= " LEFT JOIN #_delivered_weight DW ON DW.id = D.weight";
        $sql .= " LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight";
        $sql .= " LEFT JOIN #_other_weight OW ON OW.id = D.other_weight";
        $sql .= " LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable";
        $sql .= " LEFT JOIN #_assessment A ON A.deliverable_id = D.id";
        $sql .= " LEFT JOIN #_deliverable_status DS ON DS.id = D.status";
      $sql .= " WHERE D.contract_id = ".$ref." AND D.type = 0";
      $results  = $this -> get( $sql );
      $deliverables = array();
      foreach( $results as $deliId => $deliverable)
      {
        $deliverables[$deliverable['id']]            = $deliverable;
        if(($deliverable['delstatus'] & 8) == 8){
            $deliverables[$deliverable['id']]['sub_deliverable']  = $this->getSubDeliverables( $deliverable['id'] );
        } else {
          $deliverables[$deliverable['id']]['actions'] = $this->getActions( $deliverable['id']);
         //$deliverables[$deliverable['id']]['actions'] = $this->getActions( $deliverable['id']);
        }
      }
     return  $deliverables;
    }

    function getActions( $ref )
    {
        $sql = "SELECT ".$this->actionHeaders." FROM #_action A  ";
         $sql .= " INNER JOIN #_action_status AST ON AST.id = A.status";
         $sql .= " LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.owner";
        $sql .= " WHERE A.deliverable_id = '".$ref."'";
        $results = $this->get($sql);
        return $results;
    }

    function getSubDeliverables( $ref )
    {
           $sql = "SELECT ".$this->deliverableHeaders." FROM #_deliverable D ";
            $sql .= " INNER JOIN #_contract C ON C.id = D.contract_id";
            $sql .= " INNER JOIN #_contract_type CT ON CT.id = C.type";
            $sql .= " LEFT JOIN #_deliverable_categories DC ON DC.id = D.category";
            $sql .= " LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = D.owner";
            $sql .= " LEFT JOIN #_delivered_weight DW ON DW.id = D.weight";
            $sql .= " LEFT JOIN #_quality_weight DQ ON DQ.id = D.quality_weight";
            $sql .= " LEFT JOIN #_other_weight OW ON OW.id = D.other_weight";
            $sql .= " LEFT JOIN #_deliverable DD ON DD.id = D.main_deliverable";
            $sql .= " LEFT JOIN #_assessment A ON A.deliverable_id = D.id";
            $sql .= " LEFT JOIN #_deliverable_status DS ON DS.id = D.status";
          $sql .= " WHERE D.main_deliverable = ".$ref." ";
          $results  = $this -> get( $sql );
          $deliverables = array();
          foreach( $results as $deliId => $deliverable)
          {
            $deliverables[$deliverable['id']]            = $deliverable;
            $deliverables[$deliverable['id']]['actions'] = $this->getActions( $deliverable['id']);
          }
        return $deliverables;
    }

    function saveQuickReport()
    {
        $name = $_REQUEST['report_name'];
        unset($_REQUEST['report_name']);
        unset($_REQUEST['save_quick_report']);
        $updatedata = array("name" =>$name, "report" => serialize($_REQUEST), "insertuser" => $_SESSION['tid'] );
        if($this->insert("quick_report", $updatedata))
        {
            echo "Report Saved successfully";
        } else {
            echo "Error saving the report";
        }
    }

    function getQuickReports()
    {
        $results = $this->get("SELECT id, name FROM #_quick_report");
        return $results;
    }

    function getQuickData( $id )
    {
        $results = $this->getRow("SELECT report FROM #_quick_report WHERE id = $id");
        return $results['report'];
    }
}
