<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/4/11
 * Time: 12:14 AM
 * To change this template use File | Settings | File Templates.
 */
 @seesion_start();
class Search extends DBConnect
{

    function __construct(){
        echo "Is this initializing at all";
    }

    function search()
    {
        echo "This is where it get the fields";
        $this->getFields();
    }

    function getFields()
    {
        $contract       = $this->describe("contract");
        $deliverable    = $this->describe("deliverable");
        $action         = $this->describe("action");
        $type           = $this->describe("contract_type");
        $category       = $this->describe("contract_category");
        $supplier       = $this->describe("supplier");
        $owners         = $this->get("DESCRIBE assist_".$_SESSION['cc']."_timekeep");
        $assessmentfrequency   = $this->describe("assessment_frequency");
        $notificationfrequency = $this->describe("notification_frequency");
        $status          = $this->describe("status");
        $actionstatus    = $this->describe("action_status");
        $deliverabstatus = $this->describe("deliverable_status");
        $deliverablecategory  = $this->describe("deliverable_categories");
        $weight               = $this->describe("delivered_weight");
        $qualityweight        = $this->describe("quality_weight");
        $otherweight          = $this->describe("other_weight");

        $fields = array(
                        "C"  => array("joinWith" => "", "fields" => $contract ),
                        "D"  => array("joinWith" => "C.id = D.contract_id" , "fields" => $deliverable),
                        "A"  => array("joinWith" => "D.id = A.deliverable_id", "fields" => $action),
                        "CT" => array("joinWith" => "CT.id = C.type", "fields" => $type),
                        "CC" => array("joinWith" => "CC.id = C.category", "fields" => $category),
                        "CS" => array("joinWith" => "CS.id  = C.supplier", "fields" => $supplier),
                        "TK" => array("joinWith" => "TK.tkid = C.contract_owner", "fields" => $owners),
                        "AF" => array("joinWith" => "AF.id = C.assessment_frequency", "fields"   => $assessmentfrequency ),
                        "NF" => array("joinWith" => "NF.id = C.notification_frequency", "fields" => $notificationfrequency ),
                        "ST" => array("joinWith" => "ST.id = C.status" , "fields" => $status),
                        "SA" => array("joinWith" => "SA.id = A.status", "fields" => $actionstatus),
                        "DS" => array("joinWith" => "DS.id = D.status", "fields" => $deliverabstatus),
                        "DC" => array("joinWith" => "DC.id = D.category", "fields" => $deliverablecategory),
                        "DW" => array("joinWith" => "DW.id = D.weight", "fields" => $weight),
                        "QW" => array("joinWith" => "QW.id = D.quality_weight", "fields" => $qualityweight),
                        "OW" => array("joinWith" => "OW.id = D.other_weight", "fields" => $otherweight)
        );
        print "<pre>";
          print_r($fields);
        print "</pre>";
    }

}
