<?php
class SupplierCategory extends DBConnect
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getSupplierCategories()
	{
		$response = $this -> get("SELECT * FROM #_supplier_category WHERE  status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." ");
		return $response;
	}
	
	function getSupplierCategory( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_supplier_category WHERE id = $id");
		return $response;
	}
	
	function save( $data )
	{
		$response = $this -> insert("supplier_category", $data);
		return $this -> insertedId();
	}
	
	function updateSupplierCategory( $id, $data )
	{
		$dat    = $this->getSupplierCategory( $id );
        $logsObj = new Logs( $_POST , $dat, "supplier_category");
        $logsObj -> createLog();		
		$res = $this -> update("supplier_category", $data, "id=$id");
		return $res;
	}
	
	function changeStatus( $id, $tostatus )
	{
		$dat    = $this->getSupplierCategory( $id );
        $logsObj = new Logs( array("status" => $tostatus) , $dat, "supplier_category");
        $logsObj -> createLog();			
		$res = $this -> update("supplier_category", array("status" => $tostatus), "id=$id");
		return $res;
	}
	
}

?>