<?php
class ContractStatus extends DBConnect
{

	function _contruct()
	{
		parent::_contruct();
	}


	function save( $data )
	{
		$response = $this -> insert("contract_status", $data);
		return $this->insertedId();
	}

	function getStatuses()
	{
		$response  = $this -> get("SELECT * FROM #_contract_status WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."");
		return $response;
	}
	
	function getCStatus( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_contract_status WHERE id = $id ");
		return $response;
	}
	
	function updateStatus( $id, $data )
	{
		$dat    = $this->getCStatus( $id );
        $logsObj = new Logs($data , $dat, "contract_status");
        $logsObj -> createLog();		
		$response = $this -> update("contract_status", $data, "id=$id");
		return $response;
	}
	
	function updateCStatus( $id , $status)
	{
		$dat    = $this->getCStatus( $id );
        $logsObj = new Logs(array("status" => $status) , $dat, "contract_status");
        $logsObj -> createLog();		
		$response = $this -> update("contract_status", array("status" => $status), "id=$id"); 
		return $response;
	}

}
?>