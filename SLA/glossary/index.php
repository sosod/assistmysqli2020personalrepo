<?php
$scripts = array("glossary.js");
require_once("../header.php");
$gloss 		= new Glossary();
$glossaries = $gloss->getGlossaries(); 
?>
<div id="glossary">
<table>
	<tr>
    	<th>Category</th>
    	<th>Terminology</th>
    	<th>Explanation</th>                
    </tr>
<?php
foreach($glossaries as $key => $value){
?>
	<tr>
    	<td><?php echo $value['category']; ?></td>
    	<td><?php echo $value['terminology']; ?></td>
    	<td><?php echo $value['explanation']; ?></td>                
    </tr>
<?php
}
?>
</table>
</div>