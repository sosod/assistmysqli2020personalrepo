<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/10/11
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("search.js");
require_once("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="advanced-search" enctype="multipart/form-data">
<table id="contract_table">
    <tr>
    	<th>Project Name:</th>
        <td><input type="text" name="name" id="name" placeholder="enter project name" value="" /></td>
    </tr>
    <tr>
    	<th>Project Value <em>(Budget)</em>:</th>
        <td><input type="text" name="budget" id="budget" placeholder="enter project budget" value="" /></td>
    </tr>
    <tr>
    	<th>Comments:</th>
        <td><textarea name="comments" id="comments" placeholder="enter project comments"></textarea></td>
    </tr>
    <tr>
    	<th>SLA Type:</th>
        <td>
        	<select name="type" id="type">
            	<option value="">--select sla type--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>SLA Category:</th>
        <td>
        	<select name="category" id="category">
            	<option value="">--select sla category--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>Supplier:</th>
        <td>
        	<select name="supplier" id="supplier">
            	<option value="">--select sla suppier--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>Date Signature of Tender Document:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signnature_of_tender" name="signnature_of_tender" class="datepicker" />
        </td>
    </tr>
    <tr>
    	<th>Date Signature of SLA Document:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signature_of_sla" name="signature_of_sla" class="datepicker" />
        </td>
    </tr>
    <tr>
    	<th>Contracted Completion Date:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="completion_date" name="completion_date" class="datepicker" />
        </td>
    </tr>
    <tr>
    	<th>Contract Manager:</th>
        <td>
        	<select name="contract_manager" id="contract_manager">
            	<option value="">--select contract manager--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>Contract Owner:</th>
        <td>
        	<select name="contract_owner" id="contract_owner">
            	<option value="">--select contract owner--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>Contract Authorisor 2:</th>
        <td>
        <select name="contract_authorisor" id="contract_authorisor">
            	<option value="">--select contract authorisor--</option>
        </select>
        </td>
    </tr>
    <tr>
    	<th>Template Used:</th>
        <td>
        	<select name="template" id="template_">
            	<option value="">--select template--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>
        	Notification Frequency:
        </th>
        <td>
        	<select name="notification_frequency" id="notification_frequency">
            	<option value="">--select notification frequency--</option>
            </select>
        </td>
    </tr>
    <tr>
     <td></td>
    <td align="right">
     	<input type="submit" id="advancedsearch" name="advancedsearch" value="Search" class="savecontract" />
     </td>
    </tr>
</table>
</form>
<div id="result"></div>