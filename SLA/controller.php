<?php
@session_start();
class Controller extends DBConnect
{
	protected $ctrl   = "";
	protected $model  = "";
	//
	function __construct( $url )
	{
		parent::__construct();
		$this -> ctrl = strtolower($url);
	}
	
	function loadController()
	{	
		if( file_exists( "controllers/".$this -> ctrl."controller.php" ))
		{
			require_once("controllers/".$this -> ctrl."controller.php");
			$class 		= ucwords($this -> ctrl)."Controller";
			$controller = new $class();
		}	
	}
	
	function loadModel( $model )
	{
		$class 		= ucwords($model);
		$obj 		= new $class();
		return $obj;
	}
	
	function getSubMenu()
	{
		$response = $this -> get("SELECT * FROM assist_".$_SESSION['cc']."_sla_menu WHERE folder = '".$this -> ctrl."' AND parent_id <> 0");
		return $response;
	}
	
	
	function wasWhere( $requestArr, $otherNames, $pagename )
	{
		$secondMenu = "";
		$name 		= array();
		
		if($_SERVER['QUERY_STRING'] == "")
		{
			unset($_SESSION['secondMenu']);
			unset($_SESSION['firstMenu']);
			unset($_SESSION['thirdMenu']);
			
			unset($_SESSION['firstName']);
			unset($_SESSION['thirdName']);
			unset($_SESSION['secondName']);
			$_SESSION['firstMenu'] = $_SERVER['REQUEST_URI'];
			$_SESSION['firstName'] =  $this->niceName($requestArr, $otherNames, $pagename); //ucfirst(isset($otherNames[$pagename]) ? $otherNames[$pagename] : $pagename);
		} else if(strstr($_SERVER['QUERY_STRING'],"&")){
			$_SESSION['thirdMenu'] = $_SERVER['REQUEST_URI'];			
			$_SESSION['thirdName'] =  $this->niceName($requestArr, $otherNames, $pagename);//ucfirst(isset($otherNames[$pagename]) ? $otherNames[$pagename] : $pagename);
		} else {
			unset($_SESSION['secondMenu']);
			unset($_SESSION['thirdMenu']);
			unset($_SESSION['thirdName']);
			$_SESSION['secondMenu'] = $_SERVER['REQUEST_URI'];	
			$_SESSION['secondName'] = $this->niceName($requestArr, $otherNames, $pagename);//ucfirst(isset($otherNames[$pagename]) ? $otherNames[$pagename] : $pagename); 
		}
		echo "<a href='".$_SESSION['firstMenu']."' class='breadcrumbs'>".$_SESSION['firstName']."</a>".(isset($_SESSION['secondMenu']) ? "&nbsp;>>&nbsp;<a href='".$_SESSION['secondMenu']."' class='breadcrumbs'>".$_SESSION['secondName']."</a>" : "").(isset($_SESSION['thirdMenu']) ? "&nbsp; >>&nbsp;<a href='".$_SESSION['thirdMenu']."' class='breadcrumbs'>".$_SESSION['thirdName']."</a>" : "");		
	}
	//create a nice name for the menu , 
	//Changes php files into nice name as defined in the other names array
	//if the name is not in the other menu , then it just removes the extension and makes a name out of it
	function niceName( $requestStr , $niceNames, $page)
	{		
		if(isset($niceNames[$page])){
			return $niceNames[$page];		
		} else {
			if(strpos($page, "_") > 0)
			{
				return ucwords( str_replace("_", " ", $page) );
			} else {
				return ucwords($page);
			}
		}
	} 
	
}
?>