<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} 
}
class NewController extends Controller
{

	protected $delHeaderArray = array();
	
	protected $headersArray		  = array(); 

	function __construct()
	{
		
	}

	function getActiveType()
	{
		$obj = $this -> loadModel("ContractType");
		echo json_encode( $obj -> getActiveContractType() );
	}

	function getActiveCategory()
	{
		$obj = $this -> loadModel("ContractCategory");
		echo json_encode( $obj -> getCategories( ) );		
	}

	function getSupplier()
	{
		$obj = $this -> loadModel("Supplier");
		echo json_encode( $obj -> getActiveSupplier() );			
	}
	
	function getUsers()
	{
		$obj = $this -> loadModel("UserAccess");
		echo json_encode( $obj -> getUsers() );					
	}
	
	function getTemplates()
	{
		$obj = $this -> loadModel("Contract");
		echo json_encode( $obj -> getTemplates() );
	}
	
	function getAssessmentFrequency()
	{
		$obj = $this -> loadModel("AssessmentFrequency");
		echo json_encode( $obj -> getActiveAssessmentFrequencies() );
	}
	
	function getNotificationFrequency()
	{
		$obj = $this -> loadModel("Notification");
		echo json_encode( $obj -> getNotifications() );	
	}
	
	function getDeliverableCatergories()
	{
		$obj = $this -> loadModel("DeliverableCategory");
		echo json_encode( $obj -> getCategories() );	
	}
	
	function saveContract()
	{
		$insertdata 	 = array();
		$response  		 = array();
		$obj 			 = $this -> loadModel("Contract");
		$user 		 	 = $this -> loadModel("UserAccess");		
		$email 			 = $this -> loadModel("Email");			
		$defaults 		 = 0;  
		$useTemplate	 =  false;
		$templateId 	 = ""; 
		if( $this->_checkValidity( $_POST['completion_date'], $_POST['signnature_of_tender']) ) {
			 if( $this->_checkValidity( $_POST['completion_date'], $_POST['signature_of_sla']) ) {
				foreach( $_POST as $key => $value)
				{
					if( $key == "deliverable_status" ) {
						$insertdata[$key] = serialize( $value );
					} else if($key == "template" && $value != ""){
						$useTemplate = true;
						$templateId	 = $value; 
						$contractDetails = $obj -> getAContract( $value ); 
		
						if( ($contractDetails['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED)
						{
							$defaults = $defaults + Contract::DELIVERED ;
							//echo "Coming here for the delivered weight => ".$defaults;							
						}
						if( ($contractDetails['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY )
						{
							$defaults = $defaults + Contract::QUALITY ;		
							///echo "Coming here for the quality weight => ".$defaults;							
						}
						if( ($contractDetails['contractdefaults'] & Contract::OTHER) == Contract::OTHER)
						{
							$defaults = $defaults + Contract::OTHER;				
							//echo "Coming here for the other weight => ".$defaults;							
						}
						$insertdata[$key] = $value;
					} else {
						$insertdata[$key] = $value;			
					}
				}
				
				$insertdata['attachments'] = (isset($_SESSION['contract_uploads']) ? serialize($_SESSION['contract_uploads']) : "");
				
				$insertdata['contractdefaults'] = $defaults + (isset($insertdata['contractdefaults']) ? $insertdata['contractdefaults'] : 0);
		
				$insertdata['insert_user'] = $_SESSION['tid'];
				$res = $obj -> save( $insertdata );
				if( $res > 0 ){
					//if the contract uses a template , dumo the template data
					$templateText = "";
					if( $useTemplate ){					
						$templateData = $obj -> getContractTemplate( $templateId ); 
						$template 	  = $this->_processTemplate( $templateData, $res );
						if( $template > 0){
							$templateText = "Template data for this contract saved";	
						}
					}
					$managerEmail = $user -> getUser( $_POST['contract_manager']);
					$mailText = "";
					$del   = $obj -> getContract( $res );
					unset($del['deliverable_status']);
					
					$body  = $_SESSION['tkn']." added a new contract <br />";
 					$body .= $this->_createBody( $del );
					$body .= "<br /><br />Please log onto Ignite Assist in order to View or Update the Contract and to assign any required Deliverable(s) to responsible users \r\n";			
					$body .= "\n";	//blank row above Please log on line
					//echo "Body before sendig it ".$body."\r\n\n";
					if( $email -> to( (isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("New Contract") -> body( $body ) -> send()  == 1){
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Notification email sent successfully";	
					}	else {
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Error sending mail";	
					}
								
					//response
					$response = array("error" => false, "text" => "Contract saved successfully <br />".$templateText."<br />".$mailText, "id" => $res);
				} else{
					$responce = array("error" => true, "text" => "Error saving contract");
				}
						
			} else{
				$response = array("error" => true, "text" => "Date of signature of Contract cannot not be after the completion date of the contract");
			}			
		} else {
			$response = array("error" => true, "text" => "Date of Signature of tender cannot be after the completion date of the contract");
		}

		unset($_SESSION['contract_uploads']);
		echo json_encode($response);
	}

	//save template data for a contract that uses a template
	function _processTemplate( $templateData, $id )
	{
		$deliverableObj = $this -> loadModel("Deliverable");
		$actionObj 		= $this -> loadModel("Action");			

		$insertDel	  = array(); 
		$insertAction = array(); 
		$template 	  = unserialize( $templateData['data'] ); 	
		$dels		  = 0;
		$acts		  = 0;
		//print "<pre>";
			//print_r($template);
		//print "</pre>";
		$idArray	= array();
		foreach( $template as $index => $deliverable){
			// create a delivereable for  the template

			$deliverableId	= "";
			$actionId		= "";	
			$deliverableInsert = array( 
								"name" 		   		=> $deliverable["deliverable"] ,
								"description"  		=> $deliverable["deliverable_description"],
								"category"    		=> $deliverable["category"],
								"main_deliverable"  => $deliverable["main_deliverable"],																																				
								"other_weight"		=> $deliverable["other_weightId"],
								"weight" 			=> $deliverable["delivered_weightId"],
								"quality_weight"	=> $deliverable["quality_weightId"],
								"delstatus"			=> $deliverable["delstatus"],
								"contract_id"		=> $id
								 );
			//if has sub deliverables create a new reference
			if( $deliverable['maindeliverableid'] != "" ){
				$deliverableInsert['main_deliverable'] = $idArray[$deliverable['maindeliverableid']]['newId'];	
				$deliverableInsert['type'] 			   = 1;					
			}						 
			$deliverableId = $deliverableObj->save( $deliverableInsert );
			//save the old contact id and new contract id for refeernce later when creating sub deliverables
			$idArray[$deliverable['ref']]  = array( "newId" 			=> $deliverableId ,
											   		"ogId" 				=> $deliverable['ref'],
											   		"maindeliverableid" => $deliverable['maindeliverableid']		
									); 
			$dels		   = $dels +  $deliverableId;
			//echo "deliverable is => ".$deliverableId;
			//save the actions for this deliverable
			foreach( $deliverable['actions'] as $key => $action){
	
				$actionInsert	  = array( 
									"name" 		   		=> $action["action"],
									"measurable"  		=> $action["measurable"],
									"deliverable_id"	=> $deliverableId										
								);
				$actionId 		= $actionObj -> save( $actionInsert );
				$acts			= $acts +  $actionId;
			}
		}
		return $dels + $acts;
	}
	
	
	function editContractDetails()
	{
		$obj 		= $this -> loadModel("Contract");
		$insertdata	= array();
		$response   = array(); 
		foreach( $_POST as $key => $value)
		{
			if( $key == "deliverable_status" ) {
				$insertdata[$key] = serialize( $value );
			} else if( $key == "contract_id"){
				continue;
			} else {
				$insertdata[$key] = $value;			
			}
		}
		$insertdata['insert_user'] = $_SESSION['tid'];
		$res = $obj -> updateContractDefaults( $_POST['contract_id'], $insertdata);		
		if( $res  == 1 ){
			$response = array("error" => false, "text" => "Contract updated successfully");
		} else if( $res == 0){
			$response = array("error" => true, "text" => "No change made . . ");
		} else if( $res == -1){
			$response = array("error" => true, "text" => "Error updating contract .");
		}
		echo json_encode($response);		
	}
	
	function saveContractDefault()
	{
		$object 	= $this -> loadModel("Contract");
		$contract 	= $object -> getContract( $_POST['id'] ); 
		$default    = $contract['contractdefaults'];		
		foreach( $_POST as $key => $value )
		{
			if( $key == "hasDeliverables")
			{
				$default = ($value == 1 ? $default + Contract::HAS_DELIVERABLES : $default );
			}
			if( $key == "hasSubDeliverables")
			{
				$default = ($value == 1 ? $default + Contract::HAS_SUBDELIVERABLES : $default ) ;
			}
			if( $key == "weightsAssigned")
			{
				$default  = ($value == 1 ? $default + Contract::WEIGHTS_ASSIGNED : $default );
			}
			if( $key == "delivered")
			{
				$default = ($value == 1 ? $default + Contract::DELIVERED : $default ) ;
			}
			if( $key == "quality")
			{
				$default = ($value == 1 ? $default + Contract::QUALITY : $default );
			}
			if( $key == "other")
			{
				$default = ($value == 1 ? $default + Contract::OTHER : $default ) ;
			}
			if( $key == "othercategory")
			{
				$naming = $this->loadModel("Naming");
				$updatedata['othercategory'] = 1;
				$this->saveNaming(15, $value);
			}																	
		}

		$updatedata = array_merge($updatedata, array("contractdefaults" => $default));
		$res	    = $object -> updateContractDefaults( $_POST['id'], $updatedata);
		$response   = array(); 
		if( $res == 1 ){
			$response = array("text" => "Update successfull" , "error" => false);
		}  else if( $res == 0){
			$response = array("text" => "No change was made to the contract" , "error" => true);
		} else{
			$response = array("text" => "There was an error updating the contract" , "error" => true);
		}
		echo json_encode( $response );
	}

	// ===============================================================================================

	function getDeliverable()
	{
		$obj = $this -> loadModel("Deliverable");
		echo json_encode( $obj -> getDeliverable( $_POST['id']) );	
	}
	
	function getActionStatus()
	{
		$obj = $this -> loadModel("ActionStatus");
		echo json_encode( $obj -> getActionStatuses() );		
	}
	
	function getContract()
	{
		$contract 	 = $this -> loadModel("Contract");		
		$contractDetail =  $contract -> getContract( (isset($_POST['id']) ? $_POST['id'] : "") );
		$contractArr = $this->_contractInfo( $contractDetail );
		//$contractArr['contractDefaults'] = $contractDetail['contractdefaults'];
		echo json_encode( $contractArr );
	}
		
	function _contractInfo( $contract )
	{
		$headernames = $this -> loadModel("Naming");
		$headers	 = $headernames -> getNaming();
		$contractArr    = array(); 
		foreach( $headers as $key => $header){
			$name = $header['name'];
			if( isset($contract[$name]) ){
				if( $name == "deliverable_status"){
					
				} else {	
					$contractArr[$name] = array(
									"colName" 		   => $header['client_terminology'],
									"value" 	       => $contract[$name],
					);  
				}
			}
		}
		return $contractArr;
	}
	
	function getContracts()
	{
		$contract 	 = $this -> loadModel("Contract");
		$headernames = $this -> loadModel("Naming");
		$contr   	 = $contract -> getContracts( $_POST['start'], $_POST['limit'] , $conditions = "" );
		$total 		 = $contract -> getTotalContract( "" );
		$headers	 = $headernames -> getHeader();
		$contractArray = array();
		$headersArray  = array();		
		
		$attachements	= "";
		foreach( $contr as $key => $ctrl )
		{
		  foreach( $headers as $field => $val)
			{ 
				if( $field == "attachments"){
					$attachements =  $this->_processAttachment( $ctrl[$field] );
					$headersArray[$field]	   = $val;
					$contractArray[$ctrl['ref']][$field] = $attachements;																			
				} else if( isset( $ctrl[$field] ) ) {
					$headersArray[$field]	   = $val;
					$contractArray[$ctrl['ref']][$field] = $ctrl[$field];			
				}	
			}
		}
		$response 	 = array(
							"contractArr"  => $contractArray ,
							"headers"      => $headersArray ,
							"total"        => $total,
							"headCount"    => count($headersArray) 
							); 
		
		echo json_encode( $response );
	}
	
	function getApprovalData()
	{
		$contract 	 	 = $this -> loadModel("Contract");

		$conditions	     = "AND contractdefaults & ".Contract::LOCKED." = ".Contract::LOCKED." ";
		$conditions	    .= "AND contractdefaults & ".Contract::APPROVED." <> ".Contract::APPROVED." ";			
		$awaitingContr	 = $contract -> getContracts( $_POST['start'], $_POST['limit'], $conditions );
		
		$conditions	     = "AND contractdefaults & ".Contract::APPROVED." = ".Contract::APPROVED."";	
		$approvedContr	 = $contract -> getContracts( $_POST['start'], $_POST['limit'], $conditions );
		
		$awaiting 		= $this->_createApproval( $awaitingContr );		
		$approved 		= $this->_createApproval( $approvedContr );

		echo json_encode( array( "awaiting" => $awaiting, "approved" => $approved , "headers" => $this->headersArray, "total" => count($this->headersArray) ) );
			
	}
	
	function _createApproval( $contract )
	{
		$headernames  = $this -> loadModel("Naming");	
		$headers	  = $headernames -> getHeader();		
		$contractArr  = array();
		$headersArray = array();
		$notAllowed = array("deliverable_status", "contract_manager", "contract_authorisor");
		foreach( $contract as $key => $ctrl )
		{
			$ctrl['contract_status'] = $ctrl['status'];
			foreach( $headers as $field => $val)
			{
				if( in_array($field ,$notAllowed)){
				} else if( isset( $ctrl[$field] ) ) {
					$this->headersArray[$field]	   	   = $val;
					$contractArr[$ctrl['ref']][$field] = $ctrl[$field];			
				}	
			}
		}	
		return $contractArr;
	}
	/*
	This function create the data that is going to be displayed 
	Creates and loadd the headers matching the data for the deliverables
	*/
	function _processDeliverable( $value , $contract, $requestPage)
	{
		$headernames = $this -> loadModel("Naming");		
		$headers	 = $headernames -> getHeader();
		$array       = array();
		$delArray 	 = array(); 
		$headArray   = array();		
		foreach( $headers as $field => $val)
		{
			//echo $value['ref']." => ";
			//echo "Delivered => ".($contract['contractdefaults'] & Contract::DELIVERED)." => value ".Contract::DELIVERED;
			//echo " Quality => ".($contract['contractdefaults'] & Contract::QUALITY)." => value ".Contract::QUALITY;
			//echo " Other => ".($contract['contractdefaults'] & Contract::OTHER)." => value".Contract::OTHER."\r\n\n\n\n";
								
			 if($field == "delivered_weight"){
					if(($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED){				
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;					
					}				
			} else if($field == "quality_weight"){
					if(($contract['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY){				
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;					
					}												
			} else if($field == "other_weight"){
					if(($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER){				
						$delArray[$value['ref']][$field] = ($value[$field] == "" ? 0 : $value[$field]);
						$this->delHeaderArray[$field]	 = $val;					
					}				
			} else if($field == "main_deliverable"){
				
				$delArray[$value['ref']]["deliverable_type"] = ($value[$field] == "" ? "Main" : "Sub")." deliverable";
				$this->delHeaderArray["deliverable_type"]    = "Deliverable Type";	
								
				$delArray[$value['ref']]['number_of_actions'] = ($value['number_of_actions'] == "" ? "<span class='error'>&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;</span>" : "<span class='success'>&nbsp;&nbsp;&nbsp;&nbsp;".$value['number_of_actions']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
				
				$this->delHeaderArray['action_progress']	 = "Action Progress";
				$delArray[$value['ref']]['action_progress']  = ($value['action_progress'] == ""  ? "0" : $value['action_progress']);
				
				$this->delHeaderArray["number_of_actions"]	 = "Number of actions";					
			}  else if( isset($value[$field]) || array_key_exists($field, $value)){
				$delArray[$value['ref']][$field] = ($value[$field] == "" ? "" : $value[$field]);
				$this->delHeaderArray[$field]	 = $val;
			}
		}

		$this->delHeaderArray = $this->sortHeadeDisplay( $this->delHeaderArray ,  $requestPage);
		$delArray = $this->sortDisplay( $delArray , $requestPage);	
		return $delArray;
	}
	
	function sortDisplay( $del, $requestPage )
	{
		$order = array("ref", "deliverable", "deliverable_description", "deliverable_type", "deliverable_owner", "deadline", "deliverable_status", "number_of_actions", "action_progress", "deliverable_category", "actions_to_approve");
		if( $requestPage != "update"){
			$order = array_merge( $order, array("delivered_weight", "quality_weight", "other_weight"));
		}
		
		$orderedArray = array();
		foreach( $order as $key)
		{
			foreach( $del as $k => $value){
				if( isset($value[$key])){
					$orderedArray[$k][$key] = (($value[$key] == "") ? "" : $value[$key]);
				} else {
				
				}
			}
		}
		return $orderedArray;		
	}
	
	function sortHeadeDisplay( $del, $requestPage )
	{
		$order = array("ref", "deliverable", "deliverable_description", "deliverable_type", "deliverable_owner", "deadline", "deliverable_status", "number_of_actions", "action_progress", "deliverable_category",  "actions_to_approve");
		if( $requestPage != "update"){
			$order = array_merge( $order, array("delivered_weight", "quality_weight", "other_weight"));
		}		
		$orderedArray = array();
		foreach( $order as $key)
		{
			$orderedArray[$key] = (isset($this->delHeaderArray[$key]) ? $this->delHeaderArray[$key] : "");	
		}
		return $orderedArray;		
	}
	function getDeliverablesList(){
		$obj 		  = $this -> loadModel("Deliverable");
		$result 	  = $obj -> getDeliverables( (isset($_POST['id']) ? $_POST['id'] : ""), "mainSub");
		$deliverables = $result['deliverable'];
		echo json_encode( $deliverables );
	}
	

	function calcualtion( $subActions  )
	{
			$totalSbProgress = 0;
			foreach($subActions as $i => $action)
			{
				//echo "Progress => ".$action['progress']."<br />";
				$totalSbProgress = $totalSbProgress + $action['progress'];					
			}	
			$noOfAction = count($subActions);
			$aveForActions = 0;
			if( $noOfAction == 0 ){
				$aveForActions = 0;
			} else {
				$aveForActions = ($totalSbProgress / $noOfAction);						
			}
		return $aveForActions;				
	}
	
	function _getSubDedeliverables( $mainID, $deliverableObj, $contract, $aObj , $requestPage)
	{
		$result  		= $deliverableObj -> getDeliverables( $mainID , "sub");
		$subDeliverable = $result['deliverable'];
		$total 		    = $result['total'];		
		//$result 		= $this->_processDeliverable( $subDeliverable );
		$subArr 		= array();
		$totalDW 		= 0;
		$totalOW 		= 0;
		$totalQW 		= 0;				
		$totalActions   = 0; 
		$actionsProgress= 0;
		foreach( $subDeliverable  as $key => $subDel){

			$conditions								= "";
			$actions 								= $aObj->getActions( $subDel['ref'], $conditions );
			$subDel['number_of_actions'] 			= (count($actions) == "" ? 0 : count($actions));		
			$subDel['action_progress']				= $this->calcualtion( $actions );

			$actionsProgress						= $actionsProgress + $subDel['action_progress'];
			$totalActions 							= $totalActions + count($actions);		
			$result 								= $this->_processDeliverable( $subDel , $contract, $requestPage);
			//echo $subDel['owner']." => ".$_SESSION['tid']."<br /><br />";
			$result[$subDel['ref']]['hasActionAccess']= $this->_checkActionAccess( $actions );
			$result[$subDel['ref']]['hasAccess']	 = ($subDel['owner'] == $_SESSION['tid'] ? 1 : 0); 
			$subArr[$subDel['ref']] = $result[$subDel['ref']];
			
			if( isset($subArr[$subDel['ref']]['delivered_weight']) && !empty($subArr[$subDel['ref']]['delivered_weight'])) {
				$totalDW	+= $subArr[$subDel['ref']]['delivered_weight'];
			}
			 
			if(isset($subArr[$subDel['ref']]['quality_weight']) && !empty($subArr[$subDel['ref']]['quality_weight'])) {
				$totalQW	+= $subArr[$subDel['ref']]['quality_weight'];			
			}
			 
			if(isset($subArr[$subDel['ref']]['other_weight']) && !empty($subArr[$subDel['ref']]['other_weight'])) {
				$totalOW	+= $subArr[$subDel['ref']]['other_weight'];		
			}
		}

		return array( "subdel" => $subArr, "totalDW" => $totalDW, "totalQW" => $totalQW , "totalOW" => $totalOW, "actions" => $totalActions, "actionProgress" => $actionsProgress );
	}	
	
	function _checkActionAccess( $actionArr )
	{
		foreach( $actionArr as $key => $value)
		{
			if( $value['owner'] == $_SESSION['tid']){
				return 1;
			} 
		}
		return 0;
	}
	
	function getDeliverables()
	{
		$obj 		  = $this -> loadModel("Deliverable");
		$contrObj 	  = $this -> loadModel("Contract");
		$aObj 	 	  = $this -> loadModel("Action");
		
		$contract     = $contrObj->getContract( (isset($_POST['id']) ? $_POST['id'] : "")  );

		$delResult    = ""; 

		$delResult 	  = $obj -> getDeliverables( (isset($_POST['id']) ? $_POST['id'] : ""), "main");		
		$deliverables = $delResult['deliverable'];
		$total  	  = $delResult['total'];
		$requestPage  = $_POST['request'];
		
		$delerableArr = array();
		$index 		  = 0; 
		$project_name = "";
		foreach($deliverables as $key => $deliverable){
			//get the sub deliverables for this main deliverable
			$project_name	= $deliverable['project_name'];
			$delInfo 	= $this-> _getSubDedeliverables( $deliverable['ref'], $obj , $contract, $aObj, $requestPage);
			$conditions = "";
			$actions 	= $aObj->getActions( $deliverable['ref'] , $conditions);
			$subDelArr = $delInfo['subdel'];
			$subCount  = count($subDelArr);
			//proccess main deliverables	

			$deliverable['number_of_actions'] 	= (count($actions) == "" ? 0 : count($actions));
			$deliverable['action_progress']		= round( $this->calcualtion( $actions ), 2);			
			$result = $this->_processDeliverable( $deliverable , $contract, $requestPage);

		    $total = 0;
			//if main deliverable has sub deliverables 
			if( (isset($subDelArr) && !empty($subDelArr)) || ($deliverable['delstatus'] & Deliverable::MAIN_WITH_SUB) == Deliverable::MAIN_WITH_SUB) {

				if( array_key_exists( 'delivered_weight', $result[$deliverable['ref']] ) ) {
					$result[$deliverable['ref']]['delivered_weight'] = $delInfo['totalDW'];
				}
				 
				if(array_key_exists( 'quality_weight', $result[$deliverable['ref']])) {
					$result[$deliverable['ref']]['quality_weight'] = $delInfo['totalQW'];
				}
				 
				if(array_key_exists( 'other_weight', $result[$deliverable['ref']])) {
					$result[$deliverable['ref']]['other_weight'] = $delInfo['totalOW'];
				}
			if( $subCount == 0){
				$result[$deliverable['ref']]['action_progress']     =   0;
			} else{
				$result[$deliverable['ref']]['action_progress']     =   round( ($delInfo['actionProgress'] / $subCount ), 2);
			}
			$result[$deliverable['ref']]['number_of_actions'] 	= ($delInfo['actions'] == "" ? "<span class='error'>&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;</span>" : "<span class='semi'>&nbsp;&nbsp;&nbsp;&nbsp;".$delInfo['actions']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");	
					
			$result[$deliverable['ref']]['deliverable'] 	= $result[$deliverable['ref']]['deliverable']." (".$subCount.")";			
			$result[$deliverable['ref']]['main_with_sub']	= true;			
				
			//push the deliverable data into the main array					
			$delerableArr[$deliverable['ref']] 						= $result[$deliverable['ref']];	
			$delerableArr[$deliverable['ref']]['subd']   			= "no";			
			//	
			$delerableArr[$deliverable['ref']."_sub"]['subarray']	= $subDelArr;	
			$delerableArr[$deliverable['ref']."_sub"]['subd']		= "yes";				
			} else {
				$result[$deliverable['ref']]['hasAccess']			= ($deliverable['owner'] == $_SESSION['tid'] ? 1 : 0);
				$result[$deliverable['ref']]['hasActionAccess']		= $this->_checkActionAccess( $actions );
				$result[$deliverable['ref']]['main_with_sub']	= false;												
				$delerableArr[$deliverable['ref']] 				= $result[$deliverable['ref']];	
				$delerableArr[$deliverable['ref']]['subd']   	= "no";					
			}
		 }
		// group the dliverables 
		$categorizedDeliverabes 	= array();
		$allcategorizedDeliverabes	= array();
		if(isset($_POST['getDetail'])){
			$categorizedDeliverabes 	= $this->_createDetail( $delerableArr , "main");
			$allcategorizedDeliverabes  = $this->_createDetail( $delerableArr, "sub" );			
		}			
		$_contract = $this->_contractInfo( $contract );
		$delerableArr = $this->_sortDeliverables( $delerableArr );	

		echo json_encode( array(
						"deliverables" => $delerableArr,
						"categorized"  => $categorizedDeliverabes,
						"headers" 	   => $this->delHeaderArray,
						"total"		   => $total, 
						"columns"	   => count($this->delHeaderArray),
						"contractDefs" => $contract['contractdefaults'],
						"contract"	   => $_contract,
						"allCategory"  => $allcategorizedDeliverabes,
						"contract"	   => $project_name, 
						"contractData" => $_contract 
 					)
		);
	 }
 
 	function _createDetail( $delivarables, $type )
	{
		$categorizedDeliverabes	= array();
		foreach($delivarables as $key => $delieverable)
		{
			$reference = (isset($delieverable['ref']) ? $delieverable['ref'] : "");
			if( $type == "main"){
				if( isset($delieverable['subarray']) ){
					continue;
				} else{
					$categorizedDeliverabes[$delieverable['deliverable_category']][$delieverable['ref']] = $delieverable;			
				}
			} else {

				 if( isset($delieverable['subarray']) ){
				  foreach( $delieverable['subarray'] as $field => $subDeliverable){						
						
						$categorizedDeliverabes[$subDeliverable['deliverable_category']][$key]['subarray'][$subDeliverable['ref']] = $subDeliverable;	
					$categorizedDeliverabes[$subDeliverable['deliverable_category']][$key]['subd'] =  'yes';
					}		
				} else{
					$categorizedDeliverabes[$delieverable['deliverable_category']][$delieverable['ref']] = $delieverable;			
				}			
			}
		}
		$sortedCategorised = array();
		foreach($categorizedDeliverabes as $category => $catValue)
		{
			$sortedCategorised[$category] = $this->_sortDeliverables( $catValue );
		}	
		return $sortedCategorised;
	} 
 
 	function createContractDetailTable()
	{
		$table = "<table  width='100%'>";
		foreach($delerableArr as $key => $value){
		if( $value['subd'] == "yes"){
		 $table .= "<tr style='background-color:#CCCCCC;'>";
			$table .= "<td colspan='".count($this->delHeaderArray)."'>";
				$table .= "<table width='100%'>"; 
					foreach( $value['subarray'] as $subKey => $subArr){
					 $table .=	"<tr>";
						foreach( $subArr as $x => $r){
						  $table .= "<td style='text-align:center'>".$r."</td>";                    	
						 }          
					$table .= "<td><input type='submit' value='Edit', name='edit' id='name_'.".$key." />&nbsp;&nbsp;&nbsp;";
					$table .= "<input type='submit' value='Delete', name='delete' id='name_'.".$key." /><td>";						
					$table .= "</tr>";
					}
				$table .= "</table>";
			$table .= "</td>";      
		 $table .= "</tr>";	
				} else {
		 $table .= "<tr>"; 
			foreach( $value as $k => $v){
			  //if($value['subd'] || $value['main_with_sub']){
			  // do nothing
			  //} else {
				$table .= "<td>".$v."</td>";                    
			 // }
			}   
			 $table .= "<td><input type='submit' value='Edit', name='edit' id='name_'.".$key." />&nbsp;&nbsp;&nbsp;";
 			 $table .= "<input type='submit' value='Delete', name='delete' id='name_'.".$key." /><td>";			

			 $table .= "</tr>";
			 } 
			}
			$table .= "</table>";
	}
 
 
 	function _sortDeliverables( $deliverableArr )
	{
		$index = 0;
		$indexDeliverable = array();
		foreach( $deliverableArr as $key => $value)
		{
			$indexDeliverable[] = $value;
		}
		return $indexDeliverable;
	}
 
 	function hasActionAccess( $action_owner )
	{
		$userLogged 	= $_SESSION['tid'];
		if($userLogged  == $action_owner){
			return true	;			
		}
		return false;
	}
 
 	function getActions(){
		$aObj 	 		 = $this->loadModel("Action");
		$delObj 	 	 = $this -> loadModel("Deliverable");		
		$headernames 	 = $this -> loadModel("Naming");	
		$headers		 = $headernames -> getHeader();
		$conditions 	 = "";
		$deliverable  	 = $delObj -> getDeliverable( (isset($_POST['id']) ? $_POST['id'] : ""));				
		$actions 		 = $aObj->getActions( (isset($_POST['id']) ? $_POST['id'] : "" ), $conditions);
		
		$actionArray 	 = array(); 
		$headArray  	 = array();
		$actionStatus    = "";
		foreach( $actions as $key => $value )
		{			
			$actionStatus[$value['ref']]    = $value['actionstatus'];
			foreach( $headers as $field => $val)
			{ 
				if( isset($value[$field]) || array_key_exists($field, $value) ){
					if( $field == "deadline"){
						$actionArray[$value['ref']][$field] = $value[$field];
						$headArray[$field]					=  "Action Deadline";
					} else {
						$actionArray[$value['ref']][$field] = $value[$field];
						$headArray[$field]					= $val;						
					}
				}
			}
			$actionArray[$value['ref']]['userAccess']  = $this -> hasActionAccess( $value['owner'] );
		}
		$actionArray = $this->sortActions( $actionArray);
		$headArray 	= $this->sortHeadActions( $headArray);		
		
		echo json_encode( array("actions" 		=> $actionArray, 
								"headers" 		=> $headArray ,
								"deliverable"	=> $deliverable,
								"columns"		=> count( $headArray ),
								"actionstatus"	=> $actionStatus,
								"total"   		=> $aObj->getTotalActions( $_POST['id'] , "" )
							 ) 
						);		
	}
	
	function sortActions( $action )
	{
		$orderActions  = array("ref", "action", "action_owner", "deadline", "action_status", "progress" , "userAccess");
		$orderedAction = array();
		foreach( $orderActions as $key ){
			foreach($action as $k => $val ){
			
				if(isset($val[$key])){
					$orderedAction[$k][$key] = $val[$key];
				} else {
				
				}
			}
		}
		return $orderedAction;
	}
	
	function sortHeadActions( $heads )
	{
		$orderActions  = array("ref", "action", "action_owner", "deadline", "action_status", "progress" );
		$orderedHeaders = array();
		foreach( $orderActions as $key ){
			if(isset($heads[$key])){
				$orderedHeaders[$key] = $heads[$key];
			} else {
			
			}
		}	
		return $orderedHeaders;
	}
 
  	function deleteContract(){
		$obj 	   = $this -> loadModel("Contract");
		$contract  = $obj->getAContract( $_POST['id'] ); 
		$res       = $obj->updateContractDefaults( $_POST['id'], array("contractdefaults" => Contract::CONTRACT_DELETED +$contract['contractdefaults'] ) );
		if( $res == 1){
			$response = array("error" => false, "text" => "Contract deleted . . .");
		} else {
			$response = array("error" => true, "text" => "Error deleting the contract");
		}
		echo json_encode( $response );
	}
 
 	function deleteDeliverable(){
		$obj 		  = $this -> loadModel("Deliverable");
		$deliverable  = $obj->getDeliverable( $_POST['id'] ); 
		$res          = $obj->updateDeliverable($_POST['id'], array(), array("delstatus" => DBConnect::DELETE +$deliverable['delstatus'] ) );
		if( $res == 1){
			$response = array("error" => false, "text" => "Deliverable deleted . . .");
		} else {
			$response = array("error" => true, "text" => "Error deleting the deliverable");
		}
		echo json_encode( $response );
	}
	
 	function  deleteAction(){
		$obj 		  = $this -> loadModel("Action");
		$action  	  = $obj->getAction( $_POST['id'] ); 
		$res          = $obj->updateAction(array("actionstatus" => DBConnect::DELETE +$action['actionstatus'] ) , $_POST['id']);
		if( $res == 1){
			$response = array("error" => false, "text" => "Action deleted . . .");
		} else {
			$response = array("error" => true, "text" => "Error deleting the action");
		}
		echo json_encode( $response );
	}	

 	 function getDeliverableStatus()
	 {
	   $deliveredstatus = $this -> loadModel("DeliverableStatus");
	   echo json_encode( $deliveredstatus -> getActiveDeliverableStatuses() );	 	
	 }
	 
	 function getDeliveredWeights()
	 {
	   $deliveredweight = $this -> loadModel("DeliveredWieght");
	   echo json_encode( $deliveredweight -> getActiveDeliveredWieghts() );
	 }
	 
	 function getQualityWeights()
	 {
	   $qualityweight = $this -> loadModel("QualityWeight");
	   echo json_encode( $qualityweight -> getActiveQualityWeights() );	 	
	 }
	
	function getOtherWeights()
	{
		$otherweight = $this -> loadModel("OtherWeight");
		echo json_encode( $otherweight -> getActiveOtherWeights() );
	}
	
	function saveDeliverable()
	{
		$insertdata  = array();	
		$response 	 = array();
		$remindon    = (isset($_POST['remind_on']) ? $_POST['remind_on'] : "");
		$deliverable = $this -> loadModel("Deliverable");
		$user 		 = $this -> loadModel("UserAccess");		
		$email 		 = $this -> loadModel("Email");			
		$contract	 = $this -> loadModel("Contract");			
		$contractDetails = $contract -> getContract( $_POST['contract_id'] );
		
		// if remind on date is valid
		if( $this->_checkValidity( $contractDetails['completion_date'], $_POST['deadline'] )  ){
			if( $this->_checkValidity( $_POST['deadline'], $remindon) ){
				foreach( $_POST as $key => $value )
				{
					if($key == "deliverableStatus"){
						if( $value == "mainSub"){
							$insertdata['delstatus'] = Deliverable::MAIN_NO_SUB;	
						} else if($value == "main") {
							$insertdata['delstatus'] = Deliverable::MAIN_WITH_SUB;							
						}
					} else if( $key == "main_deliverable" && $value != "" ) {
						$deli = $deliverable->getDeliverable( $value );
						if( !$this->_checkValidity( $deli['deadline'], $_POST['deadline'] ) ){
							
							$response = array("error" => true, "text" => "Deadline date of subdeliverable cannot be greater than that of main deliverable ");		
							echo json_encode( $response	);	
							return false;
						}
						
						if($_POST['category'] !== $deli['deliverable_category']){
							$insertdata['category'] = $deli['deliverable_category'];
						} else{
							$insertdata['category'] = $_POST['category'];					
						}	
						$insertdata['main_deliverable'] = $_POST['main_deliverable'];					
					} else {
						$insertdata[$key] = $value;	
				   }
				}

				$insertdata['insertuser'] = $_SESSION['tid'];	
				$res 		 = $deliverable -> save( $insertdata );
				$mailText	 = "";
				if( $res > 0 ){
				
					$ownerEmail   = $user -> getUser( $contractDetails['owner']); 
					$managerEmail = $user -> getUser( $contractDetails['contract_manager']);
	
					$del   = $deliverable -> getADeliverable( $res );
					$body  = $_SESSION['tkn']." added a new deliverable<br />";
 					$body .= $this->_createBody( $del );
					$body .= "<br /><br />Please log onto Ignite Assist in order to View or Update the Deliverable and to assign any required Action(s) to responsible users \r\n";			
					$body .= "\n";	//blank row above Please log on line
					//echo "Body before sendig it ".$body."\r\n\n";
					if( $email -> to( (isset($ownerEmail['email']) ? $ownerEmail['email']."," : "" )."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Deliverable") -> body( $body ) -> send()  == 1){
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Notification email sent successfully";	
					}	else {
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Error sending mail";	
					}	
					$response  = array("text" => "Deleiverable saved successfully <br />".$mailText, "error" => false, "id" => $res);
				}  else {
					$response  = array("text" => "Error saving the deliverable", "error" => false);		
				}
			} else{
				$response = array("error" => true, "text" => "Remind on date cannot be after than deadline date");				
			}
		} else{
			$response = array("error" => true, "text" => "Delivereable deadline date cannot be after the contract deadline date");							
		}
		echo json_encode( $response );
	}
	
	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;
			
		}
	}
	
	function _createBody( $data )
	{
		$headernames 	 = $this -> loadModel("Naming");	
		$headers		 = $headernames -> getHeader();		
		$body = "";
		
		foreach ($data as $index => $value)
		{
			if(array_key_exists($index, $headers))
			{
				if($value != ""){
					$body .= " ".$headers[$index].": ".$value."<br />\n";
				}
			}
		}
		return $body;
	}
	
	function saveAction()
	{
		$action 	 = $this -> loadModel("Action");
		$insertdata  = array();
		$remindon    = (isset($_POST['remind_on']) ? $_POST['remind_on'] : "" ); 
		
		$deliObj 	 = $this -> loadModel("Deliverable");
		$user 		 = $this -> loadModel("UserAccess");		
		$email 		 = $this -> loadModel("Email");			
		$contract	 = $this -> loadModel("Contract");		
		$deliverable = $deliObj->getDeliverable( $_POST['deliverable_id']);			
		$contractDetails = $contract -> getContract( $deliverable['contract_id'] );		
		
		if( $this->_checkValidity($_POST['deadline'], $remindon)){
			if( $this->_checkValidity($deliverable['deadline'], $_POST['deadline'])) {
			
				foreach( $_POST as $field => $value ){
					$insertdata[$field] = $value;
				} 
				$res 	  = $action -> save( $insertdata );
				$mailText = "";
				if( $res > 0){
					$ownerEmail   = $user -> getUser( $contractDetails['owner']); 
					$managerEmail = $user -> getUser( $contractDetails['contract_manager']);
	
					$body = $_SESSION['tkn']." added a new action<br />";
					$act  = $action->getAAction( $res ); 
 					$body .= $this->_createBody( $act );
					$body .= "<br /><br />Please log onto Ignite Assist in order to View or Update the Action\r\n";								
					if( $email -> to( (isset($ownerEmail['email']) ? $ownerEmail['email']."," : "" )."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Action") -> body( $body ) -> send()  == 1){
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Notification email sent successfully";	
					}	else {
						$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Error sending mail";	
					}	
									
					$response = array("text" => "Action saved successfully <br />".$mailText , "id" => $res, "error" => false);
				} else {
					$response = array("text" => "Error saving the action" , "error" => true);		
				}
			} else {
				$response = array("error" => true, "text" => "Action Deadline date cannot be after than Deliverable deadline date <span class='error'>(".$deliverable['deadline'].")</span>");				
			}
		} else {
			$response = array("error" => true, "text" => "Remind on date cannot be after than deadline date");
		}
		echo json_encode( $response );
	}
	
	function lockContract()
	{
		$contractId = $_POST['id'];
		$contract 	= $this -> loadModel("Contract");
		$user 		= $this -> loadModel("UserAccess");		
		$email 		= $this -> loadModel("Email");	
		$response 		 = array();		
		$contractDetails = $contract -> getAContract( $contractId ); 
		$updateData      = array("contractdefaults" => ($contractDetails['contractdefaults'] + Contract::LOCKED) ); 
		$res 			 = $contract->updateContractDefaults( $contractId , $updateData);

		$mailText		 = ""; 
		if( $res == 1){
			$sendMail = false;
			if(isset($_POST['saveonly'])){
				if($_POST['saveonly'] == "yes"){
					$sendMail  = true;
				}
			}
			if($sendMail){
				$ownerEmail   = $user -> getUser( $contractDetails['contract_owner']); 
				$managerEmail = $user -> getUser( $contractDetails['contract_manager']);
				
				$body = $_SESSION['tkn']." requested approval of contract ".$contractDetails['name'];
				
				if( $email -> to( $ownerEmail['email'].",".$managerEmail['email'] ) -> from("admin@ignite4u.com") -> subject("Request Contract Approval") -> body( $body ) -> send()  == 1){
					$mailText = "Mail sent successfully";
				} else {
					$mailText = "Error sending mail";
				}
			}		
			$response = array("error" => false, "text" => "Confirmation of contract was successful ..<br />".$mailText);
			} else {
			$response = array("error" => true, "text" => "Error updating contract, please try again ");		
		}
		echo json_encode( $response );
	}
	
	function approveContract()
	{
		$contractId 	 = $_POST['id'];
		$contract 	 	 = $this -> loadModel("Contract");
		$user 		     = $this -> loadModel("UserAccess");		
		$email 		     = $this -> loadModel("Email");	
		
		$contractDetails = $contract -> getAContract( $contractId ); 
		$updateData      = array("contractdefaults" => ($contractDetails['contractdefaults'] + Contract::APPROVED) ); 
		$res 			 = $contract->updateContractDefaults( $contractId , $updateData);
		$response 		 = array();
		$mailText		 = ""; 
		if( $res == 1){
		
			$ownerEmail   = $user -> getUser( $contractDetails['contract_owner']); 
			$managerEmail = $user -> getUser( $contractDetails['contract_manager']);
			
			$body = $_SESSION['tkn']." has approved contract ".$contractDetails['name'];
			$body .= "<br /><br />Please log onto Ignite Assist in order to View or Update the Contract\r\n";								
			
			if( $email -> to(  $ownerEmail['email'].",".$managerEmail['email'] ) -> from("admin@ignite4u.com") -> subject("Contract Approval")-> body( $body ) -> send()  == 1){
				$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Notification email sent successfully";
			} else {
				$mailText = " &nbsp;&nbsp;&nbsp;&nbsp; Error sending mail";
			}
					
			$response = array("error" => false, "text" => "Contract approved successfully . . .<br />".$mailText);
			} else {
			$response = array("error" => true, "text" => "Error updating contract, please try again ");		
		}
		echo json_encode( $response );	
	}
	
	function saveTemplate()
	{
		$updateArr 	 = array();
		$contractId  = "";
		$contract 	 = $this -> loadModel("Contract");
		$delObj 	 = $this -> loadModel("Deliverable");
		$actionObj   = $this->loadModel("Action");
		//$contract    = $contract->getContract( (isset($_POST['id']) ? $_POST['id'] : "")  );

		foreach($_POST['data'] as $value)
		{
			if($value['name'] == "contractAppr"){
				$contractId	 = $value['value'];
			} else {
				$updateArr[$value['name']] = $value['value'];
			}
		}
		$deliverables = $delObj -> getDeliverables( $contractId, "");	
		$templateData = array();
				
		foreach($deliverables['deliverable'] as $key => $value)
		{	
			$actions 	  	   = $actionObj->getActions( $value['ref'], "" );						
			$value['actions']  = $actions ; 
			$templateData[]	   = $value; 
		}
		//$templateData  = array( "deliverable" => $deliverables['deliverable'], "actions" => $actionsArray ) ;
		
		$contractDetails = $contract -> getAContract( $contractId ); 

		$updateData		 = array(
							"template_defaults" => serialize($updateArr),
							"contractdefaults"  => ($contractDetails['contractdefaults'] + Contract::TEMPLATE) 
							);
							
		$template 		 = $contract->saveTemplate( array("contract_id" => $contractId , 
														  "name" 		=> $_POST['templatename'],
														  "data" 		=> serialize( $templateData ) 
														)
													); 						

		$res 			 = $contract->updateContractDefaults( $contractId , $updateData);
		$response 		 = array();
		if( $res == 1){
	
			$response = array("error" => false, "text" => "Contract Template Saved successfully ..");
		} else {
			$response = array("error" => true, "text" => "Error saving contract template, please try again");		
		}
		echo json_encode( $response );		
		
	}	
	
	function uploadFile()
	{
		//$response = array();
		//$response = DBConnect::upload("contract_uploads");
		$upload_dir = "../../files/".$_SESSION['cc']."/SLA/contract/";
						
		$key 	  				   = key($_FILES);
		$filename 				   = basename($_FILES[$key]['name']);
		$_SESSION['contract_uploads'][$key.time()] = $_FILES[$key]['name'];
		$fileuploaded 			   = str_replace("attachment_", "", $key)."_".time().".";
		$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
		$response 				   = array();
		//$upload_response 		   = move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext);
		if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
		{			
			$response = array(
								"text" 			=> "File uploaded .....",
								"error" 		=> false,
								"filename"  	=> $filename,
								"key"			=> $key,
								"filesuploaded" => $_SESSION['contract_uploads']
								);
		} else {
			$response = array("text" => "Error uploading file ...." , "error" => true );
		}		
		echo json_encode( $response );		
	}
	
	function _processAttachment( $attachments )
	{
		$attach = unserialize( $attachments );
		$att  	= "";
		if(isset($attach) && !empty($attach)){
			foreach( $attach as $key => $value)
			{
				$att .= $value."<br />";
			}
		}
		return $att;
	}
	
}
$method 	=  $_GET['action'];
$controller = new NewController();
if(method_exists($controller, $method)){
	$controller -> $method();
}


?>