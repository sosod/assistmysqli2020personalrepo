<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		$("#view_delivereable").deliverable({contractId:$("#contractid").val(),  addAction:true, page:"addaction"});
	});
</script>
<h3 id="project_name"></h3>
<div id="view_delivereable"></div>
<form>
	<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />
</form>
<div><?php displayGoBack("", ""); ?></div>