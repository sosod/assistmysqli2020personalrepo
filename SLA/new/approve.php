<?php
$scripts = array("setup.js", "jquery.ui.contract.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		$("#contract_awaiting").contract({activateContract:true, page:"approve"});
	});
</script>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<td valign="top">
			<h4>Contracts awaiting activation</h4>
        	<div id="contract_awaiting"></div>
        </td>
    	<td valign="top">
			<h4>Contracts activated</h4>        
        	<div id="contract_approved"></div>
        </td>        
    </tr>
</table>
</div>
