<?php
$scripts = array("contract.js");
require_once("../header.php");
$ctr 	  = new Contract();
$contract = $ctr -> getContract( $_GET['id'] ); 
$del 	  = new Deliverable();
$deliverebles = $del -> getDeliverables( $_GET['id'] ); 
$summary 	  = array();
$groupedDeliverables = array();
foreach( $deliverebles as $key => $deliverable ){
	$groupedDeliverables[$deliverable['deliverable_category']][$deliverable['ref']] = $deliverable;
}
?>
<div class="displayview">
<form method="post" id="form-contract">
<?php JSdisplayResultObj(""); ?>
<table width="80%">
	<tr>
    	<td valign="top">
        <table id="table_contract" width="65%">
             <input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />            
        </table>
        </td>
        <td valign="top"> 
        <!-- get the deliverables -->
            <table border="1" width="20%">
                <tr>
                	<th>Key Measures</th>
               		<th>Weights</th>    
                </tr>
                <?php 
                $total = 0;
                foreach( $groupedDeliverables  as $cat => $deliverableCat)
                {
                $totalPerCat = 0;
                ?>
                <tr>
                	<td colspan="2">
                		<b><?php echo $cat; ?></b>
                	</td>
                </tr>
                <?php	
                foreach( $deliverableCat as $key => $delValue ){
                ?>
                <tr>
                	<td><?php echo $delValue['deliverable']; ?></td>
                	<td>
                <?php
                $totalPerCat = $totalPerCat + ($delValue['delivered_weight']+$delValue['quality_weight']+$delValue['other_weight']);
                echo ($delValue['delivered_weight']+$delValue['quality_weight']+$delValue['other_weight']); 
                ?>
                	</td>
                </tr>	
                <?php 
                }
                ?>
                <tr>
                	<td></td>
                	<td>
                		<b>
                		<?php
                			$total = $total + $totalPerCat;
               				echo $totalPerCat;
               			?>
                	   </b>
                	</td>        
                </tr>    
                <?php
                }
                ?>
                <tr>
                	<td>Score</td>
                	<td><b><?php echo $total; ?></b></td>    
                </tr>
            </table>
        </td>
    </tr>
     <tr>
   		<td><input type="submit" name="edit_contract" id="edit_contract" value="Edit Contract Details" /></td>
   		<td><input type="submit" name="save_request" id="save_request" value="Save and Request Approval" /></td>        
    </tr>   
    <tr>
   		<td colspan="2"><a href="#" id="view_detaild_contract">View Detailed Contract Details</a></td>
    </tr>
    
</table>
</form>
</div>
<div id="detailedView">
<table  width="80%">
	<?php
		foreach($summary as $k => $val){
		?>
        	<tr>
            	<td><b><?php echo $k; ?></td>
            </tr>
		<?php
            foreach( $deliv as $ind => $value){
         ?>
        <tr>
            <td>
                
                <?php echo $ind; ?>
            </td>
       		<?php 
				foreach( $value as $field => $v) {
			?>
            <td>
                <?php     
					if( $field == "main_deliverable" && $v ==  "") { 
						echo $v;
					} else {
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$v;
					}
                ?>
            </td>   
           <?php 
		   	}
		   ?>                             
        </tr> 
        <?php 
        }
		}
	?>
 </table>
</div>