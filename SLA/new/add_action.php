<?php
$scripts = array("action.js", "jquery.ui.action.js");
require_once("../header.php");
$naming   = new Naming(); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="add-action" name="add-action" method="post">
<table width="100%" class="noborder">
<tr>
    <td valign="top" class="noborder" width="50%">
        <table width="100%">
        	<tr>
        		<td colspan="2"><h4>Deliverable Details</h4></td>
        	</tr>
        	<tr id="goback">
        		<td class="noborder"><?php displayGoBack("", ""); ?></td>
        		<td class="noborder"></td>
        	</tr>
        </table>
    </td>
    <td valign="top" class="noborder">
        <table width="100%">
        	<tr>
        		<td colspan="2"><h4>Add New Action</h4></td>
        	</tr>
            <tr>
                <th>Ref #:</th>
                <td><?php echo $_GET['id']; ?></td>                    
            </tr>
            <tr>
                <th><?php $naming->setName('action'); ?>:</th>
                <td>
                	<textarea rows="7" cols="35" id="action_name" name="action_name" placeholder="enter action name" ></textarea>
                	<!--  <input type="text" value=""  size="50"/> -->
                </td>                    
            </tr>
            <tr>
                <th><?php $naming->setName('action_owner'); ?>:</th>
                <td>
                    <select name="owner" id="owner">
                        <option value="">--owner--</option>
                    </select>
                </td>                    
            </tr>                        
            <tr>
                <th><?php $naming->setName('measurable'); ?>:</th>
                <td>
                	<textarea rows="7" cols="35" name="measurable" id="measurable" placeholder="enter measurable"></textarea>
                	<!-- <input type="text"  value="" size="50" />  -->
                </td>                    
            </tr>                                              
            <tr>
                <th><?php $naming->setName('action_status'); ?>:</th>
                <td>
                    <select name="status" id="status">
                        <option value="1">New</option>
                    </select>
                </td>                    
            </tr>                                                                              
            <tr>
            <th><?php $naming->setName('deadline'); ?>:</th>
            <td><input type="text" name="deadline" id="deadline" placeholder="select deadline" value=""  class="datepicker" size="50"/></td>                    
            </tr>                                                                                               
            <tr>
             <th><?php $naming->setName('remind_on'); ?>:</th>
             <td>
               <input type="text" name="remindon" id="remindon" placeholder="select remind on" value=""  class="datepicker" size="50"/>
            </td>
            </tr>
            <tr>
                <th></th>
                <td>
	                <input type="submit" name="add" id="add" value="Save" class="saveaction isubmit"/>
	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                <?php
	                 if(isset($_GET['contractid']) ) { 
	                 ?>
	                     <input type="submit" name="addnext" id="addnext" value="Save & Next" class="saveaction"/>
	                <?php } ?>
	                <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>" />
	                <input type="hidden" name="contractid" id="contractid" value="<?php
	                 echo (isset($_GET['contractid']) ? $_GET['contractid'] : ""); ?>" />
                </td>
            </tr>                                                                                               
        </table>
    </td>
</tr>
<tr>
	<td colspan="2" class="noborder">
		<script language="javascript">
			$(function(){	
			  $("#displayaction").action({delId:$("#deliverableid").val(), editAction:true, deleteAction:true})
			})
		</script>
		<div id="displayaction"></div>        		
	</td>
</tr>
</table>
</form>
</div>
