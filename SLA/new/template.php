<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
$contract      = new Contract();
$conditions	   = "AND contractdefaults & ".Contract::APPROVED." = ".Contract::APPROVED."";
$conditions	  .= " AND contractdefaults & ".Contract::TEMPLATE." <> ".Contract::TEMPLATE."";
$approvedContr = $contract -> getContracts( 0, 50, $conditions );
?>
<script language="javascript">
	$(function(){
		var id = "";
		$("#contractAppr").change(function(){
			if( $(this).val() == ""){
				$("#viewdelivereable").html("No data for this contract");
			} else {
				$("#buttons").show();
				$("#viewdelivereable").deliverable({required:true, editable:true, contractId:$(this).val(), page:"template"});	
			}
			 return false;
		});

		$("#savetemplate").click(function(e){
			e.preventDefault();				  	
			
			$("#template").dialog({
				title	 : "Template",
				autoOpen : true,
				height	 : "auto",
				width	 : "400px",
				buttons	 : {
							"Save" : function(){
							
								$(this).append($("<div />",{id:"dialogLoader", html:"Saving . . <img src='../images/loaderA32.gif' />"}))
								if( $("#t_name").val() == ""){
									$("#dialogLoader").html("Please enter the template name");
									return false;
								} else {
								//===============================================================
									$.post("controller.php?action=saveTemplate", {
												data 			: $("#save_template").serializeArray(),
												templatename	: $("#t_name").val()
												 } , function( response ){
										if( !response.error ){
											jsDisplayResult("ok", "ok", response.text );
											$("#dialogLoader").remove();
											$("#template").dialog().dialog("destroy");
										} else{
											jsDisplayResult("error", "error", response.text );
											$("#dialogLoader").html( response.text );										
										}
									},"json");
								// =========================================================
								}
							} , 
							"Cancel"	: function(){
								$("#template").dialog().dialog("destroy");
							}
							
						} 
				})
		});
		
		$("#cancel").click(function(){
			document.location.href = "confirmation.php?id="+$("#contractid").val();
			return false;
		});
	});
</script>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>   
<form id="save_template" name="save_template">
<table class="noborder">
	<tr>
    	<th class="noborder">Select Contract to save as template: </th>
    	<td class="noborder">
        <select id="contractAppr" name="contractAppr">
            <option value="">--contract--</option>
            <?php 
            foreach( $approvedContr as $key => $contract){
            ?>
            	<option value="<?php echo $contract['ref']; ?>"><?php echo $contract['contract']; ?></option>    
            <?php
            }
            ?>
        </select>        	
        </td>
    </tr>
</table>
<table class="noborder"> 
	<tr>
    	<td class="noborder">
            <span id="loading"></span>
            <div id="viewdelivereable"></div>
        </td>     
    </tr>
    <tr id="buttons" style="display:none;">                 	                                        
    <td class="noborder">
            <input type="button" name="savetemplate" id="savetemplate" value="Save As Template" class="savedeliverable" />
            <input type="button" name="cancel" id="cancel" value="Cancel" class="savedeliverable" />
        </td>
    </tr>                    
</table>
</form>
 </div>
 <div id="template" style="display:none;">
 	<table width="100%">
    	<tr>
        	<th>Enter template name:</th>
        	<td><input type="text" name="t_name" id="t_name" value="" /></td>            
        </tr>
    </table>
 </div>