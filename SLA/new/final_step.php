<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		$("#viewdelivereable").deliverable({required:true, editable:true, contractId:$("#contractid").val()});	
		
		$("#savetemplate").click(function(e){
			e.preventDefault();
			/*					
			$("#template_dialog").append($("<table />")
						  .append($("<tr />")
						  	.append($("<td />",{html:"Enter the name of template "}))
						  )
						  .append($("<tr />")
						  	.append($("<td />")
							  .append($("<input />",{type:"text", id:"t_name", name:"t_name", value:""}))
							)
						  )						  
			).dialog({
					  autoOpen : true, 
					  
					  })
			*/
			$.post("controller.php?action=saveTemplate", { data : $("#save_template").serializeArray() } , function( response ){
				if( !response.error ){
					$("#loading").html( response.text )
				} else{
					$("#loading").html( response.text )
				}
			},"json");
		});
		
		$("#cancel").click(function(){
			document.location.href = "confirmation.php?id="+$("#contractid").val();
			return false;
		});
	});
</script>
<form id="save_template" name="save_template">
<span id="loading"></span>
<div id="viewdelivereable"></div>
<div class="displayview">
<table>
    <tr>      
        <td></td>              	                                        
        <td colspan="7">
            <input type="button" name="savetemplate" id="savetemplate" value="Save As Template" class="savedeliverable" />
            <input type="button" name="cancel" id="cancel" value="Cancel" class="savedeliverable" />
            <input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']?>" />
        </td>
    </tr>                    
</table>
</div>
</form>
<div id="template_dialog"></div>
