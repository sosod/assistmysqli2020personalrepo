<?php
$scripts = array("contract.js");
require_once("../header.php");
$ctr 		= new Contract(); 
$contract   = $ctr -> getAContract( $_GET['id'] );
$ty 		= new ContractType(); 
$types  	= $ty -> getContractTypes(); 
$cat 		= new ContractCategory(); 
$categories	= $cat -> getCategories(); 
$sup 		= new ContractType(); 
$suppliers  = $sup -> getContractTypes(); 
$us 		= new UserAccess(); 
$users  	= $us -> getUsers(); 
$assfreq	= new AssessmentFrequency(); 
$assfreqs  	= $assfreq -> getAssessmentFrequencies(); 
$noti	    = new Notification(); 
$notifications  = $noti->getNotifications(); 
$assessFreq  = "";
?>
<div class="displayview">
<form method="post" id="form-contract">
<span id="loading"></span>
<table id="table_contract_edit">
	<tr>
    	<td colspan="2"><h4>Step 1 : Activation of the contract on the SLA system </h4></td>
    </tr>
	<tr>
    	<td colspan="2"><h6>Please complete the following fields to add a new contract on the SLA system</h6></td>
    </tr>
    <tr>
    	<th>Ref:</th>
        <td>#<?php echo $_GET['id']; ?></td>
    </tr>        
    <tr>
    	<th>Project Name :</th>
        <td><input type="text" name="name" id="name" placeholder="project name" value="<?php echo $contract['name']; ?>" /></td>
    </tr>
    <tr>
    	<th>Project Value <em>(Budger)</em> :</th>
        <td><input type="text" name="budget" id="budget" value="<?php echo $contract['budget']; ?>" /></td>
    </tr>
    <tr>
    	<th>Comments clarifying the Project :</th>
        <td><textarea name="comments" id="comments"><?php echo $contract['comments']; ?></textarea></td>
    </tr>
    <tr>
    	<th>SLA Type :</th>
        <td>
        	<select name="type" id="type">
            	<option value="">--select sla type--</option>            
            <?php
			foreach( $types as $type){
			?>
            	<option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $contract['type'] ) { ?> selected="selected" <?php  }?>><?php echo $type['name']; ?></option>			
            <?php
			}
			?>                
            </select>
        </td>
    </tr>
    <tr>
    	<th>SLA Category :</th>
        <td>
        	<select name="category" id="category">
            	<option value="">--select sla category--</option>
            <?php
			foreach( $categories as $category){
			?>
            	<option value="<?php echo $category['id']; ?>" <?php if($category['id'] == $contract['category'] ) { ?> selected="selected" <?php  }?>><?php echo $category['name']; ?></option>			
            <?php
			}
			?>                 
            </select>
        </td>
    </tr>  
    <tr>
    	<th>Supplier :</th>
        <td>
        	<select name="supplier" id="supplier">
            	<option value="">--select sla suppier--</option>
            <?php
			foreach( $suppliers as $supply){
			?>
            	<option value="<?php echo $supply['id']; ?>" <?php if($supply['id'] == $contract['supplier'] ) { ?> selected="selected" <?php  }?>><?php echo $supply['name']; ?></option>			
            <?php
			}
			?>                 
            </select>
        </td>
    </tr>       
    <tr>
    	<th>Date Signature of Tender Document :</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signnature_of_tender" name="signnature_of_tender" class="datepicker"  value="<?php echo $contract['signnature_of_tender']; ?>"/>
        </td>
    </tr> 
    <tr>
    	<th>Date Signature of SLA Document :</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signature_of_sla" name="signature_of_sla" class="datepicker" value="<?php echo $contract['signature_of_sla']; ?>" />
        </td>
    </tr> 
    <tr>
    	<th>Contracted Completion Date :</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="completion_date" name="completion_date" class="datepicker" value="<?php echo $contract['completion_date']; ?>" />
        </td>
    </tr>  
    <tr>
    	<th>Contract Manager :</th>
        <td>
        	<select name="contract_manager" id="contract_manager">
            	<option value="">--select contract manager--</option>
            <?php
			foreach( $users as $user){
				if(($user['status'] & 2) == 2){
			?>
            	<option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $contract['contract_manager'] ) { ?> selected="selected" <?php  }?>><?php echo $user['tkname']." ".$user['tksurname']; ?></option>			
            <?php
				}
			}
			?>                   
            </select>
        </td>
    </tr>     
    <tr>
    	<th>Contract Owner :</th>
        <td>
        	<select name="contract_owner" id="contract_owner">
            	<option value="">--select person responsible--</option>
            <?php
			foreach( $users as $user){
			?>
            	<option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $contract['contract_owner'] ) { ?> selected="selected" <?php  }?>><?php echo $user['tkname']." ".$user['tksurname']; ?></option>			
            <?php
			}
			?>                     
            </select>
        </td>
    </tr> 
    <tr>
    	<th>Contract Authorisor 1 :</th>
        <td><span id="contract_authorisor_1"></span></td>
    </tr>  
    <tr>
    	<th>Contract Authorisor 2 :</th>
        <td>        	
        <select name="contract_authorisor" id="contract_authorisor">
            <option value="">--select contract authorisor--</option>
            <?php
			foreach( $users as $user){
			?>
            	<option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $contract['contract_authorisor'] ) { ?> selected="selected" <?php  }?>><?php echo $user['tkname']." ".$user['tksurname']; ?></option>			
            <?php
			}
			?>                     
        </select>
        </td>
    </tr>         
    <tr>
    	<th>Template to be used :</th>
        <td>
        	<select name="template_" id="template_">
            	<option value="">--select template--</option>
            </select>
        </td>
    </tr> 
    <tr>
    	<td colspan="2"><b>What deliverable status must be assesed</b></td>
    </tr>
  <!-- <div id="deliverable_status"></div> -->
  <tr>
  	<td id="deliverable_status" colspan="2"></td>
  </tr>
   
    <tr>
    	<th>
        	What is the Assessment Frequency for this Contract :
            <!--<em>
            If weekly , select day of week and time to be send<br />
            If monthly , select day of month and time to be send<br />
            If Quartely , select day and time to be send etc.
            </em> -->
        </th>
        <td>

    	<select name="assessment_frequency" id="assessment_frequency">
            	<option value="">--assessment frequency--</option>
				<?php
                foreach( $assfreqs as $assfreq){
                ?>
                    <option value="<?php echo $assfreq['id']; ?>"
                     <?php 
					 if($assfreq['id'] == $contract['assessment_frequency'] ) { 
						$assessFreq =  $assfreq['name'] ;
					?> 
                    	selected="selected" 
					<?php  
						}
					?>>
						<?php echo $assfreq['name']; ?>
                    </option>			
                <?php
                }
                ?>                  
            </select>
        </td>
    </tr> 
    <tr id="weekly" style="display:<?php echo ($assessFreq == "Weekly" ? "inline" : "none") ?>;" class="assfreq" >
    	<th>Date : </th>
        <td><input type="text" name="weeklydate" id="weeklydate" class="assessment_frequency_date" value="<?php echo $contract['assessment_frequency_date']." ".$contract['assessment_frequency_time']; ?>" /></td>
    </tr>                                
    <tr id="monthly" style="display:<?php echo ($assessFreq == "Monthly" ? "inline" : "none") ?>;;" class="assfreq">
    	<th>Date :</th>
        <td><input type="text" name="monthlydate" id="monthlydate" class="assessment_frequency_date" value="<?php echo $contract['assessment_frequency_date']." ".$contract['assessment_frequency_time']; ?>"/></td>
    </tr>                                
    <tr id="quarterly" style="display:<?php echo ($assessFreq == "Quartely" ? "inline" : "none") ?>;;" class="assfreq">
    	<th>Date :</th>
        <td><input type="text" name="quarterlydate" id="quarterlydate" class="assessment_frequency_date" value="<?php echo $contract['assessment_frequency_date']." ".$contract['assessment_frequency_time']; ?>" /></td>
    </tr>                             
   <tr>
    	<th>
        	What is the Notification Frequency for this contract :
        </th>
        <td>
        	<select name="notification_frequency" id="notification_frequency">
            	<option value="">--select notification frequency--</option>
                <?php
				foreach($notifications as $key => $notification){
				?>
            	<option value="<?php echo $notification['id']; ?>" <?php if($notification['id'] == $contract['notification_frequency']) { ?> selected="selected" <?php  } ?>>
					<?php echo $notification['description']; ?>
                </option>                
                <?php } ?>
            </select>
        </td>
    </tr>
     <tr>
     <th>Attach a document
     </th>
     <td>
     	<span id="uploading"></span>
     	<input type="file" class="attach" name="attachment" id="attachment" />
     </td>
    </tr> 
     <tr>
     <td><?php displayGoBack("", ""); ?></td>
     <td>
     	<input type="submit" id="edit_details" name="edit_details" value="Edit Details" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />        
     </td>
    </tr>                                            
</table>
</form>
</div>
