<?php
$scripts = array("contract.js");
require_once("../header.php");
$ctr 	  = new Contract();
$contract = $ctr -> getContract( 41 ); 
$del 	  = new Deliverable();
$deliverebles = $del -> getDeliverables( 41 ); 
$summary 	  = array();
foreach($deliverebles as $key => $value){

	$summary[$value['contract_type']][$value['deliverable']] = array(
																	"owner"			   => $value['deliverable_owner'],
																	"deadline"		   => $value['deadline'],
																	"actions"		   => "",
																	"delivered_weight" => $value['delivered_weight'],
																	"quality_weight"   => $value['quality_weight'],
																	"other_weight" 	   => $value['other_weight'],
																	"main_deliverable" => $value['main_deliverable']																	
																	);
	
}

?><div id="detailedView">
<table  width="80%">
	<?php
		foreach($summary as $k => $deliv){
		?>
        	<tr>
            	<td><b><?php echo $k; ?></b></td>
            </tr>
		<?php
            foreach( $deliv as $ind => $value){
         ?>
        <tr>
            <td>
                
                <?php echo $ind; ?>
            </td>
       		<?php 
				foreach( $value as $field => $v) {
			?>
            <td>
                <?php     
					if( $field == "main_deliverable" && $v ==  "") { 
						echo $v;
					} else {
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$v;
					}
                ?>
            </td>   
           <?php 
		   	}
		   ?>                             
        </tr> 
        <?php 
        }
		}
	?>
 </table>
</div>