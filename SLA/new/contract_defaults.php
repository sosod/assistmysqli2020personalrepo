<?php
$scripts = array("contract.js");
require_once("../header.php");
?>
<div class="displayview">
<form method="post" id="form-contract">
<?php JSdisplayResultObj(""); ?>
<table id="table_contract_defaults">
	<tr>
    	<td colspan="2"><h4>Step 2 : Contract Defaults</h4></td>
    </tr>
    <tr>
    	<th>Does this contract have deliverables ?:</th>
        <td>
        	<select name="hasDeliverables" id="hasDeliverables">
            	<option value="1">Yes</option>
            	<option value="0">No</option>                
            </select>
        </td>
    </tr>
    <tr>
    	<th>Does this contract have sub-deliverables ?:</th>
        <td>
        	<select name="hasSubDeliverables" id="hasSubDeliverables">
            	<option value="1">Yes</option>
            	<option value="0">No</option>                
            </select>        
		</td>
    </tr>
    <tr>
    	<th>Must weights be assigned to this contract ?:</th>
        <td>
        	<select name="weightsAssigned" id="weightsAssigned">
            	<option value="1">Yes</option>
            	<option value="0" selected>No</option>                
            </select>        
        </td>
    </tr>
    <tr class="weight_dependent">
    	<th>Delivered:</th>
        <td>
        	<select name="delivered" id="delivered">
            	<option value="1">Yes</option>
            	<option value="0">No</option>                
            </select>        
        </td>
    </tr>
    <tr class="weight_dependent">
    	<th>Quality:</th>
        <td>
        	<select name="quality" id="quality">
            	<option value="1">Yes</option>
            	<option value="0">No</option>                
            </select>        
        </td>
    </tr>  
    <tr class="weight_dependent">
    	<th>Other:</th>
        <td>
        	<select name="other" id="other">
            	<option value="1">Yes</option>
            	<option value="0" selected>No</option>                
            </select>        
        </td>
    </tr>        
    <tr class="other_dependent">
    	<th>Other Category:</th>
        <td>
        	<input type="text" placeholder="other category name" id="othercategory" name="othercategory" />
        </td>
    </tr>   
     <tr>
     <td></td>
     <td>
     	<input type="hidden" id="contractid" name="contractid" value="<?php echo $_GET['id']; ?>" />
     	<input type="submit" id="savedefaults" name="savedefaults" value="Step 3" class="isubmit" />
     </td>
    </tr>                                            
</table>
</form>
</div>