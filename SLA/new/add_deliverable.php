<?php
$scripts = array("deliverable.js", "jquery.ui.deliverable.js");
require_once("../header.php");

$cont    = new Contract();
$contract = $cont->getAContract($_GET['id']);
$naming   = new Naming(); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
	<table width="100%" class="noborder">
    	<tr>
        	<td valign="top" class="noborder" width="50%">
            	<table id="table_contract" width="100%">
                	<tr>
                		<td colspan="2"><h4>Contract Details</h4></td>
                	</tr>  
                	<tr id="goback">
                		<td class="noborder"><?php displayGoBack("",""); ?></td>
                		<td class="noborder"></td>                		
                	</tr>                                     
                </table>
            </td>
            <td valign="top" class="noborder" width="50%">
            	<table width="100%">
            		<tr>
            			<td colspan="2"><h4>Add New Deliverable</h4></td>
            		</tr>
                	<tr>
                    	<th width="40%">Ref #:</th>
                    	<td width="60%"><?php echo $_GET['id']; ?></td>                        
                    </tr>
                    <tr>
                    	<th valign="top"><?php $naming->setName('deliverable'); ?>:</th>
						<td>
							<textarea cols="35" rows="7" placeholder="enter delivereable" name="name" id="name" rows="10" cols="5"></textarea>
							<!--  <input type="text"   value="" size="50" /> -->
						</td>                        
                    </tr>
                    <tr>
                    	<th valign="top"><?php $naming->setName('deliverable_description'); ?>:</th>
                    	<td><textarea cols="35" rows="7" name="description" id="description" placeholder="enter description"></textarea></td>                        
                    </tr>
                    <tr>
                    	<th><?php $naming->setName('deliverable_category'); ?>:</th>
                    	<td>
                        	<select name="category" id="category">
                            	<option value="">--category--</option>                             
                            </select>
                        </td>                        
                    </tr>                    
                    <tr>
                    	<th><?php $naming->setName('deliverable_type'); ?>:</th>
                    	<td>
                        	<div id="deliverableDialog"></div>
                        	<select name="type" id="type">
                            	<option value="">--type--</option>
                            	<option value="0">Main</option>                                
                            	<option value="1">Sub</option>                                
                            </select>
                        </td>                        
                    </tr>
                    <tr>
                    	<th><?php $naming->setName('main_deliverable'); ?>:</th>
                    	<td>
                        	<select name="main_deliverable" id="main_deliverable">
                            	<option value="">--select main deliverable--</option>
                            </select>
                        </td>                        
                    </tr>
                    <tr>
                    	<th><?php $naming->setName('deliverable_owner'); ?>:</th>
                    	<td>
                        	<select name="owner" id="owner">
                            	<option value="">--owner--</option>
                            </select>
                        </td>                        
                    </tr>
                    <?php 
	if((Contract::DELIVERED &  $contract['contractdefaults']) == Contract::DELIVERED ){
	?>
                    <tr class="delweights">
                    	<th><?php $naming->setName('delivered_weight'); ?>:</th>
                    	<td>
                        	<select name="weight" id="weight">
                            	<option value="">--weight--</option>
                            </select>                            
                        </td>                        
                    </tr>
                    <?php
		}
	if((Contract::QUALITY &  $contract['contractdefaults']) == Contract::QUALITY ){						
	 ?>
                    <tr class="delweights">
                    	<th><?php $naming->setName('quality_weight'); ?>:</th> 
                    	<td>
                        	<select name="quality_weight" id="quality_weight">
                            	<option value="">--weight--</option>
                            </select>                              	
                        </td>                            
                   </tr> 
                   <?php 
   	}
	if((Contract::OTHER &  $contract['contractdefaults']) == Contract::OTHER ){					
   ?>     
                   <tr class="delweights">                                                                      
                    	<th><?php $naming->setName('other_weight'); ?>:</th>
                    	<td>
                        	<select name="other_weight" id="other_weight">
                            	<option value="">--weight--</option>
                            </select>                              	
                        </td>                          
                   </tr>
                   <?php 
    }
   ?>
                   <tr>
                    	<th><?php $naming->setName('deadline'); ?>:</th>
                        <td><input type="text" name="deadline" id="deadline" placeholder="enter deadline"  value="" class="datepicker"  size="50" /></td>
					</tr>                      
                    <tr>
                        <th><?php $naming->setName('remind_on'); ?>:</th>
                        <td><input type="text" name="remind_on" id="remind_on" placeholder="enter remind on" value="" class="datepicker" size="50" /></td>                     
                	<tr>      
                    	<th></th>              	                                        
                        <td>
                        	<input type="submit" name="add" id="add" value="Save" class="savedeliverable isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        	<input type="submit" name="addnext" id="addnext" value="Save & Next" class="savedeliverable isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                            
                            <input type="submit" name="next" id="next" value="Next " />
                            <input type="hidden" name="page" value="adddeliverable"  id="page" />
                            <input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']?>" />
                        </td>
                    </tr>                    
                </table>
            </td>
        </tr>
    </table>
</div>
<div id="viewdelivereable"></div>