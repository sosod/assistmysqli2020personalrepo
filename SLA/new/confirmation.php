<?php
$scripts = array("contract.js", "jquery.ui.contract.js");
require_once("../header.php");

?>
<?php JSdisplayResultObj(""); ?>
<table width="80%" id="contractDetail" style="display:none;" class="noborder">
	<tr>
    	<td valign="top" class="noborder">
        	<table id="table_contract" width="100%"></table>
        </td>
    	<td valign="top" class="noborder">
        	<table id="contract_summary" width="100%"></table>        
        </td>        
    </tr>
</table>
<script language="javascript">
	$(function(){
		$("#contract").contract({editDeliverable : true, editAction:true, page:"confirmation"});
	})
</script>
<?php
	if(isset($_GET['contractid']) && !empty($_GET['contractid'])){
?>
	<script language="javascript">
        $(function(){
            $("#contract").contract({editDeliverable : true, editAction:true, page:"displayless", goto:"categorized", contractId:<?php echo $_GET['contractid']; ?> });
        })
    </script>
<?php		
	}
?>
<div id="contract">
	<div id="display_select"></div><br />
	<div id="contractview"></div>
</div>