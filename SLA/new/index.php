<?php
$scripts = array("contract.js", "ajaxfileupload.js", "loader.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-contract" enctype="multipart/form-data">
<table id="contract_table">
	<tr>
    	<td colspan="2"><h4>Step 1 : Activation of the contract on the SLA system </h4></td>
    </tr>
	<tr>
    	<td colspan="2"><h6>Please complete the following fields to add a new contract on the SLA system</h6></td>
    </tr>    
    <tr>
    	<th valign="top">Project Name:</th>
        <td>
        	<textarea placeholder="enter project name" rows="7" cols="35" name="name" id="name"></textarea>
        </td>
    </tr>
    <tr valign="top">
    	<th>Project Value <em>(Budget)</em>:</th>
        <td>
	        <input type="text" name="budget" id="budget" placeholder="enter project budget" value="" />     
	        <!--  <input type="text" name="budget" id="budget" placeholder="enter project budget" value="" />-->
        </td>
    </tr>
    <tr>
    	<th valign="top">Comments clarifying the Project:</th>
        <td><textarea rows="7" cols="35" name="comments" id="comments" placeholder="enter project comments"></textarea></td>
    </tr>
    <tr>
    	<th>SLA Type:</th>
        <td>
        	<select name="type" id="type">
            	<option value="">--select sla type--</option>
            </select>
        </td>
    </tr>
    <tr>
    	<th>SLA Category:</th>
        <td>
        	<select name="category" id="category">
            	<option value="">--select sla category--</option>
            </select>
        </td>
    </tr>  
    <tr>
    	<th>Supplier:</th>
        <td>
        	<select name="supplier" id="supplier">
            	<option value="">--select sla suppier--</option>
            </select>
        </td>
    </tr>        
    <tr>
    	<th>Date Signature of Tender Document:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signnature_of_tender" name="signnature_of_tender" class="datepicker" />
        </td>
    </tr>   
    <tr>
    	<th>Date Signature of SLA Document:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="signature_of_sla" name="signature_of_sla" class="datepicker" />
        </td>
    </tr> 
    <tr>
    	<th>Contracted Completion Date:</th>
        <td>
        	<input type="text" placeholder="<?php echo date("d-F-Y"); ?>" id="completion_date" name="completion_date" class="datepicker" />
        </td>
    </tr>  
    <tr>
    	<th>Contract Manager:</th>
        <td>
        	<select name="contract_manager" id="contract_manager">
            	<option value="">--select contract manager--</option>
            </select>
        </td>
    </tr>     
    <tr>
    	<th>Contract Owner:</th>
        <td>
        	<select name="contract_owner" id="contract_owner">
            	<option value="">--select contract owner--</option>
            </select>
        </td>
    </tr> 
    <tr>
    	<th>Contract Authorisor 1:</th>
        <td><span id="contract_authorisor_1"></span></td>
    </tr>  
    <tr>
    	<th>Contract Authorisor 2:</th>
        <td>        	
        <select name="contract_authorisor" id="contract_authorisor">
            	<option value="">--select contract authorisor--</option>
        </select>
        </td>
    </tr>         
    <tr>
    	<th>Template to be used:</th>
        <td>
        	<select name="template_" id="template_">
            	<option value="">--select template--</option>
            </select>
        </td>
    </tr> 
    <tr class="delStatus">
    	<td colspan="2"><b>What deliverable status must be assesed:</b></td>
    </tr>
  <!-- <div id="deliverable_status"></div> -->
  <tr class="delStatus">
  	<td id="deliverable_status" colspan="2"></td>
  </tr>
    <tr>
    	<th>What is the Assessment Frequency for this Contract: 
    	   <!--<em>
            If weekly , select day of week and time to be send<br />
            If monthly , select day of month and time to be send<br />
            If Quartely , select day and time to be send etc.
            </em> -->
        </th>
        <td>
        	<select name="assessment_frequency" id="assessment_frequency">
            	<option value="">--assessment frequency--</option>
            </select>
        </td>
    </tr> 
    <tr id="weekly" style="display:none;" class="assfreq">
    	<th>Start Date:</th>
        <td><input type="text" name="weeklydate" id="weeklydate"  class="datepicker"/></td>
    </tr>                                
    <tr id="monthly" style="display:none;" class="assfreq">
    	<th>Start Date: </th>
        <td><input type="text" name="monthlydate" id="monthlydate" class="datepicker" /></td>
    </tr>                                
    <tr id="quarterly" style="display:none;" class="assfreq">
    	<th>Start Date:</th>
        <td><input type="text" name="quarterlydate" id="quarterlydate" class="datepicker" /></td>
    </tr>                                        
    <tr>
    	<th>
        	What is the Notification Frequency for this contract:
        </th>
        <td>
        	<select name="notification_frequency" id="notification_frequency">
            	<option value="">--select notification frequency--</option>
            </select>
        </td>
    </tr> 
     <tr>
     <th>Attach a document:
     </th>
     <td>
            <div id="uploading"></div>
            <div id="attachments"></div>
            <input type="file"  name="attachment_<?php echo time(); ?>" id="attachment"  class="attach"/> 
           <!--  <input type="button" name="addanother" id="addanother" value="Add Another" />  -->
            <input type="hidden" name="fireUpload" value="true"  />    
     </td>
    </tr> 
     <tr>
     <th></th>
     <td>
     	<input type="submit" id="save" name="save" value="Save" class="savecontract isubmit" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" id="savenext" name="savenext" value="Save & Next" class="savecontract isubmit"/>
     </td>
    </tr>                                            
</table>
</form>
</div>