<?php
				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'deadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'action'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'action_owner'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'progress'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'action_status'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

$sql = "SELECT name, client_terminology FROM ".$dbref."_naming";
$rs = getRS($sql);
	while($row = mysql_fetch_array($rs)) {
		if(isset($head[$row['name']])) {
			$head[$row['name']]['text'] = $row['client_terminology'];
		}
	}
mysql_close();

	$sql = "SELECT 	SLA.id ,
			SLA.name,
			SLA.status,
			SLA.deadline,
			SLA.progress,
			SLA.status AS statusid,
			RAS.name as status,
			CONCAT(TK.tkname,' ',TK.tksurname) AS action_owner,
			SLA.owner
			FROM  ".$dbref."_action SLA 
			LEFT JOIN ".$dbref."_action_status RAS ON SLA.status = RAS.id
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON  TK.tkid  = SLA.owner 
			WHERE SLA.owner = '$tkid' AND SLA.deadline <= ($today + 3600*24*$next_due)  	
			ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " SLA.deadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " SLA.deadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
	$tasks = mysql_fetch_all_fld($sql,"id");
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
	foreach($tasks as $id => $task) {
		$deadline = "";
	    $stattusid = $task['statusid'];		
		if($stattusid !== "3"){
			$deaddiff = $today - strtotime($task['deadline']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff<0) {
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span class=soon>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class=today><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
			}			
			$actions[$id] = array();
			$actions[$id]['ref'] = $id;
			$actions[$id]['deadline'] = $deadline;
			$actions[$id]['action'] = $task['name'];
			$actions[$id]['action_owner'] = $task['action_owner'];
			$actions[$id]['progress'] = $task['progress']."%";
			$actions[$id]['action_status'] = ($task['status'] == "" ? "New" : $task['status'] );
			$actions[$id]['link'] = "manage/update_action.php?id=".$id;		
		}
	}					

/*//$head = array('tasktopicid'=>array(),'taskstatusid'=>array(),'taskaction'=>array(),'taskdeliver'=>array());
//$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND field in ('tasktopicid','taskstatusid','taskaction','taskdeliver') ORDER BY mysort";
$head = array("action" => array(), "action_owner"=> array(),  "progress"=> array(), "action_status"=> array());
$sql = "SELECT name, client_terminology FROM ".$dbref."_naming";
$rs = getRS($sql);
	while($row = mysql_fetch_array($rs)) {
		if(isset($head[$row['name']])) {
			$head[$row['name']] = $row;
		}
	}
mysql_close();
echo "<table cellpadding=3 	cellspacing=0  width='740'>";
	echo "<tr>";
		echo "<th>Ref</th>";
		echo "<th>Deadline</th>";
		foreach($head as $h) {
			echo "<th>".$h['client_terminology']."</th>";
		}
		echo "<th>&nbsp;</th>";
	echo "</tr>";
	$sql = "SELECT 	SLA.id ,
			SLA.name,
			SLA.status,
			SLA.deadline,
			SLA.progress,
			SLA.status AS statusid,
			RAS.name as status,
			CONCAT(TK.tkname,' ',TK.tksurname) AS action_owner,
			SLA.owner
			FROM  ".$dbref."_action SLA 
			LEFT JOIN ".$dbref."_action_status RAS ON SLA.status = RAS.id
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON  TK.tkid  = SLA.owner 
			WHERE SLA.owner = '$tkid' AND SLA.deadline <= ($today + 3600*24*7)  	
			ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " SLA.deadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " SLA.deadline ASC "; break;
					}
					$sql.= " LIMIT ".$action_profile['field2'];
//SLA.deadline ASC LIMIT 15";

	$rs = getRS($sql);
	if(mysql_num_rows($rs)==0) {
		echo "<tr>";
			echo "<td colspan=".(count($head)+3).">Nothing due for the next 7 days.</td>";
		echo "</tr>";
	} else {
		$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
	$tasks	= array();
	while($results = mysql_fetch_assoc($rs))
	{
		$tasks[$results['id']] = $results; 	
	}
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
	foreach($tasks as $id => $task)
	{
		$deadline = "";
	    $stattusid = $task['statusid'];		
		if($stattusid !== "3"){
			$deaddiff = $today - strtotime($task['deadline']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff<0) {
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class=today><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
			}			

		echo "<tr>";
			echo "<th>".$id."</th>";
			echo "<td>".$deadline."</td>";
			echo "<td>".$task['name']."</td>";
			echo "<td>".$task['action_owner']."</td>";
			echo "<td>".$task['progress']."%"."</td>";
			echo "<td>".($task['status'] == "" ? "New" : $task['status'] )."</td>";
			//On button click, call function viewTask & pass variables ($mod, $modloc, page_address)
			echo "<td class=center><input type=button value=View onclick=\"viewTask('$mod','$modloc','/manage/update_action.php?id=".$id."');\"></td>";		
		echo "</tr>";
	  }
	}		
	}	//end mnr if
	mysql_close();
echo "</table>";
*/
?>