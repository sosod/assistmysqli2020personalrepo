<html>
<head>
<?php
@session_start();
include_once("inc_ignite.php");
?>
<script type="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/library/jquery/js/common.js"></script>
<script type="text/javascript" src="/library/jquery/js/jquery-ui-timepicker-addon.js"></script>
<link href="/library/jquery/css/jquery-ui-date.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php
$scripts = array_merge( array("json2.js", "default.js"), $scripts );
if( isset($scripts) ){
foreach($scripts as $script) {
?>
	<script language="javascript" src="../js/<?php echo $script; ?>"></script>
<?php
	}
}
$styles = array('styles.css');
foreach( $styles as $style) {
?>
 	<link rel="stylesheet" type="text/css" href="../css/<?php echo $style; ?>" />
<?php
}
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		echo "../library/dbconnect/".strtolower($classname).".php";
	}
}
	$requestUri = preg_split('[\\/]', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY);
	$count 		 = count($requestUri);
	$pagename 	 = "";
	$basename    = ""; //foldername used to get the submenu 
    foreach( $requestUri as $key => $val)
	{
		if( $val == $_SESSION['modlocation'])
		{
			continue;
		} else {	
			//if the page has a php extenstion get the basename, 
			if( strpos($val,".") > 0){
				$pagename = substr($val, 0, strpos($val, "."));
				$basename = $requestUri[$count-2];
			} else {
				$basename = $val;
				$pagename = $val;
			}				
		}		
	}
	$ctrl 		 = new Controller( $basename );
	$ctrl -> loadController();
	$submenu     = $ctrl -> getSubMenu();
	$userObj 	 = new UserAccess();
	$user        = $userObj -> getByUserId();

?>
<style type="text/css">
    table th{
        text-align: left;
    }
</style>
</head>
<body topmargin="0" leftmargin="5" bottommargin="0" rightmargin="5">
<?php
if($basename == "new" && $pagename=="index"){
    $pagename = "contract";
}
if($basename == "manage" && $pagename=="index")
{
    $pagename = "view";
}
$mainMenu = array();
if( isset( $submenu ) && !empty( $submenu ))
{
    foreach( $submenu as $key => $menuitems )
    {
    	$active = "";
   		$client_name = ($menuitems['client_name'] == "" ? $menuitems['name'] : $menuitems['client_name']); 
		
    	if($pagename == strtolower($client_name) || strstr($pagename, strtolower($client_name))) {
    		$active = "Y";    		
    	} else if( $pagename == strtolower($menuitems['folder']) || strstr($pagename, strtolower($menuitems['folder']))) {
			$active = "Y";
	   	} else if(preg_match('/\$client_name/i',$pagename,$matches)){
    		$active = "Y";
    	} else {
    		$active = "";
    	}
    	if( strtolower($menuitems['name']) == "contract" ){
    		if( ($user['status'] & UserAccess::CREATE_CONTRACT) == UserAccess::CREATE_CONTRACT ){
				$mainMenu[$menuitems['id']] = array(	
														"id"  	  => $menuitems['id'], 
        												"url" 	  => strtolower(trim($menuitems['name'])).".php?id=",
        												"active"  => $active,
        												"display" => $menuitems['client_name'] 
        								) ;
    			
    		}
    	} else if( strtolower($menuitems['name']) == "deliverable" ){
    		if( ($user['status'] & UserAccess::CREATE_DELIVERABLE) == UserAccess::CREATE_DELIVERABLE ){
				$mainMenu[$menuitems['id']] = array(	
														"id"  	  => $menuitems['id'], 
        												"url" 	  => strtolower(trim($menuitems['name'])).".php?id=",
        												"active"  => $active,
        												"display" => $menuitems['client_name'] 
        								) ;
    			
    		}
    	} else if( strtolower($menuitems['name']) == "approve" ){
    		if( ($user['status'] & UserAccess::APPROVAL) == UserAccess::APPROVAL ){
				$mainMenu[$menuitems['id']] = array(	
														"id"  	  => $menuitems['id'], 
        												"url" 	  => strtolower(trim($menuitems['name'])).".php?id=",
        												"active"  => $active,
        												"display" => $menuitems['client_name'] 
        								) ;
    			
    		}
    	}   else{ 
        	$mainMenu[$menuitems['id']] = array(	
        										"id"  	  => $menuitems['id'], 
        										"url" 	  => strtolower(trim($menuitems['name'])).".php?id=",
        										'active'  => $active,
        										'display' => $menuitems['client_name'] 
        								) ;
    	}
    }
}

	echoNavigation(1,$mainMenu);
?>
    <h3 style="text-align:left; clear:left;">
   	<?php
    $otherNames = array(
			"edit_finyear"              => "Edit Financial Year",
			"edit_cstatus"              => "Edit Contract Status",
			"change_cstatus"            => "Change Contract Status",
			"edit_dstatus"	            => "Edit Deliverable Status",
			"change_dstatus"            => "Change Deliverable Status",
			"edit_dstatus"	            => "Edit Deliverable Status",
			"edit_astatus"	            => "Edit Action Status",
			"change_astatus"            => "Change Action Status",
			"edit_tstatus"              => "Edit Task Status",
			"edit_ctype"	            => "Edit Contract Type",
			"change_ctype"              => "Change Contract Type",
			"change_categorystatus"     => "Change Category Status",
			"edit_assfreq" 	            => "Edit Assessment Frequency",
			"change_assfreq"            => "Change Assessment Frequency",
			"edit_suppliercategory"     => "Edit Supplier Category",
			"change_suppliercategory"   => "Change Supplier Category",
			"change_supplier"		    => "Change Supplier Status",
			"edit_dweight"			    => "Edit Delivered Weight",
			"change_dweight" 		    => "Change Delivered Weight",
			"edit_oweight"			    => "Edit Other Weight",
			"change_oweight" 		    => "Change Other Weight",
			"edit_qweight"			    => "Edit Quality Weight",
			"change_qweight" 		    => "Change Quality Weight",
			"edit_smatrix"			    => "Edit Delivered Score Matrix",
			"change_smatrix"		    => "Change Delivered Score Matrix Status",
			"edit_oscore"			    => "Edit Other Score Matrix",
			"change_oscore"		        => "Edit Other Score Matrix Status",
			"edit_qscore"			    => "Edit Quality Score Matrix",
			"change_qscore"		        => "Change Quality Score Matrix Status",
			"myprofile"		            => "My Profile", 
    		"edit_category"				=> "Edit Contract Category",
    		"useraccess"				=> "User Access",
    		"edit_useraccess"			=> "Edit User Access",
    		"change_deliverablecategorystatus" => "Change Deliverable Category Status"
    );
	$currentMenu = "";; 
	$ctrl -> wasWhere( $requestUri, $otherNames , $pagename );

	?>
    </h3>	
