<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		$("#view_delivereable").deliverable({editAction:true, editDeliverable: true,  contractId:$("#contractid").val(), urlLocation:"../new/", page:$("#page").val()});
	});
</script>

<div id="view_delivereable"></div>
<form method="post">
 	<input type="hidden" name="page" value="editdeliverable"  id="page" />
	<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />
</form>
<div><?php displayGoBack("", ""); ?></div>