<?php 
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} 
}
class ManageController extends Controller
{
	protected $actionHeadArray = array();
	
	protected $actionStatus = array();
	
	function __construct()
	{
	
	}
	
	function getActiveType()
	{
		$obj = $this -> loadModel("ContractType");
		echo json_encode( $obj -> getActiveContractType() );
	}

	function getActiveCategory()
	{
		$obj = $this -> loadModel("ContractCategory");
		echo json_encode( $obj -> getActiveCategory( $_POST['id'] ) );		
	}

	function getSupplier()
	{
		$obj = $this -> loadModel("Supplier");
		echo json_encode( $obj -> getActiveSupplier() );			
	}
	
	function getUsers()
	{
		$obj = $this -> loadModel("UserAccess");
		echo json_encode( $obj -> getUsers() );					
	}
	
	function getTemplates()
	{
		$obj = $this -> loadModel("Contract");
		echo json_encode( $obj -> getTemplates() );
	}
	
	function getAssessmentFrequency()
	{
		$obj = $this -> loadModel("AssessmentFrequency");
		echo json_encode( $obj -> getActiveAssessmentFrequencies() );
	}
	
	function getDeliverableStatus()
	{
		$obj = $this -> loadModel("DeliverableStatus");
		echo json_encode( $obj -> getDeliverableStatuses() );	
	
	}
	
	function getActionStatus()
	{
		$obj = $this -> loadModel("ActionStatus");
		echo json_encode( $obj -> getActionStatuses() );	
	}
	
	function getDeliverable()
	{
		$obj = $this -> loadModel("Deliverable");
		echo json_encode( $obj -> getDeliverable( $_POST['id']) );	
	}
	function _sorStructure( $headerNames ){
		$hArr = array();
		foreach( $headerNames as $key => $headerArr){
			$hArr[$headerArr['name']] = (empty($headerArr['client_terminology']) ?   $headerArr['client_terminology'] : $headerArr['sla_terminology']);
		}
		return $hArr;
	}
	
	function updateContract()
	{
		$insertdata = array();
		$response  	= array();
		$changeArr  = array();
		$changeMsg  = ""; 		
		$update  	= array();
		$obj 		= $this -> loadModel("Contract");
		$user 		= $this -> loadModel("UserAccess");		
		$email 		= $this -> loadModel("Email");	
				
		$contract   = $obj -> getContract( $_POST['contract_id'] );
		$changes 	= $this->_changeProcessor( $_POST, $contract );
		//get owner and manager email
		$ownerEmail   = $user -> getUser( $contract['owner']); 
		$managerEmail = $user -> getUser( $contract['contract_manager']);	

		$changeArr   = $changes['changeArray'];
		$attachments = "";
		if(isset($_SESSION['contract_updates_uploads']) && !empty($_SESSION['contract_updates_uploads']))
		{
			foreach($_SESSION['contract_updates_uploads'] as $key => $value){
				$attachments .= $value."\r\n\n";
			}
			$changeArr['attachments'] = "Added attachments ".$attachments;
			$changeMsg  = "Added attachments ".$attachments;;
		}		
		$changeMsg  = $changes['message'];
		$update		= $changes['updateData'];
				
		$insertdata['changes'] 	   = serialize($changeArr);
		$insertdata['contract_id'] = $_POST['contract_id'];
		$insertdata = array_merge($changes['insertData'], $insertdata );
		
		$update['insert_user'] = $_SESSION['tid'];
		

		$res =  $obj -> updateContract( $_POST['contract_id'], $update , $insertdata);
		$mailText = "";
		if( $res  == 1 ){
		
			if( $email -> to( (isset($ownerEmail['email']) ?  $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Contract Update")-> body( $changes['message'] ) -> send() != -1){
				$mailText  = "Email sent successfully ";
			} else{
				$mailText  = "An error occured sending mail ";				
			}
					
			$response = array("error" => false, "text" => "Contract updated successfully <br /> ".$mailText);
		} else if( $res == 2){
			$response = array("error" => true, "text" => "Contract updated . . ");
		} else if( $res == -1){
			$response = array("error" => true, "text" => "Error updating contract .");
		}
		unset($_SESSION['contract_updates_uploads']);
		echo json_encode($response);
	}
	
	function editContract()
	{
		$insertdata = array();
		$response  	= array();
		$changeArr  = array();
		$changeMsg  = ""; 		
		$update  	= array();		
		$obj 		= $this -> loadModel("Contract");
		$user 		= $this -> loadModel("UserAccess");		
		$email 		= $this -> loadModel("Email");				
		$contract   = $obj -> getContract( $_POST['contract_id'] );
		//user email 
		$ownerEmail   = $user -> getUser( $contract['owner']); 
		$managerEmail = $user -> getUser( $contract['contract_manager']);
		//changes made during edit process
		$changes 	  = $this->_changeProcessor( $_POST, $contract );

		$insertdata	  = array('changes' => serialize($changes['changeArray']), 'insertuser' => $_SESSION['tid']);	
		$insertdata['contract_id'] = $_POST['contract_id'];
		$res = $obj -> editContract( $_POST['contract_id'], $insertdata, $changes['updateData'] );
		$mailText = "";
		if( $res  == 1 ){
			if( $email -> to( (isset($ownerEmail['email']) ? $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Contract Edit")-> body( $changes['message'] ) -> send() != -1){
				$mailText  = "Email sent successfully ";
			} else{
				$mailText  = "An error occured sending mail ";				
			}
			$response = array("error" => false, "text" => "Contract edited successfully <br />".$mailText);
		} else if( $res == 2){
			$response = array("error" => true, "text" => "Contract updated . . ");
		} else if( $res == -1){
			$response = array("error" => true, "text" => "Error updating contract .");
		}
		echo json_encode($response);
	}
	
	function authorizeContract()
	{
		$insertdata = array();
		$response  	= array();
		$changeMsg  = ""; 		
		$update  	= array();
		$obj 		= $this -> loadModel("Contract");
		$user 		= $this -> loadModel("UserAccess");		
		$email 		= $this -> loadModel("Email");	
				
		$contract   = $obj -> getContract( $_POST['contract_id'] );
		$changes 	= $this->_changeProcessor( $_POST, $contract );
		//get owner and manager email
		$ownerEmail   = $user -> getUser( $contract['owner']); 
		$managerEmail = $user -> getUser( $contract['contract_manager']);	

		$changeArr   = $changes['changeArray'];
		$attachments = "";
		if(isset($_SESSION['contract_updates_uploads']) && !empty($_SESSION['contract_updates_uploads']))
		{
			foreach($_SESSION['contract_updates_uploads'] as $key => $value){
				$attachments .= $value."\r\n\n";
			}
			$changeMsg  = "Added attachments ".$attachments;;
		}		
		$changeMsg  = $changes['message'];
		$update		= $changes['updateData'];
				
		$authorizationArray = array(
									'insertuser'  => $_SESSION['tid'],
									'description' => $_POST['response'],
									'approval'    => $_POST['approval'],
									'contract_id' => $_POST['contract_id']																											
									);
		

		$res =  $obj -> contractAuthorization( $authorizationArray );
		$mailText = "";
		if( $res  > 0 ){
			$body 	  = "Contract #".$_POST['contract_id']." has been authorized"; 
			if( $email -> to( (isset($ownerEmail['email']) ?  $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Contract Update")-> body( $body ) -> send() != -1){
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
			} else{
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
			}
					
			$response = array("error" => false, "text" => "Contract authorized successfully <br /> ".$mailText);
		} else if( $res == -1){
			$response = array("error" => true, "text" => "Error authorizing contract .");
		}
		echo json_encode($response);
	}

	function getEditLogs()
	{
		$del 		 = $this->loadModel("Deliverable");
		$editLogs	 = $del->getEditLogs( $_POST['id'] );
		$changes  	 = array();
		foreach( $editLogs as $key => $editArr){
			$changeMessage 	= $this->_extractChanges( $editArr );
			$changes[$editArr['insertdate']]  = array("date" => $editArr['insertdate'], "changes" => $changeMessage, "status" => $editArr['status'] );

		}
		echo json_encode( $changes );
	}

	function getDelUpdateLogs(){
		$del 		 = $this->loadModel("Deliverable");
		$updateLogs	 = $del->getUpdateLogs( $_POST['id'] );
		$changes  	 = array();
		foreach( $updateLogs as $key => $updateArr){
			$changeMessage 	= $this->_extractChanges( $updateArr );
			$changes[$updateArr['insertdate']]  = array(
														"date" 		=> $updateArr['insertdate'],
														"changes" 	=> $changeMessage,
														"status" 	=> $updateArr['status']
													 );
		}
		echo json_encode( $changes );	
	}
	
	function editDeliverable()
	{
		$del 		 = $this->loadModel("Deliverable");
		$id 		 = $_POST['id'];
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");							
		$obj 		= $this -> loadModel("Contract");
		$contract   = $obj -> getContract( $_POST['contractid'] );
		$deliverable = $del -> getDeliverable( $_POST['id'] ); 
			
		$changeDel	 = $this->_deliverableChanges( $deliverable, $_POST);

		// send mail 
		$response 	= array();
		if( isset($changeDel['changeArr']) && !empty($changeDel['changeArr'])){
			if( $deliverable['owner'] ==""){
				$delOwner 	  = array();
			} else {
				$delOwner 	  = $user -> getUser( $deliverable['owner']);	
			}
		    $ownerEmail   = $user -> getUser( $contract['owner']); 
		    $managerEmail = $user -> getUser( $contract['contract_manager']);	
			
			$body = $changeDel['changeMessage'];
			$editChanges = array(
								   "changes" 		=> serialize($changeDel['changeArr']),
								   "deliverable_id" => $id,
								   "insertuser"		=> $_SESSION['tid']  
								    ) ;
    		if( $this->_checkValidity( $contract['completion_date'], $_POST['deadline'] )  ){
				if( $this->_checkValidity( $_POST['deadline'], $_POST['remind_on']) ){	
							$res  		= $del -> editDeliverable( $id, $editChanges , $changeDel['insertData']) ;	
							$mailText	= "";
							if($res == 1){
							
								if( $email -> to( (isset($delOwner['email']) ? $delOwner['email']."," : "")."".(isset($ownerEmail['email']) ? $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Edit Deliverable") -> body( $body ) -> send() != -1){
									$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
								} else{
									$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
								}			
							
								$response = array("error" => false, "text" => "Deliverable updated successfully <br />".$mailText);
							} else if($res == 2){
								$response = array("error" => false, "text" => "Deliverable updated .... ");
							} else {
								$response = array("error" => true, "text" => "Error updating the deliverable");
							}
				} else {
					$response = array("text" => "Remind on date cannot be afer the deadline date", "error" => true);
				}
    		} else {
    			$response = array("text" => "Deliverable deadline date cannot be after the contract completion date (<b>".$contract['completion_date'].")</b>", "error" => true);
    		}				    

		}
		echo json_encode( $response );
	}
	
	function updateDeliverable()
	{
		$del 		 = $this->loadModel("Deliverable");
		$user 		 = $this -> loadModel("UserAccess");	
		$email 		 = $this -> loadModel("Email");							
		$obj 		 = $this -> loadModel("Contract");
				
		$id 		 = $_POST['id'];
		$deliverable = $del -> getDeliverable( $_POST['id'] ); 
		$contract    = $obj -> getContract( $deliverable['contract_id'] );
		
		$deliverableStatus = unserialize( $contract['deliverable_status'] );
				
		$assesText = "";
		if( isset($deliverableStatus[$_POST['deliverable_status']])){
				if( $email -> to( (isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Edit Deliverable") -> body( $deliverable['deliverable']." requires an assessment" ) -> send() != -1){
					$assesText  = " &nbsp;&nbsp;&nbsp;&nbsp; Assessment email sent successfully \r\n";
				} else{
					$assesText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending assessment mail ";				
				}		
		}
		
		$changeDel	 = $this->_deliverableChanges( $deliverable, $_POST);
		$bodyAppend  = "";
		if(isset($_POST['request_approval'])){
			if($_POST['request_approval'] == "on"){
				$bodyAppend = "A reest for approval of action #".$id;
			}
		}
		// send mail 
		$response 	= array();
		if( isset($changeDel['changeArr']) && !empty($changeDel['changeArr'])){
			$delOwner 	   = $user -> getUser( $deliverable['owner']);
		    $ownerEmail    = $user -> getUser( $contract['owner']); 
		    $managerEmail  = $user -> getUser( $contract['contract_manager']);
			
				
			$attachments = "";
			$changeMsg	 = "";
			if(isset($_SESSION['deliverable_update_uploads']) && !empty($_SESSION['deliverable_update_uploads']))
			{
				foreach($_SESSION['deliverable_update_uploads'] as $key => $value){
					$attachments .= $value."\r\n\n,";
				}
				$changeDel['changeArr']['attachments'] = "Added attachments ".$attachments;
				$changeMsg  			  			   = "Added attachments ".$attachments;
			}	
			$editChanges = array(
								   "changes" 		=> serialize($changeDel['changeArr']),
								   "deliverable_id" => $id,
								   "insertuser"		=> $_SESSION['tid']  
								    ) ;
			//update the deliverable
			$res  	 = $del->updateDeliverable( $id, array_merge($editChanges, $changeDel['updateData'] ), $changeDel['insertData']) ;			
			$mailText = "";
			if($res == 1){
				$body = $changeDel['changeMessage'];
				if( $email -> to( (isset($delOwner['email']) ? $delOwner['email']."," : "")."".(isset($ownerEmail['email']) ? $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Edit Deliverable") -> body( $bodyAppend." \r\n ".$body. "\r\n ".$changeMsg."\r\n".$assesText ) -> send() != -1){
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
				} else{
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
				}	
							
				$response = array("error" => false, "text" => "Deliverable updated successfully <br />".$mailText."<br />".$assesText);
			} else if($res == 2){
				$response = array("error" => false, "text" => "Deliverable updated .... ");
			} else {
				$response = array("error" => true, "text" => "Error updating the deliverable");
			}
			
		}
		unset($_SESSION['deliverable_update_uploads']);
		echo json_encode( $response );
	}


	function authorizeDeliverable()
	{
		$del 		 = $this->loadModel("Deliverable");
		$user 		 = $this -> loadModel("UserAccess");	
		$email 		 = $this -> loadModel("Email");							
		$obj 		 = $this -> loadModel("Contract");
		
		$id 		 = $_POST['id'];
		$deliverable = $del -> getDeliverable( $_POST['id'] ); 
		$contract    = $obj -> getContract( $deliverable['contract_id'] );
		
		$changeDel	 = $this->_deliverableChanges( $deliverable, $_POST);
		$bodyAppend  = "";
		if(isset($_POST['request_approval'])){
			if($_POST['request_approval'] == "on"){
				$bodyAppend = "A reest for approval of action #".$id;
			}
		}
		// send mail 
		$response 	= array();
		$delOwner 	   = $user -> getUser( $deliverable['owner']);
		$ownerEmail    = $user -> getUser( $contract['owner']); 
		$managerEmail  = $user -> getUser( $contract['contract_manager']);
		
		$authorizationArray = array(
									"description"      => $_POST['response'],
									"approval" 	  	   => $_POST['approve'],
									"deliverable_id"   => $_POST['id'],
									"insertuser"  	   => $_SESSION['tid']
								);
		$attachments = "";
		$changeMsg	 = "";
		if(isset($_SESSION['deliverable_update_uploads']) && !empty($_SESSION['deliverable_update_uploads']))
		{
			foreach($_SESSION['deliverable_update_uploads'] as $key => $value){
				$attachments .= $value."\r\n\n,";
			}
			$changeMsg  	  = "Added attachments ".$attachments;
		}	
			//update the deliverable
			$res  	  = $del->authorizaDeliverable( $authorizationArray ) ;			
			$mailText = "";
			$body 	  = "Contract #".$_POST['id']." has been authorized"; 
			if($res > 0){
				$body = $changeDel['changeMessage'];
				if( $email -> to( (isset($delOwner['email'])."," ? $delOwner['email'] : "")."".(isset($ownerEmail['email']) ? $ownerEmail['email']."," : "")."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Edit Deliverable") -> body( "") -> send( $body ) != -1){
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
				} else{
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
				}	
							
				$response = array("error" => false, "text" => "Deliverable authorized successfully <br />".$mailText);
			} else {
				$response = array("error" => true, "text" => "Error authorizing the deliverable");
			}
		echo json_encode( $response );
	}

	function _deliverableChanges( $deliverable, $postArr )
	{

		$naming		= $this -> loadModel("Naming");			
		$headers 	= $this->_sorStructure( $naming -> getNaming() );		
		$delChanges = array();
		$delMessage = "";
		$insertData = array();
		$updateData = array();	
		$delMessage 		.= $_SESSION['tkn']." has made the following changes \r\n\n";
		$delChanges['user'] = $_SESSION['tkn']." has made the following changes \r\n\n";	
		foreach( $postArr as $key => $value){
			if( isset($deliverable[$key]) ){
				if( $deliverable[$key] !== $value){
					if($key == "deliverable_status"){
						
						$status		=  $this->loadModel("DeliverableStatus");
						$fromStstus =  $status->getDelStatus($deliverable[$key]);
						$toStatus  	=  $status->getDelStatus($value);
						
						$delMessage 	.= $headers[$key]." changed to ".$toStatus['name']." from ".$fromStstus['name']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toStatus['name'], "from" => $fromStstus['name'] );
						$insertData['status'] = $value;
											
					} else if($key == "deliverable_category"){
						$delCat    	=  $this->loadModel("DeliverableCategory");
						$fromCat  	=  $delCat->getCategory($deliverable[$key]);
						$toCat  	=  $delCat->getCategory($value);
						
						$delMessage 	.= $headers[$key]." changed to ".$toCat['name']." from ".$fromCat['name']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toCat['name'], "from" => $fromCat['name'] );
						$insertData['category'] = $value;
											
					} else if($key == "owner"){
						$us    	  	  = new UserAccess();
						$fromUser     = array();
						if($deliverable[$key] == ""){
							$fromUser['user']  	=  "";
						} else {
							$fromUser  	=  $us->getUser($deliverable[$key]);	
						}
						
						$toUser  	=  $us->getUser($value);
						
						$delMessage 	.= $headers['deliverable_owner']." changed to ".$toUser['user']." from ".$fromUser['user']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toUser['user'], "from" => $fromUser['user'] );					
						$insertData['owner'] = $value;
												
					} else if($key == "quality_weight"){
						$qw    	  	 = new QualityWeight();
						$fromQW  	=  $qw->getQualityWeight($deliverable[$key]);
						$toQW  		=  $qw->getQualityWeight($value);
						
						$delMessage .= $headers[$key]." changed to ".$toQW['description']." from ".$fromQW['description']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toQW['description'], "from" => $fromQW['description'] );	
						$insertData['quality_weight'] = $value;
											
					}else if($key == "other_weight"){
						$ow    	  	 = new OtherWeight();
						$fromOW  	=  $ow->getOtherWeight($deliverable[$key]);
						$toOW  		=  $ow->getOtherWeight($value);
						
						$delMessage .= $headers[$key]." changed to ".$toOW['description']." from ".$fromOW['description']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toOW['description'], "from" => $fromOW['description'] );	
						$insertData['other_weight'] = $value;
												
					}else if($key == "delivered_weight"){
						$dw    	  	 = new DeliveredWieght();
						$fromDW  	=  $dw->getDeliveredWieght($deliverable[$key]);
						$toDW  		=  $dw->getDeliveredWieght($value);
						
						$delMessage .= $headers[$key]." changed to ".$toDW['description']." from ".$fromDW['description']."\r\n\n";	
						$delChanges[$key]	= array("to" => $toDW['description'], "from" => $fromDW['description'] );	
						$insertData['weight'] = $value;						
						
					} else if($key == "maindeliverable") {
						$del 		 = $this->loadModel("Deliverable");
						$toDE  		=  $del->getDeliverable($value);
						$delMessage .= $headers[$key]." changed to ".$toDE['deliverable']." from ".$deliverable[$key]."\r\n\n";	
						$delChanges[$key]	= array("to" => $toDE['deliverable'], "from" => $deliverable[$key] );	
						$insertData['main_deliverable'] = $value;	
					} else {
				
						$delMessage .= (isset($headers[$key]) ? $headers[$key] : ucwords(str_replace("_", " ",$key)))." changed to ".$value." from ".$deliverable[$key]."\r\n\n";	
						$delChanges[$key]	= array("to" => $value, "from" => $deliverable[$key] );
						$replace = str_replace("deliverable", "", $key);
						$res = ($replace == "" ? "name" : ltrim($replace,"_")); 
						$insertData[$res] = $value;						
					}
				}
			} else if(trim($key) == "remindon" && !isset($classArr[$key])){
				$delMessage		  .= "Added ".ucwords($key)." : ".$value."\r\n\n";
				$delChanges[$key] = "Added Remind On ".$value."";
				$insertData['remind_on'] = $value;
				
			} else if( trim($key) == "response") {
				$delMessage		 .= "Added ".ucwords($key)." : ".$value."\r\n\n";
				$delChanges[$key] = "Added Response ".$value."";
				$updateData['response'] = $value;
			} else {
				
			}
		}
		return array(
					 "changeMessage" => $delMessage,
					 "changeArr" 	 => $delChanges,
					 "insertData" 	 => $insertData ,
					 "updateData" 	 => $updateData
					);
	}
	
	function _changeProcessor( $postArr , $classArr){
		$naming		= $this -> loadModel("Naming");			
		$headers 	= $this->_sorStructure( $naming -> getNaming() );	
	
		$changeArr  	= array();
		$changeMessage  = "";	
		$updateData 	= array();
		$insertData		= array();
		$additions		= array("date_completed", "saving", "remindon" , "response");
		//$namimg 		= 	
		$changeArr['user'] = $_SESSION['tkn']." made the following changes to the contract <br />";
		$changeMessage    .= $_SESSION['tkn']." has made the following changes to the contract <br />";		
		foreach( $postArr as $key => $value)
		{	
			if( isset($classArr[$key]) ) {
			  if( $classArr[$key] !== $value ) {
					if(trim($key) == "status"){
					
						$stat   		= $this -> loadModel("ContractStatus");
						$fromstatus  	= $stat -> getCStatus(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$tostatus   	= $stat -> getCStatus( $value );
						$changeMessage .= $headers['contract_status']." has changed to ".$tostatus['name']." from ".$fromstatus['name']."<br />";
						$changeArr[$key]  = array("from" => $fromstatus['name'], "to" => $tostatus['name']);
						$updateData['status'] = $value;
						
					} else if(trim($key) == "supplier"){
					
						$supp   		  = $this -> loadModel("Supplier");
						$fromsupp 		  = $supp->getSupply(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$tosupp  	      = $supp -> getSupply( $value );
						$changeMessage   .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$tosupp['name']." from ".$fromsupp['name']."<br />";
						$changeArr[$key]  = array("from" => $fromsupp['name'], "to" => $tosupp['name']);
						$updateData['supplier'] = $value;
						
					} else if(trim($key) == "category"){
					
						$stat   		  = $this -> loadModel("ContractCategory");
						$fromscate 		  = $stat->getCategory(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$tocat   	  = $stat -> getCategory( $value );
						$changeMessage   .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$tocat['name']." from ".$fromscate['name']."<br />";
						$changeArr[$key]  = array("from" => $fromscate['name'], "to" => $tocat['name']);
						$updateData['category'] = $value;						
						
					} else if(trim($key) == "type"){
					
						$stat   		= $this -> loadModel("ContractType");
						$fromtype 	    = $stat->getContractType(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$totype   	    = $stat -> getContractType( $value );
						$changeMessage .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$totype['name']." from ".$fromtype['name']."<br />";
						$changeArr[$key]  = array("from" => $fromtype['name'], "to" => $totype['name']);
						$updateData['type'] = $value;								
						
					} else if(trim($key) == "notification_frequency"){
					
						$stat   	 = $this -> loadModel("Notification");
						$fromnoti 	 = $stat->getNotification(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$tonoti   	 = $stat -> getNotification( $value );
						$changeMessage  .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$tonoti['description']." from ".$fromnoti['description']."<br />";
						$changeArr[$key] = array("from" => $fromnoti['description'], "to" => $tonoti['description']);
						$updateData['notification_frequency']= $value;								
						
					} else if(trim($key) == "assessment_frequency"){
					
						$stat   		  = $this -> loadModel("AssessmentFrequency");
						$fromasses 	  = $stat->getAssessmentFrequency(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$toasses  	  = $stat -> getAssessmentFrequency( $value );
						$changeMessage 	 .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$toasses['name']." from ".$fromasses['name']."<br />";
						$changeArr[$key]  = array("from" => $fromasses['name'], "to" => $toasses['name']);
						$updateData['assessment_frequency'] = $value;							
						
					 } else if(trim($key) == "remindon" ){
						$changeMessage 	 .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$value." from ".$classArr[$key]."<br />";
						$changeArr[$key]  = array("from" => $classArr[$key], "to" => $value);
						$insertData['remindon'] = $value;
				
					} else if(trim($key) == "owner"){
						$user  		  = $this -> loadModel("UserAccess");
						$fromuser	  = $user->getUserAccessSetting(($classArr[$key] == "" | $classArr[$key] == 0) ? 1 : $classArr[$key] );
						$touser  	  = $user -> getUserAccessSetting( $value );
						$changeMessage 	 .= $headers['contract_owner']." has changed to ".$touser['user']." from ".$fromuser['user']."<br />";
						$changeArr[$key]  = array("from" => $fromuser['user'], "to" => $touser['user']);
						$updateData['contract_owner'] = $value;		
						
					} else {
						
					  $changeMessage   .= (isset($headers[$key]) ? $headers[$key] : ucwords($key)) ." has changed to ".$value." from ".$classArr[$key]."<br />";
					  $changeArr[$key]  = array("from" => $classArr[$key], "to" => $value);
					  
						  if($key == "contract"){
							$updateData['name'] = $value;		
						  } else if( $key == "project_comments"){ 					  
							$updateData['comments'] = $value;							  
						  } else {
							  $updateData[$key] = $value;
						  }
					}	
				}
			} else if(in_array($key, $additions)){
				if($value == ""){
				
				} else {
					$changeMessage  .= "Added ".$this->_createHeader($key)." : ".$value."<br />";
					$changeArr[$key] = "Added ".$this->_createHeader($key)." ".$value."";
					$insertData[$key] = $value;
				}
				//$changeMessage  .= "Added ".ucwords($key)." : ".$value."\r\n\n";	
				//$changeArr[$key] = "Added Response ".$value."";
				//$insertData['response'] = $value;
			}
		}				
		return array( 
					"message" 		=> $changeMessage ,
					"changeArray"   => $changeArr,
					"updateData"    => $updateData, 
					"insertData"	=> $insertData
					);
	}
	
	function _createHeader($field)
	{
		$naming		= $this -> loadModel("Naming");			
		$headers 	= $this->_sorStructure( $naming -> getNaming() );
		
		if( isset($headers[$field])){
			return $headers[$field];
		} else {
			return  ucwords(str_replace("_"," ", $field));
		}
	}
	
	function _extractChanges( $updateArray )
	{
		$naming		= $this -> loadModel("Naming");			
		$headers 	= $this->_sorStructure( $naming -> getNaming() );
		$adds		= array("user", "date_completed", "saving", "attachments");
		$changeMessage = "";
		if( isset($updateArray['changes']) && !empty($updateArray['changes']) ){
			$changesArr = unserialize( $updateArray['changes'] );

			foreach( $changesArr  as $index => $val ){
				if( in_array($index,$adds) ){
					$changeMessage .= $val."<br />";
				} else if( $index == "remindon" ){
					if(is_array($val)){
						$changeMessage .= (isset($headers[$index]) ? $headers[$index] : ucwords(str_replace("_"," ", $index))) ." has changed to ".$val['to']." from ".$val['from']."<br />";					
					} else {
						$changeMessage .= ucwords(str_replace("_"," ", $index))." ".$val."<br />";
					}
				} else if( $index == "response" ){
					$changeMessage .= $val."<br />";
				}else{
					if( isset($val['to']) && isset($val['from'])){

						$changeMessage .= (isset($headers[$index]) ? $headers[$index] : ucwords(str_replace("_"," ", $index))) ." has changed to ".$val['to']." from ".$val['from']."<br />";
					}
				}
			}
		}		
		return $changeMessage;
	}

	function editAction()
	{
		$act 		= $this->loadModel("Action");
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");							
		$obj 		= $this -> loadModel("Contract");
		$contract   = $obj -> getContract( $_POST['contractid'] );
		$action 	= $act->getAction( $_POST['action_id']);
		$deliObj 	 = $this -> loadModel("Deliverable");
		$deliverable = $deliObj->getDeliverable( $action['deliverable_id']);	
		$_POST['action'] = $_POST['action_name'];
		unset($_POST['action_name']);
		$changes  = $this->_processActionChanges( $_POST, $action );

		$response = array();
		if(isset($changes) && !empty($changes))
		{
			if( $action['action_owner'] != ""){
				$owner 		   = $user -> getUser( $action['action_owner'] );
			} else {
				$owner  	   = array(""); 
			}
			$contractOwner = $user -> getUser( $contract['owner'] );			
			$changesArr = array( "action_id" => $_POST['action_id'], "changes" => serialize($changes['changeArr']) );
					
			if( $this->_checkValidity($_POST['deadline'], $_POST['remind_on'])){
				if( $this->_checkValidity($deliverable['deadline'], $_POST['deadline'])) {
					$res 		= $act->editActionChanges($_POST['action_id'] , $changesArr , $changes['updateArr']);
					$mailText 	= "";
					if($res == 1){
					
						if( $email -> to( (isset($owner['email']) ?  $owner['email']."," : "")."".(isset($contractOwner['email']) ? $contractOwner['email'] : "") ) -> from("admin@ignite4u.com") -> body( $changes['changeMessage'] ) -> send() != -1){
							$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
						} else{
							$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
						}			
					
						$response = array("error" => false, "text" => "Action updated successfully <br />".$mailText);
					} else if($res == 2) {
						$response = array("error" => false, "text" => "Action updated .... ");
					} else {
						$response = array("error" => true, "text" => "Error updating the action");
					}
				} else {
					$response = array("text" => "Action deadline date cannot be after the deliverable deadline date( <b>".($deliverable['deadline'] == "" ? "<br /> The dealiverable deadline date has not been set, please fill it and try again" : $deliverable['deadline'])."</b> )", "error" => true);					
				}	
			} else {
				$response = array("text" => "Remind date cannot be after the deadline date", "error" => true);
			}			
		}
		echo json_encode($response);
	}
	
	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;
			
		}
	}	
	
	function updateAction()
	{
		$act 	= $this->loadModel("Action");
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");							
		$obj 		= $this -> loadModel("Contract");
		$contract   = $obj -> getContract( $_POST['contractid'] );
		
		$action = $act->getAction( $_POST['action_id']);
		$changes  = $this->_processActionChanges( $_POST, $action );
		$response = array();
		$bodyAppend  = "";
		if(isset($_POST['request_approval'])){
			if($_POST['request_approval'] == "on"){
				$bodyAppend = "<br /> ".$_SESSION['tkn']."  has requested for approval of action #".$_POST['action_id'];
			}
		}
		if( isset($changes) && !empty($changes))
		{
			$owner 		   = $user -> getUser( $action['action_owner'] );
			$contractOwner = $user -> getUser( $contract['owner'] );	
			$attachments = "";
			$changeMsg	 = "";
			if(isset($_SESSION['action_update_uploads']) && !empty($_SESSION['action_update_uploads']))
			{
				foreach($_SESSION['action_update_uploads'] as $key => $value){
					$attachments .= $value."\r\n\n,";
				}
				$changes['changeArr']['attachments'] = "Added attachments ".$attachments;
				$changeMsg  			  			.= "Added attachments ".$attachments;
			}	
			$changesArr   = serialize( $changes['changeArr'] );
			$updateChange = array_merge( array("changes" => $changesArr, "action_id" => $_POST['action_id']), $changes['insertArr'] );
			
			$res = $act->updateActionChanges( $_POST['action_id'], $updateChange, $changes['updateArr'] );
			$mailText = "";
			if($res == 1){
				$body = "<br /><br />Please log onto Ignite Assist in order to View Action \r\n";			
				$body .= "\n";	//blank row above Please log on line
				if( $email -> to( (isset($owner['email']) ? $owner['email']."," : "")."".(isset($contractOwner['email']) ? $contractOwner['email'] : "") ) -> from("admin@ignite4u.com") -> body( $changes['changeMessage']." ".$changeMsg."\r\n".$bodyAppend."<br />".$body ) -> send() != -1){
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
				} else{
					$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
				}				
				$response = array("error" => false, "text" => "Action updated successfully <br /> ".$mailText);
			} else if($res == 2){
				$response = array("error" => false, "text" => "Action updated .... ");
			} else {
				$response = array("error" => true, "text" => "Error updating the action");
			}
		}
		unset($_SESSION['action_update_uploads']);
		echo json_encode($response);
	}
		
	function authorizeAction()
	{
		$act 	= $this->loadModel("Action");
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");							
		$obj 		= $this -> loadModel("Contract");
		
		$action = $act->getAction( $_POST['action_id']);
		$changes  = $this->_processActionChanges( $_POST, $action );
		$response = array();
		$bodyAppend  = "";
		if(isset($_POST['request_approval'])){
			if($_POST['request_approval'] == "on"){
				$bodyAppend = "A reest for approval of action #".$_POST['action_id'];
			}
		}
		
		$owner 		   = $user -> getUser( $action['action_owner'] );
		$attachments = "";
		$changeMsg	 = "";
		if(isset($_SESSION['action_update_uploads']) && !empty($_SESSION['action_update_uploads']))
		{
			foreach($_SESSION['action_update_uploads'] as $key => $value){
				$attachments .= $value."\r\n\n,";
			}
			$changeMsg  			  			.= "Added attachments ".$attachments;
		}	
		$authorizeArray = array(
									"description" => $_POST['response'],
									"approval" 	  => $_POST['approval'],
									"action_id"	  => $_POST['action_id'],
									"insertuser"  => $_SESSION['tid']
								);
		
		$res = $act->authorizeAction( $authorizeArray );
		$mailText = "";
		$body	  = "";
		if($res > 0){
			if( $email -> to( (isset($owner['email']) ? $owner['email'] : "") ) -> from("admin@ignite4u.com") -> body( $body ) -> send() != -1){
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
			} else{
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
			}				
			$response = array("error" => false, "text" => "Action authorized successfully <br /> ".$mailText);
		} else {
			$response = array("error" => true, "text" => "Error authorizing the action");
		}
		echo json_encode($response);
	}	
		
	function _processActionChanges( $postArr, $action )
	{
		$changeMessage = "";
		$changeArr	   = array();
		$updateArr	   = array();
		$insertArr	   = array();
		$naming		   = $this -> loadModel("Naming");			
		$headers 	   = $this->_sorStructure( $naming -> getNaming() ); 

		
		$changeMessage .=  $_SESSION['tkn']." has made the following changes <br />";
		$changeArr['user'] =   $_SESSION['tkn']." has made the following changes <br />";
		foreach( $postArr as $key => $value){
			
			if( isset($action[$key])){
				if( $action[$key] != $value){
					$headerName = (isset($headers[$key]) ? $headers[$key] : ucwords(str_replace("_", " ", $key)) );
					if($key == "action_owner"){
						$us    	  	  = new UserAccess();
						$fromUser 	  = array();
						if($action[$key] !== ""){
							$fromUser  	=  $us->getUser($action[$key]);
						} else {
							$fromUser['user'] = "";
						}
						$toUser  	=  $us->getUser($value);	
						
						$changeMessage  .= $headerName." has changed to ".$toUser['user']." from ".$fromUser['user']."<br />";
						$changeArr[$key] = array("from" => $fromUser['user'], "to" =>$toUser['user'] ); 
						$updateArr['owner'] = $value;
					} else if($key == "action_status") {
						$stat 		 = $this->loadModel("ActionStatus");
						$fromStatus  = $stat->getActionStatus( $action[$key] );
						$toStatus	 = $stat->getActionStatus( $value );
						
						$changeMessage  .= $headerName." has changed to ".$toStatus['name']. " from ".$fromStatus['name']."<br />";
						$changeArr[$key] = array("from" => $fromStatus['name'], "to" => $toStatus['name'] ); 
						$updateArr['status'] = $value;						
					} else {
						$changeMessage .= $headerName." has changed to ".$value." from ".$action[$key]."<br />";					
						$changeArr[$key] = array("from" => $action[$key] , "to" => $value );	  
						if($key == "progress" && $value>="100"){
							$updateArr[$key] 		   = $value;
							$updateArr['actionstatus'] = $action['actionstatus'] + Action::ACTION_AWAITING;				
						} else {
							$updateArr[$key] = $value;
						}		
					}
				}
			} else if( $key == "response"){
				$changeMessage .= "Added response ".$value."<br />";
				$changeArr['response'] = "Added response ".$value."<br />";
				$insertArr['response'] = $value;
			} 
		}
		return array(
				"changeArr" 	=> $changeArr, 
				"changeMessage" => $changeMessage, 
				"insertArr"		=> $insertArr,
				"updateArr"		=> $updateArr
			);
	}
	
	function getActionUpdates()
	{
		$act 		   = $this->loadModel("Action");
		$actionUpdates = $act->getActionUpdate( $_POST['id']);
		$changes 	   = array();
		foreach($actionUpdates as $key => $updateArr){
			$changeMessage = $this->_extractChanges( $updateArr );
			$changes[$updateArr['insertdate']] = array(
													   "date" 	 => $updateArr['insertdate'],
													   "changes" => $changeMessage,
													   "status"	 => $updateArr['status']
													 );
		}
		echo json_encode($changes);
	}
	
	function getActionEdits()
	{
		$act 		   = $this->loadModel("Action");
		$actionEdits = $act->getActionEdit ($_POST['id']);
		$changes 	   = array();
		foreach($actionEdits as $key => $editArr){
			$changeMessage = $this->_extractChanges( $editArr );
			$changes[$editArr['insertdate']] = array(
													   "date" 	 => $editArr['insertdate'],
													   "changes" => $changeMessage,
													   "status"	 => $editArr['status']
													 );
		}
		echo json_encode($changes);
	}
		
	function getContractUpdates()
	{
		$contract 	   = $this -> loadModel("Contract");
		$contrUpdate   = $contract -> getContractUpdates( $_POST['id'] );
		$changes 	   = array(); 
		foreach( $contrUpdate as $index => $updateArr){
			$changeMessage = $this->_extractChanges( $updateArr );
			$changes[$updateArr['date']]  = array("date" => $updateArr['date'], "changes" => $changeMessage, "status" => $updateArr['status'] );
		}
		echo json_encode( $changes );
	}
	
	function getContractEdits()
	{
		$contract 	 = $this -> loadModel("Contract");
		$contrEdits	 = $contract -> getContractEdits( $_POST['id'] );
		$changes 	   = array(); 
		foreach( $contrEdits as $index => $editsArr){
			$changeMessage = $this->_extractChanges( $editsArr );
			$changes[$editsArr['date']]  = array("date" => $editsArr['date'], "changes" => $changeMessage, "status" => $editsArr['status'] );
		}
		echo json_encode( $changes );
	}
	
	function _checkDeliverableAccess( $deliverables )
	{
		foreach( $deliverables as $key => $deliverable)
		{
		  if(isset($deliverable['owner'])){
			if($deliverable['owner'] == $_SESSION['tid'])
			{
				return 1;
			}
		  }
		}
		return 0;
	}
	
	function getContracts()
	{
		$contract 	 = $this -> loadModel("Contract");
		$deliverable = $this -> loadModel("Deliverable");
		$headernames = $this -> loadModel("Naming");
		$conditions	     = "AND contractdefaults & ".Contract::APPROVED." = ".Contract::APPROVED."";				
	/*	if($_POST['template'] == true){
			$conditions	     .= " AND contractdefaults & ".Contract::TEMPLATE." = ".Contract::TEMPLATE."";		
		} else {
			$conditions	     .= " AND contractdefaults & ".Contract::APPROVED." = ".Contract::APPROVED."";					
		}
		*/
		//$conditions	     = "AND contractdefaults & ".Contract::APPROVED." = ".Contract::APPROVED."";		
		$contr   	 = $contract -> getContracts( $_POST['start'], $_POST['limit'], $conditions );
		$total 		 = $contract -> getTotalContract(  $conditions  );
		$headers	 = $headernames -> getHeader();
		$contractArray = array();
		$headersArray  = array();		
		foreach( $contr as $key => $ctrl )
		{
			$showOnPage	  = false; // shows contract details if the conditions set are met
			switch($_POST['page']){
				case "authorization" :
					$averageTotal = $this->_authorizationProcessor( $ctrl['ref'] );	
					if( $averageTotal == 100){
						$showOnPage	  = true;
					}			
				break;
				case "assessment":
					$deliverableStatus  = unserialize( $ctrl['deliverable_status'] );
					if(is_array($deliverableStatus) && !empty($deliverableStatus)){
						foreach( $deliverableStatus as $v => $stat ){
							if( $stat == 1){
								$conditions = " AND D.status = ".$v;
								$del = $deliverable -> getContractDeliverables( $ctrl['ref'], $conditions);
								if( !empty($del)){
									$showOnPage = true;
								}
							}
						}
					}
				break;
			
				default:
				$showOnPage = true;
			}		
		  
		  	if( $showOnPage ){
				foreach( $headers as $field => $val)
				{
					$fields = array("ref", "contract", "contract_type", "project_value", "project_comments", "contract_owner", "contract_status", "contract_completion_date");
					if(in_array($field, $fields)){
						if( $field == "contract_status"){
							if(isset($_POST['page']))
							{
								if($_POST['page'] == "update")
								{
									$headersArray[$field]	   = 'Contract Status';
									$contractArray[$ctrl['ref']]['contract_status'] = $ctrl['status'];				
								}
							}
						} else {
								$headersArray[$field]	   = $val;
								$contractArray[$ctrl['ref']][$field] = ($ctrl[$field] == "" ? "&nbsp;" : $ctrl[$field]);				
						}
						$conditions = "";
						$deliverables = $deliverable -> getContractDeliverables( $ctrl['ref'], $conditions);
						$contractArray[$ctrl['ref']]['hasAccess'] 			 = (($ctrl['owner'] == $_SESSION['tid'] || $ctrl['contract_manager'] == $_SESSION['tid']) ? "1" : 0);
						$contractArray[$ctrl['ref']]['hasDeliverableAccess'] = $this->_checkDeliverableAccess( $deliverables );	
					}
				}
			}
		  	/*					if( isset( $ctrl[$field] ) && $field != "deliverable_status" ) {
						if( isset($_POST['page'])){
							if($_POST['page'] == "update"){
								if($field == "contract_status"){
									$headersArray[$field]	   = "Contract";
									$contractArray[$ctrl['ref']]['contract_status'] = $ctrl['status'];		
								} else {
									$headersArray[$field]	   = $val;
									$contractArray[$ctrl['ref']][$field] = $ctrl[$field];															
								}
							} else {
								$headersArray[$field]	   = $val;
								$contractArray[$ctrl['ref']][$field] = $ctrl[$field];							
							}
						}	else {
							$headersArray[$field]	   = $val;
							$contractArray[$ctrl['ref']][$field] = $ctrl[$field];						
						}
									
					}
				}
			//if contract has deliverables which require assessment , then the contract should show on the assessment page
			$hasDeliverables = "";
			if(isset($_POST['page']) && $_POST['page'] == "assessment"){
					$assess	  = "";
					$deliverableStatus  = unserialize( $ctrl['deliverable_status'] );
					if(is_array($deliverableStatus) && !empty($deliverableStatus)){
						$delStatatus = "(";
						foreach( $deliverableStatus as $v => $stat ){
							if( $stat == 1){
								$conditions = " AND D.status = ".$v;
								$del = $deliverable -> getContractDeliverables( $ctrl['ref'], $conditions);
								if( !empty($del)){
									$assess = $ctrl['ref'];
								}
							}
						}
					}
					if( $assess !== ""){
							foreach( $headers as $field => $val)
							{
								if( isset( $ctrl[$field] ) && $field != "deliverable_status") {
									$headersArray[$field]	   = $val;
									$contractArray[$ctrl['ref']][$field] = $ctrl[$field];			
								}		
							}
					}
			} else{
				foreach( $headers as $field => $val)
				{
					if( isset( $ctrl[$field] ) && $field != "deliverable_status" ) {
						$headersArray[$field]	   = $val;
						$contractArray[$ctrl['ref']][$field] = $ctrl[$field];			
					}		
				}
			}
			*/
		}
		$response 	 = array("contractArr" => $contractArray ,
							 "headers" 	   => $headersArray ,
							 "headCount"   => count($headersArray)  ,
							 "total" 	   => $total
							 ); 
		
		echo json_encode( $response );
	}
	
	function _authorizationProcessor( $id )
	{
		$del 		 = $this -> loadModel("Deliverable");
		$result 	 = $del->getDeliverables( $id, "main" );
		$mainDeliverable    = $result['deliverable'];
		
		$countDeliverables  = count($mainDeliverable);
		$totalProgress		= 0;
		foreach( $mainDeliverable as $key => $deliverable )
		{
			if( ($deliverable['delstatus'] &  Deliverable::MAIN_WITH_SUB ) == Deliverable::MAIN_WITH_SUB ){

				//get the sub deliverable
				$subResult =  $del->getDeliverables( $deliverable['ref'], "sub" );	
				//prepare the sub deliverable
				$subDeliveralesProgress  = 0;
				$countSubDeliverables   = count($subResult['deliverable']);	
				$averageSubDeliverables = 0;
				//$averageSubDeliverables = count($subResult['deliverable']);
				foreach( $subResult['deliverable'] as $subKey => $subDeliverable ){
					$averageActionProgress = $this->_calculateActionProgress( $subDeliverable['ref'] );		
					$subDeliveralesProgress = $subDeliveralesProgress	+ $averageActionProgress;										
				}

					
				//calculate the average for the subdelireables
				if( $countSubDeliverables == 0)
				{
					$averageSubDeliverables = 0;	
				} else {
					$averageSubDeliverables  = ($subDeliveralesProgress/count($subResult['deliverable']));
				}
				$totalProgress = $totalProgress  + $averageSubDeliverables;
			} else {
			// main deliverable without subs , does process like the sub deliveables	
				$averageActionProgress = $this->_calculateActionProgress( $deliverable['ref'] );				
				$totalProgress 		   = $totalProgress + $averageActionProgress;
			}
		}
		
		if( $countDeliverables == 0 ){
			$average = 0;
		} else {
			$average = ($totalProgress / $countDeliverables);
		}
		return $average; 
	}
	
	
	function _calculateActionProgress( $id )
	{
		$actionObject  = $this->loadModel("Action");
		$actions 	   = $actionObject -> getActions( $id, "" );
		$total 		   = 0;
		$countActions  = count($actions); 
		foreach( $actions as $key => $action){
			$total = $action['progress'] + $total;
		}
		$averageProgress = 0;
		if( $countActions  == 0){
			$averageProgress = 0 ;
		} else {
			$averageProgress = $total/$countActions; 		
		}
	
		return $averageProgress;
	}
	
	function getContract()
	{
		$contract 	 = $this -> loadModel("Contract");		
		$headernames = $this -> loadModel("Naming");
		$headers	 = $headernames -> getNaming();
		$contractDetail =  $contract -> getContract( $_POST['id'] );
		$contractArr    = array(); 
		foreach( $headers as $key => $header){
			$name = $header['name'];
			if( isset($contractDetail[$name]) ){
				$contractArr[$name] = array(
									"colName" 		   => $header['client_terminology'],
									"value" 	       => $contractDetail[$name],
					);  
			}
		}
		//$contractArr['contractDefaults'] = $contractDetail['contractdefaults'];
		echo json_encode( $contractArr );
	}
	
	function _contractInfo( $contract )
	{
		$headernames = $this -> loadModel("Naming");
		$headers	 = $headernames -> getNaming();
		$contractArr    = array(); 
		foreach( $headers as $key => $header){
			$name = $header['name'];
			if( isset($contract[$name]) ){
				$contractArr[$name] = array(
									"colName" 		   => $header['client_terminology'],
									"value" 	       => $contract[$name],
					);  
			}
		}
		return $contractArr;
	}
	
    /*---------------------------------------------- Authorization ----------------------------------------------*/

	function getContractAuthorizations()
	{
		$contractObj = $this -> loadModel("Contract");
		$contract   = $contractObj -> getAuthorization( $_POST['id'] );
		$response = array();
		$changes  = ""; 
		foreach( $contract as $key => $value){
				
			$changes     = $value['changeBy']." has made the following changes<br />";		
			$changes     .= "Added description ".$value['description']."<br />";
			$changes 	 .= "Added approval ".($value['approval'] == 1 ? "Yes" : "No" )."<br />";			
			$response[]  =  array( "changes" => $changes, "date" => $value['insertdate'], "status" => $value['status'] );
		}
		echo json_encode( $response );
	}
	
	
	function getDeliverebleAuthorizations()
	{
		$deliverableObj = $this -> loadModel("Deliverable");
		$deliverabele   = $deliverableObj -> getAuthorization( $_POST['id'] );
		$response = array();
		$changes  = ""; 
		foreach( $deliverabele as $key => $value){
			$changes      = $value['changeBy']." has made the following changes<br />";				
			$changes     .= "Added description ".$value['description']."<br />";
			$changes 	 .= "Added approval ".($value['approval'] == 1 ? "Yes" : "No" )."<br />";			
			$response[]  =  array( "changes" => $changes, "date" => $value['insertdate'], "status" => $value['status'] );
		}
		echo json_encode( $response );
	}
	
	function getActionAuthorizations()
	{
		$actionObj 	= $this -> loadModel("Action");
		$actions 	= $actionObj  -> getAuthorization( $_POST['id'] );
		$response = array();
		$changes  = ""; 
		foreach( $actions as $key => $value){
			$changes      = $value['changeBy']." has made the following changes<br />";				
			$changes     .= "Added description ".$value['description']."<br />";
			$changes 	 .= "Added approval ".($value['approval'] == 1 ? "Yes" : "No" )."<br />";			
			$response[]  =  array( "changes" => $changes, "date" => $value['insertdate'], "status" => $value['status'] );
		}
		echo json_encode( $response );
	}
	
    /*---------------------------------------------- Authorization ----------------------------------------------*/
	
	function getContractDetail()
	{	
		$contractObj = $this -> loadModel("Contract");
		$del 		 = $this->loadModel("Deliverable");
		$act 		 = $this->loadModel("Action");		
		$delAction   = $contractObj -> getContractDelAction( $_REQUEST['id']);
		
		$contract    = $contractObj->getContract( (isset($_REQUEST['id']) ? $_REQUEST['id'] : "")  );	
			
		$result 	 = $del->getDeliverables( $_REQUEST['id'], "" );
		$deliverables= $result['deliverable'];
		$total 		 = $result['total'];

		$mainArray   = array();
		foreach($deliverables as $key => $deliverable)
		{	
			$conditions 	= ""; 
			$actions 		= $act->getActions( $deliverable['ref'] , $conditions);
			$assessment		= array();
			if(isset($deliverable['assessment']) && !empty($deliverable['assessment']))
			{
				$assessment = $this->_processAssessment( $deliverable['assessment'] );
			}
			
			if( isset($deliverable['maindeliverableid']) && !empty($deliverable['maindeliverableid']))
			{
				$mainArray[$deliverable['maindeliverableid']]['sub_deliverable'][$deliverable['ref']] = array(
													"ref"			   => $deliverable['ref'],
													"deliverable"	   => $deliverable['deliverable'],				
													"category"	   	   => $deliverable['deliverable_category'],																	
													"type"	  		   => "sub deliverable",													
													"owner" 		   => $deliverable['deliverable_owner'],
													"deadline" 		   => $deliverable['deadline'],
													"delivered_weight" => $deliverable['delivered_weight'],
													"quality_weight"   => $deliverable['quality_weight'],
													"other_weight"	   => $deliverable['other_weight'],
													"assessment"	   => $assessment												
													);
			} else {
			$mainArray[$deliverable['ref']] = array(
													"ref"			   => $deliverable['ref'],			
													"deliverable" 	   => $deliverable['deliverable'],
													"category"	   	   => $deliverable['deliverable_category'],																			
													"type"			   => "main deliverable",
													"owner" 		   => $deliverable['deliverable_owner'],
													"deadline" 		   => $deliverable['deadline'],
													"delivered_weight" => $deliverable['delivered_weight'],
													"quality_weight"   => $deliverable['quality_weight'],
													"other_weight"	   => $deliverable['other_weight'],
													"assessment"	   => $assessment																		
													);
			}

			if( isset($actions) && !empty($actions))
			{
				foreach( $actions as $index => $action)
				{
					
					if( isset($mainArray[$deliverable['maindeliverableid']]['sub_deliverable'][$deliverable['ref']]) && !empty($mainArray[$deliverable['maindeliverableid']]['sub_deliverable'][$deliverable['ref']])){
						$mainArray[$deliverable['maindeliverableid']]['sub_deliverable'][$deliverable['ref']]['action'][$action['ref']] = $action;
					} else {
						$mainArray[$deliverable['ref']]['actions'][$action['ref']] = $action;
					}
				}
			}
		}
		//$total 			= count($mainArray); 
		$categorized 	= $this->_categorize( $mainArray );
		$contract		= $this->_contractInfo( $contract );
		echo json_encode( array("deiverable" => $categorized , "total" => $total, "contract" => $contract) );		
		
	}
	
	function _processAssessment( $delivereableAssesment )
	{
		$dAssesment   = unserialize( $delivereableAssesment );
		$assesmentArr = array();
		$table 		  = "<table width='100%'>"; 	
		$table 		 .= "<tr>"; 
		foreach($dAssesment as $key => $assessment){		
			
			if(strstr(key($assessment), "delivered")){
				$index = str_replace("delivered_", "", key($assessment));	
				$assesmentArr['delivered'][$index]	= $assessment[key($assessment)];
				$table 		 .= "<td>".$assessment[key($assessment)]."</td>";			
			} else if(strstr(key($assessment), "quality")){
				$index = str_replace("quality_", "", key($assessment));				
				$assesmentArr['quality'][$index]	= $assessment[key($assessment)];		
				$table 		 .= "<td>".$assessment[key($assessment)]."</td>";										
			} else {
				$index = str_replace("other_", "", key($assessment));			
				$assesmentArr['other'][$index]	= $assessment[key($assessment)];	
				$table 		 .= "<td>".$assessment[key($assessment)]."</td>";								
			}		
		}
		$table 		 .= "<tr>";					
		$table 		 .= "<table>"; 		
		return $table;
	}
	
	
	function _categorize( $deliverables )
	{
		$categorisedArray = array();
		foreach( $deliverables as $key => $value)
		{
			$categorisedArray[$value['category']][$key] = $value;
		}
		return $categorisedArray;
	}
	
	function approvalData()
	{
		$actionObj 			= $this->loadModel('Action');
		$conditions 	    = "A.actionstatus & ".Action::ACTION_AWAITING." = ".Action::ACTION_AWAITING;
		$conditions		   .= " AND A.actionstatus & ".Action::ACTION_APPROVED." <> ".Action::ACTION_APPROVED; 
		$awaitingApproval   = $actionObj -> getActions( $_POST['id'], $conditions );
		
		$conditions 	 	= "";
		$conditions			= "A.actionstatus & ".Action::ACTION_APPROVED." = ".Action::ACTION_APPROVED; 
		$actionsApproved    = $actionObj -> getActions( $_POST['id'], $conditions );
		
		$awaiting 			= $this->_processAction( $awaitingApproval );
		$approved 			= $this->_processAction( $actionsApproved );		
		
		echo   json_encode( array("approved" => $approved, "awaiting" => $awaiting, "headers" => $this->actionHeadArray, "total" => count($this->actionHeadArray), "actionStatues" => $this->actionStatus ) );
	}
	
	function _processAction( $actions )
	{
		$headernames   = $this -> loadModel("Naming");
		$headers 	   = $this->_sorStructure( $headernames -> getNaming() ); 
		
		$actionArray = array();
		$actionStatus= array();
		foreach( $actions as $key => $value )
		{	
			foreach( $headers as $field => $val)
			{	
				$this->actionStatus[$value['ref']] = $value['action_status'];				
				if( isset( $value[$field]) ){
					if($field == "progress"){
						$actionArray[$value['ref']][$field] = $value[$field]." %";
						$this->actionHeadArray[$field]		= $val;
					} else {
						$actionArray[$value['ref']][$field] = $value[$field];
						$this->actionHeadArray[$field]		= $val;						
					}
				}
			}
		}
		return $actionArray;
	}
	
	function approveAction()
	{
		$actionObj 	= $this->loadModel('Action');
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");	
			
		$action		= $actionObj -> getAction( $_POST['id'] );	
		$owner 		= $user -> getUser( $action['action_owner'] );		
		
		$updatedata = array("response" => $_POST['response'], "actionstatus" => $action['actionstatus'] + Action::ACTION_APPROVED);
		$result 	= $actionObj->updateAction( $updatedata, $_POST['id'] ); 

		$mailText	= "";
		if($result == 1){
			$body = $_SESSION['tkn']." has approved action #".$action['ref'];
			if( $email -> to( $owner['email'] ) -> from("admin@ignite4u.com") -> body($body) -> send() != -1){
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; Email sent successfully ";
			} else{
				$mailText  = " &nbsp;&nbsp;&nbsp;&nbsp; An error occured sending mail ";				
			}			
			$response = array("text" => "Action approved successfully <br />".$mailText, "error" => false);				
		} else if($result == 0){
			$response = array("text" => "An error occured approving the action", "error" => true);		
		} else {
			$response = array("text" => "An error occured approving the action", "error" => true);
		}
		echo json_encode( $response );
	}
	
	function declineAction()
	{

		$actionObj 	= $this->loadModel('Action');
		$user 		= $this -> loadModel("UserAccess");	
		$email 		= $this -> loadModel("Email");	
			
		$action		= $actionObj -> getAction( $_POST['id'] );	
		$owner 		= $user -> getUser( $action['action_owner'] );		
		
		$updatedata = array("response" => $_POST['response'], "actionstatus" => $action['actionstatus'] + Action::ACTION_DECLINED);
		$result 	= $actionObj->updateAction( $updatedata, $_POST['id'] ); 

		$mailText	= "";
		if($result == 1){
			$body = $_SESSION['tkn']." has declined action #".$action['ref'];
			if( $email -> to( $owner['email'] ) -> from("admin@ignite4u.com") -> body($body) -> send() != -1){
				$mailText  = "Email sent successfully ";
			} else{
				$mailText  = "An error occured sending mail ";				
			}			
			$response = array("text" => "Action declined successfully <br />".$mailText, "error" => false);				
		} else if($result == 0){
			$response = array("text" => "An error occured approving the action", "error" => true);		
		} else {
			$response = array("text" => "An error occured approving the action", "error" => true);
		}
		echo json_encode( $response );
	}
	
	function uploadFile()
	{
		$upload_dir = "../../files/".$_SESSION['cc']."/SLA/contract/";
						
		$key 	  				   = key($_FILES);
		$filename 				   = basename($_FILES[$key]['name']);
		$_SESSION['contracts_update_uploads'][$key.time()] = $_FILES[$key]['name'];
		$fileuploaded 			   = str_replace("attachment_", "", $key)."_".time().".";
		$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
		$response 				   = array();
		//$upload_response 		   = move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext);
		if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
		{			
			$response = array(
								"text" 			=> "File uploaded .....",
								"error" 		=> false,
								"filename"  	=> $filename,
								"key"			=> $key,
								"filesuploaded" => $_SESSION['contracts_update_uploads']
								);
		} else {
			$response = array("text" => "Error uploading file ...." , "error" => true );
		}		
		echo json_encode( $response ) ;		
	}	
	
	function uploadDeliverable()
	{
		$upload_dir = "../../files/".$_SESSION['cc']."/SLA/deliverable/";
						
		$key 	  				   = key($_FILES);
		$filename 				   = basename($_FILES[$key]['name']);
		$_SESSION['deliverable_update_uploads'][$key.time()] = $_FILES[$key]['name'];
		$fileuploaded 			   = str_replace("attachment_", "", $key)."_".time().".";
		$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
		$response 				   = array();
		//$upload_response 		   = move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext);
		if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
		{			
			$response = array(
								"text" 			=> "File uploaded .....",
								"error" 		=> false,
								"filename"  	=> $filename,
								"key"			=> $key,
								"filesuploaded" => $_SESSION['deliverable_update_uploads']
								);
		} else {
			$response = array("text" => "Error uploading file ...." , "error" => true );
		}		
		echo json_encode( $response ) ;
	}
	
	function uploadAction()
	{
		$upload_dir = "../../files/".$_SESSION['cc']."/SLA/action/";
						
		$key 	  				   = key($_FILES);
		$filename 				   = basename($_FILES[$key]['name']);
		$_SESSION['action_update_uploads'][$key.time()] = $_FILES[$key]['name'];
		$fileuploaded 			   = str_replace("attachment_", "", $key)."_".time().".";
		$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
		$response 				   = array();
		//$upload_response 		   = move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext);
		if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
		{			
			$response = array(
								"text" 			=> "File uploaded .....",
								"error" 		=> false,
								"filename"  	=> $filename,
								"key"			=> $key,
								"filesuploaded" => $_SESSION['action_update_uploads']
								);
		} else {
			$response = array("text" => "Error uploading file ...." , "error" => true );
		}		
		echo json_encode( $response ) ;
	}	
	
	function saveAssesment()
	{	
		$del 		 = $this->loadModel("Deliverable");
		foreach($_POST['formData'] as $key => $valueArr){
			$insertdata['assessment'][] = array($valueArr['name'] =>  $valueArr['value'] );
		}
		$insertdata['assessment'] 	  = serialize( $insertdata['assessment'] );
		$insertdata['deliverable_id'] = str_replace("assess_", "", $_POST['id']);
		$insertdata['insertuser'] 	  = $_SESSION['tid'];		
		
		$res 	 = $del -> assessment( $insertdata );
		$reponse = array(); 	
		if( $res > 0 ){
			$response = array("error" => false, "text" => "Deliverable successfully assessed");
		} else {
			$response = array("error" => true, "text" => "Error assessing the deliverable");			
		}	
		
		echo json_encode( $response );
	}
	
   function saveContractAssessment()
   {
		$response = array();
		$response = array("error" => false, "text" => "Contract successfully assessed");
		echo json_encode( $response );
   }
   
    function saveTemplateChanges()
   {
		$response = array();
		$response = array("error" => false, "text" => "Contract template updated . . ");
		echo json_encode( $response );
   }
   
	
}

$method 	=  $_GET['action'];
$controller = new ManageController();
if(method_exists($controller, $method)){
	@call_user_method_array($method, $controller, array());
}
?>