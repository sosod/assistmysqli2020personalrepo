<?php
$scripts = array("action.js", "jquery.ui.actionapprove.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){	
	  $("#displayaction").actionapprove();
	})
</script>
<?php JSdisplayResultObj(""); ?>
<div id="displayaction"></div>

<input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>" />
<!-- <table class="noborder">
	<tr>
    	<td valign="top" class="noborder">
        	<h4>Actions Awaiting Approval</h4>
            <table id="actions_awaiting"></table>
        </td>
    	<td valign="top" class="noborder">
        	<h4>Actions Approved</h4>
            <table id="actions_approved"></table>            
        </td>        
    </tr>
</table> -->
<div id="action_approval" style="display:none">
<form method="post">
	<table class="noborder">
    	<tr>
        	<th class="noborder">Response:</th>
        	<td class="noborder"><textarea name="response" id="response"></textarea></td>            
        </tr>
    	<tr>
        	<th class="noborder">Sign Off: </th>
        	<td class="noborder">
            	<select name="signoff">
                	<option value="1">Yes</option>
                	<option value="0">No</option>                    
                </select>
            </td>            
        </tr>   
        <tr>
        	<td colspan="2" class="noborder"><span id="approve_loader"></span></td>
        </tr>           
    </table>
   </form>
</div>

