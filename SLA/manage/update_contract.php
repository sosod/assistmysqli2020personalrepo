<?php
$scripts = array("contract.js", "ajaxfileupload.js");
require_once("../header.php");
$ctr 		= new Contract(); 
$contract   = $ctr -> getContract( $_GET['id'] ); 
$ty 		= new ContractType(); 
$types  	= $ty -> getContractTypes(); 
$cat 		= new ContractCategory(); 
$categories	= $cat -> getCategories(); 
$sup 		= new ContractType(); 
$suppliers  = $sup -> getContractTypes(); 
$us 		= new UserAccess(); 
$users  	= $us -> getUsers(); 
$assfreq	= new AssessmentFrequency(); 
$assfreqs  	= $assfreq -> getAssessmentFrequencies();
$stat		= new  ContractStatus(); 
$statuses  	= $stat -> getStatuses();

?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
<tr>
	<td class="noborder" width="50%">
		<h4>Contract Details</h4>
    <table id="table_contract_update">  
        <tr>
            <th>Ref #:</th>
            <td><?php echo $_GET['id']; ?></td>
        </tr>        
        <tr>
            <th>Project Name:</th>
            <td><?php echo $contract['contract']; ?></td>
        </tr>
        <tr>
            <th>Project Value <em>(Budget)</em>:</th>
            <td><?php echo $contract['budget']; ?></td>
        </tr>
        <tr>
            <th>Comments clarifying the Project:</th>
            <td><?php echo $contract['project_comments']; ?></td>
        </tr>
        <tr>
            <th>SLA Type:</th>
            <td>
              <!--  <select name="type" id="type">
                    <option value="">--select sla type--</option>            
                <?php
                foreach( $types as $type){
                ?>
                    <option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $contract['type'] ) { ?> selected="selected" <?php  }?>><?php echo $type['name']; ?></option>			
                <?php
                }
                ?>                
                </select> -->
                <?php echo $contract['contract_type']; ?>
            </td>
        </tr>
        <tr>
            <th>SLA Category:</th>
            <td><!-- 
                <select name="category" id="category">
                    <option value="">--select sla category--</option>
                <?php
                foreach( $categories as $category){
                ?>
                    <option value="<?php echo $category['id']; ?>" <?php if($category['id'] == $contract['category'] ) { ?> selected="selected" <?php  }?>><?php echo $category['name']; ?></option>			
                <?php
                }
                ?>                 
                </select> -->
                      <?php echo $contract['contract_category']; ?>
            </td>
        </tr>  
        <tr>
            <th>Supplier:</th>
            <td>
                <!-- <select name="supplier" id="supplier">
                    <option value="">--select sla suppier--</option>
                <?php
                foreach( $suppliers as $supply){
                ?>
                    <option value="<?php echo $supply['id']; ?>" <?php if($supply['id'] == $contract['supplier'] ) { ?> selected="selected" <?php  }?>><?php echo $supply['name']; ?></option>			
                <?php
                }
                ?>                 
                </select> -->
                <?php echo $contract['contract_supplier']; ?>
            </td>
        </tr>        
        <tr>
            <th>Date Signature of Tender Document:</th>
            <td><?php echo $contract['signnature_of_tender']; ?>
            </td>
        </tr>   
        <tr>
            <th>Date Signature of SLA Document:</th>
            <td><?php echo $contract['signature_of_sla']; ?>
            </td>
        </tr> 
        <tr>
            <th>Contracted Completion Date:</th>
            <td><?php echo $contract['completion_date']; ?>
            </td>
        </tr>  
        <tr>
            <th>Responsible Person:</th>
            <td>
            <!--
                <select name="contract_owner" id="contract_owner">
                    <option value="">--select person responsible--</option>
                <?php
                foreach( $users as $user){
                ?>
                    <option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $contract['contract_owner'] ) { ?> selected="selected" <?php  }?>><?php echo $user['tkname']." ".$user['tksurname']; ?></option>			
                <?php
                }
                ?>                     
                </select> -->
                <?php echo $contract['contract_owner']; ?>
            </td>
        </tr> 
        <?php 			
		if(($contract['contractdefaults'] & Contract::TEMPLATE) != Contract::TEMPLATE )  {?>
        <tr>
            <th>
                Template to be used:
            </th>
            <td>
                <?php echo $contract['template_name']; ?>
            </td>
        </tr> 
        <?php 
			}
		?>
        <tr>
            <th>
                What is the Assessment Frequency for this Contract:
                <!--<em>
                If weekly , select day of week and time to be send<br />
                If monthly , select day of month and time to be send<br />
                If Quartely , select day and time to be send etc.
                </em> -->
            </th>
            <td>
               <!-- <select name="assessment_frequency" id="assessment_frequency">
                    <option value="">--assessment frequency--</option>
                    <?php
                    foreach( $assfreqs as $assfreq){
                    ?>
                        <option value="<?php echo $assfreq['id']; ?>" <?php if($assfreq['id'] == $contract['assessment_frequency'] ) { ?> selected="selected" <?php  }?>><?php echo $assfreq['name']; ?></option>			
                    <?php
                    }
                    ?>                  
                </select> -->
                <?php echo $contract['contract_assessment_frequency']; ?>
            </td>
        </tr> 
        <tr id="weekly" style="display:none;" class="assfreq">
            <th>Date:</th>
            <td><input type="text" name="weeklydate" id="weeklydate" class="assessment_frequency_date" /></td>
        </tr>                                
        <tr id="monthly" style="display:none;" class="assfreq">
            <th>Date:</th>
            <td><input type="text" name="monthlydate" id="monthlydate" class="assessment_frequency_date" /></td>
        </tr>                                
        <tr id="quarterly" style="display:none;" class="assfreq">
            <th>Date:</th>
            <td><input type="text" name="quarterlydate" id="quarterlydate" class="assessment_frequency_date" /></td>
        </tr>                                        
        <tr>
            <th>
                What is the Notification Frequency for this contract:
            </th>
            <td>
                <!-- <select name="notification_frequency" id="notification_frequency">
                    <option value="">--select notification frequency--</option>
                </select> -->
             <?php echo $contract['contract_notification_rules']; ?>
            </td>
        </tr> 
        <tr>
          <td class="noborder"><?php displayGoBack("", ""); ?></td>
         <td class="noborder"><?php displayAuditLogLink("contract_update", false) ?></td>
        </tr>                                               
    </table>
    </td >
	<td valign="top" class="noborder">
		<h4>Update Contract</h4>
	<table width="100%">
         <tr>
             <th>Response:</th>
             <td><textarea name="response" id="response"></textarea></td>
        </tr>      
         <tr>
         <th>Contract Status:</th>
            <td>    
                <select name="contract_status" id="contract_status">
                    <option value="">--status--</option>
                    <?php
                    foreach( $statuses as $status){
                    ?>
                        <option value="<?php echo $status['id']; ?>" 
						<?php
						 if($status['id'] == $contract['contract_status'] ) {
						  ?>
                           selected="selected"
                           <?php
						  } else if($status['id'] == 1) {
						  	?>
                            selected="selected"
							<?php
						  }
						  if( $status['id'] == 1){
						?>
                        disabled="disabled"
                        <?php } ?>
                        >
						<?php echo $status['name']; ?>
                        </option>			
                    <?php
                    }
                    ?>                  
                </select>
                </td>
        </tr>
        <tr class="closed_contract">
        	<th>Contract Value:</th>
        	<td>
            	<input type="text" name="project_value" id="project_value" value="" />
            </td>            
        </tr>
        <tr class="closed_contract">
        	<th>Saving/Overrun:</th>
        	<td>
            	<input type="text" name="saving" id="saving" value="" />
            </td>            
        </tr>        
        <tr class="closed_contract">
        	<th>Date Completed:</th>
        	<td>
            	<input type="text" name="date_completed" id="date_completed" value="" class="datepicker" />
            </td>            
        </tr>                
        <tr id="tr_remind">
        	<th>Remind On:</th>
            <td><input type="text" name="remindon" id="remindon" value="<?php echo $contract['remindon']; ?>" class="datepicker" /></td>
        </tr>             
         <tr><th>Attach a document:</th>
         <td>
            <form method="post" enctype="multipart/form-data">
                <span id="uploading"></span>
                <input type="file"  name="attachment_<?php echo time(); ?>" id="attachment"  class="attach"/> 
               <!--  <input type="button" name="addanother" id="addanother" value="Add Another" />  -->
                <input type="hidden" name="fireUpload" value="true"  />    
            </form>
            <input type="hidden" name="contractid" value="<?php echo $_GET['id']; ?>" id="contractid" />
         </td>
        </tr>         
        <tr>
        	<th></th>
            <td><input type="checkbox" name="requestapproval" id="requestapproval" value="" disabled="disabled" />
            	Notification to be sent that Update was completed
            </td>
        </tr>                     
         <tr>
         <th></th>
         <td>
         	<input type="submit" id="update_contract" name="update_contract" value="Save" />
         	<input type="submit" id="cancel_update" name="cancel_update" value="Cancel" />
         </td>
        </tr>     
    </table>
</td>
</tr>
</table>
<div id="updatelog"></div>
</div>