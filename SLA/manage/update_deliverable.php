<?php
$scripts = array("deliverable.js", "jquery.ui.deliverable.js", "ajaxfileupload.js");
require_once("../header.php");
$cont    = new Contract();
$contract = $cont->getAContract($_GET['id']);

$obj 		  = new Deliverable();
$deliverable  = $obj->getDeliverable( $_GET['id'] );

$delStatus	   = new DeliverableStatus();
$delStatuses  = $delStatus->getDeliverableStatuses();
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table width="100%" class="noborder">
	<tr>
    	<td valign="top" class="noborder" width="50%">
    		<h4>Deliverable Details</h4>
           <table width="100%" style="position:realtive;">
                <tr>
                    <th>Ref #:</th>
                    <td><?php echo $_GET['id']; ?></td>                        
                </tr>
                <tr>
                    <th>Deliverable: </th>
                    <td><?php echo $deliverable['deliverable']; ?></td>                        
                </tr>
                <tr>
                    <th>Deliverable Description:</th>
                    <td><?php echo $deliverable['deliverable_description']; ?></td>                        
                </tr>
                <tr>
                    <th>Deliverable Category:</th>
                    <td><?php echo $deliverable['category']; ?></td>                        
                </tr>                    
                <tr>
                    <th>Deliverable Type:</th>
                    <td><?php echo ($deliverable['type'] == 0 ? "Main deliverable" : "Sub Deliverable"); ?></td>                        
                </tr>
                <tr>
                    <th>Main Deliverable:</th>
                    <td><?php echo $deliverable['main_deliverable']; ?></td>                        
                </tr>
                <tr>
                    <th>Deliverable Owner:</th>
                    <td><?php echo $deliverable['deliverable_owner']; ?></td>                        
                </tr>
                <tr>
                    <th>Deliverable Status:</th>
                    <td><?php echo $deliverable['status']; ?></td>                        
                </tr>                
                <?php 
                if((Contract::DELIVERED &  $contract['contractdefaults']) == Contract::DELIVERED ){
                ?>
                <tr class="delweights">
                    <th>Delivered Weight:</th>
                    <td><?php echo $deliverable['dweight']; ?></td>                        
                </tr>
                <?php
                    }
                if((Contract::QUALITY &  $contract['contractdefaults']) == Contract::QUALITY ){						
                 ?>
                <tr class="delweights">
                    <th>Quality Weight:</th> 
                    <td><?php echo $deliverable['qweight']; ?></td>                            
               </tr> 
               <?php 
                }
                if((Contract::OTHER &  $contract['contractdefaults']) == Contract::OTHER ){					
               ?>     
               <tr class="delweights">                                                                      
                    <th>Other Weight:</th>
                    <td><?php echo $deliverable['oweight']; ?></td>                          
               </tr>
               <?php 
                }
               ?>
               <tr>
                    <th>Deadline:</th>
                    <td><?php echo $deliverable['deadline']; ?></td>
                </tr>                      
                <tr>
                    <th>Remind On:</th>
                    <td><?php echo $deliverable['remind_on']; ?></td>                     
                <tr>      
                    <td class="noborder"><?php displayGoBack("", ""); ?></td>              	          
                    <td class="noborder"><?php displayAuditLogLink("deliverable_update", false) ?></td>
                </tr>                    
            </table>        
         </td>
    	<td valign="top" class="noborder">
    		<h4>Deliverable/Sub Deliverable Update</h4>
        	<table width="100%">
        	  <tr>
                	<th>Description:</th>
                    <td><textarea name="response" id="response" cols="35" rows="7"></textarea></td>
                </tr>
                <tr>
                	<th>Status:</th>
                    <td>
                    	<select id="status" name="status">
                        	<option value="">--status--</option>
                            <?php
							foreach($delStatuses as $key => $status){
							?>
                            <option <?php if($status['id'] == $deliverable['deliverable_status']) {?> selected="selected" <?php } ?> value="<?php echo $status['id']; ?>"><?php echo $status['client_terminology']; ?></option>
                            <?php
							}
							?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<th>Remind On:</th>
                    <td><input type="text" name="remindon" id="remindon" value="<?php echo $deliverable['remind_on']; ?>" class="datepicker"  /></td>
                </tr>
                <tr>
                	<th>Attachement:</th>
                    <td>
                    <form method="post" enctype="multipart/form-data">
                        <span id="uploading"></span>
                        <input type="file"  name="attachment_<?php echo time(); ?>" id="attachment"  class="attach"/> 
                        <!--  <input type="button" name="addanother" id="addanother" value="Add Another" />  -->
                        <input type="hidden" name="fireUpload" value="true"  />    
                    </form>
            </td>
                </tr>       
                <tr>      
                    <th></th>              	          
                    <td>
                        <input type="submit" name="update_deliverable" id="update_deliverable" value="Update" />
                        <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']?>" />
                    </td>
                </tr>                                                            
            </table>
        </td>        
    </tr>
</table>

</div>
<div id="updatelog"></div>