<?php
$scripts = array("action.js");
require_once("../header.php");
$act	 	  = new Action();
$action	 	  = $act -> getAction( $_GET['id'] );
$actU 	 	  = new UserAccess();
$users		  = $actU -> getUsers();
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="add-action" name="add-action" method="post">
<table>
    <tr>
        <th>Ref #:</th>
        <td>#<?php echo $_GET['id']; ?></td>                    
    </tr>
    <tr>
    <th>Action:</th>
    <td>
    	<textarea placeholder="enter action name" name="action_name" id="action_name" rows="7" cols="35"><?php echo $action['action'];?></textarea>
    </td>                    
    </tr>
    <tr>
        <th>Action Owner:</th>
        <td>
        <select name="owner" id="owner">
            <option value="">--owner--</option>
            <?php 
            foreach($users as $key => $user){
            ?>
            <option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $action['action_owner']) {?> selected<?php }?>>
                <?php echo $user['tkname']." ".$user['tksurname']; ?>
            </option>                	
            <?php
            }
            ?> 
        </select>
        </td>                    
    </tr>                            
    <tr>
        <th>Measurable:</th>
        <td>
        	<textarea placeholder="enter measurable" name="measurable" id="measurable" rows="7" cols="35"><?php echo $action['measurable'];?></textarea>
        </td>                    
    </tr>    
    <tr>
        <th>Progress:</th>
        <td><?php echo $action['progress'];?><em style='color:red'>%</em></td>                    
    </tr>                              
    <tr>
        <th>Status:</th>
        <td><?php echo $action['status'];?></td>                    
    </tr>                                                
    <tr>
        <th>Deadline:</th>
        <td>
        <input type="text" name="deadline" id="deadline" placeholder="select deadline" value="<?php echo $action['deadline']; ?>"  class="datepicker"/>
        </td>                    
    </tr>                                                                                               
    <tr>
     <th>Remind On:</th>
     <td>
       <input type="text" name="remindon" id="remindon" placeholder="select remind on" value="<?php echo $action['remind_on'];?>"  class="datepicker"/>
    </td>
    </tr>
    <tr>
        <th></th>
        <td>
        <input type="submit" name="edit" id="edit" value="Edit"  class="editaction"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="cancel" id="cancel" value="Cancel"  class="editaction"/>        
        <input type="hidden" name="actionid" id="actionid" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['contractid']; ?>" />        
        </td>
    </tr>                                                                                           
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("action_edit", false) ?></td>
    </tr>                                                                                               
</table>
</form>
</div>
<div id="editLog"></div>