<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
$contractObj = new  Contract();
$del 		 = new Deliverable();
$act 		 = new Action();		
$delAction   = $contractObj -> getContractDelAction( $_REQUEST['id']);
$contract    = $contractObj->getContract( (isset($_REQUEST['id']) ? $_REQUEST['id'] : "")  );	
$result 	 = $del->getDeliverables( $_REQUEST['id'], "main" );

$mainDeliverable = $result['deliverable'];
$assessedStatus = unserialize($contract['deliverable_status']);
$mainArry   = array(); 
foreach( $mainDeliverable as $key => $deliverable )
{
	if( ($deliverable['delstatus'] &  Deliverable::MAIN_WITH_SUB ) == Deliverable::MAIN_WITH_SUB ){
		$mainArray[$deliverable['ref']] =  processDelivereable( $deliverable, "main" );	
		
		//get the sub deliverable
		$subResult =  $del->getDeliverables( $deliverable['ref'], "sub" );
		$totalD  = 0;
		$totalQ  = 0;			
		$totalO  = 0;				
		//prepare the sub deliverable
		foreach( $subResult['deliverable'] as $subKey => $subDeliverable ){

		
			$mainArray[$deliverable['ref']]['subdeliverable'][$subDeliverable['ref']] =  processDelivereable( $subDeliverable , "subdeliverable");	
			// process the sub deliverables
			$assessment = processAssesement( $subDeliverable['assessment'] );
			$mainArray[$deliverable['ref']]['subdeliverable'][$subDeliverable['ref']]['assessmentData'] = $assessment;	

			$totalD  				 = $totalD  +  $assessment['delivered']['total'];				
			$totalQ  				 = $totalQ  +  $assessment['quality']['total'];				
			$totalO  				 = $totalO  +  $assessment['other']['total'];														
		}
		$mainAssess = array("delivered" => $totalD, "quality" => $totalQ, "other" => $totalO); 
		$mainArray[$deliverable['ref']]['assessment'] = $mainAssess;	
	} else {
	// main deliverable without subs , does process like the sub deliveables
		$mainArray[$deliverable['ref']] =  processDelivereable( $deliverable, "main" );		
		$assessment = processAssesement( $deliverable['assessment'] );	
		$mainArray[$deliverable['ref']]['assessment'] = $assessment;	
	}
}

// create the array to be displayed 
function processDelivereable( $deliverable, $type )
{
	$delArray = array();
		$delArray["ref"]		 = $deliverable['ref'];					
		$delArray["deliverable"] = $deliverable['deliverable'];	
		$delArray["status"] 	 = $deliverable['deliverable_status'];	
		$delArray["statusid"] 	 = $deliverable['status'];						
		$delArray["category"]  	 = $deliverable['deliverable_category'];																	
		$delArray["type"]		 = $type;													
		$delArray["owner"] 	     = $deliverable['deliverable_owner'];
		$delArray["deadline"]    = $deliverable['deadline'];
		$delArray['delstatus']	 = $deliverable['delstatus'];
	return $delArray;
}

function processAssesement( $assesments )
{

	$assesArray = unserialize( $assesments );
	$assessmentArray = array();
	if(isset($assesArray) && !empty($assesArray)) {
		foreach($assesArray as $key => $assess )
		{
			$arrayKey = key($assess);
			if( strstr($arrayKey, "delivered") ){
				$index									= str_replace("delivered_", "", key($assess));	
				$assessmentArray['delivered'][$index]	= $assess[key($assess)];
			} else if( strstr($arrayKey, "quality") ) {
				$index									= str_replace("quality_", "", key($assess));	
				$assessmentArray['quality'][$index]	= $assess[key($assess)];
			} else {
				$index									= str_replace("other_", "", key($assess));	
				$assessmentArray['other'][$index]	= $assess[key($assess)];
			}
		}
		$assessmentArray['assessement_status']   			 = "assessed";	
	} else {
		$assessmentArray['other']['total']       = 0;
		$assessmentArray['other']['comment']     = "";
		$assessmentArray['delivered']['total']   = 0;
		$assessmentArray['delivered']['comment'] = "";
		$assessmentArray['quality']['total']     = 0;
		$assessmentArray['quality']['comment']   = "";		
		$assessmentArray['assessement_status']   = "notassessed";				
	}
	return $assessmentArray;
}
$categorized 	= _categorize( $mainArray );

function _categorize( $deliverables )
{
	$categorisedArray = array();
	foreach( $deliverables as $key => $value)
	{
		$categorisedArray[$value['category']][$key] = $value;
	}
	return $categorisedArray;
}

?>
<script language="javascript">
$(function(){

var contractDefaults = $("#contractDefaults").val();

$("#assess").live("click", function(){
var id =  $(this).attr("name")
var dialog = $("<div />",{id:"div_"})
			   .append($("<form />",{id:"form_"+id})
				 .append($("<table />",{align:"center"})
					.append($("<tr />")
					  .append($("<th />",{html:"Deliverable"}))
					  .append($("<td />")
						.append($("<table />").addClass("noborder")
						  .append($("<tr />")
							 .append($("<th />",{html:"Total:"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
							   .append($("<select />",{name:"delivered_total"}).addClass("selectless")
								  .append($("<option />",{value:"1", text:"1"}))
								  .append($("<option />",{value:"2", text:"2"}))													  											 
								  .append($("<option />",{value:"3", text:"3"}))
								  .append($("<option />",{value:"4", text:"4"}))											  											  
								  .append($("<option />",{value:"5", text:"5"}))
								)	
							  )
						   )
						  .append($("<tr />")
							 .append($("<th />",{html:"Comment:"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
								 .append($("<textarea />",{name:"delivered_comment", id:"delivered_comment"})) 
							 )
						   )						   
						)
					  )
					)
					.append($("<tr />")
					  .append($("<th />",{html:"Quality"}))
					  .append($("<td />")
						.append($("<table />").addClass("noborder")
						  .append($("<tr />")
							 .append($("<th />",{html:"Total"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
							   .append($("<select />",{name:"quality_total"}).addClass("selectless")
								  .append($("<option />",{value:"1", text:"1"}))
								  .append($("<option />",{value:"2", text:"2"}))													  											  
								  .append($("<option />",{value:"3", text:"3"}))
								  .append($("<option />",{value:"4", text:"4"}))
								  .append($("<option />",{value:"5", text:"5"}))
								 )
							  )
						   )
						  .append($("<tr />")
							 .append($("<th />",{html:"Comment"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
								.append($("<textarea />",{name:"quality_comment", id:"quality_comment"}))  										 
							  )
						   )						   
						)
					  )
					)
					.append($("<tr />")
					  .append($("<th />",{html:"Other"}))
					  .append($("<td />")
						.append($("<table />").addClass("noborder")
						  .append($("<tr />")
							 .append($("<th />",{html:"Total:"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
							   .append($("<select />",{name:"other_total"}).addClass("selectless")
								  .append($("<option />",{value:"1", text:"1"}))
								  .append($("<option />",{value:"2", text:"2"}))
								  .append($("<option />",{value:"3", text:"3"}))
								  .append($("<option />",{value:"4", text:"4"}))											  											  
								  .append($("<option />",{value:"5", text:"5"}))
								)
							  )
						   )
						  .append($("<tr />")
							 .append($("<th />",{html:"Comment:"})).addClass("noborder")
							 .append($("<td />").addClass("noborder")
								.append($("<textarea />",{name:"other_comment", id:"other_comment"}))  							 
							  )
						   )						   
						)
					  )
					)
				  .append($("<tr />",{id:"tr_"+id}).css({"display":"none"})
						.append($("<td />",{colspan:"2"})
							.append($("<div />",{id:"response_"+id}))
						)		
					 )																
				  )
				)
				.dialog(
					  {
					   autoOpen	: true,	  
					   modal	: true,
					   width	: "450px",
					   hieght	: "auto",
					   title	: "Assessment", 
					   buttons	: {														   	
								"Save" 		: function()
								{
									$("#tr_"+id).show()
																			
									$("#response_"+id)
									 .append($("#display_objresult"))
									 
									jsDisplayResult("info", "info", "Updating Deliverable . . . .<img src='../images/loaderA32.gif' />" );									 
									//.html(" Saving Assessement . . <img src='../images/loaderA32.gif' /> ")	
									 													
									$.post("controller.php?action=saveAssesment",
									{
										id 		 : id,
										formData : $("#form_"+id).serializeArray()
									}, function( response ){
										if( response.error ){
											jsDisplayResult("error", "error", response.text );
										} else {
											jsDisplayResult("ok", "ok", response.text );
										}	
										//$("#response_"+id).html( response.text )
								
								//$("#tr_"+id).empty()
									},"json")														
								} ,"Close"	: function()
								{
									//document.location.href  = "";
									window.location.reload(true);
									$(this).dialog("destroy");													
								}
						   }
					   })		

	});

})
</script>
<?php JSdisplayResultObj(""); ?>
<table width="80%" id="contractDetail" style="display:none;">
	<tr>
    	<td valign="top">
        	<table id="table_contract" width="100%"></table>
        </td>
    	<td valign="top">
        	<table id="contract_summary" width="100%"></table>        
        </td>        
    </tr>
</table>
<div id="view_delivereable"></div>
<form method="post">
 	<input type="hidden" name="page" value="editdeliverable"  id="page" />
	<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />
</form>
<table width="100%" class="noborder">
   <tr>
    	<th>Ref</th>
    	<th>Deliverable</th>
    	<th>Owner </th>
    	<th>Deadline</th>
        <th>Status</th>
		<th colspan="6" style="text-align:center;">Assessment</th>    
    </tr>
<?php 
$totalD = 0;
$totalQ = 0;
$totalO = 0;
foreach( $categorized as $index => $deliverable ){
?>
    <tr>
        <th style='background-color:#3232CD' colspan="5"><b><?php echo $index; ?></b></th>
        	<?php 
				if( ($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED ){
			?>
                <th style='background-color:#3232CD'>Total <br />Delivered</th>
                <th style='background-color:#3232CD'>Delivered <br />Comment</th>
            <?php 
				} 
				if( ($contract['contractdefaults'] & Contract::QUALITY ) == Contract::QUALITY ){
			?>
                <th style='background-color:#3232CD'>Total<br /> Quality</th>
                <th style='background-color:#3232CD'>Quality <br />Comment</th>
            <?php 
				} 
				if( ($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER ){
			?>            
                <th style='background-color:#3232CD'>Total<br /> Other</th>
                <th style='background-color:#3232CD'>Other<br /> Comment</th> 
            <?php 
				} 
			?>                      
    </tr>
<?php 
    foreach( $deliverable as $i => $delValue){
?>
    <tr>
        <td><?php echo $delValue['ref']; ?></td>
        <td><?php echo $delValue['deliverable']; ?></td>
        <td><?php echo $delValue['owner']; ?></td>
        <td><?php echo $delValue['deadline']; ?></td> 
        <td><?php echo $delValue['status']; ?></td>             
        <?php
		if( ($delValue['delstatus']&  Deliverable::MAIN_WITH_SUB ) == Deliverable::MAIN_WITH_SUB) {
        	if(isset($delValue['assessment']) && !empty($delValue['assessment'])){    	                    
				if( ($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED ){			
        ?>
              <td>
              <?php
				 echo $delValue['assessment']['delivered'];
				 $totalD 	= $totalD  + $delValue['assessment']['delivered'];
			  ?>
			  </td>
              <td></td>
            <?php 
            	}
				if( ($contract['contractdefaults'] & Contract::QUALITY ) == Contract::QUALITY ){
				?>
            <td>
            <?php 
					echo $delValue['assessment']['quality'];
					$totalQ   = $totalQ + $delValue['assessment']['quality'];
			 ?>
			 </td>
             <td></td>
              <?php 
			  }
			 if( ($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER ){
			?>
            <td>
				<?php 
					echo $delValue['assessment']['other']; 
					$totalO = $totalO + $delValue['assessment']['other'];
				?>
			</td>
            <td></td>                    
        	<?php
				}
        	} 
		} else {
			if($delValue['assessment']['assessement_status'] == "assessed"){
				if(isset($delValue['assessment']) && !empty($delValue['assessment'])){    	                    
					if( ($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED ){
			?>
				<td><?php
				 echo $delValue['assessment']['delivered']['total']; 
				 $totalD 	= $totalD  + $delValue['assessment']['delivered']['total'];
				 
				 ?></td>
				<td><?php echo $delValue['assessment']['delivered']['comment']; 
				?></td>
                
                <?php 
					}
					if( ($contract['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY ){				
				?>
				<td>
				<?php
				echo $delValue['assessment']['quality']['total']; 
				$totalQ   = $totalQ + $delValue['assessment']['quality']['total']; 
				?></td>
				<td><?php echo $delValue['assessment']['quality']['comment']; ?></td>
                <?php 
				}
				if( ($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER ){				
				?>
                
				<td><?php
				 echo $delValue['assessment']['other']['total']; 
				 $totalO = $totalO + $delValue['assessment']['other']['total'];
				 ?></td>
				<td><?php echo $delValue['assessment']['other']['comment']; ?></td>                    
			<?php
				}
        	} 
		} else { 
			?>
				<td colspan="6">
					<input type="button" name="assess_<?php echo $delValue['ref']; ?>" id="assess" value="Assess" style="color:#FF0000; font-weight:bold;" 
                    <?php 
						if(!isset($assessedStatus[$delValue['statusid']])){
					?> 
                    disabled="disabled"
                    <?php
					 }
					?>
                    />
				</td> 
			 <?php		
			}
	   }
        ?>         
    </tr>	
<?php
    if(isset($delValue['subdeliverable']) && !empty($delValue['subdeliverable'])){
		foreach($delValue['subdeliverable'] as $sub => $subDeliverable){
?>
    <tr style="background-color:#CCCCCC; text-align:right;">
        <td style="text-align:right;"><?php echo $subDeliverable['ref']; ?></td>
        <td style="text-align:right;"><?php echo $subDeliverable['deliverable']; ?></td>
        <td style="text-align:right;"><?php echo $subDeliverable['owner']; ?></td>
        <td style="text-align:right;"><?php echo $subDeliverable['deadline']; ?></td>
        <td style="text-align:right;"><?php echo $subDeliverable['status']; ?></td>          
        <?php
		if($subDeliverable['assessmentData']['assessement_status'] == "assessed"){
			 if(isset($subDeliverable['assessmentData']) && !empty($subDeliverable['assessmentData'])){       
					if( ($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED ){			                  
			?>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['delivered']['total']; ?></td>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['delivered']['comment']; ?></td>
                <?php 
				}
					if( ($contract['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY ){				
				?>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['quality']['total']; ?></td>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['quality']['comment']; ?></td>
                <?php
					}
					if( ($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER ){		
				?>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['other']['total']; ?></td>
				<td style="text-align:right;"><?php echo $subDeliverable['assessmentData']['other']['comment']; ?></td>                    
			<?php
				}
			} 
		}  else { 
		?>
         	<td colspan="6" style="text-align:center;">
            	<input type="button" name="assess_<?php echo $subDeliverable['ref']; ?>" id="assess"   style="color:#FF0000; font-weight:bold;"
                
				<?php 
               	 if(!isset($assessedStatus[$subDeliverable['statusid']])){
                ?> 
                value="Assessement Due"
                disabled="disabled"
                <?php
              	  } else {
                ?>
                	value = "Assess"
                <?php
				}
				?>
                />
            </td>   
         <?php		
		} 
        ?>                                       
    </tr>	
 <?php
    		}//end foreach
		}
    }
 }
?>
<tr>
    <td colspan="5"></td>
    <?php 
		if( ($contract['contractdefaults'] & Contract::DELIVERED) == Contract::DELIVERED ){	
	?>
    <td><b><?php echo $totalD; ?></b></td>
    <td></td>
    <?php 
		}
		if( ($contract['contractdefaults'] & Contract::QUALITY) == Contract::QUALITY ){		
	?>
    <td><b><?php echo $totalQ; ?></b></td>
    <td></td>
	<?php
		}
		if( ($contract['contractdefaults'] & Contract::OTHER) == Contract::OTHER ){		
	?>    
    <td><b><?php echo $totalO; ?></b></td>
    <td></td>     
    <?php
	}
	?>
</tr>
<tr>
	<td colspan="11" align="right" class="noborder">
    	<input type="hidden" id="contractid" name="contractid" value="<?php echo $_REQUEST['id']; ?>" />
    	<input type="hidden" id="contractDefaults" name="contractDefaults" value="<?php echo $contract['contractdefaults']; ?>" />        
        <input type="button" name="saveAssessment" id="saveAssessment" value="Save Assessment" />
    </td>
</tr>
</table>	
<div><?php displayGoBack("", ""); ?></div>