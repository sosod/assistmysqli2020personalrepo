<?php
$scripts = array("action.js", "ajaxfileupload.js");
require_once("../header.php");
$act	 	  = new Action();
$action	 	  = $act -> getAction( $_GET['id'] );
$actU 	 	  = new UserAccess();
$users		  = $actU -> getUsers();
$actStat 	  = new ActionStatus();
$actionStatus = $actStat->getActionStatuses();
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="add-action" name="add-action" method="post">
<table width="100%" class="noborder">
	<tr>
    	<td align="left" valign="top" class="noborder" width="50%">
            <table width="100%" height="100%">
              	<tr>
              		<td colspan="2"><h4>Action Details</h4></td>
              	</tr>
                <tr>
                    <th>Ref:</th>
                    <td>#<?php echo $_GET['id']; ?></td>                    
                </tr>
                <tr>
                <th>Action:</th>
                <td>
               <?php echo $action['action'];?>
                </td>                    
                </tr>
                <tr>
                    <th>Action Owner:</th>
                    <td><?php echo $action['owner']; ?></td>                    
                </tr>                        
                <tr>
                    <th>Measurable:</th>
                    <td><?php echo $action['measurable'];?></td>                    
                </tr>                 
                <tr>
                    <th>Progress:</th>
                    <td><?php echo $action['progress'];?></td>                    
                </tr>                              
                <tr>
                    <th>Status:</th>
                    <td><?php echo $action['status'];?></td>                    
                </tr>                                                              
                <tr>
                    <th>Deadline:</th>
                    <td><?php echo $action['deadline']; ?></td>                    
                </tr>                                                                                               
                <tr>
                 <th>Remind On:</th>
                 <td><?php echo $action['remind_on'];?>
                </td>
                </tr>
                <tr>
                    <td class="noborder"><?php displayGoBack("", ""); ?></td>
                    <td class="noborder"></td>
                </tr>                                                                                               
            </table>
        </td>
    	<td align="right" valign="top" class="noborder">
        	<table width="100%">
              	<tr>
              		<td colspan="2"><h4>Action Authorization</h4></td>
              	</tr>
            	<tr>
                	<th>Description:</th>
                	<td>
                    	<textarea name="response" id="response"></textarea>
                    </td>                    
                </tr>        
            	<tr>
                	<th>Approval:</th>
                	<td>
                        <select name="approval" id="approval">
                            <option value="">----</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>                                   
                        </select>
                    </td>                    
                </tr>                                                             
            	<tr>
                	<th>Attachment:</th>
                	<td>
                    <form method="post" enctype="multipart/form-data">
                        <span id="uploading"></span>
                        <input type="file"  name="attachment_<?php echo time(); ?>" id="attachment"  class="attach"/> 
                       <!--  <input type="button" name="addanother" id="addanother" value="Add Another" />  -->
                        <input type="hidden" name="fireUpload" value="true"  />    
                    </form>
                    </td>                    
                </tr>                                                                                                                                                
            	<tr>
                	<th class="noborder"></th>
                	<td class="noborder">
                    	<input type="hidden" name="actionid" id="actionid" value="<?php echo $_GET['id']; ?>"  />
						<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['contractid']; ?>"  />                        
                    	<input type="submit" name="authorize_action" id="authorize_action" value="Update" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="cancel" id="cancel" value="Cancel" />                        
                    </td>                    
                </tr>                                                                                
            </table>
        </td>        
    </tr>
</table>
</form>
</div>
<div id="display_authLogs"></div>