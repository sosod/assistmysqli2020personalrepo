<?php
$scripts = array("action.js", "ajaxfileupload.js");
require_once("../header.php");
$act	 	  = new Action();
$action	 	  = $act -> getAction( $_GET['id'] );
$actU 	 	  = new UserAccess();
$users		  = $actU -> getUsers();
$actStat 	  = new ActionStatus();
$actionStatus = $actStat->getActionStatuses();
$naming   = new Naming(); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="add-action" name="add-action" method="post" enctype="multipart/form-data">
<table width="100%" class="noborder">
	<tr>
    	<td align="left" valign="top" class="noborder" width="50%">
    		<h4>Action Details</h4>
            <table width="100%" height="100%">
                <tr>
                    <th>Ref #:</th>
                    <td><?php echo $_GET['id']; ?></td>                    
                </tr>
                <tr>
                <th><?php $naming->setName('action'); ?>:</th>
                <td>
               		<?php echo $action['action'];?>
                </td>                    
                </tr>
                <tr>
                    <th><?php $naming->setName('action_owner'); ?>:</th>
                    <td><?php echo $action['owner']; ?></td>                    
                </tr>                        
                <tr>
                    <th><?php $naming->setName('measurable'); ?>:</th>
                    <td><?php echo $action['measurable'];?></td>                    
                </tr>                 
                <tr>
                    <th><?php $naming->setName('progress'); ?>:</th>
                    <td><?php echo $action['progress'];?></td>                    
                </tr>                              
                <tr>
                    <th><?php $naming->setName('action_status'); ?>:</th>
                    <td><?php echo $action['status'];?></td>                    
                </tr>                                                              
                <tr>
                    <th><?php $naming->setName('deadline'); ?>:</th>
                    <td><?php echo $action['deadline']; ?></td>                    
                </tr>                                                                                               
                <tr>
                 <th><?php $naming->setName('remind_on'); ?>:</th>
                 <td><?php echo $action['remind_on'];?>
                </td>
                </tr>
                <tr>
                    <td class="noborder"><?php displayGoBack("", ""); ?></td>
                    <td class="noborder"><?php displayAuditLogLink("action_update", false) ?></td>
                </tr>                                                                                               
            </table>
        </td>
    	<td align="right" valign="top" class="noborder">
    		<h4>Update Action</h4>
        	<table width="100%">
            	<tr>
                	<th>Description:</th>
                	<td>
                    	<textarea cols="35" rows="7" name="response" id="response"></textarea>
                    </td>                    
                </tr>        
            	<tr>
                	<th>Status:</th>
                	<td>
                    	<select name="status" id="status">
                        	<option>--status--</option>
                            <?php
							foreach($actionStatus as $key => $stastus){
							?>
                            	<option value="<?php echo $stastus['id']; ?>"
                                <?php
									if($stastus['id'] == $action['action_status']){
								 ?>
                                 selected="selected"
                                 <?php 
								 } else if( $stastus['id'] == 1) {
								 ?>
                                 selected="selected"
                                 <?php
								 }
								 if( $stastus['id'] == 1) {
								 ?>
                                  disabled="disabled"
                                  <?php 
								  }
								  ?>
                                >
                                	<?php echo $stastus['name']; ?>
                                </option>
                            <?php
							}
							?>
                        </select>
                    </td>                    
                </tr>        
            	<tr>
                	<th>Progress:</th>
                	<td>
                    	<input type="text" name="progress" id="progress" value="<?php echo $action['progress']; ?>" />
                        <em style="color:#FF0000;">%</em>
                    </td>                    
                </tr>        
            	<tr>
                	<th>Remind On:</th>
                	<td>
                    	<input type="text" name="remind_on" id="remind_on" value="<?php echo $action['remind_on']; ?>" class="datepicker" />
                    </td>                    
                </tr>                                                
            	<tr>
                	<th>Attachment:</th>
                	<td>
                		<div>
	                        <span id="uploading"></span>
	                        <input type="file"  name="attachment_<?php echo time(); ?>" id="attachment"  class="attach"/> 
	                       <!--  <input type="button" name="addanother" id="addanother" value="Add Another" />  -->
	                        <input type="hidden" name="fireUpload" value="true"  />    
                    	</div>
                    </td>                    
                </tr>                                                
            	<tr>
                	<th></th>
                	<td>
                    <input type="checkbox" name="request_approval" id="request_approval" disabled />
                    Request approval that the risk has action has been completed
                    </td>                    
                </tr>                                                                                                
            	<tr>
                	<th></th>
                	<td>
                    	<input type="hidden" name="actionid" id="actionid" value="<?php echo $_GET['id']; ?>"  />
						<input type="hidden" name="contractid" id="contractid" value="<?php echo (isset($_GET['contractid']) ? $_GET['contractid'] : "") ; ?>"  />                        
                    	<input type="submit" name="update" id="update" value="Update" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="cancel" id="cancel" value="Cancel" />                        
                    </td>                    
                </tr>                                                                                
            </table>
        </td>        
    </tr>
</table>
</form>
</div>
<div id="actionUpdateLogs"></div>