<?php
$scripts = array("jquery.ui.deliverable.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		$("#contractview").deliverable({contractId:$("#contractid").val(), urlLocation:"../new/", page:"view_contract"});

	})
</script>
<table id="table_contract" width="100%"></table>
<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['id']; ?>" />
<div id="contractview" style="clear:both;"></div>
<div><?php displayGoBack("", ""); ?></div>