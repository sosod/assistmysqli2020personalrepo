<?php
$scripts = array("action.js", "jquery.ui.action.js");
require_once("../header.php");
?>
<script language="javascript">
	$(function(){
		 $("#displayaction").action({delId:$("#deliverableid").val(), editAction:true, deleteAction:true, urlLocation:"../new/", contractId:$("#contractid").val() })
	});
</script>
<table class="noborder">
	<tr>
    	<td class="noborder">
        	<table id="display_deliverable">
        		<tr>
        			<td colspan="2"><h4>Deliverable Details</h4></td>
        		</tr>
        		<tr id="goback">
        			<td class="noborder">
			           <?php
						if($_GET['page'] == "editdeliverable" || $_GET['page'] == "adddeliverable" ){
						?>
			   				<?php displayGoBack("", ""); ?>          
			
			            <?php } else {?>
			            <?php displayGoBack("../new/confirmation.php?contractid=".$_GET['contractid']."&deliverableid=".$_GET['id']."", ""); ?>
			            
			            <!-- <a href="../new/confirmation.php?contractid=<?php echo $_GET['contractid']; ?>&deliverableid=<?php echo $_GET['id']; ?>" class="goback">Go Back</a> -->            
			            <?php } ?>        			
        			</td>
        			<td class="noborder"></td>
        		</tr>
        	
        	</table>
        </td>
    	<td valign="top" class="noborder"><div id="displayaction"></div></td>        
    </tr>
</table>
<form method="post">
	<input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>" />
	<input type="hidden" name="contractid" id="contractid" value="<?php echo $_GET['contractid']; ?>" />    
</form>