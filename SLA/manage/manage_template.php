<?php
$scripts = array("jquery.ui.contract.js");
require_once("../header.php");

?>
<div id="loading"></div>
<table width="80%" id="contractDetail" style="display:none;" class="noborder">
	<tr>
    	<td valign="top" class="noborder">
        	<table id="table_contract" width="100%"></table>
        </td>
    	<td valign="top" class="noborder">
        	<table id="contract_summary" width="100%"></table>        
        </td>        
    </tr>
</table>
<script language="javascript">
	$(function(){
		$("#contract").contract({editDeliverable : true, editAction:true, page:"displayless", goto:"categorized", contractId:<?php echo $_GET['contractid']; ?> , urlLocation : "../new/", template : true});
	})
</script>
<?php
	if(isset($_GET['contractid']) && !empty($_GET['contractid'])){
?>
	<script language="javascript">
        $(function(){
            //$("#contract").contract({editDeliverable : true, editAction:true, page:"displayless", goto:"categorized", contractId:<?php echo $_GET['contractid']; ?> });
        })
    </script>
<?php		
	}
?>
<div id="contract">
	<div id="contractview"></div>
</div>
<div><?php displayGoBack("", ""); ?></div>