<?php
$scripts = array("deliverable.js");
require_once("../header.php");
$obj 		  = new Deliverable();
$deliverable  = $obj->getDeliverable( $_GET['id'] );

$cont    	  = new Contract();
$contract 	  = $cont->getAContract( $deliverable['contract_id'] );

$mains  	  = $obj->getDeliverables( $deliverable['contract_id'], "main" ); 	 

if($contract['template'] != ""){
	$mainDeliverable  = $obj->getDeliverables( $contract['template'] , "mainSub" ); 
} else {
	$mainDeliverable  = $obj->getDeliverables( $_GET['contractid'], "mainSub" ); 	
}
$q 			   = new QualityWeight();
$qweight 		= $q -> getQualityWeights(); 
$dweight 		 = new DeliveredWieght();
$deliveredweight = $dweight -> getDeliveredWieghts();
$o 		 	  = new OtherWeight();
$oweight 	  = $o -> getOtherWeights( $_GET['id'] );

$us    	  	  = new UserAccess();
$users 		  = $us->getUsers($_GET['contractid']);
$delCat    	  = new DeliverableCategory();
$categories   = $delCat->getCategories($_GET['contractid']);

?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table>
    <tr>
        <th>Ref #:</th>
        <td><?php echo $_GET['id']; ?></td>                        
    </tr>
    <tr>
        <th>Deliverable:</th>
        <td>
	        <textarea placeholder="enter delivereable" name="name" id="name" rows="7" cols="35"><?php echo $deliverable['deliverable'] ?></textarea>
        </td>                        
    </tr>
    <tr>
        <th>Deliverable Description:</th>
        <td><textarea rows="7" cols="35" name="description" id="description" placeholder="enter description"  ><?php echo $deliverable['deliverable_description'] ?></textarea></td>                        
    </tr>
    <tr>
        <th>Deliverable Category:</th>
        <td>
        <select name="category" id="category" <?php echo ($contract['template'] != "" ? "disbaled" : ""); ?> >
            <option value="">--category--</option>
            <?php
			foreach($categories as $key => $category) {
			?>
            <option value="<?php echo $category['id']; ?>" <?php if($deliverable['deliverable_category'] == $category['id']){?> selected<?php } ?>>
            <?php echo $category['name']; ?>
            </option>           
            <?php
			}
			?>                  
        </select>
        </td>                        
    </tr>    
   <?php
		if((Deliverable::MAIN_WITH_SUB & $deliverable['delstatus']) != Deliverable::MAIN_WITH_SUB ){	
			//if($deliverable['maindeliverableid'] > 0){
			echo "";
	?>    
    <tr>
        <th>Deliverable Type:</th>
        <td>
            <select name="type" id="type" >
                <option value="">--type--</option>
                <option value="0" <?php if($deliverable['type'] == 0){?> selected <?php } ?>>Main</option>                                
                <option value="1" <?php if($deliverable['type'] == 1){?> selected<?php } ?>>Sub</option>                                
            </select>
        </td>                        
    </tr>
    <tr>
        <th>Main Deliverable:</th>
        <td>
            <select name="main_deliverable" id="main_deliverable" >
                <option value="">--select main deliverable--</option>
                <?php
				foreach($mains['deliverable'] as $key => $del){
				?>
                <option value="<?php echo $del['ref']; ?>" <?php if($del['ref'] == $deliverable['maindeliverableid']) {?> selected="selected" <?php } ?>>
           			<?php echo $del['deliverable']; ?>
                </option>
                <?php
				}
				?>
            </select>
        </td>                        
    </tr>
    <?php
			//}
		}
	?>
    <tr>
        <th>Deliverable Owner:</th>
        <td>
            <select name="owner" id="owner">
                <option value="">--owner--</option>
				<?php 
                foreach($users as $id => $user){
                ?>
                <option value="<?php echo $user['tkid']; ?>" <?php if($user['tkid'] == $deliverable['owner']) {?> selected <?php }?> ><?php echo $user['tkname']." ".$user['tksurname']; ?></option>
             <?php
                }
            ?>                
            </select>
        </td>                        
    </tr>
	<?php 
    if((Contract::DELIVERED &  $contract['contractdefaults']) == Contract::DELIVERED ){
		if((Deliverable::MAIN_WITH_SUB & $deliverable['delstatus']) !== Deliverable::MAIN_WITH_SUB ){
    ?>
    <tr>
    <th>Delivered Weight:</th>
    <td>
    <select name="weight" id="weight" <?php echo ($contract['template'] != "" ? "disbaled" : ""); ?>>
   	 <option value="">--weight--</option>
    	<?php 
    	foreach($deliveredweight as $id => $weight){
    	?>
    	<option value="<?php echo $weight['id']; ?>" <?php if($weight['id'] == $deliverable['delivered_weight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
   	 <?php
		}
    ?>
    </select>                          
    </td>                        
    </tr>
			<?php
                }
			}
            if((Contract::QUALITY &  $contract['contractdefaults']) == Contract::QUALITY ){	
				if((Deliverable::MAIN_WITH_SUB & $deliverable['delstatus']) !== Deliverable::MAIN_WITH_SUB ){								
             ?>
            <tr>
                <th>Quality Weight:</th> 
                <td>
                <select name="quality_weight" id="quality_weight" <?php echo ($contract['template'] != "" ? "disbaled" : ""); ?>>
                    <option value="">--quality weight--</option>
                <?php 
                 foreach($qweight as $key => $weight){
                ?>
                <option value="<?php echo $weight['id']; ?>" <?php if($weight['id'] == $deliverable['quality_weight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
                <?php
                    }
                ?>                
                </select>                            	
                </td>                            
           </tr> 
           <?php 
            	}
			}
            if((Contract::OTHER &  $contract['contractdefaults']) == Contract::OTHER ){	
				if((Deliverable::MAIN_WITH_SUB & $deliverable['delstatus']) !== Deliverable::MAIN_WITH_SUB ){							
           ?>     
                   <tr>                                                                      
                    	<th>Other Weight:</th>
                    	<td>
                    <select name="other_weight" id="other_weight" <?php echo ($contract['template'] != "" ? "disbaled" : ""); ?>>
                        <option value="">--other weight--</option>
                    <?php 
                     foreach($oweight as $key => $weight){
                    ?>
                    <option value="<?php echo $weight['id']; ?>" <?php if($weight['id'] == $deliverable['other_weight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
                    <?php
                        }
                    ?>                
                    </select>                              	
                        </td>                          
                   </tr>
                   <?php 
				    }
				}
				   ?>
   <!-- <tr>
        <th>Delivered Weight :</th>
        <td>
        <select name="weight" id="weight">
            <option value="">--weight--</option>
            <?php 
             foreach($deliveredweight as $id => $weight){
            ?>
            <option <?php if($weight['id'] == $deliverable['dweight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
            <?php
                }
            ?>
        </select>                            
        </td>                        
    </tr>
    <tr>
        <th>Quality Weight :</th> 
        <td>
        <select name="quality_weight" id="quality_weight">
            <option value="">--quality weight--</option>
        <?php 
         foreach($qweight as $key => $weight){
        ?>
        <option <?php if($weight['id'] == $deliverable['qweight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
        <?php
            }
        ?>                
        </select>                              	
        </td>                            
   </tr>      
   <tr>                                                                      
        <th>Other Weight :</th>
        <td>
        <select name="other_weight" id="other_weight">
            <option value="">--other weight--</option>
        <?php 
         foreach($oweight as $index => $weight){
        ?>
        <option <?php if($weight['id'] == $deliverable['oweight']) {?> selected <?php }?> ><?php echo $weight['description']; ?></option>
        <?php
            }
        ?>                   
        </select>                              	
        </td>                          
   </tr> -->
   <tr>
        <th>Deadline:</th>
        <td><input type="text" name="deadline" id="deadline" placeholder="enter deadline"  value="<?php echo $deliverable['deadline'] ?>" class="datepicker" size="50" /></td>
    </tr>                      
    <tr>
        <th>Remind On:</th>
        <td><input type="text" name="remind_on" id="remind_on" placeholder="enter remind on" value="<?php echo $deliverable['remind_on'] ?>" class="datepicker" size="50" /></td>                     
    <tr>      
        <th></th>              	                                        
        <td>
            <input type="submit" name="edit" id="edit" value="Edit" class="editdeliverable"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']?>" />
            <input type="hidden" name="contid" id="contid" value="<?php echo $deliverable['contract_id']; ?>" />  
            <input type="hidden" name="is_template" id="is_template" value="<?php echo ($contract['template'] != "" ? $contract['template'] : ""); ?>" />         
        </td>
        <tr>
        	<td class="noborder">	
            <?php
			if($_GET['page'] == "editdeliverable" || $_GET['page'] == "adddeliverable" ){
			?>
   				<?php displayGoBack("", ""); ?>           

            <?php } else {?>
            <?php displayGoBack("../new/confirmation.php?contractid=".$deliverable['contract_id']."&deliverableid=".$_GET['id']."", ""); ?>      
            <?php } ?>
		</td>
            <td class="noborder"><?php displayAuditLogLink("deliverable_edit", false) ?></td>
        </tr>                   
</table>
</div>
<div id="editLog"></div>