<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEditProgress.php");
$step = "6-1";
$page = array("next"=>"6-2","step"=>"1","title"=>$process[6][1]['txt']);
$dbowner = $variables['ignitetn'];
$r0 = $variables['r0'];
$r1 = $variables['r1'];
$ref = $variables['ref'];

if(strlen($r0)>0 && strlen($r1)>0)
{
    $result[0] = $r0;
    $result[1] = $r1;
    $result[2] = $ref;
}



function echoFieldFinal($ref,$m,$fields) {
    $mx = 0;
        for($m2=1;$m2<=$m;$m2++) {
            foreach($fields as $fld) {
                $mx++;
                $mx++;
                $fn = $fld['fldfn'];
                $ft = $fld['fldtxt'];
                if($fn == "Y") { $style = "text-decoration: underline"; } else { $style = ""; }
                echo("    <td class=\"level3 field\" style=\" $style \" colspan=2> $ft ");
                echo("</td>");
            }
            echo("</tr><tr>");
            $style = "";
            foreach($fields as $fld) {
                $fn = $fld['fldfn'];
                $ft = $fld['fldtarget'];
                echo("    <td align=center>Function: ".YesNo($fn)."</td>");
                echo("    <td align=center>Target: ".YesNo($ft)."</td>");
            }
        }
    return $mx;
}


?>
<script type=text/javascript src="lib/AdminCreateVariables.js"></script>
<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php //displayResult($result); ?>
<?php
echo("<h2>Step 5.".$page['step'].": ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);

?>
<table cellpadding=3 cellspacing=0 width=550>
    <tr>
        <td class="tdheaderl b-bottom-w" width=150>Reference:</td>
        <td width=400><?php echo($ref); ?></td>
    </tr>
    <tr>
        <td class="tdheaderl b-bottom-w">Dashboard Name:</td>
        <td><?php echo($dash['dashname']); ?></td>
    </tr>
    <tr>
        <td class="tdheaderl b-bottom-w">Dashboard Owner:</td>
        <td><?php echo($variables['ignitetn']); ?></td>
    </tr>
    <tr>
        <td class="tdheaderl b-bottom-w">Capture Frequency:</td>
        <td><?php
            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE id = ".$dash['dashfrequency'];
            include("inc_db_con.php");
                $freq = mysql_fetch_array($rs);
                echo($freq['value']);
            mysql_close($con); ?></td>
    </tr>
    <tr>
        <td class="tdheaderl b-bottom-w">Link to SDBIP?:</td>
        <td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
    </tr>
</table>

<?php

echo("<form name=frm id=frm action=\"admin_edit_process.php\" method=post>");
echo("<input type=hidden name=step value=\"6-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=5-2>");
echo("<input type=hidden name=act value=move><input type=hidden name=acttype value=\"\" id=actt>");
echo("<div align=center>");
    $values = array();
    $i = 1;
    $frame = array();
    $sections = array(
        "dir"=>$dash['dashdir'],
        "sub"=>$dash['dashsub'],
        "towns"=>$dash['dashtowns'],
        "wards"=>$dash['dashwards']
    );
    $dashstatus = $dash['dashstatus'];
        if($dashstatus == "Y")
        {
            $status = "Y";
        }
        else
        {
            $stat = strFn("explode",$dashstatus,"_","");
            if(count($stat)>1)
            {
                $stat = strFn("explode",$stat[1],"-","");
                $status = "P";
            }
            else
            {
                $stat[0] = 1;
                $stat[1] = 1;
                $status = "P";
            }

        }
        $records = getRecords($ref);
        $fields = getFields($ref);
        $monthcols = count($fields)*2;
        $vars = getVariables($ref);
        //print_r($vars);
        $types = getTypes();
        $functions = getFunctions();
        $preview = 0;
        $subpreview = 0;
        $dirpreview = 0;
        if($sections['towns']<0) { unset($sections['towns']); }
        if($sections['wards']<0) { unset($sections['wards']); }
        $l = 0;
        $m = 2;
        for($m2=1;$m2<=$m;$m2++)
        {
            $l = $l + ($monthcols * $m);
        }
        $m--;
        $frame['dir'] = getSections("D",$ref);
        $frame['sub'] = getSections("S",$ref);
        $frame['towns'] = getSections("T",$ref);
        $frame['wards'] = getSections("W",$ref);
        echo("<table cellpadding=3 cellspacing=0>");
        echo("<tr>");
        echo("    <td rowspan=3>&nbsp;</td>");
        echo("    <td rowspan=3 class=\"tdheader \">Primary<br>Admin</td>");
        echo("    <td class=\"tdheader \" rowspan=3>Secondary<Br>Admin</td>");
        echo("    <td class=\"tdheader \" rowspan=3>Function?</td>");
        echo("    <td class=\"tdheader \" colspan=$monthcols >Time Period</td>");
        echo("</tr>");
        echo("<tr>");
            $mx = echoFieldFinal($ref,$m,$fields);
        echo("</tr>");
        foreach($frame['dir'] as $dir)
        {
            $dirpreview++;
            $lev = 1;
            $dirid = $dir['id'];
            $dirtxt = $dir['txt'];
            $subs = $frame['sub'][$dirid];
            $dtk = getAdmins($ref,'D',$dirid);
            $dtkp = $dtk['Y']['tkname']." ".$dtk['Y']['tksurname'];
            if(strlen($dtkp)==1) { $dtkp = $dbowner; }
            $dtks = $dtk['N']['tkname']." ".$dtk['N']['tksurname'];
            if(strlen($dtks)==1) { $dtks = $dbowner; }
                echo("<tr>");
                    echo("<td class=\"level".$lev." \" >$dirtxt</td>");
                    echo("<td class=\"level".$lev." \"  style=\"padding-left: 0px;text-align:center\">".$dtkp."</td>");
                    echo("<td class=\"level".$lev." \"  style=\"padding-left: 0px;text-align:center\">".$dtks."</td>");
                    echo("<td class=\"level".$lev." \"  style=\"padding-left: 0px;text-align:center\">-</td>");
                    echo("<td class=\"level".$lev." \" colspan=".($l-4)." align=center>&nbsp;</td>");
                echo("</tr>");
                foreach($subs as $sub)
                {
                    $subpreview++;
                    $lev = 2;
                    $subid = $sub['id'];
                    $subtxt = $sub['txt'];
                    $stk = getAdmins($ref,'S',$subid);
                    $stkp = $stk['Y']['tkname']." ".$stk['Y']['tksurname'];
                    if(strlen($stkp)==1) { $stkp = $dbowner; }
                    $stks = $stk['N']['tkname']." ".$stk['N']['tksurname'];
                    if(strlen($stks)==1) { $stks = $dbowner; }
                    echo(chr(10)."<tr>");
                        echo("<td class=level".$lev." >$subtxt</td>");
                        echo("<td class=level".$lev." style=\"padding-left: 0px;text-align:center\">".$stkp."</td>");
                        echo("<td class=level".$lev." style=\"padding-left: 0px;text-align:center\">".$stks."</td>");
                        echo("<td class=level".$lev." style=\"padding-left: 0px;text-align:center\">-</td>");
                        echo("<td class=level".$lev." colspan=".($l-4)." > </td>");
                    echo("</tr>");
                    $lev = 3;
                    $extras = array(1=>"wards",2=>"towns");

                    foreach($extras as $ex)
                    {
                        if($ex == "wards") { $type = "W"; } else { $type = "T"; }
                        if($sections[$ex]>0)
                        {
                            $secs = $frame[$ex];
                            foreach($secs as $sec)
                            {
                                    $preview++;
                                $secid = $sec['id'];
                                $sectxt = $sec['txt'];
                                $secdfid = $sec['dfid'];
                                $etk = getAdmins($ref,$type,$secid);
                                $etkp = $etk['Y']['tkname']." ".$etk['Y']['tksurname'];
                                if(strlen($etkp)==1) { $etkp = $dbowner; }
                                $etks = $etk['N']['tkname']." ".$etk['N']['tksurname'];
                                if(strlen($etks)==1) { $etks = $dbowner; }
                                echo(chr(10)."<tr>");
                                    echo("<td class=level".$lev." >$sectxt</td>");
                                    echo("<td class=level".$lev."  style=\"padding-left: 3px;text-align:center\">".$etkp."</td>");
                                    echo("<td class=level".$lev."  style=\"padding-left: 3px;text-align:center\">".$etks."</td>");
                                    echo("<td class=level".$lev."  style=\"padding-left: 3px;text-align:center\">-</td>");
                                    echo("<td class=level".$lev." colspan=".($l-4)." > </td>");
                                echo("</tr>");
                                foreach($records as $rec)
                                {
                                    $recid = $rec['recid'];
                                    $fn = $rec['recfn'];
//                                    if($fn == "Y") { $class = "total"; $varclass = "vartotal"; } else { $class = "record"; $varclass = "varrecord"; }
                                        switch($fn) {
                                            case "H":
                                                $class = "heading"; $varclass = "varheading";
                                                $colspan=count($fields);
                                                break;
                                            case "Y":
                                                $colspan=1;
                                                $class = "total"; $varclass = "vartotal";
                                                break;
                                            default:
                                                $colspan=1;
                                                $class = "record"; $varclass = "varrecord";
                                                break;
                                        }
                                    echo(chr(10)."<tr>");
                                    echo("<td class=".$class.">".$rec['rectxt']."</td>");
                                    echo("<td class=".$class."> </td>");
                                    echo("<td class=".$class."> </td>");
                                    echo("<td class=".$class."  style=\"padding-left: 3px;text-align:center\">".YesNo($rec['recfn'])."</td>");
                                    foreach($fields as $fld)
                                    {
                                        $ffn = $fld['fldfn'];
                                        if($fn!="Y" && $fn!="H") {
                                            if($ffn=="Y") { $class = "total"; } else { $class = "record"; }
                                        }
                                        echo(chr(10)."<td class=$varclass colspan=2>");
                                    if($fn=="H")
                                    {
                                        echo("&nbsp;");
                                    }
                                    else
                                    {
                                        $fldid = $fld['fldid'];
                                        $var = $vars[$recid][$fldid];
                                        $kpi = array();
                                        $kpi = getKPI($ref,$dirid,$subid,$secdfid,$recid,$fldid);
                                        $varid = $var['varid'];
                                        $varfn = getVariablesFn($varid);
                                        $fnsrc = $var['varfnsrc'];
                                        if($fnsrc == "FLD") {
                                            $fnsrc = "Fields";
                                            $varfntxt = array();
                                            foreach($varfn as $vf)
                                            {
                                                if(checkIntRef($vf))
                                                {
                                                    $varfntxt[$vf] = getField($vf);
                                                    $varfntxt[$vf] = $varfntxt[$vf]['fldtxt'];
                                                }
                                            }
                                        } else {
                                            if($fnsrc=="REC") {
                                                $fnsrc = "Records";
                                                $varfntxt = array();
                                                foreach($varfn as $vf)
                                                {
                                                    if(checkIntRef($vf))
                                                    {
                                                        $varfntxt[$vf] = getRecord($vf);
                                                        $varfntxt[$vf] = $varfntxt[$vf]['rectxt'];
                                                    }
                                                }
                                            } else {
                                                $fnsrc = "";
                                            }
                                        }
                                        if(strlen($var['varfn'])>2 || $var['varfn']!="N")
                                        {
                                        echo("<table cellpadding=1 cellspacing=0 class=noborder width=100%>");
                                        }
                                          if(strlen($var['varfn'])>2) {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\" valign=top>Function: <i>".$functions[$var['varfn']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">$fnsrc: <br><i>");
                                                    $f = 1;
                                                    foreach($varfn as $vf)
                                                    {
                                                        if($f>1) { echo("<br>"); }
                                                        echo("$f. ".$varfntxt[$vf]);
                                                        $f++;
                                                    }
                                                echo("</i></td>");
                                            echo("</tr>");
                                          }
                                          if($var['varfn']!= "N") {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Type: <i>".$types[$var['vartype']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Units: <i>".$var['varunit']."</i></td>");
                                            echo("</tr>");
                                          } else {
                                            echo("N/A");
                                          }
                                          if(count($kpi)>5 && strlen($kpi['kpivalue'])>0)
                                          {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" colspan=2 style=\"padding-left: 0px;\">SDBIP KPI: <i>".$kpi['kpiid']." - ".$kpi['kpivalue']." (".$kpi['kpidefinition'].")</i></td>");
                                            echo("</tr>");
                                          }
                                        if(strlen($var['varfn'])>2 || $var['varfn']!="N")
                                        {
                                        echo("</table>");
                                        }
                                    }
//                                    echo("<P>KPI-".$ref."-".$dirid."-".$subid."-".$secid."-".$recid."-".$fldid);
//                                    print_r($kpi);
                                        echo("</td>");
                                    }
                                    echo("</tr>");
                                }
                            }
                        }
                    }
                    $lev = 2;
                    $subvalues = array();
                    echo(chr(10)."<tr>");
                    echo("<td class=level".$lev."total colspan=1 align=right>Sub-Total for<br>$subtxt:</td>");
                    echo("<td class=level".$lev."total colspan=3 align=right> </td>");
                    $class = "level".$lev."total";
                    $varclass = "level".$lev."total";
                    foreach($fields as $fld)
                    {
                        $fldid = $fld['fldid'];
                        echo(chr(10)."<td class=$varclass colspan=2>");
//                        if($subpreview>1)
//                        {
//                            echo("As above");
//                        }
//                        else
//                        {
                                    $fldid = $fld['fldid'];
//                                    $var = $vars['SUB'][$fldid];
                                    if($fld['fldfn']=="Y") {
										$var = $vars['SUBF'][$fldid];
									} else {
										$var = $vars['SUB'][$fldid];
									}

                                        $kpi = array();
                                        $kpi = getKPI($ref,$dirid,$subid,0,0,$fldid);
                                    $varid = $var['varid'];
                                    if(checkIntRef($varid))
                                    {
                                        $varfn = getVariablesFn($varid);
                                        $vf = $varfn[1];
                                        if(checkIntRef($vf))
                                        {
                                            $varfntxt[$vf] = getRecord($vf);
                                            $varfntxt[$vf] = $varfntxt[$vf]['rectxt'];
                                        }
                                        if($var['varfn']!= "N") {
                                        echo("<table cellpadding=1 cellspacing=0 class=noborder width=100%>");
                                        }
                                          if(strlen($var['varfn'])>2) {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\" valign=top>Function: <i>".$functions[$var['varfn']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Record: <i>".$varfntxt[$vf]."</i></td>");
                                            echo("</tr>");
                                          }
                                          if($var['varfn']!= "N") {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Type: <i>".$types[$var['vartype']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Units: <i>".$var['varunit']."</i></td>");
                                            echo("</tr>");
                                          } else {
                                            echo("N/A");
                                          }
                                          if(count($kpi)>5 && strlen($kpi['kpivalue'])>0)
                                          {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" colspan=2 style=\"padding-left: 0px;\">SDBIP KPI: <i>".$kpi['kpiid']." - ".$kpi['kpivalue']." (".$kpi['kpidefinition'].")</i></td>");
                                            echo("</tr>");
                                          }
                                        if($var['varfn']!= "N") {
                                        echo("</table>");
                                        }
                                    }
//                        }
                        echo("</td>");
                    }
                    echo("</tr>");
                }
            $lev = 1;
            echo(chr(10)."<tr>");
            echo("<td class=level".$lev."total colspan=1 align=right >Total for<br>$dirtxt:</td>");
            echo("<td class=level".$lev."total colspan=3 align=right> </td>");
            $varclass = "level".$lev."total";
            $class = "level".$lev."total";
            foreach($fields as $fld)
            {
                $fldid = $fld['fldid'];
                echo(chr(10)."<td class=$varclass colspan=2>");
//                if($dirpreview>1)
//                {
//                    echo("As above");
//                }
//                else
//                {
                                    $fldid = $fld['fldid'];
//                                    $var = $vars['DIR'][$fldid];
                                    if($fld['fldfn']=="Y") {
										$var = $vars['DIRF'][$fldid];
									} else {
										$var = $vars['DIR'][$fldid];
									}
                                    $varid = $var['varid'];
                                        $kpi = array();
                                        $kpi = getKPI($ref,$dirid,0,0,0,$fldid);
                                    if(checkIntRef($varid))
                                    {
                                        echo("<table cellpadding=1 cellspacing=0 class=noborder width=100%>");
                                          if(strlen($var['varfn'])>2) {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\" valign=top>Function: <i>".$functions[$var['varfn']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Record: <i>Sub-Total per Sub-Directorate</i></td>");
                                            echo("</tr>");
                                          }
                                          if($var['varfn']!= "N") {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Type: <i>".$types[$var['vartype']]['value']."</i></td>");
                                            echo("</tr><tr>");
                                                echo("<td class=\"noborder $class\" style=\"padding-left: 0px;\">Units: <i>".$var['varunit']."</i></td>");
                                            echo("</tr>");
                                          } else {
                                            echo("N/A");
                                          }
                                          if(count($kpi)>5 && strlen($kpi['kpivalue'])>0)
                                          {
                                            echo("<tr>");
                                                echo("<td class=\"noborder $class\" colspan=2 style=\"padding-left: 0px;\">SDBIP KPI: <i>".$kpi['kpiid']." - ".$kpi['kpivalue']." (".$kpi['kpidefinition'].")</i></td>");
                                            echo("</tr>");
                                          }
                                        echo("</table>");
                                    }
//                }
                echo("</td>");
            }
            echo("</tr>");
        }
        echo("</table>");
?>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center"><input type="button" value="Accept" onclick="moveOn('Y');"> <input type=button value="Pause" onclick="document.location.href = 'admin_edit_pending.php';"> <input type=button value="Reject" onclick="moveOn('N');"> <input type=button value=Delete onclick="moveOn('C');"> <input type=button value="Preview" onclick="previewDash(<?php echo($ref); ?>);"></td>
	</tr>
</table>
</form>
<?php

        $stepprogress = setProgress(6);
        $totalprogress = setProgress(0);
        displayProgress("Step 6 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
