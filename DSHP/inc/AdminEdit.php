<?php
$step = $variables['step'];

switch($step)
{
    case "1-6":
        saveStepPreview("E_1-5","1-6");
        break;
    case "2-4":
        $old = $variables['oldstep'];
        saveStepPreview($old,"2-4");
        break;
    case "3-4":
        $old = $variables['oldstep'];
        saveStepPreview($old,"3-4");
        break;
    case "4-2":
        $old = $variables['oldstep'];
        saveStepPreview($old,"4-2");
        break;
    case "5-2":
        $old = $variables['oldstep'];
        saveStepPreview($old,"5-2");
        break;
    case "6-1":
        $old = "E_".$variables['oldstep'];
        saveStepPreview($old,"6-1");
        break;
}




function saveStepPreview($old,$new) {
    global $cmpcode;
    global $dbref;
    global $variables;
    $ref = $variables['ref'];
    $acttype = $variables['acttype'];
    $act = $variables['act'];
    $statnew = "E_".$new;
    if(checkIntRef($ref))
    {
        $dash = getDashboard($ref);
    //CHANGE STATUS
    switch($acttype)
    {
        case "Y":
            $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
            $sql.= "VALUES ($ref,now(),'$old','$statnew')";
            include("inc_db_con.php");
            //update dashstatus
            $sql = "UPDATE ".$dbref."_dashboard SET dashstatus = '$statnew' WHERE dashid = $ref";
            include("inc_db_con.php");
                $res[0] = "check";
                $res[1] = "Step ".strFn("substr",strFn("str_replace",$statnew,"-","."),2,3)." completed.";
                $res[2] = $ref;
                switch($new)
                {
                    case "1-6":
                        $url = "admin_edit2-1";
                        break;
                    case "2-4":
                        $url = "admin_edit3-1";
                        break;
                    case "3-4":
                        $url = "admin_edit4-1";
                        break;
                    case "4-2":
                        if($dash['dashsdbip']=="Y") {
                            $url = "admin_edit5-1";
                        } else {
                            $url = "admin_edit6-1";
                        }
                        break;
                    case "5-2":
                        $url = "admin_edit6-1";
                        break;
                    case "6-1":
                        $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
                        $sql.= "VALUES ($ref,now(),'$statnew','Y')";
                        include("inc_db_con.php");
                        //update dashstatus
                        $sql = "UPDATE ".$dbref."_dashboard SET dashstatus = 'Y' WHERE dashid = $ref";
                        include("inc_db_con.php");
                        $dash = getDashboard($ref);
                        if($tkid == $dash['dashowner'])
                        {
                            $url = "admin_edit";
                        } else {
                            $url = "admin_dash_view";
                        }
                        break;
                    default:
                        $url = "admin_edit_pending";
                        break;
                }
            break;
        case "C":
            $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
            $sql.= "VALUES ($ref,now(),'$old','C')";
            include("inc_db_con.php");
            //update dashstatus
            $sql = "UPDATE ".$dbref."_dashboard SET dashstatus = 'C' WHERE dashid = $ref";
            include("inc_db_con.php");
                $res[0] = "check";
                $res[1] = "Dashboard $ref canceled.";
                $res[2] = $ref;
                $url = "admin";
            break;
        case "N":
            $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
            $sql.= "VALUES ($ref,now(),'$old','$statnew')";
            include("inc_db_con.php");
            $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
            $sql.= "VALUES ($ref,now(),'$statnew','E_1-6')";
            include("inc_db_con.php");
            //update dashstatus
            $sql = "UPDATE ".$dbref."_dashboard SET dashstatus = 'E_1-6' WHERE dashid = $ref";
            include("inc_db_con.php");
                $res[0] = "check";
                $res[1] = "Dashboard creation rejected.  Starting again.";
                $res[2] = $ref;
                $url = "admin_edit2-1";
            break;
        }
    }
    //REDIRECT
    switch($act)
    {
        case "move":
            $url = $url;
            break;
        case "pause":
            $url = "admin_edit_pending";
            break;
    }
    echo("<script type=text/javascript>");
    echo("document.location.href = '".$url.".php?r0=".$res[0]."&r1=".rawurlencode($res[1])."&ref=".$ref."';");
    echo("</script>");
}



?>
