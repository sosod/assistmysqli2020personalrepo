<?php
include("inc_head.php");
include("inc/Admin.php");
//include("inc/getLists.php");
//$variables = $_REQUEST;
//print_r($variables);
$colors = array("#009900","#000099","#fe9900","#cc0001","#555555");

function fnDisplayReport($recid,$fldid,$values,$var) {
    global $dbref;
    global $cmpcode;
    global $ref;
    if($var['varfn']=="N" || strlen($var['varfn'])<1)
    {
        $echo = "&nbsp;";
    }
    else
    {
        $v = 0;
        $fid = array();
        $fid = getVariablesFn($var['varid']);
        $fnsrc = $var['varfnsrc'];
        $vals = array();
        foreach($fid as $f)
        {
            if($fnsrc=="REC")
            {
                $vals[] = $values[$f][$fldid];
            }
            else
            {
                $vals[] = $values[$recid][$f];
            }
        }
        $val = doFn($var['varfn'],$vals);
//        $echo = $val;
        switch($var['vartype'])
        {
            case "DT":
                $val2 = $val / 86400;
                if($val2 != floor($val2)) {
                    $echo = number_format($val2,2);
                } else {
                    $echo = $val2;
                }
                $echo.= " day(s)";
                break;
            case "NUM":
                if($var['varfn']!= "COUNT")
                    $echo = number_format($val,2)." ".$var['varunit'];
                else
                    $echo = $val." ".$var['varunit'];
                break;
            case "PERC":
                $echo = number_format($val,4)."%";
                break;
            case "R":
                $echo = "R ".number_format($val,2);
                break;
            default:
                $echo = $val;
                break;
        }
    }
    $ret = array($echo,$val);
    return $ret;
}

function drawTable($from,$to,$act,$view) {
    global $cmpcode;
    global $dbref;
    global $ref;
    global $dash;
    global $records;
    global $fields;
    global $vars;
    global $time;
    global $data;
	global $fromdate;
	global $todate;
    global $tplevel;
	global $summ;
	global $frame;
    $ndata = array();
    $result = "";

	if($dash['dashfrequency']==1) {
			$result.="<h3>";
            if(date("dmY",$fromdate) == date("dmY",$time[$from]['sval'])) {
                $result.= date("d F Y",$fromdate);
            } else {
                if(date("mY",$fromdate) == date("mY",$time[$from]['sval'])) {
                    $result.= date("d F Y",$fromdate);
                } else {
                    $result.= date("d F Y",$time[$from]['sval']);
                }
            }
            $result.=" - ";
            if(date("dmY",$todate) == date("dmY",$time[$to]['eval'])) {
                $result.= date("d F Y",$todate);
            } else {
                if(date("mY",$todate) == date("mY",$time[$to]['eval'])) {
                    $result.= date("d F Y",$todate);
                } else {
                    $result.= date("d F Y",$time[$to]['eval']);
                }
            }
            $result.="</h3>";
	} else {
		if($from==$to) {
			$result.="<h3>".date("F Y",$time[$from]['eval'])."</h3>";
		} else {
			$result.="<h3>".date("F Y",$time[$from]['sval'])." - ".date("F Y",$time[$to]['eval'])."</h3>";
		}
	}

$result.= "<table cellpadding=5 cellspacing=0>";
$result.= "    <tr><td></td>";
        foreach($fields as $fld)
        {
            $ffn = $fld['fldfn'];
            if($ffn=="Y") { $fldstyle = "text-decoration: underline;"; } else { $fldstyle = ""; }
            $result.="<td class=tdheader style=\"$fldstyle\">".$fld['fldtxt']."</td>";
        }
$result.= "    </tr>";
    foreach($frame as $frm)
    {
		if($frm['id']>0) {
			$fc = count($fields)+1;
			$result.= "    <tr>";
            $result.="<td colspan=$fc class=\"level3\">".$frm['txt']."</td>";
			$result.= "    </tr>";
		}
    foreach($records as $rec)
    {
        $r = $rec['recid'];
        $result.= "<tr>";
        $fn = $rec['recfn'];
        if($fn=="Y" || $fn == "H") { $recstyle = "text-decoration: underline;"; } else { $recstyle = ""; }
        if($fn == "H") { $recclass = "level2"; $recstyle.= " padding-left:5px; "; } else { $recclass = "tdheaderl"; }
        $result.= "<td class=\"$recclass\" style=\"$recstyle\">".$rec['rectxt']."</td>";
        foreach($fields as $fld)
        {
            $f = $fld['fldid'];
            $ffn = $fld['fldfn'];
            $v = $vars[$r][$f];
            if($fn == "H") {
                $tdstyle = "style=\"background-color: #ddffdd;\"";
            } else {
                if($fn == "Y" || $ffn=="Y") {
                    $tdstyle = "class=\"vartotal\"";
                } else {
                    $tdstyle = "class=\"varrecord\"";
                }
            }
            if($v['vartype']=="NUM" || $v['vartype']=="PERC" || $v['vartype']=="R") {
                $tdstyle.= " style=\"text-align: right;\"";
            }
            $result.= "<td $tdstyle >";
            if($v['vartype']=="BLK" || $fn == "H") {
                $echo = "<span style=\"text-decoration: none;\">&nbsp;</span>";
            } else {
            if(strlen($v['varfn'])==0)
            {
                $sql = "SELECT * FROM ".$dbref."_dashboard_data WHERE datadashid = $ref AND datarecid = $r AND datafldid = $f AND datastatus = 'Y'";
                if($dash['dashfrequency']>3)
                {
                    $sql.= " AND datatimeid >= $from AND datatimeid <= $to ";
                }
                else
                {
                    $sql.= " AND datatimeid >= $from AND datatimeid <= $to ";
                    $sql.= " AND CONCAT(datatimeyear,IF(datatimemonth<10,'0',''),datatimemonth,IF(datatimeday<10,'0',''),datatimeday) >= ".date("Y",$fromdate).(date("m",$fromdate)).(date("d",$fromdate));
                }
				if($frm['id']>0) {
					switch($summ[0]) 
					{
						case "D":
							$sql.= " AND datadirid = ".$frm['id'];
							break;
						case "S":
							$sql.= " AND datasubid = ".$frm['id'];
							break;
						case "T":
							$sql.= " AND dataframeid = ".$frm['id'];
							break;
						case "W":
							$sql.= " AND dataframeid = ".$frm['id'];
							break;
					}
				}
                $sql.= " ORDER BY datatimeid, datadirid, datasubid, dataframeid";
                include("inc_db_con.php");
                $c = 0;
                while($row = mysql_fetch_array($rs))
                {
$nd = "";
                    if(strlen($row['datavalue'])>0) {
                        if($v['vartype']=='NUM' || $v['vartype']=='R' || $v['vartype']=='PERC')
                        {
//                            $nd = $ndata[$r][$f] + $row['datavalue'];
							if($summ[1] > 0 || $summ[0] == "ALL") {
								$nd = $ndata[$r][$f] + $row['datavalue'];
							} else {
								$nd = $row['datavalue'] + $ndata[$frm['id']][$r][$f];
							}
                        }
                        else
                        {
                            switch($v['vartype'])
                            {
                                case "DT":
                                    $nd=$ndata[$r][$f]."<li>".date("d M Y",$row['datavalue'])."</li>";
                                    break;
                                case "LT":
                                    $lti = $row['datavalue'];
                                    if(checkIntRef($lti)) {
                                        $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$lti;
                                        include("inc_db_con3.php");
                                            $row3 = mysql_fetch_array($rs3);
                                            $nd=$ndata[$r][$f]."<li>".$row3['value']."</li>";
                                        mysql_close($con3);
                                    }
                                    break;
                                default:
                                    $nd=$ndata[$r][$f]."<li>".$row['datavalue']."</li>";
                                    break;
                            }
                        }
							if($summ[1] > 0 || $summ[0]=="ALL") {
								$ndata[$r][$f] = $nd;
							} else {
								$ndata[$frm['id']][$r][$f] = $nd;
							}
                        $c++;
                    }
                }
//                $val = $ndata[$r][$f];
							if($summ[1] > 0 || $summ[0]=="ALL") {
								$val = $ndata[$r][$f];
							} else {
								$val = $ndata[$frm['id']][$r][$f];
							}
                $echo = "";
                switch($v['vartype'])
                {
                    case "NUM":
                        $echo = number_format($val,2)." ".$v['varunit'];
                        break;
                    case "R":
                        $echo = "R ".number_format($val,2);
                        break;
                    case "PERC":
                        $echo = number_format($val,2)." %";
                        break;
                    default:
                        $echo = "<ul>".$val."</ul>";
                        break;
                }
            }
            else
            {
				if($summ[1] > 0 || $summ[0]=="ALL") {
					$ret = fnDisplayReport($r,$f,$ndata,$v);
					$ndata[$r][$f] = $ret[1];
				} else {
					$ret = fnDisplayReport($r,$f,$ndata[$frm['id']],$v);
					$ndata[$frm['id']][$r][$f] = $ret[1];
				}
                $echo = $ret[0];
//				echo("<P>.".$frm['id']."-".$r.".".$f);
            }
            }   //vartype = blk
            $result.= $echo."</td>";
        }
        $result.= "</tr>";
    }	//records
	}	//frame
$result.= "</table>";
//echo("<P>."); print_r($ndata);
$return = array("d"=>$ndata,"r"=>$result);
    return $return;
}

function drawComment($from,$to) {
    global $cmpcode;
    global $dbref;
    global $ref;
	global $summ;

//	if($summ[1]>0) {
//		$comments = getComments($ref,$from,$summ[1]);
//	} else {
		$comments = getAllComments($ref,$from);
//	}
    if(count($comments)>0)
    {
        $table = "<h4>Update Comments</h4>";
        $table.= "<table cellpadding=5 cellspacing=0 width=600>";
        foreach($comments as $comm)
        {
            $table.= "<tr>";
            $table.= "<td>";
            $dt = strfn("explode",$comm['dcdate']," ","");
            $dt2 = strfn("explode",$dt[0],"-","");
            $dt =  strfn("explode",$dt[1],":","");
            $dt3 = mktime($dt[0],$dt[1],$dt[2],$dt2[1],$dt2[2],$dt2[0]);
            $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$comm['dctkid']."'";
            include("inc_db_con.php");
                $tk = mysql_fetch_array($rs);
            mysql_close($con);
            $sql = "SELECT * FROM ".$dbref."_dashboard_frame WHERE dfid = ".$comm['dcdfid'];
            include("inc_db_con.php");
                $df = mysql_fetch_array($rs);
            mysql_close($con);
            $fr = getSection($df['dftype'],$ref,$df['dfforeignid']);
            $table.= "<p  style=\"text-indent: -25px; margin-left: 25px\">".$tk['tkname']." ".$tk['tksurname']." (".$fr[0][2].") commented on ".date("d F Y H:i",$dt3)."<br><i>&quot;".$comm['dccomment']."&quot;</i>";
            $table.= "</td>";
            $table.= "</tr>";
        }
        $table.= "</table><p>&nbsp;</p>";
    }
    return $table;
}




$ref = $variables['ref'];
$summary = $variables['summary'];
$from = $variables['from'];
$to = $variables['to'];
$tpact = $variables['tpact'];
if($tpact=="combine") {
    $tplevel = "ALL";
} else {
//    $tplevel = $variables['tplevel'];
    $tplevel = "TP";
}
$tpview = $variables['tpview'];
$graph = $variables['graph'];
//$output = $variables['output'];
$data = array();

if($summary == "ALL" || strlen($summary) == 0) {
	$summ[0] = "ALL";
	$summ[1] = 0;
	$frame[0] = array('id'=>0,'txt'=>'All');
} else {
	if($summary == "D" || $summary == "S" || $summary == "T" || $summary == "W") {
		$summ[0] = $summary;
		$summ[1] = 0;
		$frame = getSections($summ[0],$ref);
		if($summary == "S") {
			$frame2 = $frame;
			$frame = array();
			foreach($frame2 as $f2) {
				foreach($f2 as $f) {
					$frame[] = $f;
				}
			}
		} else {
			if($summary == "T" || $summary == "W") {
				$frame2 = $frame;
				$frame = array();
				foreach($frame2 as $f) {
						$frame[] = array('id'=>$f['dfid'],'txt'=>$f['txt']);
				}
			}
		}
	} else { 
		$summ = strFn("explode",$summary,"_","");
		$frame = getSection($summ[0],$ref,$summ[1]);
		if($summ[0] == "S") {
			$frame2 = $frame;
			$frame = array();
			foreach($frame2 as $f2) {
				foreach($f2 as $f) {
					$frame[] = $f;
				}
			}
		} else {
			if($summ[0] == "T" || $summ[0] == "W") {
				$frame2 = $frame;
				$frame = array();
				foreach($frame2 as $f) {
						$frame[] = array('id'=>$f['dfid'],'txt'=>$f['txt']);
				}
			}
		}
	}
}
//print_r($frame);
if(!checkIntRef($ref)) { echoError(); }

$dash = getDashboard($ref);
if($dash['dashfrequency']==1) {
	$from2 = strFn("explode",$from,"-","");
	$to2 = strFn("explode",$to,"-","");
	$fromdate = mktime(12,0,0,$from2[1],$from2[0],$from2[2]);
	$todate = mktime(12,0,0,$to2[1],$to2[0],$to2[2]);
	$sql = "SELECT id FROM ".$dbref."_list_time WHERE sval <= $fromdate AND eval >= $fromdate";
	include("inc_db_con.php");
		$rw = mysql_fetch_array($rs);
		$from = $rw['id'];
	mysql_close($con);
	$sql = "SELECT id FROM ".$dbref."_list_time WHERE sval <= $todate AND eval >= $todate";
	include("inc_db_con.php");
		$rw = mysql_fetch_array($rs);
		$to = $rw['id'];
	mysql_close($con);
}

$fields = getFields($ref);
$records = getRecords($ref);
$vars = getVariables($ref);
$time = getTime();
$frequency = getCaptureFreq();
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
$types = getTypes();

if($graph!="N")
{
    $g = 0;
    $r = 0;
    foreach($records as $rec)
    {
        $f = 0;
        $gdn = "gd_".$rec['recid']."_";
        foreach($fields as $fld)
        {
            $gdn2 = $gdn.$fld['fldid'];
//            echo("<BR> $gdn2");
            if($variables[$gdn2]=="Y" && $g<5)
            {
                $gd[$r]['id'] = $rec['recid'];
                $gd[$r]['f'][$f]['id'] = $fld['fldid'];
                $gd[$r]['f'][$f]['name'] = $fld['fldtxt']." (".$rec['rectxt'].")";
                $f++;
                $g++;
            }
            else
            {
//                $gd[$rec['recid']][$fld['fldid']] = "N";
            }
        }
        $r++;
    }
}

//echo("<P>"); print_r($gd);
?>
<h1><b><?php echo($modtitle); ?>: Report</b></h1>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
	<?php if($variables['reportcomm']=="Y") { ?>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w" valign=top>Report Comment:</td>
		<td><?php echo(strFn("str_replace",decode($dash['dashcomment']),chr(10),"<br>")); ?>
		<?php if($dash['dashcommentdate']!=$dash['dashadddate'] && $dash['dashcommentdate']>0) { ?><br><i>Last updated: <?php echo($dash['dashcommentdate']); ?></i><?php } ?>
        </td>
	</tr>
	<?php } ?>
</table>
<p>&nbsp;</p>
<?php
$table = "";
if($dash['dashfrequency']==1) {
    if($tplevel == "TP") {
    				$from = $from*1;
                    $to = $to*1;
                    for($t=$from;$t<=$to;$t++)
				    {
					    $result = drawTable($t,$t,"",$tpview);
					    $data[$t] = $result['d'];
					    $table.= $result['r'];
					    if($variables['updatecomm']=="Y") {
                            $table.= drawComment($t,$t);
					    }
				    }
    } else {
        if($tplevel == "ALL") {
					    $result = drawTable($from,$to,"combine",$tpview);
					    $data[$to] = $result['d'];
					    $table.= $result['r'];
					    if($variables['updatecomm']=="Y") {
                            $table.= drawComment($t,$t);
					    }
        }
    }
} else {
    switch($tpact)
    {
    case "compare":
        if($from==$to) {
            $result = drawTable($from,$to,"",$tpview);
            $data[$to] = $result['d'];
            $table = $result['r'];
                if($variables['updatecomm']=="Y") {
                    $table.= drawComment($to,$to);
                }
        } else {
			if($dash['dashfrequency']==1) {
				$from = $from*1;
				$to = $to*1;
				for($t=$from;$t<=$to;$t++)
				{
					$result = drawTable($t,$t,"",$tpview);
					$data[$t] = $result['d'];
					$table.= $result['r'];
					if($variables['updatecomm']=="Y") {
						$table.= drawComment($t,$t);
					}
				}
			} else {
				$from = $from*1;
				$to = $to*1;
				for($t=$from;$t<=$to;$t++)
				{
					$result = drawTable($t,$t,"",$tpview);
					$data[$t] = $result['d'];
					$table.= $result['r'];
					if($variables['updatecomm']=="Y") {
						$table.= drawComment($t,$t);
					}
				}
			}
        }
        break;
    case "combine":
            $result = drawTable($from,$to,"combine",$tpview);
            $data[$to] = $result['d'];
            $table = $result['r'];
        break;
    }
}

//echo("<P>"); print_r($data);
?>
<div style="text-align:center">
<?php
switch($graph)
{
    case "line":
/*
 * Read README file first.
 *
 * This example shows how to create a simple line chart with a few configuration
 * directives.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
 */

// Require necessary files
require("lib/AmLineChart.php");

// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js
AmChart::$jQueryPath = "lib/jquery.js";

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmLineChart("myLineChart");

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("");

// Add a label to describe the X values (inside the chart).
//$chart->addLabel("The values on the X axis describe the Purchasing Power in USD Dollars a day.", 0, 20);

// Add all values for the X axis
if($tpact=="combine") {
    $chart->addSerie(0, "".date("M Y",$time[$from]['eval'])." - ".date("M Y",$time[$to]['eval'])."");
} else {
    for($t=$from;$t<=$to;$t++)
    {
        $chart->addSerie($t, "".date("M Y",$time[$t]['eval'])."");
    }
}
//$chart->addSerie("z", " ");

// Define graphs data
$l = 0;
foreach($gd as $g)
{
    $r = $g['id'];
    foreach($g['f'] as $f)
    {
        $line[$l] = array();
        $fid = $f['id'];
        if($tpact=="combine") {
            $line[$l][0] = $data[$to][$r][$fid];
        } else {
            for($t=$from;$t<=$to;$t++)
            {
                $line[$l][$t] = $data[$t][$r][$fid];
            }
        }
        $linename = $f['name'];
        $chart->addGraph("line".$l, $linename, $line[$l], array("line_width"=>2,"bullet"=>"round","bullet_size"=>5,"color"=>$colors[$l]));
        $l++;
    }
}
//echo("<hr>"); print_r($line); echo("<HR>");
// Add graphs
//$chart->addGraph("line1", "Field 1", $line1);

//SET CONFIG
if(($to-$from)>6)
{
$chart->setConfigAll(array(
"width" => 800,
"height" => 500,
//"depth" => 10,
//"angle" => 45,
"plot_area.margins.bottom" => 150,
"background.border_alpha" => 30,
//"legend.enabled" => "false",
"legend.y" => "390",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 5,
"legend.align" => "center",
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 40,
"plot_area.margins.left" => 100
));
}
else
{
$chart->setConfigAll(array(
"width" => 600,
"height" => 500,
//"depth" => 10,
//"angle" => 45,
"plot_area.margins.bottom" => 150,
"background.border_alpha" => 30,
//"legend.enabled" => "false",
"legend.y" => "390",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 5,
"legend.align" => "center",
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 40,
"plot_area.margins.left" => 100
));
}

// Print the code
echo $chart->getCode();
    break;
    case "bar":
    require("lib/AmBarChart.php");
// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js
AmChart::$jQueryPath = "lib/jquery.js";


// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmBarChart("mybar");

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
//$chart->setTitle("<span style=\"font-weight: bold;\">".$titletxt."</span>");

// Add a label to describe the X values (inside the chart).
//$chart->addLabel("The values on the X axis describe the Purchasing Power in USD Dollars a day.", 0, 20);

// Add all values for the X axis
//$chart->addSerie("r", "Revenue");
//$chart->addSerie("o", "Operational Expenditure");
//$chart->addSerie("c", "Capital Expenditure");
if($tpact=="combine")
{
    $chart->addSerie(0, "".date("M Y",$time[$from]['eval'])." - ".date("M Y",$time[$to]['eval'])."");
} else {
    for($t=$from;$t<=$to;$t++)
    {
        $chart->addSerie($t, "".date("M Y",$time[$t]['eval'])."");
    }
}
//$chart->addSerie("z", " ");
// Define graphs data
$l = 0;
foreach($gd as $g)
{
    $r = $g['id'];
    foreach($g['f'] as $f)
    {
        $line[$l] = array();
        $fid = $f['id'];
        if($tpact=="combine") {
            $line[$l][0] = $data[$to][$r][$fid];
        } else {
            for($t=$from;$t<=$to;$t++)
            {
                $line[$l][$t] = $data[$t][$r][$fid];
            }
        }
        $linename = $f['name'];
        $chart->addGraph("line".$l, $linename, $line[$l],array("color"=>$colors[$l]));
        $l++;
    }
}

// Add graphs
/*$chart->addGraph("budget", "Budget", $budget, array("color" => "#FF9900"));
$chart->addGraph("actual", "Actuals", $actual, array("color" => "#009900"));
//$chart->addGraph("cap", "Capex", $cap);//, array("color" => "#DD0000"));
/*$chart->addGraph("oa", "Opex (Actual)", $oa, array("color" => "#AA0000"));
$chart->addGraph("cb", "Capex (Budget)", $cb, array("color" => "#FF9900"));
$chart->addGraph("ca", "Capex (Actual)", $ca, array("color" => "#CD7B00"));
*/
if(($to-$from)<5 || $tpact=="combine")
{
//SET CONFIG
$chart->setConfigAll(array(
"width" => 600,
"height" => 500,
"depth" => 0,
"angle" => 0,
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 40,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 150,
"background.border_alpha" => 30,
//"legend.enabled" => "false",
"legend.y" => "380",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;value&#125;&#93;&#93;>",
//"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
//"column.data_labels_text_color" => "#ffffff",
"column.width" => 75,
"column.spacing" => 2
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"

));
} else {
    if(($to-$from)>8)
    {
//SET CONFIG
$chart->setConfigAll(array(
"width" => 1000,
"height" => 500,
"depth" => 0,
"angle" => 0,
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 40,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 150,
"background.border_alpha" => 30,
//"legend.enabled" => "false",
"legend.y" => "380",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;value&#125;&#93;&#93;>",
//"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
//"column.data_labels_text_color" => "#ffffff",
"column.width" => 75,
"column.spacing" => 2
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"

));
    } else {
//SET CONFIG
$chart->setConfigAll(array(
"width" => 800,
"height" => 500,
"depth" => 0,
"angle" => 0,
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 40,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 150,
"background.border_alpha" => 30,
//"legend.enabled" => "false",
"legend.y" => "380",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;value&#125;&#93;&#93;>",
//"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
//"column.data_labels_text_color" => "#ffffff",
"column.width" => 75,
"column.spacing" => 2
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"

));
    }
}

echo decode($chart->getCode());
    break;
    case "pie":
/*
 * Read README file first.
 *
 * This example shows how to create a simple pie chart with a few configuration
 * directives and a connected HTML table as legend.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
 */

// Require necessary files
require("lib/AmPieChart.php");

// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js and AmCharts.js (only needed for pie legend)
AmChart::$jsPath = "lib/AmCharts.js";
AmChart::$jQueryPath = "lib/jquery.js";
// Tell AmChart to load jQuery if you don't already use it on your site.
AmChart::$loadJQuery = true;

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmPieChart("mypie");

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
//$chart->setTitle("<p><span style=\"font-weight: bold;\">".$titletxt."</span></p>");

// Add slices
//$chart->addSlice("met", "KPI&#39;s met (".$kpi['g']." of ".$kpi['tot'].")", $kpi['g'], array("color" => "#009900"));
$l = 0;
foreach($gd as $g)
{
    $r = $g['id'];
    foreach($g['f'] as $f)
    {
        $line[$l] = array();
        $fid = $f['id'];
        $line[$l] = $data[$to][$r][$fid];
        $linename = $f['name'];
        $chart->addSlice("line".$l, $linename, $line[$l], array("color"=>$colors[$l]));
        $l++;
    }
}

//Settings
    $chart->setConfigAll(array(
    "width" => 600,
    "height" => 600,
"font" => "Tahoma",
"decimals_separator" => ".",
    "pie.y" => "240",
    "pie.radius" => 200,
    "pie.height" => 1,
    "pie.angle" => 0,
    "data_labels.radius" => "-15%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{value}<br>({percents}%)]]>",
    "legend.y" => "480",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 3,
    "legend.margins" => 5,
    "legend.align" => "center",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;&#93;&#93;>",
"background.border_alpha" => 20,
"background.border_color" => "#000000"
    ));

// Print the code
echo html_entity_decode($chart->getCode());
    break;
    default:
    break;
}
echo("<P>&nbsp;</p>".$table);
?>
</div>
<p>&nbsp;</p>
</body>

</html>
