<?php include("inc_head.php"); ?>
<h1><b><?php echo($modtitle); ?>: Admin - User Responsibility Report</b></h1>
<table cellpadding=3 cellspacing=0>
    <tr>
        <th>Ref</th>
        <th>Dashboard Name</th>
        <th>Dashboard Owner</th>
        <th>Frame Type</th>
        <th>Frame Name</th>
        <th>Primary Capturer</th>
        <th>Secondary Capturer</th>
    </tr>
<?php
    $ftype = array(
        array("D","Directorate"),
        array("S","Sub-Directorate"),
        array("T","Town"),
        array("W","Ward")
    );

$sql = "SELECT d.*, tk.tkname, tk.tksurname FROM ".$dbref."_dashboard d, assist_".$cmpcode."_timekeep tk WHERE dashstatus = 'Y' AND d.dashowner = tk.tkid ORDER BY d.dashname";
include("inc_db_con.php");
while($dash = mysql_fetch_array($rs))
{
    $ref = $dash['dashid'];
    $dashowner = $dash['tkname']." ".$dash['tksurname'];
    $dashname = $dash['dashname'];
    $row = 0;
?>
    <tr>
        <th><?php echo($ref); ?></th>
        <td style="font-weight: bold;"><?php echo($dashname); ?></td>
        <td><?php echo($dashowner); ?></td>
        <?php
    foreach($ftype as $type)
    {
        $sections = getSections($type[0],$ref);
        if($type[0]=="S") {
            $sections2 = $sections;
            $sections = array();
            foreach($sections2 as $sec)
            {
                foreach($sec as $s) {
                    $sections[] = $s;
                }
            }
        }
        $admins = getDashAdmins($type[0],$ref);
        foreach($sections as $sec)
        {
            if($row>0) { ?>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold;"><?php //print_r($sections); ?></td>
                    <td></td>
            <?php
            }
            $primary = $admins[$sec['dfid']]['Y'];
            $secondary = $admins[$sec['dfid']]['N'];
            if(count($primary)>0) {
                $primary = $primary['tkname']." ".$primary['tksurname'];
            } else {
                $primary = "";
            }
            if($primary == "Ignite Support") { $primary = ""; }
            if(count($secondary)>0) {
                $secondary = $secondary['tkname']." ".$secondary['tksurname'];
            } else {
                $secondary = "";
            }
            if($secondary == "Ignite Support") { $secondary = ""; }
            ?>
            <td><?php echo($type[1]); ?></td>
            <td><?php echo($sec['txt']); ?></td>
            <td><?php echo($primary); ?></td>
            <td><?php echo($secondary); ?></td>
            <?php
            $row++;
        }
    }
        ?>
    </tr>
<?php
}
mysql_close($con);
?>
</table>


</body>

</html>
