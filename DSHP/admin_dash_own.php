<?php
include("inc_head.php");
include("inc/Admin.php");
$start = $variables['s'];
if(strlen($start)==0)
{
    $start = $_GET['s'];
}
$find1 = html_entity_decode($_GET['f'],ENT_QUOTES,"ISO-8859-1");
$find = explode(" ",$find1);
if(strlen($start)==0 || (!is_numeric($start) && $start != "e")) { $start = 0; }
$limit = 15;
$sql = "SELECT count(d.dashid) as ct FROM ".$dbref."_dashboard d, assist_".$cmpcode."_timekeep t WHERE d.dashstatus NOT IN ('C','D') AND d.dashowner = tkid ";
foreach($find as $needle)
{
    $sql.= " AND d.dashname LIKE '%".$needle."%' ";
}
include("inc_db_con.php");
    $mr = mysql_fetch_array($rs);
mysql_close($con);
$max = $mr['ct'];
$pages = ceil($max/$limit);

if(!is_numeric($start)) {
    $start = $max - ($max - ($limit*($pages-1)));
}
$page = ($start / $limit) + 1;

$tk = array();
        $sql2 = userList("",0,"A");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $tk[] = $row2;
            }
        mysql_close($con2);

?>
<script type=text/javascript>
function updateDO(ref) {
    if(!isNaN(parseInt(ref)))
    {
        var owner = document.getElementById('tki'+ref).value;
        if(owner!="X" && owner.length>3)
        {
            document.location.href = "admin_dash_own_process.php?ref="+ref+"&tki="+owner;
        }
    }
}
function goNext(s,f) {
    f = escape(f);
    document.location.href = "admin_dash_own.php?s="+s+"&f="+f;
}
</script>
<h1><b><?php echo($modtitle); ?>: Admin - Transfer Ownership</b></h1>
<table cellspacing="0" cellpadding="3" class=noborder>
<tr><td class=noborder>
<form name=veiw action=admin_dash_own.php method=get>
    <table cellpadding=5 cellspacing=0 width=100% style="margin-bottom: 10px;">
        <tr>
            <td width=40% style="border-right: 0px;"><input type=button id=b1 value=" |< " onclick="goNext(0,'<?php echo($find1)?>');"> <input type=button id=b2 value=" < " onclick="goNext(<?php echo($start-$limit); ?>,'<?php echo($find1)?>');"> Page <?php echo($page); ?>/<?php echo($pages); ?> <input type=button value=" > " id=b3 onclick="goNext(<?php echo($start+$limit); ?>,'<?php echo($find1)?>');"> <input type=button value=" >| " id=b4 onclick="goNext('e','<?php echo($find1)?>');"></td>
            <td width=20% style="border-right: 0px; border-left: 0px;" align=center>
                <?php if(strlen($find1)>0) { ?>
                    <input type=button value="Clear Search" onclick="document.location.href = 'admin_dash_own.php';">
                <?php } ?>
            </td>
            <td width=40% style="text-align:right;border-left: 0px;">Search: <input type=hidden name=s value=0><input type=text width=10 name=f> <input type=submit value=" Go "></td>
        </tr>
    </table></form>
<form action=admin_dash_own_process.php method=post>
<table cellpadding=3 cellspacing=0>
    <tr height=20>
        <th>&nbsp;Ref&nbsp;</th>
        <th>Dashboard Name</th>
        <th>Dashboard Status</th>
        <th>Current Owner</th>
        <th>New Owner</th>
    </tr>
<?php
$sql = "SELECT d.*, t.tkname, t.tksurname FROM ".$dbref."_dashboard d, assist_".$cmpcode."_timekeep t WHERE dashstatus NOT IN ('C','D') AND dashowner = tkid ";
foreach($find as $needle)
{
    $sql.= " AND dashname LIKE '%".$needle."%' ";
}
$sql.= "ORDER BY d.dashname LIMIT ".$start.", ".$limit;
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $dashstatus = $row['dashstatus'];
    if($dashstatus=="Y")
    {
        $status = "Active";
    }
    else
    {
        $ds = strFn("substr",$dashstatus,0,1);
        switch($ds)
        {
            case "E":
                $status = "Inactive - Edit";
                break;
            case "P":
                $status = "Inactive - New";
                break;
            default:
                $status = "Unknown";
                break;
        }
    }
    ?><tr>
        <td class="tdheader b-bottom-w b-top-w"><?php echo($row['dashid']); ?></td>
        <td><?php echo($row['dashname']); ?></td>
        <td><?php echo($status); ?></td>
        <td><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
        <td><input type=hidden name=ref[] id=ref<?php echo($row['dashid']); ?> value="<?php echo($row['dashid']); ?>"><select name=tki[] id=tki<?php echo($row['dashid']); ?> ><option selected value=X>--- SELECT ---</option>
        <?php
        foreach($tk as $t)
        {
            echo("<option value=".$t['tkid'].">".$t['tkname']." ".$t['tksurname']."</option>");
        }
        ?>
        </select> <input type=button value=Update onclick="updateDO(<?php echo($row['dashid']); ?>);"></td>
    </tr><?php
}
mysql_close($con);
?>
    <tr height=20>
        <td colspan=5><input type=submit value="Update All"></td>
    </tr>
</table>
</form>
</td></tr></table>
<script type=text/javascript>
<?php if($start==0) { echo(chr(10)); ?> document.getElementById('b1').disabled = true; document.getElementById('b2').disabled = true; <?php } ?>
<?php if($start == ($max - ($max - ($limit*($pages-1)))) ) { echo(chr(10));  ?>document.getElementById('b3').disabled = true; document.getElementById('b4').disabled = true;<?php } ?>
</script>
</body>

</html>
