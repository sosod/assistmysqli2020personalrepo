<?php
include("inc_head.php");
$variables = $_REQUEST;
//print_r($variables);
$timeid = $variables['timeid'];
$ref = $variables['ref'];
$timedate = $variables['timedate'];

if(checkIntRef($ref) && checkIntRef($timeid))
{
    $dash = getDashboard($ref);
    if($dash['dashfrequency']>3)
    {
        if(checkIntRef($timeid))
        {
            echo("<script type=text/javascript>");
                echo("document.location.href = 'view_dashboard.php?ref=$ref&timeid=$timeid';");
            echo("</script>");
        }
    }
    else
    {
        if(strlen($timedate)>0)
        {
            echo("<script type=text/javascript>");
                echo("document.location.href = 'view_dashboard.php?ref=$ref&timeid=$timeid&timedate=$timedate';");
            echo("</script>");
        }
    }
}

$time = getTime();
$style1 = "padding: 5 7 5 7";
$style2 = "padding: 5 10 5 10";

//if(checkIntRef($timeid))
//{
    $t = $time[$timeid];
//    echo("TIME:");
//    print_r($t);
?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('.datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/images/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:false,
                    changeYear:false,
                    minDate: new Date(<?php echo(date("Y",$t['sval'])); ?>, <?php echo(date("m",$t['sval'])-1); ?>, <?php echo(date("d",$t['sval'])); ?>),
                    maxDate: new Date(<?php echo(date("Y",$t['eval'])); ?>, <?php echo(date("m",$t['eval'])-1); ?>, <?php echo(date("d",$t['eval'])); ?>)
                });

			});

</script>
<?php
//}
?>
<script type=text/javascript>
function Validate() {
    if(document.getElementById('i').value != "X")
    {
        document.forms['frme'].submit();
    }
    else
    {
        alert("Please selected a dashboard.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: View</b></h1>
<?php displayResult($result); ?>
<form name=frme action=view.php method=post>
<table cellpadding=3 cellspacing=0>
    <tr>
        <td class="tdheaderl b-bottom-w" style="<?php echo($style1); ?>;" width=100>Dashboard:</td>
        <td style="<?php echo($style2); ?>;"><select name=ref id=i><option <?php if(!checkIntRef($ref)) { echo("selected"); } ?> value=X>--- SELECT ---</option><?php
        $sql = "SELECT dashid, dashname FROM ".$dbref."_dashboard WHERE dashstatus = 'Y' ORDER BY dashname";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option ");
                if(checkIntRef($ref) && $row['dashid']==$ref) { echo(" selected "); }
                echo("value=".$row['dashid'].">".$row['dashname']." (ref: ".$row['dashid'].")</option>");
            }
        mysql_close($con);
        ?></select></td>
    </tr>
    <?php if(checkIntRef($ref)) { ?>
    <tr>
        <td class="tdheaderl b-top-w" style="<?php echo($style1); ?>;" width=100 rowspan=2>When:</td>
        <td style="<?php echo($style2); ?>;"><select name=timeid><?php
            foreach($time as $t)
            {
                echo("<option ");
                if($timeid == $t['id']) { echo("selected "); }
                echo("value=".$t['id'].">".date("M Y",$t['eval'])."</option>");
            }
        ?></select></td>
    </tr>
    <tr>
        <td style="<?php echo($style2); ?>;"><input type=text class=datepicker readonly=readonly size=10 name=timedate></td>
    </tr>
    <?php } else { ?>
    <tr>
        <td class="tdheaderl b-top-w" style="<?php echo($style1); ?>;" width=100>When:</td>
        <td style="<?php echo($style2); ?>;"><select name=timeid><?php
            foreach($time as $t)
            {
                echo("<option ");
                if($t['id']==1) { echo("selected "); }
                echo("value=".$t['id'].">".date("M Y",$t['eval'])."</option>");
            }
        ?></select></td>
    </tr>
    <?php } ?>
    <tr>
        <td colspan=2 style="<?php echo($style1); ?>;"><input type=button value=" View " onclick="Validate();"></td>
    </tr>
</table>
</form>
</body>

</html>
