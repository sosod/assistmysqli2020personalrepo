<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEditProgress.php");
$frequency = getCaptureFreq();
?>
<script type=text/javascript>
function cancelDash(r) {
    if(!isNaN(parseInt(r)))
    {
        if(confirm("Are you sure you wish to delete dashboard "+r+"?")==true)
        {
            document.location.href = "admin_edit.php?ref="+r+"&cancelact=Cancel";
        }
    }
    else
    {
        alert("An error occurred.  Please reload the page and try again.");
    }
}

function editDash(r) {
    var fld = "step"+r;
    var step = document.getElementById(fld).value;
    if(step!="X")
    {
        if(step!="A")
        {
            var url = "admin_edit"+step+"-1.php?ref="+r;
        }
        else
        {
            var url = "admin_edit_users.php?ref="+r;
        }
        document.location.href = url;
    }

}
</script>
<a name=top></a>
<h1><b><?php echo($modtitle); ?>: Admin - Edit My Dashboards</b></h1>
<?php displayResult($result); ?>
<a name=current></a>
<h2>Active Dashboards</h2>
<table cellpadding=3 cellspacing=0 width=100%>
    <tr height=30>
        <th width=40>Ref</th>
        <th>Dashboard Name</th>
        <th width=150>Capture Frequency</th>
        <th width=150>Linked to SDBIP?</th>
        <th width=150>Last Modified</th>
        <th width=150>&nbsp;</th>
    </tr>
<?php
$b = 0;
    $sql = "SELECT * FROM ".$dbref."_dashboard WHERE dashstatus = 'Y' AND dashowner = '$tkid' ORDER BY dashname";
    include("inc_db_con.php");
        while($dash = mysql_fetch_array($rs))
        {
            $b++;
            $ref = $dash['dashid'];
            $status = $dash['dashstatus'];
            $name = $dash['dashname'];
            $sql2 = "SELECT * FROM ".$dbref."_dashboard_status WHERE statdashid = $ref ORDER BY statid DESC LIMIT 1";
            include("inc_db_con2.php");
                $stat = mysql_fetch_array($rs2);
            mysql_close($con2);
            $lastmod = $stat['statmoddate'];
            if(strlen($lastmod)==0) { $lastmod = $dash['dashadddate']; }
            $next = "admin_edit1.php?ref=".$ref;
            ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader style="border-bottom-color: #ffffff;"><?php echo($ref); ?></td>
        <td style="font-weight: bold; padding-left: 10px;"><?php echo($name); ?></td>
        <td width=150 align=center><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
        <td width=150 align=center><?php echo(YesNo($dash['dashsdbip'])); ?></td>
        <td width=150 align=center><?php echo($lastmod); ?></td>
        <td align=center><select id=<?php echo("step".$ref); ?>><option selected value=X>--- SELECT STEP ---</option>
        <?php
            for($p=2;$p<6;$p++)
            {
                if($p!=5 || ($p==5 && $dash['dashsdbip']=="Y"))
                    echo("<option value=$p >".$process[0][$p]['txt']."</option>");
            }
        ?>
        <option value=A>Update Admins</option>
        </select><Br><input id=<?php echo $b; ?> type=button value="Edit details" id=<?php echo("move".$ref); ?> onclick="editDash(<?php echo($ref); ?>)"> <input type=button value=Delete onclick="cancelDash(<?php echo($ref); ?>);"></td>
    </tr>
            <?php
        }
    mysql_close($con);
?>
</table>
<script type=text/javascript>
<?php //echo("butKill(1,$b);"); ?>
</script>

<?php
$urlback = "admin.php";
include("inc_goback.php");

include("admin_edit_pending.php");
?>

<table cellpadding=2 cellspacing=0 style="position: fixed; bottom: 0px; left: 40%">
    <tr>
        <td style="padding: 2 4 4 4;"><b>Go to:</b> <a href=#current>Active</a> | <a href=#pending>Inactive</a> | <a href=#top>Top</a></td>
    </tr>
</table>
</body>

</html>
