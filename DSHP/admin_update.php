<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreateProgress.php");
$frequency = getCaptureFreq();
$dirs = getDirectorates();
$subdirs = getSubDirectorates();
$wards = getWards();
$towns = getTowns();

function getIDS($arr,$type) {
    $ids = array();
    switch($type) {
        case "D": $fld = "dirid"; break;
        case "S": $fld = "subid"; break;
        default: $fld = "id"; break;
    }
    if($type == "S") {
        foreach($arr as $a) {
            foreach($a as $s) { $ids[] = $s[$fld]; }
        }
    } else {
        foreach($arr as $a) { $ids[] = $a[$fld]; }
    }
    return $ids;
}

$dirid = getIDS($dirs,"D");
$subid = getIDS($subdirs,"S");
$wardid = getIDS($wards,"W");
$townid = getIDS($towns,"T");

function drawSQL($type) {
    global $dbref;
    global $cmpcode;
    global $dirid;
    global $subid;
    global $wardid;
    global $townid;
    global $tkid;
    if($type == "PA") { $dap = "Y"; } else { $dap = "N"; }
    
    $sqlz = "SELECT DISTINCT d.*, tkname, tksurname FROM";
    $sqlz.= "  ".$dbref."_dashboard d";
    if($type!="DO") {
        $sqlz.= ", ".$dbref."_dashboard_frame df";
        $sqlz.= ", ".$dbref."_dashboard_admins da";
    }
    $sqlz.= ", assist_".$cmpcode."_timekeep t";
    $sqlz.= " WHERE ";
    $sqlz.= "     tkid = dashowner";
    $sqlz.= " AND dashstatus = 'Y'";
    switch($type)
    {
        case "DO":
            $sqlz.= " AND dashowner = '$tkid' ";
            break;
        default:
            //$sqlz.= " AND dashowner <> '$tkid' ";
            $sqlz.= " AND dashid = dfdashid";
            $sqlz.= " AND dfid = daframeid";
            $sqlz.= " AND dfyn = 'Y'";
            $sqlz.= " AND dayn = 'Y'";
            $sqlz.= " AND daprimary = '$dap'";
            $sqlz.= " AND datkid = '$tkid'";
            $sqlz.= " AND (";
            $sqlz.= "       (dfforeignid IN (".strFn("implode",$dirid,",","").") AND dftype = 'D')";
            $sqlz.= " OR    (dfforeignid IN (".strFn("implode",$subid,",","").") AND dftype = 'S')";
            $sqlz.= " OR    (dfforeignid IN (".strFn("implode",$wardid,",","").") AND dftype = 'W')";
            $sqlz.= " OR    (dfforeignid IN (".strFn("implode",$townid,",","").") AND dftype = 'T')";
            $sqlz.= " )";
            break;
    }
    $sqlz.= " ORDER BY dashname";
    return $sqlz;
}
?>
<h1><b><?php echo($modtitle); ?>: Admin - Update Dashboards</b></h1>
<?php
$dashsec = array();
$dashsec[1] = array(
    "id"=>"PA",
    "head"=>"Primary"
);
$dashsec[2] = array(
    "id"=>"SA",
    "head"=>"Secondary"
);
$dashsec[3] = array(
    "id"=>"DO",
    "head"=>"My Dashboards"
);
$b = 0;
foreach($dashsec as $ds)
{
    $sql = drawSQL($ds['id']);
    include("inc_db_con.php");
    if(mysql_num_rows($rs)>0) {
        echo("<h2>".$ds['head']."</h2>");
?>
<table cellpadding=3 cellspacing=0 width=100%>
    <tr height=30>
        <th width=40>Ref</th>
        <th>Dashboard Name</th>
        <th width=150>Dashboard Owner</th>
        <th width=150>Capture Frequency</th>
        <th width=150>Linked to SDBIP?</th>
        <th width=120>&nbsp;</th>
    </tr>
<?php

        while($dash = mysql_fetch_array($rs))
        {
            $b++;
            $ref = $dash['dashid'];
            $status = $dash['dashstatus'];
            $name = $dash['dashname'];
            $owner = $dash['tkname']." ".$dash['tksurname'];
//            if($ds['id']!="DO") {
                $next = "admin_update_draw1.php?ref=".$ref;
//            } else {
//                $next = "admin_update.php";
//            }
            ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader style="border-bottom-color: #ffffff;"><?php echo($ref); ?></td>
        <td style="font-weight: bold; padding-left: 10px;"><?php echo($name); ?></td>
        <td width=150 style="text-align:center"><?php echo("$owner"); ?></td>
        <td width=150 style="text-align:center"><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
        <td width=150 style="text-align:center"><?php echo(YesNo($dash['dashsdbip'])); ?></td>
        <?php if($ds['id']=="DO") { ?>
        <td style="text-align:center"><input type=button value=Comment onclick="document.location.href = 'admin_comment.php?ref=<?php echo($ref); ?>';"> <input id=<?php echo $b; ?> type=button value="Update" id=<?php echo("move".$ref); ?> onclick="document.location.href = '<?php echo($next); ?>';"></td>
        <?php } else { ?>
        <td style="text-align:center"><input id=<?php echo $b; ?> type=button value="Update" id=<?php echo("move".$ref); ?> onclick="document.location.href = '<?php echo($next); ?>';"></td>
        <?php } ?>
    </tr>
            <?php
        }
    }
    mysql_close($con);
?>
</table>
<?php
} //foreach dashsec

?>
<script type=text/javascript>
<?php //echo("butKill(1,$b);"); ?>
</script>

<?php
$urlback = "admin.php";
include("inc_goback.php");
?>


</body>

</html>
