<?php
include("inc_head_setup.php");
include("inc/setup.php");
include("inc_admin.php");
?>
<script type=text/javascript>
function Validate(me) {
    var tkid = me.tkid.value;
    var dir = me.dir.value;
    if(tkid == "X")
    {
        alert("Please select the user you wish to add as a directorate administrator.");
        return false;
    }
    else
    {
        if(dir == "D")
        {
            dirid = me.dirid.value;
            me.dir.value = "D_"+dirid;
            return true;
        }
        else
        {
            if(dir=="S")
            {
                alert("Please select which Sub-Directorate you wish to give the user administrative access to.");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    return false;
}
</script>
<h1><?php echo($modtitle); ?>: Setup - Directorate Administrators ~ Add</b></h1>
<?php displayResult($result); ?>
<?php
$dirid = $_GET['d'];
$subid = $_GET['s'];
if(strlen($subid)==0) { $subid = 0; }
    $sql = "SELECT * FROM assist_".$cmpcode."_list_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $dir = mysql_fetch_array($rs);
    mysql_close($con);
?>
<form name=add method=get action=setup_admin_config.php onsubmit="return Validate(this);">
<input type=hidden name=dirid value=<?php echo($dirid); ?>>
<input type=hidden name=a value=admin>
<input type=hidden name=act value=add>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=170>(Sub-)Directorate:</td>
		<td class=tdgeneral valign=top><select name=dir>
            <option value=D>--- DIRECTORATE ---</option>
            <option value=D_<?php echo($dirid); if($subid == 0) { echo(" selected"); } ?>><?php echo($dir['dirtxt']); ?></option>
            <option value=S>--- SUB-DIRECTORATE ---</option>
		  <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_list_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con.php");
                while($sub = mysql_fetch_array($rs))
                {
                    echo("<option ");
                    if($subid > 0 && $subid == $sub['subid']) { echo(" selected "); }
                    echo("value=S_".$sub['subid'].">".$sub['subtxt']."</option>");
                }
            mysql_close($con);
?>
        </select></td>
    </tr>
	<?php
	$sql = userList("",0,"A");
    include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=2>No Users available.</td>
	</tr>
    <?php
    }
    else
    {
    ?>
    <tr>
		<td class=tdheaderl>User:</td>
		<td class=tdgeneral valign=top><select name=tkid>
            <option value=X selected>--- SELECT ---</option>
        <?php
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            echo("<option value=\"".$id."\">".$val."</option>");
        }
        ?>
        </select></td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Save "></td>
    </tr>
    <?php
    }
    mysql_close($con);
    ?>
</table>
</form>
<p>&nbsp;</p>
</body>
</html>
