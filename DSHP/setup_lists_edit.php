<?php
include("inc_head_setup.php");
include("inc/setup.php");
$row = array();
$ref = $_GET['i'];
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function Validate(me) {
    var nt = me.w.value;
    var ot = me.w2.value;
    
    if(nt.length>0 && nt != ot)
    {
        return true;
    }
    else
    {
        alert("Please enter a new value that is different from the original value.");
        return false;
    }
}
function editWard(i) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete this list?")==true)
        {
            document.location.href = "setup_lists.php?a=lists&act=del&i="+i;
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
function delValue(i,t) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete "+t+" from this list?")==true)
        {
            document.location.href = "setup_lists_edit.php?a=lists&act=delv&v="+i+"&i=<?php echo($ref); ?>";
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Lists ~ Edit</b></h1>
<?php displayResult($result); ?>
<?php

if(strlen($ref)>0 && is_numeric($ref))
{
    $sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE id = $ref AND yn = 'Y'";
    include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $detail = mysql_fetch_array($rs);
    mysql_close($con);
}
else
{
    $mnr = 0;
}

if($mnr == 0)
{
    echo("<h2>ERROR</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
?>
<form name=ward method=get action=setup_lists.php onsubmit="return Validate(this)" language=jscript>
<input type=hidden name=a value=lists><input type=hidden name=act value=edit>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;">Reference:</th>
        <td><?php echo($ref); ?><input type=hidden name=i value=<?php echo($ref); ?>></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">Original List Name:</th>
        <td><?php echo($detail['value']); ?><input type=hidden name=v2 id=otxt value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;border-top: 1px solid #ffffff;">New List Name:</th>
        <td><input type=text size=50 maxlength=100 id=ntxt name=v value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
    <tr height=27>
        <td colspan=2><input type=submit value="Save changes"> <input type=reset> <input type=button value=" Delete " onclick="editWard(<?php echo($ref); ?>)"></td>
    </tr>
</table>
</form>
<form name=add method=get action=setup_lists_edit.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=a value=lists><input type=hidden name=act value=addv><input type=hidden name=i value=<?php echo($ref); ?>>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th width=30>Ref</th>
        <th width=510 style="border-right: 0px;">Value</th>
        <th width=60 style="border-left: 0px;">&nbsp;</th>
    </tr>
<?php
$sql = "SELECT id, value FROM ".$dbref."_list_dropdowns_values WHERE yn = 'Y' AND listid = $ref ORDER BY id";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        include("inc_tr.php");
?>
        <td class=tdheader style="border-bottom: 1px solid #ffffff;"><?php echo($row['id']); ?></td>
        <td style="padding-left: 7px; border-right: 0px;"><?php echo($row['value']); ?></td>
        <td align=center style="border-left: 0px;"><input type=button value="Edit" onclick="document.location.href = '<?php echo("setup_lists_edit_values.php?i=$ref&v=".$row['id']); ?>';"><input type=button value="Del" onclick="delValue(<?php echo($row['id'].",'".strFn("str_replace",decode($row['value']),"'","\'")."'"); ?>);" style="padding-left: 1px; padding-right: 2px;"></td>
    </tr>
<?php
    }
mysql_close($con);
    include("inc_tr.php");
?>
        <td class=tdheader>&nbsp;</td>
        <td style="padding-left: 7px; border-right: 0px;"><input type=text size=50 maxlength=100 name=listv></td>
        <td align=center style="border-left: 0px;"><input type=submit value=" Add "></td>
    </tr>
</table>
</form>

<?php
}
$urlback = "setup_lists.php";
include("inc_goback.php");

displayChangeLog("S_LISTS",$ref,$urlback);

?>
</body>

</html>
