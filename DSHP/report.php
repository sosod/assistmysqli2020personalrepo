<?php
$reportsec = array("","Y","Y","Y","Y","Y","Y");
include("inc_head.php");
$variables = $_REQUEST;
//print_r($variables);
$ref = $variables['ref'];

if(checkIntRef($ref))
{
    $dash = getDashboard($ref);
}

$time = getTime();
$style1 = "padding: 5 7 5 7";
$style2 = "padding: 5 10 5 10";

//if(checkIntRef($timeid))
//{
    $t1 = $time[1];
    $t2 = $time[12];
//    echo("TIME:");
//    print_r($t);
?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('.datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/images/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:false,
                    changeYear:false,
                    minDate: new Date(<?php echo(date("Y",$t1['sval'])); ?>, <?php echo(date("m",$t1['sval'])-1); ?>, <?php echo(date("d",$t1['sval'])); ?>),
                    maxDate: new Date(<?php echo(date("Y",$t2['eval'])); ?>, <?php echo(date("m",$t2['eval'])-1); ?>, <?php echo(date("d",$t2['eval'])); ?>)
                });

			});
</script>
<?php
//}
?>
<script type=text/javascript>
function sumChange(me) {
	if(me.value == "ALL") {
            document.getElementById('csvn').disabled = false;
            document.getElementById('csvy').disabled = false;
            document.getElementById('csvn').checked = true;
            document.getElementById('csvy').checked = false;
	} else {
            document.getElementById('csvn').disabled = false;
            document.getElementById('csvy').disabled = true;
            document.getElementById('csvn').checked = true;
            document.getElementById('csvy').checked = false;
	}
	if(me.value != "D" && me.value != "S" && me.value != "T" && me.value != "W") {
            document.getElementById('line').disabled = false;
            document.getElementById('linepic').src = "lib/images/line.gif";
            document.getElementById('bar').disabled = false;
            document.getElementById('barpic').src = "lib/images/bar.gif";
            document.getElementById('pie').disabled = false;
            document.getElementById('piepic').src = "lib/images/pie.gif";
		trackTP(document.getElementById('ta'),2);
	} else {
            document.getElementById('line').disabled = true;
            document.getElementById('linepic').src = "lib/images/line_g.gif";
            document.getElementById('bar').disabled = true;
            document.getElementById('barpic').src = "lib/images/bar_g.gif";
            document.getElementById('pie').disabled = true;
            document.getElementById('piepic').src = "lib/images/pie_g.gif";
	}
}

function trackTP(me,f) {
	if(document.getElementById('sum').value == "ALL") {
    switch(me.value) {
        case "combine":
            document.getElementById('line').disabled = true;
            document.getElementById('linepic').src = "lib/images/line_g.gif";
            document.getElementById('no').checked = true;
            if(f==1) {
                document.getElementById('tpl').disabled = true;
                document.getElementById('lev').style.display = "none";
                document.getElementById('lev2').style.display = "inline";
                document.getElementById('tpl2').disabled = false;
            }
            break;
        case "compare":
            document.getElementById('line').disabled = false;
            document.getElementById('linepic').src = "lib/images/line.gif";
            document.getElementById('no').checked = true;
            if(f==1) {
                document.getElementById('tpl2').disabled = true;
                document.getElementById('lev2').style.display = "none";
                document.getElementById('lev').style.display = "inline";
                document.getElementById('tpl').disabled = false;
            }
            break;
    }
	}
}

function graphYN(yn) {
    switch(yn)
    {
        case "N":
            document.getElementById('csvn').disabled = false;
            document.getElementById('csvy').disabled = false;
            document.getElementById('csvn').checked = true;
            document.getElementById('csvy').checked = false;
            document.getElementById('graphdata').style.display = "none";
            break;
        case "Y":
            document.getElementById('csvn').checked = true;
            document.getElementById('csvy').checked = false;
            document.getElementById('csvn').disabled = false;
            document.getElementById('csvy').disabled = true;
            document.getElementById('graphdata').style.display = "inline";
            break;
    }
}

function Validate() {
    if(document.getElementById('i').value != "X")
    {
        document.forms['frme'].submit();
    }
    else
    {
        alert("Please selected a dashboard.");
    }
}
function Validate2() {
/*        var fm = document.getElementById('fm').value;
        var to = document.getElementById('to').value;
        if(fm!="X" && fm.length > 0 && to!="X" && to.length > 0)
        {
            return true;
        } else {
            alert("Please select a time period.");
        }
*/
//        return false;
return true;
}</script>
<h1><b><?php echo($modtitle); ?>: Report</b></h1>
<?php $i = 1; ?>
<h2><?php echo($i); $i++; ?>. Select Dashboard</h2>
<?php if(!checkIntRef($ref)) { ?>
<form name=frme action=report.php method=post>
<table cellpadding=3 cellspacing=0>
    <tr>
        <td class="tdheaderl b-bottom-w" style="<?php echo($style1); ?>;" width=120>Dashboard:</td>
        <td style="<?php echo($style2); ?>;"><select name=ref id=i><option <?php if(!checkIntRef($ref)) { echo("selected"); } ?> value=X>--- SELECT ---</option><?php
        $sql = "SELECT dashid, dashname FROM ".$dbref."_dashboard WHERE dashstatus = 'Y' ORDER BY dashname";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option ");
                if(checkIntRef($ref) && $row['dashid']==$ref) { echo(" selected "); }
                echo("value=".$row['dashid'].">".$row['dashname']." (ref: ".$row['dashid'].")</option>");
            }
        mysql_close($con);
        ?></select></td>
    </tr>
    <tr>
        <td colspan=2 style="<?php echo($style1); ?>;"><input type=button value="Next -->" onclick="Validate();"></td>
    </tr>
</table>
</form>
<?php } else { ?>
<form action=report_process.php method=post onsubmit="return Validate2();" language=jscript>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=25>
        <td class="tdheaderl b-bottom-g" style="<?php echo($style1); ?>;" width=120>Dashboard:</td>
        <td style="<?php echo($style2); ?>;"><?php echo($dash['dashname']." (ref: $ref)"); ?><input type=hidden name=ref value=<?php echo($ref); ?>></td>
    </tr>
</table>
<p><b>Include Update comments? <input type=checkbox value=Y name=updatecomm><br>
Include Report comment? <input type=checkbox value=Y name=reportcomm></p>
<?php if($reportsec[$i]=="Y") { ?>
<h2><?php echo($i); $i++; ?>. Summarise Dashboard</h2>
<?php
        $dirs = getSections("D",$ref);
        $subs = getSections("S",$ref);
        $towns = getSections("T",$ref);
        $wards = getSections("W",$ref);
?>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=25>
        <td class="tdheaderl b-bottom-g" style="<?php echo($style1); ?>;" width=120>Summary Level:</td>
        <td colspan=2 style="<?php echo($style2); ?>;"><select name=summary id=sum onchange="sumChange(this);">
            <option selected value=ALL>Complete</option>
            <?php if(count($dirs)>0) { ?>
				<option value=D>--- Directorate ---</option>
				<?php foreach($dirs as $dir) { 
					echo("<option value='D_".$dir['id']."'>".$dir['txt']."</option>");
				} ?>
			<?php } ?>
            <?php if(count($subs)>0) { ?>
				<option value=S>--- Sub-Directorate ---</option>
				<?php foreach($subs as $itm) { 
					foreach($itm as $it) {
						echo("<option value='S_".$it['id']."'>".$it['txt']."</option>");
					}
				} ?>
			<?php } ?>
            <?php if(count($towns)>0) { ?>
				<option value=T>--- Towns ---</option>
				<?php foreach($towns as $itm) { 
					echo("<option value='T_".$itm['id']."'>".$itm['txt']."</option>");
				} ?>
			<?php } ?>
            <?php if(count($wards)>0) { ?>
				<option value=W>--- Wards ---</option>
				<?php foreach($wards as $itm) { 
					echo("<option value='W_".$itm['id']."'>".$itm['txt']."</option>");
				} ?>
			<?php } ?>
        </select></td>
    </tr>
<!--    <tr>
        <td class="tdheaderl b-bottom-g" rowspan=3 style="<?php echo($style1); ?>;" width=120 valign=top>Filter:</td>
                <td valign=top width=120 style="font-weight: bold; color: #006600;"><?php echo($setup[4][0]); ?>:</td>
                <td><?php
                if(count($dirs)>1)
                {
                ?>
                <select name=filterdir id=fd>
                <?php
                    foreach($dirs as $d)
                    {
                        echo("<option value=".$d['id'].">".$d['txt']."</option>");
                    }
                ?></select>
                <?php
                } else {
                    foreach($dirs as $d)
                    {
                        echo("<input type=hidden name=filterdir id=fd value=".$d['id'].">".$d['txt']);
                    }
                }
                ?></td>
            </tr>
            <tr>
                <td valign=top width=120 style="font-weight: bold; color: #006600;"><?php echo($setup[5][0]); ?>:</td>
                <td><?php
                $items = $subs;
                if(count($items)>1)
                {
                ?>
                <select name=filtersub id=fs>
                <?php
                    foreach($items as $im)
                    {
                        foreach($im as $d) {
                            echo("<option value=".$d['id'].">".$d['txt']."</option>");
                        }
                    }
                ?></select>
                <?php
                } else {
                    foreach($items as $im)
                    {
                        foreach($im as $d) {
                            echo("<input type=hidden name=filtersub id=fs value=".$d['id'].">".$d['txt']);
                        }
                    }
                }
                ?></td>
            </tr>
            <tr>
                <td valign=top width=120 style="font-weight: bold; color: #006600;">Frame:</td>
                <td><?php
                $items = array();
        foreach($towns as $t) { $items[] = $t; }
        foreach($wards as $t) { $items[] = $t; }
                if(count($items)>1)
                {
                ?>
                <select name=filterframe id=ff>
                <option selected value=ALL>All</option>
                <?php
                    foreach($items as $d)
                    {
                        echo("<option value=".$d['dfid'].">".$d['txt']."</option>");
                    }
                ?></select>
                <?php
                } else {
                    foreach($items as $d)
                    {
                        echo("<input type=hidden name=filtersub id=fs value=".$d['dfid'].">".$d['txt']);
                    }
                }
                ?></td>
            </tr> -->
</table>
<?php
} else {    //reportsec[2]==n
    echo("<input type=hidden name=summary value=1>");
}
?>
<h2><?php echo($i); $i++; ?>. Select Time Period</h2>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=25>
        <td class="tdheaderl b-bottom-w" style="<?php echo($style1); ?>;" width=120>From:</td>
        <td style="<?php echo($style2); ?>;"><?php
		$tc = 0;
        switch($dash['dashfrequency'])
{
    case 4:     //monthly
        echo("<select name=from>");
        foreach($time as $t)
        {
			$tc++;
			if($tc==1) { $sel = "selected"; } else { $sel = ""; }
            echo("<option $sel value=".$t['id'].">".date("F Y",$t['eval'])."</option>");
        }
        echo("</select>");
        break;
    case 5:     //2-monthly
        $x = 2;
        echo("2 months ending <select name=from>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
				$tc++;
				if($tc==1) { $sel = "selected"; } else { $sel = ""; }
                $tim = $time[$t];
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 6:     //3-monthly
        $x = 3;
        echo("Quarter ending <select name=from>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
				$tc++;
				if($tc==1) { $sel = "selected"; } else { $sel = ""; }
                $tim = $time[$t];
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 7:     //4-monthly
        $x = 4;
        echo("4 months ending <select name=from>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
				$tc++;
				if($tc==1) { $sel = "selected"; } else { $sel = ""; }
                $tim = $time[$t];
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 8:     //6-monthly
        $x = 6;
        echo("6 months ending <select name=from>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
				$tc++;
				if($tc==1) { $sel = "selected"; } else { $sel = ""; }
                $tim = $time[$t];
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 9:     //12-monthly
               $tim = $time[12];
                echo("Year ending ".date("d F Y",$tim['eval'])."<input type=hidden name=from value=12>");
        break;
    default:  //1 Daily,2 Weekly,3 fortnightly
            switch($dash['dashfrequency'])
            {
                case 1:
                    echo("<input type=text name=from id=from size=9 readonly=readonly class=datepicker value=\"".date("d-m-Y",$time[1]['sval'])."\">"); // to <input type=text class=datepicker size=9 readonly=readonly value=\"".date("d-m-Y",$t['sval'])."\" >");
                    break;
                case 2:
                    echo("Week: </td><td>N/A");
                    break;
                case 3:
                    echo("Fortnight: </td><td>N/A");
                    break;
            }
        break;
}

        ?></td>
    </tr>
    <tr height=25>
        <td class="tdheaderl b-bottom-w" style="<?php echo($style1); ?>;" width=120>To:</td>
        <td style="<?php echo($style2); ?>;"><?php
		$tc = 0;
		$tcyn = "N";
		$tc2 = count($time);
        switch($dash['dashfrequency'])
{
    case 4:     //monthly
        echo("<select name=to>");
        foreach($time as $t)
        {
			$tc++;
			if(($tc==$tc2 || ($t['eval']>$today && $t['sval']<$today)) && $tcyn == "N") { $sel = "selected"; $tcyn = 'Y'; } else { $sel = ""; }
            echo("<option $sel value=".$t['id'].">".date("F Y",$t['eval'])."</option>");
        }
        echo("</select>");
        break;
    case 5:     //2-monthly
        $x = 2;
        echo("2 months ending <select name=to>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
				$tc++;
                $tim = $time[$t];
				if(($tc==$tc2 || ($tim['eval']>$today && $tim['sval']<$today)) && $tcyn == "N") { $sel = "selected"; $tcyn = 'Y'; } else { $sel = ""; }
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 6:     //3-monthly
        $x = 3;
        echo("Quarter ending <select name=to>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
				$tc++;
				if(($tc==$tc2 || ($tim['eval']>$today && $tim['sval']<$today)) && $tcyn == "N") { $sel = "selected"; $tcyn = 'Y'; } else { $sel = ""; }
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 7:     //4-monthly
        $x = 4;
        echo("4 months ending <select name=to>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
				$tc++;
				if(($tc==$tc2 || ($tim['eval']>$today && $tim['sval']<$today)) && $tcyn == "N") { $sel = "selected"; $tcyn = 'Y'; } else { $sel = ""; }
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 8:     //6-monthly
        $x = 6;
        echo("6 months ending <select name=to>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
				$tc++;
				if(($tc==$tc2 || ($tim['eval']>$today && $tim['sval']<$today)) && $tcyn == "N") { $sel = "selected"; $tcyn = 'Y'; } else { $sel = ""; }
                echo("<option $sel value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 9:     //12-monthly
               $tim = $time[12];
                echo("Year ending ".date("d F Y",$tim['eval'])."<input type=hidden name=to value=12>");
        break;
    default:  //1 Daily,2 Weekly,3 fortnightly
            switch($dash['dashfrequency'])
            {
                case 1:
                    echo("<input type=text name=to id=to size=9 readonly=readonly class=datepicker value=\"".date("d-m-Y",$time[1]['eval'])."\">"); // to <input type=text class=datepicker size=9 readonly=readonly value=\"".date("d-m-Y",$t['sval'])."\" >");
                    break;
                case 2:
                    echo("Week: </td><td>N/A");
                    break;
                case 3:
                    echo("Fortnight: </td><td>N/A");
                    break;
            }
        break;
}

        ?></td>
    </tr>
    <tr height=25>
        <td class="tdheaderl b-bottom-g" style="<?php echo($style1); ?>;" width=120>Action:</td>
<?php //if ($dash['dashfrequency']==1) { ?><!--        <td style="<?php echo($style2); ?>;"><select name=tpact id=ta onchange="trackTP(this,1);"><option selected value=compare>Compare Data</option><option value=combine>Combine Data</option></select>
            <span id=lev>&nbsp;per&nbsp;<select name=tplevel id=tpl><option selected value=TP>Month</option><option value=D>Day</option></select></span></td> -->
<?php //} else { ?>        <td style="<?php echo($style2); ?>;"><select name=tpact id=ta onchange="trackTP(this,2);"><option selected value=compare>Compare Data per Month</option><option value=combine>Combine Data</option></select></td>
<?php //} ?>
<!--        <td style="<?php echo($style2); ?>;"><input type=hidden name=tpact value=compare>Compare Data per Time Period</td> -->
    </tr>
<!--     <tr height=25>
        <td class="tdheaderl b-bottom-g" style="<?php echo($style1); ?>;" width=120>View:</td>
       <td style="<?php echo($style2); ?>;"><select name=tpview><option selected value=p>Portrait</option><option value=l>Landscape</option></select></td>
    </tr> -->
</table>
<h2><?php echo($i); $i++; ?>. Select Graph</h2>
<h3>Select Graph Type</h3>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <td width=30 style="text-align:center"><input type=radio name=graph value=no checked onclick="graphYN('N');" id=no></td>
        <td><label for=no>No graph</label></td>
    </tr>
    <tr>
        <td style="text-align:center" valign=top><input type=radio name=graph value=line id=line onclick="graphYN('Y');"></td>
        <td><label for=line>Line Graph</label>
            <br>
            <img src=lib/images/line.gif id=linepic onclick="document.getElementById('line').checked = true; graphYN('Y'); ">
        </td>
    </tr>
    <tr>
        <td style="text-align:center" valign=top><input type=radio name=graph value=bar id=bar onclick="graphYN('Y');"></td>
        <td><label for=bar>Bar Graph</label>
            <br>
            <img src=lib/images/bar.gif id=barpic onclick="document.getElementById('bar').checked = true; graphYN('Y'); ">
        </td>
    </tr>
    <tr>
        <td style="text-align:center" valign=top><input type=radio name=graph value=pie id=pie onclick="graphYN('Y');"></td>
        <td><label for=pie>Pie Chart</label>
            <br>
            <img src=lib/images/pie.gif id=piepic onclick="if(document.getElementById('pie').disabled==false) { document.getElementById('pie').checked = true; graphYN('Y'); }">
        </td>
    </tr>
</table>
<span id=graphdata>
<h3>Select Graph Data</h3>
<p style="margin-top: -10px;"><i>Note: You may select a maximum of 5 data points to graph.</i></p>
<?php
$fields = getFields($ref);
$records = getRecords($ref);
$vars = getVariables($ref);
?>
<table cellpadding=5 cellspacing=0>
    <tr height=20>
        <td>&nbsp;</td>
        <?php
        foreach($fields as $fld)
        {
            echo("<td class=tdheader>".$fld['fldtxt']."</td>");
        }
        ?>
    </tr>
    <?php
    foreach($records as $rec)
    {
        $r = $rec['recid'];
        echo("<tr>");
        echo("<td class=level1>".$rec['rectxt']."</td>");
        foreach($fields as $fld)
        {
            $f = $fld['fldid'];
            switch($vars[$r][$f]['vartype'])
            {
                case "NUM":
                    echo("<td style=\"text-align:center;\"><input type=checkbox value=Y name=\"gd_".$r."_".$f."\"> ".$vars[$r][$f]['varunit']."</td>");
                    break;
                case "PERC":
                    echo("<td style=\"text-align:center;\"><input type=checkbox value=Y name=\"gd_".$r."_".$f."\">%</td>");
                    break;
                case "R":
                    echo("<td style=\"text-align:center;\">R <input type=checkbox value=Y name=\"gd_".$r."_".$f."\"></td>");
                    break;
                default:
                    echo("<td style=\"text-align:center;\"></td>");
                    break;
            }
        }
        echo("</tr>");
    }
    ?>
</table>
</span>
<h2><?php echo($i); $i++; ?>. Select the report output method:</h2>
<table cellpadding=3 cellspacing=0 width=600 class=noborder>
	<tr>
		<td width=30 class=noborder><input type="radio" name="output" value="display" checked id=csvn></td>
		<td class="noborder"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=noborder ><input type="radio" name="output" value="csv" id=csvy></td>
		<td class=noborder><label for=csvy>Microsoft Excel file</label></td>
	</tr>
</table>
<h2><?php echo($i); $i++; ?>. Click the button:</h2>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1" id=1>
<input type="reset" value="Reset" name="B2"></p>
</form>
<script type=text/javascript>
//sumChange(document.getElementById('sum'));
//trackTP(document.getElementById('ta'));
graphYN('N');
</script>
<?php } ?>
</body>

</html>
