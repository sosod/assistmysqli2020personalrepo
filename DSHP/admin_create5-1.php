<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreateProgress.php");
$step = "5-1";
$page = array("next"=>"5-2","step"=>"1","title"=>$process[5][1]['txt']);

$r0 = $variables['r0'];
$r1 = $variables['r1'];
$ref = $variables['ref'];

if(strlen($r0)>0 && strlen($r1)>0)
{
    $result[0] = $r0;
    $result[1] = $r1;
    $result[2] = $ref;
}

?>
<script type=text/javascript src="lib/AdminCreateVariables.js"></script>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 5.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);


echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
echo("<form name=frm id=frm action=\"admin_create5-2.php\" method=post>");
echo("<input type=hidden name=step value=\"5-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");
?><div align=center><?php

previewKPI();
?>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type=submit value="Next -->"></td>
	</tr>
</table>
</form>
<?php

        $stepprogress = setProgress(5);
        $totalprogress = setProgress(0);
        displayProgress("Step 5 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
