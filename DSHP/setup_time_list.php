<?php
include("inc_head_setup.php");
include("inc/setup.php");
?>
<script type=text/javascript>
function closeTP(t,txt) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        document.location.href = "setup_time_configure.php?t="+t;
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Time Periods ~ Update</b></h1>
<?php displayResult($result); ?>
<p>Auto Closure dates which fall within the next 7 days are highlighted in <span style="color: #009900; font-weight: bold;">green</span>.</p>
<table cellpadding=3 cellspacing=0 width=650>
	<tr>
		<th width=20>&nbsp;</td>
		<th height=27>Time period</td>
		<th width=60>Status</td>
		<th>Auto Reminder</td>
		<th>Auto Closure</td>
		<th>&nbsp;</td>
	</tr>
	<?php
	$t = 0;
    $sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ORDER BY sval, eval";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
        while($row = mysql_fetch_array($rs))
        {
            $t++;
            $tval = date("d F Y", $row['sval'])." - ".date("d F Y", $row['eval']);
            $tclose = $row['aclose'] - $today;
            if($tclose > -1 && $tclose < 604800)
            {
                $tclose = "<span style=\"color: #009900; font-weight: bold;\">";
            }
            else
            {
                $tclose = "";
            }
    ?>
	<?php include("inc_tr.php"); ?>
		<th height=27 align=center <?php if($t==$mnr) { echo(" style=\"border-bottom: 1px solid #ababab;\""); } ?>><?php echo($t); ?></td>
		<td class=tdgeneral style="padding-left: 5px"><?php echo($tval); ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") {
                if($row['sval']<$today) {
                    echo("Open");
                } else {
                    echo("&nbsp;");
                }
            } else {
                echo("Closed*");
            }
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['arem']>$today && $row['active']=="Y") { echo(date("d M y (D)",$row['arem'])); } else { echo("N/A"); }
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['aclose']>$today && $row['active']=="Y") { echo($tclose.date("d M y (D)",$row['aclose'])."</span>"); } else { echo("N/A"); }
        ?></td>
		<td class=tdgeneral align=center width=50><?php if($row['active']=="Y") { ?><input type=button value=Update onclick="closeTP(<?php echo($row['id']); ?>,'<?php echo($tval); ?>')"><?php } ?></td>
	</tr>
	<?php
        }
    mysql_close();
    ?>
</table>
<!-- <p style="font-size: 7.5pt; margin-top: 5px;"><i> *Note: Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.</i></p> -->
<?php
$urlback = "setup_time.php";
include("inc_goback.php");

displayChangeLog("S_TIME","X",$urlback);

?>
<p>&nbsp;</p>
</body>
</html>
