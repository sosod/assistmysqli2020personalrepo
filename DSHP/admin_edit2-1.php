<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEditProgress.php");

$page = array("next"=>"2","step"=>"1","title"=>"Records");
$step = "2-1";

$variables = $_REQUEST;
$r0 = $variables['r0'];
$r1 = $variables['r1'];
$ref = $variables['ref'];

if(strlen($r0)>0 && strlen($r1)>0)
{
    $result[0] = $r0;
    $result[1] = $r1;
    $result[2] = $ref;
}

?>
<script type=text/javascript>
function goToList(ref) {
    var lid = document.getElementById('listid').value;
    if(lid != "X")
    {
        if(!isNaN(parseInt(lid)))
        {
            var url = "admin_edit2-2.php?";
            url = url + "stepact=save";
            url = url + "&step=2-1";
            url = url + "&ref="+ref;
            url = url + "&listid="+lid;
            document.location.href = url;
        }
    }
}

function skipStep(ref) {
            var url = "admin_edit_process.php?";
            url = url + "stepact=save";
            url = url + "&step=2-4";
            url = url + "&ref="+ref;
            url = url + "&oldstep=Y";
            url = url + "&acttype=Y";
            url = url + "&act=move";
            document.location.href = url;
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 2.".$page['step'].": Setup ".$page['title']."</h2>");

if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    echoDashboardTitle($ref,$dash['dashname'],$step);
echo("<form name=frm id=frm onsubmit=\"return Validate(this);\" action=\"admin_edit2-3.php\" method=post>");
echo("<input type=hidden name=step value=\"2-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");
?>
<table cellpadding=3 cellspacing=0 class=noborder><tr><td align=center class=noborder>
<p><input type=button value="Manually create records" onclick="document.location.href = 'admin_edit2-3.php?next=3&ref=<?php echo $ref; ?>&step=2-1&stepact=save';"></p>
<p>OR</p>
<p><select id=listid>
<option selected value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE yn = 'Y' AND id IN (SELECT listid FROM ".$dbref."_list_dropdowns_values WHERE yn = 'Y')";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        echo("<option value=".$row['id'].">".$row['value']."</option>");
    }
mysql_close($con);
?>
</select><br><input type=button value="Use predefined list" onclick="goToList(<?php echo $ref; ?>);"></p>
<p>OR</p>
<p><input type=button value="Skip Step" onclick="skipStep(<?php echo $ref; ?>);"></p>
</td></tr></table>
<?php
        $stepprogress = setProgress(2);
        $totalprogress = setProgress(0);
        displayProgress("Step 2 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
