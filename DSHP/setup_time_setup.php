<?php
    include("inc_head_setup.php");
    include("inc/setup.php");
?>
<script type=text/javascript>
function previewDates() {
    var c = document.getElementById('c').value;
    var r = document.getElementById('r').value;
    document.location.href = "setup_time_setup.php?c="+c+"&r="+r;
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Time Periods ~ Setup</b></h1>
<?php displayResult($result); ?>
<?php
$c = $_GET['c'];
$r = $_GET['r'];
if(strlen($c)>0 && is_numeric($c))
{
    $ac = $c;
}
else
{
    $ac = 14;
}
if(strlen($r)>0 && is_numeric($r))
{
    $ar = $r;
}
else
{
    $ar = 7;
}
?>
<form name=update method=get action=setup_time_list.php>
<input type=hidden name=a value=time><input type=hidden name=act value=auto>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdheaderl style="border-bottom: 1px solid #ffffff;" height=27 width=150>Use Auto Reminder?:</td>
        <td class=tdgeneral><select name=ryn><?php echo(selectYesNo("Y")); ?></select></td>
    </tr>
    <tr>
        <td class=tdheaderl style="border-bottom: 1px solid #ffffff;" height=27 width=150>Email Reminder: </td>
        <td class=tdgeneral><input type=text name=r size=5 value=<?php echo($ar); ?> id=r> days after the last day of the month.</td>
    </tr>
    <tr>
        <td class=tdheaderl style="border-bottom: 1px solid #ffffff;" height=27 width=150>Use Auto Close?:</td>
        <td class=tdgeneral><select name=cyn><?php echo(selectYesNo("Y")); ?></select></td>
    </tr>
    <tr>
        <td class=tdheaderl style="border-bottom: 1px solid #ffffff;" height=27 width=150>Close Time Periods: </td>
        <td class=tdgeneral><input type=text name=c size=5 value=<?php echo($ac); ?> id=c> days after the last day of the month.</td>
    </tr>
    <tr>
        <td class=tdheader width=150>&nbsp;</td>
        <td class=tdgeneral><input type=button value="Preview Dates" onclick="previewDates()"> <input type=submit value="Update Time Periods"></td>
    </tr>
</table>
</form>
<p style="font-size: 7.5pt; margin-top: -12px;"><i>Note: <br>All auto closure of Time Periods occur at 01:00 on the date given.<!-- <br>Once a time period is closed you will need to contact Ignite Advisory Services to reopen it. --></i></p>
<?php
if(strlen($c)>0 && is_numeric($c) && strlen($r)>0 && is_numeric($r))
{
?>
<h2 class=fc>Preview of dates</h2>
<p style="font-size: 7.5pt; margin-top: 5px;"><i>Dates for individual Time Periods can be edited from the Time Period list page.</i></p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<th width=20>&nbsp;</td>
		<th height=27>Time period</td>
		<th width=50>Active</td>
		<th >Reminder</td>
		<th >Closure</td>
	</tr>
	<?php
	$aclose = $c * 86400;
	$arem = $r * 86400;
	$t = 0;
    $sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ORDER BY sval, eval";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
        while($row = mysql_fetch_array($rs))
        {
            $t++;
            $tval = date("d F Y", $row['sval'])." - ".date("d F Y", $row['eval']);
    ?>
	<?php include("inc_tr.php"); ?>
		<td class=tdheader height=27 align=center <?php if($t!=$mnr) { echo("style=\"border-bottom: 1px solid #ffffff;\""); } ?>><?php echo($t); ?></td>
		<td class=tdgeneral style="padding-left: 5px"><?php echo($tval); ?></td>
		<td class=tdgeneral align=center><?php
            echo(YesNo($row['active'],"Y"));
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") { echo(date("d M Y (D)",$row['eval']+$arem)); } else { echo("N/A"); }
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") { echo(date("d M Y (D)",$row['eval']+$aclose)); } else { echo("N/A"); }
        ?></td>
	</tr>
	<?php
        }
    mysql_close();
    ?>
</table>
<?php } ?>
<?php
$urlback = "setup_time.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
