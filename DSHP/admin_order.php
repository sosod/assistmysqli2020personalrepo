<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php
include("inc_head.php");
include("inc/Admin.php");

$type = $variables['type'];
$ref = $variables['ref'];
$src = $variables['src'];
switch($type)
{
    case "REC":
        $items = getRecords($ref);
        $id = "recid";
        $txt = "rectxt";
        break;
    case "FLD":
        $items = getFields($ref);
        $id = "fldid";
        $txt = "fldtxt";
        break;
    default:
        die("<P>An error has occurred.  Please go back and try again.</p>");
        break;
}
?>
<h1><?php echo($modtitle); ?>: Admin - Display Order</h1>
<p>To change the order in which the Directorates display, click and drag each Directorate into it's desired position.</p>
	<form id=frm action="admin_order_process.php" method="post">
<?php
$display = array();
$d = 0;
    foreach($items as $row)
    {
        $display[$d] = $row;
        $d++;
    }
	
	
	
	echo "<div style=\"margin-top: 20px;\"><ul id=sortable>";
        foreach($display as $dy)
        {
			echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$dy[$id]."\">".$dy[$txt]."</li>";
        }
	echo "</ul></div>";
	
         ?>
	
        <input type=hidden name=src value=<?php echo($src); ?>>
        <input type=hidden name=type value=<?php echo($type); ?>>
        <input type=hidden name=ref value=<?php echo($ref); ?>>
        <p><input type="submit"  value="Save Changes" class=isubmit /></p>
    </form>
<script type=text/javascript>
$(document).ready(function() {
	$("#sortable").sortable({ placeholder: "ui-state-info" });
	$("#sortable").disableSelection();
});
</script>
<?php
include("inc_goback_history.php");
?>	</body>
</html>
