<?php
include("inc_head.php");
include("inc/Admin.php");
$variables = $_REQUEST;
$ref = $variables['ref'];
if(!checkIntRef($ref)) {
    echoError();
} else {

$users = array();
        $sql2 = userList($type,0,"A");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $users[] = $row2;
            }
        mysql_close($con2);

?>
<h1><b><?php echo($modtitle); ?>: Admin - User Responsibility Report</b></h1>
<form action=admin_edit_users_process.php method=POST>
<input type=hidden name=ref value=<?php echo($ref); ?>><input type=hidden name=dash value=<?php echo($variables['dash']); ?>>
<div align=center>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <th>Frame Type</th>
        <th>Frame Name</th>
        <th>Primary Capturer</th>
        <th>Secondary Capturer</th>
    </tr>
<?php
    $ftype = array(
        array("D","Directorate"),
        array("S","Sub-Directorate"),
        array("T","Town"),
        array("W","Ward")
    );

$sql = "SELECT d.*, tk.tkname, tk.tksurname FROM ".$dbref."_dashboard d, assist_".$cmpcode."_timekeep tk WHERE dashstatus = 'Y' AND d.dashowner = tk.tkid AND dashid = $ref ORDER BY d.dashname";
include("inc_db_con.php");
while($dash = mysql_fetch_array($rs))
{
    $ref = $dash['dashid'];
    $dashowner = $dash['tkname']." ".$dash['tksurname'];
    $dashname = $dash['dashname'];
    $row = 0;
?>
    <tr>
        <?php
    foreach($ftype as $type)
    {
        $sections = getSections($type[0],$ref);
        switch($type[0])
        {
            case "D":
                $class = "class=level1 style=\"padding-left: 3px;\"";
                break;
            case "S":
                if($type[0]=="S") {
                    $sections2 = $sections;
                    $sections = array();
                    foreach($sections2 as $sec)
                    {
                        foreach($sec as $s) {
                            $sections[] = $s;
                        }
                    }
                }
                $class = "class=level2 style=\"padding-left: 3px;\"";
                break;
            case "T":
                $class = "class=level3 style=\"padding-left: 3px;\"";
                break;
            case "W":
                $class = "class=level3 style=\"padding-left: 3px;\"";
                break;
            default:
                $class = "";
                break;
        }
        $admins = getDashAdmins($type[0],$ref);
        foreach($sections as $sec)
        {
            if($row>0) { ?>
                </tr>
                <tr>
            <?php
            }
            $primary = $admins[$sec['dfid']]['Y'];
            $secondary = $admins[$sec['dfid']]['N'];
            if(count($primary)>0) {
                $prim = $primary['tkid'];
            } else {
                $prim = "X";
            }
//            if($primary['tkname']." ".$primary['tksurname'] == "Ignite Support") { $prim = "X"; }
            if(count($secondary)>0) {
                $secon = $secondary['tkid'];
            } else {
                $secon = "X";
            }
            //if($secondary == "Ignite Support") { $secondary = ""; }
            ?>
            <td <?php echo($class); ?>><?php echo($type[1]."<input type=hidden name=ftype[] value=\"".$type[0]."\">"); ?></td>
            <td><?php echo($sec['txt']."<input type=hidden name=dfid[] value=\"".$sec['dfid']."\">"); ?></td>
            <td><select name=primary[]>
                <option <?php if($prim=="X") { echo("selected"); } ?> value=X>--- SELECT ---</option>
                <option value=N>N/A</option>
                <?php
                foreach($users as $usr)
                {
                    echo(chr(10)."<option ");
                    if($prim == $usr['tkid']) { echo(" selected "); }
                    echo("value=".$usr['tkid'].">".$usr['tkname']." ".$usr['tksurname']."</option>");
                }
                ?>
            </select></td>
            <td><select name=secondary[]>
                <option <?php if($secon=="X") { echo("selected"); } ?> value=X>--- SELECT ---</option>
                <option value=N>N/A</option>
                <?php
                foreach($users as $usr)
                {
                    echo(chr(10)."<option ");
                    if($secon == $usr['tkid']) { echo(" selected "); }
                    echo("value=".$usr['tkid'].">".$usr['tkname']." ".$usr['tksurname']."</option>");
                }
                ?>
            </select></td>
            <?php
            $row++;
        }
    }
        ?>
    </tr>
<?php
}
mysql_close($con);
?>
</table>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type="submit" value="Update"> <input type=reset value=Reset> <input type=button value="Cancel" onclick="document.location.href='admin_edit.php';"></td>
	</tr>
</table>
</form>
</div>
<?php
}   //checkintref
?>

</body>

</html>
