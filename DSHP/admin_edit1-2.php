<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEdit1.php");
include("inc/AdminEditProgress.php");

$list = "SELECT dirid id, dirtxt value FROM assist_".$cmpcode."_list_dir WHERE diryn = 'Y' ORDER BY dirsort";
$page = array("next"=>"3","step"=>"2","title"=>"Directorates");

$type = "D";
$admins = array();
$a = 0;
        $sql2 = userList($type,0,"Lb");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $id2 = "a_".$row2['id'];
                $val2 = $row2['tkname']." ".$row2['tksurname'];
                $r2 = $row2['ref'];
                $admins[$r2][] = array("i" => $id2, "t" => $row2['tkid'], "v" => $val2); //.="<option value=$id2> $val2 </option>";
            }
        mysql_close($con2);
$users = array();
        $sql2 = userList($type,0,"A");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $id2 = "u_".$row2['tkid'];
                $val2 = $row2['tkname']." ".$row2['tksurname'];
                //$users.="<option value=$id2> $val2 </option>";
                $users[] = array("i" => $id2, "t" => $row2['tkid'], "v" => $val2); //.="<option value=$id2> $val2 </option>";
            }
        mysql_close($con2);

//echo("<P>-"); print_r($admins); echo("-</p>");
?>

<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 1.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(checkIntRef($ref)) { $dash = getDashboard($ref); } else { echoError(); }
    if($dash['dashdir']<0)
    {
        echo("<script type=text/javascript>");
        echo("document.location.href = 'admin_edit1-".$page['next'].".php?ref=".$ref."';");
        echo("</script>");
    }
    else
    {
$sections = getSections("D",$ref);
$sectionadmins = getDashAdmins("D",$ref);
//print_r($sectionadmins);
$items = array();
foreach($sections as $sec)
{
    $dfid = $sec['dfid'];
    $id = $sec['id'];
    $items[$id]['yn'] = "Y";
    $items[$id]['id'] = $id;
    $items[$id]['dfid'] = $dfid;
    $items[$id]['pa'] = $sectionadmins[$dfid]['Y']['tkid'];
    $items[$id]['sa'] = $sectionadmins[$dfid]['N']['tkid'];
}
//print_r($items);

echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
//    echoDashboardTitle($ref,$dash['dashname']);
echo("<form id=frm onsubmit=\"return Validate(this);\" action=\"admin_edit1-".$page['next'].".php\" method=post>");
echo("<input type=hidden name=step value=\"1-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>"); ?>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <th><?php echo($page['title']); ?></th>
        <th>Applicable?</th>
        <th>Primary Admin</th>
        <th>Secondary Admin</th>
    </tr>
    <?php
    $sql = $list;
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['id'];
            $disp = $row['value'];
            $chkname = "yn_".$id;
            $admin1 = "pa_".$id;
            $admin2 = "sa_".$id;
            $sec = $items[$id];
//            print_r($sec);
            ?>
    <tr>
        <td><?php echo("$disp <input type=hidden name=id[] value=$id>"); ?></td>
        <td style="text-align:center;"><?php echo("<input type=checkbox name=$chkname "); if($sec['yn']=="Y") { echo(" checked "); } echo(" value=Y>"); ?></td>
        <td style="text-align:center;"><?php echo("<select name=$admin1>"); $sel = "N";
        if(count($admins[$id])>0) {
            echo("<option selected value=X>--- ADMINS ---</option>");
            $admn = $admins[$id];
            foreach($admn as $adm)
            {
                echo("<option ");
                if($adm['t']==$sec['pa']) { echo(" selected "); $sel="Y"; }
                echo(" value=".$adm['i'].">".$adm['v']."</option>");
            }
            echo("<option value=X>--- ALL USERS ---</option>");
        } else {
            echo("<option selected value=X>--- SELECT ---</option>");
        }
//        echo($users);
            foreach($users as $usr)
            {
                echo("<option ");
                if($usr['t']==$sec['pa'] && $sel=="N") { echo(" selected "); $sel="Y"; }
                echo(" value=".$usr['i'].">".$usr['v']."</option>");
            }
        echo("</select>"); ?></td>
        <td style="text-align:center;"><?php echo("<select name=$admin2>"); $sel = "N";
        if(count($admins[$id])>0) {
            echo("<option selected value=X>--- ADMINS ---</option>");
            $admn = $admins[$id];
            foreach($admn as $adm)
            {
                echo("<option ");
                if($adm['t']==$sec['sa']) { echo(" selected "); $sel="Y"; }
                echo(" value=".$adm['i'].">".$adm['v']."</option>");
            }
            echo("<option value=X>--- ALL USERS ---</option>");
        } else {
            echo("<option selected value=X>--- SELECT ---</option>");
        }
//        echo($users);
            foreach($users as $usr)
            {
                echo("<option ");
                if($usr['t']==$sec['sa'] && $sel=="N") { echo(" selected "); $sel="Y"; }
                echo(" value=".$usr['i'].">".$usr['v']."</option>");
            }
        echo("</select>"); ?></td>
    </tr>
            <?php
        }
    mysql_close($con);
    ?>
</table>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type="submit" value="Next -->"></td>
	</tr>
</table>
</form>
<?php
        $stepprogress = setProgress(1);
        $totalprogress = setProgress(0);
        displayProgress("Step 1 Process",$stepprogress,$totalprogress);
    }   //endif dashdir < 0
 ?>
<P>&nbsp;</p>
</body>
</html>
