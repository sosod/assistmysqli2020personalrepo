<?php
    include("inc_ignite.php");

$fdata = "";
$fdata.= "\"User ID\",\"User\",\"User Access\",\"\",\"\",\"\",\"\",\"Directorates\",\"Sub-Directorates\"";
if($setup[2][0]=="Y") { $fdata.=",\"".$setup[3][0]."\""; }
if($setup[1][0]=="Y") { $fdata.=",\"Towns\""; }
$fdata.= "\r\n";
$fdata.= "\"\",\"\",\"Dashboard Admin\",\"Create\",\"Capture\",\"View\",\"Report\"\r\n";

$sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u ";
$sql.= "WHERE t.tkid = u.usrtkid AND u.usrmodref = '".$modref."' AND t.tkstatus = 1 AND t.tkid <> '0000'";
$sql.= " ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $tid = $row['tkid'];
        //USER SETUP
        $umnr = 0;
        $urow = array();
        $sql2 = "SELECT * FROM ".$dbref."_list_users WHERE tkid = '".$tid."' AND yn = 'Y' ORDER BY id DESC LIMIT 1";
        include("inc_db_con2.php");
            $umnr = mysql_num_rows($rs2);
            $urow = mysql_fetch_array($rs2);
        mysql_close($con2);

        if($umnr>0) {
            $fdata.= "\" ".$row['tkid']."\"";
            $fdata.= ",\"".decode($row['tkname']." ".$row['tksurname'])."\"";
            $fdata.=",\"".YesNo($urow['dash'],"No")."\"";
            $fdata.=",\"".YesNo($urow['activate'],"No")."\"";
            $fdata.=",\"".YesNo($urow['capture'],"No")."\"";
            $fdata.=",\"".YesNo($urow['view'],"No")."\"";
            $fdata.=",\"".YesNo($urow['report'],"No")."\"";
        } else {
            $fdata.= "\" ".$row['tkid']."\"";
            $fdata.= ",\"".decode($row['tkname']." ".$row['tksurname'])."\"";
            $fdata.= ",\"User setup not yet completed\"";
            $fdata.= ",\"\"";
            $fdata.= ",\"\"";
            $fdata.= ",\"\"";
            $fdata.= ",\"\"";
            $fdata.= ",\"\"";
        }
        //DIRECTORATES
            $c = 0;
            $fdata.=",\"";
            $row2 = array();
            $val = "";
            $sql2 = "SELECT d.* FROM assist_".$cmpcode."_list_dir d, ".$dbref."_list_admins a WHERE a.tkid = '$tid' AND a.type = 'D' AND a.yn = 'Y' AND d.dirid = a.ref AND d.diryn = 'Y' ORDER BY d.dirsort, d.dirtxt";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $c++;
                    $v = "$c. ".decode($row2['dirtxt']);
                    if(strlen($val)>0) { $val.=chr(10); }
                    $val.=$v;
                }
            mysql_close($con2);
            $fdata.=$val."\"";
        //SUB-DIRECTORATES
            $c = 0;
            $fdata.=",\"";
            $row2 = array();
            $val = "";
            $sql2 = "SELECT l.* FROM assist_".$cmpcode."_list_dirsub l, assist_".$cmpcode."_list_dir d, ".$dbref."_list_admins a WHERE a.tkid = '$tid' AND a.type = 'S' AND a.yn = 'Y' AND l.subyn = 'Y' AND l.subid = a.ref AND l.subdirid = d.dirid AND d.diryn = 'Y' ORDER BY d.dirsort, l.subsort, l.subtxt";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $c++;
                    $v = "$c. ".decode($row2['subtxt']);
                    if(strlen($val)>0) { $val.=chr(10); }
                    $val.=$v;
                }
            mysql_close($con2);
            $fdata.=$val."\"";
        //WARDS
        if($setup[2][0]=="Y")
        {
            $c = 0;
            $fdata.=",\"";
            $row2 = array();
            $val = "";
            $sql2 = "SELECT l.* FROM ".$dbref."_list_wards l, ".$dbref."_list_admins a WHERE a.tkid = '$tid' AND a.type = 'W' AND a.yn = 'Y' AND l.yn = 'Y' AND l.id = a.ref ORDER BY l.value";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $c++;
                    $v = "$c. ".decode($row2['value']);
                    if(strlen($val)>0) { $val.=chr(10); }
                    $val.=$v;
                }
            mysql_close($con2);
            $fdata.=$val."\"";
        }
        //TOWNS
        if($setup[1][0]=="Y")
        {
            $c = 0;
            $fdata.=",\"";
            $row2 = array();
            $val = "";
            $sql2 = "SELECT l.* FROM ".$dbref."_list_towns l, ".$dbref."_list_admins a WHERE a.tkid = '$tid' AND a.type = 'T' AND a.yn = 'Y' AND l.yn = 'Y' AND l.id = a.ref ORDER BY l.value";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $c++;
                    $v = "$c. ".decode($row2['value']);
                    if(strlen($val)>0) { $val.=chr(10); }
                    $val.=$v;
                }
            mysql_close($con2);
            $fdata.=$val."\"";
        }
        $fdata.= "\r\n";
    }
    mysql_close();

        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/dashboard_user_report_".date("Ymd_Hi",$today).".csv";
        $newfilename = "dashboard_user_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);

?>
