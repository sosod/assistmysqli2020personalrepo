<?php
/*******
     MANUALS v3
Created by: Janet Currie
Created: September 2017
***************************/



class MAN_SECTIONS extends MAN_HELPER {


	private $mid;
	private $sid;
	private $hid;


	public function __construct() {
		parent::__construct("master");
	}


	public function getSectionsForModule($mod_id) {
		$sql = "SELECT sec_id as id
					, sec_name as name
					, sec_comment as comment
					, sec_code as code
					, sec_parent_id as parent_id
					, sec_order as display
					, sec_location as location
					, IF(sec_status = ".self::ACTIVE.", true, false) as active
					, IF(sec_status = ".self::NOT_APPLICABLE.", true, false) as not_app
				FROM man_section
				WHERE sec_mod_id = ".$mod_id."
				AND sec_status <> ".self::INACTIVE."
				ORDER BY sec_location, sec_parent_id, sec_order";
		return $this->mysql_fetch_all_by_id2($sql,"location","id");
	}








	public function __destruct() {
		parent::__destruct();
	}


}


?>