<?php
/*******
     MANUALS
Created by: Janet Currie & Duncan Cosser
Created: May 2014
***************************/



class MAN_SETUP extends ASSIST_HELPER {

	private $db;
	
	public function __construct() {
		parent::__construct();
		$this->db = new ASSIST_DB("master");
	}


	public function getModulesList() {
		$sql = "SELECT M.*, count(MM.id) as c FROM man_modules M LEFT JOIN man_mega MM ON M.id = MM.mid GROUP BY M.id  ORDER BY M.module_name";
		return $this->db->mysql_fetch_all($sql);
	}
	
	public function getSectionsList() {
		$sql = "SELECT M.*, count(MM.id) as c FROM man_section M LEFT JOIN man_mega MM ON M.id = MM.sid GROUP BY M.id  ORDER BY M.section_name";
		return $this->db->mysql_fetch_all($sql);
	}
	
	public function getHeadingsList() {
		$sql = "SELECT M.*, count(MM.id) as c FROM man_headings M LEFT JOIN man_mega MM ON M.id = MM.hid GROUP BY M.id  ORDER BY M.heading_name";
		return $this->db->mysql_fetch_all($sql);
	}
	
	
	
	public function addObject($act,$var) {
		switch($act) {
			case "mod": $table = "man_modules"; $r = "Module"; break;
			case "sec": $table = "man_section"; $r = "Section"; break;
			case "head": $table = "man_headings"; $r = "Heading"; break;
		}
		$sql = "INSERT INTO ".$table." SET ";
		$s = array();
		foreach($var as $key => $v) { $s[] = $key." = '".$v."'"; }
		$sql.= implode(", ",$s);
		$this->db->db_insert($sql);
		return array("ok","New ".$r." saved successfully.");
	}
	
	
	public function editObject($act,$id,$var) {
		switch($act) {
			case "mod": $table = "man_modules"; $r = "Module"; break;
			case "sec": $table = "man_section"; $r = "Section"; break;
			case "head": $table = "man_headings"; $r = "Heading"; break;
		}
		$sql = "UPDATE ".$table." SET ";
		$s = array();
		foreach($var as $key => $v) { $s[] = $key." = '".$v."'"; }
		$sql.= implode(", ",$s)." WHERE id = ".$id;
		$this->db->db_update($sql);
		return array("ok","Updated ".$r." saved successfully.");
	}
	
	public function __destruct() {
		parent::__destruct();
	}


}


?>