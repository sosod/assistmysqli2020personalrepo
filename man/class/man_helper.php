<?php
/*******
     MANUALS v3
Created by: Janet Currie
Created: September 2017
***************************/



class MAN_HELPER extends ASSIST_MODULE_HELPER {

	protected $db;


	const ACTIVE = 2;			//available to users
	const NOT_APPLICABLE = 4;	//section not applicable to module
	const INACTIVE = 8;			//not available
	const UNDER_DEVELOPMENT = 16;	//display under development warning
	const MODULE_ARCHIVED = 32;	//module no longer supported
	const MODULE_HAS_NEW_VERSION = 64;	//module version has been replaced
	const ADMIN_MODULE = 128;	//only accessible by admin super user
	const NEW_MODULE_ALLOWED = 256;	//can be added to a client database

	public function __construct($db_type="master") {
		parent::__construct($db_type);
	}






	public function __destruct() {
		parent::__destruct();
	}


}


?>