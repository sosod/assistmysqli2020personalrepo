<?php
/*******
     MANUALS v3
Created by: Janet Currie
Created: September 2017
***************************/



class MAN_MANUALS extends MAN_HELPER {


	private $mid;
	private $sid;
	private $hid;



	public function __construct() {
		parent::__construct("master");
	}



	public function getModulesWithManuals() {
		$sql = "SELECT DISTINCT M.*
		FROM man_modules M
		INNER JOIN man_section S ON S.sec_mod_id = M.mod_id
		INNER JOIN man_manuals MN ON MN.man_sec_id = S.sec_id
		WHERE (M.mod_status & ".self::ACTIVE.") = ".self::ACTIVE." AND S.sec_status = 2 AND MN.man_status = 2
		ORDER BY M.mod_generic_name"; //echo $sql;
		return $this->mysql_fetch_all_by_id($sql, "mod_loc");
	}


	public function getManualsForModule($mod_id) {
		$sql = "SELECT MN.man_id as id
				, MN.man_sec_id as sec_id
				, MN.man_title as name
				, MN.man_comment as comment
				, MN.man_toc as toc
				, MN.man_order as display
				, MN.man_link as document
				, MN.man_insertdate as add_date
				FROM man_manuals MN
				INNER JOIN man_section S
					ON S.sec_id = MN.man_sec_id
				WHERE MN.man_status = 2 AND S.sec_status = 2 AND S.sec_mod_id = ".$mod_id."
				AND MN.man_link NOT LIKE '%demo.pdf%'
				ORDER BY S.sec_id, MN.man_order";
		return $this->mysql_fetch_all_by_id2($sql, "sec_id", "id");
	}




	public function __destruct() {
		parent::__destruct();
	}


}


?>