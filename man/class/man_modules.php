<?php
/*******
     MANUALS v3
Created by: Janet Currie
Created: September 2017
***************************/



class MAN_MODULES extends MAN_HELPER {


	private $mid;
	private $sid;
	private $hid;

	private $module_status = array(
		2=>"Current Modules",
		4=>"Archived Modules",
		8=>"Deleted Modules",
		16=>"In Development",
	);

	public function __construct() {
		parent::__construct("master");
	}


	public function getModuleDetails($modloc) {
		$data = array();

		$sql = "SELECT mod_id as id
				, mod_generic_name as name
				, mod_loc as modloc
				, mod_comment as comment
				, mod_version as version
				, mod_manual_version as manual_version
				, mod_manual_updated as manual_last_updated
				, mod_status
				FROM man_modules WHERE mod_loc = '$modloc'";
		$data['module'] = $this->mysql_fetch_one($sql);

		$mod_status = $data['module']['mod_status'];
		$data['module']['status'] = array(
			'archived'=>(($mod_status & self::MODULE_ARCHIVED) == self::MODULE_ARCHIVED),
			'new_version'=>(($mod_status & self::MODULE_HAS_NEW_VERSION) == self::MODULE_HAS_NEW_VERSION),
			'under_development'=>(($mod_status & self::UNDER_DEVELOPMENT) == self::UNDER_DEVELOPMENT),
		);

		$secObject = new MAN_SECTIONS();
		$sections = $secObject->getSectionsForModule($data['module']['id']);

		$data['sections'] = array(
			'R'=>array(),
			'L'=>array(),
		);

		foreach($sections as $location => $sec) {
			foreach($sec as $sec_id => $s) {
				$data['sections'][$location][$s['parent_id']][$sec_id] = $s;
			}
		}
		return $data;
	}







/*******************************
 * ADMIN FUNCTIONS
 */
	public function getAllModulesForAdmin() {
		$data = array();
		$sql = "SELECT mod_id as id
					, mod_loc as modloc
					, mod_generic_name as name
					, mod_comment as comment
					, mod_status as status
					, mod_version as version
					, mod_manual_version as manual_version
					, mod_manual_updated as manual_last_updated

				FROM man_modules  ORDER BY mod_generic_name";
		return $this->mysql_fetch_all_by_id2($sql, "status","modloc");
	}
	public function getModuleStatusName($status) {
		return $this->module_status[$status];
	}









	public function __destruct() {
		parent::__destruct();
	}


}


?>