<?php
/*******
     MANUALS v3
Created by: Janet Currie
Created: September 2020
***************************/



class MAN_EXTERNAL extends MAN_HELPER {


	private $mid;
	private $sid;
	private $hid;

	private $module_status = array(
		2=>"Current Modules",
		4=>"Archived Modules",
		8=>"Deleted Modules",
		16=>"In Development",
	);

	public function __construct() {
		parent::__construct("master");
	}


	/**
	 * Get ALL modules, regardless of status
	 * @return array
	 */
	private function getAllModuleDetails() {

		$sql = "SELECT mod_id as id
				, mod_generic_name as name
				, mod_loc as modloc
				, mod_comment as comment
				, mod_version as version
				, mod_manual_version as manual_version
				, mod_manual_updated as manual_last_updated
				, mod_status
				FROM man_modules ORDER BY mod_generic_name, mod_loc"; 
		$final_data = $this->mysql_fetch_all_by_id($sql,"modloc");

		foreach($final_data as $mod_loc => $data) {
			$mod_status = $data['mod_status'];
			$final_data[$mod_loc]['status'] = array(
				'archived' => (($mod_status & self::MODULE_ARCHIVED) == self::MODULE_ARCHIVED),
				'has_new_version' => (($mod_status & self::MODULE_HAS_NEW_VERSION) == self::MODULE_HAS_NEW_VERSION),
				'under_development' => (($mod_status & self::UNDER_DEVELOPMENT) == self::UNDER_DEVELOPMENT),
				'available_for_clients' => (($mod_status & self::NEW_MODULE_ALLOWED) == self::NEW_MODULE_ALLOWED),
				'admin_module' => (($mod_status & self::ADMIN_MODULE) == self::ADMIN_MODULE),
				'inactive' => (($mod_status & self::INACTIVE) == self::INACTIVE),
				'active' => (($mod_status & self::ACTIVE) == self::ACTIVE),
			);
		}
		return $final_data;
	}


	/**
	 * Get all modules available for NEW MODULES REQUESTS
	 */
	public function getModulesAvailableForNewModuleRequests() {
		$all_modules = $this->getAllModuleDetails();
		$final_data = array();
		foreach($all_modules as $mod_loc => $module) {
			/**
			 * LIMITING MODULES:
			 * 	Helpdesk
			 * 	CAPS1B only allowed for BEA0001 (Beaufort West)
			 */
			if($mod_loc!="helpdesk" && ($mod_loc!="CAPS1B" || (!isset($_SESSION['cc']) || strtoupper($_SESSION['cc'])=="BEA0001"))) {
				$status = $module['status'];
				if($status['archived']!==true
					&& $status['has_new_version']!==true
					&& $status['under_development']!==true
					&& $status['admin_module']!==true
					&& $status['inactive']===false
					&& $status['active']===true
					&& $status['available_for_clients']===true
				) {
					$module['display_name'] = $module['name'].(strlen($module['comment'])>0?" [".$module['comment']."]":"");
					$final_data[$mod_loc] = $module;
				}
			}
		}
		return $final_data;
	}
	public function getModulesAvailableForNewModuleRequestsFormattedForSelect() {
		$data = $this->getModulesAvailableForNewModuleRequests();
		$final_data = array();
		foreach($data as $key => $d) {
			$final_data[$key] = $d['display_name'];
		}
		return $final_data;
	}






	public function __destruct() {
		parent::__destruct();
	}


}


?>