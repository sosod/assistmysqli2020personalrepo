<?php
include("header.php");



$modules = $modulesObject->getAllModulesForAdmin();

?>
<h1>Help > Admin</h1>
<div style='width:50%' id=div_container>
<?php

foreach($modules as $status => $mods) {
	echo "<h4>".$modulesObject->getModuleStatusName($status)."</h4>";
	foreach($mods as $modloc => $mod) {
	?>
	<Table width=100% style='margin: 5px'>
		<tr>
			<td class='center middle' rowspan=2 width=100px>IMG HERE</td>
			<td>
				<span class=b><?php echo $mod['name']; ?></span>
				<?php
				if(strlen($mod['comment'])>0) {
					echo "<br /><span class=i style='font-weight:normal;'>".$mod['comment']."</span>";
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				<span  class=i style='font-size:75%'>System Code: <?php echo $mod['modloc']; ?>
				<br />Module Version: <?php echo $mod['version']; ?>
				<br />Manual Last Updated: <?php echo strtotime($mod['manual_last_updated']); ?>
				<br />Manual Version: <?php echo $mod['manual_version']; ?></span>
			</td>
		</tr>
	</Table>
	<?php
	}
}

?>
</div>