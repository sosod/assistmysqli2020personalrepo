<?php
include("header.php");

$admin_company = array("IASSIST","AIT0001");

if($me->isAdminUser()) {
	$admin_modules = $me->getAdminSystemMenu();
} else {
	$admin_modules = array();
}

$modules = $me->getMenuModules();
$logos = $me->getModuleLogoDetails();


?>


	<style>

	.icon{
		margin-top:5px;
		margin-bottom:5px;
	}


	.sortablefolders {
		list-style-type: none;
		margin: 0; padding: 0;
		list-style-image: url();
	}
	.sortablefolders li {
		margin: 3px 3px 3px 0; padding: 1px; float: left;
		width: 130px; height: 135px;
		text-align: center; vertical-align: middle; font-size: 1em;
		background: url();
	}

.a_hover, a_hover a {
	color: #FE9900;
	border: 1px solid #FE9900;
	cursor: pointer;
}
.no_hover {
	border: 1px solid #ababab;
}
.no_hover, .no_hover a {
	color: #555555;
}

	.sortablefolders li a {
		text-decoration: none;
	}


</style>
<script type=text/javascript>
$(function() {
	$( ".sortablefolders" ).sortable({});
	$( ".sortablefolders" ).disableSelection();
	$(".sortablefolders li").hover(
		function(){
			$(this).removeClass("no_hover").addClass("a_hover");
			$(this).children("a").css("color","#FE9900");
		},
		function(){
			$(this).removeClass("a_hover").addClass("no_hover");
			$(this).children("a").css("color","#555555");
		}
	);
	$("#div_helpdesk").click(function() {
		window.parent.header.document.location.href = '../title_login.php?act=change&m=helpdesk';
		//window.parent.header.$("#main_menu").val("helpdesk");
		//window.parent.header.$("#main_menu").trigger("change");
	});
});

</script>

<h1>Help</h1>
<?php
$can_access_admin = false;
//if(in_array(strtoupper($_SESSION['cc']),$admin_company) && !$me->isAdminUser() && !$me->isSupportUser()) {
//	$can_access_admin = true;
//	echo "<p style='position:absolute;top:5px;right:15px;'><button id=btn_admin>Admin</button></p>";
//}
?>
<div id=div_sortable style='width: 75%; margin: 0 auto; '>
<h2>Getting Started</h2>
	<ul class="sortablefolders">
	<?php
	if($me->isAdminUser()) {
		?>
		<li class="no_hover"><a href="documents/AssistActivationGuide20170109.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_master.png" /><br />Activation Guide</a></li>
	<!--	<li class="no_hover"><a href="handouts/administrators_quick_start.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_master.png" /><br />Setting Up</a></li>
		<li class="no_hover"><a href="handouts/administrators_maintenance.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_master.png" /><br />Maintenance</a></li> -->
	<?php } ?>
	<li class="no_hover"><a href="handouts/login_page.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_login.png" /><br />Logging In</a></li>
<!--	<li class="no_hover"><a href="handouts/instruction_manuals.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_manual.png" /><br />Using Manuals</a></li> -->
	<li class="no_hover"><a href="handouts/assist_platform.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_profile.png" /><br />My Assist Platform</a></li>
	<li class="no_hover"><a href="handouts/main_menu.pdf" target="_blank"><img class=icon src="../pics/module/quickstart_menu.png" /><br />Main Menu</a></li>
<?php
if($me->isAdminUser()) {
	?>
	</ul>
	<h2 style='margin-top: 180px'>System Modules</h2>
	<ul class="sortablefolders">
	<?php

unset($admin_modules['reports']);
	foreach($admin_modules as $ml=>$mt) {
	//	$ml = $m['modlocation'];
		$mr = $ml; //$m['modref'];
		//if(isset($man_modules[$ml])) {
		//	$url = "faq.php?mid=".$man_modules[$ml]['id'];
		//} else {
		//	$url = "pdf.php?loc=".$ml."&ref=".$m['modref'];
		//}
		$url = "module.php?ml=".$ml."&mr=".$mr;
		$img = $logos['path'];
		if(isset($logos['logos'][$ml])) {
			$img.= $logos['logos'][$ml];
		} else {
			$img.= $logos['default'];
		}
		echo "<li class=\"no_hover\"><a href=\"$url\"><img class=icon src=\"$img\" /><br />".$mt."</a></li>";
	}

	echo "</ul>

	<h2 style='margin-top: 180px'>User Modules</h2>";
//endif is admin user
} else {
	echo "</ul><h2 style='margin-top: 180px'>User Modules</h2>";
}


$active_modules = $manualsObject->getModulesWithManuals();
$old_manuals = $me->getOldUserManuals();
//$new_manuals = array("SDBP5","COMPL");
//$me->arrPrint($modules);
//$me->arrPrint($active_modules);
//$me->arrPrint($old_manuals);
$c=0;
echo "<ul class=sortablefolders>";
foreach($modules as $m) {
	$ml = $m['modlocation'];
	$mr = $m['modref'];
	if(isset($active_modules[$ml]) || (isset($old_manuals['location'][$ml]) && count($old_manuals['location'][$ml])>0) ) {
		$url = "module.php?ml=".$ml."&mr=".$mr;
		$img = $logos['path'];
									if($ml=="RGSTR" && strtoupper(substr($m['modtext'],0,4))!="RISK") {
										$logo_ref = "RGSTR_ALT";
									} elseif(($ml=="SDBP5" || $ml=="SDBP5B") && !is_numeric(substr($mr,-1))) {
										$logo_ref = "IND_KPI";
									} elseif($ml=="EXTERNAL") {
										$logo_ref = $mr;
									} else {
										$logo_ref = $ml;
									}

		if(isset($logos['logos'][$logo_ref])) {
			$img.= $logos['logos'][$logo_ref];
		} else {
			$img.= $logos['default'];
		}
		echo "<li class=\"no_hover\"><a href=\"$url\"><img class=icon src=\"$img\" /><br />".$m['modtext']."</a></li>";
		$c++;
	}
}
echo "</ul>";
if($c==0) {
	echo "<p>You do not have access to any manuals.</p>";
}

/*
if($_SESSION['cc']=="umz777t") {
	echo "<p>Is Admin user: ".($me->isAdminUser()?"yes":"no");
	echo "<p>Is Support user: ".($me->isSupportUser()?"yes":"no");
	echo "<p>Is Demo Company: ".($me->isDemoCompany()?"yes":"no");
}
*/
$helpdeskObject = new HELPDESK_COMPANY();
$is_company_setup_in_helpdesk = $helpdeskObject->isCompanyInHelpdesk($me->getCmpCode());
//$is_company_setup_in_helpdesk = false;
if($is_company_setup_in_helpdesk && (!$me->isAdminUser() && !$me->isSupportUser())) {//demo company check managed in helpdesk itself now - doesn't allow addition of demo companies //demo company check disabled for testing && !$me->isDemoCompany())) {
//if(!$me->isAdminUser()) {
	echo "
	<div id=div_helpdesk style='position: absolute; bottom: 10px; right: 10px; padding: 20px; cursor: pointer; width: 150px;height:150px; border: 1px solid #000099;' class=center>
		<p class=i>Need more help?<br />Contact the Helpdesk</p>
		<p><img src=../pics/helpdesk.png /></p>
	</div>";
} elseif($me->isDemoCompany() && (!$me->isAdminUser() && !$me->isSupportUser())) {
	//display greyed out helpdesk button for demo purposes
	echo "
	<div id=div_helpdesk_demo style='position: absolute; bottom: 10px; right: 10px; padding: 20px; disabled:disabled; width: 150px;height:150px; border: 1px solid #777777;' class=center>
		<p class='i' style='color: #777777;'>Need more help?<br />Contact the Helpdesk</p>
		<p><img src=../pics/helpdesk_grey.png /></p>
	</div>
	<div id=div_helpdesk_warning style='position: absolute; bottom: 10px; right: 210px; padding: 10px;padding-top:15px; width: 150px;height:165px; border: 1px solid #fe9900;' class=center>
		<p style='margin:0px;padding:0px;' class=orange><span class=' i b'>Please note:</span><br /><span class=i>To maintain the integrity of the Helpdesk, demo companies are not permitted to access it.  This button will be active on a client database, provided that the client's reseller has activated the Helpdesk for that client.</span></p>
	</div>
	";
} else if($me->isAdminUser()) {
	//TRAINING PRESENTATIONS & SKILLS PRACTICE EXAMPLES
	echo "<h2 style='margin-top: 180px'>Training Presentations & Skills Practice Examples</h2><ol>";
	$files = (glob("training/Administrator*.*"));
	foreach($files as $file) {
		$file_array = explode("/",$file);	//strip out the folder structure
		$file_name_array = explode("_",$file_array[count($file_array)-1]);//get the last element (file name) and split the components
		$last_element = $file_name_array[count($file_name_array)-1];	//get the last element which contains the file date and extension
		unset($file_name_array[count($file_name_array)-1]);	//remove the file date
		$file_title_array = array();
		foreach($file_name_array as $fn) {	//create the file title by looping through the file name array and processing any -
			$file_title_array[] = str_replace("-"," ",$fn);
		}
		$file_title = implode(" - ",$file_title_array);
		$last_element_array = explode(".",$last_element);
		$file_date = $last_element_array[0];
		$file_date_unix = mktime(12,0,0,substr($file_date,4,2),substr($file_date,6,2),substr($file_date,0,4));
		echo "
		<li><a href=$file target=_blank>".$file_title."</a><br /><span class=i style='font-size: 75%; '>
		&nbsp;&nbsp;&nbsp;File Date: ".date("d F Y",$file_date_unix)."<br />
		&nbsp;&nbsp;&nbsp;File Type: ".strtoupper($last_element_array[1])."<br />
		&nbsp;&nbsp;&nbsp;File Size: ".$me->formatBytes(filesize($file))."
		</span></li>";
	}
	echo "</ol>";

}

?>


</div>

<?php
if($can_access_admin) {
	?>
	<script type="text/javascript" >
		//$(function() {
		//	$("#btn_admin").button({icons:{primary:"ui-icon-wrench"}}).click(function(e) {e.preventDefault(); document.location.href = "admin.php";});
		//});
	</script>
	<?php
}

?>

</body>
</html>