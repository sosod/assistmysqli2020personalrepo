<?php
include("header.php");
//error_reporting(-1);

//$me->arrPrint($_REQUEST);
$modref = $_REQUEST['mr'];
$modloc = $_REQUEST['ml'];

$mod_title = $me->getModTitle($modref);

echo "<h1><a href=main.php class=breadcrumb>Help</a> >> ".$mod_title."</h1>";



$module_details = $modulesObject->getModuleDetails($modloc);
$manuals = $manualsObject->getManualsForModule($module_details['module']['id']);

//ASSIST_HELPER::arrPrint($module_details);
//ASSIST_HELPER::arrPrint($manuals);

$manual_version = strlen($module_details['manual_version'])>0?$module_details['manual_version']:"1";
$manual_version_tracking = array();
$manual_last_updated = 0;

$file_errors = array();
$manuals_count = 0;

function drawDiv($data) {
	global $manual_version_tracking;
	global $manual_last_updated;
	global $manuals;
	global $file_errors;
	global $manuals_count;
	$manual_code = "";
	foreach($data[0] as $sec_id => $section) {
		$code = (strlen($section['code'])>0 ? $section['code']."." : "");
		echo "<h3 class='".($section['not_app']==true?"not-app":"")."'>".(strlen($code)>0?$code." ":"").$section['name']."</h3><div>";
		$has_manuals = false;
		if($section['not_app']==true) {
			echo "<p class=i '>This section is not applicable to this module.</p>";
			$has_manuals = true;
		} elseif(isset($section['comment']) && strlen($section['comment'])>0) {
			echo "<p class=i '>".$section['comment']."</p>";
		}
		if(isset($manuals[$sec_id]) && count($manuals[$sec_id])>0) {
			echo "<ul>";
			$sub_level = 1;
			foreach($manuals[$sec_id] as $man_id => $man) {
				$manuals_count++;
				$link = "documents/".$man['document'];
				if(file_exists($link)){
					$code2 = $code.$manual_code.$sub_level.".";
//					echo "<li><a href=".$link.">".$code2." ".$man['name']."</a> <small>[Updated: ".date("d M Y",strtotime($man['add_date']))."]</small></li>";
					echo "<li><a href=".$link.">".$code2.". ".$man['name']."</a>".(strlen($man['comment'])>0||strlen($man['toc'])>0?" <span class='toc ui-icon ui-icon-info' toc='".$man['toc']."' comment='".$man['comment']."' state=hidden></span>":"")."<span class=float style='font-size:75%'>[Updated: ".date("d M Y",strtotime($man['add_date']))."]</span></li>";
					$man_add_date = strtotime($man['add_date']);
					if($man_add_date>$manual_last_updated) {
						$manual_last_updated = $man_add_date;
					}
					$man_add_date = strtotime(date("d F Y",$man_add_date));
					if(!in_array($man_add_date,$manual_version_tracking)) {
						$manual_version_tracking[] = $man_add_date;
					}
				} else {
					$man['document_link'] = $link;
					$file_errors[] = $man;
				}
				$sub_level++;
			}
			echo "</ul>";
			$has_manuals = true;
		}
		if(isset($data[$sec_id]) && count($data[$sec_id])>0) {
			foreach($data[$sec_id] as $sec2_id => $sec2) {
				$code3 = $code.$sec2['code'].".";
				echo "<p class=b>".$code3." ".$sec2['name']."</p>";
				if(isset($sec2['comment']) && strlen($sec2['comment'])>0) {
					echo "<p class=i '>".$sec2['comment']."</p>";
				}
				if(isset($manuals[$sec2_id]) && count($manuals[$sec2_id])>0) {
					echo "<ul>";
					$sub_level = 1;
					foreach($manuals[$sec2_id] as $man_id => $man) {
						$link = "documents/".$man['document'];
						$manuals_count++;
						if(file_exists($link)){
							$code4 = $code3.$manual_code.$sub_level;
							//echo "<li><a href=".$link.">".$code4.". ".$man['name']."</a>".(strlen($man['comment'])>0||strlen($man['toc'])>0?" <span class=toc toc='".$man['toc']."' comment='".$man['comment']."' state=hidden>[TOC]</span>":"")."<span class=float style='font-size:75%'>[Updated: ".date("d M Y",strtotime($man['add_date']))."]</span></li>";
							echo "<li><span class=float style='font-size:75%'>[Updated: ".date("d M Y",strtotime($man['add_date']))."]</span><a href=".$link.">".$code4.". ".$man['name']."</a>".(strlen($man['comment'])>0||strlen($man['toc'])>0?" <div class='toc ui-icon ui-icon-info' title='Click for additional information' toc='".$man['toc']."' comment='".$man['comment']."' state=hidden style='display:inline-block'></div>":"")."</li>";
							$man_add_date = strtotime($man['add_date']);
							if($man_add_date>$manual_last_updated) {
								$manual_last_updated = $man_add_date;
							}
							$man_add_date = strtotime(date("d F Y",$man_add_date));
							if(!in_array($man_add_date,$manual_version_tracking)) {
								$manual_version_tracking[] = $man_add_date;
							}
						} else {
							$man['document_link'] = $link;
							$file_errors[] = $man;
						}
						$sub_level++;
					}
					echo "</ul>";
				} else {
					echo "<p>No manuals are currently available for this section.</p>";
				}
			}
		} elseif(!$has_manuals) {
			echo "<p>No manuals are currently available for this section.</p>";
		}
		echo "</div>";
	}

}



?>
<div id=div_right style='width:40%; border: 1px solid #000099; margin: 10px; padding: 10px; margin-top: 0px'  class='div_structure float'>
	<h2>What are you working on?</h2>
	<div id=right_accordian>
		<?php
		if(isset($module_details['sections']['I'])) {
			$data = $module_details['sections']['I'];
			drawDiv($data);
		}
		$data = $module_details['sections']['R'];
		drawDiv($data);
		?>
	</div>
</div>
<div id=div_left class=div_structure style='width:40%; border: 1px solid #000099;margin: 10px; padding: 10px' >
	<h2>Where are you working?</h2>
	<div id=left_accordian>
		<?php
		if(isset($module_details['sections']['I'])) {
			$data = $module_details['sections']['I'];
			drawDiv($data);
		}
		$data = $module_details['sections']['L'];
		drawDiv($data);
		?>
	</div>
</div>
<div id=div_module_details style='width: 40%;margin: 10px;'  class='div_structure'>
	<div id=sub_div >
<?php
$mod_status = $module_details['module']['status'];
$manual_version.=".".count($manual_version_tracking);
?>
	<h2 class=orange>Module Details</h2>
	<table>
		<tr>
			<td>Default Module Name:</td>
			<td><?php echo $module_details['module']['name']; ?></td>
		</tr>
		<tr>
			<td>Module Code:</td>
			<td><?php echo $module_details['module']['modloc']; ?></td>
		</tr>
		<tr>
			<td>Module Comment:</td>
			<td><?php echo $module_details['module']['comment']; ?></td>
		</tr>
		<tr>
			<td>Module Status:</td>
			<td><?php
				if($mod_status['under_development']==true) {
					echo "This module is under active development and is subject to change without notice.";
				} elseif($mod_status['new_version']==true) {
					if($mod_status['archived']==true) {
						echo "This module has been replaced by a newer version and is no longer supported.";
					} else {
						echo "A newer version of this module is available, but this version is still supported.";
					}
				} elseif($mod_status['archived']==true) {
					echo "This module has been archived and is no longer supported.";
				} else {
					echo "Active and supported";
				}
				?></td>
		</tr>
		<tr>
			<td>Manual Version:</td>
			<td><?php echo $manual_version; ?></td>
		</tr>
		<tr>
			<td>Manual Updated:</td>
			<td><?php echo $manual_last_updated>0 ? date("d F Y",$manual_last_updated) : "N/A"; ?></td>
		</tr>
	</table>
	</div>
</div>
<div id=div_tscs class=div_structure style='width: 40%;margin: 10px;'>
<?php
$me->displayResult(array("info","Terms & Conditions: Please note that while every effort has been made to maintain the accuracy and scope of these manuals, some methods may differ slightly from those applied on your Assist system.  Screenshots in these manuals may differ slightly from those found in your web browser. Lists and data have been captured from a demonstration database and will differ from those in your organisation. Certain headings and text fields may differ or be absent from those in the manuals, according to the Setup steps taken by your Assist Administrator."));

//for development testing when uploading new docs to verify that all files are displaying - only display on local view
if(strpos($_SERVER['SERVER_NAME'],"localhost")!==false && count($file_errors)>0) {
	echo "<h3>File Errors: ".count($file_errors)." errors of $manuals_count </h3>";
	ASSIST_HELPER::arrPrint($file_errors);
}

?>
</div>
<script type="text/javascript">
$(function() {
	//var src = AssistHelper.getWindowSize();
	//var div_height = src.height-150;

	$("#div_module_details table").find("tr").find("td:first").addClass("b");

	$("#right_accordian").accordion({
		active: false,
		collapsible: true,
		heightStyle: "content"
	});
	$("#left_accordian").accordion({
		active: false,
		collapsible: true,
		heightStyle: "content"
	});

	//$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");//.css("height",div_height+"px");
	$("div.div_structure").css({
		'width':'45%',
		'margin':'10px',
		'padding':'10px'
	}).addClass("ui-widget-content ui-corner-all");//.css("height",div_height+"px");
	$("h2").css("margin-top","3px");
	$("#div_module_details").addClass("orange-border").find("table").addClass("noborder").find("td").addClass("noborder").css("padding","7px");
	$("#div_tscs").addClass("noborder");
	//$("#div_tscs").css("border","0px solid #ffffff");
	$("li").css({"line-height":"200%","margin-left":"0px","padding":"3px"})
		.hover(function(){
			$(this).css("background-color","#dddddd");
		},function() {
			$(this).css("background-color","");
		});
	$("a").css("color","#000099");

	$("div.toc").css({"cursor":"pointer","margin-left":"5px","margin-bottom":"-3px","background-image": "url(/library/jquery/css/images/ui-icons_888888_256x240.png)"}).click(function() {
		var my_state = $(this).attr("state");
		var toc = $(this).attr("toc");
		var has_toc = false;
		if(toc.length>0) {
			has_toc = true;
			toc = toc.replace(new RegExp('\r?\n','g'), '</li><li>');
			toc = "<span class=b>Table of Contents</span><br /><ol><li>"+toc+"</li></ol>";
		}
		var has_comment = false;
		var comment = $(this).attr("comment");
		if(comment.length>0) {
			has_comment = true;
			comment = "<span class=i>"+comment+"</span>";
		}
		if(has_toc && has_comment) {
			var divider = "<br />";
		} else {
			var divider = "";
		}
		var display = comment+divider+toc;
		if(my_state=="hidden") {
			$(this).attr("state","displayed");
			$(this).removeClass("ui-icon-info").addClass("ui-icon-circle-check");
			$(this).closest("li").append($("<div />",{class:'my_toc',html:display}));
			$(this).closest("li").find(".my_toc").css({
				"margin":"3px",
				"background-color":"#FFFFFF",
				"padding":"5px"
			});
		} else {
			$(this).attr("state","hidden");
			$(this).closest("li").find(".my_toc").remove();
			$(this).addClass("ui-icon-info").removeClass("ui-icon-circle-check");
		}
	});
	$("h3.not-app").css("color","#ababab");
});
</script>