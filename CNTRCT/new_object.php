<?php
include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type;

//$helper->arrPrint($_REQUEST);

switch($parent_object_type) {
	case "CONTRACT":
		$childObject = new CNTRCT_DELIVERABLE();
		if($display_type=="default") {
			$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'contract_id'=>$parent_object_id)); 
		}
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getDeliverableObjectName();
		$parentObject = new CNTRCT_CONTRACT($parent_object_id);
		$child_object_type = "DELIVERABLE";
		$child_object_id = 0;
		$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = 0;
		break;
	case "DELIVERABLE":
		$childObject = new CNTRCT_ACTION();
		if($display_type=="default") {
			$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$parent_object_id));
		} 
		$child_redirect = "new_action_edit.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName();
		$parentObject = new CNTRCT_DELIVERABLE($parent_object_id);		
		$child_object_type = "ACTION";
		$child_object_id = 0;		
		$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = $object_id;

		break;
	case "ACTION":
		$child_redirect = "";
		$parent_id = $object_id;
		$child_objects = array();
		break;
}

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php 
	if($display_type=="default") {
		$td2_width = "48%"; 
	?>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="CONTRACT"));
			$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>><h2>New <?php echo $child_name; ?></h2>
	<?php 
	} else {
		//$td2_width = "100%";
		//echo "		<td width=".$td2_width.">";
		echo "<td>";
	}
			//include("common/generic_object_form.php");
//			echo $page_action;
//			echo "<br />".$page_redirect_path;
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<?php if(isset($child_objects) && count($child_objects)>0 && $display_type=="default") { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	?>
	$("input:button.btn_edit").click(function() {
		var i = $(this).attr("ref");
		document.location.href = '<?php echo $child_redirect; ?>'+i;
	});
	
	
	
	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>
});
</script>