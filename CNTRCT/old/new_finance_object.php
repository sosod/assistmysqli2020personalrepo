<?php
$page_redirect_path = "new_finance.php?";


/*$scripts = array(
"/library/jquery-plugins/fixedheadertable/jquery.fixedheadertable.min.js"
);
//$css = array("/library/jquery-plugins/fixedheadertable/css/defaultTheme.css");
 */
include("inc_header.php");

include("0_finance_test_data.php");

$default_width = "400px";
$currency_default_width = "110px";
$perc_default_width = "100px";

$object_type = "CONTRACT";
$object_id = $_REQUEST['object_id'];
			
$myObject = new CNTRCT_CONTRACT();
$childObject = new CNTRCT_DELIVERABLE();
//$child_objects_ordered = $childObject->getOrderedObjects($object_id);

$suppObject = new CNTRCT_CONTRACT_SUPPLIER();
//$suppliers = $suppObject->getObjectByContractID($object_id);

//$helper->arrPrint($suppliers);
//$helper->arrPrint($child_objects_ordered);
?>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false,array(),true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<h2>Summary of <?php echo $helper->getContractObjectName()." ".$myObject->getRefTag().$object_id; 
				$summ = $myObject->getSummary($object_id); ?></h2>
			<table class='form th2' width=50%>
				<tr>
					<th width=150px>Deliverables:</th>
					<td><?php echo $summ['DEL']['deliverable']; ?></td>
				</tr><tr>
					<th>Deliverable Actions:</th>
					<td><?php echo $summ['DEL']['action']; ?></td>
				</tr><tr>
					<th>Parent Deliverables:</th>
					<td><?php echo $summ['MAIN']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Deliverables:</th>
					<td><?php echo $summ['SUB']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Deliverable Actions:</th>
					<td><?php echo $summ['SUB']['action']; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=3><div id=div_budget style='background-color: #ffffff;overflow:auto; border: 1px solid #ababab;'>
<?php
$total_contract_budget = 0; 
echo "
			<table id=tbl_budget>
				<thead>
				<tr>
					<th width=".$default_width." rowspan=2 id=th_first>".$helper->getObjectName("deliverable")."</th>";
			foreach($suppliers as $si => $s) { 
				echo "
					<th colspan=3>
						".$s['contract_supplier']."
					</th>
				";
			}
			echo "
					<th colspan=2>".$total_heading."</th>
				</tr>
				<tr>";
			foreach($suppliers as $si => $s) { 
				echo "
					<th width=".$currency_default_width.">".$budget_heading."</th>
					<th width=".$perc_default_width.">".$helper->getObjectName("contract")." ".$perc_heading."</th>
					<th width=".$currency_default_width.">".$retention_heading."</th>
				";
			}
			echo "
					<th width=".$currency_default_width.">".$budget_heading."</th>
					<th width=".$currency_default_width.">".$retention_heading."</th>
				</tr>
			</thead>";
			
			/**
			 * FIRST ROW = CONTRACT_SUPPLIER settings
			 */
			echo "
				<tr class='trhover-dark b'>
					<td class=right width=".$default_width.">".$helper->getObjectName("contract")." ".$total_heading.":</td>";
			foreach($suppliers as $si => $s) {
				$total_contract_budget+=$s['budget']; 
				echo "
					<td class=right>"; $displayObject->drawDataField("CURRENCY",$s['budget'],$currency_options); echo "</td>
					<td class=center>"; $displayObject->drawDataField("PERC","100"); echo "</td>
					<td class=center>N/A</td>
				";
			}
			echo "
					<td class=right>"; $displayObject->drawDataField("CURRENCY",$total_contract_budget,$currency_options); echo "</td>
					<td class=center>N/A</td>
				</tr>";
			/**
			 * SECOND ROW = Totals of values captured by user
			 */
			echo "
				<tr class='trhover b'>
					<td class=right width=".$default_width.">Total:</td>";
			foreach($suppliers as $si => $s) { 
				echo "
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"S_total_budget S".$s['id'],'supp'=>$s['id']),"0"); echo "</td>
					<td class=center>"; 
						//$displayObject->drawFormField("PERC",array(),"0");
						//echo "N/A";
						$displayObject->drawFormField("LABEL",array('class'=>"S_max_completion S".$s['id'],'supp'=>$s['id']),"0%"); 
					echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"S_total_retention S".$s['id'],'supp'=>$s['id']),"0"); echo "</td>
				";
			}
			echo "
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_budget "),"0"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_retention "),"0"); echo "</td>
				</tr>";
			/**
			 * ROWS 3+ = Deliverables settings
			 */
foreach($child_objects_ordered[0] as $i => $c) {
	$a = $c;
	echo "
				<tr class=b>
					<td width=".$default_width.">".$a['reftag'].": ".$a['name']."</td>";
			foreach($suppliers as $si => $s) {
				if($c['del_type']=="MAIN") {
					echo "
						<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"SD_total_budget D".$i." S".$s['id']),"0"); echo "</td>
						<td class=center>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"SD_max_completion D".$i." S".$s['id']),"0%"); echo "</td>
						<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"SD_total_retention D".$i." S".$s['id']),"0"); echo "</td>
					";
				} else {//name=budget[del_id][s_id] field_type=budget/ret/compl del=del_id supp=supp_id
					echo "
						<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$i,'class'=>"budget D".$i." S".$s['id'],'supp'=>$s['id'],'parent'=>$a['del_parent_id']),"0"); echo "</td>
						<td class=center>"; $displayObject->drawFormField("PERC",array('del'=>$i,'class'=>"completion D".$i." S".$s['id'],'supp'=>$s['id'],'parent'=>$a['del_parent_id']),"0"); echo "</td>
						<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$i,'class'=>"retention D".$i." S".$s['id'],'supp'=>$s['id'],'parent'=>$a['del_parent_id']),"0"); echo "</td>
					";
				}
			}
	echo "
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_budget D".$i),"0"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_retention D".$i),"0"); echo "</td>
				</tr>";
	if($c['del_type']=="MAIN") {
		foreach($child_objects_ordered[$i] as $x => $z) {
			$a = $z;
			echo "
				<tr>
					<td width=".$default_width.">".$a['reftag'].": ".$a['name']."</td>";
			foreach($suppliers as $si => $s) { 
				echo "
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$x,'class'=>"budget D".$x." S".$s['id']." P".$z['del_parent_id'],'supp'=>$s['id'],'parent'=>$z['del_parent_id']),"0"); echo "</td>
					<td class=center>"; $displayObject->drawFormField("PERC",array('del'=>$x,'class'=>"completion D".$x." S".$s['id']." P".$z['del_parent_id'],'supp'=>$s['id'],'parent'=>$z['del_parent_id']),"0"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$x,'class'=>"retention D".$x." S".$s['id']." P".$z['del_parent_id'],'supp'=>$s['id'],'parent'=>$z['del_parent_id']),"0"); echo "</td>
				";
			}
	echo "
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_budget D".$x),"0"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_retention D".$x),"0"); echo "</td>
				</tr>";
		}
	}
}
?>
			</table></div>
		</td>
	</tr>
	<tr><td colspan=3 class=center><input type=button value="<?php echo $helper->getActivityName("save"); ?>" class=isubmit /></td></tr>
</table>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	$("#tbl_budget tr:gt(3)").each(function() {
		$(this).find("td:last").css("background-color","#eeeeff").prev().css("background-color","#eeeeff");
	});
	$("#tbl_budget tr:eq(3)").find("td:last").css("background-color","#ddddff").prev().css("background-color","#ddddff");
	$("#tbl_budget tr:eq(2)").find("td:last").css("background-color","#ccccff").prev().css("background-color","#ccccff");
	var screen = AssistHelper.getWindowSize();
	var table_width = AssistString.substr($("#tbl_budget").css("width"),0,-2)*1;
	var div_width = (screen.width-40);
	$("#div_budget").css("width",div_width+"px");
	var h = 0;
	$("#tbl_budget tr:gt(3)").each(function() {
		var x = AssistString.substr($(this).css("height"),0,-2)*1;
		if(x>h) { h = x; }
	});
	var max_width = 500;
	var add_width = 0;
	var increment = 50;
	var h2 = 0;
	while(h>50) {
		add_width+=increment;
		table_width+=increment;
		$("#tbl_budget").prop("width",table_width+"px");
		if(add_width>=max_width) {
			h = 0;
		} else {
			h2 = 0;
			$("#tbl_budget tr:gt(3)").each(function() {
				var x = AssistString.substr($(this).css("height"),0,-2)*1;
				if(x>h2) { h2 = x; }
			});
			h = h2;
		}
	}
	var default_size = $(".number-only.budget:first").prop("size"); 
	$(".number-only").addClass("right");
	$(".number-only.budget, .number-only.retention").keyup(function() {
		if(($(this).val().length+2) > $(this).prop("size")) {
			$(this).prop("size",($(this).val().length+2));
		} 
	}).blur(function() {
		if($(this).hasClass("budget")) {
			var me = "budget";
		} else {
			var me = "retention";
		}
		if($(this).val().length==0) {
			$(this).val("0");
		}
		if(($(this).val().length+2) < $(this).prop("size") && $(this).prop("size") > default_size) {
			$(this).prop("size", default_size);//.trigger("keyup");
		}
		//my row
		var del = $(this).attr("del");
		var del_total = 0;
		$("."+me+".D"+del).each(function() {
			del_total+= ($(this).val()*1);
		});
		$(".D_total_"+me+".D"+del).text(del_total);
		//my column
		var supp = $(this).attr("supp");
		var supp_total = 0;
		$("."+me+".S"+supp).each(function() {
			supp_total+= ($(this).val()*1);
		});
		$(".S_total_"+me+".S"+supp).text(supp_total);
		//contract total
		var total = 0;
		$(".S_total_"+me).each(function() {
			total+= ($(this).text()*1);
		});
		$(".total_total_"+me).text(total);
		//parent deliverable total
		var par = $(this).attr("parent");
		if(par>0) {
			var par_total = 0;
			$("."+me+".P"+par+".S"+supp).each(function() {
				par_total+= ($(this).val()*1);
			});
			$(".SD_total_"+me+".D"+par+".S"+supp).text(par_total);
		}
		//parent deliverable total total
		var par_del_total = 0;
		$(".SD_total_"+me+".D"+par).each(function() {
			par_del_total+= ($(this).text()*1);
		});
		$(".D_total_"+me+".D"+par).text(par_del_total);
	});
	//$("#tbl_budget").fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: true });
	$(".number-only.completion").blur(function() {
		var me = "completion";
		if($(this).val().length==0) {
			$(this).val("0");
		}
		//my column
		var supp = $(this).attr("supp");
		var supp_total = 0;
		$("."+me+".S"+supp).each(function() {
			supp_total = ($(this).val()*1) > supp_total ? $(this).val()*1 : supp_total;
		});
		$(".S_max_"+me+".S"+supp).text(supp_total+"%");
		//parent deliverable max percentage completion
		var par = $(this).attr("parent");
		if(par>0) {
			var par_total = 0;
			$("."+me+".P"+par+".S"+supp).each(function() {
				par_total = ($(this).val()*1) > par_total ? $(this).val()*1 : par_total;
			});
			$(".SD_max_"+me+".D"+par+".S"+supp).text(par_total+"%");
		}
	});
});

</script>