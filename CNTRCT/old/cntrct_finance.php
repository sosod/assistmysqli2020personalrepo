<?php
/**
 * To manage the FINANCE function within the module
 * 
 * Created on: September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 * 
 */

 
class CNTRCT_FINANCE extends CNTRCT {
	
	private $finance_types = array('P'=>"Payment",'A'=>"Budget Adjustment");

	private $contract_id = 0;
	private $object_id = 0;

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $name_field = "_response";


	const REFTAG = 'CF';
	const TABLE_FLD = 'fin';
	const TABLE = 'finance';
	const LOG_TABLE = 'finance';
	const OBJECT_TYPE = "FINANCE";
	
	
	/********************************
	 * CONSTRUCTOR
	 */
	public function __construct($contract_id=0) {
		parent::__construct();
		$this->contract_id = $contract_id;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
	}
	
	
	
	/*********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$parent_id = $var['object_id'];
		unset($var['object_id']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		if($var['fin_type']=="P" && $var['fin_amount']>0) { $var['fin_amount'] *= -1; }
		$var[$this->getTableField().'_contract_id'] = $parent_id;
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_attachment'] = "";
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		 
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|finance| ".self::REFTAG.$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'parent_id'	=> $parent_id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			return array("ok","New ".$this->getObjectName(self::OBJECT_TYPE)." ".$this->getRefTag().$id." has been successfully created.");
		}
		return array("error","TESTING ".$sql." ".serialize($var));
	}
	public function updateObject($var) {
		return array("error","updateObject still have to be programmed");
	} 
	
	/********************************
	 * GET functions
	 */
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
	 
	public function getFinanceTypes() {
		return $this->finance_types;
	}
	public function getAFinanceType($f) {
		return $this->finance_types[$f];
	}
	
	
	public function getList($section="",$options=array()) {
		$final_data = array('head'=>array(),'rows'=>array());
		$headObject = new CNTRCT_HEADINGS();
		$final_data['head'] = $headObject->getMainObjectHeadings(self::OBJECT_TYPE,"LIST");
		$tblref = "F";
		
		$sql = "SELECT ".$tblref.".* ";
		foreach($final_data['head'] as $fld => $head) {
			if($head['type']=="CONTRACT_SUPPLIER") {
				$listObject = new CNTRCT_LIST($head['list_table']);
				$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$head['list_table']." 
									AS ".$head['list_table']." 
									ON ".$head['list_table'].".id = ".$tblref.".".$fld." 
									AND (".$head['list_table'].".status & ".CNTRCT::DELETED.") <> ".CNTRCT::DELETED;
				unset($listObject); 
			}
		}
		$sql.="
		FROM ".$this->getTableName()." $tblref
		".implode("",$left_joins)." 
		WHERE fin_contract_id = ".$this->contract_id." 
		ORDER BY fin_action_on ASC";
		$rows = $this->mysql_fetch_all($sql);
		$result = array();
		$displayObject = new CNTRCT_DISPLAY();
		foreach($rows as $r) {
			$row = array();
			$i = $r[$this->getIDFieldName()];
			foreach($final_data['head'] as $fld => $head) {
				if($head['type']=="CONTRACT_SUPPLIER") {
					$row[$fld] = array('display'=>$r[$head['list_table']],'id'=>$r[$fld]);
				} elseif($head['type']=="FIN_TYPE") {
					$row[$fld] = $this->getAFinanceType($r[$fld]);
				} else {
					$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('reftag'=>self::REFTAG,'html'=>true,'right'=>true));
					if($head['type']=="CURRENCY") {
						$row[$fld]['amount'] = $r[$fld];
					}
				}
			}
			$result[$i] = $row;
		}
		
		$final_data['rows'] = $result;
		return $final_data;
		
	}
	
	public function getAObject($object_id) {
		$this->contract_id = $object_id;
		$data = $this->getList();
		return $data;
	}
	
	
	public function getDetailedObjectForDisplay($object_id) {
		$displayObject = new CNTRCT_DISPLAY();
		$data = $this->getAObject($object_id);
		$csObject = new CNTRCT_CONTRACT_SUPPLIER();
		$cs = $csObject->getObjectByContractID($object_id);
		$cell_width = round(50/(count($cs)+2),0); 
		$balances = array();
		$js = "";
		$echo = "
			<table width=100%>
				<tr>";
			foreach($data['head'] as $fld=>$head) {
				$echo.="<th rowspan=2 ".($head['type']=="CURRENCY" ? "width=".$cell_width."%" : "").">".$head['name']."</th>";
			}
			$echo.="<th colspan=".(count($cs)+1).">Balances</th></tr><tr>";
			foreach($cs as $c) {
				$balances[$c['id']] = $c['budget'];
				$echo.="<th class=th2 width=".$cell_width."%>".$c['contract_supplier']."</th>";
			}
			$echo.="	
					<th class=th2 width=".$cell_width."%>".$this->getContractObjectName()." Balance</th>
				</tr>
				<tr>
					<td class=right colspan=".count($data['head']).">Original Budget</td>";
			foreach($cs as $c) {
				$x = $displayObject->getDataField("CURRENCY", $balances[$c['id']],array('right'=>true));
				$echo.="<td>".$x['display']."</td>";
			}
			$x = $displayObject->getDataField("CURRENCY", array_sum($balances),array('right'=>true));
			$echo.="<td class=b>".$x['display']."</td>
				</tr>";
		foreach($data['rows'] as $key => $row) {
			unset($csr_id); unset($csr_amt);
			$echo.="
			<tr>";
			foreach($row as $fld=>$r) {
				if(is_array($r)) {
					$echo.="<td>".$r['display']."</td>";
					$js.=isset($r['js']) ? $r['js'] : "";
					if(isset($r['id'])) { $csr_id = $r['id']; }
				} else {
					$echo.="<td>".$r."</td>";
				}
				if(strrpos($fld, "amount")!==FALSE) {
					$csr_amt = $r['amount'];
				}
			}
			if(isset($csr_id) && isset($csr_amt)) {
				$balances[$csr_id]+=$csr_amt;
			}
			foreach($cs as $c) {
				$x = $displayObject->getDataField("CURRENCY", $balances[$c['id']],array('right'=>true));
				$echo.="<td class='".($balances[$c['id']]<0 ? "idelete" : "")."'>".$x['display']."</td>";
			}
			$x = $displayObject->getDataField("CURRENCY", array_sum($balances),array('right'=>true));
			$echo.="<td class='b ".(array_sum($balances)<0 ? "idelete" : "")."'>".$x['display']."</td>
			</tr>";
		}
		$echo.="</table>";
		$result = array('display'=>$echo,'js'=>$js);
		return $result;
	}
	
	/*******************************
	 * SET / UPDATE functions
	 */
	
	
	
	/********************************
	 * PROTECTED functions
	 */
	
	
	
	
	
	
	/********************************
	 * PRIVATE functions
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************
	 * DESCTRUCT
	 */
	public function __desctruct() {
		$this->db_close();
		parent::__desctruct();
	}
	
}
?>