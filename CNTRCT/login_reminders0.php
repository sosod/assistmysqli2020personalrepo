<?php

function emailCNTRCT($db,$usr_array = array()){
	$stand = new CNTRCT_STANDALONE($db);
	$all_days = array();
	//$actObj = new CNTRCT_ACTION();
	if(count($usr_array)>0){
		//Filtering the TKIDS passing only active users with module access ($usr_array)
		$all_pro_sql = "SELECT * FROM ".$db->getDBRef()."_usernotes WHERE un_status = ".CNTRCT::ACTIVE." ORDER BY un_tkid";
		$all_pro_res = $db->mysql_fetch_all($all_pro_sql);
		foreach($all_pro_res as $key=>$val){
			if(in_array($val['un_tkid'],$usr_array)){
				//OK - this rule's person is in the list of people to send mails to
				$e = $val['un_every'];
				$td = date("w");
				$td_m = date("j");
				$td_y = date("z");
				$td_mth = date("n");
				$year = date("Y");
				$dys_ths_mnth = date("t");
				switch ($val['un_when']) {
					//Should this reminder go out today?
					case '0':
						$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						break;
					//Should this reminder go out this week?
					case '1':
						if($e==$td){
							$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						}else{
							$all_days_fail[$val['un_name']] = "Not today";
						}
						break;
					//Should reminder go out every two weeks?
					case '2':
						$scnd = false;
						if($e > 6){
							$e -= 7;
							$scnd = true;
						}
						$wk = date("W")%2==0?true:false;
						if($wk == $scnd){
							if($e==$td){
								$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
							}else{
								$all_days_fail[$val['un_name']] = "Not today";
							}
						}else{
							$all_days_fail[$val['un_name']] = "Not this week";
						}
						break;
					//Should reminder go out every month?	
					case '3':
						if($td_m == 1 || $td_m == 2 || $td_m == 25 || $td_m == $dys_ths_mnth || $td_m == ($dys_ths_mnth - 1)){
							$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						}else{
							$all_days_fail[$val['un_name']] = "Not today";
						}
						break;
					//Should reminders happen at the start of each quarter?
					case '4':
						$all_days[$val['un_name']] = "Not today - not the 1st, 2nd, last, or 2nd last of quarter";
						if
						(
							(
								($td_m == 1 || $td_m == 2 ) && ($td_mth == 1 || $td_mth == 4 || $td_mth == 7 || $td_mth == 10)
							)||(
								($td_m == $dys_ths_mnth || $td_m == ($dys_ths_mnth - 1)) && ($td_mth == 3 || $td_mth == 6|| $td_mth == 10 || $td_mth == 12)
							)
						){
							$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						}else{
							$all_days_fail[$val['un_name']] = "Not today - not the 1st, 2nd, last, or 2nd last of quarter";
						}
						break;
					//Should reminders go out according to the day of the financial year?
					case '5':
						$pro_rules = $stand->getProfile($val['un_tkid']);
						$fyear_date = $pro_rules[0]['yr_strt'];
						$date = DateTime::createFromFormat('d-M-Y',"".$fyear_date."-".$year);
						$start_day =  $date->format('z');
						$last_day = $start_day - 1 <= 0?0:$start_day - 1;
						$last_wk = $start_day - 7 <= 0?0:$start_day - 7;
						if($td_y == $start_day || $td_y == $last_day || $td_y == $last_wk){
							$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						}else{
							$all_days_fail[$val['un_name']] ="Not the first day or last day of fyear... STRT=>$start_day, LST=>$last_day, LW=>$last_wk, TD=>$td_y";
						}
						break;
					//Should reminders go out according to the day of the year?
					case '6':
						$cust = $val['un_custom'];
						$day = intval($td_y);
						$cust_date = DateTime::createFromFormat('Y-m-d',$cust);
						$custard = $cust_date->format('z');
						if($day == $custard){
							$all_days[$val['un_name']] = distributeFlow($db,$stand,$val);
						}else{
							$all_days_fail[$val['un_name']] = "Not the right day of year... CSTM => $custard, 2DY =>$day";
						}

						break;
					default:
						$all_days[]=array($td,$e);
						return "No case match for that 'WHEN'.";
						break;
				}
			}
		}
		return $all_days;
		/*
			$raw[$val['tkid']] = unserialize($val['profile']);
		foreach($raw as $key=>$val){
		//Filtering for profiles with reminders set to 'yes'
			if($val['reminders'] == "yes"){
			//Filtering for profiles with daily reminders or weekly reminders whose proc date is today
				if($val['remind_freq'] == "daily" || ($val['remind_freq'] == "weekly" && $val['remind_day'] == date("N"))){
				//Filtering by the type of notifications selected -> can be accessed in the cntrct_profile constructor for reference (hard coded here)
					switch ($val['remind_what']) {
						case '1':
							return $actObj->getActionsForNote("0002","ALL");
							break;	
						case '2':
							return $actObj->getActionsForNote("0002","THISWEEK");
							break;	
						case '3':
							return $actObj->getActionsForNote("0002","OVERTHISWEEK");
							break;	
						case '4':
							return $actObj->getActionsForNote("0002","TODAY");
							break;	
						case '5':
							return $actObj->getActionsForNote("0002","OVERTODAY");
							break;	
					}	
						
					//return array("guess what?","BINGO!");
				}else{
					return array("Noob emails sent", "sha");
				}
			}
		//Filtering for profiles with missed_reminders set to 'yes'
			if($val['missed_reminders'] == "yes"){
			//Filtering for profiles with daily reminders or weekly reminders whose proc date is today
				if($val['missed_remind_freq'] == "daily" || ($val['missed_remind_freq'] == "weekly" && $val['missed_remind_day'] == date("N"))){
					return array("Who is the wisest of them all?","B(az)INGO!");
				}else{
					return array("Noob-sibot emails sent", "sha1");
				}
			}
		}
		return $raw;
		
	}else{
		return array("No emails sent", "ha");
	}
		*/
	
	}
}
function distributeFlow($db,$stand,$rule){
	switch ($rule['un_section']) {
		case '0':
			$search = $stand->performSearch(0,$rule['un_tkid'],$rule['un_what']);
		$res[]=$search;
			break;
		case '1':
			$search = $stand->performSearch(1,$rule['un_tkid'],$rule['un_what']);
		$res[]=$search;
			break;
		case '2':
			$search = $stand->performSearch(2,$rule['un_tkid'],$rule['un_what']);
		$res[]=$search;
			break;
	}
	//Testing array
//return $search;
	if(count($search)>0){
		$email = sendMail($db,$rule['un_tkid'],$search,$rule['un_name'],$rule['un_section'],$rule['un_what']);
		$email = "Notification sent: Rule = ".$rule['un_name'].", object = ".$rule['un_section'].", user = ".$rule['un_tkid'].", status = ".implode(": ",$email);
	}else{
		$email = "No search hits";
	}
	return $email;
}

function sendMail($db,$user,$items,$rule_name,$sect,$mode){
	$adds = getAddress($db,array($user));
	$_SESSION['tid'] = "0000";
	$_SESSION['tkname'] = "Assist Administrator";
	$mod_help = new CNTRCT_HELPER();
	//return $items;	
	if(count($adds['to'])>0) {
			$subject = "Action Assist: ".$rule_name." reminder email.";
			switch($sect){
				case "0":
					$message = "There are ".$mod_help->getObjectName("Actions")." awaiting your input.
					
					
					Please log onto Assist to update your ".$mod_help->getObjectName("Actions").". They include:
					
					____
					
					";
					foreach($items as $key=>$val){
						$message.="
						
						".$mod_help->getObjectName("Action")." ".$val['action_id'].":
							Name			-  ".$val['action_name']."
							".$mod_help->getObjectName("Deliverable")."		-  ".$val['action_deliverable_id']."
							Deadline		-  ".$val['action_deadline']."
							Progress		-  ".$val['action_progress']."%
							Creation date	       -  ".$val['action_insertdate']."
							";
					
					}
					$message.="
					____
					
					
					
Go to https://assist.action4u.co.za
					
					";
					//Recipies:		-  ".implode(",",$adds['to']);
					$mail_res = mail("duncancosser@gmail.com", $subject, $message);
					return ($mail_res >0?array("OK","Mail sent"):array("error","sendMail broke"));
					break;
				case "1":
					switch ($mode) {
						case '6':
						case '7':
						case '8':
					$message = "There are ".$mod_help->getObjectName("Actions")." under your ".$mod_help->getObjectName("Deliverables")." awaiting input.
					
					
					Please log onto Assist to ".$mod_help->getActivityName("manage")." the ".$mod_help->getObjectName("Actions")." under your ".$mod_help->getObjectName("Deliverables").". They include:
					
					____
					
					";
					foreach($items as $key=>$val){
						$message.="
						
						".$mod_help->getObjectName("Action")." ".$val['action_id'].":
							Name			-  ".$val['action_name']."
							".$mod_help->getObjectName("Deliverable")."		-  ".$val['action_deliverable_id']."
							Deadline		-  ".$val['action_deadline']."
							Progress		-  ".$val['action_progress']."%
							Creation date	       -  ".$val['action_insertdate']."
							";
					
					}
					$message.="
					____
					
					
					
Go to https://assist.action4u.co.za
					
					";		
							break;
						
						default:
					$message = "There are ".$mod_help->getObjectName("Deliverables")." awaiting your input.
					
					
					Please log onto Assist to ".$mod_help->getActivityName("update")." your ".$mod_help->getObjectName("Deliverables").". They include:
					
					____
					
					";
					foreach($items as $key=>$val){
						$message.="
						
						".$mod_help->getObjectName("Deliverable")." ".$val['del_id'].":
							Name			-  ".$val['del_name']."
							".$mod_help->getObjectName("Contract")."	 	-  ".$val['del_contract_id']."
							Description		-  ".$val['del_description']."
							Deadline		-  ".$val['del_deadline']."
							Progress		-  ".$val['del_progress']."%
							Creation date	       -  ".$val['del_insertdate']."
							";
					
					}
					$message.="
					____
					
					
					
Go to https://assist.action4u.co.za
					
					";
							break;
					}
					
					$mail_res = mail("duncancosser@gmail.com", $subject, $message);
					return ($mail_res >0?array("OK","Mail sent"):array("error","sendMail broke"));
					break;
				case "2":
					$message = "There are ".$mod_help->getObjectName("Deliverables")." under your ".$mod_help->getObjectName("Contract")." awaiting input.
					
					
					Please log onto Assist to manage the ".$mod_help->getObjectName("Deliverables")." under your ".$mod_help->getObjectName("Contract").". They include:
					
					____
					
					";
					foreach($items as $key=>$val){
						$message.="
						
						".$mod_help->getObjectName("Deliverable")." ".$val['del_id'].":
							Name			-  ".$val['del_name']."
							".$mod_help->getObjectName("Contract")."      	-  ".$val['del_contract_id']."
							Description		-  ".$val['del_description']."
							Deadline		-  ".$val['del_deadline']."
							Progress		-  ".$val['del_progress']."%
							Creation date	       -  ".$val['del_insertdate']."
							";
					
					}
					$message.="
					____
					
					
					
Go to https://assist.action4u.co.za
					
					";
					$mail_res = mail("duncancosser@gmail.com", $subject, $message);
					return ($mail_res >0?array("OK","Mail sent"):array("error","sendMail broke"));
					break;
			}
		}
		return array("error","No email addresses");
}

function getAddress($db,$t,$c=array()){
	$ids = array_merge($t,$c);		
		if(count($ids)>0) {
			$sql = "SELECT tkid, CONCAT(tkname,' ',tksurname) as name, tkemail as email FROM assist_".$db->getCmpCode()."_timekeep WHERE tkid IN ('".implode("','",$ids)."')";
			$rows = $db->mysql_fetch_all($sql);
			$emails = array();
			foreach($rows as $r) {
				$emails[$r['tkid']] = array('name'=>$r['name'],'email'=>$r['email']);
			}
		}
		$to = array();
		foreach($t as $x) {
			$to[] = $emails[$x];
		}
		$cc = array();
		foreach($c as $x) {
			$cc[] = $emails[$x];
		}
		$reply_to = "no-reply@ignite4u.co.za";
		
		return array('to'=>$to,'cc'=>$cc,'reply_to'=>$reply_to);
}
/*	___return string of how many reminders were sent__________
	  		if(in_array($key, $usr_array)){
					$email[] = $mailObj->getEmailAddresses($key);
						
						
					return $email;
			}
	
		foreach($all_pro_res as $pro){
			$data[$pro['tkid']]=unserialize($pro['profile']);
		}
 * 
			}
 * 
 */
?>