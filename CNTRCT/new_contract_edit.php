<?php

$_REQUEST['object_type'] = "CONTRACT";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "CONTRACT";
$button_label = "|view|";

$add_button = true;
$add_button_label = "|add| |contract|";
$add_button_function = "showAddDialog();";

/*
$data['display'].="";
			$data['js'].="
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\")
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); })
			.click(function() {
				".$add_button[2]."
			}).addClass('abutton').children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"});";
 * 
 */
?>





<?php

include("common/generic_list_page.php");





/*
<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add>add</button></span>

<script type="text/javascript" >
$(function() {
	$("button#btn_paging_add").button({
		icons: {primary: "ui-icon-circle-plus"}
	}).click(function(e) {
		e.preventDefault();
		alert("display new");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok")
	.hover(function(){$(this).removeClass("ui-button-state-ok").addClass("ui-button-state-info")}, function(){$(this).removeClass("ui-button-state-info").addClass("ui-button-state-ok")});
	//.addClass("abutton");
	//.children(".ui-button-text").css("color","#ababab");
});
</script>

 * 
 */
?>

<div id=dlg_add_contract title="<?php echo $helper->replaceAllNames("|add| |contract|"); ?>">
	<?php
	
	$section = "NEW";
	$page_redirect_path = "new_contract_edit.php?";
	$page_action = "Add";
	
	$uaObject = new CNTRCT_USERACCESS();

	$child_object_type = "CONTRACT";
	$child_object_id = 0;
	$childObject = new CNTRCT_CONTRACT();
	
	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	echo $data['display'];

?>
</div>
<script type="text/javascript">
$(function () {
	$("#dlg_add_contract").dialog({
		autoOpen: false,
		modal: true
	});
	<?php echo $data['js']; ?>
	
});
function showAddDialog() {
	$(function() {
		$("#dlg_add_contract").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("#dlg_add_contract form[name=frm_object] table.form").css("width","800px");
			$("#dlg_add_contract").dialog("option","width",850);
		} else {
			$("#dlg_add_contract form[name=frm_object] table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_add_contract").dialog("option","width",(my_window['width']-50));
		}
		$("#dlg_add_contract").dialog("option","height",my_window['height']-50);
	});
}
</script>
