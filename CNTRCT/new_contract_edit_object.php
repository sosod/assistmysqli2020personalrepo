<?php



function drawTreeTier($object_type,$level,$obj) {
	global $displayObject;
	$placeholder_td = "";
	$pos_class = "";
	$icon_class = "";
	$has_btn = true;
	$btn_class = "expand-btn";
	$tr_class = "";
	switch($level) {
		case 1:
			$pos_class = "uni-pos";
			break;
		case 2:
			$pos_class = "bi-pos";
			$icon_class = "uni-icon";
			$placeholder_td = "";
			break;
		case 3:
			$pos_class = "tri-pos";
			$icon_class = "bi-icon";
			$placeholder_td = "<td></td>";
			break;
		case 4:
			$pos_class = "quad-pos";
			$icon_class = "tri-icon";
			$placeholder_td = "<td></td><td></td>";
			break;
	}
	switch($object_type) {
		case "contract":
			$has_btn = false;
			$td_btn = "";
			$tr_class = "grand-parent";
			break;
		case "deliverable":
		case "parent-deliverable":
			$tr_class = "parent";
			break;
		case "sub-deliverable":
			$tr_class = "sub-parent";
			break;
		case "action":
			$btn_class = "sub-btn";
			$tr_class = "child";
			break;
	}
	if($has_btn) {
		$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
	}
	echo "
	<tr class=".$tr_class.">
		".$placeholder_td."
		".$td_btn."
		<td class='".$pos_class."'>".$obj['name']." [".$obj['reftag']."]</td>
		<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
			<button class='action-button view-btn' parent_id=".$obj['id'].">View</button>
			<button class='action-button edit-btn' parent_id=".$obj['id'].">Edit</button>
		</td>
		<td class='".$pos_class." center'>".$obj['responsible_person']."</td>
		<td class='".$pos_class." center'>".$displayObject->getDateForDisplay($obj['deadline'])."</td>
	</tr>
	";
	
}



 
 
$object_type = "CONTRACT"; //$_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];



$section = "NEW";
$page_redirect_path = "new_contract_edit_object.php?obejct_id=".$object_id;
$page_action = "Edit";

require_once("inc_header.php");
//$helper->arrPrint($_REQUEST);
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$can_confirm = true;


$con_object_type = "CONTRACT";
$con_object_id = $object_id;
$conObject = new CNTRCT_CONTRACT($object_id);
$con_object_details = $conObject->getSimpleDetails($object_id);

//$helper->arrPrint($con_object_details);

$delObject = new CNTRCT_DELIVERABLE();
$del_objects = $delObject->getCategoryGroupedOrderedObjects($con_object_id);

//$helper->arrPrint($del_objects);

$actObject = new CNTRCT_ACTION();
$act_objects = $actObject->getOrderedObjects($con_object_id);

//$helper->arrPrint($act_objects);

$listObject = new CNTRCT_LIST("deliverable_category");
$category_items = $listObject->getListItems();
if(!isset($category_items[0])) {
	$unspecified_item = array(array('id'=>0,'name'=>$helper->getUnspecified()));
	//$category_items = array_merge($unspecified_item,$category_items); //REMOVED DUE TO LOSS OF KEY
	$category_items = $unspecified_item+$category_items;
}

//HEADER

echo "
<table class='tbl-container not-max'><tr><td>
<table class=tbl-tree>
	<tr>
		<td class=uni-pos></td>
		<td></td>
		<!-- <td></td> -->
		<th>Responsible Person</th>
		<th>Deadline</th>
	</tr>";
//CONTRACT DETAILS
//$count = (isset($del_objects['main']) ? count($del_objects['main'])." ".$helper->getObjectName("deliverable",true) : "<span class=required>0 ".$helper->getObjectName("deliverable",true)."</span>");
$count = 0;
foreach($del_objects['main'] as $c) {
	$count+=count($c);
}
$can_confirm = $count>0 ? $can_confirm : false;
$count = ($count>0 ? $count." ".$helper->getObjectName("deliverable",($count!=1)) : "<span class=required>0 ".$helper->getObjectName("deliverable",true)."</span>");
drawTreeTier("contract",1,$con_object_details);
echo "
	<tr>
		<td class=uni-pos object_id=0 object_type=deliverable><button class='action-button add-btn' parent_id=".$con_object_id.">Add ".$helper->getObjectName("deliverable")."</button> <span class='count float'>".$count."</span></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
";
//CATEGORIES
foreach($category_items as $ci => $cate) {
	if(isset($del_objects['main'][$ci]) && count($del_objects['main'][$ci]) > 0) {
		echo "
			<tr>
				<td class=td-line-break>&nbsp;</td>
			</tr>
			<tr>
				<td class=cate-pos>".$cate['name']."</td>
			</tr>
			<tr>
				<td class=td-line-break>&nbsp;</td>
			</tr>
				";
		
		//DELIVERABLES
		foreach($del_objects['main'][$ci] as $di => $del) {
			if($del['del_type']=="MAIN") {
				$count = (isset($del_objects['subs'][$di]) ? count($del_objects['subs'][$di]) : 0);
				$can_confirm = $count>0 ? $can_confirm : false;
				$count = ($count==0 ? "<span class=required>" : "").$count." Sub-".$helper->getObjectName("deliverable",$count!=1).($count==0 ? "</span>" : "");
			} else {
				$count = (isset($act_objects[$di]) ? count($act_objects[$di]) : 0);
				$can_confirm = $count>0 ? $can_confirm : false;
				$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName("action",$count!=1).($count==0 ? "</span>" : "");
			}
			drawTreeTier("deliverable",2,$del);
			if($del['del_type']=="MAIN") {
				echo "
				<tr>
					<td></td>
					<td class=bi-pos object_id=0 object_type=sub-deliverable><button class='action-button add-btn' parent_id='".$con_object_id."&del_category_id=".$ci."&del_type=SUB&del_parent_id=".$di."' >Add Sub-".$helper->getObjectName("deliverable")."</button> <span class='float count'>$count</span></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				";
				if(isset($del_objects['subs'][$di])) {
					foreach($del_objects['subs'][$di] as $d2i => $del2) {
						$count = (isset($act_objects[$d2i]) ? count($act_objects[$d2i]) : 0);
						$can_confirm = $count>0 ? $can_confirm : false;
						$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName("action",$count!=1).($count==0 ? "</span>" : "");
						drawTreeTier("sub-deliverable",3,$del2);
						echo "
						<tr>
							<td></td>
							<td></td>
							<td class=tri-pos object_id=0 object_type=action><button class='action-button add-btn' parent_id=".$d2i.">Add ".$helper->getObjectName("action")."</button> <span class='float count'>$count</span></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						";
						if(isset($act_objects[$d2i])) {
							foreach($act_objects[$d2i] as $ai => $act){
								drawTreeTier("action",4,$act);
							}
						}
					}
				}
			} else {
				echo "
				<tr>
					<td></td>
					<td class=bi-pos object_id=0 object_type=action><button class='action-button add-btn' parent_id=".$di.">Add ".$helper->getObjectName("action")."</button> <span class='float count'>$count</span></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				";
				if(isset($act_objects[$di])) {
					foreach($act_objects[$di] as $ai => $act){
						drawTreeTier("action",3,$act);
					}
				}
			}
		}
	}//endif count del_objects per cate
}//endif category foreach loop
?> <!--
	<tr>
		<td class='tree-icon uni-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='parent bi-pos'>Main Deliverable / Parent Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td class='tree-icon bi-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='sub-parent tri-pos'>Sub Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td class='tree-icon tri-icon'><button class='ibutton sub-btn'>X</button></td>
		<td class='child quad-pos'>Action</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td class='tree-icon uni-icon'><button class='ibutton expand-btn'>X</button></td>
		<td class='parent bi-pos'>Main Deliverable / Parent Deliverable</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td></td>
		<td class='tree-icon bi-icon'><button class='ibutton sub-btn'>X</button></td>
		<td class='child tri-pos'>Action</td>
		<td>View Edit</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	<tr>
		<td>C</td>
		<td>PD</td>
		<td>SD</td>
		<td>A</td>
		<td>V/E</td>
		<td>RP</td>
		<td>DD</td>
	</tr>
	--> 
</table>
</td></tr><tr><td>
	<div id=confirmation_status>
	
</div>

</td></tr>
<tr><td>
	
<?php
$footer = $displayObject->getPageFooter($helper->getGoBack("new_contract_edit.php"));
echo $footer['display'];
$js.=$footer['js'];
?>

</td></tr>
</table>

<?php

/*******************
 * CONTRACT
 */
$view_object_type = "CONTRACT";
$view_object_id = $con_object_details['id'];
$view_object = $con_object_details;
$viewObject = $conObject;
$parentObject = null;
$parent_object_id = 0;
$parent_object_type = "";
?>
<div id=dlg_edit_contract class=dlg-child title="Edit Contract">
	<span class=float><button class=close-btn id=edit_contract>Close</button></span>
	<h2><?php echo $helper->getObjectName($view_object_type); ?> Details</h2><?php 
	$data = $displayObject->getObjectForm($view_object_type, $viewObject, $view_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
	echo $data['display'];
	$js.=$data['js'];
	?>
</div>






<div id=dlg_child class=dlg-child title="">
		<iframe id=ifr_form_display style="border:0px solid #000000;" src="">
			
		</iframe>
</div>


<script type=text/javascript>
$(function() {
	<?php 	echo $js;	
	
	?>
	
	var object_names = [];
	object_names['contract'] = "<?php echo $helper->getObjectName("contract"); ?>";
	object_names['deliverable'] = "<?php echo $helper->getObjectName("deliverable"); ?>";
	object_names['action'] = "<?php echo $helper->getObjectName("action"); ?>";

	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	if(my_window['width']>800) {
		var my_width = 850;
	} else {
		var my_width = 800;
	}
	var my_height = my_window['height']-50;	

<?php
	if(!$can_confirm) {
		echo "
		$('div#confirmation_status').html(AssistHelper.getHTMLResult(\"error\",\"Confirmation and Activation not currently possible.  Please ensure that all ".$helper->getObjectName("deliverable",true)." have at least 1 associated Sub-".$helper->getObjectName("deliverable")." / or ".$helper->getObjectName("action").".\",\"\"));
		";
	} else {
		echo "
		$('div#confirmation_status').html(AssistHelper.getHTMLResult(\"ok\",\"Confirmation and Activation is possible for this ".$helper->getObjectName("contract").".\",\"\"));
		";
	}

?>
	
	$("table.tbl-tree").find("td.tree-icon").addClass("right").prop("width","20px");
	//Set the column spanning for each object description cell based on their position in the hierarchy but excluding the last two columns
	$("table.tbl-tree").find("td.cate-pos, td.td-line-break").prop("colspan","7");
	$("table.tbl-tree").find("td.uni-pos").not("td.center").prop("colspan","4");
	$("table.tbl-tree").find("td.bi-pos").not("td.center").prop("colspan","3");
	$("table.tbl-tree").find("td.tri-pos").not("td.center").prop("colspan","2");
	$("table.tbl-tree").find("td.quad-pos").not("td.center").prop("colspan","1");
	
	//Action positional buttons
	$("button.sub-btn").button({
		icons: {primary: "ui-icon-carat-1-sw"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
	//Parent positional buttons
	$("button.expand-btn").button({
		icons: {primary: "ui-icon-triangle-1-se"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
	
	//Edit button
	$("button.edit-btn").button({
		icons: {primary: "ui-icon-pencil"},
	}).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
*/
	//View button
	$("button.view-btn").button({
		icons: {primary: "ui-icon-extlink"},
	}).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Add button
	$("button.add-btn").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css("color","#009900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"25px","font-size":"80%"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Button styling and positioning
	$("button.ibutton:not(.sub-btn)").hover(
		function() {
			$(this).css({"border-width":"0px","background":"url()"})
		},
		function() {}
	);
	$("button.ibutton").each(function() {
		if($(this).prev().hasClass("ibutton")) {
			$(this).css("margin-left","-5px");
		}
		if($(this).next().hasClass("ibutton")) {
			$(this).css("margin-right","-5px");
		}
	});
	
	//Styling based on object type
	$("table.tbl-tree tr.grand-parent").css("background-color","#AAAAAA").find("td.td-button").css({"background-color":"#FFFFFF"});
	$("table.tbl-tree tr.parent").css("background-color","#BABABA").find("td.td-button").css("background-color","#FFFFFF");
	$("table.tbl-tree tr.sub-parent").css("background-color","#DEDEDE").find("td.td-button").css("background-color","#FFFFFF");
	
	//General styling
	$("span.count").css({"font-size":"80%","margin-top":"-3px"});
	$("table.tbl-tree th").css({"padding":"10px"});

	
	$("button.action-button").click(function() {
		AssistHelper.processing();
		var i = $(this).parent("td").attr("object_id");
		var t = $(this).parent("td").attr("object_type");
		var dta = "object_type="+t+"&object_id="+i;
//		AssistHelper.hideProcessing();
		if(t=="contract" && !$(this).hasClass("view-btn")) {
			$("div#dlg_edit_contract").dialog("open");
			$("div#dlg_edit_contract button.close-btn").blur();
		} else {
			$dlg = $("div#dlg_child");
			var act = "view";
			if($(this).hasClass("edit-btn")) {
				act = "edit";
			} else if($(this).hasClass("add-btn")) {
				act = "add";
			}
			var obj = "deliverable";
			if(t=="contract" || t=="action") { obj = t; }
			//var obj = (t=="action" ? "action" : "deliverable");
			var heading = AssistString.ucwords(act)+" "+object_names[obj];
			var parent_id = $(this).attr("parent_id"); 
			var url = "new_"+obj+"_"+act+"_object.php?display_type=dialog&object_id="+parent_id;
			
			$dlg.dialog("option","title",heading);
			//$dlg.find("iframe").prop("src",url);
			$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
			//$dlg.dialog("open");
		}
	});
	
	
	$("div button.close-btn").button({
		icons: { primary: "ui-icon-closethick" },
		text: false
	}).click(function(e) {
		e.preventDefault();
		$(this).parent("span").parent("div").dialog("close");
	}).removeClass("ui-state-default").addClass("ui-button-state-default");
	
	
	//AssistHelper.hideDialogTitlebar("class","dlg-view");
	
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			$(this).dialog('option','width',my_width);
			$(this).dialog('option','height',my_height);
		}
	});	
	
	
	
});


function dialogFinished(icon,result) {
	if(icon=="ok") {
		document.location.href = 'new_contract_edit_object.php?object_id=<?php echo $object_id; ?>&r[]=ok&r[]='+result;
	} else {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon,result);
	}
}

</script>