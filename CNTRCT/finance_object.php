<?php

$_REQUEST['form_action'] = "add";



function drawDeliverablesTable($f,$field,$drawdowns,$deliverable_drawdowns,$add_deliverables,$current) {
	global $helper;
	global $displayObject;
	
	if($f=="FINANCE_DRAWDOWN") {
		global $del_drawdowns;
		global $form_items;
		global $form_object_id;
	}
	
	$on = $helper->getObjectName($f);
					$echo="";
					$echo.="
					<table class='tbl_finance_deliverables finance-list $f ' >
						<tr>
							<th></th>
							<th>Deadline</th>
							<th>Responsible<br />Person</th>
							<th>Action<br />Progress</th>";
					if($f=="FINANCE_DRAWDOWN") {
						$echo.= "<th width=120px> $on </th>
							</tr>";
					} else {
						$echo.="
							<th width=120px>Current $on </th>
							<th width=120px>New $on </th>
							<th width=120px>New Total $on </th>
						</tr>";
					}
						$totals = 0;
					foreach($drawdowns as $di => $dd) {
						if(isset($deliverable_drawdowns['dd'][$di]) && count($deliverable_drawdowns['dd'][$di])>0) {
							$echo.= "
							<tr>
								<th class=th2>".$dd['name']."</th>
							</tr>
							";
							foreach($deliverable_drawdowns['dd'][$di] as $deli) {
								$deld = $add_deliverables[0][$deli];
								if($deld['del_type']=="DEL") {
									$v = isset($current[$deli][$field]) ? $current[$deli][$field] : 0;
									$totals+=$v;
									$echo .="
									<tr obj_id=".$deli.">
										<td>".$deld['reftag'].": ".$deld['name']." <span class=float><button class='action-button view-btn' parent_id=".$deli.">View</button></span></td>
										<td>".$displayObject->getDateForDisplay($deld['deadline'])."</td>
										<td>".$deld['responsible_person']."</td>
										<td>".$displayObject->getPercentageForDisplay($deld['action_progress'])."</td>";
									if($f=="FINANCE_DRAWDOWN") {
									$echo.= "
											<td class=center>".(isset($del_drawdowns['del'][$deli]) && $del_drawdowns['del'][$deli]!=$form_object_id ? $form_items[$del_drawdowns['del'][$deli]]['name'] : "<input type=checkbox name=dd[] value=".$deli." ".(isset($del_drawdowns['del'][$deli]) && $del_drawdowns['del'][$deli]==$form_object_id ? "checked=checked" : "")." />" )."</td>
										</tr>";
									} else {
									$echo.= "
											<td>".$displayObject->getCurrencyForDisplay($v,false)."</td>
											<td><input type=text value=0 name=del[".$deli."][$f] size=15 class='".$f."' del_type=DEL parent_id=0 /></td>
											<td>".$displayObject->getCurrencyForDisplay($v,false)."</td>
										</tr>";
									}
								} else {
									$children = array_flip(array_keys($add_deliverables[$deli]));
									$child_vals = array_intersect_key($current, $children);
									$x = 0;
									foreach($child_vals as $c) {
										$x+=isset($c[$field]) ? $c[$field] : 0;
									}
									$totals+=$x;
									$echo.= "
									<tr obj_id=".$deli.">
										<td>".$deld['reftag'].": ".$deld['name']." <span class=float><button class='action-button view-btn' parent_id=".$deli.">View</button></span></td>
										<td>".$displayObject->getDateForDisplay($deld['deadline'])."</td>
										<td>".$deld['responsible_person']."</td>
										<td>".$displayObject->getPercentageForDisplay($deld['action_progress'])."</td>";
									if($f=="FINANCE_DRAWDOWN") {
										$echo.= "
											<td class=center>".(isset($del_drawdowns['del'][$deli]) && $del_drawdowns['del'][$deli]!=$form_object_id ? $form_items[$del_drawdowns['del'][$deli]]['name'] : "<input type=checkbox name=dd[] value=".$deli." />" )."</td>
											</tr>";
									} else {
										$echo.= "
											<td>".$displayObject->getCurrencyForDisplay($x,false)."</td>
											<td class='td_parent_".$f."_".$deli."' >0.00</td>
											<td>".$displayObject->getCurrencyForDisplay($x,false)."</td>
										</tr>";
									}
									if(isset($add_deliverables[$deli])) {
										foreach($add_deliverables[$deli] as $ci => $cdel) {
											$v = isset($current[$ci][$field]) ? $current[$ci][$field] : 0; 
											$echo .="
											<tr class=sub obj_id=".$ci.">
												<td class=i>&nbsp;&nbsp;+&nbsp;".$cdel['reftag'].": ".$cdel['name']." <span class=float><button class='action-button view-btn' parent_id=".$ci.">View</button></span></td>
												<td>".$displayObject->getDateForDisplay($cdel['deadline'])."</td>
												<td>".$cdel['responsible_person']."</td>
												<td>".$displayObject->getPercentageForDisplay($cdel['action_progress'])."</td>";
									if($f=="FINANCE_DRAWDOWN") {
										$echo.= "
											<td>-</td>
										</tr>";
									} else {
										$echo.= "
												<td>".$displayObject->getCurrencyForDisplay($v,false)."</td>
												<td class='td_sub_".$f." td_sub_parent_".$deli."'><input type=text value=0 name=del[".$ci."][$f] size=15 class='".$f."' del_type=SUB parent_id=".$deli." /></td>
												<td>".$displayObject->getCurrencyForDisplay($v,false)."</td>
											</tr>";
									}
										}
									}
								}
							}
						}
					}
					if($f!="FINANCE_DRAWDOWN") {
						$echo.= "
						<tr class=total>
							<td class='b right' colspan=4>Total:</td>
							<td>".$displayObject->getCurrencyForDisplay($totals,false)."</td>
							<td class=td_".$f.">0.00</td>
							<td>".$displayObject->getCurrencyForDisplay($totals,false)."</td>
						</tr>";
					}
					$echo.="
					</table>";
					echo $echo;
					//return $totals;
}










//echo "<P>:".$helper->getActivityName("edit").":</p>";









$tab = strtoupper($_REQUEST['tab']);

$form_object_type = "FINANCE_".$tab;
$object_type = $tab;

switch($tab) {
	case "BUDGET":
		$formObject = new CNTRCT_FINANCE_BUDGET();
		break;
	case "ADJUSTMENT":
		$formObject = new CNTRCT_FINANCE_ADJUSTMENT();
		break;
	case "EXPENSE":
		$formObject = new CNTRCT_FINANCE_EXPENSE();
		break;
	case "INCOME":
		$formObject = new CNTRCT_FINANCE_INCOME();
		break;
	case "RETENTION":
		$formObject = new CNTRCT_FINANCE_RETENTION();
		break;
	case "DRAWDOWN":
		$formObject = new CNTRCT_FINANCE_DRAWDOWN();
		break;
}

$form_object_id = 0;
$parent_object_type = "CONTRACT";
$parentObject = new CNTRCT_CONTRACT();
$parent_object_id = $object_id;
$page_action = "ADD";
$page_redirect_path = $page_redirect_path."&tab=".strtolower($tab)."&";

$edit_list_objects = $formObject->getListObjects($parent_object_id);

//$helper->arrPrint($current);

/*echo $formObject->getPrimaryFinanceFieldName();
echo "<br />".$formObject->getDeliverablePrimaryFinanceFieldName();*/
$current = array('current'=>array(),'edit'=>array());
$delObject = new CNTRCT_DELIVERABLE();
if($tab!="DRAWDOWN"){
	$ddObject = new CNTRCT_FINANCE_DRAWDOWN();
	$current = $formObject->getDeliverableSummaryData($parent_object_id);
	
	$drawdowns = $ddObject->getAllItems($object_id);
	$deliverable_drawdowns = $ddObject->getDeliverableDrawDowns();
	if(!isset($drawdowns[0])) {
		$unspecified_dd = array(array('id'=>0,'name'=>$helper->getUnspecified(),'comment'=>"",'reftag'=>""));
		$drawdowns = $drawdowns + $unspecified_dd;
	}
	
	$delObject = new CNTRCT_DELIVERABLE();
	$add_deliverables = $delObject->getOrderedObjects($parent_object_id);
	
	$deliverable_drawdowns['dd'][0] = array();
	foreach($add_deliverables[0] as $i=>$d) {
		if(!isset($deliverable_drawdowns['del'][$i])) {
			$deliverable_drawdowns['dd'][0][] = $i;
		}
	}
} else {
	$a_deliverables = $delObject->getCategoryGroupedOrderedObjects($parent_object_id);
	$form_items = $formObject->getAllItems($parent_object_id);
	$del_drawdowns = $formObject->getDeliverableDrawDowns($parent_object_id);
	$listObject = new CNTRCT_LIST("deliverable_category");
	$cate_items = $listObject->getListItems();
	if(!isset($cate_items[0])) {
		$unspecified_item = array(array('id'=>0,'name'=>$helper->getUnspecified()));
		$cate_items = array_merge($unspecified_item,$cate_items);
	}
	$drawdowns = $cate_items;
	
	$add_deliverables = array(
		0=>array()
	);
	
	$deliverable_drawdowns = array(		'del' =>array(),		'dd'=>array(),	);
	foreach($a_deliverables['main'] as $ci => $cd) {
		foreach($cd as $di => $d) {
			$deliverable_drawdowns['del'][$di] = $ci;
			$deliverable_drawdowns['dd'][$ci][] = $di;
			$add_deliverables[0][$di] = $d;
		}
	}
	//$add_deliverables = array_merge($add_deliverables,$a_deliverables['subs']);
	foreach($a_deliverables['subs'] as $si => $s) {
		$add_deliverables[$si] = $s;
	}	
}

//$headObject = new CNTRCT_HEADINGS();
//$del_headings = $headObject->getMainObjectHeadings("FINANCE_DELIVERABLE_".$object_type,"ALL","MANAGE","",true);


?>
<div id="accordion" class=accordion>
	<h3 id=head_initiate next_index=1>Initiate</h3>
	<div id=initiate>
		<?php 
		if(isset($_REQUEST['r']) && (!isset($_REQUEST['accordian']) || $_REQUEST['accordian']=="initiate")) {
			ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); 
		}
		
		$form_code = $displayObject->getFinanceObjectFormTag();
		//$form_code = array('name'=>"frm_object_0",'display'=>"<form>");
		echo $form_code['display'];
		
		?>		
<!--		<table class=tbl-container width=100%>
			<tr>
				<td id=td_main_object_details>
					

					
				</td>
				<td width=25px id=td_spacer></td>
				<td id=td_form_to_be_submitted>
					
					
					
					
					
				</td>
			</tr>
</table> -->
		<?php
		
		//$helper->arrPrint($drawdowns);
		//$helper->arrPrint($deliverable_drawdowns);
		//$helper->arrPrint($add_deliverables);
		
		?>

		<div id=div_object_form class=float style='background-color: #FFFFFF;  width:48%'>
					<h2><?php echo $helper->getActivityName("add")." ".$helper->getObjectName($form_object_type); ?></h2>
					<?php
					$form_display = $displayObject->getFinanceObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$form_code['name']);
					echo $form_display['display'];
					$js.=$form_display['js'];
					$end_display = $form_display['end_display'];
					?>					

		</div>
		<div id=div_contract style='background-color: #FFFFFF; border: 1px solid #999999; width:48%; padding: 5px;'>
			
					<?php
					$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					$js.=$contract['js'];
					if($tab!="DRAWDOWN"){
						$summary_table = $displayObject->getSummaryTable($form_object_type, $formObject, $parent_object_id);
						echo $summary_table['display'];
						$js.=$summary_table['js'];
					}
					?>
								
			
		</div>
		<div id=div_deliverables_form style='background-color:#FFFFFF; clear: both;'>
					<?php
					echo "<h2>".$helper->getDeliverableObjectName(true)."</h2>";
					
					//$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					$finance_headings = $formObject->getDeliverableFinanceFields();
$f_count = 0;
foreach($finance_headings as $f) {
	if($f_count==0) {
		$field = $formObject->getDeliverablePrimaryFinanceFieldName();
	} elseif($f_count==1) {
		$field = $formObject->getDeliverableSecondaryFinanceFieldName();
	} else {
		$field = $formObject->getDeliverableExtraFinanceFieldName();
	}
			drawDeliverablesTable($f,$field,$drawdowns,$deliverable_drawdowns,$add_deliverables,$current);
			$f_count++;
		} //end foreach finance_headings
					echo $end_display; ?>
		</div>
		</form>
	</div>

	<h3 id=head_edit next_index=0>Edit</h3>
	<div id=edit> <?php
		if(isset($_REQUEST['r']) && (isset($_REQUEST['accordian']) && $_REQUEST['accordian']=="edit")) {
			ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); 
		}
		?>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					//$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					//$js.=$contract['js'];
					?>

				</td>
				<td width=4%>
				</td>
				<td width=48%><?php
				if($tab!="DRAWDOWN"){
					echo $summary_table['display'];
				} ?></td>
			</tr>
			<tr>
				<td colspan=3>
					<?php
					$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list",'type'=>"button"));
					?>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id=dlg_child class=dlg-child title="Edit">
		<iframe id=ifr_form_display style="border:0px solid #FFFFFF;" src="">
			
		</iframe>
</div>
<div id=dlg_view_child class=dlg-child title="View">
		<iframe id=ifr_form_display style="border:0px solid #FFFFFF;" src="">
			
		</iframe>
</div>
<script type="text/javascript">
$(function() {
	var allow_negative = <?php echo ($tab=="ADJUSTMENT" ? "true" : "false"); ?>;
	var warn_msg = "There is an error with 1 or more of the values you have entered.  Please review any values highlighted above.<br />Please note that only numbers (0-9) and a decimal point (.) are permitted."; 
	
	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	//if(my_window['width']>1000) {
		//var my_width = 950;
	//} else {
		var my_width = my_window['width']*0.9;
		if(my_width>800) {
			var my_view_width = 800;
		} else {
			var my_view_width = my_width;
		}
	//}
	var my_height = my_window['height']-50;	
	var object_names = [];
	object_names['contract'] = "<?php echo $helper->getObjectName("contract"); ?>";
	object_names['deliverable'] = "<?php echo $helper->getObjectName("deliverable"); ?>";
	object_names['action'] = "<?php echo $helper->getObjectName("action"); ?>";
	var tab = "<?php echo $tab; ?>";
	
//	$("h3#head_edit, div#edit").hide();
	
	$('#accordion').accordion({
		active: <?php echo (isset($_REQUEST['accordian']) && $_REQUEST['accordian']=="edit" ? 1 : 0); ?>,
		event: false,
		heightStyle: 'fill'
	}).children('h3').css({'font-family':'Verdana','font-weight':'bold','font-size':'110%','line-height':'125%'});
	$('h3').click(function() {
		if($(this).parent().prop('id')=='accordion') {
			var next_index = $(this).attr('next_index');
			$relatedContent = $(this).next();
			if($relatedContent.is(':visible')) {
				if(next_index==0) {
					$("#accordion").accordion("option","active",0);
				} else {
					$("#accordion").accordion("option","active",1);
				}
			} else {
				if(next_index==0) {
					$("#accordion").accordion("option","active",1);
				} else {
					$("#accordion").accordion("option","active",0);
				}
			}
		}
	});
	var my_screen = AssistString.substr($("div#initiate").css("width"),0,-2)*1;
	var table_width = (AssistString.substr($("table.tbl_finance_deliverables:first").css("width"),0,-2))*1;
	var form_width = my_screen*0.47;
	if(form_width>table_width) {
		var div_width = form_width;
	} else {
		var div_width = table_width+3;
	}
	//$("table.tbl-container td#td_form_to_be_submitted").css("width",(div_width)+"px").find("table.tbl_finance_deliverables").css("width","100%");//.addClass("float").find("table.tbl_finance_deliverables").css("width","100%");
	$("#div_deliverables_form").css("width",(div_width)+"px").addClass("float").find("table.tbl_finance_deliverables").css("width","100%");
	

	$("table.tbl_finance_deliverables").each(function() {
		$(this).find("tr:not(.total)").each(function() {
			$(this).find("td:lt(3)").removeClass("right");
			$(this).find("td:eq(3)").removeClass("right").addClass("center");
		});
		var x = $(this).find("tr:first th").length;
		$(this).find("tr th.th2").prop("colspan",x).css("text-align","left");
		$(this).find("tr.sub td").css("background-color","#f5f5f5");
		//$(this).find("tr:last").children("td").removeClass("center").addClass("right");
	});
if(tab!="DRAWDOWN") {
	$("table.tbl_finance_deliverables input:text").css("text-align","right");
	$("table.tbl_finance_deliverables:gt(0)").css("margin-top","10px");
} else {
	$("table.tbl_finance_deliverables tr:gt(0)").find("td:last").css("text-align","center");
}
	
	
	$("button.view-btn").button({
		icons: {primary: "ui-icon-extlink"},
	}).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
		.children(".ui-button-text").css({"font-size":"80%"});//.css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
	
	$("button.view-btn").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr("parent_id");
		var dta = "object_type=DELIVERABLE&object_id="+i;
		//$dlg = $("div#dlg_view_child");
		var act = "view";
		var obj = "deliverable";
		var heading = AssistString.ucwords(act)+" "+object_names[obj];
		var parent_id = $(this).attr("parent_id"); 
		var url = "new_"+obj+"_"+act+"_object.php?display_type=dialog&object_id="+parent_id;
		//console.log(url);
		dialogDisplay("view",url,heading);
		
		//$dlg.dialog("option","title",heading);
		//$dlg.find("iframe").prop("src",url);
		//$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
		//$dlg.dialog("open");
	});
		
	
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			if($(this).prop("id") == "dlg_view_child") {
				$(this).dialog('option','width',my_view_width);
				$(this).children("iframe:first").css("width",(my_view_width-30)+"px");
			} else {
				$(this).dialog('option','width',my_width);
				$(this).children("iframe:first").css("width",(my_width-20)+"px");
			}
			$(this).dialog('option','height',my_height);
			$(this).children("iframe:first").css("height",(my_height-50)+"px");
		},
		resizeStop: function() {
			mw = $(this).dialog('option','width');
			mh = $(this).dialog('option','height');
			$(this).children("iframe:first").css("width",(mw-20)+"px");
			$(this).children("iframe:first").css("height",(mh-50)+"px");
			//$(this).children("iframe:first").children("div").css("width",(mw-40)+"px");
		}
	});
	
	
	
	
	
	$("table.tbl_finance_deliverables input:text").blur(function() {
		var $table = $(this).parent().parent().parent(); //input -> td -> tr -> table
		var current_total = AssistString.calcPureNumberString($table.children("tr:last").children("td:last").prev().prev().html());
	//SET VARIABLES & RESTORE SAVE BUTTON + WARNING
		var err = false;
		var total_err = false;
		var my_class = $(this).attr("class");
		$(this).removeClass("required");
		$("#btn_save").prop("disabled","").removeClass("disabled-button").removeClass("ui-button-state-default").addClass("ui-state-ok").parent().find("#warning_msg").html("");
		var t = $(this).val();
		t = t.length==0 ? "0.00" : t;
		var n = "error";
		var $parent = $(this).parent().parent();	//input:text -> td -> tr
	//CHECK FOR DECIMAL PLACE
		//IF PARSEFLOAT = ERROR OR MORE THAN 1 DECIMAL = ERROR	
		if(isNaN(parseFloat(t)) || t.indexOf(".")==0 || (t.indexOf(".")!=t.lastIndexOf("."))) {
			err = true;
			total_err = true;
		} else {
		//ADD 0 IF NOT PRESENT
			var t2 = checkDecimal(t);
		//REMOVE INVALID CHARACTERS
			n = AssistString.calcPureNumberString(t2,allow_negative);
		}
		//IF ERROR ALREADY EXISTS OR THE PURE NUMBER DOES NOT EQUAL THE ORIGINAL CAPTURED VALUE
		if(err || parseFloat(n)!=t) {
			//DISPLAY ERROR
			err = true;
			total_err = true;
			$(this).addClass("required");
			$("#btn_save").attr("disabled","disabled").removeClass("ui-state-ok").addClass("ui-button-state-default").addClass("disabled-button").parent().find("#warning_msg").html(AssistHelper.getHTMLResult("error",warn_msg,"error")).addClass("red");
		} else {
		//VALIDATE OTHER INPUTs FOR EXISTING ERRORS
			$table.find("input:text").each(function() {
				if($(this).hasClass("required")) {
					$("#btn_save").attr("disabled","disabled").removeClass("ui-state-ok").addClass("ui-button-state-default").addClass("disabled-button").parent().find("#warning_msg").html(AssistHelper.getHTMLResult("error",warn_msg,"error")).children().addClass("red");
					total_err = true;
					return false;
				}
			});
		}
		//CHECK DEL_TYPE
		if($(this).attr("del_type")=="SUB") {
			var pi = $(this).attr("parent_id");
			calculateParentTotal(pi,my_class);
		}
		
		//IF NO LOCAL ERROR HAS BEEN DISCOVERED
		if(!err) {
			//CALCULATE THE ROW TOTAL
			var current = parseFloat(AssistString.calcPureNumberString($parent.find("td:last").prev().prev().html(),allow_negative));
			var my_new = current;
			//LOOP THROUGH EACH INPUT:TEXT IN ROW
			$parent.find("input:text").each(function() {
				//IF NOT PREVIOUSLY ERR'D - Add value to row total
				if(!$(this).hasClass("required") && !err) {
					my_new+=parseFloat($(this).val());
				//ELSE IF IT IS ERR'D AND IS IN CURRENT COLUMN
				} else if($(this).hasClass(my_class)) {
					err = true;
					total_err = true;
				//ELSE IF IT IS ERR'D AND IS NOT IN CURRENT COLUMN
				} else {
					total_err = true;
				}
			});
		}
		//IF NO LOCAL ERROR WAS DISCOVERED
		if(!err) {
			//DISPLAY THE ROW TOTAL
//console.log("my_new = "+my_new);
			my_new = checkDecimal(my_new);
			$parent.find("td:last").html(AssistString.formatNumber(my_new)).removeClass("red");
			//LOOP THROUGH THE COLUMN INPUTS & CALCULATE THE COLUMN TOTAL
			var total = 0;
			$table.find("input:text."+my_class).each(function() {
				if(!$(this).hasClass("required")) {
					total+=parseFloat($(this).val());
				} else {
					err = true;
				}
			});
			//IF NO COLUMN ERROR DISCOVERED
			if(!err) {
				//DISPLAY THE COLUMN TOTAL
				total = checkDecimal(total);
				$table.find("tr:last td.td_"+my_class).css("color","").html(AssistString.formatNumber(total));
				//LOOP THROUGH ANY OTHER COLUMN INPUTS TO CALCULATE THE GRAND TOTAL
//console.log("current_total = "+current_total);
				var new_total = parseFloat(current_total) + parseFloat(total);
				$table.find("input:text").not("."+my_class).each(function() {
					if(!$(this).hasClass("required")) {
						new_total+=parseFloat($(this).val());
					} else {
						total_err = true;
					}
				});
				//IF NO GENERAL ERR DISCOVERED - DISPLAY GRAND TOTAL
				if(!total_err) {
					new_total = checkDecimal(new_total);
					$table.find("tr:last td:last").css("color","").html(AssistString.formatNumber(new_total));
				} else {
					$table.find("tr:last td:last").html("ERROR").css("color","#cc0001");
				}
			}
		} else {
		//ELSE IF LOCAL ERROR DISCOVERED - DISPLAY ERROR MESSAGE IN ROW, COLUMN & GRAND TOTAL
			$parent.find("td:last").html("ERROR").addClass("red");
			$table.find("tr:last td.td_"+my_class).html("ERROR").css("color","#cc0001");
			$table.find("tr:last td:last").html("ERROR").css("color","#cc0001");
		}
	});
	//Function to add zero's after decimal point if they don't exist - returns STRING
	function checkDecimal(t) {
		t = t+"";
		if(t.indexOf(".")<0) {
			t=t+".00";
		} else {
			var x = AssistString.explode(".",t);
			if(x[1].length<2) { x[1]+="0"; }
			t = AssistString.implode(".",x);
		}
		return t;
	}
	//Function to calculate the total for a parent deliverable
	function calculateParentTotal(parent_id,my_class) {
		var $parent = $("table.tbl_finance_deliverables."+my_class+" td.td_parent_"+my_class+"_"+parent_id);
		var p_total = 0;
		var err = false;
		$("table.tbl_finance_deliverables."+my_class+" td.td_sub_"+my_class+".td_sub_parent_"+parent_id).children("input:text").each(function() {
			if($(this).hasClass("required")) {
				err = true;
				return false;
			} else {
				p_total+=parseFloat($(this).val());
			}
		});
		if(!err) {
			p_total = checkDecimal(p_total);
			$parent.removeClass("red").html(AssistString.formatNumber(p_total));
		} else {
			$parent.html("ERROR").addClass("red");
		}
	}
	
	
	
	
	
	
	$(".btn_list").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$dlg = $("#dlg_child");



		var i = $(this).attr("ref");
		var dta = "object_type=DELIVERABLE&object_id="+i;
		var act = "edit";
		var obj = "deliverable";
		var heading = "<?php echo $helper->getActivityName("edit"); ?>";
		var url = "finance_object_form.php?display_type=dialog&object_id=<?php echo $parent_object_id; ?>&tab=<?php echo $tab; ?>&form_action=edit&edit_object_id="+i;
		
		//$dlg.prop("title","<?php $helper->getActivityName("edit"); ?>");
		$dlg.dialog("option","title",heading);

		$dlg.find("iframe:first").prop("src",url);
		//$dlg.dialog("open");
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
});
function dialogFinished(icon,result) {
	if(icon=="ok") {
		document.location.href = '<?php echo $page_redirect_path; ?>accordian=edit&r[]=ok&r[]='+result;
		//alert(result);
	} else {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon,result);
	}
}

function dialogDisplay(action,url,heading) {
	$(function() {
		switch(action) {
			case "view":
				var d = "dlg_view_child";
				break;
			case "edit":
			default:
				var d = "dlg_child";
				break;
		}
		url = url+"&dlg="+d;
		$dlg = $("div#"+d);
		$dlg.dialog("option","title",heading);
		$dlg.find("iframe").prop("src",url);
		$dlg.dialog("moveToTop");
	});
}

</script>