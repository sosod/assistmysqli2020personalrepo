<?php
require_once("inc_header.php");

$parent_object_type = "CONTRACT";
$parent_object_id = $_REQUEST['object_id'];
$page_section = "MANAGE";
$page_action = "UPDATE";
$page_redirect_path = "manage_assurance_contract_add.php?object_id=".$parent_object_id."&";
switch($parent_object_type) {
	case "CONTRACT":
		$childObject = new CNTRCT_ASSURANCE_CONTRACT();
		$child_objects = $childObject->getAObject($parent_object_id);
//		ASSIST_HELPER::arrPrint($child_objects);
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getObjectName("CONTRACT")." Assurance";
		$parentObject = new CNTRCT_CONTRACT($parent_object_id);
		$child_object_type = "CONTRACT_ASSURANCE";
		$child_object_id = 0;
		//$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = 0;
		break;
	case "DELIVERABLE":
		$childObject = new CNTRCT_ACTION();
		$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$parent_object_id)); 
		$child_redirect = "new_action_edit.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName();
		$parentObject = new CNTRCT_DELIVERABLE($parent_object_id);		
		$child_object_type = "ACTION";
		$child_object_id = 0;		
		$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = $object_id;
		break;
	case "ACTION":
		$child_redirect = "";
		$parent_id = $object_id;
		$child_objects>0;
		break;
}

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class=tbl-container>
	<tr>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="CONTRACT"));
			$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>New <?php echo $child_name; ?></h2><?php 
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id,"Add", $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<?php if(count($child_objects)>0) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php //$js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
			<?php $js.=$displayObject->drawListTable($child_objects,false); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<div id='edit_dialog' style='display:none;'>
	
</div>
<iframe style='width:0px; height:0px' name='edit_frame' id='edit_frame' src=''>

</iframe>
<form name="hi" method="POST">
	<input type=hidden name=s value=CON />
	<input type=hidden name='parent_id' value=<?php echo $parent_object_id; ?> />
	<input type=hidden name=child_id value=0 />
</form>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>
	
	$("input:button.btn_edit").click(function() {
		
		var i = $(this).attr("ref");
		$("form[name=hi] input:hidden[name=child_id]").val(i);
		var url = 'manage_assurance_edit_frame.php';
		$("form[name=hi]").prop("target","edit_frame").prop("action",url).submit();
	//	var result = AssistHelper.doAjax("inc_controller.php?action=CONTRACT_ASSURANCE.Get&id="+i);
	//	var date = result.ca_date_tested;
//		console.log(result);
	//	iframe.document.src = url;
		
		//$(content);
		
		
		/*
		var editer = $(content).find("div[name='edcon']");
		$("#edit_dialog #ca_response").val(result.ca_response);
		$("#edit_dialog #ca_date_tested").val(date).datepicker({
			dateFormat:"yy-mm-dd",
			defaultDate:date,
		});
		$("#edit_dialog [name='object_id']").val(result.ca_id);
		$inputs = $("#edit_dialog td :last").find("input");
		var firstrun = true;
		$inputs.each(function(){
			if($(this).prop("class")=="close_dlg"){
				firstrun = false;
			}
		});
		if(firstrun){
			$("#edit_dialog td :last").append("<input type='button' value='Cancel' class='close_dlg' id='close_"+i+"'></input>");
		}
		*/
		//console.log($(this).prop("id"));
	});
	
	$(document).on("click",".close_dlg", function(){
		$("#edit_dialog").dialog("close");
	});
	
	$(document).on("click","#btn_save",function(){
		//alert("hi");
		//console.log($(this).siblings(":hidden").val());
		var data = AssistForm.serialize($(this).parents("form"));
		var result = AssistHelper.doAjax("inc_controller.php?action=CONTRACT_ASSURANCE.Edit",data);
		//console.log(result);
	});
	
});

function finished(){
	$(function(){
		
		var iframe = document.getElementById("edit_frame");
		
		var content = iframe.contentWindow.document.body.innerHTML;
		//console.log($(content));
		document.getElementById('edit_dialog').innerHTML = content;
		$("#edit_dialog tr:last").hide();
		$rid = $("#edit_dialog tr:last").find("[name=object_id]");
		$id = $rid.val();
		$("#edit_dialog").dialog({
			width:"575px",
			buttons:{
				Cancel:function(){
					$(this).dialog("close");
				},
				'<?php echo $helper->getActivityName("SAVE"); ?>':function(){
					//var id = document.getElementByTagName("child_id").innerHTML; 
					//alert($id);
					$form = $(this).find("[name=frm_object]");
					var data = AssistForm.serialize($form);
					var result = AssistHelper.doAjax("inc_controller.php?action=CONTRACT_ASSURANCE.Edit",data);
					console.log(result);
				}
			}
		});
		AssistHelper.hideDialogTitlebar("id","edit_dialog");
		AssistHelper.formatDialogButtons($("#edit_dialog"),1,AssistHelper.getGreenCSS());
	});

}
</script>
