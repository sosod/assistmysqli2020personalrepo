<?php
/**
 * To manage the ASSURANCE function
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_ASSURANCE extends CNTRCT {
    
	protected $object_id = 0;
	protected $object_details = array();
    
    //protected $progress_status_field = "_status_id";
	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	//protected $name_field = "_name";
	//protected $deadline_field = "_planned_date_completed";
	//protected $owner_field = "_owner_id";
	//protected $manager_field = "_manager";
	//protected $authoriser_field = "_authoriser";
	//protected $attachment_field = "_attachment";
	//protected $progress_field = "_progress";
	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "ASSURANCE";
    const TABLE = "assurance";
    const TABLE_FLD = "";
    //const REFTAG = "CAS";
	const LOG_TABLE = "assurance";
	/**
	 * STATUS CONSTANTS
	 */
	//const CONFIRMED = 32;
	//const ACTIVATED = 64;
	/**
	 * ASSESSMENT TYPE CONSTANTS
	 */
	//const QUALITATIVE = 1024;
	//const QUANTITATIVE = 2048;
	//const OTHER = 4096;
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field; //parent_field is set in child class
		//$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		//$this->progress_field = self::TABLE_FLD.$this->progress_field;
		//$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		//$this->manager_field = self::TABLE_FLD.$this->manager_field;
		//$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		//$this->owner_field = self::TABLE_FLD.$this->owner_field;
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var,$fld="") {
		unset($var['attachments']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}else if($key == "_contract_id"){
				$newkey = $fld.$key;
				$var[$newkey]=$v;
				unset($var[$key]);
			}
		}
		unset($var['object_id']);
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
//		$var[$this->getTableField().'_progress'] = 0;
//		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|assurance| added.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
				//'progress'	=> 0,
				//'status_id'	=> 1,		
			);
			/* moved to controller for attachment functionality */
			//$this->addActivityLog("contract", $log_var);
			$result = array(
				0=>"ok",
				1=>"New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully added.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}


	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		if($this->checkIntRef($object_id)) {
			//$old_object = $this->getRawObject($object_id);
			$result = $this->editMyObject($var,$attach);
			return $result;	
		}
		return array("error","An error occurred.  Please try again.");
	}	
	
	

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }

	public function getList($section,$options=array()) {
		return $this->getMyList("CONTRACT", $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject("ASSURANCE", $id,$options);
	}
	
	
	public function getRawUpdateObject($id){
		return $this->getRawObject($id);
	}
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE contract_id = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORTING",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a contract
	 */ 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { }//return $this->deadline_field; }
	 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>