<?php
  
class CNTRCT_FINANCE_BUDGET extends CNTRCT_FINANCE {
	
	protected $table_name = "_finance_budget";
	protected $field_prefix = "fb";
	
	protected $ref_tag = "CFB";
	
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_budget_amount";
	protected $secondary_finance_field = "";
	protected $extra_finance_field = "";
	
	protected $del_table_name = "_deliverable_budget";
	protected $del_field_prefix = "db";
	protected $del_id_field = "_id";
	protected $del_parent_field = "_fb_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "_budget";
	protected $del_secondary_finance_field = false;
	protected $del_extra_finance_field = false;
	
	protected $non_edit_fields = array();
	
	protected $js = "";
	
	protected $object_type = "FINANCE_BUDGET";

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
		//$this->non_edit_fields[] = $this->getPrimaryFinanceFieldName();
		//$this->non_edit_fields[] = $this->getTableField()."_supplier_id";
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		//return $this->addMyObject($var);
		$deliverables = $var['del'];
		unset($var['del']);
		$return = $this->addMyObject($var);
		if($return[0]=="ok") {
			$id = $return['object_id'];
			if(count($deliverables)>0) {
				$sql_array = array();
				foreach($deliverables as $di => $db) {
					$db = $db[$this->getMyObjectType()];
					$sql_array[] = "(null, ".$di.", ".$id.", ".(strlen($db)>0 ? $db : "0").", ".CNTRCT::ACTIVE.")"; 
				}
				$sql = "INSERT INTO ".$this->getDeliverableTableName()." VALUES ".implode(",",$sql_array);
				$this->db_insert($sql);
			}
		}
		return $return;
	}
	public function editObject($var) {
		//return array("error","function not programmed");
		
		$id = $var['object_id'];
		$deliverables = $var['del'];
		unset($var['del']);
		$return = $this->editMyObject($var);
		if($return[0]=="ok" || $return[0]=="info") {
			//$return[1] = serialize($return);
			//return $return;
			if(count($deliverables)>0) {
				$old = $return[3];
				$old_deliverables = $this->getDeliverableSummaryData($old[$this->getParentFieldName()],true,$id,true);
				$changes = array(
					'user'=>$this->getUserName(),
				);
				
				foreach($deliverables as $di => $db) {
					$d = $db[$this->getMyObjectType()];
					$o = isset($old_deliverables[$di][$this->getDeliverablePrimaryFinanceFieldName()]) ? $old_deliverables[$di][$this->getDeliverablePrimaryFinanceFieldName()] : 0;
					if(!is_numeric($d) || strlen($d)==0) {
						$d = 0;
					}
					if($d!=$o) {
						$sql = "UPDATE ".$this->getDeliverableTableName()." SET ".$this->getDeliverablePrimaryFinanceFieldName()." = ".(strlen($d)>0 ? $d : "0")." WHERE ".$this->getDeliverableParentFieldName()." = ".$id." AND ".$this->getDeliverableObjectIDFieldName()." = ".$di;
						$mar = $this->db_update($sql);
						if($mar>0) {
							$changes['|deliverable| '.$this->getDelRefTag().$di.' '.$this->getDeliverablePrimaryFinanceFieldName()] = array('to'=>$d,'from'=>$o);
						}
					}
				}
				
				if(count($changes)>1) {
					//$ch+anges["|deliverable| ".$this->getDeliverablePrimaryFinanceFieldName()] = $changes['deliverable'];
					//unset($changes['deliverable']);
					$log_var = array(
						'object_id'	=> $id,
						'changes'	=> $changes,
						'log_type'	=> CNTRCT_LOG::EDIT,
						'section'	=> $this->getMyObjectName(),
					);
					$this->addActivityLog($this->getMyLogTable(), $log_var);
				}
				$return[0] = "ok";
				$return[1] = "Changes saved successfully.";
			}
		}
		return $return;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***************************
	 * FINANCE_BUDGET functions - Deliverable related functions below
	 */

	
	/** 
	 * Function to determine the data to be displayed in the Finance Summary table
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  ('head'=>array of headings,'rows'=>array of values)
	 */
	public function getSummaryData($parent_object_id) {
		$data = array('head'=>array(),'rows'=>array());
		
		$headings = array(
			'total'=>array(
				'name'=>"Total Budget",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'deliverables'=>array(
				'name'=>"Budget Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'unallocated'=>array(
				'name'=>"Unallocated Budget",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
		);
		
		$data['head'] = $headings;
		
		
		$total_budget = $this->getRawRows($parent_object_id,"SUMMARY");
		$data['rows']['total'] = $total_budget[0][$this->getPrimaryFinanceFieldName()];
		
		$deliverable_total_budget = $this->getDeliverableRawRows($parent_object_id,"SUMMARY");
		$data['rows']['deliverables'] = $deliverable_total_budget[0][$this->getDeliverablePrimaryFinanceFieldName()];
		$data['rows']['unallocated'] = $data['rows']['total'] - $data['rows']['deliverables'];
		
		
		return $data;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*******************************************
	 * DELIVERABLE Functions
	 */

	 	
	
	
	
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>