<?php

class CNTRCT_FINANCE_EXPENSE extends CNTRCT_FINANCE {
	
	
	protected $object_type = "FINANCE_EXPENSE";
	protected $secondary_object_type = "FINANCE_RETENTION";
	protected $ref_tag = "CFE";

	protected $table_name = "_finance_expense";
	protected $field_prefix = "fe";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_total_amount";
	protected $secondary_finance_field = "_retention";
	protected $extra_finance_field = "";
	
	protected $del_table_name = "_deliverable_expense";
	protected $del_field_prefix = "de";
	protected $del_id_field = "_id";
	protected $del_parent_field = "_fe_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "_amount";
	protected $del_secondary_finance_field = "_retention_amount";
	protected $del_extra_finance_field = false;
	
	protected $non_edit_fields = array();


	protected $js = "";

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
		$this->del_headings[] = "FINANCE_RETENTION";
		//$this->non_edit_fields[] = $this->getPrimaryFinanceFieldName();
		//$this->non_edit_fields[] = $this->getSecondaryFinanceFieldName();
		//$this->non_edit_fields[] = $this->getTableField()."_supplier_id";
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		//return array("error","function not programmed");
		//return $this->addMyObject($var);
		$deliverables = $var['del'];
		unset($var['del']);
		$return = $this->addMyObject($var);
		if($return[0]=="ok") {
			$id = $return['object_id'];
			if(count($deliverables)>0) {
				$sql_array = array();
				foreach($deliverables as $di => $db) {
					$e = $db[$this->getMyObjectType()];
					$r = $db['FINANCE_RETENTION'];
					$sql_array[] = "(null, ".$di.", ".$id.", ".(strlen($e)>0 ? $e : "0").", ".(strlen($r)>0 ? $r : "0").", ".CNTRCT::ACTIVE.")"; 
				}
				$sql = "INSERT INTO ".$this->getDeliverableTableName()." VALUES ".implode(",",$sql_array);
				$this->db_insert($sql);
			}
		}
		return $return;
	}
	public function editObject($var) {
		//return array("error","function not programmed");
		
		$id = $var['object_id'];
		$deliverables = $var['del'];
		unset($var['del']);
		$return = $this->editMyObject($var);
		//development
		if($return[0]=="ok" || $return[0]=="info") {
			//$return[0]="info";
			//$return[1]=serialize($deliverables);
			//$return[1] = serialize($return);
			//return $return;
			if(count($deliverables)>0) {
				$old = $return[3];
				$old_deliverables = $this->getDeliverableSummaryData($old[$this->getParentFieldName()],true,$id,true);
				$changes = array(
					'user'=>$this->getUserName(),
					'deliverable'=>array(),
				);
				
				foreach($deliverables as $di => $db) {
					//PRIMARY
					$d = $db[$this->getMyObjectType()];
					$o = isset($old_deliverables[$di][$this->getDeliverablePrimaryFinanceFieldName()]) ? $old_deliverables[$di][$this->getDeliverablePrimaryFinanceFieldName()] : 0;
					//SECONDARY
					$ds = $db[$this->getMySecondaryObjectType()];
					$os = isset($old_deliverables[$di][$this->getDeliverableSecondaryFinanceFieldName()]) ? $old_deliverables[$di][$this->getDeliverableSecondaryFinanceFieldName()] : 0;
					if(!is_numeric($d) || strlen($d)==0) { 		$d = 0;				}
					if(!is_numeric($ds) || strlen($ds)==0) { 	$ds = 0;			}
					if($d!=$o || $ds!=$os) {
						$sql = "UPDATE ".$this->getDeliverableTableName()." 
								SET ".$this->getDeliverablePrimaryFinanceFieldName()." = ".(strlen($d)>0 ? $d : "0").",
								    ".$this->getDeliverableSecondaryFinanceFieldName()." = ".(strlen($ds)>0 ? $ds : "0")." 
								WHERE ".$this->getDeliverableParentFieldName()." = ".$id." AND ".$this->getDeliverableObjectIDFieldName()." = ".$di;
						$mar = $this->db_update($sql);
						if($mar>0) {
							if($d!=$o) {
								$changes['|deliverable| '.$this->getDelRefTag().$di.' '.$this->getDeliverablePrimaryFinanceFieldName()] = array('to'=>$d,'from'=>$o);
							}
							if($ds!=$os) {
								$changes['|deliverable| '.$this->getDelRefTag().$di.' '.$this->getDeliverableSecondaryFinanceFieldName()] = array('to'=>$ds,'from'=>$os);
							}
						}
					}
				}
				
				if(count($changes)>1) {
					//$ch+anges["|deliverable| ".$this->getDeliverablePrimaryFinanceFieldName()] = $changes['deliverable'];
					//unset($changes['deliverable']);
					$log_var = array(
						'object_id'	=> $id,
						'changes'	=> $changes,
						'log_type'	=> CNTRCT_LOG::EDIT,
						'section'	=> $this->getMyObjectName(),
					);
					$this->addActivityLog($this->getMyLogTable(), $log_var);
				}
				$return[0] = "ok";
				$return[1] = "Changes saved successfully.";
			}
		}
		return $return;
	}
	
	 
	
	
	
	
	
	
	
	
	
	/***************************
	 * FINANCE functions
	 */

	
	/** 
	 * Function to determine the data to be displayed in the Finance Summary table
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  ('head'=>array of headings,'rows'=>array of values)
	 */
	public function getSummaryData($parent_object_id) {
		$data = array('head'=>array(),'rows'=>array());
		
		$headings = array(
			'total'=>array(
				'name'=>"Total Expenses",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'deliverables'=>array(
				'name'=>"Expenses Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'unallocated'=>array(
				'name'=>"Unallocated Expenses",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
			'retention'=>array(
				'name'=>"Retention Held",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'retention_deliverables'=>array(
				'name'=>"Retention Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'retention_unallocated'=>array(
				'name'=>"Unallocated Retention",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
		);
		
		$data['head'] = $headings;
		
		
		$total = $this->getRawRows($parent_object_id,"SUMMARY");
		$data['rows']['total'] = $total[0][$this->getPrimaryFinanceFieldName()];

		$deliverable_total = $this->getDeliverableRawRows($parent_object_id,"SUMMARY");
		$data['rows']['deliverables'] = $deliverable_total[0][$this->getDeliverablePrimaryFinanceFieldName()];
		$data['rows']['unallocated'] = $data['rows']['total'] - $data['rows']['deliverables'];
		
		$ret_total = $this->getRawRows($parent_object_id,"SUMMARY",true);
		$data['rows']['retention'] = $ret_total[0][$this->getSecondaryFinanceFieldName()];

		$ret_deliverable_total = $this->getDeliverableRawRows($parent_object_id,"SUMMARY",true);
		$data['rows']['retention_deliverables'] = $ret_deliverable_total[0][$this->getDeliverableSecondaryFinanceFieldName()];
		$data['rows']['retention_unallocated'] = $data['rows']['retention'] - $data['rows']['retention_deliverables'];
		
		return $data;
	}
	
		
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>