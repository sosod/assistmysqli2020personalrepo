<?php
/**
 * To manage the ASSURANCE function
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_ASSURANCE_ACTION extends CNTRCT_ASSURANCE {
    
	protected $object_id = 0;
	protected $object_details = array();

	
    /*************
     * CONSTANTS
     */
    const TABLE = "action_assurance";
    const TABLE_FLD = "aa";
    const REFTAG = "CAAS";
	protected $id_field = "aa_id";
	protected $parent_field = "aa_action_id";
	protected $attachment_field = "aa_attachment";
    
    public function __construct($object_id=0) {
        parent::__construct();
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
    }
    
	public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    //public function getMyObjectType() { return self::OBJECT_TYPE; }
    //public function getMyLogTable() { return self::LOG_TABLE; }
	
	/*******************************
	 * CONTROLLER functions
	 */
	//public function addObject($var) { - parent class
	public function addObject($var,$fld="") {
		unset($var['attachments']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}else if($key == "_deliverable_id"){
				$newkey = $fld.$key;
				$var[$newkey]=$v;
				unset($var[$key]);
			}
		}
		unset($var['object_id']);
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|assurance| added.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
				//'progress'	=> 0,
				//'status_id'	=> 1,		
			);
			/* moved to controller for attachment functionality */
			//$this->addActivityLog("contract", $log_var);
			$result = array(
				0=>"ok",
				1=>"New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully added.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}


	//public function editObject($var,$attach=array()) { - parent class
	

	
    /*************************************
     * GET functions
     * */
     public function getObject($id){
     	$id = $id['id'];
     	$sql = "SELECT * FROM ".$this->getTableName()." WHERE aa_id = $id";
		return $this->mysql_fetch_one($sql);
     }
     
/*	 public function getAObject($id=0,$options=array()){
	 	//return $this->getMyList("ASSURANCE","MANAGE");
	 	$head = new CNTRCT_HEADINGS();
		$headarr = $head->getMainObjectHeadings("ASSURANCE","LIST");
		foreach($headarr as $key=>$val){
			foreach($val as $index=>$item){
				$data['head'][$index]=$item;
			}
		}		
		$rows = $this->getRawObject($id);
		//Sanitize rows data for display
		foreach($rows as $key=>$val){
			foreach($val as $index=>$item){
				if($index == "aa_action_id" ||$index == "aa_status" ||$index == "aa_insertuser" ||$index == "aa_insertdate"){
					unset($rows[$key][$index]);
				}else if($index == "aa_attachment" && strlen($item) > 0){
					$attach = unserialize($item);
					if($attach[0]['status'] == CNTRCT_LOG::ACTIVE){
						$attach = $attach[0];
						$link="<a href='../files/".$this->getCmpCode()."/CONTRACT/assurance/".$attach['system_filename']."'>".$attach['original_filename']."</a>";
						$rows[$key][$index]=$link;
					}
				}
			}
		}
		foreach($rows as $key=>$val){
			$rows[$val['aa_id']]=$val;
			unset($rows[$key]);
		}
		$data['rows']=$rows;
		return $data;
	 	return $this->getDetailedObject("ASSURANCE", $id,$options);
	 }*/
	 public function getAObject($id=0,$options=array()){
	 	//return $this->getMyList("ASSURANCE","MANAGE");
	 	$head = new CNTRCT_HEADINGS();
		$headarr = $head->getMainObjectHeadings("ASSURANCE","LIST","",$this->getTableField()."_"); 
		//echo $this->getTableField();
		//foreach($headarr as $key=>$val){
			foreach($headarr as $index=>$item){
				$data['head'][$index]=$item;
			}
		//}		
		$rows = $this->getRawListObject($id);
	//	ASSIST_HELPER::arrPrint($headarr);
	//	ASSIST_HELPER::arrPrint($rows);
		//Sanitize rows data for display
		$data = $this->formatRowDisplay(count($rows), $rows, $data, $this->getIDFieldName(), $head, $this->getRefTag(), $this->getStatusFieldName(), array('limit'=>false,'pagelimit'=>"",'start'=>0));
	//	ASSIST_HELPER::arrPrint($data);
		/*
		foreach($rows as $key=>$val){
			foreach($val as $index=>$item){
				if($index == "ca_contract_id" ||$index == "ca_status" ||$index == "ca_insertuser" ||$index == "ca_insertdate"){
					unset($rows[$key][$index]);
				}else if($index == "ca_attachment" && strlen($item) > 0){
					$attach = unserialize($item);
					if($attach[0]['status'] == CNTRCT_LOG::ACTIVE){
						$attach = $attach[0];
						$link="<a href='../files/".$this->getCmpCode()."/CONTRACT/assurance/".$attach['system_filename']."'>".$attach['original_filename']."</a>";
						$rows[$key][$index]=$link;
					}
				}
			}
		}
		foreach($rows as $key=>$val){
			$rows[$val['ca_id']]=$val;
			unset($rows[$key]);
		}
		$data['rows']=$rows;
		*/
		return $data;
	 	return $this->getDetailedObject("ASSURANCE", $id,$options);
	 }
	 
	 public function getRawUpdateObject($id){
		return $this->getRawObject($id);
	}
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id="") {
		$tmp = is_numeric($obj_id)?" AND aa_action_id = ".$obj_id:"";
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (aa_status & ".CNTRCT_LOG::ACTIVE.")=".CNTRCT_LOG::ACTIVE.$tmp;
		$data = $this->mysql_fetch_all($sql);
		
		return $data;
	}

	public function getRawListObject($obj_id="") {
		$tmp = is_numeric($obj_id)?" AND ".self::TABLE_FLD."_action_id = ".$obj_id:"";
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (".self::TABLE_FLD."_status & ".CNTRCT_LOG::ACTIVE.")=".CNTRCT_LOG::ACTIVE.$tmp;
		$data = $this->mysql_fetch_all($sql);
		
		return $data;
	}
	
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}




    


?>