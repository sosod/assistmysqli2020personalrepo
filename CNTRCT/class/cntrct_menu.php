<?php
/**
 * To manage the Menu items
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_MENU extends CNTRCT {
    
	private $additional_headings = array(
		'admins'=>'contract_owner_id',
		'menu'=>"Menu",
		'headings'=>"Headings",
		'lists'=>"Lists",
	);
	
	
    
    const TABLE = "setup_menu";
    /*************
     * CONSTANTS
     */
    /**
     * Can a heading be renamed by the client
     */
    const CAN_RENAME = 16;  
    /**
     * Is a heading the name of an object
     */
    const OBJECT_HEADING = 32;
         
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
	
	
	
	
	

	
	
	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editNames($section,$var);
		if($mar>0) {
			if($section=="object_names"){
				return array("ok","Object names updated successfully.");
			} else {
				return array("ok","Menu headings updated successfully.");
			}
		} else {
			return array("info","No changes were found to be saved.");
		}
		return array("error",$mar);
	}
	
	
	
	
    /*******************
     * GET functions
     * */
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    /**
     * Get the options for the Contents bar
     */
    public function getContentsBar() {
        $sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_link as section, CONCAT('CNTRCT/',menu_link,'.php') as link 
                 FROM ".$this->getTableName()." 
                 WHERE menu_parent_id = 0 
                 AND (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.") 
                 AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.") 
                 ORDER BY menu_order";
        return $this->mysql_fetch_all_by_id($sql,"section");
    }
    
    /**
     * Get the navigation buttons & breadcrumbs based on the current page
     * 
     * @param (String) page = file name excluding extention and folder hierarchy e.g. manage_update_action 
     */
    public function getPageMenu($page,$path="") {
    	$userObject = new CNTRCT_USERACCESS();
		$useraccess = $userObject->getMyUserAccess();
        $arr = explode("_",$page);
        $menu = array('buttons'=>array(),'breadcrumbs'=>array());
        $check = array($arr[0]);
        $c = $arr[0];
        unset($arr[0]);
        foreach($arr as $a){
            $c.="_".$a;
            $check[] = $c;
        }
        //buttons 1
        if(isset($check[1])){
            $sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path)>0 ? "'".$path."'," : "")."menu_link,'.php') as link, IF(menu_link='".$check[1]."',1,0) as active, menu_help as help 
                     FROM ".$this->getTableName()." 
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[0]."'
                     ".($check[0]=="manage" && $useraccess['assurance']!=true ? "AND menu_link <> 'manage_assurance' " : "")."
                     ".($check[0]=="admin" && $useraccess['update_all']!=true ? "AND menu_link <> 'admin_update' " : "")."
                     ".($check[0]=="admin" && $useraccess['edit_all']!=true ? "AND menu_link <> 'admin_edit' " : "")."
                     ".($check[0]=="admin" && $useraccess['contract']!=true ? "AND menu_link NOT IN ('admin_contract','admin_template') " : "")."
                     ".($check[0]=="admin" && $useraccess['finance']!=true ? "AND menu_link <> 'admin_finance' " : "")."
                     ".($check[0]=="manage" && $useraccess['finance']!=true ? "AND menu_link <> 'manage_finance' " : "")."
                     ".($check[0]=="new" && $useraccess['create_contract']!=true ? "AND menu_link <> 'new_contract' " : "")."
                     ".($check[0]=="new" && $useraccess['create_deliverable']!=true ? "AND menu_link <> 'new_deliverable' AND menu_link <> 'new_action' " : "")."
                     ORDER BY menu_order";
            $menu['buttons'][1] = $this->mysql_fetch_all($sql);
        }
        //buttons 2
        if(isset($check[2])) {
            $sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path)>0 ? "'".$path."'," : "")."menu_link,'.php') as link , IF(menu_link='".$check[2]."',1,0) as active, menu_help as help 
                     FROM ".$this->getTableName()." 
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_section = '".$check[1]."'
                     ORDER BY menu_order"; 
            $menu['buttons'][2] = $this->mysql_fetch_all($sql);
        }
                //breadcrumbs 
        if(count($check)>0){
            $sql = "SELECT menu_id as id, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name, menu_section as section, CONCAT(".(strlen($path)>0 ? "'".$path."'," : "")."menu_link,'.php') as link 
                     FROM ".$this->getTableName()." 
                     WHERE (menu_status & ".self::OBJECT_HEADING." <> ".self::OBJECT_HEADING.")
                     AND (menu_status & ".self::ACTIVE." = ".self::ACTIVE.")
                     AND menu_link IN ('".implode("','",$check)."')
                     ORDER BY menu_link, menu_order"; 
            $menu['breadcrumbs'] = $this->mysql_fetch_value_by_id($sql, "link", "name");
        }
        if(count($check)>count($menu['breadcrumbs'])) {
        	foreach($check as $link){
        		if(!isset($menu['breadcrumbs'][$link.'.php'])) {
        			$x = explode("_",$link);
					$l = end($x);
        			$b = $this->checkAdditionalBreadcrumbs($l);
					if($b!==false){ $menu['breadcrumbs'][] = $b; }
        		}
        	}
        }
        //Process menu array to replace any |object| placeholders with valid object_names
        $menu = $this->replaceAllNames($menu);
        return $menu;
    } 
     
	/** 
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections=array()){
		$sql = "SELECT menu_id as id, menu_section as section, menu_default as mdefault, menu_client as client 
				FROM ".$this->getTableName()." 
				WHERE (menu_status & ".CNTRCT_MENU::OBJECT_HEADING." = ".CNTRCT_MENU::OBJECT_HEADING.")"
				.(count($sections)>0 ? " AND menu_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
		//return array($sql);
	}
		
	/**
	 * Check for any additional sub headings
	 */ 
	public function checkAdditionalBreadcrumbs($b) {
		if(isset($this->additional_headings[$b])) {
			$a = $this->additional_headings[$b];
			if(substr_count($a, "_")) {
				$headingObject = new CNTRCT_HEADINGS();
				return $headingObject->getAHeadingNameByField($a);
			}
			return $a; 
		}
		return false;
	} 
	
	/**
	 * Get a single menu heading based on field - NOT FOR OBJECT NAMES
	 * 
	 * @param (String) field = the field name of the menu heading to be found.
	 */
	public function getAMenuNameByField($field){
		$sql = "SELECT IF(LENGTH(menu_client)>0,menu_client,menu_default) as name FROM ".$this->getTableName()." WHERE menu_link = '".$field."'";
		return $this->mysql_fetch_one_value($sql, "name");
	}

	/** 
	 * Get the menu names from the table for saving logs changes in Setup
	 */
	public function getOriginalMenuNamesFromDB($sections=array()){
		$sql = "SELECT menu_id as id, menu_section as section, menu_default as mdefault, menu_client as client 
				FROM ".$this->getTableName()." 
				WHERE (menu_status & ".CNTRCT_MENU::OBJECT_HEADING." <> ".CNTRCT_MENU::OBJECT_HEADING.")"
				.(count($sections)>0 ? " AND menu_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
	}
	/** 
	 * Get the menu names from the table for displaying logs changes
	 */
	public function getDefaultMenuNamesForLog(){
		$sql = "SELECT menu_section as section, menu_default as mdefault 
				FROM ".$this->getTableName()." 
				WHERE (menu_status & ".CNTRCT_MENU::OBJECT_HEADING." <> ".CNTRCT_MENU::OBJECT_HEADING.")"
				.(count($sections)>0 ? " AND menu_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_value_by_id($sql, "section", "mdefault");
		return $rows;
	}
	
	/**
	 * Get all renameable menu items for editing in Setup
	 */	 
	public function getRenameableMenuFromDB() {
		$sql = "SELECT 
				menu.menu_id as id, 
				menu.menu_link as section, 
				menu.menu_default as mdefault, 
				menu.menu_client as client , 
				menu.menu_parent_id as parent_id, 
				menu.menu_order,
				sub_parent.menu_id as sub_parent_menu_id,
				sub_parent.menu_order as sub_parent_menu_order,
				sub_parent.menu_default as sub_parent_mdefault,
				parent.menu_id as parent_menu_id,
				parent.menu_order as parent_menu_order,
				parent.menu_default as parent_mdefault
				FROM ".$this->getTableName()." as menu
				LEFT JOIN ".$this->getTableName()." as sub_parent
				  ON sub_parent.menu_id = menu.menu_parent_id
				LEFT JOIN ".$this->getTableName()." as parent
				  ON sub_parent.menu_parent_id = parent.menu_id
				WHERE ( (menu.menu_status & ".self::OBJECT_HEADING.") <> ".self::OBJECT_HEADING.") 
				  AND ( (menu.menu_status & ".self::CAN_RENAME.") = ".self::CAN_RENAME.") 
				  AND ( (menu.menu_status & ".self::ACTIVE.") = ".self::ACTIVE.") 
				ORDER BY parent.menu_order, sub_parent.menu_order, menu.menu_order";
/*SELECT menu_id as id, menu_link as section, menu_default as mdefault, menu_client as client 
				FROM ".$this->getTableName()." 
				WHERE ( (menu_status & ".self::OBJECT_HEADING.") <> ".self::OBJECT_HEADING.")
				AND ( (menu_status & ".self::CAN_RENAME.") = ".self::CAN_RENAME.")
				AND ( (menu_status & ".self::ACTIVE.") = ".self::ACTIVE.")
				ORDER BY menu_parent_id, menu_order";*/
				//echo $sql;
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$i = $r['id'];
			if(is_null($r['sub_parent_menu_id'])) {
				$spmi = 0;
				$pmi = 0;
				$data[$i]['row'] = $r;
				$data[$i]['child'] = array();
			} elseif(is_null($r['parent_menu_id'])) {
				$spmi = $r['sub_parent_menu_id'];
				$pmi = 0;
				$data[$spmi]['child'][$i] = array(
					'row'=>$r,
					'child'=>array()
				);
			} else {
				$spmi = $r['sub_parent_menu_id'];
				$pmi = $r['parent_menu_id'];
				$data[$pmi]['child'][$spmi]['child'][$i] = $r;
			}
			
			
		}
		/*array(
			id=>array(
				row=>$r
				child=>array(
					id=>array(
						row=>$r
						child=>array(
							id=>$r
						)
					)
				)
			)
		)*/
		return $data;
	}
	 
	 
	 
	 
	 
	 
    /****************************************
     * SET / UPDATE Functions
     */
    
    
    /**
	 * Function to edit object name / menu heading
	 */
    private function editNames($menu_section,$var) {
    	if($menu_section=="object_names") {
	   		$original_names = $this->getObjectNamesFromDB(array_keys($var));
	   		$log_section = "OBJECT";
			$field = "menu_section";
    	} else {
    		$original_names = $this->getOriginalMenuNamesFromDB(array_keys($var));
			$log_section = "MENU";
			$field = "menu_link";
    	}
    	$mar = 0;
    	$sql_start = "UPDATE ".$this->getTableName()." SET menu_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
    	foreach($var as $section => $val){
    		$m = 0;
			$to = (strlen($val)>0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to!=$from) {
    			$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m=$this->db_update($sql);
				if($m>0){
					if($menu_section=="object_names") {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					} else {
						$key = $section;
					}
					$c[$key] = array('to'=>$to,'from'=>$from);
					$mar+=$m;
				}
			}
    	}
		if($mar>0 && count($c)>0) {
			$changes = array_merge(array('user'=>$this->getUserName()),$c);
			/*foreach($var as $section => $val) {
				if($mar[$section]>0) {
					if($menu_section=="object_name") {
						$s = explode("_",$section);
						$changes['|'.end($s).'|'] = array('to'=>$val,'from'=>$original_names[$section]['client']);
					} else {
						$changes[$section] = array('to'=>$val,'from'=>$original_names[$section]['client']);
					}
				}
			}*/
			$log_var = array(
				'section'	=> $log_section,
				'object_id'	=> $original_names[$section]['id'],
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::EDIT,		
			);
			$this->addActivityLog("setup", $log_var);
		}
		
		return $mar;
		//return serialize(array_keys($var))." => ".serialize($original_names);
    }


	/**
	 * Function to echo the navigation buttons and breadcrumbs onscreen
	 */
	public function drawPageTop($menu,$display_navigation_buttons=true) {
		/**********
		 * Navigation buttons
		 */
		$active_button_label = "";
		if(!isset($display_navigation_buttons) || $display_navigation_buttons!==false) {
			foreach($menu['buttons'] as $level => $buttons) {
				//$helper->arrPrint($buttons);
			    echo $this->generateNavigationButtons($buttons, $level);
				if($level==1) {
					foreach($buttons as $b) {
						if($b['active']==true) {
							$active_button_label = $b['name'];
							break;
						}
					}
				}
			}
		}
		//marktime("navigation buttons");
		/**********
		 * Breadcrumbs
		 */
		$breadcrumbs = array();
		    foreach($menu['breadcrumbs'] as $link=>$text) {
		    	if(substr_count($link, ".php")>0) {
		        	$breadcrumbs[] = "<a href='".$link."' class=breadcrumb>".$text."</a>";
				} else {
		        	$breadcrumbs[] = "".$text."";
				}
		    }
		echo "<h1>".implode(" >> ",$breadcrumbs)."</h1>";		
		return $active_button_label;
	}

    
    
    
    
    
    
    
    /**********************************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    
	 
     
	 
	 
	 
	 
	 
	 
	 
     
     /***********************
     * PRIVATE functions: functions only for use within the class
     */	
	
	
	
	
	    
    public function __destruct() {
    	parent::__destruct();
    }
    
    
    
}


?>