<?php
$section = "NEW";
$page_redirect_path = "new_contract_create.php?";
$page_action = "Add";

require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$uaObject = new CNTRCT_USERACCESS();

$child_object_type = "CONTRACT";
$child_object_id = 0;
$childObject = new CNTRCT_CONTRACT();


//include("common/generic_object_form.php");
//include("common/contract_form.php");

$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
echo $data['display'];

//$helper->arrPrint($data);

 //* 	$myObject
 //* 	$page_action (ADD || EDIT)
 //*  $page_redirect_path = where to go on successful doAjax (page.php?index=request&)


?>
<script type="text/javascript">
	$(function() {
		<?php echo $data['js']; ?>
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("form[name=frm_object] table.form").css("width","800px");
		} else {
			$("form[name=frm_object] table.form").css("width",(my_window['width']-50)+"px");
		}
	});
</script>