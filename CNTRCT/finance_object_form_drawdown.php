<?php

$tab = strtoupper($_REQUEST['tab']);
if(isset($_REQUEST['display_type']) && $_REQUEST['display_type']=="dialog") {
	$no_page_heading = true;
	include("inc_header.php");
	$object_id = $_REQUEST['object_id'];
	$page_redirect_path = "manage_finance_object.php?object_id=".$_REQUEST['object_id'];
	unset($displayObject);
	$displayObject = new CNTRCT_DISPLAY_FINANCE();
	$is_dialog = true;
	$form_object_id = $_REQUEST['edit_object_id'];
} else {
	$is_dialog = false;
	$form_object_id = 0;
}

$form_object_type = "FINANCE_".$tab;
$object_type = $tab;

$formObject = new CNTRCT_FINANCE_DRAWDOWN();

$parent_object_type = "CONTRACT";
$parentObject = new CNTRCT_CONTRACT();
$parent_object_id = $_REQUEST['object_id'];
$page_action = "EDIT";
$page_redirect_path = $page_redirect_path."&tab=".strtolower($tab)."&";

$edit_list_objects = $formObject->getListObjects($parent_object_id);

$listObject = new CNTRCT_LIST("deliverable_category");
$cate_items = $listObject->getListItems();
if(!isset($cate_items[0])) {
	$unspecified_item = array(array('id'=>0,'name'=>$helper->getUnspecified()));
	$cate_items = array_merge($unspecified_item,$cate_items);
}

$delObject = new CNTRCT_DELIVERABLE();
$add_deliverables = $delObject->getCategoryGroupedOrderedObjects($parent_object_id);
//$del_ids = array_keys($add_deliverables['main']);
$heading_fields = $delObject->getSummaryFields();
$hf = array_flip($heading_fields);
unset($heading_fields[$hf['del_category_id']]);

$form_items = $formObject->getAllItems($parent_object_id);
$del_drawdowns = $formObject->getDeliverableDrawDowns($parent_object_id);

//$helper->arrPrint($del_drawdowns);

$headObject = new CNTRCT_HEADINGS();
$del_headings = $headObject->getMainObjectHeadings("DELIVERABLE","ALL","MANAGE","",true,$heading_fields);
$del_headings = $del_headings['rows'];


//$delObject->arrPrint($heading_fields);
?>
		<?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					$js.=$contract['js'];
					//$summary_table = $displayObject->getSummaryTable($form_object_type, $formObject, $parent_object_id);
					//echo $summary_table['display'];
					//$js.=$summary_table['js'];
					?>
				</td>
				<td width=4%>
				</td>
				<td width=48%><h2><?php echo $helper->getActivityName("edit")." ".$helper->getObjectName($form_object_type); ?></h2>
					<?php
					$form_display = $displayObject->getFinanceObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path);
					echo $form_display['display'];
					$js.=$form_display['js'];
					$end_display = $form_display['end_display'];

					echo "<h2>".$helper->getDeliverableObjectName()."</h2>
					
					<table id=tbl_finance_drawdown_deliverables class=drawdown-list width=100%>
						<tr>";
					foreach($del_headings as $fld => $h) {
						echo "<th>".$h['name']."</th>";
					}
					echo "<th>".$helper->getObjectName($object_type)."</th></tr>";
					foreach($cate_items as $ci => $cate) {
						if(isset($add_deliverables['main'][$ci]) && count($add_deliverables['main'][$ci]) > 0) {
							echo "
								<tr>
									<th class=th2>".$cate['name']."</th>
								</tr>
									";
							foreach($add_deliverables['main'][$ci] as $i => $del) {
								//if($del['del_type']=="DEL") {
									echo "
									<tr>
										<td class='center b'>".$del['reftag']."</td>
										<td>".$del['name']."</td>
										<td class=center>".$del['responsible_person']."</td>
										<td class=center>".$displayObject->getDateForDisplay($del['deadline'])."</td>
										<td class=center>".(isset($del_drawdowns['del'][$i]) && $del_drawdowns['del'][$i]!=$form_object_id ? $form_items[$del_drawdowns['del'][$i]]['name'] : "<input type=checkbox name=dd[] value=".$i." ".($del_drawdowns['del'][$i]==$form_object_id ? "checked=checked" : "")." />" )."</td>
									</tr>";
									if(isset($add_deliverables['subs'][$i]) && count($add_deliverables['subs'][$i])) {
										foreach($add_deliverables['subs'][$i] as $si => $sub) {
											echo "
											<tr class=sub>
												<td class='center b'>".$sub['reftag']."</td>
												<td>".$sub['name']."</td>
												<td class=center>".$sub['responsible_person']."</td>
												<td class=center>".$displayObject->getDateForDisplay($sub['deadline'])."</td>
												<td class=center>-</td>
											</tr>";
										}
									}
							}
						}
					}
					echo "</table>".$end_display; 
?>
				</td>
			</tr>
			<tr>
				<td colspan=3>

				</td>
			</tr>
		</table>

<script type="text/javascript">
$(function() {	
	//$("h3#head_edit, div#edit").hide();
	$("table#tbl_finance_drawdown_deliverables th.th2").prop("colspan",$("table#tbl_finance_drawdown_deliverables tr:first th").length).css("text-align","left");
	$("table#tbl_finance_drawdown_deliverables tr.sub td").css("background-color","#efefef");
	
	
		
	<?php 
	
	echo $js;
	
	echo $displayObject->getIframeDialogJS($form_object_type,"dlg_child",$_REQUEST); 
	
	?>
	
	var is_dialog = <?php echo $is_dialog ? "true" : "false"; ?>;

	
	
	
});
</script>