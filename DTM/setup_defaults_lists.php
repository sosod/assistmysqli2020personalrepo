<?php
require_once("inc_header.php");

//Setting the url of the page sans queries
$noparams = explode("&",$_SERVER['REQUEST_URI']);
$pageonly = explode("/",$noparams[0]);
$thispage = $pageonly[2];

$list_id = $_REQUEST['l'];
//echo $list_id;
$listObj = new DTM_LIST($list_id);
$allow_add = $listObj->canIAddRows();
//echo $listObj->getListTable();
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();
$list_data = $listObj->getListItemsForSetup();
$cat_sups = array();	

//ASSIST_HELPER::arrPrint($types);


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
	<h2><?php echo $headingObject->getAListHeading($list_id); ?></h2>
<?php



if($list_id != "travel_types" && stripos($list_id,"status")===false) {
	//echo ASSIST_HELPER::getFloatingDisplay(array("info","."));
	$listObj2 = new DTM_LIST("travel_types");
	//$cat_list_tems = $listObj2->getListItems();
	//foreach($cat_list_tems as $list => $val){
	//	if($val['status'] == DTM::SYSTEM_DEFAULT || $val['status'] == DTM::ACTIVE){
	//		$cat_sups[$val['id']]=$val['name'];
	//	}
	//}
	$cat_sups = $listObj2->getActiveListItemsFormattedForSelect();
	if(count($cat_sups)==0) {
		ASSIST_HELPER::displayResult(array("error","There are currently no Travel Types available.  Add / Edits may only be made if there are Travel Types available."));
	}
}
?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_list>
<input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
<?php 
if($list_id!="benefit_categories") {
	echo '<input type="hidden" id="sort" name="sort" value="99" />';
}
?>

<table class=list id=master_list>
    <thead>
        <tr><?php
        	foreach($fields as $fld=>$val){
        		echo"<th>$val</th>";
        	}
    	?>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        //Add list
        if($allow_add) {
        		echo "<tr id=\"add_row\">";
        		foreach($fields as $fld=>$name){
        			echo"<td >";
					//echo $fld;
					if($fld == "status"){
						echo "<div class=center>Active</div><input type=\"hidden\" name='$fld' id='$fld' value=" . DTM::ACTIVE . " />";
					}else if($fld == "preferred"){
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld, 'require_me'=>"1",'form'=>"vertical"));
					}else if($fld == "colour"){
						echo "<input type=\"hidden\" name='$fld' id=\"add_colour\" value=\"#FFFFFF\" />";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>'clr_'.$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
					}else{
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
					}
        			echo"</td>";
					
        		}
				echo "<td class=center><input type=button name=btn_add value=Add /></td></tr>";
		} //end if check adding rows is allowed
    	?>
        <?php
			foreach($list_data as $key=>$val){
				echo"<tr id=\"tr_".$val['id']."\" ".((($val['status'] & DTM::INACTIVE)==DTM::INACTIVE) ? "class=inact" : "")." >";
				foreach($fields as $fld=>$head){
					$valkey = $fld;
					$valval = $val[$fld];	
					switch($valkey){
						case "sort":
							if($list_id=="benefit_categories") {
								echo "<td fld=\"". $valkey ."\" >".$valval."</td>";
							}
							break;
						case "status":
							if(($valval & DTM::SYSTEM_DEFAULT)==DTM::SYSTEM_DEFAULT){
								echo "<td sys=1 class=\"center\">System Default";
							}else if(($valval & DTM::ACTIVE)==DTM::ACTIVE){
								echo "<td class=\"center\">Active";
							}else if(($valval & DTM::INACTIVE)==DTM::INACTIVE){
								echo "<td class=\"center\">Inactive";
							}else{
								echo "<td class=\"center\">";
							}
							
								echo "</td>";
							break;
						case "preferred":
							echo "<td>".$displayObject->getBoolForDisplay($valval)."</td>";
							break;
						case "colour":
							echo "<td style=\"background-color:".$valval."\" fld=\"". $valkey ."\"></td>";
							break;
						default:	
							echo "<td fld=\"". $valkey ."\">".$valval."</td>";
						//$js .=" rows['".$val['id']."']['". $valkey ."'] = ". $valval ." ; ";
					}
				}
				if(($val['status']& DTM::ACTIVE)==DTM::ACTIVE){
				    echo "<td class=center><input type=button value=Edit id='".$val['id']."' class='btn_edit' can_delete='".($val['can_delete']==true ? 1 : 0)."' /></td>";
				}else{
				    echo "<td class=center><input type=button value=Restore id='".$val['id']."' class='btn_restore' /></td>";
				}
				echo "</tr>";
			}	   
	   ?>

    </tbody>
</table>
</form>
</td></tr><tr><td>
	<?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"list",array('section'=>$list_id)); ?>
</td></tr>
</table>
</div>
<div id=div_dialog title="Edit">
    <h1>Edit form</h1>
    <form name=edit_frm>
    <input type="hidden" name="id" id="ref" value=0 />
    <input type="hidden" name="list_id" id="list_id" value=<?php echo $list_id; ?> />
    <table id=ed_frm class=form>
    	<?php
    	
    	foreach($fields as $fld=>$name){
    		echo"<tr>
    				<th>$name</th>
    				<td fld=edit_td_".$fld." id=\"edit_td_".$fld."\">";
					if($fld == "status"){
    					echo"Active";
					}else if($fld == "preferred"){
						echo"<p class=\"for_display\"></p>";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$fld, 'name'=>$fld));
					}else{
						echo"<p class=\"for_display\"></p>";
						$js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld));
					}
				echo"</td>";
			echo"</tr>";
    	}

?>
        
    </table>
  	</form>
  	<?php ASSIST_HELPER::displayResult(array("info","Items can only be deleted if they are not in use elsewhere within in the module.")); ?>

<div id="div_dialog_restore" title="Restore">
    	<p id="restoration_p"></p>
</div>

<script type="text/javascript">
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);
//Thanks to Erick Petru
function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

$(function () {
    <?php 
    /*****************
	 * echo stored $js strings here
	 */
    	echo  $js ;
    ?>
    	
	
    $("#div_dialog").dialog({
        modal: true,
        autoOpen: false,
        width: "auto",
        buttons: [{
           text: "Save Changes",
           click: function(){
			var okay = true;
           	var id = $(this).attr("num");
           	
           	
/*           	
			$("#ed_frm td textarea").removeClass("required").each(function(){
				var lngth = $(this).val();
				var howlong = lngth.length;
				
				if(howlong == undefined || howlong == 0){
					okay = false; 
					$(this).addClass("required");
				}
			});
				
			$("#ed_frm td input").removeClass("required").each(function(){
				if(!$(this).hasClass("color")){
					var length = $(this).val().length;
					if(length == 0){
						okay = false;
						$(this).addClass("required");
					}
				}
			});
				
			//if($("#edit_list_num").val() == "X"){
				//okay = false;
			//}
			if(okay != true){
				alert("Please fill in all of the required fields");*/
				
			var spin = false;	
			AssistHelper.processing();	
				
			$("#ed_frm .num_spinner").each(function() {
				if($(this).val().length>0 && $(this).hasClass("required")) {
					spin = true;
				} else {
					$(this).removeClass("required");
					var require_me = $(this).attr("require_me");
					require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
					if(require_me==true && $(this).val().length==0) {
						okay = false
						$(this).addClass("required");
					}
				}
			});
			
			
			$("#ed_frm ").find("input:text, textarea, select").not(".num_spinner").removeClass("required").each(function(){
				var require_me = $(this).attr("require_me");
				require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
				if(require_me==true && $(this).val().length==0) {
					okay = false
					$(this).addClass("required");
				}
			});
			
			//if($("#list_num").val() == "X" || $("#list_num").val() == "0"){
			//	okay = false;	
			//	$("#list_num").addClass("required");
			//}
			if(okay != true){
				err_text = "Please fill in all of the required fields as highlighted.";
				if(spin) {
					err_text+= " Note that only numbers (0-9) are permitted in the number spinner.";
				} 
				AssistHelper.finishedProcessing("error",err_text);
				
				
			}else{
				//All changes are within bounds
				//AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=edit_frm]"));
				var lid = $("#list_id").val();
	//			console.log(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleEdit", dta);
				if(result[0]=="ok") {
	//				console.log(result);
					document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
	//				console.log(result);
				}
				
				
			}
			//$(this).dialog("close");
           }   
        },{
           text: "Deactivate",
           click: function(){
           		var id = $(this).attr("num");
                if(confirm("Are you sure you wish to deactivate this item?")==true) {
                    var lid = $("#list_id").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Deactivate","id="+id+"&list_id="+lid);
					if(result[0]=="ok") {
						document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
                }
                $(this).dialog("close");
           }   
        },{
           text: "Restore",
           click: function(){
               alert("restoring!!");
                $(this).dialog("close");
           }   
        },{
            text: "Delete",
            click: function(){
           		var id = $(this).attr("num");
                if(confirm("Are you sure you wish to remove this item?")==true) {
                    var lid = $("#list_id").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Delete","id="+id+"&list_id="+lid);
			//		alert(id+"(id),"+lid+"(lid).");
			//		console.log(result);
					if(result[0]=="ok") {
						document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
                }
                $(this).dialog("close");
           } 
        },{
            text: "Cancel",
            click: function() {
                $(this).dialog("close");
            }
        }]
    });
    
    $("#div_dialog_restore").dialog({
        modal: true,
        autoOpen: false,
        width: "auto",
        buttons: [{
           text: "Restore",
           click: function(){
				var qid = $(this)[0].firstChild.getAttribute("num");
				var id = parseInt(qid);
				var lid = $("#list_id").val();
				var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Restore","id="+id+"&list_id="+lid);
			if(result[0]=="ok") {
				document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
				//$(this).dialog("close");
           }
        },{
            text: "Cancel",
            click: function() {
                $(this).dialog("close");
            }
        }]
    });
    AssistHelper.formatDialogButtons($("#div_dialog"),0,AssistHelper.getDialogSaveCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getRedCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),2,AssistHelper.getGreenCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getRedCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),4,AssistHelper.getCloseCSS());
    AssistHelper.formatDialogButtons($("#div_dialog_restore"),0,AssistHelper.getGreenCSS());
    AssistHelper.formatDialogButtons($("#div_dialog_restore"),1,AssistHelper.getCloseCSS());
    
    AssistHelper.hideDialogTitlebar("id", "div_dialog_restore");
    
    $("input[name='btn_add']").click(function() {
		var okay = true;
		var spin = false;
		AssistHelper.processing();
		$(".num_spinner").each(function() {
			if($(this).val().length>0 && $(this).hasClass("required")) {
				spin = true;
			} else {
				$(this).removeClass("required");
				var require_me = $(this).attr("require_me");
				require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
				if(require_me==true && $(this).val().length==0) {
					okay = false
					$(this).addClass("required");
				}
			}
		});
		
		
		$("#master_list tr#add_row").find("input:text, textarea, select").not(".num_spinner").removeClass("required").each(function(){
			var require_me = $(this).attr("require_me");
			require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
			if(require_me==true && $(this).val().length==0) {
				okay = false
				$(this).addClass("required");
			}
		});
		
		if($("#list_num").val() == "X" || $("#list_num").val() == "0"){
			okay = false;	
			$("#list_num").addClass("required");
		}
		if(okay != true){
			err_text = "Please fill in all of the required fields as highlighted.";
			if(spin) {
				err_text+= " Note that only numbers (0-9) are permitted in the number spinner.";
			} 
			AssistHelper.finishedProcessing("error",err_text);
		}else{
			//AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_list]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleAdd",dta);
			//console.log(dta);
			//result = ["error","Test"];
		//alert(result);
			if(result[0]=="ok") {
				document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
		//		alert("complete! WP");
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
    });
    
    $("input:button.btn_restore").click(function() {
        var i = $(this).prop("id");
    		
	    $('#div_dialog_restore').html("<p num="+i+" >Are you sure you want to restore item "+i+"?</p>");
	    $('#div_dialog_restore').dialog("open");
    });
    	
    	
    	
    	
    $("input:button.btn_edit").click(function() {
	    AssistHelper.formatDialogButtons($("#div_dialog"),2,AssistHelper.getDisplayCSS("hidden"));
        var i = $(this).prop("id");
        var can_delete = $(this).attr("can_delete"); 
        var sys = Array();
        if($("#tr_"+i+" td.center").attr("sys") == 1){
		    sys[i] = true;
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("hidden"));
		    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("hidden"));
		} else if(can_delete==0) {
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("hidden"));
        }else{
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("inline"));
		    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("inline"));
        }
		$("#tr_"+i+" td").each(function(){
			var h = $(this).html();
			var x = $(this).text();
			var f = $(this).attr("fld");
			$("#div_dialog #edit_td_"+f+" p").hide();
			
			console.log(f+" = "+h);
			if(f == "id"){
				$('#div_dialog #ref').val(i);	
				$('#div_dialog #edit_td_id').html(i);	
			}else if(sys[i] == true && f != "client_name"){
				$("#div_dialog #edit_"+f).hide();
				$("#div_dialog input.color").hide();
				$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #lbl_edit_"+f).hide();
				$("#div_dialog #edit_td_"+f+" br").hide();
				$("#div_dialog #edit_td_"+f+" p").show();
				$("#div_dialog #edit_td_"+f+" p").html(h);
				if(f == "colour"){
					$("#div_dialog #edit_td_"+f+" p").html("<div style='height:13px; width:100%; padding-bottom:1px; padding-top:1px; background-color:"+$(this).css("background-color")+"'></div>");
				}
			}else if(f == "colour"){
				var this_col_rgb = $(this).css("background-color");
				var this_col = rgb2hex(this_col_rgb);
				$("#div_dialog input.color").css("background-color",this_col);
				$("#div_dialog input.color").show();
				$("#div_dialog #edit_td_"+f+" p").html("<input type='hidden' id='secret_colour' name='colour' value='"+this_col+"' />");
			} else if(f=="list_num") {
				$("#div_dialog #edit_td_"+f+" br").show();
				$("#div_dialog #edit_"+f).show();
				$("#div_dialog #lbl_edit_"+f).show();
				$("#div_dialog #edit_td_"+f+" p").hide();
				//$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #edit_"+f).keyup();
				var x = 0;
				$("#div_dialog #edit_"+f+" option").each(function() {
					if($(this).prop("text")==h) {
						x = $(this).prop("value");
					}
				});
				$("#div_dialog #edit_"+f).val(x);
			}else{
				$("#div_dialog #edit_td_"+f+" br").show();
				$("#div_dialog #edit_"+f).show();
				$("#div_dialog #lbl_edit_"+f).show();
				$("#div_dialog #edit_td_"+f+" p").hide();
				$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #edit_"+f).keyup();
			}
			
			
			
		}); 
		<?php if ($list_id == "assessment_frequency"){ ?>
			AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("inline"));
			AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getRedCSS());
		<?php	} ?>
        //for each i, make an array with key=>heading and val=>value, with arbitrary numbers of headings and values.
       //console.log(rows);
	    $("#div_dialog").attr("num",i);
        $("#div_dialog").dialog("open");
	
	//change value of hidden input on colour picker change
    	$("#div_dialog input.color").change(function(){
    		var colour = "#" + $(this).val();
    		$("#secret_colour").val(colour);
    	});    
    });
    	$("#master_list input.color").change(function(){
    		var addcolour = "#" + $(this).val();
    		$("#add_colour").val(addcolour);
    	});    
    
	<?php 
	if(isset($fields['travel_type']) && count($cat_sups)==0) {
echo "$(\"#master_list\").find(\"input:button\").each(function() {
			$(this).prop(\"disabled\",true);
		});";
	}
		?>
    //}
    $("input.color").attr("spellcheck", "false");
    

});
</script>
<script type="text/javascript" src="../library/jquery-plugins/jscolor/jscolor.js"></script>
<style>
	.for_display{
		margin:0px;
		padding:0px;
	}
</style>

