<?php require_once('inc_header.php'); ?>
<?php ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r'])? $_REQUEST['r']: array())); ?>
<?php

$section = "NEW";
$sub_page = "NEW";

$button_icon = "OPEN";
$button_label = "Continue";
$button_url = "new_create.php?";
$button_text = false;

//Get Topics and modules
//Is this user in our helpdesk database\
$is_hd_user = false;

//$query = 'SELECT * FROM helpdesk_list_user_roles ';
//$query .= 'WHERE value = \'assigned_user\'';
//
////$result = mysqli_query($db_conn, $query);
//
//$user = $helper->mysql_fetch_one($query);
//
//echo '<pre>';
//echo '<p>PRINTHERE</p>';
//print_r($user);
//echo '</pre>';

//Get Topics
$listObject = new DTM_LIST('topic');
$topics = $listObject->getActiveListItemsFormattedForSelect();

//echo '<pre>';
//echo '<p>PRINTHERE</p>';
//print_r($topics);
//echo '</pre>';

//Get Topics
$listObject->changeListType('module');
$modules = $listObject->getActiveListItemsFormattedForSelect();

//echo '<pre>';
//echo '<p>PRINTHERE</p>';
//print_r($modules);
//echo '</pre>';

?>

<form name="newrequest" method="post" enctype="multipart/form-data">
    <table id="tbl_action" class="form">
        <tbody>
        <tr class="">
            <th>Request By:</th>
            <td><?php echo $_SESSION['tkn']; ?></td>
        </tr>
        <tr>
            <th id="th_helpdesk_request_topicid">Topic:</th>
            <td>
                <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_topicid', 'name' => 'hdr_topicid', 'required' => true, 'unspecified' => false,  'options' => $topics ));?>
            </td>
        </tr>
        <tr>
            <th id="th_helpdesk_request_module">Module:</th>
            <td>
                <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_moduleid', 'name' => 'hdr_moduleid', 'required' => true, 'unspecified' => false,  'options' => $modules ));?>
            </td>
        </tr>
        <tr>
            <th id="th_helpdesk_request_description">Description:</th>
            <td>
                <?php $js .= $displayObject->drawFormField('TEXT', array('id' => 'hdr_description', 'name' => 'hdr_description', 'required' => true )); ?>
            </td>
        </tr>
        <tr>
            <th>Attach Document(s):</th>
            <td>
            <!--       Come back to this later      -->
            </td>
        </tr>
        <tr>
            <th></th>
            <td>
                <input id="btn_submit" type="button" value="Submit" class="isubmit i-am-the-submit-button">
                <input type="reset" value="Reset" name="B2">
            </td>
        </tr>
        </tbody>
    </table>
</form>

<?php

echo '<pre style="font-size: 18px">';
echo '<p>PRINTHERE</p>';
print_r($_SESSION);
echo '</pre>';

?>
<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>
        $('#btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="newrequest"]'), 'REQUEST.ADD', 'new.php?')
        });
    });
</script>

<?php require_once('inc_footer.php'); ?>

