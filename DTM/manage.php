<?php require_once('inc_header.php'); ?>
<?php
/*
 * These are the company details
 * Including:
 * Company information
 * Admin users, if any
 * Preferred way of handling the helpdesk request (settings)
*/

$sql = 'SELECT * FROM dtm_companies ';
$sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
$company_info = $helper->mysql_fetch_one($sql);
?>

<?php if(isset($company_info) && is_array($company_info) && count($company_info) > 0){ ?>

    <?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>

    <?php
        $sql = 'SELECT * FROM dtm_users ';
        $sql .= 'WHERE dtm_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
        $sql .= 'AND dtm_users.user_company = '.$company_info['comp_id'] . ' ';
        $user_info = $helper->mysql_fetch_one($sql);
    ?>

    <?php if(isset($_POST) && is_array($_POST) && count($_POST) > 0 && array_key_exists('ud_user_but',$_POST)){ ?>
        <form name="user_update" method="post" enctype="multipart/form-data">
            <table class="list " id="paging_obj_list_view" width="100%">
                <tbody>
                <tr id="head_row">
                    <th>AiT Username:</th>
                    <th>DTM Username (Current):</th>
                    <th>DTM Username (Change):</th>
                    <th></th>
                </tr>
                <tr>
                    <td><?php echo $user_info['user_name_ait']; ?></td>
                    <td><?php echo ($user_info['user_name_dtm'] != '' ? $user_info['user_name_dtm'] : 'N/A'); ?></td>
                    <td><input id="dtm_user_name" name="dtm_user_name" type="text" class="btn_admin_del" style="width: 98%;"></td>
                    <td class="center"><input id="btn_submit" type="button" value="Update" class="isubmit i-am-the-submit-button"></td>
                </tr>
                </tbody>
            </table>
        </form>
    <?php }else{ ?>
        <form name="user_information" method="post" action="manage.php" enctype="multipart/form-data">
            <table class="list " id="paging_obj_list_view" width="100%">
                <tbody>
                <tr id="head_row">
                    <th>AiT Username:</th>
                    <th>DTM Username:</th>
                    <th></th>
                </tr>
                <tr>
                    <td><?php echo $user_info['user_name_ait']; ?></td>
                    <td><?php echo ($user_info['user_name_dtm'] != '' ? $user_info['user_name_dtm'] : 'N/A'); ?></td>
                    <td class="center"><input id="ud_user_but" name="ud_user_but" type="submit" value="Update User" ref="3" class="btn_admin_del"></td>
                </tr>
                </tbody>
            </table>
        </form>
    <?php } ?>

<?php }else{ ?>

    <?php
        //Check that the current user has access to setup for this "module"
        if(true){
            //Redirect him to setup and instruct him to setup the company when he gets there

            //NOTE: CREATE THIS USER IMMEDIATELY WHEN THEY CREATE THE COMPANY
        }else{
            //Instruct the current user to request that the company be setup by the users with access to setup (Also just email those guys)
            echo 'Just stay here';
        }
    ?>

<?php } // end else?>
<script type="text/javascript">
    $(function(){

        $('#btn_submit').click(function(e){
            e.preventDefault();
            HelpdeskHelper.processObjectForm($('form[name="user_update"]'), 'USERS.SIMPLEEDIT', 'manage.php?');
        });
    });
</script>

<?php require_once('inc_footer.php'); ?>

