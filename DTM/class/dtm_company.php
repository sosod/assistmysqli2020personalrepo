<?php

/**
 *
 *
 * Created on: 2 July 2016
 * Authors: Janet Currie
 *
 */
class DTM_COMPANY extends DTM
{


    private $table_name = "dtm_companies";
    private $table_field = "comp_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    private $system_log = array();
    private $user_changes;
    private $emails;


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "companies";
    const TABLE_FLD = "comp_";
    const REFTAG = "DTMC";
    const LOG_TABLE = "object";

    public function __construct()
    {
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix() . $this->id_field;
        $this->status_field = $this->getTableFieldPrefix() . $this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField()
    {
        return self::TABLE_FLD;
    }

    public function getTableName()
    {
        return $this->getDBRef() . "_" . self::TABLE;
    }

    public function getRootTableName()
    {
        return $this->getDBRef();
    }

    public function getRefTag()
    {
        return self::REFTAG;
    }

    public function getMyObjectType()
    {
        return self::OBJECT_TYPE;
    }

    public function getMyLogTable()
    {
        return self::LOG_TABLE;
    }

    public function getApproverFieldName()
    {
        return $this->approver_field;
    }

    public function getTableFieldPrefix()
    {
        return $this->table_field;
    }


    //Add new DTM to the DTM db
    public function addObject($var)
    {
        if(isset($var['attachments'])){
            unset($var['attachments']);
        }

        //Get the company information
        $sql = 'SELECT * FROM dtm_companies ';
        $sql .= 'WHERE dtm_companies.comp_ait_cc = \''. $var['comp_cc'] . '\' ';
        $company = $this->mysql_fetch_one($sql);

        if(isset($company) && is_array($company) && count($company) > 0){
            //Activate the company
            $sql = 'UPDATE dtm_companies ';
            $sql .= 'SET comp_active = 1 ';
            $sql .= 'WHERE comp_id = ' . $company['comp_id'];
            $id = $this->db_update($sql);

            $log_text = $company['comp_ait_name'] . ' has been successfully reactivated in the DTM system, by: ' . $_SESSION['tkn'];
        }else{
            //Get this company's information from the master database
            $db = new ASSIST_DB('master');
            $sql = 'SELECT cmpcode, cmpname FROM assist_company ';
            $sql .= 'WHERE cmpcode = \''. $var['comp_cc'] .'\' ';
            $ait_companies = $db->mysql_fetch_one($sql);

            //Put information into db
            $comp_values = array();
            $comp_values[$this->getTableField() . 'ait_name'] = $ait_companies['cmpname'];
            $comp_values[$this->getTableField() . 'ait_cc'] = $ait_companies['cmpcode'];
            $comp_values[$this->getTableField() . 'dtm_name'] = 'NULL';
            $comp_values[$this->getTableField() . 'active'] = 1;

            $sql = 'INSERT INTO '. $this->getTableName() .' ('.$this->getTableField() . 'ait_name, ' . $this->getTableField() . 'ait_cc, ' . $this->getTableField() . 'dtm_name, ' . $this->getTableField() . 'active) ';
            $sql .= 'VALUES (\''. $comp_values[$this->getTableField() . 'ait_name'] .'\', ';
            $sql .= '\''. $comp_values[$this->getTableField() . 'ait_cc'] .'\', ';
            $sql .= $comp_values[$this->getTableField() . 'dtm_name'] .', ';
            $sql .= $comp_values[$this->getTableField() . 'active'] .')';

            $id = $this->db_insert($sql);

            $log_text = $ait_companies['cmpname'] . ' has been successfully created in the DTM system, by: ' . $_SESSION['tkn'];
        }


        // 4. Return true for success, or false for failure
        if ($id > 0) {
            $comp_id = (isset($company) && is_array($company) && count($company) > 0 ? $company['comp_id'] : $id);
            $success = true;
        } else {
            $success = false;
        }




        //Return the relevant message upon success or failure
        if ($success) {
            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = $log_text;

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $id . " has been successfully created.",
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }
    }

    //I know we call it a deletion, but it's really a deactivation
    public function deleteObject($var) {
        if(isset($var['attachments'])){
            unset($var['attachments']);
        }

        //Get the company information
        $sql = 'SELECT * FROM dtm_companies ';
        $sql .= 'WHERE dtm_companies.comp_ait_cc = \''. $var['comp_cc'] . '\' ';
        $company = $this->mysql_fetch_one($sql);

        //Deactivate the company
        $sql = 'UPDATE dtm_companies ';
        $sql .= 'SET comp_active = 0 ';
        $sql .= 'WHERE comp_id = ' . $company['comp_id'];
        $id = $this->db_update($sql);

        //Return true for success, or false for failure
        if ($id > 0) {
            $comp_id = (isset($company) && is_array($company) && count($company) > 0 ? $company['comp_id'] : $id);
            $success = true;
        } else {
            $success = false;
        }

        //Return the relevant message upon success or failure
        if ($success) {
            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = $company['comp_ait_name'] . ' has been successfully deactivated in the DTM system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $id . " has been successfully deleted.",
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }

    }

    public function simpleEditObject($var) {
        $dtm_comp_name = $var['dtm_comp_name'];

        $sql = 'SELECT * FROM dtm_companies ';
        $sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
        $company = $this->mysql_fetch_one($sql);

        $sql = 'UPDATE dtm_companies ';
        $sql .= 'SET comp_dtm_name = \'' . $dtm_comp_name . '\' ';
        $sql .= 'WHERE comp_id = ' . $company['comp_id'];
        $id = $this->db_update($sql);

        //Return true for success, or false for failure
        if ($id > 0) {
            $comp_id = (isset($company) && is_array($company) && count($company) > 0 ? $company['comp_id'] : $id);
            $success = true;
        } else {
            $success = false;
        }

        //Return the relevant message upon success or failure
        if ($success) {
            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = 'DTM Company Name for \'' . $company['comp_ait_name'] . '\' has been successfully changed in the DTM system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "DTM Company Name for " . $company['comp_ait_name'] . " has been successfully edited.",
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }
    }

    public function updateObject($var)
    {
    }


    public function finaliseRequest($var)
    {
        $req_id = $var['id'];

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $old = $this->mysql_fetch_one($sql);

        $status = self::ACTIVE + self::ACTIVATED;
        $response = $this->getObjectName("request") . " $req_id has been finalised. ";
        $objObject = new DTM_REQUEST_OBJECTS();
        $auth_required = $objObject->checkAuthorisationRequired($req_id);
        if ($auth_required) {
            $status += self::AUTHORISATION_REQUIRED;
            $response .= " It has been sent for authorisation.";
        } else {
            $response .= " It has been sent to the " . $this->getObjectName("agent") . ".";
        }

        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . ($status) . " WHERE " . $this->getIDFieldName() . " = $req_id ";
        $this->db_update($sql);

        /**
         * NOTIFY RELEVANT PARTY
         * if(auth_required) {
         *   check $old for authorisation setting
         * } else {
         *   check travel agent db for linked user
         * }
         */


        //LOGGING

        return array("ok", $response . "   <br /><br /><span style='font-weight:bold; font-size: 150%; color:#CC0001'>QUESTION???? Who must be notified IF Request requires Authorisation????</span>");
    }


    /**
     * Function to record authorisation of request and handle notifying booker and sending to travel agent
     */
    public function authoriseAccepted($req_id)
    {
        //update request status
        $old = $this->getRawObject($req_id);
        $status = $old['req_status'] - self::AUTHORISATION_REQUIRED + self::AUTHORISE_ACCEPTED;
        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . $status . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $this->db_update($sql);
        //logging

        //notify booker

        //send to TVLA
    }

    /**
     * Function to record rejection / non-authorisation and handle notifying the booker and sending the request back to NEW process
     */
    public function authoriseRejected($req_id)
    {
        //update request status
        $old = $this->getRawObject($req_id);
        $status = $old['req_status'] - self::ACTIVATED - self::AUTHORISATION_REQUIRED + self::AUTHORISE_REJECTED;
        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . $status . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $this->db_update($sql);
        //logging
        //notify booker
    }


    /**
     * function to get a list of requests waiting on authorisation - ADMIN view
     */
    public function getListOfRequestsAwaitingAuthorisationForAdmin()
    {
        return $this->getListOfRequests("ADMIN", "AUTHORISE");
    }

    /**
     * function to get a list of requests waiting on authorisation - MANAGE view
     */
    public function getListOfRequestsAwaitingAuthorisationForManage()
    {
        return $this->getListOfRequests("MANAGE", "AUTHORISE");
    }

    /**
     * Function to get a list of incomplete requests for NEW
     */
    public function getListOfIncompleteRequests()
    {
        return $this->getListOfRequests("NEW", "NEW");
    }

    /**
     * Function to get a list of activated requests for VIEW
     */
    public function getListOfActivatedRequests()
    {
        return $this->getListOfRequests("MANAGE", "VIEW");
    }

    /**
     * Function to get a list of requests for specific page & function
     */
    public function getListOfRequests($section, $page)
    {
        //Get headings
        $headObject = new DTM_HEADINGS();
        $headings = $headObject->getMainObjectHeadings("REQUEST", "LIST", $section, "", true);
        $head = array();
        foreach ($headings as $fld => $h) {
            $head[$fld] = $h['name'];
        }
        //$this->arrPrint($headings);
        //Get rows
        $listObject = new DTM_LIST("travel_type");
        $projectObject = new DTM_PROJECTS();
        $project_owners = $projectObject->getAllOwnersForRequestDisplay();
        $sql = "SELECT req.*
				  , req_booker as raw_req_booker
				  , CONCAT(tk.tkname,' ',tk.tksurname) as req_booker
				  , TT.id as raw_req_traveltypeid
				  , TT.name as req_traveltypeid
				  , req_policyid as raw_req_policyid
				  , CONCAT('" . DTM_POLICY::REFTAG . "',req_policyid) as req_policyid 
				  , req_benefitsid as raw_req_benefitsid
				  , CONCAT('" . DTM_BENEFITS::REFTAG . "',req_benefitsid) as req_benefitsid
				  , req_authoriser as raw_req_authoriser
				  , IF(req_authoriser='project','" . $this->getObjectName("project") . "-dependent',IF(req_authoriser='any','Any',CONCAT(auth.tkname,' ',auth.tksurname))) as req_authoriser
				  , req_approver as raw_req_approver 
				  , IF(req_approver='project','" . $this->getObjectName("project") . "-dependent',IF(req_approver='any','Any',CONCAT(appr.tkname,' ',appr.tksurname))) as req_approver
				  , req_project as raw_req_project
				  , proj." . $projectObject->getNameFieldName() . " as req_project
				FROM " . $this->getTableName() . " req 
				INNER JOIN " . $this->getUserTableName() . " tk
				  ON req_booker = tk.tkid
				INNER JOIN " . $listObject->getListTable() . " TT
				  ON req_traveltype = TT.id
				LEFT OUTER JOIN " . $this->getUserTableName() . " auth
				  ON req_authoriser = auth.tkid
				LEFT OUTER JOIN " . $this->getUserTableName() . " appr
				  ON req_approver = appr.tkid
				LEFT OUTER JOIN " . $projectObject->getTableName() . " proj
				  ON req_project = proj." . $projectObject->getIDFieldName() . "
				WHERE 
					(req_status & " . self::ACTIVE . ") = " . self::ACTIVE . "
				";
        switch ($page) {
            case "NEW":
                $where = " AND req_status = " . self::ACTIVE . " 
					AND req_booker = '" . $this->getUserID() . "'";
                break;
            case "VIEW":
                $where = "
					AND (req_status & " . self::ACTIVATED . ") = " . self::ACTIVATED . "
					";
                break;
            case "AUTHORISE":
                $where = "
					AND (req_status & " . self::ACTIVATED . ") = " . self::ACTIVATED . "
					AND (req_status & " . self::AUTHORISATION_REQUIRED . ") = " . self::AUTHORISATION_REQUIRED . "
					AND (req_status & " . self::AUTHORISE_ACCEPTED . ") <> " . self::AUTHORISE_ACCEPTED . "
					AND (req_status & " . self::AUTHORISE_REJECTED . ") <> " . self::AUTHORISE_REJECTED . "
						
					";
                if ($section == "MANAGE") {
                    $where .= "
							AND (
								req_authoriser = '" . $this->getUserID() . "'
								OR
								req_authoriser = 'any'
								OR
								req_authoriser = 'project'
							)
							";
                }
                break;
            case "APPROVE":
                break;
        }
        $sql .= $where;
        $rows = $this->mysql_fetch_all_by_id($sql, "req_id");

        //Get list of travel agents
        $agentObject = new DTM_AGENTS();
        $agents = $agentObject->getAllTravelAgentsForDTM();

        $displayObject = new DTM_DISPLAY();

        $project_ids = array();
        $projectObject = new DTM_PROJECTS();
        //get list of projects ids from rows
        // & set display format of travel insurance
        foreach ($rows as $i => $r) {
            if ($r['req_travelinsurance'] == 0) {
                $rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(0);
            } else {
                $rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(1);
            }
            $rows[$i]['raw_req_travelagent'] = $r['req_travelagent'];
            $rows[$i]['req_travelagent'] = isset($agents[$r['req_travelagent']]) ? $agents[$r['req_travelagent']] : $r['req_travelagent'];
            if (!in_array($r['raw_req_project'], $project_ids)) {
                $project_ids[] = $r['raw_req_project'];
            }
        }
        //get list of parents
        if (count($project_ids) > 0) {
            $project_parents = $projectObject->getProjectHierarchyForRequest($project_ids, true);
            //get authorisers/approvers for all
            $project_owners = $projectObject->getAllOwners();
            //generate list of owners for original list of ids
            $project_authorisers = array();
            $project_approvers = array();
            foreach ($project_parents as $pi => $pp) {
                $project_authorisers[$pi] = array();
                $project_approvers[$pi] = array();
                foreach ($pp as $p) {
                    if (isset($project_owners[$p])) {
                        foreach ($project_owners[$p] as $ti => $t) {
                            if ($t['authoriser'] == true) {
                                $project_authorisers[$pi][$ti] = $t['name'];
                            }
                            if ($t['approver'] == true) {
                                $project_approvers[$pi][$ti] = $t['name'];
                            }
                        }
                    }
                }
            }
            foreach ($rows as $ri => $r) {
                $pi = $r['raw_req_project'];
                //check rows and remove those without access
                if ($section == "MANAGE" && $page == "AUTHORISE" && $r['raw_req_authoriser'] == "project" && !isset($project_authorisers[$pi][$this->getUserID()])) {
                    unset($rows[$ri]);
                } elseif ($section == "MANAGE" && $page == "APPROVE" && $r['raw_req_approver'] && !isset($project_approvers[$pi][$this->getUserID()])) {
                    unset($rows[$ri]);
                } else {
                    if ($r['raw_req_authoriser'] == "project") {
                        $rows[$ri]['req_authoriser'] = implode(";<br />", $project_authorisers[$pi]) . ";";
                    }
                    if ($r['raw_req_approver'] == "project") {
                        $rows[$ri]['req_approver'] = implode(";<br />", $project_approvers[$pi]) . ";";
                    }
                }
            }
        }


        $req_ids = array_keys($rows);
        if (count($req_ids) > 0) {
            $travellerObject = new DTM_REQUEST_TRAVELLERS();
            $travellers = $travellerObject->getList($req_ids);
            foreach ($req_ids as $r) {
                $rows[$r]['traveller'] = implode(", ", $travellers[$r]);
            }
        }

        $data = array(
            'head' => $head,
            'rows' => $rows
        );


        return $data;
    }


    public function getRawObject($req_id)
    {
        $listObject = new DTM_LIST("travel_type");
        $sql = "SELECT req.*
				  , CONCAT(tk.tkname,' ',tk.tksurname) as booker
				  , CONCAT('" . DTM_POLICY::REFTAG . "',req_policyid) as policy 
				  , CONCAT('" . DTM_BENEFITS::REFTAG . "',req_benefitsid) as benefits
				  , tt.shortcode as travel_type
				FROM " . $this->getTableName() . " req
				INNER JOIN " . $this->getUserTableName() . " tk
				  ON req_booker = tk.tkid
				INNER JOIN " . $listObject->getListTable() . " tt
				  ON tt.id = req.req_traveltype
				WHERE req_id = " . $req_id;
        $data = $this->mysql_fetch_one($sql);
        foreach ($data as $key => $d) {
            $k = explode("_", $key);
            unset($k[0]);
            $key = implode("_", $k);
            if (!isset($data[$key])) {
                $data[$key] = $d;
            }
        }
        $data['travellers'] = array();
        $data['travellers_benefit_category'] = array();
        $data['benefit_category_id'] = array();
        $data['travellers_extras'] = array();
        $rtObject = new DTM_REQUEST_TRAVELLERS();
        $rt = $rtObject->getList($req_id, true);
        foreach ($rt as $ti => $t) {
            $data['travellers'][$ti] = $t['name'];
            $data['travellers_benefit_category'][$ti] = $t['benefit_category'];
            $data['travellers_benefit_category_id'][$ti] = $t['benefit_category_id'];
            if (!in_array($t['benefit_category_id'], $data['benefit_category_id'])) {
                $data['benefit_category_id'][] = $t['benefit_category_id'];
            }
            $data['travellers_extras'][$ti] = isset($t['rt_extras']) && strlen($t['rt_extras']) > 0 ? $rtObject->decodeExtrasFromDB($t['extras']) : array();
        }

        $pObject = new DTM_PROJECTS();
        $data['projects'] = $pObject->getProjectHierarchyForRequest($data['project']);

        return $data;
    }


    public function getBenefitSettings($req_id)
    {
        return $_SESSION[$this->getModRef()]['REQUEST'][$req_id]['benefits']['settings'];
    }


    public function __destruct()
    {
        parent::__destruct();
    }


}

?>
