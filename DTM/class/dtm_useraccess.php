<?php
/**
 * To manage the user access of the DTM module
 *
 * Created on: 1 July 2016
 * Authors: Janet Currie
 *
 */

class DTM_USERACCESS extends DTM {

	private $table_name = "_useraccess";

	private $list_field_name = "ua_benefit_category";

	private $field_names = array(
		'ua_benefit_category'	=> "Benefit Category",
		'ua_traveller'			=> "Traveller",
		'ua_create_own'			=> "Create Own Travel Requests",
		'ua_create_booker'		=> "Create Proxy Travel Requests",
		'ua_super_authorise'	=> "Super Authorisor",
		'ua_edit'				=> "Edit",
		'ua_authorise'			=> "Authorise",
		'ua_approve'			=> "Approve",
		'ua_edit_all'			=> "Edit All",
		'ua_update_all'			=> "Update All",
		'ua_authorise_all'		=> "Authorise All",
		'ua_approve_all'		=> "Approve All",
		'ua_policy_admin'		=> "Policy Administrator",
		'ua_policy_super'		=> "Policy Approver",
		'ua_project_admin'		=> "Project / Cost Centre Administrator",
		'ua_report'				=> "Reports",
		'ua_setup'				=> "Setup",
	);
		//'ua_assurance'			=> "Assurance Provider",

	private $field_types = array(
		'ua_benefit_category'	=> "LIST",
	);
	private $field_source = array(
		'ua_benefit_category'	=> "list_benefit_categories",
	);
	private $field_list_values = array();

		private $field_defaults = array(
		'ua_benefit_category'	=> 0,
		'ua_traveller'			=> 0,
		'ua_create_own'			=> 0,
		'ua_create_booker'		=> 0,
		'ua_super_authorise'	=> 0,
		'ua_edit'				=> 0,
		'ua_authorise'			=> 0,
		'ua_approve'			=> 0,
		'ua_edit_all'			=> 0,
		'ua_update_all'			=> 0,
		'ua_authorise_all'		=> 0,
		'ua_approve_all'		=> 0,
		'ua_policy_admin'		=> 0,
		'ua_policy_super'		=> 0,
		'ua_project_admin'		=> 0,
		'ua_report'				=> 0,
		'ua_setup'				=> 0,
	);

	public function __construct($store=false) {
		parent::__construct();
		$this->field_names = $this->replaceObjectNames($this->field_names);
		$this->table_name = $this->getDBRef().$this->table_name;
		//Module admin is depreciated
		unset($this->field_names['ua_module']);
		unset($this->field_defaults['ua_module']);
		if($store) {
			if(!isset($_SESSION[$this->getModRef()]['useraccess']) || $_SESSION[$this->getModRef()]['useraccess']['valid_until']<time()) {
				$ua = $this->getMyUserAccess();
				$ua['valid_until'] = time()+1800;	//set validity for 30 minutes
				$_SESSION[$this->getModRef()]['useraccess'] = $ua;
			}
		}
	}

	/*******************************************************************************************
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['ua_status'])) { $var['ua_status'] = self::ACTIVE; }
		if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
        $var['ua_setup'] = 1;

		return $this->addUser($var);
	}

	public function editObject($var){
		$id = $var['ua_id'];
		unset($var['ua_id']);
		return $this->editUser($id,$var);
	}

    public function removeObject($var){
        return $this->removeUser($var);
    }

	public function getAUserAccess($var) {
		return $this->getMyUserAccess("",$var['id'],$var['process']);
	}


	/**********************************************************************************************
	 * GET functions
	 */
	public function getUserAccessFields() { return $this->field_names; }
	public function getUserAccessDefaults()	{ return $this->field_defaults; }
	public function getUserAccessTypes()	{ return $this->field_types; }
	public function getUserAccessSources()	{
		$data = array();
		foreach($this->field_types as $key => $ft) {
			if($ft=="LIST" && isset($this->field_source[$key])) {
				$listObject = new DTM_LIST($this->field_source[$key]);
				$x = $listObject->getActiveListItemsFormattedForSelect();
				if($key=="ua_benefit_category") {
					$x = array(0=>"Non-traveller")+$x;
				}
				$data[$key] = $x;
				$this->field_list_values[$key] = $x;
				unset($listObject);
			}
		}
		return $data;
	}
	public function getTableName() { return $this->table_name; }
	public function getListFieldName() { return $this->list_field_name; }
	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccess() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL 
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");

	}
	/**
	 * Get the list of users with access
	 */
	public function getActiveUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, UA.*
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		$data = $this->mysql_fetch_all_by_id($sql, "tkid");
		if(count($this->field_list_values)==0) {
			$src = $this->getUserAccessSources();
		} else {
			$src = $this->field_list_values;
		}
		foreach($data as $key=>$d) {
			foreach($this->field_types as $fld=>$ft) {
				if($ft=="LIST") {
					$data[$key]['raw_'.$fld] = $d[$fld];
					$data[$key][$fld] = isset($src[$fld][$d[$fld]]) ? $src[$fld][$d[$fld]] : $this->getUnspecified();
				}
			}
		}
		return $data;
			//return $sql;
	}
	public function getActiveUsersFormattedForSelect($ids="") {
		$rows = $this->getActiveUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	/**
	 * Get the current user's access
	 */
	public function getMyUserAccess($ui="",$id=false,$process=true) {
		if($id!==false) {
			$where = "ua_id = $id";
		} else {
			if(strlen($ui)==0) {
				$ui = $this->getUserID();
			}

			// And now for a hack that uses the dtm db's user_id instead of the tkid
            $sql = 'SELECT * FROM dtm_companies ';

            $sql .= 'INNER JOIN dtm_users ';
            $sql .= 'ON dtm_users.user_company = dtm_companies.comp_id ';
            $sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
            $sql .= 'AND dtm_users.user_tkid = \'' . $ui . '\'';

            $user = $this->mysql_fetch_one($sql);


			$where = "ua_user_id = ".$user['user_id']."";
		}
//		if($this->isAdminUser()) {
//			$row = $this->field_defaults;
//			foreach($row as $key => $r) {
//				$row[$key] = 0;
//			}
//			$row['ua_setup'] = 1;
//			$row['ua_tkid'] = $ui;
//			$row['ua_new'] = 0;
//			$row['ua_admin'] = 0;
//			//$row['ua_assurance'] = 0;
//			$row['ua_status'] = self::ACTIVE;
//		} else {
			$sql = "SELECT UA.* FROM ".$this->getTableName()." UA WHERE ".$where;
			$row = $this->mysql_fetch_one($sql);
//		}
		if(!isset($row['ua_status'])) {
			$row = $this->field_defaults;
			$row['ua_status'] = 0;
			$row['ua_tkid'] = $ui;
			$row['ua_manage'] = 1;
            $row['ua_setup'] = 0;
		} else {
			$row['ua_manage'] = 1;
		}
		if($process) {
			$user_access = array();
			foreach($row as $key => $val){
				$user_access[substr($key,3)] = $val;
			}
		} else {
			$user_access = $row;
		}
		return $user_access;
	}



	/**
	 * Needed for Traveller's Profile
	 */
	public function getBenefitCategory($user_id="") {
		if(strlen($user_id)==0) {
			$user_id = $this->getUserID();
		}
		$user = $this->getActiveUsers($user_id);
		return array('id'=>$user[$user_id]['raw_'.$this->list_field_name],'name'=>$user[$user_id][$this->list_field_name]);
	}


	/**
	 * Function to return array of current users with POLIC_SUPER access
	 */
	public function getPolicyAuthorisers($for_email=true) {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND UA.ua_policy_super = 1
				ORDER BY tkname, tksurname";
		if($for_email) {
			$rows = $this->mysql_fetch_all_by_id($sql, "id");
			$data = array();
			foreach($rows as $key => $r) {
				unset($r['id']);
				$data[$key] = $r;
			}
		} else {
			$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		}
		return $data;
	}

	/**
	 * Function to return array of current users with BOOKER
	 */
	public function getAvailableProxyUsers($for_email=false) {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND UA.ua_create_booker = 1
				ORDER BY tkname, tksurname";
		if($for_email) {
			$rows = $this->mysql_fetch_all_by_id($sql, "id");
			$data = array();
			foreach($rows as $key => $r) {
				unset($r['id']);
				$data[$key] = $r;
			}
		} else {
			$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		}
		return $data;
	}

	/**
	 * Function to return array of current users with TRAVELLER where login user has PROXY access
	 */
	public function getAvailableTravellers() {
		$profileObject = new DTM_TRAVELLERS_PROFILE();
		$proxy = $profileObject->getTravellersWhoGaveMeProxyAccess();

		if(count($proxy) > 0) {
			$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email, UA.ua_benefit_category as benefit_category
					FROM assist_".$this->getCmpCode()."_timekeep TK
					INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
					ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
					INNER JOIN ".$this->getTableName()." UA
					ON ua_tkid = tkid
					WHERE tkstatus = 1
					AND UA.ua_traveller = 1
					AND (
							( ua_tkid IN ('".implode("','",$proxy)."') )
						OR
							(ua_tkid = '".$this->getUserID()."' AND UA.ua_create_own = 1) 
					)
					ORDER BY tkname, tksurname";
			$rows = $this->mysql_fetch_all($sql);
		} else {
			$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email, UA.ua_benefit_category as benefit_category
					FROM assist_".$this->getCmpCode()."_timekeep TK
					INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
					ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
					INNER JOIN ".$this->getTableName()." UA
					ON ua_tkid = tkid
					WHERE tkstatus = 1
					AND UA.ua_traveller = 1
					AND (ua_tkid = '".$this->getUserID()."' AND UA.ua_create_own = 1) 
					ORDER BY tkname, tksurname";
			$rows = $this->mysql_fetch_all($sql);
		}
		$data = array('select'=>array(),'benefit_category'=>array());
		foreach($rows as $r) {
			$data['select'][$r['id']] = $r['name'];
			$data['benefit_category'][$r['id']] = $r['benefit_category'];
		}



		return $data;
	}




	/**
	 * Function to return array of current users with AUTHORISE
	 */
	public function getAuthorisersForSelect() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND UA.ua_authorise = 1
				ORDER BY tkname, tksurname";
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $data;
	}


	/**
	 * Function to return array of current users with APPROVER
	 */
	public function getApproversForSelect() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND UA.ua_approve = 1
				ORDER BY tkname, tksurname";
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $data;
	}



	/****************************************************************************************************
	 * SET / UPDATE functions
	 */

	/**
	 * Adds a new user
	 */
	private function addUser($var){
		$result = array("info","No change was found to be saved.");
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$result = array("ok","User added successfully.");
		} else {
		/*
		 * */
			$result = array("error","Sorry, something went wrong while trying to add the user.  Please try again.");
		}
		return $result;
	}


	private function editUser($id,$var) {
		if(ASSIST_HELPER::checkIntRef($id)) {
			$old = $this->mysql_fetch_one("SELECT * FROM ".$this->getTableName()." WHERE ua_id = ".$id);
			//$edits = array();
			//foreach($var as $fld=>$v){
			//	$edits[substr($fld,5)] = $v;
			//}
			$edits = $var;
			$update_data = $this->convertArrayToSQL($edits);
			$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ua_id = ".$id;
			$mar = $this->db_update($sql);
			if($mar>0){
				$un = $this->getAUserName($old['ua_tkid']);
				$user_access_fields = $this->getUserAccessFields();
				$changes = array(
					'user'=>$this->getUserName(),
					'response'	=> "Edited user access for user: ".$un,
				);
				if(count($this->field_list_values)==0) {
					$src = $this->getUserAccessSources();
				} else {
					$src = $this->field_list_values;
				}
				foreach($old as $key=>$value){
					if(isset($edits[$key]) && $value != $edits[$key]){
						if(isset($this->field_types[$key]) && $this->field_types[$key]=="LIST") {
							$a = isset($src[$key][$edits[$key]]) ? $src[$key][$edits[$key]] : $this->getUnspecified();
							$z = isset($src[$key][$value]) ? $src[$key][$value] : $this->getUnspecified();
							$changes[$user_access_fields[$key]] =array('to'=>$a, 'from'=>$z);
						} else {
							$changes[$user_access_fields[$key]] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						}
						//$changes[$key] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
					}
				}

				$log_var = array(
					'section'	=> "USER",
					'object_id'	=> $id,
					'changes'	=> $changes,
					'log_type'	=> DTM_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
				return array("ok","Changes to ".$un." have been saved successfully.");
			} else {
				return array("info","No change was found to be saved.");
			}
		}
		return array("error","An error occurred while trying to save the changes. Please try again.");
	}



    private function removeUser($var){
        $sql = 'DELETE FROM '.$this->getTableName().' ';
        $sql .= 'WHERE ua_user_id = '. $var['user_id'] .' ';
        $id = $this->db_update($sql);
        if($id>0) {
            $result = array("ok","User access to setup removed successfully.");
        } else {
            /*
             * */
            $result = array("error","Sorry, something went wrong while trying to add the user.  Please try again.");
        }
        return $result;
    }




	/********
	 * Check if the user has the permission to over ride authorisation process
	 */
	public function amISuperAuthoriser() {
		return $_SESSION[$this->getModRef()]['useraccess']['super_authorise']==true;
	}

}



?>