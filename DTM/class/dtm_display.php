<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */
class DTM_DISPLAY extends ASSIST_MODULE_DISPLAY {


    protected $default_attach_buttons = true;

    public function __construct() {
        $me = new DTM();
        $an = $me->getAllActivityNames();
        $on = $me->getAllObjectNames();
        unset($me);

        parent::__construct($an,$on);
    }

    public function disableAttachButtons() { $this->default_attach_buttons = false; }
    public function enableAttachButtons() { $this->default_attach_buttons = true; }


    /***********************************************************************************************************************
     * FORM FIELDS & specific data display
     */


    public function getDataFieldNoJS($type,$val,$options=array()) {
        $d = $this->getDataField($type, $val,$options);
        return $d['display'];
    }
    /**
     * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
     * @param (String) type = format of the field (Bool, Currency etc)
     * @param (String) data = the actual value to be displayed
     * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
     *
     * @return JS (Echos HTML)
     */
    public function drawDataField($type,$val,$options=array()) {
        $d = $this->getDataField($type,$val,$options);
        if(is_array($d)) {
            echo $d['display'];
        } else {
            echo $d;
        }
    }
    /**
     * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
     * @param (String) type = format of the field (Bool, Currency etc)
     * @param (String) data = the actual value to be displayed
     * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
     * 	 *
     * @return (Array) array('display'=>HTML,'js'=>javascript)
     */
    public function getDataField($type,$val,$options=array()) {
        $data = array('display'=>"",'js'=>"");
        switch($type) {
            case "PERC":
                $data['display'] = $this->getPercentageForDisplay($val);
                break;;
            case "CURRENCY":
                if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
                if(isset($options['symbol'])) {
                    $data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
                } else {
                    $data['display'] = $this->getCurrencyForDisplay($val);
                }
                if(isset($options['right']) && $options['right']==true) {
                    $data['display'] = "<div class=right >".$data['display']."</div>";
                }
                break;
            case "DATE":
                if(isset($options['include_time'])) {
                    $data['display'] = $this->getDateForDisplay($val,$options['include_time']);
                } else {
                    $data['display'] = $this->getDateForDisplay($val,false);
                }
                break;
            case "BOOL":
            case "BOOL_BUTTON":
                $data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
                break;
            case "REF":
                $data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
                break;
            case "ATTACH":
                $can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
                $object_type = isset($options['object_type']) ? $options['object_type'] : "X";
                $object_id = isset($options['object_id']) ? $options['object_id'] : "0";
                $buttons = isset($options['buttons']) ? $options['buttons'] : true;
                $data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
                break;
            //JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
            default:
                if(isset($options['html']) && $options['html']==true) {
                    $val = str_ireplace(chr(10), "<br />", $val);
                } else {
                    $val = $val;
                }
                $data = array('display'=>$val,'js'=>"");
            //$data = array('display'=>$type);
        }
        return $data;
    }


    /**
     * (ECHO) displays html of selected form field and returns any required js
     */
    public function drawFormField($type,$options=array(),$val="") {
        //echo "dFF VAL:-".$val."- ARR: "; print_r($options);
        $ff = $this->createFormField($type,$options,$val);
        //if(is_array($ff['display'])) {
        //print_r($ff);
        //}
        echo $ff['display'];
        return $ff['js'];
    }
    /**
     * Returns string of selected form field
     *
     * @param *(String) type = the type of form field
     * @param (Array) options = any additional properties
     * @param (String) val = any existing value to be displayed
     * @return (String) echo
     */
    public function createFormField($type,$options=array(),$val="") {
        //echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
        switch($type){
            case "REF":
                $data = array('display'=>$val,'js'=>"");
                break;
            case "LABEL":
                $data=$this->getLabel($val,$options);
                break;
            case "SMLVC":
                $data=$this->getSmallInputText($val,$options);
                break;
            case "MEDVC":
                $data=$this->getMediumInputText($val,$options);
                break;
            case "LRGVC":
                $data=$this->getLimitedTextArea($val,$options);
                break;
            case "TEXT":
                if(isset($options['rows']) && isset($options['cols'])) {
                    $rows = $options['rows']; unset($options['rows']);
                    $cols = $options['cols']; unset($options['cols']);
                    $data=$this->getTextArea($val,$options,$rows,$cols);
                } else {
                    $data=$this->getTextArea($val,$options);
                }
                break;
            case "LIST":
                $items = $options['options'];
                unset($options['options']);
                $data=$this->getSelect($val,$options,$items);
                break;
            case "MULTILIST":
                $items = $options['options'];
                unset($options['options']);
                if(!is_array($val)) {
                    $val2 = array();
                    if(strlen($val)>0) {
                        $val2[] = $val;
                    }
                }
                $data=$this->getMultipleSelect($val2,$options,$items);
                break;
            case "DATE": //echo "date!";
                $extra = $options['options'];
                unset($options['options']);
                $data=$this->getDatePicker($val,$options,$extra);
                break;
            case "COLOUR":
                $data=$this->getColour($val,$options);
                break;
            case "RATING":
                $data=$this->getRating($val, $options);
                break;
            case "CURRENCY": $size = 15;
            case "PERC":
            case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
            case "NUM": $size = !isset($size) ? 0 : $size;
                if(isset($options['symbol'])) {
                    $symbol = $options['symbol'];
                    $has_sym = true;
                    unset($options['symbol']);
                    $symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
                    unset($options['symbol_postfix']);
                } else {
                    $has_sym = false;
                } //ASSIST_HELPER::arrPrint($options);
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
                    if(!$has_sym) {
                        $data['display']="R&nbsp;".$data['display'];
                    } elseif(strlen($symbol)>0) {
                        if($symbol_postfix==true) {
                            $data['display']=$data['display']."&nbsp;".$symbol;
                        } else {
                            $data['display']=$symbol."&nbsp;".$data['display'];
                        }
                    } else {
                        //don't add a symbol
                    }
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                    $data['display'].="&nbsp;%";
                }
                break;
            case "BOOL_BUTTON":
                //echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
                $data = $this->getBoolButton($val,$options);
                break;
            case "ATTACH"://echo "attach: ".$val;
                //$data = array('display'=>$val,'js'=>"");
                $data = $this->getAttachmentInput($val,$options);
                break;
            case "CALC" :
                $options['extra'] = explode("|",$options['extra']);
                $data = $this->getCalculationForm($val,$options);
                break;
            default:
                $data = array('display'=>$val,'js'=>"");
                break;
        }
        return $data;
    }


















    /**
     * (ECHO) Function to display the activity log link onscreen
     *
     * @param (HTML) left = any html code to show on the left side of the screen
     * @param (String) log_table = table to query for logs
     * @param (QueryString) var = any additional variables to be passed to the log class
     * @return (ECHO) html to display log link
     * @return (JS) javascript to process log link click
     */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
        $data = $this->getPageFooter($left,$log_table,$var,$log_id);
        echo $data['display'];
        return $data['js'];
    }
    /**
     * Function to create the activity log link onscreen
     *
     * @param (HTML) left = any html code to show on the left side of the screen
     * @param (String) log_table = table to query for logs
     * @param (QueryString) var = any additional variables to be passed to the log class
     * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
     */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
        $echo = "";
        $js = "";
        if(is_array($var)) {
            $x = $var;
            $d = array();
            unset($var);
            foreach($x as $f => $v) {
                $d[] = $f."=".$v;
            }
            $var = implode("&",$d);
        }
        $echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
        if(strlen($log_table)>0){
            $js = "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
        }
        $data = array('display'=>$echo,'js'=>$js);
        return $data;
    }
    

















    /***********************************************************************************************************************
     * LIST VIEWS
     */




    /**
     * Paging....
     */
    public function drawPaging() {
        $me = $this->getPaging();
        echo $me['display'];
        return $me['js'];
    }
    public function getPaging($i,$options,$button,$object_type="",$object_options=array(),$add_button=array(false,"","")) {
        /**************
        $options = array(
        'totalrows'=> mysqli_num_rows(),
        'totalpages'=> totalrows / pagelimit,
        'currentpage'=> start / pagelimit,
        'first'=> start==0 ? false : 0,
        'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
        'prev'=> start==0 ? false : (start - pagelimit),
        'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
        'pagelimit'=>pagelimit,
        );
         **********************************/
        $data = array('display'=>"",'js'=>"");
        $data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier center'><span id='$i'>
					<span style='float: left' id=spn_paging_buttons>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
        for($p=1;$p<=$options['totalpages'];$p++) {
            $data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
        }
        $data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
					</span>";
        if(isset($add_button [0]) && $add_button[0]==true) {
            $ab_label = $this->replaceAllNames(isset($add_button[1]) && strlen($add_button[1])>0 ? $add_button[1] : "|add|");
            $data['display'].="<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
            $data['js'].="
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).click(function(e) {
				e.preventDefault();
				".$add_button[2]."
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\").children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"})
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); });
			";
        }
        $data['display'].="<!-- <span class=float id=spn_paging_search>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
        $op = "options[set]=true";
        foreach($object_options as $key => $v) {
            $op.="&options[$key]=$v";
        }
        $data['js'].= "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';
		
		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i."').children('#spn_paging_buttons').find('button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
        return $data;
    }


    /*****
     * List View
     */
    /**
     * Function to draw the list table onscreen
     * @param (Array) $child_objects = array of objects to display in table
     * @param (Array) $button = details of button to display in last column
     * @param (String) $object_type = the name of the object
     * @param (Array) $object_options = getPaging options
     * @param (Bool) $sub_table = is this table a child of another object true/false
     * @param (Array) $add_button = array(0=>true/false, 1=>label if true)
     */
    public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false,$add_button=array(false,"","")) {
        //ASSIST_HELPER::arrPrint($child_objects);
        $me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table,$add_button);
        echo $me['display'];
        return $me['js'];
    }
    public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false, $add_button = array(false,"","")) {
        //ASSIST_HELPER::arrPrint($child_objects); 
        $data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
        //$items_total = $contractObject->getPageLimit();
        //$these_items = array_chunk($child_objects['rows'], $items_total);
        $page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
        $page_options = $child_objects['paging'];
        $display_paging = true;
        if($button===false) {
            $display_paging = false;
        } elseif(isset($button['pager']) && $button['pager']===false) {
            $display_paging = false;
        }
        if($display_paging){
            $paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options,$add_button);
            $data['display'].=$paging['display'];
            $data['js'].=$paging['js'];
        }
        unset($button['pager']);
        $data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
        foreach($child_objects['head'] as $fld=>$head) {
            $data['display'].="<th>".$head['name']."</th>";
        }

        $data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
        $rows = $this->getListTableRows($child_objects, $button);
        $data['display'].=$rows['display']."</table>";
        $data['display'].="</td></tr></table>";
        $data['js'].=$rows['js'];
        return $data;
    }
    public function getListTableRows($child_objects,$button) {
        $data = array('display'=>"",'js'=>"");
        if(count($child_objects['rows'])==0) {
            $data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
        } else {
            foreach($child_objects['rows'] as $id => $obj) {
                $data['display'].="<tr>";
                foreach($obj as $fld=>$val) {
                    if(is_array($val)) {
                        if(isset($val['display'])) {
                            $data['display'].="<td>".$val['display']."</td>";
                            $data['js'].=isset($val['js']) ? $val['js'] : "";
                        } else {
                            $data['display'].="<td class=idelete>".$fld."</td>";
                        }
                    } else {
                        $data['display'].="<td>".$val."</td>";
                    }
                }
                if(isset($button['type']) && $button['type']=="button") {
                    $data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><button ref=".$id." class='".$button['class']." ui_btn' />".$button['value']."</button></td>" : "");
                    $data['js'].="
					$('button.ui_btn').button({
						icons: {primary: \"ui-icon-pencil\"},
					}).children(\".ui-button-text\").css({\"font-size\":\"80%\"}).click(function(e) {
						e.preventDefault();
						
					}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")
					";
                } else {
                    $data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
                }
                $data['display'].="</tr>";
            }
        }
        return $data;
    }















    













    
















































































    /**
     * Returns filter for selecting parent objects according to section it is fed
     *
     * @param *(String) section = the current object whose parents need to be found
     * @param (Array) options = an array of options to be displayed in the filter
     * @return (String) echo
     */
    public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
        $data['display']="
			<div id='filter'>";

        switch (strtoupper($section)) {
            case 'CONTRACT':
                $data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
                foreach($array as $index=>$key){
                    $data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
                }
                $data['js']="
					fyear = 0;
					
					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
                break;

            case 'DELIVERABLE':
                $dsp = "contracts get";

                break;

            case 'ACTION':
                $dsp = "deliverables get";

                break;

            default:
                $dsp = "Invalid arguments supplied";
                break;
        }

        $data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";

        return $data;
    }


    /**
     * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
     *
     * @param *(String) section = the current object whose list table you want to draw
     * @param (Array) fyears = an array of financial years taken from DTM_MASTER->getActiveItems();
     * @param (Array) headings = an array of the headings to use for the top row of the table, taken from DTM_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
     * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
     */
    public function getListView($section, $fyears=array(), $headings=array()){
        $data = array('display'=>"",'js'=>"");
        switch(strtoupper($section)){
            case "CONTRACT":
                $filter = $this->getFilter($section, $fyears);
                //For the financial year filter
                $data['display'] = $filter['display'];
                //For the list view table
                $data['display'].=
                    '<table class=tbl-container><tr><td>
										<table class=list id=master_list>	
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';

                foreach($headings as $key=>$val){
                    $string = "<th id='".$val['field']."' >".$val['name']."</th>";
                    $data['display'].= $string;
                }
                $data['display'].=
                    "<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
                //For the javascript

                break;
            default:
                $data['display'] = "Lol, not for you";
                break;
        }

        return $data;
    }
}


?>