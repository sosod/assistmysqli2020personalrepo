<?php

class DTM_USERS extends DTM
{

    private $table_name = "DTM_users";
    private $table_field = "dtm_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "users";
    const TABLE_FLD = "user_";
    const REFTAG = "HDR";
    const LOG_TABLE = "object";

    public function __construct(){
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix().$this->id_field;
        $this->status_field = $this->getTableFieldPrefix().$this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getApproverFieldName() {
        return $this->approver_field;
    }
    public function getTableFieldPrefix() { return $this->table_field; }


    //Tshegofatso Mosidi
    public function addObject($var) {

        $user_tkid = $var['dtm_user'];
        unset($var['dtm_user']);

        $client_db = new ASSIST_MODULE_HELPER('client', $var['comp_cc']);

        $sql = 'SELECT * FROM dtm_companies ';
        $sql .= 'INNER JOIN dtm_users ';
        $sql .= 'ON dtm_users.user_company = dtm_companies.comp_id ';
        $sql .= 'WHERE dtm_companies.comp_ait_cc = \''. $var['comp_cc'] . '\' ';
        $sql .= 'AND dtm_users.user_tkid = \'' . $user_tkid . '\'';
        $user = $this->mysql_fetch_one($sql);

        if(isset($user) && is_array($user) && count($user) > 0){//User exists in dtm_db, just reactivate
            //Activate the company
            $sql = 'UPDATE dtm_users ';
            $sql .= 'SET user_active = 1 ';
            $sql .= 'WHERE user_id = ' . $user['user_id'];
            $id = $this->db_update($sql);

            $log_text = $user['user_name_ait'] . ' has been successfully reactivated in the DTM system, by: ' . $_SESSION['tkn'];

            $company = $user;
            $id = $user['user_id'];
        }else{//User doesn't exist in dtm_db, so create then activate
            //Get user information from the company db
            $sql = 'SELECT * FROM assist_'.$var['comp_cc'].'_timekeep ';
            $sql .= 'WHERE tkstatus = \'1\' ';
            $sql .= 'AND tkid = \''. $user_tkid .'\' ';
            $user_tk_info = $client_db->mysql_fetch_one($sql);


            $sql = 'SELECT * FROM dtm_companies ';
            $sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$var['comp_cc'] . '\' ';
            $company = $this->mysql_fetch_one($sql);

            $user_id = $user_tkid;
            $user_name = $user_tk_info['tkname'] . ' ' . $user_tk_info['tksurname'];
            $user_company = $company['comp_id'];


            //Enter information in the DTM user table
            $user_values = array();
            $user_values[$this->getTableField() . 'tkid'] = $user_id;
            $user_values[$this->getTableField() . 'name_ait'] = $user_name;
            $user_values[$this->getTableField() . 'company'] = $user_company;

            $sql = 'INSERT INTO DTM_users ('. $this->getTableField() . 'tkid,'. $this->getTableField() . 'name_ait,'. $this->getTableField() . 'company, '. $this->getTableField() . 'active) ';
            $sql .= 'VALUES (\''.$user_values[$this->getTableField() . 'tkid'].'\', ';
            $sql .= '\''. $user_values[$this->getTableField() . 'name_ait'] .'\', ';
            $sql .= ''.$user_values[$this->getTableField() . 'company'].', ';
            $sql .= '1)';
            $id = $this->db_insert($sql);

            $log_text = $user_name . ' has been successfully created in the DTM system, by: ' . $_SESSION['tkn'];
        }






        if ($id > 0) {

            $user_id = $id;

            //Check that the connection between user and module exists in the database
            $sql = 'SELECT * FROM assist_menu_modules ';

            $sql .= 'INNER JOIN assist_'.$company['comp_ait_cc'].'_menu_modules_users ';
            $sql .= 'ON assist_'.$company['comp_ait_cc'].'_menu_modules_users.usrmodid = assist_menu_modules.modid ';

            $sql .= 'INNER JOIN assist_'.$company['comp_ait_cc'].'_timekeep ';
            $sql .= 'ON assist_'.$company['comp_ait_cc'].'_timekeep.tkid = assist_'.$company['comp_ait_cc'].'_menu_modules_users.usrtkid ';

            $sql .= 'WHERE assist_menu_modules.modlocation = \'DTM\' ';
            $sql .= 'AND assist_'.$company['comp_ait_cc'].'_timekeep.tkid = \''. $user_tkid .'\' ';

            $access_to_module = $client_db->mysql_fetch_one($sql);

            $success = false;
            if(isset($access_to_module) && is_array($access_to_module) && count($access_to_module) > 0){
                //Do nothing and just return a positive message
                $success = true;
            }else{
                $sql = 'SELECT * FROM assist_menu_modules ';
                $sql .= 'WHERE assist_menu_modules.modlocation = \'DTM\' ';
                $mod_info = $client_db->mysql_fetch_one($sql);

                //Make the connection
                $sql = 'INSERT INTO assist_'.$company['comp_ait_cc'].'_menu_modules_users (usrmodid,usrtkid,usrmodref) ';
                $sql .= 'VALUES ('. $mod_info['modid'] .', ';
                $sql .= '\''. $user_tkid .'\', ';
                $sql .= '\''. $mod_info{'modref'} .'\')';

                $id = $client_db->db_insert($sql);

                if(isset($id) && is_int($id) && $id > 0){
                    $success = true;
                }
            }

            if($success == true){

                //user log values
                $log_user_id = $user_id;
                $log_comp_change = $log_text;

                //Log the addition or promotion to admin user
                $log_values = array();
                $log_values['user_id'] = $log_user_id;
                $log_values['user_change'] = $log_comp_change;

                $sql = 'INSERT INTO dtm_users_log (dtm_user_id,date_time,user_change) ';
                $sql .= 'VALUES ('.$log_values['user_id'].', ';
                $sql .= 'NOW(), ';
                $sql .= '\''.$log_values['user_change'].'\')';
                $id = $this->db_insert($sql);

                if(isset($id) && is_int($id) && $id > 0){
                    $result = array(
                        0 => "ok",
                        1 => "" . $company['user_name_ait'] . " has been successfully added.",
                        'object_id' => $id,
                    );
                    return $result;
                }
            }else{
                return array("error", "Testing: " . $sql);
            }
        }
        return array("error", "Testing: " . $sql);
    }

    public function deleteObject($var) {
        $user_id = $var['user_id'];



        $sql = 'SELECT * FROM dtm_companies ';
        $sql .= 'INNER JOIN dtm_users ';
        $sql .= 'ON dtm_users.user_company = dtm_companies.comp_id ';
        $sql .= 'AND dtm_users.user_id = ' . $user_id;
        $user = $this->mysql_fetch_one($sql);

        $sql = 'UPDATE dtm_users ';
        $sql .= 'SET user_active = 0 ';
        $sql .= 'WHERE user_id = ' . $user_id;
        $id = $this->db_update($sql);

        $client_db = new ASSIST_MODULE_HELPER('client', $user['comp_ait_cc']);

        if(isset($id) && is_int($id) & $id > 0){

            //Check that the connection between user and module exists in the database
            $sql = 'SELECT * FROM assist_menu_modules ';

            $sql .= 'INNER JOIN assist_'.$user['comp_ait_cc'].'_menu_modules_users ';
            $sql .= 'ON assist_'.$user['comp_ait_cc'].'_menu_modules_users.usrmodid = assist_menu_modules.modid ';

            $sql .= 'INNER JOIN assist_'.$user['comp_ait_cc'].'_timekeep ';
            $sql .= 'ON assist_'.$user['comp_ait_cc'].'_timekeep.tkid = assist_'.$user['comp_ait_cc'].'_menu_modules_users.usrtkid ';

            $sql .= 'WHERE assist_menu_modules.modlocation = \'DTM\' ';
            $sql .= 'AND assist_'.$user['comp_ait_cc'].'_timekeep.tkid = \''. $user['user_tkid'] .'\' ';

            $access_to_module = $client_db->mysql_fetch_one($sql);

            $success = false;
            if(isset($access_to_module) && is_array($access_to_module) && count($access_to_module) > 0){
                //Here if it exists then delete it

                $sql = 'DELETE FROM assist_'.$user['comp_ait_cc'].'_menu_modules_users ';
                $sql .= 'WHERE assist_'.$user['comp_ait_cc'].'_menu_modules_users.usrmodid = '. $access_to_module['modid'] .' ';
                $sql .= 'AND assist_'.$user['comp_ait_cc'].'_menu_modules_users.usrtkid = \'' . $user['user_tkid'] . '\' ';
                $id = $client_db->db_update($sql);

                if(isset($id) && is_int($id) && $id > 0){
                    $success = true;
                }
            }else{
                //Otherwise it doesn't exist anyway, so do nothing
                $success = true;
            }


//            //Also check for useraccess and get rid of it if it does exist
//            //Get the user access table to determine who has access to the setup functionality
//            $sql = 'SELECT * FROM dtm_useraccess ';
//            $sql .= 'WHERE ua_user_id = '. $user['user_id'] .' ';
//            $useraccess = $this->mysql_fetch_one($sql);
//
//            $success = false;
//            if(isset($useraccess) && is_array($useraccess) && count($useraccess) > 0){
//                $variable = array();
//                $variable['user_id'] = $user['user_id'];
//                $value = $this->revokeSetupAccess($variable);
//
//                if(isset($value) && is_array($value) && count($value) > 0){
//                    $success = true;
//                }
//            }else{
//                //Otherwise it doesn't exist anyway, so do nothing
//                $success = true;
//            }



            if($success == true){

                //user log values
                $log_user_id = $user_id;
                $log_comp_change = $user['user_name_ait'] . ' has been successfully deactivated in the DTM system, by: ' . $_SESSION['tkn'];

                //Log the addition or promotion to admin user
                $log_values = array();
                $log_values['user_id'] = $log_user_id;
                $log_values['user_change'] = $log_comp_change;

                $sql = 'INSERT INTO dtm_users_log (dtm_user_id,date_time,user_change) ';
                $sql .= 'VALUES ('.$log_values['user_id'].', ';
                $sql .= 'NOW(), ';
                $sql .= '\''.$log_values['user_change'].'\')';
                $id = $this->db_insert($sql);


                if(isset($id) && is_int($id) && $id > 0){
                    $result = array(
                        0 => "ok",
                        1 => "" . $user['user_name_ait'] . " has been successfully deleted.",
                        'object_id' => $user['user_id'],
                    );

                    return $result;
                }

            }else{
                return array("error", "Testing: " . $sql);
            }
        }else{
            return 'error';
        }
    }

    public function simpleEditObject($var) {
        $dtm_user_name = $var['dtm_user_name'];

        $sql = 'SELECT * FROM dtm_companies ';

        $sql .= 'INNER JOIN dtm_users ';
        $sql .= 'ON dtm_users.user_company = dtm_companies.comp_id ';

        $sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
        $sql .= 'AND dtm_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
        $user = $this->mysql_fetch_one($sql);

        $sql = 'UPDATE dtm_users ';
        $sql .= 'SET user_name_dtm = \'' . $dtm_user_name . '\' ';
        $sql .= 'WHERE user_id = ' . $user['user_id'];
        $id = $this->db_update($sql);

        if(isset($id) && is_int($id) & $id > 0){

            //user log values
            $log_user_id = $user['user_id'];
            $log_comp_change = $user['user_name_ait'] . ' has been successfully deactivated in the DTM system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['user_id'] = $log_user_id;
            $log_values['user_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_users_log (dtm_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);


            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $user['user_name_ait'] . " has been successfully edited.",
                    'object_id' => $user['user_id'],
                );

                return $result;
            }
        }else{
            return 'error';
        }
    }

    public function grantSetupAccess($var) {
        //get the current user details, so that we can identify the person who gave this user access to setup
        $sql = 'SELECT * FROM dtm_users ';
        $sql .= 'WHERE dtm_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
        $current_user_info = $this->mysql_fetch_one($sql);



        $access_insert = array();
        $access_insert['ua_user_id'] = $var['user_id'];
        $access_insert['ua_insertuser'] = $current_user_info['user_id'];

        $dtm_user_access = new DTM_USERACCESS();
        $return_value = $dtm_user_access->addObject($access_insert);

        if($return_value[0] == 'ok'){
            $sql = 'SELECT * FROM dtm_users ';
            $sql .= 'WHERE dtm_users.user_id = ' . $var['user_id'] . ' ';
            $affected_user_info = $this->mysql_fetch_one($sql);

            //user log values
            $log_user_id = $affected_user_info['user_id'];
            $log_comp_change = $affected_user_info['user_name_ait'] . ' has been successfully granted setup access in the DTM system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['user_id'] = $log_user_id;
            $log_values['user_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_users_log (dtm_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);


            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $affected_user_info['user_name_ait'] . " has been successfully granted setup access.",
                    'object_id' => $affected_user_info['user_id'],
                );
                return $result;
            }
        }else{
            return $return_value;
        }

    }

    public function revokeSetupAccess($var) {
        $dtm_user_access = new DTM_USERACCESS();

        $return_value = $dtm_user_access->removeObject($var);

        if($return_value[0] == 'ok'){
            $sql = 'SELECT * FROM dtm_users ';
            $sql .= 'WHERE dtm_users.user_id = ' . $var['user_id'] . ' ';
            $affected_user_info = $this->mysql_fetch_one($sql);

            //user log values
            $log_user_id = $affected_user_info['user_id'];
            $log_comp_change = $affected_user_info['user_name_ait'] . ' has had their setup access successfully revoked in the DTM system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['user_id'] = $log_user_id;
            $log_values['user_change'] = $log_comp_change;

            $sql = 'INSERT INTO dtm_users_log (dtm_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);


            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $affected_user_info['user_name_ait'] . " no longer has setup access.",
                    'object_id' => $affected_user_info['user_id'],
                );
                return $result;
            }
        }else{
            return $return_value;
        }
    }

}