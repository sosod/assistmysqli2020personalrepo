<?php

/**
 *
 *
 * Created on: 2 July 2016
 * Authors: Janet Currie
 *
 */
class DTM_REQUEST extends DTM
{


    private $table_name = "DTM_request";
    private $table_field = "hdr_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    private $system_log = array();
    private $user_changes;
    private $emails;


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "request";
    const TABLE_FLD = "hdr_";
    const REFTAG = "HDR";
    const LOG_TABLE = "object";

    public function __construct()
    {
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix() . $this->id_field;
        $this->status_field = $this->getTableFieldPrefix() . $this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField()
    {
        return self::TABLE_FLD;
    }

    public function getTableName()
    {
        return $this->getDBRef() . "_" . self::TABLE;
    }

    public function getRootTableName()
    {
        return $this->getDBRef();
    }

    public function getRefTag()
    {
        return self::REFTAG;
    }

    public function getMyObjectType()
    {
        return self::OBJECT_TYPE;
    }

    public function getMyLogTable()
    {
        return self::LOG_TABLE;
    }

    public function getApproverFieldName()
    {
        return $this->approver_field;
    }

    public function getTableFieldPrefix()
    {
        return $this->table_field;
    }


    //Add new DTM to the DTM db
    public function addObject($var)
    {
        unset($var['attachments']);

        //Get user id
        $hdr_user = new DTM_USERS();
        $add_user = $hdr_user->addObject($_SESSION['tid'], $_SESSION['tkn'], $_SESSION['cc']);

        if (is_array($add_user) && array_key_exists('user_id', $add_user)) {
            $uid = $add_user['user_id'];
        }

        //Put information into db
        $var[$this->getTableField() . 'add_userid'] = $uid;
        $var[$this->getTableField() . 'add_date'] = date("Y-m-d H:i:s");
        $var[$this->getTableField() . 'escalation_level'] = 1;
        $var[$this->getTableField() . 'add_status'] = 1;//new
        $var[$this->getTableField() . 'system_status'] = DTM::ACTIVE;
//		$var[$this->getTableField().'ref'] = self::REFTAG;

        $sql = "INSERT INTO " . $this->getTableName() . " SET " . $this->convertArrayToSQL($var);
        $id = $this->db_insert($sql);
        if ($id > 0) {

            //This Company or The One Above
            if (($add_user['hdrut_value'] == 'client_user') || ($add_user['hdrut_value'] == 'reseller_user')) {
                $cc = $add_user['hdc_cc'];

            } elseif ($add_user['hdrut_value'] == 'client_admin_user') {
                $cc = strtolower($_SESSION['bpa_details']['cmpcode']);

            } elseif ($add_user['hdrut_value'] == 'reseller_admin_user') {
                $cc = 'iassist';
            }

            //Get the Company Information
            $sql = 'SELECT * FROM DTM_companies ';

            $sql .= 'INNER JOIN DTM_company_settings ';
            $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

            $sql .= 'INNER JOIN DTM_admin_management ';
            $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

            $sql .= 'INNER JOIN DTM_request_settings ';
            $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

            $sql .= 'WHERE DTM_companies.hdc_cc = \'' . $cc . '\'';
            $company = $this->mysql_fetch_one($sql);

            //What this company's admin management
            if ($company['hdam_id'] == 1) {// No Admin
                if ($company['hdrs_id'] == 1) {//Auto Escalate
                    /*
                     * This is retty much only going to be experienced
                     * by client_users, because we're not giving resellers the option of not having
                     * admin users, so the $cc becomes the cc of the reseller and we run the same query again
                     * to find out if the reseller has one admin to assign this request to, or many that would
                     * have to claim the request from a pot
                     * */
                    if ($add_user['hdrut_value'] == 'client_user') {
                        $cc = strtolower($_SESSION['bpa_details']['cmpcode']);

                        //Get the Company Information
                        $sql = 'SELECT * FROM DTM_companies ';

                        $sql .= 'INNER JOIN DTM_company_settings ';
                        $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

                        $sql .= 'INNER JOIN DTM_admin_management ';
                        $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

                        $sql .= 'INNER JOIN DTM_request_settings ';
                        $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

                        $sql .= 'WHERE DTM_companies.hdc_cc = \'' . $cc . '\'';
                        $reseller = $this->mysql_fetch_one($sql);

                        if ($reseller['hdam_id'] == 2) {// One Admin
                            if ($reseller['hdrs_id'] == 2) {//To the one admin user
                                //Get admin of this company
                                $admin_user = $hdr_user->getAdminUser($cc);
                            }
                        } elseif ($reseller['hdam_id'] == 3) {// Multi Admins
                            if ($reseller['hdrs_id'] == 3) {// Into a Pot
                                //Find the admin users and send them a message
                                $sql = 'SELECT * FROM DTM_companies ';

                                // Company Type
                                $sql .= 'INNER JOIN DTM_company_types ';
                                $sql .= 'ON DTM_company_types.hdct_id = DTM_companies.hdc_type ';

                                // Company DTM settings
                                $sql .= 'INNER JOIN DTM_company_settings ';
                                $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

                                $sql .= 'INNER JOIN DTM_admin_management ';
                                $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

                                $sql .= 'INNER JOIN DTM_request_settings ';
                                $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

                                //Admin users
                                $sql .= 'INNER JOIN DTM_users ';
                                $sql .= 'ON DTM_users.user_company = DTM_companies.hdc_id ';

                                $sql .= 'INNER JOIN DTM_user_types ';
                                $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                $sql .= 'INNER JOIN DTM_user_admin_types ';
                                $sql .= 'ON DTM_user_admin_types.hduat_userid = DTM_users.user_id ';

                                $sql .= 'INNER JOIN DTM_admin_types ';
                                $sql .= 'ON DTM_admin_types.hdat_id = DTM_user_admin_types.hduat_admin_type_id ';

                                // Clauses
                                $sql .= 'WHERE DTM_companies.hdc_cc = \''.$cc . '\'';
                                $company_admins = $this->mysql_fetch_all($sql);
                            }
                        }
                    }
                }
            } elseif ($company['hdam_id'] == 2) {// One Admin
                if ($company['hdrs_id'] == 2) {//To the one admin user
                    //Get admin of this company
                    $admin_user = $hdr_user->getAdminUser($cc);
                }
            } elseif ($company['hdam_id'] == 3) {// Multi Admins

                if ($company['hdrs_id'] == 3) {// Into a Pot
                    //Find the admin users and send them a message
                    //Find the admin users and send them a message
                    $sql = 'SELECT * FROM DTM_companies ';

                    // Company Type
                    $sql .= 'INNER JOIN DTM_company_types ';
                    $sql .= 'ON DTM_company_types.hdct_id = DTM_companies.hdc_type ';

                    // Company DTM settings
                    $sql .= 'INNER JOIN DTM_company_settings ';
                    $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

                    $sql .= 'INNER JOIN DTM_admin_management ';
                    $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

                    $sql .= 'INNER JOIN DTM_request_settings ';
                    $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

                    //Admin users
                    $sql .= 'INNER JOIN DTM_users ';
                    $sql .= 'ON DTM_users.user_company = DTM_companies.hdc_id ';

                    $sql .= 'INNER JOIN DTM_user_types ';
                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                    $sql .= 'INNER JOIN DTM_user_admin_types ';
                    $sql .= 'ON DTM_user_admin_types.hduat_userid = DTM_users.user_id ';

                    $sql .= 'INNER JOIN DTM_admin_types ';
                    $sql .= 'ON DTM_admin_types.hdat_id = DTM_user_admin_types.hduat_admin_type_id ';

                    // Clauses
                    $sql .= 'WHERE DTM_companies.hdc_cc = \''.$cc . '\'';
                    $company_admins = $this->mysql_fetch_all($sql);
                } elseif ($company['hdrs_id'] == 4) {// To the primary admin
                    /*
                     * saving this option for later,
                     * but the general idea being that the request would be assigned to
                     * the primary admin user, and said user would then transfer/assign
                     * this request to the admin user/respondent who's going to actually
                     * action it (Lol, see what I did there)
                     *
                     * */
                } elseif ($company['hdrs_id'] == 5) {// To admin user on duty
                    /*
                     * Another option to be coded later,
                     * this option is going to assign the request to the admin user on duty.
                     * Which all good and well unil we have more than one admin on duty, in which case the current
                     * idea is that we put the request in the pot and then notify the admins on duty*/
                }

            }

            // 3. Assign the DTM request to them in the db
            if (is_array($admin_user) && array_key_exists('user_id', $admin_user)) {
                $admin_uid = $admin_user['user_id'];
                $user_role = 1;// Assigned
                $user_status = 1;// New
                $id = $this->DTMRequestAssignment($id, $admin_uid, $user_role, $user_status);
            }


            if ($id > 0) {
                $success = true;

                // Send confirmation email to the add_user
                $message = 'You have successfully logged a DTM Request to the admin user above you';
                $this->emails[0]['from'] = 'AiT DTM';
                $this->emails[0]['to'] = 'add_user email address';
                $this->emails[0]['subject'] = 'You Have Logged a DTM Request';
                $this->emails[0]['message'] = $message;

                // Send notification email to the assigned user
                $message = 'New DTM Request';
                $this->emails[1]['from'] = 'AiT DTM';
                $this->emails[1]['to'] = 'admin_user email address';
                $this->emails[1]['subject'] = 'New DTM Request';
                $this->emails[1]['message'] = $message;
            }elseif(count($company_admins) > 0){
                $success = true;

                // Send confirmation email to the add_user
                $message = 'You have successfully logged a DTM Request, which is currently unassigned to anyone';
                $this->emails[0]['from'] = 'AiT DTM';
                $this->emails[0]['to'] = 'add_user email address';
                $this->emails[0]['subject'] = 'You Have Logged a DTM Request';
                $this->emails[0]['message'] = $message;

                // Send notification email to the admins of this company, notifying them of the new request
                $message = 'New DTM Request';
                $this->emails[1]['from'] = 'AiT DTM';
                $this->emails[1]['to'] = 'admin_user email address';
                $this->emails[1]['subject'] = 'New DTM Request';
                $this->emails[1]['message'] = $message;
            } else {
                $success = false;
            }
        } else {
            $success = false;
        }


        //Return the relevant message upon success or failure
        if ($success) {
            $result = array(
                0 => "ok",
                1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $id . " has been successfully created.",
                'object_id' => $id,
            );
            return $result;
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }
    }

    public function DTMRequestAssignment($hdrid, $hdru_userid, $user_role, $status)
    {

        // 3. Enter the hdrid, the user_id, the role_id, and the status_id into the hdr/user intermediary table
        $sql = 'INSERT INTO DTM_request_users ';
        $sql .= '(hdru_hdrid, hdru_userid, hdru_role, hdru_status)';
        $sql .= 'VALUES (' . $hdrid . ', ' . $hdru_userid . ', ' . $user_role . ', ' . $status . ')';
        $id = $this->db_insert($sql);

        // 4. Return true for success, or false for failure
        if ($id > 0) {
            return $id;
        } else {
            return $id;
        }
    }

    public function DTMRequestEscalation($hdr_id)
    {
        //Logs and messages
        $emails = array();


        // Escalate current DTM request to the user that's one level above this level
        $hdr_user = new DTM_USERS();

        // 1. Get the DTM request and all the information about it from other tables
        $sql = 'SELECT * FROM DTM_request ';
        $sql .= 'INNER JOIN DTM_users ';
        $sql .= 'ON DTM_users.user_id = DTM_request.hdr_add_userid ';

        $sql .= 'INNER JOIN DTM_companies ';
        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';
        $sql .= 'INNER JOIN DTM_user_types ';
        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

        $sql .= 'WHERE DTM_request.hdr_id = ' . $hdr_id;
        $request = $this->mysql_fetch_one($sql);

        //Get information on the user assigned to the request, which will help us in identifying where to escalate to
        $sql = 'SELECT * FROM DTM_request ';
        $sql .= 'INNER JOIN DTM_request_users ';
        $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

        $sql .= 'INNER JOIN DTM_list_user_status ';
        $sql .= 'ON DTM_list_user_status.hlus_id = DTM_request_users.hdru_status ';

        $sql .= 'INNER JOIN DTM_users ';
        $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

        $sql .= 'INNER JOIN DTM_companies ';
        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';


        $sql .= 'INNER JOIN DTM_user_types ';
        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';


        $sql .= 'INNER JOIN DTM_list_user_roles ';
        $sql .= 'ON DTM_list_user_roles.hlur_id = DTM_request_users.hdru_role ';

        $sql .= 'WHERE DTM_request.hdr_id = ' . $hdr_id . ' ';
        $sql .= 'AND DTM_request_users.hdru_role = 1';

        $assigned = $this->mysql_fetch_one($sql);

        if (count($assigned) > 0) {
            /*
             * Current assigned user, you know, the person who's actually escalating this request,
             * becomes an interested party,
             * with a status of escalated
             * */
            $sql = 'UPDATE DTM_request_users ';
            $sql .= 'SET hdru_status = 9, hdru_role = 2 ';
            $sql .= 'WHERE  hdru_hdrid = ' . $hdr_id . ' ';
            $sql .= 'AND  hdru_userid = ' . $assigned['user_id'];
            $id = $this->db_update($sql);

            if ($id > 0) {
                $sql = 'SELECT * FROM DTM_list_user_status ';
                $sql .= 'WHERE hlus_id = 1';
                $new_status = $this->mysql_fetch_one($sql);
                $this->system_log[] = 'hdru_status has changed from '.$assigned['hlus_value'] .' to ' . $new_status['hlus_value'] . ' & hdru_role has changed from assigned_user to interested_party in DTM_request_users for the user ' . $assigned['user_name'];

                /*
                 * The DTM Request status either becomes or stays as escalated,
                 * and the escalation level increases by one
                 * */
                if ($request['hdr_escalation_level'] == 1) {
                    $new_esc_lvl = 2;
                } elseif ($request['hdr_escalation_level'] == 2) {
                    $new_esc_lvl = 3;
                }
                /*
                 * Escalation level is now 2, and the status is now 9 (Escalated)
                */
                $sql = 'UPDATE DTM_request ';
                $sql .= 'SET hdr_add_status = 9, hdr_escalation_level = ' . $new_esc_lvl . ' ';
                $sql .= 'WHERE  hdr_id = ' . $hdr_id . ' ';
                $id = $this->db_update($sql);

                if ($id > 0) {
                    //Success

                    $this->system_log[] = 'hdr_escalation_level has changed from '.$request['hdr_escalation_level'] .' to ' . $new_esc_lvl . ' in DTM_request';

                    // Check if the current assigned user is a client_admin_user or a reseller_admin_user
                    if ($assigned['hdrut_value'] == 'client_admin_user') {
                        $cc = strtolower($_SESSION['bpa_details']['cmpcode']);

                    } elseif ($assigned['hdrut_value'] == 'reseller_admin_user') {
                        $cc = 'iassist';

                    }

                    //Get the Company Information
                    $sql = 'SELECT * FROM DTM_companies ';

                    $sql .= 'INNER JOIN DTM_company_settings ';
                    $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

                    $sql .= 'INNER JOIN DTM_admin_management ';
                    $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

                    $sql .= 'INNER JOIN DTM_request_settings ';
                    $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

                    $sql .= 'WHERE DTM_companies.hdc_cc = \'' . $cc . '\'';
                    $company = $this->mysql_fetch_one($sql);

                    //What this company's admin management
                    if ($company['hdam_id'] == 1) {// No Admin
                        if ($company['hdrs_id'] == 1) {//Auto Escalate
                            /*
                             * Remember when I said that this would only be experienced by client_users?
                             *
                             * Nothings changed so far so this here will never happen
                             *
                             * But, you know... You never know, so I'll hold onto to this section just in case*/

                        }
                    } elseif ($company['hdam_id'] == 2) {// One Admin
                        if ($company['hdrs_id'] == 2) {//To the one admin user
                            //Get admin of this company
                            $admin_user = $hdr_user->getAdminUser($cc);
                        }
                    } elseif ($company['hdam_id'] == 3) {// Multi Admins

                        if ($company['hdrs_id'] == 3) {// Into a Pot
                            //Find the admin users and send them a message
                            $sql = 'SELECT * FROM DTM_companies ';

                            // Company Type
                            $sql .= 'INNER JOIN DTM_company_types ';
                            $sql .= 'ON DTM_company_types.hdct_id = DTM_companies.hdc_type ';

                            // Company DTM settings
                            $sql .= 'INNER JOIN DTM_company_settings ';
                            $sql .= 'ON DTM_company_settings.hcs_company_id = DTM_companies.hdc_id ';

                            $sql .= 'INNER JOIN DTM_admin_management ';
                            $sql .= 'ON DTM_admin_management.hdam_id = DTM_company_settings.hcs_admin_man_id ';

                            $sql .= 'INNER JOIN DTM_request_settings ';
                            $sql .= 'ON DTM_request_settings.hdrs_id = DTM_company_settings.hcs_settings_id ';

                            //Admin users
                            $sql .= 'INNER JOIN DTM_users ';
                            $sql .= 'ON DTM_users.user_company = DTM_companies.hdc_id ';

                            $sql .= 'INNER JOIN DTM_user_types ';
                            $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                            $sql .= 'INNER JOIN DTM_user_admin_types ';
                            $sql .= 'ON DTM_user_admin_types.hduat_userid = DTM_users.user_id ';

                            $sql .= 'INNER JOIN DTM_admin_types ';
                            $sql .= 'ON DTM_admin_types.hdat_id = DTM_user_admin_types.hduat_admin_type_id ';

                            // Clauses
                            $sql .= 'WHERE DTM_companies.hdc_cc = \''.$cc . '\'';
                            $company_admins = $this->mysql_fetch_all($sql);
                        }
                    }

                    // 3. Assign the DTM request to them in the db
                    if (is_array($admin_user) && array_key_exists('user_id', $admin_user)) {
                        $admin_uid = $admin_user['user_id'];
                        $user_role = 1;// Assigned
                        $user_status = 1;// New
                        $id = $this->DTMRequestAssignment($hdr_id, $admin_uid, $user_role, $user_status);
                    }

                    if ($id > 0) {
                        /*
                         * Put this escalation in the log
                         * */
                        $this->system_log[] = 'HDR'.$hdr_id.' has now been assigned to ' . $admin_user['user_name'];
                        $this->user_changes = $assigned['user_name'].' (former assigned_user) has escalated HDR'. $hdr_id .' to ' . $admin_user['user_name'] . ' (current assigned_user)';


                        /*
                         * Send email to the user below to let them know that this request has now been escalated upward
                         * */
                        $message = $this->user_changes;
                        $this->emails[0]['from'] = 'AiT DTM';
                        $this->emails[0]['to'] = '{user_below} email address';
                        $this->emails[0]['subject'] = 'DTM Request has been Escalated';
                        $this->emails[0]['message'] = $message;


                        /*
                         * Send an email to the current user, who escalated this request and let him/her know that
                         * the request has been successfully escalated upward
                         * */
                        $message = 'You have successfully escalated '.$hdr_id.' to ' . $admin_user['user_name'];
                        $this->emails[1]['from'] = 'AiT DTM';
                        $this->emails[1]['to'] = 'current_admin_user email address';
                        $this->emails[1]['subject'] = 'DTM Request Successfully Escalated';
                        $this->emails[1]['message'] = $message;

                        /*
                         * Let the user above the current one, the new assigned user, know that this request has been logged to him
                         * */
                        $message = 'Hi, '.$admin_user['user_name'].'. '. $assigned['user_name'] . ' has escalated HDR'. $hdr_id .' to you';
                        $this->emails[2]['from'] = 'AiT DTM';
                        $this->emails[2]['to'] = 'admin_user_email';
                        $this->emails[2]['subject'] = '{Admin user below} has escalated HDR# to you';
                        $this->emails[2]['message'] = $message;

                        return true;
                    }elseif(count($company_admins) > 0){
                        /*
                         * Put this escalation in the log
                         * */
                        $this->system_log[] = 'HDR'.$hdr_id.' has now been assigned to ' . $admin_user['user_name'];
                        $this->user_changes = $assigned['user_name'].' (former assigned_user) has escalated HDR'. $hdr_id .' upward';


                        /*
                         * Send email to the user below to let them know that this request has now been escalated upward
                         * */
                        $message = 'HDR'. $hdr_id .' has been escalated upward';
                        $this->emails[0]['from'] = 'AiT DTM';
                        $this->emails[0]['to'] = '{user_below} email address';
                        $this->emails[0]['subject'] = 'DTM Request has been Escalated';
                        $this->emails[0]['message'] = $message;


                        /*
                         * Send an email to the current user, who escalated this request and let him/her know that
                         * the request has been successfully escalated upward
                         * */
                        $message = 'You have successfully escalated HDR'. $hdr_id .' to unassigned admin user';
                        $this->emails[1]['from'] = 'AiT DTM';
                        $this->emails[1]['to'] = 'admin_user address';
                        $this->emails[1]['subject'] = 'DTM Request Successfully Escalated';
                        $this->emails[1]['message'] = $message;

                        /*
                         * Let the users above this level know that there's an unassigned HDR ready to be claimed
                         * */
                        $message = $assigned['user_name'] . ' has escalated HDR'. $hdr_id .' to up to this level';
                        $this->emails[2]['from'] = 'AiT DTM';
                        $this->emails[2]['to'] = 'admin_user_email';
                        $this->emails[2]['subject'] = '{Admin user below} has escalated HDR# to you';
                        $this->emails[2]['message'] = $message;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            } else {
                return false;
            }

        } else {
            // There is no assigned user, which is weird, because only the assigned can initiate this function
            return false;
        }
    }


    public function updateObject($var)
    {
        unset($var['attachments']);


//        return $var;

        $description = false;
        if($var['hdrup_description'] != ''){
            $description = true;
        }

        $update_values = array();


        //Put information into db
        $var[$this->getTableField() . 'add_date'] = date("Y-m-d H:i:s");
        $var[$this->getTableField() . 'escalation_level'] = 1;
        $var[$this->getTableField() . 'add_status'] = 1;

        //Logs and messages
        $emails = array();

        //get the current user
        $sql = 'SELECT * FROM DTM_users ';
        $sql .= 'INNER JOIN DTM_companies ';
        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';
        $sql .= 'INNER JOIN DTM_user_types ';
        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';
        $sql .= 'WHERE DTM_users.user_id = ' . $var['user_id'];
        $user = $this->mysql_fetch_one($sql);

        /*
         * Get the actual DTM request and
         * all of the information about the user who launched it
         * */
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' ';

        $sql .= 'INNER JOIN DTM_users ';
        $sql .= 'ON DTM_users.user_id = ' . $this->getTableName() . '.hdr_add_userid ';

        $sql .= 'INNER JOIN DTM_companies ';
        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

        $sql .= 'INNER JOIN DTM_user_types ';
        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

        $sql .= 'WHERE  hdr_id = ' . $var['hdrid'];
        $request = $this->mysql_fetch_one($sql);

        if ($var['hdr_action'] > 0 && $var['hdr_action'] != 9 && $var['hdr_action'] < 10) {

            if ($request['hdr_escalation_level'] == 1) {//Change both the hdr table and the hdr/user intermediary
                /*
                 * Change both the hdr table and the hdr/user intermediary table,
                 * because there are currently only two users at play here, which are
                 * the add_user = (client_user || client_admin_user || reseller_user || reseller_admin_user)
                 * and the assigned user = (client_admin_user || reseller_admin_user || iassist_admin_user)
                 * */
                $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                $sql .= ' WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                $sql .= 'AND hdr_add_userid = ' . $request['hdr_add_userid'] . ' ';
                $id = $this->db_update($sql);

                if ($id > 0) {
                    $sql = 'SELECT * FROM DTM_list_user_status ';
                    $sql .= 'WHERE hlus_id = ' . $request['hdr_add_status'];
                    $old_status = $this->mysql_fetch_one($sql);

                    $sql = 'SELECT * FROM DTM_list_user_status ';
                    $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                    $new_status = $this->mysql_fetch_one($sql);

                    $this->system_log[] = 'hdr_add_status has changed from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'] . ' in DTM_request';
                    /*
                     * Now that we've changed the status at add_user level,
                     * we're going to change the assigned_user's status to match the the add_user status in the request table
                     */
                    $sql = 'SELECT * FROM DTM_request ';

                    $sql .= 'INNER JOIN DTM_request_users ';
                    $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                    $sql .= 'INNER JOIN DTM_users ';
                    $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                    $sql .= 'WHERE DTM_request.hdr_id = ' . $var['hdrid'] . ' ';
                    $sql .= 'AND DTM_request_users.hdru_role = 1';// assigned_user

                    $assigned_user = $this->mysql_fetch_one($sql);


                    $sql = 'UPDATE DTM_request_users ';
                    $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                    $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                    $sql .= 'AND  hdru_userid = ' . $assigned_user['user_id'];
                    $id = $this->db_update($sql);

                    if ($id > 0) {
                        $success = true;

                        $this->system_log[] = 'hdru_status has changed from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'] . ' in DTM_request_users';

                        /*
                         * Now, let's identify who made the update so that we know who to notify about the change,
                         * And help us to structure the log
                         * */
                        if ($user['user_id'] == $request['hdr_add_userid']) {
                            $this->user_changes = $request['user_name'].' (add_user) has changed the status from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'];

                            if ($var['hdr_action'] == 4) {
                                $this->user_changes = $request['user_name'].' has rejected the solution for HDR' . $var['hdrid'];
                            }elseif($var['hdr_action'] == 7){
                                $this->user_changes = $request['user_name'].' has confirmed resolution for HDR' . $var['hdrid'];
                            }elseif($var['hdr_action'] == 6){
                                $this->user_changes = $request['user_name'].' has cancelled HDR' . $var['hdrid'];
                            }

                            // Notify the assigned_user of the change
                            $message = $this->user_changes;
                            if($description){
                                $message .= ' and has added the following message: "';
                                $message .= $description . '"';
                            }

                            $this->getAnEmail($user['user_tkid']);

                            $this->emails[0]['from'] = 'AiT DTM';
                            $this->emails[0]['to'] = 'addmin_user email address';
                            $this->emails[0]['subject'] = 'DTM Request Changes';
                            $this->emails[0]['message'] = $message;

                        } elseif ($user['user_id'] == $assigned_user['user_id']) {
                            $this->user_changes = $assigned_user['user_name'].' (assigned_user) has changed the status from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'];

                            // Notify the add_user of the change
                            $message = $this->user_changes;
                            if($description){
                                $message .= ' and has added the following message: "';
                                $message .= $description . '"';
                            }
                            $this->emails[0]['from'] = 'AiT DTM';
                            $this->emails[0]['to'] = 'add_user email address';
                            $this->emails[0]['subject'] = 'DTM Request Changes';
                            $this->emails[0]['message'] = $message;
                        }
                    } else {
                        $success = false;
                    }
                } else {
                    $success = false;
                }
            }elseif ($var['hdr_action'] == 5) {
                /*
                     * Assign this DTM request to the user that's one level below this admin user
                     * */
                $sql = 'SELECT * FROM DTM_request ';
                $sql .= 'INNER JOIN DTM_request_users ';
                $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                $sql .= 'INNER JOIN DTM_users ';
                $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                $sql .= 'INNER JOIN DTM_companies ';
                $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                $sql .= 'INNER JOIN DTM_user_types ';
                $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                $sql .= 'INNER JOIN DTM_list_user_roles ';
                $sql .= 'ON DTM_list_user_roles.hlur_id = DTM_request_users.hdru_role ';

                $sql .= 'WHERE DTM_request.hdr_id = ' . $var['hdrid'] . ' ';


                if ($user['hdrut_value'] == 'iassist_admin_user') {
                    $sql .= 'AND  DTM_user_types.hdrut_value = \'reseller_admin_user\'';
                } elseif ($user['hdrut_value'] == 'reseller_admin_user') {
                    $sql .= 'AND  DTM_user_types.hdrut_value = \'client_admin_user\'';
                }

                $admin_user = $this->mysql_fetch_one($sql);


                // Admin_user_below exists in hdru table
                if (count($admin_user) > 0 && $user['hdrut_value'] != 'client_admin_user') {
                    $hdru = true;

                    $hdru_role = 2;

                } else {
                    $sql = 'SELECT * FROM DTM_request ';

                    $sql .= 'INNER JOIN DTM_users ';
                    $sql .= 'ON DTM_users.user_id = DTM_request.hdr_add_userid ';

                    $sql .= 'INNER JOIN DTM_companies ';
                    $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                    $sql .= 'INNER JOIN DTM_user_types ';
                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                    $sql .= 'WHERE DTM_request.hdr_id = ' . $var['hdrid'] . ' ';

                    if ($user['hdrut_value'] == 'iassist_admin_user') {
                        $sql .= 'AND  DTM_user_types.hdrut_value = \'reseller_admin_user\'';
                    } elseif ($user['hdrut_value'] == 'reseller_admin_user') {
                        $sql .= 'AND  (DTM_user_types.hdrut_value = \'client_admin_user\' || DTM_user_types.hdrut_value = \'reseller_user\')';
                    } elseif ($user['hdrut_value'] == 'client_admin_user') {
                        $sql .= 'AND  DTM_user_types.hdrut_value = \'client_user\'';
                    }

                    $add_user_below = $this->mysql_fetch_one($sql);

                    $hdru = false;

                    $hdru_role = 1;

                    //If this does yield a result, then we're dealing with one of those situations where the adduser is more than one level down
                }

                //Update the reseller admin user's or iassist admin user status to resolved, and set this user to an interested party
                /*
                 * Update the current admin user's status to resolved,
                 * and set this user's role to interested_party
                 * */
                $sql = 'UPDATE DTM_request_users ';
                $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ', hdru_role = ' . $hdru_role . ' ';
                $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                $id = $this->db_update($sql);

                $sql = 'SELECT * FROM DTM_list_user_status ';
                $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                $new_status = $this->mysql_fetch_one($sql);

                if ($id > 0) {
                    if ($hdru) {
                        $this->system_log[] = $user['user_id'] . ' has become an interested_party, and the hdru_status is ' . $new_status['hlus_value'] . ' in DTM_request_users';
                        //Update reseller_admin_user, or client_admin_user to assigned user
                        $sql = 'UPDATE DTM_request_users ';
                        $sql .= 'SET hdru_role = 1 ';
                        $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                        $sql .= 'AND  hdru_status = 9 ';
                        $sql .= 'AND  hdru_role = 2 ';
                        $sql .= 'AND  hdru_userid = ' . $admin_user['user_id'] . '';
                        $id = $this->db_update($sql);



                        if ($id > 0) {
                            $success = true;
                            $this->system_log[] = $user['user_id'] . ' has become an assigned_user for HDR' . $var['hdrid'];
                        } else {
                            //Failed
                            $success = false;
                        }
                    } else {
                        $this->system_log[] = 'hdru_status is ' . $new_status['hlus_value'] . ' in DTM_request_users ' . $user['user_name'];

                        $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                        $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                        $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                        $id = $this->db_update($sql);

                        if ($id > 0) {
                            $success = true;
                            $this->system_log[] = 'hdr_add_status is ' . $new_status['hlus_value'] . ' in DTM_request for add_user';
                        } else {
                            // FAiled
                            $success = false;
                        }
                    }
                    $this->user_changes = $user['user_name'] . ' has Marked HDR'. $var['hdrid'] . ' as Resolved';

                    if (count($admin_user) > 0) {
                        //Send email to admin user
                    } else {
                        //Send email to add_user
                    }
                } else {
                    // Failure
                    $success = false;
                }
            } elseif($var['hdr_action'] == 4){
                $sql = 'SELECT * FROM DTM_request ';
                $sql .= 'INNER JOIN DTM_request_users ';
                $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                $sql .= 'INNER JOIN DTM_users ';
                $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                $sql .= 'INNER JOIN DTM_companies ';
                $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                $sql .= 'INNER JOIN DTM_user_types ';
                $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                $sql .= 'INNER JOIN DTM_list_user_roles ';
                $sql .= 'ON DTM_list_user_roles.hlur_id = DTM_request_users.hdru_role ';

                $sql .= 'WHERE DTM_request_users.hdru_hdrid = ' . $var['hdrid'] . ' ';
                $sql .= 'AND DTM_request_users.hdru_status = 10';
                $reply_check = $this->mysql_fetch_one($sql);

                if(is_array($reply_check) && count($reply_check) > 0){// This is a reply to a request for more information
                    /*
                * Is the current user the add_user*/
                    if($user['user_id'] == $request['hdr_add_userid']){
                        if($request['hdr_escalation_level'] == 1){// Reply
                            //Change the HDR status to 4
                            $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                            $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                            $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                            $id = $this->db_update($sql);

                            if($id > 0){
                                $this->system_log[] = 'hdr_add_status is ' . $var['hdr_action'] . ' in DTM_request';
                                //Change the hdru status to 4
                                $sql = 'UPDATE DTM_request_users ';
                                $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                                $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'];
                                $id = $this->db_update($sql);

                                if($id > 0){
                                    $this->system_log[] = 'hdru_status is now ' . $var['hdr_action'] . ' in DTM_request_users';
                                    $this->user_changes = $user['user_name'] . ' has replied to the request for more information for HDR'. $var['hdrid'];
                                    //Success
                                    $success = true;
                                }else{
                                    // Failure
                                    $success = false;
                                }
                            }else{
                                // Failure
                                $success = false;
                            }
                        }else{// Reply
                            $update_values[$this->getTableField() . 'add_status'] = 9;
                            $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                            $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                            $id = $this->db_update($sql);

                            if($id > 0){
                                $sql = 'SELECT * FROM DTM_list_user_status ';
                                $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                                $new_status = $this->mysql_fetch_one($sql);
                                $this->system_log[] = 'hdr_add_status is ' . $new_status['hlus_value'] . ' in DTM_request';


                                //Change the hdru status to 4
                                $sql = 'UPDATE DTM_request_users ';
                                $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                                $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                $sql .= 'AND hdru_userid = ' . $reply_check['user_id'];
                                $id = $this->db_update($sql);

                                if($id > 0){
                                    $this->system_log[] = 'hdru_status is ' . $new_status['hlus_value'] . ' in DTM_request_users';
                                    $this->user_changes = $user['user_name'] . ' has replied to the request for more information for HDR'. $var['hdrid'];

                                    //Send an email of the reply to the admin user that asked for information HERE

                                    //Success
                                    $success = true;
                                }else{
                                    //Failure
                                    $success = false;
                                }
                            }else{
                                //Failure
                                $success = false;
                            }
                        }
                    }elseif($user['user_id'] == $reply_check['user_id']){//Reclaim
                        if($request['hdr_escalation_level'] == 1 || $request['hdr_add_status'] == 3){
                            //Change the HDR status to 4
                            if($request['hdr_escalation_level'] == 1){
                                $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                            }else{
                                $update_values[$this->getTableField() . 'add_status'] = 9;
                            }
                            $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                            $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                            $id = $this->db_update($sql);

                            $this->system_log[] = 'hdr_add_status is ' .$update_values[$this->getTableField() . 'add_status'] . ' in DTM_request';

                            //Get add user and inform them via email that the assigned admin_user has reclaimed the HDR in question
                        }else{
                            //Change the hdru status to 9
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = 9 ';
                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                            $sql .= 'AND hdru_status = 3';
                            $id = $this->db_update($sql);

                            $this->system_log[] = 'hdru_status is back to escalated in DTM_request_users';

                            //Get admin_user and inform them via email that the assigned admin_user has reclaimed the HDR in question
                        }

                        if($id > 0 ){
                            //Change the hdru status to 4
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                            $sql .= 'AND hdru_userid = ' . $reply_check['user_id'];
                            $id = $this->db_update($sql);

                            if($id > 0){
                                $this->system_log[] = 'hdru_status is now In Progress in DTM_request_users';
                                $this->user_changes = $user['user_name'] . ' has reclaimed HDR'. $var['hdrid'];
                                //Success
                                $success = true;
                            }else{
                                //Failure
                                $success = false;
                            }
                        }else{
                            //Failure
                            $success = false;
                        }

                    }else{
                        // Change current_user hdru_status to 9
                        $sql = 'UPDATE DTM_request_users ';
                        $sql .= 'SET hdru_status = 9 ';
                        $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                        $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                        $id = $this->db_update($sql);

                        if($id > 0){
                            $this->system_log[] = 'hdru_status is now Escalated in DTM_request_users';

                            //Assign this request to the higher level user, and update their status to 4 (In progress)
                            //Change the hdru status to 4
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                            $sql .= 'AND hdru_userid = ' . $reply_check['user_id'];
                            $id = $this->db_update($sql);


                            if($id > 0){
                                $this->system_log[] = 'hdru_status is now In Progress in DTM_request_users';
                                $this->user_changes = $user['user_name'] . ' has replied to the request for more information for HDR'. $var['hdrid'];

                                //Send an email to the assigned user and let know that the HDR in question is now theirs again

                                //Success
                                $success = true;
                            }else{
                                //Failure
                                $success = false;
                            }
                        }else{
                            //Failure
                            $success = false;
                        }

                    }
                }else{//Either a reject or plain marking as in progress
                    /*
                     * Is the current user the add_user
                     * */
                    if($user['user_id'] == $request['hdr_add_userid']){
                        if($request['hdr_escalation_level'] == 1){// Rejection
                            //Change the HDR status to 4
                            $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                            $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                            $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                            $affected = $this->db_update($sql);

                            //Change the hdru status to 4
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'];
                            $affected = $this->db_update($sql);
                        }else{// Rejection
                            $update_values[$this->getTableField() . 'add_status'] = 9;
                            // Change hdr status back to 9
                            $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                            $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                            $id = $this->db_update($sql);

                            if($id > 0){
                                $sql = 'SELECT * FROM DTM_list_user_status ';
                                $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                                $new_status = $this->mysql_fetch_one($sql);
                                $this->system_log[] = 'hdr_add_status is ' . $new_status['hlus_value'] . ' in DTM_request';

                                // Find the users attached to this request in the hdru table
                                if(($request['hdrut_value'] == 'client_user') || ($request['hdrut_value'] == 'client_admin_user') || ($request['hdrut_value'] == 'reseller_user')){//is client_user, client_admin_user, reseller_user

                                    //Assign to the reseller_admin_user
                                    $sql = 'SELECT * FROM DTM_request ';

                                    $sql .= 'INNER JOIN DTM_request_users ';
                                    $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                    $sql .= 'INNER JOIN DTM_users ';
                                    $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                    $sql .= 'INNER JOIN DTM_companies ';
                                    $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                    $sql .= 'INNER JOIN DTM_user_types ';
                                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                    $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                                    $sql .= 'AND  DTM_user_types.hdrut_value = \'reseller_admin_user\'';
                                    $reseller = $this->mysql_fetch_one($sql);

                                    if(count($reseller) > 0){// Is there a reseller involved in this request


                                        //Is there an iassist_admin above this one attached to this request
                                        $sql = 'SELECT * FROM DTM_request ';

                                        $sql .= 'INNER JOIN DTM_request_users ';
                                        $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                        $sql .= 'INNER JOIN DTM_users ';
                                        $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                        $sql .= 'INNER JOIN DTM_companies ';
                                        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                        $sql .= 'INNER JOIN DTM_user_types ';
                                        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                        $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';

                                        $sql .= 'AND  DTM_user_types.hdrut_value = \'iassist_admin_user\'';

                                        $iassist_admin = $this->mysql_fetch_one($sql);

                                        if(count($iassist_admin) > 0){
                                            $hdru_status = 9;
                                        }else{
                                            $hdru_status = $var['hdr_action'];
                                        }

                                        //Check for client_admin_user
                                        $sql = 'SELECT * FROM DTM_request ';

                                        $sql .= 'INNER JOIN DTM_request_users ';
                                        $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                        $sql .= 'INNER JOIN DTM_users ';
                                        $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                        $sql .= 'INNER JOIN DTM_companies ';
                                        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                        $sql .= 'INNER JOIN DTM_user_types ';
                                        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                        $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                                        $sql .= 'AND  DTM_user_types.hdrut_value = \'client_admin_user\'';
                                        $client_admin = $this->mysql_fetch_one($sql);

                                        if(count($client_admin) > 0){
                                            //Then return the status back to escalated, and make them an interested party once more
                                            $sql = 'UPDATE DTM_request_users ';
                                            $sql .= 'SET hdru_status = 9, hdru_role = 2 ';
                                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                            $sql .= 'AND hdru_userid = ' . $client_admin['user_id'];
                                            $id = $this->db_update($sql);

                                            if($id > 0){
                                                $this->system_log[] = 'hdru_status has become Escalated in DTM_request_users for ' . $client_admin['user_name'] . ' and the user role is now the assigned user again';
                                                $this->user_changes = $user['user_name'] . ' has rejected the resolution recieved from ' . $client_admin['user_name'];
                                            }
                                        }

                                        //Assign this request to the higher level user, and update their status
                                        $sql = 'UPDATE DTM_request_users ';
                                        $sql .= 'SET hdru_status = ' . $hdru_status . ', hdru_role = 1 ';
                                        $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                        $sql .= 'AND hdru_userid = ' . $reseller['user_id'];
                                        $id = $this->db_update($sql);


                                        if($id > 0){
                                            $this->system_log[] = 'hdru_status has become In Progress in DTM_request_users for ' . $reseller['user_name'] . ' and the user role is now the assigned user again';
                                            $this->user_changes = $user['user_name'] . ' has rejected the resolution recieved from ' . (count($client_admin) > 0? $client_admin['user_name'] : $reseller['user_name'] );

                                            //Success
                                            $success = true;
                                        }else{
                                            $success = false;
                                        }

                                    }else{
                                        //Assign to the client_admin_user
                                        $sql = 'SELECT * FROM DTM_request ';

                                        $sql .= 'INNER JOIN DTM_request_users ';
                                        $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                        $sql .= 'INNER JOIN DTM_users ';
                                        $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                        $sql .= 'INNER JOIN DTM_companies ';
                                        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                        $sql .= 'INNER JOIN DTM_user_types ';
                                        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                        $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                                        $sql .= 'AND  DTM_user_types.hdrut_value = \'client_admin_user\'';
                                        $client = $this->mysql_fetch_one($sql);

                                        if(count($client) > 0){
                                            //Change the hdru status to 4
                                            $sql = 'UPDATE DTM_request_users ';
                                            $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                                            $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                            $sql .= 'AND hdru_userid = ' . $client['user_id'];
                                            $id = $this->db_update($sql);
                                        }
                                    }
                                }elseif($request['hdrut_value'] == 'reseller_admin_user'){//reseller_admin_user
                                    //Assign to the iassist_admin_user
                                    $sql = 'SELECT * FROM DTM_request ';

                                    $sql .= 'INNER JOIN DTM_request_users ';
                                    $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                    $sql .= 'INNER JOIN DTM_users ';
                                    $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                    $sql .= 'INNER JOIN DTM_companies ';
                                    $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                    $sql .= 'INNER JOIN DTM_user_types ';
                                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                    $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                                    $sql .= 'AND  DTM_user_types.hdrut_value = \'iassist_admin_user\'';
                                    $iassist = $this->mysql_fetch_one($sql);

                                    if(count($iassist) > 0){
                                        //Change the hdru status to 4
                                        $sql = 'UPDATE DTM_request_users ';
                                        $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                                        $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                        $sql .= 'AND hdru_userid = ' . $iassist['user_id'];
                                        $id = $this->db_update($sql);
                                    }
                                }
                            }else{
                                //Failure
                                $success = false;
                            }
                        }
                    }else{
                        //what's the current user's user_type, as which type of admin is he
                        $user_type = $user['hdrut_value'];

                        //Is there an admin above this one attached to this request
                        $sql = 'SELECT * FROM DTM_request ';

                        $sql .= 'INNER JOIN DTM_request_users ';
                        $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                        $sql .= 'INNER JOIN DTM_users ';
                        $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                        $sql .= 'INNER JOIN DTM_companies ';
                        $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                        $sql .= 'INNER JOIN DTM_user_types ';
                        $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                        $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                        if($user_type == 'client_admin_user'){
                            $sql .= 'AND  DTM_user_types.hdrut_value = \'reseller_admin_user\'';
                        }elseif($user_type == 'reseller_admin_user'){
                            $sql .= 'AND  DTM_user_types.hdrut_value = \'iassist_admin_user\'';
                        }elseif($user_type == 'iassist_admin_user'){
                            $sql .= 'AND  DTM_user_types.hdrut_value = \'this_doesnt_exist\'';
                        }

                        $admin = $this->mysql_fetch_one($sql);

                        if(count($admin) > 0){

                            // Change current_user hdru_status to 4, Update the user_role of the current user to interested_party
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = 9, hdru_role = 2 ';
                            $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                            $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                            $id = $this->db_update($sql);

                            if($id > 0){
                                $this->system_log[] = 'hdru_status has become in escalated in DTM_request_users for ' . $user['user_name'] . ' and the user role is now back to interested party';

                                if($admin['hdrut_value'] == 'reseller_admin_user'){
                                    //Is there an iassist_admin above this one attached to this request
                                    $sql = 'SELECT * FROM DTM_request ';

                                    $sql .= 'INNER JOIN DTM_request_users ';
                                    $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                                    $sql .= 'INNER JOIN DTM_users ';
                                    $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                                    $sql .= 'INNER JOIN DTM_companies ';
                                    $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                    $sql .= 'INNER JOIN DTM_user_types ';
                                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                    $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';

                                    $sql .= 'AND  DTM_user_types.hdrut_value = \'iassist_admin_user\'';

                                    $iassist_admin = $this->mysql_fetch_one($sql);

                                    if(count($iassist_admin) > 0){
                                        $hdru_status = 9;
                                    }else{
                                        $hdru_status = $var['hdr_action'];
                                    }
                                }elseif($admin['hdrut_value'] == 'iassist_admin_user'){
                                    $hdru_status = $var['hdr_action'];
                                }


                                //Assign this request to the higher level user, and update their status to 4 (In progress)
                                $sql = 'UPDATE DTM_request_users ';
                                $sql .= 'SET hdru_status = ' . $hdru_status . ', hdru_role = 1 ';
                                $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'] . ' ';
                                $sql .= 'AND hdru_userid = ' . $admin['user_id'];
                                $id = $this->db_update($sql);


                                if($id > 0){
                                    $this->system_log[] = 'hdru_status has become In Progress in DTM_request_users for ' . $admin['user_name'] . ' and the user role is now the assigned user again';
                                    $this->user_changes = $user['user_name'] . ' has rejected the resolution recieved from ' . $admin['user_name'];

                                    //Success
                                    $success = true;
                                }
                            }else{
                                //Failure
                                $success = false;
                            }

                        }else{
                            // Change current_user hdru_status to 4, because he's the one working on it
                            $sql = 'UPDATE DTM_request_users ';
                            $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                            $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                            $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                            $id = $this->db_update($sql);

                            if ($id > 0) {
                                $success = true;

                                $sql = 'SELECT * FROM DTM_list_user_status ';
                                $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                                $new_status = $this->mysql_fetch_one($sql);
                                $this->system_log[] = 'hdru_status is now '.$new_status['hlus_value'].' in DTM_request_users for ' . $user['user_name'];

                                //Is the user below this level the add_user
                                $sql = 'SELECT * FROM DTM_request ';

                                $sql .= 'INNER JOIN DTM_users ';
                                $sql .= 'ON DTM_users.user_id = DTM_request.hdr_add_userid ';

                                $sql .= 'INNER JOIN DTM_companies ';
                                $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

                                $sql .= 'INNER JOIN DTM_user_types ';
                                $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

                                $sql .= 'WHERE  hdr_id = ' . $var['hdrid'] . ' ';
                                if($user_type == 'client_admin_user'){
                                    $sql .= 'AND  DTM_user_types.hdrut_value = \'client_user\'';
                                }elseif($user_type == 'reseller_admin_user'){
                                    $sql .= 'AND  (DTM_user_types.hdrut_value = \'client_admin_user\' OR DTM_user_types.hdrut_value = \'reseller_user\')';
                                }elseif($user_type == 'iassist_admin_user'){
                                    $sql .= 'AND  DTM_user_types.hdrut_value = \'reseller_admin_user\'';
                                }
                                $add_user_check = $this->mysql_fetch_one($sql);
                                if(is_array($add_user_check) && count($add_user_check) == 1){
                                    //Change the hdr_add_status to 4 as well
                                }
                            } else {
                                $success = false;
                            }
                        }

                    }
                }
            }elseif ($var['hdr_action'] == 6 || $var['hdr_action'] == 7 || $var['hdr_action'] == 8) {
                /*
                 * This section handles final closures
                 * and we shan't be able to modify this request once
                 * it passes through this section,
                 * because this is where requests go to die*/
                // 6 Cancellation
                // 7 Confirmed Completion
                // 8 Forced Close
                $update_values[$this->getTableField() . 'add_status'] = $var['hdr_action'];
                $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->convertArrayToSQL($update_values);
                $sql .= ' WHERE  hdr_id = ' . $var['hdrid'];
                $id = $this->db_update($sql);

                if($id > 0){
                    $this->system_log[] = 'hdr_add_status has changed from Marked As Resolved to Completed in DTM_request';

                    // 1. Change status to completed at client admin user level, reseller admin user level, and iassist
                    $sql = 'UPDATE DTM_request_users ';
                    $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                    $sql .= 'WHERE hdru_hdrid = ' . $var['hdrid'];
                    $id = $this->db_update($sql);

                    if($id > 0){
                        $this->system_log[] = 'hdru_status has changed from Marked As Resolved to Completed in DTM_request_users for all users';

                        $this->user_changes = $user['user_name'] . ' has confirmed resolution for HDR'. $var['hdrid'];

                        $success = true;
                    }else{
                        $success = false;
                    }
                }else{
                    $success = false;
                }
            }else {
                $sql = 'UPDATE DTM_request_users ';
                $sql .= 'SET hdru_status = ' . $var['hdr_action'] . ' ';
                $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                $id = $this->db_update($sql);

                if ($id > 0) {

                    $sql = 'SELECT * FROM DTM_request ';
                    $sql .= 'INNER JOIN DTM_request_users ';
                    $sql .= 'ON DTM_request_users.hdru_hdrid = DTM_request.hdr_id ';

                    $sql .= 'INNER JOIN DTM_list_user_status ';
                    $sql .= 'ON DTM_list_user_status.hlus_id = DTM_request_users.hdru_status ';

                    $sql .= 'INNER JOIN DTM_users ';
                    $sql .= 'ON DTM_users.user_id = DTM_request_users.hdru_userid ';

                    $sql .= 'INNER JOIN DTM_companies ';
                    $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';


                    $sql .= 'INNER JOIN DTM_user_types ';
                    $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';


                    $sql .= 'INNER JOIN DTM_list_user_roles ';
                    $sql .= 'ON DTM_list_user_roles.hlur_id = DTM_request_users.hdru_role ';

                    $sql .= 'WHERE DTM_request.hdr_id = ' . $var['hdr_action'] . ' ';
                    $sql .= 'AND DTM_request_users.hdru_userid = ' . $user['user_id'];
                    $old_status = $this->mysql_fetch_one($sql);

                    $sql = 'SELECT * FROM DTM_list_user_status ';
                    $sql .= 'WHERE hlus_id = ' . $var['hdr_action'];
                    $new_status = $this->mysql_fetch_one($sql);

                    $this->system_log[] = 'hdru_status has changed from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'] . ' in DTM_request_users';
                    $this->user_changes = $user['user_name'].' (assigned_user) has changed the status from '.$old_status['hlus_value'] .' to ' . $new_status['hlus_value'];

                    $success = true;
                } else {
                    $success = false;
                }
            }
        } elseif ($var['hdr_action'] == 9) {
            /*
             * Escalate this request upward
             * */
            $id = $this->DTMRequestEscalation($var['hdrid']);

            if ($id > 0) {
                $success = true;
            } else {
                $success = false;
            }
        } elseif($var['hdr_action'] > 10) {
           //This is a return
            $reception_id = $var['hdr_action'] - 10;

            $sql = 'SELECT * FROM ' . $this->getTableName() . ' ';

            $sql .= 'INNER JOIN DTM_users ';
            $sql .= 'ON DTM_users.user_id = ' . $this->getTableName() . '.hdr_add_userid ';

            $sql .= 'INNER JOIN DTM_companies ';
            $sql .= 'ON DTM_companies.hdc_id = DTM_users.user_company ';

            $sql .= 'INNER JOIN DTM_user_types ';
            $sql .= 'ON DTM_user_types.hdrut_id = DTM_users.user_type ';

            $sql .= 'WHERE  hdr_id = ' . $var['hdrid'];
            $request = $this->mysql_fetch_one($sql);

            //Find out if the user the being returned to is an add user or an interested party
            // So that we know which table to affect
            if($reception_id == $request['hdr_add_userid']){
                //This is the add user
                $sql = 'UPDATE ' . $this->getTableName() . ' ';
                $sql .= 'SET hdr_add_status = 3 ';
                $sql .= 'WHERE  hdr_id = ' . $var['hdrid'];
                $id = $this->db_update($sql);

                if($id > 0 ){
                    $this->system_log[] = 'hdr_add_status is now Returned on ' . $this->getTableName();
                }
            }else{
                //This is one of the ineterested parties
                $sql = 'UPDATE DTM_request_users ';
                $sql .= 'SET hdru_status = 3 ';
                $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                $sql .= 'AND  hdru_userid = ' . $reception_id;
                $id = $this->db_update($sql);

                if($id > 0 ){
                    $this->system_log[] = 'hdru_status is now Returned on DTM_request_users for this intersted party';
                }
            }

            if($id > 0){
                $sql = 'UPDATE DTM_request_users ';
                $sql .= 'SET hdru_status = 10 ';
                $sql .= 'WHERE  hdru_hdrid = ' . $var['hdrid'] . ' ';
                $sql .= 'AND  hdru_userid = ' . $user['user_id'];
                $id = $this->db_update($sql);

                if($id > 0){
                    $success = true;
                    $this->system_log[] = 'hdru_status is now Waiting on DTM_request_users where hdru_userid is ' . $user['user_id'];
                    $this->user_changes = $user['user_name'].' (assigned_user) has returned HDR' . $var['hdrid'] . ' to that user for more information';
                }
            }else{
                $success = false;
            }


        }elseif(is_string($var['hdr_action']) && $var['hdr_action'] == 'claim') {
            $user_role = 1;// Assigned
            $user_status = 2;// Confirmed
            $id = $this->DTMRequestAssignment($var['hdrid'], $user['user_id'], $user_role, $user_status);

            if($id > 0){
                $success = true;
                $this->system_log[] = $user['user_name'].' (assigned_user) has Claimed HDR' . $var['hdrid'] . ' and is now the asigned user, and the status is now confirmed on DTM_request_users';
                $this->user_changes = $user['user_name'].' (assigned_user) has Claimed HDR' . $var['hdrid'];
            }else{
                $success = false;
            }
        }elseif($var['hdr_action'] == 0) {
            //'This is just a plain update'
            $this->user_changes = $user['user_name'].' has added an update to HDR' . $var['hdrid'];
            $success = true;

            //Find out if the current user is the add_user
        }

        //Return the relevant message upon success or failure
        if ($success) {
            $update_log_entry = array();
            $update_log_entry['hdrup_hdrid'] = $var['hdrid'];
            $update_log_entry['hdrup_response'] = mysqli_real_escape_string($this->get_db_resource(), $var['hdrup_description']);
            $update_log_entry['hdrup_attachment'] = 'This is an attachment';
            $update_log_entry['hdrup_datetime'] = 'GETDATE()';
            $update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
            $update_log_entry['hdrup_user_changes'] = $this->user_changes;
            $update_log_entry['hdrup_userid'] = $var['user_id'];

            //Enter all the update information into the update/log table
            $sql = 'INSERT INTO DTM_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
            $sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
            $sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
            $sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
            $sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
            $sql .= $update_log_entry['hdrup_userid'].')';
            $id = $this->db_insert($sql);

            if ($id > 0) {
                $updated = true;
            } else {
                // Failure
                $updated = false;
            }

        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }

        //Return the relevant message upon success or failure
        if ($updated) {
            $result = array(
                0 => "ok",
                1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $var['hdrid'] . " has been successfully updated.",
                'object_id' => $var['hdrid'],
            );
            return $result;
        } elseif (!$updated) {
            return array("error", "Testing: " . $sql);
        }
    }


    public function finaliseRequest($var)
    {
        $req_id = $var['id'];

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $old = $this->mysql_fetch_one($sql);

        $status = self::ACTIVE + self::ACTIVATED;
        $response = $this->getObjectName("request") . " $req_id has been finalised. ";
        $objObject = new DTM_REQUEST_OBJECTS();
        $auth_required = $objObject->checkAuthorisationRequired($req_id);
        if ($auth_required) {
            $status += self::AUTHORISATION_REQUIRED;
            $response .= " It has been sent for authorisation.";
        } else {
            $response .= " It has been sent to the " . $this->getObjectName("agent") . ".";
        }

        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . ($status) . " WHERE " . $this->getIDFieldName() . " = $req_id ";
        $this->db_update($sql);

        /**
         * NOTIFY RELEVANT PARTY
         * if(auth_required) {
         *   check $old for authorisation setting
         * } else {
         *   check travel agent db for linked user
         * }
         */


        //LOGGING

        return array("ok", $response . "   <br /><br /><span style='font-weight:bold; font-size: 150%; color:#CC0001'>QUESTION???? Who must be notified IF Request requires Authorisation????</span>");
    }


    /**
     * Function to record authorisation of request and handle notifying booker and sending to travel agent
     */
    public function authoriseAccepted($req_id)
    {
        //update request status
        $old = $this->getRawObject($req_id);
        $status = $old['req_status'] - self::AUTHORISATION_REQUIRED + self::AUTHORISE_ACCEPTED;
        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . $status . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $this->db_update($sql);
        //logging

        //notify booker

        //send to TVLA
    }

    /**
     * Function to record rejection / non-authorisation and handle notifying the booker and sending the request back to NEW process
     */
    public function authoriseRejected($req_id)
    {
        //update request status
        $old = $this->getRawObject($req_id);
        $status = $old['req_status'] - self::ACTIVATED - self::AUTHORISATION_REQUIRED + self::AUTHORISE_REJECTED;
        $sql = "UPDATE " . $this->getTableName() . " SET " . $this->getStatusFieldName() . " = " . $status . " WHERE " . $this->getIDFieldName() . " = " . $req_id;
        $this->db_update($sql);
        //logging
        //notify booker
    }


    /**
     * function to get a list of requests waiting on authorisation - ADMIN view
     */
    public function getListOfRequestsAwaitingAuthorisationForAdmin()
    {
        return $this->getListOfRequests("ADMIN", "AUTHORISE");
    }

    /**
     * function to get a list of requests waiting on authorisation - MANAGE view
     */
    public function getListOfRequestsAwaitingAuthorisationForManage()
    {
        return $this->getListOfRequests("MANAGE", "AUTHORISE");
    }

    /**
     * Function to get a list of incomplete requests for NEW
     */
    public function getListOfIncompleteRequests()
    {
        return $this->getListOfRequests("NEW", "NEW");
    }

    /**
     * Function to get a list of activated requests for VIEW
     */
    public function getListOfActivatedRequests()
    {
        return $this->getListOfRequests("MANAGE", "VIEW");
    }

    /**
     * Function to get a list of requests for specific page & function
     */
    public function getListOfRequests($section, $page)
    {
        //Get headings
        $headObject = new DTM_HEADINGS();
        $headings = $headObject->getMainObjectHeadings("REQUEST", "LIST", $section, "", true);
        $head = array();
        foreach ($headings as $fld => $h) {
            $head[$fld] = $h['name'];
        }
        //$this->arrPrint($headings);
        //Get rows
        $listObject = new DTM_LIST("travel_type");
        $projectObject = new DTM_PROJECTS();
        $project_owners = $projectObject->getAllOwnersForRequestDisplay();
        $sql = "SELECT req.*
				  , req_booker as raw_req_booker
				  , CONCAT(tk.tkname,' ',tk.tksurname) as req_booker
				  , TT.id as raw_req_traveltypeid
				  , TT.name as req_traveltypeid
				  , req_policyid as raw_req_policyid
				  , CONCAT('" . DTM_POLICY::REFTAG . "',req_policyid) as req_policyid 
				  , req_benefitsid as raw_req_benefitsid
				  , CONCAT('" . DTM_BENEFITS::REFTAG . "',req_benefitsid) as req_benefitsid
				  , req_authoriser as raw_req_authoriser
				  , IF(req_authoriser='project','" . $this->getObjectName("project") . "-dependent',IF(req_authoriser='any','Any',CONCAT(auth.tkname,' ',auth.tksurname))) as req_authoriser
				  , req_approver as raw_req_approver 
				  , IF(req_approver='project','" . $this->getObjectName("project") . "-dependent',IF(req_approver='any','Any',CONCAT(appr.tkname,' ',appr.tksurname))) as req_approver
				  , req_project as raw_req_project
				  , proj." . $projectObject->getNameFieldName() . " as req_project
				FROM " . $this->getTableName() . " req 
				INNER JOIN " . $this->getUserTableName() . " tk
				  ON req_booker = tk.tkid
				INNER JOIN " . $listObject->getListTable() . " TT
				  ON req_traveltype = TT.id
				LEFT OUTER JOIN " . $this->getUserTableName() . " auth
				  ON req_authoriser = auth.tkid
				LEFT OUTER JOIN " . $this->getUserTableName() . " appr
				  ON req_approver = appr.tkid
				LEFT OUTER JOIN " . $projectObject->getTableName() . " proj
				  ON req_project = proj." . $projectObject->getIDFieldName() . "
				WHERE 
					(req_status & " . self::ACTIVE . ") = " . self::ACTIVE . "
				";
        switch ($page) {
            case "NEW":
                $where = " AND req_status = " . self::ACTIVE . " 
					AND req_booker = '" . $this->getUserID() . "'";
                break;
            case "VIEW":
                $where = "
					AND (req_status & " . self::ACTIVATED . ") = " . self::ACTIVATED . "
					";
                break;
            case "AUTHORISE":
                $where = "
					AND (req_status & " . self::ACTIVATED . ") = " . self::ACTIVATED . "
					AND (req_status & " . self::AUTHORISATION_REQUIRED . ") = " . self::AUTHORISATION_REQUIRED . "
					AND (req_status & " . self::AUTHORISE_ACCEPTED . ") <> " . self::AUTHORISE_ACCEPTED . "
					AND (req_status & " . self::AUTHORISE_REJECTED . ") <> " . self::AUTHORISE_REJECTED . "
						
					";
                if ($section == "MANAGE") {
                    $where .= "
							AND (
								req_authoriser = '" . $this->getUserID() . "'
								OR
								req_authoriser = 'any'
								OR
								req_authoriser = 'project'
							)
							";
                }
                break;
            case "APPROVE":
                break;
        }
        $sql .= $where;
        $rows = $this->mysql_fetch_all_by_id($sql, "req_id");

        //Get list of travel agents
        $agentObject = new DTM_AGENTS();
        $agents = $agentObject->getAllTravelAgentsForDTM();

        $displayObject = new DTM_DISPLAY();

        $project_ids = array();
        $projectObject = new DTM_PROJECTS();
        //get list of projects ids from rows
        // & set display format of travel insurance
        foreach ($rows as $i => $r) {
            if ($r['req_travelinsurance'] == 0) {
                $rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(0);
            } else {
                $rows[$i]['req_travelinsurance'] = $displayObject->getBoolForDisplay(1);
            }
            $rows[$i]['raw_req_travelagent'] = $r['req_travelagent'];
            $rows[$i]['req_travelagent'] = isset($agents[$r['req_travelagent']]) ? $agents[$r['req_travelagent']] : $r['req_travelagent'];
            if (!in_array($r['raw_req_project'], $project_ids)) {
                $project_ids[] = $r['raw_req_project'];
            }
        }
        //get list of parents
        if (count($project_ids) > 0) {
            $project_parents = $projectObject->getProjectHierarchyForRequest($project_ids, true);
            //get authorisers/approvers for all
            $project_owners = $projectObject->getAllOwners();
            //generate list of owners for original list of ids
            $project_authorisers = array();
            $project_approvers = array();
            foreach ($project_parents as $pi => $pp) {
                $project_authorisers[$pi] = array();
                $project_approvers[$pi] = array();
                foreach ($pp as $p) {
                    if (isset($project_owners[$p])) {
                        foreach ($project_owners[$p] as $ti => $t) {
                            if ($t['authoriser'] == true) {
                                $project_authorisers[$pi][$ti] = $t['name'];
                            }
                            if ($t['approver'] == true) {
                                $project_approvers[$pi][$ti] = $t['name'];
                            }
                        }
                    }
                }
            }
            foreach ($rows as $ri => $r) {
                $pi = $r['raw_req_project'];
                //check rows and remove those without access
                if ($section == "MANAGE" && $page == "AUTHORISE" && $r['raw_req_authoriser'] == "project" && !isset($project_authorisers[$pi][$this->getUserID()])) {
                    unset($rows[$ri]);
                } elseif ($section == "MANAGE" && $page == "APPROVE" && $r['raw_req_approver'] && !isset($project_approvers[$pi][$this->getUserID()])) {
                    unset($rows[$ri]);
                } else {
                    if ($r['raw_req_authoriser'] == "project") {
                        $rows[$ri]['req_authoriser'] = implode(";<br />", $project_authorisers[$pi]) . ";";
                    }
                    if ($r['raw_req_approver'] == "project") {
                        $rows[$ri]['req_approver'] = implode(";<br />", $project_approvers[$pi]) . ";";
                    }
                }
            }
        }


        $req_ids = array_keys($rows);
        if (count($req_ids) > 0) {
            $travellerObject = new DTM_REQUEST_TRAVELLERS();
            $travellers = $travellerObject->getList($req_ids);
            foreach ($req_ids as $r) {
                $rows[$r]['traveller'] = implode(", ", $travellers[$r]);
            }
        }

        $data = array(
            'head' => $head,
            'rows' => $rows
        );


        return $data;
    }


    public function getRawObject($req_id)
    {
        $listObject = new DTM_LIST("travel_type");
        $sql = "SELECT req.*
				  , CONCAT(tk.tkname,' ',tk.tksurname) as booker
				  , CONCAT('" . DTM_POLICY::REFTAG . "',req_policyid) as policy 
				  , CONCAT('" . DTM_BENEFITS::REFTAG . "',req_benefitsid) as benefits
				  , tt.shortcode as travel_type
				FROM " . $this->getTableName() . " req
				INNER JOIN " . $this->getUserTableName() . " tk
				  ON req_booker = tk.tkid
				INNER JOIN " . $listObject->getListTable() . " tt
				  ON tt.id = req.req_traveltype
				WHERE req_id = " . $req_id;
        $data = $this->mysql_fetch_one($sql);
        foreach ($data as $key => $d) {
            $k = explode("_", $key);
            unset($k[0]);
            $key = implode("_", $k);
            if (!isset($data[$key])) {
                $data[$key] = $d;
            }
        }
        $data['travellers'] = array();
        $data['travellers_benefit_category'] = array();
        $data['benefit_category_id'] = array();
        $data['travellers_extras'] = array();
        $rtObject = new DTM_REQUEST_TRAVELLERS();
        $rt = $rtObject->getList($req_id, true);
        foreach ($rt as $ti => $t) {
            $data['travellers'][$ti] = $t['name'];
            $data['travellers_benefit_category'][$ti] = $t['benefit_category'];
            $data['travellers_benefit_category_id'][$ti] = $t['benefit_category_id'];
            if (!in_array($t['benefit_category_id'], $data['benefit_category_id'])) {
                $data['benefit_category_id'][] = $t['benefit_category_id'];
            }
            $data['travellers_extras'][$ti] = isset($t['rt_extras']) && strlen($t['rt_extras']) > 0 ? $rtObject->decodeExtrasFromDB($t['extras']) : array();
        }

        $pObject = new DTM_PROJECTS();
        $data['projects'] = $pObject->getProjectHierarchyForRequest($data['project']);

        return $data;
    }


    public function getBenefitSettings($req_id)
    {
        return $_SESSION[$this->getModRef()]['REQUEST'][$req_id]['benefits']['settings'];
    }


    public function __destruct()
    {
        parent::__destruct();
    }


}

?>
