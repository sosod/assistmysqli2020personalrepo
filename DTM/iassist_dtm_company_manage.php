<?php require_once('inc_header.php'); ?>
<h2>Companies</h2>
<?php
//Get every registered company
$db = new ASSIST_DB('master');
$sql = 'SELECT cmpcode, cmpname FROM assist_company ';
$ait_companies = $db->mysql_fetch_all($sql);

$sql = 'SELECT * FROM dtm_companies ';
$dtm_companies = $helper->mysql_fetch_all_by_id($sql, 'comp_ait_cc');

$show_manage_button = false;
?>

<form name="companies" method="post" action="manage.php" enctype="multipart/form-data">
    <table class="list " id="paging_obj_list_view" width="100%">
        <tbody>
        <tr id="head_row">
            <th>Company Name:</th>
            <th>Company Code:</th>
            <th>ADD/REMOVE:</th>
            <th></th>
        </tr>
        <?php foreach($ait_companies as $key => $val){ ?>
        <tr>
            <td><?php echo $val['cmpname']; ?></td>
            <td><?php echo $val['cmpcode']; ?></td>
            <td class="center">
                <?php if(strtolower($val['cmpcode']) != 'iassist'){ ?>
                    <?php if((array_key_exists($val['cmpcode'], $dtm_companies) && (int)$dtm_companies[$val['cmpcode']]['comp_active'] == 1) || (array_key_exists(strtolower($val['cmpcode']), $dtm_companies) && (int)$dtm_companies[strtolower($val['cmpcode'])]['comp_active'] == 1)  || (array_key_exists(strtoupper($val['cmpcode']), $dtm_companies) && (int)$dtm_companies[strtoupper($val['cmpcode'])]['comp_active'] == 1)){ ?>
                        <?php $show_manage_button = true; ?>
                        <input type="submit" data-cc="<?php echo $val['cmpcode']; ?>"  value="Delete from DTM" ref="3" class="btn_company_change">
                    <?php }else{ ?>
                        <input type="submit" data-cc="<?php echo $val['cmpcode']; ?>"  value="Add to DTM" ref="3" class="btn_company_change">
                    <?php } ?>
                <?php }else{ ?>
                    <?php $show_manage_button = true; ?>
                <?php } ?>
            </td>
            <td class="center">
                <?php if($show_manage_button == true){ ?>
                <input type="submit" data-cc="<?php echo $val['cmpcode']; ?>"  value="Manage" ref="3" class="company_manage">
                <?php } ?>
            </td>
        </tr>
            <?php $show_manage_button = false; ?>
        <?php } ?>

        </tbody>
    </table>
</form>


<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>
        $('.btn_company_change').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var value = [];
            value["cc"] = $(this).data('cc');
            var dta = {comp_cc : value["cc"]};
            console.log(value);
            console.log(dta);

            var action;
            if($(this).val() == 'Add to DTM'){
                action = 'COMPANY.SIMPLEADD';
            }else{
                action = 'COMPANY.DELETE';
            }

            var page_action = action;
            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'iassist_dtm_company_manage.php?');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });

        $('.company_manage').click(function(e){
            e.preventDefault();
            document.location.href = 'setup_defaults.php?company_cc=' + $(this).data('cc');
        });
    });
</script>

<?php require_once('inc_footer.php'); ?>
