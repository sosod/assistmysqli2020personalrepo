<?php
error_reporting(-1);
require_once("../library/composer/vendor/autoload.php");

use \Firebase\JWT\JWT;

/*
$company -> Company of user,
$user -> Username/Login,
$claim -> Not used at this time. Any value is accepted e.g. N/A,
$userTimeout -> The custom timeout for the user in Seconds,
$exp -> Date and time the token will expire in the following format: 'Y-m-d H:i:s' e.g. 2012-03-24 17:45:12. Milliseconds also allowed,
$nbf -> Date and time the token will be valid from in the following format 'Y-m-d H:i:s' e.g. 2012-03-24 17:45:12. Milliseconds also allowed,
$secret -> base64 encoded secret
*/

//DEV ENVIRONMENT SECRET
$secret = 'GrYt1FlQd3_yaBeN08_UcxOuoQc-pMO0aX8OGK_mNZA';

function buildToken($company, $user, $claim, $userTimeout, $exp, $nbf, $secret) {
    $data = array(
        'company' => $company,
        'user' => $user,
        'Claim' => $claim,
        'userTimeout' => $userTimeout,
        'exp' => $exp,
        'nbf' => $nbf
    );



//    $secretKey = base64_decode($secret);
    $secretKey = $secret;

    $jwt = JWT::encode(
        $data,      //Data to be encoded in the JWT
        $secretKey, // The signing key
        'HS256'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
    );
    return json_encode($jwt);
}


/********* TEST DATA STARTS HERE *******/
// set the default timezone to use. Available since PHP 5.1
date_default_timezone_set('Africa/Johannesburg');

require ("../library/autoloader.php");
$helper = new DTM();

$sql = 'SELECT * FROM dtm_companies ';

$sql .= 'INNER JOIN dtm_users ';
$sql .= 'ON dtm_users.user_company = dtm_companies.comp_id ';

$sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
$sql .= 'AND dtm_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
$company_info = $helper->mysql_fetch_one($sql);

$company = $company_info['comp_dtm_name'];
$user = $company_info['user_name_dtm'];
$claim = 'ActionIt';
$userTimeout = 60*60; //1 hour
$exp = date("Y-m-d H:i:s",strtotime('+5 hours'));
$nbf = date("Y-m-d H:i:s",strtotime('-24 hours'));
//$secret - provided above;

$t = buildToken($company, $user, $claim, $userTimeout, $exp, $nbf, $secret);

/*
 * Tshegofatso Stuff
 * */
$url = 'https://services.dtmsoftwarepartners.com/Authentication/api/auth/ExternalToken';
$data = json_decode($t);

$curl = curl_init($url);
$headers = array();

$headers[] = 'Accept: application/json';
$headers[] = 'Content-Type: application/json';
$headers[] = 'source_id: 289f4fa20fe64729b039c762b20c2a97';
$headers[] = 'Client_id : 289f4fa20fe64729b039c762b20c2a97';
$headers[] = 'Access_token: ' . $data;

curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);

$link = 'https://www.dtmsuite.com/?access-token='.json_decode($result).'/';

header("Location:" . $link);