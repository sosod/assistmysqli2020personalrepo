<?php
require ("../library/autoloader.php");
$helper = new DTM();


if(isset($_GET['show_management']) && $_GET['show_management'] == 'yes_sir'){
    header("Location:manage.php");
}else{
    //Check that that current company exists in the dtm_db
    $sql = 'SELECT * FROM dtm_companies ';
    $sql .= 'WHERE dtm_companies.comp_ait_cc = \''.$_SESSION['cc'] . '\' ';
    $company_info = $helper->mysql_fetch_one($sql);

    if(isset($company_info) && is_array($company_info) && count($company_info) > 0){
        //Check that there is a value for the dtm_company_name
        if($company_info['comp_dtm_name'] != ''){
            //Check that the current user is in the dtm_db
            $sql = 'SELECT * FROM dtm_users ';
            $sql .= 'WHERE dtm_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
            $sql .= 'AND dtm_users.user_company = '.$company_info['comp_id'] . ' ';
            $user_info = $helper->mysql_fetch_one($sql);

            if(isset($user_info) && is_array($user_info) && count($user_info) > 0){
                //Check that the dtm_user_name has a value in it
                if($user_info['user_name_dtm'] != ''){
                    //use these dtm details to redirect to the auto-loggedin dtm system
                    header("Location:dtm_conn_test.php");
                }else{
                    //Redirect him to the manage page where he'll be prompted to edit his dtm_user_name
                    header("Location:manage.php");
                }
            }else{
                //Create the user then redirect him to the manage page where he'll be prompted to edit his dtm_user_name
                header("Location:manage.php");
            }

        }else{
            //Check that the current user has access to setup
            if(true){
                //Redirect him to setup and instruct him to edit the company name when they get there
            }else{
                //Redirect the current user to the manage page and instruct them to request that the company name be edited by the users with access to setup when they get there (Also just email those guys)
                header("Location:manage.php");
            }
        }

    }else{
        header("Location:manage.php");
    }
}
?>