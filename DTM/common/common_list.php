<?php require_once('../inc_header.php'); ?>

<table class="list " id="paging_obj_list_view" width="100%">
    <tbody>
    <tr id="head_row">
        <th>Client Type</th>
        <th>Customer Name</th>
        <th>Customer Surname</th>
        <th>Reference</th>
        <th>Organisation Type</th>
        <th>Organisation Name</th>
        <th>Registration Number</th>
        <th>Internal Code</th>
        <th>Added By</th>
        <th>Status</th>
        <th>Prefix</th>
        <th>Gender</th>
        <th></th>
    </tr>
    <tr>
        <td class="">[Unspecified]</td>
        <td class="">Kodi employment</td>
        <td class=""></td>
        <td class="">C3</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">1</td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class=""></td>
        <td class="">Manlik</td>
        <td class="center"><input type="button" value="View" ref="3" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">C60</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="60" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">C50</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="50" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class="">edgy!</td>
        <td class=""></td>
        <td class="">C95</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">edgy1</td>
        <td class="">Klaas September</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">Manlik</td>
        <td class="center"><input type="button" value="View" ref="95" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class="">evan</td>
        <td class="">???</td>
        <td class="">C58</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="58" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class="">evan</td>
        <td class="">???</td>
        <td class="">C59</td>
        <td class="">[Unspecified]</td>
        <td class=""></td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="59" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Organisation</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">C39</td>
        <td class="">Closed Corp</td>
        <td class="">Hazel's exquisite vegetarian cuisine</td>
        <td class="">71077345</td>
        <td class="">ORG1</td>
        <td class="">Tom Builder</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="39" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Organisation</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">C47</td>
        <td class="">Closed Corp</td>
        <td class="">juj</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="47" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Person</td>
        <td class="">lol</td>
        <td class=""></td>
        <td class="">C45</td>
        <td class="">Sole Proprietor</td>
        <td class=""></td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="45" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">[Unspecified]</td>
        <td class="">p_name</td>
        <td class="">p_surname</td>
        <td class="">C36</td>
        <td class="">[Unspecified]</td>
        <td class="">o_name</td>
        <td class="">1337</td>
        <td class="">1</td>
        <td class="">Mike Billing</td>
        <td class="">Active</td>
        <td class=""></td>
        <td class="">Manlik</td>
        <td class="center"><input type="button" value="View" ref="36" class="btn_list"></td>
    </tr>
    <tr>
        <td class="">Organisation</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">C48</td>
        <td class="">Sole Proprietor</td>
        <td class="">pip</td>
        <td class=""></td>
        <td class=""></td>
        <td class="">[Unspecified]</td>
        <td class="">Active</td>
        <td class="">[Unspecified]</td>
        <td class="">[Unspecified]</td>
        <td class="center"><input type="button" value="View" ref="48" class="btn_list"></td>
    </tr>
    </tbody>
</table>


<?php require_once('../inc_footer.php'); ?>
