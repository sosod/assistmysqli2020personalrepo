<?php require_once('inc_header.php'); ?>
<?php
/*
 * These are the company details
 * Including:
 * Company information
 * Admin users, if any
 * Preferred way of handling the helpdesk request (settings)
*/

if(isset($_GET['company_cc'])){
    $company_cc = $_GET['company_cc'];
}else{
    $company_cc = $_SESSION['cc'];
}

$sql = 'SELECT * FROM dtm_companies ';
$sql .= 'WHERE dtm_companies.comp_ait_cc = \''. $company_cc . '\' ';
$company_info = $helper->mysql_fetch_one($sql);
?>

<?php if(isset($company_info) && is_array($company_info) && count($company_info) > 0){ ?>

    <?php
    ASSIST_HELPER::displayResult(
        isset($_REQUEST['r']) ? $_REQUEST['r'] : array()
    );
    ?>

    <form name="comp_update" method="post" enctype="multipart/form-data">
        <table id="tbl_action" class="form">
            <tbody>
            <!-- Company Information - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Company Name (AiT):</th>
                <td>
                    <?php echo $company_info['comp_ait_name']; ?>
                </td>
            </tr>
            <!-- Company Information - END -->

            <!-- Company Information - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Company Code (AiT):</th>
                <td>
                    <?php echo $company_info['comp_ait_cc']; ?>
                </td>
            </tr>
            <!-- Company Information - END -->

            <!-- Company Information - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Company Name (DTM):</th>
                <td>
                    <?php echo ($company_info['comp_dtm_name'] != '' ? $company_info['comp_dtm_name'] : 'N/A' ); ?>
                </td>
            </tr>
            <!-- Company Information - END -->

            <!-- Company Information - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">New Company Name (DTM):</th>
                <td>
                    <input id="dtm_comp_name" name="dtm_comp_name" type="text" style="width: 98%;">
                </td>

            </tr>
            <!-- Company Information - END -->

            <tr>
                <th></th>
                <td>
                    <input id="btn_submit" type="button" value="Update Company" data-hdrid="user_id" class="isubmit i-am-the-submit-button">
                </td>
            </tr>
            </tbody>
        </table>
    </form>

    <br/>
    <br/>

    <?php
        $sql = 'SELECT * FROM dtm_users ';
        $sql .= 'WHERE dtm_users.user_company = '.$company_info['comp_id'] . ' ';
        $sql .= 'AND dtm_users.user_active = 1 ';
        $users = $helper->mysql_fetch_all_by_id($sql, 'user_id');

        //Get the user access table to determine who has access to the setup functionality
        $sql = 'SELECT * FROM dtm_useraccess ';
        $useraccess = $helper->mysql_fetch_all_by_id($sql, 'ua_user_id');

        //This array is used further below to populate the add_dtm_user form
        $blank_array = array();
    ?>
    <?php if(isset($users) && is_array($users) && count($users) > 0){ ?>
    <h2>USERS</h2>

    <form name="user_information" method="post" action="manage.php" enctype="multipart/form-data">
        <table class="list " id="paging_obj_list_view" width="100%">
            <tbody>
            <tr id="head_row">
                <th>#:</th>
                <th>AiT Username:</th>
                <th>DTM Username:</th>
                <th></th>
            </tr>
            <?php foreach($users as $key => $val){ ?>
            <tr>
                <td><?php echo $val['user_id']; ?></td>
                <td><?php echo $val['user_name_ait']; ?></td>
                <td><?php echo ($val['user_name_dtm'] != '' ? $val['user_name_dtm'] : 'N/A'); ?></td>
                <td class="center">
                <?php if($val['user_tkid'] != $_SESSION['tid']){ ?>
                    <input type="submit" data-userid="<?php echo $val['user_id']; ?>" value="Delete" ref="3" class="btn_admin_change">
                    <?php if(array_key_exists($val['user_id'],$useraccess)){ ?>
                        <input style="width: 130px" type="submit" data-userid="<?php echo $val['user_id']; ?>" value="Revoke Setup Access" ref="3" class="btn_admin_change">
                    <?php }else{ ?>
                        <input style="width: 130px" type="submit" data-userid="<?php echo $val['user_id']; ?>" value="Grant Setup Access" ref="3" class="btn_admin_change">
                    <?php } ?>
                <?php } ?>
                </td>
            </tr>

            <?php $blank_array[] = $val['user_tkid']; ?>

            <?php } // end foreach?>
            </tbody>
        </table>
    </form>
    <?php } ?>

    <br/>
    <br/>


    <?php
    $test_array = array('0002', '0003', '0004', '0005', '0007');

    $sql = 'SELECT * FROM assist_'.$company_info['comp_ait_cc'].'_timekeep ';
    $sql .= 'WHERE tkstatus = \'1\' ';

    if(isset($users) && is_array($users) && count($users) > 0){
        $sql .= 'AND tkid NOT IN (\'' . implode('\',\'', $blank_array) . '\') ';
    }

    $client_db = new ASSIST_MODULE_HELPER('client', $company_info['comp_ait_cc']);
    $tk_users = $client_db->mysql_fetch_all($sql);

    $tk_user_list = array();

    foreach($tk_users as $key => $val){
        if($val['tkuser'] != 'support'){
            $tk_user_list[$val['tkid']] = $val['tkname'] . ' ' . $val['tksurname'];
        }
    }
    ?>
    <form name="add_dtm_user" method="post" enctype="multipart/form-data">
        <table id="tbl_action" class="form">
            <tbody>
            <!-- Decide On An Action To Take - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Users:</th>
                <td>
                    <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'dtm_user', 'name' => 'dtm_user', 'required' => true, 'unspecified' => false,  'options' => $tk_user_list));?>
                </td>
            </tr>
            <!-- Decide On An Action To Take - END -->
            <tr>
                <th></th>
                <td>
                    <input type="hidden" id="comp_cc" name="comp_cc" value="<?php echo $company_info['comp_ait_cc']; ?>">
                    <input id="btn_submit" type="button" value="Add DTM User" data-hdrid="user_id" class="isubmit i-am-the-submit-button">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
<?php }else{ ?>
    <?php
    /*
     * This company doesn't exist in the DTM db,
     * but you shouldn't be able to reach this page if don't have user access to this module
     * which requires the company to exist for navigating here to be possible.
     * ....
     *
     * So, what I'm going to do is make IASSIST able to create companies, add users, and grant/revoke setup access to users
     * */
    ?>
    <?php
        if($_SESSION['cc'] == 'iassist'){
            //Send to iassist manage
            header("Location:iasist_dtm_company_manage.php");
        }
    ?>



    <h3>This company has not been registered in the helpdesk system yet, click the "Register Company" button to do so.</h3>
    <form name="create_company" method="post" enctype="multipart/form-data">
        <input id="btn_submit" type="button" value="Register Company" class="btn_list">
    </form>


<?php } // end else?>


<?php
////Test email softwarer from locahost
//$emailer = new ASSIST_EMAIL();
//$to = 'mosnubian@gmail.com';
//$emailer->setRecipient($to);
//$sub = 'Localhost Email Test';
//$emailer->setSubject($sub);
//$message = 'This is the message you should be getting';
//$emailer->setBody($message);
//$emailer->sendEmail();
?>

    <script type="text/javascript">
        $(function(){
            <?php echo $js; ?>

            //Create the current company because it does not exist in the helpdesk db
            $('form[name="create_company"] #btn_submit').click(function(){
                console.log($('form').serialize());
                HelpdeskHelper.processObjectForm($('form[name="create_company"]'), 'COMPANY.ADD', 'setup_defaults.php?company_cc=' + $('#comp_cc').val() + '&')
            });

            $('form[name="comp_update"] #btn_submit').click(function(){
                console.log($('form').serialize());
                HelpdeskHelper.processObjectForm($('form[name="comp_update"]'), 'COMPANY.SIMPLEEDIT', 'setup_defaults.php?company_cc=' + $('#comp_cc').val() + '&')
            });

            $('form[name="add_dtm_user"] #btn_submit').click(function(){
                console.log($('form').serialize());
                HelpdeskHelper.processObjectForm($('form[name="add_dtm_user"]'), 'USERS.ADD', 'setup_defaults.php?company_cc=' + $('#comp_cc').val() + '&')
            });

            $('.btn_admin_change').click(function(e){
                e.preventDefault();
                AssistHelper.processing();
                var value = [];
                value["user_id"] = $(this).data('userid');
                var dta = {user_id : value["user_id"]};
                console.log(value);
                console.log(dta);

                var action;
                if($(this).val() == 'Delete'){
                    action = 'USERS.DELETE';
                }else if($(this).val() == 'Grant Setup Access'){
                    action = 'USERS.GRANTACCESS';
                }else{
                    action = 'USERS.REVOKEACCESS';
                }

                var page_action = action;
                var url = 'inc_controller.php?action='+page_action;
                var result = AssistHelper.doAjax(url, dta);

                if(result[0]=="ok") {
//                console.log('what in the world have you done');
                    AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'setup_defaults.php?company_cc=' + $('#comp_cc').val() + '&');
//                location.reload();
                } else {
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }

            });
        });
    </script>

<?php
//This section is going to help me connect the added users to the module on the company db
//$sql = 'SELECT * FROM assist_'.$company_info['comp_ait_cc'].'_timekeep ';
$sql = 'SELECT * FROM assist_menu_modules ';

$sql .= 'INNER JOIN assist_'.$company_info['comp_ait_cc'].'_menu_modules_users ';
$sql .= 'ON assist_'.$company_info['comp_ait_cc'].'_menu_modules_users.usrmodid = assist_menu_modules.modid ';

$sql .= 'INNER JOIN assist_'.$company_info['comp_ait_cc'].'_timekeep ';
$sql .= 'ON assist_'.$company_info['comp_ait_cc'].'_timekeep.tkid = assist_'.$company_info['comp_ait_cc'].'_menu_modules_users.usrtkid ';

$sql .= 'WHERE assist_menu_modules.modlocation = \'DTM\' ';
$sql .= 'AND assist_'.$company_info['comp_ait_cc'].'_timekeep.tkid = \''. $_SESSION['tid'] .'\' ';

$module_users = $client_db->mysql_fetch_all($sql);

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($module_users);
//echo '</pre>';
?>

<?php require_once('inc_footer.php'); ?>