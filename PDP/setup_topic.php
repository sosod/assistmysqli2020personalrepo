<?php
$page = array("setup","topic");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"Topics"),
);

include("inc_header.php");

$setupObject = new PDP_SETUP_LISTS("topic");

$result = array();

if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
		case "add":
			$var = array('value'=>ASSIST_HELPER::code($_REQUEST['add_text']));
			$result = $setupObject->addObject($var);
			break;
		case "edit":
			$var = array('value'=>ASSIST_HELPER::code($_REQUEST['edit_text']));
			$id = $_REQUEST['id'];
			$result = $setupObject->editObject($id,$var);
			break;
		case "delete":
			$id = $_REQUEST['id'];
			$result = $setupObject->deleteObject($id);
			break;
		case "restore":
			$id = $_REQUEST['id'];
			$result = $setupObject->restoreObject($id);
			break;
	}
}


//Check db setup to set topic string length
$sql = "DESCRIBE ".$setupObject->getTableName();
$rows = $setupObject->mysql_fetch_all($sql);

foreach($rows as $row) {
	if($row['Field']=="value") {
		$type = explode("(",$row['Type']);
		$type = explode(")",$type[1]);
		$allowed_topic_string_length = $type[0];
	}
}
if(!is_numeric($allowed_topic_string_length)) {
	$allowed_topic_string_length = 45;
} else {
	$allowed_topic_string_length = $allowed_topic_string_length - 10;
}

ASSIST_HELPER::displayResult($result);
?>
<table>
    <tr>
        <th>ID</th>
        <th>Topic</th>
		<th>Status</th>
        <th>&nbsp;</th>
    </tr>
	<form name="frm_add" method="post" action="setup_topic.php"><input type="hidden" name="act" value="add" />
    <tr>
        <th>&nbsp;</th>
        <td><input type=text id=add_text name=add_text maxlength=<?php echo $allowed_topic_string_length; ?> size=40></td>
		<td class="center">New</td>
        <td class="center"><input type=button id="btn_add" value=Add /></td>
    </tr>
	</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$topics = $setupObject->getAllObjectsForSetup();
foreach($topics as $row) {
    $val = $row['value'];
    $valarr = explode(" ",$val);
    $val = implode("_",$valarr);
    $valarr = explode("&#39",$val);
    $val = implode("|",$valarr);
    $class=($row['yn']=="Y"?"":"tr-inactive");
    if($row['yn']=="Y") {
    	$class="";
    	$button = "<input type=button value=Edit class=btn-edit my_id=".$row['id']." />";
	} else {
    	$class="tr-inactive";
    	$button = "<input type=button value=Restore class=btn-restore my_id=".$row['id']." />";
	}
?>
	<tr class="<?php echo $class; ?>">
		<th><?php echo($row['id']); ?></th>
		<td id="<?php echo "td_value_".$row['id']; ?>"><?php echo($row['value']); ?></td>
		<td class="center"><?php echo ($row['yn']=="Y"?"Active":"Inactive"); ?></td>
		<td class="center"><?php echo $button; ?></td>
	</tr>
<?php
}
?>
</table>
<div id="dlg_edit" title="Edit">
	<form name="frm_edit" method="post" action="setup_topic.php">
		<input type="hidden" value="" id="id_field" name="id" />
		<input type="hidden" value="edit" name="act" />
		<table class="form">
			<tr>
				<th>ID:</th>
				<td id="td_id"></td>
			</tr>
			<tr>
				<th>Topic:</th>
				<td><input type="text" value="" size=40 maxlength="<?php echo $allowed_topic_string_length?>" name="edit_text" id="edit_text" /></td>
			</tr>
			<tr>
				<th></th>
				<td><input type="button" value="Save Changes" id="btn_save_edit" class="isubmit" /><span class="float"><input type="button" class=idelete value="Delete" id=btn_delete /></span></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function() {
	$("tr.tr-inactive td").css({"background-color": "#eeeeee","color":"#777777"}).addClass("i");
	$("#btn_add").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var add_text = $('#add_text').val();
		if(add_text.length==0) {
			$("#add_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in the missing info.");
		} else {
			$("form[name=frm_add]").submit();
		}
	});
	$("#dlg_edit").dialog({
		modal:true,
		autoOpen:false,
		width:"500px"
	});
	$(".btn-restore").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		document.location.href = "setup_topic.php?act=restore&id="+i;
	})
	$(".btn-edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		var v = $("#td_value_"+i).html();
		$("#edit_text").val(v);
		$("#td_id").html(i);
		$("#id_field").val(i);
		$("#dlg_edit").dialog("open");
		AssistHelper.closeProcessing();
	});
	$("#btn_save_edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("#edit_text").removeClass("required");
		if($("#edit_text").val().length==0) {
			$("#edit_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in required fields.");
		} else {
			$("form[name=frm_edit]").submit();
		}
	});
	$("#btn_delete").click(function(e) {
		e.preventDefault();
		if(confirm("Are you sure you wish to delete this Topic?")==true) {
			document.location.href = "setup_topic.php?act=delete&id="+$("#id_field").val();
		}
	})
});
</script>
</body>

</html>
