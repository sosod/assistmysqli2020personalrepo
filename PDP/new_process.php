<?php
//error_reporting(E_ALL);
$title = array(
	array('url'=>"new.php",'txt'=>"New"),
);
$page = array("new");
$get_udf_link_headings = false;
require_once("inc_header.php");
?>
<script type="text/javascript" >
	$(function() {
		AssistHelper.processing();
	});
</script>
<?php
$redirect = $_REQUEST['redirect'];
unset($_REQUEST['redirect']);


$attachment = false;
$original_filename = "";
$system_filename = "";

$actionObject = new PDP_ACTION();

//Add main object
$taskid = $actionObject->addObject($_REQUEST,$udf_index);
//Process attachments
        if(isset($_FILES)) {
            $folder = "../files/$cmpcode/".$modref."/action";
            $actionObject->checkFolder($modref."/action");
            $next_docid = $actionObject->getNextAttachmentID();
            foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                if(strlen($tmp_name)>0 && $_FILES['attachments']['error'][$key] == 0) {
					$original_filename = $_FILES['attachments']['name'][$key];
					$o = explode(".",$original_filename);
					$ext = count($o)>1?$o[count($o)-1]:"";
					$system_filename = $taskid."_".$next_docid."_".date("YmdHis").(strlen($ext)>0?".$ext":"");
					$full_path = $folder."/".$system_filename;
					move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
					$actionObject->addObjectAttachment($taskid,$original_filename,$system_filename);
					$next_docid++;
                }
            }
        }





        echo("<form name=nw><input type=hidden name=tsk value=Y></form>");
        ?>
        <script language=JavaScript>
        <?php 
        if($redirect=="dashboard") {
        	echo "
        	$(function() {
        		var result = 'ok';
				var msg = 'New ".$actname." ".$taskid." successfully created.';
        		AssistHelper.closeProcessing();
				$('<div />',{id:'dlg_msg2',html:AssistHelper.getHTMLResult(result,msg,'')}).dialog({
					modal:true,
					closeOnEscape:false,
					buttons:[{ text: 'Ok', click: function() {
						//$('#dlg_msg2').parent.parent.header.$('#backHome').trigger('click');
						//window.header.$('#backHome').trigger('click');
						//redirectMeHome();
						parent.header.$('#backHome').trigger('click');
					}}]
				});
				AssistHelper.hideDialogTitlebar('id','dlg_msg2');        		
			});
			function redirectMeHome() {
				parent.header.$('#backHome').trigger('click');
			}";
        } else {
        ?>
            var ntask = document.nw.tsk.value;
            if(ntask == "Y")
            {
                <?php echo "document.location.href = \"new.php?r[]=ok&r[]=New+".$actname."+".$taskid."+successfully+created.\";"; ?>
            }
        <?php 
        }
        ?>
        </script>
    </body>

</html>
