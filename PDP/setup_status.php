<?php
$page = array("setup","status");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"Status"),
);

include("inc_header.php");

$setupObject = new PDP_SETUP_LISTS("status");

$result = array();

if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
		case "add":
			$var = array(
					'value'=>ASSIST_HELPER::code($_REQUEST['add_text']),
					'sort'=>99,
					'state'=>$_REQUEST['add_state'],
					'heading'=>ASSIST_HELPER::code($_REQUEST['add_text']),
					'custom'=>"Y",
			);
			$result = $setupObject->addObject($var);
			break;
		case "edit":
			$var = array(
					'value'=>ASSIST_HELPER::code($_REQUEST['edit_text']),
					'state'=>$_REQUEST['edit_state'],
					'heading'=>ASSIST_HELPER::code($_REQUEST['edit_text']),
			);
			$id = $_REQUEST['id'];
			$result = $setupObject->editObject($id,$var);
			break;
		case "delete":
			$id = $_REQUEST['id'];
			$result = $setupObject->deleteObject($id);
			break;
		case "restore":
			$id = $_REQUEST['id'];
			$result = $setupObject->restoreObject($id);
			break;
		case "sort":
			$result = $setupObject->sortObjects($_REQUEST);
			break;
	}
}


	$allowed_string_length = 45;

ASSIST_HELPER::displayResult($result);
?>
<table>
    <tr>
        <th>ID</th>
        <th>Status Name</th>
        <th>Default %</th>
		<th>Associated <?php echo $actname; ?></th>
		<th>Status</th>
        <th><input type="button" value="Sort" id="btn_sort" /></th>
    </tr>
	<form name="frm_add" method="post" action="setup_status.php"><input type="hidden" name="act" value="add" />
    <tr>
        <th>&nbsp;</th>
        <td><input type=text id=add_text name=add_text maxlength=<?php echo $allowed_string_length; ?> size=40></td>
        <td class="center"><input type=text id=add_state name=add_state maxlength=2 size=5>%</td>
		<td class="center">-</td>
		<td class="center">New</td>
        <td class="center"><input type=button id="btn_add" value=Add /></td>
    </tr>
	</form>
<?php
$objects = $setupObject->getAllObjectsForSetup();
foreach($objects as $row) {
	if(!($row['state']<0)) {
		$class=($row['yn']=="Y"?"":"tr-inactive");
		if($row['yn']=="Y") {
			$class="";
			$button = "<input type=button value=Edit class=btn-edit my_id=".$row['id']." />";
		} else {
			$class="tr-inactive";
			$button = "<input type=button value=Restore class=btn-restore my_id=".$row['id']." />";
		}
?>
		<tr class="<?php echo $class; ?>">
			<th><?php echo($row['id']); ?></th>
			<td id="<?php echo "td_value_".$row['id']; ?>"><?php echo($row['value']); ?></td>
			<td class="center" id="<?php echo "td_state_".$row['id']; ?>"><?php echo($row['state']); ?></td>
			<td class="center"><?php echo $row['count']; ?></td>
			<td class="center"><?php echo ($row['yn']=="Y"?"Active":"Inactive"); ?></td>
			<td class="center"><?php echo $button; ?></td>
		</tr>
<?php
	}
}
?>
</table>
<div id="dlg_edit" title="Edit">
	<form name="frm_edit" method="post" action="setup_status.php">
		<input type="hidden" value="" id="id_field" name="id" />
		<input type="hidden" value="edit" name="act" />
		<table class="form">
			<tr>
				<th>ID:</th>
				<td id="td_id"></td>
			</tr>
			<tr>
				<th>Status Name:</th>
				<td><input type="text" value="" size=40 maxlength="<?php echo $allowed_string_length?>" name="edit_text" id="edit_text" /></td>
			</tr>
			<tr>
				<th>Default %</th>
				<td><input type="text" value="" size=5 maxlength="2" name="edit_state" id="edit_state" /></td>
			</tr>
			<tr>
				<th></th>
				<td><input type="button" value="Save Changes" id="btn_save_edit" class="isubmit" /><span class="float"><input type="button" class=idelete value="Delete" id=btn_delete /></span></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function() {
	$("tr.tr-inactive td").css({"background-color": "#eeeeee","color":"#777777"}).addClass("i");
	$("#btn_add").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var add_text = $('#add_text').val();
		if(add_text.length==0) {
			$("#add_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in the missing info.");
		} else {
			$("form[name=frm_add]").submit();
		}
	});
	$("#dlg_edit").dialog({
		modal:true,
		autoOpen:false,
		width:"500px"
	});
	$(".btn-restore").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		document.location.href = "setup_status.php?act=restore&id="+i;
	})
	$(".btn-edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		var v = $("#td_value_"+i).html();
		var s = $("#td_state_"+i).html();
		$("#edit_text").val(v);
		$("#edit_state").val(s);
		$("#td_id").html(i);
		$("#id_field").val(i);
		$("#dlg_edit").dialog("open");
		AssistHelper.closeProcessing();
	});
	$("#btn_save_edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("#edit_text").removeClass("required");
		if($("#edit_text").val().length==0) {
			$("#edit_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in required fields.");
		} else {
			$("form[name=frm_edit]").submit();
		}
	});
	$("#btn_delete").click(function(e) {
		e.preventDefault();
		if(confirm("Are you sure you wish to delete this Status?")==true) {
			document.location.href = "setup_status.php?act=delete&id="+$("#id_field").val();
		}
	});
	$("#btn_sort").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		document.location.href = "setup_status_sort.php";
	})
});
</script>
</body>

</html>
