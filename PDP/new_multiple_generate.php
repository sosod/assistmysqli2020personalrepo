<?php
//$title = array(
//	array('url'=>"new.php",'txt'=>"New"),
//	array('url'=>"new_multiple.php",'txt'=>"Import"),
//);
//$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new","multiple","generate");
$get_udf_link_headings = false;
require_once("inc_header.php");



$fdata = "";
$fdata2 = "";
//HEADINGS
$headings = $ah->getFullHeadings();
foreach($headings as $row) {
	$heading[$row['field']] = $row;
	$fdata.= strlen($fdata)>0 ? "," : "";
	$fdata2.= strlen($fdata)>0 ? "," : "";
	switch($row['field']) {
		case "tasktopicid":
			$fdata .="\"*".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"Max: 40 characters.\"";
			break;
		case "taskaction":
			$fdata .="\"*".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"\"";
			break;
		case "taskstatusid":
			$fdata .="\"".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"New or On-going. Default: New\"";
			break;
		case "taskdeadline":
			$fdata .="\"*".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"DD-MM-YYYY\"";
			break;
		case "taskurgencyid":
			$fdata .="\"".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"High, Normal, Low. Default: Normal.\"";
			break;
		case "taskadduser":
			$fdata .="\"".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"Name as per User list. Default: User logged in.\"";
			break;
		case "tasktkid":
			$fdata .="\"*".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"Name as per User list. Use ; to separate multiple users.\"";
			break;
		default:
			$fdata .="\"".ASSIST_HELPER::decode($row['headingfull'])."\"";
			$fdata2.="\"\"";
			break;
	}
	
}
unset($headings);
$udf_index = $ah->getUDFDetails();
foreach($udf_index as $row) {
			$fdata .=",\"".ASSIST_HELPER::decode($row['udf_value'])."\"";
			$fdata2.=",";
	switch($row['list_type']) {
		case "Y":
			$fdata2.="\"\"";
			break;
		case "D":
			$fdata2.="\"DD-MM-YYYY\"";
			break;
		case "T":
			$fdata2.="\"Max: 200 characters\"";
			break;
		case "M":
			$fdata2.="\"\"";
			break;
		case "N":
			$fdata2.="\"Numbers only\"";
			break;
	}
}

$fdata.= "\r\n";
//FORMATTING GUIDELINES
$fdata.=$fdata2."\r\n";

        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi").".csv";
        $newfilename = "template.csv";
		$ah->checkFolder("");
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);
	?>