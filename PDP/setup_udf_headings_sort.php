<?php
$title = array(
    array('url'=>"setup.php",'txt'=>"Setup"),
    array('url'=>"setup.php",'txt'=>"UDF"),
    array('txt'=>"Sort"),
);
$page = array("setup","udf");
$get_udf_link_headings = true;
require_once 'inc_header.php';

$displayObject = new ASSIST_MODULE_DISPLAY();

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();


$display = array();
$d = 0;
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '$modref' ORDER BY udfisort";
$these_rows = $ah->mysql_fetch_all($sql);

//echo '<pre style="font-size: 18px">';
//echo '<p>$these_rows</p>';
//print_r($these_rows);
//echo '</pre>';

foreach($these_rows as $row_index => $row)
{
    $display[$d] = $row;
    $d++;
}

$items = array();
foreach($display as $dy){
	$items[$dy['udfiid']] = $dy['udfivalue'];
}

//echo '<pre style="font-size: 18px">';
//echo '<p>$display</p>';
//print_r($display);
//echo '</pre>';


if(count($items)==0) {
	ASSIST_HELPER::displayResult(array("error","No items found to sort."));
	die();
}
?>
<h2>UDF Order</h2>
<table class='tbl-container not-max'><tr><td>
	<div style='border: 1px solid #999999; border-radius:5px;padding-right:50px'>
	<form name=frm_sort>
<!--        <input type=hidden name=section value='--><?php //echo $section; ?><!--' />-->
<!--        <input type=hidden name=page_type value='--><?php //echo $page_type; ?><!--' />-->
		<ul id=ul_sortable>
		<?php
		foreach($items as $key => $i) {
			echo "<li class=ui-state-default style='background:url(); background-color:#FFFFFF'><img src=/pics/sort_icon.png />&nbsp;".$i."<input type=hidden name=order[] value='".$key."' /></li>";
		}
		?>
		</ul>
	</form>
	</div>
</td></tr>
<Tr><td class=center>
	<button id=btn_save>Save Display Order</button>
</td></Tr>
<tr><td>
	<?php //$js.= $displayObject->drawPageFooter($helper->getGoBack($_SERVER['HTTP_REFERER'])); ?>
</td></tr>
</table>
<style>

 #ul_sortable { list-style-image: url(); list-style-type:none; margin: 0; padding: 0; width:100% }
  #ul_sortable li { padding: 10px; margin: 7px; height:20px ; width:100%}

  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>
<script type="text/javascript">
$(function() {
	$("#ul_sortable").sortable({
	}).disableSelection();

	$("#btn_save").button({
		icon:{primary:"ui-icon-disk"}
	}).removeClass("ui-state-default").addClass("ui-button-state-green")
	.hover(function() {
		$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");
	},function() {
		$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var data_string = "displ=";
        var order_vals = "";

        $("form[name=frm_sort]").find('input[name="order[]"]').each(function() {
            order_vals += $(this).val() + ";";
        });

        var last_char_to_remove = ";";
        order_vals = order_vals.substring(0,order_vals.length-1) // removes the last character.

        var dta = data_string + order_vals;

		console.log('****** Order Vars ******');
		console.log(order_vals);

        document.location.href = "setup_udf_headings_sort_process.php?" + dta;
		//var result = AssistHelper.doAjax("inc_controller.php?action=HEADINGS.SORT",dta);
		//if(result[0]=="ok") {
		//	AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"<?php //echo $_SERVER['HTTP_REFERER']; ?>//");
		//} else {
		//	AssistHelper.finishedProcessing(result[0],result[1]);
		//}

	});


	var width = 0;
	$("#ul_sortable li:first").each(function() {
		width = ($(this).width());
	});
	$("#ul_sortable li").closest("table").width(width+75);
	$("#ul_sortable li").closest("div").width(width+50);

});
</script>