<?php
$page = array("setup","status","sort");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup_status.php",'txt'=>"Status"),
	array('txt'=>"Display Order"),
);

include("inc_header.php");

$setupObject = new PDP_SETUP_LISTS("status");
$objects = $setupObject->getAllObjectsForSetup();
unset($objects[4]);//remove NEW as it is auto sorted
unset($objects[2]);//remove CANCELLED
unset($objects[1]);//remove COMPLETED

ASSIST_HELPER::displayResult(array("info","New & Completed are auto-sorted to the beginning and end of the Status list and are not available for manual sorting on this page."));
echo "
<form name=sort action=setup_status.php method=post>
	<input type='hidden' name='act' value='sort' />
	<div style=\"padding-left: 20px; padding-top: 10px; padding-bottom: 10px;\">
		<ul id=sortable>";
	foreach($objects as $i => $row) {
		echo "
		<li class=\"ui-state-default ui-sort\" style=\"cursor:hand; border-color: #999999; background: #fefefe url(); color: #000000;\">
			<span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$row['id']."\">".$row['value']."
		</li>";
	}
	echo "
		</ul>
	</div>
	<p><input type=submit value=\"Save Changes\" class=isubmit /> <input type='button' value='Reset' id='btn_reset' /></p>
</form>";

?>
<script type="text/javascript">
	$(function(){
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlightsort"
		});
		$( "#sortable" ).disableSelection();
		$("#btn_reset").click(function(e) {
			e.preventDefault();
			document.location.href = document.location.href;
		})
	});
</script>
</body>

</html>
