//PRIMARY AJAX FUNCTION
function ajax_setupUDFListItem(form,act,my_post) {
	$.ajax({                                      
		url: 'lib/ajax_setup_udf_list.php', 		  type: 'POST',		  data: my_post,		  dataType: 'json', 
		success: function(data) {
			switch(act) {
			case "pop_editform":
				$(form+" input:text[name=udfvvalue]").val(data['value']);
				$("#dlg_edit").dialog("open");
				break;
			case "ADD": 
				var msg = escape("New List Item '"+data['value']+"' (Ref: "+data['id']+") has been created successfully."); 
				document.location.href = 'setup_udf_list.php?id='+data['index']+'&r[]=ok&r[]='+msg; 
				break;
			case "EDIT": 
				if(data['mar']>0) {
					var msg = escape("List Item '"+data['value']+"' (Ref: "+data['id']+") has been updated successfully."); 
					document.location.href = 'setup_udf_list.php?id='+data['index']+'&r[]=ok&r[]='+msg; 
				} else if(data['mar']==0) {
					$("#dlg_edit").dialog("close");
					JSdisplayResult("info","info","No change was found to be saved.");
				} else {
					var msg = "An error occurred while trying to save the changes to the List Item.  Please try again."; 
					//document.location.href = 'setup_udf_list.php?id='+data['index']+'&r[]=error&r[]='+msg; 
					JSdisplayResult("error","error",msg);
				}
				break;
			case "DELETE": 
				if(data['mar']>0) {
					var msg = escape("List Item '"+data['value']+"' (Ref: "+data['id']+") has been successfully deleted."); 
					document.location.href = 'setup_udf_list.php?id='+data['index']+'&r[]=ok&r[]='+msg; 
				} else if(data['mar']==0) {
					JSdisplayResult("info","info","List Item could not be found to be deleted.");
					$("#dlg_edit").dialog("close");
				} else {
					var msg = "An error occurred while trying to delete the List Item.  Please try again."; 
					JSdisplayResult("error","error",msg);
					//document.location.href = 'setup_udf_list.php?id='+data['index']+'&r[]=error&r[]='+msg; 
				}
				break;
			default:
			}
		}
	});
}

