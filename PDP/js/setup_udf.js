//FUNCTION TO SET OPTIONS IN UDFILINKREF SELECT
function pop_udfilinkref(form,data) {
				for(i in data) {
					$(form+" select[name=udfilinkref]").append("<option value="+data[i]['id']+">"+data[i]['value']+"</option>");
				}
}

//FUNCTION TO SET EDIT UDF FORM
function pop_editudf(form,data) {
	//for(x in data) { alert(x+" :: "+data[x]); }
	var ol = data['udfiobject'];
	if(data['udfilinkfield'].length>0) {
		ol = ol+"_"+data['udfilinkfield'];
	}
	$(form+" select[name=object_link]").val(ol);
	$(form+" select[name=object_link]").trigger("change");
	$(form+" select[name=object_link] option").each(function() {
		$(this).attr("disabled",!$(this).hasClass(data['udfiobject']));
	});
	
	$(form+" input:text[name=udfivalue]").val(data['udfivalue']);
	$(form+" select[name=udfilist]").val(data['udfilist']);
	$(form+" select[name=udfilist]").trigger("change");
	$(form+" select[name=udfirequired]").val(data['udfirequired']);
	if(data['udfidefault'].length>0) {
		$(form+" input:text[name=udfidefault]").val(data['udfidefault']);
	}
	for(i = 0; i < 999999; i++);
	//alert(i);
	if(data['udfilinkfield'].length>0) {
		if(data['udfilinkref'].length>0 && data['udfilinkref']!=0) {
			//alert(data['udfilinkref']+" :: "+$(form+" select[name=udfilinkref] option[value="+data['udfilinkref']+"]").length);
			$(form+" select[name=udfilinkref]").val(data['udfilinkref']);
		} else {
			$(form+" select[name=udfilinkref]").addClass("required");
		}
	}
}


//FUNCTION TO VALIDATE A NEW UDF
function validateForm(form,act) {
	$(form+" input:text").removeClass();
	$(form+" select").removeClass();
	var udfiid = $(form+" input:hidden[name=udfiid]").val();
	var udfivalue = $(form+" input:text[name=udfivalue]").val();
	var udfilist = $(form+" select[name=udfilist]").val();
	var object_link = $(form+" select[name=object_link]").val();
	var udfilinkref = $(form+" select[name=udfilinkref]").val();
	//var udfirequired = $(form+" select[name=udfirequired]").val();
	var udfirequired = "0";
	//var udfidefault = $(form+" input:text[name=udfidefault]").val();
	var udfidefault = "";
	var res = new Array("ok","test done",true);

	if(udfivalue.length==0) {
		$(form+" input:text[name=udfivalue]").addClass("required");
		res = resError("incomplete");
	}
	if(udfilist.length==0) {
		$(form+" select[name=udfilist]").addClass("required");
		res = resError("incomplete");
	} else {
		if(udfilist=="N" && udfidefault.length>0 && !(!isNaN(parseFloat(udfidefault)) && isFinite(udfidefault))) {
			$(form+" select[name=udfidefault]").addClass("required");
			res = resError("incomplete");
		}
	}
	if(object_link.length==0) {
		$(form+" select[name=object_link]").addClass("required");
		res = resError("incomplete");
	} else {
		//alert("ol ok");
		if(object_link=="action" || object_link =="update") {
			var udfiobject = object_link;
			var udfilinkfield = "";
				udfilinkref = 0;
		} else {
			//alert(object_link);
			if(udfilinkref.length==0) {
				$(form+" select[name=udfilinkref]").addClass("required");
				res = resError("incomplete");
			} else {
				//alert(object_link);
				var ol = object_link.split("_");
				//alert(ol[0]);
				var udfiobject = ol[0];
				var udfilinkfield = ol[1];
			}
		}
	}
	if(udfirequired.length==0) {
		$(form+" select[name=udfirequired]").addClass("required");
		res = resError("incomplete");
	}
	//alert(res[2]);
	if(res[2]==true) {
		var post_data = "act="+act;
		post_data = post_data+"&udfiid="+udfiid;
		post_data = post_data+"&udfivalue="+escape(udfivalue);
		post_data = post_data+"&udfilist="+udfilist;
		post_data = post_data+"&udfiobject="+udfiobject;
		post_data = post_data+"&udfilinkfield="+udfilinkfield;
		post_data = post_data+"&udfilinkref="+udfilinkref;
		post_data = post_data+"&udfirequired="+udfirequired;
		post_data = post_data+"&udfidefault="+udfidefault;
		//alert(post_data);
		ajax_setupUDF(form,act,post_data);
		res = new Array();
	}
	
	return res;
}

function resError(type) {
	var res = new Array();
	switch(type) {
	case "incomplete":
		res[0] = "error";
		res[1] = "Please complete the highlighted fields.";
		res[2] = false;
		break;
	}
	return res;
}
















//PRIMARY AJAX FUNCTION
function ajax_setupUDF(form,act,my_post) {
	//alert(my_post);
	$.ajax({                                      
		url: 'lib/ajax_setup_udf.php', 		  type: 'POST',		  data: my_post,		  dataType: 'json', 
		success: function(data) {
			//alert(data['id']);
			switch(act) {
			case "pop_udfilinkref": pop_udfilinkref(form,data);	break;
			case "pop_editudf": 
				pop_editudf(form,data);	
				break;
			case "ADD": var msg = escape("New UDF '"+data['value']+"' (Ref: "+data['id']+") has been created successfully."); document.location.href = 'setup_udf.php?r[]=ok&r[]='+msg; break;
			case "EDIT": 
				if(data['mar']>0) {
					var msg = escape("UDF '"+data['value']+"' (Ref: "+data['id']+") has been updated successfully."); 
					document.location.href = 'setup_udf.php?r[]=ok&r[]='+msg; 
				} else if(data['mar']==0) {
					JSdisplayResult("info","info","No change was found to be saved.");
					$("#dlg_edit").dialog("close");
				} else {
					var msg = "An error occurred while trying to save the changes to the UDF.  Please try again."; 
					document.location.href = 'setup_udf.php?r[]=error&r[]='+msg; 
				}
				break;
			case "DELETE": 
				if(data['mar']>0) {
					var msg = escape("UDF '"+data['value']+"' (Ref: "+data['id']+") has been successfully deleted."); 
					document.location.href = 'setup_udf.php?r[]=ok&r[]='+msg; 
				} else if(data['mar']==0) {
					JSdisplayResult("info","info","UDF could not be found to be deleted.");
					$("#dlg_edit").dialog("close");
				} else {
					var msg = "An error occurred while trying to delete the UDF.  Please try again."; 
					document.location.href = 'setup_udf.php?r[]=error&r[]='+msg; 
				}
				break;
			}
		}
	});
}

