<?php

$title = array(
	array('url'=>"new.php",'txt'=>"New"),
	array('url'=>"new_multiple.php",'txt'=>"Import"),
	array('url'=>"new_multiple.php",'txt'=>"Validate"),
);
$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new","multiple","import");
$get_udf_link_headings = false;
require_once("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
$today = time();

function validateDate($d) {	//EXPECTED FORMAT DD-MM-YYYY
						if(strpos($d,"-")>0 || strpos($d,"/")>0) {
							if(strpos($d,"/")>0) {
								$d2 = explode("/",$d);//strFn("explode",$d,"/","");
							} else {
								$d2 = explode("-",$d);//strFn("explode",$d,"-","");
							}
							if(count($d2)==3) {
								if(ASSIST_HELPER::checkIntRef($d2[0]) && ASSIST_HELPER::checkIntRef($d2[1]) && ASSIST_HELPER::checkIntRef($d2[2])) {	//is numeric & > 0
									if($d2[1]<=12) {	//validate month
										if($d2[0]<=31) {	//validate day
											$d = date("d-m-Y",mktime(12,0,0,$d2[1],$d2[0],$d2[2]));
										} elseif($d2[2]<=31) {	//swop day and year
											$d = date("d-m-Y",mktime(12,0,0,$d2[1],$d2[2],$d2[0]));
										} else {
											$d = "";
										}
									} else {	
										$d = "";
									}
								}
							} else {
								$d = "";
							}
						} else {
							$d = "";
						}
		return $d;
}
function validateNum($d) {
	if(strpos($d,",")>0) { $d = str_replace(",","",$d); }//strFn("str_replace",$d,",",""); }
	if(strpos($d," ")>0) { $d = str_replace(" ","",$d); }//strFn("str_replace",$d," ",""); }
	return $d;
}
?>

        <script type ="text/javascript">
            $(function() {
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                })
            });
            $(function() {
                $('.dateme').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                })
            });

        </script>
<style type=text/css>
.required { color: #ffffff; font-weight: bold; background-color: #cc0001; }
.met { color: #000000; font-weight: normal; background-color: #ffffff; }
.disabled { background-color: #777777; }
.enabled { background-color: #009900; }
.notfound { background-color: #ffffff; color: #CC0001; text-decoration: underline; font-weight: bold; }
</style>
<script type=text/javascript>
function Validate(me) {
	return false;
}
function Verify(me,t) {
	//alert(t);
	switch(t) {
		case "txt":
			if(me.value.length>0) 
				me.className = 'met';
			else
				me.className = 'required';
			break;
		case "d":
			if(me.value.length>0) 
				me.className = 'date-type met';
			else
				me.className = 'date-type required';
			break;
		case "sel":
			if(me.value.length>0 && me.value != "X") 
				me.className = 'met';
			else
				me.className = 'required';
			break;
		case "tk":
			if(me.selectedIndex < 0) {
				me.className = 'required';
			} else {
				me.className = 'met';
				for(i=0;i<me.options.length;i++) {
					me.options[i].className = '';
				}
			}
			break;
	}
}
</script>
<?php
	echo "<p>Any required fields which are incomplete, will be highlighted in <span class=required>RED</span>.  If you click the Accept button while a required field is incomplete, the $actname will not be created.</p>";
	echo "<form name=import id=imp action=new_multiple_import_process.php method=post>";
	echo "<input type=hidden name=act value=SAVE>";
/**************** IMPORT FILE **************************/
if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
		switch($_FILES["ifile"]["error"])
		{
			case 2:
				echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
				break;
			case 4:
				echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
				break;
			default:
				echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
				break;
		}
		die();
} else { //ELSE IF NO ERROR WITH UPLOAD FILE
		$ext = substr($_FILES['ifile']['name'],-3,3);
		if(strtolower($ext)!="csv") {	//CHECK EXTENSION TYPE
			die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
		} else {
			$filename = $modref."-IMPORT_".substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
			$fileloc = "../files/".$cmpcode."/".$filename;
			//UPLOAD UPLOADED FILE
			set_time_limit(180);
			copy($_FILES["ifile"]["tmp_name"], $fileloc);
		}
}

if(!file_exists($fileloc)) {
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
}

/******************* GET LISTS ****************************/
$checks = array();
$lists = array();

$lists['tasktkid'] = $ah->getAllAvailableUsersToReceiveTasksFormattedForSelect();
$lists['taskadduser'] = $ah->getAllAvailableUsersToAddTasksFormattedForSelect();
$lists['taskurgencyid'] = $ah->getAllActiveUrgencyFormattedForSelect();
$lists['tasktopicid'] = $ah->getAllActiveTopicsFormattedForSelect();

$udfs = $ah->getAllUDFListItems();
foreach($udfs as $vi => $udf) {
	$lists[$vi] = $udf;
}





foreach($lists as $field => $list_items) {
	foreach($list_items as $id => $item) {
		$checks[$field][$ah->formatTextForComparison($item)] = $id;
	}
}


/*** HEADINGS ***/
$heading = $ah->getFullHeadings();
$udf_details = $ah->getUDFDetails();
foreach($udf_details as $udf) {
	$udf['type'] = "U";
	$heading[$udf['id']] = $udf;
}


/****************** GET DATA FROM FILE ***********************/
//GET DATA FROM FILE
set_time_limit(180);
$file = fopen($fileloc,"r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($file);
unset($data[0]);	//remove row 1 = heading
unset($data[1]);	//remove row 2 = formatting guidelines

/****************** DISPLAY DATA ****************************/
echo "<form name=import id=imp action=new_multiple_import_process.php method=post>";
echo "<input type=hidden name=act value=SAVE>";
echo "<table cellpadding=3 cellspacing=0>";
//HEADINGS
	echo "<tr>";
	foreach($heading as $h) {
		echo "<th>";
		switch($h['type']) {
			case "U": echo $h['udf_value']; break;
			default: echo $h['headingfull'];
		}
		echo "</th>";
	}
	echo "</tr>";
//DATA
$count = 0;
foreach($data as $dta) {
	$count++;
	echo "<tr>";
	$csv = 0;
	foreach($heading as $h) {
		$ht = $h['type'];
		$d = $dta[$csv]; $csv++;
		echo "<td>";
		switch($ht) {
			case "U":	//UDF
				$hf = $h['id'];
				switch($h['list_type']) {
					case "Y":
						$l = $lists[$hf];
						$c = $checks[$hf];
						if(strlen($d)>0) {
							$chk = $ah->formatTextForComparison($d);
							if(isset($c[$chk])) { $cid = $c[$chk]; } else { $cid = "X"; }
						} else {
							$cid = "X";
						}
						echo "<select name=".$hf."[]><option value=X"; if($cid == "X") { echo " selected"; } echo ">--- SELECT ---</option>";
						foreach($l as $id => $m) {
								echo "<option value=".$id." ".($cid==$id?"selected":"").">".$m."</option>";
						}
						echo "</select>";
						if($cid == "X" && strlen($d)>0) { echo "<br /><span class=notfound>Value not found:</span><br />&nbsp;&nbsp;$d"; }
						break;
					case "D":
						$d = validateDate($d);
						echo "<input type='text' size='10' value='$d' name='".$hf."[]' class='dateme'>";
						break;
					case "T":
						echo "<input type=text size=25 value=\"$d\" name=\"".$hf."[]\">";
						break;
					case "M":
						echo "<textarea name=\"".$hf."[]\" rows=3 cols=20>$d</textarea>";
						break;
					case "N":
						$d = validateNum($d);
						echo "<input type=text size=15 name=\"".$hf."[]\" value=\"$d\"><br /><span style=\"font-size: 6.5pt; line-height: 7pt; font-style: italic;\">Numbers only</span>";
						break;
				}
				break;
			default:
				$hf = $h['field'];
				switch($hf) {
					case "tasktopicid":
						$l = $lists[$hf];
						$c = $checks[$hf];
						$chk = $ah->formatTextForComparison($d);
						if(isset($c[$chk])) {
							$cid = $c[$chk];
						} else {
							$cid = "X";
						}
						echo "<select name=".$hf."[] id=".$hf.$count." onchange=\"Verify(this,'sel');\"><option value=X ".($cid=="X"?"selected":"").">--- SELECT ---</option>";
							foreach($l as $id => $m) {
								echo "<option value=".$id." ".($cid == $id?"selected":"").">".$m."</option>";
							}
						echo "</select>";
						if($cid == "X") { 
							$error[] = array('sel',$hf.$count); 
							if(strlen($chk)>0) {
								echo "<br /><span class=notfound>Value not found:</span><br />&nbsp;&nbsp;".$d;
							}
						}
						break;
					case "taskurgencyid":
						if(strlen($d)==0) { $d = "Normal"; }
						$l = $lists[$hf];
						$c = $checks[$hf];
						$chk = $ah->formatTextForComparison($d);
						if(isset($c[$chk])) {
							$cid = $c[$chk];
						} else {
							$cid = "X";
						}
						echo "<select name=".$hf."[] id=".$hf.$count." onchange=\"Verify(this,'sel');\"><option value=X"; if($cid == "X") { echo " selected"; } echo ">--- SELECT ---</option>";
							foreach($l as $id => $m) {
								echo "<option value=".$id." ".($cid==$id?"selected":"").">".$m."</option>";
							}
						echo "</select>";
						if($cid == "X") { $error[] = array('sel',$hf.$count); }
						break;
					case "taskaction":
						echo "<textarea name=".$hf."[] rows=3 cols=25 ";
						if(strlen($d)==0) { echo " class=required"; }
						echo " onchange=\"Verify(this,'txt');\">".$d."</textarea>";
						break;
					case "taskstatusid":
						if(strtolower(ASSIST_HELPER::decode($d))=="on-going" || strtolower(ASSIST_HELPER::decode($d))=="ongoing" || strtolower(ASSIST_HELPER::decode($d)) == "on going") {
							echo "New <input type=hidden size=5 value=5 name=".$hf."[]>";
						} else {
							echo "New <input type=hidden size=5 value=4 name=".$hf."[]>";
						}
						break;
					case "taskdeadline":	//EXPECTED FORMAT DD-MM-YYYY
						$d = validateDate($d);
						echo "<input type='text' name='".$hf."[]' size='10' id=\"".$hf.$count."\" readonly='readonly' value=\"$d\"/ class='date-type' onchange=\"Verify(this,'d');\">";
						if(strlen($d)==0) { $error[] = array('d',$hf.$count); }
						break;
					case "taskadddate":
						echo date("d-M-Y"); $csv--; $d = "";
						break;
					case "taskadduser":
						if(strlen($d)>0) {
							$c = $checks[$hf];
							$chk = $ah->formatTextForComparison($d);
							if(isset($c[$chk])) { $cid = $c[$chk]; } else { $cid = "X"; }
						} else {
							$cid = $tkid;
						}
						echo "<select name=".$hf."[] onchange=\"Verify(this,'sel');\" id=\"".$hf.$count."\" ><option value=X"; if($cid == "X") { echo " selected"; } echo ">--- SELECT ---</option>";
						foreach($lists['taskadduser'] as $id => $m) {
								echo "<option value=".$id;
								if($cid == $id) { echo " selected"; }
								echo ">".$m."</option>";
						}
						echo "</select>";
						if($cid == "X") { echo "<br /><span class=notfound>User&nbsp;not&nbsp;identified:</span><br />".$d; $error[] = array('sel',$hf.$count); }
						break;
					case "tasktkid":	//task recipient
						$notfound = array();
						if(strlen($d)>0) {
							$c = $checks[$hf];
							$usrs = explode(";",$d);//strFn("explode",$d,";");
							foreach($usrs as $u) {
								$chk = $ah->formatTextForComparison($u);
								if(isset($c[$chk])) { $cid[$c[$chk]] = "Y"; } else { $notfound[] = $u; }
							}
						} else {
							$cid[$tkid] = "Y";
						}
						echo "<select name=".$hf.$count."[] id=\"".$hf.$count."\" multiple size=5 onchange=\"Verify(this,'tk');\">";
						foreach($lists['tasktkid'] as $id => $m) {
								echo "<option value=".$id." ".(isset($cid[$id]) && $cid[$id]=="Y"?"selected":"").">".$m."</option>";
						}
						echo "</select>";
						if(count($notfound)>0) { echo "<br /><span class=notfound>Users&nbsp;not&nbsp;identified:</span>"; }
						foreach($notfound as $nf) {
							echo "<br />".$nf;
						}
						$error[] = array('tk',$hf.$count);
						break;
					default:
						echo "<textarea name=".$hf."[] rows=3 cols=25>".$d."</textarea>";
						break;
				}
				break;
		}
		//RESET VARIABLES
		$d = ""; $chk = ""; $cid = "";
		echo "</td>";
	}
	echo "</tr>";
}	
echo "</table>";
if($count>0) {
		echo "<p style=\"text-align:center;\"><input type=hidden name=count value=$count><input type=submit value=\"  Accept  \" style=\"color: #FFFFFF; padding: 10px;\" id=accept class=enabled></p>";
		echo "<script type=text/javascript>";
		echo "var targ;";
		foreach($error as $e) {
			echo "targ = document.getElementById('".$e[1]."');";
			echo "Verify(targ,'".$e[0]."');";
		}
		echo "</script>";
}
?>
</body>
</html>