<?php
//Page setup
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('url'=>"setup_udf.php",'txt'=>"User Defined Fields (UDFs)"),
	array('txt'=>"Edit List"),
);
$page = array("setup","udf");
$get_udf_link_headings = true;
require_once 'inc_header.php';
$setupObject = new PDP_SETUP_UDF();


//udf index setup
$id = $_REQUEST['id'];
$result = array();
if(!ASSIST_HELPER::checkIntRef($id)) {
	echo "<script type=text/javascript>
			document.location.href = 'setup_udf.php?r[]=error&r[]=An error occurred while trying to access the UDF list.  Please try again.';
		</script>";
}
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiid = ".$id;
$uindex = $setupObject->mysql_fetch_one($sql);
if(count($uindex)==0 || !isset($uindex['udfivalue'])) {
	echo "<script type=text/javascript>
			document.location.href = 'setup_udf.php?r[]=error&r[]=An error occurred while trying to access the UDF ".$id.".  Please try again.';
		</script>";
}
echo "<h2>".$uindex['udfivalue']."</h2>";

//action setup
if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
		case "add":
			$var = array('value'=>ASSIST_HELPER::code($_REQUEST['add_text']),'index'=>$id);
			$result = $setupObject->addListObject($var);
			break;
		case "edit":
			$var = array('value'=>ASSIST_HELPER::code($_REQUEST['edit_text']));
			$vid = $_REQUEST['vid'];
			$result = $setupObject->editListObject($vid,$var);
			break;
		case "delete":
			$vid = $_REQUEST['vid'];
			$result = $setupObject->deleteListObject($vid);
			break;
		case "restore":
			$vid = $_REQUEST['vid'];
			$result = $setupObject->restoreListObject($vid);
			break;
	}
}






	$allowed_string_length = 45;

ASSIST_HELPER::displayResult($result);
?>
<table>
    <tr>
        <th>ID</th>
        <th>List Item</th>
		<th>Status</th>
        <th>&nbsp;</th>
    </tr>
	<form name="frm_add" method="post" action="setup_udf_list.php">
		<input type="hidden" name="act" value="add" />
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
    <tr>
        <th>&nbsp;</th>
        <td><input type=text id=add_text name=add_text maxlength=<?php echo $allowed_string_length; ?> size=40></td>
		<td class="center">New</td>
        <td class="center"><input type=button id="btn_add" value=Add /></td>
    </tr>
	</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$objects = $setupObject->getAllListObjectsForSetup($id);
foreach($objects as $row) {
    $class=($row['yn']=="Y"?"":"tr-inactive");
    if($row['yn']=="Y") {
    	$class="";
    	$button = "<input type=button value=Edit class=btn-edit my_id=".$row['id']." />";
	} else {
    	$class="tr-inactive";
    	$button = "<input type=button value=Restore class=btn-restore my_id=".$row['id']." />";
	}
?>
	<tr class="<?php echo $class; ?>">
		<th><?php echo($row['id']); ?></th>
		<td id="<?php echo "td_value_".$row['id']; ?>"><?php echo($row['value']); ?></td>
		<td class="center"><?php echo ($row['yn']=="Y"?"Active":"Inactive"); ?></td>
		<td class="center"><?php echo $button; ?></td>
	</tr>
<?php
}
?>
</table>
<div id="dlg_edit" title="Edit">
	<form name="frm_edit" method="post" action="setup_udf_list.php">
		<input type="hidden" value="" id="id_field" name="vid" />
		<input type="hidden" value="edit" name="act" />
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<table class="form">
			<tr>
				<th>ID:</th>
				<td id="td_id"></td>
			</tr>
			<tr>
				<th>List Item:</th>
				<td><input type="text" value="" size=40 maxlength="<?php echo $allowed_string_length?>" name="edit_text" id="edit_text" /></td>
			</tr>
			<tr>
				<th></th>
				<td><input type="button" value="Save Changes" id="btn_save_edit" class="isubmit" /><span class="float"><input type="button" class=idelete value="Delete" id=btn_delete /></span></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function() {
	$("tr.tr-inactive td").css({"background-color": "#eeeeee","color":"#777777"}).addClass("i");
	$("#btn_add").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var add_text = $('#add_text').val();
		if(add_text.length==0) {
			$("#add_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in the missing info.");
		} else {
			$("form[name=frm_add]").submit();
		}
	});
	$("#dlg_edit").dialog({
		modal:true,
		autoOpen:false,
		width:"500px"
	});
	$(".btn-restore").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		document.location.href = "setup_udf_list.php?act=restore&id=<?php echo $id; ?>&vid="+i;
	})
	$(".btn-edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr('my_id');
		var v = $("#td_value_"+i).html();
		$("#edit_text").val(v);
		$("#td_id").html(i);
		$("#id_field").val(i);
		$("#dlg_edit").dialog("open");
		AssistHelper.closeProcessing();
	});
	$("#btn_save_edit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("#edit_text").removeClass("required");
		if($("#edit_text").val().length==0) {
			$("#edit_text").addClass("required");
			AssistHelper.finishedProcessing("error","Please fill in required fields.");
		} else {
			$("form[name=frm_edit]").submit();
		}
	});
	$("#btn_delete").click(function(e) {
		e.preventDefault();
		if(confirm("Are you sure you wish to delete this List Item?")==true) {
			document.location.href = "setup_udf_list.php?act=delete&id=<?php echo $id; ?>&vid="+$("#id_field").val();
		}
	})
});
</script>
</body>

</html>