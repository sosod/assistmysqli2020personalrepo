<?php

$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('txt'=>"List Columns"),
);
$page = array("setup","columns");
$get_udf_link_headings = true;
require_once 'inc_header.php';

$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : die("<p>An error has occurred.  Please go back and try again.</p>");
switch($section) {
case "my":	
case "all":	$title[] = array('txt'=>ucfirst($section)." ".ucfirst($actname)."s"); break;
case "own":	$title[] = array('txt'=>ucfirst($actname)."s Owned"); break;
}
   
include("inc_header.php");

$dN = explode("_",$_REQUEST['dN']);
$dY = explode("_",$_REQUEST['dY']);

if(count($dY)>0) {
	foreach($dN as $n => $d) {
		if(checkIntRef($d)) {
			$sql = "UPDATE ".$dbref."_list_display SET ".$section."disp = 'N', ".$section."sort = 999 WHERE id = $d";
			//echo "<P>".$sql;
            $ah->db_update($sql);
		}
	}
	foreach($dY as $y => $d) {
		if(checkIntRef($d)) {
			$sql = "UPDATE ".$dbref."_list_display SET ".$section."disp = 'Y', ".$section."sort = ".($y+1)." WHERE id = $d";
			//echo "<P>".$sql;
            $ah->db_update($sql);
		}
	}
	echo "<script type=text/javascript>
		document.location.href = 'setup_columns.php?section=".$section."&r[]=ok&r[]=Display updated successfully.';
	</script>";
} else {
	echo "<script type=text/javascript>
		document.location.href = 'setup_columns.php?section=".$section."&r[]=error&r[]=Please select at least 1 column to display.';
	</script>";
} ?>
</body>

</html>
