<style type=text/css>
body{
  font-size: 62.5%;
}
h1 {
	text-decoration: none;
	font-size: 14pt;
	line-height: 16pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
h2 {
	text-decoration: none;
	font-size: 12pt;
	line-height: 14pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
h3 {
	text-decoration: none;
	font-size: 10pt;
	line-height: 12pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
table td { vertical-align: top; }
.noborder { border: 0px solid #FFFFFF; }
.minw { min-width: 700px; }
.left { text-align: left; }
.right { text-align: right; }
.centre { text-align: center; }
.top { vertical-align: top; }
.blank { background-color: #ffffff; }
.tdhover { background-color: #e1e1e1; }
.legend {
	font: Tahoma;
	font-size: 7.5pt;
	line-height: 8.5pt;
	border-color: #ffffff;
	border-width: 4px; 
	vertical-align: top;
	text-align: left;
}
.charttitle {
	font-weight: bold;
	font-size: 13pt;
	line-height: 14pt;
	font:Verdana; 
	text-decoration:underline;
	margin: 0 0 0 0;
	text-align: center;
}
/* css for timepicker */
/*.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; color: #ffffff; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
/* css for sort */
#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; width: 200px;}
#sortable li span { position: absolute; margin-left: -1.3em; }
.ui-state-highlightsort { 
	height: 1.5em; line-height: 1.2em; 
	list-style-image: none;
}
.ui-sort { list-style-image: none; }
</style>