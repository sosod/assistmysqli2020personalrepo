<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$view_type = $_REQUEST['act']; 
$get_udf_link_headings = false;

$redirect_me = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";

require_once 'inc_header.php';

$actionObject = new PDP_ACTION();

$src = explode("/",$_SERVER['HTTP_REFERER']);
//arrPrint($src);
if(substr($src[count($src)-1],0,4)=="link") {
	$return_addr = "home";
} else {
	$return_addr = $src[count($src)-1];
}
function drawHistoryRow($log) {
	global $update_udfs;
	global $history_attachments;
	global $history_udfs;
	global $update_udf_lists;
	$cmpcode = $_SESSION['cc'];
	$modref = $_SESSION['modref'];
	
	$logid = $log['logid'];
	$log_udf = isset($history_udfs[$logid]) ? $history_udfs[$logid] : array();
	$log_attach = isset($history_attachments[$logid]) ? $history_attachments[$logid] : array();
	$file_location = "../files/".$cmpcode."/".$_SESSION['modref']."/";
	foreach($log_attach as $i => $l) {
		if(strlen($l['file_location'])>0) { $path = $file_location.$l['file_location']."/"; } else { $path = $file_location; }
		if(!file_exists($path.$l['system_filename'])) {
			unset($log_attach[$i]);
		}
	}

	$log_update = html_entity_decode ($log['logupdate']);

		echo "<tr>
				<td colspan=2 style='padding: 0 0 0 0;'>
					<table id=log_".$logid." width=99% class=noborder>
						<tr>
							<th width=50 rowspan=2>".date("d M Y H:i",$log['logactdate'])."</th>
							<td >
								<p style='margin-top: 0px;'>".$log_update."</p>";
		if(count($log_attach)>0) {
			echo "
							<p><span class=b>Attachments:</span>";
			foreach($log_attach as $a) {
				echo "<br />+ <a href='download2.php?i=".$a['id']."'>".$a['original_filename']."</a>";
			}
			echo "</p>";
		}
		echo "
								<table class=noborder id=tbl_udf>";
								foreach($update_udfs as $u) {
									if(isset($log_udf[$u['udfiid']]) && ( ($u['udfilist']=="Y" && ASSIST_HELPER::checkIntRef($log_udf[$u['udfiid']]['udfvalue'])) || (strlen($log_udf[$u['udfiid']]['udfvalue'])>0) )) {
										$lv = isset($log_udf[$u['udfiid']]['udfvalue']) ? $log_udf[$u['udfiid']]['udfvalue'] : "";
										echo "
											<tr>
												<td class='b right' >".$u['udfivalue'].":</td>
												<td>";
												switch($u['udfilist']) {
												case "Y":
													echo (isset($lv) && ASSIST_HELPER::checkIntRef($lv) && isset($update_udf_lists[$u['udfiid']][$lv])) ? $update_udf_lists[$u['udfiid']][$lv] : "N/A" ;
													break;
												case "M":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "D":
													if(strlen($lv)>0) { 
														if(is_numeric($lv)) { echo date("d M Y",$lv); } else { echo $lv; }
													} else {
														echo "N/A";
													}
													
//													echo strlen($lv)>0 && is_numeric($lv) ? date("d M Y",$lv) : "N/A";
													break;
												case "T":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "N":
													echo strlen($lv)>0 && is_numeric($lv) ? number_format($lv,2) : "N/A";
													break;
												}
											echo "</td>
											</tr>";
									}
								}
						echo	"</table>
		</td>
							<td  class='right'>
								<p style='margin-top: 0px;'><span class=b>Status:</span> ".$log['status']."<br />(".$log['logstate']."% completed)</p>
								<p><span class=b>Date Logged:</span><br />".date("d M Y H:i",$log['logdate'])."</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
}


//GET USER ACCESS PERMISSIONS
$tact = $ah->getMyUserAccess();

//GET TASK ID
$taskid = $_REQUEST['i'];
if(ASSIST_HELPER::checkIntRef($taskid)) {
	//GET TASK DETAILS INTO ARRAY TASK
//	$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
	$task = $actionObject->getTask($taskid);
	
	//HISTORY INFORMATION
		$sql = "SELECT l.*, stat.value as status 
				FROM ".$dbref."_log l
				INNER JOIN ".$dbref."_list_status stat
				  ON l.logstatusid = stat.pkey
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U','E','D') 
				ORDER BY logactdate DESC, logdate DESC";
		$history = $ah->mysql_fetch_all_fld($sql,"logid");
		$sql = "SELECT l.* 
				FROM ".$dbref."_log l
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U') 
				ORDER BY logactdate DESC, logdate DESC
				LIMIT 1";
		$last = $ah->mysql_fetch_all($sql);
		$last_log_id = isset($last[0]['logid']) ? $last[0]['logid'] : false;
		$sql = "SELECT a.* 
				FROM ".$dbref."_task_attachments a
				INNER JOIN ".$dbref."_log l
				  ON a.logid = l.logid
				  AND l.logtaskid = ".$taskid;
		$history_attachments = $ah->mysql_fetch_all_fld2($sql,"logid", "id");
		$sql = "SELECT u.*, l.logid 
				FROM assist_".$cmpcode."_udf u
				INNER JOIN assist_".$cmpcode."_udfindex i
				  ON u.udfindex = i.udfiid
				  AND i.udfiobject = 'update'
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = '".$modref."'
				INNER JOIN ".$dbref."_log l
				  ON l.logid = u.udfnum
				  AND l.logtaskid = ".$taskid."
				WHERE u.udfref = '".$modref."'";
		$history_udfs = $ah->mysql_fetch_all_fld2($sql,"logid", "udfindex");
		
	//UPDATE INFORMATION	
		$sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND id NOT IN ('CN','NW','ON') ORDER BY sort";
		$status = $ah->mysql_fetch_all_fld($sql,"pkey");
		
		$sql = "SELECT * 
				FROM assist_".$cmpcode."_udfindex 
				WHERE udfiref = '".$modref."' 
				AND udfiyn = 'Y' 
				AND udfiobject = 'update' 
				AND (
						udfilinkref = 0 
					OR 
						udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid']."
					OR 
						udfilinkfield = 'taskstatusid'
				)
				ORDER BY udfisort, udfivalue";
		$update_udfs = $ah->mysql_fetch_all($sql);
		$update_udf_lists = $ah->getAllUDFListItems("update");

	
} else {
	die("<p>An error occurred while trying to access the requested ".$actname.".  Please go back and try again.</p>");
}

//$last = array_pop($history_udfs);
$k = array_keys($history);
$last = $last_log_id!== false && isset($history_udfs[$last_log_id]) ? $history_udfs[$last_log_id] : array();


?>
<style type=text/css>
[type=file] { margin-bottom: 5px; }
#tbl_udf, #tbl_udf td { padding: 2px 1px 2px 1px; }
</style>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function statusPerc() {
           /* var stat = document.taskupdate.logstatusid.value;
            stat = parseInt(stat);
            if(stat == 3 || stat > 5)
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
                if(stat > 5 && lstat[stat] > 0)
                {
                    document.taskupdate.logstate.value = lstat[stat];
                }
                else
                {
                    document.taskupdate.logstate.value = lstat[0];
                }
            }
            else
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }*/
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>
<table width=100% id=tbl_container>
	<tr><td class=td_container style='padding-right: 30px' width=50%>
        <h2 class=fc><?php echo ucfirst($actname);?> Status</h2>
        <table width=95% id=tbl_object><tbody>
            <tr>
                <th width="150">Reference:</b></th>
				<td><?php echo $task['taskid']; ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskadduser"; echo $headings['action'][$fld]; ?>:</th>
				<td><?php echo $task['adduser']; ?></td>
            </tr>
			<tr>
				<th><?php $fld = "taskadddate"; echo $headings['action'][$fld]; ?>:</th>
				<td><?php echo date("d-M-Y H:i:s",$task['taskadddate']); ?></td>
			</tr>
            <tr>
				<th><?php $fld = "tasktkid"; echo $headings['action'][$fld]; ?>:</th>
                <td><?php
					$sql = "SELECT * 
							FROM assist_".$cmpcode."_timekeep u 
							INNER JOIN ".$dbref."_task_recipients r 
							  ON (u.tkid=r.tasktkid) 
							WHERE r.taskid= '".$task['taskid']."' ";

					$assignees = array();
					$rows = $ah->mysql_fetch_all($sql);
					foreach($rows as $row) {
						$assignees[] = $row['tkname']." ".$row['tksurname'];
					}
					if(count($assignees)>1) {
						echo implode(";<br />",$assignees).";";
					} else {
						echo $assignees[0];
					}
                ?></td>
            </tr>
            <tr>
                <th><?php $fld = "tasktopicid"; echo $headings['action'][$fld]; ?>:</td>
				<td><?php echo $task['topic']; ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskurgencyid"; echo $headings['action'][$fld]; ?>:</th>
				<td><?php echo $task['urgency']; ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskstatusid"; echo $headings['action'][$fld]; ?>:</th>
				<td><?php echo $task['status'].($task['taskstate']>0 && $task['taskstate']<100 ? " (".$task['taskstate']."%)" : ""); ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskdeadline"; echo $headings['action'][$fld]; ?>:</th>
                <td><?php echo date("d-M-Y",$task['taskdeadline']); ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskaction"; echo $headings['action'][$fld]; ?>:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskaction']);
                ?></td>
            </tr>
            <tr>
                <th><?php $fld = "taskdeliver"; echo $headings['action'][$fld]; ?>:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskdeliver']);
                ?></td>
            </tr>
            <?php
            $sql = "SELECT * 
					FROM assist_".$cmpcode."_udfindex 
					WHERE udfiref = '".$modref."' 
					AND udfiyn = 'Y' 
					AND udfiobject = 'action' 
					AND (udfilinkref = 0 OR udfilinkref = ".$task['tasktopicid'].")
					ORDER BY udfisort, udfivalue";
            $rows = $ah->mysql_fetch_all_by_id($sql,"udfiid");
            if(count($rows)>0) {
            	$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex IN (".implode(",",array_keys($rows)).")";
            	$udf_rows = $ah->mysql_fetch_all_by_id($sql,"udfindex");
			} else {
            	$udf_rows = array();
			}
            foreach($rows as $udfid => $row) {
            	$row2 = isset($udf_rows[$udfid]) ? $udf_rows[$udfid] : array('udfvalue'=>"");
                ?>
            <tr>
                <th><?php echo($row['udfivalue']); ?>:</th>
                <td><?php
                        switch($row['udfilist']) {
                            case "Y":
                                if(ASSIST_HELPER::checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$udfid." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    $row3 = $actionObject->mysql_fetch_one($sql2);
									echo isset($row3['udfvvalue']) ? $row3['udfvvalue'] : "N/A";
                                } else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo ASSIST_HELPER::decode($row2['udfvalue']);
                                else
                                    echo "N/A";
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",ASSIST_HELPER::decode($row2['udfvalue'])));
                                break;
                        }
                ?>&nbsp;</td>
            </tr>
                <?php
            }
			unset($rs2);

            $sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE att.file_location <> 'deleted' AND att.taskid = ".$taskid;
            $rows = $actionObject->mysql_fetch_all($sql);
            if(count($rows) > 0) {
                echo "<tr>
				<th>Attachment(s):</th>
				<td>";
$tattach = array();
                foreach($rows as $row) {
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
                    $file = "../files/$cmpcode/".$_SESSION['modref']."/".(strlen($row['file_location'])>0 ? $row['file_location']."/" : "").$row['system_filename'];
                    if(file_exists($file)) {
						$tattach[] =  "+ <a href='download2.php?i=".$row['id']."'>".$row['original_filename']."</a>";
					}
                }
                echo implode("<br />",$tattach)."</td></tr>";
            }

            if(($tkid == $task['taskadduser'] || $view_type == "ALL")) { ?>
                 <tr><th></th><td class=right><input type="button" value="Edit"  onclick="redirect(<?php echo($taskid);?>);"/></td></tr>
           <?php } ?>
        </tbody></table>
        <?php 
		$ah->displayGoBack();
if(count($history)>0) {
?>
	<table id=tbl_history width=95%>
	<?php
	echo "	<tr>
				<th colspan=2>History</th>
			</tr>";
	foreach($history as $h) {
		drawHistoryRow($h);
	}
	
	?>
	</table>
<?php 
	$ah->displayGoBack();
} 
?>
</td><td class=td_container width=50%><div class=float>
<?php
if($task['taskstatusid']>2) {	//not completed or cancelled		
		echo "<h2>".ucfirst($actname)." Updates</h2>
		<form name=update action=update_process.php method=post  enctype='multipart/form-data'>
		<input type=hidden name=redirect value=".$redirect_me." />
		<input type=hidden name=logtaskid value=".$taskid." />
		<input type=hidden name=src value='".$return_addr."' />
		<table id=tbl_updates>
			<tr>
				<th colspan=2>Update</th>
			</tr>
			<tr class=no-highlight>
				<td class=center colspan=2>
					<textarea rows=6 cols=70 name=logupdate></textarea>
				</td>
			</tr>
			<tr>
				<td class=left style='border-right: 0px solid #ababab; border-bottom: 0px;'>
					<p><span class=b>Status:</span> <select name=logstatusid>";
					foreach($status as $s) {
						echo "<option value=".$s['pkey']." s=".$s['state'].">".$s['value']."</option>";
					}
		echo "
					</select></p>
					<p><input type=text name=logstate id=logstate size=5 value=".($task['taskstate'])." style='text-align: right; margin-left: 45px' />% complete </p>
					<p><span class=b>Date of Activity:</span> <input type=text name=logactdate value='".date("d M Y H:i")."' class=datetime /></p>
				</td>
				<td class=right  style='border-left: 0px solid #ababab; border-bottom: 0px;'>
					<span id=spn_attach_update><input type=file name=attachments[] /></span>
					<br /><a href=javascript:void(0) id=p_attach>Attach another</a>
				</td>
			</tr>";
				$udftypes = array('Y'=>array(),'D'=>array());
			if(count($update_udfs)>0) {
				echo "
					<tr><td colspan=2 style='border-top: 0px;'>
						<table id=tbl_update_udf>";
				foreach($update_udfs as $u) {
					$ui = $u['udfiid'];
					$uv = isset($last[$ui]['udfvalue']) ? $last[$ui]['udfvalue'] : "";
					if(isset($udftypes[$u['udfilist']])) { $udftypes[$u['udfilist']][] = $u['udfiid']; }
					$class = $u['udfiobject']." ".(strlen($u['udfilinkfield'])>0 ? $u['udfilinkfield']." ".$u['udfilinkref'] : "");
					echo "
						<tr class='udf $class'>
							<td class='b right'>".$u['udfivalue'].":</td>
							<td>";
						switch($u['udfilist']) {
						case "Y":
							$x = isset($update_udf_lists[$ui][$uv]);
							echo "<select name=udf[".$ui."]><option value=0 ".($x ? "" : "selected")." >N/A</option>";
							foreach($update_udf_lists[$ui] as $vindex => $vdata) {
								echo "<option value=".$vindex."  ".($uv==$vindex ? "selected" : "").">".$vdata."</option>";
							}
							echo "</select>";
							break;
						case "M":
							echo "<textarea name=udf[".$u['udfiid']."] rows=5 cols=60>".ASSIST_HELPER::decode($uv)."</textarea>";
							break;
						case "D":
							echo "<input name=udf[".$u['udfiid']."] type=text size=11 class=jdate2012 value='";
							if(strlen($uv)>0) {
								if(is_numeric($uv)) {
									echo date("d-M-Y",$uv);
								} else {
									echo $uv;
								}
							} else {
								echo "";
							}
							echo "' />";
							break;
						case "T":
							echo "<input name=udf[".$u['udfiid']."] type=text size=60 value='".ASSIST_HELPER::decode($uv)."' />";
							break;
						case "N":
							echo "<input name=udf[".$u['udfiid']."] type=text size=15 class=number value='".$uv."' /> <span class=i style='' id=lbl_".$u['udfiid'].">(numbers only)</span>";
							break;
						}
					echo "</td>
						</tr>";
				}
				echo "	</table>
					</td></tr>";
			}
			echo "
			<tr>
				<td class=center colspan=2><input type=hidden name=udf_types value='".serialize($udftypes)."' /><input type=button value='Save Update' class=isubmit /><input type=reset /></td>
			</tr>
		</table>
		</form>";
		$ah->displayGoBack();
}//end taskstatusid if
		?></div>
</td></tr>
</table>
<script type=text/javascript>
$(function() {
	$("tr").unbind('mouseenter mouseleave');
	$("td").addClass("top");
	$("table.noborder td").addClass("noborder");
	$("table#tbl_container, table#tbl_container td.td_container").addClass("noborder");
	$("#tbl_object th").addClass("left");
	$("#tbl_update_udf, #tbl_update_udf td").addClass("noborder");
	$("#tbl_updates select[name=logstatusid]").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_updates input[name=logstate]").val(<?php echo $task['taskstate']; ?>).blur(function() {
		var v = parseInt($(this).val());
		if(v==100) {
			$("#tbl_updates select[name=logstatusid]").val(1);
			$(this).attr("disabled","disabled");
		}
	});
	var previous_status;
	var previous_progress;
	$("#tbl_updates select[name=logstatusid]").focus(function() {
		previous_status = $(this).val();
		previous_progress = $("#tbl_updates input[name=logstate]").val();
	});
	$("#tbl_updates select[name=logstatusid]").change(function() {
		var v = $(this).val();
		if(v==1) {
			$("#tbl_updates input:text[name=logstate]").val(100);
			$("#tbl_updates input:text[name=logstate]").attr("disabled","true");
		} else {
			$("#tbl_updates input:text[name=logstate]").removeAttr("disabled");
			var s = $("#tbl_updates select[name=logstatusid] option:selected").attr("s");
			if(!isNaN(parseInt(s)) && s > 0) {
				$("#tbl_updates input:text[name=logstate]").val(s);
			} else if(parseInt(previous_status)==1){
				$("#tbl_updates input:text[name=logstate]").val(99);
			}
		}
		$("#tbl_update_udf tr.taskstatusid").each(function() {
			if($(this).hasClass(v)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	//$("#tbl_updates select[name=logstatusid]").trigger("change");
	$("#tbl_updates #p_attach").click(function() {
		$("#tbl_updates #spn_attach_update").append("<br /><input type=file name=attachments[] />");
	});
	$("form[name=update] input:button.isubmit").click(function() {
		var form = "form[name=update]";
		$(form+" textarea,"+form+" select,"+form+" input").removeClass("required");
		var err = false;
		var logupdate = $(form+" textarea[name=logupdate]").val();
		if(logupdate.length==0) {
			$(form+" textarea[name=logupdate]").addClass("required");
			err = true;
		}
		var logstatusid = $(form+" select[name=logstatusid]").val();
		if(logstatusid.length==0) {
			$(form+" select[name=logstatusid]").addClass("required");
			err = true;
		}
		var logstate = $(form+" input[name=logstate]").val();
		if(logstate.length==0) {
			$(form+" input[name=logstate]").addClass("required");
			err = true;
		}
		var logactdate = $(form+" input[name=logactdate]").val();
		if(logactdate.length==0) {
			$(form+" input[name=logactdate]").addClass("required");
			err = true;
		}
		$("#tbl_update_udf input:text").each(function() {
			var u = 0;
			if($(this).hasClass("number")) {
				u = $(this).val();
				if(u.length>0 && (isNaN(parseFloat(u)) || !isFinite(u))) {
					$(this).addClass("required");
					$("#lbl_"+$(this).attr("name")).css("color","#cc0001");
					err = true;
				}
			}
		});
		if(!err) {
			$(form).submit();
		} else {
			alert("Please complete the required fields as highlighted in red.");
		}
	});
});
</script>		

    </body>

</html>
