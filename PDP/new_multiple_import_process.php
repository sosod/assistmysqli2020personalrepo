<?php

$title = array(
	array('url'=>"new.php",'txt'=>"New"),
	array('url'=>"new_multiple.php",'txt'=>"Import"),
	array('url'=>"new_multiple.php",'txt'=>"Validate"),
);
$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new","multiple","import");
$get_udf_link_headings = false;
require_once("inc_header.php");

$actionObject = new PDP_ACTION();

echo "<P>Processing...</p>";

$rows_imported = $_REQUEST['count'];
$headings_to_process = $ah->getFullHeadings();
$tasks_created = 0;
for($i=0;$i<$rows_imported;$i++){
	$input_data = array('sendEmail'=>false);
	foreach($headings_to_process as $fld => $head) {
		if($fld!="tasktkid") {
			if(isset($_REQUEST[$fld][$i])) {
				$input_data[$fld] = $_REQUEST[$fld][$i];
			}
		} else {
			$c = $i+1;
			$input_data[$fld] = $_REQUEST[$fld.($c)];
		}
	}
	foreach($udf_index['action']['ids'] as $udfi) {
		if(isset($_REQUEST[$udfi][$i])) {
			$input_data[$udfi] = $_REQUEST[$udfi][$i];
		}
	}
	$taskid = $actionObject->addObject($input_data,$udf_index,false);
	$tasks_created+=(ASSIST_HELPER::checkIntRef($taskid)?1:0);
}

echo "
<script type='text/javascript'>
document.location.href = 'new_multiple.php?r[]=ok&r[]=".urlencode($rows_imported." new ".$ah->getActionName($rows_imported==1?false:true)." created successfully.")."'
</script>";
?>
</body>
</html>