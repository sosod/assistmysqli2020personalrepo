<?php
$title = array(
	array('url'=>"report.php",'txt'=>"Report Generator"),
);
$page = array("report","process");
$get_udf_link_headings = false;
require_once 'inc_header.php';








$order_by = (isset($_REQUEST['orderby']) && strlen($_REQUEST['orderby'])>0) ? $_REQUEST['orderby']  : "taskid DESC";

//GET VARIABLES
$tact = $ah->getUserAccess();
$actname.="s";
$fields = $_REQUEST['field'];
$output = strtoupper($_REQUEST['output']);
//SEARCH VARIABLES
$search_where = "";
$search = array();
include("inc_adv_search_filter.php");
//START NAVIGATION SETTINGS
$current = isset($_REQUEST['start']) && ASSIST_HELPER::checkIntRef($_REQUEST['start']) ? $_REQUEST['start'] : 0;
//GET UDFS
$udf_index2 =$ah->mysql_fetch_all_fld("SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".$modref."'","udfiid");
$udf_keys = array_keys($udf_index2);

$update_filter = $_REQUEST['logsincluded'];
//echo "<h2>".$update_filter."</h2>";

$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfref = '".$modref."'";
$udfs =$ah->mysql_fetch_all_fld2($sql,"udfnum","udfindex");
$udf_list_values = array();
if(count($udf_keys)>0) {
	$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvyn = 'Y' AND udfvindex IN (".implode(",",$udf_keys).") ORDER BY udfvindex, udfvvalue";
	$udf_list_values =$ah->mysql_fetch_all_fld2($sql,"udfvindex","udfvid");
}
//GET TASKS
$task_attachments =$ah->mysql_fetch_all_fld("SELECT taskid, count(id) as tc FROM ".$dbref."_task_attachments WHERE taskid > 0 AND file_location <> 'deleted' GROUP BY taskid","taskid");
$log_attachments =$ah->mysql_fetch_all_fld("SELECT l.logid, count(a.id) as tc FROM ".$dbref."_task t INNER JOIN ".$dbref."_log l ON t.taskid = l.logtaskid INNER JOIN ".$dbref."_task_attachments a ON l.logid = a.logid AND a.file_location <> 'deleted' GROUP BY l.logid","logid");

	$title = "All ".$actname;
	$display = array('field'=>"alldisp",'sort'=>"allsort");
	$where = "FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_task_recipients tr
			  ON tr.taskid = t.taskid 
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep ta
			  ON t.taskadduser = ta.tkid
			LEFT OUTER JOIN ".$dbref."_list_topic topic
			  ON t.tasktopicid = topic.id
			LEFT OUTER JOIN ".$dbref."_list_urgency urgency
			  ON t.taskurgencyid = urgency.id
			LEFT OUTER JOIN ".$dbref."_list_status status
			  ON t.taskstatusid = status.pkey ".
			(strlen($search_where)>0 ? "WHERE " : "");
	$sql = "SELECT t.taskid
			, topic.value as tasktopicid
			, CONCAT_WS(' ',ta.tkname,ta.tksurname) as taskadduser
			, status.value as taskstatusid
			, status.pkey as status
			,t.taskdeadline
			, urgency.value as taskurgencyid
			,t.taskaction
			,t.taskdeliver
			,t.taskadddate
			".$where." ".$search_where."
			ORDER BY ".$order_by; //." LIMIT 0,30";
//echo $sql;
$tasks =$ah->mysql_fetch_all_fld($sql,"taskid");
$task_ids = array_keys($tasks);
$task_recipients = array();
$update_logs = array();
if(count($task_ids)>0) {
/*	$sql = "SELECT tr.id, tr.taskid, CONCAT_WS(  ' ', tk.tkname, tk.tksurname ) as tkname
			FROM ".$dbref."_task_recipients tr
			INNER JOIN assist_".$cmpcode."_timekeep tk 
			  ON tk.tkid = tr.tasktkid
			  AND tk.tkstatus = 1
			INNER JOIN assist_".$cmpcode."_menu_modules_users mmu 
			  ON tk.tkid = mmu.usrtkid
			  AND mmu.usrmodref =  '".$_SESSION['modref']."'
			WHERE tr.taskid IN (".implode(",",$task_ids).") ";*/
	$sql = "SELECT tr.id, tr.taskid, CONCAT_WS(  ' ', tk.tkname, tk.tksurname ) as tkname
			FROM ".$dbref."_task_recipients tr
			INNER JOIN assist_".$cmpcode."_timekeep tk 
			  ON tk.tkid = tr.tasktkid
			WHERE tr.taskid IN (".implode(",",$task_ids).") ";
	$task_recipients =$ah->mysql_fetch_all_fld2($sql,"taskid","id");
	$sql = "SELECT l.logid, l.logtaskid
			, l.logdate
			, CONCAT_WS(' ',tk.tkname,tk.tksurname) as logtkid
			, l.logupdate
			, s.value as logstatusid
			, l.logstate
			, l.logactdate
			FROM ".$dbref."_log l
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			  ON tk.tkid = l.logtkid
			INNER JOIN ".$dbref."_list_status s
			  ON s.pkey = l.logstatusid
			WHERE l.logtaskid IN (".implode(",",$task_ids).")
			AND l.logtype = 'U'";
	$update_logs =$ah->mysql_fetch_all_fld2($sql,"logtaskid","logid");
}

//GET HEADINGS
//$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND ".$display['field']." = 'Y' ".(count($udf_keys)>0 ? "AND (type = 'S' OR field IN (".implode(",",$udf_keys)."))" : "AND type = 'S'")." ORDER BY ".$display['sort']."";
//$head =$ah->mysql_fetch_all_fld($sql,"field");




//OUTPUT FORMAT
switch($output) {
case "blank":
	$page[0] = $_SESSION['ia_cmp_name']." ".$_SESSION['modtext']." Report";
	$page[9] = "Report drawn at ".date("d F Y H:i:s");
	$table[0] = "";
	$table[9] = "";
	$row[0] = "";
	$row[9] = "";
	$cell[0]['head'] = "";
	$cell[0]['normal'] = "";
	$cell[0]['ref'] = "";
	$cell[0]['center'] = "";
	$cell[0]['right'] = "";
//1 = action
	$cell[1]['head'] = "";
	$cell[1]['normal'] = "";
	$cell[1]['ref'] = "";
	$cell[1]['center'] = "";
	$cell[1]['right'] = "";
//2 = update
	$cell[2]['head'] = "";
	$cell[2]['normal'] = "";
	$cell[2]['ref'] = "";
	$cell[2]['center'] = "";
	$cell[2]['right'] = "";
	
	$cell[9]['head'] = "";
	$cell[9]['normal'] = "";
	$cell[9]['ref'] = "";
	$cell[9]['center'] = "";
	$cell[9]['right'] = "";
	
	$tag_close = "";
	
	$newline = "";
	$num_format['thou'] = " ";
	$num_format['dec'] = 2;
	break;
case "XLS":
	$page[0] = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">
				<head>
				<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">
				<!--[if gte mso 9]>
				<xml>
				<x:ExcelWorkbook>
				<x:ExcelWorksheets>
				<x:ExcelWorksheet>
				<x:Name>KPI Report</x:Name>
				<x:WorksheetOptions>
				<x:Panes>
				</x:Panes>
				</x:WorksheetOptions>
				</x:ExcelWorksheet>
				</x:ExcelWorksheets>
				</x:ExcelWorkbook>
				</xml>
				<![endif]-->
				<style>
				td { font-style: Calibri; font-size:11pt; vertical-align: top; border-width: thin; border-color: #000000; border-style: solid;} 
				.title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } 
				.title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
				.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } 
				.center { text-align: center; }
				.right { text-align: right; }
				.i { font-style: italic; }
				</style>
				</head>
				<body>
				<table>
				<tr><td class=title nowrap colspan=".(count($fields)+1).">".$_SESSION['ia_cmp_name']."</td></tr><tr><td class=title2 nowrap colspan=".(count($fields)+1).">".$_SESSION['modtext']." Report</td></tr>
				<tr></tr>";
	$page[9] = "<tr></tr>
				<tr><td style=\"font-size:7pt;font-style:italic;\" nowrap colspan=".(count($fields)+1).">Report drawn on ".date("d F Y H:i:s").".</td></tr></table></body></html>";
	$table[0] = "";
	$table[9] = "";
	$row[0] = "<tr>";
	$row[9] = "</tr>";
	$cell[0]['head'] = "<td class=\"head\">";
	$cell[0]['normal'] = "<td>";
	$cell[0]['ref'] = "<td class=\"head\">";
	$cell[0]['center'] = "<td class=\"center\">";
	$cell[0]['right'] = "<td class=\"right\">";
//1 = action
	$cell[1]['head'] = "<td class=head rowspan=";
	$cell[1]['normal'] = "<td rowspan=";
	$cell[1]['ref'] = "<td class=head rowspan=";
	$cell[1]['center'] = "<td class=center rowspan=";
	$cell[1]['right'] = "<td class=right rowspan=";
	
	$cell[9]['head'] = "</td>";
	$cell[9]['normal'] = "</td>";
	$cell[9]['ref'] = "</td>";
	$cell[9]['center'] = "</td>";
	$cell[9]['right'] = "</td>";
	
	$tag_close = ">";
	
	$newline = chr(10);
	$num_format['thou'] = " ";
	$num_format['dec'] = 2;
	break;
case "onscreen":
default:
	$page[0] = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">
				<html>
				<head>
				<meta http-equiv=\"Content-Language\" content=\"en-za\">
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">
				<title>www.Ignite4u.co.za</title>
				</head>
				<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\" />
				<base target=\"main\">
				<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
					<h1 class=center>".$_SESSION['ia_cmp_name']."</h1>
					<h2 class=center>".$_SESSION['modtext']." Report</h2>";
	$page[9] = "<p class=i style='font-size: 7pt;'>Report drawn at ".date("d F Y H:i:s")."</p>
				</body></html>";
	$table[0] = "<table width=100%>";
	$table[9] = "</table>";
	$row[0] = "<tr>";
	$row[9] = "</tr>";
	$cell[0]['head'] = "<th>";
	$cell[0]['normal'] = "<td>";
	$cell[0]['ref'] = "<th>";
	$cell[0]['center'] = "<td class=center>";
	$cell[0]['right'] = "<td class=right>";
//1 = action
	$cell[1]['head'] = "<th rowspan=";
	$cell[1]['normal'] = "<td rowspan=";
	$cell[1]['ref'] = "<th rowspan=";
	$cell[1]['center'] = "<td class=center rowspan=";
	$cell[1]['right'] = "<td class=right rowspan=";
//2 = update
	$cell[2]['head'] = "<th";
	$cell[2]['normal'] = "<td";
	$cell[2]['ref'] = "<th";
	$cell[2]['center'] = "<td class=center";
	$cell[2]['right'] = "<td class=right";
	
	$cell[9]['head'] = "</th>";
	$cell[9]['normal'] = "</td>";
	$cell[9]['ref'] = "</th>";
	$cell[9]['center'] = "</td>";
	$cell[9]['right'] = "</td>";
	
	$tag_close = ">";
	
	$newline = "<br />";
	$num_format['thou'] = " ";
	$num_format['dec'] = 2;
	
	break;
}


$echo = "";
$echo.= $page[0];
$echo.= $table[0];

//headings
$head = array();
$echo.= $row[0];
	$echo.= $cell[0]['head']."Ref".$cell[9]['head'];
	foreach($headings['action'] as $key => $h) {
		if(in_array($key,$fields)) {
			$echo.= $cell[0]['head'].$h.$cell[9]['head'];
			$head[] = array('key'=>$key,'type'=>"A");
		}
	}
	foreach($udf_index['action']['index'] as $key => $u) {
		$h = $u['udfivalue'];
		if(in_array($key,$fields)) {
			$echo.= $cell[0]['head'].$h.$cell[9]['head'];
			$head[] = array('key'=>$key,'type'=>"AU");
		}
	}
$update_head = array();
	foreach($headings['update'] as $key => $h) {
		if(in_array($key,$fields)) {
			$echo.= $cell[0]['head'].$h.$cell[9]['head'];
			$update_head[] = array('key'=>$key,'type'=>"U");
		}
	}
	foreach($udf_index['update']['index'] as $key => $u) {
		$h = $u['udfivalue'];
		if(in_array($key,$fields)) {
			$echo.= $cell[0]['head'].$h.$cell[9]['head'];
			$update_head[] = array('key'=>$key,'type'=>"UU");
		}
	}
$echo.= $row[9];
//arrPrint($fields);
//arrPrint($head);

foreach($tasks as $t) {
	
	$obj_id = $t['taskid'];
	$continue = true; 
	switch($update_filter) {
		case "ALL":
			if(!isset($update_logs[$obj_id]) || count($update_logs[$obj_id])==0) {
				$continue = false;
			}
			break;
		case "NONEALL": 
			$continue = true;
			break;
		case "RECENT":
			if(!isset($update_logs[$obj_id]) || count($update_logs[$obj_id])==0) {
				$continue = false;
			}
			break;
		case "NONERECENT":
			$continue = true;
			break;
		case "NONE":
			if(isset($update_logs[$obj_id]) && count($update_logs[$obj_id])>0) {
				$continue = false;
			}
			break;
		default:
			$continue = true;
			break;
	}
	if($continue) {
		$echo.= $row[0];
			if(($update_filter == "ALL" || $update_filter=="NONEALL") && count($update_head)>0 && isset($update_logs[$obj_id]) && count($update_logs[$obj_id])>1) {
				 $rowspan = count($update_logs[$obj_id]); 
			} else {
				 $rowspan = 1; 
			}
			//echo "<P>".$obj_id." :: ".$rowspan."</p>";
			$echo.= $cell[1]['ref'].$rowspan.$tag_close.$obj_id.$cell[9]['ref'];
		//TASKS
		foreach($head as $h) {
			$key = $h['key'];
			$cell_format = "normal";
			$rowspan = 1;
			switch($h['type']) {
			case "A":
				$v = (isset($t[$key]) ? $t[$key] : $key);
				if(($update_filter == "ALL" || $update_filter=="NONEALL") && isset($update_logs[$obj_id]) && count($update_logs[$obj_id])>1) { $rowspan = count($update_logs[$obj_id]); }
				switch($key) {
				case "taskdeadline":
					$cell_format = "center";
					$v = is_numeric($v) && strlen($v)>0 && $v!=0 ? date("d M Y",$v) : ""; 
					break;
				case "taskadddate":
					$cell_format = "center";
					$v = is_numeric($v) ? date("d M Y",$v) : $v; 
					break;
				case "tasktkid":
					$v = isset($task_recipients[$obj_id]) ? $task_recipients[$obj_id] : array();
					if(count($v)>0) {
						$vtr = array();
						foreach($v as $r) {
							$vtr[] = $r['tkname'];
						}
						$txt = implode($newline,$vtr);
					} else {
						$txt = "N/A";
					}
					$v = $txt;
					break;
				case "taskattach":
					$v = (isset($task_attachments[$obj_id]) ? $task_attachments[$obj_id]['tc']." attachment(s)" : "");
					break;
				}
				break;
			case "AU":
				$v = (isset($udfs[$obj_id][$key]) ? $udfs[$obj_id][$key]['udfvalue'] : "");
				if(($update_filter == "ALL" || $update_filter=="NONEALL") && $h['type']=="AU" && isset($update_logs[$obj_id]) && count($update_logs[$obj_id])>1) { $rowspan = count($update_logs[$obj_id]); }
				$ui_key = 'action';
				switch($udf_index[$ui_key]['index'][$key]['udfilist']) {
				case "Y":
					$v = is_numeric($v) && isset($udf_list_values[$key][$v]) ? $udf_list_values[$key][$v]['udfvvalue'] : $v;
					break;
				case "M":
					$v = str_replace(chr(10),$newline,$v);
					break;
				case "D":
					$cell_format = "center";
					$v = ( strlen($v)>0 && is_numeric($v) ? date("d M Y",$v) : $v);
					break;
				case "N":
					$cell_format = "right";
					$v = is_numeric($v) ? number_format($v,$num_format['dec'],".",$num_format['thou']) : $v;
					break;
				}
				break;
			}
			$echo.= $cell[1][$cell_format].$rowspan.$tag_close.ASSIST_HELPER::decode($v).$cell[9][$cell_format];
		}
		//UPDATE LOGS
		$log = array();
		$lc = 1;
		$rowspan = 1;
		$ulogs = array();
		if(isset($update_logs[$obj_id])) {
			if($update_filter=="RECENT" || $update_filter=="NONERECENT") {
				$ulogs[] = end($update_logs[$obj_id]); 
			} else {
				$ulogs = $update_logs[$obj_id]; 
			}
			foreach($ulogs as $log) {
				if($lc>1) { $echo.= $row[9].$row[0]; }
				$lc++;
				$log_id = $log['logid'];
				foreach($update_head as $h) {
					$key = $h['key'];
					$cell_format = "normal";
					switch($h['type']) {
					case "U":
						$v = (isset($log[$key]) ? $log[$key] : $key);
						switch($key) {
						case "logdate":
							$cell_format = "center";
							$v = date("d M Y H:i:s",$v);
							break;
						case "logactdate":
							$cell_format = "center";
							$v = date("d M Y H:i",$v);
							break;
						case "logattach":
							$v = (isset($log_attachments[$log_id]) ? $log_attachments[$log_id]['tc']." attachment(s)" : "");
							break;
						default:
							$v = ASSIST_HELPER::decode($v);
						}
						break;
					case "UU":
						$v = (isset($udfs[$log_id][$key]) ? $udfs[$log_id][$key]['udfvalue'] : "");
						$ui_key = 'update';
						switch($udf_index[$ui_key]['index'][$key]['udfilist']) {
						case "Y":
							$v = is_numeric($v) && isset($udf_list_values[$key][$v]) ? $udf_list_values[$key][$v]['udfvvalue'] : $v;
							break;
						case "M":
							$v = str_replace(chr(10),$newline,$v);
							break;
						case "D":
							$cell_format = "center";
							$v = ( strlen($v)>0 && is_numeric($v) ? date("d M Y",$v) : $v);
							break;
						case "N":
							$cell_format = "right";
							$v = is_numeric($v) ? number_format($v,$num_format['dec'],".",$num_format['thou']) : $v;
							break;
						}
						break;
					}
					$echo.= $cell[1][$cell_format].$rowspan.$tag_close.ASSIST_HELPER::decode($v).$cell[9][$cell_format];
				}
			}
		} else {	//if not update logs - display blank cells
			foreach($update_head as $h) {
				$echo.= $cell[1][$cell_format].$rowspan.$tag_close.$cell[9][$cell_format];
			}
		}
		//END ROW
		$echo.= $row[9];
	}
} 

$echo.= $table[9];
$echo.= $page[9];

switch($output) {
case "CSV":
case "XLS":
	if($output == "CSV") { $ext = ".csv"; $content = 'text/plain'; } else { $ext = ".xls"; $content = 'application/ms-excel'; }
	$path = $ah->getReportPath();
	$sys_filename = $_SESSION['modref']."_".date("Ymd_His").$ext;
	$ah->saveEcho($path,$sys_filename,$echo);
	$usr_filename = "report_".date("Ymd_His").$ext;
	$ah->downloadFile2($path, $sys_filename, $usr_filename, $content);
	break;
case "onscreen":
default:
	echo $echo;
}
?>