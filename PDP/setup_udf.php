<?php 
//include("inc_ignite.php");
//error_reporting(-1);
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('txt'=>"User Defined Fields (UDFs)"),
);
$page = array("setup","udf");
$get_udf_link_headings = true;
require_once 'inc_header.php';

$setupObject = new PDP_SETUP_UDF();
$topics = $setupObject->getAllActiveTopicsFormattedForSelect();
$status = $setupObject->getAllActiveStatusesFormattedForSelect();


$result = array();
if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
		case "add":
			$link = explode("_",$_REQUEST['object_link']);
			$object = $link[0];
			$field = isset($link[1]) ? $link[1] : "";
			$var = array(
					'value'=>ASSIST_HELPER::code($_REQUEST['add_value']),
					'list'=>$_REQUEST['udfilist'],
					'ref'=>$setupObject->getModRef(),
					'custom'=>"Y",
					'sort'=>99,
					'required'=>"0",
					'default'=>"",
					'object'=>$object,
					'linkfield'=>$field,
					'linkref'=>(isset($_REQUEST['udfilinkref']) ? $_REQUEST['udfilinkref'] : ""),
			);
			$result = $setupObject->addObject($var);
			break;
		case "edit":
			$link = explode("_",$_REQUEST['object_link']);
			$object = $link[0];
			$field = isset($link[1]) ? $link[1] : "";
			$var = array(
					'value'=>ASSIST_HELPER::code($_REQUEST['edit_value']),
					'list'=>$_REQUEST['udfilist'],
					'object'=>$object,
					'linkfield'=>$field,
					'linkref'=>(isset($_REQUEST['udfilinkref']) ? $_REQUEST['udfilinkref'] : ""),
			);
			$id = $_REQUEST['udfiid'];
			$result = $setupObject->editObject($id,$var);
			break;
		case "delete":
			$id = $_REQUEST['id'];
			$result = $setupObject->deleteObject($id);
			break;
		case "restore":
			$id = $_REQUEST['id'];
			$result = $setupObject->restoreObject($id);
			break;
		case "sort":
			$result = $setupObject->sortObjects($_REQUEST);
			break;
	}
}




ASSIST_HELPER::displayResult($result);




//$ah->JSdisplayResultPrep();
//$ah->JSdisplayResult();
?>
<form name=addudf action=setup_udf.php method=post>
<input type=hidden name=act value=add />
<input type=hidden name=udfiref value='<?php echo strtoupper($modref); ?>' />
<table>
    <tr>
        <th>Ref</th>
        <th>Field Name</th>
        <th>Field Format</th>
        <th>Linked to...</th>
<!--		<th>Required</th>
        <th>Default Value</th> -->
        <th></th>
    </tr>
	<!-- ADD NEW UDF FORM -->
	<tr>
        <th><input type=hidden name=udfiid value=0 /></th>
        <td><input type=text name=add_value id="add_value" maxlength="45" /></td>
        <td><select name=udfilist f=addudf><?php
			foreach($udf_options['udfilist'] as $ui => $u) {
				echo "<option ".($ui=="Y" ? "selected" : "")." value=".$ui.">".$u['txt']."</option>";
			}
		?></select></td>
        <td>
			<select name=object_link f=addudf><?php
				foreach($udf_options['udfiobject'] as $uai => $ua) {
					echo "<option l=0 value=".$uai." class='$uai'>".$ua."</option>";
					foreach($udf_options['udfilinkfield'][$uai] as $ub) {
						echo "<option l=$ub value=".$uai."_".$ub." class='$uai'>".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
					}
				}
			?></select>
			<span id=spn_udfilinkref><br /><select name=udfilinkref></select></span>
		</td>
        <td class=center><input type=hidden name=udfirequired value=0 /><input type=hidden name=udfidefault value='' />
			<input type=button value=Add class=isubmit name=btn_add id="btn_add" />
		</td>
	</tr>
	<!-- END ADD FORM -->
<?php
$udfs = $setupObject->getAllUDFsForSetup();
if(count($udfs)==0) {
        ?>
        <tr>
            <td colspan=7>No UDFs to display</td>
        </tr>
        <?php
} else {
	foreach($udfs as $u) {
		$udf_object = strlen($u['udfiobject'])>0 ? $u['udfiobject'] : "action";
		$udf_link_field = $u['udfilinkfield'];
		$udf_link_ref = $u['udfilinkref'];
		$link_to = $udf_options['udfiobject'][$udf_object];
		if(strlen($udf_link_field)>0 && in_array($udf_link_field,$udf_options['udfilinkfield'][$udf_object])) {
			if(isset($udf_options['udfilinkfield']['headings'][$udf_link_field])) {
				$link_to.= " - ".$udf_options['udfilinkfield']['headings'][$udf_link_field]['display'];
			} else {
				$link_to.= " - ".$udf_link_field;
			}
			if(ASSIST_HELPER::checkIntRef($udf_link_ref) && isset($udf_options['udfilinkfield']['tables'][$udf_link_field])) {
				switch($udf_link_field) {
				case "tasktopicid":
					$link_to.= "<br />(".$topics[$udf_link_ref].")";
					break;
				case "taskstatusid":
					$link_to.= "<br />(".$status[$udf_link_ref].")";
					break;
				}
			}
		}
		/*if($udf_object=="update") {
			$link_to.="<br />[Remember value]";
		}*/
		echo "
			<tr>
				<th class=top>".$u['udfiid']."</th>
				<td class=b>".$u['udfivalue']."</td>
				<td class=center>".$udf_options['udfilist'][$u['udfilist']]['txt']."".($u['udfilist']=="Y" ? "<br /><input type=button value='Edit List' id=".$u['udfiid']." class=edit_list />" : "")."</td>
				<td class=center>".$link_to."</td>
				<td class=center><input type=button value='Edit UDF' id=edit_".$u['udfiid']." my_id='".$u['udfiid']."' l='".$udf_link_field."' class=edit /></td>
			</tr>
		";
	}
}
?>
</table>
</form>
<script type=text/javascript>
		var links = new Array();
		links['tasktopicid'] = new Array();
		<?php
		foreach($topics as $i => $l) {
			echo "
			links['tasktopicid'][$i] = '".$l."';";
		}
		?>
		links['taskstatusid'] = new Array();
		<?php
		foreach($status as $i => $l) {
			echo "
			links['taskstatusid'][$i] = '".$l."';";
		}
		?>
	function setUDFilinkref(form,fld) {
		for(i in links[fld]) {
			$(form+" select[name=udfilinkref]").append("<option value="+i+">"+links[fld][i]+"</option>");
		}
	}
	
$(function() {
	$("select[name=udfilistX]").change(function() {
		var form = "form[name="+$(this).prop("f")+"]";
		$(form+" input:text[name=udfidefault]").show();
		$(form+" input:text[name=udfidefault]").prop("size","20");
		$(form+" input:text[name=udfidefault]").datepicker("destroy");
		$(form+" input:button[name=btn_clear_date]").hide();
		switch($(this).val()) {
		case "D":
			$(form+" input:button[name=btn_clear_date]").show();
			$(form+" input:text[name=udfidefault]").prop("size","10");
			$(form+" input:text[name=udfidefault]").datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: 'both',
				buttonImage: '../library/jquery/css/calendar.gif',
				buttonImageOnly: true,
				dateFormat: 'yy-mm-dd'
			});
			break;
		case "Y":
			$(form+" input:text[name=udfidefault]").hide();
		}
	});
	$("select[name=object_link]").change(function() {
		var form = "form[name="+$(this).attr("f")+"]"; 
		var me = $(form+" select[name=object_link] option:selected").attr("l"); 
		$(form+" #spn_udfilinkref").show();
		$(form+" select[name=udfilinkref] option").remove();
		if(isNaN(parseInt(me)) || parseInt(me)!=0) {
			//ajax_setupUDF(form,"pop_udfilinkref","act=getList&t="+me);
			setUDFilinkref(form,me);
		} else {
			$(form+" #spn_udfilinkref").hide();
		}
	});
	$("input:button[name=btn_clear_date]").click(function() {
		var form = $(this).attr("f");
		form = "form[name="+form+"]";
		$(form+" input:text[name=udfidefault]").val("");
	});

	//ADD FORM FUNCTIONS
	$("form[name=addudf] select[name=udfilist]").trigger("change");
	$("form[name=addudf] select[name=object_link]").trigger("change");
	$("#btn_add").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("#add_value").removeClass("required");
		if($("#add_value").val().length==0) {
			$("#add_value").addClass("required");
			AssistHelper.finishedProcessing("error","Please complete the missing information.");
		} else {
			$("form[name=addudf]").submit();
		}
	});
	
	$("input:button.edit_list").click(function() {
		var i = $(this).prop("id");
		document.location.href = "setup_udf_list.php?id="+i;
	});
	
});
</script>



<div id=dlg_edit title="Edit UDF">
	<h1>Edit UDF</h1>
	<form name=editudf method="post" action="setup_udf.php">
		<input type=hidden name=udfiid id=edit_id value='0' />
		<input type=hidden name=act id=edit_act value='edit' />
	<table id=tbl_edit width="95%" class="form">
		<tr>
			<th>Ref:</th>
			<td id="td_edit_id"></td>
		</tr>
		<tr>
			<th>Field Name:</th>
			<td><input type=text name=edit_value id="edit_value" maxlength="45" /></td>
		</tr>
		<tr>
			<th>Field Format:</th>
			<td><select name=udfilist f=editudf id="edit_list"><?php
			foreach($udf_options['udfilist'] as $ui => $u) {
				echo "<option ".($ui=="Y" ? "selected" : "")." value=".$ui.">".$u['txt']."</option>";
			}
			?></select></td>
		</tr>
		<tr>
			<th>Link:</th>
			<td>
				<select name=object_link f=editudf id="edit_object_link"><?php
					foreach($udf_options['udfiobject'] as $uai => $ua) {
						echo "<option l=0 value=".$uai." class=$uai>".$ua."</option>";
						foreach($udf_options['udfilinkfield'][$uai] as $ub) {
							echo "<option l=$ub value=".$uai."_".$ub." class=$uai>".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
						}
					}
				?></select>
				<span id=spn_udfilinkref><br /><select name=udfilinkref id="edit_link_ref"></select></span>
			</td>
		</tr>
		<tr>
			<th></th>
			<td><input type=button value="Save Changes" class=isubmit name=btn_edit id="btn_edit" /></td>
		</tr>
	</table>
	<p class=float><input type=button value="Delete UDF" class=idelete id="btn_delete" /></p>
	</form>
</div>
<script type=text/javascript>
//var udfiobject = new Array();
//var udfiobject_keys = new Array();
//var udfilink = new Array();
//var udfilink_keys = new Array();
<?php
/*$uo = 0;
foreach($udf_options['udfiobject'] as $uai => $ua) {
	echo chr(10)."udfiobject[$uo] = '$ua';
	udfiobject_keys[$uo] = '$uai';
	udfilink[$uo] = new Array();
	udfilink_keys[$uo] = new Array()"; 
	foreach($udf_options['udfilinkfield'][$uai] as $ub) {
	//	echo "<option l=$ub value=".$uai."_".$ub.">".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
	}
	$uo++;
}*/
?>
	$(function() {
		$("#dlg_edit").dialog({
			modal: true,
			width: 500,
			height: 350,
			autoOpen: false
		});
		$(".edit").click(function() {
			var id = $(this).attr("my_id");
			$("#edit_id").val(id);
			$("#td_edit_id").html(id);
			var r = AssistHelper.doAjax("lib/inc_controller.php?action=UDF.get","id="+id);
			console.log(r);
			$("#edit_value").val(r['udfivalue']);
			$("#edit_list").val(r['udfilist']);
			$("#edit_object_link").val(r['object_link']);
			$("#edit_object_link").trigger("change");
			if(r['udfilinkref']!=0 && AssistString.stripos(r['object_link'],"_")!==false) {
				$("#edit_link_ref").val(r['udfilinkref']);
			}
			$("#dlg_edit").dialog("open");
		});
		$("#btn_edit").click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			$("#edit_value").removeClass("required");
			if($("#edit_value").val().length==0) {
				$("#edit_value").addClass("required");
				AssistHelper.finishedProcessing("error","Please complete the missing information");
			} else {
				$("form[name=editudf]").submit();
			}
		});
		$("#btn_delete").click(function() {
			if(confirm("Are you sure you wish to DELETE this UDF?")==true) {
				var i = $("#edit_id").val();
				document.location.href = "setup_udf.php?act=delete&id="+i;
			} 
		});
	});
</script>
</body>

</html>
