<?php
$title = array(
	array('url'=>"new.php",'txt'=>"New"),
);
$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new");
$get_udf_link_headings = false;
require_once("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

?>
        <script type ="text/javascript">
            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                })
            });

        function Validate(tform){
            var form = document.forms['newtask'];
            var count = 0;
            var message = '';
            for(var i = 0; i < form.tasktkid.length;i++){
                if(form.tasktkid.options[i].selected){
                    count++;
                }
            }
            if(count == 0){
                if(form.elements['tact']){
                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }else{
                    message += "Please assign this task to at least 1 user.\n";
                }

            }
            if(tform.taskurgencyid.value == "X"){
                message += "Please select the task priority.\n";
            }
            if(tform.taskstatusid.value == "X"){
                message += "Please select task status.\n";
            }
            if(tform.datepicker.value == ""){
                message += "Please select task deadline date.\n";
            }
            if(tform.tasktopicid.value == "X"){
                message += "Please select task topic.\n";
            }
            if(tform.taskaction.value == ""){
                message += "Please enter task details.\n"
            }
            if(message != '')
            {
                alert(message);
                return false;
            }
            return true;
        }
    </script>
<?php ASSIST_HELPER::displayResult($result); ?>
<!-- <p><input type=button value="Import <?php echo $actname; ?>" onclick="document.location.href = 'new_multiple.php';"></p> -->
        <form name=newtask method=post action=new_process.php enctype="multipart/form-data">
        	<input type=hidden name=redirect value="<?php echo $redirect; ?>" />
            <table id=tbl_action>
                <tr>
                    <th><?php $fld = "taskadduser"; echo $headings['action'][$fld]; ?>:</th>
                    <td><?php echo($tkname); ?><input type=hidden size=5 name=taskadduser value=<?php echo($tkid); ?>></td>
                </tr>
                <tr>
                    <th id=th_tasktkid><?php $fld = "tasktkid"; echo $headings['action'][$fld]; ?>:</th>
                    <td><?php
                        if($taact == 20) { //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                            echo($tkname."<input type=hidden size=5 name=tasktkid[] value=".$tkid." id=tasktkid>");
                            echo("<input type='hidden' name='tact' id='tact' value='$taact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
							$users = $ah->getAllAvailableUsersToReceiveTasksFormattedForSelect();
							$size = count($users);
                            ?>
                        <select  id="tasktkid" class=i_am_required name="tasktkid[]" multiple="multiple" size="<?php echo($size > 5 ? 10 : 5);?>">
                                <?php
								foreach($users as $id => $user) {
                                    echo("<option ".($size==1 ? "selected" : "")." name=".$id." value=".$id.">".$user."</option>");
								}
                                ?>
                        </select><br /><i>Ctrl + left click to select multiple users</i><?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th id=th_tasktopicid><?php $fld = "tasktopicid"; echo $headings['action'][$fld]; ?>:</th>
                    <td>
                        <select name="tasktopicid" id=tasktopicid class=i_am_required>
                            <option value=0>--- SELECT ---</option><?php
							$topics = $ah->getAllActiveTopicsFormattedForSelect();
							foreach($topics as $id => $topic) {
								echo "<option value=".$id.">".$topic."</option>";
							}
						?></select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskurgencyid><?php $fld = "taskurgencyid"; echo $headings['action'][$fld]; ?>:</th>
                    <td>
                        <select name="taskurgencyid" class=i_am_required>
                            <option value=X>--- SELECT ---</option><?php
							$urgency = $ah->getAllActiveUrgencyFormattedForSelect();
							foreach($urgency as $id => $urg) {
								echo "<option ".($id== 2 ? "selected" : '')." value=".$id.">".$urg."</option>";
							}
						?></select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskstatusid><?php $fld = "taskstatusid"; echo $headings['action'][$fld]; ?>:</th>
<!--                    <td>-->
<!--                        <select name="taskstatusid"  class=i_am_required>-->
<!--							<option value=X>--- SELECT ---</option>--><?php
//							$statuses = $ah->getAllActiveStatusesFormattedForSelect();
//							foreach($statuses as $id => $status) {
//								echo "<option value=".$id." ".((int)$id==4?"selected":"")." >".$status."</option>";
//							}
							?>
<!--                        </select>-->
<!--                    </td>-->
                    <?php
                        $statuses = $ah->getAllActiveStatusesFormattedForSelect();
                        $new_status_id = 4;//As per the comented out foreach above
                    ?>
                    <td><?php echo($statuses[$new_status_id]); ?><input type=hidden size=5 name=taskstatusid value=<?php echo($new_status_id); ?>></td>
                </tr>
                <tr>
                    <th id=th_taskdeadline><?php $fld = "taskdeadline"; echo $headings['action'][$fld]; ?>:</th>
                    <td><input type=text size=15 class='jdate2012 i_am_required' name=taskdeadline readonly=readonly /></td>
                </tr>
                <tr>
                    <th id=th_taskaction><?php $fld = "taskaction"; echo $headings['action'][$fld]; ?>:</th>
                    <td><textarea rows="7" name="taskaction" cols="50" class=i_am_required></textarea></td>
                </tr>
                <tr>
                    <th id="th_taskdeliver"><?php $fld = "taskdeliver"; echo $headings['action'][$fld]; ?>:</th>
                    <td><textarea rows="7" name="taskdeliver" cols="50"></textarea></td>
                </tr>

<?php
$udf_index = $ah->getUDFDetails();
foreach($udf_index as $udf_id => $udf) {
	$class = $udf['udf_object']." ".(strlen($udf['link_field'])>0 ? $udf['link_field']." ".$udf['link_ref'] : "");
	echo "<tr class='udf $class'>
		<th>".$udf['udf_value'].":</th>
		<td>";
	switch($udf['list_type']) {
	case "Y":
		echo "<select name=".$udf_id."><option selected value=0>---SELECT---</option>";
		$udf_list_items = $ah->getUDFListItems($udf_id);
		foreach($udf_list_items as $ui_id => $udf_list_item) {
			echo "<option value=".$ui_id.">".$udf_list_item['value']."</option>";
		}
		echo "</select>";
		break;
	case "T":
		echo "<input type=text name=".$udf_id." size=50 />";
		break;
	case "M":
		echo "<textarea name=".$udf_id." rows=5 cols=40></textarea>";
		break;
	case "D":
		echo "<input class='jdate2012'  type='text' name='".$udf_id."' size='15' readonly='readonly' />";
		break;
	case "N":
		 echo "<input type='text' name='".$udf_id."' size='15' class=numb />&nbsp;<br /><span class=i style='font-size: 7pt;'>Only numeric values are allowed.</span>";
		break;
	default:
		echo "<input type=text name=".$udf_id." size='50'>";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th><?php $fld = "taskattach"; echo $headings['action'][$fld]; ?>:</th>
                    <td>
						<div id=docs><input type="file" name="attachments[]" id="attachment" size="30" /></div>
						<a href="javascript:void(0)" id="attachlink" style='margin-top: 5px;'>Attach another file</a>
					</td>
                </tr>
                <tr>
					<th>Send Email:</th>
					<td>
						<input type="checkbox" name="sendEmail" id="sendEmail" value="1" />
						<i>Tick to send <?php echo strtolower($actname);?> details to yourself.</i>
                    </td>
				</tr>
                <tr>
					<th></th>
                    <td>
                        <input type="button" value="Submit" class=isubmit />
                        <input type="reset" value="Reset" name="B2" />
					</td>
                </tr>
            </table>
        </form>
<script type=text/javascript>
$(function() {
	$("th").addClass("left").addClass("top");
	$("td").addClass("top");

	$("#tasktopicid").change(function() {
		var v = $(this).val();
		$(".tasktopicid").each(function() {
			if($(this).hasClass(v)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	$("#tasktopicid").trigger("change");

	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			//$(this).css("background-color","#ffcccc").css("border","1px solid #cc0001");
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});

	$('#attachlink').click(function(){
		$("#docs").append("<br /><input type=file name=attachments[] size=30 style='margin-top: 5px;' />");
	});

	$("form[name=newtask] input:button.isubmit").click(function() {
		var form = "form[name=newtask]";
		var err = new Array();
		var my_tag = "";
		var name = "";
		var val = "";
		var fld = "";
		$(".i_am_required").removeClass("required");
		$(".i_am_required").each(function() {
			my_tag = $(this).get(0).tagName; //attr("tagName");
			name = $(this).attr("name");
			val = $(this).val();
			if(my_tag.toUpperCase()=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(!val || val.length==0 || (my_tag=="SELECT" && (val=="X" || val=="0"))) {
				if(name=="tasktkid[]") { name="tasktkid"; }
				fld = $("#th_"+name).text();
				if(fld.charAt(fld.length-1)==":") {
					fld = fld.substr(0,fld.length-1);
				}
				err.push("- "+fld);
				$(this).addClass("required");
			}
		});
		if(err.length > 0) {
			alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
		} else {
			$(form).submit();
		}
	});
	$("select.i_am_required").change(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if(!(!val || val=="X" || val=="0")) {
				$(this).removeClass("required");
			}
		}
	});
	$("textarea.i_am_required, input:text.i_am_required").keyup(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if($(this).attr("tagName")=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(val.length>0) {
				$(this).removeClass("required");
			}
		}
	});
});
</script>
    </body>
</html>

