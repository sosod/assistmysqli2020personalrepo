<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	
);
$page = array("update");
$get_udf_link_headings = false;
require_once 'inc_header.php';

$tact = $ah->getMyUserAccess();
$actname.="s";

//GET LIST OF STATUSES
$status = $ah->getAllActiveStatusesForView();
$actions = array();
$table_width = 400;

$actions = $ah->getCountOfActions($tact);
if(isset($actions['ALL'])) { $table_width+=200; }

function displayList($act,$count) {
	global $status;
	$echo = "<ul>";
		foreach($status as $key => $row) {
			$echo.= "<li>".(isset($count[$key]) ? "<a href=view_list.php?act=".$act."&s=".$key.">" : "").$row." (".(isset($count[$key]) ? $count[$key] : "0").")</a></li>";
		}
	$echo.= "	</ul>";
	return $echo;
}

echo "<table width=".$table_width.">
	<tr>
		<th width=200>My $actname</th>
		<th width=200>$actname I Assigned</th>"
		.($tact>20 ? "<th width=200>All $actname</th>" : "")
	."</tr>
	<tr>
		<td class=top>"
			.displayList("MY",$actions['MY'])
		."
			<!-- <p class=float><a href=search.php?s=0&act=MY>Advanced Search</a></p> -->
		</td>
		<td class=top>"
			.displayList("OWN",$actions['OWN'])
		."
			<!-- <p class=float><a href=search.php?s=0&act=OWN>Advanced Search</a></p> -->
		</td>".($tact>20 ? "
		<td class=top>"
			.displayList("ALL",$actions['ALL'])
		."
			<!-- <p class=float><a href=search.php?s=0&act=ALL>Advanced Search</a></p> -->
		</td>" : "")
	."</tr>
	</table>";

?>
<script type=text/javascript>
	$(function() {
		$("tr").unbind("mouseenter mouseleave");
		$("table").css("margin-left","20px");
	});
</script>



</body>

</html>
