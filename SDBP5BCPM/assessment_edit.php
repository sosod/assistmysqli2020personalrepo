<?php

$page_action = "edit";

require_once("inc_header.php");

$userObj = new SDBP5BCPM_USERACCESS();
$users = $userObj->getMenuUsersFormattedForSelect();
$user_ids = array_keys($users);

$scdObj = new SDBP5BCPM_SCORECARD();
$active_objects = $scdObj->getObjectsForEdit();
$active_users = $scdObj->getUsersWithActiveAssessments();

$employees = array_diff($user_ids, $active_users);

$scd_object_names = $scdObj->getObjectName($scdObj->getMyObjectType(),true);
$scd_object_name = $scdObj->getObjectName($scdObj->getMyObjectType());


//ASSIST_HELPER::arrPrint($pending_assessments);

?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
if(count($active_objects)>0) {
	foreach($active_objects as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>
				".$pa['status']."
			</td>
			<td class=center>
				<button class=btn_edit ".($pa['can_edit'] ? "" : " disabled=disabled ")." obj_id=".$pa['obj_id'].">Edit</button>
				<button class=btn_reassign ".($pa['can_reassign'] ? "":" disabled=disabled ")." obj_id=".$pa['obj_id'].">Reassign</button>
				".((!$pa['can_restore']) ? "<button class=btn_deactivate ".($pa['can_deactivate'] ? "":" disabled=disabled ")." obj_id=".$pa['obj_id'].">Deactivate</button>" : "<button class=btn_restore obj_id=".$pa['obj_id'].">Restore</button>")."
				<button class=btn_delete ".($pa['can_delete'] ? "":" disabled=disabled ")." obj_id=".$pa['obj_id'].">Delete</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=7>No ".$scd_object_names." available.</td>
	</tr>";
}

?>
</table>
<?php


$scdObj->displayResult(array("info","Please Note:<br />- Only ".$scd_object_names." which have been activated are available on this page.<br />- ".$scd_object_names." which have not yet been triggered can be Edited, Reassigned to a different employee or Deleted.<br />- ".$scd_object_names." which have been triggered can only be deactivated."));


?>

<div id=dlg_reassign title="Reassign <?php echo $scd_object_name; ?>">
	<h2 style='margin:0px;margin-top:5px'>Select Employee</h2>
	<h4 style='margin:0px;margin-top:10px' id=p_assess><?php echo $scd_object_name; ?>: </h4>
	<p class=b id=p_tki></p>
	<input type=hidden name=obj_id id=reassign_obj_id value='0' />
	<p><select name=sel_employee id=sel_employee>
		<option selected value=X>--- SELECT EMPLOYEE ---</option><?
		//foreach($users as $key => $user) {
		foreach($employees as $key) {
			if(isset($users[$key])) {
				$user = $users[$key];
				echo "<option value='".$key."'>".$user."</option>";
			}
		}
	?></select></p>
	<p><button id=btn_save>Save</button></p>
</div>

<div id=dlg_edit title="Edit <?php echo $scd_object_name; ?>">
	<input type=hidden id=edit_obj_id value="" />
	<p>Editing <?php echo $scd_object_name; ?> <label id=lbl_obj_id for=edit_obj_id /></label> will undo the confirmation and activation done on step 6 of the creation process and it will no longer be available for triggering.</p>
	<p>The <?php echo $scd_object_name; ?> will not be available for triggering until you complete the entire edit process, including activating the <?php echo $scd_object_name; ?> on step 6.</p>
	<p>Are you sure you wish to continue?</p>
</div>

<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");
	
	var edit_dialog
	
	$("#dlg_reassign").dialog({
		modal: true,
		autoOpen: false
	});
	$("#dlg_edit").dialog({
		modal: true,
		autoOpen: false,
		buttons: [{
           text: "Yes",
           icons: {
        		primary: "ui-icon-check"
      		},
           click: function(){
				var obj_id = $("#dlg_edit #edit_obj_id").val();
           		document.location.href = "assessment_edit_step1.php?obj_id="+obj_id;
           }
		},{
           text: "No",
           icons: {
        		primary: "ui-icon-closethick"
      		},
           click: function(){
           		$("#dlg_edit").dialog("close");
           }
		}]
	});
	
	AssistHelper.hideDialogTitlebar("id","dlg_edit");
	AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),0,"ui-state-ok");
	AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),1,"ui-state-error");
	
	$("#btn_save").button({icons: {primary:"ui-icon-disk"}
	}).click(function(e) {
		e.preventDefault();
		if($("#sel_employee").val()=="X") {
			alert("Please select an employee.");
		} else {
			AssistHelper.processing();
			var dta = "obj_id="+$("#dlg_reassign #reassign_obj_id").val()+"&employee="+$("#sel_employee").val();
			console.log(dta);
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Reassign",dta);
			console.log(result); 
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"		
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});
	
	
	$(".btn_edit").button({
		icons: {primary: "ui-icon-pencil"},
	}).click(function(e) {
		e.preventDefault();
		$("#dlg_edit #edit_obj_id").val($(this).attr("obj_id"));
		var ref = $(this).parent().parent().find("td:first").html();
		$("#lbl_obj_id").html(ref);
		$("#dlg_edit").dialog("open");
		//document.location.href = "assessment_edit_step1.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_reassign").button({
		icons: {primary: "ui-icon-person"},
	}).click(function(e) {
		e.preventDefault();
		var obj_id = $(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		var tki = $(this).parent().parent().find("td:eq(1)").html();
//		console.log(tki);
//		console.log($("#dlg_reassign #sel_employee").val());
//		console.log($("#sel_employee option[value='0001']").length);
		//$("#sel_employee option[value='0001']").prop("selected","selected");
		$("#dlg_reassign #reassign_obj_id").val(obj_id);
		$("#dlg_reassign #p_assess").html("<?php echo $scd_object_name; ?>: "+ref);
		$("#dlg_reassign #p_tki").html("Current Employee: "+tki);
		$("#dlg_reassign").dialog("open");
		//$("#sel_employee").val(tki);
		//console.log($("#dlg_reassign #sel_employee").val());
		//$("#dlg_reassign #sel_employee").find("option").each(function() {
		//	console.log($(this).val()+": "+$(this).html()+" - "+$(this).is(":selected"));
		//});
		//document.location.href = "assessment_edit_step1.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	
	$(".btn_restore").button({
		icons: {primary: "ui-icon-check"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to restore <?php echo $scd_object_name; ?> "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Restore","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"

	$(".btn_delete").click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete <?php echo $scd_object_name; ?> "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_deactivate").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to deactivate <?php echo $scd_object_name; ?> "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Deactivate","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

});
</script>