<?php
$page_section = "report";
require_once("inc_header.php");

$me = new SDBP5BCPM_REPORT();

if(isset($_REQUEST['i'])) {
	$report_id = $_REQUEST['i'];
	$me->setReportID($_REQUEST['i']);
	$me->arrPrint($_REQUEST);
	echo $me->getReportTitle();
	
	
	if($report_id=="dashboard") {
		$period_id = $_REQUEST['filter']['period_id'];
		$assessObj = new SDBP5BCPM_ASSESSMENT();
		$assessments = $assessObj->getAssessmentByPeriod($period_id);
		$page_action = SDBP5BCPM_TRIGGER::REPORT_ASSESS;
			$display_kpa=false;
			$display_cc=false;
			$display_bonus = false;
			$display_js = false;
			$user_ids = false;
		
		foreach($assessments as $as) {
			echo "<h2>".$as['employee']."</h2>";
			$assess_id = $as[$assessObj->getIDFieldName()];
			
			
			include("common/scoresheet.php");
		}
		
		
		$me->arrPrint($assessments);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
} else {
	$apObj = new SDBP5BCPM_PERIOD();
	$periods = $apObj->getAssessmentPeriodsForSelect();
	$available_periods = count($periods)>0?true:false;
	$reports = $me->getListOfReports();
	//$me->arrPrint($reports);
	?>
	
	<table class=list>
		<tr>
			<th>Report</th>
			<th>Description</th>
			<th>Filter</th>
			<th></th>
		</tr>
		<?php
	foreach($reports as $i => $r) {
		echo "
		<tr>
			<td class=b>".$r['title']."</td>
			<td>".$r['description']."</td>
			<td>
				<select id=filter_".$i." f_type=period_id>
					<!-- <option value=ALL>All periods</option> -->
					";
					$selected = false;
			foreach($periods as $pi => $p) {
				echo "<option ".(!$selected?"selected":"")." value=".$pi.">".$p."</option>";
				$selected= true;
			}
		echo "
				</select></td>
			<td><button class='btn_generate size_button' id=".$i.">Generate</button></td>
		</tr>
		";
	}	
		?>
	</table>
	<script type=text/javascript>
		$(".btn_generate").button({
			icons: {primary: "ui-icon-script"},
		}).click(function() {
			AssistHelper.processing();
			var i = $(this).prop("id");
			var f = $("#filter_"+i).val();
			var ft = $("#filter_"+i).attr("f_type");
			document.location.href = 'report_fixed.php?i='+i+'&filter['+ft+']='+f;
		});
		resizeButtons($(".btn_generate"));
	</script>
	<?php
}
?>