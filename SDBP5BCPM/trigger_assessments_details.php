<?php

$page_action = "trigger"; 

require_once("inc_header.php");

$obj_id = $_REQUEST['obj_id'];
$scd_id = $obj_id;
$scdObj = new SDBP5BCPM_SCORECARD();
$scorecard = $scdObj->getAObjectSummary($obj_id);
//die();

$userObj = new SDBP5BCPM_USERACCESS();
//LIMIT FOR USERS WITH ACCESS TO MODERATE AND REVIEW
$mod_users = $userObj->getModeratorUsersFormattedForSelect();
$final_users = $userObj->getFinalReviewUsersFormattedForSelect();

$mod_user_select = "";
foreach($mod_users as $key => $u) {
	if($key!=$scorecard['employee_tkid']) {
		$mod_user_select.="<option value=".$key.">".$u."</option>";
	}
}
$final_user_select = "";
foreach($final_users as $key => $u) {
	if($key!=$scorecard['employee_tkid']) {
		$final_user_select.="<option value=".$key.">".$u."</option>";
	}
}


$assessObj = new SDBP5BCPM_ASSESSMENT();
$assessments = $assessObj->getAssessmentsForEdit($obj_id);

$assessment_keys = array_keys($assessments);

$triggerObj = new SDBP5BCPM_TRIGGER();

if(count($assessment_keys)>0) {
	$triggers = $triggerObj->getTriggersByParentID($assessment_keys);
} else {
	$triggers = array();
}

$apObj = new SDBP5BCPM_PERIOD();
$periods = $apObj->getAssessmentPeriodsForSelect(true,$obj_id);
$available_periods = count($periods)>0?true:false;

//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($triggers);

//echo "<h2>".$scorecard['employee']."</h2><h3>".$scdObj->getObjectName($scdObj->getMyObjectType()).": ".$scorecard['ref']."</h3>";
echo $scdObj->getAssessmentScoreHeading($obj_id);

?>
<table class=list id=tbl_list>
	<tr>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Assessments</th>
	</tr>
<?php
$pa = $scorecard;
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
		</tr>";

?>
</table>
<?php 

?>
<table class='tbl-container not-max'><tr><td>
<?php if($available_periods) { ?><span class=float style='padding-top: 20px;'><button id=btn_add scd_id=<?php echo $obj_id; ?>>Add New</button></span><?php } ?>
<h2>Triggers</h2>
<table class=list>
	<tr>
		<th rowspan=2>Ref</th>
		<th rowspan=2>Period</th>
		<th colspan=2>Self Assessment</th>
		<th colspan=3>Moderation</th>
		<th colspan=3>Final Review</th>
		<th rowspan=2></th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Deadline /<br />Date Completed</th>
		<th>Moderator</th>
		<th>Status</th>
		<th>Deadline /<br />Date Completed</th>
		<th>Reviewer</th>
		<th>Status</th>
		<th>Deadline /<br />Date Completed</th>
	</tr>	
	<?php
	if(count($assessments)==0) {
		echo "<tr><td colspan=11>No Assessments have been triggered.</td></th>";
	} else {
		foreach($assessments as $a_id => $as) {
			$assessment_status = "SELF";
			$show_edit = false;
			$active_mods = count($triggers[$a_id][$triggerObj->getModeratorType()])>0;
			$mod_count = count($triggers[$a_id][$triggerObj->getModeratorType()])>0 ? count($triggers[$a_id][$triggerObj->getModeratorType()]) : 1;
			$mod_keys = array_keys($triggers[$a_id][$triggerObj->getModeratorType()]);
			$mod_triggers = $triggers[$a_id][$triggerObj->getModeratorType()];
			$self_status = $triggers[$a_id][$triggerObj->getSelfType()]['statusid'];
			switch($self_status) {
				case 2: $icon = "info"; $show_edit = true; break;
				case 3: $icon = "ok"; break;
				case 1: 
				default:
					$show_edit = true;
					$icon = "error"; 
					break;
			}
			$date_class = "center ";
			if($self_status<3) {
				$deadline = strtotime($triggers[$a_id][$triggerObj->getSelfType()]['date']." 23:59:59"); 
				if($deadline<strtotime(date("d F Y H:i:s"))) {
					$date_class.= " b red";
				}
			}
			echo "
			<tr>
				<td class=b rowspan=".$mod_count.">".$as['ref']."</td>
				<td rowspan=".$mod_count." id=td_period".$a_id.">".$as['period']."</td>
				<td rowspan=".$mod_count." id=td_self_status".$a_id.">".$assessObj->getDisplayIconAsDiv($icon)." ".$triggers[$a_id][$triggerObj->getSelfType()]['status']."</td>
				<td rowspan=".$mod_count." id=td_self_date".$a_id." class='$date_class'>".$triggers[$a_id][$triggerObj->getSelfType()]['date']."</td>
			";
			if($active_mods) {
				$assessment_status = "MOD";
				$m = $mod_triggers[$mod_keys[0]];
				unset($mod_triggers[$mod_keys[0]]);
				switch($m['statusid']) {
					case 2: $icon = "info";  $show_edit = true; break;
					case 3: $icon = "ok"; break;
					case 1: 
					default: $show_edit = true;
						$icon = "error"; break;
				}
				echo "
					<td><span title='".$m['user']."'>Moderator ".$mod_keys[0]."</span></td>
					<td id=td_mod_status_".$a_id."_1>".$assessObj->getDisplayIconAsDiv($icon)." ".$m['status']."</td>
					<td id=td_mod_date_".$a_id."_1>".$m['date']."</td>
				";
			} else {
				echo "<td colspan=3 class=center><button class=btn_mod_trigger assess_id=".$a_id.">Trigger moderation</button></td>";
			}
			if(isset($triggers[$a_id][$triggerObj->getFinalReviewType()]['status'])) {
				$assessment_status = "FINAL";
				$final = $triggers[$a_id][$triggerObj->getFinalReviewType()];
				switch($final['statusid']) {
					case 2: $icon = "info";  $show_edit = true; break;
					case 3: $icon = "ok"; break;
					case 1: 
					default:  $show_edit = true;
						$icon = "error"; break;
				}
				echo "
					<td rowspan=".$mod_count.">".$triggers[$a_id][$triggerObj->getFinalReviewType()]['user']."</td>
					<td rowspan=".$mod_count.">".$assessObj->getDisplayIconAsDiv($icon)." ".$triggers[$a_id][$triggerObj->getFinalReviewType()]['status']."</td>
					<td rowspan=".$mod_count.">".$triggers[$a_id][$triggerObj->getFinalReviewType()]['date']."</td>
				";
			} elseif($active_mods) {
				echo "<td colspan=3 rowspan=".$mod_count." class=center><button class=btn_final_trigger mod_count=".$mod_count." assess_id=".$a_id.">Trigger final</button></td>";
			} else {
				echo "<td colspan=3 rowspan=".$mod_count."></td>";
			}
				echo "<td rowspan=".$mod_count.">".($show_edit ? "<button class=btn_edit status=".$assessment_status." assess_id=".$a_id.">Edit</button>" : "<button class=btn_view assess_id=".$a_id.">View</button>")."</td>";
			echo "</tr>";
			foreach($mod_triggers as $mkey => $m) {
				switch($m['statusid']) {
					case 2: $icon = "info"; break;
					case 3: $icon = "ok"; break;
					case 1: 
					default:
						$icon = "error"; break;
				}
				echo "
				<tr>
					<td><span title='".$m['user']."'>Moderator ".$mkey."</span></td>
					<td id=td_mod_status_".$a_id."_".$mkey.">".$assessObj->getDisplayIconAsDiv($icon)." ".$m['status']."</td>
					<td id=td_mod_date_".$a_id."_".$mkey.">".$m['date']."</td>
				</tr>
				";
			}
		}
	}
	?>
	

</table>
</td></tr></table>
<p>&nbsp;</p>
<div id=dlg_new title="Trigger Self-Assessment">
<?php if($available_periods) { 
	
	$js = "";
	
	?>
	<form name=frm_new>
		<input type=hidden name=scd_id value='<?php echo $scd_id; ?>' id=new_scd_id />
		<input type=hidden name=tkid value='<?php echo $scorecard['employee_tkid']; ?>' id=new_tkid />
		<input type=hidden name=trigger_type value='<?php echo $triggerObj->getSelfType(); ?>' id=new_type />
		<input type=hidden name=assess_id value='0' id=edit_assess_id />
		<table class=form id=tbl_form>
			<tr>
				<th>Assessment Period:</th>
				<td><select name=period id=period>
					<option value=X selected>--- SELECT ---</option><?php
					foreach($periods as $key => $p) {
						echo "<option value=".$key.">".$p."</option>";
					} 
					?></select></td>
			</tr>
			<tr>
				<th>Self Assessment deadline:</th>
				<td><?php $js.= $displayObject->drawDatePicker("",array('id'=>"self_deadline",'name'=>"self_deadline"),array('minDate'=>"0")); ?></td>
			</tr>
		</table>
	<p>
		<span class='float' id=spn_self_delete><button class=btn_delete status='<?php echo $triggerObj->getSelfType(); ?>' id=btn_self_delete>Delete Trigger</button></span>
		<button id=btn_save_new class=btn_save>Save Trigger</button>
	</p>
	</form>
<?php } ?>
</div>
 
<div id=dlg_mod title="Trigger Moderation">
	<form name=frm_mod>
		<input type=hidden name=assess_id id=mod_assess_id value='' />
		<input type=hidden name=trigger_type id=mod_trigger_type value='<?php echo $triggerObj->getModeratorType(); ?>' />
		<span id=mod_warning><?php ASSIST_HELPER::displayResult(array("info","If the self assessment has not yet been completed, triggering moderation will automatically mark the self assessment as Closed and can no longer be completed by the employee.")); ?></span>
		<table class=form id=tbl_mod_form>
			<tr>
				<th>Assessment Period:</th>
				<td colspan=2 id=td_mod_period_display>DISPLAY HERE</td>
			</tr>
			<tr>
				<th>Self Assessment:</th>
				<td colspan=2 id=td_mod_self_status>Completed</td>
			</tr>
			<?php for($i=1;$i<11;$i++) { ?>
			<tr>
				<th>Moderator <?php echo $i; ?>:</th>
				<td><select name=tkid[<?php echo $i; ?>] class=sel_mod_tkid><option value=X selected>--- SELECT ---</option><?php echo $mod_user_select; ?></select></td>
				<td><?php $js.= $displayObject->drawDatePicker("",array('id'=>"mod_deadline_".$i,'name'=>"mod_deadline[$i]"),array('minDate'=>"0")); ?></td>
			</tr>
			<?php } ?>
		</table>
		<p>
			<span class='float' id=spn_mod_delete><button class=btn_delete status='<?php echo $triggerObj->getModeratorType(); ?>' id=btn_mod_delete>Delete Trigger</button></span>
			<button class=btn_save id=btn_save_mod>Save Trigger</button>
		</p>
	</form>
</div>

<div id=dlg_final title="Trigger Final Review">
	<form name=frm_final>
		<input type=hidden name=assess_id id=final_assess_id value='' />
		<input type=hidden name=trigger_type id=final_trigger_type value='<?php echo $triggerObj->getFinalReviewType(); ?>' />
		<span id=final_warning><?php ASSIST_HELPER::displayResult(array("info","If any moderators are not yet complete, triggering final review will automatically mark the moderation as Closed and can no longer be completed by the employee.")); ?></span>
		<table class=form id=tbl_form_final width=100%>
			<tr>
				<th>Assessment Period:</th>
				<td id=td_final_period_display >DISPLAY HERE</td>
			</tr>
			<tr>
				<th>Self Assessment:</th>
				<td id=td_final_self_status>Completed</td>
			</tr>
			<?php for($i=1;$i<11;$i++) { ?>
			<tr id=tr_mod_<?php echo $i; ?>>
				<th>Moderator <?php echo $i; ?>:</th>
				<td id=td_final_mod_status_<?php echo $i; ?>>Completed</td>
			</tr>
			<?php } ?>
			<tr>
				<th>Final Reviewer:</th>
				<td><select name=final_tkid id=final_tkid><option value=X selected>--- SELECT ---</option><?php echo $final_user_select; ?></select></td>
			</tr>
			<tr>
				<th>Final Review Deadline:</th>
				<td><?php $js.= $displayObject->drawDatePicker("",array('id'=>"final_deadline",'name'=>"final_deadline"),array('minDate'=>"0")); ?></td>
			</tr>
		</table>
		<p>
			<span class='float' id=spn_final_delete><button class=btn_delete status='<?php echo $triggerObj->getFinalReviewType(); ?>' id=btn_final_delete>Delete Trigger</button></span>
			<button class=btn_save id=btn_save_final>Save Trigger</button></p>
	</form>
</div>

   
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	
	//$("table.noborder").find("td").addClass("noborder");
	
	<?php echo $js;  ?>
	$("#dlg_new").dialog({
		autoOpen: false,
		modal: true,
		width: "600px"
	});
	$("#dlg_mod").dialog({
		autoOpen: false,
		modal: true,
		width: "600px"
	});
	$("#dlg_final").dialog({
		autoOpen: false,
		modal: true,
		width: "600px"
	});
	
	//ADD A NEW ASSESSMENT / TRIGGER - Self Assessment
	$("#btn_add").button({icons: {primary:"ui-icon-circle-plus"}
	}).click(function(e) {
		e.preventDefault();
		//Reset period drop down (disabled in Edit)
		$("#dlg_new #period").val("X").prop("disabled","");
		//Reset Deadline input (disabled in Edit)
		$("#self_deadline").val("").prop("disabled","");
		//make sure that save button is showing
		$("#btn_save_new").show();
		//hide delete button (edit function)
		$("#spn_self_delete").hide();
		//show dialog
		$("#dlg_new").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"		
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});
	
	//TRIGGER MODERATION OF AN EXISTING ASSESSMENT
	$(".btn_mod_trigger").button({icons: {primary:"ui-icon-star"}
	}).click(function(e) {
		e.preventDefault();
		//Get Assessment ID and populate form
		var a = $(this).attr("assess_id");
		$("#mod_assess_id").val(a);
		//Display Period
		var p = $("#td_period"+a).html();
		$("#td_mod_period_display").html(p);
		//Display Self-Assessment status
		var s = $("#td_self_status"+a).html()+" ("+$("#td_self_date"+a).html()+")";
		$("#td_mod_self_status").html(s);
		
		//show moderator fields
		$("#tbl_mod_form tr:gt(0)").show();
		//show warning
		$("#mod_warning").show();
		
		//hide delete button (edit function)
		$("#btn_mod_delete").hide();
		//show save button (hidden in edit)
		$("#btn_save_mod").show()
		
		//open dialog
		$("#dlg_mod").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});

	//TRIGGER FINAL REVIEW OF AN EXISTING ASSESSMENT
	$(".btn_final_trigger").button({icons: {primary:"ui-icon-star"}
	}).click(function(e) {
		e.preventDefault();
		//Get the Assessment ID
		var a = $(this).attr("assess_id");
		$("#final_assess_id").val(a);
		
		//Get the Period detail
		var p = $("#td_period"+a).html();
		$("#td_final_period_display").html(p);
		
		//Get the Self Assessment status
		var s = $("#td_self_status"+a).html()+" ("+$("#td_self_date"+a).html()+")";
		$("#td_final_self_status").html(s);
		
		//show trigger details (hidden in edit)
		$("#tbl_form_final tr:gt(0)").show();
		//show warning
		$("#final_warning").show();
		
		//Get moderator status
		var mc = $(this).attr("mod_count");
		var ms = "";
		for(var m=1;m<=11;m++) {
			if(m>mc) {
				$("#tr_mod_"+m).hide();
			} else {
				$("#tr_mod_"+m).show();
				ms = $("#td_mod_status_"+a+"_"+m).html()+" ("+$("#td_mod_date_"+a+"_"+m).html()+")";
				$("#td_final_mod_status_"+m).html(ms);
			}
		}
		
		//hide delete button
		$("#btn_final_delete").hide();
		//show save button
		$("#btn_save_final").show();
		
		//open dialog
		$("#dlg_final").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"		
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});

	//format all save buttons
	$(".btn_save").button({icons: {primary:"ui-icon-disk"}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"		
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"100%"	
	});

	//delete buttons
	$(".btn_delete").button({icons: {primary:"ui-icon-trash"}
	}).click(function(e) {
		e.preventDefault();
		//alert("PROGRAM ME");
		if(confirm("Are you sure you wish to delete this trigger?")) {
			AssistHelper.processing();
			var as = $(this).attr("status");
			switch(as) {
				case "SELF":
					var a = $("#edit_assess_id").val();
					break;
				case "MOD":
					var a = $("#mod_assess_id").val();
					break;
				case "FINAL":
					var a = $("#final_assess_id").val();
					break;
			}
			var dta = "assess_id="+a+"&type="+as;
			console.log(dta);
			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Trigger.Delete",dta);
			console.log(r);
			if(r[0]=="ok") {
				var url = "trigger_assessments_details.php?obj_id=<?php echo $scd_id; ?>";
				AssistHelper.finishedProcessingWithRedirect(r[0],r[1],url);
			} else {
				//alert(r[1]);
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #CC0001"		
	});
	resizeButtons($(".btn_delete"));

	//format View buttons	
	$(".btn_view").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "trigger_assessment_view.php?obj_id="+$(this).attr("assess_id");
	});
	resizeButtons($(".btn_view"));
	
	//Edit button
	$(".btn_edit").button({
		icons: {primary: "ui-icon-pencil"},
	}).click(function(e) {
		e.preventDefault();
		var a = $(this).attr("assess_id");
		var as = $(this).attr("status");
		switch(as) {
			case "SELF":
				//set assessment id
				$("#dlg_new #edit_assess_id").val(a);
				//set ajax info to get details
				var dta = "assess_id="+a+"&type="+as;
				var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Trigger.getEditObjectByParentID",dta);
				//populate fields and disable to prevent changing of data
				$("#self_deadline").val(r['deadline_formatted']).prop("disabled","disabled");
				$("#dlg_new #period").val(r['period_id']).prop("disabled","disabled");
				//show delete button
				$("#spn_self_delete").show();
				//hide save button
				$("#btn_save_new").hide();
				//open dialog
				$("#dlg_new").dialog("open");
				
				break;
			case "MOD":
				//set assessment id
				var a = $(this).attr("assess_id");
				$("#mod_assess_id").val(a);
				//hide warning
				$("#mod_warning").hide();
				//hide table rows
				$("#tbl_mod_form tr:gt(0)").hide();
				//set Period info
				var p = $("#td_period"+a).html();
				$("#td_mod_period_display").html(p);
				//var s = $("#td_self_status"+a).html()+" ("+$("#td_self_date"+a).html()+")";
				//$("#td_mod_self_status").html(s);

				//show delete button
				$("#btn_mod_delete").show();
				//hide save button
				$("#btn_save_mod").hide();
			
				$("#dlg_mod").dialog("open");
				break;
			case "FINAL":
				//set assessment id
				var a = $(this).attr("assess_id");
				$("#final_assess_id").val(a);
				//hide table rows
				$("#tbl_form_final tr:gt(0)").hide();
				//hide warning
				$("#final_warning").hide();
				//set Period info
				var p = $("#td_period"+a).html();
				$("#td_final_period_display").html(p);

				//show delete button
				$("#btn_final_delete").show();
				//hide save button
				$("#btn_save_final").hide();
			
				$("#dlg_final").dialog("open");
				break;
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	
	
	
	
	
	
	$("#btn_save_new").click(function(e) {
		e.preventDefault();
		var err = false;
		AssistHelper.processing();
		if($("#self_deadline").val().length==0 || $("#period").val().length==0 || $("#period").val()=="X") {
			err = true;
			AssistHelper.finishedProcessing("error","Please complete all the required fields.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_new]"));
			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Add",dta);
			if(r[0]=="ok") {
				var url = "trigger_assessments_details.php?obj_id=<?php echo $scd_id; ?>";
				AssistHelper.finishedProcessingWithRedirect(r[0],r[1],url);
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	});
	

	$("#btn_save_mod").click(function(e) {
		e.preventDefault();
		var err = true;
		AssistHelper.processing();
		var i = 1;
		$(".sel_mod_tkid").each(function() {
			if($(this).val().length>0 && $(this).val()!="X" && $("#mod_deadline_"+i).val().length>0) {
				err = false;
			} else {
				if($(this).val().length>0 && $(this).val()!="X") {
					$(this).addClass("required");
				} else {
					 $("#mod_deadline_"+i).addClass("required");
				}
			}
		});
		
		if(err) {
			AssistHelper.finishedProcessing("error","Please complete all the required fields.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_mod]"));
			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Trigger.Add",dta);
			if(r[0]=="ok") {
				var url = "trigger_assessments_details.php?obj_id=<?php echo $scd_id; ?>";
				AssistHelper.finishedProcessingWithRedirect(r[0],r[1],url);
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	});
	

	$("#btn_save_final").click(function(e) {
		e.preventDefault();
		var err = false;
		AssistHelper.processing();
		if($("#final_deadline").val().length==0 || $("#final_tkid").val().length==0 || $("#final_tkid").val()=="X") {
			err = true;
			AssistHelper.finishedProcessing("error","Please complete all the required fields.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_final]"));
			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Trigger.Add",dta);
			if(r[0]=="ok") {
				var url = "trigger_assessments_details.php?obj_id=<?php echo $scd_id; ?>";
				AssistHelper.finishedProcessingWithRedirect(r[0],r[1],url);
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	});

	
	/*
	
	$(".btn_restore").button({
		icons: {primary: "ui-icon-check"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to restore Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Restore","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"

	$(".btn_delete").click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_deactivate").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to deactivate Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Deactivate","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
*/
});
</script>