<?php
require_once("inc_header.php");

$userObj = new SDBP5BCPM_USERACCESS();
$users = $userObj->getMenuUsersFormattedForSelect();
$user_ids = array_keys($users);

$scdObj = new SDBP5BCPM_SCORECARD();
$pending_objects = $scdObj->getPendingObjects();
$active_users = $scdObj->getUsersWithActiveAssessments();

$employees = array_diff($user_ids, $active_users);

?>
<p><button id=btn_add>Create New</button></p>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th></th>
	</tr>
<?php
if(count($pending_objects)>0) {
	foreach($pending_objects as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class=''>".$pa['employee']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>
				<button class=btn_continue obj_id=".$pa['obj_id'].">Continue</button>
				<button class=btn_delete obj_id=".$pa['obj_id'].">Delete</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=7>No pending assessments awaiting completion.</td>
	</tr>";
}

?>
</table>
<?php




?>
<div id=dlg_new title="Create New">
	<h2>Select Employee</h2>
	<p><?php
	if(count($employees)>0) {
		echo "<select name=sel_employee id=sel_employee>
		<option selected value=X>--- SELECT EMPLOYEE ---</option>";
		//foreach($users as $key => $user) {
		foreach($employees as $key) {
			if(isset($users[$key])) {
				$user = $users[$key];
				echo "<option value=".$key.">".$user."</option>";
			}
		}
		echo "</select>";
	} else {
		echo "No employees available.";
	}
	?></p>
	<p><button id=btn_new_next1>Next </button></p>
</div>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list td:eq(1)").removeClass("center");
	$("#dlg_new").dialog({
		modal: true,
		autoOpen: false
	});
	$("#btn_add").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).click(function() {
		$("#dlg_new").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	$("#btn_new_next1").button({
		icons: {secondary: "ui-icon-circle-arrow-e"},
	}).click(function() {
		$("#sel_employee").css("background-color","");
		var i = $("#sel_employee").val();
		if(i.length<4) {
			$("#sel_employee").css("background-color","#cc0001");
			alert("Please select an employee.");
		} else {
			AssistHelper.processing();
			var dta = "step=0&obj_id=0&employee_id="+i;
			console.log(dta);
			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Add",dta);
			if(r[0]=="ok") {
				document.location.href = "assessment_create_step1.php?obj_id="+r['obj_id']+"&r[]=ok&r[]="+r[1];
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-right":"25px","font-size":"80%"
	});
	
	$(".btn_continue").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"

	$(".btn_delete").click(function() {
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_create.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
});
</script>