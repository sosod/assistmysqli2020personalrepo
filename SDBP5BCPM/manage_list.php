<?php
//$page_action = "SELF";//SDBP5BCPM_TRIGGER::SELF_ASSESS;

/******
 * Requirements:
 * page_action = SELF || MOD || FINAL
 * page_section = self || moderation || final
 */

require_once("inc_header.php");

//end();
$scdObj = new SDBP5BCPM_SCORECARD();
$assessObj = new SDBP5BCPM_ASSESSMENT();
$triggerObj = new SDBP5BCPM_TRIGGER();

$triggers = $triggerObj->getTriggersForCompletion($page_action);
$active_objects = $triggers['objects'];
$summary_details = $triggers['details'];

$scd_object_names = $scdObj->getObjectName($scdObj->getMyObjectType(),true);
$scd_object_name = $scdObj->getObjectName($scdObj->getMyObjectType());

$asmt_object_names = $assessObj->getObjectName($assessObj->getMyObjectType(),true);
$asmt_object_name = $assessObj->getObjectName($assessObj->getMyObjectType());


?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<?php if($page_action!=$triggerObj->getSelfType()) { ?><th>Employee</th><?php } ?>
		<th>Assessment Period</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Deadline</th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
if(count($active_objects)>0) {
	foreach($active_objects as $pa) {
		$pa['count'] = $summary_details[$pa['parent_id']]['count'];
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			".($page_action!=$triggerObj->getSelfType() ? "<td>".$pa['employee']."</td>" : "")."
			<td class='' >".$pa['period']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>
				".$pa['deadline']."
			</td>
			<td class=center>
				".$pa['status']."
			</td>
			<td class=center>
				".($pa['can_continue'] ? "<button class=btn_continue obj_id=".$pa['id'].">Complete</button>" : "<button class=btn_view obj_id=".$pa['id'].">View</button>")."
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=9>No ".$asmt_object_names." available.</td>
	</tr>";
}

?>
</table>



<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");
	
	$(".btn_view").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "manage_<?php echo $page_section; ?>_view.php?view_type=<?php echo $page_action; ?>&obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_continue").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		document.location.href = "manage_<?php echo $page_section; ?>_details.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

});
</script>