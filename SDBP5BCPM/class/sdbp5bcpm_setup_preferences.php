<?php

class SDBP5BCPM_SETUP_PREFERENCES extends SDBP5BCPM {
	
	private $questions = array();
	
	private $table_name = "_setup_preferences";
	private $field_prefix = "pref";
	
	private $fields = array(
		'pref_id'=>"id",
		'pref_name'=>"name",
		'pref_description'=>"description",
		'pref_type'=>"type",
		'pref_value'=>"value",
		'pref_default'=>"default_value",
		'pref_options'=>"options",
		'pref_sort'=>"sort",
		'pref_status'=>"status",
	);
	private $field_names = array();

	const LOG_TABLE = "setup";
	
	public function __construct() {
		parent::__construct();
		$this->questions = $this->getQuestionsFromDB();
		$this->field_names = array_flip($this->fields);
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function updateObject($var,$attach=array()) {
		unset($attach);	//remove unnecessary array from the inc_controller
		$questions = $this->getQuestions();
		$mar = 0;
		$r_err = array();
		$r_ok = array();
		$err = false;
		$changes = array();
		foreach($questions as $key => $q) {
			$new = $var['Q_'.$key];
			
			$old = $q['value'];
			if($new != $old) {
				//update database
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->field_names['value']." = '".$new."' WHERE ".$this->field_names['id']." = ".$key;
				$mar = $this->db_update($sql);
				if($mar>0) {
					$options = explode("|",$q['options']);
					$changes[$key] = array('to'=>"",'from'=>"");
					foreach($options as $opt) {
						$opts = explode("_",$opt);
						if($new==$opts[0]) {
							$changes[$key]['to'] = $opts[1];
						} elseif($old==$opts[0]) {
							$changes[$key]['from'] = $opts[1];
						}
						if($changes[$key]['to']!="" && $changes[$key]['from']!="") { break; }
					}
					$r_ok[] = $key;
				} else {
					$r_err[] = $key;
					$err = true;
				}
			}
		}
		if(count($changes)>0) {
			$changes['user']=$this->getUserName();
			
			$log_var = array(
				'section'	=> "PREF",
				'object_id'	=> "",
				'changes'	=> $changes,
				'log_type'	=> SDBP5BCPM_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			if(!$err) {
				return array("ok","Changes saved successfully.");
			} elseif(count($r_ok)==0) {
				return array("error","An error occurred while trying to save changes.");
			} elseif(count($r_err)>0) {
				$return = array("okerror",count($r_ok)." changes saved successfully, but the following changes did not save: PREF".implode(", PREF",$r_err).".");
				return $return;
			} else {
				return $array("okerror","An unknown error occurred.  Please review the page and confirm that your changes have been saved.");
			}
		} elseif($err) {
			return array("error","An error occurred while trying to save changes.");
		}
		return array("info","No change found to be saved.");
	}
	
	
	
	
	
	
	
	
	
	
	
	/***************************
	 * GET functions
	 */
	public function getTableName() { return $this->getDBRef().$this->table_name; }
	public function getTableFieldPrefix() { return $this->field_prefix; }
	/**
	 * Returns the list of questions to be displayed on the Setup > Preferences page
	 */
	public function getQuestions() {
		return $this->questions;
	}
	
	/**
	 * Returns the questions from the database
	 */
	public function getQuestionsFromDB() {
		$sql = "SELECT ";
		$f = array();
		foreach($this->fields as $fld => $key) {
			$f[] = $fld." as ".$key;
		}
		$sql.=implode(", ",$f)." FROM ".$this->getTableName()." WHERE pref_status & ".SDBP5BCPM::ACTIVE." = ".SDBP5BCPM::ACTIVE." ORDER BY pref_sort";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}
	
	public function getQuestionForLogs()  {
		$data = array();
		foreach($this->questions as $key => $q) {
			$data[$key] = $q['name'];
		}
		return $data;
	}
	
	

	
	/******************
	 * SET functions
	 */
	private function updateQuestionsWithAnswersFromDB() {
		$questions = $this->getQuestions();
		foreach($questions as $key => $q) {
			
		}
	}
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>