<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 23 May 2017
 * Authors: Janet Currie
 * 
 */
 
class SDBP5BCPM_ASSESSMENT extends SDBP5BCPM {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_scdid";
	protected $name_field = "_periodid";
    /*
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "ASSESSMENT";
    const TABLE = "assessments";
    const TABLE_FLD = "asmt";
    const REFTAG = "SA";
	const LOG_TABLE = "assessments";
	/**
	 * STATUS CONSTANTS
	 */
	
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
/*		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$sql = "INSERT INTO ".$this->getTableName()." SET
				asmt_scdid = ".$var['scd_id']."
				, asmt_periodid = ".$var['period']."
				, asmt_status = ".self::INACTIVE."
				, asmt_insertuser = '".$this->getUserID()."'
				, asmt_insertdate = now()
				";
		$id = $this->db_insert($sql);
		if($id>0) {
			$log_var = array(
				'scd_id'=>$var['scd_id'],
				'prd_id'=>$var['period'],
			);
			$this->logMyAction($id, "CREATE", "", $var,$log_var);
			$var['assess_id'] = $id;
			$triggerObj = new SDBP5BCPM_TRIGGER();
			$result = $triggerObj->addObject($var);
			if($result[0]=="ok") {
				$sql = "UPDATE ".$this->getTableName()." SET asmt_status = ".self::ACTIVE." WHERE asmt_id = ".$id;
				$mar = $this->db_update($sql);
				if($mar>0) {
					
					return array("ok","New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." successfully created.");
				}
			}
		}
		return array("error","An error occurred.  Please reload the page and try again.");
	}

	public function deleteObject($id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$id;
		$old = $this->mysql_fetch_one($sql);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$mar = $this->db_update($sql);
			$log_var = array(
				'scd_id'=>$old[$this->getTableField().'_scdid'],
				'prd_id'=>$old[$this->getTableField().'_periodid'],
			);
			$var = array('status'=>self::INACTIVE);
			$this->logMyAction($id, "DELETE", $old, $var,$log_var);		
	}

	/*

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type']; 
		$ref = $var['ref'];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}
		
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
    
	public function getAssessmentsForEdit($parent_id) {
		$tbl_fld = $this->getTableField();
		$prdObj = new SDBP5BCPM_PERIOD();
		$periods = $prdObj->getAllAssessmentPeriodsForSelect();
		$sql = "SELECT A.*
				, CONCAT('".$this->getRefTag()."', A.".$this->getIDFieldName().") as ref
				, A.".$tbl_fld."_periodid as period_id
				FROM ".$this->getTableName()." A 
				WHERE A.".$this->getParentFieldName()." = $parent_id 
				AND (A.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$triggers = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		foreach($triggers as $key => $trig) {
			$triggers[$key]['period'] = $periods[$trig['period_id']];
		}
		return $triggers;
	}

	public function getAssessmentDetails($keys=array()) {
		
	}
	
	public function getTimePeriodsForAssessment($period_id=0,$obj_id=0) {
		if($period_id==0) {
			$data = array();
			$row = $this->getRawObject($obj_id);
			$period_id = $row[$this->getTableField().'_periodid'];
		}
		$prdObj = new SDBP5BCPM_PERIOD();
		$periods = $prdObj->getTimePeriodsByAssessmentPeriod($period_id);
		
		return $periods;
	}
	
	
	
	public function getRelatedIDs($id,$trigger_type="SELF",$user_id=false,$page_section=false) {
		$row = array();
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		$trgrObj = new SDBP5BCPM_TRIGGER();
		if($trigger_type==$trgrObj->getViewType() && $page_section!==false) {
			$trigger_type = $page_section;
			$user_id=false;
		} elseif($trigger_type==$trgrObj->getReportType() || $trigger_type==$trgrObj->getViewType()) {
			$trigger_type = $trgrObj->getFinalReviewType(); //echo $trigger_type;
			$user_id=false;
		}
		$sql = "SELECT 
				A.*
				, ".$this->getIDFieldName()." as assessment
				, ".$this->getParentFieldName()." as scorecard
				, ".$trgrObj->getIDFieldName()." as atrigger
				, ".$this->getTableField()."_periodid as period
				FROM ".$this->getTableName()." A
				INNER JOIN ".$trgrObj->getTableName()." 
				  ON ".$this->getIDFieldName()." = ".$trgrObj->getParentFieldName()." 
				  AND ".$trgrObj->getTableField()."_type LIKE '".$trigger_type."%' 
				  ".($user_id!==false ? "AND ".$trgrObj->getTableField()."_tkid = '".$user_id."'":"")." 
				  AND (".$trgrObj->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				WHERE ".$this->getIDFieldName()." = ".$id;
				//echo $sql;
		$row = $this->mysql_fetch_one($sql); 
		$row['trigger'] = $row['atrigger'];
		return $row;
	}
	
	
	public function getEmployeeBeingAssessed($assess_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$assess_id;
		$assessment = $this->mysql_fetch_one($sql);
		$scorecard_id = $assessment[$this->getParentFieldName()];
		$scrObject = new SDBP5BCPM_SCORECARD();
		$employee = $scrObject->getEmployee($scorecard_id);
		return $employee;
	}
	
	
	public function getAssessmentByPeriod($period_id) {
		$scdObj = new SDBP5BCPM_SCORECARD();
		$sql = "SELECT a.* , s.*, CONCAT(tk.tkname,' ',tk.tksurname) as employee
				FROM ".$this->getTableName()." a
				INNER JOIN  ".$scdObj->getTableName()." s
				  ON s.".$scdObj->getIDFieldName()." = a.".$this->getParentFieldName()."
				  AND (".$scdObj->getActiveStatusSQL("s").")
				INNER JOIN ".$this->getUserTableName()." tk
				  ON s.".$scdObj->getTableField()."_tkid = tk.tkid AND tk.tkstatus = 1
				WHERE a.".$this->getNameFieldName()." = ".$period_id." 
				AND (".$this->getActiveStatusSQL("a").")";
		return $this->mysql_fetch_all($sql);
	}
	
	
	
	
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	public function getAObject($id=0,$options=array()) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$id;
		$row = $this->mysql_fetch_one($sql);
		return $row;
	}
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Object where 
			//status = active and
			//status <> deleted
		$sql = " ( ( ".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
		 		AND  ( ".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE." )";
		return $sql;
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */

     
     
     
     
     
     
     
     
     
     
     
     
     
	private function logMyAction($id,$action,$old,$new,$var) {
		$logObj = new SDBP5BCPM_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id." for |".SDBP5BCPM_PERIOD::OBJECT_TYPE."| ".SDBP5BCPM_PERIOD::REFTAG.$var['prd_id']." on |".SDBP5BCPM_SCORECARD::OBJECT_TYPE."| ".SDBP5BCPM_SCORECARD::REFTAG.$var['scd_id'].".";
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id." for |".SDBP5BCPM_PERIOD::OBJECT_TYPE."| ".SDBP5BCPM_PERIOD::REFTAG.$var['prd_id']." on |".SDBP5BCPM_SCORECARD::OBJECT_TYPE."| ".SDBP5BCPM_SCORECARD::REFTAG.$var['scd_id'].".";
//				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			/*case "RESTORE":
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;*/
		}
		$logObj->addObject(
			$var,
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			0,
			$id
		);
		
	}
     
}


?>