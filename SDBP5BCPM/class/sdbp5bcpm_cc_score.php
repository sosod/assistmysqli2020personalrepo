<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class SDBP5BCPM_CC_SCORE extends SDBP5BCPM {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_trgrid";
	protected $name_field = "_value";
    /*
	protected $progress_status_field = "_statusid";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "SCORE";
    const TABLE = "cc_scores";
    const TABLE_FLD = "ccs";
    const REFTAG = "CS";
	const LOG_TABLE = "scores";
	/**
	 * Type CONSTANTS
	 */
	/**
	 * STATUS CONSTANTS
	 */
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
/*
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		
		$trgr_id = $var['trigger'];
		$lne_id = $var['line'];
		$lvl_id = $var['level'];
		$value = $var['value'];
		$iu = $this->getUserID();
		$s = self::ACTIVE;
		
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ccs_trgrid = ".$trgr_id." AND ccs_lneid = ".$lne_id." AND ccs_lvlid = ".$lvl_id." AND ".$this->getStatusFieldName()." = ".self::ACTIVE;
		//get all records
		$old = $this->mysql_fetch_all($sql);
		
		if(count($old)==0) {
			$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trgr_id, $lne_id, $lvl_id, $value, $s, '$iu', now())";
			$id = $this->db_insert($sql);
			$this->addLogRecord($id,"C","0",$value,$sql);
			$result = array("ok","Record added.");
		} elseif($old[0][$this->getTableField()."_value"]!=$value) {
			//update first record
			$old = $old[0];
			$sql = "UPDATE ".$this->getTableName()." SET ccs_value = ".$value." WHERE ".$this->getIDFieldName()." = ".$old[$this->getIDFieldName()];
			$mnr = $this->db_update($sql);
			$this->addLogRecord($old[$this->getIDFieldName()],"E",$old[$this->getNameFieldName()],$value,$sql);
			//LOG CHANGE
			$result = array("ok","Record updated.");
		} else {
			$result = array("info","No change.");
		}
		
		
		return $result;
	}

	private function addLogRecord($ccs_id,$type,$old,$new,$i_sql) {
		$ui = $this->getUserID();
		$i_sql = addslashes($i_sql);
		$sql = "INSERT INTO ".$this->getTableName()."_log VALUES (null,$ccs_id,'$type',$old,$new,'$i_sql','$ui',now())";
		$this->db_insert($sql);
	} 

	/*

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type']; 
		$ref = $var['ref'];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}
		
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
    
	
	public function getProgressOfScoring($trigger_id) {
		$sql = "SELECT count(ccs_lneid) as count, ccs_lneid as line FROM ".$this->getTableName()." WHERE ccs_trgrid = $trigger_id GROUP BY ccs_lneid";
		$rows = $this->mysql_fetch_value_by_id($sql, "line", "count");
		return $rows;
	}

	public function getScores($trigger_id,$line_id) {
		$sql = "SELECT ccs_value as score, ccs_lvlid as level FROM ".$this->getTableName()." WHERE ccs_trgrid = $trigger_id AND ccs_lneid = $line_id";
		$rows = $this->mysql_fetch_value_by_id($sql, "level", "score");
		return $rows;
	}
	
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}
	
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	/*public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */

/*
	public function saveScore($var) {
		$tbl_fld = $this->getTableField();
		
		$trigger_id = $var['trigger_id'];
		$existing_scores = $var['existing_scores']>0 ? true : false;
		$result = false;
		
		$kpa_score = $var['kpa_score'];
		$kpa_comment = $var['kpa_comment'];
		$cc_score = $var['cc_score'];
		$cc_comment = $var['cc_comment'];
		
		$score_ids = $var['score_id'];
		
		$scr_status = self::ACTIVE;
		
		if($existing_scores) {
			$changes = 0;
			//KPAs
			$line_ids = array_keys($kpa_score);
			foreach($line_ids as $l) {
				if(!isset($score_ids[$l]) || $score_ids[$l]==0) {
					if(strlen($kpa_score[$l])>0) {
						$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trigger_id , $l , ".$kpa_score[$l]." , '".$this->code($kpa_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
						$changes+=$this->db_insert($sql);
					}
				} else {
					$sid = $score_ids[$l];
					if(strlen($kpa_score[$l])>0) {
						$s = $kpa_score[$l];
					} else {
						$s = 0;
					}
					$sql = "UPDATE ".$this->getTableName()." SET ".$tbl_fld."_value = $s , ".$tbl_fld."_comment = '".$this->code($kpa_comment[$l])."' WHERE ".$this->getIDFieldName()." = $sid ";
					$changes+=$this->db_update($sql);
				}
			}
			//CCs
			$line_ids = array_keys($cc_score);
			foreach($line_ids as $l) {
				if(!isset($score_ids[$l]) || $score_ids[$l]==0) {
					if(strlen($cc_score[$l])>0) {
						$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trigger_id , $l , ".$cc_score[$l]." , '".$this->code($cc_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
						$changes+=$this->db_insert($sql);
					}
				} else {
					$sid = $score_ids[$l];
					if(strlen($cc_score[$l])>0) {
						$s = $cc_score[$l];
					} else {
						$s = 0;
					}
					$sql = "UPDATE ".$this->getTableName()." SET ".$tbl_fld."_value = $s , ".$tbl_fld."_comment = '".$this->code($cc_comment[$l])."' WHERE ".$this->getIDFieldName()." = $sid ";
					$changes+=$this->db_update($sql);
				}
			}
			if($changes>0) { $result = true; }
		} else {
			$insert_data = array();
			//KPAs
			$line_ids = array_keys($kpa_score);
			foreach($line_ids as $l) {
				//if(strlen($kpa_score[$l])>0) {
					$insert_data[] = "(null, $trigger_id , $l , ".(strlen($kpa_score[$l])>0 ? $kpa_score[$l] : 0)." , '".$this->code($kpa_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}
			//CCs
			$line_ids = array_keys($cc_score);
			foreach($line_ids as $l) {
				//if(strlen($cc_score[$l])>0) {
					$insert_data[] = "(null, $trigger_id , $l , ".(strlen($cc_score[$l])>0 ? $cc_score[$l] : 0)." , '".$this->code($cc_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}
			if(count($insert_data)>0) {
				$sql = "INSERT INTO ".$this->getTableName()." VALUES ".implode(",",$insert_data);
				$mid = $this->db_insert($sql);
				$result = true;
			}
		}
		
		return $result;
	}



	public function getScoresForCompletingAssessment($trigger_id) {
		$tbl_fld = $this->getTableField();
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$tbl_fld."_value as score
				, ".$tbl_fld."_comment as comment
				, ".$tbl_fld."_lneid as line_id 
				FROM ".$this->getTableName()." 
				WHERE ".$this->getParentFieldName()." = $trigger_id 
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		return $this->mysql_fetch_all_by_id($sql, "line_id");
	}
	 
	public function getAverageModeratorsScoreByAssessmentID($assess_id) {
		$value_fld = $this->getTableField()."_value";
		$line_fld = $this->getTableField()."_lneid";
		$triggerObj = new SDBP5BCPM_TRIGGER();
		$sql = "SELECT AVG($value_fld) as ave , $line_fld as id 
				FROM `".$this->getTableName()."` S 
				INNER JOIN ".$triggerObj->getTableName()." T 
					ON T.".$triggerObj->getIDFieldName()." = S.".$this->getParentFieldName()." 
					AND T.".$triggerObj->getTableField()."_type LIKE '".$triggerObj->getModeratorType()."%' 
					AND T.".$triggerObj->getParentFieldName()." = $assess_id
					AND (".$triggerObj->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND $value_fld > 0
				GROUP BY S.".$line_fld; //echo $sql;
		return $this->mysql_fetch_value_by_id($sql, "id", "ave");
	}
     
     */
}


?>