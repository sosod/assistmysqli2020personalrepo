<?php
if(!isset($no_include_header) || !$no_include_header) {
require_once("inc_header.php");
}


$create_step = 6;
//$create_type = "PROJ";
//$create_name = "Project";
$obj_id = !isset($obj_id) ? $_REQUEST['obj_id'] : $obj_id;

$scdObj = new SDBP5BCPM_SCORECARD();
$sources = $scdObj->getKPASources(); 
$pa = $scdObj->getAObjectSummary($obj_id);
//$kpas = $assessObj->getKPAList();

$sdbp5Obj = new SDBP5_PMS();
$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryKPASource());
$head = $sdbp5Obj->getHeadings($scdObj->getPrimaryKPASource(),true,false);

unset($head['main']['kpi_natkpaid']);

echo $scdObj->getAssessmentCreateHeading($create_step,$obj_id,$page_action);

?>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $obj_id; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$lineObj = new SDBP5BCPM_LINE();
$lines = $lineObj->getFullLinesByType($obj_id);
$weights = $lineObj->getLineWeights($obj_id);
$kpa_weights = $lineObj->getKPAWeightsByType($obj_id);

//ASSIST_HELPER::arrPrint($head);
?>
<h3>KPAs</h3>
<table id=tbl_kpa class=list>
<thead>
	<tr>
		<?php
		foreach($head['main'] as $fld=>$name) {
			echo "<th>".$name."</th>";
		}
		?>
		<th>KPA<br />Weight</th>
		<th>Overall<br />Weight</th>
	</tr>
</thead>
<tbody>
<?php
$grand_tot = 0;
$types = array("KPI","PROJ");
$names = array('KPI'=>"KPIs",'PROJ'=>"Projects");

foreach($kpas as $k => $a) {
foreach($types as $kt) {	
	$tot = 0;
	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
		//echo "<h3>".$a."</h3>";
		echo "<tr class=subth><td colspan=".(count($head['main'])+2).">".$a." - ".$names[$kt]."</td></tr>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k][$kt];
		foreach($objects as $i => $obj) {
			echo "
			<tr  class=\"".($obj['active']==1 ? "" : "inactive")."\">";
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				$w = (isset($weights[$i]) ? $weights[$i] : 1);
			echo "
				<td id=td_".$k.$kt."_".$i." class=right>".round(($w/$kw)*100,2)."%</td>
				<td class=right><div style='width:60px'>".$w."%</div></td>
			</tr>";
			$tot+=(isset($weights[$i]) ? $weights[$i] : 1);
		}
		echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>100%</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".$tot."%</th>
		</tr>";
	}
	$grand_tot+=$tot;
}  
}
?>
	</tbody>
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=<?php echo (count($head['main'])); ?>>Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot><?php echo $grand_tot; ?>%</th>
		</tr>
	</tfoot>
</table>

<h3>Core Competencies</h3>

<?php
$listObj = new SDBP5BCPM_LIST("competencies");
$objects = $listObj->getListItems();
$lines = $lineObj->getLineSrcIDs($obj_id, $lineObj->getModRef(),"CC");


?>
<table class=list id=tbl_cc>
	<tr>
		<th>Core Competency</th>
		<th>Description</th>
		<th>Weight</th>
	</tr>
	<?php
	$tot = 0;
	foreach($objects as $i => $obj) {
		if(isset($lines[$i])) {
			$li = $lines[$i];
			$w = isset($weights[$li]) ? $weights[$li] : 1;
			$tot+=$w;
			echo "
			<tr>
				<td>".$obj['name']."</td>
				<td>".$obj['description']."</td>
				<td class=right>".$w."%</td>
			</tr>";
		}
	}
	?>
	<tr class=total>
		<td></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot><?php echo $tot; ?>%</td>
	</tr>
</table>
</form>

<?php if($page_action!="view") { ?>
<p class=center><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Confirm & Activate</button></p>
<?php
}
?>





<?php






//ASSIST_HELPER::arrPrint($_REQUEST);
if($page_action!="view") {
	$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$obj_id);
}


?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
		
	
	<?php
	echo $echo['js'];
	?>
	
	$("table.noborder, table.noborder td").css("border","0px");

	$("#btn_next").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		var err = false;
		/*$("td.total_weight").each(function() {
			if(($(this).html()*1)!=100) {
				err = true;
			}
		});*/
		if(err) {
			alert("Not all KPA Weights total 100.  Please review the weights again.");
		} else {
			AssistHelper.processing();
			var dta = "obj_id=<?php echo $obj_id; ?>";
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Activate",dta);
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>.php?r[]=ok&r[]="+result[1];
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
		//document.location.href = 'assessment_create_step3.php?obj_id=<?php echo $obj_id; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		//if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $obj_id; ?>';
		//}
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #cc0001"
	});

});

</script>