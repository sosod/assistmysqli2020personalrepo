<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php
$kr = array(
	0 => array('result'=>"KPI's Not Yet Measured",'kpi'=>2,'color'=>"#999999"),
	1 => array('result'=>"KPI's Not Met",'kpi'=>20,'color'=>"#CC0001"),
	2 => array('result'=>"KPI's Almost Met",'kpi'=>21,'color'=>"#FE9900"),
	3 => array('result'=>"KPI's Met",'kpi'=>40,'color'=>"#009900"),
	4 => array('result'=>"KPI's Well Met",'kpi'=>20,'color'=>"#006600"),
	5 => array('result'=>"KPI's Extremely Well Met",'kpi'=>10,'color'=>"#000066")
);
$kr2 = array();
$kpi = 0;
foreach($kr as $ky => $k) {
	$kpi+=$k['kpi'];
	$val = array();
	foreach($k as $key => $s) {
		$val[] = $key.":\"".$s."\"";
	}
	$kr2[$ky] = implode(",",$val);
}
$chartData = "{".implode("},{",$kr2)."}";
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amCharts Example</title> 
        <link rel="stylesheet" href="style.css" type="text/css">
        <script src="../../amcharts/javascript/amcharts.js" type="text/javascript"></script>
        <script src="../../amcharts/javascript/raphael.js" type="text/javascript"></script>        
        <link rel="stylesheet" href="../../../../default.css" type="text/css">
		<?php include("../../../../SDBP3/inc_style.php"); ?>
        <script type="text/javascript">

        var chart;
		var legend;

        var chartData = [
				/*{result:"KPI's Not Met",kpi:21,color:"CC0001",lvisible:"false"},
				{result:"KPI's Almost Met",kpi:20,color:"FE9900",lvisible:"false"},
				{result:"KPI's Met",kpi:41,color:"009900",lvisible:"false"},
				{result:"KPI's Well Met",kpi:21,color:"006600",lvisible:"false"},
				{result:"KPI's Extremely Well Met",kpi:20,color:"000066",lvisible:"false"},
				{result:"KPI's Not Yet Measured",kpi:2,color:"999999",lvisible:"false"}*/
				<?php echo $chartData; ?>
		];

         window.onload = function() {
            chart = new AmCharts.AmPieChart();
			chart.color = "#ffffff";
            chart.dataProvider = chartData;
            chart.titleField = "result";
			chart.valueField = "kpi";
			chart.colorField = "color";
			chart.labelText = "[[percents]]%";
			chart.labelRadius = -25;
			chart.angle = 5;
			chart.depth3D = 5;
			chart.hideLabelsPercent = 5;
			chart.hoverAlpha = 0.5;
			chart.startDuration = 0;
			chart.radius = 120;

			/*legend = new AmCharts.AmLegend();
			legend.align = "center";
			legend.maxColumns = 1;
			legend.rollOverColor = "#009900";
			//chart.addLegend(legend);*/

			chart.write("chartdiv");
		}
	    </script>
<style type=text/css>
.legend2 {
	font: Tahoma;
	font-size: 8pt;
	line-height: 9pt;
	border: 3px solid #ffffff;
	vertical-align: top;
	text-align: left;
}
</style>
    </head>

	<body>
	<table cellpadding=3 style="margin-right: auto; margin-left: auto; border-color: #ffffff;"><tr><td style="text-align: center" class=noborder>
		<div id="chartdiv" style="width:450px; height:260px; background-color:#FFFFFF;"></div>
<?php
$tdl = 3;
	echo "<div align=center><table cellpadding=0 cellspacing=2 style=\"border: 1px solid #ababab;margin: 0 0 0 0;\" >";
		echo "<tr><td class=legend2><table cellspacing=3 cellpadding=3 style=\"border-width: 0px; margin: 0 0 0 0;\">";
			$k = 0;
			$kc = 3;
			foreach($kr as $krs) {
					$k++;
					//if($k<=$kc) {
						echo "<tr>";
						echo "<td class=legend2 width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
						echo "<td class=legend2>";
						echo $krs['result']."</a> (".$krs['kpi']." of ".$kpi.")</td>";
						echo "</tr>";
					//}
			}
		echo "</table></td>";
/*		echo "<td class=legend2><table cellspacing=13 style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($kr as $krs) {
					$k++;
					if($k>$kc) {
						echo "<tr>";
						echo "<td class=legend2 width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
						echo "<td class=legend2>";
						echo $krs['result']."</a> (".$krs['kpi']." of ".$kpi.")</td>";
						echo "</tr>";
					}
			}
		echo "</table></td>";*/
		echo "</tr>";
	echo "</table></div>";

?>
</td></tr></table>	</body>
</html>


