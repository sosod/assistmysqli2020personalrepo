<?php
include("inc_ignite.php");
$local_db = new ASSIST_DB();
$helper = new ASSIST_MODULE_HELPER();
$helper->echoPageHeader();
?>

        <h1 class=fc><b>Update Complaint</b></h1>
<p>Processing....</p>
        <?php
		$b = isset($_REQUEST['b']) ? $_REQUEST['b'] : "" ;
        $logadmintext = isset($_POST['logadmintext']) ? $_POST['logadmintext'] : "";
        $email = isset($_POST['email'])? $_POST['email'] : "";
        $utype = isset($_POST['utype']) ? $_POST['utype'] :"" ;
        $logtkid = isset($_POST['logtkid']) ? $_POST['logtkid'] : "" ;
        $logurgencyid = isset($_POST['logurgencyid']) ? $_POST['logurgencyid'] : "";
        $logrespid = isset($_POST['logrespid']) ? $_POST['logrespid'] : "" ;
        $logstatusid = isset($_POST['logstatusid']) ? $_POST['logstatusid'] : 0;
        $callid = isset($_POST['callid']) ? $_POST['callid'] : "";
        $doneyn = "N";
        /*$dday = $_POST['dday'];
        $dmon = $_POST['dmon'];
        $dyear = $_POST['dyear'];
        $dhr = $_POST['dhr'];
        $dmin = $_POST['dmin'];*/
		$actdate = isset($_REQUEST['actiondate']) ? $_REQUEST['actiondate'] : "";
		$dateact = strlen($actdate)>0 ? strtotime($actdate) : $today;
        $result = array();
        if($utype != "C" && strlen($logadmintext)==0) {
            ?>
        <input type=hidden id=i value=<?php echo($callid); ?>>
        <script type="text/javascript">
            alert("Please enter an Update Message.");
            var i = document.getElementById('i').value;
            if(i.length>0)
            {
                var url = "update_call.php?i="+i;
            }
            else
            {
                var url = "update.php";
            }
            document.location.href = url;
        </script>
            <?php
        }
        else {

            /*if(strlen($dday)>0 && strlen($dday)<3 && strlen($dmon)>0 && strlen($dyear)==4 && strlen($dhr)>0 && strlen($dhr)<3 && strlen($dmin)>0 && strlen($dmin)<3 && is_numeric($dday) && is_numeric($dmon) && is_numeric($dyear) && is_numeric($dhr) && is_numeric($dmin)) {
                $dateact = mktime($dhr,$dmin,1,$dmon,$dday,$dyear);
            }
            else {
                $dateact = $today;
            }*/

            $char1[0] = "'";
            $char2[0] = "&#39";

            $marr = explode($char1[0],$logadmintext);
            $logadmintext = implode($char2[0],$marr);


//GET TO EMAIL
            $sql = "SELECT tk.tkemail, tk.tkid FROM assist_".$cmpcode."_timekeep tk, assist_".$cmpcode."_caps_list_respondant p WHERE tk.tkid = p.admin AND tk.tkstatus = 1 AND p.id = ".$logrespid;
            //include("inc_db_con.php");
            $row = $local_db->mysql_fetch_one($sql);
            $to = $row['tkemail']; //respondant
            $logtkid = $row['tkid'];
            //mysql_close();
            $sql = "";
            $row = "";

            $from = "no-reply@ignite4u.co.za"; //user

            switch ($utype) {
                case "C":
                //SET SQL statements
                    $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET callstatusid = ".$logstatusid." WHERE callid = ".$callid;
                    $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                    $sqllog .= "SET logdate = '".$today."', ";
                    $sqllog .= "logtkid = '".$logtkid."', ";
                    $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                    $sqllog .= "logrespid = ".$logrespid.", ";
                    $sqllog .= "logadmintext = '".$logadmintext."', ";
                    $sqllog .= "logstatusid = ".$logstatusid.", ";
                    $sqllog .= "logemail = '".$email."', ";
                    $sqllog .= "logcallid = ".$callid.", ";
                    $sqllog .= "logsubmitdate = '".$today."', ";
                    $sqllog .= "logutype = '".$utype."', ";
                    $sqllog .= "logdateact = '".$dateact."', ";
                    $sqllog .= "logsubmittkid = '".$tkid."'";
                    //Update database
                    $sql = "";
                    $sql = $sqllog;
                    //include("inc_db_con.php");
                    $logid = $local_db->db_insert($sql);//mysql_insert_id();
                    $sql = "";
                    $sql = $sqlcall;
                    //include("inc_db_con.php");
					$local_db->db_update($sql);
                    //Send email
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
                    //include("inc_db_con.php");
                    $row = $local_db->mysql_fetch_one($sql); //mysql_fetch_array($rs);
                    //mysql_close();
                    $callmess = explode(chr(10),$row['callmessage']);
                    $callmessage = implode("<br>",$callmess);
                    $subject = "Complaint cancelled";
                    $message = "<font face=arial>";
                    $message .= "<p><small>Complaint ".$row['callref']." has been cancelled.<br>";
                    $message .= "The original complaint was: <br>";
                    $message .= "<i>".$callmessage."</i></p>";
                    $message .= "</small></font>";
                    $header = "From: no-reply@ignite4u.co.za\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
                    /*mail($to,$subject,$message,$header);
                    $header = "";
                    $message = "";*/
                    $mail = new ASSIST_EMAIL($to,$subject,$message,"HTML");
                    $mail->sendEmail();

                    $doneyn = "C";
                    break;
                case "A":
                //SET SQL statements
                    //1. update the call.
                    $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET callstatusid = ".$logstatusid." WHERE callid = ".$callid;
                    $affected_rows = $local_db->db_update($sqlcall);

                    //If successful log it
                    $tref = "CAPS";
                    $tsql = $sqlcall;
                    $trans = "Updated call status for call " . $callid;
                    include("inc_transaction_log.php");

                    $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                    $sqllog .= "SET logdate = '".$today."', ";
                    $sqllog .= "logtkid = '".$logtkid."', ";
                    $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                    $sqllog .= "logrespid = ".$logrespid.", ";
                    $sqllog .= "logadmintext = '".$logadmintext."', ";
                    $sqllog .= "logstatusid = ".$logstatusid.", ";
                    $sqllog .= "logemail = '".$email."', ";
                    $sqllog .= "logcallid = ".$callid.", ";
                    $sqllog .= "logsubmitdate = '".$today."', ";
                    $sqllog .= "logutype = '".$utype."', ";
                    $sqllog .= "logdateact = '".$dateact."', ";
                    $sqllog .= "logsubmittkid = '".$tkid."'";
                    //Update database
                    //$sql = "";
                    //$sql = $sqllog;
                    //include("inc_db_con.php");
                    $logid = $local_db->db_insert($sqllog);//mysql_insert_id();

                    if ($logid > 0){
                        $tref = "CAPS";
                        $tsql = $sqllog;
                        $trans = "Updated call log for call ".$callid;
                        include("inc_transaction_log.php");
                    }


                    $result = array("ok", "Complaint successfully updated.","log_status"=>$logstatusid);
                    $doneyn = "U";
                    //$sql = "";
                    //$sql = $sqlcall;
                    //include("inc_db_con.php");


                    //Send email
                    /*        $upmess = explode(chr(10),$logusertext);
        $upmessage = implode("<br>",$upmess);
        $subject = "Complaint updated";
        $message = "<font face=arial>";
        $message .= "<p><small>Complaint ".$callref." has submitted an update to call ".$callid.".<br>";
        $message .= "The update message is: <br>";
        $message .= "<i>".$logusertext."</i></p>";
        $message .= "</small></font>";
        $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
        mail($to,$subject,$message,$header);
        $header = "";
        $message = "";
                    */

                    break;
                default:
                    $doneyn = "C";
                    break;

            }
//uploads

            $original_filename = "";
            $system_filename = "";
            $attachments = "";
            //print_r($_FILES);die;
            if(isset($_FILES['attachments'])) {
                $folder = "../files/$cmpcode/CAPS";
                /* if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
                $helper->checkFolder("CAPS");
$file = 0;
                foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                    //print_r($tmp_name);die;
                    if($_FILES['attachments']['error'][$key] == 0) {
                        $original_filename = $_FILES['attachments']['name'][$key];
                        $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                        $parts = explode(".", $original_filename);
                        $file++;
                        $system_filename = $logid."_".$file."_".date("YmdHi").".$ext";
                        $full_path = $folder."/".$system_filename;
                        move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                        $sql = "INSERT INTO assist_".$cmpcode."_caps_attachments (callid,logid,original_filename,system_filename)
                                VALUES (0,$logid,'$original_filename','$system_filename')";
                        //include("inc_db_con.php");
						$local_db->db_insert($sql);
                        $filename = "../files/".$cmpcode."/CAPS/".$system_filename;
//                        $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
                    }
                }

            }
            $logupdate="";
/*            if($attachments != '') {
                $logupdate .= "<br />Complaint Attachment(s):<br /> $attachments";
                $logupdate = addslashes(htmlentities($logupdate));
                //$sql = "UPDATE assist_".$cmpcode."_caps_log SET logadmintext=CONCAT_WS('\n',logadmintext,'$logupdate') WHERE logid=$logid";
                //include("inc_db_con.php");
            }*/

            ?>
        <form name=update>
            <input type=hidden name=done value=<?php echo($doneyn); ?>>
            <input type=hidden name=callid value=<?php echo($callid); ?>>
        </form>
        <script language=JavaScript>
            $(document).ready(function () {
                var doneyn = document.update.done.value;
                var callid = document.update.callid.value;

                if (doneyn == "C") {
                    document.location.href = "update.php";
                } else {
                    var b = escape('<?php echo $b; ?>');
                    var new_url = "update_call.php?b=" + b + "&i=" + callid + "&r[]=<?php echo $result[0]; ?>&r[]=<?php echo $result[1]; ?>&log_status=<?php echo $result['log_status']; ?>#update";
                    document.location.href = new_url;
                }
            });
        </script>
            <?php
        }   //if text entered
        ?>
    </body>
</html>
