<?php

function logMe($id,$fld,$trans,$old,$new,$section,$action,$sql,$table="udf") {
    global $tkid, $cmpcode , $helper;
    //$tkn = $_SESSION['tkn'];
    $l = array(
        'sec'=>		$section,
        'ref'=>		$id,
        'action'=>	$action,
        'fld'=> 	$fld,
        'trans'=> 	$trans,
        'old'=>		$old,
        'new'=>		$new,
        'sql'=> 	addslashes($sql)
    );

    $sql = "INSERT INTO assist_".$cmpcode."_".$table."_log 
		(id, date, tkid, tkname, section, ref, action, field, transaction, old, new, active, lsql)
		VALUES
		(null, now(), '".$helper->getUserID()."', '".$helper->getUserName()."', '".$l['sec']."', '".$l['ref']."', '".$l['action']."', '".$l['fld']."', '".$l['trans']."', '".$l['old']."', '".$l['new']."', true, '".$l['sql']."')";
    $helper->db_insert($sql);
}

?>