<?php
require_once "inc_head.php";
require_once "../inc_assist.php";
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
if(isset($result[1])) { $result[1] = stripslashes($result[1]); }
?>
<h1>Setup >> User Defined Fields</h1>
<?php $helper->displayResult($result); ?>
<table id=udf><thead>
    <tr>
        <th>Ref</th>
        <th>Field Name</th>
        <th>Field Type</th>
        <th>Required</th>
        <th></th>
    </tr>
</thead><tbody>
<form id=frm_add name=new action=setup_udf_process.php method=post>
<input type=hidden name=act value=ADD />
	<tr>
		<th></th>
        <td><input type=text name="udfivalue" maxlength=50 size=30 id=a /></td>
        <td><select name="udfilist" id="b">
				<option selected value='Y'>Preset list</option>
				<option value='T'>Text (Max: 200 characters)</option>
				<option value='M'>Text (No limit)</option>
				<option value='D'>Date</option>
				<option value='N'>Number</option>
            </select></td>
        <td class=center><select name=udfirequired><option selected value=1>Yes</option><option value=0>No</option></select></td>
        <td class=center><input type=button value=Add class=isubmit /></td>
	</tr>
</form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiyn = 'Y' ORDER BY udfisort";
$udfs = $db->mysql_fetch_all($sql);
if(count($udfs)==0) {
	echo "<tr>
			<th></th>
            <td colspan=4>No UDFs to display</td>
        </tr>";
} else {
    foreach($udfs as $row) {
		echo "<tr id=tr_".$row['udfiid'].">
        <th class=center>".$row['udfiid']."</th>
        <td fld=value>".$row['udfivalue']."</td>
        <td fld=list val=".$row['udfilist'].">";
		switch($row['udfilist']) {
			case "M":
				echo "Large Text (no limit)";
				break;
			case "T":
				echo "Small Text (max 200 characters)";
				break;
			case "Y":
				echo "Preset List";
				break;
			case "D":
				echo "Date";
				break;
			case "N":
				echo "Number";
				break;
			default:
				echo "N/A";
				break;
		}
        echo "</td>
		<td class=center fld=req>".($row['udfirequired']==1 ? "Yes" : "No")."</td>
        <td class=center fld=but><input type=button value=Edit class=edit id=".$row['udfiid']." />";
            if(in_array($row['udfilist'],array("L","Y"))) {
                echo "<input type=button value=List class=list id=".$row['udfiid']." />";
            }
        echo "</td>
			</tr>";
    }
}
?>
</tbody></table>
<table class="log_footer" style="border: 0px solid black;"><tr><td style="border: 0px solid black;">
<?php $helper->displayGoBack("setup.php","Go Back");?>
        </td><td style="border: 0px solid black;">
<?php
$log_sql = "SELECT date as log_date, tkname as log_user, transaction as log_action FROM assist_".$cmpcode."_udf_log WHERE section='".$helper->getModRef()."' ORDER BY id DESC";
$log_flds = array('date'=>"log_date",'user'=>"log_user",'action'=>"log_action");
displayAuditLog($log_sql,$log_flds);
?></td>
    </tr>
</table>
<div id=dlg_edit title="Edit UDF">
<form id=frm_edit name=frm_edit method=post action=setup_udf_process.php>
<input type=hidden name=act value=EDIT />
	<table>
		<tr>
			<th width=100>Reference:</th>
			<td><input type=hidden id=txt_id name=udfiid value="" /><label id=lbl_id></label></td>
		</tr>
		<tr>
			<th>Field Name:</th>
			<td><input type=text size=50 name=udfivalue maxlength=50 id=txt_name /></td>
		</tr>
		<tr>
			<th>Field Type:</th>
			<td id=td_list><select name="udfilist" id=sel_list>
				<option selected value='Y'>Preset list</option>
				<option value='T'>Text (Max: 200 characters)</option>
				<option value='M'>Text (No limit)</option>
				<option value='D'>Date</option>
				<option value='N'>Number</option>
            </select></td>
		</tr>
		<tr>
			<th>Required:</th>
			<td><select name=udfirequired id=sel_req><option selected value=1>Yes</option><option value=0>No</option></select></td>
		</tr>
		<tr>
			<th></th>
			<td><input type=button value="Save Changes" class=isubmit /> <input type=reset></td>
		</tr>
	</table>
	<p><span class=float><input type=button value=Delete class=idelete /></span></p>
</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_edit").dialog({
		autoOpen: false,
		modal: true,
		width: 450,
		height: 240,
	});
	$("#udf input:button").click(function() {
		var i = $(this).attr("id"); //alert(i);
		switch($(this).attr("class")) {
		case "edit":
			var v = "";
			$("#dlg_edit #txt_id").val(i);
			$("#dlg_edit #lbl_id").html(i);
			$("#tr_"+i+" td").each(function() {
				v = $(this).html();
				switch($(this).attr("fld")) {
				case "value":	
					$("#dlg_edit #txt_name").val(v); 
					break;
				case "list":
					v2 = $(this).attr("val");
					if(v2=="Y" || v2 == "L") { 
						$("#dlg_edit #td_list").html("<input type=hidden id=sel_list name=udfilist value='' />"+v);
					} else {
						$("#dlg_edit #td_list").html("<select name=udfilist id=sel_list><option value=Y>Preset list</option><option value=T>Text (Max: 200 characters)</option><option value=M>Text (No limit)</option><option value=D>Date</option><option value=N>Number</option></select>");
					}
					$("#dlg_edit #sel_list").val(v2); 
					break;
				case "req":		
					if(v=="Yes") { v = 1; } else { v = 0; } 
					$("#dlg_edit #sel_req").val(v); 
					break;
				}
			});
			$("#dlg_edit").dialog("open");
			break;
		case "list":
			document.location.href = "setup_udf_list.php?i="+i;
			break;
		case "isubmit":
			var addA = $("#a");
			var addB = $("#b");
			addA.removeClass("required");
			addB.removeClass("required");
			var a = addA.val();  
			var b = addB.val();
//alert($("form[name=new] #a").attr("value"));
//var err = true;
			var err = false;
			if(a.length==0) { addA.addClass("required"); err = true; }
			if(b.length==0) { addB.addClass("required"); err = true; }
			if(err) {
				alert("Please complete the required fields as highlighted.");
			} else {
				$("form[name=new]").submit();
			}
			break;
		}
	});
	$("#dlg_edit .isubmit").click(function() {
			var addA = $("form[name=frm_edit] #txt_name");
			var addB = $("form[name=frm_edit] #sel_list");
			addA.removeClass("required");
			addB.removeClass("required");
			var a = addA.val();
			var b = addB.val();
			var err = false;
			if(a.length==0) { addA.addClass("required"); err = true; }
			if(b.length==0) { addB.addClass("required"); err = true; }
			if(err) {
				alert("Please complete the required fields as highlighted.");
			} else {
				$("#frm_edit").submit();
			}
	});
	$("#dlg_edit .idelete").click(function() {
		if(confirm("Are you sure you wish to delete this UDF?")==true) {
			var i = $("#frm_edit #txt_id").val();
			document.location.href = 'setup_udf_process.php?udfiid='+i+'&act=DELETE';
		}
	});
});
</script>
</body>
</html>