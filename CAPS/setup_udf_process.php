<?php
require_once 'inc_head.php';
require_once 'setup_udf_log.php';
echo "<h1>Setup >> User Defined Fields</h1>";

if(!isset($_REQUEST['act']) || !(strlen($_REQUEST['act'])>0)) {
	$result = array("error","Ignite Assist could not determine your requested action.  Please try again.");
} else {
	$i = isset($_REQUEST['udfiid']) ? $_REQUEST['udfiid'] : "";
	$v = isset($_REQUEST['udfivalue']) ? $_REQUEST['udfivalue'] : "";
	$r = isset($_REQUEST['udfirequired']) ? $_REQUEST['udfirequired'] : "";
	$l = isset($_REQUEST['udfilist']) ? $_REQUEST['udfilist'] : "";
	$fields = array('udfiid'=>$i,'udfivalue'=>$v,'udfirequired'=>$r,'udfilist'=>$l,'act'=>$_REQUEST['act']);
    $flds = serialize(array_keys($fields));
    $fields = serialize($fields);
    $date = date("Y-m-d H:i:s");
	switch($_REQUEST['act']) {
	case "ADD":
		$sql = "INSERT INTO assist_".$cmpcode."_udfindex (udfiid,udfivalue,udfilist,udfiref,udficustom,udfisort,udfiyn,udfirequired,udfidefault) VALUES (null,'".$helper->code($v)."','$l','$modref','N',1,'Y','".$r."','')";
		$id = $db->db_insert($sql);
		if($helper->checkIntRef($id)) {
		    $tran = "Added new UDF \'".$helper->code($v)."\' (Ref: \'".$id."\').";
			$log = "INSERT INTO assist_".$cmpcode."_udf_log (id,date,tkid,tkname,section,ref,action,field,transaction,new,active,lsql,old) VALUES (null,'".$date."','".$tkid."','".$helper->getUserName()."','".$helper->getModRef()."',".$id.",'C','".$fields."','".$tran."','".$flds."',1,'".addslashes($sql)."','')";
			$db->db_insert($log);
			$result = array("ok","UDF \'".$helper->code($v)."\' has been created successfully.");
            //logMe($id,serialize($flds),"Added new UDF \'".$helper->code($v)."\' (Ref: ".$id.").",serialize(array()),serialize($fields),"MAIN",$_REQUEST['act'],$sql,"C","udf");
		} else {
			$result = array("error","Ignite Assist was unable to create the UDF.  Please try again.");
		}
		break;
	case "EDIT":
		$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$i;
		$old = $db->mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET udfivalue = '".$helper->code($v)."', udfilist = '$l', udfirequired = ".$r." WHERE udfiid = $i";
		$mar = $db->db_update($sql);
		if($mar>0) {
		    $tran = "Edited UDF \'".$helper->code($v)."\' (Ref: \'".$i."\').";
			$log = "INSERT INTO assist_".$cmpcode."_udf_log (id,date,tkid,tkname,section,ref,action,field,transaction,new,active,lsql,old) VALUES (null,'".$date."','".$tkid."','".$helper->getUserName()."','".$helper->getModRef()."',".$i.",'E','".$fields."','".$tran."','".$flds."',1,'".addslashes($sql)."','".serialize($old)."')";
			$db->db_insert($log);
			$result = array("ok","UDF \'".$helper->code($v)."\' has been edited successfully.");
            //logMe($mar,serialize($flds),"Edited UDF \'".$helper->code($v)."\' (Ref: ".$mar.").",serialize($old),serialize($fields),"MAIN",$_REQUEST['act'],$sql,"E","udf");
		} else {
			$result = array("info","No change was found to be saved.");
		}
		break;
	case "DELETE":
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET udfiyn = 'N' WHERE udfiid = ".$i;
		$up_id = $db->db_update($sql);
        $sql1 = "SELECT udfivalue FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$i;
        $row = $db->mysql_fetch_one($sql1);
		if($up_id>0) {
            $tran = "Deleted UDF \'".$row['udfivalue']."\' (Ref: \'".$i."\').";
			$log = "INSERT INTO assist_".$cmpcode."_udf_log (id,date,tkid,tkname,section,ref,action,field,transaction,new,active,lsql,old) VALUES (null,'".$date."','".$tkid."','".$helper->getUserName()."','".$helper->getModRef()."',".$i.",'D','".$fields."','".$tran."','".$flds."',1,'".addslashes($sql)."','".serialize(array())."')";
			$db->db_insert($log);
			$result = array("ok","UDF ".$i." has been successfully deleted.");
            //logMe($mar,serialize($flds),"Deleted UDF \'".$row['udfivalue']."\' (Ref: ".$mar.").",serialize(array()),serialize($fields),"MAIN",$_REQUEST['act'],$sql,"E","udf");
		} else {	
			$result = array("info","No change was found to be saved.");
		}
		break;
	default:
		$result = array("error","Ignite Assist could not determine your requested action.  Please try again.");
	}
}

echo "
<script type=text/javascript>
	document.location.href = 'setup_udf.php?r[]=".$result[0]."&r[]=".$result[1]."';
</script>
";

?>