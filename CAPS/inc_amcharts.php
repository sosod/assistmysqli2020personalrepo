<?php


/*****************************
JAVA BASED AM CHARTS 
*****************************/

function drawJavaGraph($result_settings,$values,$type = "view") { 
	$kr2 = array();
	foreach($result_settings as $ky => $k) {
		$k['obj'] = isset($values[$k['text']]) ? $values[$k['text']] : 0;
		$val = array();
		foreach($k as $key => $s) {
			$val[] = $key.":\"".$s."\"";
		}
		$kr2[$ky] = implode(",",$val);
	}
	$chartData = "{".implode("},{",$kr2)."}";


	$echo = "<script type=\"text/javascript\">
        var chart;
		var legend;
        var chartData = [".$chartData."];
		
		$(document).ready(function() {
            main_chart = new AmCharts.AmPieChart();
			main_chart.color = \"#000000\";
            main_chart.dataProvider = chartData;
            main_chart.titleField = \"value\";
			main_chart.valueField = \"obj\";
			main_chart.colorField = \"color\";
			main_chart.pulledField=\"pull\";
			main_chart.labelText = \"[[percents]]% ([[value]])\";
			main_chart.labelRadius = 25;
			main_chart.outlineColor = \"#FFFFFF\";
			main_chart.outlineAlpha = 0.8;
			main_chart.outlineThickness = 2;
			main_chart.angle = 30;
			main_chart.depth3D = 15;
			main_chart.hideLabelsPercent = 1;
			main_chart.hoverAlpha = 0.5;
			main_chart.startDuration = 0;
			main_chart.radius = 130;
			main_chart.marginBottom = 10;
			main_chart.marginRight = 30;
			main_chart.write(\"main\");
		});
		</script>
					<div id=\"main\" style=\"margin: 0 auto; width:500px; height:300px; background-color:#ffffff;\"></div>";

	echo $echo;
}







function drawPieLegend($values,$show_link = false,$g=0) {
	global $result_settings;
	global $report_settings;
	$echo = "<table class=legend style=\"margin: 0 auto;margin-top: 10px\">";
	foreach($result_settings as $r) {
		if($show_link) { 
			$url = "<a href=# onclick=\"goReport(".$r['id'].",'".$report_settings['groupby']."',".$g.")\">";
		} else {
			$url = "";
		}
		$echo.= "<tr><td class=res><img src='/library/amcharts2/".strtoupper(substr($r['color'],1,6)).".gif' width=10 height=10>&nbsp;".$r['value']."</td>
		<td class=center>".(isset($values[$r['text']]) ? $url.$values[$r['text']]." (".round(($values[$r['text']]/array_sum($values))*100,1)."%)</a>" : "-")."</td></tr>";
	}	
		$echo.= "<tr><td class=\"total right\">Total:</td>
		<td class=\"total center\">".array_sum($values)."</td></tr>";
	$echo.= "</table>";
	echo $echo;
	
}




function chartEncode($str) {

	//$str = code($str);

	$str = str_replace("&","&#38;",$str);
	$str = str_replace("\"","&#34;",$str);
	
	$str = str_replace("[","&#91;",$str);
	$str = str_replace("]","&#93;",$str);

	$str = str_replace("{","&#123;",$str);
	$str = str_replace("}","&#125;",$str);

	return $str;
}


function drawLegendStyle() {
echo "
	<style type=text/css>
		/** STYLE FOR LEGEND **/
		table.legend td { font-size: 7pt; }
		table td .head1 {
			text-align: center;
			font-weight: bold;
			vertical-align: middle;
			font-size: 7pt;
			color: #000000;
		}
		table td .head2 {
			font-style: italic;
			text-align: center;
			font-weight: normal;
			vertical-align: bottom;
			font-size: 7pt;
			color: #000000;
		}
		table td .res {
			font-weight: bold;
			vertical-align: middle;
			font-size: 6.5pt;
			color: #000000;
		}
		table tbody th {
			font-size: 7.5pt;
			text-align: center;
		}
		table tbody th.th2 {
			font-size: 7.5pt;
			font-style: italic;
		}
		table td .total {
			background-color: #dedede;
			font-weight: bold;
			vertical-align: middle;
			font-size: 7pt;
			color: #000000;
		}
		</style>";
}


?>