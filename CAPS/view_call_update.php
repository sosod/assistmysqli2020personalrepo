<?php
require_once 'inc_head.php';
?>

<h1>View >> Update</b></h1>
        <?php
		$callid = isset($_REQUEST['callid']) ? $_REQUEST['callid'] : $_REQUEST['i'];
		$sql = "SELECT * FROM ".$dbref."_call WHERE callid = ".$callid;
		$call = $db->mysql_fetch_one($sql);
        $logadmintext = isset($_REQUEST['logadmintext'])?$_REQUEST['logadmintext']:"";
        $email = isset($_REQUEST['email'])? $_REQUEST['email']:"";
        $utype = isset($_REQUEST['utype'])? $_REQUEST['utype'] :"";
        $logtkid = isset($_REQUEST['logtkid']) ? $_REQUEST['logtkid'] : $call['calltkid'];
        $logurgencyid = isset($_REQUEST['logurgencyid']) ? $_REQUEST['logurgencyid'] : $call['callurgencyid'];
        $logrespid = isset($_REQUEST['logrespid']) ? $_REQUEST['logrespid'] : $call['callrespid'];
        $logstatusid = isset($_REQUEST['logstatusid']) ? $_REQUEST['logstatusid'] : $call['callstatusid'];
        $doneyn = "N";

        $char1[0] = "'";
        $char2[0] = "&#39";

        $marr = explode($char1[0],$logadmintext);
        $logadmintext = implode($char2[0],$marr);


//GET TO EMAIL
        $sql = "SELECT tk.tkemail FROM assist_".$cmpcode."_timekeep tk, assist_".$cmpcode."_caps_list_respondant p WHERE tk.tkid = p.admin AND tk.tkstatus = 1 AND p.id = ".$logrespid;
        //include("inc_db_con.php");
        $row = $db->mysql_fetch_one($sql);
        $to = $row['tkemail']; //respondant
        //mysql_close();
        $sql = "";
        $row = "";

        $from = "assist@ignite4u.co.za"; //user


        switch ($utype) {
            case "C":
            //SET SQL statements
			$logadmintext = "Complaint cancelled by ".$_SESSION['tkn'];
                $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET callstatusid = ".$logstatusid." WHERE callid = ".$callid;
                $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                $sqllog .= "SET logdate = '".$today."', ";
                $sqllog .= "logtkid = '".$logtkid."', ";
                $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                $sqllog .= "logrespid = ".$logrespid.", ";
                $sqllog .= "logadmintext = '".$logadmintext."', ";
                $sqllog .= "logstatusid = ".$logstatusid.", ";
                $sqllog .= "logemail = '".$email."', ";
                $sqllog .= "logcallid = ".$callid.", ";
                $sqllog .= "logsubmitdate = '".$today."', ";
                $sqllog .= "logutype = '".$utype."', ";
                $sqllog .= "logdateact = '".$today."', ";
                $sqllog .= "logsubmittkid = '".$tkid."'";
                //Update database
                $sql = "";
                $sql = $sqllog;
                //include("inc_db_con.php");
                $logid = $db->db_insert($sql);//mysql_insert_id();
                $sql = "";
                $sql = $sqlcall;
                //include("inc_db_con.php");
				$db->db_update($sql);
                //Send email
                $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
                //include("inc_db_con.php");
                $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
                //mysql_close();
                $callmess = explode(chr(10),$row['callmessage']);
                $callmessage = implode("<br>",$callmess);
                $subject = "Complaint cancelled";
                $message = "<font face=arial>";
                $message .= "<p><small>Complaint ".$row['callref']." has been cancelled.<br>";
                $message .= "The original complaint was: <br>";
                $message .= "<i>".$callmessage."</i></p>";
                $message .= "</small></font>";
                $header = "From: no-reply@ignite4u.co.za\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
                /*mail($to,$subject,$message,$header);
                $header = "";
                $message = "";*/
                $mail = new ASSIST_EMAIL($to,$subject,$message,"HTML");
                $mail->sendEmail();

                $doneyn = "C";
				
				$cancel = "view.php";//$_REQUEST['src'];

				echo "<script type=text/javascript>document.location.href = '".$cancel."?r[]=ok&r[]=Complaint ".$row['callref']." has been cancelled.';</script>";
                break;
            case "U":
            //SET SQL statements
                $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                $sqllog .= "SET logdate = '".$today."', ";
                $sqllog .= "logtkid = '".$logtkid."', ";
                $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                $sqllog .= "logrespid = ".$logrespid.", ";
                $sqllog .= "logadmintext = '".$logadmintext."', ";
                $sqllog .= "logstatusid = ".$logstatusid.", ";
                $sqllog .= "logemail = '".$email."', ";
                $sqllog .= "logcallid = ".$callid.", ";
                $sqllog .= "logsubmitdate = '".$today."', ";
                $sqllog .= "logutype = '".$utype."', ";
                $sqllog .= "logdateact = '".$today."', ";
                $sqllog .= "logsubmittkid = '".$tkid."'";
                //Update database
                $sql = "";
                $sql = $sqllog;
                //include("inc_db_con.php");
                $logid = $db->db_insert($sql);//mysql_insert_id();

                //Send email
                /*        $upmess = explode(chr(10),$logusertext);
        $upmessage = implode("<br>",$upmess);
        $subject = "Complaint updated";
        $message = "<font face=arial>";
        $message .= "<p><small>Complaint ".$callref." has submitted an update to call ".$callid.".<br>";
        $message .= "The update message is: <br>";
        $message .= "<i>".$logusertext."</i></p>";
        $message .= "</small></font>";
        $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
        mail($to,$subject,$message,$header);
        $header = "";
        $message = "";
                */
                $doneyn = "U";
                break;
            default:
                $doneyn = "C";
                break;
        }
//uploads
        $original_filename = "";
        $system_filename = "";
        $attachments = "";
        //print_r($_FILES);die;
        if(isset($_FILES)) {
            $folder = "../files/$cmpcode/CAPS";
            /* if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
            $helper->checkFolder("CAPS");
		$file = 0;
            foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                //print_r($tmp_name);die;
                if($_FILES['attachments']['error'][$key] == 0) {
                    $original_filename = $_FILES['attachments']['name'][$key];
                    $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                    $parts = explode(".", $original_filename);
                    $file++;
                    $system_filename = $logid."_".$file."_".date("YmdHi").".$ext";
                    $full_path = $folder."/".$system_filename;
                    move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                    $sql = "INSERT INTO assist_".$cmpcode."_caps_attachments (callid,logid,original_filename,system_filename) VALUES (0,$logid,'$original_filename','$system_filename')";
                    //include("inc_db_con.php");
					$db->db_insert($sql);
                    $filename = "../files/$cmpcode/CAPS/".$system_filename;
                    //$attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
                }
            }
        }
        $logupdate="";
/*        if($attachments != ''){
            $logupdate .= "<br />Complaint Attachment(s):<br /> $attachments";
            $logupdate = addslashes(htmlentities($logupdate));
            $sql = "UPDATE assist_".$cmpcode."_caps_log SET logadmintext=CONCAT_WS('\n',logadmintext,'$logupdate') WHERE logid=$logid";
            include("inc_db_con.php");
        }*/
        ?>
        <form name=update>
            <input type=hidden name=done value=<?php echo($doneyn); ?>>
            <input type=hidden name=callid value=<?php echo($callid); ?>>
        </form>
        <script language=JavaScript>
            var doneyn = document.update.done.value;
            var callid = document.update.callid.value;
            if(doneyn == "C")
            {
                document.location.href = "view.php";
            }
            else
            {
                document.location.href = "view_call.php?i="+callid;
            }
        </script>
    </body>
</html>
