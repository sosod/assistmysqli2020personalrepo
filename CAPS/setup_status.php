<?php
require_once 'inc_head.php';
//    include("inc_ignite.php");
//    include("inc_hp.php");
    
    $val = isset($_POST['val'])?$_POST['val']:"";
    $sort = isset($_POST['sort'])?$_POST['sort']:"";
    
    if(strlen($val) > 0)
    {
        //CREATE SORT INT IF BLANK OR NOT NUM
        if($helper->checkIntRef($sort) || $sort < 1)
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' ORDER BY sort DESC";
            //---echo($sql);
            //include("inc_db_con.php");
            $row = $db->mysql_fetch_one($sql);
            $old = $row['sort'];
            $sort = $old + 1;
        }
        //GET ID NUMBER
        $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status ORDER BY id DESC";
            //---echo($sql);
        //include("inc_db_con.php");
        $row = $db->mysql_fetch_one($sql);
        $idold = $row['id'];
        $id = $idold + 1;
        //ADD RECORD
        $sql = "INSERT INTO assist_".$cmpcode."_caps_list_status SET id = ".$id.", value = '".$val."', sort = ".$sort.", yn = 'Y'";
            //---echo($sql);
        //include("inc_db_con.php");
		$db->db_insert($sql);
    }
?>
<h1 >Setup >> Status</b></h1>
<p style="margin-top: -10px; margin-bottom: 20px">Please note that some status' may not be edited/deleted as they are a part of the system.</p>
<table>
	<tr>
		<th>ID</th>
		<th>Value</th>
		<th>Order</th>
		<th>&nbsp;</th>
	</tr>
<form name=add method=post action=setup_status.php>
	<tr>
		<td class=center>#</td>
		<td><input type="text" name="val" size="20"></td>
		<td><input type="text" name="sort" size="6"></td>
		<td><input type="submit" value="Add" name="B3" class=isubmit /></td>
	</tr>
</form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' ORDER BY sort ASC";
//include("inc_db_con.php");
$n = $db->db_get_num_rows($sql);
if($n > 0)
{
    /*while($row = mysql_fetch_array($rs))
    {*/
	$rows = $db->mysql_fetch_all($sql);
	foreach($rows as $row) {
    ?>
	<tr>
		<td class=center><?php echo $row['id'];?></td>
		<td ><?php echo $row['value'];?></td>
		<td class=center><?php echo $row['sort'];?></td>
		<td ><?php if($row['id'] > 5) {?><input type=button value=Edit name=B4 onclick="Edit(<?php echo($row['id'])?>)"><?php }else{echo("&nbsp;");}?></td>
	</tr>
    <?php
    }
}
else
{
    echo("no rows");
}
//mysql_close();
?>
</table>
<?php $helper->displayGoBack("setup.php","Go Back"); ?>
<script type=text/javascript>
	$("input:button").button().css("font-size","75%");
	$("input:submit").button().css("font-size","75%");
function Edit(id) {
    document.location.href = "setup_status_edit.php?u="+id;
}
$(function() {
	$("th").addClass("center");
});
</script>

</body>

</html>
