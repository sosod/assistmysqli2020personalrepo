<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Complaints Assist: Transfer</b></h1>
<?php
$callid = $_POST['callid'];
$callrespid = $_POST['callrespid'];

$sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
//include("inc_db_con.php");
    $call = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
//mysql_close();

$callconname = $call['callconname'];
$callcontel = $call['callcontel'];
$callconaddress1 = $call['callconaddress1'];
$callconaddress2 = $call['callconaddress2'];
$callconaddress3 = $call['callconaddress3'];
$callarea = $call['callarea'];
$callurgencyid = $call['callurgencyid'];
$callstatusid = $call['callstatusid'];
$callmessage = $call['callmessage'];

$callconname2 = str_replace("&#39","'",$callconname);
$callcontel2 = str_replace("&#39","'",$callcontel);
$callconaddress12 = str_replace("&#39","'",$callconaddress1);
$callconaddress22 = str_replace("&#39","'",$callconaddress2);
$callconaddress32 = str_replace("&#39","'",$callconaddress3);
$callarea2 = str_replace("&#39","'",$callarea);
$callmessage2 = str_replace("&#39","'",$callmessage);
if($callurgencyid == "X")
{
    $callurgencyid = 2;
}

$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE id = ".$callurgencyid;
//include("inc_db_con.php");
$row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
$urg = $row['value'];
//mysql_close();
$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE id = ".$callstatusid;
//include("inc_db_con.php");
$row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
$stat = $row['value'];
//mysql_close();

$sql = "UPDATE assist_".$cmpcode."_caps_call ";
$sql = $sql."SET callrespid = ".$callrespid." ";
$sql = $sql."WHERE callid = ".$callid;
//include("inc_db_con.php");
$db->db_update($sql);
    $tref = "CAPS";
    $tsql = $sql;
    $trans = "Transferred call ".$callid;
    include("inc_transaction_log.php");

$sql = "INSERT INTO assist_".$cmpcode."_caps_log ";
$sql = $sql."SET ";
$sql = $sql."logtkid = '".$tkid."', ";
$sql = $sql."logurgencyid = ".$callurgencyid.", ";
$sql = $sql."logrespid = ".$callrespid.", ";
$sql = $sql."logadmintext = 'Transferred call to another respondant.', ";
$sql = $sql."logemail = 'Y', ";
$sql = $sql."logstatusid = ".$callstatusid.", ";
$sql = $sql."logcallid = ".$callid.", ";
$sql = $sql."logdate = '".$today."', ";
$sql = $sql."logsubmitdate = '".$today."', ";
$sql = $sql."logsubmittkid = '".$tkid."', ";
$sql = $sql."logutype = 'T'";
//include("inc_db_con.php");
$db->db_insert($sql);
    $tref = "CAPS";
    $tsql = $sql;
    $trans = "Added call log of transfer of call ".$callid.".";
    include("inc_transaction_log.php");

$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant WHERE id = ".$callrespid;
//include("inc_db_con.php");
    $resp = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
//mysql_close();

$mess = str_replace(chr(10),"<br>",$callmessage2);
$sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$resp['admin']."' AND tkstatus = 1";
//include("inc_db_con.php");
$row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
//mysql_close();
if(strlen($row['tkemail'])>0)
{
    $to = $row['tkemail'];
    $from = "no-reply@ignite4u.co.za";
    $subject = "Complaint ".isset($call['callref'])?$call['callref']:''." has been transferred.";
    $message = "<font face=arial>";
    $message = $message."<p><small><i>A complaint has been transferred to your department/division:</i><br>";
    $message = $message."<b>Call ref:</b> ".isset($call['callref'])?$call['callref']:''."<br>";
    $message = $message."<b>Area/Ward/Town:</b> ".$callarea2."<br>";
    $message = $message."<b>Resident's name:</b> ".$callconname2."<br>";
    $message = $message."<b>Resident's tel:</b> ".$callcontel2."<br>";
    $message = $message."<b>Resident's address:</b> ".$callconaddress12;
    if(strlen($callconaddress2)>0){$message.=", ".$callconaddress22;}
    if(strlen($callconaddress3)>0){$message.=", ".$callconaddress32;}
    $message.="<br>";
    $message = $message."<b>Urgency:</b> ".$urg."<br>";
    $message = $message."<b>Status:</b> ".$stat."<br>";
    $message = $message."<b>Message:</b> ".$mess."";
    $sql = "SELECT a.logemail, ";
    $sql.= "a.logdate, a.logadmintext, ";
    $sql.= "t.tkname as n, t.tksurname as sn ";
    $sql.= "FROM assist_".$cmpcode."_caps_log a, ";
    $sql.= "assist_".$cmpcode."_timekeep t ";
//    $sql.= ", assist_".$cmpcode."_caps_list_respondant p ";
    $sql.= "WHERE a.logcallid = ".$callid." ";
//    $sql.= "AND a.logrespid = p.id ";
//    $sql.= "AND p.admin = t.tkid ";
    $sql.= "AND a.logtkid = t.tkid ";
    $sql.= "AND (a.logutype = 'U' OR a.logutype = 'A') ";
    $sql.= "ORDER BY logid DESC";
	$rows = $db->mysql_fetch_all($sql);//include("inc_db_con.php");
    if($helper->db_get_num_rows($sql)>0)
    {
        $message.= "</small></p><p><small>The following updates have been made to the complaint:";
        /*while($row = mysql_fetch_array($rs))
        {*/
		foreach($rows as $row) {
            $message.= "<br><b>".date("d F Y",$row['logdate'])." - ".$row['n']." ".$row['sn']." said:</b><br>";
            $message.= str_replace(chr(10),"<br>",$row['logadmintext']);
        }
    }
    //mysql_close();
    $message = $message."</small></p></font>";
    //echo($message."<hr>");
$demail = "";
$vemail = "";
if(strlen($resp['hodept'])>1)
{
    $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$resp['hodept']."' AND tkstatus = 1";
    //include("inc_db_con.php");
    $drow = $db->mysql_fetch_one($sql);
    //mysql_close();
    $demail = $drow['tkemail'];
}
if(strlen($resp['hodiv'])>1)
{
    $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$resp['hodiv']."' AND tkstatus = 1";
    //include("inc_db_con.php");
    $vrow = $db->mysql_fetch_one($sql);
    //mysql_close();
    $vemail = $vrow['tkemail'];
}
    $header = "From: no-reply@ignite4u.co.za\r\nReply-to: ".$from;
    if(strlen($demail)>0)
    {
        $header.= "\r\nCC:".$demail;
    }
    if(strlen($vemail)>0)
    {
        $header.= "\r\nCC:".$vemail;
    }
    $header.= "\r\nContent-type: text/html; charset=us-ascii";
    /*mail($to,$subject,$message,$header);
    $header = "";
    $message = "";*/
    $mail = new ASSIST_EMAIL($to,$subject,$message,"HTML");
    $mail->sendEmail();

}
?>
<p>The complaint has been transferred to <?php echo($resp['value']);?>.</p>
</body>

</html>
