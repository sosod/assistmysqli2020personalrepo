<?php

				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'logged'=>array('text'=>"Logged", 'long'=>false, 'deadline'=>true),
					'complaint'=>array('text'=>"Complaint", 'long'=>true, 'deadline'=>false),
					'area'=>array('text'=>"Area", 'long'=>false, 'deadline'=>false),
					'resident'=>array('text'=>"Resident", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

	$sql = "SELECT c.* FROM ".$dbref."_call c
			INNER JOIN ".$dbref."_list_respondant r
			ON c.callrespid = r.id AND r.yn = 'Y' AND r.admin = '$tkid'
			WHERE c.callstatusid NOT IN (4,5)
			ORDER BY ";
				switch($action_profile['field3']) {
					case "dead_desc":	$sql.= " c.calldate DESC "; break;
					default:
					case "dead_asc":	$sql.= " c.calldate ASC "; break;
				}
			$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
	$calls = $my_assist->mysql_fetch_all($sql);
	//mysql_close();
	foreach($calls as $c) {
		$span = $c['calldate']<$now ? "<span class=\"overdue b\">" : "<span class=\"today b\">";
		$resident = $c['callconname'];
		$resident.= strlen($c['callcontel'])>0 && $c['callcontel']!="N/A" ? "<br />Tel: ".$c['callcontel'] : "";
		$resident.= strlen($c['callconaddress1']) ? "<br />".$c['callconaddress1'] : "";
		$resident.= strlen($c['callconaddress2']) ? "<br />".$c['callconaddress2'] : "";
		$resident.= strlen($c['callconaddress3']) ? "<br />".$c['callconaddress3'] : "";

		$actions[$c['callid']]['ref'] = str_replace("/","/<br />",$c['callref']);
		$actions[$c['callid']]['logged'] = $span.str_replace(" ","<br />",date("d-M-Y H:i",$c['calldate']));
		$actions[$c['callid']]['complaint'] = str_replace(chr(10),"<br />",$c['callmessage']);
		$actions[$c['callid']]['area'] = $c['callarea'];
		$actions[$c['callid']]['resident'] = $resident;
		$actions[$c['callid']]['link'] = "update_call.php?i=".$c['callid'];
	}
				

/*
$now = $today - (3600*24);
echo "<table width=100%><thead>
		<tr>
			<th width=80>Ref</th>
			<th width=100>Logged</th>
			<th>Complaint</th>
			<th>Area</th>
			<th>Resident</th>
			<th width=70></th>
		</tr></thead><tbody>";
	$sql = "SELECT c.* FROM ".$dbref."_call c
			INNER JOIN ".$dbref."_list_respondant r
			ON c.callrespid = r.id AND r.yn = 'Y' AND r.admin = '$tkid'
			WHERE c.callstatusid NOT IN (4,5)
			ORDER BY ";
				switch($action_profile['field3']) {
					case "dead_desc":	$sql.= " c.calldate DESC "; break;
					default:
					case "dead_asc":	$sql.= " c.calldate ASC "; break;
				}
			$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
	$calls = mysql_fetch_all($sql);
	mysql_close();
	foreach($calls as $c) {
		$span = $c['calldate']<$now ? "<span class=overdue><b>" : "<span class=\"today b\">";
		$resident = $c['callconname'];
		$resident.= strlen($c['callcontel'])>0 && $c['callcontel']!="N/A" ? "<br />Tel: ".$c['callcontel'] : "";
		$resident.= strlen($c['callconaddress1']) ? "<br />".$c['callconaddress1'] : "";
		$resident.= strlen($c['callconaddress2']) ? "<br />".$c['callconaddress2'] : "";
		$resident.= strlen($c['callconaddress3']) ? "<br />".$c['callconaddress3'] : "";
		echo "<tr>
				<th>".str_replace("/","/<br />",$c['callref'])."</th>
				<td class=center>".$span.str_replace(" ","<br />",date("d-M-Y H:i",$c['calldate']))."</span></td>
				<td>".str_replace(chr(10),"<br />",$c['callmessage'])."</td>
				<td>".$c['callarea']."</td>
				<td>".$resident."</td>
				<td class=\"center middle\"><input type=button value=Update  onclick=\"viewTask('$mod','$modloc','update_call.php?i=".$c['callid']."');\" /></td>
			</tr>";
	}
echo "</tbody></table>";
*/
?>