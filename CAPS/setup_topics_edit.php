<?php
	require_once 'inc_head.php';

    $id = isset($_GET['id']) ? $_GET['id'] : "";
if(strlen($id)==0)
{
    $id = isset($_POST['id']) ? $_POST['id'] : "";
    $val = isset($_POST['val']) ? $_POST['val'] : "";
    $admin = isset($_POST['admin']) ? $_POST['admin'] : "";
    $hodept = isset($_POST['hodept']) ? $_POST['hodept'] : "";
    $hodiv = isset($_POST['hodiv']) ? $_POST['hodiv'] : "";

    if(strlen($val) > 0 && $admin != "X" && strlen($id) > 0)
    {
        //CREATE SORT INT IF BLANK OR NOT NUM
        //ADD RECORD
        $sql = "UPDATE assist_".$cmpcode."_caps_list_respondant SET value = '".$val."', admin = '".$admin."', hodept = '".$hodept."', hodiv = '".$hodiv."', yn = 'Y' WHERE id = ".$id;
            //---echo($sql);
        //include("inc_db_con.php");
		$db->db_update($sql);
            $tref = "CAPS";
            $tsql = $sql;
            $trans = "Edited respondant ".$id.".";
            include("inc_transaction_log.php");
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
    }
}
else
{
    $type = isset($_GET['t']) ? $_GET['t'] : "";
    if($type == "del") {
        $sql = "UPDATE assist_".$cmpcode."_caps_list_respondant SET yn = 'N' WHERE id = ".$id;
            //---echo($sql);
        //include("inc_db_con.php");
			$db->db_update($sql);
            $tref = "CAPS";
            $tsql = $sql;
            $trans = "Removed respondant ".$id.".";
            include("inc_transaction_log.php");
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
    } elseif($type == "restore") {
        $sql = "UPDATE assist_".$cmpcode."_caps_list_respondant SET yn = 'Y' WHERE id = ".$id;
            //---echo($sql);
        //include("inc_db_con.php");
		$db->db_update($sql);
            $tref = "CAPS";
            $tsql = $sql;
            $trans = "Restored respondant ".$id.".";
            include("inc_transaction_log.php");
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
    }
}
?>
<script type=text/javascript>
function delResp(id) {
    if(confirm("Are you sure you wish to delete this respondant?")==true)
    {
        document.location.href = "setup_topics_edit.php?id="+id+"&t=del";
    }
}
</script>
<h1 >Setup >> Respondants >> Edit</h1>

<table>
	<tr>
		<th>ID</th>
		<th>Dept/Division</th>
		<th>Respondant</th>
		<th>Head of Dept</th>
		<th>Head of Division</th>
	</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant WHERE id = ".$id." ORDER BY value ASC";
//include("inc_db_con.php");
$n = $db->db_get_num_rows($sql);//mysql_num_rows($rs);
if($n > 0)
{
    $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
    $resp = $row['admin'];
    $hod = $row['hodept'];
    $hov = $row['hodiv'];
}
else
{
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
}
//mysql_close();
?>
<form name=add method=post action=setup_topics_edit.php>
	<tr>
		<th><?php echo($id); ?><input type=hidden name=id value=<?php echo($id); ?>></th>
		<td><input type="text" name="val" size="20" value="<?php echo($row['value']);?>" maxlength=50></td>
		<td><select name=admin><option value=X>--- SELECT ---</option>
        <?php
/*$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000'  ORDER BY tkname, tksurname";*/
		/***
		 * QUERY MUST SELECT USERS WHO ARE ASSIGNED MODULE USERS
		 */
		$sql2 = "SELECT tkid, CONCAT(tkname,' ',tksurname) as name 
		FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users
		  ON tkid = usrtkid AND usrmodref = '$modref'
		WHERE tkstatus = 1 AND tkid <> '0000'  
		ORDER BY tkname, tksurname";

		$users = $db->mysql_fetch_fld2_one($sql2,"tkid","name");//$db->mysql_fetch_all($sql2);
		foreach($users as $i => $u) {
			if($resp == $i)
			{
				echo("<option selected value=".$i.">".$u."</option>");
			}else {
				echo("<option value=" . $i . ">" . $u . "</option>");
			}
		}
/*foreach($rows as $row2) {
    $tid = isset($row2['tkid']) ? $row2['tkid'] :"";
	$tname = isset($row2['tkname']) ? $row2['tkname'] :"";
	$tsurname = isset($row2['tksurname']) ? $row2['tksurname'] :"";
    if($resp == $tid)
    {
        echo("<option selected value=".$tid.">".$tname." ".$tsurname."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$tname." ".$tsurname."</option>");
    }
}*/
//mysql_close($rs2);
        ?>
        </select></td>
		<td><select name=hodept>
        <?php
$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid = '".$hod."' ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$d = $db->db_get_num_rows($sql2);
//mysql_close($rs2);
if($d>0)
{
    echo("<option value=X>--- SELECT ---</option>");
}
else
{
    echo("<option selected value=X>--- SELECT ---</option>");
}
		/***
		 * QUERY MUST SELECT USERS WHO ARE ASSIGNED MODULE USERS
		 */
		$sql2 = "SELECT tkid, CONCAT(tkname,' ',tksurname) as name 
		FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users
		  ON tkid = usrtkid AND usrmodref = '$modref'
		WHERE tkstatus = 1 AND tkid <> '0000'  
		ORDER BY tkname, tksurname";

		$users = $db->mysql_fetch_fld2_one($sql2,"tkid","name");//$db->mysql_fetch_all($sql2);
		foreach($users as $i => $u) {
			if($hod == $i)
			{
				echo("<option selected value=".$i.">".$u."</option>");
			}else {
				echo("<option value=" . $i . ">" . $u . "</option>");
			}
		}
//mysql_close($rs2);
        ?>
        </select></td>
		<td ><select name=hodiv>
        <?php
$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid = '".$hov."' ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$v = $db->db_get_num_rows($sql2);//mysql_num_rows($rs2);
//mysql_close($rs2);
if($v>0)
{
    echo("<option value=X>--- SELECT ---</option>");
}
else
{
    echo("<option selected value=X>--- SELECT ---</option>");
}
		/***
		 * QUERY MUST SELECT USERS WHO ARE ASSIGNED MODULE USERS
		 */
		$sql2 = "SELECT tkid, CONCAT(tkname,' ',tksurname) as name 
		FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users
		  ON tkid = usrtkid AND usrmodref = '$modref'
		WHERE tkstatus = 1 AND tkid <> '0000'  
		ORDER BY tkname, tksurname";

		$users = $db->mysql_fetch_fld2_one($sql2,"tkid","name");//$db->mysql_fetch_all($sql2);
		foreach($users as $i => $u) {
			if($hov == $i)
			{
				echo("<option selected value=".$i.">".$u."</option>");
			}else {
				echo("<option value=" . $i . ">" . $u . "</option>");
			}
		}
//mysql_close();
        ?>
        </select></td>
	</tr>
 <tr>
    <td colspan=5><input type=submit value="Save Changes" class=isubmit /> <span class=float><input type=button value=Deactivate onclick=delResp(<?php echo($id); ?>) class=idelete /></span></td>
</tr>
</form>
</table>
<?php $helper->displayGoBack("setup_topics.php","Go Back"); ?>
<script type=text/javascript>
	$("input:button").button().css("font-size","75%");
	$("input:submit").button().css("font-size","75%");
$(function() {
	$("th").addClass("center");
});
</script>
</body>

</html>
