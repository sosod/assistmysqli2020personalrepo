<?php
    require_once "inc_head.php";
	$nav = array(
		'ip'=>array('id'=>"ip",'display'=>"Current Calls",'active'=>"",'url'=>"view.php?st=ip"),
		'cl'=>array('id'=>"cl",'display'=>"Closed Calls",'active'=>"",'url'=>"view.php?st=cl")
	);
	
    //HEADING
	$title = "View Status >> ";
    $st = isset($_REQUEST['st'])? $_REQUEST['st'] : "";
	$search = "";
    if(isset($st) && $st == "cl") {
        $sqlst = "c.callstatusid = 4";
        $title.= "Closed Calls";
		$nav['cl']['active'] = "checked";
    } else {
        $sqlst = "c.callstatusid <> 4 AND c.callstatusid <> 5";
        $title.= "Current Calls";
		$nav['ip']['active'] = "checked";
		$st = "ip";
    }
	
	//PAGING
    $start = isset($_REQUEST['s'])? $_REQUEST['s'] : 0;
	$sql_search = "";
	if(isset($_REQUEST['f'])) {
		$search = $helper->decode(trim($_REQUEST['f']));
		if(isset($search) && strlen($search)>0) {
			$find = explode(" ",$search);
			if(count($find)>0) {
				$sql_search= "AND (";
				$f = 0;
				foreach($find as $needle)
				{
					if($f>0) { $sql_search.=" OR "; }
					$sql_search.= "c.callref LIKE '%".$needle."%'";
					$f++;
				}
				$sql_search.= ")";
			}
		}
	}
	if(!isset($start) || (!checkIntRef($start) && $start != "e")) { $start = 0; }
	$limit = 30;
	$sqls = array();
	$sqls[0] = "SELECT count(callid) as ct ";
	$sqls[1]= "FROM ".$dbref."_call c";
	$sqls[1].= " INNER JOIN ".$dbref."_list_status st ON st.id = c.callstatusid";
	$sqls[1].= " INNER JOIN ".$dbref."_list_respondant rp ON rp.id = c.callrespid ";
	$sqls[1].= " INNER JOIN assist_".$cmpcode."_timekeep tk ON tk.tkid = rp.admin ";
	$sqls[1].= "WHERE ".$sqlst." ".$sql_search;
	$sql = implode(" ",$sqls);
	//include("inc_db_con.php");
		$mr = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
	//mysql_close($con);
	$max = $mr['ct'];
	$pages = ceil($max/$limit);
	if(!is_numeric($start)) {
		$start = $max - ($max - ($limit*($pages-1)));
	}
	$page = ($start / $limit) + 1;

?>
<script type=text/javascript>
function Validate() {
    return false;
}

function viewCall(c) {
    document.location.href = 'view_call.php?i='+c;
}
</script>
<?php
drawNav($nav,$st);
echo "<h1>$title</h1>";
$helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
echo "<table class=noborder><tr><td class=noborder>";
drawPaging('view.php?st='.$st.'&',$search,$start,$limit,$page,$pages,'Search Ref','<input type=hidden name=st value='.$st.'>');

if($start<0) { $start = 0; }
$sqls[0] = "SELECT c.*, st.value status, rp.value resp, CONCAT_WS(' ',tkname,tksurname) as tkn ";
$sqls[2] = "ORDER BY c.callref DESC LIMIT $start, $limit";
$sql = implode(" ",$sqls);
//include("inc_db_con.php");
//$rs = getRS($sql);
$n = $db->db_get_num_rows($sql);
if($n > 0)
{
?>
<table id=list><thead>
    <tr>
        <th>Ref</th>
        <th>Date</th>
        <th>Assigned To</th>
        <th>Area</th>
        <th>Resident</th>
        <th>Address</th>
        <th>Status</th>
        <th>&nbsp;</th>
    </tr></thead><tbody>
    <?php
/*while($row = mysql_fetch_array($rs))
{*/
	$rows = $db->mysql_fetch_all($sql);
	foreach($rows as $row) {
	$callid = $row['callid'];
		//$style = "background-color: #FFFFFF; color: #000000;";
		$style = "";
		$s = $row['callstatusid'];
		/*if(in_array($s,$new_statusid)) {
			$style = "color: #FFFFFF; background-color: ".$result_settings['NEW']['color'];
		} elseif(in_array($s,$closed_statusid)) {
			$style = "color: #FFFFFF; background-color: ".$result_settings['CL']['color'];
		} else {
			$style = "color: #FFFFFF; background-color: ".$result_settings['IP']['color'];
		}*/
		if(in_array($s,$new_statusid)) {
			$style = "ui-widget ui-state-error\" style=\"font-size: 8pt;";
		} elseif(in_array($s,$closed_statusid)) {
			$style = "ui-widget ui-state-ok\" style=\"font-size: 8pt;";
		} else {
			$style = "ui-widget ui-state-info\" style=\"font-size: 8pt; color: ".$result_settings['IP']['color'];
		}
    echo "<tr>";
		echo "<td >".$row['callref']."&nbsp;</td>";
		echo "<td class=center >".date('d-M-Y',$row['calldate'])."&nbsp;</td>";
		echo "<td >".$row['resp']." (".$row['tkn'].")&nbsp;</td>";
		echo "<td >".$row['callarea']."&nbsp;</td>";
		echo "<td >".$row['callconname']."&nbsp;</td>";
		echo "<td >";
		$address = "";
		for($a=1;$a<4;$a++) {
			if(strlen($address)>0 && strlen(trim($row['callconaddress'.$a]))>0) { $address.="<br />"; }
			$address.=trim($row['callconaddress'.$a]);
		}
         echo $address."&nbsp;</td>";
			echo "<td class=center><div class=\"$style\">&nbsp;".$row['status']."&nbsp;</div></td>";
//		echo "<td class=center>".$row['status']."&nbsp;</td>";
		echo "<td class=center><input type=button value=View onclick=\"viewCall($callid);\"></td>";
    echo "</tr>";
}
    ?>
</tbody></table>
<script type=text/javascript>
$(function() {
	$("#list th").addClass("center");
	$("#list th").css("height","25px");
});
</script>
<?php
}
else
{
	echo("<P>There are no calls to view.</p>");
}

echo "</td></tr></table>";
?>
</body>

</html>
