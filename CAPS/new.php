<?php
require_once 'inc_head.php';
//MAP documentation: https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete
$js = "";
?>
<h1>New Complaint</h1>
<?php
$helper = new ASSIST_MODULE_HELPER();
$helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
        <form name=logcall action=new_process.php method=POST onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">

            <table id="call_tbl">
                <tr>
                    <th>Your name:</th>
                    <td ><?php echo($tkname); ?><input type=hidden name=calltkid value=<?php echo($tkid);?>></td>
                </tr>
                <tr>
                    <th>Resident's Name:</th>
                    <td ><input type=text name=callconname size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Resident's Telephone:</th>
                    <td ><input type=text name=callcontel size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th rowspan=3>Resident's Address:</th>
                    <td ><input type=text name=callconaddress1 size=40 maxlength=90>
					</td>
                </tr>
                <tr id="callconaddress2">
                    <td ><input type=text name=callconaddress2  size=40 maxlength=90></td>
                </tr>
                <tr id="callconaddress3">
                    <td ><input type=text name=callconaddress3  size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Area/Ward/Town:</th>
                    <td ><input type=text name=callarea size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Respondant:</th>
                    <td ><select name=callrespid><option selected value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * 
                            		FROM assist_".$cmpcode."_caps_list_respondant r
                            		INNER JOIN assist_".$cmpcode."_timekeep t
                            		ON  t.tkid = r.admin
                            		LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mmu
                            		ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '$modref' 
                            		WHERE r.yn = 'Y' ORDER BY value ASC";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $id = $row['id'];
								if($row['tkstatus']==1 && !is_null($row['usrmodref'])) {
									$disabled = "";
	                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
								} else {
									$disabled = "disabled=disabled";
	                                $val = $row['value']." (No active Admin)";
								}
                                echo("<option $disabled value=".$id.">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Urgency:</th>
                    <td ><select size="1" name="callurgencyid"><option selected value=X>--- SELECT ---</option>
<?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE yn = 'Y' ORDER BY sort ASC";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option value=".$id.">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Complaint:</th>
                    <td >
                        <textarea rows="6" name="callmessage" cols="40"></textarea></td>
                </tr>
                <?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";

//echo $sql;
				$rows = $db->mysql_fetch_all($sql);
				foreach($rows as $row) {
    ?>
                <tr>
                    <th><?php echo($row['udfivalue']); ?>:</th>
                    <td>
                    <?php
                    switch($row['udfilist']) {
                        case "Y":
                            echo("<select name=".$row['udfiid']." ".($row['udfirequired'] == 1?' req=1' :'')."><option value=X>---SELECT---</option>");
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
            //include("inc_db_con2.php");
							$rows = $db->mysql_fetch_all($sql2);
							foreach($rows as $row2) {
                                        echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                    }
                                    echo("</select>");
                                    break;
                                case "T":
                                    echo("<input type=text name=".$row['udfiid']." size=50 ".($row['udfirequired'] == 1?' req=1' :'').">");
                                    break;
                                case "M":
                                    echo("<textarea name=".$row['udfiid']." rows=5 cols=40 ".($row['udfirequired'] == 1?' req=1' :'')."></textarea>");
                                    break;
                                case "D":
                                    //echo("<input class='datepicker hasDatepicker'  type='text' name='".$row['udfiid']."' size='15' readonly='readonly'/>");
									$displayObject = new CAPS_DISPLAY();
									$value = date("d-M-Y");
									$js.= $displayObject->drawFormField("DATE",array('id'=>$row['udfiid'], 'name'=>$row['udfiid'],'req'=>$row['udfirequired'] == 1?'1' :'0'),$value);
                                    break;
                                case "N":
                                     echo("<input type='text' name='".$row['udfiid']."' size='15' ".($row['udfirequired'] == 1?' req=1' :'').">&nbsp;<small><i>Only numeric values are allowed.</i></small>");
                                    break;
                                default:
                                    echo("<input type=text name=".$row['udfiid']." size='50' ".($row['udfirequired'] == 1?' req=1' :'').">");
                                    break;
                            }
                            ?>
                    </td>
                </tr>


                            <?php
                        }
                        //mysql_close();
                        ?>
                <tr >
                    <th>Attach document(s):</th>
                    <td><div id="firstdoc"><input type="file" name="attachments[]" id="attachment" size="30"/></div>
					<span class=float style="margin-top: 5px;"><a href="javascript:void(0)" id="attachlink">Attach another file</a></span></td>
                </tr>
                <tr>
					<th></th>
                    <td>
                        <input type="submit" value="Submit" name="B1" class=isubmit id="submit" />
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
            </table>

        </form>

		<script type=text/javascript>
		$(document).ready(function(){
			<?php echo $js; ?>
			$("th").addClass("left");
			$("th").addClass("top");
			$("tr").attr("height","30");
			
                $('#attachlink').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdoc');
                });


                $("#call_tbl .isubmit").click(function (e) {
					//e.preventDefault();
					//AssistHelper.processing();
					var $form = $("form[name=logcall]");
					var err = false;
					$form.find("select").each(function() {
						//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
						$(this).removeClass("required");
						if(($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
							if($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
								err = true;
								$(this).addClass("required");
							}
						}
					});
					$form.find("input:text, textarea").each(function() {
						//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
						$(this).removeClass("required");
						if(($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
							if($(this).val() == "" || $(this).val().length == 0) {
								err = true;
								$(this).addClass("required");
							}
						}
					});

					if(err) {
						//AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
						alert("Please complete the required fields highlighted in red.");
						e.preventDefault(); //This prevents the page from submitting.

					}
				});




        });

			function Validate(me) {
            var cname = me.callconname.value;
            var ctel = me.callcontel.value;
            var cadd1 = me.callconaddress1.value;
            var cadd2 = me.callconaddress2.value;
            var cadd3 = me.callconaddress3.value;
            var area = me.callarea.value;
            var resp = me.callrespid.value;
            var mess = me.callmessage.value;
            var urgid = me.callurgencyid.value;

            if(cname.length == 0)
            {
                alert("Please enter the resident's name.");
            }
            else
            {
                if(ctel.length == 0)
                {
                    if(confirm("You have not entered a contact number for the resident\n\nAre you sure you wish to continue?")==true)
                    {
                        me.callcontel.value = "N/A";
                        ctel = "N/A";
                    }
                }
                if(ctel.length > 0)
                {
                    if(cadd1.length==0 && cadd2.length == 0 && cadd3.length==0)
                    {
                        if(confirm("You have not entered address details for the resident\n\nAre you sure you wish to continue?")==true)
                        {
                            me.callconaddress1.value = "N/A";
                            cadd1 = "N/A";
                        }
                    }
                    if(cadd1.length > 0 || cadd2.length > 0 || cadd3.length > 0)
                    {
                        if(resp == "X")
                        {
                            alert("Please select the respondant.");
                        }
                        else
                        {
                            if(mess.length == 0)
                            {
                                alert("Please enter a message indicating the complaint.");
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    </script>
</body></html>
