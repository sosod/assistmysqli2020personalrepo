<?php

class CAPS extends ASSIST_MODULE_HELPER {


	private $headings = array(
		'head'=>array(
			'callref'=>"Reference",
			'calldate'=>"Date Logged",
			'calltkid'=>"Logged By",
			'callconname' =>"Resident's Name",
			'callcontel'=>"Resident's Telephone",
			'callconaddress'=>"Resident's Address",
			'callrespid'=>"Assigned To",
			'callurgencyid'=>"Urgency",
			'callmessage'=>"Complaint",
			'callstatusid'=>"Status",
			'callattachment'=>"Attachment",
		),
		'types'=>array(
			'callref'=>"REF",
			'calldate'=>"DATETIME",
			'calltkid'=>"LIST",
			'callconname'=>"SMLTEXT",
			'callcontel'=>"SMLTEXT",
			'callconaddress'=>"TEXT",
			'callrespid'=>"LIST",
			'callurgencyid'=>"LIST",
			'callmessage'=>"TEXT",
			'callstatusid'=>"LIST",
			'callattachment'=>"ATTACH",
		),
	);


	const REFTAG = "";

	public function __construct() {
		parent::__construct();

		//get UDF headings first
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".strtoupper($this->getModRef())."'";
		//while($row = mysql_fetch_assoc($rs)) {
			//echo $sql;
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $row) {
			$this->headings['head'][$row['udfiid']] = $row['udfivalue'];
			switch($row['udfilist']) {
				case "Y":
					$x = "UDFLIST";
					break;
				case "N":
					$x = "UDFNUM";
					break;
				case "D":
					$x = "UDFDATE";
					break;
				case "T":
				case "M":
				default:
					$x = "UDFTEXT";
					break;
			}
			$this->headings['types'][$row['udfiid']] = $x;
		}
//ASSIST_HELPER::arrPrint($this->headings);
	}


	//HEADINGS
	function getListHeadings() {
		$data = array();
		foreach($this->headings['list_view'] as $h) {
			$data[$h] = $this->headings['head'][$h];
		}
		return $data;
	}
	function getAllHeadings() {
		return $this->headings['head'];
	}
	function getHeadingTypes() {
		return $this->headings['types'];
	}


	
	public function getUDFData($key) {
		$sql = "SELECT udfvid as id, udfvvalue as name FROM assist_".$this->getCmpCode()."_udfvalue WHERE udfvindex = ".$key." AND udfvyn = 'Y' ORDER BY udfvvalue";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}




	function getUsedAddUsers() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_call r
				ON r.calltkid = t.tkid AND r.callstatusid <> 5
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}

	/*function getUsedResponsiblePersons() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_call r
				ON r.calltkid = t.tkid AND r.callstatusid <> 5
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}*/

	function getUsedPortfolios() {
		$data = array();
		$sql = "SELECT DISTINCT p.id, CONCAT(p.value,' (',IF(tk.tkname IS NULL,'Unspecified User',CONCAT(tk.tkname,' ',tk.tksurname)),')') as name 
				FROM `".$this->getDBRef()."_list_respondant` p 
				INNER JOIN ".$this->getDBRef()."_call c 
				ON c.callrespid = p.id 
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep tk 
				ON tk.tkid = p.admin ORDER BY p.value";
		/*$sql = "SELECT DISTINCT p.id as id, CONCAT(d.value,' - ',p.value) as name
				FROM ".$this->getDBRef()."_list_portfolio p
				INNER JOIN ".$this->getDepartmentDBRef()." d
				ON p.deptid = d.id
				INNER JOIN ".$this->getDBRef()."_res r 
				ON r.resportfolioid = p.id AND r.resstatusid <> 'CN'
				ORDER BY ".$this->getDepartmentSortField("d").", p.value";*/
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}	

	function getUsedUrgency() {
		$data = array();
		$sql = "SELECT DISTINCT u.id as id, u.value as name 
				FROM ".$this->getDBRef()."_list_urgency u 
				INNER JOIN ".$this->getDBRef()."_call r 
				ON r.callurgencyid = u.id AND r.callstatusid <> 5 
				WHERE yn = 'Y' ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	
	function getUsedStatus() {
		$data = array();
		$sql = "SELECT s.id as id, s.value as name 
				FROM ".$this->getDBRef()."_list_status s 
				INNER JOIN ".$this->getDBRef()."_call r 
				ON r.callstatusid = s.id 
ORDER BY sort";
//				WHERE s.id <> 5 
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}

    function forceCloseTicket($log_status,$current_status_id,$call_id){
	    $result = 0;
        if ($log_status !== $current_status_id ){
            $sql = "UPDATE assist_".$this->getCmpCode()."_caps_call SET callstatusid = ".$log_status." WHERE callid = ".$call_id;
            $result = $this->db_update($sql);
        }
        return $result;
    }





	public function __destruct() {
		parent::__destruct();
	}

}



?>