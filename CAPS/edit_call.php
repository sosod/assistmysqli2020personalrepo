<?php
require_once 'inc_head.php';
$b = isset($_REQUEST['b'])? $_REQUEST['b'] : "";
$style = isset($style) ? $style : "";
$js = "";
//arrPrint($_SERVER);
$src = $_SERVER['HTTP_REFERER'];
$src = explode("/",$src);
$urlback = $src[count($src)-1];
$cancel_back = substr($urlback,0,1);
switch($cancel_back) {
	case "u": $cancel_back = "update.php"; break;
	case "v": default: $cancel_back = "view.php"; break;
}

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

$callid = $_REQUEST['callid'];

$req = "edit_call.php?callid=".$callid."&b=".$b;
$logsort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array("logdateact","DESC");


$sql = "SELECT * FROM ".$dbref."_call WHERE callid = $callid";
$call = $db->mysql_fetch_one($sql);
/*$callgeolocation = $call['callgeolocation'];
$geotags = array();
$geotags = explode(",",$callgeolocation);*/
/*$callref = $_POST['callref'];
$calldate = $_POST['calldate'];
$calltkid = $_POST['logtkid'];
$callconname = $_POST['callconname'];
$callcontel = $_POST['callcontel'];
$callconaddress1 = $_POST['callconaddress1'];
$callconaddress2 = $_POST['callconaddress2'];
$callconaddress3 = $_POST['callconaddress3'];
$callarea = $_POST['callarea'];
$callrespid = $_POST['callrespid'];
$callurgencyid = $_POST['callurgencyid'];
$callmessage = $_POST['callmessage'];
$callid = $_POST['callid'];
$callstatusid = $_POST['callstatusid'];*/
?>

<h1>Edit a call</b></h1>
<?php $helper->displayResult($result); ?>
<h2>Complaint Details</h2>
        <form name=logcall action=edit_process.php method="post"  enctype="multipart/form-data">
            <input type="hidden" name="utype" id="utype" value="<?php echo $_REQUEST['utype']; ?>" />
            <input type="hidden" name="callid" id="callid" value="<?php echo $callid; ?>" />
			<input type=hidden name=src value="<?php echo $urlback; ?>" />
			<!--<input type="hidden" id="latitude" name="latitude" value="<?php /*echo isset($geotags[0])?$geotags[0]:""; */?>">
			<input type="hidden" id="longitude" name="longitude" value="<?php /*echo isset($geotags[1])?$geotags[1]:""; */?>">-->
            <table id=complaint width=650>
                <tr>
                    <th  width=150>Reference:</th>
                    <td  width=500>
                        <?php
                        echo($call['callref']);
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Date Logged:</th>
                    <td >
                        <?php
                        echo(date("d-M-Y H:i", $call['calldate']));
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Logged by:</th>
                    <td >
                        <?php
                        $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$call['calltkid']."'";
                        //include("inc_db_con.php");
                        //$row = mysql_fetch_array($rs);
						$row = $db->mysql_fetch_one($sql);
                        echo($row['tkname']." ".$row['tksurname']);
                        //mysql_close();
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Edited By:</th>
                    <td ><?php echo($tkname); ?><input type=hidden name=calltkid value="<?php echo($tkid); ?>"></td>
                </tr>
                <tr>
                    <th>Resident's Name:</th>
                    <td ><input type=text name=callconname size=40 maxlength=90 value="<?php echo $call['callconname']; ?>"></td>
                </tr>
                <tr>
                    <th>Resident's Telephone:</th>
                    <td ><input type=text name=callcontel size=40 maxlength=90 value="<?php echo $call['callcontel']; ?>"></td>
                </tr>
                <tr>
                    <th  rowspan=3>Resident's Address:</th>
                    <td ><input type=text name=callconaddress1 id="pac-input" size=40 maxlength=90 value="<?php echo $call['callconaddress1']; ?>">
					</td>
                </tr>
                <tr id="callconaddress2">
                    <td ><input type=text name=callconaddress2 size=40 maxlength=90 value="<?php echo $call['callconaddress2']; ?>"></td>
                </tr>
                <tr id="callconaddress3">
                    <td ><input type=text name=callconaddress3 size=40 maxlength=90 value="<?php echo $call['callconaddress3']; ?>"></td>
                </tr>
                <tr>
                    <th>Area/Ward/Town:</th>
                    <td ><input type=text name=callarea size=40 maxlength=90 value="<?php echo $call['callarea']; ?>"></td>
                </tr>
                <tr>
                    <th>Respondant:</th>
                    <td >
                        <select name=callrespid><option value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant r, assist_".$cmpcode."_timekeep t
                                WHERE r.yn = 'Y' AND t.tkstatus = 1 AND t.tkid = r.admin ORDER BY value ASC";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $id = $row['id'];
                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
                                echo("<option value=".$id." ".($call['callrespid'] == $id ? "selected=selected" : "").">".$val."</option>");
								if($call['callrespid']==$id) { $admin = $row['tkname']." ".$row['tksurname']; }
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td >
                        <select size="1" name="callstatusid">
                            <option value='X'>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' ORDER BY sort ASC";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option value=".$id." ".($call['callstatusid'] == $id ? "selected=selected" : "").">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Urgency:</th>
                    <td ><select size="1" name="callurgencyid"><option  value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE yn = 'Y' ORDER BY sort ASC";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option value=".$id." ".($call['callurgencyid'] == $id ? "selected=selected" : "").">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Complaint:</th>
                    <td >
                        <textarea rows="6" name="callmessage" cols="40"><?php echo $call['callmessage']; ?></textarea></td>
                </tr>
                <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];

                                //include("inc_db_con2.php");
                                $udf = $db->db_get_num_rows($sql2);
                                $row2 = $db->mysql_fetch_one($sql2)//mysql_fetch_array($rs2);
                                //mysql_close($con2);
                ?>
                                <tr>
                                    <th><?php echo($row['udfivalue']); ?>:</th>
                            <td >
                        <?php
                                switch ($row['udfilist'])
                                {
                                    case "Y":
                                        if ($helper->checkIntRef($row2['udfvalue']))
                                        {
                                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                            //include("inc_db_con2.php");
                                            if ($db->db_get_num_rows($sql2) > 0)
                                            {
                                                $row3 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                                            }
                                            //mysql_close($con2);
                                        }
                                        echo("<select name='".$row['udfiid']."' ".($row['udfirequired'] == 1?' req=1' :'')."><option value=X>---SELECT---</option>");
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                                        //include("inc_db_con2.php");
                                        /*while ($row2 = mysql_fetch_array($rs2))
                                        {*/
										$rows = $db->mysql_fetch_all($sql2);
										$row3_udfvid = isset($row3['udfvid'])? $row3['udfvid'] : "";
										foreach($rows as $row2) {
                                            echo("<option ".($row3_udfvid == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                        }
                                        //mysql_close($con2);
                                        echo("</select>");
                                        break;
                                    case "T":
                                        if (empty($row2['udfid']))
                                        {
                                            echo("<input type=text name='".$row['udfiid']."' value='' size='50'>");
                                        }
                                        else
                                        {
                                            echo("<input type=text name='".$row['udfiid']."' value='".$row2['udfvalue']."' size='50' ".($row['udfirequired'] == 1?' req=1' :'').">");
                                        }
                                        break;
                                    case "M":
                                        if (empty($row2['udfid']))
                                        {
                                            echo ("<textarea name='".$row['udfiid']."' rows='5' cols='40' ></textarea>");
                                        }
                                        else
                                        {
                                            echo("<textarea name='".$row['udfiid']."' rows='5' cols='40' ".($row['udfirequired'] == 1?' req=1' :'').">".$row2['udfvalue']."</textarea>");
                                        }
                                        break;
                                    case "D":
                                        //echo "<input class='date-type' type='text' name='".$row['udfiid']."' value='".(empty($row2['udfid']) ? "" : $row2['udfvalue'])."' size='15' readonly='readonly'/>";
										$displayObject = new CAPS_DISPLAY();
										$value = (empty($row2['udfvalue']) ? "" : $row2['udfvalue']);
										$js.= $displayObject->drawFormField("DATE",array('id'=>$row['udfiid'], 'name'=>$row['udfiid'],'req'=>$row['udfirequired'] == 1?'1' :'0'),$value);
                                        break;
                                    case "N":
                                        echo "<input type='text' name='".$row['udfiid']."' value='".(empty($row2['udfid']) ? "" : $row2['udfvalue'])."' size='15' />&nbsp;<small><i>Only numeric values are allowed.</i></small>";
                                        break;
                                    default:
                                        //echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                        break;
                                }
                        ?>
                                &nbsp;</td>
                        </tr>


<?php }; ?>
                <tr>
                    <th>Attach document(s):</th>
                    <td>
						<span class=float><a href="javascript:void(0)" id="attachlinkMAIN">Attach another file</a></span>
						<div id=firstdoc><input type="file" name="attachments[]" id="attachment" size="30"/></div>
					</td>
                </tr>
                <?php
                            $sql = "SELECT * FROM  assist_".$cmpcode."_caps_attachments AS c WHERE c.callid = ".$callid;
                            //$rs = getRS($sql);
                ?>
                
                            <tr>
                                <th>Attachment(s):</th>
                                <td>
                        <?php
                            if (!$db->db_get_num_rows($sql))
                            {
                                echo ("<i>No attachments found.</i>");
                            }
                            else
                            {
                                echo "<table style='border:none;'>";
                            }

							$rows = $db->mysql_fetch_all($sql);
							foreach($rows as $row) {
                                $file = "../files/".$cmpcode."/CAPS/".$row['system_filename'];
                                if (!file_exists($file))
                                    continue;
                                $call_attach_id = $row['id'];

                                echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:void(0)' class=downloadAttach id=".$call_attach_id.">".$row['original_filename']."</a>
                       </td>
                       </tr>");
                            }
                            if ($db->db_get_num_rows($sql) > 0)
                            {
                                echo("</table>");
                            }
                        ?>

                        </td>
                    </tr>
                    <tr>
						<th></th>
                        <td >
                            <input type="submit" value="Save Changes" name="B1" class=isubmit />
                            <input type="reset" value="Reset" name="B2">
                            <span class=float><input type=button value="Cancel Complaint" class=idelete id=cancelme /></span>       
							</td>
                    </tr>
                </table>

            </form>

            <h2 class=fc>Updates</h2>
            <table id=update  width=650>
<?php
$callstatusid = isset($callstatusid)? $callstatusid : 0 ;
                            if ($callstatusid != 4 && $callstatusid != 5)
                            {
?>
                                <!-- <form name=update method=post action=view_call_update.php enctype="multipart/form-data">
                                    <tr>
                                        <th>Date</td>
                                        <th colspan=2>Update message</td>
                                    </tr>

                                    <tr>
                                        <td class=center><input type=hidden name=logdate value=<?php echo $today; ?>><?php echo(date("d-M-Y H:i", $today)); ?></td>
                                        <td colspan=2><textarea cols=80 rows=7 name=logadmintext></textarea><br>
                                            <input type=hidden name=email value=Y>
                                            <input type=hidden name=utype value=U>
                                            <input type=hidden size=1 name=logtkid value=<?php echo($calltkid); ?>>
                                            <input type=hidden size=1 name=logurgencyid value=<?php echo($callurgencyid); ?>>
                                            <input type=hidden size=1 name=logrespid value=<?php echo($callrespid); ?>>
                                            <input type=hidden size=1 name=logstatusid value=<?php echo($callstatusid); ?>>
                                            <input type=hidden size=1 name=callid value=<?php echo($callid); ?>>
											<span class=float style="margin-top: 5px"><a href="javascript:void(0)" id="attachlink">Attach another file</a></span>
											<div id=firstdocUP style="margin-top: 5px;"><input type="file" name="attachments[]" id="attachment" size="30"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class=center>
                                            <input type=submit class=isubmit value="<?php echo("Send message to ".$admin); ?>">
                                        </td></tr>
                                </form> -->
            <?php
                            }
								
								$sql = "SELECT * FROM  `".$dbref."_attachments` WHERE logid IN (
										SELECT logid FROM ".$dbref."_log WHERE logcallid =".$callid.")";
								//$rs = getRS($sql);
								$attach = array();
								/*while($row = mysql_fetch_assoc($rs)) {
								//arrPrint($row);
									$attach[$row['logid']][] = $row;
								}*/
								//$attach = $db->mysql_fetch_all_fld($sql,'logid');
								$rows = $db->mysql_fetch_all($sql);
								foreach($rows as $row) {
								//arrPrint($row);
								$attach[$row['logid']][] = $row;
								}
								//mysql_close();//arrPrint($attach);

                                $sql = "SELECT a.logid, a.logemail, a.logdateact, p.admin, a.logsubmittkid,  ";
                                $sql.= "a.logdate, a.logadmintext, s.value as status, a.logstatusid, ";
                                $sql.= "t.tkname as n, t.tksurname as sn, tb.tkname as nb, tb.tksurname as snb, logutype ";
                                $sql.= "FROM assist_".$cmpcode."_caps_log a, ";
                                $sql.= "assist_".$cmpcode."_caps_list_status s, ";
                                $sql.= "assist_".$cmpcode."_timekeep t, ";
                                $sql.= "assist_".$cmpcode."_timekeep tb, ";
                                $sql.= "assist_".$cmpcode."_caps_list_respondant p ";
                                $sql.= "WHERE a.logcallid = ".$callid." ";
                                $sql.= "AND a.logstatusid = s.id ";
                                $sql.= "AND a.logrespid = p.id ";
                                $sql.= "AND p.admin = t.tkid ";
                                $sql.= "AND a.logsubmittkid = tb.tkid ";
                                $sql.= "AND a.logadmintext <> '' ";
                                $sql.= "AND (a.logutype = 'U' OR a.logutype = 'A' OR a.logutype='E') ";
                                $sql.= "ORDER BY ".$logsort[0]." ".$logsort[1].", logid DESC";
								$logs = $db->mysql_fetch_all($sql);
								if(count($logs)>0) {
									echo "<tr class=\"ui-widget ui-state-white\">
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdate></span>Date Logged</th>
											<th>Update</th>
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdateact></span>Details</th>
										</tr>";
									foreach($logs as $l) {
										$i = $l['logid'];//echo $i;
										$attachments = array();
										if (isset($attach[$i])) {
											foreach ($attach[$i] as $a) {
												if (file_exists("../files/" . $cmpcode . "/CAPS/" . $a['system_filename'])) {
													$attachments[] = "<a href=javascript:void(0) class=downloadAttach id=" . $a['id'] . ">" . $a['original_filename'] . "</a>";
												}
											}
										}
										if(count($attachments)>0) {
											$log_attach = "<br />Attachment".(count($attachments)>1 ? "s" : "").":<br />".implode("<br />",$attachments);
										} else {
											$log_attach = "";
										}
										
										if(strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):")!==false) {
											$log_text = substr($helper->decode($l['logadmintext']),0,strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):"));
										} else {
											$log_text = $helper->decode($l['logadmintext']);
										}
										//$attach_pos = strpos(decode($l['logadmintext']),"Complaint Attachment(s):");
										//$log_text = $attach_pos!==false ? "true" : "false";
										
										echo "<tr>
												<td class=center>".date("d-M-Y H:i",$l['logdate'])."</td>
												<td>".$log_text;
												//arrPrint($attachments);
										echo "
												".$log_attach."</td>
												</td><td>
												<span class=b>Status: </span>
												<br /><span class=\"".$style."\">".$l['status']."</span>
												<br /><span class=b>Respondant: </span>
												<br />".$l['n']." ".$l['sn']."
												".(date("dMYHi",$l['logdate'])!=date("dMYHi",$l['logdateact']) ? "<br /><span class=b>Action taken: </span><br />".date("d-M-Y H:i",$l['logdateact']) : "")."
												</td>
											</tr>";
									}
								}
								//mysql_close();
        ?>
                            </table>
<?php
$helper->displayGoBack($urlback,"Go Back");
                            //include("inc_back.php");
?>

        <script type ="text/javascript">

            $(document).ready(function(){
				var logsort = new Array('<?php echo $logsort[0]; ?>','<?php echo $logsort[1]; ?>');
				var sorting = new Array();
				sorting['logdate'] = "logdateact";
				sorting['logdateact'] = "logdate";
				sorting['DESC'] = "ASC";
				sorting['ASC'] = "DESC";

				$("input:button").button().css("font-size","75%");
				$("input:submit").button().css("font-size","75%");
				$("input:reset").button().css("font-size","75%");

				$("#complaint .isubmit").click(function (e) {
					//e.preventDefault();
					//AssistHelper.processing();
					var $form = $("form[name=logcall]");
					var err = false;
					$form.find("select").each(function() {
						//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
						$(this).removeClass("required");
						if(($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
							if($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
								err = true;
								$(this).addClass("required");
							}
						}
					});
					$form.find("input:text, textarea").each(function() {
						//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
						$(this).removeClass("required");
						if(($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
							if($(this).val() == "" || $(this).val().length == 0) {
								err = true;
								$(this).addClass("required");
							}
						}
					});

					if(err) {
						//AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
						alert("Please complete the required fields highlighted in red.");
						e.preventDefault(); //This prevents the page from submitting.

					}
				});


				
				
				$("#update #logdate, #update #logdateact").click(function() {
					var url = '<?php echo $req; ?>';
					var id = $(this).attr("id");
					var newsort = new Array();
					newsort[0] = id;
					if(id==logsort[0]) {
						newsort[1] = sorting[logsort[1]];
					} else {
						newsort[1] = "DESC";
					}
					url = url+"&sort[]="+newsort[0]+"&sort[]="+newsort[1];
					document.location.href = url;
				});
				$("#update th").addClass("center");
				$("#complaint th").addClass("left");
				$("#complaint th").addClass("top");
				
                $('#attachlink').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdocUP');
                });
                $('#attachlinkMAIN').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdoc');
                });
				$("#cancelme").click(function() {
					if(confirm("Are you sure you wish to cancel this complaint?")==true) {
						url = 'view_call_update.php?utype=C&callid=<?php echo $callid; ?>&src=<?php echo $cancel_back; ?>&logstatusid=<?php echo $cancel_statusid; ?>';
						document.location.href = url;
					}
				});
            });

        </script>

    </body>

</html>
