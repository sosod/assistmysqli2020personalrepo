<?php 
/***** DRAW NAVIGATION BUTTONS *****/
function drawNav($nav,$id) {
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?><script type=text/javascript>
		  $(function()
		  {
			$("#<?php echo $id ?>").buttonset();
			$("#<?php echo $id ?> input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo chr(10)."<div id=$id>";
	foreach($nav as $key => $m) {
	    $display = isset($m['display']) ? $m['display'] : " ";
	    $url = isset($m['url']) ? $m['url'] : "";
		if($key == "my") { $m['display'] = "&nbsp;&nbsp;&nbsp;".$display."&nbsp;&nbsp;&nbsp;"; }
		echo chr(10)."<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$url."\" "; if($m['active']=="checked") { echo chr(10)."checked=checked"; } echo chr(10)." />";
		echo chr(10)."<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
	}
	echo chr(10)."</div>";
}	//end drawNav($nav)
function drawNavSub($nav,$id,$pad) {
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?><script type=text/javascript>
		  $(function()
		  {
			$("#<?php echo $id ?>").buttonset();
			$("#<?php echo $id ?> input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo chr(10)."<div id=$id style=\"padding-top: 5px; padding-left: ".$pad."px;\">";
	foreach($nav as $key => $m) {
        $display = isset($m['display']) ? $m['display'] : " ";
        $url = isset($m['url']) ? $m['url'] : "";
		echo chr(10)."<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$url."\" "; if($m['active']=="checked") { echo chr(10)."checked=checked"; } echo chr(10)." />";
		echo chr(10)."<label for=\"$key\">&nbsp;&nbsp;".$display."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
	}
	echo chr(10)."</div>";
}	//end drawNav($nav)

function drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form) {
/*** variables:
		$url = $_SERVER['REQUEST_URI']
		$search   $search_label
		$page / $pages
		$start
		$limit
***/
echo "<script type=text/javascript>
function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+'s='+s+'&f='+f;
}
$(function() {
	$('.paging').css('padding','0px');
	$('.paging').css('margin','5px 0px 3px');
	$('.paging').css('width','25px');
	$('.paging').css('line-height','9pt');
});
</script>";
	echo chr(10)."<form name=page action=$url method=get>".$form;
		echo chr(10)."<table cellpadding=5 cellspacing=0 width=100% style=\"margin-bottom: 10px;\">";
			echo chr(10)."<tr>";
				echo chr(10)."<td width=45% style=\"border-right: 0px;\">";
					echo chr(10)."<input type=button id=b1 value=\"|<\" onclick=\"goNext('$url',0,'$search');\" class=paging>";
						//echo chr(10)."&nbsp;";
					echo chr(10)."<input type=button id=b2 value=\"<\" onclick=\"goNext('$url',".($start-$limit).",'$search');\" class=paging>";
				if($page<2) {
					echo chr(10)."<script type=text/javascript>";
					echo chr(10)."document.getElementById('b1').disabled = true;";
					echo chr(10)."document.getElementById('b2').disabled = true;";
					echo chr(10)."</script>";
				}
						echo chr(10)." Page $page / $pages ";
					echo chr(10)."<input type=button value=\">\" id=b3 onclick=\"goNext('$url',".($start+$limit).",'$search');\" class=paging>";
						//echo chr(10)."&nbsp;";
					echo chr(10)."<input type=button value=\">|\" id=b4 onclick=\"goNext('$url','e','$search');\" class=paging>";
				if($page==$pages) {
					echo chr(10)."<script type=text/javascript>";
					echo chr(10)."document.getElementById('b3').disabled = true;";
					echo chr(10)."document.getElementById('b4').disabled = true;";
					echo chr(10)."</script>";
				}
				echo chr(10)."</td>";
				echo chr(10)."<td width=10% style=\"border-right: 0px; border-left: 0px;\" class=center>";
					if(strlen($search)>0) { 
						echo chr(10)."<input type=button value=\"Clear Search\" onclick=\"document.location.href = '$url';\" style=\"float: center\">";
					} 
				echo chr(10)."</td>";
				echo chr(10)."<td width=45% class=right style=\"border-left: 0px;\">$search_label:&nbsp;<input type=hidden name=s value=0><input type=text width=7 name=f>&nbsp;<input type=submit value=\" Go \"></td>";
			echo chr(10)."</tr>";
		echo chr(10)."</table>";
	echo chr(10)."</form>";
}	//end drawPage()
?>