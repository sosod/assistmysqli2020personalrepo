<?php
require_once 'inc_head.php';
//error_reporting(-1);
?>
        <h1>Edit Complaint</h1>
        <?php
//$logadmintext = $_POST['logadmintext'];        
        $email = isset($_POST['email'])? $_POST['email'] : "";
        $utype = isset($_POST['utype'])? $_POST['utype'] : "";
		
		$urlback = (isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0) ? $_REQUEST['src'] : "view_call.php?i=".$_REQUEST['callid'];
//$logtkid = $_POST['logtkid'];
//$logurgencyid = $_POST['logurgencyid'];
//$logrespid = $_POST['logrespid'];
// $logstatusid = $_POST['logstatusid'];
        $callid = $_POST['callid'];
        $doneyn = "N";

        $char1[0] = "'";
        $char2[0] = "&#39";
///change on 01 Feb 2011
        $callconname = $_POST['callconname'];
        $callcontel = $_POST['callcontel'];
        $callconaddress1 = $_POST['callconaddress1'];
        $callconaddress2 = $_POST['callconaddress2'];
        $callconaddress3 = $_POST['callconaddress3'];
        $callarea = $_POST['callarea'];
        $callrespid = $_POST['callrespid'];
        $callurgencyid = $_POST['callurgencyid'];
        $callmessage = $_POST['callmessage'];
        $callstatusid = $_POST['callstatusid'];
        $callid = $_POST['callid'];
		/*$callGeolocation_lat = isset($_POST['latitude'])?$_POST['latitude']:"";
		$callGeolocation_long = isset($_POST['longitude'])?$_POST['longitude']:"";
		$firstLatitudeThenLongitude = array($callGeolocation_lat, $callGeolocation_long);
		$callGeolocation  = implode(",", $firstLatitudeThenLongitude);*/
//$callconname = str_replace("'", "&#39", $callconname);
        $callconname = $helper->code($callconname);
// echo  $callconname;die;
        $callcontel = $helper->code($callcontel);
        $callconaddress1 = $helper->code($callconaddress1);
        $callconaddress2 = $helper->code($callconaddress2);
        $callconaddress3 = $helper->code($callconaddress3);
        $callarea = $helper->code($callarea);
        $callmessage = $helper->code($callmessage);

        /* $callcontel = str_replace("'", "&#39", $callcontel);
          $callconaddress1 = str_replace("'", "&#39", $callconaddress1);
          $callconaddress2 = str_replace("'", "&#39", $callconaddress2);
          $callconaddress3 = str_replace("'", "&#39", $callconaddress3);
          $callarea = str_replace("'", "&#39", $callarea);
          $callmessage = str_replace("'", "&#39", $callmessage); */
        if ($callurgencyid == "X")
        {
            $callurgencyid = 2;
        }
///end - change on 01 Feb 2011
//$marr = explode($char1[0], $logadmintext);
//$logadmintext = implode($char2[0], $marr);
//GET TO EMAIL
        $sql = "SELECT tk.tkemail FROM assist_".$cmpcode."_timekeep tk, assist_".$cmpcode."_caps_list_respondant p WHERE tk.tkid = p.admin AND tk.tkstatus = 1 AND p.id = ".$callrespid;

        //include("inc_db_con.php");
        $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
        $to = $row['tkemail']; //respondant
        //mysql_close();
        $sql = "";
        $row = "";

        $from = "assist@ignite4u.co.za"; //user

        $logid = 0;
        switch ($utype)
        {
            case "E":
                $sql = "SELECT CONCAT(t.tkname,' ',t.tksurname) AS fullname FROM assist_".$cmpcode."_timekeep t WHERE tkid='$tkid' AND t.tkstatus = 1";
                //include("inc_db_con.php");
                $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
                $tkname = $row['fullname'];
                $sql = "SELECT c.*,p.value AS respondant,s.value AS status,u.value AS urgency FROM assist_".$cmpcode."_caps_call c,
            assist_".$cmpcode."_caps_list_respondant p,
                assist_".$cmpcode."_caps_list_status s,
                    assist_".$cmpcode."_timekeep t,
                    assist_".$cmpcode."_caps_list_urgency u
                WHERE p.admin = t.tkid  AND
                    c.callrespid=p.id AND
                    c.callstatusid=s.id AND
                    c.callurgencyid=u.id AND
                    c.callid = ".$callid;
                //include("inc_db_con.php");
                $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
                $changes = "";
                if ($row['callconname'] != $callconname)
                {
                    $changes .= "<b>Resident's Name</b> changed from <i>".$row['callconname']."</i> <b>to</b> <i>".$callconname."</i><br>";
                }
                if ($row['callcontel'] != $callcontel)
                {
                    $changes .= "<b>Resident's Telephone</b> changed from <i>".$row['callcontel']."</i> <b>to</b> <i>".$callcontel."</i><br>";
                }
                $address = array($row['callconaddress1'], $row['callconaddress2'], $row['callconaddress3']);
                if ($row['callconaddress1'] != $callconaddress1 OR
                        $row['callconaddress2'] != $callconaddress2 OR
                        $row['callconaddress3'] != $callconaddress3)
                {
                    $changes .= "<b>Resident's Address</b> changed from: <br>
                <i>".$row['callconaddress1']."<br>
                ".$row['callconaddress2']."<br>
            ".$row['callconaddress3']."</i><br> <b>to</b><br>
            <i>$callconaddress1<br>
            $callconaddress2<br>
            $callconaddress3</i><br>";
                }
                if ($row['callarea'] != $callarea)
                {
                    $changes .= "<b>Area/Ward/Town</b> changed from <i>".$row['callarea']."</i> <b>to</b> <i>".$callarea."</i><br>";
                }
                if ($row['callrespid'] != $callrespid)
                {
                    $sql2 = "SELECT value FROM assist_".$cmpcode."_caps_list_respondant WHERE id=$callurgencyid";
                    //include("inc_db_con2.php");
                    $row2 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                    $changes .= "<b>Respondant</b> changed from <i>".$row['respondant']."</i> <b>to</b> <i>".$row2['value']."</i><br>";
                }
                if ($row['callstatusid'] != $callstatusid)
                {
                    $sql2 = "SELECT value FROM assist_".$cmpcode."_caps_list_status WHERE id=$callstatusid";
                    //include("inc_db_con2.php");
                    $row2 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                    $changes .= "<b>Status</b> changed from <i>".$row['status']."</i> <b>to</b> <i>".$row2['value']."</i><br>";
                }
                if ($row['callurgencyid'] != $callurgencyid)
                {
                    $sql2 = "SELECT value FROM assist_".$cmpcode."_caps_list_urgency WHERE id=$callurgencyid";
                    //include("inc_db_con2.php");
                    $row2 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                    $changes .= "<b>Urgency</b> changed from <i>".$row['urgency']."</i> <b>to</b> <i>".$row2['value']."</i><br>";
                }
                if ($row['callmessage'] != $callmessage)
                {
                    $changes .= "<b>Complaint</b> changed from <i>".$row['callmessage']."</i> <b>to</b> <i>".$callmessage."</i><br>";
                }
                $changes = $helper->code("<br><br>".$changes);
                //SET SQL statements
                $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET
                    callconname = '".$callconname."',
                    callcontel = '".$callcontel."',
                    callconaddress1 = '".$callconaddress1."',
                    callconaddress2 = '".$callconaddress2."',
                    callconaddress3 = '".$callconaddress3."',
                    callarea = '".$callarea."',
                    callrespid = $callrespid,
                    callurgencyid = $callurgencyid,
                    callmessage = '".$callmessage."',
                    callstatusid = ".$callstatusid." WHERE callid = ".$callid;

                $logadmintext = "Complaint changed by $tkname $changes";
                $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                $sqllog .= "SET logdate = '".$today."', ";
                $sqllog .= "logtkid = '".$tkid."', ";
                $sqllog .= "logurgencyid = ".$callurgencyid.", ";
                $sqllog .= "logrespid = ".$callrespid.", ";
                $sqllog .= "logadmintext = '".$logadmintext."', ";
                $sqllog .= "logstatusid = ".$callstatusid.", ";
                $sqllog .= "logemail = '".$to."', ";
                $sqllog .= "logcallid = ".$callid.", ";
                $sqllog .= "logsubmitdate = '".$today."', ";
                $sqllog .= "logutype = '".$utype."', ";
                $sqllog .= "logdateact = '".$today."', ";
                $sqllog .= "logsubmittkid = '".$tkid."'";

                //Update database
                $sql = "";
                $sql = $sqllog;
                //include("inc_db_con.php");
                $logid = $db->db_insert($sql);
                $sql = "";
                $sql = $sqlcall;

                //include("inc_db_con.php");
				$db->db_update($sql);
                //Send email
                $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
                //include("inc_db_con.php");
                $row = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
                //mysql_close();
                $callmess = explode(chr(10), $row['callmessage']);
                $callmessage = implode("<br>", $callmess);
                $subject = "Complaint changed";
                $message = "<font face=arial>";
                $message .= "<p><small>Complaint ".$row['callref']." has been changed.<br>";
                $message .= "The original complaint was: <br>";
                $message .= "<i>".$callmessage."</i></p>";
                $message .= "</small></font>";
                $header = "From: no-reply@ignite4u.co.za\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
                /*mail($to, $subject, $message, $header);
                $header = "";
                $message = "";*/
                $mail = new ASSIST_EMAIL($to,$subject,$message,"HTML");
                $mail->sendEmail();

                $doneyn = "C";
                break;
        }

        $udfFields = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
        //include("inc_db_con.php");
        /*while ($row = mysql_fetch_array($rs))
        {*/
		$rows = $db->mysql_fetch_all($sql);
		foreach($rows as $row) {
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];
            //include("inc_db_con2.php");
            $udf = $db->db_get_num_rows($sql2);
            $row2 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
            //mysql_close($con2);
            $udfFields[$row['udfivalue']] = '';
            switch ($row['udfilist'])
            {
                case "Y":
                    if ($helper->checkIntRef($row2['udfvalue']))
                    {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue
                        WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                        //include("inc_db_con2.php");
                        if ($db->db_get_num_rows($sql2) > 0)
                        {
                            $row3 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                        }
                        //mysql_close($con2);
                    }
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                    //include("inc_db_con2.php");
                    /*while ($row2 = mysql_fetch_array($rs2))
                    {*/
					$rows = $db->mysql_fetch_all($sql2);
					$udfvid = isset($row3['udfvid'])? $row3['udfvid'] : "";
					foreach($rows as $row2) {
                        if ($udfvid == $row2['udfvid'])
                            $udfFields[$row['udfivalue']] = $row2['udfvvalue'];
                    }
                    //mysql_close($con2);
                    break;
                case "T":
                case "M":
                case "D":
                    $udfFields[$row['udfivalue']] = $row2['udfvalue'];
                    break;
                default:
                    break;
            }
        }
        $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";

        //include("inc_db_con.php");
        $udf = $db->db_get_num_rows($sql);
        if ($udf > 0)
        {
            $u = 0;
            /*while ($row = mysql_fetch_array($rs))
            {*/
			$rows = $db->mysql_fetch_all($sql);
			foreach($rows as $row) {
                $udfindex[$u] = $row['udfiid'];
                $u++;
            }
        }

        //mysql_close();

        //GET UDF DATA
        if ($udf > 0)
        {
            foreach ($udfindex as $udfi)
            {
                $udfval[$udfi] = $helper->code($_POST[$udfi]); //str_replace("'","&339",$_POST[$udfi]);
            }
        }

        //$udfidArr = array();

        foreach ($_POST as $key => $value)
        {
            if (is_int($key))
            {
                $sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfindex=$key AND udfnum=$callid AND udfref='".$modref."'";
                
                //include("inc_db_con.php");
                if ($db->db_get_num_rows($sql) > 0)
                {
                   /* if ($value == 'X' OR empty($value) OR $value == null )
                    {
                        $sql = "DELETE IGNORE FROM assist_".$cmpcode."_udf WHERE udfindex=$key AND udfnum=$callid AND udfref='".$modref."'";
                        include("inc_db_con.php");                        
                    }
                    else
                    {*/
                        $sql = "UPDATE assist_".$cmpcode."_udf SET";
                        $sql.= " udfvalue='".$_POST[$key]."' WHERE udfindex=$key AND udfnum=$callid AND udfref='".$modref."'";
                        //include("inc_db_con.php");
						$db->db_update($sql);
                  //  }
                }
                else
                {
                   /* if ($value == 'X' OR empty($value) OR $value == null )
                    {
                        $sql = "DELETE IGNORE FROM assist_".$cmpcode."_udf WHERE udfindex=$key AND udfnum=$callid AND udfref='".$modref."'";
                    }
                    else{*/
                        $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=$key,udfvalue='".$_POST[$key]."',udfnum=$callid,udfref='".$modref."'";
                  //  }
                    

                    //include("inc_db_con.php");
					$db->db_insert($sql);
                }
            }
        }

        $afterUdfFields = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
        //include("inc_db_con.php");
        /*while ($row = mysql_fetch_array($rs))
        {*/
		$rows = $db->mysql_fetch_all($sql);
		foreach($rows as $row) {
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];
            //include("inc_db_con2.php");
            $udf = $db->db_get_num_rows($sql2);
            $row2 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
            //mysql_close($con2);
            $afterUdfFields[$row['udfivalue']] = '';
            switch ($row['udfilist'])
            {
                case "Y":
                    if ($helper->checkIntRef($row2['udfvalue']))
                    {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                        //include("inc_db_con2.php");
                        if ($helper->db_get_num_rows($sql2) > 0)
                        {
                            $row3 = $db->mysql_fetch_one($sql2);//mysql_fetch_array($rs2);
                            //echo($row3['udfvvalue']);
                        }
                        //mysql_close($con2);
                    }
                    //echo("<select name=".($row2['udfid'] == "" ? "-1" : $row2['udfid'])."><option value=X>---SELECT---</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                    //include("inc_db_con2.php");
                    /*while ($row2 = mysql_fetch_array($rs2))
					{*/
					$rows = $db->mysql_fetch_all($sql2);
					foreach($rows as $row2) {

                        //echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                        if ($row3['udfvid'] == $row2['udfvid'])
                            $afterUdfFields[$row['udfivalue']] = $row2['udfvvalue'];
                    }
                    //mysql_close($con2);
                    //echo("</select>");
                    break;
                case "T":
                case "D":
                case "M":
                    $afterUdfFields[$row['udfivalue']] = $row2['udfvalue'];
                    break;
                default:
                    break;
            }
        }
        $logupdate = "";
        foreach ($udfFields as $key => $value)
        {
            if ($value != $afterUdfFields[$key])
                $logupdate .= "<b>".$key."</b> changed ".($value != null ? "from " : "" )."<i>".$value."</i> <b>to</b> <i>".$afterUdfFields[$key]."</i><br />";
        }
        $break = $helper->code("<br/>");
        $sql = "UPDATE ".$dbref."_log SET logadmintext=CONCAT_WS('$break',logadmintext,'$logupdate') WHERE logid=$logid";
        //echo $sql;die;
        //include("inc_db_con.php");
		$db->db_update($sql);

//uploads
        echo '<pre>';
        $original_filename = "";
        $system_filename = "";
        $attachments = "";
//print_r($_FILES);die;
        if (isset($_FILES))
        {
            $folder = "../files/$cmpcode/CAPS";
            /* if(!is_dir($folder)){
              mkdir($folder, 0777, true);
              } */
            $helper->checkFolder("CAPS");
			$f = 0;
            foreach ($_FILES['attachments']['tmp_name'] as $key => $tmp_name)
            {
                //print_r($tmp_name);die;
                if ($_FILES['attachments']['error'][$key] == 0)
                {
                    $original_filename = $_FILES['attachments']['name'][$key];
                    $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                    $parts = explode(".", $original_filename);
                    $file = $parts[0];
                    $system_filename = $logid."_".$f."_".date("YmdHi").".$ext";
					$f++;
                    $full_path = $folder."/".$system_filename;
                    move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                    $sql = "INSERT INTO assist_".$cmpcode."_caps_attachments (callid,logid,original_filename,system_filename) VALUES ($callid,0,'$original_filename','$system_filename')";
                    //include("inc_db_con.php");
					$db->db_insert($sql);
                    $filename = "../files/$cmpcode/CAPS/".$system_filename;
                    $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
                }
            }
        }
        $logupdate = "";
        /*if ($attachments != '')
        {
            $logupdate .= "<br />Complaint Attachment(s):<br /> $attachments";
            $logupdate = addslashes(htmlentities($logupdate));
            $sql = "UPDATE assist_".$cmpcode."_caps_log SET logadmintext=CONCAT_WS('\n',logadmintext,'$logupdate') WHERE logid=$logid";
            //include("inc_db_con.php");
        }*/
        echo "<script type='text/javascript'>document.location.href='".$urlback."&r[]=ok&r[]=Complaint successfully edited.';</script>";
        ?>

    </body>
</html>
