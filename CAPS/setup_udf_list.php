<?php
//include("inc_ignite.php");
require_once 'inc_head.php';
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
if(isset($result[1])) { $result[1] = stripslashes($result[1]); }
?>
<script language=JavaScript>
</script>
<h1>Setup >> User Defined Field >> Edit List</b></h1>
<?php
$id = $_REQUEST['i'];
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiid = ".$id;
//include("inc_db_con.php");
    $uindex = $db->mysql_fetch_one($sql);//mysql_fetch_array($rs);
//mysql_close();
?>
<h2>UDF: <u><?php echo($uindex['udfivalue']); ?></u></h2>
<?php $helper->displayResult($result); ?>
<form id=frm_add method=post action=setup_udf_list_process.php>
<input type=hidden name=act value=ADD />
<table id=list><thead>
    <tr>
        <th>Ref</th>
        <th>List Item</th>
        <th></th>
    </tr>
	</thead><tbody>
    <tr>
        <th>&nbsp;</th>
        <td ><input type=text size=50 maxlength=50 name=udfvvalue id=uv><input type=hidden name=udfvindex value="<?php echo($id); ?>" /></td>
        <td class=center><input type=submit value=Add class=isubmit id=0 /></td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$id." AND udfvyn = 'Y' ORDER BY udfvvalue";
//include("inc_db_con.php");
    if($db->db_get_num_rows($sql)==0)
    {
        ?>
        <tr>
			<th></th>
            <td colspan=6>No list items to display</td>
        </tr>
        <?php
    }
    else
    {
        /*while($row = mysql_fetch_array($rs))
        {*/
		$rows = $db->mysql_fetch_all($sql);
		foreach($rows as $row) {
            $valjs = $helper->code($row['udfvvalue']);
            $valjs = str_replace("'","\'",$valjs);
            $valjs = str_replace("\"","&#034;",$valjs);

			echo "<tr>
					<th>".$row['udfvid']."</th>
					<td id=td_".$row['udfvid'].">".$row['udfvvalue']."</td>
					<td class=center><input type=button value=Edit class=edit id=".$row['udfvid']."></td>
				</tr>";
        }
    }
//mysql_close();
?></tbody>
</table>
</form>
<?php $helper->displayGoBack("setup_udf.php","Go Back"); ?>
<div id=dlg_edit title="Edit Item">
<form id=frm_edit method=post action=setup_udf_list_process.php>
<input type=hidden name=act value=EDIT id=act />
<input type=hidden name=udfvindex value=<?php echo $id; ?> />
	<table id=tbl_edit>
		<tr>
			<th>Reference:</th>
			<td><input type=hidden name=id id=txt_id><label id=lbl_id></label></td>
		</tr>
		<tr>
			<th>Item:</th>
			<td><input type=text size=50 maxlength=50 name=udfvvalue id=txt_val /></td>
		</tr>
		<tr>
			<th></th>
			<td><input type=button value="Save Changes" class=isubmit /> <input type=reset></td>
		</tr>
	</table>
	<p><span class=float><input type=button value=Delete class=idelete /></span></p>
</form>
</div>
<script type=text/javascript>
	$(function() {
		$("#list tbody th").addClass("center");
		$("#uv").focus();
		$("#dlg_edit").dialog({
			autoOpen: false,
			modal: true,
			width: 450,
			height: 200,
		});
		$("#list input:button").click(function() {
			var i = $(this).attr("id"); //alert(i+" :: "+$(this).attr("class"));
			switch($(this).attr("class")) {
			case "edit":
				var v = $("#td_"+i).attr("innerText"); 
				$("#dlg_edit #txt_val").val(v);
				$("#dlg_edit #txt_id").val(i);
				$("#dlg_edit #lbl_id").attr("innerText",i);
				$("#dlg_edit").dialog("open");
				break;
			case "isubmit":
				var v = $("#uv").val();
				if(v.length==0) {
					$("#uv").addClass("required");
					alert("Please enter the list item you wish to add before clicking the Add button.");
				} else {
					$("#frm_add").submit();
				}
				break;
			}
		});
		$("#frm_edit input:button").click(function() {
			switch($(this).attr("class")) {
			case "isubmit":
				var v = $("#frm_edit #txt_val").val();
				if(v.length==0) {
					$("#frm_edit #txt_val").addClass("required");
					alert("Please enter the list item before clicking the Save Changes button.");
				} else {
					$("#frm_edit #act").val("EDIT");
					$("#frm_edit").submit();
				}
				break;
			case "idelete":
				if(confirm("Are you sure you wish to delete this list item?")==true) {
					$("#frm_edit #act").val("DELETE");
					$("#frm_edit").submit();
				}
				break;
			}
		});
	});
</script>
</body>

</html>
