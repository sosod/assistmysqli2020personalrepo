<?php
    require_once "inc_head.php";

	$sqls['my'] = "SELECT count(id) as my FROM ".$dbref."_list_respondant WHERE yn = 'Y' AND admin = '$tkid'";
	if($calltkid_close=="Y") {
		$sqls['cil'] = "SELECT count(callid) as cil FROM ".$dbref."_call WHERE calltkid = '$tkid'";
	}
	$sqls['mydiv'] = "SELECT count(id) as mydiv FROM ".$dbref."_list_respondant WHERE yn = 'Y' AND hodiv = '$tkid'";
	$sqls['dept'] = "SELECT count(id) as dept FROM ".$dbref."_list_respondant WHERE yn = 'Y' AND hodept = '$tkid'";
	
	$count = array();
	foreach($sqls as $key => $sql) {
		$row = $db->mysql_fetch_one($sql);
		//include("inc_db_con.php");
		//$row = mysql_fetch_assoc($rs);
		//mysql_close($con);
		$count[$key] = $row[$key];
	}
	
	//$count = array('my'=>10,'div'=>10,'dept'=>10);
	
	$nav = array();
	$available_sec = "";
    $use_my_as_default = "";
	foreach($count as $key => $c) {
		if($helper->checkIntRef($c)) {
			switch($key) {
				case "my":
					$nav['my'] = array('id'=>"my",'display'=>"My Complaints",'active'=>"",'url'=>"update.php?sec=my&st=ip");
					break;
				case "mydiv":
					$nav['div'] = array('id'=>"div",'display'=>"Complaints in My Division",'active'=>"",'url'=>"update.php?sec=div&st=ip");
					break;
				case "dept":
					$nav['dept'] = array('id'=>"dept",'display'=>"Complaints in My Department",'active'=>"",'url'=>"update.php?sec=dept&st=ip");
					break;
				case "cil":
					$nav['cil'] = array('id'=>"cil",'display'=>"Complaints I logged",'active'=>"",'url'=>"update.php?sec=cil&st=ip");
					break;
			}
		}
        /**
         * 1.
         * IF THERE'S A $KEY (sec) THAT RETURNS A VALUE OF 1, WE'LL USE THAT KEY AS DEFAULT FOR RADIO BUTTON SELECTION
         * Sondelani Dumalisile - 21 March 2021
         * comment: This was added during standardisation after noticing that an empty button is selected that doesn't have an id in the $nav array.
         *          this resulted in an empty page being selected...
         */


		if ($c > 0){
            if($key === "my"){
                $use_my_as_default = $key; //if "my"(my complaints) has data then it will be the default selected button
            }
		    $available_sec = $key;
		}

        //$helper->arrPrint($use_my_as_default);
	}

        /**
        * 2.
        * ...Now if the $_REQUEST['sec'] is not set, we'll use the key ($available_sec) that returned a value of 1. if $use_my_as_default has a value, it will be used as default.
        *  Sondelani Dumalisile - 21 March 2021
        */
    if (!isset($_REQUEST['sec']) && strlen($available_sec)>0){
        if(isset($use_my_as_default) && $use_my_as_default === "my"){
            $_REQUEST['sec'] = $use_my_as_default;
        }else{
            $_REQUEST['sec'] = $available_sec;
        }
    }

	/*** SET PADDING FOR SECONDARY NAVIGATION BUTTONS ***/
		//C I LOGGED
		if(isset($nav['my'])) {
			$pad_cil = 120;
		} else {
			$pad_cil = 0;
		}
		//DIVISION
		if(count($nav)==1) {
			$pad_div = 0;
		} else {
			$is = "";
			$is.= (isset($nav['my'])) ? "Y" : "N";
			$is.= (isset($nav['cil'])) ? "Y" : "N";
			switch($is) {
				case "YY": $pad_div = 305; break;
				case "YN": $pad_div = 137; break;
				case "NY": $pad_div = 150; break;
				default: $pad_div = 0; break;
			}
		}
		//DEPARTMENT
		if(count($nav)==1) {
			$pad_dept = 0;
		} else {
			$is = "";
			$is.= (isset($nav['my'])) ? "Y" : "N";
			$is.= (isset($nav['cil'])) ? "Y" : "N";
			$is.= (isset($nav['div'])) ? "Y" : "N";
			switch($is) {
				case "YYY": $pad_dept = 521; break;
				case "YYN": $pad_dept = 318; break;
				case "YNN": $pad_dept = 150; break;
				case "YNY": $pad_dept = 354; break;
				case "NYY": $pad_dept = 366; break;
				case "NYN": $pad_dept = 164; break;
				case "NNY": $pad_dept = 200; break;
				default: $pad_dept = 0; break;
			}
/*			if(isset($nav['cil']) && isset($nav['div'])) {
				$pad_dept = 521;
			} elseif(isset($nav['cil'])) {
				$pad_dept = 318;
			} else {
				$pad_dept = 354;
			}*/
		}

	
	foreach($nav as $key => $n) {
		$nav[$key]['sub'] = array(
			'ip'=>array('id'=>"ip",'display'=>"Current Calls",'active'=>"",'url'=>"update.php?sec=".$key."&st=ip"),
			'cl'=>array('id'=>"cl",'display'=>"Closed Calls",'active'=>"",'url'=>"update.php?sec=".$key."&st=cl")
		);
	}
	
	$title = "Update ";

	if(isset($_REQUEST['sec']) && strlen($_REQUEST['sec'])>0) {
		$sec = $_REQUEST['sec'];
		switch($sec) { 
			case "cil": $pad = $pad_cil; $sqlfilter = "c.calltkid = '".$tkid."'"; break; 	
			case "my": $pad = 0; $sqlfilter = "rp.admin = '".$tkid."'"; break; 
			case "div": $pad = $pad_div; $sqlfilter = "rp.hodiv = '".$tkid."'"; break; 	//all = 305	//no dept = 290	// no cil = 122	// nothing = 0
			case "dept": $pad = $pad_dept; $sqlfilter = "rp.hodept = '".$tkid."'"; break; 	//all = 517 //no div = 314 // no cil = 148 //nothing = 0
		}
	} else {
		$sec = "my";
		$pad = 0;
		$sqlfilter = "rp.admin = '".$tkid."'";
	}

	$nav[$sec]['active'] = "checked";
	$display = isset($nav[$sec]['display']) ? $nav[$sec]['display'] :"";
	$title.= $display." >> ";
	
	if(isset($_REQUEST['st']) && strlen($_REQUEST['st'])>0) {
		$st = $_REQUEST['st'];
	} else {
		$st = "ip";
	}
	if($st == "cl") {
		$sqlst2 = "c.callstatusid <> 4 AND c.callstatusid <> 5";
		$sqlst = "c.callstatusid = 4";
		$title.= "Closed calls";
		$button = "View";
	} else {
		$sqlst2 = "c.callstatusid = 4";
		$sqlst = "c.callstatusid <> 4 AND c.callstatusid <> 5";
		$title.= "Current calls";
		$button = "Update";
	}
	$nav[$sec]['sub'][$st]['active'] = "checked";


	//PAGING
	if(isset($_REQUEST['s'])) {
		$start = $_REQUEST['s'];
	} else { 
		$start = 0;
	}
	$sql_search = "";
	if(isset($_REQUEST['f'])) {
		$search = $helper->decode(trim($_REQUEST['f']));
		if(isset($search) && strlen($search)>0) {
			$find = explode(" ",$search);
			if(count($find)>0) {
				$sql_search= "AND (";
				$f = 0;
				foreach($find as $needle)
				{
					if($f>0) { $sql_search.=" OR "; }
					$sql_search.= "c.callref LIKE '%".$needle."%'";
					$f++;
				}
				$sql_search.= ")";
			}
		}
	} else {
		$search = "";
	}
if(!isset($start) || (!$helper->checkIntRef($start) && $start != "e")) { $start = 0; }
$limit = 30;
$sqls = array();
$sqls[0] = "SELECT count(callid) as ct";	
$sqls[1] = " FROM assist_".$cmpcode."_caps_call c ";
$sqls[1].= " INNER JOIN assist_".$cmpcode."_caps_list_status st ON c.callstatusid = st.id ";
$sqls[1].= " INNER JOIN assist_".$cmpcode."_caps_list_respondant rp ON c.callrespid = rp.id";
$sqls[1].= " INNER JOIN assist_".$cmpcode."_timekeep tk ON rp.admin = tk.tkid";
$sqls[1].= " WHERE ".$sqlst." ".$sql_search." ";
$sqls[1].= isset($sqlfilter) ?" AND ".$sqlfilter." " : "";
$sql = implode(" ",$sqls);
	//include("inc_db_con.php");
		//$mr = mysql_fetch_array($rs);
	//mysql_close($con);
	$mr = $db->mysql_fetch_one($sql);
	$max = $mr['ct'];
	$pages = ceil($max/$limit);
	if(!is_numeric($start)) {
		$start = $max - ($max - ($limit*($pages-1)));
	}
	$page = ($start / $limit) + 1;
	
?>
<script language=JavaScript>
function Validate() {
    return false;
}
function viewCall(c,b) {
    document.location.href = 'update_call.php?i='+c+'&b='+escape(b);
}
</script>
<?php
	drawNav($nav,$sec);
	drawNavSub($nav[$sec]['sub'],$st,$pad);
echo "<h1>$title</h1>";
$helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
echo "<table class=\"noborder minw\"><tr><td class=noborder >";
drawPaging('update.php?sec='.$sec.'&st='.$st.'&',$search,$start,$limit,$page,$pages,'Search Ref','<input type=hidden name=st value='.$st.'><input type=hidden name=sec value='.$sec.'>');

if($start<0) { $start = 0; }
$sqls[0] = "SELECT c.*, st.value status, rp.value resp, CONCAT_WS(' ',tkname,tksurname) as tkn ";
$sqls[2] = " ORDER BY c.callid DESC LIMIT $start, $limit";
$sql = implode(" ",$sqls);
$rows = $db->mysql_fetch_all($sql);
//include("inc_db_con.php");
//$n = mysql_num_rows($rs);
$n = count($rows);
if($n > 0)
{
?>
<table class=minw>
    <tr>
        <th>Ref</th>
        <th>Date Logged</th>
        <th>Assigned To</th>
        <th>Resident</th>
        <th>Area</th>
        <th>Status</th>
        <th>&nbsp;</th>
    </tr>
    <?php
    //while($row = mysql_fetch_array($rs))
    foreach($rows as $row)
    {
		$callid = $row['callid'];
		$style = "";
		$s = $row['callstatusid'];
		if(in_array($s,$new_statusid)) {
			$style = "ui-widget ui-state-error\" style=\"font-size: 8pt;";
		} elseif(in_array($s,$closed_statusid)) {
			$style = "ui-widget ui-state-ok\" style=\"font-size: 8pt;";
		} else {
			$style = "ui-widget ui-state-info\" style=\"font-size: 8pt; color: ".$result_settings['IP']['color'];
		}
		echo "<tr>";
			echo "<td class=center>".$row['callref']."</td>";
			echo "<td class=center>".date('d-M-Y',$row['calldate'])."</td>";
			echo "<td>".$row['resp']." (".$row['tkn'].")</td>";
			echo "<td>".$row['callconname']."</td>";
			echo "<td>".$row['callarea']."</td>";
			echo "<td class=center><div class=\"$style\">&nbsp;".$row['status']."&nbsp;</div></td>";
			echo "<td class=center><input type=button value=$button onclick=\"viewCall($callid,'update.php?sec=".$sec."&st=".$st."');\"></td>";
		echo "</tr>";
    }
	echo "</table>";
}
else
{
	echo("<P>There are no calls to view.</p>");
}
?>
</td></tr></table>
<script type=text/javascript>
$(function() {
	$("th").addClass("center");
});
</script>
</body>

</html>
