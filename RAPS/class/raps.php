<?php


class RAPS extends RAPS_HELPER {

	const REFTAG = "RES";
	

	private $setup_log_table = "raps_setup";
	protected $attach_java_already_echoed = false;
	
	private $module_administrator = "0000";
	private $department_source = "MODULE";
	private $department_table = "list_dept";
	private $my_access = array(
		0			=>	false,
		'receive_new'	=> false,
		'receive_action'=> false,
		'edit'		=>	false,
		'update'	=>	false,
		'add'		=>	false,
		'email_copy'=>	false,
		'setup'		=>	false,
	);
	
	private $log_sections = array(
		'setup'=>array(
			'module_admin'=>"S_ADMN",
			'dept_src'=>"S_DEPT",
			'portfolio'=>"S_PORT",
			'user_access'=>"S_USER",
		),
		'res'=>array(
			'new'	=> "C",
			'update'=> "U",
			'edit'	=> "E",
			'delete'=> "D",
		),
	);
	
	private $headings = array(
		'head'=>array(
			'id'=>"Ref",
			'ref'=>"Resolution Reference",
			'tkid'=>"Responsible Person",
			'actionowner'=>"Action Owner",
			'adduser'=>"Assigned By",
			'adddate'=>"Assigned On",
			'portfolioid'=>"Portfolio",
			'urgencyid'=>"Priority",
			'statusid'=>"Status",
			'state'=>"Progress",
			'meetingdate'=>"Council Meeting Date",
			'deadline'=>"Deadline",
			'action'=>"Resolution Instructions",
			'notes'=>"Additional Notes",
			'attachment'=>"Attachments",
		),
		'types'=>array(
			'id'=>"REF",
			'ref'=>"SMLTEXT",
			'tkid'=>"LIST",
			'actionowner'=>"LIST",
			'adduser'=>"LIST",
			'adddate'=>"DATE",
			'portfolioid'=>"LIST",
			'urgencyid'=>"LIST",
			'statusid'=>"LIST",
			'state'=>"PERC",
			'meetingdate'=>"DATE",
			'deadline'=>"DATE",
			'action'=>"TEXT",
			'notes'=>"TEXT",
			'attachment'=>"ATTACH",
		),
		'list_view'=>array("id","ref","tkid","actionowner","portfolioid","statusid","deadline","action"),
		
	);
	


	public function __construct() {
		$_REQUEST['jquery_version'] = "1.10.0";
		parent::__construct();
		$this->setModuleAdministrator();
		$this->setDepartmentSource();
		
		$this->ra = new RAPS_ACCESS($this->my_access);
		$this->ra->setModuleAdmin($this->module_administrator);
		$this->ra->setMyUserID($this->getUserID());
		
		$this->setUserAccess();
	}




	
	/***********************
			STARTUP FUNCTIONS 
	***********************/
	function setModuleAdministrator() {
		$cmpcode = $this->cc;
		$mr = $this->mod_ref;
		$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '$mr' AND refid = 0";
		//$rs = $this->db_query($sql);
			$c = $this->db_get_num_rows($sql);
			$row = $this->mysql_fetch_one($sql);
			$setupadmn = $row['value'];
		//unset($rs);
		if($c == 0 || strlen($setupadmn) == 0) {
			$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '$mr', refid = 0, value = '0000', comment = 'Setup Administrator', field = ''";
			$this->db_insert($sql);
			$setupadmn = "0000";
				$trans = "Created module administrator record";
				$this->logMe($this->log_sections['setup']['module_admin'],"C",0,"value",$trans,"",$setupadmn,$sql,$this->setup_log_table);
		}
		$this->module_administrator = $setupadmn;
		if($this->module_administrator==$this->getUserId()) {
			$this->my_access['setup'] = true;
		}
	}
	
	function setDepartmentSource() {
		$cmpcode = $this->cc;
		$mr = $this->mod_ref;
		$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '$mr' AND refid = 1";
		$rs = $this->db_query($sql);
			$c = $this->db_get_num_rows($sql);//mysql_num_rows($rs);
			$row = $this->mysql_fetch_one($sql);//mysql_fetch_array($rs);
			$deptsrc = strtoupper($row['value']);
		if($c==0 || strlen($deptsrc)==0) {
			$deptsrc = "MODULE";
			$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '$mr', refid = 1, value = '".$deptsrc."', comment = 'Source for Department List', field = ''";
			$this->db_insert($sql);
				$trans = "Created department source record";
				$this->logMe($this->log_sections['setup']['dept_src'],"C",0,"value",$trans,"",$deptsrc,$sql,$this->setup_log_table);
		}
		$this->department_source = $deptsrc;
		$this->checkDepartmentTable();
	}
	
	function checkDepartmentTable() {
		if($this->department_source=="MODULE") {
			$this->department_table = "list_dept";
		} elseif($this->department_source=="ASSIST") {
			$this->department_table = "list_dir";
		} else {
			$this->department_table = "sdbip_dept";
		}
	}
	
	function setUserAccess($tkid="") {
		$data = array();
		$cmpcode = $this->cc;
		$tkid = strlen($tkid)==0 ? $this->user_id : $tkid;
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_users WHERE yn = 'Y' AND tkid = '".$tkid."'";
		$row = $this->mysql_fetch_one($sql);
		if(count($row)==0) {
			$data[0] = false;
		} else {
			$data[0] = true;
			$data['update']			= $this->ra->canUpdateAll($row['u']);
			$data['receive_new']	= $this->ra->canReceiveNew($row['r']);
			$data['receive_action']	= $this->ra->canReceiveAction($row['r']);
			$data['edit']			= $this->ra->canEdit($row['e']);
			$data['add']			= $this->ra->canAdd($row['a']);
			$data['email_copy'] 	= $this->ra->canGetCopyEmail($row['m']);
			$data['setup']		 	= $this->ra->canSetup($row['s']);
		}
		$this->ra->setMyAccess($data);
		return $data;
	}
	
	
	
	
	/***********************
			SETUP FUNCTIONS 
	***********************/
	function updateModuleAdmin($new) {
		$cmpcode = $this->cc;
		$mr = $this->mod_ref;
		$usql = "UPDATE assist_".$cmpcode."_setup SET value = '".$new."' WHERE ref = '$mr' AND refid = 0";
		$mar = $this->db_update($usql);
		if($mar==0 || $new == $this->module_administrator) {
			$result = array("info","No change was made.");
		} else {
			$old = $this->module_administrator;
			$this->module_administrator = $new;
			if($new=="0000") {
				$new_name = "Assist Administrator";
			} else {
				$new_name = $this->assist->getTKDetails($new,"","tkn");
			}
			if($old=="0000") {
				$old_name = "Assist Administrator";
			} else {
				$old_name = $this->assist->getTKDetails($old,"","tkn");
			}
			$trans = "Changed Module Administrator to '".$new_name."' from '".$old_name."'";
			$this->logMe($this->log_sections['setup']['module_admin'],"U",0,"value",$trans,$old,$new,$usql,$this->setup_log_table);
			$result = array("ok","Module administrator updated successfully.");
		}
		return $result;
	}
	
	
	
	
	
	/***********************
			GET FUNCTIONS 
	***********************/
	
	//USER ACCESS
	
	function getAllModuleUsers() {
		$data = array();
		$cmpcode = $this->cc;
		$mr = $this->mod_ref;
		$sql = "SELECT t.tkid, CONCAT(t.tkname,' ',t.tksurname) as tkn 
				FROM assist_".$cmpcode."_timekeep t
				INNER JOIN assist_".$cmpcode."_menu_modules_users m 
				ON m.usrtkid = t.tkid AND m.usrmodref = '$mr'
				WHERE t.tkstatus = 1 AND t.tkid <> '0000' 
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"tkid","tkn");
		return $data;
	}
	
	function getAllSetupModuleUsers() {
		$data = array();
		$cc = $this->cc;
		$mr = $this->mod_ref;
		$sql = "SELECT u.*, CONCAT(t.tkname,' ',t.tksurname) as tkn 
				FROM ".$this->getDBRef()."_list_users u
				INNER JOIN assist_".$cc."_menu_modules_users m
				ON m.usrtkid = u.tkid AND m.usrmodref = '$mr'
				INNER JOIN assist_".$cc."_timekeep t 
				ON t.tkid = u.tkid AND t.tkstatus = 1
				WHERE u.yn = 'Y' 
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_all_by_id($sql,"tkid");
		return $data;
	}
	
	function getAllNewModuleUsers() {
		$data = array();
		$all = $this->getAllModuleUsers();
		$active = $this->getAllSetupModuleUsers();
		foreach($all as $key => $i) {
			if(!isset($active[$key])) {
				$data[$key] = $i;
			}
		}
		return $data;
	}
	
	function getUsedAddUsers() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_res r
				ON r.resadduser = t.tkid AND r.resstatusid <> 'CN'
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}


	function getUsersToReceiveNew() {
		$data = array();
		$deptlist = $this->getActiveDepartments();
		$sql = "SELECT u.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name, u.deptid 
				FROM ".$this->getDBRef()."_list_users u
				INNER JOIN assist_".$this->cc."_menu_modules_users mmu
				ON mmu.usrtkid = u.tkid AND mmu.usrmodref = '".$this->mod_ref."'
				INNER JOIN assist_".$this->cc."_timekeep t
				ON t.tkid = mmu.usrtkid AND t.tkstatus = 1
				WHERE u.yn = 'Y'
				AND r IN ('Y','C')
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		foreach($data as $key=>$d) {
			$disp = $d['name'];
			if(isset($deptlist[$d['deptid']])) {
				$disp.=" (".$deptlist[$d['deptid']].")";
			}
			$data[$key]['display'] = $disp;
		}
		return $data;
	}
	function getUsedResponsiblePersons() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_res r
				ON r.restkid = t.tkid AND r.resstatusid <> 'CN'
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}

	function getActionOwners() {
		$data = array();
		$deptlist = $this->getActiveDepartments();
		$sql = "SELECT u.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name, u.deptid 
				FROM ".$this->getDBRef()."_list_users u
				INNER JOIN assist_".$this->cc."_menu_modules_users mmu
				ON mmu.usrtkid = u.tkid AND mmu.usrmodref = '".$this->mod_ref."'
				INNER JOIN assist_".$this->cc."_timekeep t
				ON t.tkid = mmu.usrtkid AND t.tkstatus = 1
				WHERE u.yn = 'Y'
				AND r IN ('Y','A')
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		foreach($data as $key=>$d) {
			$disp = $d['name'];
			if(isset($deptlist[$d['deptid']])) {
				$disp.=" (".$deptlist[$d['deptid']].")";
			}
			$data[$key]['display'] = $disp;
		}
		return $data;
	}

	function getUsedActionOwners() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_res r
				ON r.resactionowner = t.tkid AND r.resstatusid <> 'CN'
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	
	
	//DEPARTMENTS
	function getDepartmentSource() {
		return $this->department_source;
	}
	function getActiveDepartments() {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDepartmentDBRef()." d WHERE yn = 'Y' ORDER BY ".$this->getDepartmentSortField("d");
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	
	function getAllDepartments() {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDepartmentDBRef()." d ORDER BY ".$this->getDepartmentSortField("d");
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	function getUsedDepartments() {
		$data = array();
		$sql = "SELECT DISTINCT D.id as id, D.value as name 
				FROM ".$this->getDepartmentDBRef()." D 
				INNER JOIN ".$this->getDBRef()."_list_portfolio P
				ON P.deptid = D.id
				INNER JOIN ".$this->getDBRef()."_res R
				ON R.resportfolioid = P.id AND R.resstatusid <> 'CN'
				ORDER BY ".$this->getDepartmentSortField("D");
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	
	function getADepartment($i) {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDepartmentDBRef()." d WHERE id = $i ORDER BY ".$this->getDepartmentSortField("d");
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	function getDepartmentSortField($d) {
		switch($this->department_table) {
			case "sdbip_dept":	return $d.".sort"; break;
			case "list_dir": 	return $d.".value"; break;
			default: 			return $d.".sort, ".$d.".value"; break;
		}
	}
	function getDepartmentDBRef() {
		switch($this->department_table) {
			case "sdbip_dept": 	return "assist_".$this->cc."_sdbip_dept"; break;
			case "list_dir": 	return "assist_".$this->cc."_list_dept"; break;
			case "list_dept":
			default: 			return "".$this->getDBRef()."_list_dept"; break;
		}
	}
	
	//PORTFOLIOS
	
	function getActivePortfolios() {
		$deptlist = $this->getActiveDepartments();
		if(count($deptlist)>0) {
			$sql = "SELECT p.* FROM ".$this->getDBRef()."_list_portfolio p
					INNER JOIN ".$this->getDepartmentDBRef()." d
					ON p.deptid = d.id
					WHERE p.yn = 'Y' 
					AND p.deptid IN (".implode(",",array_keys($deptlist)).") 
					ORDER BY ".$this->getDepartmentSortField("d").", p.value";
			$portfolios = $this->mysql_fetch_all_by_id($sql,"id");
			foreach($portfolios as $key => $p) {
				if(!isset($deptlist[$p['deptid']])) {
					unset($portfolios[$key]);
				} else {
					$portfolios[$key]['display'] = $this->decode($deptlist[$p['deptid']]." - ".$p['value']);
					$portfolios[$key]['status'] = $p['yn']=="Y" ? true : false;
				}
			}
		} else {
			$portfolios = array();
		}
		return $portfolios;
	}
	
	function getAllPortfolios() {
		$deptlist = $this->getAllDepartments();
		$sql = "SELECT p.* FROM ".$this->getDBRef()."_list_portfolio p
				INNER JOIN ".$this->getDepartmentDBRef()." d
				ON p.deptid = d.id
				ORDER BY ".$this->getDepartmentSortField("d").", p.value";
		$portfolios = $this->mysql_fetch_all_by_id($sql,"id");
		foreach($portfolios as $key => $p) {
			if(!isset($deptlist[$p['deptid']])) {
				unset($portfolios[$key]);
			} else {
				$portfolios[$key]['display'] = $this->decode($deptlist[$p['deptid']]." - ".$p['value']);
				$portfolios[$key]['status'] = $p['yn']=="Y" ? true : false;
			}
		}
		return $portfolios;
	}
	
	function getAPortfolio($i) {
		$data = array();
		$sql = "SELECT p.* FROM ".$this->getDBRef()."_list_portfolio p
				WHERE p.id = $i ";
		$p = $this->mysql_fetch_one($sql);
		$d = $this->getADepartment($p['deptid']);
		return $this->decode($d['name']." - ".$p['value']);
	}
	function getUsedPortfolios() {
		$data = array();
		$sql = "SELECT DISTINCT p.id as id, CONCAT(d.value,' - ',p.value) as name
				FROM ".$this->getDBRef()."_list_portfolio p
				INNER JOIN ".$this->getDepartmentDBRef()." d
				ON p.deptid = d.id
				INNER JOIN ".$this->getDBRef()."_res r 
				ON r.resportfolioid = p.id AND r.resstatusid <> 'CN'
				ORDER BY ".$this->getDepartmentSortField("d").", p.value";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}	
	
	//URGENCY
	function getAUrgency($i) {
		$data = array();
		$sql = "SELECT u.* FROM ".$this->getDBRef()."_list_urgency u
				WHERE u.id = $i ";
		$u = $this->mysql_fetch_one($sql);
		return $u['value'];
	}
	function getActiveUrgency() {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	function getUsedUrgency() {
		$data = array();
		$sql = "SELECT DISTINCT u.id as id, u.value as name FROM ".$this->getDBRef()."_list_urgency u INNER JOIN ".$this->getDBRef()."_res r ON r.resurgencyid = u.id AND r.resstatusid <> 'CN' WHERE yn = 'Y' ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	
	//STATUS
	function getActiveStatus() {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDBRef()."_list_status WHERE yn = 'Y' AND id <> 'CN' ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	function getAStatus($i) {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDBRef()."_list_status WHERE id = '$i'";
		$data = $this->mysql_fetch_one($sql);
		return $data['name'];
	}
	function getAllStatus() {
		$data = array();
		$sql = "SELECT id as id, value as name FROM ".$this->getDBRef()."_list_status ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}
	function getDefaultState($s) {
        $sql = "SELECT state FROM ".$this->getDBRef()."_list_status WHERE id = '".$s."'";
		return $this->mysql_fetch_one_value($sql,"state");
	}
	function getIncompleteStatusId() {
		$sql = "SELECT id FROM  ".$this->getDBRef()."_list_status WHERE id NOT IN ('CL','CN')";
		return $this->mysql_fetch_value_by_id($sql,"id","id");
	}
	function getUsedStatus() {
		$data = array();
		$sql = "SELECT s.id as id, s.value as name FROM ".$this->getDBRef()."_list_status s INNER JOIN ".$this->getDBRef()."_res r ON r.resstatusid = s.id WHERE s.id <> 'CN' ORDER BY sort";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}




	/***********************
			VALIDATION FUNCTIONS 
	***********************/
	function isModuleAdmin($i) {
		return $this->module_administrator==$i;
	}
	

	/***********************
			LOGGING FUNCTIONS 
	***********************/
	function logMe($section,$action,$id,$fld,$trans,$old,$new,$sql,$table="") {
				$l = array(
					'sec'=>		$section,
					'ref'=>		$id,
					'action'=>	$action,
					'fld'=> 	is_array($fld) ? serialize($fld) : $fld,
					'trans'=> 	is_array($trans) ? serialize($trans) : $this->assist->code($trans),
					'old'=>		is_array($old) ? serialize($old) : $old,
					'new'=>		is_array($new) ? serialize($new) : $new,
					'sql'=> 	addslashes($sql)
				);
				$this->createLog($l,$table);
	}	
	private function createLog($l,$table="") {
		$cmpcode = $this->cc;
		$tkid = $this->user_id;
		$tkn = $this->getUserName();//$_SESSION['tkn'];
		if(!isset($table) || strlen($table)==0) { $table = $this->setup_log_table; }
		switch($table) {
			case "raps":
				break;
			case "raps_setup":
				$sql = "INSERT INTO assist_".$cmpcode."_".$table."_log 
					(id, date, tkid, tkname, section, ref, action, field, transaction, old, new, active, lsql)
					VALUES
					(null, now(), '$tkid', '$tkn', '".$l['sec']."', '".$l['ref']."', '".$l['action']."', '".$l['fld']."', '".$l['trans']."', '".$l['old']."', '".$l['new']."', true, '".$l['sql']."')";
				break;
		}
		$this->db_insert($sql);
	}

	public function updateRes($action,$res,$trans,$actdate="",$statusid="",$state="",$deadline=""){
		$logdate = $this->getToday();
		$logtkid = $this->getUserID();
		$logupdate = $this->code($trans);
		$logstatusid = strlen($statusid)>0 ? $statusid : $res['resstatusid'];
		$logstate = strlen($state)>0 ? $state : $res['resstate'];
		$logresid = $res['resid'];
		$logdeadline = strlen($deadline)>0 ? $deadline : $res['resdeadline'];
		$logaction = $action;
		$logactdate = strlen($actdate)>0 ? $actdate : $logdate;
		
		$sql = "INSERT INTO ".$this->getDBRef()."_log (logdate,logtkid,logupdate,logstatusid,logstate,logresid,logdeadline,logaction,logactdate) VALUES ($logdate,'$logtkid','$logupdate','$logstatusid',$logstate,$logresid,$logdeadline,'$logaction',$logactdate)";
		$this->db_insert($sql);
	}


	
	
	/***********************
			DATA FUNCTIONS 
	***********************/
	
	function getViewSQL($type,$start,$length,$filter,$sort) {	
		$sql = "SELECT ";
		switch($type){
			case "PAGING":
				$sql.= " count(resid) as c ";
				break;
			case "VIEW":
				$sql.= "
					R.resid as resid
					, R.resref as resref
					, CONCAT(resp.tkname,' ',resp.tksurname) as restkid
					, restkid as responsible_person
					, CONCAT(action.tkname,' ',action.tksurname) as resactionowner
					, resactionowner as action_owner
					, CONCAT(addu.tkname,' ',addu.tksurname) as resadduser
					, resadduser as add_user
					, resadddate
					, R.resportfolioid
					, P.deptid
					, U.value as resurgencyid
					, U.id as urgencyid
					, S.value as resstatusid
					, S.id as statusid
					, R.resstate
					, R.resmeetingdate
					, R.resdeadline
					, R.resaction
					, R.resnotes
				";
				break;
			case "LIST":
			default:
				$sql.= "
					resid as resid
					, resref as resref
					, CONCAT(resp.tkname,' ',resp.tksurname) as restkid
					, CONCAT(action.tkname,' ',action.tksurname) as resactionowner
					, CONCAT(addu.tkname,' ',addu.tksurname) as resadduser
					, resadddate
					, resportfolioid
					, resurgencyid
					, resstatusid
					, resstate
					, resmeetingdate
					, resdeadline
					, resaction
					, resnotes
				";
				break;
		}
		$sql.= "
				FROM ".$this->getDBRef()."_res R 
				INNER JOIN assist_".$this->getCmpCode()."_timekeep resp
				ON R.restkid = resp.tkid
				INNER JOIN assist_".$this->getCmpCode()."_timekeep action
				ON R.resactionowner = action.tkid
				INNER JOIN assist_".$this->getCmpCode()."_timekeep addu
				ON R.resadduser = addu.tkid
				INNER JOIN ".$this->getDBRef()."_list_portfolio P
				ON R.resportfolioid = P.id
				INNER JOIN ".$this->getDBRef()."_list_status S
				ON R.resstatusid = S.id
				LEFT OUTER JOIN ".$this->getDBRef()."_list_urgency U
				ON R.resurgencyid = U.id
				WHERE ".$this->getSQLStatusForWhere("R")
				." ".(strlen($filter)>0 ? " AND ".$filter : "")
				.$sort.(($length>0) ? " LIMIT $start , $length" : "");
		return $sql;
	}

	function getViewResolutions($start,$length,$filter,$sort) {	
		$data = array();
		$port = $this->getAllPortfolios();
		$status = $this->getAllStatus();
		$sql = $this->getViewSQL("LIST",$start,$length,$filter,$sort);
		$data = $this->mysql_fetch_all($sql);
		foreach($data as $key=>$d) {
			$data[$key]['resportfolioid'] = $port[$d['resportfolioid']]['display'];
			$data[$key]['resstatusid'] = $status[$d['resstatusid']].($d['resstate']>=0 ? " (".$d['resstate']."%)" : "");
			$data[$key]['resaction'] = $this->decode($d['resaction']);
			$data[$key]['resnotes'] = $this->decode($d['resnotes']);
			$data[$key]['resref'] = $this->decode($d['resref']);
		}
		return $data;
	}
	
	function getViewPagingDetails($start,$length,$filter) {
		$data = array('beginning'=>0,'previous'=>0,'next'=>0,'end'=>0,'page'=>1,'pages'=>1);
		$sql = $this->getViewSQL("PAGING",0,0,$filter,""); 
		$r = $this->mysql_fetch_one($sql); 
		$c = $r['c']; 
		//pages
		$pages = ceil($c/$length)<1 ? 1 : ceil($c/$length);
		//page
		$page = ($start/$length)+1;
		//beginning
		$beginning = 0;
		//previous		
		if($start==0) {
			$previous = 0;
		} else {
			$previous = ($start-$length)<0 ? 0 : $start-$length;
		}
		//next		//end
		if($c<$length || ($c-$start)<$length) {
			$next = 0;
			$end = 0;
		} else {
			$next = $start+$length;
			$end = $c-($c%$length);
		}
		//data
		$data['beginning'] = $beginning;
		$data['previous'] = $previous;
		$data['next'] = $next;
		$data['end'] = $end;
		$data['page'] = $page;
		$data['pages'] = $pages;
		return $data;
	}
	
	function getManageUpdateActionResolutions($start,$length,$filter,$sort) {	
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' AND R.resactionowner = '".$this->getUserID()."'";
		$data = $this->getViewResolutions($start,$length,$filter,$sort);
		return $data;
	}
	function getManageUpdateActionPagingDetails($start,$length,$filter) {
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' AND R.resactionowner = '".$this->getUserID()."'";
		$data = $this->getViewPagingDetails($start,$length,$filter);
		return $data;
	}
	
	function getManageUpdateResolutions($start,$length,$filter,$sort) {	
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' AND R.restkid = '".$this->getUserID()."'";
		$data = $this->getViewResolutions($start,$length,$filter,$sort);
		return $data;
	}
	function getManageUpdatePagingDetails($start,$length,$filter) {
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' AND R.restkid = '".$this->getUserID()."'";
		$data = $this->getViewPagingDetails($start,$length,$filter);
		return $data;
	}
	
	function getManageEditResolutions($start,$length,$filter,$sort) {	
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resadduser = '".$this->getUserID()."'";
		$data = $this->getViewResolutions($start,$length,$filter,$sort);
		return $data;
	}
	function getManageEditPagingDetails($start,$length,$filter) {
		$filter.= (strlen($filter)>0 ? " AND " : "")." R.resadduser = '".$this->getUserID()."'";
		$data = $this->getViewPagingDetails($start,$length,$filter);
		return $data;
	}
	
	function getAdminResolutions($start,$length,$filter,$sort,$page) {	
		if($page=="admin_update") {
			$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' ";
		}
		$data = $this->getViewResolutions($start,$length,$filter,$sort);
		return $data;
	}
	function getAdminPagingDetails($start,$length,$filter,$page) {
		if($page=="admin_update") {
			$filter.= (strlen($filter)>0 ? " AND " : "")." R.resstatusid <> 'CL' ";
		}
		$data = $this->getViewPagingDetails($start,$length,$filter);
		return $data;
	}
	
	
	
	function getAResolution($resid) {
		$sql = $this->getViewSQL("VIEW",0,0,"R.resid = ".$resid,"");
		$data = $this->mysql_fetch_one($sql);
		$data['portfolioid'] = $data['resportfolioid'];
		$data['resportfolioid'] = $this->getAPortfolio($data['resportfolioid']);
		$d = $this->getADepartment($data['deptid']);
		$data['dept'] = $d['name'];
		return $data;
		//return $sql;
	}
	
	function getResAttachments($resid) {
		$data = array();
		
		$sql = "SELECT * FROM ".$this->getDBRef()."_attachments WHERE resid = ".$resid;
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		
		return $data;
	}
	function getLogAttachments($id) {
		$data = array();
		
		$sql = "SELECT * FROM ".$this->getDBRef()."_attachments WHERE logid = ".$id;
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		
		return $data;
	}
	function getLogAttachmentsByResID($resid) {
		$data = array();
		
		$sql = "SELECT a.* FROM ".$this->getDBRef()."_attachments a
				INNER JOIN ".$this->getDBRef()."_log l
				ON l.logid = a.logid AND l.logresid = ".$resid;
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		
		return $data;
	}
	function getAnAttachment($id) {
		$data = array();
		
		$sql = "SELECT * FROM ".$this->getDBRef()."_attachments WHERE id = ".$id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	
	
	
	//HEADINGS
	function getListHeadings() {
		$data = array();
		foreach($this->headings['list_view'] as $h) {
			$data[$h] = $this->headings['head'][$h];
		}
		return $data;
	}
	function getAllHeadings() {
		return $this->headings['head'];
	}
	function getHeadingTypes() {
		return $this->headings['types'];
	}
	
	
	//LOGS
	function displayActivityLog($resid) {
		$sql = "SELECT l.*
				, CONCAT(tk.tkname,' ',tk.tksurname) as user
				, S.value as status
				FROM ".$this->getDBRef()."_log l
				INNER JOIN ".$this->getDBRef()."_list_status S
				ON S.id = l.logstatusid
				INNER JOIN assist_".$this->getCmpCode()."_timekeep tk
				ON l.logtkid = tk.tkid
				WHERE logresid = $resid 
				AND logaction NOT IN ('C')
				ORDER BY logactdate DESC,logdate DESC,logid DESC";
		$log = $this->mysql_fetch_all_by_id($sql,"logid");
		if(count($log)>0) {
			
			echo "
			<h3>Activity Log</h3>
			<table class=list width=99%>
				<tr>
					<th width=100>Date</th>
					<th width=150>User</th>
					<th>Activity</th>
					<th width=100>Status</th>
				</tr>";
			foreach($log as $l) {
				echo "
				<tr>
					<td class=center>".date("d-M-Y",$l['logactdate'])."</td>
					<td>".$l['user']."</td>
					<td>".str_replace(chr(10)." - ","<br /> - ",$l['logupdate']);
					$this->displayAttachments($l['logid'],false,"LOG");
				echo "</td>
					<td>".$l['status']." (".$l['logstate']."%)</td>
				</tr>";
			}
			echo "
			</table>";
		} else {
			echo "
			<h3>Activity Log</h3>
			<p>There is no log to display.</p>";
		}
	}
	
	function displayAttachments($id,$can_edit=false,$type="RES") {
		switch($type) {
		case "LOG":
			$attach = $this->getLogAttachments($id);
			break;
		case "RES":
		default:
			$attach = $this->getResAttachments($id);
		}
			$valid_files = false;
			$folder = "";
			$location = explode("/",$_SERVER["REQUEST_URI"]);
			$l = count($location)-2;
			for($f=0;$f<$l;$f++) {
				$folder.= "../";
			}
			$folder.="files/".$this->getCmpCode()."/RAPS";
				
			foreach($attach as $key=>$a) {
				$file = $folder."/".$a['system_filename'];
				if(file_exists($file)) {
					if($valid_files===false) { echo "<p class=b>Attached Files:</p><ul>"; }
					$valid_files = true;
					echo "<li class=hover id=li_".$key."><a id=".$key." class='down_attach hand u'>".$a['original_filename']."</a>".($can_edit==true ? "<span class='float red hand delete_attach' key=".$key.">Delete</span>" : "")."</li>";
				}
			}
			if($valid_files===true) {
				echo "</ul>";
				if($this->attach_java_already_echoed!==true) {
					$this->attach_java_already_echoed = true;
					echo "<script type=text/javascript>
					$(function() {
						$('a.down_attach').click(function() {
							var i = $(this).prop('id');
							document.location.href = 'ajax/download_attachment.php?id='+i;
						});
					});
					</script>";
				}
			}
	}
	function displayAttachmentForm($next_step="") {
		echo "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <input type=button value=\"Clear\" class=cancel_attach  />
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			<input type=hidden name=next_step value='".$next_step."' id=next_step />
			<input type=hidden value='' name=result id=result />
			<input type=hidden value='' name=response id=response />
			<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #ffffff;\"></iframe>
		<script type=text/javascript>
		$(function() {
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<input type=button value=\"Clear\" class=cancel_attach />').bind('click',function() { handler($('#firstdoc input:button').index(this)); });
				});
			});
			$('input:button.cancel_attach').click(function() {
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		});
		</script>";
	}
	
	
	//EMAIL SENDING
	function sendResolutionNotification($type,$resid,$logdata=array(),$oldres=array()) {
		/* Types:
			NEW - email to be sent on creation of new resolution
			UPDATE - email to be sent on update of resolution
			EDIT - email to be sent on edit of resolution
			ACTION_OWNER - email to be sent when action_owner is reassigned
		*/
		$copyEmail = isset($logdata['copyemail']) && $logdata['copyemail']===true ? true : false;
		$return = "";
		$data = $this->getAResolution($resid);
		$headings = $this->getAllHeadings();
		$sendEmail = false;
		$reply_to = array('name'=>$this->getUserName(),'email'=>$this->getAnEmail($this->getUserID()));
		$cc = array();
		switch($type) {
			case "NEW":
				if($data['responsible_person']!=$this->getUserID() || $copyEmail===true) { 
					$sendEmail = true;
					$to = array('name'=>$data['restkid'],'email'=>$this->getAnEmail($data['responsible_person']));
					$addusername = $reply_to['name'];
					$subject = "New Resolution ".RAPS::REFTAG.$resid."";
					$message = "";
					$message.= $addusername." has assigned a new resolution (".RAPS::REFTAG.$resid.") to you as the ".strtolower($headings['tkid']).".\n";
					$message.= "\n".$this->generateResolutionDetailsForEmail($data);
					$message.= "Please log onto Ignite Assist in order to update this resolution.";
				}
			break;
			case "UPDATE":
				//updated by admin = notify add_user, responsible_person & action_owner; 
				//updated by responsible_person = notify add_user and action_owner; 
				//updated by action_owner = notify add_user and responsible_person
				$send_to = array();
				$to = array();
				if($data['add_user']!=$this->getUserID()) { $send_to[] = $data['add_user']; }
				if($data['responsible_person']!=$this->getUserID() && !in_array($data['responsible_person'],$send_to)) { $send_to[] = $data['responsible_person']; }
				if($data['action_owner']!=$this->getUserID() && !in_array($data['action_owner'],$send_to)) { $send_to[] = $data['action_owner']; }
				if(count($send_to)>0) { 
					$sendEmail = true;
					foreach($send_to as $t) {
						$to[] = array('name'=>$this->getAUserName($t),'email'=>$this->getAnEmail($t));
					}
					if($copyEmail===true && !in_array($this->getUserID,$send_to)) { 
						$cc = $this->getAnEmail($this->getUserID());
					}
					$subject = "Resolution ".RAPS::REFTAG.$resid." Updated";
					$message = "Resolution ".RAPS::REFTAG.$resid." has been updated by ".$this->getUserName().".\n";
					$message.= "The update is as follows:\n";
					$message.= "Response: ".$logdata['logupdate']."\n";
					$message.= "Action on: ".$logdata['logactdate']."\n";
					$message.= "Status: ".$data['resstatusid'].($data['resstate']>=0 ? " (".$data['resstate']."%)" : "")."\n";
					$message.= "\n".$this->generateResolutionDetailsForEmail($data);
				}
			break;
			case "EDIT":
				$sendToAdd = $data['add_user']!=$this->getUserID() ? true : false;
				$sendToRespPerson = true;
				$sendToActionOwner = true;
				$sendCopy = $copyEmail;
				unset($logdata['copyemail']);

				$subject = "Resolution ".RAPS::REFTAG.$resid." Edited";
				if($oldres['responsible_person']!=$data['responsible_person'] && ($data['responsible_person']!=$this->getUserID() || $copyEmail==true)) {
					$sendToRespPerson = false;
					$to = array('name'=>$data['restkid'],'email'=>$this->getAnEmail($data['responsible_person']));
					if($copyEmail==true && $data['responsible_person']!=$this->getUserID()) {
						$cc = $this->getAnEmail($this->getUserID());
					}
					$message = "Resolution ".RAPS::REFTAG.$resid." has been reassigned to you as the ".strtolower($headings['tkid']).".\n";
					$message.= "\n".$this->generateResolutionDetailsForEmail($data);
					$message.= "Please log onto Ignite Assist in order to update this resolution.";
					$this->sendResEmail($to,$subject,$message,$reply_to,$cc);
				}
				if($oldres['action_owner']!=$data['action_owner'] && $data['responsible_owner']!=$data['action_owner'] && ($data['action_owner']!=$this->getUserID() || $copyEmail==true)) {
					$sendToActionOwner = false;
					$this->sendResolutionNotification("ACTION_OWNER",$resid,array('copyemail'=>$copyEmail));
				}
				if($sendToAdd==true || $sendToRespPerson==true || $sendToActionOwner==true || $sendCopy == true) {
					$send_to = array();
					$to = array();
					//if($sendToAdd==true && $data['add_user'] != $this->getUserID()) {	$send_to[] = $data['add_user'];	}
					//if($sendToRespPerson==true && $data['responsible_person'] != $this->getUserID()) {	$send_to[] = $data['responsible_person'];	}
					//if($sendToActionOwner==true && $data['action_owner'] != $this->getUserID()) {	$send_to[] = $data['action_owner'];	}
					if($sendToAdd==true && $data['add_user']!=$this->getUserID()) { $send_to[] = $data['add_user']; }
					if($sendToRespPerson==true && $data['responsible_person']!=$this->getUserID() && !in_array($data['responsible_person'],$send_to)) { $send_to[] = $data['responsible_person']; }
					if($sendToActionOwner==true && $data['action_owner']!=$this->getUserID() && !in_array($data['action_owner'],$send_to)) { $send_to[] = $data['action_owner']; }
					if(count($send_to)>0 || $sendCopy==true) {
						$sendEmail = true;
						foreach($send_to as $t) {
							$to[] = array('name'=>$this->getAUserName($t),'email'=>$this->getAnEmail($t));
						}
						if($sendCopy==true && !in_array($this->getUserID,$send_to)) { 
							if(count($to)>0) {
								$cc = $this->getAnEmail($this->getUserID());
							} else {
								$to[] = array('name'=>$this->getAUserName($this->getUserID()),'email'=>$this->getAnEmail($this->getUserID()));
							}
						}
						//$return = serialize($to);
						$message = "Resolution ".RAPS::REFTAG.$resid." has been edited by ".$this->getUserName().".\n";
						if(count($logdata)>1) {
							$message.= "The following changes were made:\n";
						} else {
							$message.= "The following change was made:\n";
						}
						$message.= implode(";\n",$logdata).";\n";
						$message.= "\n".$this->generateResolutionDetailsForEmail($data);
					}
				}			
			break;
			case "ACTION_OWNER":
				if($this->getUserID()!=$data['action_owner'] || $copyEmail===true) {
					$sendEmail = true;
					$to = array();
					$to[] = array('name'=>$data['resactionowner'],'email'=>$this->getAnEmail($data['action_owner']));
					if($this->getUserID()!=$data['action_owner'] && $logdata['copyemail']===true) {
						$cc = $this->getAnEmail($this->getUserID());
					}
					$subject = "Resolution ".RAPS::REFTAG.$resid." Reassigned";
					$message = "";
					$message.= $this->getUserName()." has assigned Resolution ".RAPS::REFTAG.$resid." to you as the ".strtolower($headings['actionowner']).".\n";
					$message.= "\n".$this->generateResolutionDetailsForEmail($data);
					//$message.= "\n ::".$copyEmail."::".serialize($logdata)."::\n cc:".serialize($cc);
					$message.= "Please log onto Ignite Assist in order to update this resolution.";
				}
			break;
		}
		if($sendEmail) {
			$this->sendResEmail($to,$subject,$message,$reply_to,$cc);
			//return serialize($to);
		}
	}
	function generateResolutionDetailsForEmail($data) {
			$message = "Resolution Details:\n";
					$message .= "Responsible Person: ".$data['restkid']."\n";
					$message .= "Action Owner: ".$data['resactionowner']."\n";
					$message .= "Portfolio: ".$data['resportfolioid']."\n";
					$message .= "Priority: ".$data['resurgencyid']."\n";
					$message .= "Council Meeting Date: ".date("d F Y",$data['resmeetingdate'])."\n";
					$message .= "Deadline: ".date("d F Y",$data['resdeadline'])."\n";
					$message .= "Status: ".$data['resstatusid'].($data['resstate']>=0 ? " (".$data['resstate']."%)" : "")."\n";
					$message .= "Instructions:\n".$this->decode($data['resaction'])."\n";
				if(strlen($data['resnotes'])>0) {
					$message .= "Additional Notes:\n".$this->decode($data['resnotes'])."\n";
				}
		return $message;
	}
	function sendResEmail($to,$subject,$message,$reply_to,$cc=array()) {
			$email = new ASSIST_EMAIL();
				$email->setRecipient($to);
				$email->setSubject($subject);
				$email->setBody($message);
				if(count($cc)>0) {
					$email->setCC($cc);					//not required
				}
					//$email->setBCC($bcc);					//not required
				$email->setSender($reply_to);
					//$email->setContentType($content_type);  //default = text
			$email->sendEmail();
	}
	//REPORTING
	function getSQLStatusForWhere($t) {
		return $t.".resstatusid <> 'CN'";
	}
	


	public function __destruct() {
		parent::__destruct();
	}
	
	

}

?>