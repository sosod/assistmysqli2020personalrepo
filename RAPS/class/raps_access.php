<?php

class RAPS_ACCESS {

	private $my_access = array();
	private $module_admin = "0000";
	private $my_id = "";
	
	public function __construct($ma=array()) {
		$this->setMyAccess($ma);
	}
	
	function setMyAccess($ma) {
		$this->my_access = $ma;
	}

	function setModuleAdmin($a) { $this->module_admin = $a; }
	function setMyUserID($a) { $this->my_id = $a; }
	
	/*function canViewAll($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['view_all']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}*/

	function canReceiveNew($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['receive_new']; 
		} elseif($x=="Y" || $x=="C") { 
			return true; 
		}
		return false;
	}
	
	function canReceiveAction($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['receive_action']; 
		} elseif($x=="Y" || $x=="A") { 
			return true; 
		}
		return false;
	}

	function canEdit($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['edit']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}
	function canUpdateAll($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['update']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}
	
	function canAdd($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['add']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}
	
	function canGetCopyEmail($x="") {
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['email_copy']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}
	
	function canSetup($x="") {
/*		if($this->module_admin==$this->my_id) { 
			return true; 
		}*/
		if(strlen($x)==0) { 
			return $this->my_access[0]&&$this->my_access['setup']; 
		} elseif($x=="Y") { 
			return true; 
		}
		return false;
	}
	
	function accessToAdmin() {
		return ($this->my_access['edit'] && $this->my_access['update']);
	}
	
}



?>