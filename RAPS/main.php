<?php 

include("inc_header.php");
include("../CAPS/inc_amcharts.php");
//require_once("../library/class/assist_graph.php");
//$db = new ASSIST_DB("client");
//$db->setDBRef("RAPS");
?>
<h1>Resolutions Assist</h1>
<?php

$colors = array(
	'IP'=>"orange",
	'NW'=>"red",
	'CL'=>"green",
	'ON'=>"grey",
);

$sql = "SELECT s.id, s.value, count(r.resid) c
		FROM `".$me->getDBRef()."_list_status` s
		LEFT JOIN ".$me->getDBRef()."_res r
		ON r.resstatusid = s.id
		WHERE s.id <> 'CN'
		GROUP BY s.id, s.value
		ORDER BY s.sort";
$rows = $me->mysql_fetch_all($sql);
$total = 0;
if(count($rows)>0) {
	
	$result_settings = array(
		'NW'=>array(
			'value'=>"New",
			'text'=>"NW",
			'color'=>"#CC0001",
			'pull'=>false,
		),
		'IP'=>array(
			'value'=>"In Progress",
			'text'=>"IP",
			'color'=>"#FE9900",
			'pull'=>false,
		),
		'CL'=>array(
			'value'=>"Completed",
			'text'=>"CL",
			'color'=>"#009900",
			'pull'=>false,
		),
		'ON'=>array(
			'value'=>"On-going",
			'text'=>"ON",
			'color'=>"#999999",
			'pull'=>false,
		)
	);
	
	
	$cate = array();
	$count = array();
	foreach($rows as $r) {
		$total+=$r['c'];
		$result_settings[$r['id']]['value'] = $r['value'];
		$count[$r['id']] = $r['c'];
	}
	
	if($total>0) {
		$sql = "SELECT resadddate FROM ".$me->getDBRef()."_res LIMIT 1";
		$row = $me->mysql_fetch_one($sql);
		drawLegendStyle();
		echo "<table class=container style=\"margin: 0 auto;\"><tr><td class=center>
		<span class=b style=\"margin: 0 auto;\">Status of all Resolutions logged since <br />".date("d F Y H:i:s",$row['resadddate']).".</span>";
		drawJavaGraph($result_settings,$count,"view");
		echo "</td></tr>
		<tr><td class=center>";
		drawPieLegend($count,false,0);
		echo "<p><span class=i style=\"font-size: 6.5pt;\">Graph accurate as at<br />".date("d F Y H:i:s").".</span></p>
		</td></tr></table>";
		echo "<script type=text/javascript>
		$(function() {
			$('table.container, table.container td').addClass('noborder');
		});
		</script>";
	} else {
		echo "<P>Please select an action from the menu above.</p>";
	}
	
} else {
	echo "<P>Please select an action from the menu above.</p>";
}
?>
</body>

</html>
