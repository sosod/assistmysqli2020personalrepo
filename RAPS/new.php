<?php
include("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

$headings = $me->getAllHeadings();

$status = $me->getActiveStatus();
$urgency = $me->getActiveUrgency();
$users = $me->getUsersToReceiveNew();
$port = $me->getActivePortfolios();

?>
<h1>New Resolution</h1>
<?php 
if($me->ra->canAdd()!==true) {
	echo "<P>You do not have permission to create new resolutions</p>";
} else {
?>
<p><i>Please note that all fields are required unless otherwise indicated.</i></p>
<?php $me->displayResult($result); ?>
<form name=new method=post action="ajax/upload_attachment.php" language=jscript enctype="multipart/form-data">
<input type=hidden value="" name=resid id=resid />
<input type=hidden value="RES" name=attach_type  />
<table class=form>
	<tr>
		<th id=resref><?php echo $headings['ref']; ?>:</th>
        <td><input type=text name=resref maxlength=30 /></td>
	</tr>
	<tr>
		<th id=restkid><?php echo $headings['tkid']; ?>:</th>
		<td>
            <select name="restkid">
                <option selected value=X>--- SELECT ---</option>
                <?php
				foreach($users as $key=>$u) {
					echo "<option value='".$key."' deptid=".$u['deptid'].">".$me->decode($u['display'])."</option>";
				}
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<th><?php echo $headings['adduser']; ?>:</th>
		<td><?php echo($me->getUserName()); ?><input type=hidden size=5 name=resadduser value="<?php echo($me->getUserID()); ?>"></td>
	</tr>
	<tr>
		<th id=resportfolioid><?php echo $headings['portfolioid']; ?>:</th>
		<td>
            <select name="resportfolioid">
                <option selected value=X>--- SELECT ---</option><?php
				foreach($port as $key=>$p) {
					echo "<option value=".$key." class=D".$p['deptid'].">".$p['display']."</option>";
				}
            ?></select>
        </td>
	</tr>
	<tr>
		<th id=resurgencyid><?php echo $headings['urgencyid']; ?>:</th>
		<td>
            <select name="resurgencyid">
                <option value=X>--- SELECT ---</option><?php
				foreach($urgency as $key=>$v) {
					echo "<option ".($key==2 ? "selected" : "")." value=$key>$v</option>";
				}
            ?></select>
        </td>
	</tr>
	<tr>
		<th id=resstatusid><?php echo $headings['statusid']; ?>:</td>
		<td>
            <select name="resstatusid"><?php
				foreach($status as $key=>$v) {
					if(!in_array($key,array("CL","IP","CN"))) {
						echo "<option value=$key>$v</option>";
					}
				}
            ?></select>
        </td>
	</tr>
	<tr>
        <th id=resmeetingdate><?php echo $headings['meetingdate']; ?>:</th>
		<td><input type="text" name="resmeetingdate" size=10 class=jdate2012 value='<?php echo date("d-M-Y"); ?>' /></td>
	</tr>
	<tr>
		<th id=resdeadline><?php echo $headings['deadline']; ?>:</th>
        <td><input type="text" name="resdeadline" size=10 class=jdate2012 /></td>
	</tr>
	<tr>
		<th id=resaction><?php echo $headings['action']; ?>:</th>
        <td><textarea name="resaction"></textarea></td>
	</tr>
	<tr>
		<th id=resnotes><?php echo $headings['notes']; ?>:<br /><span class=i>(Not required)</span></th>
        <td><textarea name="resnotes"></textarea></td>
	</tr>
    <tr>
		<th>Attach document(s):</th>
		<td >
			<?php $me->displayAttachmentForm("new.php?"); ?>
		</td>
	</tr>
    <tr>
		<th>Receive copy email?</th>
		<td ><select name=copyemail><option value=N selected>No</option><option value=Y>Yes</option></select></td>
	</tr>
	<tr>
		<th></th>
		<td><input type="button" value="Submit" class=isubmit />
		<input type="reset" value="Reset" name="B2"></td>
	</tr>

</table>
</form>
<!-- div to display dialog when required fields are missing -->
<div id=dlg_error title="Required fields">
<h1 class=red>Error</h1>
<p>Please complete the following required fields:</p>
<ul id=ul_req>
</ul>
</div>
<div id=dlg_loading>
<p class=center>Please wait while your form is processed...</p>
<p class=center><img src="../pics/ajax_loader_v2.gif" /></p>
</div>
<div id=dlg>
</div>
<style type=text/css>
.green { border: 1px solid #009900; color: #009900; }
.green:hover { border: 1px solid #fe9900; color: #fe9900; }
</style>
<script type=text/javascript>
$(function() {
	var notRequired = new Array("resnotes");
	$("select[name=resstatusid]").val($("select[name=resstatusid] option:first").val());
	$("select[name=restkid]").change(function() {
		var d = $("select[name=restkid] option:selected").attr("deptid");
		if($("select[name=resportfolioid] option.D"+d).size()>0) {
			$("select[name=resportfolioid] option.D"+d+":first").prop("selected","selected");
		} else {
			$("select[name=resportfolioid] option:first").prop("selected","selected");
		}
	});
	
	var dlgErrorOpts = ({
		modal: true,
		autoOpen: false,
		width: 300,
		height: 320,
		buttons:[{
			text: "Ok",
			
			click: function() { $(this).dialog("close"); }
		}]
	});
	var dlgLoadingOpts = ({
		modal: true,
		autoOpen: false,
		width: 250,
		height: 270
	});
	$("#dlg").dialog({modal: true,autoOpen: false});
	$(".ui-dialog-titlebar").hide();
	$("#dlg_error, #dlg_loading").hide();
	/*$("#dlg_error").dialog("option","buttons").each(function() { 
		//$(this).removeClass("ui-state-highlight").addClass("green");
		alert($(this).text());
	});*/
	
	$("form[name=new] input:button.isubmit").click(function() { 
		$("#dlg").html($("#dlg_loading").html());
		//$("#dlg").dialog("option",dlgLoadingOpts).dialog('option','buttons',{}).dialog("open");
$("#dlg").dialog("option",dlgLoadingOpts).dialog("open");
		var $form = $("form[name=new]");
		var resid = 0;
		var response = "";
		var result = "ok";
	
		var valid = true;
		//commented out for file upload testing purposes
		$("#dlg_error #ul_req li").remove();
		var d = "";
		//var data = "act=CREATE";
		$("select, input:text, textarea").removeClass("required").each(function() {
			var $me = $(this);
			var n = $me.prop("name");
			d = $me.is("textarea") ? $me.text() : $me.val();
			if(d.length==0) {
				d = $me.is("textarea") ? $me.val() : $me.text();
			}
			if($.inArray(n,notRequired)<0) {
				if(d.length==0 || ($me.is("select") && d=="X")) {
					$me.addClass("required");
					var i = $("#"+n).html();
					i = i.replace(":","");
					$("#dlg_error #ul_req").append("<li>"+i+"</li>");
					valid = false;
				}
			}
			//data+="&"+n+"="+d;
		});
		if(valid) {
			//check for files to be uploaded
			var f = 0;
			$("input:file").each(function() {
				f+=$(this).val().length;
			});
			$form.prop("target","file_upload_target");
			var dta = AssistForm.serialize($form);
			//alert(f);
			$.ajax({                                      
				url: 'ajax/new.php', 		  type: 'POST',		  data: dta,		  dataType: 'json',
				success: function(d) { 
					result = d[0]; 
					response = d[1];
					$("#resid").val(d[2]);
					$("#result").val(d[0]);
					$("#response").val(d[1]);
					//alert(d[2]+" = "+$("#resid").val());
					if(f>0) {
						//alert("f>0");
						//alert("submitting to "+$("form[name=new]").prop("target"));
						$form.submit();
					} else {
					//$("#dlg").dialog("close");
						document.location.href = 'new.php?r[]='+result+'&r[]='+response;
					}
				},
				error: function(d) { console.log(d); }
			});
		} else {
			$("#dlg").dialog("close");
			$("#dlg").html($("#dlg_error").html());
			$("#dlg").dialog("option",dlgErrorOpts).dialog("open");
			$('.ui-dialog :button').blur();
			$(".ui-dialog :button:last").css({"color":"#009900","border":"1px solid #009900"});
		}
	});
	
});
</script>
<?php } ?>
<p>&nbsp;</p>

</body>

</html>
