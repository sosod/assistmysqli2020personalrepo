<?php
//error_reporting(-1);
include_once("../module/autoloader.php");
@session_start();
spl_autoload_register("Loader::autoload");
$loc = explode("/",$_SERVER['PHP_SELF']);
$self = substr($loc[count($loc)-1],0,-4);

//check for redirect
$me = new RAPS();


switch($self) {
	case "manage":			header("Location: manage_view.php");	break;
	case "manage_update":	header("Location: manage_update_action.php");	break;
	case "admin":			if($me->ra->canEdit()) { header("Location: admin_edit.php"); } else { header("Location: admin_update.php"); }	break;
}

$me->displayPageHeader();

$base = explode("_",$self);

$menu = array();

switch($base[0]) {
case "admin":
	$menu = array(
		'edit'		=> array('id'=>"edit",'url'=>"admin_edit.php",'display'=>"Edit",'active'=>($base[1]=="edit" ? true : false)),
		'update'	=> array('id'=>"update",'url'=>"admin_update.php",'display'=>"Update",'active'=>($base[1]=="update" ? true : false)),
	);
	if(!$me->ra->canEdit()) { unset($menu['edit']); }
	if(!$me->ra->canUpdateAll()) { unset($menu['update']); }
	break;
case "manage":
	$menu = array(
		'view'		=> array('id'=>"view",'url'=>"manage_view.php",'display'=>"View",'active'=>($base[1]=="view" ? true : false)),
		'update'	=> array('id'=>"update",'url'=>"manage_update.php",'display'=>"Update",'active'=>($base[1]=="update" ? true : false)),
		'edit'		=> array('id'=>"edit",'url'=>"manage_edit.php",'display'=>"Edit",'active'=>($base[1]=="edit" ? true : false)),
//		'approve'	=> array('id'=>"approve",'url'=>"manage_approve.php",'display'=>"Approve",'active'=>($base[1]=="approve" ? true : false)),
//		'assurance' => array('id'=>"assurance",'url'=>"manage_assurance.php",'display'=>"Assurance",'active'=>($base[1]=="assurance" ? true : false)),
	);
	break;
case "report":
	$menu = array(
		'generate'	=> array('id'=>"generate",'url'=>"report_generate.php",'display'=>"Generator",'active'=>($base[1]=="generate" ? true : false)),
//		'fixed'		=> array('id'=>"fixed",'url'=>"report_fixed.php",'display'=>"Fixed Reports",'active'=>($base[1]=="fixed" ? true : false)),
	);
	break;
case "setup":
	$menu = array(
		'setup'			=> array('id'=>"setup",'url'=>"setup.php",'display'=>"Defaults",'active'=>(!isset($base[1]) || $base[1]!="access" ? true : false)),
		'setup_access'	=> array('id'=>"setup_access",'url'=>"setup_access.php",'display'=>"User Access",'active'=>(isset($base[1]) && $base[1]=="access" ? true : false)),
	);
	break;
}
if(isset($menu) && count($menu)>0) {
	$me->drawNavButtons($menu);
}

$menu2 = array();
if(isset($base[1])) {
	switch($base[1]) {
	case "update":
		if($base[0]=="manage") {
			$menu2 = array(
				'res'		=> array('id'=>"res",'url'=>$base[0]."_".$base[1]."_res.php",'display'=>"Resolutions",'active'=>($base[2]=="res" ? true : false)),
				'action'	=> array('id'=>"action",'url'=>$base[0]."_".$base[1]."_action.php",'display'=>"Action",'active'=>($base[2]=="action" ? true : false)),
			);
		}
		break;
	}
	if(isset($menu2) && count($menu2)>0) {
		$me->drawNavButtons($menu2);
	}
}


//default variables needed
$can_edit_action_owner = false;
$can_update = false;
$can_edit_attach = false;







?>
<style type=text/css>
/* Ignite Assist Styles */
.ui-state-ok, .ui-widget-content .ui-state-ok, .ui-widget-header .ui-state-ok  {border: 1px solid #009900; background: #efffef; color: #006600; }
.ui-state-ok a, .ui-widget-content .ui-state-ok a,.ui-widget-header .ui-state-ok a { color: #363636; }
.ui-state-yes, .ui-widget-content .ui-state-yes, .ui-widget-header .ui-state-yes  {border: 0px solid #009900; background: #ffffff; color: #006600; }
.ui-state-yes a, .ui-widget-content .ui-state-yes a,.ui-widget-header .ui-state-yes a { color: #363636; }
.ui-state-info, .ui-widget-content .ui-state-info, .ui-widget-header .ui-state-info  {border: 1px solid #fe9900; background: #fbf9ee ; color: #363636; }
.ui-state-info a, .ui-widget-content .ui-state-info a,.ui-widget-header .ui-state-info a { color: #363636; }
.ui-state-fail, .ui-widget-content .ui-state-fail, .ui-widget-header .ui-state-fail  {border: 0px solid #cc0001; background: #ffffff ; color: #cd0a0a; }
.ui-state-fail a, .ui-widget-content .ui-state-fail a,.ui-widget-header .ui-state-fail a { color: #363636; }
.ui-state-info .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);
}
.ui-state-fail .ui-icon, .ui-state-error .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_cd0a0a_256x240.png);
}
.ui-state-ok .ui-icon, .ui-state-yes .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png);
}
table.filter { margin-bottom: 10px; }
</style>
<script type=text/javascript>
$(function() {
	$("textarea").prop("cols","75").prop("rows","10");
	$("input:text.jdate2012").prop("size","12");
});
</script>