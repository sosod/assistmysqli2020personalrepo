<?php 
/******* 
  REQUIRED VARIABLES FROM CALLING PAGE:
    $page = "manage_view";
	$page_details = "manage_view_details";
	$button_name = "View Details" || "Update" || "Edit"
********/

$isManageUpdate = ($base[0]=="manage" && $base[1]=="update");

$headings = $me->getListHeadings();
$types = $me->getHeadingTypes();


$filter['status'] = $me->getUsedStatus();
$filter['status']['INCOMPLETE'] = "All Incomplete";
$filter['dept'] = $me->getUsedDepartments();
$fi = $me->getUsedResponsiblePersons() + $me->getUsedActionOwners();
asort($fi);
$filter['user'] = $fi;

$filter['sql'] = array();
if(isset($_REQUEST['resstatusid']) && $_REQUEST['resstatusid']!="X") {
	$fs = $_REQUEST['resstatusid'];
	if($fs=="INCOMPLETE") {
		$incompl = $me->getIncompleteStatusId();
		if(count($incompl)>0) {
			$filter['sql'][] = "R.resstatusid IN ('".implode("','",$incompl)."')";
		}
	} else {
		$filter['sql'][] = "R.resstatusid = '".$_REQUEST['resstatusid']."'";
	}
	$filter['resstatusid'] = $_REQUEST['resstatusid'];
} else {
	$filter['resstatusid'] = "X";
}
if(isset($_REQUEST['deptid']) && $_REQUEST['deptid']!="X") {
	$filter['sql'][] = "P.deptid = ".$_REQUEST['deptid'];
	$filter['deptid'] = $_REQUEST['deptid'];
} else {
	$filter['deptid'] = "X";
}
if(isset($_REQUEST['userid']) && $_REQUEST['userid']!="X") {
	$ui = $_REQUEST['userid'];
	$ut = isset($_REQUEST['usertype']) && strlen($_REQUEST['usertype'])>0 ? $_REQUEST['usertype'] : "BOTH";
	switch($ut) {
	case "restkid":
	case "resactionowner":
		$filter['sql'][] = "R.".$ut." = '".$ui."'";
		break;
	case "BOTH":
	default:
		$filter['sql'][] = "(R.restkid = '".$ui."' OR R.resactionowner = '".$ui."')";
		break;
	}
	$filter['userid'] = $_REQUEST['userid'];
	$filter['usertype'] = $_REQUEST['usertype'];
} else {
	$filter['userid'] = "X";
	$filter['usertype'] = "BOTH";
}
$filter['sql'] = implode(" AND ",$filter['sql']);

if(isset($_REQUEST['displayorder']) && $_REQUEST['displayorder']!="X") {
	$filter['sort'] = " ORDER BY R.res".$_REQUEST['displayorder'];
	$filter['displayorder'] = $_REQUEST['displayorder'];
} else {
	$filter['displayorder'] = "id";
	$filter['sort'] = " ORDER BY R.resid";
}

$start = isset($_REQUEST['s']) ? $_REQUEST['s'] : 0;
$length = isset($_SESSION['USER_PROFILE'][1]['field2']) ? $_SESSION['USER_PROFILE'][1]['field2'] : 20;


switch($page) {
	case "manage_update_action":
	case "manage_update":
		$paging = $me->getManageUpdateActionPagingDetails($start,$length,$filter['sql']);
		$res = $me->getManageUpdateActionResolutions($start,$length,$filter['sql'],$filter['sort']);
		break;
	case "manage_update_res":
		$paging = $me->getManageUpdatePagingDetails($start,$length,$filter['sql']);
		$res = $me->getManageUpdateResolutions($start,$length,$filter['sql'],$filter['sort']);
		break;
	case "manage_edit":
		$paging = $me->getManageEditPagingDetails($start,$length,$filter['sql']);
		$res = $me->getManageEditResolutions($start,$length,$filter['sql'],$filter['sort']);
		break;
	case "admin_edit":
	case "admin_update":
		$paging = $me->getAdminPagingDetails($start,$length,$filter['sql'],$page);
		$res = $me->getAdminResolutions($start,$length,$filter['sql'],$filter['sort'],$page);
		break;
	case "manage_view":
	default:
		$paging = $me->getViewPagingDetails($start,$length,$filter['sql']);
		$res = $me->getViewResolutions($start,$length,$filter['sql'],$filter['sort']);
		break;
}

if(isset($_REQUEST['r']) && is_array($_REQUEST['r'])) {
	$me->displayResult($_REQUEST['r']);
}

?>
<!-- filter -->
<table class='form filter' id=tbl_filter>
	<tr>
		<th width=150>Status:</th>
		<td><select name=resstatusid><?php 
		$a = "status";
		$s = "resstatusid";
				echo "<option value=X ".(!isset($filter[$s]) || strlen($filter[$s])==0 || !isset($filter[$a][$filter[$s]]) ? "selected" : "").">--- SELECT ---</option>";
				foreach($filter[$a] as $i=>$f) {
					echo "<option value=".$i." ".($filter[$s]==$i ? "selected" : "").">".$f."</option>";
				}
		?></select></td>
	</tr>
	<tr>
		<th width=150>Department:</th>
		<td><select name=deptid><?php 
		$a = "dept";
		$s = "deptid";
				echo "<option value=X ".(!isset($filter[$s]) || strlen($filter[$s])==0 || !isset($filter[$a][$filter[$s]]) ? "selected" : "").">--- SELECT ---</option>";
				foreach($filter[$a] as $i=>$f) {
					echo "<option value=".$i." ".($filter[$s]==$i ? "selected" : "").">".$f."</option>";
				}
		?></select></td>
	</tr>
<?php if(!$isManageUpdate) { ?>
	<tr>
		<th width=150>User:</th>
		<td><select name=userid><?php 
		$a = "user";
		$s = "userid";
				echo "<option value=X ".(!isset($filter[$s]) || strlen($filter[$s])==0 || !isset($filter[$a][$filter[$s]]) ? "selected" : "").">--- SELECT ---</option>";
				foreach($filter[$a] as $i=>$f) {
					echo "<option value=".$i." ".($filter[$s]==$i ? "selected" : "").">".$f."</option>";
				}
		$s = "usertype";
		?></select> <select name=usertype>
			<option <?php echo (!isset($filter[$s]) || strlen($filter[$s])==0 || $filter[$s]=="BOTH" ? "selected" : ""); ?> value=BOTH>Responsible Person or Action Owner</option>
			<option <?php echo ($filter[$s]=="restkid" ? "selected" : ""); ?> value=restkid>Responsible Person only</option>
			<option <?php echo ($filter[$s]=="resactionowner" ? "selected" : ""); ?> value=resactionowner>Action Owner only</option>
		</select></td>
	</tr>
<?php } ?>
	<tr>
		<th></th>
		<td><input type=button value='Clear Filter' id=clear_filter /></td>
	</tr>
</table>
<!-- data -->
<table class=container width=100%>
	<tr>
		<td><?php
			echo "
			<span id=spn_paging>
				<input type=button value='|<' id=beginning start='".$paging['beginning']."' /> <input type=button id=previous value='<' start='".$paging['previous']."' /> Page ";
				if($paging['pages']>1) {
					echo "<select id=page>";
					for($i=1;$i<=$paging['pages'];$i++) {
						echo "<option value=$i ".($i==$paging['page'] ? "selected" : "").">$i</option>";
					}
					echo "</select> of ".$paging['pages']." pages";
				} else {
					echo "1 of 1 pages";
				}					
				echo " <input type=button value='>' start='".$paging['next']."' id=next /> <input type=button id=end value='>|' start='".$paging['end']."' /></span>
			<span class=float id=spn_sort>Order by: <select name=displayorder>";
				foreach($headings as $fld=>$h) {
					if($types[$fld]!="LIST") {
						echo "<option ".($fld==$filter['displayorder'] ? "selected" : "")." value=$fld>".$h."</option>";
					}
				}
			echo "
			</select></span>";
		?></td>
	</tr>
	<tr>
		<td>
			<table class=list width=100%>
				<thead>
					<tr>
<?php
foreach($headings as $fld=>$h) {
	echo "<th>".$h."</th>";
}
?>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
if(count($res)>0) {
	foreach($res as $r) {
		echo "
		<tr>";
			foreach($headings as $fld=>$h) {
				$v = $r['res'.$fld];
				switch($types[$fld]) {
					case "REF": $v = RAPS::REFTAG.$v; break;
					case "DATE": $v = date("d-M-Y",$v); break;
					case "TEXT":
						if(strlen($v)>100) {
							$v = "<span id=text_".$fld."_".$r['resid'].">".substr($v,0,75)."...</span> <img src='/pics/plus.gif' class=moretext i=".$r['resid']." fld=$fld alt='$v' />";
						}
						$v = str_replace(chr(10),"<br />",$v);
						break;
				}
				echo "<td>".$v."</td>";
			}
		echo "
			<td class=center width=100><input type=button value='".$button_name."' id=".$r['resid']." class=view_details /></td>
		</tr>";
	}
} elseif(strlen($filter['sql'])>0) {
	echo "<tr><td colspan=".(1+count($headings)).">There are no resolutions which meet your selected criteria.</td></tr>";
} else {
	echo "<tr><td colspan=".(1+count($headings)).">There are no resolutions to display.</td></tr>";
}
?>
				</tbody>
			</table>
		</td>
	</tr>
</table>

<script type=text/javascript>
$(function() {
	var page = "<?php echo $page; ?>.php?";
	var page_details = "<?php echo $page_details; ?>.php?";
	var rows = <?php echo $length; ?>;
	var start = <?php echo $start; ?>;
	function getFilterSettings() {
		var u = "";
		var r = $("select[name=resstatusid]").val();
		if(r!="X") { u += "&resstatusid="+r; }
		var d = $("select[name=deptid]").val();
		if(d!="X") { u += "&deptid="+d; }
		<?php if(!$isManageUpdate) { ?>
		var ui = $("select[name=userid]").val();
		var ut = $("select[name=usertype]").val();
		if(ui!="X") { u += "&userid="+ui+"&usertype="+ut; }
		<?php } ?>
		var i = $("select[name=displayorder]").val();
		if(i!="X") { u += "&displayorder="+i; }
		return u;
	}
	$("img.moretext").css("cursor","pointer").click(function() {
		var a = $(this).prop("alt");
		var f = $(this).attr("fld");
		var i = $(this).attr("i");
		var s = "#text_"+f+"_"+i;
		var b = $(s).html();
		//alert(b);
		$(s).html(a);
		$(this).prop("alt",b);
		if($(this).prop("src") == "/pics/minus.gif") {
			$(this).prop("src","/pics/plus.gif");
		} else {
			$(this).prop("src","/pics/minus.gif");
		}
	});
	$("table.filter select, select[name=displayorder]").change(function() { 
		var u = getFilterSettings();
		document.location.href = page+u;
	});
	$("table.filter input:button").click(function() {
		$("select[name=resstatusid]").val("X");
		$("select[name=deptid]").val("X");
		$("select[name=userid]").val("X");
		$("select[name=usertype]").val("BOTH");
		$("select[name=displayorder]").trigger("change");
	});
	$("table.container, table.container td").css("border","0px solid #ffffff");
	$("table.list, table.list td").css("border","1px solid #ababab");
	<?php 
	if($paging['page']==1) {
		echo "
		$('#beginning, #previous').prop('disabled','disabled');
		";
	}
	if($paging['page']==$paging['pages']) {
		echo "
		$('#end, #next').prop('disabled','disabled');
		";
	}
	?>
	$("#spn_paging input:button").click(function() {
		var s = $(this).attr("start");
		var u = getFilterSettings();
		u+="&s="+s;
		document.location.href = page+u;
	});
	$("#spn_paging select").change(function() {
		var s = ($(this).val() * rows)-rows;
		var u = getFilterSettings();
		u+="&s="+s;
		document.location.href = page+u;
	});
	$("input:button.view_details").click(function() {
		var i = "&id="+$(this).prop("id");
		var u = getFilterSettings();
		var url = page_details+i+u+"&s="+start+"&src="+page;
		document.location.href = url;
	});
});
</script>