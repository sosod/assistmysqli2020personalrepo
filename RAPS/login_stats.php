<?php
/**
 * from main/stats.php
 * @var ASSIST_DB $sdb
 */
$now = strtotime(date("d F Y"));
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));
$sql = "SELECT t.resdeadline, count(t.resid) as c FROM assist_".$cmpcode."_".$modref."_res t
WHERE resdeadline < ".$future." AND resstatusid NOT IN ('CL','CN') 
AND (restkid = '$tkid' OR resactionowner = '$tkid') 
GROUP BY resdeadline";
$rs = $sdb->mysql_fetch_all($sql);
foreach($rs as $row) {
	$d = $row['resdeadline'];
	if($d < $now) {
		$count['past']+=$row['c'];
	} elseif($d==$now) {
		$count['present']+=$row['c'];
	} else {
		$count['future']+=$row['c'];
	}
}
//mysql_close();
?>