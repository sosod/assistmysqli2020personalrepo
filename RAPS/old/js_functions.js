function createXMLHttpRequestObject(){
				var xmlHttp;
				try{
					xmlHttp = new XMLHttpRequest();
				}catch(e){
					var xmlHttpVersions = new Array('MSXML2.XMLHTTP.6.0','MSXML2.XMLHTTP.5.0',
												'MSXML2.XMLHTTP.4.0','MSXML2.XMLHTTP.3.0','MSXML2.XMLHTTP','Microsoft.XMLHTTP');
					for(var i = 0; i < xmlHttpVersions.length && !xmlHttp; i++){
						try{
							xmlHttp = new ActiveXObject(xmlHttpVersions[i]);
						}catch(e){}
					}
				}
				if(!xmlHttp){
					alert("Error creating the xmlHttpRequestObject");
				}else{
					return xmlHttp;
				}
			}

			var xmlHttp = createXMLHttpRequestObject();
			function process(){                            
                            var url = "dept_portfolios.php";
                            var modulemenu = document.getElementById('restkidsel');
                            var strVal = modulemenu.options[modulemenu.selectedIndex].value;
                            var persArr = strVal.split("_");
                            var person = persArr[0];
                            var dept = persArr[1];
                            var params = "person="+person+"&dept="+dept;
				if(xmlHttp){
					try{
						xmlHttp.open("POST",url, true);
                                                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                xmlHttp.setRequestHeader("Content-length", params.length);
                                                xmlHttp.setRequestHeader("Connection", "close");
						xmlHttp.onreadystatechange = handleRequestStateChange;
						xmlHttp.send(params);
					}catch(e){
						alert("Can't connect to server:"+ e.toString());
					}
				}
			}
			function handleRequestStateChange(){
				if (xmlHttp.readyState == 4){
					if (xmlHttp.status == 200){
						try{
							handleServerResponse();
						}catch(e){
							alert("Error reading the response:"+e.toString());
						}
					}else {
						alert("There was a problem retrieving the data:"+toString());
					}
				}
			}
			function handleServerResponse(){
				var xmlResponse = xmlHttp.responseXML;
				if (!xmlResponse || !xmlResponse.documentElement)
					throw("Invalid XML structure:\n" + xmlHttp.responseText);
				var rootNodeName = xmlResponse.documentElement.nodeName;
				if (rootNodeName == "parsererror")
					throw("Invalid XML structure");
				xmlRoot = xmlResponse.documentElement;
				var options = xmlRoot.getElementsByTagName("option");
				var html = '';
                                document.newres.resportfolioid.options.length = 0;
                                 document.newres.resportfolioid.options[0] = new Option('Please select a person above first','X',true,true);
                                var j=1;
				for(var i=0;i<options.length;i++){
					var node = options.item(i);
					//html += '<option value='+node.getAttribute('value')+'>'+node.firstChild.data+'</option>';
                                        document.newres.resportfolioid.options[j] = new Option(node.firstChild.data,node.getAttribute('value'),false,false);
                                        j++;
				}
				
				//var target = document.getElementById('resportfolioid');
                               // alert(target.id);
				//target.innerHTML = html;
                               // alert(target.innerHTML);
                                
			}                        
