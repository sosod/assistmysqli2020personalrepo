<?php
include("inc_ignite.php");
include("inc_raps.php");


//GET TASKS DETAILS
$resid = $_GET['i'];
$sql = "SELECT * FROM assist_".$cmpcode."_raps_res WHERE resid = ".$resid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>

        <script type ="text/javascript">
            $(document).ready(function(){
                $('#attachlinkWS').click(function(){
                    $('<tr><td align="right" colspan="3" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdocWS');
                });
            });
        </script>
        <script type="text/javascript">
            function editRes(resid){
                document.location.href = "edit.php?resid="+resid;
            }
            function download(path){
                document.location.href = "download.php?path="+path;
            }
        </script>
    </head>
    <script language=JavaScript>
        function statusPerc() {
            var stat = document.taskupdate.logstatusid.value;
            if(stat != "IP")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
            else
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;

            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "IP" && isNaN(parseInt(logstate)))
                {
                    alert("Please complete all the fields.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
    </script>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Resolutions Assist: Update Resolution</b></h1>
        <table border="1" cellspacing="0" cellpadding="4" width=500>
            <tr>
                <td class=tdgeneral valign="top" width=150><b>Resolution Reference:</b></td>
                <td class=tdgeneral valign="top" width=350><?php
                    echo($task['resref']);
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top" width=150><b>Assigned to:</b></td>
                <td class=tdgeneral valign="top" width=350><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['restkid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['tkname']." ".$row['tksurname']);
                    mysql_close();
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Assigned by:</b></td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['resadduser']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['tkname']." ".$row['tksurname']);
                    mysql_close();
                    ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Assigned on:</b></td>
                <td class=tdgeneral valign="top"><?php echo(date("d F Y",$task['resadddate'])); ?></td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Porfolio:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT p.value portf, d.value dept FROM assist_".$cmpcode."_raps_list_portfolio p, assist_".$cmpcode."_sdbip_dept d WHERE p.deptid = d.id AND p.id = '".$task['resportfolioid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['dept']." - ".$row['portf']);
                    mysql_close();
                    ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Priority:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_urgency WHERE id = '".$task['resurgencyid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    mysql_close();
                    ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Status:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_status WHERE id = '".$task['resstatusid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    if($task['resstatusid'] == "IP") {
                        echo(" (".$task['resstate']."%) ");
                    }
                    else {
                        if($task['resstatusid'] != "ON") {
                            echo(" (".$row['state']."%) ");
                        }
                    }
                    mysql_close();
                    ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Council Meeting Date:</td>
                <td class=tdgeneral valign="top"><?php echo(date("d F Y",$task['resmeetingdate'])); ?>&nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Deadline:</td>
                <td class=tdgeneral valign="top"><?php echo(date("d F Y",$task['resdeadline'])); ?>&nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Instructions:</td>
                <td class=tdgeneral valign="top">
<?php
$action = explode(chr(10),$task['resaction']);
$taskaction = implode("<br>",$action);
                    echo($taskaction);
                    ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class=tdgeneral valign="top"><b>Additional Notes:</b></td>
                <td class=tdgeneral valign="top">
<?php
$deliver = explode(chr(10),$task['resnotes']);
$taskdeliver = implode("<br>",$deliver);
                    echo($taskdeliver);
                    ?>
                    &nbsp;</td>
            </tr>
                    <?php
        $sql = "SELECT * FROM  assist_".$cmpcode."_raps_attachments AS res WHERE res.resid = ".$resid;
        include("inc_db_con.php");
            if(mysql_num_rows($rs) > 0) {
                echo ("<tr><td  valign='top'><b>Attachment(s):</b></td><td><table style='border:none;'>");
                while($row = mysql_fetch_assoc($rs)) {
                    $filename = "../files/$cmpcode/RAPS/".$row['system_filename'];
                    if(!file_exists($filename))
                        continue;
                    // $resattach_id = $row['id'];
                    echo ("<tr><td  style='border:none;'><a href='javascript:download(\"$filename\")' >");
                    echo($row['original_filename']."<br />");
                    echo ("</a></td>");
                }
                echo ("</table></td></tr>");
            }
            ?>
            <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs)) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$resid." AND udfindex = ".$row['udfiid'];
                include("inc_db_con2.php");
                $udf = mysql_num_rows($rs2);
                $row2 = mysql_fetch_array($rs2);
                mysql_close($rs2);
                ?>
            <tr>
                <td class=tdgeneral valign="top"><b><?php echo($row['udfivalue']); ?>:</b><br>
                    &nbsp;</td>
                <td class=tdgeneral valign="top" colspan=2>
    <?php
    if($udf>0) {
        switch($row['udfilist']) {
            case "Y":
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    include("inc_db_con2.php");
                                    $row3 = mysql_fetch_array($rs2);
                                    echo($row3['udfvvalue']);
                                    mysql_close($rs2);
                                    break;
                                default:
                                    echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                    break;
                            }
                        }
                        ?>
                    &nbsp;</td>
            </tr>
                        <?php
                    }
                    mysql_close();
                    ?>

<?php if($task['resadduser'] == $tkid) {?>
            <tr>
                <td colspan="2"><input type="button" value="Edit" onclick="editRes(<?php echo($task['resid']);?>)"/></td>
            </tr>
                <?php }?>

        </table>

        <h2 class=fc>Resolution Updates</h2>
        <form name=taskupdate method=post action=view_res_update_process.php onsubmit="return Validate(this);" language='jscript' enctype="multipart/form-data">
            <input type=hidden name=resid value=<?php echo($resid);?>>
            <table border="1" id="table1" cellspacing="0" cellpadding="5" width=500>
                <tr>
                    <td valign="top" class=tdheader>Date</td>
                    <td valign="top" class=tdheader>Resolution Update</td>
                    <td valign="top" class=tdheader>Status</td>
                </tr>
<?php
//IF THE TASK ISN'T CLOSED AND THE TASK TKID = CURRENT USER THEN DISPLAY THE UPDATE BOX
//$task['resstatusid'] != "CL" && $task['restkid'] == $tkid
if($task['resstatusid'] != "CL" && $task['restkid'] == $tkid) {
    ?>
                <tr>
                    <td valign="top" class=tdgeneral><?php echo(date("d-M-Y",$today)); ?></td>
                    <td valign="top" class=tdgeneral><textarea rows="5" name="logupdate" cols="40"></textarea></td>
                    <td valign="top" class=tdgeneral><select size="1" name="logstatusid" onchange=statusPerc()>
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_status WHERE yn = 'Y' AND state > -100 ORDER BY sort";
                    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs)) {
        if($task['resstatusid'] != "ON") {
            if($row['id'] == "IP") {
                echo("<option selected value=".$row['id'].">".$row['value']."</option>");
                                        }
                                        else {
                                            if($row['id'] != "NW") {
                                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                                            }
                                        }
                                    }
                                    else {
                                        if($row['id'] == "ON") {
                                            echo("<option selected value=".$row['id'].">".$row['value']."</option>");
                                        }
                                        else {
                                            if($row['id'] != "NW") {
                                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                                            }
                                        }
                                    }
                                }
                                ?>
                        </select><br>&nbsp;<br>
                        <input type="text" name="logstate" size="4" value=<?php echo($task['resstate']); ?>>% complete</td>
                </tr>
                <tr id="firstdocWS">
                    <td class=tdgeneral colspan="3" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="3"><a href="javascript:void(0)" id="attachlinkWS">Attach another file</a></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3" class=tdgeneral>
                        <input type="submit" value="Submit" name="B1">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
                                <?php
                            }
                            else {
                                echo("<input type=hidden name=logstatusid value=CL>");
                            }

//DISPLAY THE UPDATES ON THE CURRENT TASK
                            $sql = "SELECT * FROM assist_".$cmpcode."_raps_log l, assist_".$cmpcode."_raps_list_status s WHERE l.logstatusid = s.id AND l.logresid = ".$resid." ORDER BY logid DESC";
include("inc_db_con.php");
$l = mysql_num_rows($rs);
$displaycount = 0;
if($l == 0) {
    $displaycount++;
    ?>
                <tr>
                    <td valign="top" class=tdgeneral colspan=3>There are no updates to display.&nbsp;</td>
                </tr>
                    <?php
                }
                else {
                    $l2 = 0;
                    while($row = mysql_fetch_array($rs)) {
                        $l2++;
                        if($l > $l2) {
                            $displaycount++;
                            ?>
                <tr>
                    <td valign="top" class=tdgeneral><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
                            $logarr = explode(chr(10),$row['logupdate']);
                            $logupdate = implode("<br>",$logarr);
                            $logupdate = html_entity_decode($logupdate);
                            $sql = "SELECT * FROM assist_".$cmpcode."_raps_attachments WHERE logid=".$row['logid'];
                                $rs2 = mysql_query($sql);
                                $attachments = "";
                                while($row3 = mysql_fetch_array($rs2)) {
                                    $file = "../files/$cmpcode/RAPS/".$row3['system_filename'];
                                    if(!file_exists($file))
                                        continue;
                                    $attachments .= "<a href='javascript:download(\"".$file."\")'>".$row3['original_filename']."</a><br />";
                                }
                                echo($logupdate);
                                if(!empty($attachments))
                                    echo "<br />Attachments:<br />".$attachments;
            ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
            echo($row['value']);
                            if($row['logstatusid'] == "IP") {
                                echo("<br>(".$row['logstate']."%)");
                            }
                            ?>&nbsp;</td>
                </tr>
                            <?php
                        }
                    }
                }
                if($displaycount==0) {
                    ?>
                <tr>
                    <td valign="top" class=tdgeneral colspan=3>There are no updates to display.&nbsp;</td>
                </tr>
    <?php
                        }

                        ?>
            </table>

        </form>
        <script language=JavaScript>
            var statu = document.taskupdate.logstatusid.value;
            if(statu == "ON")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        </script>
                <?php
//}
                ?>
    </body>

</html>
