<?php
include("inc_ignite.php");
include("inc_raps.php");

//GET STATUS ID
$sid = $_GET['s'];
$rt = $_GET['t'];
//GET STATUS TEXT
    if($sid == "ALL")
    {
        $statusheading = "All incomplete resolutions";
        $status = 0;
        $statsql = "SELECT resaction, resid, resref, l.value portval, s.value statval, resdeadline, d.value dept, restkid  ";
        $statsql.= "FROM assist_".$cmpcode."_raps_res t, assist_".$cmpcode."_raps_list_portfolio l, assist_".$cmpcode."_raps_list_status s, assist_".$cmpcode."_sdbip_dept d ";
        $statsql.= "WHERE t.resstatusid = s.id ";
        if($rt=="my")
        {
            $statsql.= "AND t.restkid = '".$tkid."' ";
        }
        $statsql.= "AND t.resstatusid <> 'CL' AND t.resstatusid <> 'CN' ";
        $statsql.= "AND t.resportfolioid = l.id ";
        $statsql.= "AND d.id = l.deptid";
    }
    else
    {
        $sql = "SELECT value, heading FROM assist_".$cmpcode."_raps_list_status WHERE id = '".$sid."'";
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $statusheading = $row['heading'];
            $status = $row['value'];
        mysql_close();
        $statsql = "SELECT resaction, resid, resref, l.value portval, s.value statval, resdeadline, d.value dept, restkid ";
        $statsql.= "FROM assist_".$cmpcode."_raps_res t, assist_".$cmpcode."_raps_list_portfolio l, assist_".$cmpcode."_raps_list_status s, assist_".$cmpcode."_sdbip_dept d ";
        $statsql.= "WHERE t.resstatusid = s.id ";
        if($rt=="my")
        {
            $statsql.= "AND t.restkid = '".$tkid."' ";
        }
        $statsql.= "AND t.resstatusid = '".$sid."' ";
        $statsql.= "AND t.resportfolioid = l.id ";
        $statsql.= "AND d.id = l.deptid";
/*        $statsql = "SELECT taskaction, taskid, l.value topval, s.value statval, taskdeadline ";
        $statsql.= "FROM assist_".$cmpcode."_ta_task t, assist_".$cmpcode."_ta_list_topic l, assist_".$cmpcode."_ta_list_status s ";
        $statsql.= "WHERE t.taskstatusid = s.id ";
        if($t=="my")
        {
            $statsql.= "AND t.tasktkid = '".$tkid."' ";
        }
        $statsql.= "AND t.taskstatusid = '".$sid."' ";
        $statsql.= "AND t.tasktopicid = l.id";
*/    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function viewTask(id) {
    if(id > 0)
    {
        document.location.href="view_res_update.php?i="+id;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Resolutions Assist: View Resolutions - <?php echo($statusheading); ?></b></h1>
<?php
//GET LIST OF TASKS THAT ARE WITHIN STATUS
$sql = $statsql;
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0)
{
?>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdheader valign="top">Ref</td>
		<?php if($t!="my") { echo("<td class=tdheader valign=top>Assigned to</td>"); } ?>
		<td class=tdheader valign="top">Portfolio</td>
		<td class=tdheader valign="top">Instruction</td>
		<td class=tdheader valign="top">Status</td>
		<td class=tdheader valign="top">Deadline</td>
		<td class=tdheader valign="top">&nbsp;</td>
	</tr>
<?php
while($row = mysql_fetch_array($rs))
{
    $taskaction = $row['resaction'];
    $tactarr = explode(chr(10),$taskaction);
    $taskaction = implode("<Br>",$tactarr);
?>
	<tr>
		<td class=tdheader valign="top"><?php echo($row['resref']); ?>&nbsp;</td>
		<?php if($t!="my") {
            $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['restkid']."'";
            include("inc_db_con2.php");
                $row2 = mysql_fetch_array($rs2);
            mysql_close($con2);
            echo("<td class=tdgeneral valign=top>".$row2['tkname']." ".$row2['tksurname']."</td>");
        } ?>
		<td class=tdgeneral valign="top"><?php echo($row['dept']." - ".$row['portval']); ?>&nbsp;</td>
		<td class=tdgeneral valign="top" width=250><?php echo($taskaction); ?>&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($row['statval']); ?>&nbsp;</td>
		<td class=tdgeneral valign="top"><?php
            $tdeadline = $row['resdeadline'];
            if($tdeadline < $today && $sid != 'CL')
            {
                echo("<b><font class=fc>".date("d-M-Y",$row['resdeadline'])."</font></b>");
            }
            else
            {
                echo(date("d-M-Y",$row['resdeadline']));
            }
             ?>&nbsp;</td>
		<td class=tdheader valign="top">&nbsp;<input type="button" value="View" name="B4" onclick="viewTask(<?php echo($row['resid']); ?>)">&nbsp;</td>
	</tr>
<?php
}
}//IF MYSQL_NUM_ROWS > 0
else
{
    echo("<p>There are currently no tasks to view.</p>");
}
mysql_close();
?>
</table>

<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=# onclick="document.location.href = 'view.php';" class=grey>Go back</a></p>
</body>

</html>
