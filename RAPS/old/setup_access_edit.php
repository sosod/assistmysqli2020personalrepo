<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

include("inc_ta.php");
//GET EDIT VARIABLES
$result = "";
$aid = $_GET['id'];
$type = $_GET['type'];
$deptid = $_GET['deptid'];
$v = $_GET['v'];
$r = $_GET['r'];
$a = $_GET['a'];
$e = $_GET['e'];
$m = $_GET['m'];

if($type == "edit")    //IF EDIT TYPE = EDIT
{
    //GET TRANSACTION LOG INFO
    $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_users WHERE id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    //PERFORM EDIT UPDATE
    $sql = "UPDATE assist_".$cmpcode."_raps_list_users SET v = '".$v."', r = '".$r."', a = '".$a."', e = '".$e."', m = '".$m."', deptid = ".$deptid." WHERE id = ".$aid;
    include("inc_db_con.php");
    //SET TRANSACTION LOG VALUES
    $tref = "RAPS";
    $tsql = $sql;
    $told = "UPDATE assist_".$cmpcode."_raps_list_users SET v = '".$row['v']."', r = '".$row['r']."', a = '".$row['a']."', e = '".$row['e']."', m = '".$row['m']."', deptid = ".$row['deptid']." WHERE id = ".$aid;
    $trans = "Updated user ".$aid;
    //PERFORM TRANSACTION LOG UPDATE
    include("inc_transaction_log.php");
    $result = "done";
}
else
{
    if($type == "d")    //IF EDIT TYPE = DELETE
    {
        //UPDATE LIST-ACCESS AND SET USER TO N
        $sql = "UPDATE assist_".$cmpcode."_raps_list_users SET yn = 'N' WHERE id = ".$aid;
        include("inc_db_con.php");
            //Set transaction log sql value for delete
            $tref = "RAPS";
            $tsql = $sql;
            $told = "UPDATE assist_".$cmpcode."_raps_list_users SET yn = 'Y' WHERE id = ".$aid;;
            $trans = "Removed user access for ".$aid.".";
            include("inc_transaction_log.php");
        $result = "done";
    }
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delUser(id,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to delete access to Resolutions Assist for "+tkn+"."))
    {
        document.location.href = "setup_access_edit.php?id="+id+"&type=d";
    }
}

function Validate(me) {
    var access = me.access.value;
    var deptid = me.deptid.value;
    if(access != "X" && access.length > 0)
    {
        if(deptid != "X" && deptid.length > 0)
        {
            return true;
        }
        else
        {
            alert("Please indicate to which department this user belongs.");
        }
    }
    else
    {
        alert("Please indicate the level of access for the user.");
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Resolution Assist: Setup - User Access</b></h1>
<?php
//echo($transaction[1]);
//print_r($taskid);
if($result != "done")   //IF NO ACTION WAS TAKEN THEN DISPLAY EDIT FORM
{
?>
<form name=edit method=get action=setup_access_edit.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=type value=edit><input type=hidden name=id value=<?php echo($aid);?>>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader" rowspan=2>ID</td>
		<td class="tdheader" rowspan=2>User</td>
		<td class="tdheader" rowspan=2>Department</td>
		<td class="tdheader" colspan=5>Access</td>
	</tr>
	<tr>
		<td class="tdheader" width=60>View All</td>
		<td class="tdheader" width=60>Receive</td>
		<td class="tdheader" width=60>Add</td>
		<td class="tdheader" width=60>Edit</td>
		<td class="tdheader" width=60>Receive Email<br>Updates</td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT u.*, d.value dept, t.tkname, t.tksurname ";
    $sql .= "FROM assist_".$cmpcode."_raps_list_users u, ";
    if($deptsrc=="Assist")
    {
        $sql.= "assist_".$cmpcode."_list_dept d";
    }
    else
    {
        $sql.= "assist_".$cmpcode."_sdbip_dept d";
    }
    $sql .= ", assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE u.yn = 'Y' AND t.tkid = u.tkid AND d.id = u.deptid AND u.id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $tkn = $row['tkname']." ".$row['tksurname'];
    mysql_close();
        switch ($row['v'])
        {
            case "Y":
                $v = "<select name=v><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $v = "<select name=v><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $v = "<select name=v><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['r'])
        {
            case "Y":
                $r = "<select name=r><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $r = "<select name=r><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $r = "<select name=r><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['a'])
        {
            case "Y":
                $a = "<select name=a><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $a = "<select name=a><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $a = "<select name=a><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['e'])
        {
            case "Y":
                $e = "<select name=e><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $e = "<select name=e><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $e = "<select name=e><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['m'])
        {
            case "Y":
                $m = "<select name=m><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $m = "<select name=m><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $m = "<select name=m><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }

?>
        <tr class="tdgeneral" valign=top>
            <td><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral"><select name=deptid><option value=X>--- SELECT ---</option>
            <?php
            if($deptsrc=="Assist")
            {
                $sql = "SELECT * FROM assist_".$cmpcode."_list_dept WHERE yn = 'Y' ORDER BY value";
            }
            else
            {
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
            }
            include("inc_db_con.php");
            $d = 0;
                while($row2 = mysql_fetch_array($rs))
                {
                    $id = $row2['id'];
                    $val = $row2['value'];
                    if($row['deptid'] == $id)
                    {
                        echo("<option selected value=".$id.">".$val."</option>");
                        $d=1;
                    }
                    else
                    {
                        echo("<option value=".$id.">".$val."</option>");
                    }
                }
            mysql_close();
            ?>
        </select>
        <?php if($d==0){echo("<script language=JavaScript>document.edit.deptid.value='X';</script>");}?></td>
            <td class="tdgeneral" align=center><?php echo($v); ?></td>
            <td class="tdgeneral" align=center><?php echo($r); ?></td>
            <td class="tdgeneral" align=center><?php echo($a); ?></td>
            <td class="tdgeneral" align=center><?php echo($e); ?></td>
            <td class="tdgeneral" align=center><?php echo($m); ?></td>
        </tr>
<?php
    mysql_close();
?>

        <tr>
            <td colspan=8 class="tdgeneral"><input type=submit value=" Edit "> <input type=button value=Delete onclick="delUser(<?php echo($aid) ;?>,'<?php echo(str_replace(" ","_",$tkn)) ;?>')"> <input type=reset></td>
        </tr>
</table>
</form>
<?php
}
?>
<form name=result>
<input type=hidden name=res value=<?php echo($result); ?>>
</form>
<script language=JavaScript>
var res = document.result.res.value;
//IF ACTION WAS TAKEN (result = done) THEN REDIRECT
if(res == "done")
{
    document.location.href="setup_access.php";
}
</script>
</body>

</html>
