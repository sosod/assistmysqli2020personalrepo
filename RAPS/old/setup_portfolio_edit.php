<?php
include("inc_ignite.php");
include("inc_raps.php");
//GET TOPIC ID
$pid = $_GET['i'];

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate() {
    var newval = document.upd.txt.value;
    var oldval = document.upd.oldval.value;
    var newdept = document.upd.deptid.value;
    var olddept = document.upd.olddept.value;

    //alert(":"+newval+"-"+oldval+":|:"+newdept+"-"+olddept+":");

    if(newval == oldval && newdept == olddept)
    {
        alert("The updated portfolio is the same as the old portfolio.\n\nPlease enter an updated portfolio.");
        return false;
    }
    else
    {
        if(newval.length > 0 && newdept != "X")
        {
            return true;
        }
        else
        {
            if(newval.length == 0)
            {
                alert("You've entered a blank portfolio.\n\nIf you wish to delete this portfolio, please click the 'Delete' button\nelse please enter an updated portfolio.");
            }
            else
            {
                if(newdept == "X")
                {
                    alert("You've not selected a department for this portfolio.\n\nIf you wish to delete this portfolio, please click the 'Delete' button\nelse please select a department for this portfolio.");
                }
            }
        }
    }
    return false;
}

function delTop(id) {
    if(confirm("Are you sure you want to delete this topic?"))
    {
        document.location.href = "setup_portfolio_edit_delete.php?i="+id;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Resolutions Assist: Setup - Update Portfolios</b></h1>
<p>&nbsp;</p>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader >Original</td>
        <td class=tdheader >Updated</td>
    </tr>
<form name=upd method=post action=setup_portfolio_edit_update.php onsubmit="return Validate();" language=jscript>
<?php
//GET TOPIC DETAILS AND DISPLAY
$sql = "SELECT p.value port, d.value dept, p.id, p.deptid FROM assist_".$cmpcode."_raps_list_portfolio p, ";
if($deptsrc=="Assist")
{
    $sql.= "assist_".$cmpcode."_list_dept d ";
}
else
{
    $sql.= "assist_".$cmpcode."_sdbip_dept d ";
}
$sql.= " WHERE p.id = ".$pid." AND p.yn = 'Y' AND d.id = p.deptid ORDER BY d.sort, p.value";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$deptid = $row['deptid'];
mysql_close();
?>
    <tr>
        <td class=tdgeneral><?php echo($row['dept']); ?><input type=hidden name=olddept value="<?php echo($row['deptid']); ?>"> - <?php echo($row['port']); ?>&nbsp;&nbsp;<input type=hidden name=oldval value="<?php echo($row['port']); ?>"><input type=hidden name=id value=<?php echo($pid); ?>></td>
        <td class=tdgeneral><select name=deptid><option value=X>--- SELECT ---</option>
<?php
if($deptsrc=="Assist")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_list_dept WHERE yn = 'Y' ORDER BY value";
}
else
{
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
}
include("inc_db_con.php");
$d = 0;
    while($rowd = mysql_fetch_array($rs))
    {
        $id = $rowd['id'];
        $val = $rowd['value'];
        if($id == $deptid)
        {
            echo("<option selected value=".$id.">".$val."</option>");
            $d = 1;
        }
        else
        {
            echo("<option value=".$id.">".$val."</option>");
        }
    }
mysql_close();
?>
        </select> - <input type=text name=txt maxlength=45 size=30 value="<?php echo($row['port']); ?>"></td>
<?php
if($d==0)
{
    echo("<script language=JavaScript>document.upd.deptid.value='X';</script>");
}
?>
    </tr>
    <tr>
        <td class=tdgeneral colspan=4><input type=submit value=Update> <input type=button value=Delete onclick="delTop(<?php echo($pid);?>)"></td>
    </tr>
</table>
</form>
</body>

</html>
