<?php include("inc_ignite.php");
?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
    </head>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Resolutions Assist: New Resolution</b></h1>
        <?php
//echo '<pre>';
//print_r($_POST);
//GET DATA FROM FORM
//$restkid = $_POST['restkid']; //Assigned to
        $tkidDeptArr = explode("_",$_POST['restkidsel']);
        $restkid = $tkidDeptArr[0];

        $resadduser = $_POST['resadduser']; //Assigned by
        $resportfolioid = $_POST['resportfolioid']; //Portfolio
        $resurgencyid = $_POST['resurgencyid']; //Priority
        $resstatusid = $_POST['resstatusid']; //Status - New or ongoing
        /*$cday = $_POST['cday']; //Council meeting date
$cmon = $_POST['cmon'];
$cyear = $_POST['cyear'];
$dday = $_POST['dday']; //Deadline
$dmon = $_POST['dmon'];
$dyear = $_POST['dyear'];*/
        $resaction = $_POST['resaction']; //Resolution Instructions
        $resnotes = $_POST['resnotes']; //Additional notes
        $resadddate = $today;
        $resref = $_POST['resref'];

//SET DATES
        $resmeetingdate = strtotime($_POST['resmeetingdate']);//mktime(12,0,0,$cmon,$cday,$cyear);
        $resdeadline = strtotime($_POST['resdeadline']);//mktime(23,59,59,$dmon,$dday,$dyear);
//REMOVE ' & #
        $resaction = str_replace("#","&#35",$resaction);
        $resnotes = str_replace("#","&#35",$resnotes);
        $resaction = str_replace("'","&#39",$resaction);
        $resnotes = str_replace("'","&#39",$resnotes);
        $resref = htmlentities($resref,ENT_QUOTES,"ISO-8859-1");

//GET STATE FOR GIVEN STATUS
        $sql = "SELECT state FROM assist_".$cmpcode."_raps_list_status WHERE id = '".$resstatusid."'";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $resstate = $row['state'];
        mysql_close();

//ADD RECORD TO RES
        $sql = "INSERT INTO assist_".$cmpcode."_raps_res SET ";
        $sql .= "resref = '".$resref."', ";
        $sql .= "restkid = '".$restkid."', ";
        $sql .= "resurgencyid = ".$resurgencyid.", ";
        $sql .= "resportfolioid = ".$resportfolioid.", ";
        $sql .= "resaction = '".$resaction."', ";
        $sql .= "resnotes = '".$resnotes."', ";
        $sql .= "resstatusid = '".$resstatusid."', ";
        $sql .= "resstate = ".$resstate.", ";
        $sql .= "resdeadline = ".$resdeadline.", ";
        $sql .= "resmeetingdate = ".$resmeetingdate.", ";
        $sql .= "resadddate = ".$resadddate.", ";
        $sql .= "resadduser = '".$resadduser."'";
        include("inc_db_con.php");
        $resid = mysql_insert_id($con);
        //LOG
        $tsql = $sql;
        $trans = "Added new resolution ".$resid;
        include("inc_transaction_log.php");
        if(strlen($resref)==0 && $resid > 0) {
            $sql = "UPDATE assist_".$cmpcode."_raps_res SET resref = '".$resid."' WHERE resid = ".$resid;
            include("inc_db_con.php");
        }

        //uploads
        $original_filename = "";
        $system_filename = "";
        $attachments = "";
        if(isset($_FILES)) {
            $folder = "../files/$cmpcode/RAPS";
            /*if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
            checkFolder("RAPS");
            foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                if($_FILES['attachments']['error'][$key] == 0) {
                    $original_filename = $_FILES['attachments']['name'][$key];
                    $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                    $parts = explode(".", $original_filename);
                    $file = $parts[0];
                    $system_filename = $resid."_".$file."_".date("YmdHi").".$ext";
                    $full_path = $folder."/".$system_filename;
                    move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                    $sql = "INSERT INTO assist_".$cmpcode."_raps_attachments (resid,logid,original_filename,system_filename) VALUES ($resid,0,'$original_filename','$system_filename')";
                    include("inc_db_con.php");
                    $filename = "../files/$cmpcode/RAPS/".$system_filename;
                    $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
                }
            }
        }
        
//ADD RECORD TO LOG
        $logupdate = "New resolution added.".chr(10)."Instructions: ".$resaction;
        $sql = "INSERT INTO assist_".$cmpcode."_raps_log SET ";
        $sql .= "logdate = '".$today."', ";
        $sql .= "logtkid = '".$restkid."', ";
        $sql .= "logupdate = '".$logupdate."', ";
        $sql .= "logstatusid = '".$resstatusid."', ";
        $sql .= "logstate = ".$resstate.", ";
        $sql .= "logresid = '".$resid."', ";
        $sql .= "logdeadline = '".$resdeadline."'";
        include("inc_db_con.php");
        $logid = mysql_insert_id($con);
//LOG
        $trans = "Added Resolution log record ".$logid.".";
        $tsql = $sql;
        include("inc_transaction_log.php");


//NOTIFY TASK-TK
        if($tasktkid <> $tkid) {
            //Get tasktkid details
            $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$restkid."'";
            include("inc_db_con.php");
            $rowtkid = mysql_fetch_array($rs);
            mysql_close();
            //Get taskadduser details
            $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$resadduser."'";
            include("inc_db_con.php");
            $rowadduser = mysql_fetch_array($rs);
            mysql_close();
            //Get topic details
//    $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_portfolio p WHERE id = ".$resportfolioid;
            $sql = "SELECT CONCAT_WS(' - ',d.value,p.value) AS value FROM assist_".$cmpcode."_raps_list_portfolio p, assist_".$cmpcode."_sdbip_dept d WHERE p.deptid = d.id AND p.id = ".$resportfolioid;
            include("inc_db_con.php");
            $rowtopic = mysql_fetch_array($rs);
            mysql_close();
            $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_urgency WHERE id = ".$resurgencyid;
            include("inc_db_con.php");
            $rowurg = mysql_fetch_array($rs);
            mysql_close();
            //Set email variables
            $subject = "";
            $message = "";
            $to = $rowtkid['tkemail'];
            $from = $rowadduser['tkemail'];
            $subject = "New Resolution on Ignite Assist";
            $message = "<font face=arial>";
            $message .= "<p><small><i>".$rowadduser['tkname']." ".$rowadduser['tksurname']." has assigned a new resolution to you:</i><br>";
            $message .= "<b>Portfolio:</b> ".$rowtopic['value']."<br>";
            $message .= "<b>Council Meeting Date:</b> ".date("d F Y",$resmeetingdate)."<br>";
            $message .= "<b>Deadline:</b> ".date("d F Y",$resdeadline)."<br>";
            $message .= "<b>Priority:</b> ".$rowurg['value']."<br>";
            $message .= "<b>Instructions:</b><br>".str_replace(chr(10),"<br>",$resaction)."<br>";
            $message .= "<b>Additional Notes:</b><br>".str_replace(chr(10),"<br>",$resnotes)."<br>";
            $message .= "<p><i>Please log onto Ignite Assist in order to update this resolution.</i></p>";
            $message .= "</small></font>";
            //Send email
            $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
            mail($to,$subject,$message,$header);
            //Update transaction log
            $trans = "Sent email to ".$rowtkid['tkname']." ".$rowtkid['tksurname']." for new resolution ".$resid.".";
            $tsql = str_replace("'","|",$message);
            $tref = 'RAPS';
            include("inc_transaction_log.php");
            $header = "";
            $message = "";
        }

        $sql = "SELECT t.tkid, t.tkemail FROM assist_".$cmpcode."_raps_list_users u, assist_".$cmpcode."_timekeep t WHERE u.yn = 'Y' AND u.m = 'Y' AND t.tkemail <> '' AND t.tkid = u.tkid AND u.tkid <> '".$resadduser."' AND u.tkid <> '".$restkid."'";
        include("inc_db_con.php");
        if(mysql_num_rows($rs)>0) {
            $emerr = "N";
            $to = "";
            $cc = "";
            $to = $rowadduser['tkemail'];
            while($row = mysql_fetch_array($rs)) {
                $cc.="\r\nCC:".$row['tkemail'];
            }
            $from = $rowadduser['tkemail'];
            $subject = "New Resolution on Ignite Assist";
            $message = "<font face=arial>";
            $message .= "<p><small><i>".$rowadduser['tkname']." ".$rowadduser['tksurname']." has assigned a new resolution to ".$rowtkid['tkname']." ".$rowtkid['tksurname'].":</i><br>";
            $message .= "<b>Portfolio:</b> ".$rowtopic['value']."<br>";
            $message .= "<b>Council Meeting Date:</b> ".date("d F Y",$resmeetingdate)."<br>";
            $message .= "<b>Deadline:</b> ".date("d F Y",$resdeadline)."<br>";
            $message .= "<b>Priority:</b> ".$rowurg['value']."<br>";
            $message .= "<b>Instructions:</b><br>".str_replace(chr(10),"<br>",$resaction)."<br>";
            $message .= "<b>Additional Notes:</b><br>".str_replace(chr(10),"<br>",$resnotes)."<br>";
            $message .= "<p><i>Please log onto Ignite Assist in order to view updates to this resolution.</i></p>";
            $message .= "</small></font>";
            //Send email
            $header = "From: ".$from."\r\nReply-to: ".$from.$cc."\r\nContent-type: text/html; charset=us-ascii";
            mail($to,$subject,$message,$header);
            //Update transaction log
            $trans = "Sent email to ".$rowtkid['tkname']." ".$rowtkid['tksurname']." for new resolution ".$resid.".";
            $tsql = str_replace("'","|",$message);
            $tref = 'RAPS';
//        include("inc_transaction_log.php");
            $header = "";
            $message = "";
        }
        mysql_close();

//echo($cc);

//echo("<form name=nw><input type=hidden name=tsk value=Y></form>");
        echo("<p>Success!  The new resolution has been added.</p>");
        echo("<p>&nbsp;</p>");
        echo("<p><img src=/pics/tri_left.gif align=absmiddle > <a href=new.php class=grey>Go back</a></p>");
        ?>
    </body>

</html>
