<?php
include("inc_ignite.php");
include("inc_raps.php");
//echo '<pre>';
// print_r($_POST);die;
if(isset($_POST) && $_POST['B1'] == 'Submit') {
    $resstate=0;
    $resid = $_POST['resid'];
    $changes = "Resolution id $resid edited by <i>".getTK($tkid, $cmpcode, 'tkn')."</i>";
    //get old res record
    $sql = "SELECT * FROM assist_".$cmpcode."_raps_res WHERE resid=".$resid;
    include("inc_db_con.php");
    $oldRow = mysql_fetch_array($rs);

    $tkidDeptArr = explode("_",$_POST['restkidsel']);
    $restkid = $tkidDeptArr[0];

    $resadduser = $_POST['resadduser']; //Assigned by
    $resportfolioid = $_POST['resportfolioid']; //Portfolio
    $resurgencyid = $_POST['resurgencyid']; //Priority
    $resstatusid = $_POST['resstatusid']; //Status - New or ongoing
    $resaction = $_POST['resaction']; //Resolution Instructions
    $resnotes = $_POST['resnotes']; //Additional notes
    $resadddate = $today;
    $resref = $_POST['resref'];

    //SET DATES
    $resmeetingdate = strtotime($_POST['resmeetingdate']);//mktime(12,0,0,$cmon,$cday,$cyear);
    $resdeadline = strtotime($_POST['resdeadline']);//mktime(23,59,59,$dmon,$dday,$dyear);

//REMOVE ' & #
    $resaction = str_replace("#","&#35",$resaction);
    $resnotes = str_replace("#","&#35",$resnotes);
    $resaction = str_replace("'","&#39",$resaction);
    $resnotes = str_replace("'","&#39",$resnotes);
    $resref = htmlentities($resref,ENT_QUOTES,"ISO-8859-1");

    //GET STATE FOR GIVEN STATUS
    $sql = "SELECT state,id FROM assist_".$cmpcode."_raps_list_status WHERE id = '".$resstatusid."'";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $resstate = $row['state'];
    $statusStrId = $row['id'];

    if($resstatusid == 'IP') {
        $sql = "SELECT * FROM assist_".$cmpcode."_raps_log r WHERE logresid=".$resid." ORDER BY logdate DESC LIMIT 1";
        include("inc_db_con.php");
        $rowL0 = mysql_fetch_array($rs);
        if($rowL0['logstatusid'] != 'IP') {
            if($rowL0['logstatusid'] == 'ON' || $rowL0['logstatusid'] == 'CL') {
                $sql = "SELECT * FROM assist_".$cmpcode."_raps_log r WHERE logresid=".$resid." AND logstatusid='IP' ORDER BY logdate DESC LIMIT 1";
                include("inc_db_con.php");
                $rowL1 = mysql_fetch_array($rs);
                $num = mysql_num_rows($rs);
                if($num > 0) {
                    $resstate = $rowL1['logstate'];
                }else {
                    $resstate = 0;
                }
            }
        }else {
            $resstate = $oldRow['resstate'];
        }
    }
    mysql_close();
    //ADD RECORD TO RES
    $sql = "UPDATE  assist_".$cmpcode."_raps_res SET ";
    $sql .= "resref = '".$resref."', ";
    $sql .= "restkid = '".$restkid."', ";
    $sql .= "resurgencyid = ".$resurgencyid.", ";
    $sql .= "resportfolioid = ".$resportfolioid.", ";
    $sql .= "resaction = '".$resaction."', ";
    $sql .= "resnotes = '".$resnotes."', ";
    $sql .= "resstatusid = '".$resstatusid."', ";
    $sql .= "resstate = ".$resstate.", ";
    $sql .= "resdeadline = ".$resdeadline.", ";
    $sql .= "resmeetingdate = ".$resmeetingdate.", ";
    $sql .= "resadddate = ".$resadddate.", ";
    $sql .= "resadduser = '".$resadduser."' WHERE resid=".$resid;
    include("inc_db_con.php");

    //uploads
    $original_filename = "";
    $system_filename = "";
    $attachments = "";
    if(isset($_FILES)) {
        $folder = "../files/$cmpcode/RAPS";
        /*if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
        checkFolder("RAPS");
        foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
            if($_FILES['attachments']['error'][$key] == 0) {
                $original_filename = $_FILES['attachments']['name'][$key];
                $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                $parts = explode(".", $original_filename);
                $file = $parts[0];
                $system_filename = $resid."_".$file."_".date("YmdHi").".$ext";
                $full_path = $folder."/".$system_filename;
                move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                $sql = "INSERT INTO assist_".$cmpcode."_raps_attachments (resid,logid,original_filename,system_filename) VALUES ($resid,0,'$original_filename','$system_filename')";
                include("inc_db_con.php");
                $filename = "../files/$cmpcode/RAPS/".$system_filename;
                $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
            }
        }
    }
    //LOG
    if($resref != $oldRow['resref']) {
        $changes .= "<br />Resolution Reference changed from <i>".$oldRow['resref']."</i> to <i>$resref</i>";
    }
    if($restkid != $oldRow['restkid']) {
        $changes .= "<br />Assigned To field changed from <i>".getTK($oldRow['restkid'], $cmpcode, 'tkn')."</i> to <i>".getTK($restkid, $cmpcode, 'tkn')."</i>";
    }
    if($resportfolioid != $oldRow['resportfolioid']) {
        $sql = "SELECT d.value dept, p.value port FROM assist_".$cmpcode."_sdbip_dept d, assist_".$cmpcode."_raps_list_portfolio p
                WHERE d.id=p.deptid AND p.id=".$oldRow['resportfolioid'];
        include("inc_db_con.php");
        $rowO = mysql_fetch_array($rs);
        $sql = "SELECT d.value dept, p.value port FROM assist_".$cmpcode."_sdbip_dept d, assist_".$cmpcode."_raps_list_portfolio p
                WHERE d.id=p.deptid AND p.id=".$resportfolioid;
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $changes .= "<br />Portfolio changed from <i>".$rowO['dept']." - ".$rowO['port']."</i> to <i>".$row1['dept']." - ".$row1['port']."</i>";
    }
    if($resurgencyid != $oldRow['resurgencyid']) {
        $sql = "SELECT u.value FROM assist_".$cmpcode."_raps_list_urgency u WHERE u.id=".$oldRow['resurgencyid'];
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $sql = "SELECT u.value FROM assist_".$cmpcode."_raps_list_urgency u WHERE u.id=".$resurgencyid;
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $changes .= "<br />Priority change from <i>".$row['value']."</i> to <i>".$row1['value']."</i>";
    }
    if($resstatusid != $oldRow['resstatusid']) {
        $sql = "SELECT s.value FROM assist_".$cmpcode."_raps_list_status s WHERE s.id='".$oldRow['resstatusid']."'";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $sql = "SELECT s.value FROM assist_".$cmpcode."_raps_list_status s WHERE s.id='".$resstatusid."'";
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $changes .= "<br />Status changed from <i>".$row['value']."</i> to <i>".$row1['value']."</i>";
    }
    if($resmeetingdate != $oldRow['resmeetingdate']) {
        $changes .= "<br />Council Meeting date changed from <i>".date("d F Y",$oldRow['resmeetingdate'])."</i> to <i>".date("d F Y",$resmeetingdate)."</i>";
    }
    if($resdeadline != $oldRow['resdeadline']) {
        $changes .= "<br />Deadline date changed from <i>".date("d F Y",$oldRow['resdeadline'])."</i> to <i>".date("d F Y",$resdeadline)."</i>";
    }
    if($resaction != $oldRow['resaction']) {
        $changes .= "<br />Resolution instructions changed from <i>".$oldRow['resaction']."</i> to <i>".$resaction."</i>";
    }
    if($resnotes != $oldRow['resnotes']) {
        if($oldRow['resnotes'] == "" AND $resnotes != "")
            $changes .= "<br />New addtional notes:<i>$resnotes</i>";
        else if($oldRow['resnotes'] != "" AND $resnotes != "")
            $changes .= "<br />Additional notes changed from <i>".$oldRow['resnotes']."</i> to <i>".$resnotes."</i>";
        else if($resnotes == "" && $oldRow['resnotes'] != "")
            $changes .= "<br />Additional notes:<i>".$oldRow['resnotes']."</i> were removed from this resoltuion";
    }
    if($attachments != '') {
        $changes .= "<br />Attachments:<br />".$attachments;
    }
    $changes = addslashes(htmlentities($changes));
    $sql = "INSERT INTO assist_".$cmpcode."_raps_log (logdate,logtkid,logupdate,logstatusid,logstate,logresid,logdeadline) VALUES
                                                     ($today,$restkid,'$changes','$statusStrId',$resstate,$resid,'$resdeadline')";
    include("inc_db_con.php");


    echo "<script type='text/javascript'>document.location.href='view_res_update.php?i=$resid';</script>";

}else if(isset($_POST) && $_POST['B3'] == 'Delete') {
    $resid = $_POST['resid'];
    $sql = "UPDATE assist_".$cmpcode."_raps_res SET resstatusid='CN' WHERE resid=$resid";
    include("inc_db_con.php");

    $tkidDeptArr = explode("_",$_POST['restkidsel']);
    $restkid = $tkidDeptArr[0];
    //SET DATES
    $resdeadline = strtotime($_POST['resdeadline']);//mktime(23,59,59,$dmon,$dday,$dyear);
    $changes = "Resolution with id $resid <i>Cancelled</i> by ".getTK($tkid, $cmpcode, 'tkn');
    $statusStrId = 'CN';
    $resstate = -100;
    $sql = "INSERT INTO assist_".$cmpcode."_raps_log (logdate,logtkid,logupdate,logstatusid,logstate,logresid,logdeadline) VALUES
                                                     ($today,$restkid,'$changes','$statusStrId',$resstate,$resid,'$resdeadline')";
    include("inc_db_con.php");
    echo "<script type='text/javascript'>document.location.href='view.php?i=$resid';</script>";
}else {
//GET TASKS DETAILS
    $resid = $_GET['resid'];
    $sql = "SELECT * FROM assist_".$cmpcode."_raps_res WHERE resid = ".$resid;
    include("inc_db_con.php");
    $task = mysql_fetch_array($rs);
    mysql_close();
}

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>

        <script type ="text/javascript">

            $(function() {
                $("#resdeadline").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true		}),
                $("#resmeetingdate").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
            });
            $(document).ready(function(){
                $('#attachlinkWS').click(function(){
                    $('<tr><td align="right" colspan="3" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdocWS');
                });
            });
        </script>
        <script src="js_functions.js" type="text/javascript"></script>
    </head>
    <script language=JavaScript>
        function delAttachment(resid,attachment_id){
            var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed, otherwise cancel.');
            if(answer){
                document.location.href = "delete_attachment.php?resid="+resid+"&attachment_id="+attachment_id;
            }
            return false;
        }
        function Validate(me){

            var message ="";
            var restkidsel = me.restkidsel.value;
            var resref = me.resref.value;
            var resportfolioid = me.resportfolioid.value;
            var resurgencyid = me.resurgencyid.value;
            var resstatusid = me.resstatusid.value;
            var resaction = me.resaction.value;
            var resdeadline = me.resdeadline.value;
            var resmeetingdate = me.resmeetingdate.value;
            if(resref == "" || resref.length == 0){
                message += "Please resolution reference.\n";
            }

            if(restkidsel == "X" || restkidsel.length == 0){
                message += 'Please indicate who must receive this resolution.\n';
            }
            if(resportfolioid == "X" || resportfolioid.length == 0){
                message += 'Please indicate to which portfolio this resolution belongs.\n';
            }

            if(resdeadline == '' || resdeadline.length == 0){
                message += "Please select resolution deadline date.\n";
            }
            if(resmeetingdate == "" || resmeetingdate.length == 0 ){
                message += 'Please select resolution council meeting date.\n';
            }
            if(resaction == "" || resaction.length == 0){
                message += 'Please enter resolution instructions.\n';
            }
            if(resstatusid == "X"){
                message += "Please select resolution status.\n";
            }
            if(resurgencyid == "X"){
                message += "Please select resolution priority.\n";
            }
            if(message != ""){
                alert(message);
                return false;
            }
            return true;
        }
        function warning(){
            var answer = confirm("Are you sure you want to delete this resolution?\n Click Ok to continue with deleting,otherwise Cancel");
            if(answer){
                return true;
            }
            return false;
        }
        function changeDept() {
            var tkid = document.newres.restkidsel.value;
            if(tkid != "X")
            {
                var tkid2 = tkid.split("_");
                var dept = tkid2[1];
                var tkid3 = tkid2[0];
                document.newres.restkid.value = tkid3;
            }
            else
            {
                var dept = "X";
                document.newres.restkid.value = "";
            }
            var defsel;
            var sel;
            var act = 1;
            document.newres.resportfolioid.options.length=0;
            if (dept != "X")
            {
                for(c=1;c<cate.length;c++)
                {
                    for (i=0; i<cate[c].length; i++)
                    {
                        if(i==0 && c==dept)
                        {
                            defsel = true;
                            sel = true;
                        }
                        else
                        {
                            defsel = false;
                            sel = false;
                        }
                        document.newres.resportfolioid.options[document.newres.resportfolioid.options.length]=new Option(cate[c][i].split("|")[0], cate[c][i].split("|")[1],defsel,sel)
                    }
                }
            }
            else
            {
                document.newres.resportfolioid.options[0]=new Option("Please select a person above first", "X", true, true)
            }
        }
    </script>
    <link rel="stylesheet" href="/default.css" type="text/css">

    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Resolutions Assist: Edit Resolution</b></h1>
        <?php //echo '<pre>';print_r($jobadmin);?>
        <?php if($jobadmin[0] == "Y" && $jobadmin['a'] == "Y") {

            if($deptsrc=="Assist") {
                $sql = "SELECT * FROM assist_".$cmpcode."_list_dept WHERE yn = 'Y' ORDER BY value";
            }
            else {
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
            }
            include("inc_db_con.php");
            $dr = 0;
            while($row = mysql_fetch_array($rs)) {
                $deptrow[$dr] = $row;
                $dr++;
            }
            mysql_close();
            ?>
        <p><i>Please note that all fields are required unless otherwise indicated.</p>
        <form name=newres method=post action=edit.php onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
            <table border="1" id="table1" cellspacing="0" cellpadding="4">
                <tr>
                    <td class=tdgeneral valign="top"><b>Resolution Reference:</b></td>
                    <td class=tdgeneral valign="top"><input type=text name=resref maxlength=15 value="<?php echo($task['resref']);?>"></td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Assigned to:</b></td>
                    <td class=tdgeneral valign="top">
                        <select size="1" id="restkidsel" name="restkidsel" onchange="process();">
                            <option  value=X>--- SELECT ---</option>
                                <?php
                                $sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name, a.deptid, d.value dept ";
                                $sql.= "FROM assist_".$cmpcode."_timekeep t, ";
                                $sql.= "assist_".$cmpcode."_raps_list_users a, ";
                                if($deptsrc=="Assist") {
                                    $sql.= "assist_".$cmpcode."_list_dept d ";
                                }
                                else {
                                    $sql.= "assist_".$cmpcode."_sdbip_dept d ";
                                }
                                $sql.= "WHERE t.tkid = a.tkid ";
                                $sql.= "AND t.tkid <> '0000' ";
                                $sql.= "AND a.yn = 'Y' ";
                                $sql.= "AND a.r = 'Y' ";
                                $sql.= "AND d.id = a.deptid ";
                                $sql.= "ORDER BY t.tkname, t.tksurname";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option ".($row['tkid'] == $task['restkid'] ? "selected=selected" : "")." value=".$row['tkid']."_".$row['deptid'].">".$row['name']." (".$row['dept'].")</option>");
                                }
                                mysql_close();
                                ?>
                        </select><input type=hidden name=restkid>
                    </td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Assigned by:</b></td>
                    <td class=tdgeneral valign="top"><?php echo($tkname); ?><input type=hidden size=5 name=resadduser value=<?php echo($tkid); ?>></td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Portfolio:</td>
                    <td class=tdgeneral valign="top">

                        <select size="1" name="resportfolioid" id="resportfolioid">
                            <!-- <option selected value=X>Please select a person above first</option>-->
                                <?php

                                $sql = "SELECT p.value port, d.value dept,p.id portid, p.deptid ".
                                        "FROM assist_".$cmpcode."_raps_list_portfolio p, ".
                                        "assist_".$cmpcode."_sdbip_dept d ".
                                        "WHERE p.id=".$task['resportfolioid']." AND p.deptid = d.id AND p.yn = 'Y'";
                                include("inc_db_con.php");
                                $rowW = mysql_fetch_array($rs);
                                $port = $rowW['value'];
                                $dept = $rowW['dept'];
                                $portid = $rowW['portif'];
                                $deptid = $rowW['deptid'];

                                $sql = "SELECT p.value port, d.value dept, p.id portid, p.deptid ";
                                $sql.= "FROM assist_".$cmpcode."_raps_list_portfolio p, ";
                                $sql.= "assist_".$cmpcode."_sdbip_dept d ";
                                $sql.= "WHERE p.deptid = d.id ";
                                $sql.= "AND p.yn = 'Y' ";
                                $sql.= "AND d.id = ".$deptid;
                                $sql.= " ORDER BY d.sort, p.value";
                                include("inc_db_con.php");
                                ?>
                                <?php while($rowY = mysql_fetch_array($rs)) {?>
                            <option <?php ($rowY['portid'] == $portid ? "selected=selected" : "" ) ?> value="<?php echo($rowY['portid']);?>"><?php echo($rowY['dept']." - ".$rowY['port']);?></option>
                                    <?php }?>
                        </select>
                            <?php //echo '<pre>';print_r($deptrow);?>
                        <script language=JavaScript>
                            var cate=new Array();
                            cate[0]="";
    <?php
    foreach($deptrow as $dept) {
        $d = $dept['id'];
        $sql = "SELECT p.value port, d.value dept, p.id, p.deptid ";
        $sql.= "FROM assist_".$cmpcode."_raps_list_portfolio p, ";
        $sql.= "assist_".$cmpcode."_sdbip_dept d ";
        $sql.= "WHERE p.deptid = d.id ";
        $sql.= "AND p.yn = 'Y' ";
        $sql.= "AND d.id = ".$d;
        $sql.= " ORDER BY d.sort, p.value";
        include("inc_db_con.php");
        if(mysql_num_rows($rs)>0) {
            $row = mysql_fetch_array($rs);
            $carr = "cate[".$d."] = [\"".$row['dept']." - ".$row['port']."|".$row['id']."\"";
            while($row = mysql_fetch_array($rs)) {
                $carr.= ", \"".$row['dept']." - ".$row['port']."|".$row['id']."\"";
            }
            $carr.= "];";
            echo(chr(10).$carr);
        }
        else {
            $carr = "cate[".$d."] = \"\"";
            echo(chr(10).$carr);
        }
        mysql_close();
    }
    ?>
                        </script>
                    </td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Priority:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="resurgencyid">
                            <option value=X>--- SELECT ---</option>
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_urgency ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    if($row['id']==2) {
                                        echo("<option selected value=".$row['id'].">".$row['value']."</option>");
                                    }
                                    else {
                                        echo("<option ".($task['resurgencyid'] == $row['id'] ? "selected=selected" : "")." value=".$row['id'].">".$row['value']."</option>");
                                    }
                                }
                                mysql_close();
                                ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Status:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="resstatusid">
                            <option value=X>--- SELECT ---</option>
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_raps_list_status WHERE id IN ('CL','IP','NW','ON') AND yn = 'Y' ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    if($row['id'] == "NW") {
                                        echo("<option selected value=".$row['id'].">".$row['value']."</option>");
                                    }
                                    else {
                                        echo("<option ".($task['resstatusid'] == $row['id'] ? "selected=selected" : "")." value=".$row['id'].">".$row['value']."</option>");
                                    }
                                }
                                mysql_close();
                                ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Council Meeting Date:</b></td>
                    <td class=tdgeneral valign="top"><input type="text" name="resmeetingdate"  id="resmeetingdate" size="10" value="<?php echo(date("d M Y",$task['resmeetingdate']));?>">
                    </td>
                </tr>
                <tr>
                    <td class=tdgeneral valign="top"><b>Deadline:</b></td>
                    <td class=tdgeneral valign="top"><input type="text" name="resdeadline" id="resdeadline" size="10" value="<?php echo(date("d M Y",$task['resdeadline']));?>"></td>
                </tr>
                <tr>
                    <td class=tdgeneral colspan=2 valign="top"><b>Resolution Instructions:</b><br>
                        <textarea rows="5" name="resaction" cols="35"><?php echo($task['resaction']);?></textarea></td>
                </tr>
                <tr>
                    <td class=tdgeneral colspan=2 valign="top"><b>Additional Notes:</b> <i>(Not required)</i><br>
                        <textarea rows="5" name="resnotes" cols="35"><?php echo($task['resnotes']);?></textarea></td>
                </tr>
                    <?php
                    $sql = "SELECT * FROM  assist_".$cmpcode."_raps_attachments AS raps WHERE raps.resid = ".$resid;
                    include("inc_db_con.php");
                    ?>
                <tr>
                    <td valign="top"><b>Attachment(s):</b></td>
                    <td class=tdgeneral>
                            <?php
                            if(!mysql_num_rows($rs)) {
                                echo ("<i>No attachments found.</i>");
                            }else {
                                echo "<table style='border:none;'>";
                            }

                            while($row = mysql_fetch_assoc($rs)) {

                                $file = "../files/".$cmpcode."/RAPS/".$row['system_filename'];
                                if(!file_exists($file))
                                    continue;
                                $raps_attach_id = $row['id'];

                                echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:download(\"$file\")'>".$row['original_filename']."</a>
                       </td>
                       <td style='border:none;'>
                            <input type='button' name='deleteAttachment' id='deleteAttachment' value='Delete' onclick='delAttachment(\"$resid\",\"$raps_attach_id\")' />
                       </td>
                       </tr>");
                            }
                            if(mysql_num_rows($rs) > 0) {
                                echo("</table>");
                            }
                            ?>

                    </td>
                </tr>
                <tr id="firstdocWS">
                    <td class=tdgeneral colspan="3" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="3"><a href="javascript:void(0)" id="attachlinkWS">Attach another file</a></td>
                </tr>
                <tr>
                    <td class=tdgeneral colspan="2" valign="top">
                        <input type="hidden" value="<?php echo($resid);?>" name="resid" />
                        <input type="submit" value="Submit" name="B1" />
                        <input type="reset" value="Reset" name="B2" />
                        <input type="submit" value="Delete" name="B3" onclick="return warning();"/>
                    </td>
                </tr>
            </table>
        </form>
            <?php } else { ?><p>You do not have permission to assign new resolutions.</p><?php } ?>
    </body>

</html>
