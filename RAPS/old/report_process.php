<?php
include("inc_ignite.php");

//GET VARIABLES
$field = $_POST['field'];
//$udf = $_POST['udf'];
$sfilter = $_POST['statusfilter'];
$tfilter = $_POST['tkidfilter'];
$afilter = $_POST['addfilter'];
$pfilter = $_POST['portfilter'];
$cmdfrom = $_POST['cmdfrom'];
$cmdto = $_POST['cmdto'];
$csvfile = $_POST['csvfile'];


if(strlen($cmdfrom) > 0)
{
    $cmdfarr = explode("-",$cmdfrom);
    $cffilter = mktime(0,0,0,$cmdfarr[1],$cmdfarr[0],$cmdfarr[2]);
}
if(strlen($cmdto) > 0)
{
    $cmdtarr = explode("-",$cmdto);
    $ctfilter = mktime(12,0,0,$cmdtarr[1],$cmdtarr[0],$cmdtarr[2]);
}

//CONVERT FROM array[#] = text TO array[text] = Y
foreach($field as $f)
{
    $farray[$f] = "Y";
}
$uarray = "";
foreach($udf as $u)
{
    $uarray[$u] = "Y";
}
$udffilter = "";

/*    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = 'TA' ORDER BY udfisort";
    include("inc_db_con.php");
        $udfnum = mysql_num_rows($rs);
        $u = 0;
        while($row = mysql_fetch_array($rs))
        {
            $udfindex[$u]['id'] = $row['udfiid'];
            $udfindex[$u]['value'] = $row['udfivalue'];
            $udfindex[$u]['list'] = $row['udfilist'];
            $u++;
            $udfname = "udf".$row['udfiid']."filter";
            $ufilter = $_POST[$udfname];
            //echo("<p>ind: ".$row['udfiid']."<Br>val: ".$row['udfivalue']."<Br>list: ".$row['udfilist']."<Br>filter: ".$ufilter."<Br>len: ".strlen($ufilter));
            if((strlen($ufilter) > 0 && ($row['udfilist'] == "T" || $row['udfilist'] == "M")) || ($ufilter != "ALL" && ($row['udfilist']=="Y" || $row['udfilist'] == "L")))
            {
                $udffilter[$row['udfiid']] = $ufilter;
            }
            else
            {
                $udfnum = $udfnum - 1;
            }
        }
    mysql_close();
*//*echo("<p>udfs: ");
print_r($udfindex);
    echo("<P>udfnum ".$udfnum);
    echo("<p>uarray: ");
print_r($uarray);
    echo("<P>count ".count($udf));
echo("<p>udffilter: ");
print_r($udffilter);
*/
//SET SQL VARIABLE
$sql = "SELECT s.id sid";
$sql.= ", a.resid tid";
$sql.= ", a.resref rref";
$sql.= ", t.tkname tname";
$sql.= ", t.tksurname tsurname";
$sql.= ", l.value topic";
$sql.= ", a.resaction taction";
$sql.= ", a.resnotes tdeliver";
$sql.= ", s.value tstatus";
$sql.= ", a.resstate tstate";
$sql.= ", a.resdeadline tdeadline";
$sql.= ", a.resadddate tadddate";
$sql.= ", k.tkname toname";
$sql.= ", k.tksurname tosurname";
$sql.= ", a.resmeetingdate";
$sql.= ", d.value dept ";
$sql .= " FROM assist_".$cmpcode."_raps_res a, assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_raps_list_portfolio l, assist_".$cmpcode."_sdbip_dept d, assist_".$cmpcode."_timekeep k, assist_".$cmpcode."_raps_list_status s";
$sql .= " WHERE a.restkid = t.tkid";
$sql .= " AND a.resstatusid = s.id";
$sql .= " AND a.resportfolioid = l.id";
$sql .= " AND a.resadduser = k.tkid";
$sql .= " AND l.deptid = d.id";
switch($tfilter)
{
    case "ALL":
        break;
    default:
        $sql .= " AND a.restkid = '".$tfilter."'";
        break;
}
switch($afilter)
{
    case "ALL":
        break;
    default:
        $sql .= " AND a.resadduser = '".$afilter."'";
        break;
}
switch($pfilter)
{
    case "ALL":
        break;
    default:
        $pfiltarr = explode("_",$pfilter);
        if(is_numeric($pfiltarr[1]) && strlen($pfiltarr[1])>0)
        {
            if($pfiltarr[0]=="d")
            {
                $sql .= " AND l.deptid = '".$pfiltarr[1]."'";
            }
            else
            {
                if($pfiltarr[0]=="p")
                {
                    $sql .= " AND l.id = '".$pfiltarr[1]."'";
                }
            }
        }
        break;
}
switch($sfilter)
{
    case 1:
        $sql .= " AND (s.id = 'NW' OR s.id = 'IP' OR s.id = 'ON')";
        $farray['duration']=="N";
        break;
    case 2:
        $sql .= " AND (s.id = 'NW' OR s.id = 'IP')";
        $farray['duration']=="N";
        break;
    case 3:
        $sql .= " AND (s.id = 'ON')";
        $farray['duration']=="N";
        break;
    case 4:
        $sql .= " AND (s.id = 'CL')";
        break;
    case 5:
        $sql .= " AND (s.id <> 'CN')";
        break;
    default:
        $sql .= " AND (s.id <> 'CN')";
        break;
}
if(strlen($cffilter)>0 && strlen($ctfilter)==0)
{
    $sql .= " AND a.resmeetingdate = ".$cffilter;
}
else
{
    if(strlen($ctfilter)>0 && strlen($cffilter)==0)
    {
        $sql .= " AND a.resmeetingdate = ".$ctfilter;
    }
    else
    {
        if(strlen($ctfilter)>0 && strlen($cffilter)>0)
        {
            if($ctfilter == $cffilter)
            {
                $sql .= " AND a.resmeetingdate = ".$ctfilter;
            }
            else
            {
                $sql .= " AND a.resmeetingdate >= ".$cffilter." AND a.resmeetingdate <= ".$ctfilter;
            }
        }
    }
}
$sql .= " ORDER BY a.resid";
//echo("<P>".$sql);
//Set transaction log
/* $tsql2 = $sql;
$tsql = str_replace("'","|",$tsql2);
$tref = 'TA';
$trans = "Generated task report to view Task id";
if($farray['adddate'] == "Y") {$trans .= ", Date added";}
if($farray['tkid'] == "Y") {$trans .= ", Person tasked";}
if($farray['adduser'] == "Y") {$trans .= ", Task owner";}
if($farray['topicid'] == "Y") {$trans .= ", Task topic";}
if($farray['action'] == "Y") {$trans .= ", Task instructions";}
if($farray['deliver'] == "Y") {$trans .= ", Task deliverables";}
if($farray['deadline'] == "Y") {$trans .= ", Task deadline";}
$trans .= ", Task status; Report output as ";
if($csvfile == "Y") {$trans .= "CSVfile.";} else {$trans .= "onscreen display.";}
include("inc_transaction_log.php");
*/
//RUN SQL QUERY
//$sql = $tsql2;
include("inc_db_con.php");


if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{
        //CREATE HEADER ROW
            $fdata = "\"Ref\"";
            if($farray['adddate'] == "Y") {$fdata .= ",\"Assigned on\"";}
    		if($farray['tkid'] == "Y") {$fdata .= ",\"Assigned to\"";}
            if($farray['adduser'] == "Y") {$fdata .= ",\"Assigned by\"";}
            if($farray['portfolio'] == "Y") {$fdata .= ",\"Portfolio\"";}
            if($farray['meeting'] == "Y") {$fdata .= ",\"Council Meeting Date\"";}
            if($farray['deadline'] == "Y") {$fdata .= ",\"Deadline\"";}
            if($farray['action'] == "Y") {$fdata .= ",\"Instructions\"";}
            if($farray['notes'] == "Y") {$fdata .= ",\"Additional notes\"";}
            if($farray['update'] == "Y") {$fdata .= ",\"Latest Update\"";}
            if($farray['update'] == "Y") {$fdata .= ",\"Latest Update Date\"";}
            if($farray['update'] == "Y") {$fdata .= ",\"Duration\"";}
            $fdata .= ",\"Status\"";
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y") {$fdata .= ",\"".$udfi['value']."\"";}
                }
            }
    //LOOP TROUGH QUERY RESULTS AND ADD DETAILS TO ROW IF array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
            $tchk = "X";
/*        if(count($udf) > 0 || $udfnum > 0)
        {
            foreach($udfindex as $udfi)
            {
                $uchk = "N";
                //echo("<P>uarray[id] ".$uarray[$udfi['id']]);
                if(($uarray[$udfi['id']] == "Y" || strlen($udffilter[$udfi['id']]) > 0) && $tchk != "N")
                {
                    $sql2 = "SELECT * FROM assist_ign0001_udf u";
                    $sql2.= " WHERE u.udfref = 'TA'";
                    $sql2.= " AND u.udfnum = ".$row['tid'];
                    $sql2.= " AND u.udfindex = ".$udfi['id'];
                    if(($udfi['list'] == "T" || $udfi['list'] == "M") && strlen($udffilter[$udfi['id']]) > 0)
                    {
                        $sql2.= " AND u.udfvalue LIKE '%".$udffilter[$udfi['id']]."%'";
                    }
                    else
                    {
                        if(($udfi['list'] == "L" || $udfi['list'] == "Y") && strlen($udffilter[$udfi['id']]) > 0)
                        {
                            $sql2.= " AND u.udfvalue = '".$udffilter[$udfi['id']]."'";
                        }
                    }
                    //echo("<P>".$sql2);
                    include("inc_db_con2.php");
                        $rscount = mysql_num_rows($rs2);
                        //echo("<P>".$sql2);
                        //echo("<P>".$rscount);
                        if(strlen($udffilter[$udfi['id']]) > 0 && $rscount == 0)
                        {
                            $tchk = "N";
                        }
                        if($rscount > 0 && $tchk != "N" && $uarray[$udfi['id']] == "Y")
                        {
                            $uchk = "Y";
                            $row2 = mysql_fetch_array($rs2);
                            //echo("<br>row ");
                            //print_r($row2);
                            $udfvalue[$udfi['id']] = $row2['udfvalue'];
                        }
                    mysql_close($con2);
                    if($uchk == "Y")
                    {
                        if($udfi['list'] == "L" || $udfi['list'] == "Y")
                        {
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvalue[$udfi['id']];
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                $udfvalue[$udfi['id']] = $row2['udfvvalue'];
                            mysql_close($con2);
                        }
                        else
                        {
                            //$udfvalue[$udfi['id']] = str_replace(chr(10),"<br>",$udfvalue[$udfi['id']]);
                        }
                    }
                }
                //echo("<P>".$tchk);
            }
        }
        else
        {
            $tchk = "Y";
        }
//echo("<P>tchk ".$tchk);
//echo("<P>udfvalue: ");
//print_r($udfvalue);
*/        if($tchk == "Y" || $tchk == "X")
        {

	   $fdata .= "\r\n";
		    $fdata .= "\"".$row['rref']."\"";
		    if($farray['adddate'] == "Y") {$fdata .= ",\"".date("d M Y H:i",$row['tadddate'])."\"";}
		    $tname = $row['tname']." ".$row['tsurname'];
		    $tname = str_replace("&#39","'",$tname);
		    $tname = str_replace("&#246","&#246;",$tname);
		    if($farray['tkid'] == "Y") {$fdata .= ",\"".html_entity_decode($tname, ENT_QUOTES, "ISO-8859-1")."\"";}
		    $toname = $row['toname']." ".$row['tosurname'];
		    $toname = str_replace("&#39","'",$toname);
		    if($farray['adduser'] == "Y") {$fdata .= ",\"".html_entity_decode($toname, ENT_QUOTES, "ISO-8859-1")."\"";}
		    $topic = $row['dept']." - ".$row['topic'];
		    $topic = str_replace("&#39","'",$topic);
		    if($farray['portfolio'] == "Y") {$fdata .= ",\"".html_entity_decode($topic, ENT_QUOTES, "ISO-8859-1")."\"";}
		    if($farray['meeting'] == "Y") {$fdata .= ",\"".date("d M Y",$row['resmeetingdate'])."\"";}
		    if($farray['deadline'] == "Y") {$fdata .= ",\"".date("d M Y",$row['tdeadline'])."\"";}
		    $action = $row['taction'];
		    $action = str_replace("&#39","'",$action);
		    $action = str_replace("\"","'",$action);
		    if($farray['action'] == "Y") {$fdata .= ",\"".html_entity_decode($action, ENT_QUOTES, "ISO-8859-1")."\"";}
		    $deliver = $row['tdeliver'];
		    $deliver = str_replace("&#39","'",$deliver);
		    $deliver = str_replace("\"","'",$deliver);
		    if($farray['notes'] == "Y") {$fdata .= ",\"".html_entity_decode($deliver, ENT_QUOTES, "ISO-8859-1")."\"";}
		    if($farray['update'] == "Y" || $farray['updatedate']=="Y" || $farray['duration']=="Y")
            {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_raps_log WHERE logresid = ".$row['tid']." AND logupdate NOT LIKE 'New Resolution added.%' ORDER BY logdate DESC";
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $updatedate = $row2['logdate'];
            }
		    if($farray['update'] == "Y")
            {
                $update = $row2['logupdate']." ";
                $fdata .= ",\"".html_entity_decode($update, ENT_QUOTES, "ISO-8859-1")."\"";
            }
		    if($farray['updatedate'] == "Y")
            {
                if(strlen($updatedate)>0 && is_numeric($updatedate))
                {
                    $ud = date("d M Y H:i",$updatedate);
                }
                else
                {
                    $ud = "";
                }
                $fdata .= ",\"".$ud."\"";
            }
		    if($farray['duration'] == "Y")
            {
                if($row['sid'] == "CL" && strlen($updatedate)>0 && is_numeric($updatedate) && strlen($row['tadddate']) > 0 && is_numeric($row['tadddate']))
                {
                    $duration = $updatedate - $row['tadddate'];
                    $durdays = floor($duration/86400);
                    $durhours = floor(($duration-($durdays*86400))/3600);
                    $durmins = floor(($duration - ($durdays*86400 + $durhours*3600))/60);
                    $dursecs = $duration - ($durdays*86400 + $durhours*3600 + $durmins*60);
                    if($durhours<10) { $durhours = "0".$durhours; }
                    if($durmins<10) { $durmins = "0".$durmins; }
                    if($dursecs<10) { $dursecs = "0".$dursecs; }
                    $duration2 = $durdays." days ".$durhours.":".$durmins.":".$dursecs;
                }
                else
                {
                    $duration = "";
                }
                $fdata .= ",\"".$duration2."\"";
            }
            $duration = "";
            $durdays = "";
            $durhours = "";
            $durmins = "";
            $dursecs = "";
            $duration2 = "";
            $ud = "";
            $updatedate = "";
    		$fdata .= ",\"".$row['tstatus'];
            if($row['sid'] == "IP" || $row['sid'] == "NW")
            {
                $fdata .= " (".$row['tstate']."%)";
            }
            $fdata .= "\"";
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y")
                    {
                        $fdata .= ",\"".$udfvalue[$udfi['id']]."\"";
                    }
                }
            }
        }
    }
    mysql_close();
    //WRITE DATA TO FILE
	checkFolder($report_save_path);
    $filename = "../files/".$cmpcode."/".$report_save_path."/raps_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "resolutions_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
}
else    //ELSE OUTPUT IS ONSCREEN DISPLAY
{
//SET DISPLAY OF HTML HEAD & BODY
$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>Resolutions Assist: Report</b></h1>";
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    $display .= "<p>There are no resolutions that meet your criteria.<br>Please go back and select different criteria.</p>";
}
//ELSE
else
{
    //CREATE TABLE AND SET TDHEADER
    $display .= "<table border=1 cellspacing=0 cellpadding=2 style=\"border-collapse: collapse; border: 1px solid #dddddd;\">	<tr>		<td class=tdheaderorange>Ref</td>";
            if($farray['adddate'] == "Y") {$display .= "<td class=tdheaderorange>Assigned on</td>";}
    		if($farray['tkid'] == "Y") {$display .= "<td class=tdheaderorange>Assigned to</td>";}
            if($farray['adduser'] == "Y") {$display .= "<td class=tdheaderorange>Assigned by</td>";}
            if($farray['portfolio'] == "Y") {$display .= "<td class=tdheaderorange>Portfolio</td>";}
            if($farray['meeting'] == "Y") {$display .= "<td class=tdheaderorange>Council Meeting Date</td>";}
            if($farray['deadline'] == "Y") {$display .= "<td class=tdheaderorange>Deadline</td>";}
            if($farray['action'] == "Y") {$display .= "<td class=tdheaderorange>Instructions</td>";}
            if($farray['notes'] == "Y") {$display .= "<td class=tdheaderorange>Additional Notes</td>";}
            if($farray['update'] == "Y") {$display .= "<td class=tdheaderorange>Latest Update</td>";}
            if($farray['update'] == "Y") {$display .= "<td class=tdheaderorange>Latest Update Date</td>";}
            if($farray['duration'] == "Y") {$display .= "<td class=tdheaderorange>Duration</td>";}
            $display .= "<td class=tdheaderorange>Status</td>";
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y") {$display .= "<td class=tdheaderorange>".$udfi['value']."</td>";}
                }
            }

	$display .= "</tr>";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $tchk = "X";
/*        if(count($udf) > 0 || $udfnum > 0)
        {
            foreach($udfindex as $udfi)
            {
                $uchk = "N";
                //echo("<P>uarray[id] ".$uarray[$udfi['id']]);
                if(($uarray[$udfi['id']] == "Y" || strlen($udffilter[$udfi['id']]) > 0) && $tchk != "N")
                {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf u";
                    $sql2.= " WHERE u.udfref = 'RAPS'";
                    $sql2.= " AND u.udfnum = ".$row['tid'];
                    $sql2.= " AND u.udfindex = ".$udfi['id'];
                    if(($udfi['list'] == "T" || $udfi['list'] == "M") && strlen($udffilter[$udfi['id']]) > 0)
                    {
                        $sql2.= " AND u.udfvalue LIKE '%".$udffilter[$udfi['id']]."%'";
                    }
                    else
                    {
                        if(($udfi['list'] == "L" || $udfi['list'] == "Y") && strlen($udffilter[$udfi['id']]) > 0)
                        {
                            $sql2.= " AND u.udfvalue = '".$udffilter[$udfi['id']]."'";
                        }
                    }
                    //echo("<P>".$sql2);
                    include("inc_db_con2.php");
                        $rscount = mysql_num_rows($rs2);
                        //echo("<P>".$sql2);
                        //echo("<P>".$rscount);
                        if(strlen($udffilter[$udfi['id']]) > 0 && $rscount == 0)
                        {
                            $tchk = "N";
                        }
                        if($rscount > 0 && $tchk != "N" && $uarray[$udfi['id']] == "Y")
                        {
                            $uchk = "Y";
                            $row2 = mysql_fetch_array($rs2);
                            //echo("<br>row ");
                            //print_r($row2);
                            $udfvalue[$udfi['id']] = $row2['udfvalue'];
                        }
                    mysql_close($con2);
                    if($uchk == "Y")
                    {
                        if($udfi['list'] == "L" || $udfi['list'] == "Y")
                        {
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvalue[$udfi['id']];
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                $udfvalue[$udfi['id']] = $row2['udfvvalue'];
                            mysql_close($con2);
                        }
                        else
                        {
                            $udfvalue[$udfi['id']] = str_replace(chr(10),"<br>",$udfvalue[$udfi['id']]);
                        }
                    }
                }
                //echo("<P>".$tchk);
            }
        }
        else
        {
            $tchk = "Y";
        }*/
//echo("<P>tchk ".$tchk);
//echo("<P>udfvalue: ");
//print_r($udfvalue);
        if($tchk == "Y" || $tchk == "X")
        {
    	$display .= "<tr>";
	   	    $display .= "<td class=tdheaderorange valign=top>".$row['rref']."</td>";
		    if($farray['adddate'] == "Y") {$display .= "<td class=tdgeneral valign=top align=center>".date("d M Y H:i:s",$row['tadddate'])."</td>";}
		    if($farray['tkid'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['tname']." ".$row['tsurname']."</td>";}
		    if($farray['adduser'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['toname']." ".$row['tosurname']."</td>";}
		    if($farray['portfolio'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['dept']." - ".$row['topic']."&nbsp;</td>";}
		    if($farray['meeting'] == "Y") {$display .= "<td class=tdgeneral valign=top align=center>".date("d M Y",$row['resmeetingdate'])."&nbsp;</td>";}
		    if($farray['deadline'] == "Y") {$display .= "<td class=tdgeneral valign=top align=center>".date("d M Y",$row['tdeadline'])."&nbsp;</td>";}
		    if($farray['action'] == "Y") {$display .= "<td class=tdgeneral valign=top>".str_replace(chr(10),"<br>",$row['taction'])."&nbsp;</td>";}
		    if($farray['notes'] == "Y") {$display .= "<td class=tdgeneral valign=top>".str_replace(chr(10),"<br>",$row['tdeliver'])."&nbsp;</td>";}
		    if($farray['update'] == "Y" || $farray['updatedate'] == "Y" || $farray['duration'] == "Y")
            {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_raps_log WHERE logresid = ".$row['tid']." AND logupdate NOT LIKE 'New Resolution added.%' ORDER BY logid DESC LIMIT 1";
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
            }
		    if($farray['update'] == "Y")
            {
                $update = $row2['logupdate']." ";
                $display .= "<td class=tdgeneral valign=top>".str_replace(chr(10),"<br>",$update)."&nbsp;</td>";
            }
		    if($farray['updatedate'] == "Y")
            {
                $updatedate = $row2['logdate'];
                if(strlen($updatedate)>0)
                {
                    $ud = date("d M Y H:i:s",$updatedate);
                }
                else
                {
                    $ud = "&nbsp;";
                }
                $display .= "<td class=tdgeneral align=center valign=top>".$ud."</td>";
            }
		    if($farray['duration'] == "Y")
            {
                if($row['sid']=="CL" && strlen($updatedate)>0 && is_numeric($updatedate) && strlen($row['tadddate'])>0 && is_numeric($row['tadddate']))
                {
                    $duration = $updatedate - $row['tadddate'];
                    $durdays = floor($duration/86400);
                    $durhours = floor(($duration-($durdays*86400))/3600);
                    $durmins = floor(($duration - ($durdays*86400 + $durhours*3600))/60);
                    $dursecs = $duration - ($durdays*86400 + $durhours*3600 + $durmins*60);
                    if($durhours<10) { $durhours = "0".$durhours; }
                    if($durmins<10) { $durmins = "0".$durmins; }
                    if($dursecs<10) { $dursecs = "0".$dursecs; }
                    $duration2 = $durdays." days ".$durhours.":".$durmins.":".$dursecs;
                }
                else
                {
                    $duration = "&nbsp;";
                }
                $display .= "<td class=tdgeneral valign=top>".$duration2."</td>";
            }
            $duration = 0;
            $durdays = "";
            $durhours = "";
            $duration2 = "";
            $durmins = "";
            $dursecs = "";
        $ud = "";
        $updatedate = "";
		$display .= "<td class=tdgeneral valign=top>".$row['tstatus'];

            if($row['sid'] == "IP" || $row['sid'] == "NW")
            {
                $display .= " (".$row['tstate']."%)";
            }
        $display .= "</td>";
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y")
                    {
                        $display .= "<td class=tdgeneral valign=top>".$udfvalue[$udfi['id']]."&nbsp;</td>";
                    }
                }
            }
        $display .= "</tr>";
        }   //if tchk = yes

    }
    $display .= "</table>";
}
mysql_close();
$display .= "</body></html>";
//WRITE DISPLAY TO SCREEN
echo($display);


}   //ENDIF CSVFILE = Y
?>

