<?php

if($_REQUEST['src']!="frontpage") {
	$req = $_REQUEST;
	unset($req['id']);
	unset($req['src']);
	unset($req['jquery_version']);
	unset($req['r']);
	$history = "";
	foreach($req as $key => $val) {
		$history.="&".$key."=".$val;
	}
	$ajax_path = $self.".php";
	$ajax_querystring = $history."&id=".$_REQUEST['id']."&src=".$_REQUEST['src'];
	$history = $_REQUEST['src'].$history;
} else {
	$ajax_path = "frontpage";
	$history = "frontpage";
	$ajax_querystring = "";
}
$resid = $_REQUEST['id'];
$res = $me->getAResolution($resid);
$attach = $me->getResAttachments($resid);
$headings = $me->getAllHeadings();
$types = $me->getHeadingTypes();

if($can_edit_action_owner===true) {
	$ao = $me->getActionOwners();
	$actionowners = array('portfolio'=>array(),'other'=>array());
	foreach($ao as $a) {
		if($a['deptid']==$res['deptid']) {
			$actionowners['portfolio'][] = $a;
		} else {
			$actionowners['other'][] = $a;
		}
	}
}

if(isset($_REQUEST['r'])) {
	ASSIST_HELPER::displayResult($_REQUEST['r']);
}
?>
<script type=text/javascript>
	var return_path = '<?php echo $ajax_path; ?>'; 
	var return_querystring = '<?php echo $ajax_querystring; ?>';
	var redirect_path = '<?php echo $history; ?>';
	var resid = '<?php echo $resid; ?>';
</script>

<div class=float  style='background-color: #ffffff; width: 50%;'><?php 
if($can_update===true && $res['resstatusid']!="CL") {
	$status = $me->getActiveStatus();
	unset($status['NW']);
?>
<h2 style='margin-left: 1%'>Update</h2>
<form name=frm_update method=post action="ajax/upload_attachment.php" language=jscript enctype="multipart/form-data">
<input type=hidden value="<?php echo $resid; ?>" name=resid id=resid />
<input type=hidden value="0" name=logid id=logid />
<input type=hidden value="SAVE_UPDATE" name=act id=act />
<input type=hidden value="LOG" name=attach_type  />
<table class='form' width=99% style='margin-left: 1%'>
	<tr>
		<th id=logupdate>Response:</th>
		<td><textarea name=logupdate></textarea></td>
	</tr>
	<tr>
		<th id=logactdate>Action on:</th>
		<td><input type=text name=logactdate value='<?php echo date("d-M-Y"); ?>' class=jdate2012 size=10 /></td>
	</tr>
	<tr>
		<th id=logstatusid>Status:</th>
		<td><select name=logstatusid><?php
			foreach($status as $key=>$v) {
				echo "<option ".($key=="IP" ? "selected" : "")." value=$key>$v</option>";
			}
		?></select></td>
	</tr>
	<tr>
		<th id=logstate>Progress:</th>
		<td><input type=text size=5 class=right name=logstate value="<?php echo $res['resstate']; ?>" />% <input type=hidden name=oldstate value='<?php echo $res['resstate']; ?>' /></td>
	</tr>
	<tr>
		<th>Attachment:</th>
		<td><?php $me->displayAttachmentForm($history); ?>
		</td>
	</tr>
    <tr>
		<th>Receive copy email?</th>
		<td ><select name=copyemail><option value=N selected>No</option><option value=Y>Yes</option></select></td>
	</tr>
	<tr>
		<th></th>
		<td><input type=button value='Save Update' class=isubmit /> <input type=reset value=Reset /></td>
	</tr>
</table>
</form>
<div id=dlg_error title="Required fields">
<h1 class=red>Error</h1>
<p>Please complete the following required fields:</p>
<ul id=ul_req>
</ul>
</div>
<div id=dlg_loading>
<p class=center>Please wait while your form is processed...</p>
<p class=center><img src="../pics/ajax_loader_v2.gif" /></p>
</div>
<div id=dlg>
</div>
<style type=text/css>
.green { border: 1px solid #009900; color: #009900; }
.green:hover { border: 1px solid #fe9900; color: #fe9900; }
</style>
<script type=text/javascript>
$(function() {
	$("select[name=logstatusid]").change(function() {
		if($(this).val()=="CL") {
			$("input[name=logstate]").val("100").prop("disabled","disabled");
		} else {
			$("input[name=logstate]").val($("input[name=oldstate]").val()).prop("disabled","");
		}
	});
	$("input[name=logstate]").blur(function() {
		if($(this).val()=="100") {
			$("select[name=logstatusid]").val("CL");
			$(this).prop("disabled","disabled");
		}
	});


	var notRequired = new Array();
	var dlgErrorOpts = ({
		modal: true,
		autoOpen: false,
		width: 300,
		height: 320,
		buttons:[{
			text: "Ok",
			click: function() { $(this).dialog("close"); }
		}]
	});
	var dlgLoadingOpts = ({
		modal: true,
		autoOpen: false,
		width: 250,
		height: 270
	});
	$("#dlg").dialog({modal: true,autoOpen: false});
	$(".ui-dialog-titlebar").hide();
	$("#dlg_error, #dlg_loading").hide();
	$("form[name=frm_update] input:button.isubmit").click(function() { 
		$("#dlg").html($("#dlg_loading").html());
		$("#dlg").dialog("option",dlgLoadingOpts).dialog("open");
		var $form = $("form[name=frm_update]");
		var resid = 0;
		var response = "";
		var result = "ok";
		var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9");
		var valid = true;
		//commented out for file upload testing purposes
		$("#dlg_error #ul_req li").remove();
		var d = "";
		//var data = "act=CREATE";
		$("select, input:text, textarea").removeClass("required").each(function() {
			var $me = $(this);
			var n = $me.prop("name");
			d = $me.is("textarea") ? $me.text() : $me.val();
			if(d.length==0) {
				d = $me.is("textarea") ? $me.val() : $me.text();
			}
			if($.inArray(n,notRequired)<0) {
				if(d.length==0 || ($me.is("select") && d=="X")) {
					$me.addClass("required");
					var i = $("#"+n).html();
					i = i.replace(":","");
					$("#dlg_error #ul_req").append("<li>"+i+"</li>");
					valid = false;
				} else if(n=="logstatusid" && d=="IP") {
					if($("input[name=logstate]").val()=="0" || $("input[name=logstate]").val()=="100") {
						$("input[name=logstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must be between 1% - 99% if status is In Progress.</li>");
					valid = false;
					}
				} else if(n=="resstatusid" && d=="CL") {
					if($("input[name=resstate]").val()!="100") {
						$("input[name=resstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must equal 100% if status is Completed.</li>");
					valid = false;
					}
				} else if(n=="resstatusid" && d=="NW") {
					if($("input[name=resstate]").val()!="0") {
						$("input[name=resstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must equal 0% if status is New.</li>");
					valid = false;
					}
				} else if(n=="logstate") {
					var v;
					var valid_perc = true;
					for(i=0;i<d.length;i++) {
						v = d.charAt(i);
						if($.inArray(v,validNumbers)<0) {
							$me.addClass("required");
							$("#dlg_error #ul_req").append("<li>Progress can only contain a valid number (0-9).</li>");
							i = d.length+1;
							valid_perc = false;
					valid = false;
						}
					}
					if(valid_perc) {
						var vp = d*1;
						if(vp>100 || vp<0) {
							$me.addClass("required");
							$("#dlg_error #ul_req").append("<li>Progress cannot be less than 0% or greater than 100%.</li>");
					valid = false;
						}
					}
				}
			}
		});
		if(valid) {
			var f = 0;
			$("input:file").each(function() {
				f+=$(this).val().length;
			});
			$("input[name=logstate]").prop("disabled","");
			var dta = AssistForm.serialize($form);
			//alert($("select[name=copyemail]").val());
			$.ajax({                                      
				url: 'ajax/manage_raps.php', 		  type: 'POST',		  data: dta,		  dataType: 'json',
				success: function(d) { 
					result = d[0];
					response = d[1];
					$("#logid").val(d[2]);
					$("#result").val(d[0]);
					$("#response").val(d[1]);
					//alert(response+" LOG"+d[2]);
					if(f>0) {
						$form.prop("target","file_upload_target");
						$form.submit();
					} else {
						if(redirect_path=="frontpage") {
							parent.header.location.href = '/title_login.php?m=action_dashboard';
						} else {
							document.location.href = redirect_path+'&r[]='+result+'&r[]='+response;
						}
					}
					//$("#dlg").dialog("close");
				},
				error: function(d) { console.log(d); }
			});
		} else {
			$("#dlg").dialog("close");
			$("#dlg").html($("#dlg_error").html());
			$("#dlg").dialog("option",dlgErrorOpts).dialog("open");
			$('.ui-dialog :button').blur();
			$(".ui-dialog :button:last").css({"color":"#009900","border":"1px solid #009900"});
		}
	});
});
</script><?php 
} //endif can update

$me->displayActivityLog($resid); 

?><p>&nbsp;</p></div>
<div class=float  style='background-color: #ffffff; width: 50%; margin-bottom: 10px;'>
<h2>Details</h2>
<table class=form width=99% style='margin-right: 1%'><?php
foreach($headings as $fld=>$h) {
	$v = isset($res['res'.$fld]) ? $res['res'.$fld] : "";
	switch($types[$fld]) {
		case "DATE":
			$v = date("d-M-Y",$v);
			break;
		case "LIST":
			$v = strlen($v)>0 ? $v : "[Unspecified]";
			break;
		case "PERC":
			$v = ASSIST_HELPER::format_percent($v,0);
		case "TEXT":
		default:
			$v = str_replace(chr(10),"<br />",ASSIST_HELPER::decode($v));
			break;
	}
		echo "
		<tr>
			<th width=150>".$h.":</th>
			<td>";
		if($types[$fld]!="ATTACH") {
			echo $v;
			if($can_edit_action_owner===true && $fld == "actionowner") {
				echo "<span class=float><input type=button value='Reassign' id=reassign_actionowner /></span>";
			}
		} else {
			$me->displayAttachments($resid);
		}
		echo "
			</td>
		</tr>";
}
?></table>
</div>
<?php

$me->displayGoBack($history);



if(isset($can_edit_action_owner) && $can_edit_action_owner === true) {
	?>
<div id=dlg_reassign title='Reassign <?php echo $headings['actionowner']; ?>'>
<form name=frm_edit>
	<table class=form>
		<tr>
			<th>Current:</th>
			<td><?php echo $res['resactionowner']; ?></td>
		</tr>
		<tr>
			<th>New:</th>
			<td><select name=resactionowner><?php
			foreach($actionowners as $key=>$ap) {
				if($key=="portfolio") {
					echo "<option value=X disabled=disabled>--- ".strtoupper($res['dept'])." ---</option>";
				} else {
					echo "<option value=X disabled=disabled>--- OTHER DEPARTMENTS ---</option>";
				}
				foreach($ap as $a) {
					echo "<option value='".$a['id']."'>".$a['name']."</option>";
				}
			}
			?></select></td>
		</tr>
    <tr>
		<th>Receive copy email?</th>
		<td ><select name=copyemail_reassign><option value=N selected>No</option><option value=Y>Yes</option></select></td>
	</tr>
	</table>
	<p class=float><input type=button value="Save Changes" class=isubmit #save /> <input type=button value=Cancel id=cancel /></p>
</form>
</div>
<script type=text/javascript>
$(function() {
	var resp_person = '<?php echo $res['responsible_person']; ?>';
	var resp_name = '<?php echo $headings['tkid'].": ".$res['restkid']; ?>';
	var action_owner_id = '<?php echo $res['action_owner']; ?>';
	var action_owner_name = '<?php echo $res['resactionowner']; ?>';
	if($("select[name=resactionowner] option[value="+resp_person+"]").text().length==0) {
		$("select[name=resactionowner]").prepend("<option value="+resp_person+">"+resp_name+"</option>");
	}
	$("select[name=resactionowner]").val(action_owner_id);
	var w = $("form[name=frm_edit] table").width()+50;
	$("#dlg_reassign").dialog({
		modal: true,
		autoOpen: false,
		width: w,
		height: 200,
	});
	$("form[name=frm_edit] input:button").button();
	$("form[name=frm_edit] input:button.isubmit").button().css("color","#009900");
	$("form[name=frm_edit] #cancel").click(function() {
		$("#dlg_reassign").dialog("close");
	});
	$("#reassign_actionowner").click(function() {
		$("#dlg_reassign").dialog("open");
		//alert(return_path+"?r[]=ok&r[]=test"+return_querystring);
	});
	$("form[name=frm_edit] input:button.isubmit").click(function() {
		//alert(resid+" => "+return_path+" => "+return_querystring);
		$("select[name=resactionowner]").removeClass("required");
		var ao = $("select[name=resactionowner]").val();
		if(ao=="X" || ao.length==0) {
			$("select[name=resactionowner]").addClass("required");
			alert("Please select a valid user as the Action Owner.");
		} else {
			var aon = $("select[name=resactionowner] option:selected").text();
			var copyemail = $("select[name=copyemail_reassign]").val();
			//alert(copyemail);
			var data = "i="+resid+"&new[resactionowner]="+ao+"&new[action_owner]="+aon+"&old[resactionowner]="+action_owner_id+"&old[action_owner]="+action_owner_name+"&copyemail="+copyemail;
			//alert(data);
			$.ajax({                                      
				url: 'ajax/manage_raps.php', 		  type: 'POST',		  data: "act=CHANGE_ACTIONOWNER&"+data,		  dataType: 'json', 
				success: function(d) {
					//console.log(d[1]);
					document.location.href = return_path+"?r[]="+d[0]+"&r[]="+d[1]+return_querystring;
				},
				error: function(d) { console.log(d); }
			});
		}
	});
});
</script>
	<?php
}
?>