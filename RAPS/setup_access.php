<?php
include("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

$rec_options = array(
	'N'=>"No",
	'C'=>"New Only",
	'A'=>"Action Only",
	'Y'=>"Both"
);

?>
<h1><a class=breadcrumb href=setup.php>Setup</a> >> User Access</h1>
<?php $me->displayResult($result); ?>
<form name=new_user>
<table class=list>
	<tr>
		<th rowspan=2></th>
		<th rowspan=2>User</th>
		<th rowspan=2>Department</th>
		<th colspan=5>Access</th>
		<th rowspan=2></th>
	</tr>
	<tr>
		<th class=th2 id=rec>Receive</th>
		<th class=th2>Add</th>
		<th class=th2>Edit All</th>
		<th class=th2>Update All</th>
		<th class=th2>Setup</th>
		<!-- <th class=th2>Receive<br />Copy<br />Email<br />Updates</th> -->
	</tr>
<?php
$deptlist = $me->getActiveDepartments();

$inactive_users = $me->getAllNewModuleUsers();
if(count($inactive_users)>0) {
	echo "
	<tr>
		<td></td>
		<td><select name=tkid><option selected value=X>--- SELECT ---</option>";
		foreach($inactive_users as $key=>$u) {
			echo "<option value=$key>$u</option>";
		}
echo "
		</select></td>
		<td><select name=deptid><option selected value=X>--- SELECT ---</option>";
		foreach($deptlist as $key=>$u) {
			echo "<option value=$key>$u</option>";
		}
echo "
		</select></td>
		<td class=center><select name=r><option selected value=N>No</option><option value=C>New Only</option><option value=A>Action only</option><option value=Y>Both</option></select></td>
		<td class=center><select name=a><option selected value=N>No</option><option value=Y>Yes</option></select></td>
		<td class=center><select name=e><option selected value=N>No</option><option value=Y>Yes</option></select></td>
		<td class=center><select name=u><option selected value=N>No</option><option value=Y>Yes</option></select></td>
		<td class=center><select name=s><option selected value=N>No</option><option value=Y>Yes</option></select></td>
		<!-- <td class=center><select name=m><option selected value=N>No</option><option value=Y>Yes</option></select></td> -->
		<td class=center><input type=hidden name=m value=N /><input type=button value=Add id=add /></td>
	</tr>";
}

$active_users = $me->getAllSetupModuleUsers();

foreach($active_users as $u) {
	echo "
	<tr>
		<td>".$u['tkid']."</td>
		<td>".$u['tkn']."</td>
		<td>".(isset($deptlist[$u['deptid']]) ? $deptlist[$u['deptid']] : "Unspecified")."</td>
		<td class=center>".($u['r']=="N" ? $me->drawStatus($u['r']) : $rec_options[$u['r']])."</td>
		<td>".$me->drawStatus($u['a'])."</td>
		<td>".$me->drawStatus($u['e'])."</td>
		<td>".$me->drawStatus($u['u'])."</td>
		<td>".$me->drawStatus($u['s'])."</td>
		<!-- <td>".$me->drawStatus($u['m'])."</td> -->
		<td class=center><input type=button value=Edit class=edit id=".$u['id']." /></td>
	</tr>";
}

?>
</table>
</form>

<script type=text/javascript>
$(function() {
	var w = $("#rec").width();
	$("form[name=new_user] table .th2").width(w);
	$("form[name=new_user] #add").click(function() { 
		$("select").removeClass("required");
		valid = true;
		err = "";
		if($("select[name=tkid]").val()=="X") {
			valid = false;
			err+="\n - User";
			$("select[name=tkid]").addClass("required");
		}
		if($("select[name=deptid]").val()=="X") {
			valid = false;
			err+="\n - Department";
			$("select[name=deptid]").addClass("required");
		}
		if(valid) {
			var data = "act=ADD";
			$("form[name=new_user] select").each(function() {
				data+= "&"+$(this).prop("name")+"="+$(this).val();
			});
			$.ajax({                                      
				url: 'ajax/setup_access.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: function(d) {
					document.location.href = 'setup_access.php?r[]='+d[0]+'&r[]='+d[1];
				},
				error: function(d) { console.log(d[0]); }
			});
		} else {
			alert("Please complete the missing information:"+err);
		}
	});
	$("input:button.edit").click(function() {
		var i = $(this).prop("id"); 
		var data = "act=getEDIT&i="+i;
			$.ajax({                                      
				url: 'ajax/setup_access.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: setEditForm,
				error: function(d) { console.log(d); }
			});
	});
	function setEditForm(d) {
//		console.log(d[1]);
		var form = "form[name=frm_edit]";
		$(form+" #user").text(d['tkid']);
		$(form+" select[name=deptid]").val(d['deptid']);
		$(form+" input:hidden[name=id]").val(d['id']);
		$(form+" select[name=r]").val(d['r']);
		$(form+" select[name=a]").val(d['a']);
		$(form+" select[name=e]").val(d['e']);
		$(form+" select[name=u]").val(d['u']);
		$(form+" select[name=s]").val(d['s']);
		//$(form+" select[name=m]").val(d['m']);
		
		$("#dlg_edit").dialog("open");
	}
});
</script>

<div title="Edit" id=dlg_edit>
<h1>Edit User</h1>
<form name=frm_edit>
<input type=hidden name=id value='' />
<table class=form id=edit_table>
	<tr>
		<th>User:</th>
		<td id=user></td>
	</tr>
	<tr>
		<th>Department:</th>
		<td><select name=deptid>
		<option value=X selected>--- SELECT ---</option><?php
		foreach($deptlist as $key=>$d) {
			echo "<option value=$key>$d</option>";
		}
		?></select></td>
	</tr>
	<tr>
		<th>Access:</th>
		<td>
			<table class=form width=100%>
				<tr>
					<th class=th2 >Receive:</th>
					<td><select name=r><option value=N>No</option><option value=C>New Only</option><option value=A>Action Only</option><option value=Y>Both</option></select></td>
				</tr>
				<tr>
					<th class=th2>Add:</th>
					<td><select name=a><option value=N>No</option><option value=Y>Yes</option></select></td>
				</tr>
				<tr>
					<th class=th2>Edit All:</th>
					<td><select name=e><option value=N>No</option><option value=Y>Yes</option></select></td>
				</tr>
				<tr>
					<th class=th2>Update All:</th>
					<td><select name=u><option value=N>No</option><option value=Y>Yes</option></select></td>
				</tr>
				<tr>
					<th class=th2>Setup:</th>
					<td><select name=s><option value=N>No</option><option value=Y>Yes</option></select></td>
				</tr>
				<!-- <tr>
					<th class=th2>Receive Copy<br />Email Updates:</th>
					<td><select name=m><option value=N>No</option><option value=Y>Yes</option></select></td>
				</tr> -->
			</table>
		</td>
	</tr>
</table>
<p class=float><input type=button value="Save Changes" class=isubmit /> <input type=button value=Cancel id=cancel /></p>
</form>
</div>
<script type=text/javascript>
$(function() {
	var w = $("form[name=frm_edit] #edit_table").width()+50;
	//alert(w);
	$("#dlg_edit").dialog({
		modal: true,
		width: w,
		autoOpen: false
	});
	$("form[name=frm_edit] input:button").button();
	$("input:button.isubmit").button().css("color","#009900");
	$("form[name=frm_edit] #cancel").click(function() {
		$("#dlg_edit").dialog("close");
	});
	$("form[name=frm_edit] input:button.isubmit").click(function() {
			var data = "act=EDIT&id="+$("form[name=frm_edit] input:hidden[name=id]").val();
			$("form[name=frm_edit] select").each(function() {
				data+= "&"+$(this).prop("name")+"="+$(this).val();
			});
			$.ajax({                                      
				url: 'ajax/setup_access.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: function(d) {
					document.location.href = 'setup_access.php?r[]='+d[0]+'&r[]='+d[1];
				},
				error: function(d) { console.log(d); }
			});
	});
});
</script>
</body>

</html>
