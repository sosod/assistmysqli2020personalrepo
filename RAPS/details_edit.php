<?php


$req = $_REQUEST;
unset($req['id']);
unset($req['src']);
unset($req['jquery_version']);
unset($req['r']);
$history = "";
foreach($req as $key => $val) {
	$history.="&".$key."=".$val;
}
$ajax_path = $self.".php";
$ajax_querystring = $history."&id=".$_REQUEST['id']."&src=".$_REQUEST['src'];
$history = $_REQUEST['src'].$history;

$resid = $_REQUEST['id'];
$res = $me->getAResolution($resid);

$headings = $me->getAllHeadings();
$types = $me->getHeadingTypes();

$actionowners = $me->getActionOwners();
$status = $me->getActiveStatus();
$urgency = $me->getActiveUrgency();
$users = $me->getUsersToReceiveNew();
$port = $me->getActivePortfolios();

if(isset($_REQUEST['r']) && is_array($_REQUEST['r'])) {
	$me->displayResult($_REQUEST['r']);
}
/*
$res2 = $res;
	$res2['tkid'] = $res2['restkid'];
	$res2['restkid'] = $res2['responsible_person'];
	$res2['actionowner'] = $res2['resactionowner'];
	$res2['resactionowner'] = $res2['action_owner'];
	$res2['urgency'] = $res2['resurgencyid'];
	$res2['resurgencyid'] = $res2['urgencyid'];
	$res2['urgencyid'] = $res2['urgency'];
	$res2['portfolio'] = $res2['resportfolioid'];
	$res2['resportfolioid'] = $res2['portfolioid'];
	$res2['portfolioid'] = $res2['portfolio'];
	$res2['status'] = $res2['resstatusid'];
	$res2['resstatusid'] = $res2['statusid'];
	$res2['statusid'] = $res2['status'];

$me->arrPrint($res2);
$me->arrPrint($res);
*/
?>

<div class=float  style='background-color: #ffffff; width: 50%;'><p>&nbsp;</p><?php 

$me->displayActivityLog($resid); 

?><p>&nbsp;</p></div>
<div class=float  style='background-color: #ffffff; width: 50%; margin-bottom: 10px;'>
<h2>Details</h2>
<p><i>Please note that all fields are required unless otherwise indicated.</i></p>
<form name=frm_edit method=post action="ajax/upload_attachment.php" language=jscript enctype="multipart/form-data">
<input type=hidden value="<?php echo $resid; ?>" name=resid id=resid />
<input type=hidden value="EDIT" name=act  />
<input type=hidden value="RES" name=attach_type  />
<table class=form width=99% style='margin-right: 1%'>
	<tr>
		<th id=resid><?php echo $headings['id']; ?>:</th>
        <td><?php echo RAPS::REFTAG.$resid; ?></td>
	</tr>
	<tr>
		<th id=resref><?php echo $headings['ref']; ?>:</th>
        <td><input type=text name=resref maxlength=30 value='<?php echo $res['resref']; ?>' /></td>
	</tr>
	<tr>
		<th id=restkid><?php echo $headings['tkid']; ?>:</th>
		<td>
            <select name="restkid"><?php
				if(!isset($users[$res['responsible_person']])) {
					echo "<option disabled=disabled selected value=".$res['responsible_person'].">".$res['restkid']."</option>";
				}
				foreach($users as $key=>$u) {
					echo "<option ".($key==$res['responsible_person'] ? "selected" : "")." value='".$key."' deptid=".$u['deptid'].">".$me->decode($u['display'])."</option>";
				}
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<th id=restkid><?php echo $headings['actionowner']; ?>:</th>
		<td>
            <select name="resactionowner"><?php
				if(!isset($actionowners[$res['action_owner']])) {
					echo "<option disabled=disabled selected value=".$res['action_owner'].">".$res['resactionowner']."</option>";
				}
				foreach($actionowners as $key=>$u) {
					echo "<option ".($key==$res['action_owner'] ? "selected" : "")." value='".$key."' deptid=".$u['deptid'].">".$me->decode($u['display'])."</option>";
				}
                ?>
            </select>
        </td>
	</tr>
	<tr>
		<th><?php echo $headings['adduser']; ?>:</th>
		<td><?php echo $res['resadduser']; ?></td>
	</tr>
	<tr>
		<th><?php echo $headings['adddate']; ?>:</th>
		<td><?php echo date("d-M-Y",$res['resadddate']); ?><input type=hidden size=5 name=resadduser value="<?php echo($me->getUserID()); ?>"></td>
	</tr>
	<tr>
		<th id=resportfolioid><?php echo $headings['portfolioid']; ?>:</th>
		<td>
            <select name="resportfolioid"><?php
				foreach($port as $key=>$p) {
					echo "<option ".($key==$res['portfolioid'] ? "selected" : "")." value=".$key." class=D".$p['deptid'].">".$p['display']."</option>";
				}
            ?></select>
        </td>
	</tr>
	<tr>
		<th id=resurgencyid><?php echo $headings['urgencyid']; ?>:</th>
		<td>
            <select name="resurgencyid"><?php
				foreach($urgency as $key=>$v) {
					echo "<option ".($key==$res['urgencyid'] ? "selected" : "")." value=$key>$v</option>";
				}
            ?></select>
        </td>
	</tr>
	<tr>
		<th id=resstatusid><?php echo $headings['statusid']; ?>:</td>
		<td>
            <select name="resstatusid"><?php
				foreach($status as $key=>$v) {
					echo "<option ".($key==$res['statusid'] ? "selected" : "")." value=$key>$v</option>";
				}
            ?></select>
        </td>
	</tr>
	<tr>
		<th id=resref><?php echo $headings['state']; ?>:</th>
        <td><input type=text name=resstate maxlength=3 size=5 class=right value='<?php echo $res['resstate']; ?>' />%  <input type=hidden name=oldstate value='<?php echo $res['resstate']; ?>' /></td>
	</tr>
	<tr>
        <th id=resmeetingdate><?php echo $headings['meetingdate']; ?>:</th>
		<td><input type="text" name="resmeetingdate" size=10 class=jdate2012 value='<?php echo date("d-M-Y",$res['resmeetingdate']); ?>' /></td>
	</tr>
	<tr>
		<th id=resdeadline><?php echo $headings['deadline']; ?>:</th>
        <td><input type="text" name="resdeadline" size=10 class=jdate2012 value='<?php echo date("d-M-Y",$res['resdeadline']); ?>' /></td>
	</tr>
	<tr>
		<th id=resaction><?php echo $headings['action']; ?>:</th>
        <td><textarea name="resaction"><?php echo $me->decode($res['resaction']); ?></textarea></td>
	</tr>
	<tr>
		<th id=resnotes><?php echo $headings['notes']; ?>:<br /><span class=i>(Not required)</span></th>
        <td><textarea name="resnotes"><?php echo $me->decode($res['resnotes']); ?></textarea></td>
	</tr>
    <tr>
		<th>Attach document(s):</th>
		<td>
			<?php
			$me->displayAttachmentForm($history);
			$me->displayAttachments($resid, $can_edit_attach);
			?>
		</td>
	</tr>
    <tr>
		<th>Receive copy email?</th>
		<td ><select name=copyemail><option value=N selected>No</option><option value=Y>Yes</option></select></td>
	</tr>
	<tr>
		<th></th>
		<td><input type="button" value="Save Changes" class=isubmit />
		<input type="reset" value="Reset" name="B2" />
		<span class=float><input type=button value="Delete Resolution" class=idelete id=delete /></td>
	</tr>
</table>
<?php 
$me->displayGoBack($history);
//$me->displayActivityLog($resid);
//$me->displayGoBack($history);
?>
</form>
</div>
<!-- div to display dialog when required fields are missing -->
<div id=dlg_error title="Required fields">
<h1 class=red>Error</h1>
<p>Please complete the following required fields:</p>
<ul id=ul_req>
</ul>
</div>
<div id=dlg_loading>
<p class=center>Please wait while your form is processed...</p>
<p class=center><img src="../pics/ajax_loader_v2.gif" /></p>
</div>
<div id=dlg_response>
<?php ASSIST_HELPER::displayResult(array("ok","")); ?>
</div>
<div id=dlg>
</div>
<style type=text/css>
.green { border: 1px solid #009900; color: #009900; }
.green:hover { border: 1px solid #fe9900; color: #fe9900; }
</style>
<script type=text/javascript>
$(function() {
	var resid = '<?php echo $resid; ?>';
	var form = "form[name=frm_edit]";
	var notRequired = new Array("resnotes");
	var history = '<?php echo $history; ?>';
	//$("select[name=resstatusid]").val($("select[name=resstatusid] option:first").val());
	/*$("select[name=restkid]").change(function() {
		var d = $("select[name=restkid] option:selected").attr("deptid");
		if($("select[name=resportfolioid] option.D"+d).size()>0) {
			$("select[name=resportfolioid] option.D"+d+":first").prop("selected","selected");
		} else {
			$("select[name=resportfolioid] option:first").prop("selected","selected");
		}
	});*/
	
	if($("select[name=resstatusid]").val()=="100") { $("input[name=resstate]").prop("disabled","disabled"); }
	
	$("select[name=resstatusid]").change(function() {
		if($(this).val()=="CL") {
			$("input[name=resstate]").val("100").prop("disabled","disabled");
		} else {
			$("input[name=resstate]").val($("input[name=oldstate]").val()).prop("disabled","");
		}
	});
	$("input[name=resstate]").blur(function() {
		if($(this).val()=="100") {
			$("select[name=resstatusid]").val("CL");
			$(this).prop("disabled","disabled");
		}
	});

	
	$("li.hover").hover(function() { $(this).css("background-color","#d8d8d8");},function() {$(this).css("background-color","");});
	
	$("span.delete_attach").click(function() {
		var key = $(this).attr("key");
		var name = $("#"+key).text();
		if(confirm("Are you sure you wish to delete the attachment: \n"+name)) {
			$.ajax({                                      
				url: 'ajax/delete_attachment.php', 		  type: 'POST',		  data: "resid="+resid+"&id="+key,		  dataType: 'json', 
				success: function(d) { 
					var result = d[0];
					var response = d[1];
					$("#dlg").html($("#dlg_response").html());
					var r = $("#dlg p").html()+response;
					$("#dlg p").html(r);
					openDialog(dlgResponseOpts);
					//$("#dlg").dialog("option",dlgResponseOpts).dialog("open");
					//$(".ui-dialog :button").blur();
					//$(".ui-dialog :button:last").css({"color":"#009900","border":"1px solid #009900"});
					$("#li_"+key).hide();
				},
				error: function(d) { console.log(d); }
			});
		}
	});
	
	function openDialog(options) {
					$("#dlg").dialog("option",options).dialog("open");
					$(".ui-dialog :button").blur();
					$(".ui-dialog :button:last").css({"color":"#009900","border":"1px solid #009900"});
	}
	
	var dlgErrorOpts = ({
		modal: true,
		autoOpen: false,
		width: 300,
		height: 320,
		buttons:[{
			text: "Ok",

			click: function() { $(this).dialog("close"); }
		}]
	});
	var dlgLoadingOpts = ({
		modal: true,
		autoOpen: false,
		width: 250,
		height: 270
	});
	var dlgResponseOpts = ({
		modal: true,
		autoOpen: false,
		width: 300,
		height: 150,
		buttons:[{
			text: "Ok",
			click: function() { $(this).dialog("close"); }
		}]
	});
	$("#dlg").dialog({modal: true,autoOpen: false});
	$(".ui-dialog-titlebar").hide();
	$("#dlg_error, #dlg_loading, #dlg_response").hide();
	
	$(form+" input:button.isubmit").click(function() { 
		$("#dlg").html($("#dlg_loading").html());
		$("#dlg").dialog("option",dlgLoadingOpts).dialog("open");
		var resid = 0;
		var response = "";
		var result = "ok";
		var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9");
	
		var valid = true;
		$("#dlg_error #ul_req li").remove();
		var d = "";
		$("select, input:text, textarea").removeClass("required").each(function() {
			var $me = $(this);
			var n = $me.prop("name");
			d = $me.is("textarea") ? $me.text() : $me.val();
			if(d.length==0) {
				d = $me.is("textarea") ? $me.val() : $me.text();
			}
			if($.inArray(n,notRequired)<0) {
				if(d.length==0 || ($me.is("select") && d=="X")) {
					$me.addClass("required");
					var i = $("#"+n).html();
					i = i.replace(":","");
					$("#dlg_error #ul_req").append("<li>"+i+"</li>");
					valid = false;
				} else if(n=="resstatusid" && d=="IP") {
					if($("input[name=resstate]").val()=="0" || $("input[name=resstate]").val()=="100") {
						$("input[name=resstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must be between 1% - 99% if status is In Progress.</li>");
					valid = false;
					}
				} else if(n=="resstatusid" && d=="CL") {
					if($("input[name=resstate]").val()!="100") {
						$("input[name=resstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must equal 100% if status is Completed.</li>");
					valid = false;
					}
				} else if(n=="resstatusid" && d=="NW") {
					if($("input[name=resstate]").val()!="0") {
						$("input[name=resstate]").addClass("required");
						$("#dlg_error #ul_req").append("<li>Progress must equal 0% if status is New.</li>");
					valid = false;
					}
				} else if(n=="resstate") {
					var v;
					var valid_perc = true;
					for(i=0;i<d.length;i++) {
						v = d.charAt(i);
						if($.inArray(v,validNumbers)<0) {
							$me.addClass("required");
							$("#dlg_error #ul_req").append("<li>Progress can only contain a valid number (0-9).</li>");
							i = d.length+1;
							valid_perc = false;
					valid = false;
						}
					}
					if(valid_perc) {
						var vp = d*1;
						if(vp>100 || vp<0) {
							$me.addClass("required");
							$("#dlg_error #ul_req").append("<li>Progress cannot be less than 0% or greater than 100%.</li>");
					valid = false;
						}
					}
				}
			}
		});
		if(valid) {
			//alert("processing form");
			//check for files to be uploaded
			var f = 0;
			$("input:file").each(function() {
				f+=$(this).val().length;
			});
			$("input[name=resstate]").prop("disabled","");
			$.ajax({                                      
				url: 'ajax/manage_raps.php', 		  type: 'POST',		  data: $(form).serialize(),		  dataType: 'json', 
				success: function(d) { 
					result = d[0]; 
					response = d[1];
					//alert(response);
					$("#result").val(d[0]);
					$("#response").val(d[1]);
					if(f>0) {
						$(form).prop("target","file_upload_target");
						$(form).submit();
					} else {
						document.location.href = history+'&r[]='+result+'&r[]='+response;
					}
					//$("#dlg").dialog("close");
				},
				error: function(d) { console.log(d); }
			});
		} else {
			$("#dlg").dialog("close");
			$("#dlg").html($("#dlg_error").html());
			openDialog(dlgErrorOpts);
			//$("#dlg").dialog("option",dlgErrorOpts).dialog("open");
			//$('.ui-dialog :button').blur();
			//$(".ui-dialog :button:last").css({"color":"#009900","border":"1px solid #009900"});
		}
	});
	$("#delete").click(function() {
		if(confirm("Are you sure you wish to delete this resolution?")==true) {
			var i = $("#resid").val();
			$.ajax({                                      
				url: 'ajax/manage_raps.php', 		  type: 'POST',		  data: "act=DELETE&resid="+i,		  dataType: 'json', 
				success: function(d) { 
					result = d[0]; 
					response = d[1];
					document.location.href = history+'&r[]='+result+'&r[]='+response;
				},
				error: function(d) { console.log(d); }
			});
		}
	});
});
</script>
