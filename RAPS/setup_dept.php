<?php
include("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

$deptlist = $me->getActiveDepartments();

?>
<h1><a class=breadcrumb href=setup.php>Setup</a> >> Departments</h1>
<?php $me->displayResult($result); ?>
<form name=new>
<input type=hidden name=act value=ADD />
<table class=list>
    <tr>
        <th>ID</th>
        <th>Department</th>
        <th>&nbsp;</th>
    </tr>
    <tr>
        <td>&nbsp;</td>
		<td><input type=text name=value /></td>
        <td><input type=button value=Add id=add /></td>
    </tr>
</form>
<?php
//GET CURRENT Portfolios DETAILS AND DISPLAY


foreach($deptlist as $key => $d) {
	echo "
		<tr>
			<td>".$key."</td>
			<td>".$d."</td>
			<td><input type=button value=Edit class=edit id=".$key." /></td>
		</tr>
	";
}
?>
</table>


<script type=text/javascript>
$(function() {
	$("form[name=new] #add").click(function() {
		var form = "form[name=new]";
		var res = validate(form);
		var valid = res[0];
		var err = res[1];
		if(valid) {
			var data = "act=ADD";
			$(form+" input:text").each(function() {
				data+= "&"+$(this).prop("name")+"="+encodeURIComponent($(this).val());
			});
			//data = $(form).serialize();
			$.ajax({                                      
				url: 'ajax/setup_dept.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: function(d) {
					document.location.href = 'setup_dept.php?r[]='+d[0]+'&r[]='+d[1];
				},
				error: function(d) { console.log(d); }
			});
			//alert(data);
		} else {
			alert("Please complete the missing information:"+err);
		}
	});
	$("input:button.edit").click(function() {
		var i = $(this).prop("id");
		var data = "act=getEDIT&i="+i;
			$.ajax({                                      
				url: 'ajax/setup_dept.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: setEditForm,
				error: function(d) { console.log(d); }
			});
	});
	function setEditForm(d) {
		var form = "form[name=frm_edit]";
		$(form+" input:hidden[name=id]").val(d['id']);
		$(form+" input:text[name=value]").val(d['value']);
		
		$("#dlg_edit").dialog("open");
	}
	function validate(form) {
		$(form+" select, "+form+" input:text").removeClass("required");
		valid = true;
		err = "";
		if($(form+" input:text[name=value]").val().length == 0) {
			valid = false;
			err+="\n - Department name";
			$(form+" input:text[name=value]").addClass("required");
		}
		return new Array(valid,err);
	}
	
	
	//EDIT FUNCTIONS
	
	$("input:text").prop("maxlength","45").prop("size","50");
	var w = $("form[name=frm_edit] #edit_table").width()+50;
	//alert(w);
	$("#dlg_edit").dialog({
		modal: true,
		width: w,
		autoOpen: false
	});
	$("form[name=frm_edit] input:button").button();
	$("input:button.isubmit").button().css("color","#009900");
	$("input:button.idelete").button().css({"color":"#cc0001","margin-left":"50px"});
	$("form[name=frm_edit] #cancel").click(function() {
		$("#dlg_edit").dialog("close");
	});
	$("form[name=frm_edit] input:button.isubmit").click(function() {
		var form = "form[name=frm_edit]";
		var res = validate(form);
		var valid = res[0];
		var err = res[1];
		if(valid) {
			var data = "act=EDIT&id="+$("form[name=frm_edit] input:hidden[name=id]").val();
			$(form+" input:text").each(function() {
				data+= "&"+$(this).prop("name")+"="+encodeURIComponent($(this).val());
			});
			$.ajax({                                      
				url: 'ajax/setup_dept.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: function(d) {
					document.location.href = 'setup_dept.php?r[]='+d[0]+'&r[]='+d[1];
				},
				error: function(d) { console.log(d); }
			});
		} else {
			alert("Please complete the missing information:"+err);
		}
	});
	$("form[name=frm_edit] input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to delete this Department?")==true) {
			var data = "act=DELETE&id="+$("form[name=frm_edit] input:hidden[name=id]").val();
			$.ajax({                                      
				url: 'ajax/setup_dept.php', 		  type: 'POST',		  data: data,		  dataType: 'json', 
				success: function(d) {
					document.location.href = 'setup_dept.php?r[]='+d[0]+'&r[]='+d[1];
				},
				error: function(d) { console.log(d); }
			});
		}
	});
	
		
});
</script>

<div id=dlg_edit title=Edit>
<h1>Edit Department</h1>
<form name=frm_edit>
<input type=hidden name=id value='' />
<table class=form id=edit_table>
	<tr>
		<th>Department:</th>
		<td><input type=text name=value /></td>
	</tr>
</table>
<p class=float><input type=button value="Save Changes" class=isubmit /> <input type=button value=Cancel id=cancel /> <input type=button value=Delete class=idelete /></p>
</form>
</div>

<script type=text/javascript>
$(function() {

	
});
</script>

</body>

</html>
