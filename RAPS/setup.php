<?php
include("inc_header.php");

$result = array();

$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "";
if(strlen($act)>0 && $act =="UPDATE_ADMIN") {
	$a = isset($_REQUEST['admin']) ? $_REQUEST['admin'] : "";
	if(strlen($a)>0) {
		$result = $me->updateModuleAdmin($a);
	} else {
		$result = array("error","An error occurred while trying to update the Module Administrator.  Please try again.");
	}
}

$users = array('0000'=>"Assist Administrator");
$users+=$me->getAllModuleUsers();


?>
<h1>Setup</h1>
<style type=text/css>
table.form th, table.form td {
	vertical-align: middle;
}
</style>
<?php $me->displayResult($result); ?>
<form name=frm_update method=post action=setup.php>
	<input type=hidden name=act value='UPDATE_ADMIN' />
<table class=form>
<!--	<tr>
		<th>Setup Administrator:</th>
		<td>Define who can access the Setup functions: <span class=float style='vertical-align: middle;'>&nbsp;&nbsp;<select name=admin>
<?php
foreach($users as $key => $value) {
	echo "<option ".($me->isModuleAdmin($key)===true ? "selected" : "")." value='$key'>$value</option>";
}

?>
</select> <input type=button id=admin value=Save class=isubmit /></span></td>
	</tr>
<!--	<tr>
		<th>User Access:</th>
		<td >Define the access each user has to the module. <input type=button id=setup_access value=Configure class=float /></td>
	</tr> -->
<?php $ds = $me->getDepartmentSource(); 
if($ds=="MODULE") { ?>
	<tr>
		<th style='padding-right: 20px;'>Departments:</th>
		<td style='vertical-align: middle;'><input type=button id=setup_dept value=Configure class=float style='margin-left: 20px;' />Define the departments to be used in the module. </td>
	</tr>
<?php } ?>
	<tr>
		<th style='padding-right: 20px;'>Portfolios:</th>
		<td style='vertical-align: middle;'><input type=button id=setup_portfolio value=Configure class=float style='margin-left: 20px;' />Define the portfolios to be used in the module. </td>
	</tr>
	<tr>
		<th style='padding-right: 20px;'>Data Report:</th>
		<td style='vertical-align: middle;'><input type=button id=setup_datareport value=View class=float style='margin-left: 20px;' />View the data usage in this module. </td>
	</tr>
</table>
</form>
<script type=text/javascript>
$(function() {
	$("input:button").click(function() {	
		var i = $(this).prop("id");
		switch(i) {
			case "admin":
				$("form[name=frm_update]").submit();
				break;
			case "setup_dept":
			case "setup_datareport":
			case "setup_portfolio":
				document.location.href = i+".php";
				break;
			default: alert("An error occurred.  Please reload the page and try again.");
		}
	});
});
</script>

</body>

</html>
