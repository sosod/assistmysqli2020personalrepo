<?php //error_reporting(-1);
include_once("RAPS/class/raps_helper.php");
include_once("RAPS/class/raps.php");

				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'resdeadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'restopicid'=>array('text'=>"Portfolio", 'long'=>false, 'deadline'=>false),
					'resstatusid'=>array('text'=>"Status", 'long'=>false, 'deadline'=>false),
					'resaction'=>array('text'=>"Instructions", 'long'=>true, 'deadline'=>false),
					'resmeetingdate'=>array('text'=>"Meeting Date", 'long'=>false, 'deadline'=>true),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

				
					$sql = "SELECT t.*, tkname, tksurname, top.value as topic, stat.value as status ";
					$sql.= "FROM ".$dbref."_res t";
					$sql.= ", ".$dbref."_list_portfolio top";
					$sql.= ", ".$dbref."_list_status stat";
					$sql.= ", assist_".$cmpcode."_timekeep ";
					$sql.= "WHERE t.resadduser = tkid ";
					$sql.= "AND (t.restkid = '$tkid' OR t.resactionowner = '$tkid') ";
					$sql.= "AND t.resportfolioid = top.id ";
					$sql.= "AND t.resstatusid = stat.id ";
					$sql.= "AND t.resstatusid NOT IN ('CL','CN') AND t.resdeadline <= ($today + 3600*24*$next_due) ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.resdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.resdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);				
					$res = $my_assist->mysql_fetch_all($sql);
					foreach($res as $task) {
						$obj_id = $task['resid'];
						$actions[$obj_id]['ref'] = $obj_id;
									if($task['resstatusid']!=5) {
										$deaddiff = $today - $task['resdeadline'];
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$d = "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff==0) {
											$d =  "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											$d =  "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									}
						$actions[$obj_id]['resdeadline'] = $d;
								foreach($head as $fld => $h) {
									$val = $task[$fld];
									$style = "";
									switch($fld) {
										case "resstatusid":
											$val = $task['status'];
											if($task['resstate']>=0) { $val.="<br />(".$task['resstate']."%)"; }
											break;
										case "restopicid":
											$val = $task['topic'];
											break;
										case "resadduser":
											$val = $task['tkname']." ".$task['tksurname'];
											break;
										case "resmeetingdate":
										case "resdeadline":
											$val = date("d M Y",$val);
											break;
										case "ref":
											$val = RAPS::REFTAG.$task['resid'];
											break;
										default:
											$val = $my_assist->decode($val);
											if(strlen($val)>100) { $val = strFn("substr",$val,0,75)."..."; }
											$val = str_replace(chr(10),"<br />",$val);
											break;
									}
									$actions[$obj_id][$fld] = $val;
								}
							$actions[$obj_id]['link'] = "view_res_update.php?i=".$task['resid'];
				}
/*				$head = array(
					'restopicid'=>array(
						'headingshort'=>"Portfolio",
					),
					'resstatusid'=>array(
						'headingshort'=>"Status",
					),
					'resaction'=>array(
						'headingshort'=>"Instructions",
					),
					'resmeetingdate'=>array(
						'headingshort'=>"Meeting Date",
					)
				);
				/*$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND field in ('tasktopicid','taskstatusid','taskaction','taskdeliver') ORDER BY mysort";
				$rs = getRS($sql);
					while($row = mysql_fetch_array($rs)) {
						$head[$row['field']] = $row;
					}
				mysql_close();*//*
				echo "<table cellpadding=3 cellspacing=0  width='740'>";
					echo "<tr>";
						echo "<th>Ref</th>";
						echo "<th>Deadline</th>";
						foreach($head as $h) {
							echo "<th>".$h['headingshort']."</th>";
						}
						echo "<th>&nbsp;</th>";
					echo "</tr>";
					$sql = "SELECT t.*, tkname, tksurname, top.value as topic, stat.value as status ";
					$sql.= "FROM ".$dbref."_res t";
					$sql.= ", ".$dbref."_list_portfolio top";
					$sql.= ", ".$dbref."_list_status stat";
					$sql.= ", assist_".$cmpcode."_timekeep ";
					$sql.= "WHERE t.resadduser = tkid ";
					$sql.= "AND t.restkid = '$tkid' ";
					$sql.= "AND t.resportfolioid = top.id ";
					$sql.= "AND t.resstatusid = stat.id ";
					$sql.= "AND t.resstatusid NOT IN ('CL','CN') AND t.resdeadline <= ($today + 3600*24*7) ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.resdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.resdeadline ASC "; break;
					}
					$sql.= " LIMIT ".$action_profile['field2'];
//t.resdeadline ASC LIMIT 20";
					$rs = getRS($sql);
					if(mysql_num_rows($rs)==0) {
						echo "<tr>";
							echo "<td colspan=".(count($head)+3).">Nothing due for the next 7 days.</td>";
						echo "</tr>";
					} else {
						$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
						while($task = mysql_fetch_array($rs)) {
							echo "<tr>";
								echo "<th>".$task['resid']."</th>";
								echo "<td>";
									if($task['resstatusid']!=5) {
										$deaddiff = $today - $task['resdeadline'];
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											echo "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff==0) {
											echo "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											echo "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									}
								echo"</td>";
								foreach($head as $fld => $h) {
									//$fld = $h['field'];
									$val = $task[$fld];
									$style = "";
									switch($fld) {
										case "resstatusid":
											$val = $task['status'];
											if($task['resstate']>=0) { $val.="<br />(".$task['resstate']."%)"; }
											break;
										case "restopicid":
											$val = $task['topic'];
											break;
										case "resadduser":
											$val = $task['tkname']." ".$task['tksurname'];
											break;
										case "resmeetingdate":
											$val = date("d M Y",$val);
											break;
										default:
											$val = decode($val);
											if(strlen($val)>100) { $val = strFn("substr",$val,0,75)."..."; }
											$val = str_replace(chr(10),"<br />",$val);
											break;
									}
									echo "<td style=\"".$style."\">".$val."</td>";
								}
							//On button click, call function viewTask & pass variables ($mod, $modloc, page_address)
							echo "<td class=\"center middle\"><input type=button value=View onclick=\"viewTask('$mod','$modloc','view_res_update.php?i=".$task['resid']."');\"></td>";
							echo "</tr>";
						}	//while end
					}	//end mnr if
					mysql_close();
				echo "</table>"; */
?>