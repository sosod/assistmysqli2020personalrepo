<?php
//include("inc.php");
include "../../module/autoloader.php";
$me = new RAPS();


$id = $_REQUEST['id'];
$a = $me->getAnAttachment($id);

$resid = $_REQUEST['resid'];
$res = $me->getAResolution($resid);

$folder = "";
$location = explode("/",$_SERVER["REQUEST_URI"]);
$l = count($location)-2;
for($f=0;$f<$l;$f++) {
	$folder.= "../";
}
$activefolder=$folder."files/".$me->getCmpCode()."/RAPS";
$deletefolder=$folder."files/".$me->getCmpCode()."/RAPS/deleted";
$me->checkFolder("RAPS/deleted");				

$file = $activefolder."/".$a['system_filename'];
if(file_exists($file)) {
	$newfile = $deletefolder."/deleted_".date("YmdHis")."_".$a['system_filename'];
	if(copy($file,$newfile)){
		unlink($file);
	}
}

$trans = "Deleted attachment: ".$a['original_filename'];
$me->updateRes("E",$res,$trans);



echo json_encode(array("ok","Attachment '".$a['original_filename']."' has been deleted."));//.$_SERVER['REQUEST_URI']);



?>