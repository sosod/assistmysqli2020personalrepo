<?php


//include("inc.php");
include "../../module/autoloader.php";
$me = new RAPS();

$var = $_REQUEST;
$copyemail = isset($_REQUEST['copyemail']) && $_REQUEST['copyemail']=="Y" ? true : false;

$resid = isset($_REQUEST['resid']) ? $_REQUEST['resid'] : $_REQUEST['i'];
$res = $me->getAResolution($resid);
	$res['tkid'] = $res['restkid'];
	$res['restkid'] = $res['responsible_person'];
	$res['actionowner'] = $res['resactionowner'];
	$res['resactionowner'] = $res['action_owner'];
	$res['urgency'] = $res['resurgencyid'];
	$res['resurgencyid'] = $res['urgencyid'];
	$res['urgencyid'] = $res['urgency'];
	$res['portfolio'] = $res['resportfolioid'];
	$res['resportfolioid'] = $res['portfolioid'];
	$res['portfolioid'] = $res['portfolio'];
	$res['status'] = $res['resstatusid'];
	$res['resstatusid'] = $res['statusid'];
	$res['statusid'] = $res['status'];

switch($var['act']) {
	case "CHANGE_ACTIONOWNER": 	$result = updateActionOwner($var,$resid,$res); 	break;
	case "SAVE_UPDATE": 		$result = saveUpdate($var,$resid,$res); 		break;
	case "EDIT":		 		$result = editResolution($var,$resid,$res);		break;
	case "DELETE":		 		$result = deleteResolution($var,$resid,$res);	break;
}

$result[1] = urlencode($result[1]);
echo json_encode($result);




function updateActionOwner($var,$resid,$res) {
	global $me;
	global $copyemail;

	$new = array('id'=>$var['new']['resactionowner'],'name'=>$var['new']['action_owner']);
	$old = array('id'=>$var['old']['resactionowner'],'name'=>$var['old']['action_owner']);
	
	$sql = "UPDATE ".$me->getDBRef()."_res SET resactionowner = '".$new['id']."' WHERE resid = ".$resid;
	$mar = $me->db_update($sql);
	
	$trans = "Reassigned Action Owner to '".$new['name']."' from '".$old['name']."'.";
	
	$me->updateRes("E",$res,$trans);
	
	$me->sendResolutionNotification("ACTION_OWNER",$resid,array('copyemail'=>$copyemail));
	
	return array("ok",$trans);
}


function saveUpdate($var,$resid,$res) {
	global $me;
	global $copyemail;
	
	$data = array(
		'date'		=>	strtotime(date("d F Y H:i:s")),
		'tkid'		=>	$me->getUserID(),
		'update'	=>	$me->code($var['logupdate']),
		'statusid'	=>	$var['logstatusid'],
		'state'		=>	$var['logstatusid']=="CL" ? "100" : $var['logstate'],
		'resid'		=>	$resid,
		'deadline'	=>	$res['resdeadline'],
		'action'	=>	"U",
		'actdate'	=>	strtotime($var['logactdate'].date("H:i:s")),
	);

	//CREATE NEW RESOLUTION RECORD
	$x = array_keys($data);
	$sql = "INSERT INTO ".$me->getDBRef()."_log (log".implode(", log",$x).") VALUES ('".implode("', '",$data)."')";
	//$response[1] = $sql;
	$id = $me->db_insert($sql);
	
	$sql = "UPDATE ".$me->getDBRef()."_res SET resstatusid = '".$data['statusid']."', resstate = '".$data['state']."' WHERE resid = ".$data['resid'];
	$me->db_update($sql);

	//SEND NOTIFICATION
	$logdata = array(
		'logupdate'=>$me->decode($data['update']),
		'logstatusid'=>$data['statusid'],
		'logstate'=>$data['state'],
		'logactdate'=>date("d M Y",$data['actdate']),
		'copyemail'=>$copyemail,
	);
	$me->sendResolutionNotification("UPDATE",$resid,$logdata);

			
	//SET RESPONSE		
	$response[0] = "ok";
	$response[1] = "Resolution ".RAPS::REFTAG.$resid." has been successfully updated.  ";
	$response[2] = $id;


	
	
	
	
	
	return $response;
}






function editResolution($var,$resid,$res) {
	global $me;
	global $copyemail;
	
	$headings = $me->getAllHeadings();
	$types = $me->getHeadingTypes();
	
	$response[0] = "info";
	$response[1] = "No changes could be found for Resolution ".RAPS::REFTAG.$resid.".";
	
	
	

	$data = array(
		'ref'		  =>	$me->code($var['resref']),
		'tkid'		  =>	$var['restkid'],
		'actionowner' =>	$var['resactionowner'],
		'urgencyid'	  =>	$var['resurgencyid'],
		'portfolioid' =>	$var['resportfolioid'],
		'action'	  =>	$me->code($var['resaction']),
		'notes'		  =>	$me->code($var['resnotes']),
		'statusid'	  =>	$var['resstatusid'],
		'state'		  =>	$var['resstate'],
		'deadline'	  =>	$me->dateToInt($var['resdeadline']." 23:59:59"),
		'meetingdate' =>	$me->dateToInt($var['resmeetingdate']),
	);

	$changes = array();
	$updates = array('copyemail'=>$copyemail);
	
	foreach($data as $fld => $d) {
		if($d!=$res['res'.$fld]) {
			$text = "res".$fld." = '".$d."'";
			$new = $d;
			$old = $res['res'.$fld];
			if($types[$fld]=="DATE") {
				$new = date("d-M-Y",$d);
				$old = date("d-M-Y",$res['res'.$fld]);
			} elseif($fld=="tkid") {
				$new = $me->getAUserName($d);
				$old = $res[$fld];
			} elseif($fld=="actionowner") {
				$new = $me->getAUserName($d);
				$old = $res[$fld];
			} elseif($fld=="statusid") {
				$new = $me->getAStatus($d);
				$old = $res[$fld];
			} elseif($fld=="urgencyid") {
				$new = $me->getAUrgency($d);
				$old = $res[$fld];
			} elseif($fld=="portfolioid") {
				$new = $me->getAPortfolio($d);
				$old = $res[$fld];
			} elseif($types[$fld]=="PERC") {
				$new = $d."%";
				$old = $res['resstate']."%";
			}
			$updates[] = " - ".$headings[$fld]." to '".$new."' from '".$old."'";
			$changes[] = $text;
		}
	}

	if(count($changes)>0) {
		$sql = "UPDATE ".$me->getDBRef()."_res SET ".implode(", ",$changes)." WHERE resid = ".$resid;
		$me->db_update($sql);
		
		$trans = "Resolution edited.  The following change(s) have been made:".chr(10).implode(chr(10),$updates);
		$me->updateRes("E",$res,$trans,"",$data['statusid'],$data['state'],$data['deadline']);
		
		
		
		//SEND NOTIFICATION
		$me->sendResolutionNotification("EDIT",$resid,$updates,$res);

		
		
		
		$response[0] = "ok";
		//$response[1] = $sql;
		$response[1] = "Resolution ".RAPS::REFTAG.$resid." has been successfully edited.";
		//$response[2] = $resid;
	} else {
		$response[0] = "info";
		$response[1] = "No changes could be found for Resolution ".RAPS::REFTAG.$resid.".";
		$response[2] = $resid;
	}


	return $response;

}


function deleteResolution($var,$resid,$res) {
	global $me;
	
	//Mark resolution as cancelled
	$sql = "UPDATE ".$me->getDBRef()."_res SET resstatusid = 'CN' WHERE resid = ".$resid;
	$me->db_update($sql);
	//insert log record for cancellation
	$data = array(
		'date'		=>	strtotime(date("d F Y H:i:s")),
		'tkid'		=>	$me->getUserID(),
		'update'	=>	$me->code("Resolution has been cancelled."),
		'statusid'	=>	"CN",
		'state'		=>	$res['resstate'],
		'resid'		=>	$resid,
		'deadline'	=>	$res['resdeadline'],
		'action'	=>	"D",
		'actdate'	=>	strtotime(date("d F Y H:i:s")),
	);

	//CREATE NEW RESOLUTION RECORD
	$x = array_keys($data);
	$sql = "INSERT INTO ".$me->getDBRef()."_log (log".implode(", log",$x).") VALUES ('".implode("', '",$data)."')";
	$id = $me->db_insert($sql);	
	
	$deleted_attachments = array();
	//delete resolution attachments
	$ra = $me->getResAttachments($resid);
	foreach($ra as $a) {	$r = deleteAttachment($a); 	if(strlen($r)>0) { $deleted_attachments[] = " - ".$r." (Resolution)"; }	}
	
	//delete log attachments
	$la = $me->getLogAttachmentsByResID($resid);
	foreach($la as $a) {	$r = deleteAttachment($a); 	if(strlen($r)>0) { $deleted_attachments[] = " - ".$r." (Log)"; }	}

	if(count($deleted_attachments)>0) {
		$trans = "Deleted attachments due to cancellation of Resolution:".chr(10).implode(chr(10),$deleted_attachments);
		$me->updateRes("D",$res,$trans,"","CN");
	}
	
	return array("ok","Resolution ".RAPS::REFTAG.$resid." has been successfully deleted.");

}


function deleteAttachment($a) {
	global $me;
	
	$folder = "";
	$location = explode("/",$_SERVER["REQUEST_URI"]);
	$l = count($location)-2;
	for($f=0;$f<$l;$f++) {
		$folder.= "../";
	}
	$activefolder=$folder."files/".$me->getCmpCode()."/RAPS";
	$deletefolder=$folder."files/".$me->getCmpCode()."/RAPS/deleted";
	$me->checkFolder("RAPS/deleted");				

	$trans = "";
	
	$file = $activefolder."/".$a['system_filename'];
	if(file_exists($file)) {
		$newfile = $deletefolder."/deleted_".date("YmdHis")."_".$a['system_filename'];
		if(copy($file,$newfile)){
			unlink($file);
			$trans = $a['original_filename'];
		}
	}

	//$trans = "Deleted attachment: ".$a['original_filename'];
	return $trans;
}



























//echo json_encode(array("ok",$res));

?>