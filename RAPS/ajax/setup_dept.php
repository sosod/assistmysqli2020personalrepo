<?php
include "inc.php";

$log_section = "S_DEPT";

switch($_REQUEST['act']) {
	case "ADD":		$response = addNew($me);		break;
	case "getEDIT":	$response = getDetails($me,$_REQUEST['i']);	break;
	case "EDIT":	$response = saveEdit($me);			break;
	case "DELETE":	$response = deletePort($me);			break;
	default: $response = array("error","The system couldn't determine what action you were trying to perform.  Please try again.");
}




$response[1] = urlencode($response[1]);
echo json_encode($response);









function addNew($me) {
	global $log_section;

	$v = $me->code($_REQUEST['value']);
	$sql = "INSERT INTO ".$me->getDBRef()."_list_dept SET value = '".$v."', yn = 'Y', code = '', sort = 99";
	$id = $me->db_insert($sql);
	$n = $me->decode($v);
	$me->logMe($log_section,"C",$id,array(),"Created new department ".$n." ($id)",array(),$_REQUEST,$sql,$table="");
	
	return array("ok","Successfully added department ".$n.".");

}


function getDetails($me,$i) {
	$row = array();
	$sql = "SELECT * FROM ".$me->getDBRef()."_list_dept WHERE id = ".$i." ORDER BY id DESC";
	//$row[1] = $sql;
	$row = $me->mysql_fetch_one($sql);
	
	$row[0] = "got details";
	$row[1] = "";
	
	return $row;
	
}


function saveEdit($me) {
	global $log_section;

	$i = $_REQUEST['id'];
	$old = getDetails($me,$i);
	$n = $me->decode($old['value']);
	$fields = array("value");
	$change = false;
	foreach($fields as $f) {
		if($old[$f]!=$_REQUEST[$f]) {
			$change = true;
			break;
		}
	}
	if($change===true) {
		$v = $_REQUEST['value'];
		$sql = "UPDATE ".$me->getDBRef()."_list_dept SET value = '".$me->code($v)."' WHERE id = ".$i;
		$id = $me->db_update($sql);
		$n = $v;
		$me->logMe($log_section,"E",$i,$fields,"Updated department ".$i.": ".$n,$old,$_REQUEST,$sql,$table="");
		return array("ok","Successfully saved changes to department ".$n.".");
	} else {
		return array("info","No change was found to be saved for ".$n.".");
	}
	return array("error","An unknown error occurred.");
	//return $old;
}

function deletePort($me) {
	global $log_section;

	$i = $_REQUEST['id'];
	$old = getDetails($me,$i);
	$n = $me->decode($old['value']);
		$sql = "UPDATE ".$me->getDBRef()."_list_dept SET yn = 'N' WHERE id = ".$i;
		$me->db_update($sql);
		$me->logMe($log_section,"D",$i,"yn","Deleted department ".$i.": ".$n,"Y","N",$sql,$table="");
	return array("ok","Successfully deleted department ".$n.".");
}


?>