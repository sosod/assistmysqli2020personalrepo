<?php
include "inc.php";

switch($_REQUEST['act']) {
	case "ADD":		$response = addNew($me);		break;
	case "getEDIT":	$response = getDetails($me,$_REQUEST['i']);	break;
	case "EDIT":	$response = saveEdit($me);			break;
	case "DELETE":	$response = deletePort($me);			break;
	default: $response = array("error","The system couldn't determine what action you were trying to perform.  Please try again.");
}




$response[1] = urlencode($response[1]);
echo json_encode($response);









function addNew($me) {
	$deptid = $_REQUEST['deptid'];
	$v = $me->code($_REQUEST['value']);
	$sql = "INSERT INTO assist_".$me->getCmpCode()."_raps_list_portfolio SET value = '".$v."', yn = 'Y', deptid = ".$deptid;
	$id = $me->db_insert($sql);
	$d = $me->getADepartment($deptid);
	$n = $me->decode($d['name']." - ".$v);
	$me->logMe("S_PORT","C",$id,array(),"Created new portfolio ".$n,array(),$_REQUEST,$sql,$table="");
	
	return array("ok","Successfully added portfolio ".$n.".");

}


function getDetails($me,$i) {
	$row = array();
	$sql = "SELECT * FROM assist_".$me->getCmpCode()."_raps_list_portfolio WHERE id = ".$i." ORDER BY id DESC";
	//$row[1] = $sql;
	$row = $me->mysql_fetch_one($sql);
	
	if(isset($row['deptid'])) {
		$row['dept'] = $me->getADepartment($row['deptid']);
	}
	
	$row[0] = "got details";
	$row[1] = "";
	
	return $row;
	
}


function saveEdit($me) {
	$i = $_REQUEST['id'];
	$old = getDetails($me,$i);
	
	$fields = array("deptid","value");
	$change = false;
	foreach($fields as $f) {
		if($old[$f]!=$_REQUEST[$f]) {
			$change = true;
			break;
		}
	}
	$n=$me->decode($old['dept']['name'])." - ".$_REQUEST['value'];
	if($change===true) {
		$deptid = $_REQUEST['deptid'];
		$v = $_REQUEST['value'];
		$sql = "UPDATE assist_".$me->getCmpCode()."_raps_list_portfolio SET value = '".$me->code($v)."', deptid = ".$deptid." WHERE id = ".$i;
		$id = $me->db_update($sql);
		$me->logMe("S_PORT","E",$id,$fields,"Updated portfolio ".$n,$old,$_REQUEST,$sql,$table="");
		return array("ok","Successfully saved changes to portfolio ".$n.".");
	} else {
		return array("info","No change was found to be saved for ".$n.".");
	}
	return array("error","An unknown error occurred.");
	//return $old;
}

function deletePort($me) {
	$i = $_REQUEST['id'];
	$old = getDetails($me,$i);
	$n=$me->decode($old['dept']['name']." - ".$old['value']);

		$sql = "UPDATE assist_".$me->getCmpCode()."_raps_list_portfolio SET yn = 'N' WHERE id = ".$i;
		$me->db_update($sql);
		$me->logMe("S_PORT","D",$id,"yn","Deleted portfolio ".$n,"Y","N",$sql,$table="");
	return array("ok","Successfully deleted portfolio ".$n.".");
}


?>