<?php
//include "inc.php";
include "../../module/autoloader.php";
$me = new RAPS();
$response = array("error","Your request could not be processed at this time.  Please try again later.",0);


//GET DATA
$data = array(
	'ref'		  =>	$me->code($_REQUEST['resref']),
	'tkid'		  =>	$_REQUEST['restkid'],
	'actionowner' =>	$_REQUEST['restkid'],				//actionowner is defaulted to responsible person on create
	'urgencyid'	  =>	$_REQUEST['resurgencyid'],
	'portfolioid' =>	$_REQUEST['resportfolioid'],
	'action'	  =>	$me->code($_REQUEST['resaction']),
	'notes'		  =>	$me->code($_REQUEST['resnotes']),
	'statusid'	  =>	$_REQUEST['resstatusid'],
	'state'		  =>	$me->getDefaultState($_REQUEST['resstatusid']),
	'deadline'	  =>	$me->dateToInt($_REQUEST['resdeadline']." 23:59:59"),
	'meetingdate' =>	$me->dateToInt($_REQUEST['resmeetingdate']),
	'adddate'	  =>	$me->getToday(),
	'adduser'	  =>	$me->getUserID(),
);

//CREATE NEW RESOLUTION RECORD
$x = array_keys($data);
$sql = "INSERT INTO ".$me->getDBRef()."_res (res".implode(", res",$x).") VALUES ('".implode("', '",$data)."')";
//$response[1] = $sql;
$id = $me->db_insert($sql);

//ADD LOG RECORD
        $logupdate = "New resolution added.".chr(10)."Instructions: ".$data['action'];
        $sql = "INSERT INTO ".$me->getDBRef()."_log SET 
				logdate = '".$data['adddate']."', 
				logtkid = '".$data['tkid']."', 
				logupdate = '".$logupdate."', 
				logstatusid = '".$data['statusid']."', 
				logstate = ".$data['state'].", 
				logresid = '".$id."', 
				logdeadline = '".$data['deadline']."',
				logaction = 'C',
				logactdate = '".$data['adddate']."'";
		$me->db_insert($sql);


//NOTIFY RECIPIENT
$copyemail = isset($_REQUEST['copyemail']) && $_REQUEST['copyemail']=="Y" ? true : false;
$me->sendResolutionNotification("NEW",$id,array("copyemail"=>$copyemail));
/*$to = array('name'=>$me->getAUserName($data['tkid']),'email'=>$me->getAnEmail($data['tkid']));
$reply_to = array('name'=>$me->getAUserName($data['adduser']),'email'=>$me->getAnEmail($data['adduser']));
$addusername = $reply_to['name'];
$portfolio = $me->getAPortfolio($data['portfolioid']);
$urgency = $me->getAUrgency($data['urgencyid']);


$subject = "New Resolution ".RAPS::REFTAG.$id."";
$message = "";


            $message .= $addusername." has assigned a new resolution (".RAPS::REFTAG.$id.") to you:\n";
            $message .= "Portfolio:</b> ".$portfolio."\n";
            $message .= "Council Meeting Date: ".date("d F Y",$data['meetingdate'])."\n";
            $message .= "Deadline: ".date("d F Y",$data['deadline'])."\n";
            $message .= "Priority: ".$urgency."\n";
            $message .= "Instructions:\n".$me->decode($data['action'])."\n";
            $message .= "Additional Notes:\n".$me->decode($data['notes'])."\n";
            $message .= "Please log onto Ignite Assist in order to update this resolution.";

$email = new ASSIST_EMAIL();
		$email->setRecipient($to);
		$email->setSubject($subject);
		$email->setBody($message);
		//$email->setCC($cc);					//not required
		//$email->setBCC($bcc);					//not required
		$email->setSender($reply_to);
		//$email->setContentType($content_type);  //default = text
$email->sendEmail();
*/

		
//SET RESPONSE		
$response[0] = "ok";
$response[1] = "New resolution '".$data['ref']."' has been added successfully as ".RAPS::REFTAG.$id.".";
$response[2] = $id;





$response[1] = urlencode($response[1]);
echo json_encode($response);


?>