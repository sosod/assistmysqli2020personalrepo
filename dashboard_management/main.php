<?php
require_once("../module/autoloader.php");
ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));
$displayObject = new ASSIST_MODULE_DISPLAY();
echo "
<h1>Dashboard Management</h1>
<p>This section allows you to customise the Action Dashboard which is available to all users.</p>";


/***************************************************
 * SECTION TO MANAGE WHICH SDBIP / IKPI MODULES APPEAR ON THE ACTION DASHBOARD
 */

$me = new DASHBOARD_MANAGEMENT_ACTION();
$my_controller_class_nickname = $me->getControllerClassNickname();

echo "
<h2>Action Modules</h2>

<div id='div_hide_mod' class='float' style='width:500px;padding: 30px;margin: 30px;border: 1px solid #fe9900;margin-top: -10px'>
	<h2 class='orange' style='margin-top: 0px'>Help</h2>
	<p>This section allows you to show/hide certain modules which are possibly no longer applicable to for day-to-day work but which need to be retained for historical purposes. Choosing to hide a module here will not affect a user's access to the module in any way.  The module will simply be removed from the Action Dashboard of all users but it will still be accessible from the Main Menu.</p>
	<p>Please note that not all modules can be hidden here.  The list is currently limited to very specific modules.  If there is a module you would like to have hidden which does not appear on this list, please contact your Assist Business Partner.</p>
	<p>Users who are currently logged in will only see the changes made here when they next login.</p>
</div>
";
$modules_available_for_hiding = $me->getModulesAvialableForHiding();
//ASSIST_HELPER::arrPrint($modules_available_for_hiding);

$js = "";
if(count($modules_available_for_hiding)==0) {
	echo "<p>Sorry, you do not have any modules available which are suitable for hiding.</p>";
} else {
	?>
	<table class="tbl-container"><tr><td>
	<form name="frm_action">
		<table id="tbl_mod_hide">
			<thead>
			<tr>
				<th>Ref</th>
				<th>Module Name</th>
				<th>Show</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach($modules_available_for_hiding as $modref => $mod) {
				echo "
			<tr>
				<td>$modref</td>
				<td>".$mod['modtext']."</td>
				<td>";
				$js .= $displayObject->drawBoolButton($mod['mod_dashboard'], array('id' => "mod_dashboard_".$modref, 'name' => "mod_dashboard[$modref]", 'small_size' => true));
				echo "</td>
			</tr>";
			}
			?>
			<tr>
				<td class=center colspan="3">
					<button id="btn_mod_hide_save">Save Changes</button>
				</td>
			</tr>
			</tbody>
		</table>
	</form>
				<?php
$log_table = "DASH_ACTION";
$log_id = $log_table;
				$js.= "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = 'log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=".$my_controller_class_nickname.".getLogs',dta);
					console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[1]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}

			});
			";
echo  "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%></td>
			<td width=50%><span id=".$log_id."_disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span></td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
				?>
			</td></tr></table>
	<?php
}
?>
<script type="text/javascript">
	$(function() {
		<?php echo $js; ?>
		$("#div_hide_mod p").css({"font-size":"125%","line-height":"150%"});
		$("#btn_mod_hide_save").button({
			icons:{primary:"ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-state-green")
				.hover(function() {
					$(this).removeClass("ui-button-state-green").addClass("ui-button-state-orange");
				},function() {
					$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_action]"));
			// console.log(dta);
			var result = AssistHelper.doAjax("inc_controller.php?action=<?php echo $my_controller_class_nickname; ?>.saveChanges",dta);
			AssistHelper.finishedProcessing(result[0],result[1]);
		});
		$("table.tbl-container").css("border","0px solid #ffffff");
		$("table.tbl-container tr:first td:first").css("border","0px solid #ffffff");
		$("table.tbl-subcontainer").css("border","0px solid #ffffff");
		$("table.tbl-subcontainer tbody tr td").css("border","0px solid #ffffff");

	})
</script>
<?php


//Start prep for future section - need to reset $js etc.
unset($me);
$js = "";
unset($my_controller_class_nickname);


/***************************************************
 * END ----------------------------- SECTION TO MANAGE WHICH SDBIP / IKPI MODULES APPEAR ON THE ACTION DASHBOARD
 */




/***************************************************
 * SECTION TO ...
 */
?>