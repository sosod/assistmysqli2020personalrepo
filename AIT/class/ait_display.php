<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class AIT_DISPLAY extends ASSIST_MODULE_DISPLAY {
    
    
	protected $default_attach_buttons = true;
	
    public function __construct() {
    	$me = new AIT();
		$an = $me->getAllActivityNames();
		$on = $me->getAllObjectNames();
		unset($me);
    	
        parent::__construct($an,$on);
    }
    
	public function disableAttachButtons() { $this->default_attach_buttons = false; }
	public function enableAttachButtons() { $this->default_attach_buttons = true; }
	
	
	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */
	
	
	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		$d = $this->getDataField($type,$val,$options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 * 
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val); 
				break;;
			case "CURRENCY":
				if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
			case "BOOL_BUTTON":
				$data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display'=>$val,'js'=>"");
				//$data = array('display'=>$type);
		}
		return $data;
	}
	
    
	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		//echo "dFF VAL:-".$val."- ARR: "; print_r($options);
		$ff = $this->createFormField($type,$options,$val);
		//if(is_array($ff['display'])) {
			//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}
	/** 
	 * Returns string of selected form field
	 * 
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "LABEL":
				$data=$this->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$this->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$this->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} 
				$data=$this->getMultipleSelect($val2,$options,$items);
				break;
			case "DATE": //echo "date!";
				$extra = $options['options'];
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;	
			case "CURRENCY": $size = 15;
			case "PERC": 
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
			case "BOOL":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			case "ATTACH"://echo "attach: ".$val;
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInput($val,$options);
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $this->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /**
	 * (ECHO) Function to display the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$data = $this->getPageFooter($left,$log_table,$var,$log_id);
		echo $data['display'];
		return $data['js'];
    }
    /**
	 * Function to create the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
    }
 	
	
	
	
	
	
	
	
	
	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */
	
	
	
	
	/*****
	 * returns the details table for the object it is fed.
	 * 
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 * 
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new AIT_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new AIT_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new AIT_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new AIT_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id,false,false,false,array(),false,false);
	}
	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons="X") {
		if($attachment_buttons=="X") { $attachment_buttons = $this->default_attach_buttons; }
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button,$sub_table,$view_all_button,$button,$compact_view,$attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}
	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons=true) {
		if(is_array($object_type)) {
			$var = $object_type;
			$object_id = $var['object_id'];
			$object_type = $var['object_type'];
		}
		//echo $object_type." :: ".$object_id;
		$js = "";
		$echo = "";
		//$echo = "abc :: ".$object_type." :: ".$object_id;
		//add code to get js from assist_module_display to trigger on attachment click for download
		if($attachment_buttons){
			$js.=$this->getAttachmentDownloadJS($object_type,$object_id);
		}
		$parent_buttons = "";
		switch(strtoupper($object_type)) {
			case "ACTION": 
				$myObject = new AIT_ACTION();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("deliverable")."' class='float btn_parent' id=DELIVERABLE /> <input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
				} 
				break;
			case "SUB-DELIVERABLE":
			case "DELIVERABLE": 
				$myObject = new AIT_DELIVERABLE();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("contract")."' class='float btn_parent' id=CONTRACT />";
				} 
				break;
			case "CONTRACT": 
				$myObject = new AIT_CONTRACT();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("FINANCE")."' class='float btn_parent' id=FINANCE />";
				} 
				break;
			case "FINANCE":
				$myObject = new AIT_FINANCE();
				break;
			case "TEMPCON":
				$myObject = new AIT_TEMPLATE();
				break;
			default:
				$myObject = new AIT();
				break;
		}

		if($view_all_button===true) {
			$parent_buttons.="<input class='float btn_view_all' type=button id='view_all' value='View All' />";
		}
		if($object_type=="FINANCE") {
			$result = $myObject->getDetailedObjectForDisplay($object_id);
			$js = $result['js'];
			$echo = $result['display'];
		} else {
			if($include_parent_button || $view_all_button) {
				$js.= "
						$('input:button.btn_view_all').button();
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type!="CONTRACT" ? "0.5" :"0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
			} //echo "cv: ".$compact_view.":";
			if($object_type != "TEMPCON"){
				$get_object_options = array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons,'compact_view'=>($compact_view));
//				$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons),($compact_view===true?array('page'=>"COMPACT"):array()));
				$object = $myObject->getObject($get_object_options);
				//ASSIST_HELPER::arrPrint($object);
			}else{
				$object = $myObject->getListObject($object_id, "CON");
			}
			
			$echo.="
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head":"")."'>".($object_type=="DELIVERABLE" && $sub_table ? "Sub-" : "").$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					foreach($object['head'] as $fld => $head) {
						if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else { $val = $fld; }
						$echo.= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".$val."</td>
						</tr>";
					}
					//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button)>0){
				$echo.="
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}		
			$echo.="</table>";
		}

		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
		//return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type));
	}
	
		
















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */



	
	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array(),$add_button=array(false,"","")) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier center'><span id='$i'>
					<span style='float: left' id=spn_paging_buttons>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
					</span>";
		if(isset($add_button [0]) && $add_button[0]==true) {
			$ab_label = $this->replaceAllNames(isset($add_button[1]) && strlen($add_button[1])>0 ? $add_button[1] : "|add|");
			$data['display'].="<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['js'].="
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).click(function(e) {
				e.preventDefault();
				".$add_button[2]."
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\").children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"})
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); });
			";
		}
		$data['display'].="<!-- <span class=float id=spn_paging_search>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			} 
		$data['js'].= "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';
		
		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i."').children('#spn_paging_buttons').find('button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}
	
	
	/*****
	 * List View
	 */
	/**
	 * Function to draw the list table onscreen
	 * @param (Array) $child_objects = array of objects to display in table
	 * @param (Array) $button = details of button to display in last column
	 * @param (String) $object_type = the name of the object
	 * @param (Array) $object_options = getPaging options
	 * @param (Bool) $sub_table = is this table a child of another object true/false
	 * @param (Array) $add_button = array(0=>true/false, 1=>label if true)
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false,$add_button=array(false,"","")) {
		//ASSIST_HELPER::arrPrint($child_objects);
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table,$add_button);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false, $add_button = array(false,"","")) {
		//ASSIST_HELPER::arrPrint($child_objects); 
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		//$items_total = $contractObject->getPageLimit();
		//$these_items = array_chunk($child_objects['rows'], $items_total);
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button===false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager']===false) {
			$display_paging = false;
		}
		if($display_paging){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options,$add_button);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		unset($button['pager']);
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}
					
				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			$data['js'].=$rows['js'];
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		if(!isset($child_objects['rows']) || count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				$data['display'].="<tr>";
				foreach($obj as $fld=>$val) {
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
				if(isset($button['type']) && $button['type']=="button") {
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><button ref=".$id." class='".$button['class']." ui_btn' />".$button['value']."</button></td>" : "");
					$data['js'].="
					$('button.ui_btn').button({
						icons: {primary: \"ui-icon-pencil\"},
					}).children(\".ui-button-text\").css({\"font-size\":\"80%\"}).click(function(e) {
						e.preventDefault();
						
					}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")
					";
				} else {
					$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
				}
				$data['display'].="</tr>";
			}
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag) 
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page) 
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0) {
		$last_deliverable_status = 0 ;
		if(is_array($form_object_type)) {
			$var = $form_object_type;
			$form_object_type = $var['object_type'];
			$form_object_id = $var['object_id'];
			$page_action = $var['page_action'];
			$page_redirect_path = $var['page_redirect_path'];
			$parent_object_id = $var['parent_id'];
			switch($form_object_type) {
				case "CONTRACT":
					$parent_object_type = "";
					$formObject = new AIT_CONTRACT($form_object_id);
					$parentObject = null;
					break;
				case "DELIVERABLE":
				case "SUB-DELIVERABLE":
					$parent_object_type = "CONTRACT";
					$formObject = new AIT_DELIVERABLE();
					$parentObject = new AIT_CONTRACT($parent_object_id);
					break;
				case "ACTION":
					$parent_object_type = "DELIVERABLE";
					$formObject = new AIT_ACTION;
					$parentObject = new AIT_DELIVERABLE();
					break;
			}
		}
		
		if(stripos($page_action,".")!==false) {
			$pa = explode(".",$page_action);
			$page_action = $pa[1];
			$page_section = strtoupper($pa[0]);
		} else {
			$page_section = "MANAGE";
		}
		
		$headingObject = new AIT_HEADINGS();
		
		if($form_object_type=="SUB_DELIVERABLE") {
			$th_class = "th2";
			$tbl_class = "sub_indent";
			$form_object_type = "DELIVERABLE";
		} else {
			$th_class = "";
			$tbl_class = "";
		}
		
		$attachment_form = false;
		
		$data = array('display'=>"",'js'=>"");
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		if($page_action=="UPDATE") {
			$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($form_object_type,"FORM"));
		} else {
			if(stripos($form_object_type,"ASSURANCE")===false) {
				$head_object_type = $form_object_type;
			} else {
				$head_object_type = "ASSURANCE";
			}
			$fld_prefix = $formObject->getTableField()."_";
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,"FORM","",$fld_prefix));
		}
		//ASSIST_HELPER::arrPrint($headings);
		//ASSIST_HELPER::arrPrint($headingObject->getHeadingsForLog());
		
		$pa = ucwords($form_object_type).".".$page_action;
		$pd = $page_redirect_path;
		
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE) || (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		$is_update_page = (strrpos(strtoupper($page_action),"UPDATE")!==FALSE);
		$is_add_page = (strrpos(strtoupper($page_action),"ADD")!==FALSE);
		$is_copy_page = (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		
		$copy_protected_fields = array();
		$copy_protected_heading_types = array();
		
		if($is_edit_page) {
			$form_object = $formObject->getRawObject($form_object_id);
			$form_activity = "EDIT";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
			}
		} elseif($is_update_page) {
			$form_object = $formObject->getRawUpdateObject($form_object_id);
			$form_activity = "UPDATE";
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);
		$form_name = "frm_object_".$form_object_type."_".$form_object_id."_".strtolower(str_replace(".","",$page_action));

		$js.="  console.log('".$form_name."');
				var ".$form_name."_page_action = '".$pa."';
				var ".$form_name."_page_direct = '".$pd."';
				
		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);
		
		$echo.="
			<div id=div_error class=div_frm_error>
				
			</div>
			".($display_form ? "<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">" : "")."
				<table class='form $tbl_class' width=100% id=tbl_object_form_".$tbl_id.">";
			$form_valid8 = true;
			$form_error = array();
			foreach($headings['rows'] as $fld => $head) {
				if($head['parent_id']==0) {
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING" && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($head['section']=="DELIVERABLE_ASSESS") {
								if($parentObject->mustIDoAssessment()===FALSE || $parentObject->mustIAssignWeights()===FALSE) {
									$display_me = false;
								} else {
									//validate the assessment status field here
									$lt = explode("_",$head['list_table']);
									switch($lt[1]) {
										case "quality": $display_me = $parentObject->mustIAssessQuality(); break;
										case "quantity": $display_me = $parentObject->mustIAssessQuantity(); break;
										case "other": $display_me = $parentObject->mustIAssessOther(); break;
									}
								}
							}
							if($display_me) {
								$list_items = array();
								$listObject = new AIT_LIST($head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER","TEMPLATE","DEL_TYPE"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new AIT_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "TEMPLATE":
									$listObject = new AIT_TEMPLATE();
									$list_items = $listObject->getActiveTemplatesFormattedForSelect();
									break;
								case "OWNER":
									$listObject = new AIT_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									$listObject = new AIT_MASTER($head['list_table']);
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break; 
								case "DEL_TYPE":
									$listObject = new AIT_DELIVERABLE();
									$list_items = $listObject->getDeliverableTypes($parent_object_id);
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif(in_array($h_type,array("DELIVERABLE"))) {
							$list_items = array();
							switch($h_type) {
								case "DELIVERABLE":
									$listObject = new AIT_DELIVERABLE();
									$list_items = $listObject->getDeliverablesWithDeadlineForParentSelect($parent_object_id);
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = array();
							foreach($list_items as $i => $d) {
								$now = strtotime(date("Y-m-d"));
								$then  = strtotime($d['deadline']);
								$diff = ($then-$now)/(3600*24);
								
								$options['options'][$i] = array('value'=>$d['name'],'prop'=>array('parent_deadline'=>$diff));
							}
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$js.=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} elseif($head['type']=="DEL_TYPE"){
								//$row[$fld] = $this->deliverable_types[$val];
								$del_types = $formObject->getAllDeliverableTypes();
								$val = $del_types[$val];
							} elseif($head['type']=="DELIVERABLE" && $fld=="del_parent_id") {
								if($val==0) {
									$val = "N/A";
								} else {
									$val = AIT_DELIVERABLE::REFTAG.$val;
								}
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);








/*
				} elseif($head['type']=="DELIVERABLE") {
					if($r["del_type"]=='SUB') {
						if($r[$fld]==0) {
							$row[$fld] = $this->getUnspecified();
						} else {
							if(!isset($deliverable_names_for_subs)) {
								$deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($i);
								//$this->arrPrint($deliverable_names_for_subs);
							}
							$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
						}
					} else {
						unset($final_data['head'][$fld]);
					}
				} elseif($head['section']=="DELIVERABLE_ASSESS") {
					$assess_status = $r['contract_assess_type'];
					$display_me = true;
						if($cObject->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $cObject->mustIAssessQuality($assess_status); break;
								case "quantity": $display_me = $cObject->mustIAssessQuantity($assess_status); break;
								case "other": $display_me = $cObject->mustIAssessOther($assess_status); break;
							}
						}
					if($display_me) {
						$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
						if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
							$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
						}
					} else {
						unset($final_data['head'][$fld]);
						//$row[$fld] = $assess_status;
					}
				} elseif($this->isDateField($fld) || $head['type']=="DATE") {
					$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
				//format by fld
				} elseif($fld==$id_fld){//} || ($obj_type=="ACTION" && $fld==$a_id_fld) || ($obj_type=="DELIVERABLE" && $fld==$d_id_fld) || ($obj_type=="CONTRACT" && $fld==$c_id_fld)) {
					if($fld==$id_fld && !$format_for_emails) {
						$row[$fld] = $ref_tag.$val;
					} else {
						$row[$fld] = $val;
					}

				} elseif($fld=="contract_assess_status") {
					$val = $r['contract_assess_status'];
					$listObject = new AIT_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$key] = array(
							'field'=>$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
					$td = array();
					foreach($sub_head as $key => $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($sh_type=="BOOL"){
							$pow = pow(2,$key);
							$test = ( (($val & $pow) == $pow) ? "1" : "0");
							$sval = $displayObject->getDataField("BOOL", $test);
						}
						$td[]=array($shead['name']=>$sval['display']);
					}
					$row[$fld] = $td;
				} elseif($fld=="contract_assess_type") {
					$sub_head = $c_headings['sub'][$head['id']];
					$val = $r['contract_assess_type'];
					$sub_display = true;
					$td = array();
					foreach($sub_head as $shead) {
						$v = $shead['field'];
						switch($v) {
							case "contract_assess_other_name":
								if($cObject->mustIAssessOther($val)) { 
									$v = $r[$v];
								} else {
									$sub_display = false;
								} 
								break;
							case "contract_assess_qual": 	$cas = !isset($cas) ? $cObject->mustIAssessQuality($val) : $cas;
							case "contract_assess_quan": 	$cas = !isset($cas) ? $cObject->mustIAssessQuantity($val) : $cas;
							case "contract_assess_other":	$cas = !isset($cas) ? $cObject->mustIAssessOther($val) : $cas; 
								if($cas) {
									$v = $displayObject->getDataField("BOOL", "1");
								} else {
									$v = $displayObject->getDataField("BOOL", "0");
								}
								$v = $v['display'];
								break;
						}
						if($sub_display) {
							$td[] = array($shead['name']=>$v);
						}
						unset($cas);
					}
					$row[$fld] = $td;
				} elseif($head['type']=="REF") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
				} elseif($head['type']=="ATTACH") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
					$attach_display_options = array(
						'can_edit'=>false,
						'object_type'=>$obj_type,
						'object_id'=>$id,
						'buttons'=>$attachment_buttons,
					);
					$row[$fld] = $displayObject->getDataField($head['type'], $val,$attach_display_options);
				} else {
					//$row[$fld] = $val;
					$row[$fld] = $displayObject->getDataField($head['type'], $val);
				}
			}


*/

							
							
							
							
							
							
							
							
							
														
														
														
													
													
												
												
											
											
										
										
									
									
								
								
							
							
						
						
						
					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
						if($fld=="contract_assess_status") {
							if($is_edit_page || $is_update_page) {
								$pval = isset($form_object['contract_assess_status']) ? $form_object['contract_assess_status'] : "";
							} 
							$form_object[$fld]=array(0=>array());
							//echo "contract asessment statuses!!!!".$pval;
							$listObject = new AIT_LIST("deliverable_status");
							$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
							$last_deliverable_status = 0;
							$sub_head = array();
							$sh_type = "BOOL";
							foreach($list_items as $key => $status) {
								$sub_head[$fld."-".$key] = array(
									'field'=>$fld."-".$key,
									'type'=>$sh_type,
									'name'=>$status,
									'required'=>1,
									'parent_link'=>"",
									'default_value'=>($key==5 ? "1" : "0"),
								);
								$last_deliverable_status = $key;

								if($is_edit_page && (strlen($pval)>0 || $pval>0)) {
									$bitwise_value = pow(2,$key); //echo "<br>".$key."::".$bitwise_value.":".($pval & $bitwise_value).":";
									$sub_head[$fld."-".$key]['default_value'] = 0;
									$form_object[$fld][0][$fld."-".$key] = 0;
									if(($pval & $bitwise_value) == $bitwise_value ) {
										$sub_head[$fld."-".$key]['default_value'] = 1;
										$form_object[$fld][0][$fld."-".$key] = 1;
									}
								}
							}
							unset($listObject);
						} elseif($fld=="contract_assess_type"){
							$sub_head = $headings['sub'][$head['id']];
//								ASSIST_HELPER::arrPrint($headings['sub'][$head['id']]);
	//							ASSIST_HELPER::arrPrint($form_object[$fld]);
								if($is_edit_page || $is_update_page) {
									$pval = $form_object[$fld];
								} else {
									$pval = isset($head['default_value']) ? $head['default_value'] : 0;
								}
								$form_object[$fld] = array(0=>array());
								foreach($sub_head as $skey => $shead) {
									$sfld = $shead['field'];
									switch($sfld) {
										case "contract_assess_other_name":
											if($is_edit_page || $is_update_page) {
												$form_object[$fld][0][$sfld] = $form_object[$sfld];
											} else {
												$form_object[$fld][0][$sfld] = isset($shead['default_value']) ? $shead['default_value'] : "";
											}
											break;
										default:
											if($is_edit_page || $is_update_page) {
												$bitwise_value = 0;
												$b = explode("_",$sfld);
												$f = end($b);
												switch($f) {
													case "other":	$bitwise_value = AIT_CONTRACT::OTHER;	break;
													case "qual":	$bitwise_value = AIT_CONTRACT::QUALITATIVE;	break;
													case "quan":	$bitwise_value = AIT_CONTRACT::QUANTITATIVE;	break;
												}
												
												$sub_head[$skey]['default_value'] = 0;
												$form_object[$fld][0][$sfld] = 0;
												if(($pval & $bitwise_value) == $bitwise_value ) {
													$sub_head[$skey]['default_value'] = 1;
													$form_object[$fld][0][$sfld] = 1;
												}
											} else {
												$form_object[$fld][0][$sfld] = $sub_head[$skey]['default_value'];
											}
											break;
									}
								}
						} else {
							$sub_head = $headings['sub'][$head['id']];
						}
						if(isset($form_object[$fld]) && is_array($form_object[$fld])) {
							$sub_form_object = $form_object[$fld];
						} else {
							$sub_form_object = isset($form_object[$fld]) ? array($form_object[$fld]) : array();
						}
						if(count($sub_form_object)==0) {
							foreach($sub_head as $shead) {
								$sub_form_object[0][$shead['field']] = "";
							}	
						}
						$td = "
						<div class=".$fld."><span class=spn_".$fld.">";
						$add_another[$fld] = false;
						foreach($sub_form_object as $sfo) {
							$td.="<span>";
							if($fld=="contract_supplier") {
								$td.="<input type=hidden value='".(isset($sfo['cs_id']) ? $sfo['cs_id'] : 0)."' name=".$fld."_id[] />";
							}
							$td.="<table class=sub_form width=100%>";
							foreach($sub_head as $shead) {
								//ASSIST_HELPER::arrPrint($shead);
								$sh_type = $shead['type'];
								$sfld = $shead['field'];
								if($fld=="contract_supplier") {
									$options = array('name'=>$sfld."[]",'req'=>$head['required']);
								} else {
									$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
								}
								$val = "";
								if($sh_type=="LIST") {
									$list_items = array();
									$listObject = new AIT_LIST($shead['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									$options['options'] = $list_items;
									if(count($list_items)==0 && $shead['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									if(count($list_items)>1) {
										$add_another[$fld] = true;
									}
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
									}
									unset($listObject);
								} elseif($sh_type=="BOOL"){
									$sh_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									}
									if(strlen($val)==0) {
										$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
									}
								} elseif($sh_type=="CURRENCY") {
									$options['extra'] = "processCurrency";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								}
								$sdisplay = $this->createFormField($sh_type,$options,$val);
								$js.= $sdisplay['js'];
								$td.="
								<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
							}
						$td.= "
							</table></span>
							";
						}
						$td.="
							</span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
						$td_blank = "";
						if($add_another[$fld]) {
											$td_blank="<div id=".$fld."_blank><span>";
											if($fld=="contract_supplier") {
												$td_blank.="<input type=hidden value=\"0\" name=".$fld."_id[] />";
											}
											$td_blank.="<table class=sub_form width=100%>";
											foreach($sub_head as $shead) {
												$sh_type = $shead['type'];
												$sfld = $shead['field'];
												if($fld=="contract_supplier") {
													$options = array('name'=>$sfld."[]",'req'=>$head['required']);
												} else {
													$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
												}
												$val = "";
												if($sh_type=="LIST") {
													$list_items = array();
													$listObject = new AIT_LIST($shead['list_table']);
													$list_items = $listObject->getActiveListItemsFormattedForSelect();
													$options['options'] = $list_items;
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
													unset($listObject);
												} elseif($sh_type=="BOOL"){
													$sh_type = "BOOL_BUTTON";
													$options['yes'] = 1;
													$options['no'] = 0;
													$options['extra'] = "boolButtonClickExtra";
													$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
												} elseif($sh_type=="CURRENCY") {
													$options['extra'] = "processCurrency";
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
												}
												$sdisplay = $this->createFormField($sh_type,$options,$val);
												$td_blank.="
												<tr ".(strlen($shead['parent_link'])>0 ? "class=\"tr_".$shead['parent_link']."\"" : "").">
													<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
													<td>".$sdisplay['display']."</td>
												</tr>";
											}
										$td_blank.= "
											</table></span>
										</div>
											";		
						} 
						$display = array('display'=>$td.$td_blank);
					} 
					if($display_me) {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
				}
			}
			if(!$form_valid8) {
				$js.="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
			$is_new_del_edit_page = (strrpos(strtoupper($pd),"NEW_DELIVERABLE_ADD_OBJECT")!==FALSE);
			if($page_section!="NEW" && !$is_add_page && !$is_copy_page && !$is_new_del_edit_page && !($form_object_type == 'CONTRACT' && $is_edit_page)){
				if($form_object_type == 'CONTRACT' && $is_add_page){
					$checks = true;
				}else{
					$checks = false;
				}
				$echo.="
					<tr>
						<th>Notifications:
						".($form_activity=="NEW" ? "<br /><span class=i style='font-weight: normal; font-size: 75%'>This will notify the indicated recipients that a new Contract has been created and is available to have Deliverables and Actions added to it." : "")."
						".($form_activity=="UPDATE" ? "<br /><span class=i style='font-weight: normal; font-size: 75%'>This will notify the indicated recipients that an update has been added." : "")."
						".($form_activity=="EDIT" ? "<br /><span class=i style='font-weight: normal; font-size: 75%'>This will notify the indicated recipients of any changes made." : "")."
						</th>
						<td>
							<input hidden req='1' title='This is a required field.' id='notification_chooser' name='notification_chooser' value='mail' />
							<div id='notification_btns' style=''>
								<input type='radio' checked='checked' name='radio' id='mail_btn'><label for='mail_btn'>Email</label>
								<input type='radio' name='radio' id='both_btn'><label for='both_btn'>Email and SMS</label>
								".($form_activity=="NEW" ? "<input type='radio' name='radio' id='none_btn'><label for='none_btn'>None</label>": "")."
							<!--		<input type='radio' name='radio' id='sms_btn'><label for='sms_btn'>SMS</label> &nbsp; -->
								<div id='why_no_sms' style='display:inline; width:110px;'>
								</div>
							</div>
							<div id='notification_recipients'>
								<p>Recipients:</p>
							</div>
						</td>
					</tr>";
					$data['js'].="
						$('#notification_recipients').html('');
						
						$('#notification_btns').buttonset();
						$('#mail_btn').button({
							icons:{
								primary: 'ui-icon-mail-closed'
							}
						});
						$('#both_btn').button({
							icons:{
								primary: 'ui-icon-mail-closed',
								secondary: 'ui-icon-signal'
							}
						});
						$('#none_btn').button({
							icons:{
								primary: 'ui-icon-cancel'
							}
						});
				//		$('#sms_btn').button({
				//			icons:{
				//				secondary: 'ui-icon-signal'
				//			}
				//		});
				
						var useSMS = AssistHelper.doAjax('inc_controller.php?action=SetupNotifications.Approve&activity=".$page_action."&object=".$form_object_type."');
				//		console.log(useSMS);
						window.noteTp = 0;
						
						if('".$form_object_type."' == 'CONTRACT' && '".$page_action."' == 'Add'){
							con = 	{};
							recipes = [];
						}else{
							getRecipes();
						}
						";
						$data['js'].="
						$(\"select[id*='authoriser']\").change(function(){
							con.authoriser = $(this).val(); //console.log($(this).val());
							getRecipes(con);
						});
						$(\"select[id*='manager']\").change(function(){
							con.manager = $(this).val();
							getRecipes(con);
						});
						";
						$data['js'].="
						function getRecipes(x){
							x = (typeof x === 'undefined') ? '' : x;
							x = (typeof x === 'object') ? JSON.stringify(x) : x;
							var url = 'inc_controller.php?action=".$form_object_type.".Recipients&id=".$form_object_id."&activity=".$page_action."&extra='+x;
							//console.log(url);
							recipes = AssistHelper.doAjax(url);
							//console.log(recipes);
							$('#notification_btns').buttonset('refresh');
							if(window.noteTp === 0){
								drawRecipes(recipes);
							}else{
								drawBoth(recipes);
							}
						}
						
						
						function drawRecipes(recipes){
						$('#notification_recipients').html('');
						$('#notification_btns').buttonset('refresh');
							for(x in recipes){
								if(recipes[x] instanceof Array){
									$('#notification_recipients').append('<p>'+x+'s:');
									for(var i=0; i< recipes[x].length; i++){
										$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;"; if($checks){ $data['js'].= "<input type=checkbox name=\"excluder-'+recipes[x][i].tkid+'\" />"; } $data['js'].="'+recipes[x][i].name+' - '+recipes[x][i].email+'<br>');
									}
									$('#notification_recipients').append('</p>');
								}else{
									$('#notification_recipients').append('<p>'+x+':</p> &nbsp;&nbsp;&nbsp;"; if($checks){ $data['js'].= "<input type=checkbox name=\"excluder-'+recipes[x].tkid+'\" />"; } $data['js'].="'+recipes[x].name+' - '+recipes[x].email+'<br>');
								}
							}
						}
		
						
						$('#mail_btn').click(function(){
							$('#notification_chooser').val('email');
							window.noteTp = 0;
							drawRecipes(recipes);
						});
						
						function drawBoth(recipes){
							$('#notification_recipients').html('');
							$('#notification_btns').buttonset('refresh');
							for(x in recipes){
								if(recipes[x] instanceof Array){
									$('#notification_recipients').append('<p>'+x+'s:');
									for(var i=0; i< recipes[x].length; i++){
										$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;"; if($checks){ $data['js'].= "<input type=checkbox name=\"excluder-'+recipes[x][i].tkid+'\" />"; } $data['js'].="<input hidden id=\"recipient_mobile_'+recipes[x][i].tkid+'\" name=\"recipient_mobile['+recipes[x][i].tkid+']\" value=\"'+recipes[x][i].mobile+'\" />'+recipes[x][i].name+' - '+recipes[x][i].email+' - '+recipes[x][i].mobile+'<br>');
									}
									$('#notification_recipients').append('</p>');
								}else{
									$('#notification_recipients').append('<input hidden id=\"recipient_mobile_'+recipes[x].tkid+'\" name=\"recipient_mobile['+recipes[x].tkid+']\" value=\"'+recipes[x].mobile+'\" /><p>'+x+':</p> &nbsp;&nbsp;&nbsp;"; if($checks){ $data['js'].= "<input type=checkbox name=\"excluder-'+recipes[x].tkid+'\" />"; } $data['js'].="'+recipes[x].name+' - '+recipes[x].email+' - '+recipes[x].mobile+'<br>');
								}
							}
						}
						
						$('#both_btn').click(function(){
							$('#notification_chooser').val('both');
							window.noteTp = 1;
							drawBoth(recipes);
						});
						if(useSMS[0] == false){
							$('#sms_btn, #both_btn').button('option', 'disabled', true);
							$('#mail_btn').attr('checked','checked');
							$('#notification_btns').buttonset('refresh');
						}else{
						}
						
						//Owner change trigger - to update the recipients list
						$(\"select[id*='owner']\").change(function(){
							if('".$form_object_type."' == 'CONTRACT' && '".$page_action."' == 'Add'){
								con.owner = $(this).val();
								$(\"input[name|='excluder']\").each(function(){
									if($(this).prop('checked') == false){
											
									}
								});
								getRecipes(con);
								//$"."form = $(\"form[name=".$form_name."]\");
								//alert(AssistForm.serialize($"."form));
							}else{
								getRecipes($(this).val());
							}
						});
						
						function cancelRecipes() {
							$('#notification_recipients').html('');
							$('#notification_btns').buttonset('refresh');
						}
						
						$('#none_btn').click(function(){
							$('#notification_chooser').val('none');
							window.noteTp = 0;
							cancelRecipes();
						});						
						//debugger;
					";
				}
				if(!$is_copy_page) {
					$echo.="
					<tr>
						<th></th>
						<td>
							<input type=button value=\"Save ".$formObject->getObjectName($form_object_type)."\" class='isubmit save-btn' id=btn_save />
							".($form_object_type=="CONTRACT" ? "<input type=hidden name=last_deliverable_status value='".$last_deliverable_status."' />" : "")."
							<input type=hidden name=object_id value=".$form_object_id." />
							".(($form_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
						</td>
					</tr>";
				}
			$echo.="
			</table>
		".($display_form ? "</form>" : "");
		$data['js'].="	
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($form_object_type)."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"
				
				
				".$js."
				
				";

		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			if($form_object_type=="DELIVERABLE") {
				$data['js'].="
				function setDeadlineForMainDel() {
					$('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");
				}
				setDeadlineForMainDel();
				function setDeadlineForSubDel(new_date) {
					new_date = \"+\"+new_date+\"D\";
					$('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",new_date);
					//alert(new_date);
				} 
				
				";
			} else {
				$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
			}
		}
		if($form_object_type=="DELIVERABLE") {
			$data['js'].="
				$(\"#del_type\").change(function() {
					if($(this).val()==\"SUB\") {
						$(\".tr_del_type\").show();
					} else {
						$(\".tr_del_type\").hide();
						setDeadlineForMainDel();
					}
				});
				
				if($(\"#del_parent_id\").children(\"option\").length<=1) {
					$(\"#del_type option[value=SUB]\").prop(\"disabled\",true);
				}
				$(\"#del_type\").trigger(\"change\");
				
				$(\"#del_parent_id\").change(function() {
					if($(this).val()=='X' || $(this).val()*1==0) {
						setDeadlineForMainDel();
					} else {
						var p_d = $(this).find('option:selected').attr('parent_deadline');
						setDeadlineForSubDel(p_d);
					}
				});
							
			";
		}		
		$data['js'].="
				
				
				$(\"form[name=".$form_name."] select\").each(function() {
					//if($(this).children(\"option\").length<=1 && !$(this).hasClass(\"required\")) {
					//}
					//console.log($(this).prop('id')+' = '+$(this).val());
					$(this).trigger('change');
					//console.log($(this).prop('id')+' = '+$(this).val());

					//if select only has 1 option + unspecified then auto select the second option
					//alert($(this).children(\"option\").length);
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});
								
				$(\"form[name=".$form_name."] input:button.save-btn\").click(function() {
					$"."form = $(\"form[name=".$form_name."]\");
					//$"."form.find(\"input[name|='excluder']\").each(function(){
					//	console.log($(this).prop('checked'));
					//});
					console.log(AssistForm.serialize($"."form));";
					//alert(AssistForm.serialize($"."form));";
		if($attachment_form) {
			$data['js'].="
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//alert(f);
					if(f>0) {
						AITHelper.processObjectFormWithAttachment($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					} else {
						$('#has_attachments').val(0);
						//alert(AssistForm.serialize($"."form));
						AITHelper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					}
					";
		} else {
			$data['js'].="
					AITHelper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					";
		}
		$data['js'].="
					return false;
				});
				
				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}
				
				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");		
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();   
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}
				
				$data['js'].="

				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});
				
				//alert(AssistForm.serialize($(\"form[name=frm_deliverable]\")));
				
				
			";
				
				
		$data['display'] = $echo;
		return $data;		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*******************
	 * JS code for handling forms displayed inside iframe inside dialogs
	 * 
	 * @param (String) object_type
	 * @param (Array) additional variables
	 * @return (String) JQuery
	 */
	public function getIframeDialogJS($object_type,$parent_dlg_id,$var) {
		$echo = "";
		if($object_type=="DELIVERABLE") {
			if(isset($var['del_category_id']) && ($var['del_category_id'])>0) {
				$dci = $var['del_category_id'];
				$echo.= "
				$('table.tbl-container form #del_category_id').val('".$dci."').trigger('change');
				$('table.tbl-container form #del_category_id option:not(:selected)').remove();
				";
			}
			if(isset($var['del_type']) && strlen($var['del_type'])>0) {
				$dt = $var['del_type'];
				$echo.= "
				$('table.tbl-container form #del_type').val('".$dt."').trigger('change');
				$('table.tbl-container form #del_type option:not(:selected)').remove();
				";
			}
			if(isset($var['del_parent_id']) && $var['del_parent_id']>0) {
				$dpi = $var['del_parent_id'];
				$echo.= "
				$('table.tbl-container form #del_parent_id').val('".$dpi."').trigger('change');
				$('table.tbl-container form #del_parent_id option:not(:selected)').remove();
				";
			}
		}
		
		$echo.= "
		var dlg_size_buffer = 70;
		var ifr_size_buffer = 20;
		window.parent.$('#".$parent_dlg_id."').dialog('open');
		";
		if($object_type=="ACTION") {
			$echo.= "
			var my_width = $('table.tbl-container').css('width');
			if(AssistString.stripos(my_width,'px')>0) {
				my_width = parseInt(AssistString.substr(my_width,0,-2))+ifr_size_buffer;
				window.parent.$('#".$parent_dlg_id."').find('iframe').prop('width',(my_width)+'px');
				var dlg_width = window.parent.$('#".$parent_dlg_id."').dialog('option','width');
				var test_width = my_width+dlg_size_buffer;
				if(dlg_width > test_width) {
					window.parent.$('#".$parent_dlg_id."').dialog('option','width',test_width);
				}
			}
			";
		}
		$echo.= "
		var my_height = $('table.tbl-container').css('height');
		if(AssistString.stripos(my_height,'px')>0) {
			my_height = parseInt(AssistString.substr(my_height,0,-2))+ifr_size_buffer;
			window.parent.$('#".$parent_dlg_id."').find('iframe').prop('height',(my_height)+'px');
			var dlg_height = window.parent.$('#".$parent_dlg_id."').dialog('option','height');
			var test_height = my_height+dlg_size_buffer;
			if(dlg_height > test_height) {
				window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
				var check = !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')));
				while(!check) {
					window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
					test_height+=dlg_size_buffer;
					if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')))) {
						check = true;
					}
				}
			}
		}
		";
		return $echo;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 
	 * Returns filter for selecting parent objects according to section it is fed
	 * 
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";
				
		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;
					
					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;
			
			case 'DELIVERABLE':
				$dsp = "contracts get";
				
				break;
			
			case 'ACTION':
				$dsp = "deliverables get";
				
				break;
			
			default:
				$dsp = "Invalid arguments supplied";
				break;
		}
		
		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";
		
        return $data;
    }
	

	/** 
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 * 
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from AIT_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from AIT_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].= 
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>	
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';
					
												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript
					
					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}
		
		return $data;
    }
}


?>