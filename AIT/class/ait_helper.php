<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class AIT_HELPER extends ASSIST_MODULE_HELPER {  
	
	private $default_object_type = "ACTION";
	
	
	
	
	private $my_attachment_download_link = "inc_attachment_controller.php";
	private $my_attachment_download_options = "action=GET_ATTACH";
	private $my_attachment_delete_link = "inc_attachment_controller.php";
	private $my_attachment_delete_options = "action=DELETE_ATTACH";
	private $my_attachment_delete_ajax = true;
	private $my_attachment_delete_function = "doDeleteAttachment";
	
	
	
	
	protected $object_names;
	protected $default_object_names = array(
		'contract'=>"Contract",
		'contract name'=>"Contract Name",
		'deliverable'=>"Deliverable",
		'deliverable name'=>"Deliverable Name",
		'deliverables'=>"Deliverables",
		'action'=>"Action",
		'action name'=>"Action Name",
		'actions'=>"Actions",
		'finance'=>"Finance",
		'template'=>"Template",
		'templates'=>"Templates",
		'attachment'=>"Attachment",
		'attachments'=>"Attachments",
		'assurance'=>"Assurance",
		'contract_assurance'=>"Contract Assurance",
		'deliverable_assurance'=>"Deliverable Assurance",
		'action_assurance'=>"Action Assurance",
		'drawdown'=>"Drawdown",
		'budget'=>"Budget",
		'budgets'=>"Budgets",
		'adjustment'=>"Adjustment",
		'adjustments'=>"Adjustments",
		'expense'=>"Expense",
		'expenses'=>"Expenses",
		'income'=>"Income",
		'incomes'=>"Incomes",
		'retention'=>"Retention",
		'retentions'=>"Retentions",
		'supplier'=>"Supplier",
		'suppliers'=>"Suppliers",
		'dashboard'=>"Dashboard",
		'notification'=>"Notification",
		'notifications'=>"Notifications",
	);
	protected $object_name_menu_id = array('contract'=>1,'deliverable'=>2,'action'=>3,'finance'=>4,'template'=>5);
	
	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct() {
		parent::__construct();
		//$this->object_names = $this->default_object_names;
		//$this->activity_names = $this->default_activity_names;
		
		$this->setDefaultObjectNames($this->default_object_names);
		
        $this->checkObjectNames();
        $this->checkActivityNames();
				
		
		$this->setAttachmentDownloadOptions();
		$this->setAttachmentDeleteOptions();
		$this->setHelperAttachmentDownloadLink($this->getAttachmentDownloadLink());
		$this->setHelperAttachmentDeleteLink($this->getAttachmentDeleteLink());
		$this->setHelperAttachmentDeleteByAjax($this->getAttachmentDeleteByAjax());
		$this->setHelperAttachmentDeleteFunction($this->getAttachmentDeleteFunction());
	}


















	/*********************************
	 * ATTACHMENT FUNCTIONS
	*/


	protected function setAttachmentDownloadOptions($action="GET_ATTACH",$add_to_default=true) {
		if($action=="") {	//reset to default
			$this->my_attachment_download_options = "action=GET_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_download_options .= "&action=".$action;
		} else {
			$this->my_attachment_download_options = $action;
		}
		$this->setHelperAttachmentDownloadOptions($this->my_attachment_download_options);
	}
	
	protected function setAttachmentDeleteOptions($action="",$add_to_default=true) {
		if($action=="") {	//reset to default
			$this->my_attachment_delete_options = "action=DELETE_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_delete_options .= "&action=".$action;
		} else {
			$this->my_attachment_delete_options = $action;
		}
		$this->setHelperAttachmentDeleteOptions($this->my_attachment_delete_options);
	}
	
	public function getAttachmentDeleteOptions() { return $this->my_attachment_delete_options; }
	public function getAttachmentDeleteLink() { return $this->my_attachment_delete_link; }
	public function getAttachmentDownloadOptions() { return $this->my_attachment_download_options; }
	public function getAttachmentDownloadLink() { return $this->my_attachment_download_link; }
	public function getAttachmentDeleteFolder() { return $this->getModRef()."/deleted"; }
	public function getAttachmentDeleteByAjax() { return $this->my_attachment_delete_ajax; }
	public function getAttachmentDeleteFunction() { return $this->my_attachment_delete_function; }

	public function getAttachmentFieldName() {
		return $this->attachment_field;
	}









	public function getStorageFolder() {
		return strtoupper($this->getModRef())."/".strtolower($this->getMyObjectType());
	}
	
	public function getFullFolderPath() {
		$folder = $this->getParentFolderPath()."/".$this->getStorageFolder();
		return $folder;	
	}
	
	
	public function getDeletedFolder() {
		$folder = $this->getParentFolderPath()."/".strtoupper($this->getModRef())."/deleted";
		return $folder;
	}
	
	
	public function getParentFolderPath() {
		$folder = "";
		$location = explode("/",$_SERVER["REQUEST_URI"]);
		$l = count($location)-2;
		for($f=0;$f<$l;$f++) {
			$folder.= "../";
		}
		$folder.="files/".$this->getCmpCode();
		return $folder;	
	}

	public function getDeletedFileName($object_type,$sys) {
		return $object_type."_".date("YmdHis")."_".$sys;
	}




	
	
	/********************************
	 * Attachment processing functions for objects
	 */
	
	
	/**
	 * Function to save attachments in the database
	 */
	public function saveAttachments($activity,$object_id,$attach) {
		$attach = serialize($attach);
		//$attach = base64_encode($attach);
		switch($activity) {
			case "ADD":
			case "EDIT":
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getAttachmentFieldName()." = '".($attach)."' WHERE ".$this->getIDFieldName()." = ".$object_id;
				break;
			case "UPDATE":
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getUpdateAttachmentFieldName()." = '".($attach)."' WHERE ".$this->getIDFieldName()." = ".$object_id;
				break;
		}
		$mnr = $this->db_update($sql);
		return $mnr;
	}
	
	/**
	 * Function to get attachments from a specific object
	 */
	public function getAttachmentDetails($object_id,$i="all",$activity="") {
		if($activity=="UPDATE") {
			$attach_field = $this->getUpdateAttachmentFieldName();
		} else {
			$attach_field = $this->getAttachmentFieldName();
		}
		$sql = "SELECT ".$attach_field." as a FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$a = $this->mysql_fetch_one_value($sql, "a");
		if(strlen($a)==0) {
			$attach = array();
		} else {
			if(substr($a,0,2)!="a:") {
				$attach = unserialize(base64_decode($a));
			} else {
				$attach = unserialize($a);
			}
		}
		if($i=="all") {
			return $attach;
		} else {
			return $attach[$i];
		}
	}
	
	public function deleteAttachment($object_id,$i,$activity="") {
		//get original attachment details
		$attach = $this->getAttachmentDetails($object_id,"all",$activity);
		$obj = $this->getRawObject($object_id);
		//update $i attachment status to deleted
		$old = $attach[$i];
		$attach[$i]['status'] = AIT::DELETED;
		$attach[$i]['deleted_location'] = $this->getDeletedFolder();
		$attach[$i]['deleted_filename'] = $this->getDeletedFileName($this->getMyObjectType(),$old['system_filename']);
		//update table
		$mnr = $this->saveAttachments($activity, $object_id, $attach);
		if($mnr>0) {
			//activity log
				$changes = array(
					'user'=>$this->getUserName(),
					'contract_attachment'=>"|attachment| ".$old['original_filename']." was removed.",
				);
				$log_var = array(
					'object_id'	=> $object_id,
					'changes'	=> $changes,
					'log_type'	=> ($activity=="UPDATE" ? AIT_LOG::UPDATE : AIT_LOG::EDIT),
					'progress'	=> $obj[$this->getProgressFieldName()],
					'status_id'	=> $obj[$this->getProgressStatusFieldName()],		
				);
				$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok",$this->getObjectName("attachment")." ".$old['original_filename']." successfully removed.");
		} else {
			return array("error","An error occurred while trying to ".$this->getActivityName("delete")." ".$this->getObjectName("attachment").".  Please reload the page and try again.");
		}
	}
	
	



































/*************************************
 * NAMING functions
 */
 
    protected function checkObjectNames($modref="") {
    	if(strlen($modref)==0) {
    		$modref = $this->getModRef();
    	}
        if(isset($_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP']) && $_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME']-1800) && isset($_SESSION[$modref]['OBJECT_NAMES'])) {
            $on = $_SESSION[$modref]['OBJECT_NAMES'];
        } else {
        	$nameObject = new AIT_NAMES();
        	$on = $nameObject->fetchObjectNames();
            $_SESSION[$modref]['OBJECT_NAMES'] = $on;
            $_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
        }
        $this->setObjectNames($on);
    } 
    protected function checkActivityNames($modref="") {
    	if(strlen($modref)==0) {
    		$modref = $this->getModRef();
    	}
        if(isset($_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP']) && $_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME']-1800) && isset($_SESSION[$modref]['ACTIVITY_NAMES'])) {
            $an = $_SESSION[$modref]['ACTIVITY_NAMES'];
        } else {
        	$nameObject = new AIT_NAMES();
        	$an = $nameObject->fetchActivityNames();
            $_SESSION[$modref]['ACTIVITY_NAMES'] = $an;
            $_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
        }
		$this->setActivityNames($an);
    } 

	protected function formatRowsForSelect($rows,$name="name") {
		$data = array();
		foreach($rows as $key => $r) {
			$data[$key] = $r[$name];
		}
		return $data;
	}
     
	 
	 
	 
	 
   /******************************************
    * FUNCTIONS HERE ARE TO PREVENT INFINITE LOOPS!!!!!!
    */
   
	 
   /**
    * Get the object names from the table in easy readable format
    */
	protected function fetchObjectNames() {
		$names = array();
		$sql = "SELECT menu_section as section, IF(LENGTH(menu_client)>0,menu_client,menu_default) as name 
				FROM ".$this->getDBRef()."_setup_menu 
				WHERE (menu_status & ".AIT_MENU::OBJECT_HEADING." = ".AIT_MENU::OBJECT_HEADING.")";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r){
			$s = explode("_",$r['section']);
			$names[end($s)] = $r['name'];
		}
		return $names;
	}


	public function getObjectNameMenuID($o) {
    	$x = explode("_",$o);
		if(count($x)>1 && $x[0]=="TEMPLATE"){
			$o=$x[1];
		}	
    	return $this->object_name_menu_id[strtolower($o)]; 
	}
    public function getContractObjectName($plural=false) { return $this->getObjectName("contract").($plural?"s":""); }
    public function getDeliverableObjectName($plural=false) { return $this->getObjectName("deliverable").($plural?"s":""); }
    public function getActionObjectName($plural=false) { return $this->getObjectName("action").($plural?"s":""); }
	public function getTemplateObjectName($plural=false) { return $this->getObjectName("template").($plural?"s":""); }

	/**
	 * Function to replace both |object| and |activity| with the current usage
	 *//*
	public function replaceAllNames($arr) {
		$arr = $this->replaceObjectNames($arr);
		$arr = $this->replaceActivityNames($arr);
		return $arr;
	}
    /**
	 * Function to replace |object| with object_name
	 *//*
    public function replaceObjectNames($arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($this->object_names as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($this->object_names as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }  

    /**
	 * Function to replace |field| with head[name]
	 *//*
    public function replaceHeadingNames($head,$arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($head as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($head as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }
    */
	
	
		 
	protected function isDateField($fld) {
		if(strrpos($fld,"date")!==FALSE || strrpos($fld, "reminder")!==FALSE || strrpos($fld, "deadline")!==FALSE || strrpos($fld,"action_on")!==FALSE) {
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*************************
	 * Module generic functions
	 */
	

	public function addActivityLog($log_table,$var) {
		$logObject = new AIT_LOG($log_table);
		$logObject->addObject($var);
	}









	/**
	 * Function to generate SQL statement for bitwise status check
	 */
	protected function generateStatusSQL($status,$compare,$t="",$tbl_field_prefix="") {
		if(strlen($tbl_field_prefix)==0) { $tbl_field_prefix = $this->getTableField(); }
		return "( ".(strlen($t)>0 ? $t."." : "")."".$tbl_field_prefix."_status & ".$status.") ".$compare." ".$status;
	}
	/**
	 * Generate Status SQL statement dependent on page
	 */
	protected function getStatusSQL($section="REPORT",$t="",$deleted=false,$is_contract=true,$tbl_field_prefix="") {
		$statuses = array(0=>array(),1=>array());
		if($deleted) {
			$statuses[1][] = AIT::DELETED; 
			$statuses[0][] = AIT::ACTIVE; 
		} else {
			$statuses[0][] = AIT::DELETED; 
			$statuses[1][] = AIT::ACTIVE; 
		}
		switch($section) {
			case "CONFIRMATION":
			case "NEW":
				if($is_contract){
					$statuses[0][] = AIT_CONTRACT::CONFIRMED; 
					$statuses[0][] = AIT_CONTRACT::ACTIVATED;
				} 
				break;
			case "ACTIVATION":
				if($is_contract){
					$statuses[1][] = AIT_CONTRACT::CONFIRMED; 
					$statuses[0][] = AIT_CONTRACT::ACTIVATED;
				} 
				break;
			case "ALL":
			case "DELETED":
				break;
			case "REPORT":
			default:
				if($is_contract) {
					$statuses[1][] = AIT_CONTRACT::CONFIRMED; 
					$statuses[1][] = AIT_CONTRACT::ACTIVATED;
				} 
				break;
		}
		$sql = array();  //print_r($statuses);
		foreach($statuses[0] as $not) {
			$sql[] = $this->generateStatusSQL($not,"<>",$t,$tbl_field_prefix);
		}
		foreach($statuses[1] as $yes) {
			$sql[] = $this->generateStatusSQL($yes,"=",$t,$tbl_field_prefix);
		}
		return " (".implode(" AND ",$sql).") ";
	}    
     
	 
	 











}

?>