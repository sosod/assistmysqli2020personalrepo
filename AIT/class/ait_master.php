<?php
/**
 * To manage any MASTER FILE classes
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class AIT_MASTER extends AIT {
    
    private $master_list = "";
    
    public function __construct($ml="") {
        parent::__construct();
		$this->master_list = $ml;
		$this->fields = $this->getFields();
    }
    

/**************
 * FINANCIAL YEARS
 */
	public function getActiveItems($id="") {
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$rows = $masterObject->getActive($ids="");
		return $rows;
	}
    public function getActiveItemsFormattedForSelect($ids="") {
    	return $this->formatRowsForSelect($this->getActiveItems($ids),"value");
    }
	public function getFields(){
		$fields = array();
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$fields['id'] = $masterObject->getFld("ID");
		$fields['name'] = $masterObject->getFld("NAME");
		$fields['table'] = "assist_".$this->getCmpCode()."_".$masterObject->getFld("TABLE");
		unset($masterObject);
		return $fields;
	}
    public function getItemsForReport() {
    	$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$contractObject = new AIT_CONTRACT();
    	return $masterObject->getLinkedForReport($contractObject->getTableName(),$contractObject->getTableField()."_financial_year_id");
    }
	public function getSortBy() {
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		return $masterObject->getSortBy("",true);
	}
}


?>