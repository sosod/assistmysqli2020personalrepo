<?php

class AIT_TIME extends AIT {

	private $annual_public_holidays = array(
		"1 January",
		"21 March",
		"27 April",
		"1 May",
		"16 June",
		"9 August",
		"24 September",
		"16 December",
		"25 December",
		"26 December",
	);
	private $flexible_public_holidays = array(
		'2017'=>array(
			"2 January 2017",
			"14 April 2017",
			"17 April 2017",
			"25 September 2017",
		),
		'2018'=>array(
			"30 March 2018",
			"2 April 2018",
			"17 December 2018",
		),
	);

	private $autocomplete_list_fields = array(
		'time_reseller'=>"assist_reseller",
		'time_client'=>"assist_company",
		'time_project'=>"list_project",
		'time_activity'=>"list_activity",
		'time_location'=>"list_location",
	);

	private $boolean_fields = array(
		"time_billable",
		"time_product",
		"time_support",
		"time_private",
	);

	private $financial_year = array(
		'start'=>array('day'=>1,'month'=>"3"),
		'end'=>array('day'=>28,'month'=>"2"),
	);
	private $first_financial_year = array(
		'start'=>"1 March 2017 00:00:00",
		'end'=>"28 February 2018 23:59:59",
	);


	const TABLE_NAME = "time";
	const TABLE_FIELD = "time";

	const REFTAG = "TIME";


	const COMPLETE = 256;

	public function __construct() {
		parent::__construct();
		$this->first_financial_year['start'] = strtotime($this->first_financial_year['start']);
		$this->first_financial_year['end'] = strtotime($this->first_financial_year['end']);
	}


	/***************************
	 * CONTROLLER FUNCTIONS
	 */


	public function addObject($var) {
		$result = array("error","TIME.ADD not programmed");
		$result = $this->saveObject($var,"ADD");
		return $result;
	}
	public function editObject($var) {
		$result = array("error","TIME.EDIT not programmed");
		$object_id = $var['time_id'];
		unset($var['time_id']);
		$result = $this->saveObject($var,"EDIT",$object_id);
		return $result;
	}

	public function saveObject($var,$action="ADD",$object_id=0) {
//$this->arrPrint($var);

		$number_of_records_selected = $var['records'];			unset($var['records']);
		$date_selected = $var['time_date'];						unset($var['time_date']);

		$autocomplete_texts = array();
		$freshly_added_autocomplete_items = array();
		$original_list_items = array();
		$original_list_items_reversed = array();
		$private_list_items = array();
		$private_list_items_reversed = array();
		$listObject = new AIT_LIST();

		$include_external_sources = false;

		foreach($this->autocomplete_list_fields as $fld => $list) {
			$autocomplete_texts[$fld] = $var[$fld.'_display'];			unset($var[$fld.'_display']);
			$freshly_added_autocomplete_items[$fld] = array();
			//get private list items
			$listObject->changeListType($list);
			$original_list_items[$fld] = $listObject->getActiveListItemsFormattedForSelect(" (is_private = 0 OR owner = '".$this->getUserID()."' ) ",$include_external_sources);
			foreach($original_list_items[$fld] as $id => $item) {
				$i = str_replace(" ", "", strtolower($item));
				if(!isset($original_list_items_reversed[$fld][$i])) {
					$original_list_items_reversed[$fld][$i] = $id;
				}
			}
			$private_list_items[$fld] = $listObject->getActiveListItemsFormattedForSelect(" (is_private = 1 AND owner <> '".$this->getUserID()."' ) ",$include_external_sources);
			foreach($private_list_items[$fld] as $id => $item) {
				$i = str_replace(" ", "", strtolower($item));
				if(!isset($private_list_items_reversed[$fld][$i])) {
					$private_list_items_reversed[$fld][$i] = $id;
				}
			}
		}

		$records_saved = 0;
		$records_found_to_save = 0;
		if(isset($var['record']) && count($var['record'])>0) {
			$records = $var['record'];							unset($var['record']);
			foreach($records as $key => $r) {
				if($r=="Y") {
					$records_found_to_save++;

					$comment = $var['time_description'][$key];
					$ref = $var['time_project_ref'][$key];

					//Process time
					$start = date("Y-m-d",$date_selected)." ".$var['time_start'][$key];
					$end = date("Y-m-d",$date_selected)." ".$var['time_end'][$key];
					$minutes = round((strtotime($end) - strtotime($start))/60);

					//Set data to be saved
					$insert_data = array(
						$this->getTableField().'_tkid' => $this->getUserID(),
						$this->getTableField().'_start' => $start,
						$this->getTableField().'_end' => $end,
						$this->getTableField().'_minutes' => $minutes,
						$this->getTableField().'_description' => ($comment),
						$this->getTableField().'_project_ref' => ($ref),
					);
					if($action=="ADD") {
						$insert_data[$this->getTableField().'_status'] = self::ACTIVE;
						$insert_data[$this->getTableField().'_insertuser'] = $this->getUserID();
						$insert_data[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
					}

					//Process lists
					foreach($this->autocomplete_list_fields as $fld => $list) {
						$value = $var[$fld][$key];
						//if the value is 0 then it is a new item BUT it could already exist as a private item for someone else
						if($value.""=="0" || !is_numeric($value) || strlen($value)==0) {
							$text = $autocomplete_texts[$fld][$key];
							//1. check if it has already been added as a new item on this submission
							//2. check if it exists as a private item elsewhere
							//3. check that it doesn't already exist (value might not be set if user manually typed full text in and didn't select from autocomplete list)
							//4. otherwise add
							if(isset($freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))])) {
								$value = $freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))];
							} elseif(isset($private_list_items_reversed[$fld][str_replace(" ", "", strtolower($text))])) {
								//get the value
								$value = $private_list_items_reversed[$fld][str_replace(" ", "", strtolower($text))];
								//add it to the freshly added list so that it doesn't trigger this process again
								$freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))] = $value;
								//strip the private marker from the list so that it is available to all
								$listObject->changeListType($list);
								$return = $listObject->removePrivateMarker($value);
							} elseif(isset($original_list_items_reversed[$fld][str_replace(" ", "", strtolower($text))])) {
								//get the value
								$value = $original_list_items_reversed[$fld][str_replace(" ", "", strtolower($text))];
							} else {
								$listObject->changeListType($list);
								$return = $listObject->addFromAutoComplete($text,$value,$var['time_private'][$key]);
								$value = $return['id'];
								$freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))] = $value;
							}
							//REPLACED ON 18 NOV TO ACCOUNT FOR PRIVATE ITEMS
							/*if(!isset($freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))])) {
								$listObject = new AIT_LIST($list);
								$return = $listObject->addFromAutoComplete($text,$value,$var['time_private'][$key]);
								$value = $return['id'];
								$freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))] = $value;
							} else {
								$value = $freshly_added_autocomplete_items[$fld][str_replace(" ", "", strtolower($text))];
							}*/
						}
						$insert_data[$fld] = $value;
					}

					//Process BOOL
					foreach($this->boolean_fields as $fld) {
						$insert_data[$fld] = $var[$fld][$key];
					}

					if($action=="ADD") {
						$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
						$object_id = $this->db_insert($sql);
						if($object_id>0) {
							$records_saved++;
						}
					}
					if($action=="EDIT") {
						$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE time_id = ".$object_id;
						$mar = $this->db_update($sql);
						if($mar>0) {
							$records_saved++;
						}
					}
				}
			}
		}
		unset($listObject);
		if($records_found_to_save==0 || (isset($var['record']) && count($var['record'])==0)) {
			$result = array("error","You tried to save blank records.");
		} elseif($records_saved==0) {
			$result = array("error","An error occurred while trying to save your records.");
		} else {
			$result = array("ok",$records_saved." time record(s) saved.");
		}
		return $result;
	}



	public function markDateComplete($var) {
		$date_to_complete = $var['date_to_mark'];
		$sql = "SELECT * FROM ".$this->getTableName()."_status
				WHERE ts_tkid = '".$this->getUserID()."'
				AND ts_date = '".$date_to_complete."'";
		$test_rows = $this->mysql_fetch_all($sql);
		if(count($test_rows)==0) {
			$sql = "INSERT INTO ".$this->getTableName()."_status VALUES (null, '".$this->getUserID()."', '".$date_to_complete."', ".(self::ACTIVE + self::COMPLETE).")";
			$this->db_insert($sql);
		} else {
			$sql = "UPDATE ".$this->getTableName()."_status
					SET ts_status = ".(self::ACTIVE+self::COMPLETE)."
					WHERE ts_tkid = '".$this->getUserID()."'
					AND ts_date = '".$date_to_complete."'";
			$this->db_update($sql);
		}
		return array("ok",date("d F Y",strtotime($date_to_complete))." has been marked as complete");
	}

	public function undoDateComplete($var) {
		$date_to_complete = $var['date_to_mark'];
		$sql = "UPDATE ".$this->getTableName()."_status
				SET ts_status = ".self::ACTIVE."
				WHERE ts_tkid = '".$this->getUserID()."'
				AND ts_date = '".$date_to_complete."'";
		$this->db_update($sql);
		return array("ok",date("d F Y",strtotime($date_to_complete))." has been marked as incomplete.");
	}













	/***************************
	 * GET/SET functions
	 */
	public function getTableName() { return $this->getDBRef()."_".self::TABLE_NAME; }
	public function getTableField() { return self::TABLE_FIELD; }
	public function getRefTag() { return self::REFTAG; }




	public function getRawObject($id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE time_id = ".$id;
		return $this->mysql_fetch_one($sql);
	}


	public function getLastEndTimeForToday($date_to_retrieve) {
		$start = date("Y-m-d",$date_to_retrieve)." 00:00:00";
		$end = date("Y-m-d",$date_to_retrieve)." 23:59:59";
		$sql = "SELECT *
				FROM ".$this->getTableName()."
				WHERE time_end >= '$start'
				AND time_end <= '$end'
				AND time_tkid = '".$this->getUserID()."'
				AND time_status = ".self::ACTIVE."
				ORDER BY time_end DESC
				LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		if(count($row)>0 && isset($row['time_end']) && strlen($row['time_end'])>0) {
			return date("H:i",strtotime($row['time_end']));
		} else {
			return "";
		}

	}

	public function getMyDetailedRecords($date_to_retrieve) {
		$start = date("Y-m-d",$date_to_retrieve)." 00:00:00";
		$end = date("Y-m-d",$date_to_retrieve)." 23:59:59";
		$sql = "SELECT *
				FROM ".$this->getTableName()."
				WHERE time_start >= '$start'
				AND time_start <= '$end'
				AND time_tkid = '".$this->getUserID()."'
				AND time_status = ".self::ACTIVE."
				ORDER BY time_start";
		$rows = $this->mysql_fetch_all($sql);

		$list_items = array();
		$listObject = new AIT_LIST("assist_reseller");
		$list_items['time_reseller'] = $listObject->getAllListItemsFormattedForSelect();
		$listObject->changeListType("assist_company");
		$list_items['time_client'] = $listObject->getAllListItemsFormattedForSelect();
		$listObject->changeListType("list_project");
		$list_items['time_project'] = $listObject->getAllListItemsFormattedForSelect();
		$listObject->changeListType("list_activity");
		$list_items['time_activity'] = $listObject->getAllListItemsFormattedForSelect();
		$listObject->changeListType("list_location");
		$list_items['time_location'] = $listObject->getAllListItemsFormattedForSelect();

		//$this->arrPrint($list_items);

		foreach($rows as $i => $row) {
			foreach($row as $fld => $r) {
				if(isset($list_items[$fld])) {
					if(isset($list_items[$fld][$r])) {
						$rows[$i][$fld] = $list_items[$fld][$r];
					} else {
						$rows[$i][$fld] = $this->getUnspecified();
					}
				}
			}
		}

		return $rows;
	}



	public function getSummaryRecordsByMonth($date_to_retrieve) {
		$month = intval(date("m",($date_to_retrieve)));
		$year = intval(date("Y",($date_to_retrieve)));
		$sql = "SELECT COUNT(time_id) as records, SUM(time_minutes) as minutes, (SUM(time_minutes))/60 as hours, DAY(time_start) as day
				FROM ".$this->getTableName()."
				WHERE MONTH(time_start) = ".$month."
				AND YEAR(time_start) = ".$year."
				AND time_tkid = '".$this->getUserID()."'
				AND time_status = ".self::ACTIVE."
				GROUP BY DAY(time_start)";
		$records = $this->mysql_fetch_all_by_id($sql,"day");
		return $records;
	}


	public function getSummaryRecordsByMonthForBusiness($date_to_retrieve) {
		$month = intval(date("m",($date_to_retrieve)));
		$year = intval(date("Y",($date_to_retrieve)));
		$sql = "SELECT COUNT(time_id) as records, SUM(time_minutes) as minutes, (SUM(time_minutes))/60 as hours, DAY(time_start) as day
				FROM ".$this->getTableName()."
				WHERE MONTH(time_start) = ".$month."
				AND YEAR(time_start) = ".$year."
				AND time_tkid = '".$this->getUserID()."'
				AND time_private = 0
				AND time_status = ".self::ACTIVE."
				GROUP BY DAY(time_start)";
		$records = $this->mysql_fetch_all_by_id($sql,"day");
		return $records;
	}




	public function getCompletedDatesByMonth($date_to_retrieve) {
		$month = intval(date("m",($date_to_retrieve)));
		$year = intval(date("Y",($date_to_retrieve)));
		$sql = "SELECT ts_date as status_date, ts_status as status, ts_tkid as tkid
				FROM ".$this->getTableName()."_status
				WHERE MONTH(ts_date) = ".$month."
				AND YEAR(ts_date) = ".$year."
				AND ts_tkid = '".$this->getUserID()."'
				AND (ts_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (ts_status & ".self::COMPLETE.") = ".self::COMPLETE."
				";
		$records = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($records as $row) {
			$d = strtotime($row['status_date']." 12:00:00");
			$data[$d] = $row;
		}
		return $data;
	}

	public function isDateComplete($date_to_check,$tkid = "") {
		$date_to_check = date("Y-m-d",$date_to_check);
		if(strlen($tkid)==0) { $tkid = $this->getUserID(); }
		$sql = "SELECT *
				FROM ".$this->getTableName()."_status
				WHERE (ts_date) = '".$date_to_check."'
				AND (ts_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (ts_status & ".self::COMPLETE.") = ".self::COMPLETE."
				AND ts_tkid = '".$tkid."'
				";
		$data = $this->mysql_fetch_all($sql);
		return (count($data)>0);
	}




	public function getFinancialYear($date_to_retrieve) {
			if(date("m",$date_to_retrieve)*1 >= $this->financial_year['start']['month']) {
				$start_year = date("Y",$date_to_retrieve);
			} else {
				$start_year = date("Y",$date_to_retrieve)-1;
			}
			if($this->financial_year['end']['month'] > $this->financial_year['start']['month']) {
				$end_year = $start_year;
			} else {
				$end_year = $start_year+1;
			}
			$start = mktime(0,0,0,$this->financial_year['start']['month'],$this->financial_year['start']['day'],$start_year);
			//NOTE: End is calculated by deducting 1 second from start of next financial year - to catch any leap years === DON'T CHANGE START OF mktime TO 23:59:59
			$end = mktime(0,0,0,$this->financial_year['start']['month'],$this->financial_year['start']['day'],$end_year)-1;

		return array('start'=>$start,'end'=>$end);

	}

	public function getNextFinancialYear($date_to_check) {
		//assume date_to_check is current date being viewed
		//add a year
		if(!is_numeric($date_to_check)) {
			$date_to_check = strtotime($date_to_check);
		}
		$date_to_check+=365*24*60*60;
		//compare against current financial year - does it exceed the end of the current financial year?
		$current_date = time();
		$current_financial_year = $this->getFinancialYear($current_date);
		if($date_to_check>$current_financial_year['end']) {
			return false;
		} else {
			return $this->getFinancialYear($date_to_check);
		}
	}
	public function getPreviousFinancialYear($date_to_check) {
		//assume date_to_check is current date being viewed
		//deduct a year
		if(!is_numeric($date_to_check)) {
			$date_to_check = strtotime($date_to_check);
		}
		$date_to_check-=365*24*60*60;
		//compare against first financial year - does it exceed the start of the first financial year?
		$first_financial_year = $this->first_financial_year;
		if($date_to_check<$first_financial_year['start']) {
			return false;
		} else {
			return $this->getFinancialYear($date_to_check);
		}
	}

	/*********************
	 * Analysis / Pivot Tables
	 */
	public function getAnalysis($type,$date_to_retrieve,$type_to_retrieve="MONTH",$user_to_retrieve = "") {
		if($type_to_retrieve=="MONTH"){
			$month = intval(date("m",($date_to_retrieve)));
			$year = intval(date("Y",($date_to_retrieve)));
			$date_sql = "AND YEAR(T.time_start) = $year AND MONTH(T.time_start) = $month";
		} else {
			if(date("m",$date_to_retrieve)*1 >= $this->financial_year['start']['month']) {
				$start_year = date("Y",$date_to_retrieve);
			} else {
				$start_year = date("Y",$date_to_retrieve)-1;
			}
			if($this->financial_year['end']['month'] > $this->financial_year['start']['month']) {
				$end_year = $start_year;
			} else {
				$end_year = $start_year+1;
			}
			$start = mktime(0,0,0,$this->financial_year['start']['month'],$this->financial_year['start']['day'],$start_year);
			$end = mktime(0,0,0,$this->financial_year['start']['month'],$this->financial_year['start']['day'],$end_year)-1;

			$date_sql = "AND T.time_start >= '".date("Y-m-d H:i:s",$start)."' AND time_end <= '".date("Y-m-d H:i:s",$end)."'";
		}
		if(strlen($user_to_retrieve)==0) {
			$user_to_retrieve = $this->getUserID();
		}
		if($user_to_retrieve=="ALL") {
			$user_sql = "";
		} else {
			$user_sql = " AND T.`time_tkid` = '".$user_to_retrieve."' ";
		}
		$list_items = array();
		switch($type) {
			case "TOTAL":
				$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_private as type_time
				, T.time_client as client_id
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				$date_sql
				$user_sql
				GROUP BY T.time_private, T.time_client";
				$listObject = new AIT_LIST("assist_company");
				$list_items['client_id'] = $listObject->getAllListItemsFormattedForSelect();
				$list_items['type_time'] = array(
					'0'=>"Business",
					'1'=>"Private",
				);
				break;
			case "SUPPORT":
				$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_client as client_id
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				$date_sql
				$user_sql
				AND T.`time_private` = '0'
				AND T.time_support = 1
				GROUP BY T.time_client";
				$listObject = new AIT_LIST("assist_company");
				$list_items['client_id'] = $listObject->getAllListItemsFormattedForSelect();
				break;
			case "DEVELOPMENT":
				$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_activity as activity_id
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				$date_sql
				$user_sql
				AND T.`time_private` = '0'
				AND T.time_product = 1
				GROUP BY T.time_activity";
				$listObject = new AIT_LIST("list_activity");
				$list_items['activity_id'] = $listObject->getAllListItemsFormattedForSelect();
				break;
			case "BILLABLE":
				$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_activity as activity_id
				, T.time_project as project_id
				, T.time_client as client_id
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				$date_sql
				$user_sql
				AND T.`time_private` = '0'
				AND T.time_billable = 1
				GROUP BY T.time_activity";
				$listObject = new AIT_LIST("list_activity");
				$list_items['activity_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("assist_company");
				$list_items['client_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("list_project");
				$list_items['project_id'] = $listObject->getAllListItemsFormattedForSelect();
				break;
			case "TIME_BY_MONTH":
				$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_tkid as tkid
				, YEAR(T.time_start) as year
				, MONTH(T.time_start) as month
				, T.time_project as project_id
				, T.time_activity as activity_id
				, T.time_client as client_id
				, T.time_reseller as reseller_id
				, T.time_location as location_id
				, T.time_billable as is_billable
				, T.time_product as product_development
				, T.time_support as client_support
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				$date_sql
				$user_sql
				AND T.`time_private` = '0'
				GROUP BY
				T.time_tkid
				, YEAR(T.time_start)
				,MONTH(T.time_start)
				,T.time_project
				,T.time_activity
				,T.time_client
				,T.time_reseller
				,T.time_location
				,T.time_billable
				,T.time_product
				,T.time_support
				";
				$listObject = new AIT_LIST("list_project");
				$list_items['project_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("list_activity");
				$list_items['activity_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("list_location");
				$list_items['location_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("assist_company");
				$list_items['client_id'] = $listObject->getAllListItemsFormattedForSelect();
				$listObject->changeListType("assist_reseller");
				$list_items['reseller_id'] = $listObject->getAllListItemsFormattedForSelect();
				break;
		}

		$rows = $this->mysql_fetch_all($sql);
		$list_fields = array_keys($list_items);

		foreach($rows as $key => $row) {
			foreach($list_fields as $fld) {
				$exploded_fld = explode("_",$fld);
				$title_fld = $exploded_fld[0];
				$rows[$key][$title_fld] = $list_items[$fld][$row[$fld]];
			}
		}

		return $rows;

	}



	public function getAnalysisForReport($filters=array()) {
		$filter_sql = array();

		foreach($filters as $fld => $value) {
			switch($fld) {
				case "client":
					if(count($value)>0) {
						$filter_sql[] = "time_client IN (".implode(",",$value).")";
					}
					break;
				case "reseller":
					if(count($value)>0) {
						$filter_sql[] = "time_reseller IN (".implode(",",$value).")";
					}
					break;
				case "location":
					if(count($value)>0) {
						$filter_sql[] = "time_location IN (".implode(",",$value).")";
					}
					break;
				case "project":
					if(count($value)>0) {
						$filter_sql[] = "time_project IN (".implode(",",$value).")";
					}
					break;
				case "activity":
					if(count($value)>0) {
						$filter_sql[] = "time_activity IN (".implode(",",$value).")";
					}
					break;
				case "tkid":
					if(count($value)>0) {
						$filter_sql[] = "time_tkid IN ('".implode("','",$value)."')";
					}
					break;
				case "time":
					$value = ASSIST_HELPER::removeBlanksFromArray($value,false);
					if(count($value)==0) {
						//ignore
					} elseif(count($value)==1) {
						$value = $value[0];
						$filter_sql[] = "time_start >= '".date("Y-m-d",strtotime($value))." 00:00:00' AND time_end <= '".date("Y-m-d",strtotime($value))." 23:59:59'";
					} else {
						$value1 = $value[0];
						$value2 = $value[1];
						if($value1 == $value2) {
							$value = $value[0];
							$filter_sql[] = "time_start >= '".date("Y-m-d",strtotime($value))." 00:00:00' AND time_end <= '".date("Y-m-d",strtotime($value))." 23:59:59'";
						} else {
							$value1b = strtotime($value1." 12:00:00");
							$value2b = strtotime($value1." 12:00:00");
							if($value1b>$value2b) {
								$start = $value2;
								$end = $value1;
							} else {
								$start = $value1;
								$end = $value2;
							}
							$filter_sql[] = "time_start >= '".date("Y-m-d",strtotime($start))." 00:00:00' AND time_end <= '".date("Y-m-d",strtotime($end))." 23:59:59'";
						}
					}
					break;
			}
		}

		$filter_sql = implode(" AND ",$filter_sql);


		$sql = "SELECT sum(T.time_minutes) as time_minutes
				, T.time_tkid as time_tkid
				, YEAR(T.time_start) as year
				, MONTH(T.time_start) as month
				, T.time_reseller as time_reseller
				, T.time_client as time_client
				, T.time_project as time_project
				, T.time_activity as time_activity
				, T.time_location as time_location
				, T.time_billable as is_billable
				, T.time_product as is_development
				, T.time_support as is_support
				FROM ".$this->getTableName()." T
				WHERE T.time_status = ".self::ACTIVE."
				".(strlen($filter_sql)>0?" AND ".$filter_sql:"")."
				AND T.`time_private` = '0'
				GROUP BY
				T.time_tkid
				, YEAR(T.time_start)
				,MONTH(T.time_start)
				,T.time_project
				,T.time_activity
				,T.time_client
				,T.time_reseller
				,T.time_location
				,T.time_billable
				,T.time_product
				,T.time_support
				";
		$rows = $this->mysql_fetch_all($sql); //echo $sql;
		return $rows;
	}









	/*****************************
	 * General functions
	 */



	public function isPublicHoliday($now) {
	/*
	 * Convert incoming date to 12:00 to make comparable with listed dates
	 */
		//Check if it is a text date or a unix date
		if(!is_int($now)) {
			$now = strtotime($now);
		}
		//strip time and add back as 12:00
		$now = date("d M Y",$now)." 12:00:00";
		//convert back to unix
		$now = strtotime($now);

	/*
	 * Get the year of the date being checked
	 */
		$y = date("Y",$now);

	/*
	 * Check fixed public holidays
	 */
		$result = false;
		foreach($this->annual_public_holidays as $public) {
			//Add on the year and set the time to 12:00
			$public.=" ".$y." 12:00:00";
			//convert to unix
			$public = strtotime($public);
			//compare
			if($now == $public) {
				$result = true;
				break;
			}
		}

	/*
	 * If not a fixed public holiday then check the flexible holidays for the incoming date's year
	 */
		if(!$result) {
			foreach($this->flexible_public_holidays[$y] as $public) {
				//Set the time to 12:00 - DON'T ADD THE YEAR AS IT IS SET IN THE FLEXIBLE PUBLIC HOLIDAY VAR
				$public.=" 12:00:00";
				//convert to unix
				$public = strtotime($public);
				//compare
				if($now == $public) {
					$result = true;
					break;
				}
			}
		}

	/*
	 * Return result
	 */
		return $result;
	}




















	/**
	 * get list of users who have captured time for reporting purposes
	 */

	public function getListOfTimeUsersForReport() {
		$sql = "SELECT tkid as id, CONCAT(tkname,' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				WHERE tkid IN (SELECT ".$this->getTableField()."_tkid FROM ".$this->getTableName().")
				ORDER BY tkname, tksurname";
		$rows = $this->mysql_fetch_value_by_id($sql, "id", "name");
		return $rows;
	}








}

?>