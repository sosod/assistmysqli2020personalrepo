<?php
/**
 * To manage the various list items
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class AIT_LIST extends AIT {

    private $my_list = "";
    private $list_table = "";
    private $fields = array();
    private $field_names = array();
    private $field_types = array();
    private $associatedObject;
    private $object_table = "";
    private $object_field = "";
	private $parent_field = "";
	private $sort_by = "sort, name";
	private $sort_by_array = array("sort","name");
	private $my_name = "name";
	private $sql_name = "|X|name";

	private $is_combo_list = false;
	private $combo_lists = array("assist_company","assist_reseller");
	private $partner_table = "";
	private $partner_field = "";
	private $partner_status = "";

    private $lists = array();
    private $default_field_names = array(
        'id'=>"Ref",
        'name'=>"Name",
        'default_name'=>"Default Name",
        'client_name'=>"Your Name",
        'shortcode'=>"Short Code",
        'description'=>"Description",
        'status'=>"Status",
        'colour'=>"Colour",
        'rating'=>"Score",
        'list_num'=>"Supplier Category",
    );
    private $default_field_types = array(
        'id'=>"REF",
        'default_name'=>"LRGVC",
        'client_name'=>"LRGVC",
        'name'=>"LRGVC",
        'shortcode'=>"SMLVC",
        'description'=>"TEXT",
        'status'=>"STATUS",
        'colour'=>"COLOUR",
        'rating'=>"RATING",
        'list_num'=>"LIST",
    );
    private $required_fields = array(
        'id'=>false,
        'name'=>true,
        'default_name'=>false,
        'client_name'=>true,
        'shortcode'=>true,
        'description'=>false,
        'status'=>false,
        'colour'=>true,
        'rating'=>true,
        'list_num'=>true,
    );

	const AUTO_ADD = 256;
	const PRIVATE_ITEM = 512;

/**
 * @param {String} list     The list identifier
 */
    public function __construct($list=""){
        parent::__construct();
		if(strlen($list)>0) {
        	$this->my_list = $list;
        	$this->setListDetails();
		}
    }
	public function changeListType($list="") {
		if(strlen($list)>0) {
        	$this->my_list = $list;
        	$this->setListDetails();
		}
	}

	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['status'])) { $var['status'] = self::ACTIVE; }
		//if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		//if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
		return $this->addListItem($var);
	}
	public function addFromAutoComplete($text,$code="",$is_private=false) {
		$var = array();
		foreach($this->fields as $fld) {
			switch($fld) {
				case "id": break;
				case "name":
					$var[$fld] = $text; //$this->code($text); - REMOVED 2017-11-20 JC - code not required as submitted via AssistForm.Serialize function which includes code
					break;
				case "shortcode":
					$var[$fld] = $code.""=="0" ? "" : $code; //$this->code($code); - REMOVED 2017-11-20 JC - code not required as submitted via AssistForm.Serialize function which includes code
					break;
				case "status":
					$var[$fld] = self::ACTIVE + self::AUTO_ADD + ($is_private ? self::PRIVATE_ITEM : 0);
					break;
				default:
					$var[$fld] = "";
					break;
			}
		}
		$var["sort"] = "99";
		$var['is_private'] = $is_private;
		$var['owner'] = $this->getUserID();
		return $this->addListItem($var,AIT_LOG::CREATE_FROM_AUTOCOMPLETE);
	}
	public function editObject($var){
		$id = $var['id'];
		return $this->editListItem($id,$var);
	}
	public function deleteObject($var){
		$id = $var['id'];
		return $this->deleteListItem($id);
	}
	public function deactivateObject($var){
		$id = $var['id'];
		return $this->deactivateListItem($id);
	}
	public function restoreObject($var){
		$id = $var['id'];
		return $this->restoreListItem($id);
	}

	public function removePrivateMarker($id) {
		$var = $this->mysql_fetch_one("SELECT * FROM ".$this->getListTable()." WHERE id = ".$id);

		$sql = "UPDATE ".$this->getListTable()." SET is_private = 0 WHERE id = ".$id;
		$this->db_update($sql);

		$changes = array(
			'user'=>$this->getUserName(),
			'response'=> "Removed private marker from item " . $id . " (" . $var[$this->getMyName()].") due to use by more than one user.",
		);
		$log_var = array(
			'section'	=> $this->my_list,
			'object_id'	=> $id,
			'changes'	=> $changes,
			'log_type'	=> AIT_LOG::OPEN_PRIVATE,
		);
		$this->addActivityLog("setup", $log_var);

		return true;
	}

    /***************************
     * GET functions
     **************************/
	public function getMyName(){
		return $this->my_name;
	}
	public function getSQLName($t="") {
		return str_ireplace("|X|", (strlen($t)>0?$t.".":""), $this->sql_name);
	}
	public function getListTable(){
		return $this->list_table;
	}
	public function getSortBy($as_array=true) {
		if($as_array) {
			return $this->sort_by_array;
		} else {
			return $this->sort_by;
		}
	}
	public function getAllListTables($lists=array()) {
		$data = array();
		foreach($lists as $l) {
        	$this->my_list = $l;
        	$this->setListDetails();
			$data[$l] = $this->getListTable();
		}
		return $data;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldNames() {
		return $this->field_names;
	}
	/**
	 * Returns the required fields for the given list
	 */
	public function getRequredFields() {
		return $this->required_fields;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldTypes() {
		return $this->field_types;
	}
    /**
     * Returns list items in an array with the id as key depending on the input options
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getListItems($options="") {
        $sql = "SELECT * ";
		if(in_array("client_name",$this->fields)) {
			$sql.=", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
		}
        $sql.= " FROM ".$this->getListTable()." WHERE (status & ".AIT::DELETED.") <> ".AIT::DELETED.(strlen($options)>0 ? " AND ".$options : "")." ORDER BY ".$this->sort_by;
        //echo $sql;
        //return $this->mysql_fetch_all_by_id($sql, "id");
        $data = $this->mysql_fetch_all_by_id($sql, "id");
		//ASSIST_HELPER::arrPrint($data);
		return $data;
    }
    /**
     * Returns all list items which have been used formatted for reporting purposes
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getItemsForReport($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".AIT::DELETED." <> ".AIT::DELETED." ) AND (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table."))";
        $items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
    }

    public function getItemsInUse($parent_id=0) {
        $options="(status & ".AIT::DELETED." <> ".AIT::DELETED." )
        AND (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table.($parent_id > 0 ? " WHERE ".$this->parent_field." = ".$parent_id : "")."))";
		//echo $options;
        $items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
    }
    /**
     * Returns all active list items in an array with the id as key
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItems($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".AIT::ACTIVE." = ".AIT::ACTIVE." )";
        return $this->getListItems($options);
    }
    public function getActiveComboListItems($options="",$include_external_src=true) {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".AIT::ACTIVE." = ".AIT::ACTIVE." )";
        $data = $this->getListItems($options);

        $codes = array("IASSIST","BLANK");
        foreach($data as $d) {
        	if(strlen($d['shortcode'])>0 && !in_array($d['shortcode'],$codes)) {
        		$codes[] = $d['shortcode'];
        	}
        }
		//Don't include the external source of list items if you are only getting private or are checking for new items to be added
		if($include_external_src) {
	        $mdb = new ASSIST_DB("master");
	        $sql = "SELECT cmpcode as id, CONCAT(cmpname,' (',cmpcode,')') as name FROM assist_company WHERE ".$this->partner_field." NOT IN ('".implode("','",$codes)."') AND ".$this->partner_status;
			if($this->my_list=="assist_reseller") {
				$sql.=" AND ".$this->partner_field." IN (SELECT cmpcode FROM ".$this->my_list." WHERE active = 1)";
			}
			$extra = $mdb->mysql_fetch_all($sql);
			foreach($extra as $ex) {
				$data[$ex['id']] = $ex;
			}
		}
        return $data;
    }
    /**
     * Returns all active list items formatted for display in SELECT => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItemsFormattedForSelect($options="",$include_external_src=true) {
    	if($this->is_combo_list) {
    		$rows = $this->getActiveComboListItems($options,$include_external_src);
    	} else {
	        $rows = $this->getActiveListItems($options);
    	}
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num",$this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
    }

    /**
     * Returns all active list items formatted for display in Reports => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItemsFormattedForReport($options="") {
    	//if($this->is_combo_list) {
    	//	$rows = $this->getActiveComboListItems($options,false);
    	//} else {
	        $rows = $this->getItemsForReport($options);
    	//}
		/*$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num",$this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
    */
    	return $rows;
	}



	public function getActiveListItemsNotInUseFormattedForSelect($parent_id,$is_edit_page=false,$edit_id="") {
		$items_in_use = $this->getItemsInUse($parent_id);  //echo $parent_id; $this->arrPrint($items_in_use);
		$active_items = $this->getActiveListItemsFormattedForSelect(); //$this->arrPrint($active_items);
		$data = $active_items['options'];  //$this->arrPrint($data);
		//array_diff($data, $items_in_use);
		foreach($items_in_use as $i => $v) {
			if(isset($data[$i]) && (!$is_edit_page || $i!=$edit_id)) {
				unset($data[$i]);
			}
		}
		if($is_edit_page && strlen($edit_id)>0 && !isset($data[$edit_id])) {
			$data[$edit_id] = $items_in_use[$edit_id];
		}
		//$this->arrPrint($data);
		return $data;
		//return $active_items;
		//return $items_in_use;
	}


    /**
     * Returns all list items not deleted
     * @return (Array of Records)
     */
    public function getAllListItems() {
        return $this->getListItems();
    }

    /**
     * Returns all list items formatted for display in SELECT => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getAllListItemsFormattedForSelect($options="") {
    	/* COMBO LIST OPTION NOT REQUIRED FOR THIS FUNCTION
		 * 		All List Items only needed for display & therefore link to items already added to the local list table
		 * 		where combo list is only used for drop down lists on New form to allow for adding items not already in local list table
    	*/
    	//if($this->is_combo_list) {
    		//$rows = $this->getActiveComboListItems($options);
    	//} else {
	        $rows = $this->getAllListItems($options);
    	//}
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num",$this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
    }

    /**
     * Returns a specific list item determined by the input id
     * @param (INT) id = The ID of the list item
     * @return (One Record)
	 * OR
	 * @param (Array) id = array of IDs to be found
	 * @return (Array of Arrays) records found
     */
    public function getAListItem($id) {
    	if(is_array($id)) {
        	$data = $this->getListItems("id IN (".implode(",",$id).")");
			return $data;
    	} else {
        	$data = $this->getListItems("id = ".$id);
	        return $data[$id];
        }
    }
	/**
	 * returns the names of specific list items determined by the input id(s)
	 * @param (int) id = id of the list item to be found
	 * @return (String) name of list item
	 * OR
	 * @param (array) id = array of ids to be found
	 * @return (Array of Strings) names of the list items with the ID as index
	 */
	public function getAListItemName($id) {
    	$data = $this->getAListItem($id);
    	if(is_array($id)) {
    		$items = array();
			foreach($data as $i=>$d) {
				$items[$i] = $d['name'];
			}
			return $items;
    	} else {
	        return $data['name'];
        }
	}

    /**
     * Returns a list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @param (INT) id = The ID of the list item
     * @return (One record)
     */
    public function getAListItemForSetup($id) {
        $data = $this->getAListItem($id);
        /*****
         * TO BE PROGRAMMED:
         *     $sql = SELECT count(O.$this->object_field) as in_use
         *      FROM $this->object_table O
         *      ON L.id = O.$this->object_field
         *      WHERE O.$this->object_field = $id
         *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
         * $row = $this->mysql_fetch_one_value($sql,"in_use")
         * parse results: if in_use > 0 then $data['can_delete'] = false else = true
         *
         */
         return $data;
    }

    /**
     * Returns  list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @return (records)
     */
	public function getListItemsForSetup() {
		$data = $this->getListItems();
		/*****
		 * TO BE PROGRAMMED:
		 *     $sql = SELECT count(O.$this->object_field) as in_use
		 *      FROM $this->object_table O
		 *      ON L.id = O.$this->object_field
		 *      WHERE O.$this->object_field = $id
		 *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
		 * $row = $this->mysql_fetch_one_value($sql,"in_use")
		 * parse results: if in_use > 0 then $data['can_delete'] = false else = true
		 *
		 */
		if(count($data)>0) {
			$keys = array_keys($data);
			if(is_array($this->object_field)) {
				$u = array();
				$used = array();
				foreach($this->object_field as $k => $of) {
					$ot = $this->object_table[$k];
					$sql = "SELECT ".$of." as fld, count(".$of.") as in_use FROM ".$ot." WHERE ".$of." IN (".implode(",",$keys).") GROUP BY ".$of;
					$u[] = $this->mysql_fetch_all_by_id($sql, "fld");
					foreach($u as $f => $s) {
						$s['in_use'] = isset($s['in_use']) ? $s['in_use'] : 0;
						if(isset($used[$f])) {
							$used[$f]['in_use']+= $s['in_use'];
						} else {
							$used[$f] = $s;
						}
					}
				}
			} else {
				$sql = "SELECT ".$this->object_field." as fld, count(".$this->object_field.") as in_use FROM ".$this->object_table." WHERE ".$this->object_field." IN (".implode(",",$keys).") GROUP BY ".$this->object_field;
				$used = $this->mysql_fetch_all_by_id($sql, "fld");
			}
			foreach($data as $key=>$item) {
				if(!isset($used[$key]) || $used[$key]['in_use']==0) {
					$data[$key]['can_delete'] = true;
				} else {
					$data[$key]['can_delete'] = false;
				}
			}
		}
		return $data;
	}




    /****************************
     * SET/UPDATE functions
     *****************************/
    /**
     * Create a new list item
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function addListItem($var,$log_type="") {
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getListTable()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0){
			$result = array("ok","List item added successfully.",'id'=>$id);
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Added a new item " . $id . ": " . $var[$this->getMyName()],
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> (strlen($log_type)==0 ? AIT_LOG::CREATE : $log_type),
			);
			$this->addActivityLog("setup", $log_var);
		} else {
			$result = array("error","Sorry, something went wrong while trying to add the list item. Please try again.",'id'=>0);
		}
		return $result;
    }
    /**
     * Edit a list item details
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function editListItem($id,$var) {
        $old_sql = "SELECT * FROM " . $this->getListTable() . " WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "UPDATE ".$this->getListTable()." SET ".$insert_data . " WHERE id = $id";
		$mar = $this->db_update($sql);
		if($mar>0){
			$result = array("ok","List item modified successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " modified.",
			);
			foreach($old_result as $key=>$value){
				if(isset($var[$key]) && $value != $var[$key]){
					if($key=="list_num") {
						$listObject2 = new AIT_LIST("contract_supplier_category");
						$item_names = $listObject2->getAListItemName(array($var[$key],$value));
						$item_names[0] = $this->getUnspecified();
						$changes[$key] =array('to'=>$item_names[$var[$key]], 'from'=>$item_names[$value]);
					} else {
						$changes[$key] =array('to'=>$var[$key], 'from'=>$value);
					}
				}
			}
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> AIT_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error","Sorry, something went wrong while trying to change the list item. Please try again.");
		}
		return $result;
    }
    /**
     * Delete a list item by removing the ACTIVE status and adding a DELETED status - the item should no longer show anywhere within the module
     * @param (INT) id = id of list item to be deleted
     * @return (BOOL) status of success as true/false
     */
    public function deleteListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + DELETED) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".AIT::ACTIVE." + ".AIT::DELETED.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar>0){
			$result = array("ok","List item removed successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deleted.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> AIT_LOG::DELETE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error","Sorry, something went wrong while trying to remove the list item. Please try again.");
		}

		return $result;
    }

    /**
     * Deactivate a list item by removing the ACTIVE status and adding INACTIVE status - the item should no longer be availavble in new or edit
     * @param (INT) id = id of list item to be deactivated
     * @return (BOOL) status of success as true/false
     */
    public function deactivateListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + INACTIVE) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".AIT::ACTIVE." + ".AIT::INACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar>0){
			$result = array("ok","List item deactivated successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deactivated.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> AIT_LOG::DEACTIVATE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error","Sorry, something went wrong while trying to deactivate the list item. Please try again.");
		}

		return $result;
    }
    /**
     * Restore a deactivated list item by removing the INACTIVE status and adding ACTIVE - the list item should be available for use throughout the module
     * @param (INT) id = id of list item to be restored
     * @return (BOOL) status of success as true/false
     */
    public function restoreListItem($id) {

        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".AIT::INACTIVE." + ".AIT::ACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);

		if($mar>0){
			$result = array("ok","List item restored successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " restored.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> AIT_LOG::RESTORE,
			);
			$this->addActivityLog("setup", $log_var);

		} else {
			$result = array("error","Sorry, something went wrong while trying to restore the list item. Please try again.");
		}

		return $result;

    }















    /**
     * Sets the various variables needed by the class depending on the list name provided
     */
    private function setListDetails() {
		$this->sort_by = "sort, name";
		$this->sort_by_array = array("sort","name");
		$this->my_name = "name";
		$this->sql_name = "|X|name";
        switch($this->my_list) {
			case "assist_company":
			case "assist_reseller":
				$this->is_combo_list = true;
				$this->partner_table = "assist_company";
				$this->partner_field = "cmpcode";
				$this->partner_status = "cmpstatus = 'Y' AND cmp_is_demo = 0";
                $this->list_table = $this->getDBRef()."_list_".$this->my_list;
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new AIT_TIME();
                $this->object_table = $this->associatedObject->getTableName();
				$x = explode("_",$this->my_list);
                $this->object_field = $this->associatedObject->getTableField()."_".($x[1]=="company"?"client":$x[1]);
				break;
            case "list_activity":             case "activity":
			case "list_project":			case "project":
			case "list_location":			case "location":
			default:
                $this->list_table = $this->getDBRef()."_".$this->my_list;
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new AIT_TIME();
                $this->object_table = $this->associatedObject->getTableName();
				$x = explode("_",$this->my_list);
                $this->object_field = $this->associatedObject->getTableField()."_".$x[1];
                //$this->object_field = $this->associatedObject->getTableField()."_".$this->my_list;
                break;

        }
		if($this->my_name!="name") { $this->sql_name = " if(length(|X|client_name) > 0, |X|client_name, |X|default_name) "; } else {}
		foreach($this->fields as $key){
			if(!isset($this->field_names[$key])) {
				$this->field_names[$key] = $this->default_field_names[$key];
			}
			if(!isset($this->field_types[$key])) {
				$this->field_types[$key] = $this->default_field_types[$key];
			}
		}
    }

}

?>