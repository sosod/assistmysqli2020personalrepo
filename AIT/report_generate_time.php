<?php
$page_title = "Time";
include("inc_header.php");
$timeObj = new AIT_TIME();
$options = array();

$js = "";
//get all users
$options['time_tkid'] = $timeObj->getListOfTimeUsersForReport();
//get all resellers
//get all client
//get all projects
//get all activities
$headings = $headingObject->getMainObjectHeadings("TIME");
//ASSIST_HELPER::arrPrint($headings);

foreach($headings['rows'] as $fld => $h) {
	if($h['type']=="AUTO_LIST" || $h['type']=="JQUERY_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	} elseif($h['type']=="COMBO_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	}
}
foreach($options as $fld => $opt) {
	asort($options[$fld]);
}




	?>
<form name=frm_report >
	<h2>1. Filters</h2>
	<table id=tbl_filter class=form>
	<!--	<tr>
			<th>Report:</th>
			<td><select><option>Billable per Client</option></select></td>
	</tr> -->
		<tr>
			<th>Timekeepers:</th>
			<td><select name=sel_users[] multiple=multiple size=<?php $fld = "time_tkid"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th><?php $fld = "time_reseller"; echo $headings['rows'][$fld]['name']; ?>:</th>
			<td><select name=sel_reseller[] multiple=multiple size=<?php echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th><?php $fld = "time_client"; echo $headings['rows'][$fld]['name']; ?>:</th>
			<td><select name=sel_client[] multiple=multiple size=<?php  echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th><?php $fld = "time_project"; echo $headings['rows'][$fld]['name']; ?>:</th>
			<td><select name=sel_project[] multiple=multiple size=<?php  echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th><?php $fld = "time_activity"; echo $headings['rows'][$fld]['name']; ?>:</th>
			<td><select name=sel_activity[] multiple=multiple size=<?php echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th><?php $fld = "time_location"; echo $headings['rows'][$fld]['name']; ?>:</th>
			<td><select name=sel_location[] multiple=multiple size=<?php  echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
	<!--	<tr>
			<th>Type:</th>
			<td class=b><table id=tbl_type>
				<tr><td><input type=checkbox /></td><td>Billable:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_billable",'small_size'=>true),0);  ?></td></tr>
				<tr><td><input type=checkbox /></td><td>Development:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_development",'small_size'=>true),0);  ?></td></tr>
				<tr><td><input type=checkbox /></td><td>Support:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_support",'small_size'=>true),0);  ?></td></tr>
			</table></td>
	</tr> -->
		<tr>
			<th>Time:</th>
			<td>
				<input type=radio class=time-type name=time_type value=all checked for=lbl_all_time id=rad_all_time /><label id=lbl_all_time for=rad_all_time>All Time</label><br />
				<input type=radio class=time-type name=time_type value=limit for=lbl_limit_time id=rad_limit_time /><label id=lbl_limit_time for=rad_limit_time>From <?php $js.= $displayObject->drawFormField("DATE", array('id'=>"time_from",'name'=>"time[]"),"");  ?> to <?php $js.= $displayObject->drawFormField("DATE", array('id'=>"time_to",'name'=>"time[]"),"");  ?></label>
				<input type=hidden name=time_type_selected id=txt_time_type value='all' />
			</td>
		</tr>
	</table>
	<h2>2. Report Settings</h2>
	<table id=tbl_settings class=form>
		<tr>
			<th>Group By:</th>
			<?php
			$group_by = array(
				'time_reseller'=>"",
				'time_client'=>"",
				'time_project'=>"",
				'time_activity'=>"",
				'time_location'=>"",
				'time_tkid'=>"Timekeepers",
				'is_billable'=>"Billable",
				'is_development'=>"Development",
				'is_support'=>"Support",
			);
			foreach($group_by as $fld => $tv) {
				if(isset($headings['rows'][$fld]['name'])) {
					$group_by[$fld] = $headings['rows'][$fld]['name'];
				}
			}
			?>
			<td><?php
			$display = array();
			$report_group_limit = 4;
			$limit = count($group_by)>$report_group_limit?$report_group_limit:count($group_by);
			for($i=1;$i<=$limit;$i++) {
				$d = "<select class=sel-group name=group_by[]>".($i>1?"<option value=X selected>Not applicable</option>":"");
					foreach($group_by as $fld => $gp) {
						$d.="<option value=".$fld." ".($i==1&&$fld=="time_tkid"?"selected=selected":"").">".$gp."</option>";
					}
				$d.= "</select>";
				$display[] = $d;
			}
			echo implode("<br />",$display);

			?></td>
		</tr>
		<tr>
			<th>Report Heading:</th>
			<td><input type=text name=report_name1 value='<?php echo $timeObj->getCmpName(); ?>' size=50  /><br />
			<input type=text name=report_name2 value='Time Report' size=50 style='margin-top: 5px' /></td>
		</tr>
		<tr>
			<th></th>
			<td><button class=btn-generate>Generate</button></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function() {
		<?php echo $js; ?>
		$(".btn-generate").button({
			icons:{primary:"ui-icon-script"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var err = false;
				$("#time_from").removeClass("required");
				$("#time_to").removeClass("required");
			if($("#txt_time_type").val()=="limit" && ($("#time_from").val().length==0 && $("#time_to").val().length==0)) {
				var err = true;
				$("#time_from").addClass("required");
				$("#time_to").addClass("required");
				AssistHelper.finishedProcessing("error","Please enter a time or choose All Time.");
			}
			if(!err) {
				var dta = AssistForm.serialize($("form[name=frm_report]"));
				//console.log(dta);
				document.location.href = "report_generate_time_generate.php?"+dta;
			}
		});
		$(".time-type").click(function() {
			var v = $(this).val();
			$("#txt_time_type").val(v);
		})
		$("#tbl_type").css("border","1px solid #ffffff").find("td").css("border","1px solid #ffffff");
		$(".sel-group").css("margin","5px 0px 5px 0px");
	});
</script>