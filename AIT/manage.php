<?php
include("inc_header.php");



?>
<div id=div_wrapper style='margin:0 auto; padding: 25px; width: 250px'>
	<div class='div_icons click_me' page=time >
		<p class=b><img src=common/time.png?<?php echo time(); ?>  title="Time" /><br /><?php echo $helper->getObjectName("TIME")?><br />&nbsp;</p>
	</div>
</div>
<hr />
<h3 class=center>Still to Come</h3>
<div id=div_wrapper2 style='margin: 0 auto; padding: 25px; width:95%; text-align: center'>
	<div class=div_icons>
		<p><img src=common/travel_grey.png?<?php echo time(); ?>  page=travel title="Travel" /><br />Travel</p>
	</div class=div_icons>
	<div class=div_icons>
		<p><img src=common/expense_grey.png?<?php echo time(); ?>  page=costs title="Costs" /><br />Costs</p>
	</div>
	<div class=div_icons>
		<p><img src=common/leave_grey.png?<?php echo time(); ?>  page=leave title="Leave" /><br />Leave</p>
	</div>
	<div class=div_icons>
		<p><img src=common/office_grey.png?<?php echo time(); ?> page=office title="Office Bookings" /><br />Office Bookings</p>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$("div.div_icons").css({"border":"1px solid #dedede","width":"250px","height":"250px","display":"inline-block","text-align":"center"})
	.hover(function() {
		$(this).css("background","#e6e6ff");
	},function() {
		$(this).css("background","#ffffff");
	});
	$(".click_me").css("cursor","pointer").click(function() {
		var p = $(this).attr("page");
		document.location.href = "manage_"+p+".php";
	});
	$("#tbl_icons").prop("width","100%").find("td").prop("width","25%");
});
</script>