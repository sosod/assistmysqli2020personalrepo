<?php
$page_title = "Time";
include("inc_header.php");

//ASSIST_HELPER::arrPrint($_REQUEST);

if(!isset($_REQUEST['action'])) {
	$_REQUEST['action']="ADD";
}


$page_action = $_REQUEST['action'];
$is_edit_page = $page_action=="EDIT";
$is_add_page = $page_action=="ADD";


?>
<style type="text/css" >
	table th {
		background-color: #FFFFFF;
		border: 1px solid #dedede;
		color: #000099;
	}
	span.th {
		color: #000099;
		font-weight:bold;
	}
	span.help {
		font-style: italic;
		font-size: 80%;
		color: #777777;
	}
	table, table td, table.form td {
		border: 1px solid #dedede;
	}
	table td.hide-me {
		border: 1px solid #ffffff;
	}
</style>
<?php

$timeObj = new AIT_TIME();


$m = explode("-",$_REQUEST['m']);
$current = mktime(12,0,0,$m[1],$m[2],$m[0]);

$number_of_rows = (isset($_REQUEST['i']) && ASSIST_HELPER::checkIntRef($_REQUEST['i'])) ? $_REQUEST['i'] : 1;

$go_back_src = base64_decode($_REQUEST['src']);

$headings = $headingObject->getMainObjectHeadings("TIME");
ASSIST_HELPER::displayResult(array("info","All fields except Description are required. Time is to be entered as 24 hours time in the format HH:mm."));


$options = array();
foreach($headings['rows'] as $fld => $h) {
	if($h['type']=="AUTO_LIST" || $h['type']=="JQUERY_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = $listObject->getActiveListItemsFormattedForSelect(" (is_private = 0 OR owner='".$timeObj->getUserID()."') ");
		unset($listObject);
	} elseif($h['type']=="COMBO_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = $listObject->getActiveListItemsFormattedForSelect(" (is_private = 0 OR owner='".$timeObj->getUserID()."') ");
		unset($listObject);
	}
}
//ASSIST_HELPER::arrPrint($headings);


$last_time = "";
if($is_edit_page) {
	$time_id = $_REQUEST['time_id'];
	$edit_record = $timeObj->getRawObject($time_id);
}
if($is_add_page) {
	//get last end time for current day to pre-populate first end time slot
	$last_time = $timeObj->getLastEndTimeForToday($current);
}




$form_fields_for_display = array();
$js_variables_set = array();


//prep the input fields
for($i=1;$i<=$number_of_rows;$i++) {
	$form_fields_for_display[$i] = array();
	foreach($headings['rows'] as $fld => $h) {
			$echo = "";
			$class = "";
			$h_type = $h['type'];
			switch($h_type) {
				case "REF":
					$echo = false;
					break;
				case "DATE":
					$echo = false;
					break;
				case "TIME":
					if($is_add_page && $fld=="time_start") {
						$display_time = $last_time;
					} else {
						$display_time = "";
					}
					$echo = "<input type=text size=4 value='".($is_edit_page?date("H:i",strtotime($edit_record[$fld])):$display_time)."' class=time id=".$fld."_".$i." name=".$fld."[$i] record=r".$i." /></span>";
					break;
				case "JQUERY_LIST":
					$echo = "<select id=".$fld."_".$i." name=".$fld."_display[$i] record=r".$i.">";
					if(isset($options[$fld])) {
						foreach($options[$fld] as $value => $label) {
							$echo.="<option value=".$value.">".$label."</option>";
						}
					}
					$echo.="</select>";
					$js.="
					//$('#".$fld."_".$i."').selectmenu();
					";
					break;
				case "AUTO_LIST":
				case "COMBO_LIST":
					$echo = "<input size=50 type=text id=".$fld."_".$i." name=".$fld."_display[$i] record=r".$i."  class=auto_list js_field='$fld' />";
					$echo.= "<input type=hidden id=".$fld."_".$i."_raw name=".$fld."[$i] value='".($is_edit_page ? $edit_record[$fld] : "")."' />";
					if(isset($options[$fld]) && !isset($js_variables_set[$fld])) {
						$js.="
						var $fld = [";
						$list = array();
						foreach($options[$fld] as $value => $label) {
							$list[] = "
							{value:'".$value."',label:'".(strpos($label, "'") !== false || strpos($label, '"') !== false ? ASSIST_HELPER::code($label) : $label)."'}
							";
						}
						$js.=implode(",",$list)."];
						for(i in $fld ) {
							var x = ".$fld."[i];
							".$fld."[i].value = AssistString.decode(x.value);
							".$fld."[i].label = AssistString.decode(x.label);
						}
						";
						//unset($options[$fld]);
						$js_variables_set[$fld] = true;
					} else {
					}
					$js.="
					$('#".$fld."_".$i."').autocomplete({
						minLength: 2,
						source: ".$fld.",
						select: function(event,ui) {
							$('#".$fld."_".$i."').val(AssistString.decode(ui.item.label));
							$('#".$fld."_".$i."_raw').val(ui.item.value);
							return false;
						},
						response: function(event,ui) {
							$('#".$fld."_".$i."_raw').val('');
						},
						change: function(event,ui) {
							$('#".$fld."_".$i."').removeClass('ui-state-ok');
							var list_value = $('#".$fld."_".$i."_raw').val();
							var input_text = $('#".$fld."_".$i."').val().toLowerCase();
							input_text = input_text.trim();
							//console.log(list_value+'='+input_text);
							//if no list value is present,
								//check the array for a matching value
								//otherwise indicate a new list item by setting value to 0
							//else confirm that list value matches the input text (in case user just typed and didn't select)
							if(list_value.length==0) {
								list_value = getAutocompleteValue(input_text,".$fld.");
								$('#".$fld."_".$i."_raw').val(list_value);
							} else {
								var test_object = {};
								var test_value = 0;
								var test_label = '';
								var error = false;
								for(i in ".$fld.") {
									test_object = $fld"."[i];
									test_value = test_object.value;
									if(parseInt(test_value)==parseInt(list_value)) {
										test_label = test_object.label.toLowerCase();
										test_label = test_label.trim();
										if(test_label!=input_text) {
											error = true;
										}
										break;
									}
								}
								if(error) {
									list_value = getAutocompleteValue(input_text,".$fld.");
									$('#".$fld."_".$i."_raw').val(list_value);
								}
							}
							if(list_value==0) {
								$('#".$fld."_".$i."').addClass('ui-state-ok');
							}
						}
					});
					";
					if($is_edit_page) {
						$js.="
						$('#".$fld."_".$i."').val(AssistString.decode('".($options[$fld][$edit_record[$fld]])."'));
						";
					}
					break;
				case "CALC":
					$display = $displayObject->createFormField("LABEL",array('id'=>$fld."_$i",'name'=>$fld."[$i]"),($is_edit_page ? $edit_record[$fld] : "TBC"));
					$echo = $display['display'];
					$js.= $display['js'];
					break;
				case "BOOL":
					$display = $displayObject->createFormField($h_type,array('id'=>$fld."_$i",'name'=>$fld."[$i]",'form'=>"vertical"),($is_edit_page ? $edit_record[$fld] : 0));
					$echo = $display['display'];
					$js.= $display['js'];
					break;
				default:
					$display = $displayObject->createFormField($h_type,array('id'=>$fld."_$i",'name'=>$fld."[$i]",'record'=>"r".$i),($is_edit_page ? $edit_record[$fld] : ""));
					$echo = $display['display'];
					$js.= $display['js'];
					break;
			}
			if($echo!==false) {
				$form_fields_for_display[$i][$fld] = $echo;
			}
	}
}





?>
	<h2><?php echo ($is_add_page ? "Add Record(s)" : "Edit Record ".$timeObj->getRefTag().$time_id); ?> for <?php echo date("d F Y",$current)." (".date("l",$current).")"; ?></h2>
	<form name=frm_time>
		<input type=hidden name=action value='TIME.<?php echo $page_action; ?>' />
		<input type=hidden name=records value='<?php echo $number_of_rows; ?>' />
		<input type=hidden name=time_date value='<?php echo $current; ?>' />
		<?php if($is_edit_page) { ?>
		<input type=hidden name=record[1] value='Y' />
		<input type=hidden name=time_id value='<?php echo $time_id; ?>' />
		<?php } ?>
	<table id=tbl_time class='hide-me'>
		<tr>
			<td class='hide-me'><h3>Timing</h3></td>
			<td class='hide-me'><h3>Details</h3></td>
			<td class='hide-me' colspan=2><h3>Additional Notes</h3></td>
		</tr>
		<?php
		for($i=1;$i<=($number_of_rows);$i++) {
			if($is_add_page) {
		?>
		<tr>
			<th class=th2 colspan=4><span style='float:left; font-weight:normal' class=i><input type=checkbox value=Y name=record[<?php echo $i; ?>] id=<?php echo "record_r".$i; ?> record=<?php echo "r".$i; ?> i=<?php echo $i; ?> /> Save record</span>Record <?php echo $i; ?></th>
		</tr>
		<?php
			}
		?>
		<tr id=tr_r<?php echo $i; ?> class=tr_records>
			<td class='hide-me'>
				<table id=tbl_duration class=form>
					<tr>
						<th><?php $fld = "time_start"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_end"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_minutes"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
				</table>
			</td>
			<td class='hide-me'>
				<table id=tbl_details class=form>
					<tr>
						<th><?php $fld = "time_reseller"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_client"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_project"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_activity"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<th><?php $fld = "time_location"; echo $headings['rows'][$fld]['name']; ?>:</th>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
				</table>
			</td>
			<td class='hide-me'>
				<table id=tbl_notes1>
					<tr>
						<th><?php $fld = "time_description"; echo $headings['rows'][$fld]['name']; ?></th>
					</tr>
					<tr>
						<td><?php echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
				</table>
			</td>
			<td class='hide-me'>
				<table id=tbl_notes1>
					<tr>
						<th width=33%><?php $fld = "time_billable"; echo $headings['rows'][$fld]['name']; ?></th>
						<th width=34%><?php $fld = "time_product"; echo $headings['rows'][$fld]['name']; ?></th>
						<th width=34%><?php $fld = "time_support"; echo $headings['rows'][$fld]['name']; ?></th>
						<th width=33%><?php $fld = "time_private"; echo $headings['rows'][$fld]['name']; ?></th>
					</tr>
					<tr>
						<td><?php $fld = "time_billable"; echo $form_fields_for_display[$i][$fld]; ?></td>
						<td><?php $fld = "time_product"; echo $form_fields_for_display[$i][$fld]; ?></td>
						<td><?php $fld = "time_support"; echo $form_fields_for_display[$i][$fld]; ?></td>
						<td><?php $fld = "time_private"; echo $form_fields_for_display[$i][$fld]; ?></td>
					</tr>
					<tr>
						<td colspan=4 class=center>
							<span class='th'><?php $fld = "time_project_ref"; echo $headings['rows'][$fld]['name']; ?>:&nbsp;<?php echo $form_fields_for_display[$i][$fld]; ?></span>
							<br /><span class=help>(<?php echo $headings['rows'][$fld]['help']; ?>)</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php
		} //end loop through number of rows
		?>
		<tr>
			<td id=td_save class=hide-me><button id=btn_save>Save</button></td>
		</tr>
	</table>
</form>
<span ><button id=btn_back>Go Back</button></span>
<p>&nbsp;</p>
<div id=div_auto_list title="" style='border:1px solid #009900; background: #efffef; width:300px; position: fixed; top: 5px; right: 5px; z-index:9999999; '>
	<p class='b green' id=p_auto_heading>List options:</p>
	<ul id=ul_auto></ul>
</div>

<script type=text/javascript>

$(function() {
	var scrSize = AssistHelper.getWindowSize();
	//$("#div_auto_list").css("height",scrSize.height-10);
	<?php echo $js; ?>
//	$(".auto_list").blur(function() {
//		if($("#div_auto_list").is(":focus")) {
			//do nothing if you have moved away from the list box to the div
//		} else {
//			$("#div_auto_list").hide();
//		}
//	});
var $container = $("#div_auto_list");
var div_height = parseInt(AssistString.substr($("#div_auto_list").css("height"),0,-2));

 $(document).mouseup(function (e) {
//console.log(e);
//console.log(e.target);
        if (!$(e.target).hasClass("auto_list")	//if the target of the click isn't an autocomplete form
        	&& !$container.is(e.target) // if the target of the click isn't the container...
            && ($container.has(e.target).length === 0) // ... nor a descendant of the container
            && (e.target != $('html').get(0)) // nor the scrollbar
            )
        {
            toolbarClose();
        }
    });
 $(document).keyup(function (e) {
        if ((e.keyCode == 9)	//tab pressed
        	&& !$(e.target).hasClass("auto_list")	//if the target of the click isn't an autocomplete form
        	&& !$container.is(e.target) // if the target of the click isn't the container...
            && ($container.has(e.target).length === 0) // ... nor a descendant of the container
            )
        {
            toolbarClose();
        }
    });

 function toolbarClose() {
    $container.hide("slide", { direction: "right" }, 500);
 }
	$(".auto_list").focus(function() {
		var jf = $(this).attr("js_field");
		var heading = $(this).parent().parent().find("th:first").html();
		$("#p_auto_heading").html(heading);
		switch(jf) {
			case "time_reseller":
				var source = time_reseller;
				break;
			case "time_client":
				var source = time_client;
				break;
			case "time_project":
				var source = time_project;
				break;
			case "time_activity":
				var source = time_activity;
				break;
			case "time_location":
				var source = time_location;
				break;
			default:
				var source = false;
				break;
		}
		if(source !== false && $.isArray(source)) {
			$("#ul_auto").empty();
			for(i in source) {
				$("#ul_auto").append("<li>"+source[i].label+"</li>");
			}
			$("#div_auto_list").show("slide", { direction: "right" }, 500);
		}
	});
	$container.css("overflow","scroll");
	$container.css("height",((scrSize.height)-10)+"px");
	$container.hide();
	$("#td_save").addClass("right").prop("colspan",4);
	$("textarea").prop("rows","8");
	<?php
	if($is_add_page) {
	?>
	$("input:checkbox").change(function() {
		var r = $(this).attr("record");
		var i = parseInt($(this).attr("i"));
		if($(this).is(":checked")) {
			$("#tr_"+r).show();
			$("#tr_"+r+" input:text").addClass("is_required");
		} else {
			$("#tr_"+r).hide();
			$("#tr_"+r+" input:text").removeClass("is_required");
		}
		if(i>1) {
			var previous_end_time = $("#time_end_"+(i-1)).val();
			$("#time_start_"+i).val(previous_end_time);
		}
	});

	$("tr.tr_records").hide();
	$("#record_r1").prop("checked","checked").trigger("change");

	<?php
	} //end if is_add_page
	?>

/*
	// Simulate tab key when enter or space is pressed
	$('button.btn_yes, button.btn_no').keyup(function(e){
	    if(e.which === 13 || e.which==32){
			console.log(e.keyCode);
			console.log($(this).prop("id"));
			//console.log($(this).next().prop("id"));
            //$(this).next().css("bacjground-color","#cc0001");
	        //return false;
	        selectNextTabbableOrFocusable(":focusable");
	    }
	});

	/*$("button.btn_yes, button.btn_no").keyup(function(e) {
		if(e.keyCode==32) {
			console.log(e.keyCode);
			console.log($(this).prop("id"));
			//$(this).focus();
		}
	});*/


	$("button#btn_back").button({
		icons:{primary:"ui-icon-arrowthick-1-w"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var url = "<?php echo $go_back_src; ?>";
		document.location.href = url;
	}).css("background","url()");


	$("button#btn_save").button({
		icons:{primary:"ui-icon-disk"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("input:text").removeClass("required");
		var err = false;
		var valid_time_inputs = ["0","1","2","3","4","5","6","7","8","9", ":"];
		$("#tbl_time tr:gt(0)").each(function() {
			$(this).find("input:text.is_required").each(function() {
				var i = $(this).prop("id");
				var v = $(this).val();
				var l = v.length;
				if(l==0 && AssistString.substr(i,0,16)!="time_project_ref") {
					err = true;
					$(this).addClass("required");
					console.log(i+" = not filled in");
				} else {
					if($(this).hasClass("time")) {
						if(l!=5) {
							err = true;
							//console.log(i+" = time incorrect length");
							$(this).addClass("required");
						} else {
							var t = l;
							while(t--) {
								if($.inArray(v.charAt(t)+"",valid_time_inputs)<0) {
									err = true;
									$(this).addClass("required");
									//console.log(i+" = incorrect character "+v.charAt(t));
								}
							}
						}
					}
				}
			});
		});
		if(err) {
			AssistHelper.finishedProcessing("error","Your form can't be submitted due to an error.  Remember that all fields, except Description and Ref, are required and Time must be in 24 hours formatted as HH:mm.");
		} else {
			//AssistHelper.closeProcessing();
			//$("form[name=frm_time]").submit();
			var dta = AssistForm.serialize($("form[name=frm_time]"));
			var result = AssistHelper.doAjax("inc_controller.php",dta);
			if(result[0]=="ok"){
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"<?php echo $go_back_src; ?>");
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}


	}).removeClass("ui-state-default").addClass("ui-state-ok");
	$("button.btn_yes, button.btn_no").css("font-size","90%");

	function getAutocompleteValue(input_text,source) {
		input_text = input_text.trim().toLowerCase();
		var test_object = {};
		var test_value = 0;
		var test_label = '';
		var found = false;
		var list_value = 0;
		for(i in source) {
			test_object = source[i];
			test_value = test_object.value;
			test_label = test_object.label.toLowerCase();
			test_label = test_label.trim();
			if(test_label==input_text) {
				list_value = test_value;
				found = true;
				break;
			}
		}
		if(!found) {
			list_value = 0;
		}
		//console.log(list_value);
		return list_value;
	}
});
</script>