<?php

include("inc_header.php");

ASSIST_HELPER::displayResult(array("info","Page under development.  If you have any suggestions, please contact Janet Currie."));
die();
	?>
<div id=div_wrapper style='margin:0 auto; padding: 25px; width: 250px'>
	<div class='div_icons click_me' page=time >
		<p class=b><img src=common/time.png  title="Time" /><br /><?php echo $helper->getObjectName("TIME")?><br />&nbsp;</p>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$("div.div_icons").css({"border":"1px solid #dedede","width":"250px","height":"250px","display":"inline-block","text-align":"center"})
	.hover(function() {
		$(this).css("background","#e6e6ff");
	},function() {
		$(this).css("background","#ffffff");
	});
	$(".click_me").css("cursor","pointer").click(function() {
		AssistHelper.processing();
		var p = $(this).attr("page");
		document.location.href = "report_fixed_"+p+".php";
	});
	$("#tbl_icons").prop("width","100%").find("td").prop("width","25%");
});
</script>