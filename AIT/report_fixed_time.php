<?php

include("inc_header.php");
$timeObj = new AIT_TIME();
$options = array();

$js = "";
//get all users
$options['time_tkid'] = $timeObj->getListOfTimeUsersForReport();
//get all resellers
//get all client
//get all projects
//get all activities
$headings = $headingObject->getMainObjectHeadings("TIME");


foreach($headings['rows'] as $fld => $h) {
	if($h['type']=="AUTO_LIST" || $h['type']=="JQUERY_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	} elseif($h['type']=="COMBO_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	}
}
foreach($options as $fld => $opt) {
	asort($options[$fld]);
}




	?>
<form name=frm_report >
	<table id=tbl_filter class=form>
	<!--	<tr>
			<th>Report:</th>
			<td><select><option>Billable per Client</option></select></td>
	</tr> -->
		<tr>
			<th>Timekeepers:</th>
			<td><select name=sel_users[] multiple=multiple size=<?php $fld = "time_tkid"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Reseller:</th>
			<td><select name=sel_reseller[] multiple=multiple size=<?php $fld = "time_reseller"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Client:</th>
			<td><select name=sel_client[] multiple=multiple size=<?php $fld = "time_client"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Project:</th>
			<td><select name=sel_project[] multiple=multiple size=<?php $fld = "time_project"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Activity:</th>
			<td><select name=sel_activity[] multiple=multiple size=<?php $fld = "time_activity"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Location:</th>
			<td><select name=sel_location[] multiple=multiple size=<?php $fld = "time_location"; echo (count($options[$fld])<10?count($options[$fld]):10); ?>><?php
				foreach($options[$fld] as $i => $v) {
					if(strlen($v)==0) {
						$v = "&nbsp;&nbsp;&nbsp;[Blank]";
					}
					echo "<option value=".$i.">".$v."</option>";
				}
			?></select></td>
		</tr>
		<tr>
			<th>Type:</th>
			<td class=b><table id=tbl_type>
				<tr><td>Billable:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_billable",'small_size'=>true),0);  ?></td></tr>
				<tr><td>Development:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_development",'small_size'=>true),0);  ?></td></tr>
				<tr><td>Support:</td><td><?php $js.= $displayObject->drawFormField("BOOL", array('id'=>"is_support",'small_size'=>true),0);  ?></td></tr>
			</table></td>
		</tr>
		<tr>
			<th>Time:</th>
			<td>
				<input type=radio class=time-type name=time_type value=all checked for=lbl_all_time id=rad_all_time /><label id=lbl_all_time for=rad_all_time>All Time</label><br />
				<input type=radio class=time-type name=time_type value=limit for=lbl_limit_time id=rad_limit_time /><label id=lbl_limit_time for=rad_limit_time>From <input type=text name=time[] id=time_from size=10 /> to <input type=text name=time[] id=time_to size=10 /></label>
				<input type=hidden name=time_type_selected id=txt_time_type value='all' />
			</td>
		</tr>
		<tr>
			<th></th>
			<td><button class=btn-generate>Generate</button></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function() {
		<?php echo $js; ?>
		$(".btn-generate").button({
			icons:{primary:"ui-icon-script"}
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var err = false;
				$("#time_from").removeClass("required");
				$("#time_to").removeClass("required");
			if($("#txt_time_type").val()=="limit" && ($("#time_from").val().length==0 && $("#time_to").val().length==0)) {
				var err = true;
				$("#time_from").addClass("required");
				$("#time_to").addClass("required");
				AssistHelper.finishedProcessing("error","Please enter a time or choose All Time.");
			}
			if(!err) {
				var dta = AssistForm.serialize($("form[name=frm_report]"));
				//console.log(dta);
				document.location.href = "report_fixed_generate.php?"+dta;
			}
		});
		$(".time-type").click(function() {
			var v = $(this).val();
			$("#txt_time_type").val(v);
		})
		$("#tbl_type").css("border","1px solid #ffffff").find("td").css("border","1px solid #ffffff");
	});
</script>