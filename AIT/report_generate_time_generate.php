<?php
$no_page_heading = true;
include("inc_header.php");
$timeObj = new AIT_TIME();
$timeObj = new AIT_TIME();
$options = array();


if(isset($_REQUEST['report_settings'])) {
	$report_settings = $_REQUEST['report_settings'];
	unset($_REQUEST['report_settings']);
	$report_settings = base64_decode($report_settings);
	$report_settings = json_decode($report_settings);
	$_REQUEST = $report_settings;
}


$report_settings = base64_encode(json_encode($_REQUEST));

//ASSIST_HELPER::arrPrint($_REQUEST);
if(strlen($_REQUEST['report_name1'])>0) {
	echo "<h1 class=center>".$_REQUEST['report_name1']."</h1>";
}
if(strlen($_REQUEST['report_name2'])>0) {
	echo "<h2 class=center>".$_REQUEST['report_name2']."</h2>";
}


$js = "";
//get all users
$options['time_tkid'] = $timeObj->getListOfTimeUsersForReport();
//get all resellers
//get all client
//get all projects
//get all activities
$headings = $headingObject->getMainObjectHeadings("TIME");
//ASSIST_HELPER::arrPrint($headings);



foreach($headings['rows'] as $fld => $h) {
	if($h['type']=="AUTO_LIST" || $h['type']=="JQUERY_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	} elseif($h['type']=="COMBO_LIST") {
		$listObject = new AIT_LIST($h['list_table']);
		$options[$fld] = ($listObject->getActiveListItemsFormattedForReport(" (is_private = 0) "));
		unset($listObject);
	}
}
foreach($options as $fld => $opt) {
	asort($options[$fld]);
}

$filter = array();
foreach($_REQUEST as $fld => $value) {
	switch($fld) {
		case "sel_client":
			$filter['client'] = $value;
			break;
		case "sel_users":
			$filter['tkid'] = $value;
			break;
		case "sel_reseller":
			$filter['reseller'] = $value;
			break;
		case "sel_project":
			$filter['project'] = $value;
			break;
		case "sel_activity":
			$filter['activity'] = $value;
			break;
		case "sel_location":
			$filter['location'] = $value;
			break;
		case "time_type":
			if($value=="all") {
				//do nothing & ignore selected time
			} else {
				$filter['time'] = $_REQUEST['time'];
			}
			break;
	}
}

$raw_time_by_month = $timeObj->getAnalysisForReport($filter);

//ASSIST_HELPER::arrPrint($options);


$group_by = $_REQUEST['group_by'];
foreach($group_by as $i => $g) {
	if($g=="X" || strlen($g)==0) {
		unset($group_by[$i]);
	}
}

//if no grouping given, default to users
if(!(count($group_by)>0)) {
	$group_by[] = "time_tkid";
}

//ASSIST_HELPER::arrPrint($group_by);

$g = $group_by;
$group_by = array();
foreach($g as $gp) {
	$group_by[] = $gp;
}

//ASSIST_HELPER::arrPrint($group_by);
$time_columns = array();
$time = array(); $i=0;
foreach($raw_time_by_month as $raw) {
	//if($i==0) { ASSIST_HELPER::arrPrint($raw); } $i++;
	if(!isset($time_columns[$raw['year']][$raw['month']])) {
		$time_columns[$raw['year']][$raw['month']] = true;
	}
	switch(count($group_by)) {
/*		case 10:
			if(!isset($row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]][$raw[$group_by[9]]])) {
				$row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]][$raw[$group_by[9]]]=0;
			}
			$row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]][$raw[$group_by[9]]]+=$raw['time_minutes'];
			break;
		case 9:
			if(!isset($row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]])) {
				$row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]]=0;
			}
			$row[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]][$raw[$group_by[5]]][$raw[$group_by[6]]][$raw[$group_by[7]]][$raw[$group_by[8]]]+=$raw['time_minutes'];
			break;
		case 5:
			if(!isset($time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]])) {
				$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]]=0;
			}
			$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw[$group_by[4]]]+=$raw['time_minutes'];
			break;*/
		case 4:
			if(!isset($time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]])) {
				$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]]=array();
			}
			$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw[$group_by[3]]][$raw['year']][$raw['month']]+=$raw['time_minutes'];
			break;
		case 3:
			if(!isset($time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]])) {
				$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]]=array();
			}
			$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw[$group_by[2]]][$raw['year']][$raw['month']]+=$raw['time_minutes'];
			break;
		case 2:
			if(!isset($time[$raw[$group_by[0]]][$raw[$group_by[1]]])) {
				$time[$raw[$group_by[0]]][$raw[$group_by[1]]]=array();
			}
			$time[$raw[$group_by[0]]][$raw[$group_by[1]]][$raw['year']][$raw['month']]+=$raw['time_minutes'];
			break;
		case 1:
			if(!isset($time[$raw[$group_by[0]]])) {
				$time[$raw[$group_by[0]]]=array();
			}
			$time[$raw[$group_by[0]]][$raw['year']][$raw['month']]+=$raw['time_minutes'];
			break;
	}
}

//ASSIST_HELPER::arrPrint($group_by);

$boolean_fields = array("is_billable","is_development","is_support");

foreach($boolean_fields as $fld) {
	$options[$fld][1] = $fld;
	$options[$fld][0] = "Non-".$fld;
}


//sort the time columns so that they display in order and add any missing months

//sort time columns to get into correct order for display and add missing months to keep continuity
foreach($time_columns as $year => $months) {
	//prelim sort to get into order for check for missing elements
	ksort($months,SORT_NUMERIC);
	$i = 0;
	$test_months = $months;
	foreach($test_months as $month => $state) {
		//only test if not first month in list
		if($i>0) {
			//check for missing
			if($month!=($prev_month+1)) {
				for($i=($prev_month+1);$i<$month;$i++) {
					$months[$i]++;
				}
			}
		}
		$i++;
		$prev_month = $month;
	}
	//resort to put any added months into the correct position
	ksort($months,SORT_NUMERIC);
	$time_columns[$year] = $months;
}



switch(count($group_by)) {


	case 4:
		$loop_count = 0;
		//foreach($time as $key0 => $time0) {
		foreach($options[$group_by[0]] as $key0 => $name) {
			if(!isset($time[$key0])) {
				//do nothing and move on
			} else {
				$time0 = $time[$key0];
				$i = 0;
				if($loop_count>0) {
					//echo "<hr />";
				}
				$loop_count++;
				$group_field = $group_by[$i];
				$display_value = getLineItemName($group_field, $key0);
				$colspan=2;
				echo "<h3>".$display_value."</h3>";
				echo "
				<table>
					<tr>
						<th class=head></th>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								$colspan++;
								echo "<th class=head>".date("M-Y",strtotime($year."-".$month."-01 12:00:00"))."</th>";
							}
						}
						echo "
						<th class=head>Total</th>
					</tr>";
				$final_total = 0;
				$final_sub_total = array();
				foreach($options[$group_by[1]] as $key1 => $name1) {
				//foreach($time0 as $key1 => $time1) {
					if(!isset($time0[$key1])) {
						//skip
					} else {
						$time1 = $time0[$key1];
						$section_total1=0;
						$sub_total1=array();
						$i = 1;
						$group_field = $group_by[$i];
						$display_value = getLineItemName($group_field, $key1);
						echo "
							<tr>
								<td  colspan=".$colspan." class=th0>".$display_value."</td >
							</tr>";
						foreach($options[$group_by[2]] as $key2 => $name2) {
						//foreach($time1 as $key2 => $time2) {
							if(!isset($time1[$key2])) {
								//skip
							} else {
								$time2 = $time1[$key2];
								$section_total=0;
								$sub_total = array();
								$i = 2;
								$group_field = $group_by[$i];
								$display_value = getLineItemName($group_field, $key2);
								echo "
								<tr>
									<td colspan=".$colspan." class=th1>".$display_value."</td >
								</tr>";
								foreach($options[$group_by[3]] as $key3 => $name3) {
								//foreach($time2 as $key3 => $time3) {
									if(!isset($time2[$key3])) {
										//skip
									} else {
										$time3 = $time2[$key3];
										$row_total = 0;
										$i = 3;
										$group_field = $group_by[$i];
										$display_value = getLineItemName($group_field, $key3);
										echo "
										<tr class=normal_row>
											<td style='padding-left:15px'>".$display_value."</td>
											";
											foreach($time_columns as $year => $months) {
												foreach($months as $month => $state) {
													$minutes = isset($time3[$year][$month]) ?$time3[$year][$month]:0;
													$sub_total[$year][$month]+=$minutes;
													$sub_total1[$year][$month]+=$minutes;
													$final_sub_total[$year][$month]+=$minutes;
													echo "<td class=center>".getHours($minutes)."</td>";
													$row_total+=$minutes;
												}
											}
											echo "
											<td class=row_total>".getHours($row_total)."</td>
										</tr>";
										$section_total+=$row_total;
										$section_total1+=$row_total;
									}//end if
								}//end foreach
								echo "
								<tr>
									<td class=sub_total>Sub-total:</td>";
										foreach($time_columns as $year => $months) {
											foreach($months as $month => $state) {
												echo "<td class='sub_time_total'>".(isset($sub_total[$year][$month]) ? getHours($sub_total[$year][$month]) : getHours(0))."</td>";
											}
										}
								echo "
									<td class=sub_row_total>".getHours($section_total)."</td>
								</tr>";
							}//endif exists
						}//end foreach
						echo "
						<tr>
							<td class=section_total>Total:</td>";
								foreach($time_columns as $year => $months) {
									foreach($months as $month => $state) {
										echo "<td class='section_time_total'>".(isset($sub_total1[$year][$month]) ? getHours($sub_total1[$year][$month]) : getHours(0))."</td>";
									}
								}
						echo "
							<td class=section_row_total>".getHours($section_total1)."</td>
						</tr>";
						$final_total+=$section_total1;
					}//endif time1 exists
				}//end foreach group1
				echo "
				<tr>
					<td class=grand_total>Grand Total:</td>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								echo "<td class='grand_time_total'>".(isset($final_sub_total[$year][$month]) ? getHours($final_sub_total[$year][$month]) : getHours(0))."</td>";
							}
						}
				echo "
					<td class=grand_row_total>".getHours($final_total)."</td>
				</tr>";
				echo "
				</table>";
			}//endif time key0 exists
		}//end foreach group
		break;






	case 3:
		$loop_count = 0;
		//foreach($time as $key0 => $time0) {
		foreach($options[$group_by[0]] as $key0 => $name) {
			if(!isset($time[$key0])) {
				//do nothing and move on
			} else {
				$time0 = $time[$key0];
				$i = 0;
				if($loop_count>0) {
					//echo "<hr />";
				}
				$loop_count++;
				$group_field = $group_by[$i];
				$display_value = getLineItemName($group_field, $key0);
				$colspan=2;
				echo "<h3>".$display_value."</h3>";
				echo "
				<table>
					<tr>
						<th class=head></th>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								$colspan++;
								echo "<th class=head>".date("M-Y",strtotime($year."-".$month."-01 12:00:00"))."</th>";
							}
						}
						echo "
						<th class=head>Total</th>
					</tr>";
				$final_total = 0;
				$final_sub_total = array();
				foreach($options[$group_by[1]] as $key1 => $name1) {
				//foreach($time0 as $key1 => $time1) {
					if(!isset($time0[$key1])) {
						//skip
					} else {
						$time1 = $time0[$key1];
						$section_total1=0;
						$sub_total1=array();
						$i = 1;
						$group_field = $group_by[$i];
						$display_value = getLineItemName($group_field, $key1);
						echo "
							<tr>
								<td  colspan=".$colspan." class=th0>".$display_value."</td >
							</tr>";
						/*foreach($options[$group_by[2]] as $key2 => $name2) {
						//foreach($time1 as $key2 => $time2) {
							if(!isset($time1[$key2])) {
								//skip
							} else {
								$time2 = $time1[$key2];
								$section_total=0;
								$sub_total = array();
								$i = 2;
								$group_field = $group_by[$i];
								$display_value = getLineItemName($group_field, $key2);
								echo "
								<tr>
									<td colspan=".$colspan." class=th1>".$display_value."</td >
								</tr>";*/
								foreach($options[$group_by[2]] as $key3 => $name3) {
								//foreach($time2 as $key3 => $time3) {
									if(!isset($time1[$key3])) {
										//skip
									} else {
										$time3 = $time1[$key3];
										$row_total = 0;
										$i = 2;
										$group_field = $group_by[$i];
										$display_value = getLineItemName($group_field, $key3);
										echo "
										<tr class=normal_row>
											<td style='padding-left:15px'>".$display_value."</td>
											";
											foreach($time_columns as $year => $months) {
												foreach($months as $month => $state) {
													$minutes = isset($time3[$year][$month]) ?$time3[$year][$month]:0;
													$sub_total[$year][$month]+=$minutes;
													$sub_total1[$year][$month]+=$minutes;
													$final_sub_total[$year][$month]+=$minutes;
													echo "<td class=center>".getHours($minutes)."</td>";
													$row_total+=$minutes;
												}
											}
											echo "
											<td class=row_total>".getHours($row_total)."</td>
										</tr>";
										$section_total+=$row_total;
										$section_total1+=$row_total;
									}//end if
								}//end foreach
					/*			echo "
								<tr>
									<td class=sub_total>Sub-total:</td>";
										foreach($time_columns as $year => $months) {
											foreach($months as $month => $state) {
												echo "<td class='sub_time_total'>".(isset($sub_total[$year][$month]) ? getHours($sub_total[$year][$month]) : getHours(0))."</td>";
											}
										}
								echo "
									<td class=sub_row_total>".getHours($section_total)."</td>
								</tr>";
							}//endif exists
						}//end foreach */
						echo "
						<tr>
							<td class=section_total>Total:</td>";
								foreach($time_columns as $year => $months) {
									foreach($months as $month => $state) {
										echo "<td class='section_time_total'>".(isset($sub_total1[$year][$month]) ? getHours($sub_total1[$year][$month]) : getHours(0))."</td>";
									}
								}
						echo "
							<td class=section_row_total>".getHours($section_total1)."</td>
						</tr>";
						$final_total+=$section_total1;
					}//endif time1 exists
				}//end foreach group1
				echo "
				<tr>
					<td class=grand_total>Grand Total:</td>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								echo "<td class='grand_time_total'>".(isset($final_sub_total[$year][$month]) ? getHours($final_sub_total[$year][$month]) : getHours(0))."</td>";
							}
						}
				echo "
					<td class=grand_row_total>".getHours($final_total)."</td>
				</tr>";
				echo "
				</table>";
			}//endif time key0 exists
		}//end foreach group
		break;









	case 2:
		$loop_count = 0;
		//foreach($time as $key0 => $time0) {
		foreach($options[$group_by[0]] as $key0 => $name) {
			if(!isset($time[$key0])) {
				//do nothing and move on
			} else {
				$time0 = $time[$key0];
				$i = 0;
				if($loop_count>0) {
					//echo "<hr />";
				}
				$loop_count++;
				$group_field = $group_by[$i];
				$display_value = getLineItemName($group_field, $key0);
				$colspan=2;
				echo "<h3>".$display_value."</h3>";
				echo "
				<table>
					<tr>
						<th class=head></th>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								$colspan++;
								echo "<th class=head>".date("M-Y",strtotime($year."-".$month."-01 12:00:00"))."</th>";
							}
						}
						echo "
						<th class=head>Total</th>
					</tr>";
				$final_total = 0;
				$final_sub_total = array();
				/*foreach($options[$group_by[1]] as $key1 => $name1) {
				//foreach($time0 as $key1 => $time1) {
					if(!isset($time0[$key1])) {
						//skip
					} else {
						$time1 = $time0[$key1];
						$section_total1=0;
						$sub_total1=array();
						$i = 1;
						$group_field = $group_by[$i];
						$display_value = getLineItemName($group_field, $key1);
						echo "
							<tr>
								<td  colspan=".$colspan." class=th0>".$display_value."</td >
							</tr>";
						/*foreach($options[$group_by[2]] as $key2 => $name2) {
						//foreach($time1 as $key2 => $time2) {
							if(!isset($time1[$key2])) {
								//skip
							} else {
								$time2 = $time1[$key2];
								$section_total=0;
								$sub_total = array();
								$i = 2;
								$group_field = $group_by[$i];
								$display_value = getLineItemName($group_field, $key2);
								echo "
								<tr>
									<td colspan=".$colspan." class=th1>".$display_value."</td >
								</tr>";*/
								foreach($options[$group_by[1]] as $key3 => $name3) {
								//foreach($time2 as $key3 => $time3) {
									if(!isset($time0[$key3])) {
										//skip
									} else {
										$time3 = $time0[$key3];
										$row_total = 0;
										$i = 1;
										$group_field = $group_by[$i];
										$display_value = getLineItemName($group_field, $key3);
										echo "
										<tr class=normal_row>
											<td style='padding-left:15px'>".$display_value."</td>
											";
											foreach($time_columns as $year => $months) {
												foreach($months as $month => $state) {
													$minutes = isset($time3[$year][$month]) ?$time3[$year][$month]:0;
													$sub_total[$year][$month]+=$minutes;
													$sub_total1[$year][$month]+=$minutes;
													$final_sub_total[$year][$month]+=$minutes;
													echo "<td class=center>".getHours($minutes)."</td>";
													$row_total+=$minutes;
												}
											}
											echo "
											<td class=row_total>".getHours($row_total)."</td>
										</tr>";
										$section_total+=$row_total;
										$section_total1+=$row_total;
						$final_total+=$row_total;
									}//end if
								}//end foreach
					/*			echo "
								<tr>
									<td class=sub_total>Sub-total:</td>";
										foreach($time_columns as $year => $months) {
											foreach($months as $month => $state) {
												echo "<td class='sub_time_total'>".(isset($sub_total[$year][$month]) ? getHours($sub_total[$year][$month]) : getHours(0))."</td>";
											}
										}
								echo "
									<td class=sub_row_total>".getHours($section_total)."</td>
								</tr>";
							}//endif exists
						}//end foreach */
						/*echo "
						<tr>
							<td class=section_total>Total:</td>";
								foreach($time_columns as $year => $months) {
									foreach($months as $month => $state) {
										echo "<td class='section_time_total'>".(isset($sub_total1[$year][$month]) ? getHours($sub_total1[$year][$month]) : getHours(0))."</td>";
									}
								}
						echo "
							<td class=section_row_total>".getHours($section_total1)."</td>
						</tr>";
					}//endif time1 exists
				}//end foreach group1*/
				echo "
				<tr>
					<td class=grand_total>Grand Total:</td>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								echo "<td class='grand_time_total'>".(isset($final_sub_total[$year][$month]) ? getHours($final_sub_total[$year][$month]) : getHours(0))."</td>";
							}
						}
				echo "
					<td class=grand_row_total>".getHours($final_total)."</td>
				</tr>";
				echo "
				</table>";
			}//endif time key0 exists
		}//end foreach group
		break;











	case 1:
		$loop_count = 0;
		//foreach($time as $key0 => $time0) {
		/*foreach($options[$group_by[0]] as $key0 => $name) {
			if(!isset($time[$key0])) {
				//do nothing and move on
			} else {
				$time0 = $time[$key0];
				$i = 0;
				if($loop_count>0) {
					//echo "<hr />";
				}
				$loop_count++;
				$group_field = $group_by[$i];
				$display_value = getLineItemName($group_field, $key0);
				$colspan=2;
				echo "<h3>".$display_value."</h3>";*/
				echo "
				<table>
					<tr>
						<th class=head></th>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								$colspan++;
								echo "<th class=head>".date("M-Y",strtotime($year."-".$month."-01 12:00:00"))."</th>";
							}
						}
						echo "
						<th class=head>Total</th>
					</tr>";
				$final_total = 0;
				$final_sub_total = array();
				/*foreach($options[$group_by[1]] as $key1 => $name1) {
				//foreach($time0 as $key1 => $time1) {
					if(!isset($time0[$key1])) {
						//skip
					} else {
						$time1 = $time0[$key1];
						$section_total1=0;
						$sub_total1=array();
						$i = 1;
						$group_field = $group_by[$i];
						$display_value = getLineItemName($group_field, $key1);
						echo "
							<tr>
								<td  colspan=".$colspan." class=th0>".$display_value."</td >
							</tr>";
						/*foreach($options[$group_by[2]] as $key2 => $name2) {
						//foreach($time1 as $key2 => $time2) {
							if(!isset($time1[$key2])) {
								//skip
							} else {
								$time2 = $time1[$key2];
								$section_total=0;
								$sub_total = array();
								$i = 2;
								$group_field = $group_by[$i];
								$display_value = getLineItemName($group_field, $key2);
								echo "
								<tr>
									<td colspan=".$colspan." class=th1>".$display_value."</td >
								</tr>";*/
								foreach($options[$group_by[0]] as $key3 => $name3) {
								//foreach($time2 as $key3 => $time3) {
									if(!isset($time[$key3])) {
										//skip
									} else {
										$time3 = $time[$key3];
										$row_total = 0;
										$i = 0;
										$group_field = $group_by[$i];
										$display_value = getLineItemName($group_field, $key3);
										echo "
										<tr class=normal_row>
											<td style='padding-left:15px'>".$display_value."</td>
											";
											foreach($time_columns as $year => $months) {
												foreach($months as $month => $state) {
													$minutes = isset($time3[$year][$month]) ?$time3[$year][$month]:0;
													$sub_total[$year][$month]+=$minutes;
													$sub_total1[$year][$month]+=$minutes;
													$final_sub_total[$year][$month]+=$minutes;
													echo "<td class=center>".getHours($minutes)."</td>";
													$row_total+=$minutes;
												}
											}
											echo "
											<td class=row_total>".getHours($row_total)."</td>
										</tr>";
										$section_total+=$row_total;
										$section_total1+=$row_total;
						$final_total+=$row_total;
									}//end if
								}//end foreach
					/*			echo "
								<tr>
									<td class=sub_total>Sub-total:</td>";
										foreach($time_columns as $year => $months) {
											foreach($months as $month => $state) {
												echo "<td class='sub_time_total'>".(isset($sub_total[$year][$month]) ? getHours($sub_total[$year][$month]) : getHours(0))."</td>";
											}
										}
								echo "
									<td class=sub_row_total>".getHours($section_total)."</td>
								</tr>";
							}//endif exists
						}//end foreach */
						/*echo "
						<tr>
							<td class=section_total>Total:</td>";
								foreach($time_columns as $year => $months) {
									foreach($months as $month => $state) {
										echo "<td class='section_time_total'>".(isset($sub_total1[$year][$month]) ? getHours($sub_total1[$year][$month]) : getHours(0))."</td>";
									}
								}
						echo "
							<td class=section_row_total>".getHours($section_total1)."</td>
						</tr>";
					}//endif time1 exists
				}//end foreach group1*/
				echo "
				<tr>
					<td class=grand_total>Grand Total:</td>";
						foreach($time_columns as $year => $months) {
							foreach($months as $month => $state) {
								echo "<td class='grand_time_total'>".(isset($final_sub_total[$year][$month]) ? getHours($final_sub_total[$year][$month]) : getHours(0))."</td>";
							}
						}
				echo "
					<td class=grand_row_total>".getHours($final_total)."</td>
				</tr>";
				echo "
				</table>";
			//}//endif time key0 exists
		//}//end foreach group
		break;






}

echo "<p class=i>Report drawn on: ".date("d F Y H:i:s").".</p>";
//echo "<div style='width:500px;margin-left:3px;'><p class=i style='font-size:7.5pt; color:#999999; line-height:9pt;margin:0px;padding:0px;'>Report Settings:<br />".wordwrap($report_settings,50,"</br>",true)."</p></div>";

function getHours($minutes) {
	if($minutes==0) {
		return "-";
	}
	return number_format(($minutes/60),2);
}

function getLineItemName($group_field,$key) {
	global $boolean_fields, $options, $headings, $timeObj;
	$head = $headings['rows'];
	if(!in_array($group_field, $boolean_fields)) {
		$display_value = isset($options[$group_field][$key]) ? $options[$group_field][$key] : "Unknown ".$head[$group_field]['name'].": ".$key;
	} else {
		$f = explode("_",$group_field);
		$heading_field = "time_".$f[1];
		$display_value = ($key==0?"Non-":"").$head[$heading_field]['name'];
	}
	if(strlen($display_value)==0) {
		$display_value = $timeObj->getUnspecified();
	}
	return $display_value;
}

	?>
<style type="text/css">
	td.th0 {
		font-size: 125%;
		text-align: left;
		background-color: #999999;
		font-weight: bold;
	}
	td.th1 {
		font-size: 110%
		text-align: left;
		background-color: #cdcdcd;
		color: #000000;
		font-weight:bold;
	}
	th.head {
		background-color: #555555;
	}
	h1 {
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
	}
	h2 {
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
	}
	h3 {
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
	}
	h4 {
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
	}

	tr.normal_row td {
		font-size: 90%;

	}

	.row_total {
		text-align: center;
		background-color: #dedede;
		font-weight: bold;
	}
	.sub_total {
		background-color: #eeeeee;font-size: 90%;
		text-align: right;font-style: italic;
	}
	.sub_time_total {
		text-align: center;font-size: 90%;
		background-color: #eeeeee;font-style: italic;
	}
	.sub_row_total {
		text-align: center;
		background-color: #cccccc;font-size: 90%;
		font-weight:bold;font-style: italic;
	}
	.section_total {
		background-color: #cccccc;
		text-align: right;
		font-weight:bold;
	}
	.section_time_total {
		text-align: center;
		background-color: #cccccc;
		font-weight:bold;
	}
	.section_row_total {
		text-align: center;
		background-color: #ababab;
		font-weight:bold;
	}
	.grand_total {
		font-size: 115%;
		background-color: #999999;
		text-align: right;
		font-weight:bold;
	}
	.grand_time_total {
		font-size:115%;
		text-align: center;
		background-color: #999999;
		font-weight:bold;
	}
	.grand_row_total {
		font-size:115%;
		text-align: center;
		background-color: #777777;
		font-weight:bold;
	}
</style>