<?php
$page_title = "Time";
include("inc_header.php");

function array_sort_by_activity($a, $b) {
    return strcmp($a['activity'], $b['activity']);
}
function array_sort_by_client($a, $b) {
    return strcmp($a['client'], $b['client']);
}


$timeObj = new AIT_TIME();

	$my_date = isset($_REQUEST['m']) ? $_REQUEST['m'] : time();
	$current = strtotime(date("d F Y 12:0:0",$my_date));
	$today = strtotime(date("d F Y")." 12:00:00");



$analysis_period = "YEAR";
$financial_year = $timeObj->getFinancialYear($current);

$next_financial_year = $timeObj->getNextFinancialYear($current);
$can_go_next = $next_financial_year!==false;
if($can_go_next) {
	$next_click = $next_financial_year['start'];
	$next_text = "Next Financial Year";
}

$prev_financial_year = $timeObj->getPreviousFinancialYear($current);
$can_go_prev = $prev_financial_year!==false;
if($can_go_prev) {
	$prev_click = $prev_financial_year['start'];
	$prev_text = "Prev Financial Year";
}


$raw_time_by_month = $timeObj->getAnalysis("TIME_BY_MONTH",$current,$analysis_period);

echo "
<table id=tbl_container style='margin: 0 auto'>
	<tr id=tr_move>
		<td class='left'>".(($can_go_prev)?"<button class=btn-move id=btn_prev go=".$prev_click.">".$prev_text."</button>":"")."</td>
		<td class='right'>".(($can_go_next)?"<button class=btn-move id=btn_next go=".$next_click.">".$next_text."</button>":"")."</td>
	</tr>
<tr><td colspan=3>
";


//ONLY DISPLAY FOR RK & JC FOR NOW UNTIL TIME SHEET COMPLETED

if(in_array($timeObj->getUserID(),array("0001","0002"))) {


$business_raw_time_by_month = $timeObj->getAnalysis("TIME_BY_MONTH",$current,$analysis_period,"ALL");

echo "
<h2 class=center>".$timeObj->getCmpName()." Financial Year Analysis</h2>
<h3 class=center>".date("d F Y",$financial_year['start'])." - ".date("d F Y",$financial_year['end'])."</h3>";



$objects = array(
	'is_billable'=>"Billable",
	'product_development'=>"Product Development",
	'client_support'=>"Client Support",
);
$lines = $objects;
$lines['admin'] = "Admin";

$time_by_month = array();
$time_by_employee_by_month = array();
$total_time_by_month = array();
$total_time_by_lines = array();


Foreach($business_raw_time_by_month as $row) {
	$employee = $row['tkid'];
	//create default "admin" record if required
	$object = "admin";
	if(!isset($time_by_month[$row['year']][$row['month']][$object])) {
		$time_by_month[$row['year']][$row['month']][$object] = 0;
	}
	if(!isset($time_by_employee_by_month[$employee][$row['year']][$row['month']][$object])) {
		$time_by_employee_by_month[$employee][$row['year']][$row['month']][$object] = 0;
	}
	//check if time as been allocated to a specific type
	$time_recorded = false;
	foreach($objects as $object=>$object_name) {
		if(!isset($time_by_month[$row['year']][$row['month']][$object])) {
			$time_by_month[$row['year']][$row['month']][$object] = 0;
			$time_by_employee_by_month[$employee][$row['year']][$row['month']][$object] = 0;
		}
		if($row[$object]==true) {
			$time_recorded = true;
			$time_by_month[$row['year']][$row['month']][$object]+= $row['time_minutes'];
			$time_by_employee_by_month[$employee][$row['year']][$row['month']][$object]+= $row['time_minutes'];
		}
	}
	//otherwise allocate to admin
	if(!$time_recorded) {
		$object = "admin";
		$time_by_month[$row['year']][$row['month']][$object]+= $row['time_minutes'];
		$time_by_employee_by_month[$employee][$row['year']][$row['month']][$object]+= $row['time_minutes'];
	}
}




$analysis_date = $financial_year['start'];
$financial_months = array();

echo "<h4>Time by Type</h4>";


echo "<table><tr><th class=th-object></th>";
while($analysis_date<=$financial_year['end']) {
	$financial_months[] = $analysis_date;
	$total_time[$analysis_date] = 0;
	echo "<th class=th-month>".date("M Y",$analysis_date)."</th>";
	$analysis_date = mktime(12,0,0,date("m",$analysis_date)+1,1,date("Y",$analysis_date));
}
echo "<th>Total</th></tr>";





foreach($lines as $p=>$object_name) {
	if($p=="billable") {
		$p == "is_billable";
	}
	echo  "<tr><td class=b>".$object_name."</td>";
	foreach($financial_months as $p_date) {
		$p_year = date("Y",$p_date)*1;
		$p_month = date("m",$p_date)*1;
		echo "<td class=center>";
		if(isset($time_by_month[$p_year][$p_month][$p]) && $time_by_month[$p_year][$p_month][$p]>0) {
			echo number_format($time_by_month[$p_year][$p_month][$p]/60,2);
			$total_time_by_lines[$p]+=$time_by_month[$p_year][$p_month][$p];
			$total_time_by_month[$p_date]+=$time_by_month[$p_year][$p_month][$p];
		} else {
			echo "-";
		}
		echo "</td>";
	}
	echo "<td class='td-total center'>".number_format($total_time_by_lines[$p]/60,2)."</td>";
	echo "</tr>";
}

echo "<tr class=tr-total><td class=' right'>Total:</td>";
	foreach($financial_months as $p_date) {
		echo "<td class=' center'>".number_format(($total_time_by_month[$p_date])/60,2)."</td>";
	}
	echo "<td class='grand-total center'>".number_format(array_sum($total_time_by_month)/60,2)."</td>";
echo "</tr>";

echo "</table>";
echo "<h4>Time by Employee</h4>";

$total_time_by_month = array();
$total_time_by_lines = array();
$analysis_date = $financial_year['start'];
$financial_months = array();

$employees = array_keys($time_by_employee_by_month);
$employee_names = $timeObj->getAUserName($employees);

echo "<table><tr><th class=th-object></th>";
while($analysis_date<=$financial_year['end']) {
	$financial_months[] = $analysis_date;
	$total_time[$analysis_date] = 0;
	echo "<th class=th-month>".date("M Y",$analysis_date)."</th>";
	$analysis_date = mktime(12,0,0,date("m",$analysis_date)+1,1,date("Y",$analysis_date));
}
echo "<th>Total</th></tr>";

foreach($employees as $employee) {
	$emp_total = 0;
	echo "<tr class=sub-heading><td class=b>".$employee_names[$employee]."</td>";
	$p = "ALL";
		foreach($financial_months as $p_date) {
			$p_year = date("Y",$p_date)*1;
			$p_month = date("m",$p_date)*1;
			echo "<td class=center>";
			if(isset($time_by_employee_by_month[$employee][$p_year][$p_month]) && array_sum($time_by_employee_by_month[$employee][$p_year][$p_month])>0) {
				echo number_format(array_sum($time_by_employee_by_month[$employee][$p_year][$p_month])/60,2);
				$emp_total+=array_sum($time_by_employee_by_month[$employee][$p_year][$p_month]);
			} else {
				echo "-";
			}
			echo "</td>";
		}
		echo "<td class='td-total center'>".number_format($emp_total/60,2)."</td>";

	echo "</tr>";
	foreach($lines as $p=>$object_name) {
		echo  "<tr><td style='padding-left:25px'>".$object_name."</td>";
		foreach($financial_months as $p_date) {
			$p_year = date("Y",$p_date)*1;
			$p_month = date("m",$p_date)*1;
			echo "<td class=center>";
			if(isset($time_by_employee_by_month[$employee][$p_year][$p_month][$p]) && $time_by_employee_by_month[$employee][$p_year][$p_month][$p]>0) {
				echo number_format($time_by_employee_by_month[$employee][$p_year][$p_month][$p]/60,2);
				$total_time_by_lines[$employee][$p]+=$time_by_employee_by_month[$employee][$p_year][$p_month][$p];
				$total_time_by_month[$p_date]+=$time_by_employee_by_month[$employee][$p_year][$p_month][$p];
			} else {
				echo "-";
			}
			echo "</td>";
		}
		echo "<td class='td-total center'>".number_format($total_time_by_lines[$employee][$p]/60,2)."</td>";
		echo "</tr>";
	}
}
echo "<tr class=tr-total><td class=' right'>Total:</td>";
	foreach($financial_months as $p_date) {
		echo "<td class=' center'>".number_format(($total_time_by_month[$p_date])/60,2)."</td>";
	}
	echo "<td class='grand-total center'>".number_format(array_sum($total_time_by_month)/60,2)."</td>";
echo "</tr>";

echo "</table>";





}
//end if only display for JC & RK



echo "<p>&nbsp;</p><hr /><p>&nbsp;</p>
<h2 class=center>".$timeObj->getUserName()." Financial Year Analysis</h2><h3 class=center>".date("d F Y",$financial_year['start'])." - ".date("d F Y",$financial_year['end'])."</h3>
";












$time_by_month = array();
$total_time_by_month = array();
$total_time_by_lines = array();


Foreach($raw_time_by_month as $row) {
	//create default "admin" record if required
	$object = "admin";
	if(!isset($time_by_month[$row['year']][$row['month']][$object])) {
		$time_by_month[$row['year']][$row['month']][$object] = 0;
	}
	//check if time as been allocated to a specific type
	$time_recorded = false;
	foreach($objects as $object=>$object_name) {
		if(!isset($time_by_month[$row['year']][$row['month']][$object])) {
			$time_by_month[$row['year']][$row['month']][$object] = 0;
		}
		if($row[$object]==true) {
			$time_recorded = true;
			$time_by_month[$row['year']][$row['month']][$object]+= $row['time_minutes'];
		}
	}
	//otherwise allocate to admin
	if(!$time_recorded) {
		$object = "admin";
		$time_by_month[$row['year']][$row['month']][$object]+= $row['time_minutes'];
	}
}

$analysis_date = $financial_year['start'];
$financial_months = array();


echo "<table><tr><th class=th-object></th>";
while($analysis_date<=$financial_year['end']) {
	$financial_months[] = $analysis_date;
	$total_time[$analysis_date] = 0;
	echo "<th class=th-month>".date("M Y",$analysis_date)."</th>";
	$analysis_date = mktime(12,0,0,date("m",$analysis_date)+1,1,date("Y",$analysis_date));
}
echo "<th>Total</th></tr>";


foreach($lines as $p=>$object_name) {
	echo  "<tr><td class=b>".$object_name."</td>";
	foreach($financial_months as $p_date) {
		$p_year = date("Y",$p_date)*1;
		$p_month = date("m",$p_date)*1;
		echo "<td class=center>";
		if(isset($time_by_month[$p_year][$p_month][$p]) && $time_by_month[$p_year][$p_month][$p]>0) {
			echo number_format($time_by_month[$p_year][$p_month][$p]/60,2);
			$total_time_by_lines[$p]+=$time_by_month[$p_year][$p_month][$p];
			$total_time_by_month[$p_date]+=$time_by_month[$p_year][$p_month][$p];
		} else {
			echo "-";
		}
		echo "</td>";
	}
	echo "<td class='td-total center'>".number_format($total_time_by_lines[$p]/60,2)."</td>";
	echo "</tr>";
}

echo "<tr class=tr-total><td class=' right'>Total:</td>";
	foreach($financial_months as $p_date) {
		echo "<td class=' center'>".number_format(($total_time_by_month[$p_date])/60,2)."</td>";
	}
	echo "<td class='grand-total center'>".number_format(array_sum($total_time_by_month)/60,2)."</td>";
echo "</tr>";

echo "</table>";

















$reports = array(
	'reseller' => "Partner Time By Month",
	'client' => "Client Time By Month",
	'project'=>"Project Time By Month",
	'location'=>"Location Time By Month",
);



foreach($reports as $object=>$title) {
echo "
<h4>$title</h4>
";



$time_by_month = array();
$lines = array();
$total_time_by_month = array();
$total_time_by_lines = array();


Foreach($raw_time_by_month as $row) {
	if(!isset($time_by_month[$row['year']][$row['month']][$row[$object]])) {
		$time_by_month[$row['year']][$row['month']][$row[$object]] = 0;
	}
	$time_by_month[$row['year']][$row['month']][$row[$object]]+= $row['time_minutes'];
	if(!in_array($row[$object],$lines)) {
		$lines[] = $row[$object];
		$total_time_by_lines[$row[$object]] = 0;
	}
}
sort($lines);

$analysis_date = $financial_year['start'];
$financial_months = array();


echo "<table><tr><th class=th-object>".ucfirst($object)."</th>";
while($analysis_date<=$financial_year['end']) {
	$financial_months[] = $analysis_date;
	$total_time[$analysis_date] = 0;
	echo "<th class=th-month>".date("M Y",$analysis_date)."</th>";
	$analysis_date = mktime(12,0,0,date("m",$analysis_date)+1,1,date("Y",$analysis_date));
}
echo "<th>Total</th></tr>";


foreach($lines as $p) {
	echo  "<tr><td class=b>".$p."</td>";
	foreach($financial_months as $p_date) {
		$p_year = date("Y",$p_date)*1;
		$p_month = date("m",$p_date)*1;
		echo "<td class=center>";
		if(isset($time_by_month[$p_year][$p_month][$p])) {
			echo number_format($time_by_month[$p_year][$p_month][$p]/60,2);
			$total_time_by_lines[$p]+=$time_by_month[$p_year][$p_month][$p];
			$total_time_by_month[$p_date]+=$time_by_month[$p_year][$p_month][$p];
		} else {
			echo "-";
		}
		echo "</td>";
	}
	echo "<td class='td-total center'>".number_format($total_time_by_lines[$p]/60,2)."</td>";
	echo "</tr>";
}

echo "<tr class=tr-total><td class=' right'>Total:</td>";
	foreach($financial_months as $p_date) {
		echo "<td class=' center'>".number_format(($total_time_by_month[$p_date])/60,2)."</td>";
	}
	echo "<td class='grand-total center'>".number_format(array_sum($total_time_by_month)/60,2)."</td>";
echo "</tr>";

echo "</table>";









}









//end container table
echo "</td></tr></table>";
















?>



<script type=text/javascript>
$(function() {
	$("#tbl_container, #tbl_container td").css("border","0px");
	$("tr.sub-heading td").css("background-color","#dedede").addClass("b");

	var heading_width = 0;
	$(".th-object").each(function() {
		var w = parseInt(AssistString.substr($(this).css("width"),0,-2));
		if(w>heading_width) {
			heading_width = w;
		}
	});
	$(".th-object").css("width",(heading_width+2)+"px");

	var heading_width = 0;
	$(".th-month").each(function() {
		var w = parseInt(AssistString.substr($(this).css("width"),0,-2));
		if(w>heading_width) {
			heading_width = w;
		}
	});
	$(".th-month").css("width",(heading_width+2)+"px");
	$("td.grand-total").each(function() {
		var w = parseInt(AssistString.substr($(this).css("width"),0,-2));
		if(w>heading_width) {
			heading_width = w;
		}
	});
	$("td.grand-total").css("width",(heading_width+2)+"px");

	$("button.btn-move")
	.click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var m = $(this).attr("go"); console.log(m);
		document.location.href = "view_time.php?m="+m;
	}).css({"background":"url()","font-weight":"bold","border-width":"0px"});
	$("#btn_prev").button({
		icons:{primary:"ui-icon-arrowthick-1-w"}
	}).removeClass("ui-state-default").addClass("ui-state-error");

	$("#btn_current").button({
		icons:{primary:"ui-icon-arrowthick-1-s"}
	}).removeClass("ui-state-default").addClass("ui-state-ok");

	$("#btn_next").button({
		icons:{secondary:"ui-icon-arrowthick-1-e"}
	}).removeClass("ui-state-default").addClass("ui-state-info");

});
</script>