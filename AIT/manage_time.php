<?php
$page_title = "Time";
include("inc_header.php");

function array_sort_by_activity($a, $b) {
    return strcmp($a['activity'], $b['activity']);
}
function array_sort_by_client($a, $b) {
    return strcmp($a['client'], $b['client']);
}

function drawDateRow($now) {
	global $today;
	global $timeObj;
	global $summary_rows;
	$class = " ";
	$btn_class = " ";
	if($now==$today) {
		$class.= "today ";
		$btn_class.= " today ";
	} elseif(date("N",$now)>5 || $timeObj->isPublicHoliday($now)) {
		$class.="weekend ";
		$btn_class.=" weekend ";
	}
	if($now<$today) {
		$btn_class = "past";
	}

	$allow_open = true;
	if($now>$today) {
		$allow_open = true;//false;
		$class.=" future";
		$btn_class = "";
	}
	$records = isset($summary_rows[date("d",$now)]) ? ($summary_rows[date("d",$now)]['records']) : 0;
	$hours = isset($summary_rows[date("d",$now)]) ? round(($summary_rows[date("d",$now)]['hours']),2) : "-";
	echo "
				<tr class='$class'>
					<td>".date("d M Y",$now)."</td>
					<td>".date("l",$now)."</td>
					<td class=center>".$records."</td>
					<td class=center>".$hours."</td>
					<td>".($allow_open?"<button class='btn_open $btn_class' go=".date("Y-m-d",$now).">Open</button>":"")."</td>
				</tr>
	";

}



$timeObj = new AIT_TIME();

//CHECK IF A PAGE FORWARD OR BACK HAS BEEN CALLED AS $_REQUEST[m]=YYYY-M
if(isset($_REQUEST['m'])) {
	$m = explode("-",$_REQUEST['m']);
	$current = mktime(12,0,0,$m[1],1,$m[0]);
	if($current!=strtotime(date("d F Y 12:0:0"))) {
		$allow_next = true;
	} else {
		$allow_next = false;
	}
} else {
	$current = strtotime(date("d F Y 12:0:0"));
	$allow_next = false;
}
$today = strtotime(date("d F Y")." 12:00:00");
$current_text = date("F Y",$current);
$current_click = date("Y-m",$current);
$prev = mktime(12,0,0,date("m",$current)-1,1,date("Y",$current));
$prev_text = date("F Y",$prev);
$prev_click = date("Y-m",$prev);
if($prev<mktime(12,0,0,3,1,2017)) {
	$can_go_prev_month = false;
} else {
	$can_go_prev_month = true;
}
$next = mktime(12,0,0,date("m",$current)+1,1,date("Y",$current));
$next_text = date("F Y",$next);
$next_click = date("Y-m",$next);

$summary_rows = $timeObj->getSummaryRecordsByMonthForBusiness($current);
$completed_days_records = $timeObj->getCompletedDatesByMonth($current);

//ASSIST_HELPER::arrPrint($_REQUEST);

?>
<table id=tbl_calendar class=tbl-container width=100%>
	<tr id=tr_move>
		<td width=25% class='left'><?php if($can_go_prev_month) { ?><button class=btn-move id=btn_prev go=<?php echo $prev_click; ?>><?php echo $prev_text; ?></button><?php } ?></td>
		<td width=50% class='center'><button class=btn-move id=btn_current go=<?php echo $current_click; ?>><?php echo $current_text; ?></button></td>
		<td width=20% class='right'>
			<?php if($allow_next) { ?>
				<button class=btn-move id=btn_next go=<?php echo $next_click; ?>><?php echo $next_text; ?></button>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td colspan=3>
			<h3>Calendar</h3>
			<?php
			$first_day_of_the_month = mktime(12,0,0,date("m",$current),1,date("Y",$current));
			$last_day_of_the_month = mktime(12,0,0,date("m",$current),date("t",$current),date("Y",$current));
			$first_day_of_the_week = date("w",$first_day_of_the_month);
			if($first_day_of_the_week==0) {
				$first_day_of_calendar = $first_day_of_the_month;
			} else {

			}
			?>
			<table id=tbl_calendar_days width=100%>
				<tr>
					<th width=14.3%>Sunday</th>
					<th width=14.3%>Monday</th>
					<th width=14.3%>Tuesday</th>
					<th width=14.3%>Wednesday</th>
					<th width=14.3%>Thursday</th>
					<th width=14.3%>Friday</th>
					<th width=14.2%>Saturday</th>
				</tr>
				<?php
				$start_date = $first_day_of_the_month;
				$last_date_has_been_reached = false;
				for($number_of_weeks=1;$number_of_weeks<=6;$number_of_weeks++) {
					if($last_date_has_been_reached) {
						break;
					}
					?>
					<tr style='height: 100px'><?php
						for($placeholder=0;$placeholder<$first_day_of_the_week;$placeholder++) {
							echo "<td class=no_hover>&nbsp;</td>";
						}
						$movement = 0;
						for($week_day=$first_day_of_the_week;$week_day<7;$week_day++) {
							$now = $start_date+($movement*24*60*60);
							if(date("m",$now)==date("m",$first_day_of_the_month)) {
								$class = " ";
								$btn_class = " ";
								if($now==$today) {
									$class.= "today ";
									$btn_class.= " today ";
								} elseif(date("N",$now)>5 || $timeObj->isPublicHoliday($now)) {
									$class.="weekend ";
									$btn_class.=" weekend ";
								}
								if($now<$today) {
									$btn_class = "past";
								}

								$allow_open = true;
								if($now>$today) {
									$allow_open = true;//false;
									$class.=" future";
									$btn_class = "";
								}

								$records = isset($summary_rows[date("j",$now)]) ? ($summary_rows[date("j",$now)]['records']) : 0;
								$hours = isset($summary_rows[date("j",$now)]) ? round(($summary_rows[date("j",$now)]['hours']),2) : "-";

								$date_class = "b";
								//REPLACED BY day_status ICON BELOW - JC 11 OCT 2017
								/*if($now<=$today && date("N",$now)<6 && $records==0) {
									$date_class.=" red";
								}*/


								$day_status = "";
								if(isset($completed_days_records[$now])) {
									$day_status = ASSIST_HELPER::drawStatus("Y");
								} elseif($records>0){
									$day_status = ASSIST_HELPER::drawStatus("pending");
								} else {
									$day_status = ASSIST_HELPER::drawStatus("warn");
								}


								echo "
									<td class='$class' my_date='".date("Y-m-d",$now)."'>
										<span style='float:left'>".$day_status."</span>&nbsp;
										<span class='".$date_class."'>".date("d M Y",$now)."</span>
										".($records>0?"<table class='calendar-details' id='tbl_".date("Y-m-d",$now)."'>
											<tr>
												<td class=i my_date='".date("Y-m-d",$now)."'>
													$hours hour".($hours!=1?"s":"")."<br />$records record".($records>1?"s":"")."
												</td>
												<td>
													<button id=btn_".date("Y-m-d",$now)." class='btn_open $btn_class' go=".date("Y-m-d",$now).">Open</button>
												</td>
											</tr>
										</table>":"<table class='calendar-details' id='tbl_".date("Y-m-d",$now)."'>
											<tr>
												<td>
													<button id=btn_".date("Y-m-d",$now)." class='btn_manage $btn_class' go=".date("Y-m-d",$now).">Manage</button>
												</td>
											</tr>
										</table>")."
									</td>";
								$movement++;
							} else {
								echo "<td class=no_hover>&nbsp;</td>";
							}
						}
						?>
					</tr>
					<?php
					$start_date = $start_date+($movement*24*60*60);
					if($start_date > $last_day_of_the_month) { $last_date_has_been_reached = true; }
					$first_day_of_the_week = 0;
				}
				?>
			</table>
<?php
ASSIST_HELPER::displayResult(array("info","The number of hours and records displayed in the calendar only reflects Business (non-Private) time captured.  Private records are available on each individual daily page."));
?>




		</td>
	</tr>
	<tr>
		<td colspan=3>
<?php





echo "
<table style='border-width:0px' width=100%>
	<tr>
		<td width=48%><h2>Time Analysis for the Month of ".date("F Y",$current)."</h2></td>
		<td width=4%></td>
		<td width=48%><h2>Time Analysis for the Financial Year</h2></td>
	</tr>














	<tr>
		<td width=48%>";
		$analysis_period = "MONTH";
		$billable = $timeObj->getAnalysis("TOTAL",$current,$analysis_period);
usort($billable,"array_sort_by_client");
echo "<h3>Total Hours per Client</h3>";
if(count($billable)==0) {
	echo "<p>No time recorded for this ".$analysis_period.".</p>";
} else {
	$time_processed = array();
	$time_total = array();
	foreach($billable as $record) {
		if(!isset($time_processed[$record['type']][$record['client']])) {
			$time_processed[$record['type']][$record['client']] = $record['time_minutes'];
		} else {
			$time_processed[$record['type']][$record['client']]+= $record['time_minutes'];
		}
		if(!isset($time_total[$record['type']])) {
			$time_total[$record['type']] = 0;
		}
		$time_total[$record['type']]+=$record['time_minutes'];
	}


	echo "
	<table>
	<tr>
		<th colspan=2></th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($time_processed as $type=>$time_client) {
		echo "<tr class=b style='background-color: #ccccff'><td colspan=2 >$type</td>
		<td class=center>".$time_total[$type]."</td>
		<td class=center>".number_format($time_total[$type]/60,2)."</td></tr>";
		foreach($time_client as $client => $minutes) {
			$total_min+=$minutes;
			echo "
			<tr class=tr_hover>
				<td class='b right' width=20px>-</td>
				<td>".$client."</td>
				<td class=center>".$minutes."</td>
				<td class=center>".number_format(($minutes/60),2)."</td>
			</tr>";
		}
	}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right' colspan=2>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";

}		echo "
		</td>
		<td width=4%></td>
		<td width=48%>";
		$analysis_period = "YEAR";
$billable = $timeObj->getAnalysis("TOTAL",$current,$analysis_period);
usort($billable,"array_sort_by_client");

echo "<h3>Total Hours per Client</h3>";
if(count($billable)==0) {
	echo "<p>No time recorded for this ".$analysis_period.".</p>";
} else {
	$time_processed = array();
	$time_total = array();
	foreach($billable as $record) {
		if(!isset($time_processed[$record['type']][$record['client']])) {
			$time_processed[$record['type']][$record['client']] = $record['time_minutes'];
		} else {
			$time_processed[$record['type']][$record['client']]+= $record['time_minutes'];
		}
		if(!isset($time_total[$record['type']])) {
			$time_total[$record['type']] = 0;
		}
		$time_total[$record['type']]+=$record['time_minutes'];
	}


	echo "
	<table>
	<tr>
		<th colspan=2></th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($time_processed as $type=>$time_client) {
		echo "<tr class=b style='background-color: #ccccff'><td colspan=2 >$type</td>
		<td class=center>".$time_total[$type]."</td>
		<td class=center>".number_format($time_total[$type]/60,2)."</td></tr>";
		foreach($time_client as $client => $minutes) {
			$total_min+=$minutes;
			echo "
			<tr class=tr_hover>
				<td class='b right' width=20px>-</td>
				<td>".$client."</td>
				<td class=center>".$minutes."</td>
				<td class=center>".number_format(($minutes/60),2)."</td>
			</tr>";
		}
	}
	echo "
	<tr style='background-color: #efefef'>
		<td class='b right' colspan=2>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";

}
		echo "
		</td>
	</tr>

















	<tr>
		<td width=48%>";
		$analysis_period = "MONTH";
		$billable = $timeObj->getAnalysis("BILLABLE",$current,$analysis_period);

echo "<h3>Billable Hours per Client / Project / Activity</h3>";
if(count($billable)==0) {
	echo "<p>No billable hours recorded for this ".$analysis_period.".</p>";
} else {
	$billable_processed = array();
	$billable_client_total = array();
	foreach($billable as $record) {
		if(!isset($billable_processed[$record['client']][$record['project']][$record['activity']])) {
			$billable_processed[$record['client']][$record['project']][$record['activity']] = $record['time_minutes'];
		} else {
			$billable_processed[$record['client']][$record['project']][$record['activity']]+= $record['time_minutes'];
		}
		if(!isset($billable_client_total[$record['client']])) {
			$billable_client_total[$record['client']] = 0;
		}
		$billable_client_total[$record['client']]+=$record['time_minutes'];
	}






	echo "
	<table>
	<tr>
		<th colspan=3></th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($billable_processed as $client=>$billable_projects) {
		echo "<tr class=b style='background-color: #ccccff'><td colspan=3 >$client</td>
		<td class=center>".$billable_client_total[$client]."</td>
		<td class=center>".number_format($billable_client_total[$client]/60,2)."</td></tr>";
		foreach($billable_projects as $project=>$billable_activities) {
			echo "<tr class=i style='background-color: #efefff'><td class=right>+</td><td colspan=2 >$project</td>
			<td class=center>".array_sum($billable_activities)."</td>
			<td class=center>".number_format(array_sum($billable_activities)/60,2)."</td></tr>";
			foreach($billable_activities as $activity => $minutes) {

		$total_min+=$minutes;
		echo "
		<tr class=tr_hover>
			<td class=b width=20px></td>
			<td class='b right' width=20px>-</td>
			<td>".$activity."</td>
			<td class=center>".$minutes."</td>
			<td class=center>".number_format(($minutes/60),2)."</td>
		</tr>";
	}}}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right' colspan=3>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";

}		echo "
		</td>
		<td width=4%></td>
		<td width=48%>";
		$analysis_period = "YEAR";
$billable = $timeObj->getAnalysis("BILLABLE",$current,$analysis_period);
//ASSIST_HELPER::arrPrint($billable);

echo "<h3>Billable Hours per Client / Project / Activity</h3>";
if(count($billable)==0) {
	echo "<p>No billable hours recorded for this ".strtolower($analysis_period).".</p>";
} else {
	$billable_processed = array();
	$billable_client_total = array();
	foreach($billable as $record) {
		if(!isset($billable_processed[$record['client']][$record['project']][$record['activity']])) {
			$billable_processed[$record['client']][$record['project']][$record['activity']] = $record['time_minutes'];
		} else {
			$billable_processed[$record['client']][$record['project']][$record['activity']]+= $record['time_minutes'];
		}
		if(!isset($billable_client_total[$record['client']])) {
			$billable_client_total[$record['client']] = 0;
		}
		$billable_client_total[$record['client']]+=$record['time_minutes'];
	}

	echo "
	<table>
	<tr>
		<th colspan=3></th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	ksort($billable_processed);
	foreach($billable_processed as $client=>$billable_projects) {
		echo "<tr class=b style='background-color: #ccccff'><td colspan=3 >$client</td>
		<td class=center>".$billable_client_total[$client]."</td>
		<td class=center>".number_format($billable_client_total[$client]/60,2)."</td></tr>";
		ksort($billable_projects);
		foreach($billable_projects as $project=>$billable_activities) {
			echo "<tr class=i style='background-color: #efefff'><td class=right>+</td><td colspan=2 >$project</td>
			<td class=center>".array_sum($billable_activities)."</td>
			<td class=center>".number_format(array_sum($billable_activities)/60,2)."</td></tr>";
			ksort($billable_activities);
			foreach($billable_activities as $activity => $minutes) {

		$total_min+=$minutes;
		echo "
		<tr class=tr_hover>
			<td class=b width=20px></td>
			<td class='b right' width=20px>-</td>
			<td>".$activity."</td>
			<td class=center>".$minutes."</td>
			<td class=center>".number_format(($minutes/60),2)."</td>
		</tr>";
	}}}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right' colspan=3>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";

}
		echo "
		</td>
	</tr>


	<tr>
		<td width=48%>";
		$analysis_period = "MONTH";
$development = $timeObj->getAnalysis("DEVELOPMENT",$current,$analysis_period);
//ksort($development);
usort($development,"array_sort_by_activity");
echo "<h3>Product Development Hours</h3>";
if(count($development)==0) {
	echo "<p>No development hours recorded for this ".strtolower($analysis_period).".</p>";
} else {
	echo "
	<table>
	<tr>
		<th>Activity</th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($development as $record) {
		$total_min+=$record['time_minutes'];
		echo "
		<tr class=tr_hover>
			<td class=b>".$record['activity']."</td>
			<td class=center>".$record['time_minutes']."</td>
			<td class=center>".number_format(($record['time_minutes']/60),2)."</td>
		</tr>";
	}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right'>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";
}
		echo"
		</td>
		<td width=4%></td>
		<td width=48%>";
		$analysis_period = "YEAR";
$development = $timeObj->getAnalysis("DEVELOPMENT",$current,$analysis_period);
usort($development,"array_sort_by_activity");
echo "<h3>Product Development Hours</h3>";
if(count($development)==0) {
	echo "<p>No development hours recorded for this ".strtolower($analysis_period).".</p>";
} else {
	echo "
	<table>
	<tr>
		<th>Activity</th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($development as $record) {
		$total_min+=$record['time_minutes'];
		echo "
		<tr class=tr_hover>
			<td class=b>".$record['activity']."</td>
			<td class=center>".$record['time_minutes']."</td>
			<td class=center>".number_format(($record['time_minutes']/60),2)."</td>
		</tr>";
	}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right'>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";
}
		echo"
		</td>
	</tr>

	<tr>
		<td width=48%>";
		$analysis_period = "MONTH";
$support = $timeObj->getAnalysis("SUPPORT",$current,$analysis_period);
usort($support,"array_sort_by_client");
echo "<h3>Client Support Hours</h3>";
if(count($support)==0) {
	echo "<p>No support hours recorded for this ".strtolower($analysis_period).".</p>";
} else {
	echo "
	<table>
	<tr>
		<th>Client</th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($support as $record) {
		$total_min+=$record['time_minutes'];
		echo "
		<tr class=tr_hover>
			<td class=b>".$record['client']."</td>
			<td class=center>".$record['time_minutes']."</td>
			<td class=center>".number_format(($record['time_minutes']/60),2)."</td>
		</tr>";
	}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right'>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";
}
		echo "</td>
		<td width=4%></td>
		<td width=48%>";
		$analysis_period = "YEAR";
$support = $timeObj->getAnalysis("SUPPORT",$current,$analysis_period);
usort($support,"array_sort_by_client");
echo "<h3>Client Support Hours</h3>";
if(count($support)==0) {
	echo "<p>No support hours recorded for this ".strtolower($analysis_period).".</p>";
} else {
	echo "
	<table>
	<tr>
		<th>Client</th>
		<th>Minutes</th>
		<th>Hours</th>
	</tr>";
	$total_min = 0;
	foreach($support as $record) {
		$total_min+=$record['time_minutes'];
		echo "
		<tr class=tr_hover>
			<td class=b>".$record['client']."</td>
			<td class=center>".$record['time_minutes']."</td>
			<td class=center>".number_format(($record['time_minutes']/60),2)."</td>
		</tr>";
	}

	echo "
	<tr style='background-color: #efefef'>
		<td class='b right'>Total:</td>
		<td class='b center'>".$total_min."</td>
		<td class='b center'>".number_format(round($total_min/60,2),2)."</td>
	</tr>
	</table>
	";
}
		echo "</td>
	</tr>

</table>





";




?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php echo $js; ?>
	var my_hovered_classes = "";
	$("table.tbl-calendar tr").hover(function() {
		//my_hovered_classes = $(this).attr("class");
		//$(this).attr("class","hover");
		$(this).addClass("hover");
	},function() {
		//$(this).attr("class",my_hovered_classes);
		$(this).removeClass("hover");
	});
	$("table.tbl-calendar td").addClass("middle");
	$("#tr_move").css("border","1px solid #000099");


	$("#btn_prev").button({
		icons:{primary:"ui-icon-arrowthick-1-w"}
	}).removeClass("ui-state-default").addClass("ui-state-error");

	$("#btn_current").button({
		icons:{primary:"ui-icon-arrowthick-1-s"}
	}).removeClass("ui-state-default").addClass("ui-state-ok");

	$("#btn_next").button({
		icons:{secondary:"ui-icon-arrowthick-1-e"}
	}).removeClass("ui-state-default").addClass("ui-state-info");

	$("button.btn-move")
	.click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var m = $(this).attr("go");
		document.location.href = "manage_time.php?m="+m;
	}).css({"background":"url()","font-weight":"bold","border-width":"0px"});

	$("button.btn_open, button.btn_manage").button({
		icons:{primary:"ui-icon-pencil"},
	}).click(function(e) {
		e.preventDefault();
		//AssistHelper.processing();
		var m = $(this).attr("go");
		goToNextPage(m);
		//document.location.href = "manage_time_object.php?m="+m+"&src="+AssistString.base64_encode(document.location.href);
	}).css({"background":"url()"});
	$("button.btn_open").button("option","text",false);

	$("button.today").removeClass("ui-state-default").addClass("ui-state-green")
		.hover(function() {
			$(this).removeClass("ui-state-green").addClass("ui-state-info");
		},function() {
			$(this).removeClass("ui-state-info").addClass("ui-state-green");
		});
	$("button.past").removeClass("ui-state-default").addClass("ui-state-black");




	$("#tbl_calendar_days td").css({"border":"1px solid #efefef","padding":"10px"})
		.hover(function() {
			if(!$(this).hasClass("no_hover")) {
				$(this).addClass("td_hover").find("td").addClass("td_hover");
			}
		},function(){
			$(this).removeClass("td_hover").find("td").removeClass("td_hover")
		});
	$("table.calendar-details").css({"border":"0px solid #ffffff","margin":"10px auto"})
		.find("td").css("border","0px solid #ffffff");
});
function goToNextPage(m) {
	AssistHelper.processing();
	var url = "manage_time_object.php?m="+m+"&src="+AssistString.base64_encode(document.location.href);

	//console.log(url);
	//AssistHelper.closeProcessing();
	document.location.href = url;
}
</script>




