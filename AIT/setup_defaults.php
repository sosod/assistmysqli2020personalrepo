<?php


require_once("inc_header.php");


$list_names = $headingObject->getStandardListHeadings();
//ASSIST_HELPER::arrPrint($list_names);
?>
<style type="text/css">
	#div_left, #div_right {
		padding: 0px 20px 20px 20px;
		width: 45%;
		height: 100%;
	}
	#div_container {
		padding: 0px 10px 0px 10px;
	}
</style>
<div id=div_container>
	<div id=div_right>
	<h2>Lists</h2>
	<table class="form middle" >
		<?php
			foreach($list_names as $key => $val){
				$firsthalf = explode("_", $key);
				if($firsthalf[1] == 'status'){
					echo "	<tr>
	        					<th width=200px>" . $val . ":</th>
	        					<td>Configure the $val list items for use in the " . $helper->getObjectName($firsthalf[0]) . " section.<span class=float><input type=button value=Configure class=btn_list id=" . $key . " /></span></td>
	    					</tr>";
					}else{
					if($key=="contract_supplier") {
						$key2 = "contract_supplier_category";
						$val2 = "Supplier Category";
						echo "	<tr>
		        					<th width=200px>" . $val2 . ":</th>
		        					<td>Configure the " . $val2 . " list items.<span class=float><input type=button value=Configure class=btn_list id=" . $key2 . " /></span></td>
		    					</tr>";
					}
					echo "	<tr>
	        					<th width=200px>" . $val . ":</th>
	        					<td>Configure the " . $val . " list items.<span class=float><input type=button value=Configure class=btn_list id=" . $key . " /></span></td>
	    					</tr>";
				}
			}
		?>
	
	</table>
	</div>
	<div id=div_left>
	<h2>Module Settings</h2>
	<table class="form middle" >
	    <tr> 
	        <th width=200px>Preferences:</th>
	        <td>Customise the module settings.<span class=float><input type=button value=Configure class=btn_setup id=preferences /></span></td>
	    </tr>
	    <tr> 
	        <th width=200px>Naming:</th>
	        <td>Configure the naming convention for the various objects and activities.<span class=float><input type=button value=Configure class=btn_setup id=names /></span></td>
	    </tr>
	    <tr>
	        <th width=200px>Menus:</th>
	        <td>Configure the naming convention for the menus.<span class=float><input type=button value=Configure class=btn_setup id=menu /></span></td>
	    </tr>
	    <tr>
	        <th>Headings:</th>
	        <td>Configure the naming convention for the field headings.<span class=float><input type=button value=Configure class=btn_setup id=headings /></span></td>
	    </tr>
	    <tr>
	        <th>List Columns:</th>
	        <td>Configure the columns to be displayed on the list pages.<span class=float><input type=button value=Configure class=btn_setup id=columns /></span></td>
	    </tr>
	    <tr>
	    	<th><?php echo $headingObject->getAHeadingNameByField("contract_owner_id"); ?>:</th>
	        <td>Configure the administrators to be assigned to the various <?php echo $helper->getTerm("SUB",true); ?>.<span class=float><input type=button value=Configure class=btn_setup id=admins /></span></td>
	    </tr>
	    <tr> 
	        <th width=200px>Notifications:</th>
	        <td>Customise the module notifications.<span class=float><input type=button value=Configure class=btn_setup id=notifications /></span></td>
	    </tr>
<!--	    <tr>
	        <th><?php echo $menuObject->getAMenuNameByField("glossary"); ?>:</th>
	        <td>Configure the <?php echo $menuObject->getAMenuNameByField("glossary"); ?>.<span class=float><input type=button value=Configure id=glossary class=btn_setup /></span></td>
	</tr> -->
	</table>
	<!-- <h2>Module Reports</h2>
	<table class="form middle" >
	    <tr>
	        <th width=200px>User Access:</th>
	        <td>View a summary of all User Access settings.<span class=float><input type=button value=Configure class=btn_report id=report_useraccess /></span></td>
	    </tr> 
	    <tr>
	        <th width=200px>Data Storage:</th>
	        <td>View a report of the Data Usage by the module.<span class=float><input type=button value=Configure class=btn_report id=report_data /></span></td>
	    </tr>
	</table> -->
	
	</div>
</div> <!-- end div container -->
<script type=text/javascript>
$(function() {
	$("h2").css("margin-top","15px");

    $("input:button").button().css("font-size","75%");
    $("input:button.btn_setup").click(function() {
        document.location.href = "setup_defaults_"+$(this).prop("id")+".php";
    });
    $("input:button.btn_list").click(function() {
        document.location.href = "setup_defaults_lists.php?l="+$(this).prop("id");
    });
    $("input:button.btn_report").click(function() {
        document.location.href = "setup_defaults_"+$(this).prop("id")+".php";
    });
    $("#div_right").css("float","right");
    $("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
    $("table.form").css({"width":"100%"});
    $(window).resize(function() {
    	var x = 0;
    	$("#div_right").children().each(function() {
    		x+=AssistString.substr($(this).css("height"),0,-2)*1;
    	});
    	$("#div_container").css("height",(x+50)+"px");
    });
	$(window).trigger("resize");
});
</script>


	<!--
    <tr>
        <th width=200px><|Contract Types|></th>
        <td>Configure the <|Contract Type|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_type /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Categories|></th>
        <td>Configure the <|Contract Category|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_category /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Statuses|></th>
        <td>Configure the <|Contract Status|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_status /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Suppliers|></th>
        <td>Configure the <|Contract Supplier|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_supplier /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Supplier Categories|></th>
        <td>Configure the <|Contract Supplier Category|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_supplier_category /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Payment Terms|></th>
        <td>Configure the <|Contract Payment Term|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_payment_term /></span></td>
    </tr>
    <tr>
        <th width=200px><|Contract Payment Frequencies|></th>
        <td>Configure the <|Contract Payment Frequency|> list items.<span class=float><input type=button value=Configure class=btn_list id=contract_payment_frequency /></span></td>
    </tr>
    <tr>
        <th width=200px><|Deliverable Reporting Categories|></th>
        <td>Configure the <|Deliverable Reporting Category|> list items.<span class=float><input type=button value=Configure class=btn_list id=deliverable_reporting_category /></span></td>
    </tr>
    <tr>
        <th width=200px><|Deliverable Quantity Weights|></th>
        <td>Configure the <|Deliverable Quantity Weight|> list items.<span class=float><input type=button value=Configure class=btn_list id=deliverable_quantity_weight /></span></td>
    </tr>
    <tr>
        <th width=200px><|Deliverable Quality Weights|></th>
        <td>Configure the <|Deliverable Quality Weight|> list items.<span class=float><input type=button value=Configure class=btn_list id=deliverable_quality_weight /></span></td>
    </tr>
    <tr>
        <th width=200px><|Deliverable Other Weights|></th>
        <td>Configure the <|Deliverable Other Weight|> list items.<span class=float><input type=button value=Configure class=btn_list id=deliverable_other_weight /></span></td>
    </tr>
    <tr>
        <th width=200px><|Deliverable Score Matrices|></th>
        <td>Configure the <|Deliverable Score Matrix|> list items.<span class=float><input type=button value=Configure class=btn_list id=delivered_score_matrix /></span></td>
    </tr>
    <tr>
        <th width=200px><|Quality Score Matrices|></th>
        <td>Configure the <|Quality Score Matrix|> list items.<span class=float><input type=button value=Configure class=btn_list id=quality_score_matrix /></span></td>
    </tr>
    <tr>
        <th width=200px><|Other Score Matrices|></th>
        <td>Configure the <|Other Score Matrix|> list items.<span class=float><input type=button value=Configure class=btn_list id=other_score_matrix /></span></td>
    </tr>
    <tr>
        <th width=200px><|Action Statuses|></th>
        <td>Configure the <|Action Status|> list items.<span class=float><input type=button value=Configure class=btn_list id=action_status /></span></td>
    </tr>
    <tr>
        <th width=200px><|Authorisation Statuses|></th>
        <td>Configure the <|Authorisation Status|> list items.<span class=float><input type=button value=Configure class=btn_list id=authorisation_status /></span></td>
    </tr>
   -->