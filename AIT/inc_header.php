<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME
 * ******************
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>'.$s.' => '.$total_time.' seconds.';

}
*/

//error_reporting(-1);

require_once ("../module/autoloader.php");
//marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/",$self);
$self = explode(".",end($self));
$page = $self[0];
if(isset($my_page)) {
	$page.="_".$my_page;
}
$navigation_path = explode("_",$page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
    //'setup'				=> "setup_defaults"
    //'report_generate'	=> "report_generate_contract",
$redirect = array(
    'report'			=> "report_generate",
);
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}
//marktime("redirect");
/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new AIT_MENU();
$menu = $menuObject->getPageMenu($page);

//ASSIST_HELPER::arrPrint($menu);
 /*
if($navigation_path[0]=="admin" && $navigation_path[1]=="update" && $menu['buttons'][1][0]['link']!="admin_update.php") {
	header("Location:".$menu['buttons'][1][0]['link']);
} elseif($navigation_path[0]=="new" && $navigation_path[1]=="contract" && $menu['buttons'][1][0]['link']!="new_contract.php") {
	header("Location:".$menu['buttons'][1][0]['link']);
}
 //marktime("menu");
  */
/***********
 * Start general header
 */

$headingObject = new AIT_HEADINGS();
$displayObject = new AIT_DISPLAY();
$nameObject = new AIT_NAMES();
$helper = new AIT();

$s = array("/library/js/assist.ui.paging.js","/library/jquery-plugins/jscolor/jscolor.js");
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

//$helper->arrPrint($_REQUEST);


$js = ""; //to catch any js put out by the various display classes

//marktime("general");

/*********
 * Module-wide Javascript
 */
?>

<style type=text/css>
/* Order of classes are important - hover must be last! */
	tr.weekend td, td.weekend {
		background-color: #eaeaff;
		color: #000000;
	}
	tr.future td, td.future {
		font-style: italic;
		color: #777777;
	}
	tr.today td, td.today {
		color: #009900;
		background-color: #ccffcc;
	}
	tr.hover td {
		background-color: #000000;
		color: #ffffff;
	}
	td.td_hover {
		background-color: #fff5e6;
		border: 1px solid #fe9900;
		color: #fe9900;
	}
	tr.tr-total td, td.td-total {
		font-weight: bold;
		background-color: #dddddd;
	}
	tr.tr-total td.grand-total {
		font-weight: bold;
		background-color: #999999;
		text-decoration: underline;
	}
</style>
<script type="text/javascript" >
$(function() {
	$("table.tbl-container").addClass("noborder");
	$("table.noborder").find("td").addClass("noborder");

	$(".click_me").css("cursor","pointer");
});
</script>

<?php
//marktime("module wide");
/**********
 * Navigation buttons
 */
if(!isset($no_page_heading) || !$no_page_heading) {
	$active_button_label = $menuObject->drawPageTop($menu,(isset($display_navigation_buttons) ? $display_navigation_buttons : true),isset($page_title)?$page_title:"");
}
 /*
$active_button_label = "";
if(!isset($display_navigation_buttons) || $display_navigation_buttons!==false) {
	foreach($menu['buttons'] as $level => $buttons) {
		//$helper->arrPrint($buttons);
	    echo $helper->generateNavigationButtons($buttons, $level);
		if($level==1) {
			foreach($buttons as $b) {
				if($b['active']==true) {
					$active_button_label = $b['name'];
					break;
				}
			}
		}
	}
}
//marktime("navigation buttons");
/**********
 * Breadcrumbs
 */ /*
$breadcrumbs = array();
    foreach($menu['breadcrumbs'] as $link=>$text) {
    	if(substr_count($link, ".php")>0) {
        	$breadcrumbs[] = "<a href='".$link."' class=breadcrumb>".$text."</a>";
		} else {
        	$breadcrumbs[] = "".$text."";
		}
    }
echo "<h1>".implode(" >> ",$breadcrumbs)."</h1>";
  */
//marktime("end header");
?>

<!------------Hint Box ------------>
<!-- <div id="hint_box" style="padding:8px; width:170px; box-shadow: 2px 2px 5px #333333; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div> -->
