<?php
$page_title = "Time";
include("inc_header.php");


$now = strtotime(date("d F Y")." 12:00:01");

$timeObj = new AIT_TIME();

$m = explode("-",$_REQUEST['m']);
$current = mktime(12,0,0,$m[1],$m[2],$m[0]);
$next_day = $current + (60*60*24);
$prev_day = $current - (60*60*24);

		$can_i_add_records = true;
		$can_i_edit_records = true;

if($prev_day<mktime(12,0,0,3,1,2017)) {
	$can_go_prev_day = false;
} else {
	$can_go_prev_day = true;
}

if(!in_array($timeObj->getUserID(),array("0001","0002","0018","0019","0023","0016"))) {
	if($current > $now) {
		//can't add future dated records
		$can_i_add_records = false;
	} elseif((($now-$current)/(60*60*24))>8) {
		//if selected date is older than 1 week from today then check if selected date falls before the 1 Oct launch date
		//TO BE REMOVED AT YEAR END - ONLY ALLOWED FOR CAPTURING OF HISTORICAL DATA!
		if(!($current<mktime(12,0,0,10,1,2017))) {
			$can_i_add_records = false;
			$can_i_edit_records = false;
		}
	}
}
$go_back_src = base64_decode($_REQUEST['src']);

$stats = array();
$headings = $headingObject->getMainObjectHeadings("TIME");
$daily_records = $timeObj->getMyDetailedRecords($current);
$is_date_complete = $timeObj->isDateComplete($current);

//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($headings['rows']);

unset($headings['rows']['time_date']);

?>
<table class=tbl-container><tr><td>
	<?php if($can_i_add_records) { ?>
	<span class=float ><select id=sel_add><?php for($i=1;$i<=10;$i++) { echo "<option ".($i>1?"":"selected")." value=$i>$i Record".($i>1?"s":"")."</option>"; } ?></select>&nbsp;<button id=btn_add>Add</button></span>
	<?php } ?>
	<h2>Daily Records for <?php echo date("d F Y",$current)." (".date("l",$current).")"; ?>
	</h2>
	<table id=tbl_time>
		<tr>
			<?php
			foreach($headings['rows'] as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
			?>
		<th></th></tr>
		<?php

		if(count($daily_records)==0) {
			echo "<tr><td colspan=".(count($headings['rows'])+1).">No records for today</td></tr>";
		} else {
			$total_minutes = array('total'=>0,'business'=>0,'private'=>0);
			foreach($daily_records as $row) {
				$total_minutes['total']+=$row['time_minutes'];
				if($row['time_private']==1) {
					$total_minutes['private']+=$row['time_minutes'];
				} else {
					$total_minutes['business']+=$row['time_minutes'];
				}
				echo "<tr>";
				foreach($headings['rows'] as $fld => $head) {
					$head_type = $head['type'];
					switch($head_type) {
						case "REF":
							$row['time_ref'] = $row[$fld];
							$row[$fld] = $timeObj->getRefTag().$row[$fld];
							break;
						case "TIME":
							$row[$fld] = date("H:i",strtotime($row[$fld]));
							break;
						case "BOOL":
							$v = $displayObject->getDataField($head_type,$row[$fld]);
							$js.=$v['js'];
							$row[$fld] = $v['display'];
							break;
						default:
					}
					echo "<td>".$row[$fld]."</td>";

				}
				if($can_i_edit_records) {
					echo "<td><button class=btn_edit time_id=".$row['time_ref'].">Edit</button></td></tr>";
				} else {
					echo "<td></td>";
				}
				//stats
				if(!isset($stats[$row['time_reseller']])) {
					$stats[$row['time_reseller']]=array();
				}
				if(!isset($stats[$row['time_reseller']][$row['time_client']])) {
					$stats[$row['time_reseller']][$row['time_client']]=array();
				}

			}
			echo "<tr><td colspan=".(count($headings['rows'])+1).">
			<table width=100% style='border-width: 0px'>
				<tr>
					<td class=b width=7%>Total:</td>
					<td width=31% class='i center'>Business: ".$total_minutes['business']." minutes (".(round(($total_minutes['business']/60),2))." hours)</td>
					<td width=31% class='i center'>Private: ".$total_minutes['private']." minutes (".(round(($total_minutes['private']/60),2))." hours)</td>
					<td width=31% class='b center'>Total: ".$total_minutes['total']." minutes (".(round(($total_minutes['total']/60),2))." hours)</td>
				</tr>
			</table>
			</td></tr>";
		}

		?>
	</table>
</td></tr>
<tr><td>
	<?php if($is_date_complete) { ?>
		<span class=float><button id=btn_undo_complete>Undo Day Complete</button></span>
	<?php } else { ?>
		<span class=float><button id=btn_complete>Mark Day Complete</button></span>
	<?php } ?>
<span ><?php if($can_go_prev_day) { ?><button id=btn_prev><?php echo date("d M Y", $prev_day); ?></button>&nbsp;<?php } ?><button id=btn_calendar>Calendar</button>&nbsp;<button id=btn_next><?php echo date("d M Y", $next_day); ?></button></span>
</td></tr>
</table>


<script type=text/javascript>
$(function() {
	var src = "<?php echo base64_encode("manage_time_object.php?m=".$_REQUEST['m']."&src=".$_REQUEST['src']); ?>";
	<?php echo $js; ?>
	$("#tbl_time tr").find("td:lt(4)").addClass("center");
	$("#tbl_time tr").find("td:gt(9)").addClass("center");
	$("button.btn_edit").button({
		icons:{primary:"ui-icon-pencil"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr("time_id");
		var url = "manage_time_form.php?src="+src+"&m=<?php echo $_REQUEST['m']; ?>&action=EDIT&time_id="+i;
		document.location.href = url;
		//AssistHelper.finishedProcessing("info",url);
	});
	$("button#btn_add").button({
		icons:{primary:"ui-icon-plus"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $("#sel_add").val();
		document.location.href = "manage_time_form.php?src="+src+"&m=<?php echo $_REQUEST['m']; ?>&i="+i
	}).removeClass("ui-state-default").addClass("ui-state-ok");


	var calendar_url = "manage_time.php?m=<?php echo date("Y-m",$current); ?>";

	$("button#btn_calendar").button({
		icons:{primary:"ui-icon-calendar"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		document.location.href = calendar_url;
	});
	$("button#btn_prev").button({
		icons:{primary:"ui-icon-arrowthick-1-w"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var url = "manage_time_object.php?src="+AssistString.base64_encode(calendar_url)+"&m=<?php echo date("Y-m-d",$prev_day); ?>";
		document.location.href = url;
	});
	$("button#btn_next").button({
		icons:{secondary:"ui-icon-arrowthick-1-e"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var url = "manage_time_object.php?src="+AssistString.base64_encode(calendar_url)+"&m=<?php echo date("Y-m-d",$next_day); ?>";
		document.location.href = url;
	});

	$("button#btn_complete").button({
		icons:{primary:"ui-icon-check"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var dta = "date_to_mark=<?php echo date("Y-m-d",$current); ?>";
		var result = AssistHelper.doAjax("inc_controller.php?action=TIME.MARKDATECOMPLETE",dta);
		if(result[0]=="ok") {
			AssistHelper.finishedProcessingWithRedirect(result[0],result[1],calendar_url);
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
		//var url = "manage_time_object.php?src="+AssistString.base64_encode(calendar_url)+"&m=<?php echo date("Y-m-d",$next_day); ?>";
		//document.location.href = url;
	});

	$("button#btn_undo_complete").button({
		icons:{primary:"ui-icon-closethick"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var dta = "date_to_mark=<?php echo date("Y-m-d",$current); ?>";
		var result = AssistHelper.doAjax("inc_controller.php?action=TIME.UNDODATECOMPLETE",dta);
		if(result[0]=="ok") {
			AssistHelper.finishedProcessingWithRedirect(result[0],result[1],calendar_url);
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
		//var url = "manage_time_object.php?src="+AssistString.base64_encode(calendar_url)+"&m=<?php echo date("Y-m-d",$next_day); ?>";
		//document.location.href = url;
	});


});
</script>
<style type="text/css">
	.ui-state-default {
		background: url();
	}
</style>