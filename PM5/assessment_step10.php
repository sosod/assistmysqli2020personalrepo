<?php
$no_print_breadcrumbs = isset($no_print_breadcrumbs) ? $no_print_breadcrumbs : false;
require_once("inc_header.php");
$create_step = 10;
//$create_type = "PROJ";
//$create_name = "Project";
$obj_id = $_REQUEST['obj_id'];
$scdObj = new PM5_SCORECARD();

$sources = $scdObj->getKPASources();
$line_weight_precision_allowed = $scdObj->getLineWeightPrecisionAllowed();
$weight_blank_zero_echo = "";
for($i=0;$i<$line_weight_precision_allowed;$i++){
	$weight_blank_zero_echo.="0";
}

$primary_source = $scdObj->getPrimaryKPASource();
$source_modlocation = $scdObj->getPrimarySourceModLoc();
$pa = $scdObj->getAObjectSummary($obj_id);
$kpas = $scdObj->getKPAs();
$types = $scdObj->getObjectTypes(); //array("KPI","TOP","PROJ");
$names = $scdObj->getObjectNames(); //array('KPI'=>"Operational Indicators",'TOP'=>"Strategic Indicators",'PROJ'=>"Capital Indicators");

$class_name = $source_modlocation."_PMS";
$sdbp5Obj = new $class_name();
$head = $sdbp5Obj->getGenericHeadings($scdObj->getPrimaryKPASource(),true,false,SDBP6_DEPTKPI::OBJECT_TYPE);


$page_action = !isset($page_action) ? "create" : $page_action;

echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id'],$page_action);

?>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$lineObj = new PM5_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id']);
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);

$error = false;
$total_line_count=0;

$fld_prefix = $sdbp5Obj->getFieldPrefix($types);


$display_kpa = false;
foreach($kpas as $k => $a) {
	foreach($types as $kt) {
		if(isset($lines[$k][$kt]) && count($lines[$k][$kt]) > 0) {
			$display_kpa = true;
		}
	}
}
if($display_kpa) {
	?>
	<h3>Indicators</h3>
	<table id=tbl_kpa class=list>
		<thead>
		<tr>
			<?php
			foreach($head['main'] as $fld => $name) {
				echo "<th>".$name."</th>";
			}
			?>
			<th>Indicator<br/>Weight</th>
			<th>Overall<br/>Weight</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$grand_tot = 0;

		foreach($kpas as $k => $a) {
			foreach($types as $kt) {
				$fp = $fld_prefix[$kt];
				$tot = 0;
				if(isset($lines[$k][$kt]) && count($lines[$k][$kt]) > 0) {
					$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
					//echo "<h3>".$a."</h3>";
					echo "<tr class=subth><td colspan=".(count($head['main']) + 2).">".$a." - ".$names[$kt]."</td></tr>";
					//ASSIST_HELPER::arrPrint($lines[$k]);
					$objects = $lines[$k][$kt];
					foreach($objects as $i => $obj) {
						$total_line_count++;
						echo "
			<tr  class=\"".($obj['active'] == 1 ? "" : "inactive")."\">";
						foreach($head['main'] as $generic_fld=>$name) {
							if($generic_fld!="sys_ref") {
								if(isset($obj[$fp."_".$generic_fld])) {
									$fld = $fp."_".$generic_fld;
								} else {
									$fld = $generic_fld;
								}
							} else {
								$fld = "ref";
							}
							echo "<td>".$obj[$fld].($fld == "ref" && $obj['active'] != 1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : "")."</td>";
						}
						$w = (isset($weights[$i]) ? $weights[$i] : -1);
						//if weight is a negative then assume that KPA weighting step not done.
						if($w < 0) {
							$error = true;
						}
						echo "
				<td id=td_".$k.$kt."_".$i." class=right>".number_format(round(($w / $kw) * 100, $line_weight_precision_allowed),$line_weight_precision_allowed)."%</td>
				<td class=right><div style='width:60px' class='float'>".number_format($w,$line_weight_precision_allowed)."%</div></td>
			</tr>";
						$tot += (isset($weights[$i]) ? $weights[$i] : 1);
					}
					echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>100.".$weight_blank_zero_echo."%</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".number_format($tot,$line_weight_precision_allowed)."%</th>
		</tr>";
				}
				$grand_tot += $tot;
			}
		}
		?>
		</tbody>
		<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=<?php echo(count($head['main'])); ?>>Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot><?php echo number_format($grand_tot,$line_weight_precision_allowed); ?>%</th>
		</tr>
		</tfoot>
	</table>


	<?php

} //end display KPA





$sources = $scdObj->getJALSources();
//$kpas = $assessObj->getKPAList();
unset($sdbp5Obj);

$sdbp5Obj = new JAL1_PMS();
$primary_source = $scdObj->getPrimaryJALSource();
$kpas = $sdbp5Obj->getKPAs($primary_source);
$head = $sdbp5Obj->getHeadings($primary_source,true,false);
unset($head['main']['activity_subfunction_id']);



$lineObj = new PM5_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id'],false,array(),"JAL");
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);

//ASSIST_HELPER::arrPrint($kpas);
//ASSIST_HELPER::arrPrint($lines);

$display_jal = true;
if(count($kpas)==0 || count($lines)==0) {
	$display_jal = false;
}

if($display_jal) {
?>
<h3>Activities</h3>

<table id=tbl_jal class=list width=100%>
<thead>
	<tr>
		<?php
		foreach($head['main'] as $fld=>$name) {
			echo "<th>".$name."</th>";
		}
		?>
		<th>Function<br />Weight</th>
		<th>Overall<br />Weight</th>
	</tr>
</thead>
<tbody>
<?php
$grand_tot = 0;
$types = array("JAL");
$names = array('JAL'=>"Activities");

foreach($kpas as $k => $a) {
foreach($types as $kt) {
	$tot = 0;
	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
		//echo "<h3>".$a."</h3>";
		echo "<tr class=subth><td colspan=".(count($head['main'])+2).">".$a." - ".$names[$kt]."</td></tr>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k][$kt];
		foreach($objects as $i => $obj) { $total_line_count++;
			echo "
			<tr  class=\"".($obj['active']==1 ? "" : "inactive")."\">";
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				$w = (isset($weights[$i]) ? $weights[$i] : -1);
				//if weight is a negative then assume that KPA weighting step not done.
				if($w<0) { $error = true; }
			echo "
				<td id=td_".$k.$kt."_".$i." class=right>".number_format(round(($w/$kw)*100,$line_weight_precision_allowed),$line_weight_precision_allowed)."%</td>
				<td class=right><div style='width:60px' class='float'>".number_format($w,$line_weight_precision_allowed)."%</div></td>
			</tr>";
			$tot+=(isset($weights[$i]) ? $weights[$i] : 1);
		}
		echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>".number_format(100,$line_weight_precision_allowed)."%</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".number_format($tot,$line_weight_precision_allowed)."%</th>
		</tr>";
	}
	$grand_tot+=$tot;
}
}
?>
	</tbody>
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=<?php echo (count($head['main'])); ?>>Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot><?php echo number_format($grand_tot,$line_weight_precision_allowed); ?>%</th>
		</tr>
	</tfoot>
</table>

<?php
}//end display JAL






$listObj = new PM5_LIST("competency_category");
$category_objects = $listObj->getActiveListItemsFormattedForSelect();
$listObj->changeListType("competencies");
$competency_objects = $listObj->getListItemsGroupedByParent($listObj->getParentField());
$lines = $lineObj->getLineSrcIDs($_REQUEST['obj_id'], $lineObj->getModRef(),"CC");

$display_cc = true;
if(count($objects)==0 || count($lines)==0) {
	$display_cc = false;
}
if($display_cc) {

	/**
	 * Check that each category has lines otherwise get rid of the category for display purposes
	 */
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		$line_count = 0;
		if(count($objects) > 0) {
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$line_count++;
				}
			}
		}
		if($line_count==0) {
			unset($category_objects[$cate_id]);
		}
	}

	?>

<h3>Core Competencies</h3>
<table class=list id=tbl_cc>
	<tr>
		<th>Core Competency</th>
		<th>Description</th>
		<th>Weight</th>
	</tr>
	<?php
	$colspan = 3;
	$tot = 0;
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		if(count($objects) > 0) {
			echo "<tr class=subth><td colspan='".$colspan."'>$cate</td></tr>";
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$total_line_count++;
					$li = $lines[$i];
					$w = isset($weights[$li]) ? $weights[$li] : -1;
					//if weight is a negative then assume that CC weighting step not done.
					if($w < 0) {
						$error = true;
					}
					$tot += $w;
					echo "
					<tr>
						<td>".$obj['name']."</td>
						<td>".$obj['description']."</td>
						<td class=right>".number_format($w,$line_weight_precision_allowed)."%</td>
					</tr>";
				}
			}
		}
	}
	?>
	<tr class=total>
		<td></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot><?php echo number_format($tot,$line_weight_precision_allowed); ?>%</td>
	</tr>
</table>

<?php

}//end display CC
?>










<h3>Component Weights</h3>
<?php

$echo = $displayObject->getComponentWeighting($obj_id,$pa,false);
echo $echo['display'];



if($total_line_count==0) {
	$error = true;
}



?>
</form>




<?php
//don't display save buttons & creation steps if page is being called in the Manage > View section - check also done in function as backup
if($page_action!="view") {
	?>
	<p class='center no-print'><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Confirm & Activate</button></p>
	<?php
	$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id']);
}//end if check for Manage > View section






?>

<span style='width:100%;margin-right: 25px;border:1px solid #ffffff'>
	<p class='right'><button class='no-print' id=btn_sign act=display>Display Space for Signing</button></p>
	<table style='margin: 0 auto; margin-top: 10px; page-break-inside: avoid;' id=tbl_sign>
		<tr class=head>
			<td class=b>On Behalf of Employee</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td class=b>On Behalf of Organisation</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Name</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Name</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Signature</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Signature</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Date</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Date</td>
		</tr>
	</table>
</span>
<p>&nbsp;</p>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;


	<?php
	echo $echo['js'];
	?>


	$("#tbl_sign").addClass("noborder").find("td").addClass("center").addClass("noborder");
	$("#tbl_sign").hide();
	$("#tbl_sign tr.handwrite").find("td").height(70).css("vertical-align","bottom");
	$("#tbl_sign tr.comment").find("td").addClass("i");

	$("#btn_sign").button({
		icons:{primary:"ui-icon-pencil"}
	}).click(function(e) {
		e.preventDefault();
		var act = $(this).attr("act");
		if(act=="display") {
			$("#tbl_sign").show();
			$(this).attr("act","hide");
			$(this).button("option","label","Hide Space for Signing");
		} else {
			$("#tbl_sign").hide();
			$(this).attr("act","display");
			$(this).button("option","label","Display Space for Signing");
		}
	});

	$("table.noborder, table.noborder td").css("border","0px");

	$("#btn_next").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		AssistHelper.processing();
		var err = false;
		/*$("td.total_weight").each(function() {
			if(($(this).html()*1)!=100) {
				err = true;
			}
		});*/
		if(err) {
			//alert("Not all KPA Weights total 100.  Please review the weights again.");
			AssistHelper.finishedProcessing("error","An error has occurred.  Please reload the page and try again.");
		} else {
			var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>";
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Activate",dta);
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>.php?r[]=ok&r[]="+result[1];
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
		//document.location.href = 'assessment_create_step3.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");//.css({"color":"#009900","border":"1px solid #009900"});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		AssistHelper.processing();
		//if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		//}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");

	<?php
	if($error) {
		?>
	$("#btn_next").hide();//.prop("disabled","disabled").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
	$("#btn_sign").hide();
	AssistHelper.processing();
	AssistHelper.finishedProcessing("error","This Scorecard is not complete.  Please review all steps and ensure that each step has been completed and saved.");
		<?php
	}
	?>

});

</script>