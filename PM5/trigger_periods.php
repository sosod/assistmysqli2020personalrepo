<?php
require_once("inc_header.php");


$apObject = new PM5_PERIOD();
$periods = $apObject->getAllAssessmentPeriods();

//ASSIST_HELPER::arrPrint($module_setup);


if(count($periods)==0) {
	//check if time periods have been created
	$time_periods = $apObject->getListOfTimePeriods();
	//if no time periods then create
	if(count($time_periods)==0) {
		$time_periods = $apObject->createTimePeriodsAndAssessmentPeriods($module_setup['FIN_YEAR']['value']);
		$periods = $apObject->getAllAssessmentPeriods();
		if(count($periods)==0) {
			die(ASSIST_HELPER::displayResult(array("error","An error has occurred while trying to create the periods.  Please contact your Business Partner for more assistance.")));
		}
	}
}
$available_periods = count($periods)>0?true:false;



?>
<table class=list>
	<tr>
		 <th>Ref</th>
		 <th>Name</th>
		 <th>Start Date</th>
		 <th>End Date</th>
		 <th>Status</th>
<!--		 <th></th>-->
	</tr>
	<?php
	
	if($available_periods) {
		foreach($periods as $pi => $p) {
			echo "
			<tr>
				 <td>".$pi."</td>
				 <td>".$p['name']."</td>
				 <td>".date("d M Y",strtotime($p['start_date']))."</td>
				 <td>".date("d M Y",strtotime($p['end_date']))."</td>
				 <td>".$p['status']."</td>
			</tr>";
		//		 <td><button id='btn_edit_".$pi."' class='btn-edit' period_id='".$pi."'>Edit</button></td>
		}
	} else {
		echo "
		<tr>
			<td colspan=6>No periods available.</td>
		</tr>";
	}
	
	?>
</table>

<script type="text/javascript">

</script>