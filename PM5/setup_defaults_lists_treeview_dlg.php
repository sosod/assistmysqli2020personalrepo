<?php
require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

$list_id = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];
$action = $_REQUEST['action'];
$parent_id= $_REQUEST['parent_id'];

$listObj = new PM5_LIST($list_id);
$fields = $listObj->getFieldNames();
$types = $listObj->getFieldTypes();
$list_data = $listObj->getListItems();

$competency_id = $_REQUEST['competency_id'];

if($list_id == "competencies"){
    $listObj2 = new PM5_LIST("competency_category");
    $cat_list_tems = $listObj2->getListItems();
    foreach($cat_list_tems as $list => $val){
        if($val['status'] == PM5::SYSTEM_DEFAULT || $val['status'] == PM5::ACTIVE){
            $cat_sups[$val['id']]=$val['name'];
        }
    }
}

if($list_id == "competencies_levels"){
    $competency_object = new PM5_COMPETENCIES();
    $competency_levels = $competency_object->getCompetencyLevelsByCompetencyID($competency_id);
    $competency = $competency_object->getCompetencyByID($competency_id);
    $proficiencies = $competency_object->getAllProficiencies();
}

?>
<input type="hidden" id="object_id" name="object_id" value="<?php echo $object_id ; ?>"  />
<input type="hidden" id="object_type" name="object_type" value="<?php echo $list_id ; ?>"  />
<input type="hidden" id="action" name="action" value="<?php echo $action ; ?>"  />
<input type="hidden" id="competency_id" name="competency_id" value="<?php echo $competency_id ; ?>"  />
<input type="hidden" id="parent_id" name="parent_id" value="<?php echo $parent_id ; ?>"  />
<?php
if($action == 'view'){
    require_once("setup_defaults_lists_treeview_dlg_view.php");
}elseif($action == 'edit'){
    require_once("setup_defaults_lists_treeview_dlg_edit.php");
}else{
    require_once("setup_defaults_lists_treeview_dlg_add.php");
}
?>
<script type="text/javascript">
    $(function() {
        //Tell parent window to open the dialog once the page has finished loading
        window.parent.openDialog();
    });
</script>
