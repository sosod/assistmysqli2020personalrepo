<?php

$page_action = "trigger";

require_once("inc_header.php");

$apObject = new PM5_PERIOD();
$periods = $apObject->getAllAssessmentPeriods();
if(count($periods)==0) {
	ASSIST_HELPER::displayResult(array("error","No Assessment Periods have been created.  Please go to Assessments > Assessment Periods to auto-generate Assessment Periods."));
	die();
}
unset($apObject);

$scdObj = new PM5_SCORECARD();
$scorecards = $scdObj->getObjectsForTrigger();


?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Function<br />Activities</th>
		<th><?php echo $scdObj->getObjectName(PM5_ASSESSMENT::OBJECT_TYPE,true); ?></th>
		<th></th>
	</tr>
<?php
if(count($scorecards)>0) {
	foreach($scorecards as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
			<td class=center>
			<button class=btn_open obj_id=".$pa['obj_id'].">Open</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=7>No ".$scdObj->getObjectName($scdObj->getObjectType(),true)." available.</td>
	</tr>";
}

?>
</table>
<?php



?>

<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");
	
/*	
	$("#btn_save").button({icons: {primary:"ui-icon-disk"}
	}).click(function(e) {
		e.preventDefault();
		if($("#sel_employee").val()=="X") {
			alert("Please select an employee.");
		} else {
			AssistHelper.processing();
			var dta = "obj_id="+$("#dlg_reassign #reassign_obj_id").val()+"&employee="+$("#sel_employee").val();
			console.log(dta);
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Reassign",dta);
			console.log(result); 
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"		
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});
	
	*/
	$(".btn_open").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "trigger_assessments_details.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	/*
	$(".btn_reassign").button({
		icons: {primary: "ui-icon-person"},
	}).click(function(e) {
		e.preventDefault();
		var obj_id = $(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		var tki = $(this).parent().parent().find("td:eq(1)").html();
//		console.log(tki);
//		console.log($("#dlg_reassign #sel_employee").val());
//		console.log($("#sel_employee option[value='0001']").length);
		//$("#sel_employee option[value='0001']").prop("selected","selected");
		$("#dlg_reassign #reassign_obj_id").val(obj_id);
		$("#dlg_reassign #p_assess").html("Assessment: "+ref);
		$("#dlg_reassign #p_tki").html("Current Employee: "+tki);
		$("#dlg_reassign").dialog("open");
		//$("#sel_employee").val(tki);
		//console.log($("#dlg_reassign #sel_employee").val());
		//$("#dlg_reassign #sel_employee").find("option").each(function() {
		//	console.log($(this).val()+": "+$(this).html()+" - "+$(this).is(":selected"));
		//});
		//document.location.href = "assessment_edit_step1.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	
	$(".btn_restore").button({
		icons: {primary: "ui-icon-check"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to restore Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Restore","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"

	$(".btn_delete").click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_deactivate").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function(e) {
		e.preventDefault();
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to deactivate Assessment "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Assessment.Deactivate","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_edit.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
*/
});
</script>