<?php
require_once("inc_header.php");

//first check which active users have access to the module
//$userObj = new PM5_USERACCESS();
//$users = $userObj->getMenuUsersFormattedForSelect();
//$user_ids = array_keys($users);

//find out if user has access to all scorecards or only their own employees
//TO BE PROGRAMMED - ASSUME limit = false
$limit = false;


//then check for any users who already have assessments
$scdObj = new PM5_SCORECARD();
$pending_objects = $scdObj->getPendingObjects($limit);
//$active_users = $scdObj->getUsersWithActiveAssessments();

/*echo "<hr /><h1>Users</h1>";
ASSIST_HELPER::arrPrint($users);
echo "<hr /><h1>pending objects</h1>";
ASSIST_HELPER::arrPrint($pending_objects);
echo "<hr /><h1>active users</h1>";
ASSIST_HELPER::arrPrint($active_users);
echo "<hr />";

*/
//$employees = array_diff($user_ids, $active_users);

$empObj = new PM5_EMPLOYEE();
$available_employees = $empObj->getAvailableEmployeesForCreate();

$bonusObject = new PM5_SETUP_BONUS();
$bonus_scales = $bonusObject->getListOfActiveBonusScales();
//ASSIST_HELPER::arrPrint($bonus_scales);

ASSIST_HELPER::displayResult(array("info","Only Employees who are non-system users or who have menu access to this module, are available to have scorecards created.  Any Employee who already has an existing scorecard, cannot have another created until that scorecard has been deactivated."));
?>
<p><button id=btn_add>Create New</button></p>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Job Title</th>
		<th>Bonus Scale</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Activities</th>
		<th>Core<br />Competencies</th>
		<th>Created On</th>
		<th></th>
	</tr>
<?php
if(count($pending_objects)>0) {
	foreach($pending_objects as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class=''>".$pa['employee']."</td>
			<td class=''>".(str_replace("(Started","<br /><small><i>(Started",$pa['job'])."</i></small>")."</td>
			<td class=''>".$pa['bonus_scale']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>".date("d M Y H:i",strtotime($pa['scd_insertdate']))."</td>
			<td class=center>
				<button class=btn_continue obj_id=".$pa['obj_id'].">Continue</button>
				<button class=btn_delete obj_id=".$pa['obj_id'].">Delete</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=10>No pending scorecards awaiting completion.</td>
	</tr>";
}

?>
</table>
<?php




?>
<div id=dlg_new title="Create New">
	<h2>Select Employee</h2>
	<p><?php
	if(count($available_employees)>0) {
		echo "
		<input type=hidden name=emp_id id=emp_id value='' />
		<select name=sel_employee id=sel_employee>
		<option selected value=X>--- SELECT EMPLOYEE ---</option>";
		foreach($available_employees as $tkid => $emp) {
			echo "<option value=".$tkid." emp_id='".$emp['emp_id']."' jobs='".implode("|",$emp['job'])."'>".$emp['name']."</option>";
		}

		echo "</select></p><p><select id=sel_job name=sel_job>
		<option selected value=X>--- SELECT JOB ---</option>";
		echo "</select></p><p><select id=sel_bonusscale name=sel_bonus>
		<option selected value=X>--- SELECT ".($scdObj->replaceObjectNames("|bonusscale|"))." ---</option>";
		foreach($bonus_scales as $b => $s) {
			echo "<option value=".$b.">".$s."</option>";
		}
		echo "</select></p><p><button id=btn_new_next1>Next </button>";
	} else {
		echo "No employees available.";
	}
	?>
	</p>
</div>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").each(function() {
		$(this).find("td:eq(1)").removeClass("center");
		$(this).find("td:eq(2)").removeClass("center");
	});
	$("#dlg_new").dialog({
		modal: true,
		autoOpen: false
	});
	$("#btn_add").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).click(function() {
		$("#dlg_new").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	$("#btn_new_next1").button({
		icons: {secondary: "ui-icon-circle-arrow-e"},
	}).click(function() {
		$("#sel_employee").css("background-color","");
		$("#sel_job").css("background-color","");
		$("#sel_bonusscale").css("background-color","");
		var i = $("#sel_employee").val();
		var ji = $("#sel_job").val();
		var bsi = $("#sel_bonusscale").val();
		if(i.length<4) {
			$("#sel_employee").css("background-color","#cc0001");
			alert("Please select an employee.");
		} else if(ji=="X") {
			$("#sel_job").css("background-color","#cc0001");
			alert("Please select which Job / Contract this scorecard relates to.");
		} else if(bsi=="X") {
			$("#sel_bonusscale").css("background-color","#cc0001");
			alert("Please select which <?php echo $scdObj->replaceObjectNames("|bonusscale|"); ?> this scorecard must use for Performance % and Bonus calculations.");
		} else {
			AssistHelper.processing();
			var dta = "step=0&obj_id=0&employee_id="+i+"&emp_id="+$("#emp_id").val()+"&job_id="+$("#sel_job").val()+"&bonus_id="+$("#sel_bonusscale").val();

			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Add",dta);
			if(r[0]=="ok") {
				document.location.href = "assessment_create_step1.php?obj_id="+r['obj_id']+"&r[]=ok&r[]="+r[1];
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-grey");
	//.children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-right":"25px","font-size":"80%"
	//});
	$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed");

	$("#sel_job").hide();
	$("#sel_bonusscale").hide();

	$("#sel_employee").change(function() {
		if($(this).val()=="X") {
			$("#sel_job").hide();
			$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
		} else {
			var $sel_option = $(this).find("option:selected");
			var emp_id = $sel_option.attr("emp_id");
			$("#emp_id").val(emp_id);
			var jobs_ids = $sel_option.attr("jobs");
			var jobs = AssistHelper.doAjax("inc_controller_assessment.php?action=EMP.getJobsFormattedForSelect","job_id="+jobs_ids);

			if(jobs.length==0) {
				alert("Oops, that employee does not appear to have any available Jobs/Contracts.  Please select another Employee.");
			} else {
				$("#sel_job").show();
				$("#sel_job").find("option:gt(0)").remove();
				for(i in jobs) {
					$("#sel_job").append($("<option />",{value:i,html:jobs[i]}));
				}
				$("#dlg_new").dialog("option","width","auto");
			}
		}
	});
	$("#sel_job").change(function() {
		if($(this).val()=="X") {
			$("#sel_bonusscale").hide();
		} else {
			$("#sel_bonusscale").show();
		}
	});
	$("#sel_bonusscale").change(function() {
		if($(this).val()=="X") {
			$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
		} else {
			$("#btn_new_next1").prop("disabled","").css("cursor","pointer").removeClass("ui-button-bold-grey").addClass("ui-button-bold-green");
		}
	});

	$(".btn_continue").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"

	$(".btn_delete").click(function() {
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete Scorecard "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = "assessment_create.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

	<?php
	if(count($pending_objects)==0) {
		?>
		var c = $("#tbl_list tr:first").find("th, td").length - 1;
		$("#tbl_list tr:last td:last").prop("colspan",c);
		<?php
	}
	?>
});
</script>