<?php //error_reporting(-1);
require_once("../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.");//.serialize($_REQUEST));
/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
//echo "<pre>";print_r($_REQUEST);echo "</pre>";

$my_action = explode(".",$_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);

$page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : false;
unset($_REQUEST['page_direct']);
//$has_attachments = (isset($_REQUEST['has_attachments']) && $has_attachments!=false && $has_attachments!="false") ? $_REQUEST['has_attachments'] : false;
$has_attachments = isset($_REQUEST['has_attachments']) ? $_REQUEST['has_attachments'] : false;
unset($_REQUEST['has_attachments']);
$attach = array();

$activity_log_handled_by_class = array("USERACCESS","LISTS","MENU","NAMES","SETUPBONUS");
$result[1].=$class."-".$activity;
switch($class) {
	case "SCORECARD":
		$object = new PM5_SCORECARD();
		break;
	case "SCORECARDWEIGHT":
		$object = new PM5_SCORECARD_WEIGHT();
		break;
	case "ASSESSMENT":
		$object = new PM5_ASSESSMENT();
		break;
	case "TRIGGER":
		$object = new PM5_TRIGGER();
		break;
	case "LINES":
	case "LINE":
		$object = new PM5_LINE();
		break;
	case "SRCOBJECT":
		$class = $_REQUEST['modloc']."_PMS";
		$object = new $class();
		break;
	case "SDBP5":
	case "SDBP5B":
		$object = new SDBP5B_PMS();
		break;
	case "SDBP5C":
		$object = new SDBP5C_PMS();
		break;
	case "CC_SCORE":
		$object = new PM5_CC_SCORE();
		break;
	case "SETUPBONUS":
		$object = new PM5_SETUP_BONUS();
		break;
	case "EMP":
		$object = new PM5_EMPLOYEE();
		break;

}
$result[2]=":".(is_array($my_action) ? implode(";",$my_action) : $my_action).":";
$result[3] = array($class,$activity);
//echo json_encode($result); die();



if(isset($object)) {
	switch($activity) {
		case "ADD":
			$result = $object->addObject($_REQUEST);
			break;
		case "UPDATE":
			$result = $object->updateObject($_REQUEST);
			break;
        case "UPDATEBYBONUSSCALEID":
            $result = $object->updateObjectbyBonusScaleID($_REQUEST);
            break;
		case "DEACTIVATE":
			$result = $object->deactivateObject($_REQUEST);
			break;
		case "DELETE":
			$result = $object->deleteObject($_REQUEST);
			break;
		case "RESTORE":
			$result = $object->restoreObject($_REQUEST);
			break;
		case "REASSIGN":
			$result = $object->reassignObject($_REQUEST);
			break;
		case "ACTIVATE":
			$result = $object->activateObject($_REQUEST);
			break;
		default:
			//$result[5] = "unknown activity - using call_user_func($class , $activity , ".implode(":",$_REQUEST).")";
			$result = call_user_func(array($object, $activity),$_REQUEST);
			break;
	}
} else {
	$result[4] = "no object set";
}
//echo json_encode(array_merge($_REQUEST,$result)); die();
//echo ":".$has_attachments.":";
		//echo "<script type=text/javascript>window.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
	if($has_attachments==false) {
		//if($page_direct=="dialog") {
		//	echo "<script type=text/javascript>window.parent.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
		//} else {
			echo json_encode($result);
		//}
	} else {
		if($page_direct=="dialog") {
			echo "<script type=text/javascript>window.parent.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
		} else {
			$page_direct.="r[]=".$result[0]."&r[]=".$result[1];
			//echo "java script redirect to: ".$page_direct;
			echo "<script type=text/javascript>window.parent.location.href = '".$page_direct."';</script>";
		//	$object->arrPrint($result);
		}
	}







?>