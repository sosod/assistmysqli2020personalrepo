<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class PM5_PERIOD extends PM5 {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	
	protected $formatted_db_name = "CONCAT(MONTHNAME(TS.end_date),IF(YEAR(TS.end_date)=YEAR(TE.end_date),'',CONCAT(' ',YEAR(TS.end_date))),' - ',MONTHNAME(TE.end_date), ' ',YEAR(TE.end_date))";
    /*
	protected $parent_field = "_assessid";
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PERIOD";
    const TABLE = "periods";
    const TABLE_FLD = "prd";
    const REFTAG = "SP";
	const LOG_TABLE = "periods";
	/**
	 * STATUS CONSTANTS
	 */
	//const SELF_COMPLETED = 1024;
	//const MOD_COMPLETED = 2048;
	//const FINAL_COMPLETED = 8192;
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
/*		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	/*
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		$sql = "hello";
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
		*/
	/*
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    



	public function getPeriodsSQL($flds=array(),$tbls=array(),$where="",$active_only=true) {
		$tbl_fld = $this->getTableField();
		$sql = "SELECT P.*, ".$this->getIDFieldName()." as id
				, ".$this->formatted_db_name." as name
				, TS.start_date as start_date
				, TE.end_date as end_date 
				".(count($flds)>0 ? ", ".implode(",",$flds) : "")."
				FROM ".$this->getTableName()." P 
				INNER JOIN ".$this->getDBRef()."_list_time TS ON P.".$tbl_fld."_startid = TS.id
				INNER JOIN ".$this->getDBRef()."_list_time TE ON P.".$tbl_fld."_endid = TE.id 
				".(count($tbls)>0 ? " ".implode(" ",$tbls) : "")."";
		if(strlen($where)>0 || $active_only) {
			$sql.= " WHERE ".($active_only ? " (P.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE : " 1 ");
			$sql.=$where;
		}
		$sql.=" ORDER BY TS.start_date, TE.end_date";
		
		return $sql;
	}


	
    /**
	 * Returns a formatted list of the Periods
	 * @param not_in_use (BOOL)
	 * @param scd_id (INT) - for use with not_in_use
	 * @param obj_id (INT) - if to limit to only 1 period
	 * @return data (Array)
	 */
	public function getAssessmentPeriods($not_in_use=false,$scd_id=0,$obj_id=0) {
		/*$tbl_fld = $this->getTableField();
		$sql = "SELECT P.*, ".$this->getIDFieldName()." as id
				, ".$this->formatted_db_name." as name
				, TS.end_date as start_date
				, TE.end_date as end_date 
				FROM ".$this->getTableName()." P 
				INNER JOIN ".$this->getDBRef()."_list_time TS ON P.".$tbl_fld."_startid = TS.id
				INNER JOIN ".$this->getDBRef()."_list_time TE ON P.".$tbl_fld."_endid = TE.id ";
		if($obj_id==0) {
			$sql.= " WHERE (P.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		} else {
			$sql.= " WHERE ".$this->getIDFieldName()." = ".$obj_id;
		}
		$sql.=" ORDER BY TS.start_date, TE.end_date";*/
		/**
		 * if(not_in_use===true)
		 * 	filter for assess_id
		 */
		$where = "";
		if($obj_id>0) {
			$where = " AND ".$this->getIDFieldName()." = ".$obj_id;
		}
		 
		$sql = $this->getPeriodsSQL(array(),array(),$where);
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		 
		 
		 
		return $data;
	}
	
	public function getAllAssessmentPeriods($not_in_use=false,$scd_id=0,$obj_id=0) {
		$where = "";
		 
		 
		$sql = $this->getPeriodsSQL(array(),array(),$where, false);
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		 
		foreach($data as $di => $d) {
			if($d[$this->getStatusFieldName()]==self::ACTIVE) {
				$data[$di]['status'] = "Active";
				$data[$di]['raw_status'] = "active";
			} elseif($d[$this->getStatusFieldName()]==self::INACTIVE) {
				$data[$di]['status'] = "Inactive";
				$data[$di]['raw_status'] = "inactive";
			} else {
				$data[$di]['status'] = "Unknown";
				$data[$di]['raw_status'] = "";
			}
		}
		 
		return $data;
	}
	
	
	/***
	 * Get a list of Assessment periods formatted for use in a drop down.
	 * @param BOOL not_in_use = false 
	 * @param INT scd_id = 0
	 * @return ARRAY(id=>title)
	 */
	public function getAssessmentPeriodsForSelect($not_in_use=false,$scd_id=0) {
		$data = $this->getAssessmentPeriods($not_in_use,$scd_id);
		
		$list = array();
		foreach($data as $key => $d) {
			$list[$key] = $d['name'];
		}
		return $list;
	}
	
	public function getAllAssessmentPeriodsForSelect() {
		$where = "";
		 
		 
		$sql = $this->getPeriodsSQL(array(),array(),$where, false);
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		
		$list = array();
		foreach($data as $key => $d) {
			$list[$key] = $d['name'];
		}
		return $list;
		
	}
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	*/
	public function getAObject($id=0,$options=array()) {
		$data = array();
		$rows = $this->getAssessmentPeriods(false,0,$id);
		return $rows;
	}
	/*
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT *, prd_startid as start, prd_endid as end FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	public function getTimePeriodsByAssessmentPeriod($period_id=0) {
		$row = $this->getRawObject($period_id);
		$start_id = $row['start'];
		$end_id = $row['end'];
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id >= $start_id AND id <= $end_id ORDER BY start_date, end_date";
		$rows = $this->mysql_fetch_all_by_id($sql,"id");
		$data = array();
		foreach($rows as $key => $r) {
			$data[$key] = date("F Y",strtotime($r['end_date']));
		}
		return $data;
	}






	/**
	 * get list of time periods
	 */
	public function getListOfTimePeriods() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE (status & ".self::ACTIVE.") = ".self::ACTIVE." ORDER BY start_date";
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		return $rows;
	}

	/**
	 * Create time records for first use
	 */
	public function createTimePeriodsAndAssessmentPeriods($fin_year_id){
		$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
		$fin_row = $masterObject->getAListItem($fin_year_id);
		$raw_start_date = $fin_row['start_date'];
		$raw_end_date = $fin_row['end_date'];

		$var = array(
			'type'=>"year",
			'start_date'=>$raw_start_date,
			'end_date'=>$raw_end_date
		);

		$result = $this->addTimePeriod($var);

//		ASSIST_HELPER::arrPrint($result);
		$time_periods = $this->getListOfTimePeriods();

		//create assessment periods
		$default_assessment_periods = array(
			'quarter_1'=>array(1,3),
			'quarter_2'=>array(4,6),
			'quarter_3'=>array(7,9),
			'quarter_4'=>array(10,12),
			'half_1'=>array(1,6),
			'half_2'=>array(7,12),
			'year'=>array(1,12),
		);
		$time_keys = array_keys($time_periods);
		foreach($default_assessment_periods as $descrip_key => $period_keys) {
			$start_location = $period_keys[0]-1;
			$end_location = $period_keys[1]-1;
			$start_key = $time_keys[$start_location];
			$end_key = $time_keys[$end_location];
			$insert_data = array(
				$this->getTableField().'_startid'=>$start_key,
				$this->getTableField().'_endid'=>$end_key,
				$this->getTableField().'_status'=>self::ACTIVE,
				$this->getTableField().'_insertuser'=>$this->getUserID(),
				$this->getTableField().'_insertdate'=>date("Y-m-d h:i:s"),
			);
			$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
			$this->db_insert($sql);
		}


		//return assessment periods
//		$periods = $this->getAllAssessmentPeriods();
		return $time_periods;

	}


	public function addTimePeriod($var) {
		$error = false;
		$type = $var['type'];
		if($type=="year") {
			//check that time periods haven't already been created for the given SDBIP
				$year_start_date = strtotime($var['start_date']." 00:00:00");
				$year_end_date = strtotime($var['end_date']." 23:59:59");
				//check for valid start and end dates - does the end date fall after the start date?
				if(($year_end_date)<($year_start_date)) {
					$error = true;
					$result = array("error","Invalid ".$this->getObjectName("financialyear")." start and end dates provided.  Unable to continue.  Error: End date falls before start date.");
				} else {	//else for if check for valid start and end dates - does the end date fall after the start date?
					//if start date is not the first day of the month
					if(date("d",$year_start_date)*1!=1) {
						//check if the end date has a day that is 1 day less than the given start date (i.e. 15 July 2018 to 14 July 2019)
						//if yes then use those days to create the time periods
						if(date("d",$year_start_date)*1==(date("d",$year_end_date)*1+1)) {
							$day = date("d",$year_start_date)*1;
							$month = date("m",$year_start_date)*1;
							$year = date("Y",$year_start_date)*1;
							$tp_start = mktime(0,0,0,$month,$day,$year);

							$eday = date("d",$year_end_date)*1;
							$emonth = date("m",$year_end_date)*1;
							$eyear = date("Y",$year_end_date)*1;
							$tp_end = mktime(23,59,59,$emonth,$eday,$eyear);
							//loop through the months and add each as time periods individually
							while($tp_start < $year_end_date && $tp_end <= $year_end_date) {
								$add_var = array(
									'start'=>$tp_start,
									'end'=>$tp_end
								);
								$result = $this->addTimeRecord($add_var);
								//prep for next period
								$year = ($month==12?$year+1:$year);
								$month = ($month==12?1:$month+1);
								$tp_start = mktime(00,00,00,$month,$day,$year);
								$eyear = ($emonth==12?$eyear+1:$eyear);
								$emonth = ($emonth==12?1:$emonth+1);
								$tp_end = mktime(23,59,59,$emonth,$eday,$eyear);
							}
						} else {
							$day = date("d",$year_start_date)*1;
							$month = date("m",$year_start_date)*1;
							$year = date("Y",$year_start_date)*1;
							$tp_start = mktime(0,0,0,$month,$day,$year);
							$tp_end = mktime(23,59,59,$month,date("t",$year_start_date)*1,$year);
							//create filler time period to get start date to first day
							$add_var = array(
								'start'=>$tp_start,
								'end'=>$tp_end
							);
							$result = $this->addTimeRecord($add_var);
							//reset start date to 1 of next month
							$day = 1;
							$year = ($month==12?$year+1:$year);
							$month = ($month==12?1:$month+1);
							$tp_start = mktime(00,00,00,$month,$day,$year);
							$tp_end = mktime(23,59,59,$month,date("t",$tp_start),$year);

							//loop through the months and add each as time periods individually
							while($tp_start < $year_end_date && $tp_end <= $year_end_date) {
								$add_var = array(
									'start'=>$tp_start,
									'end'=>$tp_end
								);
								$result = $this->addTimeRecord($add_var);
								//prep for next period
								$year = ($month==12?$year+1:$year);
								$month = ($month==12?1:$month+1);
								$tp_start = mktime(00,00,00,$month,$day,$year);
								$tp_end = mktime(23,59,59,$month,date("t",$tp_start),$year);
							}
						}
						//otherwise create 1 shortened time period for the first month and then default to monthly after that
					} else {	//else to if start date is not first day of the month
						$day = date("d",$year_start_date)*1;
						$month = date("m",$year_start_date)*1;
						$year = date("Y",$year_start_date)*1;
						$tp_start = mktime(0,0,0,$month,$day,$year);
						$tp_end = mktime(23,59,59,$month,date("t",$tp_start),$year);
						//loop through the months and add each as time periods individually
						while($tp_start < $year_end_date) {
							$add_var = array(
								'start'=>$tp_start,
								'end'=>$tp_end
							);
							$result = $this->addTimeRecord($add_var);
							//prep for next period
							$year = ($month==12?$year+1:$year);
							$month = ($month==12?1:$month+1);
							$tp_start = mktime(00,00,00,$month,$day,$year);
							$tp_end = mktime(23,59,59,$month,date("t",$tp_start),$year);
						}
					}	//endif start date is not the first date of the month
					//if result[0] is still "info" then assume all successful and adjust result message
					if($result[0]=="info") {
						$result = array("ok","All ".$this->getObjectName("TIMES")." successfully created.");
					}
				} //endif check for valid start and end dates - does the end date fall after the start date?
			} else { //else for test if sdbip_id doesn't already have time periods
				$result = array("error",$this->getObjectName("TIMES")." have already been created for this ".$this->getObjectName("SDBIP"));
			} //endif test if sdbip_id doesn't already have time periods

		return $result;
	}

	public function addTimeRecord($var) {
		$sql = "INSERT INTO ".$this->getDBRef()."_list_time SET
					 start_date = '".date("Y-m-d H:i:s",$var['start'])."'
					, end_date = '".date("Y-m-d H:i:s",$var['end'])."'
					, status = ".self::ACTIVE;
		$id = $this->db_insert($sql);
		if($this->checkIntRef($id)) {
			return true;
		} else {
			return false;
		}

	}
	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */

     
     
     
     
     
     
     
     
     
     
     
     
     

}


?>