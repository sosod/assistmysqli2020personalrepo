<?php
/**
 * To manage the user access of the PM5 module
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 *
 * 21 Dec 2015 [JC]: Modified for the PM5 module
 *
 *
 */

class PM5_USERACCESS extends PM5 {

	private $table_name = "_useraccess";

		//'ua_create_deliverable'	=> "Create |deliverable|",
	private $field_names = array(
		'ua_module'				=> "Module Admin",
		'ua_create_object'		=> "Create |scorecard| (All)",
		'ua_trigger'			=> "Trigger |assessment|",
		'ua_can_moderate'		=> "Can Moderate",
		'ua_can_final'			=> "Can Final Review",
		'ua_view_all'			=> "View All",
		'ua_report'				=> "Reports (All)",
		'ua_setup'				=> "Setup",
	);
		//'ua_update_all'			=> "Update All",
		//'ua_assurance'			=> "Assurance Provider",

		//'ua_create_deliverable'	=> 0,
	private $field_defaults = array(
		'ua_module'				=> 0,
		'ua_create_object'		=> 0,
		'ua_trigger'			=> 0,
		'ua_can_moderate'		=> 0,
		'ua_can_final'			=> 0,
		'ua_view_all'			=> 0,
		'ua_report'				=> 0,
		'ua_setup'				=> 0,
	);
		//'ua_assurance'			=> 0,
		//'ua_update_all'			=> 0,

	public function __construct() {
		parent::__construct();
		$this->field_names = $this->replaceObjectNames($this->field_names);
		$this->table_name = $this->getDBRef().$this->table_name;
		//Module admin is depreciated
		unset($this->field_names['ua_module']);
		unset($this->field_defaults['ua_module']);
	}

	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['ua_status'])) { $var['ua_status'] = self::ACTIVE; }
		if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
		return $this->addUser($var);
		//return array("info","No changes were found to be saved.");
	}
	public function editObject($var){
		$id = $var['ua_id'];
		unset($var['ua_id']);
		return $this->editUser($id,$var);
	}



	/**
	 * GET functions
	 */
	public function getUserAccessFields() { return $this->field_names; }
	public function getUserAccessDefaults()	{ return $this->field_defaults; }
	public function getTableName() { return $this->table_name; }
	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccess() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");

	}
	/**
	 * Get the list of users with access
	 */
	public function getActiveUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, UA.*
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;
	}
	public function getActiveUsersFormattedForSelect($ids="") {
		$rows = $this->getActiveUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}


	/*******************************************
	 * Get the list of users with MENU access irrespective of the their USER ACCESS
	 */
	public function getMenuUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;
	}
	public function getMenuUsersFormattedForSelect($ids="") {
		$rows = $this->getMenuUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	public function getModeratorUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()."
				ON ua_tkid = tkid AND ua_can_moderate = 1
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;
	}
	public function getModeratorUsersFormattedForSelect($ids="") {
		$rows = $this->getModeratorUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	public function getFinalReviewUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()."
				ON ua_tkid = tkid AND ua_can_final = 1
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;
	}
	public function getFinalReviewUsersFormattedForSelect($ids="") {
		$rows = $this->getFinalReviewUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	public function getTriggerUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()."
				ON ua_tkid = tkid AND ua_trigger = 1
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;
	}
	public function getTriggerUsersFormattedForSelect($ids="") {
		$rows = $this->getTriggerUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	/**
	 * Get the current user's access
	 */
	public function getMyUserAccess($ui="") {
		$ui = strlen($ui)==0 ? $this->getUserID() : $ui;
		$sql = "SELECT UA.* FROM ".$this->getTableName()." UA WHERE ua_tkid = '".$ui."'";
		$row = $this->mysql_fetch_one($sql);
		if(!isset($row['ua_status'])) {
			$row = $this->field_defaults;
			$row['ua_status'] = 0;
			$row['ua_tkid'] = $ui;
			$row['ua_admin'] = 0;
			$row['ua_assessment'] = 0;
		} else {
			$row['ua_admin'] = 0;
			$row['ua_assessment'] = 0;
			if($row['ua_update_all']==1) {
				$row['ua_admin']=1;
			}
			if($row['ua_create_object']==1) {
				$row['ua_assessment']=1;
			}
		}
		$user_access = array();
		foreach($row as $key => $val){
			$user_access[substr($key,3)] = $val;
		}
		return $user_access;
	}



	/**
	 * SET / UPDATE functions
	 */
	/**
	 * Adds a new user
	 */
	private function addUser($var){
		$result = array("info","No change was found to be saved.");
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$result = array("ok","User added successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'	=> "Added user access for user: ".$this->getAUserName($var['ua_tkid']),
			);
			$log_var = array(
				'section'	=> "USER",
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> PM5_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);
		} else {
		/*
		 * */
			$result = array("error","Sorry, something went wrong while trying to add the user.  Please try again.");
		}
		return $result;
	}


	private function editUser($id,$var) {
		if(ASSIST_HELPER::checkIntRef($id)) {
			$old = $this->mysql_fetch_one("SELECT * FROM ".$this->getTableName()." WHERE ua_id = ".$id);
			//$edits = array();
			//foreach($var as $fld=>$v){
			//	$edits[substr($fld,5)] = $v;
			//}
			$edits = $var;
			$update_data = $this->convertArrayToSQL($edits);
			$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ua_id = ".$id;
			$mar = $this->db_update($sql);
			if($mar>0){
				$un = $this->getAUserName($old['ua_tkid']);
				//$user_access_fields = $this->getUserAccessFields();
				$changes = array(
					'user'=>$this->getUserName(),
					'response'	=> "Edited user access for user: ".$un,
				);
				foreach($old as $key=>$value){
					if(isset($edits[$key]) && $value != $edits[$key]){
						//$changes[$user_access_fields[$key]] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						//$changes[$key] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						$name = (str_replace(" ","_",$this->field_names[$key]));
						$changes[$name] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
					}
				}

				$log_var = array(
					'section'	=> "USER",
					'object_id'	=> $id,
					'changes'	=> $changes,
					'log_type'	=> PM5_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
				return array("ok","Changes to ".$un." have been saved successfully.");
			} else {
				return array("info","No change was found to be saved.");
			}
		}
		return array("error","An error occurred while trying to save the changes. Please try again.");
	}







	public function canICreateDeliverables($ui="") {
		//$ua = $this->getMyUserAccess($ui);
		//return ($ua['create_deliverable']==1);
		return false;
	}




}



?>