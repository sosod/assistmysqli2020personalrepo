<?php

class PM5_SETUP_BONUS extends PM5 {

	private $not_applicable_text = "Not Applicable";
	private $not_applicable_id = 0;


	const OBJECT_TYPE = "BONUSSCALE";
	const SESSION_EXPIRATION_MINUTES = 10;


	public function __construct($bonus_scale_id=false) {
		parent::__construct();
		if($bonus_scale_id!==false) {

		$this->bonus_scale = $this->getBonusScaleForProcessing($bonus_scale_id);
		$this->bonus_scale_updated_from_central_class = true;
		}
	}




	/********************
	 * CONTROLLER Functions
	 */
    public function addItem($var) {
        $insert_vars = $var;
        $insert_vars['status'] = PM5::ACTIVE;

        $bonus = $var['bonus'];

        $bonus_scale_id = $var['bonus_scale_id'];
        $bonus_scale_items = $this->getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, true);

//        if(!isset($bonus_scale_items[$bonus])){
            $insert_vars['end'] = $var['start'] + 9.99;

            $sql = "INSERT INTO ".$this->getDBRef()."_setup_bonus_scale SET ".$this->convertArrayToSQLForSave($insert_vars);
            $id = $this->db_insert($sql);

            if(is_numeric($id) && (int)$id != 0){
                $result = array('ok', $bonus . ' Created', 'sql' => $sql);
            }else{
                $result = array("error", "An error occurred while trying to add the item.  Please try again. (Error code: BNSC001)");
            }
//        }else{
//            $result = array('info', $bonus . ' already exists');
//        }

        return $result;
    }

	public function updateObject($var,$attach=array()) {
		//return array("error","setupbonus.updateobject");
		return $this->saveCustomBonusScale($var);
	}

    public function updateObjectbyBonusScaleID($var,$attach=array()) {
        //return array("error","setupbonus.updateobject");
        return $this->saveCustomBonusScaleListItemByBonusScaleID($var);
    }

    public function getObjectType() { return self::OBJECT_TYPE; }

	public function getBonusScaleByID($bonus_scale_id){
        $list_id = 'bonus_scale';
        $listObj = new PM5_LIST($list_id);

        $bonus_scale = $listObj->getListItemByID($bonus_scale_id);

        return $bonus_scale[$bonus_scale_id];

    }

	/**
	 * Get list of available bonus scales (formatted in array('id'=>'name'))
	 * @param bool $incl_blank = add the "Not applicable" option?
	 * @param bool $force_update = force an update to the session store (for use when adding new list items in Setup)
	 * @return array|mixed
	 */
	public function getListOfActiveBonusScales($incl_blank = true,$force_update = false) {
		$type = "active";
		if($force_update || !isset($_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data']) || count($_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data']) == 0 || $_SESSION[$this->getModRef()][$this->getObjectType()][$type]['expiration_timestamp'] < time()) {
			$list_id = 'bonus_scale';
			$listObj = new PM5_LIST($list_id);

			$bonus_scale = $listObj->getActiveListItemsFormattedForSelect();
			if($incl_blank===true) {
				$blank = array($this->not_applicable_id => $this->not_applicable_text);
				$bonus_scale = $blank + $bonus_scale;
			}

			$_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data'] = $bonus_scale;
			$_SESSION[$this->getModRef()][$this->getObjectType()][$type]['expiration_timestamp'] = time() + (self::SESSION_EXPIRATION_MINUTES * 60);
		} else {
			$bonus_scale = $_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data'];
		}

		return $bonus_scale;

	}

	public function getListOfAllBonusScales($incl_blank = true,$force_update = false){


		$type = "all";
		if($force_update || !isset($_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data']) || count($_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data']) == 0 || $_SESSION[$this->getModRef()][$this->getObjectType()][$type]['expiration_timestamp'] < time()) {
			$list_id = 'bonus_scale';
			$listObj = new PM5_LIST($list_id);

			$bonus_scale = $listObj->getAllListItemsFormattedForSelect();
			if($incl_blank===true) {
				$blank = array($this->not_applicable_id => $this->not_applicable_text);
				$bonus_scale = $blank + $bonus_scale;
			}

			$_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data'] = $bonus_scale;
			$_SESSION[$this->getModRef()][$this->getObjectType()][$type]['expiration_timestamp'] = time() + (self::SESSION_EXPIRATION_MINUTES * 60);
		} else {
			$bonus_scale = $_SESSION[$this->getModRef()][$this->getObjectType()][$type]['data'];
		}


		return $bonus_scale;

	}




	public function getBonusScaleForProcessing($bonus_scale_id) {
		if($bonus_scale_id>0) {

		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_bonus_scale WHERE status = ".self::ACTIVE." AND bonus_scale_id = ".$bonus_scale_id." ORDER BY start";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		$data = $this->convertBonusScaleFromEditToView($data);
		//ASSIST_HELPER::arrPrint($data);
		} else {
			$data = array();
		}

		return $data;
	}


	/******************************
	 * Functions originally in PM5
	 */

	public function calcRatingPercentage($score,$bonus_scale_id=0) {
		$divisor = 3;
		if($bonus_scale_id==0) {
			$prefObject = new PM5_SETUP_PREFERENCES();
			$divisor = $prefObject->getAnswerToQuestionByCode("na_bonus_divisor");
		} else {
			$bonus_scale = $this->getBonusScaleByID($bonus_scale_id);
			$divisor = $bonus_scale['divisor'];
		}
		return round($score/$divisor,2);
	}






	public function __destruct() {
		parent::__destruct();
	}
}




?>