<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class PM5 extends PM5_HELPER {
	/***
	 * Module Wide variables
	 */
	protected $weighted_score_rounding_precision = 3;
	protected $object_form_extra_js = "";

	protected $deliverable_types = array('DEL'  => "Deliverable With Actions",
										 'MAIN' => "Deliverable With Sub-Deliverables",
										 'SUB'  => "Sub-Deliverable With Actions",);

	protected $kpa_field;

	protected $copy_function_protected_fields = array("del_type", "del_parent_id");
	protected $copy_function_protected_heading_types = array("ATTACH", "DEL_TYPE");

	protected $has_target = false;

	protected $bonus_scale_updated_from_central_class = false;
	protected $kpa_weight = 0.8;
	protected $max_score = 5;
	protected $mid_score = 3;
	protected $default_score = 3;
	protected $min_score = 0;
	protected $bonus_scale = array('0'     => 0,
								   '1.30'  => 5,
								   '1.339' => 67,
								   '1.377' => 7,
								   '1.415' => 8,
								   '1.453' => 9,
								   '1.50'  => 10,
								   '1.535' => 11,
								   '1.569' => 12,
								   '1.603' => 13,
								   '1.637' => 14,);
	protected $default_bonus_scale = array('0'     => 0,
										   '1.30'  => 5,
										   '1.339' => 6,
										   '1.377' => 7,
										   '1.415' => 8,
										   '1.453' => 9,
										   '1.50'  => 10,
										   '1.535' => 11,
										   '1.569' => 12,
										   '1.603' => 13,
										   '1.637' => 14,);
	protected $bonus_scale_headings = array('bonus'  => "Bonus",
											'start'  => "% Performance",
											'status' => "In Use",);
	protected $bonus_max = 14;

	protected $sources = array();
	protected $jal_sources = array();
	protected $primary_source = "";
	protected $jal_primary_source = "";
	protected $src_ref = "";

	protected $line_weight_precision_allowed = 4;
	protected $score_precision_allowed = 3;

	protected $module_settings;
	protected $types = array("KPI","TOP","PROJ");
	protected $names = array('KPI'=>"Operational Indicators",'TOP'=>"Strategic Indicators",'PROJ'=>"Capital Indicators");

	protected $steps = array("",
							 "Select Operational Indicators",
							 "Select Strategic Indicators",
							 "Select Capital Indicators",
							 "Weight Indicators",
							 "Select Core Competencies",
							 "Weight Core Competencies",
							 "Select Job Activities",
							 "Weight Job Activities",
							 "Weight Components",
							 "Finalise",);

	/**
	 * Module Wide Constants
	 * */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;

	const AWAITING_APPROVAL = 16;
	const APPROVED = 128;

	const CC_REFTAG = "CC";


	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref="") {
		parent::__construct($modref);
		unset($this->steps[0]);
		$this->checkSettings();

	}

	private function checkSettings() {
		if(!isset($_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp'])
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp']<time()
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data']===false
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data']['SETUP_COMPLETE']===false
		) {
			$setupObject = new PM5_SETUP_MODULE($this->getLocalModRef());
			$this->module_settings = $setupObject->getSettings(true);
		} else {
			$this->module_settings = $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data'];
		}
	}

	public function getWeightedScoreRoundingPrecision() {
		return $this->weighted_score_rounding_precision;
	}


	/*******
	 * Functions to manage Bonus Scale
	 * Added by JC in March 2018 - based on Custom Result Settings in SDBP5B_HELPER
	 *
	 * Partially MADE OBSOLETE BY CHANGES IMPLEMENTED IN May 2019 - JC + TM - AA-90 + AA-106
	 *
	 */

	public function getDefaultBonusScale() {
		return $this->default_bonus_scale;
	}

	public function getBonusScaleHeadings() {
		return $this->bonus_scale_headings;
	}

	public function getBonusMax() {
		return $this->bonus_max;
	}

	public function canICustomBonusScale() {/*
		$sql = "SHOW TABLES LIKE '".$this->getDBRef()."_setup_bonus_scale'";
		$check_for_result_settings_table = $this->mysql_fetch_all($sql);
		if(count($check_for_result_settings_table)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_setup_bonus_scale";
			$rows = $this->mysql_fetch_all($sql);
			if(count($rows)>0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}*/
		return true;
	}

	public function getCustomBonusScaleDirectlyFromDatabase($for_editing = false) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_bonus_scale ".(!$for_editing ? " WHERE status=2" : "")." ORDER BY bonus";
		$data = $this->mysql_fetch_all_by_id($sql, "bonus");
		if($for_editing) {
			$bonus_scale = $data;
		} else {
			$bonus_scale = $this->convertBonusScaleFromEditToView($data);
		}
		return $bonus_scale;
	}

	public function getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, $for_editing = false) {
		$sql = "SELECT * 
				FROM ".$this->getDBRef()."_setup_bonus_scale ".(!$for_editing ? " WHERE status=2" : "").(!$for_editing ? " AND" : " WHERE")." bonus_scale_id = $bonus_scale_id ORDER BY start";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		if($for_editing) {
			$bonus_scale = $data;
		} else {
			$bonus_scale = $this->convertBonusScaleFromEditToView($data);
		}
		return $bonus_scale;
	}

	protected function convertBonusScaleFromEditToView($result_settings) {
		$bonus_scale = array();
		foreach($result_settings as $i => $setting) {
			if($setting['status'] > 0) {
				$key = (($setting['start']) * 1) / 100;
				$bonus = $setting['bonus'];
				$bonus_scale[strval($key)] = $bonus;
			}
		}
		return $bonus_scale;
	}

	public function saveCustomBonusScale($var) {
		$result = array("error", "An error occured while trying to save your update.  Please try again.");
//return array('result_message'=>$result);
		$old = $this->getCustomBonusScaleDirectlyFromDatabase(true);
		$result_settings = $old;
		$changes = array();

		foreach($result_settings as $i => $setting) {
			if((int)$i != 0) {
				$key = "status";
				$var[$key][$i] *= 2;
				if($var[$key][$i] != $result_settings[$i][$key]) {
					$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
					$result_settings[$i][$key] = $var[$key][$i];
				}
				if($var[$key][$i] > 0) {
					$key = "start";
					if($var[$key][$i] != $result_settings[$i][$key]) {
						$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
					}
				}
			}
		}

		foreach($result_settings as $i => $setting) {
			$id = $setting['id'];
			$key = "start";
			$value = $setting[$key];
			$setting[$key] = ASSIST_HELPER::code($value);

			$sql = "UPDATE ".$this->getDBRef()."_setup_bonus_scale SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = ".$id;
			$this->db_update($sql);

			//LOG CHANGES!!!
			if(isset($changes[$i]) && count($changes[$i]) > 0) {
				foreach($changes[$i] as $key => $value) {
					if($key == "start") {
						$v_log = array('text' => "Updated ".$i."% Bonus percentage performance bracket to start at <i>".number_format($value['new'], 2)."%</i> from <i>".number_format($value['old'], 2)."%</i>.",
									   'old'  => $value['old'],
									   'new'  => $value['new'],);
					} else {
						if($value['old'] == 0) {
							$v_log = array('text' => "Activated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						} else {
							$v_log = array('text' => "Deactivated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						}
					}
					//$this->logChanges('SETUP',0,$v_log,code($sql));
					$c = array('response' => $v_log['text'],);
					$log_changes = array_merge(array('user' => $this->getUserName()), $c);
					$log_var = array('section'   => "BS",
									 'object_id' => $id,
									 'changes'   => $log_changes,
									 'log_type'  => PM5_LOG::EDIT,);
					$this->addActivityLog("setup", $log_var);

				}
			}
		}

		//update session
		$this->updateMyActiveBonusScale($result_settings, true);

		$result = array("ok",
						"Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message' => $result, 'bonus_scale' => $result_settings);
	}

	public function saveCustomBonusScaleListItemByBonusScaleID($var) {
		$bonus_scale_id = $var['bonus_scale_id'];
		$old = $this->getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, true);
		$result_settings = $old;
		$changes = array();

		foreach($result_settings as $i => $setting) {
			$key = "status";
			if(in_array($i, $var[$key])) {
				$var[$key][$i] *= 2;
				if($var[$key][$i] != $result_settings[$i][$key]) {
					$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
					$result_settings[$i][$key] = $var[$key][$i];
				}
				if($var[$key][$i] > 0) {
					$key = "start";
					if($var[$key][$i] != $result_settings[$i][$key]) {
						$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
					}
				}
			}
		}

		foreach($result_settings as $i => $setting) {
			$id = $setting['id'];
			$key = "start";
			$value = $setting[$key];
			$setting[$key] = ASSIST_HELPER::code($value);

			$sql = "UPDATE ".$this->getDBRef()."_setup_bonus_scale SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = $id AND bonus_scale_id = $bonus_scale_id";
			$this->db_update($sql);

			//LOG CHANGES!!!
			if(isset($changes[$i]) && count($changes[$i]) > 0) {
				foreach($changes[$i] as $key => $value) {
					if($key == "start") {
						$v_log = array('text' => "Updated ".$i."% Bonus percentage performance bracket to start at <i>".number_format($value['new'], 2)."%</i> from <i>".number_format($value['old'], 2)."%</i>.",
									   'old'  => $value['old'],
									   'new'  => $value['new'],);
					} else {
						if($value['old'] == 0) {
							$v_log = array('text' => "Activated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						} else {
							$v_log = array('text' => "Deactivated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						}
					}
					//$this->logChanges('SETUP',0,$v_log,code($sql));
					$c = array('response' => $v_log['text'],);
					$log_changes = array_merge(array('user' => $this->getUserName()), $c);
					$log_var = array('section'   => "BS",
									 'object_id' => $id,
									 'changes'   => $log_changes,
									 'log_type'  => PM5_LOG::EDIT,);
					$this->addActivityLog("setup", $log_var);

				}
			}
		}

		//update session
		$this->updateMyActiveBonusScale($result_settings, true);

		$result = array("ok",
						"Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message' => $result, 'bonus_scale' => $result_settings);
	}

	public function updateMyActiveBonusScale($result_settings = array(), $convert = false) {
		if(count($result_settings) > 0) {
			$_SESSION[$this->getModRef()]['bonus_scale']['data'] = $convert ? $this->convertBonusScaleFromEditToView($result_settings) : $result_settings;
		} else {
			$_SESSION[$this->getModRef()]['bonus_scale']['data'] = $this->default_result_settings;
		}
		$_SESSION[$this->getModRef()]['bonus_scale']['expiration'] = time() + 30 * 60;
	}

	public function getMyActiveBonusScale($return = false) {
		//if the session has been set & the expiration time hasn't expired
		if(isset($_SESSION[$this->getModRef()]['bonus_scale']['expiration']) && $_SESSION[$this->getModRef()]['bonus_scale']['expiration'] > time() && isset($_SESSION[$this->getModRef()]['bonus_scale']['data'])) {
			//use the current settings
			$this->bonus_scale = $_SESSION[$this->getModRef()]['bonus_scale']['data'];
		} else {
			//check if table exists & is populated
			$check_for_result_settings_table = $this->canICustomBonusScale();
			if($check_for_result_settings_table) {
				//get from database
				$this->bonus_scale = $this->getCustomBonusScaleDirectlyFromDatabase();
				//update session
				$this->updateMyActiveBonusScale($this->bonus_scale, false);
			} else {
				//else use default
				$this->bonus_scale = $this->default_bonus_scale;
				//update session to prevent multiple database checks
				$this->updateMyActiveBonusScale($this->bonus_scale, false);
			}
		}
		if($return) {
			return $this->bonus_scale;
		}
	}









	public function getLineWeightPrecisionAllowed() { return $this->line_weight_precision_allowed; }
	public function getScorePrecisionAllowed() { return $this->score_precision_allowed; }
	public function getObjectTypes() { return $this->types; }
	public function getObjectNames() { return $this->names; }



	/**************************
	 * SDBIP functions
	 */


	/**
	 * Function to check the SESSION for sources of KPIs/Projects and update if expired
	 * Applicable to SDBIP modules
	 */
	private function checkKPASources($force = false) { //$force=true; //ASSIST_HELPER::arrPrint($this->module_settings);
		if($force || !isset($_SESSION[$this->getModRef()]['SOURCES']['KPA']) || $_SESSION[$this->getModRef()]['SOURCES']['KPA']['expiration'] < time()) {
			$this->checkSettings();
			if($this->module_settings !== false) {
				$settings = $this->module_settings;
				//set sources
				if($settings['ORG_SOURCE'] != false && strlen($settings['ORG_SOURCE']['value']) > 0) {
					$set = $settings['ORG_SOURCE'];
					$org_source = array('active' => true,
						'modref' => $set['value'],
						'modlocation' => $set['setting'],
						'modtext' => $set['name'],
						'modobject' => $set['object_id'],
						'type' => "ORG",
					);
					$this->sources[$set['value']] = $org_source;
				}
				if($settings['IKPI_SOURCE'] != false && strlen($settings['IKPI_SOURCE']['value']) > 0) {
					$set = $settings['IKPI_SOURCE'];
					$ikpi_source = array('active' => true,
						'modref' => $set['value'],
						'modlocation' => $set['setting'],
						'modtext' => $set['name'],
						'modobject' => $set['object_id'],
						'type' => "IKPI",
					);
					$this->sources[$set['value']] = $ikpi_source;
				}
				//set primary source
				switch($settings['PRIMARY']['value']) {
					case "ORG":
						$this->primary_source = $org_source['modref'];
						break;
					case "IKPI":
						$this->primary_source = $ikpi_source['modref'];
						break;
				}
				//kpa field
				$this->kpa_field = array('field' => $settings['GROUPING']['value'], 'type' => $settings['GROUPING']['setting']);
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['data'] = $this->sources;
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['field'] = $this->kpa_field;
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['expiration'] = strtotime(date("d F Y H:i:s")) + (30 * 60);
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['primary'] = $this->primary_source;
			}
		} else {
			$this->sources = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['data'];
			$this->kpa_field = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['field'];
			$this->primary_source = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['primary'];
		}
	}

	public function getKPAField() {

	}


	/**
	 * Function to retrieve KPI/Project sources
	 * Applicable to SDBIP modules
	 */
	public function getKPASources() {
		$this->checkKPASources();
		$data = array();
		foreach($this->sources as $src) {
			if($src['active']) {
				$data[] = $src;
			}
		}
		return $data;
	}

	/**
	 * Applicable to SDBIP modules
	 */
	public function getAKPASourceName($src) {
		$this->getKPASources();
		return $this->sources[$src]['modtext'];
	}

	/**
	 * Applicable to SDBIP modules
	 */
	public function getPrimaryKPASource() {
		$this->getKPASources();
		return $this->primary_source;
	}

	public function getPrimarySourceModLoc() {
		switch($this->module_settings['PRIMARY']['value']) {
			case "ORG":
				return $this->module_settings['ORG_SOURCE']['setting'];
				break;
			case "IKPI":
				return $this->module_settings['IKPI_SOURCE']['setting'];
				break;
		}
	}

	public function getSourceModloc($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['setting'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['setting'];
		}
	}
	public function getSourceObject($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['object_id'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['object_id'];
		}
	}
	public function getSourceSDBIPID($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['object_id'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['object_id'];
		}
	}
	public function getKPAs() {
		return $this->getGroupList();
	}
	public function getGroupList() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_group ORDER BY name DESC";
		$rows =  $this->mysql_fetch_value_by_id($sql, "id", "name");
		$rows[0] = $this->getUnspecified();
		return $rows;
	}

	public function getGroupMapping($filter_modref="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_group_map WHERE (grp_status & 2) = 2 ".(strlen($filter_modref)>0?" AND grp_src_modref = '$filter_modref'":"");
		return $this->mysql_fetch_all($sql);
	}

	public function getCompleteGroupingMap($filter_modref="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_group_map GM
				INNER JOIN ".$this->getDBRef()."_list_group G
				ON G.id = GM.grp_local_id
				WHERE (GM.grp_status & 2) = 2
				AND (G.status & 2) = 2 
				";
		if(strlen($filter_modref)>0) {
			$sql.=" AND GM.grp_src_modref = '".$filter_modref."'";
		}
		$list = $this->mysql_fetch_all_by_id($sql, "grp_id");
		foreach($list as $i => $l) {
			$list[$i]['comparison'] = $this->formatTextForComparison($l['name']);
		}
		return $list;
	}

	public function getGroupMapConvertingSourceToLocal($modref) {
		$map = $this->getGroupMapping($modref);
		$data = array();
		foreach($map as $m) {
			$data[$m['grp_src_id']] = $m['grp_local_id'];
		}
		return $data;
	}

	public function getGroupingField() {
		return $this->module_settings['GROUPING']['value'];
	}


	public function updateGroupingList($group,$src_modref,$src_modloc) {
		$current_list = $this->getGroupList();
		$current_list_for_comparison = array();
		foreach($current_list as $i => $n) {
			$current_list_for_comparison[$this->formatTextForComparison($n)] = $i;
		}
		$current_map = $this->getGroupMapping($src_modref);
		$processed_map = array();
		foreach($current_map as $map) {
			$processed_map[$map['grp_src_id']] = $map['grp_local_id'];
		}
		foreach($group as $src_id => $name) {
			if(!isset($current_list_for_comparison[$this->formatTextForComparison($name)])) {
				//insert new record - no coding - should have come raw out of the DB and already been encoded before being saved originally
				$sql = "INSERT ".$this->getDBRef()."_list_group SET name = '$name', status = '".self::ACTIVE."', code = ''";
				$i = $this->db_insert($sql);
				$current_list_for_comparison[$this->formatTextForComparison($name)] = $i;
				//insert grouping map
				$sql = "INSERT INTO ".$this->getDBRef()."_setup_group_map SET grp_src_modref = '$src_modref', grp_src_modloc = '$src_modloc', grp_src_id = '$src_id', grp_local_id = '$i', grp_status = ".self::ACTIVE.", grp_insertuser = '".$this->getUserID()."', grp_insertdate = now()";
				$this->db_insert($sql);
			} elseif(!isset($processed_map[$src_id])) {
				$local_id = $current_list_for_comparison[$this->formatTextForComparison($name)];
				//insert grouping map
				$sql = "INSERT INTO ".$this->getDBRef()."_setup_group_map SET grp_src_modref = '$src_modref', grp_src_modloc = '$src_modloc', grp_src_id = '$src_id', grp_local_id = '$local_id', grp_status = ".self::ACTIVE.", grp_insertuser = '".$this->getUserID()."', grp_insertdate = now()";
				$this->db_insert($sql);
			}
		}

	}




	/**************************
	 * JAL functions
	 */


	/**
	 * Function to check the SESSION for sources of Activities and update if expired
	 * Applicable to JAL modules
	 */
	private function checkJALSources() {
		//If SESSION doesn't hold any sources then get fresh from DB
		unset($_SESSION[$this->getModRef()]['SOURCES']['JAL']); //dev purposes
		if(!isset($_SESSION[$this->getModRef()]['SOURCES']['JAL'])) {
			$this->updateJALSources();
		} else {
			$s = $_SESSION[$this->getModRef()]['SOURCES']['JAL'];
			$now = strtotime(date("d F Y H:i:s"));
			//Check if expired
			if($s['expiration'] < $now) {
				$this->updateJALSources();
			} else {
				$this->jal_sources = $s['data'];
				$this->jal_primary_source = $s['primary'];
			}
		}
	}

	/**
	 * Function to update the SESSION stored sources of KPIs/Projects if expired or not yet created
	 * Applicable to JAL modules
	 */
	private function updateJALSources() {
		$this->getJALSourcesFromDB();
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['data'] = $this->sources;
		//Set expiration of data to now + 30 minutes
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['expiration'] = strtotime(date("d F Y H:i:s")) + (30 * 60);
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['primary'] = $this->primary_source;
	}

	/**
	 * Function to get latest sources of KPIs/Projects
	 * Applicable to JAL modules
	 */
	private function getJALSourcesFromDB() {
		$this->jal_sources = array();
		$sql = "SELECT * FROM assist_menu_modules
				WHERE (
					modlocation LIKE 'JAL%'
				) AND modyn = 'Y'
				ORDER BY modref"; //echo $sql;
		$srcs = $this->mysql_fetch_all_by_id($sql, "modref");  //$this->arrPrint($srcs);
		$found_primary = false;
		foreach($srcs as $s => $row) {
			if($found_primary === false) {
				$this->jal_primary_source = $s;
				$found_primary = true;
			}
			$this->jal_sources[$s] = array('active'      => true,
										   'modref'      => $s,
										   'modlocation' => $srcs[$s]['modlocation'],
										   'modtext'     => $srcs[$s]['modtext']);
		}
	}

	/**
	 * Function to retrieve Activity sources
	 * Applicable to JAL modules
	 */
	public function getJALSources() {
		$this->checkJALSources();
		$data = array();
		foreach($this->jal_sources as $src) {
			if($src['active']) {
				$data[] = $src;
			}
		}
		return $data;
	}

	/**
	 * Applicable to JAL modules
	 */
	public function getAJALSourceName($src) {
		$this->getJALSources();
		return $this->jal_sources[$src]['modtext'];
	}

	/**
	 * Applicable to JAL modules
	 */
	public function getPrimaryJALSource() {
		$this->getJALSources();
		return $this->jal_primary_source;
	}


	/****************************
	 * GENERAL FUNCTIONS
	 */

	public function getSteps() {
		return $this->steps;
	}

	public function getStep($s) {
		return $this->steps[$s];
	}

	public function getNumberOfSteps() {
		return (count($this->steps));
	}

	public function getMidScore() {
		return $this->mid_score;
	}

	public function getDefaultScore() {
		return $this->default_score;
	}

	public function getFinalWeights() {
		$result = array('kpa' => $this->kpa_weight,);
		$result['cc'] = 1 - $result['kpa'];
		return $result;
	}


	public function calcBonusPercentage($r, $bonus_scale_id = 0) {
		$result = "0%";
		if($this->bonus_scale_updated_from_central_class === false) {
			$listObject = new PM5_SETUP_BONUS();
			$this->bonus_scale = $listObject->getBonusScaleForProcessing($bonus_scale_id);
		}
		foreach($this->bonus_scale as $min => $bonus) {
			if(($min * 1) <= $r) {
				$result = $bonus;
			} else {
				break;
			}
		}
		return $result;
	}


	public function getBonusRatings($bonus_scale_id = 0) {
		if($this->bonus_scale_updated_from_central_class === false) {
			$listObject = new PM5_SETUP_BONUS();
			$this->bonus_scale = $listObject->getBonusScaleForProcessing($bonus_scale_id);
		}
		$b = array();
		$a = 0;
		foreach($this->bonus_scale as $min => $bonus) {
			if($a > 0) {
				$b[$a - 1]['max'] = ($min - 0.0001);
			}
			$x = array('min'   => $min,
					   'max'   => false,
					   'bonus' => $bonus,);
			$b[$a] = $x;
			$a++;
		}
		return $b;
	}

	public function getBonusRatingTable($bonus_scale_id = 0) {

		if($bonus_scale_id > 0) {

			$bonusObject = new PM5_SETUP_BONUS();
			$bonus_scale_details = $bonusObject->getBonusScaleByID($bonus_scale_id);

//		ASSIST_HELPER::arrPrint($bonus_scale_details);

			$bonusperc = $this->getBonusRatings($bonus_scale_id);
			$echo = "
			<h3>Bonus Table</h3>
		<table>
			<tr>
				<th width='120px'>% Performance</th>
				<th>Bonus</th>
			</tr>
			";
			foreach($bonusperc as $b) {
				//if($b['bonus']>0) {
				$min = number_format(($b['min'] * 100), 2);
				$max = $b['max'] !== false ? " - ".number_format(($b['max'] * 100), 2)."%" : "+";
				$td_size = "";
				if(strlen($b['bonus'])>50) {
					$td_size = " width=150px ";
				}
				$echo .= "
				<tr>
					<td>".$min."%".$max."</td>
					<td $td_size >".$b['bonus']."</td>
				</tr>";
				//}
			}
			$echo .= "
		</table>";
		} else {
			$echo = "";
		}
		return $echo;
	}


	/*********************
	 * MODULE OBJECT functions
	 * for CONTRACT, DELIVERABLE AND ACTION
	 */
	public function getObject($var) { //$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'], $options);
				if($var['type'] == "LIST" || $var['type'] == "ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data, $this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details) > 0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'], $var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'], $var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}

	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}

	public function getProgressFieldName() {
		return $this->progress_field;
	}

	public function getIDFieldName() {
		return $this->id_field;
	}

	public function getParentFieldName() {
		return $this->parent_field;
	}

	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}

	public function getNameFieldName() {
		return $this->name_field;
	}

	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}

	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}

	public function getOwnerFieldName() {
		return $this->owner_field;
	}

	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}

	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}

	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}

	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}

	public function getPageLimit() {
		$profileObject = new PM5_PROFILE();
		return $profileObject->getProfileTableLength();
	}

	public function getDateOptions($fld) {
		if(stripos($fld, "action_on") !== FALSE) {
			$fld = "action_on";
		}
		switch($fld) {
			case "action_on":
				return array('maxDate' => "'+0D'");
				break;
			default:
				return array();
		}
	}

	public function getExtraObjectFormJS() {
		return $this->object_form_extra_js;
	}

	public function getAllDeliverableTypes() {
		return $this->deliverable_types;
	}

	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key => $t) {
			$arr[] = $t;
		}
		return $arr;
	}

	public function getDeliverableTypes($contract_id) {
		$contractObject = new PM5_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables() == FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types;
	}

	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt, $ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE", $options);
		//$this->arrPrint($data);
		$result = array('past'    => 0,
						'present' => 0,
						'future'  => 0,);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z < $now) {
				$result['past']++; //echo "past";
			} elseif($z > $now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE", $options);
		return $data;
	}

	public function getMyList($obj_type, $section, $options = array()) {
		$final_data = array();
		/*
				//echo "<P>section: ".$section;
				//echo "<P>options: "; $this->arrPrint($options);
				if(isset($options['page'])) {
					$page = strtoupper($options['page']);
					unset($options['page']);
				} else {
					$page = "DEFAULT";
				}
				$trigger_rows = true;
				if(isset($options['trigger'])) {
					$trigger_rows = $options['trigger'];
					unset($options['trigger']);
				}
				//echo $page;
				if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
					$future_deadline = $options['deadline'];
					unset($options['deadline']);
					if(!isset($options['limit'])) { $options['limit'] = false; }
				}
				$compact_details = ($page == "CONFIRM" || $page == "ALL");
				if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
					$pagelimit = false;
					$start = 0;
					$limit = false;
					unset($options['start']);
					unset($options['limit']);
				}else{
					$pagelimit = $this->getPageLimit();
					$start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
					$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
					if($start<0) {
						$start = 0;
						$limit = false;
					}
				}
				$order_by = ""; //still to be processed
				$left_joins = array();

				$headObject = new PM5_HEADINGS();
				$final_data = array('head'=>array(),'rows'=>array());

				//set up contract variables
				$c_headings = $headObject->getMainObjectHeadings("CONTRACT",($obj_type=="CONTRACT"?"LIST":"FULL"),$section);
				$cObject = new PM5_CONTRACT();
				$c_tblref = "C";
				$c_table = $cObject->getTableName();
				$c_id = $c_tblref.".".$cObject->getIDFieldName();
				$c_status = $c_tblref.".".$cObject->getStatusFieldName();
				$c_status_fld = $c_tblref.".".$cObject->getProgressStatusFieldName();
				$c_name = $cObject->getNameFieldName();
				$c_deadline = $cObject->getDeadlineFieldName();
				$c_field = $c_tblref.".".$cObject->getTableField();

				$where = $cObject->getActiveStatusSQL($c_tblref);

				$d_tblref = "D";
				$dObject = new PM5_DELIVERABLE();
				$d_table = $dObject->getTableName();
				$d_id = $d_tblref.".".$dObject->getIDFieldName();
				$d_status = $d_tblref.".".$dObject->getStatusFieldName();
				$d_parent = $d_tblref.".".$dObject->getParentFieldName();

				$a_tblref = "A";
				$aObject = new PM5_ACTION();
				$a_table = $aObject->getTableName();
				$a_id = $a_tblref.".".$aObject->getIDFieldName();
				$a_status = $a_tblref.".".$aObject->getStatusFieldName();
				$a_parent = $a_tblref.".".$aObject->getParentFieldName();
				$a_progress = $a_tblref.".".$aObject->getProgressFieldName();
				$a_progress2 = $aObject->getProgressFieldName();


				//if type is something other than contract
				if($obj_type!="CONTRACT") {
					//setup deliverable variables
					$d_headings = $headObject->getMainObjectHeadings("DELIVERABLE",($obj_type=="DELIVERABLE" ? "LIST" : "FULL"),$section);
					$a_status_fld = $d_tblref.".".$dObject->getProgressStatusFieldName();
					$d_name = $dObject->getNameFieldName();
					$d_deadline = $dObject->getDeadlineFieldName();
					$d_field = $d_tblref.".".$dObject->getTableField();
					$where.=" AND ".$dObject->getActiveStatusSQL($d_tblref);
					//setup action variables
					if($obj_type=="ACTION") {
						$a_status_fld = $a_tblref.".".$aObject->getProgressStatusFieldName();
						$a_name = $aObject->getNameFieldName();
						$a_deadline = $aObject->getDeadlineFieldName();
						$a_field = $a_tblref.".".$aObject->getTableField();
						$a_headings = $headObject->getMainObjectHeadings("ACTION","LIST",$section);
						$id_fld = $aObject->getIDFieldName();
						if(!$compact_details){
							$sql ="SELECT DISTINCT $a_status as my_status, ".$a_tblref.".*
							, IF(".$a_tblref.".action_target>0,CONCAT(".$a_tblref.".action_target,' ',".$a_tblref.".action_target_unit),'N/A') as action_target
							, IF(".$a_tblref.".action_actual>0,CONCAT(".$a_tblref.".action_actual,' ',".$a_tblref.".action_target_unit),IF(".$a_tblref.".action_target>0,CONCAT('0 ',".$a_tblref.".action_target_unit),'N/A')) as action_actual
							, CONCAT(".$d_tblref.".".$d_name.",' [".$dObject->getRefTag()."',".$d_id.",']') as ".$d_name."
							, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name;
						}else{
							$sql ="SELECT DISTINCT $a_status as my_status, ".$a_tblref.".* ";
						}
						$from = " $a_table $a_tblref INNER JOIN $d_table $d_tblref ON $d_id = $a_parent INNER JOIN $c_table $c_tblref ON $c_id = $d_parent ";
						$where.= " AND ".$aObject->getActiveStatusSQL($a_tblref);
						$final_data['head'][$id_fld] = $a_headings[$id_fld];
						if(!$compact_details){
							$final_data['head'][$c_name] = $c_headings[$c_name];
							$final_data['head'][$d_name] = $d_headings[$d_name];
						}
						foreach($a_headings as $fld=>$head){
							if($fld!=$id_fld) {
								$final_data['head'][$fld] = $head;
							}
						}
						$where_tblref = $a_tblref;
						$where_deadline = $a_deadline;
						$ref_tag = $aObject->getRefTag();
						if($page=="LOGIN_TABLE") {
							$order_by = $a_tblref.".".$a_deadline;
						}
						$status_id_fld = $aObject->getProgressStatusFieldName();
						$where_status_id_fld = $a_tblref.".".$aObject->getProgressStatusFieldName();
						$where_status_fld = $a_tblref.".".$aObject->getStatusFieldName();
					} else {
						$sql ="SELECT DISTINCT $d_status as my_status
								, ".$d_tblref.".*
								, AVG(".$a_progress.") as ".$a_progress2."
								, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name;
						$a_headings = array();
						$from = " $d_table $d_tblref
								INNER JOIN $c_table $c_tblref ON $c_id = $d_parent
								LEFT OUTER JOIN $a_table $a_tblref
								  ON $a_parent = $d_id AND ( $a_status & ".PM5::ACTIVE." ) = ".PM5::ACTIVE."
						";
						$id_fld = $dObject->getIDFieldName();
						$final_data['head'][$id_fld] = $d_headings[$id_fld];
						$final_data['head'][$c_name] = $c_headings[$c_name];
						foreach($d_headings as $fld=>$head){
							if($fld!=$id_fld) {
								$final_data['head'][$fld] = $head;
							}
						}
						$where_tblref = $d_tblref;
						$where_deadline = $d_deadline;
						$ref_tag = $dObject->getRefTag();
						if($page=="LOGIN_TABLE") {
							$order_by = $d_tblref.".".$d_deadline;
						}
						$where_status_id_fld = $d_tblref.".".$dObject->getProgressStatusFieldName();
						$status_id_fld = $dObject->getProgressStatusFieldName();
						$where_status_fld = $d_tblref.".".$dObject->getStatusFieldName();
						$group_by = "GROUP BY ".$d_id;
					}
				} else {
					$id_fld = $cObject->getIDFieldName();
					$sql = "SELECT DISTINCT $c_status as my_status, ".$c_tblref.".*, AVG(".$a_progress.") as ".$a_progress2;
					$d_headings = array();
					$a_headings = array();
					$from = " $c_table $c_tblref
					LEFT OUTER JOIN $d_table $d_tblref
					  ON $d_parent = $c_id AND ( $d_status & ".PM5::ACTIVE." ) = ".PM5::ACTIVE."
					LEFT OUTER JOIN $a_table $a_tblref
					  ON $a_parent = $d_id AND ( $a_status & ".PM5::ACTIVE." ) = ".PM5::ACTIVE."
					";
					$final_data['head'] = $c_headings;
					$where_tblref = $c_tblref;
					$where_deadline = $c_deadline;
					$ref_tag = $cObject->getRefTag();
					if($page=="LOGIN_TABLE") {
						$order_by = $c_tblref.".".$c_deadline;
					}
					$where_status_id_fld = $c_tblref.".".$cObject->getProgressStatusFieldName();
					$status_id_fld = $cObject->getProgressStatusFieldName();
					$where_status_fld = $c_tblref.".".$cObject->getStatusFieldName();
					$group_by = "GROUP BY ".$c_id;
				}

				if(count($options)>0) {
					foreach($options as $key => $filter) {
						if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
							$key = $this->getTableField()."_".$key;
						}
						$where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
					}
				}


					$all_headings = array_merge($c_headings,$d_headings,$a_headings);

					$listObject = new PM5_LIST();

					//$list_headings = $headObject->getAllListHeadings();
					//$list_tables = $listObject->getAllListTables(array_keys($list_headings));


					foreach($all_headings as $fld => $head) {
						$lj_tblref = substr($head['section'],0,1);
						if($head['type']=="LIST") {
							$listObject->changeListType($head['list_table']);
							$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
							$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])."
												AS ".$head['list_table']."
												ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
												AND (".$head['list_table'].".status & ".PM5::DELETED.") <> ".PM5::DELETED;
						} elseif($head['type']=="MASTER") {
							$tbl = $head['list_table'];
							$masterObject = new PM5_MASTER($fld);
							$fy = $masterObject->getFields();
							$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
							$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
						} elseif($head['type']=="USER") {
							$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
							$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
						} elseif($head['type']=="OWNER") {
							$ownerObject = new PM5_CONTRACT_OWNER();
							$tbl = $head['list_table'];
							$dir_tbl = $fld."_parent";
							$sub_tbl = $fld."_child";
							$own_tbl = $fld;
							$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
							$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$lj_tblref.".".$fld." = ".$sub_tbl.".subid";
							$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
							$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
						}
					}
					$sql.= " FROM ".$from.implode(" ",$left_joins);
					$sql.= " WHERE ".$where;
					switch($section) {
						case "ACTIVE":
							$sql.=" AND ".$cObject->getActiveStatusSQL("C");
							if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
								$sql.=" AND ".$dObject->getActiveStatusSQL($d_tblref);
							}
							if($obj_type=="ACTION") {
								$sql.=" AND ".$aObject->getActiveStatusSQL($a_tblref);
							}
							break;
						case "NEW":
							if($page=="ACTIVATE_WAITING") {
								$sql.=" AND ".$cObject->getActivationStatusSQL("C");
							} elseif($page=="ACTIVATE_DONE") {
								$sql.=" AND ".$cObject->getReportingStatusSQL("C");
							} elseif($page=="COPY_CONTRACT") {
								$sql.=" AND ".$cObject->getActiveStatusSQL("C");
							} else {
								$sql.=" AND ".$cObject->getNewStatusSQL("C");
								if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
									$sql.=" AND ".$dObject->getActiveStatusSQL($d_tblref);
								}
								if($obj_type=="ACTION") {
									$sql.=" AND ".$aObject->getActiveStatusSQL($a_tblref);
								}
							}
							switch($page) {
								case "COPY_CONTRACT":
									break;
								case "EDIT_CONTRACT":
								case "EDIT":
								case "CONFIRM":
								//case "FINANCE":
									$sql.= " AND ".$c_tblref.".contract_manager = '".$this->getUserID()."'";
									break;
								case "ACTIVATE_WAITING":
								case "ACTIVATE_DONE":
									$sql.= " AND ".$c_tblref.".contract_authoriser = '".$this->getUserID()."'";
									break;
								case "ADD_DELIVERABLE":
								case "ADD_ACTION":
								case "DEFAULT":
								default:
									//$sql.= ($obj_type=="DELIVERABLE" ? "AND ".$d_tblref.".del_type <> 'MAIN'" : "")." AND ( ".$c_tblref.".contract_manager = '".$this->getUserID()."' OR ".$c_tblref.".contract_authoriser = '".$this->getUserID()."' OR ".$own_tbl.".owner_tkid = '".$this->getUserID()."' )";
									//$sql.= " AND ( ".$c_tblref.".contract_manager = '".$this->getUserID()."' OR ".$c_tblref.".contract_authoriser = '".$this->getUserID()."' OR ".$own_tbl.".owner_tkid = '".$this->getUserID()."' )";
											$sql.= " AND (
												".$c_tblref.".contract_manager = '".$this->getUserID()."'
												OR ".$c_tblref.".contract_authoriser = '".$this->getUserID()."'
												OR (
														".$own_tbl.".owner_tkid = '".$this->getUserID()."'
														AND (
															(".$own_tbl.".owner_status & ".PM5_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE.") = ".PM5_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE.")
													)
												)";
											if($page=="ADD_ACTION") {
												$sql.= "AND ".$d_tblref.".del_type <> 'MAIN'";
											}
									break;
							}
							break;
						case "MANAGE":
							//echo "<P>MANAGING BY SWITCH = ".$page;
							$sql.=($obj_type=="DELIVERABLE" ? "AND ".$d_tblref.".del_type <> 'MAIN'" : "")." AND ".$cObject->getReportingStatusSQL($c_tblref);
							if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
								$sql.=" AND ".$dObject->getReportingStatusSQL($d_tblref);
							}
							if($obj_type=="ACTION") {
								$sql.=" AND ".$aObject->getReportingStatusSQL($a_tblref);
							}
							switch($page) {
								case "UPDATE_CONTRACT":		$owner_status = PM5_CONTRACT_OWNER::CAN_UPDATE;
								case "EDIT_DELIVERABLE":	$owner_status = !isset($owner_status) ? PM5_CONTRACT_OWNER::CAN_EDIT : $owner_status;
								case "APPROVE_DELIVERABLE":	$owner_status = !isset($owner_status) ? PM5_CONTRACT_OWNER::CAN_APPROVE : $owner_status;
								case "APPROVED_DELIVERABLE":	$owner_status = !isset($owner_status) ? PM5_CONTRACT_OWNER::CAN_APPROVE : $owner_status;
									$sql.= " AND (
												".$c_tblref.".contract_manager = '".$this->getUserID()."'
												OR ".$c_tblref.".contract_authoriser = '".$this->getUserID()."'
												OR (
													".$own_tbl.".owner_tkid = '".$this->getUserID()."'
													AND ( (".$own_tbl.".owner_status & ".$owner_status.") = ".$owner_status." )
												)
											 )";
									if($page=="APPROVE_DELIVERABLE") {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::AWAITING_APPROVAL.") = ".PM5::AWAITING_APPROVAL." ) ";
									} elseif($page=="APPROVED_DELIVERABLE") {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::APPROVED.") = ".PM5::APPROVED." ) ";
									} else {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::APPROVED.") <> ".PM5::APPROVED." ) ";
									}
									break;
								case "ASSESSMENT":
								case "FINANCE":
									$sql.= " AND ".$c_tblref.".contract_manager = '".$this->getUserID()."'";
									break;
		//						case "FINANCE_UPDATE_CONTRACT":
		//							$sql.= " AND ".$c_tblref.".contract_manager = '".$this->getUserID()."'
		//									 AND ( ".$c_status." & ".PM5_CONTRACT::FINANCE_INITIATED." = ".PM5_CONTRACT::FINANCE_INITIATED." )";
		//							break;
		//						case "FINANCE_INITIATE_CONTRACT":
		//							$sql.= " AND ".$c_tblref.".contract_manager = '".$this->getUserID()."'
		//									 AND ( ".$c_status." & ".PM5_CONTRACT::FINANCE_INITIATED." <> ".PM5_CONTRACT::FINANCE_INITIATED." )";
		//							break;
								case "UPDATE_DELIVERABLE":
								case "APPROVE_ACTION":
								case "APPROVED_ACTION":
								case "EDIT_ACTION":
									$sql.= " AND ".$d_tblref.".del_owner = '".$this->getUserID()."'";
									if($page=="APPROVE_ACTION") {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::AWAITING_APPROVAL.") = ".PM5::AWAITING_APPROVAL." ) ";
									} elseif($page=="APPROVED_ACTION") {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::APPROVED.") = ".PM5::APPROVED." ) ";
									} else {
										$sql.=" AND ( (".$where_status_fld." & ".PM5::APPROVED.") <> ".PM5::APPROVED." )
												AND ( (".$where_status_fld." & ".PM5::AWAITING_APPROVAL.") <> ".PM5::AWAITING_APPROVAL.") ";
									}
									break;
								case "UPDATE_ACTION":
									$sql.= " AND ".$a_tblref.".action_owner = '".$this->getUserID()."'
											AND ( (".$a_status." & ".PM5::AWAITING_APPROVAL.") <> ".PM5::AWAITING_APPROVAL." )
											AND ( (".$a_status." & ".PM5::APPROVED.") <> ".PM5::APPROVED." ) ";
									break;
								case "LOGIN_TABLE":
								case "LOGIN_STATS":
									//all objects not completed where you are the owner for updating
									switch($obj_type) {
										case "ACTION":
											$where_deadline = $a_deadline;
											$where_status = $a_status_fld;
											$sql.= " AND ".$a_tblref.".action_owner = '".$this->getUserID()."'";
											break;
										case "DELIVERABLE":
											$where_deadline = $d_deadline;
											$sql.= " AND ".$d_tblref.".del_owner = '".$this->getUserID()."'";
											$where_status = $d_status_fld;
											break;
										case "CONTRACT":
											$where_deadline = $c_deadline;
											$sql.= " AND (
												".$c_tblref.".contract_manager = '".$this->getUserID()."'
												OR ".$c_tblref.".contract_authoriser = '".$this->getUserID()."'
												OR (
														".$own_tbl.".owner_tkid = '".$this->getUserID()."'
														AND ( (".$own_tbl.".owner_status & ".PM5_CONTRACT_OWNER::CAN_UPDATE.") = ".PM5_CONTRACT_OWNER::CAN_UPDATE.")
													)
												)";
											$where_status = $c_status_fld;
											break;
									}
									$sql.= " AND ".$where_status." <> 3 AND ".$where_tblref.".".$where_deadline." < '".date("Y-m-d",$future_deadline)."' ";
									break;
								case "VIEW":
								default:
									break;
							}
							break;
						case "ADMIN":
							$sql.=" AND ".$cObject->getReportingStatusSQL($c_tblref);
							if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
								$sql.=" AND ".$dObject->getReportingStatusSQL($d_tblref);
							}
							if($obj_type=="ACTION") {
								$sql.=" AND ".$aObject->getReportingStatusSQL($a_tblref);
							}
							switch($page) {
								case "UPDATE_ACTION":
								case "UPDATE_DELIVERABLE":
									$sql.= " AND ( (".$where_status_fld." & ".PM5::APPROVED.") <> ".PM5::APPROVED." ) ";

									break;
								case "EDIT_ACTION":
								case "EDIT_DELIVERABLE":
									/** approval status has no effect on edit function - the objects won't be available for edit but must be available to be unlocked **/
		/*							break;
							}
							break;
						case "REPORT":
						case "SEARCH":
							$sql.=" AND ".$cObject->getReportingStatusSQL($c_tblref);
							if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
								$sql.=" AND ".$dObject->getReportingStatusSQL($d_tblref);
							}
							if($obj_type=="ACTION") {
								$sql.=" AND ".$aObject->getReportingStatusSQL($a_tblref);
							}
							break;
					}
					$sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";
					$mnr = $this->db_get_num_rows($sql);
				if($trigger_rows==true) {
					$start = ($start!=false && is_numeric($start) ? $start : "0");
					$sql.=(strlen($order_by)>0 ? " ORDER BY ".$order_by : " ORDER BY ".$where_tblref.".".$where_deadline).($limit !== false?" LIMIT ".$start." , $limit ":"");
					//echo $sql;
					//return array($sql);
					$rows = $this->mysql_fetch_all_by_id($sql,$id_fld);
					$final_data = $this->formatRowDisplay($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,$status_id_fld,array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
				} else {
					$final_data['rows'] = array();
				}
				$final_data['head'] = $this->replaceObjectNames($final_data['head']);*/
		return $final_data;
	}

	function formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, $status_id_fld, $paging) {
		/*
			   $limit = $paging['limit'];
			   $pagelimit = $paging['pagelimit'];
			   $start = $paging['start'];
			   $dObject = new PM5_DELIVERABLE();
				   //ASSIST_HELPER::arrPrint($rows);
				   $keys = array_keys($rows);
				   $displayObject = new PM5_DISPLAY();
				   foreach($rows as $r) {
					   $row = array();
					   $i = $r[$id_fld];
					   foreach($final_data['head'] as $fld=> $head) {
						   if($head['parent_id']==0){
							   if($headObject->isListField($head['type']) && $head['type']!="DELIVERABLE") {
								   $row[$fld] = $r[$head['list_table']];
								   if($fld==$status_id_fld) {
									   if(($r['my_status'] & PM5::AWAITING_APPROVAL) == PM5::AWAITING_APPROVAL) {
										   $row[$fld].=" (Awaiting approval)";
									   } elseif(($r['my_status'] & PM5::APPROVED) == PM5::APPROVED) {
										   $row[$fld].= " (Approved)";
									   }
								   }
							   } elseif($this->isDateField($fld)) {
								   $row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
							   } elseif($head['type']=="DELIVERABLE") {
								   if($r["del_type"]=='SUB') {
									   if($r[$fld]==0) {
										   $row[$fld] = $this->getUnspecified();
									   } else {
										   if(!isset($deliverable_names_for_subs)) {
											   $deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($keys);
										   }
										   $row[$fld] = $deliverable_names_for_subs[$r[$fld]];
									   }
								   } else {
									   $row[$fld] = "N/A";
								   }
							   } else {
								   //$row[$fld] = $r[$fld];
								   $row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
							   }
						   }
					   }
					   $final_data['rows'][$i] = $row;
					   //$mnr++;
				   }

				   if($mnr==0 || $limit === false) {
					   $totalpages = 1;
					   $currentpage = 1;
				   } else {
					   $totalpages = round(($mnr/$pagelimit),0);
					   $totalpages += (($totalpages*$pagelimit)>=$mnr ? 0 : 1);
					   if($start==0) {
						   $currentpage = 1;
					   } else {
						   $currentpage = round($start/$pagelimit,0);
						   $currentpage += (($currentpage*$pagelimit)>$start ? 0 : 1);
					   }
				   }
				   $final_data['paging'] = array(
					   'totalrows'=>$mnr,
					   'totalpages'=>$totalpages,
					   'currentpage'=>$currentpage,
					   'pagelimit'=>$pagelimit,
					   'first'=>($start==0 ? false : 0),
					   'prev'=>($start==0 ? false : ($start-$pagelimit)),
					   'next'=>($totalpages==$currentpage ? false : ($start+$pagelimit)),
					   'last'=>($currentpage==$totalpages ? false : ($totalpages-1)*$pagelimit),
				   );*/
		return $final_data;
	}


	public function getDetailedObject($obj_type, $id, $options = array()) { //ASSIST_HELPER::arrPrint($options);
		$final_data = array();
		/*
			$left_joins = array();

			if(isset($options['compact_view'])) {
				$compact_view = $options['compact_view'];
			} else {
				$compact_view = false;
			}

			if(isset($options['type'])) {
				if($options['type']=="EMAIL"){
					$format_for_emails = true;
				}
				unset($options['type']);
			} else {
				$format_for_emails = false;
			}
			if(isset($options['attachment_buttons'])) {
				$attachment_buttons = $options['attachment_buttons'];
				unset($options['attachment_buttons']);
			} else {
				$attachment_buttons = true;
			}

			$headObject = new PM5_HEADINGS();
			$final_data = array('head'=>array(),'rows'=>array());

			//set up contract variables
			$c_headings = $headObject->getMainObjectHeadings("CONTRACT",($compact_view==true ? "COMPACT" : "DETAILS"));
			$cObject = new PM5_CONTRACT();
			$c_tblref = "C";
			$c_table = $cObject->getTableName();
			$c_id = $c_tblref.".".$cObject->getIDFieldName();
			$c_id_fld = $cObject->getIDFieldName();
			$c_status = $c_tblref.".".$cObject->getStatusFieldName();
			$c_name = $cObject->getNameFieldName();
			$c_owner = $cObject->getOwnerFieldName();
			$c_manager = $cObject->getManagerFieldName();
			$c_deadline = $cObject->getDeadlineFieldName();
			$c_field = $c_tblref.".".$cObject->getTableField();

			$where = $cObject->getActiveStatusSQL($c_tblref);

				$dObject = new PM5_DELIVERABLE();
				$d_tblref = "D";
				$d_table = $dObject->getTableName();
				$d_id_fld = $dObject->getIDFieldName();
				$d_parent = $d_tblref.".".$dObject->getParentFieldName();

					$aObject = new PM5_ACTION();
					$a_tblref = "A";
					$a_table = $aObject->getTableName();
					$a_id_fld = $aObject->getIDFieldName();
					$a_parent = $a_tblref.".".$aObject->getParentFieldName();

			//if type is something other than contract
			if($obj_type!="CONTRACT") {
				//setup deliverable variables
				$d_status = $d_tblref.".".$dObject->getStatusFieldName();
				$d_id = $d_tblref.".".$dObject->getIDFieldName();
				$d_headings = $headObject->getMainObjectHeadings("DELIVERABLE",($compact_view==true ? "COMPACT" : "DETAILS"));
				$d_name = $dObject->getNameFieldName();
				$d_owner = $dObject->getOwnerFieldName();
				$d_deadline = $dObject->getDeadlineFieldName();
				$d_field = $d_tblref.".".$dObject->getTableField();
				$where.=" AND ".$dObject->getActiveStatusSQL($d_tblref);
				//setup action variables
				if($obj_type=="ACTION") {
					$a_status = $a_tblref.".".$aObject->getStatusFieldName();
					$a_id = $a_tblref.".".$aObject->getIDFieldName();
					$a_name = $aObject->getNameFieldName();
					$a_deadline = $aObject->getDeadlineFieldName();
					$a_owner = $aObject->getOwnerFieldName();
					$a_field = $a_tblref.".".$aObject->getTableField();
					$a_headings = $headObject->getMainObjectHeadings("ACTION",($compact_view==true ? "COMPACT" : "DETAILS"));
					$id_fld = $aObject->getIDFieldName();
					if($format_for_emails) {
						$final_data['head'][$id_fld] = $a_headings['rows'][$id_fld];
						$final_data['head'][$a_name] = $a_headings['rows'][$a_name];
						$final_data['head'][$a_owner] = $a_headings['rows'][$a_owner];
						$final_data['head'][$a_deadline] = $a_headings['rows'][$a_deadline];
						$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
						$final_data['head'][$c_id_fld] = $c_headings['rows'][$c_id_fld];
						$final_data['head'][$c_manager] = $c_headings['rows'][$c_manager];
						$final_data['head'][$c_owner] = $c_headings['rows'][$c_owner];
						$final_data['head'][$c_deadline] = $c_headings['rows'][$c_deadline];
						$final_data['head'][$d_name] = $d_headings['rows'][$d_name];
						$final_data['head'][$d_id_fld] = $d_headings['rows'][$d_id_fld];
						$final_data['head'][$d_owner] = $d_headings['rows'][$d_owner];
						$final_data['head'][$d_deadline] = $d_headings['rows'][$d_deadline];
						$sql ="SELECT ".$a_tblref.".".$id_fld." as id
								, CONCAT('".$aObject->getRefTag()."',".$a_id.") as ".$a_id_fld."
								, CONCAT(".$a_tblref.".".$a_name.",' [".$aObject->getRefTag()."',".$a_id.",']') as ".$a_name."
								, ".$a_tblref.".".$a_name."
								, ".$a_tblref.".".$a_owner."
								, ".$a_tblref.".".$a_deadline."
								, CONCAT('".$dObject->getRefTag()."',".$d_id.") as ".$d_id_fld."
								, CONCAT(".$d_tblref.".".$d_name.",' [".$dObject->getRefTag()."',".$d_id.",']') as ".$d_name."
								, CONCAT('".$cObject->getRefTag()."',".$c_id.") as ".$c_id_fld."
								, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name."
								, ".$c_tblref.".".$c_deadline."
								, ".$c_tblref.".".$c_owner."
								, ".$c_tblref.".".$c_manager."
								, ".$d_tblref.".".$d_deadline."
								, ".$d_tblref.".".$d_owner."
								";
					} else {
						$sql ="SELECT ".$a_tblref.".*, CONCAT(".$d_tblref.".".$d_name.",' [".$dObject->getRefTag()."',".$d_id.",']') as ".$d_name." , CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name;
						foreach($a_headings['rows'] as $fld=>$head){
							//if($fld!=$id_fld) {
								$final_data['head'][$fld] = $head;
							//}
						}
					}
					$from = " $a_table $a_tblref INNER JOIN $d_table $d_tblref ON $d_id = $a_parent INNER JOIN $c_table $c_tblref ON $c_id = $d_parent ";
					$where.= " AND ".$aObject->getActiveStatusSQL($a_tblref);
					$where_tblref = $a_tblref;
					$ref_tag = $aObject->getRefTag();
				} else {
					$id_fld = $dObject->getIDFieldName();
					$a_headings = array('rows'=>array());
					$from = " $d_table $d_tblref
							INNER JOIN $c_table $c_tblref ON $c_id = $d_parent
							LEFT OUTER JOIN $a_table $a_tblref
							  ON $a_parent = $d_tblref.$d_id_fld ";
					if($format_for_emails) {
						$sql ="SELECT ".$d_tblref.".".$id_fld." as id
								, CONCAT('".$dObject->getRefTag()."',".$d_id.") as ".$d_id_fld."
								, CONCAT(".$d_tblref.".".$d_name.",' [".$dObject->getRefTag()."',".$d_id.",']') as ".$d_name."
								, CONCAT('".$cObject->getRefTag()."',".$c_id.") as ".$c_id_fld."
								, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name."
								, ".$c_tblref.".".$c_deadline."
								, ".$c_tblref.".".$c_owner."
								, ".$c_tblref.".".$c_manager."
								, ".$d_tblref.".".$d_deadline."
								, ".$d_tblref.".".$d_owner."
								, ".$c_tblref.".contract_assess_type
								, ".$c_tblref.".contract_assess_other_name
								, ".$c_tblref.".contract_do_assessment";
						$final_data['head'][$id_fld] = $d_headings['rows'][$id_fld];
						$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
						$final_data['head'][$c_id_fld] = $c_headings['rows'][$c_id_fld];
						$final_data['head'][$c_manager] = $c_headings['rows'][$c_manager];
						$final_data['head'][$c_owner] = $c_headings['rows'][$c_owner];
						$final_data['head'][$c_deadline] = $c_headings['rows'][$c_deadline];
						$final_data['head'][$d_name] = $d_headings['rows'][$d_name];
						$final_data['head'][$d_id_fld] = $d_headings['rows'][$d_id_fld];
						$final_data['head'][$d_owner] = $d_headings['rows'][$d_owner];
						$final_data['head'][$d_deadline] = $d_headings['rows'][$d_deadline];
					} else {
						$sql ="SELECT ".$d_tblref.".* , AVG(A.action_progress) as action_progress, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name.", ".$c_tblref.".contract_assess_type, ".$c_tblref.".contract_assess_other_name, ".$c_tblref.".contract_do_assessment";
						$final_data['head'][$id_fld] = $d_headings['rows'][$id_fld];
						$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
						$final_data['head'][$d_name] = $d_headings['rows'][$d_name];
						foreach($d_headings['rows'] as $fld=>$head){
							//if($fld!=$id_fld) {
								$final_data['head'][$fld] = $head;
							//}
						}
						$group_by = " GROUP BY $d_id";
					}
					$where_tblref = $d_tblref;
					$ref_tag = $dObject->getRefTag();
				}
			} else {
				$id_fld = $cObject->getIDFieldName();
				$a_headings = array('rows'=>array());
				$d_headings = array('rows'=>array());
				if($format_for_emails) {
					$final_data['head'][$id_fld] = $d_headings['rows'][$id_fld];
					$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
					$final_data['head'][$c_id_fld] = $c_headings['rows'][$c_id_fld];
					$final_data['head'][$c_manager] = $c_headings['rows'][$c_manager];
					$final_data['head'][$c_owner] = $c_headings['rows'][$c_owner];
					$final_data['head'][$c_deadline] = $c_headings['rows'][$c_deadline];
					$sql ="SELECT ".$c_tblref.".".$id_fld." as id
							, CONCAT('".$cObject->getRefTag()."',".$c_id.") as ".$c_id_fld."
							, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name."
							, ".$c_tblref.".".$c_deadline."
							, ".$c_tblref.".".$c_owner."
							, ".$c_tblref.".".$c_manager."";
				} else {
					$sql = "SELECT ".$c_tblref.".*, AVG(A.action_progress) as action_progress";
					$d_headings = array('rows'=>array());
					$a_headings = array('rows'=>array());
					$final_data['head'] = $c_headings['rows'];
					unset($final_data['head']['cs_supplier_id']);
					unset($final_data['head']['cs_project_value']);
					unset($final_data['head']['contract_template_id']);
					$group_by = " GROUP BY ".$c_tblref.".".$id_fld;
				}
				$from = " $c_table $c_tblref
						LEFT OUTER JOIN $d_table $d_tblref
						  ON $c_tblref.$id_fld = $d_parent
						LEFT OUTER JOIN $a_table $a_tblref
						  ON $a_parent = $d_tblref.$d_id_fld ";
				$where_tblref = $c_tblref;
				$ref_tag = $cObject->getRefTag();
			}

			$where.= " AND ".$where_tblref.".".$id_fld." = ".$id;

			$all_headings = array_merge(
				$c_headings['rows'],
				$d_headings['rows'],
				$a_headings['rows']
			);
			//return $d_headings;
			$listObject = new PM5_LIST();

			//$list_headings = $headObject->getAllListHeadings();
			//$list_tables = $listObject->getAllListTables(array_keys($list_headings));

			foreach($all_headings as $fld => $head) {
				$lj_tblref = substr($head['section'],0,1);
				if($head['type']=="LIST" && $head['parent_id']==0) {
					$listObject->changeListType($head['list_table']);
					$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])."
										AS ".$head['list_table']."
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
										AND (".$head['list_table'].".status & ".PM5::DELETED.") <> ".PM5::DELETED;
				} elseif($head['type']=="MASTER") {
					$tbl = $head['list_table'];
					$masterObject = new PM5_MASTER($fld);
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				} elseif($head['type']=="OWNER") {
					$ownerObject = new PM5_CONTRACT_OWNER();
					$tbl = $head['list_table'];
					$dir_tbl = $fld."_parent";
					$sub_tbl = $fld."_child";
					$own_tbl = $fld;
					$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$lj_tblref.".".$fld." = ".$sub_tbl.".subid";
					$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
					$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
				} elseif($head['type']=="CONTRACT_SUPPLIER") {

				}
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where.(isset($group_by) ? $group_by : "");
			//return $sql;
			$r = $this->mysql_fetch_one($sql);
			$row = array();
			$displayObject = new PM5_DISPLAY();
	//		foreach($rows as $r) {
		//$this->arrPrint($r);
				$i = $r[$id_fld];
				foreach($final_data['head'] as $fld=> $head) {
					$val = isset($r[$fld]) ? $r[$fld] : $fld;
					//format by type
					if($headObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
						$row[$fld] = ( (!isset($r[$head['list_table']]) || is_null($r[$head['list_table']])) ? $this->getUnspecified() : $r[$head['list_table']]);
					//} elseif($head['type']=="BOOL") {
						//$row[$fld] = $displayObject->getDataField("BOOL", $val);
					} elseif($head['type']=="DEL_TYPE"){
						$row[$fld] = $this->deliverable_types[$val];
					} elseif($head['type']=="DELIVERABLE") {
						if($r["del_type"]=='SUB') {
							if($r[$fld]==0) {
								$row[$fld] = $this->getUnspecified();
							} else {
								if(!isset($deliverable_names_for_subs)) {
									$deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($i);
									//$this->arrPrint($deliverable_names_for_subs);
								}
								$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
							}
						} else {
							unset($final_data['head'][$fld]);
						}
					} elseif($head['section']=="DELIVERABLE_ASSESS") {
						$assess_status = $r['contract_assess_type'];
						$display_me = true;
							if($cObject->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
								$display_me = false;
							} else {
								//validate the assessment status field here
								$lt = explode("_",$head['list_table']);
								switch($lt[1]) {
									case "quality": $display_me = $cObject->mustIAssessQuality($assess_status); break;
									case "quantity": $display_me = $cObject->mustIAssessQuantity($assess_status); break;
									case "other": $display_me = $cObject->mustIAssessOther($assess_status); break;
								}
							}
						if($display_me) {
							$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
							if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
								$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
							}
						} else {
							unset($final_data['head'][$fld]);
							//$row[$fld] = $assess_status;
						}
					} elseif($this->isDateField($fld) || $head['type']=="DATE") {
						$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
					//format by fld
					} elseif($fld==$id_fld){//} || ($obj_type=="ACTION" && $fld==$a_id_fld) || ($obj_type=="DELIVERABLE" && $fld==$d_id_fld) || ($obj_type=="CONTRACT" && $fld==$c_id_fld)) {
						if($fld==$id_fld && !$format_for_emails) {
							$row[$fld] = $ref_tag.$val;
						} else {
							$row[$fld] = $val;
						}
					//removed to finance section
					/*} elseif($fld=="contract_supplier") {
						$csObject = new PM5_CONTRACT_SUPPLIER();
						$sub_rows = $csObject->getObjectByContractID($id);
						$sub_head = $c_headings['sub'][$head['id']];
						if(count($sub_rows)>0) {
							$val = array();
							foreach($sub_rows as $sr) {
								$x = array();
								foreach($sub_head as $shead) {
									switch($shead['type']) {
										case "LIST":
											$v = $sr[$shead['list_table']];
											break;
										case "CURRENCY":
											$v = $displayObject->getDataField($shead['type'], $sr[$shead['field']]);
											$v = $v['display'];
											break;
										default:
											$v = $sr[$shead['field']];
									}
									$x[$shead['name']] = $v;
								}
								$val[] = $x;
							}
						} else {
							$val = "";
						}
						$row[$fld] = $val; */
		/*
	} elseif($fld=="contract_assess_status") {
		$val = $r['contract_assess_status'];
		$listObject = new PM5_LIST("deliverable_status");
		$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
		$last_deliverable_status = 0;
		$sub_head = array();
		$sh_type = "BOOL";
		foreach($list_items as $key => $status) {
			$sub_head[$key] = array(
				'field'=>$key,
				'type'=>$sh_type,
				'name'=>$status,
				'required'=>1,
				'parent_link'=>"",
			);
			$last_deliverable_status = $key;
		}
		$td = array();
		foreach($sub_head as $key => $shead) {
			$sh_type = $shead['type'];
			$sfld = $shead['field'];
			if($sh_type=="BOOL"){
				$pow = pow(2,$key);
				$test = ( (($val & $pow) == $pow) ? "1" : "0");
				$sval = $displayObject->getDataField("BOOL", $test);
			}
			$td[]=array($shead['name']=>$sval['display']);
		}
		$row[$fld] = $td;
	} elseif($fld=="contract_assess_type") {
		$sub_head = $c_headings['sub'][$head['id']];
		$val = $r['contract_assess_type'];
		$sub_display = true;
		$td = array();
		foreach($sub_head as $shead) {
			$v = $shead['field'];
			switch($v) {
				case "contract_assess_other_name":
					if($cObject->mustIAssessOther($val)) {
						$v = $r[$v];
					} else {
						$sub_display = false;
					}
					break;
				case "contract_assess_qual": 	$cas = !isset($cas) ? $cObject->mustIAssessQuality($val) : $cas;
				case "contract_assess_quan": 	$cas = !isset($cas) ? $cObject->mustIAssessQuantity($val) : $cas;
				case "contract_assess_other":	$cas = !isset($cas) ? $cObject->mustIAssessOther($val) : $cas;
					if($cas) {
						$v = $displayObject->getDataField("BOOL", "1");
					} else {
						$v = $displayObject->getDataField("BOOL", "0");
					}
					$v = $v['display'];
					break;
			}
			if($sub_display) {
				$td[] = array($shead['name']=>$v);
			}
			unset($cas);
		}
		$row[$fld] = $td;
	} elseif($head['type']=="REF") {
		$row[$fld] = $val;
		//echo "<P>".$fld." = ".$val;
	} elseif($head['type']=="ATTACH") {
		$row[$fld] = $val;
		//echo "<P>".$fld." = ".$val;
		$attach_display_options = array(
			'can_edit'=>false,
			'object_type'=>$obj_type,
			'object_id'=>$id,
			'buttons'=>$attachment_buttons,
		);
		$row[$fld] = $displayObject->getDataField($head['type'], $val,$attach_display_options);
	} else {
		//$row[$fld] = $val;
		$row[$fld] = $displayObject->getDataField($head['type'], $val);
	}
}
if($format_for_emails) {
	switch($obj_type) {
		case "ACTION":
			$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$sorted_row['child'][$a_name] = $row[$a_name];
			$sorted_row['child'][$a_owner] = $row[$a_owner];
			$sorted_row['child'][$a_deadline] = $row[$a_deadline];
			$sorted_row['parent'][$d_name] = $row[$d_name];
			$sorted_row['parent'][$d_owner] = $row[$d_owner];
			$sorted_row['parent'][$d_deadline] = $row[$d_deadline];
			$sorted_row['grandparent'][$c_name] = $row[$c_name];
			$sorted_row['grandparent'][$c_owner] = $row[$c_owner];
			$sorted_row['grandparent'][$c_manager] = $row[$c_manager];
			$sorted_row['grandparent'][$c_deadline] = $row[$c_deadline];
			$field_names['child']['id'] = $row[$a_id_fld];
			$field_names['child']['name'] = $a_name;
			$field_names['child']['owner'] = $a_owner;
			$field_names['child']['deadline'] = $a_deadline;
			$field_names['parent']['id'] = $row[$d_id_fld];
			$field_names['parent']['name'] = $d_name;
			$field_names['parent']['owner'] = $d_owner;
			$field_names['parent']['deadline'] = $d_deadline;
			$field_names['grandparent']['id'] = $row[$c_id_fld];
			$field_names['grandparent']['name'] = $c_name;
			$field_names['grandparent']['owner'] = $c_owner;
			$field_names['grandparent']['manager'] = $c_manager;
			$field_names['grandparent']['deadline'] = $c_deadline;
			break;
		case "DELIVERABLE":
			$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$sorted_row['child'][$d_name] = $row[$d_name];
			$sorted_row['child'][$d_owner] = $row[$d_owner];
			$sorted_row['child'][$d_deadline] = $row[$d_deadline];
			$sorted_row['parent'][$c_name] = $row[$c_name];
			$sorted_row['parent'][$c_owner] = $row[$c_owner];
			$sorted_row['parent'][$c_manager] = $row[$c_manager];
			$sorted_row['parent'][$c_deadline] = $row[$c_deadline];
			$field_names['child']['id'] = $row[$d_id_fld];
			$field_names['child']['name'] = $d_name;
			$field_names['child']['owner'] = $d_owner;
			$field_names['child']['deadline'] = $d_deadline;
			$field_names['parent']['id'] = $row[$c_id_fld];
			$field_names['parent']['name'] = $c_name;
			$field_names['parent']['owner'] = $c_owner;
			$field_names['parent']['manager'] = $c_manager;
			$field_names['parent']['deadline'] = $c_deadline;
			break;
		case "CONTRACT":
			$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
			$sorted_row['child'][$c_name] = $row[$c_name];
			$sorted_row['child'][$c_owner] = $row[$c_owner];
			$sorted_row['child'][$c_manager] = $row[$c_manager];
			$sorted_row['child'][$c_deadline] = $row[$c_deadline];
			$field_names['child']['id'] = $row[$c_id_fld];
			$field_names['child']['name'] = $c_name;
			$field_names['child']['owner'] = $c_owner;
			$field_names['child']['manager'] = $c_manager;
			$field_names['child']['deadline'] = $c_deadline;
			break;
	}
	$final_data['rows'] = $sorted_row;
	$final_data['email_fields'] = $field_names;
	//$final_data['email_full'] = $r;
} else {
	$final_data['rows'] = $row;
}
//}*/
		return $final_data;
	}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}



	/***********************************
	 * GET functions
	 */

	/**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
	public function drawPageFooter($left = "", $log_table = "", $var = "") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
		echo "WRONG PAGE FOOTER";
		return "";
	}

	/**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
	public function getPageFooter($left = "", $log_table = "", $var = "") {
		/*    	$echo = "";
				$js = "";
				if(is_array($var)) {
					$x = $var;
					$d = array();
					unset($var);
					foreach($x as $f => $v) {
						$d[] = $f."=".$v;
					}
					$var = implode("&",$d);
				}
				$echo = "
				<table width=100% class=tbl-subcontainer><tr>
					<td width=50%>".$left."</td>
					<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
						<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
					</span>" : "")."</td>
				</tr></table><div id=div_audit_log></div>";
				if(strlen($log_table)>0){
					$js = "
					$(\"#disp_audit_log\").click(function() {
						var state = $(this).attr('state');
						if(state==\"show\"){
							$(this).find('img').prop('src','/pics/tri_down.gif');
							$(this).attr('state','hide');
							$(\"#div_audit_log\").html(\"\");
							$(\"#log_txt\").html('Display Activity Log');
						} else {
							$(this).find('img').prop('src','/pics/tri_up.gif');
							$(this).attr('state','show');
							var dta = '".$var."&log_table='+$(this).attr('table');
							var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
							$(\"#div_audit_log\").html(result[0]);
							$(\"#log_txt\").html('Hide Activity Log');
						}

					});
					";
				}
				$data = array('display'=>$echo,'js'=>$js);
				return $data;
		 **/
		return array('display' => "WRONG PAGE FOOTER", 'js' => "");
	}

	public function drawActivityLog() {

	}




	/*********************************************
	 * SET/UPDATE functions
	 */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new PM5_LOG($log_table);
	//	$logObject->addObject($var);
	//}


	public function notify($data, $type, $object) {
		$noteObj = new PM5_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
	}


	public function editMyObject($var, $attach = array(), $recipients = array(), $noter = "", $include = array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new PM5_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(), "FORM");
		$mar = 0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld => $v) {
			if(isset($headings['rows'][$fld]) || in_array($fld, array("contract_assess_status",
																	  "contract_assess_type",
																	  "contract_assess_other_name"))) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type'] == "DATE") {
					if(strlen($v) > 0) {
						$insert_data[$fld] = date("Y-m-d", strtotime($v));
					}
				} elseif($fld == "del_type") {
					if($v == "DEL") {
						$var['del_parent_id'] = 0;
						$insert_data['del_parent_id'] = 0;
					}
					$insert_data[$fld] = $v;
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}


		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

		if($mar > 0 || count($attach) > 0) {
			$changes = array('user' => $this->getUserName(),);
			foreach($insert_data as $fld => $v) {
				$head = $headings['rows'][$fld];
				if($old[$fld] != $v || $headings['rows'][$fld]['type'] == "HEADING") {
					$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type' => "TEXT");
					if(in_array($h['type'], array("LIST", "MASTER", "USER", "OWNER", "DEL_TYPE", "DELIVERABLE"))) {
						$list_items = array();
						$ids = array($v, $old[$fld]);
						switch($h['type']) {
							case "LIST":
								$listObject = new PM5_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new PM5_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "OWNER":
								$ownerObject = new PM5_CONTRACT_OWNER();
								$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new PM5_MASTER($head['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "DELIVERABLE":
								$delObject = new PM5_DELIVERABLE();
								$list_items = $delObject->getDeliverableNamesForSubs($ids);
								break;
							case "DEL_TYPE":
								$delObject = new PM5_DELIVERABLE();
								$list_items = $delObject->getDeliverableTypes($ids);
								break;
						}

						unset($listObject);
						$changes[$fld] = array('to'   => $list_items[$v],
											   'from' => $list_items[$old[$fld]],
											   'raw'  => array('to' => $v, 'from' => $old[$fld]));
					} elseif($h['type'] == "HEADING") {
						if($fld == "contract_assess_type") {
							$shead = $headings['sub'][$h['id']];
							foreach($shead as $sh) {
								$sfld = $sh['field'];
								switch($sh['type']) {
									case "BOOL":
										$bitwise_value = 0;
										$b = explode("_", $sfld);
										$f = end($b);
										switch($f) {
											case "other":
												$bitwise_value = PM5_CONTRACT::OTHER;
												break;
											case "qual":
												$bitwise_value = PM5_CONTRACT::QUALITATIVE;
												break;
											case "quan":
												$bitwise_value = PM5_CONTRACT::QUANTITATIVE;
												break;
										}
										$new_val = 0;
										if(($v & $bitwise_value) == $bitwise_value) {
											$new_val = 1;
										}
										$old_val = 0;
										if(($old[$fld] & $bitwise_value) == $bitwise_value) {
											$old_val = 1;
										}
										if($old_val != $new_val) {
											$changes[$sfld] = array('to' => $new_val, 'from' => $old_val);
										}
										break;
									default:
										if($old[$sfld] != $insert_data[$sfld]) {
											$changes[$sfld] = array('to' => $insert_data[$sfld], 'from' => $old[$sfld]);
										}
										break;
								}
							}
						} elseif($fld == "contract_assess_status") {
							$listObject = new PM5_LIST("deliverable_status");
							$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
							unset($listObject);
							foreach($list_items as $key => $status) {
								$bitwise_value = pow(2, $key);
								$new_val = 0;
								if(($v & $bitwise_value) == $bitwise_value) {
									$new_val = 1;
								}
								$old_val = 0;
								if(($old[$fld] & $bitwise_value) == $bitwise_value) {
									$old_val = 1;
								}
								if($old_val != $new_val) {
									$changes[$fld][$key] = array('to' => $new_val, 'from' => $old_val);
								}
							}
						}
					} else {
						$changes[$fld] = array('to' => $v, 'from' => $old[$fld]);
					}
				}
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getAttachmentFieldName()] = $x;
			$log_var = array('object_id' => $object_id,
							 'changes'   => $changes,
							 'log_type'  => PM5_LOG::EDIT,
							 'progress'  => $old[$this->getTableField()."_progress"],
							 'status_id' => $old[$this->getTableField()."_status_id"],);
			//**For Notifications and subsequent logging**//
			if(strlen($noter) > 0) {
				$log_var['recipients'] = $recipients;
				$log_var['include'] = $include;
				$note = $this->notify($log_var, $noter, $this->getMyObjectType());
				unset($log_var['recipients']);
				unset($log_var['include']);
				if($note[0] == "ok") {
					$log_var['changes']['notification'] = "no notifications sent";
				} else if($note[0] == "info") {
					$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
				} else {
					$log_var['changes']['notification'] = $note;
				}
			}
			//****//
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok",
						 $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been updated successfully.",
						 array($log_var, $this->getMyObjectType()));
		}
		return array("info", "No changes were found to be saved.  Please try again.");
	}


	public static function date_sort($a, $b) {
		return strtotime($a) - strtotime($b);
	}







	/********************************************
	 * PROTECTED functions
	 */


	/*******************************************
	 * PRIVATE functions
	 */


	public function __destruct() {
		parent::__destruct();
	}


}


?>