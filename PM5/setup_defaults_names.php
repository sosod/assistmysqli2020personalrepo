<?php
require_once("inc_header.php");

echo ASSIST_HELPER::getFloatingDisplay(array("info","Any blank headings will be replaced with the default terminology."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$names = array();

$names['object_names'] = $nameObject->getObjectNamesFromDB();

$names['activity_names'] = $nameObject->getActivityNamesFromDB();

ASSIST_HELPER::displayResult(array("warn","Any changes made on this page will take 30 minutes to propogate to all users currently logged in.  Changes will reflect immediately for any user who logs in after they are made."));


foreach($names as $key => $n) {

	switch($key) {
		case "object_names":
			$head = "Object Names";
			$log_section = "OBJECT";
			break;
		case "activity_names":
			$head = "Activity Names";
			$log_section = "ACTIVITY";
			break;
	}

echo "<h2>$head</h2>";
?>
<table class='tbl-container not-max'><tr><td>
<?php
?>
<form name=frm_<?php echo $key; ?>>
	<input type=hidden name=section value='<?php echo $key; ?>' />
	<table id=tbl_<?php echo $key; ?>>
		<tr>
			<th>Ref</th>
			<th>Default Terminology</th>
			<th>Your Terminology</th>
			<th></th>
		</tr>
		<?php
		foreach($n as $object ){
			$ff = $displayObject->createFormField("MEDVC",array('id'=>$object['section'],'name'=>$object['section'],'orig'=>$object['client'],'class'=>"valid8me"),$object['client']);
			echo "
			<tr id=tr_".$object['id'].">
				<td>".$object['id']."</td>
				<td>".$object['mdefault']."</td>
				<td id=td_".$object['id'].">".$ff['display']."</td>
				<td><input type=button value=Save class=btn_save ref='".$object['section']."' /></td>
			</tr>";
		}
		?>
		<tr>
			<td colspan=2></td>
			<td colspan=1 class=center><input type=button value="Save All" class=btn_save_all /></td>
			<td colspan=1></td>
		</tr>	
	</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>$log_section),$key); ?></td>
	</tr>
</table>
<?php
} //end foreach $names
?>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	
	//alert($("#tbl_object_names").css("width"));
	$("#tbl_object_names").parents("tr").next().children("td").children("table").css("width",$("#tbl_object_names").css("width"));
	
	$("#tbl_menu_names tr.contents td").css("background-color","#e6e6f5");
	//$("#tbl_menu_names tr.has_child td").css("background-color","#e6e6f5");
	
	$("input:text.valid8me").keyup(function() {
		if($(this).val()!=$(this).attr('orig')) {
			$(this).addClass("orange-border");
		} else {
			$(this).removeClass("orange-border");
		}
		var pi = $(this).parents("table").prop("id");
		var c = $("#"+pi+" .orange-border").length;
		if(c==0) {
			$("#"+pi+" tr:last input:button").val("No Changes to Save");
			$("#"+pi+" tr:last input:button").prop("disabled",true);
		} else { 
			$("#"+pi+" tr:last input:button").prop("disabled",false);
			var v = (c>1 ? "Save All "+c+" Changes" : "Save The "+c+" Change");
			$("#"+pi+" tr:last input:button").val(v);
		}
	});
	
	$(".btn_save_all, .btn_save").click(function() { 
		var pi = $(this).parents("table").prop("id");
		if(pi=="tbl_object_names") {
			$form = $("form[name=frm_object_names]");
		} else {
			$form = $("form[name=frm_activity_names]");
		}
		if($(this).hasClass("btn_save")) {
			var dta = "section="+$form.find("input:hidden[name=section]").val();
			var r = $(this).attr("ref");
			var v = $("#"+r).val();
			dta+="&"+r+"="+v;
		} else {
			var dta = AssistForm.serialize($form);
		}
		//alert(dta);
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Names.Edit",dta);
		if(result[0]=="ok" && !$(this).hasClass("btn_save")) {
			document.location.href = 'setup_defaults_names.php?r[]='+result[0]+'&r[]='+result[1];
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	
	$("#tbl_object_names, #tbl_activity_names").find("input:text.valid8me:first").each(function() {
		$(this).trigger("keyup");
	});
	
});
</script> 