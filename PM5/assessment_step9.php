<?php
require_once("inc_header.php");
$create_step = 9;
//$create_type = "PROJ";
$create_name = "Component";
$create_names = "Components";

$obj_id = $_REQUEST['obj_id'];
$scdObj = new PM5_SCORECARD();
$line_weight_precision_allowed = $scdObj->getLineWeightPrecisionAllowed();
$scorecard = $scdObj->getAObjectSummary($obj_id);
$pa = $scorecard;

//$scdObj->arrPrint($pa);

$total_lines = 0;
if(isset($pa['count'])) {
	$display=true;
	foreach($pa['count'] as $key => $a) {
		foreach($a as $b => $c) {
			$total_lines+=$c;
		}
	}
} else {
	$display=false;
}

echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);

if($display) {

?>
<h3>Weight <?php echo $create_names; ?></h3>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $obj_id; ?>" />
	<input type=hidden name=sw_scdid value="<?php echo $obj_id; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<?php
//$scd_id,$scorecard,$display_weight_input=false,$display_scoring=false,$display_bonus=false,$indi_lines=array(),$indi_weights=array(),$kpa_weights=array(),$cc_lines=array(),$cc_objects=array(),$cc_weights=array(),$kpa_scores=array(),$jal_scores=array(),$cc_score=0
$echo = $displayObject->getComponentWeighting($obj_id,$scorecard,true);
echo $echo['display'];
?>
</form>


<p class='center no-print'><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Save & Next Step</button></p>


<?php

} else {	//if display=true

	echo "
	<P>No components have been added to this scorecard.  No further progress can be made.  Please go back and add Indicators, Core Competencies or Job Activities.</p>
	<p class=center><button id=btn_back>Back</button> </p>
	";


}




//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id']);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;

	$("table.noborder, table.noborder td").css("border","0px");


	$("#btn_next").button({
		icons: {primary: "ui-icon-disk",secondary:"ui-icon-arrowthick-1-e"},
	}).click(function() {
		var err = false;
		/*$("td.total_weight").each(function() {
			if(($(this).html()*1)!=100) {
				err = true;
			}
		});*/
		if(err) {
			//alert("Not all KPA Weights total 100.  Please review the weights again.");
			alert("An error has occurred.  Please reload the page and try again.");
		} else {
			AssistHelper.processing();
			//var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>";
			var dta = AssistForm.serialize($("form[name=frm_weights]"));
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=ScorecardWeight.saveWeights",dta);
			//var result = ["error","testing"];
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
		//document.location.href = 'assessment_create_step3.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});
<?php
if($display) {
	?>
	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		//if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		//}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");
	<?php
} else {
	?>
	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		//if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo $create_step-1;?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		//}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");
	<?php
}




echo $echo['js'];
?>

});

</script>