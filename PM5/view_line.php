<?php
require_once("inc_header.php");


//ASSIST_HELPER::arrPrint($_REQUEST);
$line_id = $_REQUEST['obj_id'];

/*******************
 * Get line details -> modref & src id & src type
 * ---Display Specific SCD details (line weight, KPA)??
 * [[Replicate SDBP5/view_dept_detail.php]]:
 * - Display KPI details
 * - Display KPI results
 * - display CP details => if required
 * - display CP results => if required
 */

//TODO Determine $line_modloc getting it from $line['module'] - required for integration with SDBP6!! - Not changed now (21 May 2019) as not sure of impact to rest of function [JC]

$lineObj = new PM5_LINE();
$line = $lineObj->getAObject($line_id,array(),true);
//ASSIST_HELPER::arrPrint($line);
$line_modloc = "SDBP5";
if($line['srctype']=="PROJ") {
	//$sdbp5Obj = new SDBP6_PMS();
	$kpi_id = $line['srcid'];//$sdbp5Obj->getKPIIDFromCAP($line['srcid'],$line['srcmodref']);
	$kpi_type = "dept";
} elseif($line['srctype']=="JAL") {
//	die("JAL!");
	$line_modloc = $line['module'];
	$kpi_type = "activity";
	$kpi_id = $line['srcid'];
//	$line_modloc = "JAL1";
} else {
	if($line['srctype']=="TOP") {
		$kpi_type = "top";
	} else {
		$kpi_type = "dept";
	}
	$kpi_id = $line['srcid'];
}

if(!isset($line['module']) || is_null($line['module']) || strlen($line['module'])==0) {
	die("<p>The source module of the line could not be identified.  Please close Assist and try again.</p>");
}

$module_folder = $line['module'];

//ASSIST_HELPER::arrPrint($line);

include("inc_iframe_session_fix_start.php");

$_SESSION['ref'] = $line['srcmodref'];
$_SESSION['modref'] = $line['srcmodref'];
$_SESSION['modlocation'] = $line_modloc;
$_SESSION['dbref'] = "assist_".strtolower($lineObj->getCmpCode())."_".strtolower($line['srcmodref']);

$_SESSION['PM5']['external_call_settings'] = array(
		'ref'=>$line['srcmodref'],
		'modref'=>$line['srcmodref'],
		'modlocation'=>$line_modloc,
		'dbref'=>$_SESSION['dbref'],
);

//echo "<div class=float width=250><pre>";print_r($_SESSION);echo "</div>";
$url = "/".$module_folder."/view_".$kpi_type."_external.php?show_header=0&id=".$kpi_id."";
//echo $url;
//echo "<hr />";
echo "<iframe id=ifr_line_object style='width: ".($_REQUEST['width']-50)."px; height: ".($_REQUEST['height']-50)."px; border: 0px dashed #009900;' src='$url'></iframe>";
/*
$js = "";

$sdbp5Obj = new SDBP5_PMS($line['srcmodref']);
$kpi_display = $sdbp5Obj->getKPIDetailDisplay($line['srcid'],($line['srctype']=="PROJ"?true:false));
echo $kpi_display['display'];
$js.=$kpi_display['js'];

$kpi_results_display = $sdbp5Obj->getKPIResultsDisplay($kpi['kpi_id']);
echo $kpi_results_display['display'];
$js.=$kpi_results_display['js'];

if($line['srctype']=="PROJ") {
$sdbp5Obj = new SDBP5_PMS($line['srcmodref']);
	$cp_display = $sdbp5Obj->getCAPDetailDisplay($line['srcid']);
	echo $cp_display['display'];
	$js.=$cp_display['js'];

	$cp_results_display = $sdbp5Obj->getCAPResultsDisplay($line['srcid']);
	echo $cp_results_display['display'];
	$js.=$cp_results_display['js'];

}*/



?>
<script type="text/javascript">
	$(function() {
		<?php //echo $js; ?>
	});
</script>