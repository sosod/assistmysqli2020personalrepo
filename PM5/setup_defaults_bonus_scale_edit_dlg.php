<?php

require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);


$displayObject = new PM5_DISPLAY();
$js = '';

$bonus_scale_id = $_REQUEST['bonus_scale_id'];
$bonus_object = new PM5_SETUP_BONUS();
$bonus_scale = $bonus_object->getBonusScaleByID($bonus_scale_id);

$divisor_options = array(3=>"3",5=>"5");

$list_id = 'bonus_scale';

$listObj = new PM5_LIST($list_id);
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
?>
<h1>Edit Bonus Scale</h1>
<form name=frm_list>
    <input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
    <input type="hidden" id="id" name="id" value="<?php echo $bonus_scale_id; ?>" />
<table id=ed_frm class=form>
    <?php foreach($fields as $fld => $name){ ?>
        <tr>
            <th><?php echo $name; ?></th>
            <td fld=edit_td_<?php echo $fld; ?> id="edit_td_<?php echo $fld; ?>">
                <?php
				if($fld == "status") {
					echo "Active";
				} elseif($fld=="divisor") {
					$js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld,'options'=>$divisor_options,'req'=>true), $bonus_scale[$fld]);
                } else {
					$js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld), $bonus_scale[$fld]);
				}
				?>
            </td>
        </tr>
    <?php } ?>
</table>
</form>

<script type="text/javascript">
    $(function() {
        <?php echo $js; ?>

        //Tell parent window to open the dialog once the page has finished loading
        window.parent.openDialog();
    });
</script>