<?php
require_once("inc_header.php");


$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
$module_setup['FIN_YEAR']['setting'] = $masterObject->getAListItemName($module_setup['FIN_YEAR']['value']);
unset($masterObject);

$fld = "ORG_SOURCE";
if($module_setup[$fld]!==false && $module_setup[$fld]['value']!==false) {
	$org_source_name = $moduleSetupObject->getSourceModuleDetails($module_setup[$fld]['object_id'],$module_setup[$fld]['value'],$module_setup[$fld]['setting']);
} else {
	$org_source_name = "Not available";
}
$fld = "IKPI_SOURCE";
if($module_setup[$fld]!==false && $module_setup[$fld]['value']!==false) {
	$ikpi_source_name = $moduleSetupObject->getSourceModuleDetails($module_setup[$fld]['object_id'],$module_setup[$fld]['value'],$module_setup[$fld]['setting']);
} else {
	$ikpi_source_name = "Not available";
}
?>
<h2>Module Setup</h2>
<div id="div_notice" class="tbl-info light-orange-back float" style="width:300px;margin:10px;margin-right: 50px">
	<h3 class="orange">Please Note</h3>
	<p>If you need to change anything on this page, please contact your Assist Administrator / Business Partner.  </p><p>Note that if any Scorecards have been created, then they will be removed if any changes are made to this section.</p>
</div>
<?php
//ASSIST_HELPER::arrPrint($module_setup);



?>
<table class="form" id="tbl_setup">
	<tr>
		<th>Setup Status:</th>
		<td><?php echo ($module_setup['SETUP_COMPLETE']===true?"Complete":"Not Yet Complete"); ?></td>
	</tr>
	<tr>
		<th>Financial Year:</th>
		<td><?php echo $module_setup['FIN_YEAR']['setting']." (".ASSIST_MASTER_FINANCIALYEARS::REFTAG.$module_setup['FIN_YEAR']['value'].")"; ?></td>
	</tr>
	<tr>
		<th>Primary KPI Source:</th>
		<td><?php echo $moduleSetupObject->getASectionName($module_setup['PRIMARY']['value']); ?></td>
	</tr>
	<tr>
		<th><?php echo $moduleSetupObject->getASectionName("ORG"); ?> KPI Source:</th>
		<td><?php echo $org_source_name; ?></td>
	</tr>
	<tr>
		<th><?php echo $moduleSetupObject->getASectionName("IKPI"); ?> KPI Source:</th>
		<td><?php echo $ikpi_source_name; ?></td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		$("#div_notice p").css({"font-size":"130%","line-height":"140%"});
		$("#tbl_setup tr td, #tbl_setup tr th").css("padding","7px");
	});
</script>