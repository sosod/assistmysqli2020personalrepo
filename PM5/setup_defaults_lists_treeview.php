<?php
require_once("inc_header.php");

$competency_id = $_REQUEST['comp_id'];

$competency_object = new PM5_COMPETENCIES();

$treeview_datastructure = $competency_object->getCompetencyTreeViewDataStructureByCompetencyID($competency_id);
?>

<table class='tbl-container not-max'>
    <tr>
        <td>
            <table class=tbl-tree>
                <?php foreach($treeview_datastructure as $comp_id => $comp_info){ ?>
                    <tr class="grand-parent" style="background-color: rgb(170, 170, 170);">
                        <td class="uni-pos" colspan="4">Competency: <?php echo $comp_info['name'] . ' [' . $comp_id . ']'; ?></td>
                        <td class="center td-button" object_type="competencies" object_id="<?php echo $comp_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
                            <button class='action-button view-btn' parent_id="<?php echo $comp_id; ?>">View</button>
                            <button class='action-button edit-btn' parent_id="<?php echo $comp_id; ?>">Edit</button>
                        </td>
                        <td class="uni-pos center"></td>
                        <td class="uni-pos center"></td>
                    </tr>

                    <tr>
                        <td class="td-line-break" colspan="7">&nbsp;</td>
                    </tr>
                    <?php foreach($comp_info['proficiencies'] as $prof_id => $prof_info){ ?>
                        <tr class="parent" style="background-color: rgb(186, 186, 186);">
                            <td class="tree-icon uni-icon" width="20px"><button class="ibutton expand-btn">X</button></td>
                            <td class="bi-pos" colspan="3">Proficiency: <?php echo $prof_info['name']; ?></td>
                            <td class='center td-button' object_type="proficiencies" object_id="<?php echo $prof_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
                                <button class='action-button view-btn' parent_id="<?php echo $prof_id; ?>">View</button>
                                <button class='action-button edit-btn' parent_id="<?php echo $prof_id; ?>">Edit</button>
                            </td>
                            <td class="bi-pos center"></td>
                            <td class="bi-pos center"></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td class=bi-pos object_id="0" object_type="competencies_levels" data-competency_id="<?php echo $comp_id; ?>">
                                <button class='action-button add-btn' parent_id="<?php echo $prof_id; ?>">Add Competency Level</button>
                                <span class='float count'><?php echo (isset($prof_info['competency_levels']) ? count($prof_info['competency_levels']) : 0) . ' Competency Levels'; ?></span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php foreach($prof_info['competency_levels'] as $comp_level_id => $comp_level_info){ ?>
                            <tr class="child">
                                <td class=""></td>
                                <td class="tree-icon bi-icon" width="20px"><button class='ibutton sub-btn'>X</button></td></td>
                                <td class="tri-pos" colspan="2">Competency Level: <?php echo $comp_level_info['name']; ?></td>
                                <td class='center td-button' object_type="competencies_levels" object_id="<?php echo $comp_level_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
                                    <button class='action-button view-btn' parent_id="<?php echo $comp_level_id; ?>">View</button>
                                    <button class='action-button edit-btn' parent_id="<?php echo $comp_level_id; ?>">Edit</button>
                                </td>
                                <td class="tri-pos center"></td>
                                <td class="tri-pos center"></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </table>
        </td>
    </tr>
</table>


<div id=dlg_list title="Select lines">
    <iframe id=ifr_list></iframe>
</div>

<!--    ********************************* STYLE BASED JS  - START **********************  -->
<script>
    $(function() {
        var object_names = [];
        object_names['competencies'] = "Competency";
        object_names['proficiencies'] = "Proficiency";
        object_names['competencies_levels'] = "Competency LEVEL";

        var my_window = AssistHelper.getWindowSize();
        //alert(my_window['height']);
        if(my_window['width']>800) {
            var my_width = 850;
        } else {
            var my_width = 800;
        }
        var my_height = my_window['height']-50;

        $("table.tbl-tree").find("td.tree-icon").addClass("right").prop("width","20px");
        //Set the column spanning for each object description cell based on their position in the hierarchy but excluding the last two columns
        $("table.tbl-tree").find("td.cate-pos, td.td-line-break").prop("colspan","7");
        $("table.tbl-tree").find("td.uni-pos").not("td.center").prop("colspan","4");
        $("table.tbl-tree").find("td.bi-pos").not("td.center").prop("colspan","3");
        $("table.tbl-tree").find("td.tri-pos").not("td.center").prop("colspan","2");
        $("table.tbl-tree").find("td.quad-pos").not("td.center").prop("colspan","1");

        //Action positional buttons
        $("button.sub-btn").button({
            icons: {primary: "ui-icon-carat-1-sw"},
            text: false
        }).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
        //Parent positional buttons
        $("button.expand-btn").button({
            icons: {primary: "ui-icon-triangle-1-se"},
            text: false
        }).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});

        //Edit button
        $("button.edit-btn").button({
            icons: {primary: "ui-icon-pencil"},
        }).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
        */
        //View button
        $("button.view-btn").button({
            icons: {primary: "ui-icon-extlink"},
        }).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
        */
        //Add button
        $("button.add-btn").button({
            icons: {primary: "ui-icon-circle-plus"},
        }).removeClass("ui-state-default").addClass("ui-button-state-ok").css("color","#009900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"25px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
        */
        //Button styling and positioning
        $("button.ibutton:not(.sub-btn)").hover(
            function() {
                $(this).css({"border-width":"0px","background":"url()"})
            },
            function() {}
        );
        $("button.ibutton").each(function() {
            if($(this).prev().hasClass("ibutton")) {
                $(this).css("margin-left","-5px");
            }
            if($(this).next().hasClass("ibutton")) {
                $(this).css("margin-right","-5px");
            }
        });

        //Styling based on object type
        $("table.tbl-tree tr.grand-parent").css("background-color","#AAAAAA").find("td.td-button").css({"background-color":"#FFFFFF"});
        $("table.tbl-tree tr.parent").css("background-color","#BABABA").find("td.td-button").css("background-color","#FFFFFF");
        $("table.tbl-tree tr.sub-parent").css("background-color","#DEDEDE").find("td.td-button").css("background-color","#FFFFFF");

        //General styling
        $("span.count").css({"font-size":"80%","margin-top":"-3px"});
        $("table.tbl-tree th").css({"padding":"10px"});


        //Proficiency Edit Button Disable Hack
        $("button.edit-btn").each(function(){
            var object_type = $(this).parent("td").attr("object_type");
            if(object_type == 'proficiencies'){
                $(this).css("color","grey").css("filter","grayscale(100%)");
            }
        });

        $("button.action-button").click(function() {
            if($(this).parent("td").attr("object_type") == 'proficiencies' && $(this).hasClass("edit-btn")){
                e.preventDefault();
            }else{
                AssistHelper.processing();
                var object_id = $(this).parent("td").attr("object_id");
                var object_type = $(this).parent("td").attr("object_type");

                var competency_id = $(this).parent("td").data("competency_id");
                var parent_id = $(this).attr("parent_id");

                console.log('Object Type:');
                console.log(object_type);
                console.log('Object ID:');
                console.log(object_id);
                console.log('PARENT ID:');
                console.log(object_id);


                var act = "view";
                if($(this).hasClass("edit-btn")) {
                    act = "edit";
                } else if($(this).hasClass("add-btn")) {
                    act = "add";
                }

                /*
                * What we're doing: action as either "view" or "add" or "edit
                * The type of object we're acting on: "competency", "proficiency" or "competency_level"
                * The ID of the odject
                * "*/

                var dta = "object_id=" + object_id + "&object_type=" + object_type + "&action=" + act + "&competency_id=" + competency_id + "&parent_id=" + parent_id;

                var heading = AssistString.ucwords(act) + " " + object_names[object_type];
                //var parent_id = $(this).attr("parent_id");
                var url = "setup_defaults_lists_treeview_dlg.php?" + dta;

                $("#dlg_list").dialog("option","title",heading);

                //call iframe in dialog
                $("#ifr_list").prop("src",url);
                //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
                //$("#dlg_list").dialog("open");
            }
        });


        /************************************************************************************************/
        /************************************************************************************************/
        /************************************************************************************************/
        $("#dlg_list").dialog({
            autoOpen: false,
            modal: true,
            width: my_width,
            height: my_height,
            buttons: [{
                text:"Save",
                icons: {primary: "ui-icon-disk"},
                click:function(e) {
                    e.preventDefault();
                    AssistHelper.processing();
                    var object_id = $("#ifr_list").contents().find("#object_id").val();
                    var object_type = $("#ifr_list").contents().find("#object_type").val();
                    var action = $("#ifr_list").contents().find("#action").val();

                    console.log('Object Type AGN:');
                    console.log(object_type);
                    console.log('Object ID AGN:');
                    console.log(object_id);
                    console.log('ACTION AGN:');
                    console.log(action);

                    var field_value_pairs = new Array();

                    $("#ifr_list").contents().find("td").each(function() {
                        var field_name =  $(this).attr('fld');
                        field_name = field_name.replace('edit_td_', ''); //'123456';

                        $(this).children().each(function(){
                            var field_value;
                            if($(this).is('textarea')){
                                field_value = $(this).val();
                            }else if($(this).is('select')){
                                field_value = $(this).val();
                            }else{
                                field_value = null;
                            }

                            if(field_value !== null){
                                //THIS IS WHERE WE ADD THE ACTUAL FIELD VALUES
                                field_value = field_value.trim();
                                var field_value_pair_string = field_name + "=" + (field_value == "" ? "null" : field_value);
                                field_value_pairs.push(field_value_pair_string);
                            }
                        });
                    });

                    console.log('FV PAIRS AGN:');
                    console.log(field_value_pairs);

                    var dta = "object_id="+object_id;

                    field_value_pairs.forEach(function(element) {
                        dta += '&' + element;
                    });

                    dta = encodeURI(dta);

                    console.log('DTA:');
                    console.log(dta);

                    var ajax_action = 'DO.NOTHING';
                    if(object_type == 'competencies_levels' && action == 'edit'){
                        ajax_action = 'COMPETENCIES.EDITLEVELS';
                    }else if(object_type == 'competencies_levels' && action == 'add'){
                        var competency_id = $("#ifr_list").contents().find("#competency_id").val();
                        var parent_id = $("#ifr_list").contents().find("#parent_id").val();
                        dta += '&competency_id=' + competency_id;
                        dta += '&proficiency_id=' + parent_id;
                        ajax_action = 'COMPETENCIES.ADDLEVEL';
                    }else if(object_type == 'competencies' && action == 'edit'){
                        ajax_action = 'COMPETENCIES.EDITCOMPETENCY';
                    }

                    if(ajax_action != 'DO.NOTHING') {
                        var result = AssistHelper.doAjax("inc_controller.php?action=" + ajax_action,dta);

                        console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                        console.log(result);
                    } else {
                        var result = ["error","Dont Worry... Just Par for the course"];
                    }


                    //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                    if(result[0]=="ok") {
                        var url = document.location.href;
                        AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                    } else {
                        //if result was not "ok" then display the message
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                }
            },{
                text: "Cancel",
                click:function(e){
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_list").dialog("close");
                }
            }]
        });

        AssistHelper.formatDialogButtonsByClass($("#dlg_list"),0,"ui-state-green");

        /**
         * Iframe within the dlg_lines div
         */
        $("#ifr_list").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").css("border","1px solid #000000");


    });

    function openDialog() {
        $(function() {
            $("#dlg_list").dialog("open");
            AssistHelper.closeProcessing();
        });
    }
</script>
<!--    ********************************* STYLE BASED JS  - ENDT **********************  -->