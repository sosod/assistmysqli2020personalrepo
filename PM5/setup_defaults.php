<?php


require_once("inc_header.php");


/** @var PM5_SETUP_MODULE $module_setup -> created in inc_header.php */
if($module_setup === false) {

	//FORCE USER TO SETUP MODULE FIRST

	if(!isset($moduleSetupObject)) {
		$moduleSetupObject = new PM5_SETUP_MODULE();
	}

	//get available primary sources
	$primary_sources = $moduleSetupObject->getPrimaryKPISourcesAvailableForSelection();

?>
	<div style="margin:10px auto;width:600px;">

		<h2>Module Setup</h2>
		<p>Please define the core module settings which are required before you can start using the module.<br />No settings will be saved until you click the final Save Settings button.</p>
		<div id="div_step1" class="div-module-setup">
			<h2>Step 1: Primary KPI Source</h2>
			<p>Please select the primary source of KPIs for use within this module:</p>
			<p class="center"><select id="sel_step1" name="primary"><option selected value="X">--- SELECT ---</option> <?php
				foreach($primary_sources as $key => $text) {
					echo "<option value=".$key.">".$text."</option>";
				}
				?></select></p>
			<p class="center"><button id="btn_step1" class="btn_next">Next</button></p>
		</div>
		<div id="div_step2" class="div-module-setup">
			<h2>Step 2: Financial Year</h2>
			<p>Please select the relevant Financial Year applicable to this module:</p>
			<p class="center"><select id="sel_step2" name="fin_year"><option selected value="X">--- SELECT ---</option></select></p>
			<p class="center"><button id="btn_back1" class="btn_back">Back</button>&nbsp;<button id="btn_step2" class="btn_next">Next</button></p>
		</div>
		<div id="div_step3" class="div-module-setup">
			<h2>Step 3: Secondary KPI Source</h2>
			<p>If applicable, please select a secondary source of KPIs for use within this module:</p>
			<p class="center"><select id="sel_step3" name="secondary"><option selected value="X">Not Applicable / Available</option></select></p>
			<p class="center"><button id="btn_back2" class="btn_back">Back</button>&nbsp;<button id="btn_step3" class="btn_next">Next</button></p>
		</div>
		<div id="div_step4" class="div-module-setup">
			<h2>Step 4: Grouping</h2>
			<p>Please select the field to use for grouping KPIs within this module:</p>
			<p class="center"><select id="sel_step4" name="grouping"><option selected value="X">--- SELECT ---</option></select></p>
			<p class="center"><button id="btn_back3" class="btn_back">Back</button>&nbsp;<button id="btn_step4" class="btn_save">Save Settings</button></p>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("div.div-module-setup").addClass("tbl-ok");
			$(".btn_back").button({
					icons:{ primary: "ui-icon-arrowthick-1-w" }
				}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function() {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
				},function() {
					$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
				}).click(function(e) {
					e.preventDefault();
					AssistHelper.processing();
					$("div.div-module-setup").hide();
					var i = $(this).prop("id");
					switch(i) {
						case "btn_back1":
							$("#div_step1").show();
							break;
						case "btn_back2":
							$("#div_step2").show();
							break;
						case "btn_back3":
							$("#div_step3").show();
							break;
					}
					AssistHelper.closeProcessing();
			});
			$(".btn_next").button({
					icons:{ secondary: "ui-icon-arrowthick-1-e" }
				}).removeClass("ui-state-default").addClass("ui-button-coloured-green")
				.hover(function() {
					$(this).removeClass("ui-button-coloured-green").addClass("ui-button-coloured-orange");
				},function() {
					$(this).addClass("ui-button-coloured-green").removeClass("ui-button-coloured-orange");
				}).click(function(e) {
					e.preventDefault();
					AssistHelper.processing();
					var result = ["ok",""];
					var i = $(this).prop("id");
					var finish_with_redirect = false;
					switch(i) {
						case "btn_step1":
							//validate that select is correct
							if($("#sel_step1").val()!="X") {
								$("#sel_step1").removeClass("required");
								//get objects from module
								var step2_items = AssistHelper.doAjax("inc_controller.php?action=SETUPMODULE.getFinYearOptions","module="+$("#sel_step1").val());
								console.log(step2_items);
								if(step2_items.length==0) {
									result[0] = "error";
									result[1] = "No activated Annual Plans where found on that module.  Please check the status of the annual plans within the source module or choose another source module.";
								} else {
									$("#sel_step2 option").remove();
									$("#sel_step2").append("<option selected value='X'>--- SELECT ---</option>");
									for(i in step2_items) {
										//console.log(i+" = "+step2_items[i]['id']+" => "+step2_items[i]['value']);
										$("#sel_step2").append("<option value='"+step2_items[i]['id']+"'>"+step2_items[i]['value']+"</option>");
									}
								}
								$("div.div-module-setup").hide();
								$("#div_step2").show();
							} else {
								$("#sel_step1").addClass("required");
								result[0] = "error";
								result[1] = "Please select the primary source module.";
							}
							break;
						case "btn_step2":
							//validate that select is correct
							if($("#sel_step2").val()!="X") {
								$("#sel_step2").removeClass("required");
								//get objects from module
								var step3_items = AssistHelper.doAjax("inc_controller.php?action=SETUPMODULE.getSecondaryModuleOptions","primary="+$("#sel_step1").val()+"&fin_year="+$("#sel_step2").val());
								console.log(step3_items);
								if(step3_items.length==0) {
									result[0] = "info";
									result[1] = "No secondary Indicator sources were found for the given Financial Year.  Moving on to the next step";
									finish_with_redirect = true;
									var redirect_function_call = "moveOnIfNoSecondarySource";
									AssistHelper.closeProcessing();
									//AssistHelper.finishedProcessing(result[0],result[1]);
									$("div.div-module-setup").hide();
									$("#div_step3").show();
									//$("#btn_step3").trigger("click");
								} else {
									$("#sel_step3 option").remove();
									$("#sel_step3").append("<option selected value='X'>Not applicable</option>");
									for(i in step3_items) {
										//console.log(i+" = "+step2_items[i]['id']+" => "+step2_items[i]['value']);
										$("#sel_step3").append("<option value='"+step3_items[i]['id']+"'>"+step3_items[i]['value']+"</option>");
									}
									$("div.div-module-setup").hide();
									$("#div_step3").show();
								}
							} else {
								$("#sel_step2").addClass("required");
								result[0] = "error";
								result[1] = "Please select the financial year.";
							}
							break;
						case "btn_step3":
							//don't need to validate select - not applicable/unspecified is allowed
								//get valid grouping options for all modules involved
								var dta = "primary="+$("#sel_step1").val()+"&secondary="+$("#sel_step3").val();
								console.log(dta);
							var step4_items = AssistHelper.doAjax("inc_controller.php?action=SETUPMODULE.getGroupingOptions",dta);
								console.log(step4_items);
							if(step4_items.length==0 || step4_items[0]=="error") {
								result[0] = "info";
								result[1] = "No valid grouping fields were found.  A common field must be shared between all selected modules and their components in order to use this module.  Please review your module setup and contact your Business Partner for assistance.";
							} else {
								$("#sel_step4 option").remove();
								$("#sel_step4").append("<option selected value='X'>--- SELECT ---</option>");
								for(i in step4_items) {
									//console.log(i+" = "+step2_items[i]['id']+" => "+step2_items[i]['value']);
									$("#sel_step4").append("<option value='"+step4_items[i]['id']+"'>"+step4_items[i]['value']+"</option>");
								}
								$("div.div-module-setup").hide();
								$("#div_step4").show();
							}
							break;
					}
					if(result[0]=="ok") {
						AssistHelper.closeProcessing();
					} else if(finish_with_redirect!==true) {
						//AssistHelper.finishedProcessingWithFollowonFunction(result[0],result[1],redirect_function_call);
					//} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
			});

			$(".btn_save").button({
					icons:{ secondary: "ui-icon-disk" }
				}).removeClass("ui-state-default").addClass("ui-button-coloured-green")
				.hover(function() {
					$(this).removeClass("ui-button-coloured-green").addClass("ui-button-coloured-orange");
				},function() {
					$(this).addClass("ui-button-coloured-green").removeClass("ui-button-coloured-orange");
				}).click(function(e) {
					e.preventDefault();
					AssistHelper.processing();
					//validate that select is correct
					if($("#sel_step4").val()!="X") {
						$("#sel_step4").removeClass("required");
						//get objects from module
						var dta = "save=settings";
						$("select").each(function() {
							dta+="&"+$(this).prop("name")+"="+$(this).val();
						});
						console.log(dta);
						var result = AssistHelper.doAjax("inc_controller.php?action=SETUPMODULE.saveSettings",dta);
						if(result[0]=="ok") {
							AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"setup_defaults.php");
						} else {
							AssistHelper.finishedProcessing(result[0],result[1]);
						}
					} else {
						$("#sel_step4").addClass("required");
						var result = [];
						result[0] = "error";
						result[1] = "Please select the grouping field.";
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
			});
			$("div.div-module-setup").hide();
			$("#div_step1").show();
		});

	</script>
<?php


} else {	//else if check if module setup has been done

//Carry on with normal setup functions
//ASSIST_HELPER::arrPrint($module_setup);

	$list_names = $headingObject->getStandardListHeadings();

	?>
	<style type="text/css">
		#div_left, #div_right {
			padding: 0px 20px 20px 20px;
			width: 45%;
			height: 100%;
		}

		#div_container {
			padding: 0px 10px 0px 10px;
		}
	</style>
	<div id=div_container>
		<div id=div_right>
			<h2>Lists</h2>
			<table class="form middle tbl_child">
				<?php
				foreach($list_names as $key => $val) {
					$firsthalf = explode("_", $key);
					if(isset($firsthalf[1]) && $firsthalf[1] == "status") {
						echo "	<tr>
	        					<th width=200px>".$val.":</th>
	        					<td>Configure the $val list items for use in the ".$helper->getObjectName($firsthalf[0])." section.<span class=float><input type=button value=Configure class=btn_list id='".$key."' /></span></td>
	    					</tr>";
					} else {
						echo "	<tr>
	        					<th width=200px>".$val.":</th>
	        					<td>Configure the ".$val." list items.<span class=float><input type=button value=Configure class=btn_list id='".$key."' /></span></td>
	    					</tr>";
					}
				}
				?>

			</table>
		</div>
		<div id=div_left>
			<h2>Module Settings</h2>
			<table class="form middle tbl_child">
				<tr>
					<th width=200px>Module Setup:</th>
					<td>Configure the initial module setup. This must be done before any scorecards can be created. <span class=float><input type=button value=Configure class=btn_setup
																																			 id="module_setup" /></span></td>
				</tr>
				<tr>
					<th width=200px>Module Preferences:</th>
					<td>Configure module settings.<span class=float><input type=button value=Configure class=btn_setup id="preferences" /></span></td>
				</tr>

				<tr>
					<th width=200px><?php echo $headingObject->replaceObjectNames("|bonusscale|"); ?>:</th>
					<td>Customise the <?php echo $headingObject->replaceObjectNames("|bonusscale|"); ?>.<span class=float><input type=button value=Configure class=btn_setup id="bonus_scale" /></span>
					</td>

			</table>


		</div>
	</div> <!-- end div container -->
	<script type=text/javascript>
		$(function() {
			$("h2").css("margin-top", "15px");

			$("input:button").button().css("font-size", "75%");
			$("input:button.btn_setup").click(function() {
				if($(this).prop("id") == "bonus_scale") {
					var url = "setup_defaults_" + $(this).prop("id") + "_list_table.php";
				} else {
					var url = "setup_defaults_" + $(this).prop("id") + ".php";
				}
				document.location.href = url;
			});
			$("input:button.btn_list").click(function() {
				if($(this).prop("id") == "competencies_levels" || $(this).prop("id") == "competencies") {
					document.location.href = "setup_defaults_list_table.php?l=" + $(this).prop("id");
				} else {
					document.location.href = "setup_defaults_lists.php?l=" + $(this).prop("id");
				}
			});
			$("input:button.btn_report").click(function() {
				document.location.href = "setup_defaults_" + $(this).prop("id") + ".php";
			});
			$("#div_right").css("float", "right");
			$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
			$("table.form").css({"width": "100%"});
			$(window).resize(function() {
				var x = 0;
				$("#div_right").children().each(function() {
					x += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				var y = 0;
				$("#div_left").children().each(function() {
					y += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				if(x > y) {
					$("#div_container").css("height", (x + 50) + "px");
				} else {
					$("#div_container").css("height", (y + 50) + "px");
				}
			});
			$(window).trigger("resize");
		});
	</script>


	<?php
}//end if check if module setup has been done.
	?>