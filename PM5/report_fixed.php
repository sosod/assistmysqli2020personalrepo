<?php
$page_section = "report";
require_once("inc_header.php");

//ASSIST_HELPER::displayResult(array("info","This section is under development.  Please contact Action iT should you have any suggested reports to be included in this section."));
//die();

$me = new PM5_REPORT();

if(isset($_REQUEST['i'])) {
	$report_id = $_REQUEST['i'];
	$me->setReportID($_REQUEST['i']);
	//$me->arrPrint($_REQUEST);
    echo "<h2>".$me->getReportTitle()."</h2>";
	
	
	if($report_id=="dashboard") {
		$period_id = $_REQUEST['filter']['period_id'];
		$assessObj = new PM5_ASSESSMENT();
		$assessments = $assessObj->getAssessmentByPeriod($period_id);
		$page_action = PM5_TRIGGER::REPORT_ASSESS;
			$display_kpa=false;
			$display_cc=false;
			$display_bonus = false;
			$display_js = false;
			$user_ids = false;
		
		foreach($assessments as $as) {
			echo "<h2>".$as['employee']."</h2>";
			$assess_id = $as[$assessObj->getIDFieldName()];
			
			
			include("common/scoresheet.php");
		}
		
		
		$me->arrPrint($assessments);
		
	}elseif($report_id=="perf_status"){
        include("report_fixed_perf_status.php");
    }elseif($report_id=="performance_by_score"){
        include("report_fixed_performance_by_score.php");
    }elseif($report_id=="bell_curve"){
        include("report_fixed_bell_curve.php");
    }


	?>
<?php } else { ?>
    <?php
        $apObj = new PM5_PERIOD();
        $periods = $apObj->getAssessmentPeriodsForSelect();
        $available_periods = count($periods)>0?true:false;
        $reports = $me->getListOfReports();
        //$me->arrPrint($reports);
    ?>
    <table class=list>
        <tr>
            <th>Report</th>
            <th>Description</th>
            <th>Filter</th>
            <th></th>
        </tr>
        <?php foreach($reports as $i => $r) { ?>
            <tr>
                <td class=b><?php echo $r['title'] ?></td>
                <td><?php echo $r['description'] ?></td>
                <td>
                    <?php if(!isset($r['filter']) || (isset($r['filter']) && $r['filter']!==false)) { ?>
                        <?php $selected = false; ?>
                        <select id=filter_<?php echo $i ?> f_type=period_id>
                            <?php foreach($periods as $pi => $p) { ?>
                                <option <?php echo (!$selected?"selected":"") ?> value="<?php echo $pi ?>"><?php echo $p ?></option>
                                <?php $selected = true; ?>
                            <?php } ?>
                        </select>
                    <?php } else { ?>
                        <?php echo "N/A" ?>
                    <?php } ?>
                </td>
                <td><button class='btn_generate size_button' id="<?php echo $i ?>" data-report_filter"<?php echo (!isset($r['filter']) || (isset($r['filter']) && $r['filter']!==false) ? "For a change" : "none") ?>">Generate</button></td>
            </tr>
        <?php } ?>
    </table>
	<script type=text/javascript>
		$(".btn_generate").button({
			icons: {primary: "ui-icon-script"},
		}).click(function() {
			AssistHelper.processing();
			var i = $(this).prop("id");
			var f = $("#filter_"+i).val();
			var ft = $("#filter_"+i).attr("f_type");

			var report_filter = $(this).data('report_filter');

			var get_vars = '?i='+i+'&filter['+ft+']='+f;

			if(report_filter == "none"){
                document.location.href = 'report_fixed.php' + get_vars;
            }else{
                $("#dlg_report_filters").dialog("option","title","Report Filters");
                var url = "report_fixed_filters.php" + get_vars;
                //call iframe in dialog
                $("#ifr_report_filters").prop("src",url);
            }
		});
		resizeButtons($(".btn_generate"));
	</script>

    <div id=dlg_report_filters title="Report Filters">
        <iframe id=ifr_report_filters></iframe>
    </div>

    <script type="text/javascript">
        $(function(){
            //Determine the necessary width and height settings for the dlg_lines & ifr_lines elements
            var scr = AssistHelper.getWindowSize();
            var dlg_width = scr['width']*0.95;
            var dlg_height = scr['height']*0.95;
            var ifr_width = dlg_width-25;
            var ifr_height = dlg_height-100;

            /**
             * Dialog to display available lines after clicking the "Go" button
             */
            $("#dlg_report_filters").dialog({
                autoOpen: false,
                modal: true,
                width: dlg_width,
                height: dlg_height,
                buttons: [{
                    text:"Generate",
                    icons: {primary: "ui-icon-disk"},
                    click:function(e) {
                        e.preventDefault();
                        AssistHelper.processing();

                        var i = $("#ifr_report_filters").contents().find("#i").val();
                        var f = $("#filter_"+i).val();
                        var ft = $("#filter_"+i).attr("f_type");

                        var job_manager_title = $("#ifr_report_filters").contents().find("#job_manager_title").val()
                        var job_dept = $("#ifr_report_filters").contents().find("#job_dept").val()
                        var emp_section = $("#ifr_report_filters").contents().find("#emp_section").val()
                        var job_level = $("#ifr_report_filters").contents().find("#job_level").val()
                        var sort_by = $("#ifr_report_filters").contents().find("#sort_by").val()
                        var sort_asc_desc = $("#ifr_report_filters").contents().find("#sort_asc_desc").val()

                        var get_vars = '?i='+i+'&filter['+ft+']='+f+'&filter[job_manager_title]='+job_manager_title+'&filter[job_dept]='+job_dept;
                        get_vars += '&filter[job_level]='+job_level+'&filter[sort_by]='+sort_by+'&filter[sort_asc_desc]='+sort_asc_desc;


                        document.location.href = 'report_fixed.php' + get_vars;
                    }
                },{
                    text: "Cancel",
                    click:function(e){
                        e.preventDefault();
                        $("#dlg_report_filters").dialog("close");
                    }
                }]
            });
            AssistHelper.formatDialogButtonsByClass($("#dlg_report_filters"),0,"ui-state-green");
            /**
             * Iframe within the dlg_lines div
             */
            $("#ifr_report_filters").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");
        });

        /**
         * Function called by the ifr_lines Iframe to open the dlg_lines dialog once the iframe source page has loaded and is ready to be displayed to the user
         */
        function openDialog() {
            $(function() {
                $("#dlg_report_filters").dialog("open");
                AssistHelper.closeProcessing();
            });
        }
    </script>
	<?php
}
?>