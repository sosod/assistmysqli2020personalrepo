<?php

// clone of "risk_versus_control.php"

$period_id = $_REQUEST['filter']['period_id'];
unset($_REQUEST['filter']['period_id']);
$filters = $_REQUEST['filter'];

$scdObj = new PM5_SCORECARD();
$assessObj = new PM5_ASSESSMENT();
$assessments = $assessObj->getAssessmentForPerformanceReportByPeriod($period_id, $filters);

//Employee Fields
$emp_obj = new PM5_EMPLOYEE();
$job_titles = $emp_obj->getAllJobTitlesFormattedForSelect();
$departments = $emp_obj->getAllDepartmentsFormattedForSelect();
$job_levels = $emp_obj->getAllJobLevelsFormattedForSelect();
?>
<?php
$layout_js = "";
$options = array('id'=>"display_self",'name'=>"display_self");
$self_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$self_button['js'];
$options = array('id'=>"display_mod",'name'=>"display_mod");
$mod_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$mod_button['js'];
$options = array('id'=>"display_final",'name'=>"display_final");
$final_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$final_button['js'];
?>

<div id=div_layout>
    <h5>Display</h5>
    <table>
        <tr>
            <td class=b>Self:</td>
            <td><?php echo $self_button['display']; ?></td>
        </tr>
        <tr>
            <td class=b>Moderation:</td>
            <td><?php echo $mod_button['display']; ?></td>
        </tr>
        <tr>
            <td class=b>Final:</td>
            <td><?php echo $final_button['display']; ?></td>
        </tr>
    </table>
</div>

<table class=list id=tbl_list  style="width: 100%">
    <tr>
        <th>Ref</th>
        <!-- EMPLOYEE DETAILS - BEGIN -->
        <th>Employee</th>
        <th>Job Title</th>
        <th>Department</th>
        <th>Manager</th>
        <th>Job Level</th>
        <!-- EMPLOYEE DETAILS - END -->

        <!-- LINES - BEGIN -->
        <th>Organisational<Br />KPIs</th>
        <th>Individual<br />KPIs</th>
        <th>Organisational<Br />Top Layer KPIs</th>
        <th>Individual<br />Top Layer KPIs</th>
        <th>Organisationl<br />Projects</th>
        <th>Individual<br />Projects</th>
        <th>Core<br />Competencies</th>
        <th><?php echo $scdObj->getObjectName(PM5_ASSESSMENT::OBJECT_TYPE,true); ?></th>
        <!-- LINES - END -->


        <!-- SCORE DETAILS - BEGIN -->
        <th class="self_title">Self Assessment Score</th>
        <th class="mod_title">Moderation Score</th>
        <th class="final_title">Final Review Score</th>
        <!-- SCORE DETAILS - END -->
    </tr>
    <?php if(count($assessments)>0) { ?>
        <?php foreach($assessments as $pa) { ?>
            <tr>
                <td class='center b'><?php echo $pa['asmt_id'] ?></td>
                <!-- EMPLOYEE DETAILS - BEGIN -->
                <td emp_tkid="<?php echo $pa['scd_tkid'] ?>"><?php echo $pa['employee'] ?></td>
                <td><?php echo ((int)$pa['job_id'] != 0 ? $job_titles[$pa['job_id']] : $emp_obj->getUnspecified()) ?></td>
                <td><?php echo $departments[$pa['job_dept']] ?></td>
                <td><?php echo $job_titles[$pa['job_manager_title']] ?></td>
                <td><?php echo ((int)$pa['job_level'] != 0 ? $job_levels[$pa['job_level']] : $emp_obj->getUnspecified()) ?></td>
                <!-- EMPLOYEE DETAILS - END -->

                <!-- LINES - BEGIN -->
                <td><?php echo (isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)?></td>
                <td class=center><?php echo (isset($pa['assessments']) ? $pa['assessments'] : 0) ?></td>
                <!-- LINES - END -->

                <!-- SCORE DETAILS - BEGIN -->
                <td class="self_title center"><?php echo round($pa['self_score_average'], 1) ?></td>
                <td class="mod_col center"><?php echo round($pa['moderation_score_average'], 1) ?></td>
                <td class="final_col center"><?php echo round($pa['final_score_average'], 1) ?></td>
                <!-- SCORE DETAILS - END -->
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td></td><td colspan=21>No Objects available.</td>
        </tr>
    <?php } ?>
</table>

<script type="text/javascript">
    $(function() {
        $('#div_layout').addClass('no-print').addClass('float').css({'position':'relative','bottom':'50px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
        $('#div_layout').find('table').css('margin-right','10px');
        <?php echo $layout_js ?>

        $('#display_self_yes').click(function() {
            if(!($('.self_title:first').is(':visible'))) {
                $('.self_title').show();
                $('.self_col').show();
            }
        });
        $('#display_self_no').click(function() {
            if(($('.self_title:first').is(':visible'))) {
                $('.self_title').hide();
                $('.self_col').hide();
            }
        });
        $('#display_mod_yes').click(function() {
            if(!($('.mod_title:first').is(':visible'))) {
                $('.mod_title').show();
                $('.mod_col').show();
            }
        });
        $('#display_mod_no').click(function() {
            if(($('.mod_title:first').is(':visible'))) {
                $('.mod_title').hide();
                $('.mod_col').hide();
            }
        });

        $('#display_final_yes').click(function() {
            if(!($('.final_title:first').is(':visible'))) {
                $('.final_title').show();
                $('.final_col').show();
            }
        });
        $('#display_final_no').click(function() {
            if($('.final_title:first').is(':visible')) {
                $('.final_title').hide();
                $('.final_col').hide();
            }
        });

    });
</script>
