<?php 
switch($_REQUEST['act']) {
case "REVIEW":
case "SAVE":
	$total_changes = 0;
	$log_headings = array();
	echo "<table class=noborder><tr class=no-highlight><td class=noborder>";	//border table
	//GET REQUEST DATA
	if(!isset($_REQUEST['t'])) { die(displayResult(array("error","An error occurred while trying to determine the time period."))); }
	$time_id = $_REQUEST['t'];
	if($_REQUEST['act']=="REVIEW") {
		echo "<p>Please note: 
			<br />1. This function only updates the ACTUALS and does not affect the budget figures.
			<br />2. Any changes will be highlighted in green.  Any errors will be highlighted in red.</p>";
	} else {
		echo JSdisplayResultPrep("Processing...");
	}
	echo "<form id=up method=post action=".$self.">
			<input type=hidden name=t value=".$time_id." />
			<input type=hidden name=tab value=".$tab." />
			<input type=hidden name=page_id value=".$page_id." />
			<input type=hidden name=act value=SAVE />";
	//CHECK FOR IMPORT FILE
		$err = false;
		$file_location = $modref."/import/".$section."/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$file_location;
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
				}
			}
		}
		//READ FILE
		if(!$err) {
			echo "<input type=hidden name=f value='".(isset($filen) ? $filen : $_REQUEST['f'])."' />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);
		//GET DATA
			if(count($data)>2) {
				unset($data[0]); unset($data[1]);
				$new = array();
				$object_sql = "SELECT r.* FROM ".$dbref."_revbysrc r
				WHERE r.rs_active = true ORDER BY r.rs_sort";
				
				$results_sql = "SELECT r.* FROM ".$dbref."_revbysrc_results r
				INNER JOIN ".$dbref."_revbysrc rs ON r.rr_rsid = rs.rs_id AND rs.rs_active = true
				WHERE rr_timeid <= ".$time_id;

				$objects = mysql_fetch_alls($object_sql,"rs_id");
				$results = mysql_fetch_alls($results_sql,"rr_rsid");
				
				echo "<table>
						<thead><tr>
							<th rowspan=2>Ref</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'value']['h_client']."</th>
							<th rowspan=2 style=\"border-right: 2px solid #ffffff;\">".$mheadings[$section][$table_fld.'vote']['h_client']."</th>
							<th style=\"border-left: 2px solid #ffffff;\" colspan=".count($mheadings[$r_section]).">Original Values</th>
							<th style=\"border-left: 2px solid #ffffff;\" colspan=".count($mheadings[$r_section]).">Updated Values</th>";
					echo "<th rowspan=2></th>";
					echo "</tr><tr>";
						for($i=1;$i<=2;$i++) {
							foreach($mheadings[$r_section] as $fld => $h) {
								if($fld == "rr_budget") { $style = "border-left: 2px solid #ffffff;"; } else { $style = ""; }
								echo "<th style=\"$style\">".$h['h_client']."</th>";
								if(!isset($log_headings[$fld])) { $log_headings[$fld] = $h['h_client']; }
							}
						}
					echo "</tr></thead>
						<tbody>";
				foreach($data as $d => $di) {
					$err = false;
					if(strlen($di[0])>0) {
						$obj_id = substr($di[0],strlen($id_labels_all[$section]),strlen($di[0]));
						if(!isset($objects[$obj_id])) { $err = true; } else {	$obj = $objects[$obj_id];  $res = $results[$obj_id]; $new[$obj_id] = array(); }
						echo "<tr>
							<th>".$id_labels_all[$section].$obj_id."</th>";
							if(!$err) {
								echo "<td>".$obj[$table_fld.'value']."</td>";
								echo "<td>".$obj[$table_fld.'vote']."</td>";
								foreach($mheadings[$r_section] as $fld => $h) {
									if($fld == "rr_budget") { $style = "border-left: 2px solid #000099;"; } else { $style = ""; }
									echo "<td class=right style=\"font-style: italic; background-color: #eeeeee; $style \">".number_format($res[$fld],2)."</td>";
								}
								$change = false;
								$class = "";
								foreach($mheadings[$r_section] as $fld => $h) {
									if($fld == "rr_budget") { 
										$style = "border-left: 2px solid #000099;"; 
										$val = $res[$fld];
									} else { 
										$style = ""; 
										$val = $di[5];
										if(!is_numeric($val)) { 
											$change = false; $class = "result1"; $err = true;
										} elseif($val!=$res[$fld]) {
											$class = "result3";
											$change = true;
											$new[$obj_id][$fld] = $val;
										}
									}
									
									if($class=="result1") {
										echo "<td class=\"$class right\" style=\"$style\">Invalid number</td>";
									} else {
										echo "<td class=\"$class\" style=\"text-align: right; $style\">".number_format($val,2).($fld=="perc" ? "%" : "")."</td>";
									}
									$new[$obj_id]['change'] = $change;
								}
								$sql = "";
								if($new[$obj_id]['change']) {
									if($_REQUEST['act']=="SAVE") {
										$sql_fld = array();
										$log_changes = array();
										foreach($new[$obj_id] as $fld=>$v) {
											if($fld!="change") {
												$sql_fld[] = $fld." = ".number_format($v,4,".","");
												$log_changes[] = $log_headings[$fld]." = ".number_format($v,2).(strpos($fld,"perc")>0?"%":"");
											}
										}
if(count($sql_fld)>0) {
										$sql = "UPDATE ".$dbref."_".$table_tbl."_results SET ".implode(", ",$sql_fld)." WHERE rr_rsid = ".$obj_id." AND rr_timeid = ".$time_id;
										$mar = db_update($sql);
										$total_changes+=$mar;
										//echo "<td>$sql</td>";
										if($mar>0) {
											$v = array(
												'fld' => "",
												'timeid' => $time_id,
												'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']." as follows:<br /> - ".implode("<br /> - ",$log_changes)),
												'old' => "",
												'new' => addslashes($file),
												'act' => "U",
												'YN' => "Y"
											);
											logChanges($section,$obj_id,$v,addslashes($sql));
											echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> Success</td>";
										} else {
											echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span> Error on update</td>";
										}
} else {
										echo "<td></td>";
}
									} else {
										echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									}
								} elseif($err) {
									echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
								} else {
									echo "<td></td>";
								}
								//echo "<td>$sql</td>";
							} else {	//error
								echo "<td colspan=21>";
									displayResult(array("error","Reference not found"));
								echo "</td>";
							}
						echo "</tr>";
					}	//if strlen ref
				}	//foreach data 
				echo "</tbody>
				</table>";
				if($_REQUEST['act']=="REVIEW") {
					echo "<p>&nbsp;</p>";
					echo "<table width=100%><tr><td class=center><input type=submit value=\"Accept changes\" class=isubmit /></td></tr></table></td></tr></table>";
				} else {
					if($total_changes>0) {
						$v = array(
							'fld' => "",
							'timeid' => $time_id,
							'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']),
							'old' => "",
							'new' => addslashes($file),
							'act' => "U",
							'YN' => "Y"
						);
						logChanges($section,0,$v,"");
					?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("ok","ok","Update complete");
							});
						</script><?php
					} else {
						?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("info","info","Update complete.  No changes were found.");
							});
						</script><?php
					}
				}
				displayGoBack($self."?page_id=".$page_id."&tab=".$tab,"Go Back");
			} else {
				die(displayResult(array("error","No data found to import.")));
			}
		} else {
			die(displayResult(array("error","An error occurred while trying to read the file.")));
		}
	break;
default:
?>
<form id=up action=<?php echo $self; ?> method=post enctype="multipart/form-data">
<input type=hidden name=page_id value=<?php echo $page_id; ?> />
<input type=hidden name=tab value=<?php echo $tab; ?> />
<input type=hidden name=act value=REVIEW />
	<h4>Update Process</h4>
		<ol>
			<li>Select time period to update: <select id=ti name=t><option selected value=X>--- SELECT MONTH ---</option><?php 
				foreach($time as $ti => $t) {
					if($t['active'] && $t['active_finance']) {
						echo "<option value=$ti>".$t['display_full']."</option>";
					}
				}
			?></select>
			<li>Export project details: <input type=button value=Export id=export /><br /><span style="font-style: italic">Please ensure that all new Line Itens have been added using the "Create" tab before clicking the "Export" button. </span></li>
			<li>Update the ACTUAL financial details in the CSV document saved in step 1. </li>
			<li>Import updated information:  <input type=file name=ifile id=ifl /> <input type=button value=Import id=import /><br />
				Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.<br />
				Until you click the "Accept" button, on the next page, no information will be updated. </li>
		</ol>
	<h4>Import file information: </h4>
		<ul>
			<li>This process is only for updating the ACTUAL results.  To change the budgets, please use the Edit function.</li>
			<li>The first two lines will be treated as header rows and will not be imported. </li>
			<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 2. </li>
			<li>The system will upload the financials to the line item identified by the Reference in the first column. Any lines that do not contain a valid Reference number will be ignored during the upload. If you wish to add a line item please use the "Create" tab. </li>
		</ul>
</form>
<?php
	$log_sql = "SELECT rlog_date, CONCAT_WS(' ',tkname,tksurname) as rlog_tkname, rlog_transaction FROM ".$dbref."_".$table_tbl."_log INNER JOIN assist_".$cmpcode."_timekeep ON rlog_tkid = tkid WHERE rlog_kpiid = '0' AND rlog_yn = 'Y' ORDER BY rlog_date DESC";
	$log_flds = array('date'=>"rlog_date",'user'=>"rlog_tkname",'action'=>"rlog_transaction");
displayAuditLog($log_sql,$log_flds);
?>
<script type=text/javascript>
	$(document).ready(function() {
		$("#export").click(function() {
			var t = $("#ti").val();
			$("#ti").removeClass("required");
			if(t!="X" && t.length>0 && !isNaN(parseInt(t))) {
				document.location.href = 'manage_fin_export.php?page_id=<?php echo $page_id; ?>&t='+t;
			} else {
				$("#ti").addClass("required");
				alert("Please select the relevant time period in step 1.");
			}
		});
		$("#import").click(function() {
			var f = $("#ifl").val();
			var t = $("#ti").val();
			$("#ti, #ifl").removeClass("required");
			if(t!="X" && t.length>0 && !isNaN(parseInt(t)) && f.length>0) {
				$("#up").submit();
			} else {
				err = "";
				if(t=="X" || t.length==0 || isNaN(parseInt(t))) {
					$("#ti").addClass("required");
					err = err+"\n - Select the relevant time period";
				}
				if(f.length==0) {
					$("#ifl").addClass("required");
					err = err+"\n - Select the import file";
				}
				if(err.length>0) {
					alert("Please attend to the following errors: "+err);
				}
			}
		});
	});
</script>
<?php
	break;	//default
} 
?>