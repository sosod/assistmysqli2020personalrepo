<?php
if (!isset($helperObj)) {
    $helperObj = new SDBP5B_HELPER();
}
/***** DRAW NAVIGATION BUTTONS *****/
function drawNav($nav,$id) {
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?><script type=text/javascript>
		  $(function()
		  {
			$("#m2").buttonset();
			$("#m2 input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo "<div id=m2 style=\"font-size: 8pt; font-weight: bold;\">";
	foreach($nav as $key => $m) {
		echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
		echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
	}
	echo "</div>";
}	//end drawNav($nav)
function drawNavSub($nav,$id) {
	/*** variables:
		$nav[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	?><script type=text/javascript>
		  $(function()
		  {
			$("#m3").buttonset();
			$("#m3 input[type='radio']").click( function()
			{
			  document.location.href = $(this).val();
			});
		  });
	</script>
	<?php
	echo "<div id=m3 style=\"padding-top: 5px; font-size: 0.9em\">";
	foreach($nav as $key => $m) {
		echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
		echo "<label for=\"$key\" style=\"background: #fafaff url();\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
	}
	echo "</div>";
}	//end drawNav($nav)

function drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form,$width) {
/*** variables:
		$url = $_SERVER['REQUEST_URI']
		$search   $search_label
		$page / $pages
		$start
		$limit
***/
	echo "<form name=page action=$url method=get>";
		echo "<table cellpadding=5 cellspacing=0 width=$width style=\"margin-bottom: 10px;\">";
			echo "<tr>";
				echo "<td width=45% style=\"border-right: 0px;\" class=blank>";
					echo "<input type=button id=b1 value=\"|<\" onclick=\"goNext('$url',0,'$search');\">";
						echo "&nbsp;";
					echo "<input type=button id=b2 value=\"<\" onclick=\"goNext('$url',".($start-$limit).",'$search');\">";
				if($page<2) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b1').disabled = true;";
					echo "document.getElementById('b2').disabled = true;";
					echo "</script>";
				}
						echo " Page $page / $pages ";
					echo "<input type=button value=\">\" id=b3 onclick=\"goNext('$url',".($start+$limit).",'$search');\">";
						echo "&nbsp;";
					echo "<input type=button value=\">|\" id=b4 onclick=\"goNext('$url','e','$search');\">";
				if($page==$pages) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b3').disabled = true;";
					echo "document.getElementById('b4').disabled = true;";
					echo "</script>";
				}
				echo "</td>";
				echo "<td width=10% style=\"border-right: 0px; border-left: 0px;\" class=\"centre blank\">";
					if(strlen($search)>0) {
						echo "<input type=button value=\"Clear Search\" onclick=\"document.location.href = '$url';\" style=\"float: center\">";
					}
				echo "</td>";
				echo "<td width=45%  class=\"right blank\" style=\"border-left: 0px;\">$search_label:&nbsp;<input type=hidden name=s value=0><input type=text width=10 name=f>&nbsp;<input type=submit value=\" Go \"></td>";
			echo "</tr>";
		echo "</table>";
	echo "</form>";
}	//end drawPage()


function drawPaging_noSearch($url,$search,$start,$limit,$page,$pages,$search_label,$form) {
/*** variables:
		$url = $_SERVER['REQUEST_URI']
		$search   $search_label
		$page / $pages
		$start
		$limit
***/
		echo "<table cellpadding=5 cellspacing=0 width=220 style=\"margin-bottom: 10px;\">";
			echo "<tr class=no-highlight>";
				echo "<td class=\"centre blank\">";
					echo "<input type=button id=b1 value=\"|<\" onclick=\"goNext('$url',0,'$search');\">";
						echo "&nbsp;";
					echo "<input type=button id=b2 value=\"<\" onclick=\"goNext('$url',".($start-$limit).",'$search');\">";
						echo " Page $page / $pages ";
					echo "<input type=button value=\">\" id=b3 onclick=\"goNext('$url',".($start+$limit).",'$search');\">";
						echo "&nbsp;";
					echo "<input type=button value=\">|\" id=b4 onclick=\"goNext('$url','e','$search');\">";
				echo "</td>";
			echo "</tr>";
		echo "</table>";
				if($page<2) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b1').disabled = true;";
					echo "document.getElementById('b2').disabled = true;";
					echo "</script>";
				}
				if($page==$pages) {
					echo "<script type=text/javascript>";
					echo "document.getElementById('b3').disabled = true;";
					echo "document.getElementById('b4').disabled = true;";
					echo "</script>";
				}
}	//end drawPage()

//View page filter box
function drawViewFilter($who,$what,$when,$sel,$display,$filter,$head) {
/* 	$filter = array(
		'who'	=> #
		'what'	=> #
		'when'	=> array(0=>#,1=>#),
	)
*/
	global $self;
	global $valid_dir_sub;
	global $helperObj;
echo "<table class=noborder width=100%>
	<tr class=no-highlight>
		<td class=noborder>$head</td>
		<td class=\"right noborder\">";
	echo "<form id=filter action=$self method=post>";
	echo "<table style=\"margin-bottom: -10px; float: right;\">
		<tr>
		<td><i>";
	if($who) {
		global $lists;
		$dir = $lists['dir'];
		if(isset($lists['subdir']['dir'])) {
			$sub = $lists['subdir']['dir'];
		}
		echo "&nbsp;Who: <select name=filter_who>";
				//<option value=All ".( (!isset($filter['who']) || !is_array($filter['who']) || strlen($filter['who'][0])!=1 || !checkIntRef($filter['who'][1]) || strtolower($filter['who'][1])=="all") ? "selected" : "")." >All</option>";
			foreach($dir as $d) {
				if(isset($valid_dir_sub[$d['id']])) {
					if($filter['who'][1]=="All") { $filter['who'] = array("D",$d['id']); }
					echo "<option value=D_".$d['id']." ".( ($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") ? "selected" : "")." >".(strlen($helperObj->decode($d['value']))>53 ? substr($helperObj->decode($d['value']),0,53)."..." : $d['value'])."</option>";
					if(isset($lists['subdir']['dir'])) {
						foreach($sub[$d['id']] as $s) {
							if(isset($valid_dir_sub[$d['id']][$s['id']])) {
								echo "<option value=S_".$s['id']." ".( ($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") ? "selected" : "")." >&nbsp;-&nbsp;".(strlen($helperObj->decode($s['value']))>50 ? substr($helperObj->decode($s['value']),0,50)."..." : $s['value'])."</option>";
							}
						}
					}
				}
			}
		echo "</select>&nbsp;&nbsp;&nbsp;";
	}
	if($what) {
		echo "What: <select name=filter_what>
				<option value=All ".((!isset($filter['what']) || strtolower($filter['what'])=="all") ? "selected" : "")." >All KPIs</option>
				<option value=op ".((isset($filter['what']) && strtolower($filter['what'])=="op") ? " selected " : "")." >Operational KPIs</option>
				<option value=cap ".((isset($filter['what']) && strtolower($filter['what'])=="cap") ? " selected " : "")." >Capital Project KPIs</option>
				<option value=top ".((isset($filter['what']) && strtolower($filter['what'])=="top") ? " selected " : "")." >Top Layer KPIs</option>
			</select>&nbsp;&nbsp;&nbsp;";
	}
	if($when) {
		global $time;
		echo "When: <select name=filter_when[]>";
			foreach($time as $t) {
				echo "<option value=".$t['id']." ".(( (!isset($filter['when'][0]) && $t['id']==1) || ($t['id'] == $filter['when'][0]) ) ? "selected" : "")." >".$t['display_short']."</option>";
			}
		echo "</select>";
		if($sel>1) {
			echo "&nbsp;to&nbsp;<select name=filter_when[]>";
				foreach($time as $t) {
					echo "<option value=".$t['id']." ".(( (!isset($filter['when'][1]) && $t['id']==12) || ($t['id'] == $filter['when'][1]) ) ? "selected" : "")." >".$t['display_short']."</option>";
				}
			echo "</select>";
		}
		echo "&nbsp;&nbsp;&nbsp;";
	}
	if($display) {
		echo "Display: <select name=filter_display>";
			echo "<option ".(($filter['display']!="ALL") ? "selected" : "")." value=LIMIT>Limited</option>";
			echo "<option ".(($filter['display']=="ALL") ? "selected" : "")." value=ALL>All columns</option>";
		echo "</select>";
	}
	echo "		<input type=submit value=Apply id=view_filter />&nbsp;&nbsp;</i>
			</td>
		</tr>
	</table></form>";
	echo "</td></tr></table>";
	if($who) {
		return $filter['who'];
	}
}



function drawManageFilter($who,$what,$when,$display,$filter,$head) {
/* 	$filter = array(
		'who'	=> #
		'what'	=> #
		'when'	=> array(0=>#,1=>#),
	)
*/
	global $self;
	global $page_id;
	global $valid_dir_sub;
	global $my_access; //arrPrint($my_access['manage']);
	global $base;
	global $head_dir;
	global $mheadings;
	global $helperObj;
echo "<table class='noborder' width=100% height=40px>
	<tr class=no-highlight>
		<td class=noborder><h2 style=\"margin-top: 0px; \">$head</h2></td>
		<td class=noborder>";
	echo "<form id=filter action=".$self."?page_id=".$page_id." method=post style='margin-bottom: -20px;'>";
	echo "<table style='float: right;'>
		<tr>
		<td><i>";
	if($who) {
		global $lists;
		$dir = $lists['dir'];
		if(isset($lists['subdir']['dir'])) {
			$sub = $lists['subdir']['dir'];
		}
		echo "&nbsp;Who: <select id=who name=filter_who>";
			if($base[1]=="dept") {
			$sel = false;
				foreach($dir as $d) {
					if(isset($valid_dir_sub[$d['id']])) {
						if($filter['who'][1]=="All" && isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id]) { $filter['who'] = array("D",$d['id']); }
						echo "<option value=\"".(isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id] ? "D_".$d['id']."\" style=\"color: #006600;\"" : "X\" style=\"color: #999999;\"").( ($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") ? "selected" : "")." >"
								.(strlen($helperObj->decode($d['value']))>53 ? substr($helperObj->decode($d['value']),0,53)."..." : $d['value'])
								."</option>";
						if($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") $sel = true;
						if(isset($lists['subdir']['dir'])) {
							foreach($sub[$d['id']] as $s) {
								if(isset($valid_dir_sub[$d['id']][$s['id']]) && ( (isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id] ) || (isset($my_access['manage']['SUB'][$s['id']]) && $my_access['manage']['SUB'][$s['id']]['act_'.$page_id]))) {
									if($filter['who'][1]=="All") { $filter['who'] = array("S",$s['id']); }
									echo "<option value=\"S_".$s['id']."\" style=\"color: #006600;\" ".( ($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") ? "selected" : "")." >
											&nbsp;-&nbsp;".(strlen($helperObj->decode($s['value']))>50 ? substr($helperObj->decode($s['value']),0,50)."..." : $s['value'])
											."</option>";
									if($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") $sel = true;
								}
							}
						}
					}
				}
				if($page_id == "update" && count($my_access['manage']['OWN'])>0) {
					echo "<option value=X style=\"color: #999999;\">".$mheadings['KPI']['kpi_ownerid']['h_client']."</option>";
					foreach($my_access['manage']['OWN'] as $i => $o) {
						if(isset($lists['owner'][$i]) && $lists['owner'][$i]['active']) {
							if($filter['who'][1]=="All") { $filter['who'] = array("O",$i); }
							echo "<option ".(($filter['who'][1]==$i && $filter['who'][0]=="O") ? "selected" : "")." value=O_".$i." style=\"color: #006600;\">&nbsp;-&nbsp;".$helperObj->decode($lists['owner'][$i]['value'])."</option>";
							if($filter['who'][1]==$i && $filter['who'][0]=="O") $sel = true;
						}
					}
				}
				/*if(!$sel) {
					echo "<script type=text/javascript>
							$(function() {

							});
						  </script>";
				}*/
			} else {
				foreach($dir as $d) {
					if(isset($valid_dir_sub[$d['id']])) {
						if($filter['who'][1]=="All") { $filter['who'] = array("D",$d['id']); }
						echo "<option value=\"D_".$d['id']."\"".( ($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") ? "selected" : "")."  style=\"color: #006600;\">"
								.(strlen($helperObj->decode($d['value']))>53 ? substr($helperObj->decode($d['value']),0,53)."..." : $d['value'])
								."</option>";
						if(isset($lists['subdir']['dir'])) {
							foreach($sub[$d['id']] as $s) {
								if(isset($valid_dir_sub[$d['id']][$s['id']])) {
									echo "<option value=\"S_".$s['id']."\" ".( ($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") ? "selected" : "")."  style=\"color: #006600;\">
											&nbsp;-&nbsp;".(strlen($helperObj->decode($s['value']))>50 ? substr($helperObj->decode($s['value']),0,50)."..." : $s['value'])
											."</option>";
								}
							}
						}
					}
				}
				if($base[1]=="top" && $page_id == "update" && count($my_access['manage']['T_OWN'])>0) {
					echo "<option value=X style=\"color: #999999;\">".$mheadings['TOP']['top_ownerid']['h_client']."</option>";
					foreach($my_access['manage']['T_OWN'] as $i => $o) {
						if(isset($lists['owner'][$i]) && $lists['owner'][$i]['active']) {
							if($filter['who'][1]=="All") { $filter['who'] = array("O",$i); }
							echo "<option ".(($filter['who'][1]==$i && $filter['who'][0]=="O") ? "selected" : "")." value=O_".$i." style=\"color: #006600;\">&nbsp;-&nbsp;".decode($lists['owner'][$i]['value'])."</option>";
							if($filter['who'][1]==$i && $filter['who'][0]=="O") $sel = true;
						}
					}
				}
			}
		echo "</select>&nbsp;&nbsp;&nbsp;";
	}
	if($what) {
		echo "What: <select name=filter_what>
				<option value=All ".((!isset($filter['what']) || strtolower($filter['what'])=="all") ? "selected" : "")." >All KPIs</option>
				<option value=op ".((isset($filter['what']) && strtolower($filter['what'])=="op") ? " selected " : "")." >Operational KPIs</option>
				<option value=cap ".((isset($filter['what']) && strtolower($filter['what'])=="cap") ? " selected " : "")." >Capital Project KPIs</option>
				<option value=top ".((isset($filter['what']) && strtolower($filter['what'])=="top") ? " selected " : "")." >Top Layer KPIs</option>
			</select>&nbsp;&nbsp;&nbsp;";
	}
	if($when) {
		global $time;
		echo "When: <select name=filter_when>";
			foreach($time as $t) {
				echo "<option value=".$t['id']." ".(( (!isset($filter['when']) && $t['id']==1) || ($t['id'] == $filter['when']) ) ? "selected" : "")." >".$t['display_short']."</option>";
			}
		echo "</select>";
		echo "&nbsp;&nbsp;&nbsp;";
	}
	if($display) {
		echo "Display: <select name=filter_display>";
			echo "<option ".(($filter['display']!="ALL") ? "selected" : "")." value=LIMIT>Limited</option>";
			echo "<option ".(($filter['display']=="ALL") ? "selected" : "")." value=ALL>All columns</option>";
		echo "</select>";
	}
	echo "		<input type=button value=Apply id=manage_filter />&nbsp;&nbsp;</i>
			</td>
		</tr>
	</table></form>
	<script type=text/javascript>
		$(function() {
			$(\"#manage_filter\").click(function() {";
			if($who) {
				echo "v = $(\"#who\").val();
				if(v==\"X\") {
					alert(\"You do not have access to the selected ".$head_dir.".\");
				} else {
					$(\"#filter\").submit();
				}";
			} else {
				echo "$(\"#filter\").submit();";
			}
			echo "});
		});
	</script>
	";
	echo "</td></tr></table>";
	if($who) {
		return $filter['who'];
	}
}
function drawManageFormFilter($who,$what,$when,$display,$filter,$page_id) {
	echo "<input type=hidden name=action id=action value=\"SAVE\" /><input type=hidden name=page_id value=".$page_id." />";
	if($who) {
		//echo "<br />Who: ";
		echo "<input type=hidden name=filter_who value=\"".implode("_",$filter['who'])."\" />";
	}
	if($what) {
		//echo "<br />What: ";
		echo "<input type=hidden name=filter_what value=\"".$filter['what']."\" />";
	}
	if($when) {
		//echo "<br />When: ";
		echo "<input type=hidden name=filter_when value=\"".$filter['when']."\" />";
	}
	if($display) {
		//echo "<br />Display: ";
		echo "<input type=hidden name=filter_display value=\"".$filter['display']."\" />";
	}
}
function drawManageFinFilter($who,$what,$when,$display,$filter) {
/* 	$filter = array(
		'who'	=> #
		'what'	=> #
		'when'	=> array(0=>#,1=>#),
	)
*/
	global $self;
	global $page_id;
	global $valid_dir_sub;
	global $my_access;
	global $base;
	global $head_dir;
	global $mheadings;
	global $helperObj;
echo "<table class=noborder style=\"margin-top: -5px;\">
	<tr class=no-highlight>
		<td class=\"right top noborder\" style=\"padding-bottom: 5px;\">";
	echo "<form id=filter action=".$self."?page_id=".$page_id." method=post style=\"margin-bottom: 0px;\">";
	echo "<table style=\"margin-bottom: -10px;\">
		<tr>
		<td valign=top><i>";
	if($who) {
		global $lists;
		$dir = $lists['dir'];
		if(isset($lists['subdir']['dir'])) {
			$sub = $lists['subdir']['dir'];
		}
		echo "&nbsp;Who: <select id=who name=filter_who>";
			if($base[1]=="dept") {
			$sel = false;
				foreach($dir as $d) {
					if(isset($valid_dir_sub[$d['id']])) {
						if($filter['who'][1]=="All" && isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id]) { $filter['who'] = array("D",$d['id']); }
						echo "<option value=\"".(isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id] ? "D_".$d['id']."\" style=\"color: #006600;\"" : "X\" style=\"color: #999999;\"").( ($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") ? "selected" : "")." >"
								.(strlen($helperObj->decode($d['value']))>53 ? substr($helperObj->decode($d['value']),0,53)."..." : $d['value'])
								."</option>";
						if($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") $sel = true;
						if(isset($lists['subdir']['dir'])) {
							foreach($sub[$d['id']] as $s) {
								if(isset($valid_dir_sub[$d['id']][$s['id']]) && ( (isset($my_access['manage']['DIR'][$d['id']]) && $my_access['manage']['DIR'][$d['id']]['act_'.$page_id] ) || (isset($my_access['manage']['SUB'][$s['id']]) && $my_access['manage']['SUB'][$s['id']]['act_'.$page_id]))) {
									if($filter['who'][1]=="All") { $filter['who'] = array("S",$s['id']); }
									echo "<option value=\"S_".$s['id']."\" style=\"color: #006600;\" ".( ($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") ? "selected" : "")." >
											&nbsp;-&nbsp;".(strlen($helperObj->decode($s['value']))>50 ? substr($helperObj->decode($s['value']),0,50)."..." : $s['value'])
											."</option>";
									if($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") $sel = true;
								}
							}
						}
					}
				}
				if($page_id == "update" && count($my_access['manage']['OWN'])>0) {
					echo "<option value=X style=\"color: #999999;\">".$mheadings['KPI']['kpi_ownerid']['h_client']."</option>";
					foreach($my_access['manage']['OWN'] as $i => $o) {
						if(isset($lists['owner'][$i]) && $lists['owner'][$i]['active']) {
							if($filter['who'][1]=="All") { $filter['who'] = array("O",$i); }
							echo "<option ".(($filter['who'][1]==$i && $filter['who'][0]=="O") ? "selected" : "")." value=O_".$i." style=\"color: #006600;\">&nbsp;-&nbsp;".$helperObj->decode($lists['owner'][$i]['value'])."</option>";
							if($filter['who'][1]==$i && $filter['who'][0]=="O") $sel = true;
						}
					}
				}
				/*if(!$sel) {
					echo "<script type=text/javascript>
							$(function() {

							});
						  </script>";
				}*/
			} else {
				foreach($dir as $d) {
					if(isset($valid_dir_sub[$d['id']])) {
						if($filter['who'][1]=="All") { $filter['who'] = array("D",$d['id']); }
						echo "<option value=\"D_".$d['id']."\"".( ($filter['who'][1]==$d['id'] && $filter['who'][0]=="D") ? "selected" : "")." >"
								.(strlen($helperObj->decode($d['value']))>53 ? substr($helperObj->decode($d['value']),0,53)."..." : $d['value'])
								."</option>";
						if(isset($lists['subdir']['dir'])) {
							foreach($sub[$d['id']] as $s) {
								if(isset($valid_dir_sub[$d['id']][$s['id']])) {
									echo "<option value=\"S_".$s['id']."\" ".( ($filter['who'][1]==$s['id'] && $filter['who'][0]=="S") ? "selected" : "")." >
											&nbsp;-&nbsp;".(strlen($helperObj->decode($s['value']))>50 ? substr($helperObj->decode($s['value']),0,50)."..." : $s['value'])
											."</option>";
								}
							}
						}
					}
				}
			}
		echo "</select>&nbsp;&nbsp;&nbsp;";
	}
	if($what) {
		echo "What: <select name=filter_what>
				<option value=All ".((!isset($filter['what']) || strtolower($filter['what'])=="all") ? "selected" : "")." >All KPIs</option>
				<option value=op ".((isset($filter['what']) && strtolower($filter['what'])=="op") ? " selected " : "")." >Operational KPIs</option>
				<option value=cap ".((isset($filter['what']) && strtolower($filter['what'])=="cap") ? " selected " : "")." >Capital Project KPIs</option>
				<option value=top ".((isset($filter['what']) && strtolower($filter['what'])=="top") ? " selected " : "")." >Top Layer KPIs</option>
			</select>&nbsp;&nbsp;&nbsp;";
	}
	if($when) {
		global $time;
		echo "When: <select name=filter_when>";
			foreach($time as $t) {
				echo "<option value=".$t['id']." ".(( (!isset($filter['when']) && $t['id']==1) || ($t['id'] == $filter['when']) ) ? "selected" : "")." >".$t['display_short']."</option>";
			}
		echo "</select>";
		echo "&nbsp;&nbsp;&nbsp;";
	}
	if($display) {
		echo "Display: <select name=filter_display>";
			echo "<option ".(($filter['display']!="ALL") ? "selected" : "")." value=LIMIT>Limited</option>";
			echo "<option ".(($filter['display']=="ALL") ? "selected" : "")." value=ALL>All columns</option>";
		echo "</select>";
	}
	echo "		<input type=button value=Apply id=manage_filter />&nbsp;&nbsp;</i>
			</td>
		</tr>
	</table></form>
	<script type=text/javascript>
		$(function() {
			$(\"#manage_filter\").click(function() {";
			if($who) {
				echo "v = $(\"#who\").val();
				if(v==\"X\") {
					alert(\"You do not have access to the selected ".$head_dir.".\");
				} else {
					$(\"#filter\").submit();
				}";
			} else {
				echo "$(\"#filter\").submit();";
			}
			echo "});
		});
	</script>
	";
	echo "</td></tr></table>";
	if($who) {
		return $filter['who'];
	}
}

function drawSortUl($items,$width) {
	echo "	<style type=text/css>
		.ui-state-highlightsort {
			height: 1.5em; line-height: 1.2em;
			list-style-image: none;
			border: 1px solid #fe9900;
		}
	</style>
<script type=text/javascript>
	$(function() {
		$( \"#sortable\" ).sortable({
			placeholder: \"ui-state-highlightsort\",
		});
		$( \"#sortable\" ).disableSelection();
	});
	</script>
	<div class=blank><ul id=sortable>";
		foreach($items as $i) {
			drawSortLi($i['id'],$i['value'],$width);
		}
	echo "</ul></div>";
}

function drawSortLi($id, $value, $width) {
	echo "<li class=\"ui-state-default\" style=\"cursor:hand; width: ".$width."px; overflow: auto\">
	<span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
	&nbsp;<input type=hidden name=sort[] value=\"".$id."\">".$value."</li>";
}
/******* GENERAL FUNCTIONS *******/



/** DETERMINE FONT COLOUR BY STATUS COLOUR **/
function textColour($color) {
	$text = "000000";
	global $helperObj;
	$w = 0;
	$b = 0;
	for($i=0;$i<strlen($color);$i+=1) {
		$sub = substr($color,$i,1);
		if($helperObj->checkIntRef($sub) || $sub == "0") {
			$b++;
			if($i/2==ceil($i/2)) $b++;
		} else {
			$w++;
			if($i/2==ceil($i/2)) $w++;
		}
	}
	if($b>=$w) { $text = "ffffff"; } else { $text="000000"; }
	return $text;
}


function goBack($url,$txt="Go Back") {
    global $helperObj;
    $helperObj->displayGoBack($url,$txt);
	/*global $self;
	if(strlen($txt)==0) { $txt = "Go Back"; }
	if(strlen($url)>0) {
		echo "<p><a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></p>";
	} else {
		echo "<p><a href=# onclick=\"history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=# onclick=\"history.back();\">Go Back</a></p>";
	}*/
}


function logChanges($section,$id,$v,$lsql) {
/*
	$section = "KPI", "TOP", "SETUP", "CAP"
	$id = obj_id
	$v = array('fld','timeid','text','old','new','act','YN')
*/
	$easy = array("CAP","CF","RS");
	global $dbref;
	global $tkid;
	global $today;
	global $helperObj;
	$fields = array();
	$values = array();
	if(in_array($section,$easy)) {
				$f = 0;
		switch($section) {
		case "CAP":
				$fld = "clog_";
				$table_tbl = "capital";
				$fields[$f] = $fld."capid";			$values[$f] = $id;					$f++;
			break;
		case "CF":
				$fld = "flog_";
				$table_tbl = "cashflow";
				$fields[$f] = $fld."kpiid";			$values[$f] = $id;					$f++;
			break;
		case "RS":
				$fld = "rlog_";
				$table_tbl = "revbysrc";
				$fields[$f] = $fld."kpiid";			$values[$f] = $id;					$f++;
			break;
		}
				$fields[$f] = $fld."tkid";			$values[$f] = "'$tkid'";			$f++;
				$fields[$f] = $fld."date";			$values[$f] = $today;				$f++;
				$fields[$f] = $fld."field";			$values[$f] = "'".$v['fld']."'";	$f++;
				$fields[$f] = $fld."timeid";		$values[$f] = "".$v['timeid']."";	$f++;
				$fields[$f] = $fld."transaction";	$values[$f] = "'".$v['text']."'";	$f++;
				$fields[$f] = $fld."old";			$values[$f] = "'".$v['old']."'";	$f++;
				$fields[$f] = $fld."new";			$values[$f] = "'".$v['new']."'";	$f++;
				$fields[$f] = $fld."act";			$values[$f] = "'".$v['act']."'";	$f++;
				$fields[$f] = $fld."yn";			$values[$f] = "'".$v['YN']."'";		$f++;
				$fields[$f] = $fld."lsql";			$values[$f] = "'$lsql'";			$f++;
				$sql = "INSERT INTO ".$dbref."_".$table_tbl."_log
						(".implode(",",$fields).") VALUES
						(".implode(",",$values).")";
				$mid = $helperObj->db_insert($sql);
	} else {
		switch($section) {
			case "KPI":
				$sql = "INSERT INTO ".$dbref."_kpi_log (klog_kpiid,klog_tkid,klog_date,klog_field,klog_timeid,klog_transaction,klog_old,klog_new,klog_act,klog_yn,klog_lsql) VALUES
				($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
                $helperObj->db_insert($sql);
				break;
			case "TOP":
				$sql = "INSERT INTO ".$dbref."_top_log (tlog_topid,tlog_tkid,tlog_date,tlog_field,tlog_timeid,tlog_transaction,tlog_old,tlog_new,tlog_act,tlog_yn,tlog_lsql) VALUES
				($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
                $helperObj->db_insert($sql);
				break;
			case "SETUP":
				$sql = "INSERT INTO ".$dbref."_setup_log (slog_section, slog_ref, slog_tkid, slog_tkname, slog_date, slog_field, slog_transaction, slog_old, slog_new, slog_yn, slog_lsql) VALUES
				('".$v['section']."','".$v['ref']."','$tkid','".code($_SESSION['tkn'])."',now(),'".$v['field']."','".$v['text']."','".$v['old']."','".$v['new']."','".$v['YN']."','$lsql')";
                $helperObj->db_insert($sql);
				break;
		}
	}
}


function displayLog($sql,$flds) {
	/* $flds = array('date'=>,'user'=>,'action'=>); */
/*	$logs = mysql_fetch_all($sql);
	if(count($logs)>0) {
		echo "<h3 class=log>Change log</h3>";
		echo "<table width=700>
				<tr>
					<th class=log>Date</th>
					<th class=log>User</th>
					<th class=log>Action</th>
				</tr>";
			foreach($logs as $l) {
				echo "<tr>";
					$a=0;
					foreach($flds as $f) {
						$a++;
						if($a==1)
							echo "<td class=\"log centre\">".date("d M Y H:i",strtotime($l[$f]))."</td>";
						else
							echo "<td class=log>".$l[$f]."</td>";
					}
				echo "</tr>";
			}
		echo "</table>";
	}*/
	displayAuditLog($sql,$flds);
}

/*******************************/
/******* KPI RESULT CALC *******/
/*******************************/
/*
 	0	grey	Not measured / Not available
	1	red		Not met [0 - 74.99%]
	2	orange	almost met [75 - 99.99%]
	3	green	met [100%]
	4	d.green	well met [100.01 - 149.99%]
	5	blue	extremely well met [150%+]
*/
//calcChange for Capital Projects/Monthly Cashflow
function calcChange($fld,$targ,$actl) {
	$val = 0;
	switch($fld) {
	case "perc":
			if($targ>0) {
				$val = $actl/$targ*100;
			} else {
				$val = 100;
			}
		break;
	case "var":
		$val = $targ - $actl;
		break;
	}
	return $val;
}

//function KPIcalcResult(array of all targets/actuals, calctype, time period range, current time id)
function KPIcalcResult($values,$ct,$filter,$t) {
	global $result_settings;
	global $base;
	$almost_met_limit = $result_settings[2]['limit_decimal'];
	$well_met_limit = $result_settings[5]['limit_decimal'];

	//$result = array('r'=>0,'style'=>"result0",'text'=>"N/A",'target'=>0,'actual'=>0);
	$result = $result_settings[0];
	/* determine target & actual to use in calculating result */
	$targ = 0; $actl = 0;
	if($t=="ALL") {		//calculate overall result
		switch($ct) {
			case "CO":
				foreach($values['target'] as $i => $vt) {
					//echo "<P>".$i." :: ".$targ." :: ".$actl;
					$va = $values['actual'][$i];
					if($targ < $vt) { $targ = $vt; }
					if($actl < $va) { $actl = $va; }
				}
				break;
			case "ACC":
			case "ACCREV":
			case "ZERO":
				$targ = array_sum($values['target']);
				$actl = array_sum($values['actual']);
				break;
			case "STD":
			case "REV":
				$targ = array_sum($values['target']);
				$actl = array_sum($values['actual']);
				$tc = 0;
				foreach($values['target'] as $i => $vt) {
					if($vt>0) { $tc++; }
				}
				if($tc == 0) { $tc = 1; }
				$targ/=$tc;
				$actl/=$tc;
				break;
			case "LAST":
			case "LASTREV":
			case "LASTR":
			case "LASTZERO":
			case "LASTZ":
				foreach($values['target'] as $i => $vt) {
					$va = $values['actual'][$i];
					if($vt > 0) { $targ = $vt; }
					if($va > 0) { $actl = $va; }
				}
				break;
			case "NA":
				break;
		}
	} else {
				$targ = $values['target'][$t];
				$actl = $values['actual'][$t];
	}
	/* compare targ & actl to determine result value */
	switch($ct) {
		case "CO":
		case "STD":
		case "ACC":
		case "LAST":
			//echo "<P>".$targ." :: ".$actl;
			if($targ!= 0 || $actl != 0) {
				if($targ==0) {
					//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					$result = $result_settings[5];
				} else {
					$div = $actl/$targ;
					//echo " :: ".$div;
					if($div<$almost_met_limit) {
						$result = $result_settings[1];
						//$result['r']=1;	$result['style']="result1";	$result['text']="R";
					} elseif($div < 1) {
						$result = $result_settings[2];
						//$result['r']=2;	$result['style']="result2";	$result['text']="O";
					} elseif($div==1) {
						$result = $result_settings[3];
						//$result['r']=3;	$result['style']="result3";	$result['text']="G";
					} elseif($div < $well_met_limit) {
						$result = $result_settings[4];
						//$result['r']=4;	$result['style']="result4";	$result['text']="G2";
					} elseif($div >= $well_met_limit) {
						$result = $result_settings[5];
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					}
				}
			}
			break;
		case "REV":
		case "ACCREV":
		case "ZERO":
		case "LASTREV":
		case "LASTR":
		case "LASTZERO":
		case "LASTZ":
			if(in_array($ct,array("ZERO","LASTZ","LASTZERO")) || (in_array($ct,array("REV","LASTREV","LASTR","ACCREV")) && ($targ!=0 || $actl!=0))) {
				if($actl < $targ) {
					$result = $result_settings[5];
					//$result['r']=5;	$result['style']="result5";	$result['text']="B";
				} elseif($actl==$targ) {
					$result = $result_settings[3];
					//$result['r']=3;	$result['style']="result3";	$result['text']="G";
				} else {
					$result = $result_settings[1];
					//$result['r']=1;	$result['style']="result1";	$result['text']="R";
				}
			}
			break;
		case "NA":
			break;
	}
	$result['target'] = $targ;
	$result['actual'] = $actl;
	return $result;
}

function KPIresultDisplay($val,$tt) {
	if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
	switch($tt) {
		case 1:	return "R ".number_format($val,$decimal); break;
		case 2:	return number_format($val,$decimal)."%"; break;
		case 3: return number_format($val,$decimal); break;
		default: return $val; break;
	}
}

function KPIresultEdit($val,$tt,$fld,$annual=false,$hidden=false) {
	if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
	switch($tt) {
		case 1:	return "R <input type=".($hidden===true ? "hidden" : "text")." name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  />"; break;
		case 2:	return "<input type=".($hidden===true ? "hidden" : "text")." name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  /> %"; break;
		case 3: return "<input type=".($hidden===true ? "hidden" : "text")." name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\" />"; break;
		default: return $val; break;
	}
}



function FINresultDisplay($val,$tt) {
	if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
	switch($tt) {
		case 1:	return "R ".($val==0 ? "-" : number_format($val,$decimal)); break;
		case 2:	return number_format($val,$decimal)."%"; break;
		case 3: return number_format($val,$decimal); break;
		default: return $val; break;
	}
}








/********************/
/*** MODULE SETUP ***/
/********************/
function logUser() {
	global $tkid;
	global $tkname;
	global $helperObj;
//	global $dbref;
$modref = strtolower($_SESSION['ref']);
$cmpcode = strtolower($_SESSION['cc']);
$dbref = "assist_".$cmpcode."_".$modref;
	$req = $_REQUEST;
	$page = substr($_SERVER['PHP_SELF'],7,strlen($_SERVER['PHP_SELF']));
	$tkname = strFn("substr",$helperObj->code($tkname),0,99);
	$val = "";
	if(count($req)>0) {
		$vals = array();
		$keys = array_keys($req);
		foreach($keys as $k) {
			if($k!="PHPSESSID") {
				if(is_array($req[$k])) {
					$v = "array(";
					$v2 = array();
						foreach($req[$k] as $a => $b) {
							$v2[] = $a."=".$b;
						}
					$v.=implode(", ",$v2).")";
				} else {
					$v = $req[$k];
				}
				$vals[] = $k."=".$v;
			}
		}
		$val = $helperObj->code(implode(", ",$vals));
	}
	$sql = "INSERT INTO ".$dbref."_user_log (id,tkid,tkname,dttoday,page,request) VALUES (null,'$tkid','$tkname',now(),'$page','$val')";
    $helperObj->db_insert($sql);
}
function getUserAccess($tkid) {
	$user = array('access'=>array(),'manage'=>array(),'act'=>array());
	global $dbref;
    global $helperObj;
	$top_types = array("TOP","T_OWN");

	/* Setup > User Access */
	global $user_access_fields;
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true AND tkid = '$tkid'";
	$a = $helperObj->mysql_fetch_all($sql);
	if(count($a)>0) {
		$a = $a[0];
	} else {
		foreach($user_access_fields as $fld => $h) {
			$a[$fld] = false;
		}
	}
if(isset($a['module']) && $a['module']==true) {
		foreach($user_access_fields as $fld => $h) {
			$a[$fld] = true;
		}
}
	$user['access'] = $a;
	/* Setup > Administrators */
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_admins WHERE active = true and tkid = '$tkid' ORDER BY type, ref";
	$manage = $helperObj->mysql_fetch_all_by_id2($sql,"type","ref");
	$user['manage'] = $manage;
	$act = array();
	$aa = array('update'=>0,'edit'=>0,'create'=>0,'approve'=>0);
	$act['KPI'] = $a;
	$act['TOP'] = $a;
	foreach($user['manage'] as $ur => $usr) {
		if(in_array($ur,$top_types)) { $ut = "TOP"; } else { $ut = "KPI"; }
		foreach($usr as $u) {
			foreach(array_keys($aa) as $a) {
				if(isset($u['act_'.$a]) && $u['act_'.$a]) {
					if(isset($act[$ut][$a])) { $act[$ut][$a]++; } else { $act[$ut][$a] = 1; }
				}
			}
		}
	}
	$user['act'] = $act;
	return $user;
}
function getModuleDefaults() {
	global $dbref;
	global $helperObj;
	$sql = "SELECT * FROM ".$dbref."_setup_defaults WHERE active = true ORDER BY sort";
	//$rs = getRS($sql);
	$setup_defaults = array();
	//while($row = mysql_fetch_assoc($rs))
    $rows = $helperObj->mysql_fetch_all($sql);
    foreach ($rows as $row)
	{
		$setup_defaults[$row['code']] = $row['value'];
	}
	if(!isset($setup_defaults['PLC'])) { $setup_defaults['PLC']="N"; }
	return $setup_defaults;
}

function getModuleHeadings($get) {
	global $dbref;
    global $helperObj;
	$sql = "SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
			FROM ".$dbref."_setup_headings sh
			INNER JOIN ".$dbref."_setup_headings_setup shs
			  ON sh.h_id = shs.head_id
			  AND shs.active = true
			WHERE
			  sh.h_active = true
			AND
			  shs.section IN ('".implode("','",$get)."')
			ORDER BY
			  shs.section,
			  shs.c_sort,
			  shs.i_sort
			";
	//$rs = getRS($sql);
	$h = array();
	if(in_array("KPI",$get) || in_array("CF",$get) || in_array("CAP",$get)) {
		$row = $helperObj->mysql_fetch_all("SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
			FROM ".$dbref."_setup_headings sh
			INNER JOIN ".$dbref."_setup_headings_setup shs
			  ON sh.h_id = shs.head_id
			  AND shs.active = true
			WHERE
			  sh.h_active = true
			AND
			  sh.h_table = 'dir'
			");
		$h['dir'] = $row;
	}
	//while($row = mysql_fetch_assoc($rs))
    $rows = $helperObj->mysql_fetch_all($sql);
    foreach ($rows as $row){
		$h[$row['section']][$row['field']] = $row;
		if($row['fixed']) {
			$h['FIXED'][$row['field']] = $row;
		}
	}
	return $h;
}

function getTime($get_time,$get_open_time) {
	global $dbref;
	global $today;
	global $helperObj;
	//global $my_access;
	//global $base;
	//global $page_id;

	//arrPrint($base); echo $page_id;
	//arrPrint($my_access);


	$sql = "SELECT * FROM ".$dbref."_list_time WHERE id IN (".implode(",",$get_time).")";
	//$rs = getRS($sql);
	$t = array();
	//while($row = mysql_fetch_assoc($rs)) {
    $rows = $helperObj->mysql_fetch_all($sql);
    foreach ($rows as $row){
		if(!$get_open_time || ($today >=  strtotime($row['start_date']))) {
		//if(!$get_open_time || (($row['active_primary']) && $today>=strtotime($row['start_date']))) {
			$t[$row['id']] = $row;
			$t[$row['id']]['start_stamp'] = strtotime($row['start_date']);
			$t[$row['id']]['end_stamp'] = strtotime($row['end_date']);
			$t[$row['id']]['display_short'] = date("M Y",strtotime($row['end_date']));
			$t[$row['id']]['display_full'] = date("F Y",strtotime($row['end_date']));
		}
	}
	return $t;
}

function getOpenTime($time,$access) {
	$newtime = array();
	foreach($time as $ti => $t) {
	    $t_index = isset($t['active_'.$access]) ? $t['active_'.$access] : false;
		if($t_index) {
			$newtime[$ti] = $t;
		}
	}
	return $newtime;
}


function displayValue($val) {
	if(strlen($val)>50)
		return "<small>".$val."</small>";
	else
		return $val;
}














function mysql_fetch_alls($sql,$fld) {
	$result = getRS($sql);
    $resultArray = array();
    while($row = mysql_fetch_assoc($result)) {
		$resultArray[$row[$fld]] = $row;
	}
    return $resultArray;
}

function mysql_fetch_alls2($sql,$fld1, $fld2) {
	$result = getRS($sql);
    $resultArray = array();
    while($row = mysql_fetch_assoc($result)) {
		$resultArray[$row[$fld1]][$row[$fld2]] = $row;
	}
    return $resultArray;
}









/***** SUPPORT IMPORT ******/
function openLogImport() {
	global $cmpcode;
	global $modref;
	global $helperObj;
	$modref = strtoupper($modref);
	$ilf = $modref."/import/";
    $helperObj->checkFolder($ilf);
	$ilf.="import_log.csv";
	$import_log_file = fopen("../files/".$cmpcode."/".$ilf,"a");

	return $import_log_file;
}
//"date("Y-m-d H:i:s")","$_SERVER['REMOTE_ADDR']","action","$self","implode(",",$_REQUEST)"
function implodeArray($array) {
	$a = array();
	foreach($array as $key => $val) {
		if(is_array($val))
			$a[] = $key."=[".implode("|",$val)."]";
		else
			$a[] = $key."=".$val;
	}
	return $a;
}
function logImport($handle, $action, $page) {
	$fdata = array();
	$fdata[] = date("Y-m-d H:i:s");
	$fdata[] = $_SERVER['REMOTE_ADDR'];
	$fdata[] = $action;
	$fdata[] = $page;
	$arg = implodeArray($_REQUEST);
	fwrite($handle,"\"".implode("\",\"",$fdata)."\",\"".implode("\",\"",$arg)."\"\n");
}

function htmlPageHeader() {
	$echo = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">";
	$echo.= "<html>";
	$echo.= "<head>";
	$echo.= "<meta http-equiv=\"Content-Language\" content=\"en-za\">";
	$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">";
	$echo.= "<title>www.Action4u.co.za</title>";
	$echo.= "</head>";
	$echo.= "<script type =\"text/javascript\" src=\"https://assist.action4u.co.za/lib/jquery/js/jquery.min.js\"></script>";
	$echo.= "<script type =\"text/javascript\" src=\"https://assist.action4u.co.za/lib/jquery/js/jquery-ui.min.js\"></script>";
	$echo.= "<script type =\"text/javascript\" src=\"https://assist.action4u.co.za/lib/jquery/js/jquery-ui-timepicker-addon.js\"></script>";
	$echo.= "<link href=\"https://assist.action4u.co.za/lib/jquery/css/jquery-ui.css\" rel=\"stylesheet\" type=\"text/css\"/>";
	$echo.= "<link rel=\"stylesheet\" href=\"https://assist.action4u.co.za/assist.css\" type=\"text/css\">";
	$echo.= "<link rel=\"stylesheet\" href=\"https://assist.action4u.co.za/styles/style_blue.css\" type=\"text/css\">";
	$echo.= "<link rel=\"stylesheet\" href=\"https://assist.action4u.co.za/SDBP4/inc/main.css\" type=\"text/css\">";
	$echo.= "<script type =\"text/javascript\" src=\"https://assist.action4u.co.za/SDBP4/inc/main.js\"></script>";
	$echo.= "<base target=\"main\">";
	$echo.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
	return $echo;
}

/* REPORT */
function drawKRSum($what,$txt,$val,$i,$glossary="") {
	global $output;
	global $result_settings;
	global $helperObj;
	$echo  = "";
	if(!$helperObj->checkIntRef($val)) { $val = 0; }
	switch($what) {
		case "title":
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"\",\"Summary of Results\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td></td><td nowrap class=title2 style=\"text-align: left;\">Summary of Results</td>";
			} else {
				$echo ="</table><h2>Summary of Results</h2><table cellpadding=3 cellspacing=5 >";
			}
			break;
		case "title0":
			$t = "Summary of Results: ".$txt;
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"\",\"".$t."\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td></td><td nowrap class=title4 style=\"text-align: left;\">".$t."</td>";
			} else {
				$echo ="</table><h4 style=\"margin-bottom: 5px; margin-top: 10px;\">".$t."</h4><table cellpadding=3 cellspacing=5 width=330 style=\"margin-bottom: 20px;\">";
			}
			break;
		case "sum":
			if($output=="csv") {
				$echo ="\"\",\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo = "<tr><td></td><td nowrap style=\"font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$txt."</td><td style=\"font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\"></td><td nowrap style=\"text-align: left; font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$val."</td></tr>";
			} else {
				$echo = "<tr><td class=noborder>&nbsp;&nbsp;</td><td class=noborder style=\"font-weight: bold;\">".$txt."</td><td class=noborder><td class=noborder style=\"text-align: right;font-weight: bold;\">&nbsp;".$val."</td></tr>";
				$echo.= "</table>";
			}
			break;
		default:
			if($output=="csv") {
				$echo.="\"\",\"".$txt."\",\"".$result_settings[$i]['glossary']."\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo.= "<tr><td style=\"background-color: ".$result_settings[$i]['color']."\"> </td><td nowrap>".$txt."</td><td nowrap style='font-size: 75%; font-style:italic'>".$result_settings[$i]['glossary']."</td><td nowrap style=\"text-align: left;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td class=\"".$result_settings[$i]['style']." noborder\">&nbsp;</td><td class=noborder>".$txt."</td><td class=\"i noborder\" style='font-size:7pt'>&nbsp;".$result_settings[$i]['glossary']."</td><td class=\"right noborder\">&nbsp;".$val."</td></tr>";
			}
			break;
	}
	return $echo;
}

function echoMultipleSelectTip() {
	return "<br /><span class=\"ctrlclick iinform\">CTRL + Click to select multiple options</span>";
}




function displayTRDept($tr_dept,$type) {
	switch($type) {
	case "CSV":
	case "LOG":
		$tr_dept = str_replace("_|_B_|_","",$tr_dept);
		$tr_dept = str_replace("_||B||_","",$tr_dept);
		$tr_dept = str_replace("_|_I_|_","",$tr_dept);
		$tr_dept = str_replace("_||I||_","",$tr_dept);
		$tr_dept = str_replace("&lt;br /&gt;",chr(10),$tr_dept);
		break;
	case "HTML":
	case "XLS":
		$tr_dept = str_replace("_|_B_|_","<b>",$tr_dept);
		$tr_dept = str_replace("_||B||_","</b>",$tr_dept);
		$tr_dept = str_replace("_|_I_|_","<i>",$tr_dept);
		$tr_dept = str_replace("_||I||_","</i>",$tr_dept);
		$tr_dept = str_replace("&lt;br /&gt;","<br />",$tr_dept);
		break;
	}
	return $tr_dept;
}


function getList($table,$fld,$active) {
	global $dbref;
	global $helperObj;
	$list = array();
	$sql = "SELECT * FROM ".$dbref."_list_".$table." WHERE active = $active";
	$list = $helperObj->mysql_fetch_all_fld($sql,$fld);
	return $list;
}


function getValidPLC($kpi_id) {
	global $dbref;
	global $helperObj;
	$plcrow = array();

	$sql = "SELECT * FROM ".$dbref."_kpi_plc WHERE plckpiid = ".$kpi_id." AND plcyn = 'Y'";
		$plcrow = $helperObj->mysql_fetch_one($sql);

	if(isset($plcrow['plcid'])) {
		return array(0=>true,1=>$plcrow);
	} else {
		return array(0=>false);
	}

}








function applyFormat($v) {
	$format = array(
		'_|_B_|_'=>"<span class=b>",
		'_|_I_|_'=>"<span class=i>",
		'_||B||_'=>"</span>",
		'_||I||_'=>"</span>",
		'&lt;br /&gt;'=>"<br />",
	);
	foreach($format as $c => $f) {
		$v = str_replace($c,$f,$v);
	}
	return $v;
}

?>