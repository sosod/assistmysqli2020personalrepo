

var okIcon = "<div style='width: 16px; margin: 0 auto' class='ui-widget ui-state-yes'><span class='ui-icon ui-icon-check'>&nbsp;&nbsp;</span></div>";

function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+"&s="+s+"&f="+f;
}

//Function to change the background of a table row on hover
//EFFECTS ALL TABLES
/*$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).css("background-color","#EFEFEF"); },
		function(){ $(this).css("background-color","#FFFFFF"); }
	);
});*/
$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
		$("tr.no-highlight").off("mouseenter mouseleave");

	$(".datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'yy-mm-dd',
                    changeMonth:true,
                    changeYear:true
                });
	$(".datepicker").on("datepickercreate",function() {
		$(this).datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'yy-mm-dd',
                    changeMonth:true,
                    changeYear:true
                });
	});
});

function validateActual(v) {
	valid = true;
	decimal = 0;
	if(v.length>0) {
		if(!isNaN(parseFloat(v))) {
			for(var i=0; i<v.length; i++) {
				if(isNaN(v.substring(i,i+1))) {
					if(v.substring(i,i+1)==".") {
						if(decimal>0) {
							valid = false;
							break;
						} else {
							decimal++;
						}
					} else {
						valid = false;
						break;
					}
				}
			}
		} else {
			valid = false;
		}
	} else {
		valid = false;
	}
	return valid;
}

function validatePerf(required,minsize,v) {
	vali8 = false;
	if(!required) {
		vali8 = true;
	} else {
		vali8 = validateMinSize(minsize,v);
	}
	return vali8;
}

function validateMinSize(minsize,v) {
	val8 = false;
			var len = v.length;
			if(len>=minsize) {
				val8 = true;
			}
			//alert("vMS "+valid8+" len: "+len);
	return val8;
}

function validateCorrect(required,minsize,v,t,a,ct) {
	va8 = true;
	//alert(required);
	if(!required) {
		va8 = true;
	} else {
			a = parseFloat(a);
			t = parseFloat(t);
		if(ct=="ZERO" || ct=="REV" || ct=="LASTREV") {
			if(a>t) {
				va8 = validateMinSize(minsize,v);
			}
		} else {
			if(a<t) {
				va8 = validateMinSize(minsize,v);
			}
		}
	}
	//alert("vC "+va8+" ms: "+minsize+" v: '"+v+"' ct:"+ct+" t: "+t+" a: "+a+" a<t: "+(a<t));
	return va8;
}

function validateMe(fld,required,minsize,v,t,a,ct) {
	var v8 = true;
	var valid_act = true;
	switch(fld) {
	case "kr_perf": case "kr_perf[]":
	case "tr_perf": case "tr_perf[]":
		if(valid_act) {
			v8 = validatePerf(required,minsize,v);
		}
		break;
	case "kr_correct": case "kr_correct[]":
	case "tr_correct": case "tr_correct[]":
		if(valid_act) {
			v8 = validateCorrect(required,minsize,v,t,a,ct);
		}
		break;
	case "kr_actual": case "kr_actual[]":
	case "tr_actual": case "tr_actual[]":
		v8 = validateActual(v);
		valid_act = v8;
		break;
	}
	return v8;
}

function stripslashes (str) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +      fixed by: Mick@el
    // +   improved by: marrtins    // +   bugfixed by: Onno Marsman
    // +   improved by: rezna
    // +   input by: Rick Waldron
    // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   input by: Brant Messenger (http://www.brantmessenger.com/)    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: stripslashes('Kevin\'s code');
    // *     returns 1: "Kevin's code"
    // *     example 2: stripslashes('Kevin\\\'s code');
    // *     returns 2: "Kevin\'s code"
	return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
            return '\\';
        case '0':            return '\u0000';
        case '':
            return '';
        default:
            return n1;
		}
    });
}


function doAjax(url,dta) {
	var result;
	//alert("ajax");
	$.ajax({
		url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
		success: function(d) {
			//alert("d "+d[1]);
			//console.log(d);
			result=d;
			//return d;
		},
		error: function(d) { console.log(d); result=d; }
	});
	//alert("r "+result[1]);
	return result;
}

function doDeleteAttachment(index,url,dta) {
	var r = doAjax(url,dta);
	var i = index.split("_");
	a = i[0];
	//alert(r[0]+" => "+r[1]+" => "+r[2]);
	if(r[0]==1 || r[0]==true || r[0]=="ok") {
		$(function() {
			$("#li_"+index).hide();
			if(r[2].length==0) {
				var prefix = "kr_";
			} else {
				var prefix = r[2];
			}
			//alert(prefix);
			$("#"+a+"_"+prefix+"attachment").html(okIcon);
		});
	}
	showResponse(r[1]);
	//alert("Attachment deleted.");
	//alert(r[0]+" :: "+r[1]);
}


// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = [37, 38, 39, 40];

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;
}

function keydown(e) {
    for (var i = keys.length; i--;) {
        if (e.keyCode === keys[i]) {
            preventDefault(e);
            return;
        }
    }
}

function wheel(e) {
  preventDefault(e);
}

function disable_scroll() {
  if (window.addEventListener) {
      window.addEventListener('DOMMouseScroll', wheel, false);
  }
  window.onmousewheel = document.onmousewheel = wheel;
  document.onkeydown = keydown;
}

function enable_scroll() {
    if (window.removeEventListener) {
        window.removeEventListener('DOMMouseScroll', wheel, false);
    }
    window.onmousewheel = document.onmousewheel = document.onkeydown = null;
}



function alertArray(arr,i) {
	var data = "";
	var v;
	var t;
	for(x in arr) {
		v = arr[x];
		if($.isArray(v)) {
			t = alertArray(v,i+1)+"\n";
		} else {
			t = v;
		}
		var spaces = "";
		for(a=0;a<i;a++) { spaces+="\t"; }
		data += "\n"+spaces+x+"=>{"+t+"};";
	}
	if(i<1) {
		alert(data);
	} else {
		return data;
	}
}