<?php 
$section = "CAP";
			$get_lists = true;
			$get_active_lists = false;
			$locked_column = true;

include("inc/header.php");
//arrPrint($mheadings['CAP_R']);

//Filter settings
$filter = array();
	//WHEN
/*	if(isset($_REQUEST['filter_when'])) {
		$f_when = $_REQUEST['filter_when'];
		$filter_when = array(
			0=>($f_when[0]<=$f_when[1] ? $f_when[0] : $f_when[1]),
			1=>($f_when[0]<=$f_when[1] ? $f_when[1] : $f_when[0])
		);
	} else {
		$filter_to = ($today < strtotime("2011-06-30 23:59:59")) ? 1 : ( (date("m",$today)>6) ? (date("m",$today)-6) : (date("m",$today)+6) );
		$filter_from = ($filter_to > 4) ? $filter_to - 2 : 1;
		$filter_when = array($filter_from,$filter_to);
	}
	$filter['when'] = $filter_when;*/
	if(isset($_REQUEST['filter_when'])) {
		$filter_when = $_REQUEST['filter_when'];
	} else {
		$filter_to = $current_time_id;
		$filter_when = array($filter_to,$filter_to);
	}
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


//arrPrint($filter);


if(!$import_status[$section]) {
	die("<p>Capital Projects have not yet been loaded.  Please try again later.</p>");
} else {

	$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
		INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT c.cap_subid FROM ".$dbref."_capital c
		INNER JOIN ".$dbref."_subdir subdir ON cap_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE cap_active = true ) WHERE d.active = true";
	$valid_dir_sub = $helperObj->mysql_fetch_all_fld2($dir_sql,"dir","subdir");
//arrPrint($valid_dir_sub);

	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = "<h2 style=\"margin-top: 0px;\">";
	//if($filter['when'][0]==$filter['when'][1]) {
		$head.= $time[$filter['when'][0]]['display_full'];
	/*} else {
		$head.= $time[$filter['when'][0]]['display_full']." - ".$time[$filter['when'][1]]['display_full'];
	}*/
	$head.="</h2>";
	$filter['who'] = drawViewFilter(true,false,true,1,true,$filter,$head);
	//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
	//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);

	//SQL
	$object_sql = "SELECT c.* FROM ".$dbref."_capital c
	INNER JOIN ".$dbref."_subdir subdir ON cap_subid = subdir.id AND subdir.active = true
	".($filter['who'][0]=="D" && $helperObj->checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
	INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
	WHERE cap_active = true ".($filter['who'][0]=="S" && $helperObj->checkIntRef($filter['who'][1]) ? " AND cap_subid = ".$filter['who'][1] : "");
//echo $object_sql;	
	$results_sql = "SELECT r.* FROM ".$dbref."_capital_results r
	INNER JOIN ".$dbref."_capital c ON r.cr_capid = c.cap_id AND c.cap_active = true
	WHERE cr_timeid = ".$filter['when'][0]." AND cr_capid IN (SELECT c.cap_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
//	WHERE cr_timeid <= ".$filter['when'][1]." AND cr_capid IN (SELECT c.cap_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
	
	$results = $helperObj->mysql_fetch_all_fld2($results_sql,"cr_capid","cr_timeid");
	/*$fld_target = "cr_target";
	$fld_actual = "cr_actual";*/
	$rheadings = "CAP_R";
	
	if(isset($mheadings[$section]['capital_wards'])) {
		$wards_sql = "SELECT cw_capid, id, value, code FROM ".$dbref."_capital_wards INNER JOIN ".$dbref."_list_wards ON cw_listid = id 
		WHERE cw_active = true AND cw_capid IN (SELECT c.cap_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		$wa['capital_wards'] = $helperObj->mysql_fetch_all_fld2($wards_sql,"cw_capid","id");
	}
	if(isset($mheadings[$section]['capital_area'])) {
		$area_sql = "SELECT ca_capid, id, value, code FROM ".$dbref."_capital_area INNER JOIN ".$dbref."_list_area ON ca_listid = id 
		WHERE ca_active = true AND ca_capid IN (SELECT c.cap_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		$wa['capital_area'] = $helperObj->mysql_fetch_all_fld2($area_sql,"ca_capid","id");
	}
	if(isset($mheadings[$section]['capital_fundsource'])) {
		$fundsrc_sql = "SELECT cs_capid, id, value, code FROM ".$dbref."_capital_fundsource INNER JOIN ".$dbref."_list_fundsource ON cs_listid = id 
		WHERE cs_active = true AND cs_capid IN (SELECT c.cap_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		$wa['capital_fundsource'] = $helperObj->mysql_fetch_all_fld2($fundsrc_sql,"cs_capid","id");
	}

	
	$object_sql.= " ORDER BY dir.sort, subdir.sort, cap_id";
	
	include("view_table.php");

	
	
}	//if import_status['import_Section'] == false else	
?>
<p>&nbsp;</p>
</body>
</html>