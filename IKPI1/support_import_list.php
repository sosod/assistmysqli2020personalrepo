<?php 
//error_reporting(-1);
$page_title = "Lists";
$import_section = "LIST";
include("inc/header.php"); 
function valError($v,$e) {
	return $v." <b>->".$e."</b>";
}

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > ".$page_title, $self);


$listoflists = array(
	'all'=>				array('ignite'=>"All Lists",'doc'=>0),
	'dir'=>				array('ignite'=>"Directorates",'doc'=>0),
	'subdir'=>			array('ignite'=>"Sub-Directorates",'doc'=>4),
	'list_gfs'=>		array('ignite'=>"Function",'doc'=>12),
	'list_munkpa'=>		array('ignite'=>"Municipal KPA",'doc'=>15),
	'list_natkpa'=>		array('ignite'=>"National KPA",'doc'=>19),
	'list_ndp'=>		array('ignite'=>"NDP Objectives",'doc'=>23),
	'list_pdo'=>		array('ignite'=>"Pre-Determined Objectives",'doc'=>26),
	'list_natoutcome'=>	array('ignite'=>"National Outcome",'doc'=>29),
	'list_idp_kpi'=>	array('ignite'=>"Departmental SDBIP: IDP Objective",'doc'=>32),
	'list_idp_top'=>	array('ignite'=>"Top Layer: IDP Objective",'doc'=>37),
	'list_kpiconcept'=>	array('ignite'=>"KPI Concept",'doc'=>40),
	'list_kpitype'=>	array('ignite'=>"KPI Type",'doc'=>44),
	'list_riskrating'=>	array('ignite'=>"Risk Rating",'doc'=>48),
	'list_wards'=>		array('ignite'=>"Wards",'doc'=>52),
	'list_area'=>		array('ignite'=>"Area",'doc'=>56),
	'list_owner'=>		array('ignite'=>"KPI Owner / Driver",'doc'=>59),
	'list_fundsource'=>	array('ignite'=>"Funding Source",'doc'=>69),
	'list_repcate'=>	array('ignite'=>"Reporting Category",'doc'=>72),
);
foreach($listoflists as $l => $la) {
	if(!in_array($l,array("all","dir","subdir"))) {
		if($l=="list_gfs") {
			$listoflists[$l]['doc']+=3;
		} else {
			$listoflists[$l]['doc']+=7;
		}
	}
}
$sql = "SELECT h_client, h_table, h_length FROM ".$dbref."_setup_headings WHERE h_type IN ('LIST','WARDS','AREA','FUNDSRC','TEXTLIST')";
$client_headings = mysql_fetch_all($sql);
foreach($client_headings as $chead) {
	$htbl = $chead['h_table'];
	$chv = $chead['h_client'];
	$chl = $chead['h_length'];
	if(isset($listoflists[$htbl])) {
		$listoflists[$htbl]['client'] = $chv;
		$listoflists[$htbl]['h_client'] = $chv;
		$listoflists[$htbl]['h_length'] = $chl;
	} elseif(isset($listoflists['list_'.$htbl])) {
		$listoflists['list_'.$htbl]['client'] = $chv;
		$listoflists['list_'.$htbl]['h_client'] = $chv;
		$listoflists['list_'.$htbl]['h_length'] = $chl;
	}
}
$mheadings['LIST'] = $listoflists;
include("support_import_headings.php"); 

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

//arrPrint($_REQUEST);
//arrPrint($_FILES);

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_dir";
		$r_sql[] = "TRUNCATE ".$dbref."_subdir";		
		$r_sql[] = "TRUNCATE ".$dbref."_list_gfs";
		$r_sql[] = "TRUNCATE ".$dbref."_list_munkpa";
		$r_sql[] = "TRUNCATE ".$dbref."_list_natkpa";
		$r_sql[] = "TRUNCATE ".$dbref."_list_natoutcome";
		$r_sql[] = "TRUNCATE ".$dbref."_list_idp_kpi";
		$r_sql[] = "TRUNCATE ".$dbref."_list_idp_top";
		$r_sql[] = "TRUNCATE ".$dbref."_list_area";
		$r_sql[] = "TRUNCATE ".$dbref."_list_wards";
		$r_sql[] = "TRUNCATE ".$dbref."_list_owner";
		$r_sql[] = "TRUNCATE ".$dbref."_list_fundsource";
		$r_sql[] = "TRUNCATE ".$dbref."_list_kpiconcept";
		$r_sql[] = "TRUNCATE ".$dbref."_list_kpitype";
		$r_sql[] = "TRUNCATE ".$dbref."_list_riskrating";
		$r_sql[] = "TRUNCATE ".$dbref."_list_ndp";
		$r_sql[] = "TRUNCATE ".$dbref."_list_pdo";
		$r_sql[] = "TRUNCATE ".$dbref."_list_repcate";
		foreach($r_sql as $sql) {	
			$mnr = db_update($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = 'LIST'");
		$import_status['LIST'] = false;
		$result = array("ok","All List tables successfully reset.");
		logImport($import_log_file, $page_title." > Reset tables", $self);
		break;
}

$t = isset($_REQUEST['t']) ? $_REQUEST['t'] : "all";
//echo "<h2>".$listoflists[$t]."</h2>";

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		th.ignite { background-color: #555555; text-align: left; }
		th.doc { background-color: #999999; text-align: left; }
		</style>
<?php
logImport($import_log_file, $page_title." > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
$onscreen = "";
switch($act) {
	case "VIEW": case "RESET": case "GENERATE":
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).prop("id");
					var t = "<?php echo $t; ?>";
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=LIST&t='+t; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = 'support_import_list.php?act=RESET&t='+t; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").prop("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
		if($t=="all") { 
			?>
			<form id=list action=support_import_list.php method=post enctype="multipart/form-data">
				<input type=hidden name=t value="<?php echo $t; ?>" />
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to the Lists sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information.&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php if(!$import_status['LIST']) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Select the CSV file and click the Import button. &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php } ?>
				</table>
			</form>
			<?php displayGoBack("support_import.php"); 
			
			drawCSVColumnsGuidelines("","");
			
			
		} else {	//t == all
		}
		break;
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>List Item Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			$accept_text = "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
			$onscreen.= $accept_text;
			echo $accept_text;
		}
		$onscreen.= "<form id=save method=post action=support_import_list.php>";
		$onscreen.= "<input type=hidden name=t value=".$t." /><input type=hidden name=act value=ACCEPT />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, $page_title." > ERROR > $file doesn't exist", $self);
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, $page_title." > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, $page_title." > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value='".(isset($filen) ? $filen : $_REQUEST['f'])."' />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);

			if(count($data)>2) {
				$columns = array(
					'dir' => 			array('Ref'=>1,'Value'=>2),
					'subdir'=> 			array($listoflists['dir']['h_client']=>6,'Ref'=>9,'Value'=>7,'Primary'=>8),
					'list_gfs'=> 		array('Ref'=>13,'Value'=>12),
					'list_munkpa'=> 	array('Ref'=>17,'Value'=>15,'Code'=>16),
					'list_natkpa'=> 	array('Ref'=>21,'Value'=>19,'Code'=>20),
					'list_ndp'=>	 	array('Ref'=>24,'Value'=>23),
					'list_pdo'=>	 	array('Ref'=>27,'Value'=>26),
					'list_natoutcome'=> array('Ref'=>30,'Value'=>29),
					'list_idp_kpi'=> 	array('Ref'=>35,'Value'=>32,'Mun. KPA'=>33,'Nat. KPA'=>34),
					'list_idp_top'=> 	array('Ref'=>38,'Value'=>37),
					'list_kpiconcept'=>	array('Ref'=>42,'Value'=>40,'Code'=>41),
					'list_kpitype'=>	array('Ref'=>46,'Value'=>44,'Code'=>45),
					'list_riskrating'=>	array('Ref'=>50,'Value'=>48,'Code'=>49),
					'list_wards'=> 		array('Ref'=>52,'Value'=>53,'Municipal Ref'=>54),
					'list_area'=> 		array('Ref'=>56,'Value'=>57),
					'list_owner'=> 		array('Value'=>59),
					'list_fundsource'=> array('Ref'=>70,'Value'=>69),
					'list_repcate'=> 	array('Ref'=>73,'Value'=>72),
				);
				//add 7 to the column reference number to account for the expanded GFS/Function list
				foreach($columns as $c => $ca) {
					if($c!="dir" && $c!="subdir") {
						foreach($ca as $x=>$y) {
							$columns[$c][$x] = $y+7;
						}
					}
				}
				//arrPrint($listoflists);
				unset($listoflists['all']);
				$idp_lists = array();
				$idp_lists['list_munkpa'] = array();
				$idp_lists['list_natkpa'] = array();
				$a_sql = array();
				$z_sql = array();
				$doc_head = $data[0]; //arrPrint($doc_head);
				foreach($listoflists as $key => $list_info) {
					//arrPrint($list_info);
					$str_max = (isset($list_info['h_length']) && checkIntRef($list_info['h_length']) ) ? $list_info['h_length'] : 200;
					$a_sql = array();
					/*$onscreen.= "<h2 style='margin-bottom: -10px; margin-top: 20px;'>Default Heading: ".$list_info['ignite']."</h2>";
					$onscreen.= "<h3 style='margin-bottom: -10px'>Module Heading: ".$list_info['client']."</h3>";
					$onscreen.= "<h3 style='margin-bottom: 5px'>Document Heading: ".(strlen($doc_head[$list_info['doc']])>0 ? $doc_head[$list_info['doc']] : $doc_head[$list_info['doc']+1])."</h3>";
					*/
					$onscreen.="<hr style='margin-top: 20px;'>
					<table style='margin: 10px 5px 10px 0px;'>
						<tr>
							<th class=ignite>Module Heading:</th>
							<td class=b style='padding-right: 20px;'>".$list_info['h_client']."</td>
						</tr>
						<tr>
							<th class=doc>Document Heading:</th>
							<td class=b style='padding-right: 20px;'>".(strlen($doc_head[$list_info['doc']])>0 ? $doc_head[$list_info['doc']] : $doc_head[$list_info['doc']+1])."</td>
						</tr>
					</table>	
					";
					$onscreen.="<table>";
					
						$cols = $columns[$key];
						//HEADING ROW
						$onscreen.= "<tr>";
							$onscreen.= "<th>&nbsp;</th>";
						foreach($cols as $td => $cell) {
							$onscreen.= "<th>".$td."</th>";
						}
						$onscreen.= "</tr>";
						//DATA
						//Process data
						switch($key) {
							case "dir":
								$cell_ref = 1;		$cell_val = 2;
								$a_sql = array();
								$values = array();
								$refs = array();
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case 1: 
												if(checkIntRef($d[$cell])) { 
													if(!in_array($d[$cell],$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = $d[$cell];
													} else {
														$values[$i][$cell][1] =valError($d[$cell],"Duplicate reference");
													}
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid reference</b>";
												} 
												break;
											case 2: 
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													$res++; 
													$values[$i][$cell][0]=true; 
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid length</b>";
												} 
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											if($act=="ACCEPT") { $a_sql[] = "(".$values[$i][$cell_ref][1].",'".code(trim($values[$i][$cell_val][1]))."',true,".$values[$i][$cell_ref][1].")"; }
										}
									} else {
										unset($values[$i]);
									}
								}
								$dir_refs = $refs;
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active, sort) VALUES ".implode(",",$a_sql);
								}
								break;
							case "subdir":
								$values = array();
								$refs = array();
								$primary = array();
								$a_sql = array();
								$cell_ref = 9; $cell_val = 7; $cell_pri = 8; $cell_dir = 6;
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case 6:									//Dir ID
												if(!checkIntRef($d[$cell])) {
													//invalid reference
													$values[$i][$cell][1] = valError($d[$cell],"Invalid Reference");
												} elseif(!in_array($d[$cell],$dir_refs)) {
													//invalid directorate
													$values[$i][$cell][1] = valError($d[$cell],"Invalid Directorate");
												} else {
													//valid
													$res++;
													$values[$i][$cell][0]=true; 
												}
												break;
											case 9: 								//Sub ID
												if(checkIntRef($d[$cell])) { 
													if(!in_array($d[$cell],$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = $d[$cell];
													} else {
														$values[$i][$cell][1].=" <b>-> Duplicate reference</b>::".implode(" ",$refs);
													}
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid reference</b>";
												} 
												break;
											case 7: 								//value
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													$res++; 
													$values[$i][$cell][0]=true; 
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid length</b>";
												} 
												break;
											case 8:
												if(strtoupper($d[$cell])!="Y" && strtoupper($d[$cell])!="N") {
													$values[$i][$cell][1]= valError($d[$cell],"Invalid value");
												} elseif(strtoupper($d[$cell])=="Y") {
													if(checkIntRef($values[$i][6][1])) {
														if(!in_array($values[$i][6][1],$primary)) {
															$primary[] = $values[$i][6][1];
															$res++;
															$values[$i][$cell][0] = true;
														} else {
															$values[$i][$cell][1] = valError($d[$cell],"Primary already exists");
														}
													} else {
														$values[$i][$cell][1] = valError($d[$cell],"Unknown Directorate");
													}
												} else {
													$values[$i][$cell][0] = true;
													$res++;
												}
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											if($act=="ACCEPT") { $a_sql[] = "(".$values[$i][$cell_ref][1].",'".code(trim($values[$i][$cell_val][1]))."',true,".((strtolower($values[$i][$cell_pri][1])=="y") ? 1 : $values[$i][$cell_ref][1]).",".((strtolower($values[$i][$cell_pri][1])=="y") ? "true" : "false").",".$values[$i][$cell_dir][1].")"; }
										}
									} else {
										unset($values[$i]);
									}
								}
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active, sort, head, dirid) VALUES ".implode(",",$a_sql);
								}
								break;
							case "list_area":
							case "list_fundsource":
							case "list_gfs":
							case "list_natoutcome":
							case "list_idp_top":
							case "list_ndp":
							case "list_pdo":
							case "list_repcate":
								/*switch($key) {
									case "list_area": 		$cell_ref = 50;		$cell_val = 51;		break;
									case "list_fundsource":	$cell_ref = 64;		$cell_val = 63; 	break;
									case "list_gfs": 		$cell_ref = 13;		$cell_val = 12;		break;
									case "list_natoutcome":	$cell_ref = 24;		$cell_val = 23;		break;
									case "list_idp_top":	$cell_ref = 32;		$cell_val = 31;		break;
								}*/
								$cell_ref = $columns[$key]['Ref'];
								$cell_val = $columns[$key]['Value'];
								$a_sql = array();
								$values = array();
								$refs = array();
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case $cell_ref: 
												if(checkIntRef($d[$cell])) { 
													if(!in_array($d[$cell],$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = $d[$cell];
													} else {
														$values[$i][$cell][1] =valError($d[$cell],"Duplicate reference");
													}
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid reference</b>";
												} 
												break;
											case $cell_val: 
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													$res++; 
													$values[$i][$cell][0]=true; 
												} else {
													$values[$i][$cell][1] = valError($d[$cell],"Invalid length");
												} 
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											if($act=="ACCEPT") { $a_sql[] = "(".$values[$i][$cell_ref][1].",'".code(trim($values[$i][$cell_val][1]))."',true,0)"; 
																}
										}
									} else {
										unset($values[$i]);
									}
								}
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active, sort) VALUES ".implode(",",$a_sql);
								}
								break;
							case "list_idp_kpi":
								//arrPrint($idp_lists);
//									$cell_ref = 29;		$cell_val = 26;		$cell_mun = 27;		$cell_nat = 28;
								$cell_ref = $columns[$key]['Ref'];
								$cell_val = $columns[$key]['Value'];
								$cell_mun = $columns[$key]['Mun. KPA'];
								$cell_nat = $columns[$key]['Nat. KPA'];
								$values = array();
								$refs = array();
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case $cell_ref: 
												if(checkIntRef($d[$cell])) { 
													if(!in_array($d[$cell],$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = $d[$cell];
													} else {
														$values[$i][$cell][1] =valError($d[$cell],"Duplicate reference");
													}
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid reference</b>";
												} 
												break;
											case $cell_val: 
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													$res++; 
													$values[$i][$cell][0]=true; 
												} else {
													$values[$i][$cell][1] = valError(code(trim($d[$cell])),"Invalid length (length: ".strlen(code(trim($d[$cell])))."; max allowed: ".$str_max.")");
												} 
												break;
											case $cell_mun:
												if(strlen(code(trim($d[$cell])))>0) {
													if(!isset($idp_lists['list_munkpa'][strtolower(trim(code($d[$cell])))]) && !checkIntRef($idp_lists['list_munkpa'][strtolower(trim(code($d[$cell])))])) {
														$values[$i][$cell][1] = valError($d[$cell],"Invalid Mun. KPA. Will import without association.");
														$values[$i][$cell][2] = 0;
													} else {
														$res++; 
														$values[$i][$cell][0] = true; 
														$values[$i][$cell][1] = "[".$idp_lists['list_munkpa'][strtolower(trim(code($d[$cell])))]."] ".$d[$cell];
														$values[$i][$cell][2] = $idp_lists['list_munkpa'][strtolower(trim(code($d[$cell])))];
													}
												} else {
													$res++; 
													$values[$i][$cell][0] = true; 
													$values[$i][$cell][1] = "";
													$values[$i][$cell][2] = 0;
												}
												break;
											case $cell_nat:
												if(strlen(code(trim($d[$cell])))>0) {
													if(!isset($idp_lists['list_natkpa'][strtolower(trim(code($d[$cell])))]) || !checkIntRef($idp_lists['list_natkpa'][strtolower(trim(code($d[$cell])))])) {
														$values[$i][$cell][1] = valError($d[$cell],"Invalid Mun. KPA. Will import without association.");
														$values[$i][$cell][2] = 0;
													} else {
														$res++; 
														$values[$i][$cell][0] = true; 
														$values[$i][$cell][1] = "[".$idp_lists['list_natkpa'][strtolower(trim(code($d[$cell])))]."] ".$d[$cell];
														$values[$i][$cell][2] = $idp_lists['list_natkpa'][strtolower(trim(code($d[$cell])))];
													}
												} else {
													$res++; 
													$values[$i][$cell][0] = true; 
													$values[$i][$cell][1] = "";
													$values[$i][$cell][2] = 0;
												}
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											if($act=="ACCEPT") { $a_sql[] = "(".$values[$i][$cell_ref][1].",'".code(trim($values[$i][$cell_val][1]))."',true,0,".((isset($values[$i][$cell_mun][2]) && checkIntRef($values[$i][$cell_mun][2])) ? $values[$i][$cell_mun][2] : 0).",".((isset($values[$i][$cell_nat][2]) && checkIntRef($values[$i][$cell_nat][2])) ? $values[$i][$cell_nat][2] : 0).")"; 
																}
										}
									} else {
										unset($values[$i]);
									}
								}
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active, sort, munkpaid, natkpaid) VALUES ".implode(",",$a_sql);
								}
								break;
							case "list_munkpa":
							case "list_natkpa":
							case "list_wards":
							case "list_kpiconcept":
							case "list_kpitype":
							case "list_riskrating":
								/*switch($key) {
									case "list_munkpa": 		$cell_ref = 17;		$cell_val = 15;		$cell_code = 16;		break;
									case "list_natkpa": 		$cell_ref = 21;		$cell_val = 19;		$cell_code = 20;		break;
									case "list_wards":			$cell_ref = 46;		$cell_val = 47;		$cell_code = 48;		break;
									case "list_kpiconcept":		$cell_ref = 36;		$cell_val = 34;		$cell_code = 35;		break;
									case "list_kpitype":		$cell_ref = 40;		$cell_val = 38;		$cell_code = 39;		break;
									case "list_riskrating":		$cell_ref = 44;		$cell_val = 42;		$cell_code = 43;		break;
								}*/
								$cell_ref = $columns[$key]['Ref'];
								$cell_val = $columns[$key]['Value'];
								$cell_code = $key=="list_wards" ? $columns[$key]['Municipal Ref'] : $columns[$key]['Code'];
								$values = array();
								$refs = array();
								$codes = array();
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case $cell_ref: 
												if(checkIntRef($d[$cell])) { 
													if(!in_array($d[$cell],$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = $d[$cell];
													} else {
														$values[$i][$cell][1] =valError($d[$cell],"Duplicate reference");
													}
												} else {
													$values[$i][$cell][1].=" <b>-> Invalid reference</b>";
												} 
												break;
											case $cell_val: 
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													$res++; 
													$values[$i][$cell][0]=true; 
												} else {
													$values[$i][$cell][1] = valError($d[$cell],"Invalid length");
												} 
												break;
											case $cell_code: 
												if(strlen(code(trim($d[$cell])))<20 && strlen(code(trim($d[$cell])))>0) { 
													if(!in_array(strtolower(trim($d[$cell])),$codes)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$codes[] = strtolower(trim($d[$cell]));
													} else {
														$values[$i][$cell][1] = valError($d[$cell],"Duplicate");
													}
												} else {
													$values[$i][$cell][1] = valError($d[$cell],"Invalid length");
												} 
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											$idp_lists[$key][strtolower(trim(code($d[$cell_val])))] = $d[$cell_ref];
											if($act=="ACCEPT") { $a_sql[] = "(".$values[$i][$cell_ref][1].",'".code(trim($values[$i][$cell_val][1]))."',true,0,'".code($values[$i][$cell_code][1])."')"; 
																}
										}
									} else {
										unset($values[$i]);
									}
								}
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active, sort, code) VALUES ".implode(",",$a_sql);
								}
								break;
							case "list_owner":
//								$cell_val = 53;
//								$cell_ref = $columns[$key]['Ref'];
								$cell_val = $columns[$key]['Value'];
//								$cell_code = $columns[$key]['Code'];
								$values = array();
								$refs = array();
								for($i=2;$i<count($data);$i++) {
									$d = $data[$i];
									$values[$i] = array();
									$values[$i][0] = false;
									$res = 0;
									$len = 0;
									foreach($cols as $td => $cell) {
										$values[$i][$cell] = array(false,$d[$cell]);
										$len+=strlen($d[$cell]);
										switch($cell) {
											case $cell_val: 
												if(strlen(code(trim($d[$cell])))<$str_max && strlen(code(trim($d[$cell])))>0) { 
													if(!in_array(trim(strtolower(code($d[$cell]))),$refs)) {
														$res++; 
														$values[$i][$cell][0]=true; 
														$refs[] = trim(strtolower(code($d[$cell])));
													} else {
														$values[$i][$cell][1] = valError($d[$cell],"Duplicate");
													}
												} else {
													$values[$i][$cell][1] = valError($d[$cell],"Invalid length");
												} 
												break;
											default: $values[$i][$cell][1].=" -> Invalid cell";
										}
									}
									if($len>0) {
										if($res==count($cols)) {  
											$values[$i][0]=true; 
											if($act=="ACCEPT") { $a_sql[] = "(null,'".code(trim($values[$i][$cell_val][1]))."',true)"; 
																}
										}
									} else {
										unset($values[$i]);
									}
								}
								if(count($a_sql)>0) {
									$z_sql[$key] = "INSERT INTO ".$dbref."_".$key." (id, value, active) VALUES ".implode(",",$a_sql);
								}
								break;
						}
						//Display data
						foreach($values as $v) {
							$onscreen.= "<tr>";
							if($v[0]) {
								$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span></td>";
							} else {
								$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span></td>";
							}
							foreach($cols as $a => $c) {
								if($v[$c][0]) {
									$onscreen.= "<td class=\"ui-state-ok\">";
								} else {
									$onscreen.= "<td class=\"ui-state-error\">";
								}
								$onscreen.= "&nbsp;".$v[$c][1]."</td>";
							}
							$onscreen.= "</tr>";
						}
					
					$onscreen.="</table>";
					
				}	//foreach lists
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit /> <input type=button id=reset value=Reject /><p>";
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
				} else {
					foreach($z_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.= "<P>".$sql;
					}
					db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = 'LIST'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
					//Sorting Sub-Directorates
					echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=List+items+created.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, $page_title." > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
	
	
	
		break;
}	//switch act
?>
</body>
</html>