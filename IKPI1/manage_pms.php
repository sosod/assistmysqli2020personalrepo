<?php

//Filter settings
$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
			INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k WHERE kpi_active = true ) 
			WHERE d.active = true";
	if($base[1]=="dept") {
			$dir_sql.= " AND (
				d.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'DIR' AND active = true AND tkid = '$tkid' AND (act_update = true OR act_edit = true)) 
			OR
				s.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'SUB' AND active = true AND tkid = '$tkid' AND (act_update = true OR act_edit = true)) 
			)";
	}
			//echo $dir_sql;
$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");

if(count($valid_dir_sub)>0 || count($my_access['manage']['OWN'])>0) {
	//arrPrint($my_access['manage']);
	//arrPrint($valid_dir_sub);
	echo "<form id=pmsreport action=manage_pms_generate.php method=post>
			<input type=hidden name=page_id value=".$page_id." /><input type=hidden name=act value=GENERATE />
			<table>";
		echo "<tr>";
			echo "<th>Group By:</th>";
			echo "<td><select name=groupby id=gby>
						<option selected value=kpi_ownerid>".$mheadings['KPI']['kpi_ownerid']['h_client']."</option>
						<option value=kpi_subid>".$mheadings['KPI']['kpi_subid']['h_client']."</option>
					</select>
				</td>";
		echo "</tr>";
		echo "<tr id=filter>";
			echo "<th>Filter:</th>";
			echo "<td>
					<div id=sub>
						<select name=kpi_subid[] multiple size=5 id=ks>";
						foreach($valid_dir_sub as $did => $d) {
							$subs = $lists['subdir']['dir'][$did];
							foreach($subs as $sid => $s) {
								if(isset($my_access['manage']['DIR'][$did]) || isset($my_access['manage']['SUB'][$sid])) {
									echo "<option value=".$sid.">".$lists['dir'][$did]['value']." - ".$s['value']."</option>";
								}
							}
						}
						echo "</select>".echoMultipleSelectTip()."
					</div>
					<div id=owner>
						<select name=kpi_ownerid[] multiple size=5 id=ko>";
						foreach($valid_dir_sub as $did => $d) {
							$subids = array();
							foreach($d as $i => $l) {
								$subids[] = $i;
							}
							if(count($subids)>0) {
								$o_sql = "SELECT DISTINCT kpi_ownerid as ko, count(kpi_id) as kc FROM ".$dbref."_kpi WHERE kpi_subid IN (".implode(",",$subids).") AND kpi_active = true GROUP BY kpi_ownerid";
								$os = mysql_fetch_alls($o_sql,"ko");
								foreach($os as $i => $c) {
									echo "<option value=".$i.">".$lists['dir'][$did]['value']." - ".$lists['owner'][$i]['value']." (".$c['kc']." kpis)</option>";
								}
							}
						}
						if($base[1]=="dept" && count($my_access['manage']['OWN'])>0) {
							echo "<option value=X>--- MANAGE --- </option>";
							foreach($my_access['manage']['OWN'] as $i => $l) {
								echo "<option value=$i>".$lists['owner'][$i]['value']."</option>";
							}						
						}
						echo "</select>".echoMultipleSelectTip()."
					</div>
				</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>Format:</th>";
			echo "<td><select name=format>
						<option selected value=agreement>PMS Agreement Report</option>
						<option value=development>PMS Development Plan Report</option>
					</select></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>&nbsp;</th>";
			echo "<td><input type=button value=Generate class=isubmit /> <input type=reset value=Reset /></td>";
		echo "</tr>";
	echo "</table></form>";
	?>
	<script type=text/javascript>
		$(document).ready(function() {
		/* ON CHANGE OF GROUP BY */
			$("#gby").change(function() {
				var v = $(this).val();
				if(v=="kpi_ownerid") {	
					$("#owner").show();
					$("#sub").hide();
					$("#filter th").attr("innerText",'<?php echo $mheadings['KPI']['kpi_ownerid']['h_client']; ?> Filter:');
				} else {
					$("#sub").show();
					$("#owner").hide();
					$("#filter th").attr("innerText",'<?php echo $mheadings['KPI']['kpi_subid']['h_client']; ?> Filter:');
				}
			});
		/* SUBMIT */
		$(".isubmit").click(function() {
			var v = $("#gby").val();
			var c = 0;
			if(v=="kpi_ownerid") {
				c = $("#ko :selected").size();
			} else {
				c = $("#ks :selected").size();
			}
			if(c==0) {
				alert("Please select a filter.");
			} else {
				$("#pmsreport").submit();
			}
		});
		/* ON LOAD */
			$("th").addClass("left top");
			$("#gby").trigger('change');
			$("#pmsreport #filter select").each(function() {
				i = $(this).attr("id");
				opt = $("#"+i+" option");
				o = opt.size();
				if(o > 10) { $(this).attr("size",10); }
			});
		});
	</script>
	<?php
	
} else {
	die("<P>You do not have administrative access to any ".$mheadings['dir'][0]['h_client']." or ".$mheadings['KPI']['kpi_ownerid']['h_client']." from which to draw a PMS Report.</p>");
}
?>