<?php
/**
 * VARIABLES REQUIRED FROM CALLING FPD PAGE
 * @var ASSIST_MODULE_HELPER $my_assist
 */

function IKPI1frontpage($modref,$modtitle,$limit=7) {
	global $my_assist;
	$echo = "";
	$cmpcode = $_SESSION['cc'];
	$tkid = $_SESSION['tid'];
	$dbref = strtolower("assist_".$cmpcode."_".$modref);
	$today = strtotime(date("d F Y H:i:s"));
	//$helper = new ASSIST_MODULE_HELPER();
	//require_once("/SDBP4/inc/inc.php");

	
	$sql = "SELECT * FROM ".$dbref."_import_status";
	$x = $my_assist->mysql_fetch_all($sql);
	$importStatus = array();
	foreach($x as $r) {
		$importStatus[$r['value']] = $r['status'];
	}
	
	
	//check if any reminders might exist (e.g. any open time periods with reminder fields)
	$sql = "SELECT * FROM ".$dbref."_list_time WHERE (active_primary = true AND close_primary > $today) OR (active_secondary=true AND close_secondary>$today) OR (active_finance = true AND close_finance >$today)";
	$check_time = $my_assist->mysql_fetch_all($sql);
	
	if(count($check_time)>0) {
	
		$user_access = IKPI1getUserAccess($dbref,$tkid); //arrPrint($user_access);
		//arrPrint($user_access);
		
		$manage = 0;
		if(isset($user_access['manage']['DIR'])) { $manage+=count($user_access['manage']['DIR']); }
		if(isset($user_access['manage']['SUB'])) { $manage+=count($user_access['manage']['SUB']); }
		if(isset($user_access['manage']['OWN'])) { $manage+=count($user_access['manage']['OWN']); }
		
		if($manage>0 || $user_access['access']['toplayer'] || $user_access['access']['finance']) {
			$sql = "SELECT * FROM ".$dbref."_list_time 
					WHERE active = true
					AND start_date < '".date("Y-m-d H:i:s")."'
					AND (active_primary = true OR active_secondary = true OR active_finance = true)
					AND (close_primary > 0 OR close_secondary > 0 OR close_finance > 0)";
			$time = $my_assist->mysql_fetch_all_fld($sql,"id");
			$week = $today + (86400 * $limit);
			//arrPrint($time);
			$reminders = array('primary'=>false,'secondary'=>false,'toplayer'=>false,'finance'=>false);
			if($manage>0 && !$user_access['access']['second']) {
				$reminders['primary'] = true;
				$reminders['secondary'] = false;
			} elseif($manage>0 && $user_access['access']['second']) {
				$reminders['primary'] = false;
				$reminders['secondary'] = true;
			}
			if($user_access['access']['toplayer']) {
				$reminders['toplayer'] = true;
			}
			if($user_access['access']['finance']) {
				$reminders['finance'] = true;
			}
			$closures = array();
			foreach($time as $ti => $t) {
				$fld = "primary"; $section = "Primary";
				if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week && $importStatus['KPI']==1) {
					$closures[]= "Departmental SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).".";
				}
				$fld = "secondary"; $section = "Secondary";
				if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week && $importStatus['KPI']==1) {
					$closures[]= "Departmental SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld])."."
								.($t['close_primary']>0 ? "<br /><i><small>&nbsp;&nbsp;&nbsp;(Following closure for primary users on ".date("d M Y",$t['close_primary']).".)</small></i>" : "");
				}
				if(ceil($ti/3)==$ti/3) {
					$fld = "secondary"; $section = "Top Layer";
					if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week && $importStatus['TOP']==1) {
						$closures[]= "Top Layer SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).".";
					}
				}
				$fld = "finance"; $section = "Finance";
				if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week && ($importStatus['CF']==1 || $importStatus['CAP']==1 || $importStatus['RS']==1) ) {
					$closures[]= "Financials - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).". ";
				}
			}

			if(count($closures)>0) {
				$echo = "<div align=center style=\"margin: 10px 10px;\"><table style=\"border: 2px solid #cc0001;\"><tr><td style=\"padding: 10 10;\">
					<h2 style=\"margin-top: 0px; color: #cc0001; font-size: 12pt\">".$modtitle." Reminder</h2>
					<p class=nopad>Please note the following forthcoming closure dates:<ul><li>
						".implode("</li><li>",$closures)."
					</li></ul>
					<p class=nopad style=\"font-style: italic\">Please ensure that you have completed all necessary updates.</p>
					</td></tr></table>
					</div>";
			} else {
				$echo = "<p>No ".$modtitle." reminders awaiting your attention.</p>";
			}
		}
	}	//check if any reminders might exist	
	
	return $echo;
}


function IKPI1getUserAccess($dbref,$tkid) {
	global $my_assist;
	$user = array('access'=>array(),'manage'=>array(),'act'=>array());
	//global $dbref;
	
	/* Setup > User Access */
$user_access_fields = array(
	'module'=>"Module<br />Admin",
	'kpi'=>"KPI<br />Admin",
	'finance'=>"Finance<br />Admin",
	'toplayer'=>"Top Layer<br />Admin",
	'assurance'=>"Assurance<br />Provider",
	'second'=>"Secondary<br />Time Period",
	'setup'=>"Setup",
);
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true AND tkid = '$tkid' ORDER BY id DESC LIMIT 1"; //echo $sql;
	$a = $my_assist->mysql_fetch_all($sql);
	if(count($a)>0) {
		$a = $a[0];
	} else {
		foreach($user_access_fields as $fld => $h) {
			$a[$fld] = false;
		}
	}
	$user['access'] = $a;
	/* Setup > Administrators */
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_admins WHERE active = true and tkid = '$tkid' ORDER BY type, ref";
	$manage = $my_assist->mysql_fetch_all_fld2($sql,"type","ref");
	$user['manage'] = $manage;
	$act = array('update'=>0,'edit'=>0,'create'=>0,'approve'=>0);
	foreach($user['manage'] as $usr) {
		foreach($usr as $u) {
			foreach(array_keys($act) as $a) {
				if($u['act_'.$a]) $act[$a]++;
			}
		}
	}
	$user['act'] = $act;
	return $user;
}

?>