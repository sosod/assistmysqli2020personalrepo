<?php
/**
 * To manage the functions required by external modules
 *
 * Created on: 10 June 2018
 * Authors: Janet Currie
 *
 */

class IKPI1_EXTERNAL extends ASSIST_MODULE_HELPER {

	private $local_modref;
	private $sourceObject;
	private $object_type;

    public function __construct($modref="",$object_type="",$create_source_object=false) {
        parent::__construct("client","",false,$modref);
		$this->local_modref = $modref;
		$this->object_type = $object_type;
		if(strlen($object_type)>0 && $create_source_object===true) {
			switch($object_type) {
				case IKPI1_CAPITAL::OBJECT_TYPE:
					$this->sourceObject = new IKPI1_CAPITAL($this->local_modref,true);
					break;
				case IKPI1_TOP::OBJECT_TYPE:
					$this->sourceObject = new IKPI1_TOP($this->local_modref,true);
					break;
				case IKPI1_DEPT::OBJECT_TYPE:
					$this->sourceObject = new IKPI1_DEPT($this->local_modref,true);
					break;
			}
		}
    }




	public function replaceExternalNames($a) {
		$idpObject = new IKPI1($this->local_modref,true);
		$a = $idpObject->replaceAllNames($a);
		return $a;
	}





	public function getSourceRecords() {
		$records = $this->sourceObject->getRawObjectsForExternalModule();
		return $records;
	}


	public function getRefTag() {
    	return $this->sourceObject->getRefTag();
	}



	public function getListItemsForSelect() {
    	return $this->sourceObject->getListItemsForSelect();
	}























	public function __destruct() {
		parent::__destruct();
	}
}


?>