<?php

class SDBP5B_HELPER extends ASSIST_MODULE_HELPER {

	public $today;
	public $assist_module_helper;

	protected $result_settings = array(
		0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."	, 'limit_int'=>false	, 'limit_link'=>false	, 'limit_decimal'=>false),
		1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"R"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% <= Actual/Target <= 74.99%"								, 'limit_int'=>false	, 'limit_link'=>2		, 'limit_decimal'=>0.75),
		2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"O"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75.000% <= Actual/Target <= 99.999%"								, 'limit_int'=>75		, 'limit_link'=>false	, 'limit_decimal'=>0.75),
		3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"G"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual meets Target (Actual/Target = 100%)"										, 'limit_int'=>100		, 'limit_link'=>false	, 'limit_decimal'=>1),
		4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"G2"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100.001% <= Actual/Target <= 149.999%"								, 'limit_int'=>false	, 'limit_link'=>5		, 'limit_decimal'=>1.5),
		5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"B"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "150.000 <= Actual/Target"									, 'limit_int'=>150		, 'limit_link'=>false	, 'limit_decimal'=>1.5),
	);

	protected $default_result_settings = array(
		0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."	, 'limit_int'=>false	, 'limit_link'=>false	, 'limit_decimal'=>false),
		1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"R"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% <= Actual/Target <= 74.999%"								, 'limit_int'=>false	, 'limit_link'=>2		, 'limit_decimal'=>0.75),
		2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"O"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75%.000 <= Actual/Target <= 99.999%"								, 'limit_int'=>75		, 'limit_link'=>false	, 'limit_decimal'=>0.75),
		3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"G"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual meets Target (Actual/Target = 100%)"										, 'limit_int'=>100		, 'limit_link'=>false	, 'limit_decimal'=>1),
		4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"G2"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100.001% <= Actual/Target <= 149.999%"								, 'limit_int'=>false	, 'limit_link'=>5		, 'limit_decimal'=>1.5),
		5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"B"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "150.000% <= Actual/Target"									, 'limit_int'=>150		, 'limit_link'=>false	, 'limit_decimal'=>1.5),
	);

	protected $result_settings_headings = array(
		'value'=>"Name",
		'text'=>"Short Code",
		'color'=>"Colour",
		'limit_int'=>"Limit",
		'glossary'=>"Explanation",
	);

	protected $result_settings_management_properties = array(
		'settings_with_limit_int' => array(2,5),
		'settings_with_linked_limit_int' => array(1,4),
		'setting_with_fixed_limit' => 3,
		'setting_with_no_limit' => 0,
	);

	private $my_attachment_download_link = "controller/attachment.php";
	private $my_attachment_download_options = "action=GET_ATTACH";
	private $my_attachment_delete_link = "controller/attachment.php";
	private $my_attachment_delete_options = "action=DELETE_ATTACH";
	private $my_attachment_delete_ajax = true;
	private $my_attachment_delete_function = "doDeleteAttachment";


	public function __construct($dbtype="client",$cc="",$autojob=false,$modref="") {
		$_REQUEST['jquery_version'] = "1.10.0";
		parent::__construct($dbtype,$cc,$autojob,$modref);
		$this->today = strtotime(date("d F Y H:i:s"));//+(122*86400);
		$this->setAttachmentDownloadOptions();
		$this->setAttachmentDeleteOptions();
		$this->setHelperAttachmentDownloadLink($this->getAttachmentDownloadLink());
		$this->setHelperAttachmentDeleteLink($this->getAttachmentDeleteLink());
		$this->setHelperAttachmentDeleteByAjax($this->getAttachmentDeleteByAjax());
		$this->setHelperAttachmentDeleteFunction($this->getAttachmentDeleteFunction());
		//RESULT SETTINGS
		$this->getMyActiveResultSettings(false);

        $this->assist_module_helper = new ASSIST_MODULE_HELPER('client', $_SESSION['cc']);
	}


/*******
 * Functions to manage Result Settings
 * Added by JC in Dec 2017
 */

	public function getResultSettingsManagementProperties() {
		return $this->result_settings_management_properties;
	}
	public function getDefaultResultSettings() {
		return $this->default_result_settings;
	}
	public function getResultSettingsHeadings() {
		return $this->result_settings_headings;
	}
	public function canICustomResultSettings() {
		$sql = "SHOW TABLES LIKE '".$this->getDBRef()."_setup_result_settings'";
		$check_for_result_settings_table = $this->mysql_fetch_all($sql);
		return (count($check_for_result_settings_table)>0 ? true : false);
	}
	public function getCustomResultSettingsDirectlyFromDatabase() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_result_settings ORDER BY id";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		//replace codes from glossary text with actual limits
		foreach($data as $i => $setting) {
			$x = $data[$i]['value'];
			$data[$i]['value'] = ASSIST_HELPER::decode($x);
			$x = $data[$i]['glossary'];
			$x = ASSIST_HELPER::decode($x);
			$x = str_replace("|limit2-|",($data[2]['limit_int']-0.001),$x);
			$x = str_replace("|limit2|",$data[2]['limit_int'].".000",$x);
			$x = str_replace("|limit5-|",($data[5]['limit_int']-0.001),$x);
			$x = str_replace("|limit5|",$data[5]['limit_int'].".000",$x);
			$data[$i]['glossary'] = $x;
		}
		return $data;
	}

	public function saveCustomResultSettings($var) {
	    global $helperObj;
		$result = array("error","An error occured while trying to save your update.  Please try again.");
		$management_properties = $this->getResultSettingsManagementProperties();

		$old = $this->getCustomResultSettingsDirectlyFromDatabase();
		$result_settings = $old;
		$changes = array();

		foreach($result_settings as $i => $setting) {
			foreach($setting as $key => $value) {
				if($key=="limit_int") {
					if(in_array($i,$management_properties['settings_with_limit_int'])) {
						if($var[$key][$i]!=$result_settings[$i][$key]) {
							$changes[$i][$key] = array('old'=>$result_settings[$i][$key],'new'=>$var[$key][$i]);
							$result_settings[$i][$key] = $var[$key][$i];
							$result_settings[$i]['limit_decimal'] = $var[$key][$i]/100;
						}
					} elseif(in_array($i,$management_properties['settings_with_linked_limit_int'])) {
						$result_settings[$i]['limit_decimal'] = $var[$key][$setting['limit_link']]/100;
					}
				} elseif(isset($var[$key][$i])) {
					if($var[$key][$i]!=$result_settings[$i][$key]) {
						$changes[$i][$key] = array('old'=>$result_settings[$i][$key],'new'=>$var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
					}
				}
			}
		}




		foreach($result_settings as $i => $setting) {
			$id = $setting['id'];
			unset($setting['id']);
			foreach($setting as $key => $value) {
				$setting[$key] = ASSIST_HELPER::code($value);
			}
			$sql = "UPDATE ".$this->getDBRef()."_setup_result_settings SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = ".$id;
			$this->db_update($sql);

			//LOG CHANGES!!!
			if(isset($changes[$i]) && count($changes[$i])>0) {
				foreach($changes[$i] as $key => $value) {
					$v_log = array(
						'section'=>"RES",
						'ref'=>$i,
						'field'=>$key,
						'text'=>"Updated <i>".$helperObj->code($this->default_result_settings[$i]['value'])." - ".$this->result_settings_headings[$key]."</i> to <i>".$helperObj->code($value['new'])."</i> from <i>".$helperObj->code($value['old'])."</i>.",
						'old'=>$value['old'],
						'new'=>$value['new'],
						'YN'=>"Y"
					);
					$this->logChanges('SETUP',0,$v_log,$helperObj->code($sql));
				}
			}
		}

		//update session
		$this->updateMyActiveResultSettings($result_settings);

		$result = array("ok","Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message'=>$result,'result_settings'=>$result_settings);
	}
	public function updateMyActiveResultSettings($result_settings=array()) {
		if(count($result_settings)>0) {
			$_SESSION[$this->getModRef()]['result_settings']['data'] = $result_settings;
		} else {
			$_SESSION[$this->getModRef()]['result_settings']['data'] = $this->default_result_settings;
		}
		$_SESSION[$this->getModRef()]['result_settings']['expiration'] = time()+30*60;
	}
	public function getMyActiveResultSettings($return=false) {
		//if the session has been set & the expiration time hasn't expired
		if(isset($_SESSION[$this->getModRef()]['result_settings']['expiration']) && $_SESSION[$this->getModRef()]['result_settings']['expiration']>time() && isset($_SESSION[$this->getModRef()]['result_settings']['data'])) {
			//use the current settings
			$this->result_settings = $_SESSION[$this->getModRef()]['result_settings']['data'];
		} else {
			//check if table exists & is populated
			$check_for_result_settings_table = $this->canICustomResultSettings();
			if($check_for_result_settings_table) {
				//get from database
				$this->result_settings = $this->getCustomResultSettingsDirectlyFromDatabase();
				//update session
				$this->updateMyActiveResultSettings($this->result_settings);
			} else {
				//else use default
				$this->result_settings = $this->default_result_settings;
				//update session to prevent multiple database checks
				$this->updateMyActiveResultSettings($this->result_settings);
			}
		}
		if($return) {
			return $this->result_settings;
		}
	}



/***********
 * Functions to manage Attachments
 */



	public function setAttachmentDownloadOptions($opt="",$add_to_default=true) {
		if($opt=="") {	//reset to default
			$this->my_attachment_download_options = "section=KPI&action=GET_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_download_options .= "&".$opt;
		} else {
			$this->my_attachment_download_options = $opt;
		}
		$this->setHelperAttachmentDownloadOptions($this->my_attachment_download_options);
	}

	protected function setAttachmentDeleteOptions($opt="",$add_to_default=true) {
		if($opt=="") {	//reset to default
			$this->my_attachment_delete_options = "section=KPI&action=DELETE_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_delete_options .= "&".$opt;
		} else {
			$this->my_attachment_delete_options = $opt;
		}
		$this->setHelperAttachmentDeleteOptions($this->my_attachment_delete_options);
	}

	public function getAttachmentDeleteOptions() { return $this->my_attachment_delete_options; }
	public function getAttachmentDeleteLink() { return $this->my_attachment_delete_link; }
	public function getAttachmentDownloadOptions() { return $this->my_attachment_download_options; }
	public function getAttachmentDownloadLink() { return $this->my_attachment_download_link; }
	public function getAttachmentDeleteFolder() { return $this->getModRef()."/deleted"; }
	public function getAttachmentDeleteByAjax() { return $this->my_attachment_delete_ajax; }
	public function getAttachmentDeleteFunction() { return $this->my_attachment_delete_function; }

	public function getResultSettings() { return $this->result_settings; }

	public function stripSpecialCharacters($comm) {
		$comm = str_replace("–","-",$comm);
		$comm = str_replace("“","&quot;",$comm);
		$comm = str_replace("”","&quot;",$comm);
		$comm = str_replace("’","&#039;",$comm);
		$comm = str_replace("‘","&#039;",$comm);
		return $comm;
	}


	/***********************
			SHORTCUT FUNCTIONS
	***********************/
	protected function getUpdateDetailsArray() {
		/*$data = array(
			'ref'	=> "",
			'value'	=> "",
			'unit'	=> "",
			'poe'	=> "",
			'targettype'=>3,
			'calctype'=>"STD",
			'results'	=> array(
				'target'=>array(),
				'actual'=>array(),
				'r'=>array(),
				'ytd_target'=>array(),
				'ytd_actual'=>array(),
				'ytd_r'=>array(),
				'perfcomm'=>array(),
				'correct'=>array(),
			),
		);
		for($x=1;$x<=12;$x++) {
			$data['results']['target'][$x] = 0;
			$data['results']['actual'][$x] = 0;
			$data['results']['r'][$x] = array();
			$data['results']['ytd_target'][$x] = 0;
			$data['results']['ytd_actual'][$x] = 0;
			$data['results']['ytd_r'][$x] = array();
			$data['results']['perfcomm'][$x] = "";
			$data['results']['correct'][$x] = "";
		}*/
		$data = array(
			'ref'	=> "",
			'dir'	=> "",
			'value'	=> "",
			'unit'	=> "",
			'poe'	=> "",
			'targettype'=>3,
			'calctype'=>"STD",
			'ct'	=> "",
			'results'=>array(
				'target'=>array(),
				'actual'=>array(),
				'perfcomm'=>array(),
				'correct'=>array(),
				'poe'=>array(),
				'attach'=>array(),
			),
		);
		return $data;
		//return array(1=>"def");
	}

	function KPIresultDisplay($val,$tt) {
		return $this->formatResult($val,$tt);
	}

	function KPIresultEdit($val,$tt,$fld) {
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	return "R <input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right\" />"; break;
			case 2:	return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right\" /> %"; break;
			case 3: return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right\" />"; break;
			default: return $val; break;
		}
	}

	function formatResult($val,$tt,$decimal=2) {
		switch($tt) {
			case 1:	case "currency":		return "R ".number_format($val,$decimal); break;
			case 2:	case "perc": case "%": 	return number_format($val,$decimal)."%"; break;
			case 3: case "number":			return number_format($val,$decimal); break;
			default: 						return $val; break;
		}
	}

	static function formatTarget($val,$tt,$decimal=2) {
		switch($tt) {
			case 1:	case "currency":		return "R ".number_format($val,$decimal); break;
			case 2:	case "perc": case "%": 	return number_format($val,$decimal)."%"; break;
			case 3: case "number":			return number_format($val,$decimal); break;
			default: 						return $val; break;
		}
	}

	function FINresultDisplay($val,$tt) {
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	return "R ".($val==0 ? "-" : number_format($val,$decimal)); break;
			case 2:	return number_format($val,$decimal)."%"; break;
			case 3: return number_format($val,$decimal); break;
			default: return $val; break;
		}
	}

	/*******************************/
	/******* KPI RESULT CALC *******/
	/*******************************/
	/*
		0	grey	Not measured / Not available
		1	red		Not met [0 - 74.99%]
		2	orange	almost met [75 - 99.99%]
		3	green	met [100%]
		4	d.green	well met [100.01 - 149.99%]
		5	blue	extremely well met [150%+]
	*/
	//calcChange for Capital Projects/Monthly Cashflow
	function calcChange($fld,$targ,$actl) {
		$val = 0;
		switch($fld) {
		case "perc":
				if($targ>0) {
					$val = $actl/$targ*100;
				} else {
					$val = 100;
				}
			break;
		case "var":
			$val = $targ - $actl;
			break;
		}
		return $val;
	}

	//function KPIcalcResult(array of all targets/actuals, calctype, time period range, current time id)
	function KPIcalcResult($values,$ct,$filter,$t) {
		$result_settings = $this->result_settings;
		$almost_met_limit = $result_settings[2]['limit_decimal'];
		$well_met_limit = $result_settings[5]['limit_decimal'];
		//$result = array('r'=>0,'style'=>"result0",'text'=>"N/A",'target'=>0,'actual'=>0);
		$result = $result_settings[0];
		/* determine target & actual to use in calculating result */
		$targ = 0; $actl = 0;
		$tc = 0;
		$targsum = 0; $actlsum=0;

		if($t=="ALL") {		//calculate overall result
			switch($ct) {
				case "CO":
					foreach($values['target'] as $i => $vt) {
						$va = $values['actual'][$i];
						if($targ < ($vt*1)) { $targ = $vt; }
						if($actl < ($va*1)) { $actl = $va; }
					}
					break;
				case "ACC":
				case "ZERO":
				case "ACCREV":
					$targ = array_sum($values['target']);
					$actl = array_sum($values['actual']);
					break;
				case "STD":
				case "REV":
					//$targ = array_sum($values['target']);		$targsum = $targ;
					//$actl = array_sum($values['actual']);		$actlsum = $actl;
					$tc = 0;
					foreach($values['target'] as $i => $vt) {
						if(($vt*1)>0) { $tc++; }
						$targ+=($vt*1);
						$actl+=($values['actual'][$i]*1);
					}
					$targsum = $targ;
					$actlsum = $actl;
					if($tc == 0) { $tc = 1; }
					$targ/=$tc;
					$actl/=$tc;
					break;
				case "LAST":
				case "LASTREV":
				case "LASTR":
				case "LASTZERO":
				case "LASTZ":
					foreach($values['target'] as $i => $vt) {
						$va = $values['actual'][$i];
						if(($vt*1)>0) { $targ = $vt; }
						if(($va*1)>0) { $actl = $va; }
					}
					break;
				case "NA":
					break;
			}
		} else {
					$targ = $values['target'][$t];
					$actl = $values['actual'][$t];
		}
		$targ*=1;	$actl*=1;
		/* compare targ & actl to determine result value */
		switch($ct) {
			case "CO":
			case "STD":
			case "ACC":
			case "LAST":
				if($targ!= 0 || $actl != 0) {
					if($targ==0) {
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
						$result = $result_settings[5];
					} else {
						$div = $actl/$targ;
						if($div<$almost_met_limit) {
							$result = $result_settings[1];
							//$result['r']=1;	$result['style']="result1";	$result['text']="R";
						} elseif($div < 1) {
							$result = $result_settings[2];
							//$result['r']=2;	$result['style']="result2";	$result['text']="O";
						} elseif($div==1) {
							$result = $result_settings[3];
							//$result['r']=3;	$result['style']="result3";	$result['text']="G";
						} elseif($div < $well_met_limit) {
							$result = $result_settings[4];
							//$result['r']=4;	$result['style']="result4";	$result['text']="G2";
						} elseif($div >= $well_met_limit) {
							$result = $result_settings[5];
							//$result['r']=5;	$result['style']="result5";	$result['text']="B";
						}
					}
				}
				break;
			case "REV":
			case "ACCREV":
			case "ZERO":
			case "LASTREV":
			case "LASTR":
			case "LASTZERO":
			case "LASTZ":
				if(in_array($ct,array("ZERO","LASTZ","LASTZERO")) || (in_array($ct,array("REV","LASTREV","ACCREV")) && ($targ!=0 || $actl!=0))) {
					if($actl < $targ) {
						$result = $result_settings[5];
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					} elseif($actl==$targ) {
						$result = $result_settings[3];
						//$result['r']=3;	$result['style']="result3";	$result['text']="G";
					} else {
						$result = $result_settings[1];
						//$result['r']=1;	$result['style']="result1";	$result['text']="R";
					}
				}
				break;
			case "NA":
				break;
		}
		$result['target'] = $targ;
		$result['actual'] = $actl;
		$result['tc'] = $tc;
		$result['sum'] = array('target'=>$targsum,'actual'=>$actlsum);
		$result['values'] = $values;
		return $result;
	}

	function getModuleHeadings($get) {
		$dbref = $this->getDBRef();

		$sql = "SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
				FROM ".$dbref."_setup_headings sh
				INNER JOIN ".$dbref."_setup_headings_setup shs
				  ON sh.h_id = shs.head_id
				  AND shs.active = true
				WHERE
				  sh.h_active = true
				AND
				  shs.section IN ('".implode("','",$get)."')
				ORDER BY
				  shs.section,
				  shs.c_sort,
				  shs.i_sort
				";
		$rs = $this->mysql_fetch_all($sql);
		$h = array();
		if(in_array("KPI",$get) || in_array("CF",$get) || in_array("CAP",$get)) {
			$row = $this->mysql_fetch_all("SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
				FROM ".$dbref."_setup_headings sh
				INNER JOIN ".$dbref."_setup_headings_setup shs
				  ON sh.h_id = shs.head_id
				  AND shs.active = true
				WHERE
				  sh.h_active = true
				AND
				  sh.h_table = 'dir'
				");
			$h['dir'] = $row;
		}
		//while($row = mysqli_fetch_assoc($rs))
		foreach($rs as $row) {
			$h[$row['section']][$row['field']] = $row;
			if($row['fixed']) {
				$h['FIXED'][$row['field']] = $row;
			}
		}
		return $h;
	}

	function getModuleDefaults() {
		$dbref = $this->getDBRef();
		$sql = "SELECT * FROM ".$dbref."_setup_defaults WHERE active = true ORDER BY sort";
		$rs = $this->mysql_fetch_all($sql);
		$setup_defaults = array();
		//while($row = mysqli_fetch_assoc($rs)) {
		foreach($rs as $row) {
			$setup_defaults[$row['code']] = $row['value'];
		}
		return $setup_defaults;
	}

	function getTime($get_time,$get_open_time=false) {
		$dbref = $this->getDBRef();
		$today = strtotime(date("d F Y H:i:s"));

		$sql = "SELECT * FROM ".$dbref."_list_time WHERE id IN (".implode(",",$get_time).")";
		$rs = $this->mysql_fetch_all($sql);
		$t = array();
		//while($row = mysqli_fetch_assoc($rs)) {
		foreach($rs as $row) {
			if(!$get_open_time || ($today >=  strtotime($row['start_date']))) {
				$t[$row['id']] = $row;
				$t[$row['id']]['start_stamp'] = strtotime($row['start_date']);
				$t[$row['id']]['end_stamp'] = strtotime($row['end_date']);
				$t[$row['id']]['display_short'] = date("M Y",strtotime($row['end_date']));
				$t[$row['id']]['display_full'] = date("F Y",strtotime($row['end_date']));
				$t[$row['id']]['open_status'] = $row['active_primary']==1 ? true : false;
				$t[$row['id']]['deadline'] = $row['close_primary'];
			}
		}
		return $t;
	}

	function getImportStatus() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_import_status";
		$status = $this->mysql_fetch_value_by_id($sql,"value","status");
		return $status;
	}




	function logChanges($section,$id,$v,$lsql) {
	/*
		$section = "KPI", "TOP", "SETUP", "CAP"
		$id = obj_id
		$v = array('fld','timeid','text','old','new','act','YN')
	*/
		$easy = array("CAP","CF","RS");
		$dbref = $this->getDBRef();
		$tkid = $this->getUserID();
		$today = strtotime(date("d F Y H:i:s"));

		$fields = array();
		$values = array();
		if(in_array($section,$easy)) {
					$f = 0;
			switch($section) {
			case "CAP":
					$fld = "clog_";
					$table_tbl = "capital";
					$fields[$f] = $fld."capid";			$values[$f] = $id;					$f++;
				break;
			case "CF":
					$fld = "flog_";
					$table_tbl = "cashflow";
					$fields[$f] = $fld."kpiid";			$values[$f] = $id;					$f++;
				break;
			case "RS":
					$fld = "rlog_";
					$table_tbl = "revbysrc";
					$fields[$f] = $fld."kpiid";			$values[$f] = $id;					$f++;
				break;
			}
					$fields[$f] = $fld."tkid";			$values[$f] = "'$tkid'";			$f++;
					$fields[$f] = $fld."date";			$values[$f] = $today;				$f++;
					$fields[$f] = $fld."field";			$values[$f] = "'".$v['fld']."'";	$f++;
					$fields[$f] = $fld."timeid";		$values[$f] = "".$v['timeid']."";	$f++;
					$fields[$f] = $fld."transaction";	$values[$f] = "'".$v['text']."'";	$f++;
					$fields[$f] = $fld."old";			$values[$f] = "'".$v['old']."'";	$f++;
					$fields[$f] = $fld."new";			$values[$f] = "'".$v['new']."'";	$f++;
					$fields[$f] = $fld."act";			$values[$f] = "'".$v['act']."'";	$f++;
					$fields[$f] = $fld."yn";			$values[$f] = "'".$v['YN']."'";		$f++;
					$fields[$f] = $fld."lsql";			$values[$f] = "'$lsql'";			$f++;
					$sql = "INSERT INTO ".$dbref."_".$table_tbl."_log
							(".implode(",",$fields).") VALUES
							(".implode(",",$values).")";
					$mid = $this->db_insert($sql);
		} else {
			/* Added on 2016-11-13 by JC to deal with single quotes getting accidentally sent through to logging - identified from db logs */
			if(isset($v['old']) && strpos($v['old'],"'")!==false) {
				$v['old'] = $this->code($v['old']);
			}
			if(isset($v['new']) && strpos($v['new'],"'")!==false) {
				$v['new'] = $this->code($v['new']);
			}
			if(isset($v['text']) && strpos($v['text'],"'")!==false) {
				$v['text'] = $this->code($v['text']);
			}

			switch($section) {
				case "KPI":
				case "KAS":
					$sql = "INSERT INTO ".$dbref."_kpi_log (klog_kpiid,klog_tkid,klog_date,klog_field,klog_timeid,klog_transaction,klog_old,klog_new,klog_act,klog_yn,klog_lsql) VALUES
					($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
					$this->db_insert($sql);
					break;
/*					$sql = "INSERT INTO ".$dbref."_kpi_assurance_log (klog_kpiid,klog_tkid,klog_date,klog_field,klog_timeid,klog_transaction,klog_old,klog_new,klog_act,klog_yn,klog_lsql) VALUES
					($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
					$this->db_insert($sql);
					break;*/
				case "TOP":
					$sql = "INSERT INTO ".$dbref."_top_log (tlog_topid,tlog_tkid,tlog_date,tlog_field,tlog_timeid,tlog_transaction,tlog_old,tlog_new,tlog_act,tlog_yn,tlog_lsql) VALUES
					($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
					$this->db_insert($sql);
					break;
				case "TAS":
					$sql = "INSERT INTO ".$dbref."_top_assurance_log (tlog_topid,tlog_tkid,tlog_date,tlog_field,tlog_timeid,tlog_transaction,tlog_old,tlog_new,tlog_act,tlog_yn,tlog_lsql) VALUES
					($id,'$tkid',$today,'".$v['fld']."',".$v['timeid'].",'".$v['text']."','".$v['old']."','".$v['new']."','".$v['act']."','".$v['YN']."','".$lsql."')";
					$this->db_insert($sql);
					break;
				case "SETUP":
					$sql = "INSERT INTO ".$dbref."_setup_log (slog_section, slog_ref, slog_tkid, slog_tkname, slog_date, slog_field, slog_transaction, slog_old, slog_new, slog_yn, slog_lsql) VALUES
					('".$v['section']."','".$v['ref']."','$tkid','".$this->code($_SESSION['tkn'])."',now(),'".$v['field']."','".$v['text']."','".$v['old']."','".$v['new']."','".$v['YN']."','$lsql')";
					$this->db_insert($sql);
					break;
			}
		}
	}

	function logUser() {
	//	global $tkid;
	//	global $tkname;
	//	global $dbref;
	//$modref = strtolower($_SESSION['ref']);
	//$cmpcode = strtolower($_SESSION['cc']);
	//$dbref = "assist_".$cmpcode."_".$modref;
		$tkid = $this->getUserID();
		$tkname = $this->getUserName();
		$dbref = $this->getDBRef();

		$req = $_REQUEST;
		$page = substr($_SERVER['PHP_SELF'],7,strlen($_SERVER['PHP_SELF']));
		$tkname = substr($this->code($tkname),0,99);
		$val = "";
		if(count($req)>0) {
			$vals = array();
			$keys = array_keys($req);
			foreach($keys as $k) {
				if($k!="PHPSESSID") {
					if(is_array($req[$k])) {
						$v = "array(";
						$v2 = array();
							foreach($req[$k] as $a => $b) {
								//Author:Sondelani Dumalisile - 22 June 2021
								//comment: Spotted during standardisation.
								//caused Array to string conversion error because variable $b was assumed to be a string but was still an array.
								if (is_array($b)){
									foreach ($b as $b1){
										$v2[] = $a."=".$b1;
									}
								}else{
									$v2[] = $a."=".$b;
								}
							}
						$v.=implode(", ",$v2).")";
					} else {
						$v = $req[$k];
					}
					$vals[] = $k."=".$v;
				}
			}
			$val = $this->code(implode(", ",$vals));
		}
		$sql = "INSERT INTO ".$dbref."_user_log (id,tkid,tkname,dttoday,page,request) VALUES (null,'$tkid','$tkname',now(),'$page','$val')";
		$this->db_insert($sql);
	}




}


?>