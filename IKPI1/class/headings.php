<?php

class HEADINGS extends SDBP5B_HELPER {

	const ACTIVE = 1;

	private $headings_sections;

	public function __construct() {
		parent::__construct();
	}


	function getHeadings($get) {
		$dbref = $this->getDBRef();

		$sql = "SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
				FROM ".$dbref."_setup_headings sh
				INNER JOIN ".$dbref."_setup_headings_setup shs
				  ON sh.h_id = shs.head_id
				  AND shs.active & ".SDBP5_HEADINGS::ACTIVE." = ".SDBP5_HEADINGS::ACTIVE."
				WHERE
				  sh.h_active & ".SDBP5_HEADINGS::ACTIVE." = ".SDBP5_HEADINGS::ACTIVE."
				AND
				  shs.section IN ('".implode("','",$get)."')
				ORDER BY
				  shs.section,
				  shs.c_sort,
				  shs.i_sort
				";
		$rs = $this->mysql_fetch_all($sql);
		$h = array();
		if(in_array("KPI",$get) || in_array("CF",$get) || in_array("CAP",$get)) {
			$row = $this->assist_module_helper->mysql_fetch_one("SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table
				FROM ".$dbref."_setup_headings sh
				INNER JOIN ".$dbref."_setup_headings_setup shs
				  ON sh.h_id = shs.head_id
				  AND shs.active = true
				WHERE
				  sh.h_active = true
				AND
				  sh.h_table = 'dir'
				");
			$h['dir'] = $row;
		}
		foreach($rs as $row) {
			$h[$row['section']][$row['field']] = $row;
			if($row['fixed']) {
				$h['FIXED'][$row['field']] = $row;
			}
		}
		return $h;
	}




}


?>