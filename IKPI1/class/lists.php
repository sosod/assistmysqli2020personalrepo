<?php

class LISTS extends SDBP5B_HELPER {

	private $code_lists = array("munkpa","natkpa","kpitype","calctype","riskrating");
	private $list_types = array("LIST","WARDS","AREA","FUNDSRC","TEXTLIST");


	public function __construct() {
		parent::__construct();
	}


	function getList($l,$where="",$sort="") {
		$data = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_".$l." ".(strlen($where)>0 ? " WHERE ".$where : "")." ORDER BY ".(strlen($sort)>0 ? $sort : "sort,value");
		return $this->mysql_fetch_all_by_id($sql,"id");
	}

	function getAllListItems($l) {
		return $this->getList($l);
	}
	function getActiveListItems($l) {
		return $this->getList($l,"active = 1");
	}

}


?>