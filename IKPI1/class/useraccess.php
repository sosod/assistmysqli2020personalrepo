<?php

class USERACCESS extends SDBP5B_HELPER {

	public function __construct() {
		//echo "USERACCESS!!!";
		parent::__construct();
	}
	public function testUserAccess() { echo "this is the user access class responding"; }

	public function getUserAccess($user_id="") {
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		$data = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_user_access WHERE tkid = '".$user_id."' AND active = 1";
		echo $sql;
		$data = $this->assist_module_helper->mysql_fetch_one($sql);
		$this->arrPrint($data);
		if(count($data)==0) {
			$data[0] = false;
		} else {
			$data[0] = true;
		}

		return $data;

	}

	public function getAdminAccess($user_id="") {
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		$data = array();

		$sql = "SELECT * FROM ".$this->getDBRef()."_user_admins WHERE tkid = '".$user_id."' AND active = 1";
		$rows = $this->mysql_fetch_all($sql);

		if(count($rows)==0) {
			$data[0] = false;
		} else {
			$data[0] = true;
			$dirObj = new DIRSUB();
			$dirs = $dirObj->getDirectorates();
			$subs = $dirObj->getSubDirectorates();
			$dirsubs = $dirObj->groupSubDirectorates($subs);
			$listObj = new LISTS();
			$owns = $listObj->getActiveListItems("owner");
			foreach($rows as $r) {
				unset($r['id']);
				$active = false;
				$t = $r['type'];
				$i = $r['ref'];
				if( ($r['act_update']+$r['act_edit']+$r['act_create']+$r['act_approve'])>0) {
					switch($t) {
						case "DIR":
							if(isset($dirs[$i])) {
								$active = true;
								if(isset($dirsubs[$i])) {
									foreach($dirsubs[$i] as $si => $x) {
										$s = $r;
										$s['type'] = "SUB";
										$s['ref'] = $si;
										$data['SUB'][$si] = $s;
									}
								}
							}
							break;
						case "TOP":
							if(isset($dirs[$i])) { $active = true; }
							break;
						case "SUB":
							if(isset($subs[$i])) { $active = true; }
							break;
						case "OWN":
							if(isset($owns[$i])) { $active = true; }
							break;
					}
					if($active && !isset($data[$t][$i])) {
						$data[$t][$i] = $r;
					}
				}
			}
		}

		return $data;
	}

	public function getFullAccess($user_id = "") {
		$data = array(
			'access'=>$this->getUserAccess($user_id),
			'admin'=>$this->getAdminAccess($user_id),
		);
		return $data;
	}

}


?>