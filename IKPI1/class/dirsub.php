<?php

class DIRSUB extends SDBP5B_HELPER {

	public function __construct() {
		parent::__construct();
	}


	public function getDirSubName($si) {
		$sql = "SELECT d.value as dir, s.value as sub
				FROM ".$this->getDBRef()."_subdir s
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON d.id = s.dirid
				WHERE s.id = $si";
		$row = $this->assist_module_helper->mysql_fetch_one($sql);
		$data = "";
		if(isset($row['dir'])) {
			$data = $row['dir']." - ".$row['sub'];
		}
		return $data;
	}


	public function getDirectorates() {
		$data = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_dir WHERE active = 1 ORDER BY sort, value";
		$data = $this->mysql_fetch_all_by_id($sql,"id");
		return $data;
	}

	public function getSubDirectorates() {
		$data = array();

		$dirs = $this->getDirectorates();
		$dir_ids = array_keys($dirs);

		if(count($dir_ids)>0) {
			$sql = "SELECT s.* FROM ".$this->getDBRef()."_subdir s
					WHERE s.dirid IN (".implode(",",$dir_ids).") AND
					s.active = 1 ORDER BY s.dirid ASC, s.head DESC, s.sort ASC, s.value ASC";
			$data = $this->mysql_fetch_all_by_id($sql,"id");
		}

		return $data;
	}

	public function groupSubDirectorates($subs = array()) {
		if(count($subs)==0) { $subs = $this->getSubDirectorates(); }

		$data = array();

		foreach($subs as $i => $s) {
			$di = $s['dirid'];
			if(!isset($data[$di])) { $data[$di] = array(); }
			$data[$di][$i] = $s;
		}
		return $data;
	}
	public function getParentID($sub_id) {
			$sql = "SELECT s.dirid
				FROM ".$this->getDBRef()."_subdir s
				WHERE s.id = $sub_id";
			$row = $this->assist_module_helper->mysql_fetch_one($sql);
			return $row['dirid'];
	}

}


?>