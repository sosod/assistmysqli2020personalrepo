<?php

class IKPI1_CAPITAL extends IKPI1_HELPER {

	private $section = "CAP";
	private $table_name;
	private $results_table_name;

	const ACTIVE = 1;

	const HEAD = "CAP";
	const HHEAD = "CAP_H";
	const RHEAD = "CAP_R";

	const REFTAG = "CP";

	const OBJECT_TYPE = "PROJECT";	//used by external class

	public function __construct($modref="",$external=false) {
		parent::__construct("client","",false,$modref);
		$this->table_name = $this->getDBRef()."_capital";
		$this->results_table_name = $this->getDBRef()."_capital_results";
	}


	/********************************
			GET FUNCTIONS
	********************************/
	public function getHeadingCodes() { return array(self::HEAD,self::HHEAD,self::RHEAD); }

	public function getResults($i) {
		$result_sql = "SELECT * FROM ".$this->results_table_name." WHERE cr_capid = ".$i." ORDER BY cr_timeid";
		$results = $this->mysql_fetch_all_fld($result_sql,"cr_timeid");

		return $results;
	}


	public function getLiteDetails($i) {
		$sql = "SELECT c.cap_name, c.cap_descrip, c.cap_vote, cap_subid as si
				FROM ".$this->table_name." c
				WHERE c.cap_id = ".$i;
		$data = $this->mysql_fetch_one($sql);
		if(isset($data['si'])) {
			$ds = new DIRSUB();
			$data['dir'] = $ds->getDirSubName($data['si']);
		}
		return $data;
	}

	public function getRawObjectsForExternalModule($limit=false) {
			$sql = "SELECT * FROM ".$this->table_name." WHERE cap_active = 1";
			$rows = $this->mysql_fetch_all_by_id($sql,"cap_id");

			if(!$limit) {
				$sql = "SELECT * FROM ".$this->table_name."_wards WHERE cw_active = 1";
				$wards = $this->mysql_fetch_all_by_id2($sql, "cw_capid", "cw_listid");
				$sql = "SELECT * FROM ".$this->table_name."_area WHERE ca_active = 1";
				$area = $this->mysql_fetch_all_by_id2($sql, "ca_capid", "ca_listid");
			}

			foreach($rows as $i => $r) {
				$rows[$i]['targets'] = array();//no financials are imported as they are loaded through the integration server
				if(!$limit) {
					if(isset($wards[$i])) {
						$rows[$i]['capital_wards'] = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, array_keys($wards[$i]));
					}
					if(isset($area[$i])) {
						$rows[$i]['capital_area'] = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, array_keys($area[$i]));
					}
				}
			}
			return $rows;
	}

	public function getRefTag() { return self::REFTAG; }



	public function getListItemsForSelect() {
		$rows = $this->getRawObjectsForExternalModule(true);
		$data = array();

		foreach($rows as $i => $r) {
			$data[$i] = $r['cap_name']." (".self::REFTAG.$i.")";
		}

		return $data;
	}


}


?>