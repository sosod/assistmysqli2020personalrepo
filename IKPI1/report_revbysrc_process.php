<?php 
ini_set('max_execution_time',1800);
/*** GET REQUEST ***/
$fields = $_REQUEST['fields'];
//$krsum = $_REQUEST['summary'];
$filter = $_REQUEST['filter'];
$output = $_REQUEST['output'];
$report_head = strlen($_REQUEST['rhead'])>0 ? $_REQUEST['rhead'] : $_SESSION['modtext'].": ".$section_head." Report";
$report_time = array($_REQUEST['r_from'],$_REQUEST['r_to']);
sort($report_time);
$groupby = "X";
$group["oX"] = array('id'=>"oX",'value'=>"",'objects'=>array());

/* OBJECT SQL */
$sql_fields = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "LIST":
			$sql_fields[] = $h['h_table'].".value as ".$fld;
			if($fld!="dir") {
				$sql_fields[] = "o.".$fld." as o".$fld;
			} else {
				$sql_fields[] = "dir.id as odir";
			}
			break;
		default: $sql_fields[] = "o.".$fld;
	}
}
switch($section) {
case "RS":
	$object_sqla[0] = "SELECT o.rs_id as obj_id, ".implode(", ",$sql_fields);
	$object_sqla[1] = "FROM ".$dbref."_revbysrc o ";
						foreach($mheadings[$section] as $fld => $h) {
							$a = "id";
							if($h['h_type']=="LIST") {
								$object_sqla[1].= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
							}
						}
	$object_sqla[1].= " WHERE o.rs_active = true";
	break;
}
	$my_sort = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
			if(is_numeric($amnr) || is_numeric($wmnr)) {
				$object_sqla[1].= " AND o.".$table_fld."id IN (".implode(",",$wa_obj).") ";
			}
			break;
		case "AREA":
		case "FUNDSRC":
			break;
		case "DATE":
			$datefilter = $filter[$fld];
			if( ( strlen($datefilter[0])+strlen($datefilter[1]) ) > 0 ) {	//if there is a date then do something
				if($datefilter[0]==$datefilter[1] || strlen($datefilter[0])==0 || strlen($datefilter[1])==0) {	//if there is only one date
					if(strlen($datefilter[0])==0 || $datefilter[0]==$datefilter[1]) {
						$df = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[1]." 23:59:59") : strtotime($datefilter[1]); 
					} else {
						$df = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[0]." 23:59:59") : strtotime($datefilter[0]); 
					}
					$object_sqla[1].= " AND o.".$fld." = $df ";
				} else {
					$df0 = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[0]." 23:59:59") : strtotime($datefilter[0]); 
					$df1 = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[1]." 23:59:59") : strtotime($datefilter[1]); 
					$object_sqla[1].= " AND o.".$fld." >= $df0 AND o.$fld <= $df1 ";
				}
			}
			break;
		case "LIST":
			if($filter[$fld][0]!="ANY") {
				if($fld!=$table_fld."calctype") {
					$object_sqla[1].=" AND ".$h['h_table'].".id IN (".implode(",",$filter[$fld]).") ";
				} else {
					$object_sqla[1].=" AND ".$h['h_table'].".code IN ('".implode("','",$filter[$fld])."') ";
				}
			}
			if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = $h['h_table'].".value"; }
			break;
		default:
			$flt = $helperObj->decode(trim($filter[$fld][0]));
			$match = $filter[$fld][1];
			if(strlen($flt)>0) {
				switch($match) {
					case "EXACT":	$object_sqla[1].=" AND o.".$fld." LIKE '%".$flt."%' "; break;
					case "ALL":
						$flt = explode(" ",$flt);
						foreach($flt as $f) { $object_sqla[1].=" AND o.".$fld." LIKE '%".$f."%' "; }
						break;
					case "ANY":
					default:
						$flt = explode(" ",$flt);
						$object_sqla[1].= " AND (o.".$fld." LIKE '%";
						$object_sqla[1].= implode("%' OR o.".$fld." LIKE '%",$flt);
						$object_sqla[1].= "%') ";
						break;
				}
			}
			break;
	}
}
$object_sqla[2]=" ORDER BY o.".$table_fld."sort, o.".$table_fld."value";
//$rs = getRS(implode(" ",$object_sqla));
$rs = $helperObj->mysql_fetch_all(implode(" ",$object_sqla));
$objects = array();
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rs as $row){
	$objects[$row['obj_id']] = $row; 
	if($groupby!="X") {
//		echo "<P>".$row['obj_id']." :: ".'o'.$groupby." :: ".$row['o'.$groupby]; arrPrint($group[$row['o'.$groupby]]);
		if(isset($group[$row['o'.$groupby]])) { $group[$row['o'.$groupby]]['objects'][$row['obj_id']] = "Y"; }
	} else {
		$group['oX']['objects'][$row['obj_id']] = "Y";
	}
}

/* RESULTS */
$r_sql = "SELECT * FROM ".$dbref."_".$table_tbl."_results WHERE ".$r_table_fld."timeid <= ".$report_time[1]." AND ".$r_table_fld.$table_id."id IN (SELECT ".$table_fld."id ".$object_sqla[1].") ORDER BY ".$r_table_fld.$table_id."id, ".$r_table_fld."timeid";
//$results = mysql_fetch_alls2($r_sql,"".$r_table_fld.$table_id."id","".$r_table_fld."timeid");
$results = $helperObj->mysql_fetch_all_fld2($r_sql,"".$r_table_fld.$table_id."id","".$r_table_fld."timeid");

/*** START OUTPUT ***/
//$c2 = count($mheadings[$h_section])*count($mheadings[$r_section]);
$c2 = count($mheadings[$r_section]);
$fc = count($fields)+1+($c2*(1+$report_time[1]-$report_time[0]));
$echo = "";
//$cspan = 1 + count($fields);
//$tspan = (count($time) * $c2) + 3;
//$cspan += $tspan;
switch($output) {
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	for($c=1;$c<$c2;$c++) { $cellz[10].="\"\","; }
	$cella[19] = "\""; //heading cell
	$cellz[19] = "\","; //heading cell
	for($c=1;$c<count($mheadings[$r_section]);$c++) { $cellz[19].="\"\","; }
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22]['OVERALL'] = "\"";	//ptd normal cell
	$cella[22]['NUM'] = "\""; //result cell
	$cella[22]['DOUBLE'] = "\""; //result cell
	$cella[22]['PERC'] = "\""; //result cell
	$cella[3] = "\""; //total result cell
	for($c=1;$c<=count($fields);$c++) { $cella[3].="\",\""; }
	$cella[32]['NUM'] = "\""; //result cell total
	$cella[32]['DOUBLE'] = "\""; //result cell total
	$cella[32]['PERC'] = "\""; //result cell total
	$cella[32]['OVERALL'] = "\""; //overall result cell total
	$rowa = "";
	$rowz = "\r\n";
	$pagea="\"".$cmpname."\"\r\n\"".$report_head."\"";
	$table[1] = "\r\n";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	$decimal = ".";
	$thou = "";
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if($c2>0) { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=\"head\" colspan=$c2>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[19] = "<td class=\"head\" style=\"background-color: #bcbcbc;\"  colspan=".count($mheadings[$r_section]).">"; //period-to-date heading cell
	$cellz[19] = "</td>"; //period-to-date heading cell
	$cella[11] = "<td class=\"head\" style=\"background-color: #bcbcbc;\">"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<td class=\"head\" rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td class=kpi>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=kpi style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22]['OVERALL'] = "<td style=\"text-align: right;background-color: #eeeeee\" class=kpi>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['DOUBLE'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['PERC'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[3] = "<td colspan=".(count($fields)+1)." class=kpi style=\"text-align: right; font-weight: bold; background-color: #eeeeee;\">"; //total result cell
	$cella[32]['NUM'] = "<td class=kpi style=\"text-align: right; font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['DOUBLE'] = "<td class=kpi style=\"text-align: right; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['PERC'] = "<td class=kpi style=\"text-align: right; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['OVERALL'] = "<td class=kpi style=\"text-align: right; font-weight: bold; background-color: #bcbcbc;\">"; //overall result cell total
	$rowa = chr(10)."<tr>";
	$rowz = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>"
			.chr(10)." td { font-style: Calibri; font-size:11pt; } 
			.kpi { border-width: thin; border-color: #000000; border-style: solid; vertical-align: top; } 
			.kpi_r { border-width: thin; border-color: #000000; border-style: solid; text-align: center; color: #FFFFFF; vertical-align: top; } 
			.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } "
			.chr(10)." .title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } "
			.chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
			.title3 { font-size:14pt; font-style: Calibri; color:#000099; font-weight:bold;} 
			.title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}
			</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$fc>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$fc>";
		$pagea.=$report_head;
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td></td><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = " ";
	$decimal = ".";
	$thou = " ";
	break;
default:
	$gcell[1] = "<h3>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	if($c2>0) { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=$c2>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[19] = "<th colspan=".count($mheadings[$r_section]).">"; //time heading cell
	$cellz[19] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=2>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[22]['OVERALL'] = "<td style=\"background-color: #eeeeee\" class=right>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=right>"; //result cell
	$cella[22]['DOUBLE'] = "<td class=right>"; //result cell
	$cella[22]['PERC'] = "<td class=right>"; //result cell
	$cella[3] = "<td colspan=".(count($fields)+1)." class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //total result cell
	$cella[32]['NUM'] = "<td class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['DOUBLE'] = "<td class=right style=\"background-color: #eeeeee;\">"; //result cell total
	$cella[32]['PERC'] = "<td class=right style=\"background-color: #eeeeee;\">"; //result cell total
	$cella[32]['OVERALL'] = "<td class=right style=\"font-weight: bold; background-color: #bcbcbc;\">"; //overall result cell total
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"inc/main.css\" type=\"text/css\">";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 style=\"text-align: center;\">".$cmpname."</h1><h2 style=\"text-align: center;\">";
	$pagea.=$report_head;
	$pagea.= "</h2>";
	$table[1] = "<table width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	$decimal = ".";
	$thou = ",";
	break;
}
$totals = array();
foreach($result_settings as $i => $r) { $totals[$i] = 0; }
//START PAGE
$echo = $pagea;
$echo.= $table[1];
//GROUP BY 
foreach($group as $i => $g) {
	$g_objects = $g['objects'];
	if(count($g_objects)>0) {
		$groupresults = array();
		for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
			foreach($mheadings as $fld=>$h) { $groupresults[$t][$fld]=0; }
		}
		if(strlen($g['value'])>0) {
			$echo.= $table[9].$gcell[1].$helperObj->decode($g['value']).$gcell[9].$table[1];
		}
		//Main heading
		$echo.= $rowa;
			$echo .= $cella[12]."Ref".$cellz[12];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) { $echo .= $cella[12].$helperObj->decode($h['h_client']).$cellz[12]; }
			}
			for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
				$echo.=$cella[10].$time[$t]['display_full'].$cellz[10];
			}
			if($report_time[0]!=$report_time[1]) {
				$echo.=$cella[19]."Total for the Period".$cellz[19];
			}
		$echo .= $rowz;
		//Time heading
		if($c2>0) {
			$echo.= $rowa;
			if($output=="csv") {
				for($c=0;$c<=count($fields);$c++) $echo.=$cella[12].$cellz[12];
			}
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
						foreach($mheadings[$r_section] as $fld => $h) {
							$echo.=$cella[1].$helperObj->decode($h['h_client']).$cellz[1];
						}
				}
			if($report_time[0]!=$report_time[1]) {
						foreach($mheadings[$r_section] as $fld => $h) {
							$echo.=$cella[1].$helperObj->decode($h['h_client']).$cellz[1];
						}
			}
			$echo .= $rowz;
		}
		foreach($g_objects as $obj_id => $g_obj) {
			$obj = $objects[$obj_id];
			$results[$obj_id]['TOTAL'] = array();
			$echo .= $rowa.$cella[2].$id_labels_all[$section].$obj_id.$cellz[2];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) {
					$echo.=$cella[2];
					switch($h['h_type']) {
						case "WARDS":
						case "AREA":
						case "FUNDSRC":
							$echo.= $helperObj->decode(implode("; ",$object_wa[strtolower($h['h_table'])][$obj_id]));
							break;
						case "DATE":
							$echo.= $obj[$fld]>0 ? date("d M Y",$obj[$fld]) : "";
							break;
						case "LIST":
						default:
							$echo.= str_replace(chr(10),$newline,$helperObj->decode($obj[$fld]));
							break;
					}
					$echo.=$cellz[2];
				}
			}
			if($c2>0) { //results
				/** MONTHLY RESULTS **/
				$res = $results[$obj_id];
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
						foreach($mheadings[$r_section] as $fld => $h) {
							if(!isset($res[$report_time[1]+1][$fld])) { $res[$report_time[1]+1][$fld]=0; }
							if(!isset($groupresults[$t][$fld])) { $groupresults[$t][$fld] = 0; } $groupresults[$t][$fld]+=$res[$t][$fld];
							switch($h['h_type']) {
							case "PERC":
								$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fld],2,$decimal,$thou)."%".$cellz[2];
								break;
							case "DOUBLE":
							default:
								$res[$report_time[1]+1][$fld]+=$res[$t][$fld];
								$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
				}
				/** OBJECT TOTAL **/
				$t = $report_time[1]+1;
				if($report_time[1]!=$report_time[0]) {
						foreach($mheadings[$r_section] as $fld => $h) {
							switch($h['h_type']) {
							case "PERC":
								//$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fld],2,$decimal,$thou)."%".$cellz[2];
								break;
							case "DOUBLE":
							default:
								$echo.=$cella[22]['OVERALL'].number_format($res[$t][$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
				}
			}
			$echo.=$rowz;
		} //foreach g['objects']
		if($c2>0) {
			$echo.=$rowa.$cella[3]."Total:".$cellz[2];
				$res = $groupresults;
				/* MONTHLY TOTAL */
					for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
						foreach($mheadings[$r_section] as $fld => $h) {
							switch($h['h_type']) {
							case "PERC":
								$echo.=$cella[32][$h['h_type']].$cellz[2];
								break;
							case "DOUBLE":
							default:
								if(!isset($res[$report_time[1]+1][$fld])) { $res[$report_time[1]+1][$fld]=0; } $res[$report_time[1]+1][$fld]+=$res[$t][$fld];
								$echo.=$cella[32][$h['h_type']].number_format($res[$t][$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
					}
				/** GRAND TOTAL **/
				if($report_time[1]!=$report_time[0]) {
						foreach($mheadings[$r_section] as $fld => $h) {
							switch($h['h_type']) {
							case "PERC":
								//$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fld],2,$decimal,$thou)."%".$cellz[2];
								break;
							case "DOUBLE":
							default:
								$echo.=$cella[32]['OVERALL'].number_format($res[$t][$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
				}
			$echo.=$rowz;
		}
	} //if count(g['objects']) > 0
} //foreach group
$echo .= $table[9];
$echo.= $pagez;

if($output == "csv") { $ext = ".csv"; $content = "text/plain"; } else { $ext = ".xls"; $content = "application/ms-excel"; }
switch($output) {
case "csv":
case "excel":
	$path = $report_save_path;
	$sys_filename = $modref."_".$section."_".date("Ymd_His").$ext;
    $helperObj->saveEcho($path,$sys_filename,$echo);
	$usr_filename = $section."_report_".date("Ymd_His",$today).$ext;
    $helperObj->downloadFile2($path, $sys_filename, $usr_filename, $content);
	break;
default:
	echo $echo;
	break;
}
?>