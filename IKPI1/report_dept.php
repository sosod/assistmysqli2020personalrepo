<?php 
$section = "KPI"; 
$get_lists = true;
if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") { $get_active_lists = false; } else { $get_active_lists = true; }
include("inc/header.php"); 

switch($page_id) {
	case "generate":
	/* SETUP VARIABLES */
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",$table_fld."repcate",
		"dir",$table_fld."subid",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
	);
	$head0['dir'] = array_key_exists(0,$mheadings['dir']) ? $mheadings['dir'][0] : array();
	$head_kpi = isset($mheadings[$section]) ? $mheadings[$section] : array();
	$head = array_merge($head0,$head_kpi);

	$src = isset($_REQUEST['src']) ? $_REQUEST['src'] : "GENERATOR";
	if($src=="GRAPH" && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
		$data = unserialize(base64_decode($_REQUEST['dta']));
		//arrPrint($data);
		if(isset($data['dirsub_filter']) && strpos($data['dirsub_filter'],"_")!==false) {
			$ds_filter = explode("_",$data['dirsub_filter']);
			if($ds_filter[0]=="D") {
				$_REQUEST['filter']['dir'][0] = $ds_filter[1];
			} elseif($ds_filter[0]=="S") {
				$_REQUEST['filter']['kpi_subid'][0] = $ds_filter[1];
			}
		}
		$repcate = $data['repcate'];
		if(count($repcate)==0) {
			$repcate = array("ANY");
		}
		$_REQUEST['filter'][$table_fld."repcate"] = $repcate;
		$_REQUEST['fields'] = array(
				0 => "dir",
				1 => "kpi_subid",
				2 => "kpi_topid",
				3 => "kpi_capitalid",
				4 => "kpi_gfsid",
				5 => "kpi_idpref",
				6 => "kpi_natoutcomeid",
				7 => "kpi_idpid",
				8 => "kpi_natkpaid",
				9 => "kpi_munkpaid",
				10 => "kpi_mtas",
				11 => "kpi_value",
				12 => "kpi_unit",
				13 => "kpi_conceptid",
				14 => "kpi_typeid",
				15 => "kpi_riskref",
				16 => "kpi_risk",
				17 => "kpi_riskratingid",
				18 => "kpi_wards",
				19 => "kpi_area",
				20 => "kpi_ownerid",
				21 => "kpi_baseline",
				22 => "kpi_pyp",
				23 => "kpi_perfstd",
				24 => "kpi_poe",
				25 => "kpi_calctype",
				26 => "results",
				27 => "kr_perf",
				28 => "kr_correct",
				29=>"kpi_repcate",
			);
		//$_REQUEST['r_from'] => from graph report
		//$_REQUEST['r_to'] => from graph report
		$_REQUEST['r_calc'] = "captured";
		$_REQUEST['summary'] = "Y";
		//$_REQUEST['filter']['group_by_filter'] = array([0]=>#) => from graph where applicable
		//$_REQUEST['filter']['result'] = # => from graph
		//$_REQUEST['groupby'] => from graph
		$_REQUEST['filter']['kpi_wards'] = array("ANY");
		$_REQUEST['filter']['kpi_area'] = array("ANY");
		$_REQUEST['sort'] = array(
				0 => "dir",
				1 => "kpi_gfsid",
				2 => "kpi_idpref",
				3 => "kpi_natoutcomeid",
				4 => "kpi_idpid",
				5 => "kpi_natkpaid",
				6 => "kpi_munkpaid",
				7 => "kpi_mtas",
				8 => "kpi_value",
				9 => "kpi_unit",
				10 => "kpi_conceptid",
				11 => "kpi_typeid",
				12 => "kpi_riskref",
				13 => "kpi_risk",
				14 => "kpi_riskratingid",
				15 => "kpi_ownerid",
				16 => "kpi_baseline",
				17 => "kpi_pyp",
				18 => "kpi_perfstd",
				19 => "kpi_poe",
				20 => "kpi_id",
			);	
		$_REQUEST['output'] = "display";
		$_REQUEST['rhead'] = "";
	}
//arrPrint($_REQUEST);
		if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
			include("report_kpi_process.php");
		} else {
			include("report_kpi_generate.php");
		}
		break;
	case "quick":
		include("report_dept_quick.php");
		break;
	default:
		echo "<p>An error occurred.  Please try again.</p>";
		break;
}


if(!(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE")) { echo "</body></html>"; }

?>