<?php

function formatStringForGraph($s) {
	$s = str_replace(chr(10)," ",$s);
	$s = str_replace("\r\n"," ",$s);
	$s = str_replace("\n"," ",$s);
	$s = str_replace("&amp;","and",$s);
	return $s;
}

if($kpi_nm=="EXCLUDE") {
	unset($result_settings[0]);
}

if($kpi_met=="COMBINE") {
	unset($result_settings[4]);
	unset($result_settings[5]);
}


if($graph_display=="ONS") {		
		
	echo "
		<script type=text/javascript>
			//FUNCTION TO DIRECT ON CLICK TO TABULAR REPORT
				function goReport(result,fld,id) {
					var url = '".$report_settings['path']."?page_id=generate&act=GENERATE&src=GRAPH&r_from=".$report_settings['r_from']."&r_to=".$report_settings['r_to']."&groupby=".$report_settings['groupby']."&filter[result]='+result;
					if(fld!=\"ALL\" && !isNaN(parseInt(id)) && id>0) {
						url = url + '&filter[' + fld + '][]=' + id;
					}
					//alert(url);
					document.location.href = url;
				}
				function goToReport(result,fld,id,dta) {
					var url = '".$report_settings['path']."?page_id=generate&act=GENERATE&src=GRAPH&r_from=".$report_settings['r_from']."&r_to=".$report_settings['r_to']."&groupby=".$report_settings['groupby']."&filter[result]='+result;
					if(fld!=\"ALL\" && !isNaN(parseInt(id)) && id>0) {
						url = url + '&filter[' + fld + '][]=' + id;
					}
					if(dta.length>0) {
						url = url + '&dta='+dta;
					}
					//alert(url);
					document.location.href = url;
				}
			</script>
			";
		
	if($page_layout=="MOBILE") {
		
		foreach($result_settings as $ky => $k) {
			$k['obj'] = isset($count[$k['text']]) ? $count[$k['text']] : 0;
			$val = array();
			foreach($k as $key => $s) {
				$val[] = $key.":\"".formatStringForGraph($s)."\"";
			}
			$kr2[$ky] = implode(",",$val);
		}
		$chartData = "{".implode("},{",$kr2)."}";
		$sub_chartData = array();
		$sub_ids = array();
        $valueAxis_maximum = 0;
        $my_lists = array();
		if($groupby!="NA") {
			switch($groupby) {
			case "obj_subid": case "obj_dirid":
				if($filter_who[0]=="X" ) { 
					$my_lists = $lists['dir']; 
				} else {
					$my_lists = $lists['subdir']['dir'][$filter_who[1]];
				}
				break;
			default:
				$my_lists = $lists[$mheadings[$section][$groupby]['h_table']];
			}
/* Added to catch any line breaks which are possible in some list items [JC - 13 Jan 2019] */
foreach($my_lists as $key => $l) {
	foreach($l as $k => $v) {
		$my_lists[$key][$k] = formatStringForGraph($v);
	}
}
			if($graph_type[0]=="PIE") {
				foreach($my_lists as $s_id => $s) { 
					if(isset($sub_count[$s_id])) { 
						$sub_ids[$s_id] = $s['value'];
						$kr2 = array();
						foreach($result_settings as $ky => $k) {
							$k['obj'] = isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0;
							$val = array();
							foreach($k as $key => $s) {
								$val[] = $key.":\"".formatStringForGraph($s)."\"";
							}
							$kr2[$ky] = implode(",",$val);
						}
						$sub_chartData[$s_id] = "{".implode("},{",$kr2)."}";
					}
			
				}
			} else {
				$max_count = 0;
				$row_count = 0;
				$sub_row = 0;
				$max_kpis = 0;
				$valueAxis_maximum = 0;
				foreach($my_lists as $s_id => $s) { 
					if(isset($sub_count[$s_id])) { 
						$max_count++; if($max_count>$max[$row_count]) { $sub_row++; $sub_chartData[$sub_row] = array(); $sub_ids[$sub_row] = array(); $max_count = 1; $row_count = 1; }
						$sval = explode(" ",ASSIST_HELPER::decode($s['value']));
						$s_name = "";
						$s_n = "";
						$sn_rows = 1;
						foreach($sval as $sv) {
							if(strlen($s_n.$sv)>18) {
								$s_name.=$s_n."\\n";
								$s_n = "";
								$sn_rows++;
							}
							$s_n.=" ".$sv;
							if($sn_rows>4) { $s_name = substr($s_name,0,-2); $s_n="..."; break; } //else {$s_n.=$sn_rows; }
						}
						$s_name.=" ".$s_n;
						if($sn_rows>3) { $s_name = "\\n".$s_name; }
						$cD = "{text:\"".$s_name."\"";
						foreach($result_settings as $ky => $k) {
							$cD.= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
							$max_kpis+=(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
						}
						$cD.= "}";
						$sub_chartData[$sub_row][] = $cD;
						$sub_ids[$sub_row][] = $s_id;
						$valueAxis_maximum = $max_kpis > $valueAxis_maximum ? $max_kpis : $valueAxis_maximum;	$max_kpis = 0;
					}
			
				}
			}
		}




		if($graph_type[0]=="PIE") {
			drawPieGraph($report_settings,$page_layout,$graph_title,array('NM'=>$kpi_nm,'MET'=>$kpi_met),$result_settings,$count,$chartData,$graph_type[1],$sub_chartData,$my_lists,$sub_ids,$sub_count);
		} else {
			drawBarGraph($report_settings,$page_layout,$graph_title,array('NM'=>$kpi_nm,'MET'=>$kpi_met),$result_settings,$count,$chartData,$graph_type[1],$valueAxis_maximum,$sub_chartData,$my_lists,$sub_ids,$sub_count,$display_grid);
		}
	
		echo "
			<script type=text/javascript>
				$(\"th\").addClass(\"center\");
			</script>";

	
	} elseif($page_layout=="COMP") {
	
			switch($groupby) {
			case "obj_subid": case "obj_dirid":
				if($filter_who[0]=="X" ) { 
					$my_lists = $lists['dir']; 
				} else {
					$my_lists = $lists['subdir']['dir'][$filter_who[1]];
				}
				break;
			default:
				$my_lists = $lists[$mheadings[$section][$groupby]['h_table']];
			}

			drawLegendStyle();
			//$_SESSION['screen']['width'] = 1900;
			$screen_width = $_SESSION['screen']['width'];
			$pie_width = 540;


		if($graph_type[0]=="BAR") {

			$bar_width = 80;
			$first_bars = floor(($screen_width - $pie_width - 50)/$bar_width);
			$rest_bars = floor(($screen_width - 50)/$bar_width);
			
			//echo "<p>Your screen resolution is: ".$_SESSION['screen']['width']."x".$_SESSION['screen']['height']." meaning that the bar graph can have a max of ".$first_bars." & the rest ".$rest_bars.".</p>";
			echo "<div align=center><table class=noborder >
					<tr>
						<td class=\"noborder center\">";
				drawFlashMainPie($graph_title['main'],$count);
				if($groupby=="NA") {
					echo "</td></tr><tr><td class=\"noborder center\">";
					drawFlashLegend("MAIN",$graph_title,$count,array());
				} else {
					$g = 0;
					$group = array();
					foreach($my_lists as $l) {
						$l['value'] = formatStringForGraph($l['value']);
						if(isset($sub_count[$l['id']])) {
							$group[] = $l;
						} 
						if(($g==0 && count($group)==$first_bars) || ($g>0 && count($group)==$rest_bars)) {
							createComputerBarGraph($group,$g,$graph_title,array('main'=>$count,'sub'=>$sub_count),$graph_type[1],$display_grid);
							$g++;
							$group = array();
						}
					}
					if(count($group)>0) {
							createComputerBarGraph($group,$g,$graph_title,array('main'=>$count,'sub'=>$sub_count),$graph_type[1],$display_grid);
							$g++;
							$group = array();
					}
				}
			echo "</td>
					</tr>
				</table></div>
				<script type=text/javascript>
				$(function() {
					$(\"tr\").off('mouseenter mouseleave');
					//$('td').removeClass('noborder');
				});
				</script>";

		} else {	//graph_type = pie
		
			
			
				$cols = floor($screen_width/$pie_width);
				$page_break = 1;
				$p = 0;
			
			$g = 0;
			echo "
			<div align=center><table class=noborder >
				<tr>
					<td class=\"noborder center\" >";
			if($graph_type[1]!="ABOVE") {
				$g++;
				echo "<table class=noborder width=100%><tr><td class=\"center noborder\">";
			}
					drawFlashMainPie($graph_title['main'],$count,"single");
					drawPieLegend($count,true);
			if($graph_type[1]=="ABOVE") {
				echo "</td>
					</tr><tr><td class=\"noborder center\"><table class=noborder width=100% ".($page_layout=="LAND" ? "style=\"page-break-before: always;\"" : "")."><tr>";
			}
			foreach($my_lists as $l) {
				$l['value'] = formatStringForGraph($l['value']);
				if(isset($sub_count[$l['id']])) {
					$g++;
					if($g>$cols) { 
						$p++; 
						if($p==$page_break || $page_layout=="LAND") {
							$p = 0;
							echo "</tr></table><table class=noborder width=100% style=\"page-break-before: always;\"><tr>"; 
						} else {
							echo "</tr></table><table class=noborder width=100%><tr>"; 
						}
						$g = 1; 
					}
					echo "<td class=\"center noborder\">";
						drawFlashMainPie($l['value'],$sub_count[$l['id']],"single",$l['id']);
						drawPieLegend($sub_count[$l['id']],true,$l['id']);
					echo "</td>";
				}
			}
			echo "</tr></table></td></tr>
			</table></div>
				<script type=text/javascript>
				$(function() {
					$(\"tr\").off('mouseenter mouseleave');
					//$('td').removeClass('noborder');
				});
				</script>		
			";
				
		
		
		}	//end if graph_type = pie/bar




	
	
	
	
	
	
	}








} elseif($graph_display=="PRINT") {	


	drawLegendStyle();

	switch($groupby) {
	case "obj_subid": case "obj_dirid":
		if($filter_who[0]=="X" ) { 
			$my_lists = $lists['dir']; 
		} else {
			$my_lists = $lists['subdir']['dir'][$filter_who[1]];
		}
		break;
	default:
		$my_lists = $lists[$mheadings[$section][$groupby]['h_table']];
	}



	if($graph_type[0]=="BAR") {

		switch($page_layout) {
		case "PORT":
			$bars = 7;
			break;
		case "LAND":
		default:
			$bars = 11;
			break;
		}

			echo "
		<div align=center><table class=noborder width=100%>
			<tr>
				<td class=\"noborder center\">";
		drawFlashMainPie($graph_title['main'],$count);
				echo "</td>
						</tr>
						<tr>
							<td class=noborder>";
					drawFlashLegend("MAIN",$graph_title,$count,array());
		$g = 0;
		$group = array();
		foreach($my_lists as $l) {
			$l['value'] = formatStringForGraph($l['value']);
			if(isset($sub_count[$l['id']])) {
				$group[] = $l;
			} 
			if(count($group)==$bars) {
				createFlashBarGraph($group,$g,$graph_title,$sub_count,$graph_type[1],$display_grid);
				$g++;
				$group = array();
			}
		}
		if(count($group)>0) {
				createFlashBarGraph($group,$g,$graph_title,$sub_count,$graph_type[1],$display_grid);
				$g++;
				$group = array();
		}
		echo "</td>
				</tr>
			</table></div>
			<script type=text/javascript>
			$(function() {
				$(\"tr\").off('mouseenter mouseleave');
				//$('td').removeClass('noborder');
			});
			</script>";




	} else {	//IF GRAPH_TYPE==PIE
	
		switch($page_layout) {
		case "PORT":
			$cols = 3;
			$page_break = 2;
			$p = $graph_type[1]=="ABOVE" ? 1 : 0;
			break;
		case "LAND":
		default:
			$cols = 4;
			$page_break = 1;
			$p = 0;
			break;
		}
		
		$g = 0;
		echo "
		<div align=center><table class=noborder >
			<tr>
				<td class=\"noborder center\" >";
		if($graph_type[1]!="ABOVE") {
			$g++;
			echo "<table class=noborder width=100%><tr><td class=\"center noborder\">";
		}
				drawFlashMainPie($graph_title['main'],$count,"multi");
				drawPieLegend($count);
		if($graph_type[1]=="ABOVE") {
			echo "</td>
				</tr><tr><td class=\"noborder center\"><table class=noborder width=100% ".($page_layout=="LAND" ? "style=\"page-break-before: always;\"" : "")."><tr>";
		}
		foreach($my_lists as $l) {
			$l['value'] = formatStringForGraph($l['value']);
			if(isset($sub_count[$l['id']])) {
				$g++;
				if($g>$cols) { 
					$p++; 
					if($p==$page_break || $page_layout=="LAND") {
						$p = 0;
						echo "</tr></table><table class=noborder width=100% style=\"page-break-before: always;\"><tr>"; 
					} else {
						echo "</tr></table><table class=noborder width=100%><tr>"; 
					}
					$g = 1; 
				}
				echo "<td class=\"center noborder\">";
					drawFlashMainPie($l['value'],$sub_count[$l['id']],"multi",$l['id']);
					drawPieLegend($sub_count[$l['id']]);
				echo "</td>";
			}
		}
		echo "</tr></table></td></tr>
		</table></div>
			<script type=text/javascript>
			$(function() {
				$(\"tr\").off('mouseenter mouseleave');
				//$('td').removeClass('noborder');
			});
			</script>		
		";
	
	
	
	
	}
		



} else {
	echo "<P>An error occurred in determining your selected display format.  Please go back and try again.</p>";
}


?>