<?php


$show_header = false;
if($src=="home") {
	$show_header = true;
}
$section = $_REQUEST['kpi'];
include("inc/header.php");


$moduleDisplayObject = new ASSIST_MODULE_DISPLAY();
$bool_button_js = "";

$obj_id = $_REQUEST['id'];
$time_id = $_REQUEST['time_id'];

			$myObj->logUser();
$section = $myObj->getSection($section);

$time = $timeObj->getTimeById($time_id);

$update_record = $myObj->getUpdateRecord($obj_id,$time_id);
$update_details = array();
$update_details['data'] = $myObj->getUpdateFormDetails($obj_id); //$object->getSectionHead();//
$a = $update_details['data']['results']['attach'][$time_id];
$update_record['attach_display'] = $myObj->getObjectAttachmentDisplay(true,$a);

if($section=="TOP") {
	$myObj->displayResult(array("error","Warning!  Updating this KPI will prevent any future automatic updates from the Departmental SDBIP for this Time Period."));
}

if($src=="home") {
	echo "<h1>Update ".($section=="KPI" ? "Departmental" : "Top Layer")." KPI</h1>";
}
 ?>

<div id=div_update title="" >
	<div id=div_update_form style='background-color: #ffffff; '>
		<form name=frm_update method=post action="controller/manage_table_update.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=section value='<?php echo $section; ?>' id=section />
		<input type=hidden name=action value='SAVE_UPD' id=action />
		<input type=hidden name=after_action value='' id=after_action />
		<input type=hidden name=obj_id value='<?php echo $obj_id; ?>' id=obj_id />
		<input type=hidden name=id value='<?php echo $obj_id; ?>' id=obj_id />
		<input type=hidden name=time_id value='<?php echo $time_id; ?>' id=time_id />
		<input type=hidden name=old_actual value='<?php echo $update_record[$r_table_fld.'actual']; ?>' id=old_actual />
		<input type=hidden name=old_target value='<?php echo $update_record[$r_table_fld.'target']; ?>' id=old_target />
		<input type=hidden name=calctype value='<?php echo $update_details['data']['calctype']; ?>' id=calctype />
		<h2>Update <?php echo $time['display']; ?></h2>
		<table class=form width=100%>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."target"]['h_client']; ?>:</th>
				<td id=target><?php echo $update_details['data']['results']['target'][$time_id]; ?></td>
			</tr><tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?>:</th>
				<td id=td_actual><?php echo $update_details['data']['target_formatting']['prefix']; ?><input type=text size=10 id=actual name="<?php echo $r_table_fld."actual"; ?>" value="<?php echo $update_record[$r_table_fld.'actual']; ?>" /><?php echo $update_details['data']['target_formatting']['postfix']; ?></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required)</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=perfcomm name="<?php echo $r_table_fld."perf"; ?>"><?php echo $update_record[$r_table_fld.'perf']; ?></textarea></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required if ".$mheadings[$r_section][$r_table_fld."actual"]['h_client']." does not meet ".$mheadings[$r_section][$r_table_fld."target"]['h_client'].")</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=correct name="<?php echo $r_table_fld."correct"; ?>"><?php echo $update_record[$r_table_fld.'correct']; ?></textarea></td>
			</tr><?php
			if($setup_defaults['POE']=="T" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>: <span class=float>
				<?php
				if(isset($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) && strlen($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) >0) {
					$me->displayIcon("info","id=poe_glossary");
				} ?></span>
				</th>
				<td ><textarea cols=50 rows=7 id=proof name="poe"><?php echo $update_record['attachment']['poe']; ?></textarea></td>
			</tr>
			<?php
			}	//if setup defaults allows for textfield
			if($setup_defaults['POE']=="A" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th>Attachment:</th>
				<td >
					<div id=attach_form><?php $me->displayAttachmentForm(); ?></div>
					<div id=div_update_attach_display><?php echo $update_record['attach_display']; ?></div>
				</td>
			</tr>
			<?php
			}	//if setup defaults allows for attachments
			?>
			<tr>
				<th>Is the update for this time period complete?</th>
				<td><?php
				$update_status = false;
				if($update_record[$r_table_fld.'update']==true) {
					$update_status = true;
				} else {
					$update_status = (isset($setup_defaults['STATUS']) ?  $setup_defaults['STATUS'] : true);
				}
				echo "
				<div style='border: 0px solid #009900;padding:5px;margin-bottom:5px;'>
				<div style='width: 250px' class=float>Note: Required comment fields will not be required until you select \"Yes\".</div>";
				$bool_button_js = $moduleDisplayObject->drawBoolButton(($update_status ?  1 : 0),array('id'=>$r_table_fld."update",'name'=>$r_table_fld."update"));
				echo "</div>";
					ASSIST_HELPER::displayResult(array("info","If you select \"No\", the KPI will remain on your action dashboard for further updates (for this time period).  If you select \"Yes\", the KPI will fall out of your action dashboard for this time period but will still be accessible within the SDBIP Module on the Manage > Update page."));

					?>
				</td>
			</tr>
			<tr>
				<th></th>
				<td><input type=button value="Save Update" class=isubmit /> <input type=reset /></td>
			</tr>
		</table>
		</form>
		<div style='margin-top:10px; float:right;'>
<?php
?>
		</div>
	</div>

<div id=div_details style='background-color: #ffffff; ' >
	<div id=div_update_contents style='background-color: #ffffff;'>
		<h2>Details</h2>
		<?php

			$kpi_head = $headObj->getHeadings($myObj->getHeadingCodes()); //arrPrint($kpi_head);
			$head = array_merge(array('dir'=>$kpi_head['dir']),$kpi_head[$myObj->getHeadCode()]);
			$rhead = $kpi_head[$myObj->getRHeadCode()];
			$kpi_time = $timeObj->getAllTime();

			$i = ($section=="KPI" ? DEPT::REFTAG : TOP::REFTAG).$obj_id;
			$kr = $update_details['data']; //$kpiObj->getUpdateFormDetails($k);
			$kr['display_id'] = $i;
			
			$drawObj->drawLiteKPI($myObj,$head,$rhead,$kpi_time,$kr,true);
		?>
		<?php if($section!="TOP") {

			$logs = $myObj->getAssuranceHistory($obj_id,$_REQUEST['time_id']);
			$log_display = $drawObj->getAssuranceHistoryHTML($logs);

			?>
			<h2>Assurance Review History</h2>
			<div id=div_assurance title=""><?php echo $log_display; ?></div>
		<?php } ?>

	</div>
</div>
	<div id=div_poe_glossary title="<?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>">
		<p><?php echo str_replace(chr(10),"<br />",ASSIST_HELPER::decode($mheadings[$r_section][$r_table_fld."attachment"]['glossary'])); ?></p>
	</div>
	<div id=dlg_result>
	</div>

</div>
<div id=dlg_msg title=Processing>
</div>
<div id=dlg_response title=Success>
<?php
echo str_replace(chr(10),"",$me->getDisplayResult(array("ok","Update processed successfully.")));
?>
</div>
<script type="text/javascript">


//	setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		var windowSize = getWindowSize();
		var dialogWidth = (windowSize['width']/2)-25;
		var centerVertical = windowSize['height']/2;
		var centerHorizontal = windowSize['width']/2;
		//var dialogWidth = windowSize['width']<850 ? windowSize['width']-50 : 800;
		//var dialogHeight = windowSize['height']<850 ? windowSize['height']-50 : 800;
		var section = "<?php echo $section; ?>";
		var use_plc = "<?php echo ($me->usePLC() ? "Y" : "N"); ?>";
		var require_comment = <?php echo $me->requireUpdateComments()==true ? "true" : "false"; ?>;
		var comment_length = <?php echo $me->requiredCommentLength(); ?>;
		var okIcon = "<?php echo ASSIST_HELPER::drawStatus(true); ?>";
		var timeInc = <?php echo $time_increment; ?>;
		var month1 = <?php echo $first_month; ?>;
		var processing_html = "<p class=center>Please wait while your form is processed...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p><p class=center>This may take some time depending on the size of the attachments added.</p>";
		var loading_html = "<p class=center>Please wait while your form loads...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p>";
		var redirecting_html = "<p class=center>Redirecting...</p>";
		var src = "<?php echo $src; ?>";

	$(function() {

<?php

echo $bool_button_js;

if($src=="home") {
	?>
	$("#div_update_form").css("width","49%").addClass("float");
	$("#div_details").css("width","49%");
	<?php
}
?>
		$("tr.no-highlight").unbind("mouseenter mouseleave");

		$("form[name=frm_update] input").keypress(function(e) {
			if(e.keyCode==13) {
				e.preventDefault();
			}
		});

		var attach_form = $("#attach_form").html();


		var loadingSize = {width:250,height:300};
		var dlgLoadingOpts = ({
			modal: true,
			closeOnEscape: false,
			autoOpen: false,
			width: loadingSize['width'],
			height: loadingSize['height'],
		});

		var resultSize = {width:300,height:200};
		var dlgResultsOpts = ({
			closeOnEscape: true,
			modal: true,
			autoOpen: false,
			width: resultSize['width'],
			height: resultSize['height'],
			buttons: [{ text: "Ok", click: function() {
					$("#dlg_response").dialog("close");
				}
			}],
			close: function() {
				if(src=="home") {
					$("#dlg_msg").html("<p>Redirecting...</p>").dialog('option','height',100).dialog("open");
					$('#backHome', window.parent.header.document).click();
				} else {
					window.parent.onSuccess($("#obj_id").val());
				}

			}

		});
		var errorSize = {width:300,height:320};
		var dlgErrorOpts = ({
			modal: true,
			autoOpen: false,
			width: errorSize['width'],
			height: errorSize['height'],
			buttons:[{
				text: "Ok",
				click: function() { $(this).dialog("close"); }
			}]
		});
		$("#dlg_msg").dialog({modal: true,autoOpen: false}).html(loading_html);
		$("#dlg_msg").dialog("option",dlgLoadingOpts);
		$("#dlg_msg").dialog("option","position",{ my: "center top", at: "center top", of: document });
		//$("#dlg_msg").dialog('option',dlgLoadingOpts).dialog("open");
		$("#dlg_response").dialog(dlgResultsOpts);
		$("#dlg_response").dialog("option","position",{ my: "center top", at: "center top", of: document });
		$(".ui-dialog-titlebar").hide();
		$("#div_poe_glossary").dialog({
			autoOpen: false,
			modal: true
		});
		$("#div_update #poe_glossary").css("cursor","pointer").click(function() {
			$("#div_poe_glossary").dialog("open").dialog("option","position",{ my: "center", at: "center", of: $("#div_update") });
		});
		$("#div_details table").not(".not-max").css("width","100%");
		$("#div_update input:button.cancel").click(function() {
			$("#div_update").dialog("close");
		});

			$("#attach_form").html(attach_form);
			var i = <?php echo $obj_id; ?>; //$(this).prop("id");
			var ti = <?php echo $time_id; ?>;//$("#div_update #time_id").val();
				$("li.hover").hover(
					function(){ $(this).addClass("trhover-dark"); },
					function(){ $(this).removeClass("trhover-dark"); }
				);

		$("#div_update input:button.isubmit").click(function() {
			$("#dlg_msg").html(processing_html).dialog('option',dlgLoadingOpts).dialog('option','buttons',[]).dialog("open");
			var err = "";
			var valid = true;
			var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9",".");
			//validate actual
			$("#div_update #actual").removeClass("required");
			var actual = $("#div_update #actual").val();
			if(actual.length > 0) {
				var period = 0;
				var a = actual.split("");
				for(x in a) {
					/* Replaced on 2016-11-13 by JC */
					//if($.inArray(a[x],validNumbers)<0) {
					//	valid = false;
					//	break;
					//}
					/* Added on 2016-11-13 by JC to deal with issue of user capturing an actual of "0.." - picked up from DB error log */
					if(a[x]==".") {	//Check how many periods are in the number and that they are not the first or last digit
						period++;
						if(period>1 || x == 0 || (x+1)==actual.length) {
							valid = false;
							break;
						}
					} else {
						if($.inArray(a[x],validNumbers)<0) {
							valid = false;
							break;
						}
					}
				}
			} else {
				valid = false;
			}
			if(!valid) {
				$("#div_update #actual").addClass("required");
				err+="<li><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?> (Only numbers are allowed)</li>";
			}
			//validate comments
				var t = $("#old_target").val();
				var ct = $("#calctype").val();

				var update_status = $("#<?php echo $r_table_fld; ?>update").val();

				//to catch any Microsoft special characters
				$("#perfcomm").val(AssistString.code($("#perfcomm").val()));
				$("#perfcomm").val(AssistString.decode($("#perfcomm").val()));
				$("#correct").val(AssistString.code($("#correct").val()));
				$("#correct").val(AssistString.decode($("#correct").val()));

				//validate comment fields if update_status is set to Complete (1)
				//console.log("Status:"+update_status);
				//console.log("Correct:"+$("#correct").val());
				//console.log("Correct length:"+$("#correct").val().length);
				//console.log("require_comment:"+require_comment);
				//console.log("comment_length:"+comment_length);


				if(update_status==true) {
					var fld = $("#perfcomm").prop("name");
					valid_perf = validateMe(fld,require_comment,comment_length,$("#perfcomm").val(),t,actual,ct);
					fld = $("#correct").prop("name");
					valid_correct = validateMe(fld,require_comment,comment_length,$("#correct").val(),t,actual,ct);
					if(!valid_perf && require_comment) {
						$("#div_update #perfcomm").addClass("required");
						err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?> is required (must exceed "+comment_length+" characters in length.)</li>";
					}
					if(!valid_correct && require_comment) {
						$("#div_update #correct").addClass("required");
						err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?> is required where the target has not been met for the period (must exceed "+comment_length+" characters in length.)</li>";
					}
				} else {
					valid_perf = true;
					valid_correct = true;
				}
			//if valid
			if(valid && valid_perf && valid_correct) {
				//save update
				var f = 0;
				$("#div_update input:file").each(function() {
					f+=$(this).val().length;
				});
				$form = $("form[name=frm_update]");
				$form.prop("target","file_upload_target");
						var post_action = "var okIcon = '"+okIcon.replace(/'/g,'"')+"'; 	";
						post_action+= "window.parent.$('#dlg_msg').dialog('close');	";
						post_action+= "window.parent.$('#dlg_response #spn_displayResultText').text('UPDATERESPONSEMSG');	";
						post_action+= "window.parent.$('#dlg_response').dialog('open'); ";
						$("form[name=frm_update] #after_action").val(post_action);
						$form.submit();
			//else not valid then display error message
			} else {
				//alert("DEV NOTE: DISPLAY ERROR MESSAGE HERE: "+err);
				$("#dlg_msg").html("<h1 class=red>Error</h1><p>Please complete the following required fields:</p><ul id=ul_req>"+err+"</ul>").dialog("option",dlgErrorOpts);
			}
		});

	});
	function showResponse(txt) {
		$(function() {
			$("#dlg_response #spn_displayResultText").text("Attachment deleted successfully.");
			$("#dlg_response").dialog("open");
		});
	}
	function showDialog(r,response) {
		$(function() {
				$('<div />',{id:'response_msg', html:response}).dialog({
					autoOpen	: true,
					modal 		: true,
					title 		: r,
					width 		: 430,
					height    	: 'auto',
					position	: [(centerHorizontal-(430/2)), (centerVertical-50)],
					buttons		: [{ text: 'Ok', click: function() {
										$('#response_msg').dialog('destroy');
										$('#response_msg').remove();
									}
								}],
					});
		});
	}
	<?php if($src!="home") { ?>
	$(function() {
		window.parent.openUpdateDialog();
	});
	<?php } ?>
</script>