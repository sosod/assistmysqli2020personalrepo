<?php 
$get_lists = true;
$get_active_lists = true;
$section = "TOP";
$page_title = "Edit";
$obj_id = $_REQUEST['id'];
include("inc/header.php"); 
$my_head = $mheadings[$section];
unset($my_head['top_repkpi']);
unset($my_head['top_risk']);
unset($my_head['top_riskratingid']);
unset($my_head['top_wards']);
unset($my_head['top_area']);
unset($my_head['top_baseline']);
unset($my_head['top_poe']);
unset($my_head['top_pyp']);
unset($my_head['top_annual']);
unset($my_head['top_revised']);
unset($my_head['top_mtas']);
unset($my_head['top_gfsid']);
unset($my_head['top_natoutcomeid']);
unset($my_head['top_munkpaid']);
unset($my_head['top_natkpaid']);
unset($my_head['top_idpid']);
unset($my_head['top_repcate']);
unset($my_head['top_pdoid']);
unset($my_head['top_ndpid']);
?>
<?php
/*** GET OBJECT DETAILS ***/

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "TEXTLIST":
			$fields[] = "t.".$fld." as o".$fld;
			$fields[] = "t.".$fld." as ".$fld;
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			$fields[] = "t.".$fld." as o".$fld;
			break;
		default: $fields[] = "t.".$fld;
	}
}

$object_sql = "SELECT targettype.id as tt, calctype.code as ct, ".implode(", ",$fields)."
FROM ".$dbref."_top t
INNER JOIN ".$dbref."_dir as dir ON dir.id = t.top_dirid";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="dir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " LEFT OUTER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE t.top_id = ".$obj_id;
//echo "<P>".$object_sql;
$object = mysql_fetch_all($object_sql);
$object = $object[0];


if(isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
	case "ADD":
		if(isset($_REQUEST['kpi_id']) && checkIntRef($_REQUEST['kpi_id'])) {
			$sql = "UPDATE ".$dbref."_kpi SET kpi_topid = ".$obj_id." WHERE kpi_id = ".$_REQUEST['kpi_id'];
			$mar = db_update($sql);
			$result = array("ok","Departmental KPI ".$id_labels_all['KPI'].$_REQUEST['kpi_id']." has been successfully associated with Top Layer KPI ".$id_label.$obj_id.".");
			$v = array(
				'fld'=>"kpi_topid",
				'timeid'=>0,
				'text'=>"Associated Departmental KPI ".$id_labels_all['KPI'].$_REQUEST['kpi_id']." with Top Layer KPI ".$id_label.$obj_id.".",
				'old'=>0,
				'new'=>$obj_id,
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sql));
			logChanges("KPI",$_REQUEST['kpi_id'],$v,code($sql));
		} else {
			$result = array("error","An error occurred.  Please try again.");
		}
		break;
	case "DEL":
		if(isset($_REQUEST['kpi_id']) && checkIntRef($_REQUEST['kpi_id'])) {
			$sql = "UPDATE ".$dbref."_kpi SET kpi_topid = 0 WHERE kpi_id = ".$_REQUEST['kpi_id'];
			$mar = db_update($sql);
			$result = array("ok","Departmental KPI ".$id_labels_all['KPI'].$_REQUEST['kpi_id']." has been successfully removed from Top Layer KPI ".$id_label.$obj_id.".");
			$v = array(
				'fld'=>"kpi_topid",
				'timeid'=>0,
				'text'=>"Removed link between Departmental KPI ".$id_labels_all['KPI'].$_REQUEST['kpi_id']." and Top Layer KPI ".$id_label.$obj_id.".",
				'old'=>0,
				'new'=>$obj_id,
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sql));
			logChanges("KPI",$_REQUEST['kpi_id'],$v,code($sql));
		} else {
			$result = array("error","An error occurred.  Please try again.");
		}
		break;
	}
}

if(isset($result)) { displayResult($result); }
?>
<table class=noborder>
	<tr class=no-highlight>
		<td class=noborder>
<h2>Details</h2>
<table id=tbl width=750>
	<tr>
		<th class=left>Reference</th>
		<td><?php echo (isset($id_label)?$id_label:"").$obj_id; ?><input type=hidden name=id id=obj_id value=<?php echo $obj_id; ?> /></td>
	</tr>
<?php 
foreach($my_head as $fld => $h) {
	echo chr(10)."<tr>";
		echo "<th class=\"left top\">".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;</th>";
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "TEXTLIST":
			break;
		case "LIST":
			echo "<td>".$object[$fld]."</td>";
			break;
		case "BOOL":
			echo "<td>".$object[$fld]."</td>";
			break;
		default:
			switch($fld) {
				case "top_annual":
				case "top_revised":
					break;
				default:
					echo "<td>".ASSIST_HELPER::decode($object[$fld])."</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
 ?>
</table>
<h2>Associated Departmental KPIs</h2>
<form action=<?php echo $self; ?> method=post>
<input type=hidden value=<?php echo $obj_id; ?> name=id />
<input type=hidden value=<?php echo $page_id; ?> name=page_id />
<input type=hidden value=ADD name=action />
<?php
	$kpi_sql = "SELECT kpi_value, kpi_id, dir.id as dir FROM ".$dbref."_kpi 
				INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id 
				INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id
				WHERE kpi_topid = 0 AND kpi_active = true";
	$kpis = mysql_fetch_alls2($kpi_sql,"dir","kpi_id");
	
	$dept_sql = "SELECT kpi_value, kpi_id, kpi_subid, dir.value as dir FROM ".$dbref."_kpi 
				INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id 
				INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id
				WHERE kpi_topid = ".$obj_id." AND kpi_active = true";
	$dept_kpis = mysql_fetch_alls($dept_sql,"kpi_id");
	echo "<table>
			<tr>
				<th>Ref</th>
				<th>KPI</th>
				<th></th>
			</tr>";
	foreach($dept_kpis as $k_id => $k) {
		echo "<tr>
				<th>".$id_labels_all['KPI'].$k_id."</th>
				<td>".$k['dir']." - ".$k['kpi_value']."</td>
				<td><input type=button value=\"Remove link\" class=\"idelete\" id=".$k_id." /></td>
			</tr>";
	}
		echo "<tr>
				<th></th>
				<td class=middle><select id=dir>";
				foreach($lists['dir'] as $i => $l) {
					echo "<option ".($i==$object['otop_dirid'] ? "selected" : "")." value=$i>".$l['value']."</option>";
				}
		echo "		</select>&nbsp;<img src=/pics/tri_right.gif  class=middle>&nbsp;<select name=kpi_id id=kpis></select></td>
				<td class=middle><input type=submit value=\"Add link\" class=\"isubmit\" id=addkpi style=\"padding-left: 22px;padding-right: 21px\" /></td>
			</tr>";
	echo "</table>";
?>
</form>
</td></tr></table>
<script type=text/javascript>
$(document).ready(function() {
	var kpis = new Array();
	var kv = "";
	<?php
	foreach($kpis as $d => $k) {
		echo chr(10)."kpis[$d] = new Array();";
		$a = 0;
		$max = 47;
		foreach($k as $i => $l) {
			$kv = ASSIST_HELPER::decode($l['kpi_value']);
			//$kv = nl2br($kv);
			$kv = str_replace(chr(10)," ",$kv);
			$kv = str_replace(chr(13)," ",$kv);
			//$kv = str_replace("\n","<br />",$kv);
			//$kv = str_replace("\r\n"," ",$kv);
			if(strlen($kv)>$max) {
				$kv = substr($kv,0,$max-3)."...";
			}
			echo chr(10)."kv = '".addslashes($kv)."';
							kpis[$d][$a] = ['".$i."',kv];";
			$a++;
		}
	}
	?>
	$("#dir").change(function() {
		v = $(this).val();
		k = kpis[v];
		$("#kpis option").remove();
		for(x in k) {
			$("#kpis").append("<option value="+k[x][0]+">[<?php echo $id_labels_all['KPI']; ?>"+k[x][0]+"] "+k[x][1]+"</option>");
		}
	});
	$("#dir").trigger("change");
	$(".idelete").click(function() {
		i = $(this).attr("id");
		if(confirm("Are you sure you wish to remove the link between Top Layer KPI <?php echo $id_label.$obj_id; ?> and Departmental KPI <?php echo $id_labels_all['KPI']; ?>"+i+"?")) {
			document.location.href = '<?php echo $self."?page_id=".$page_id."&id=".$obj_id; ?>&action=DEL&kpi_id='+i;
		}
	});
});
</script>
<?php
displayGoBack($base[0]."_".$base[1].".php?page_id=edit&filter_who=D_".$object['otop_dirid'],"Go Back");
?>
<p>&nbsp;</p>
<?php

	$log_sql = "SELECT tlog_date, CONCAT_WS(' ',tkname,tksurname) as tlog_tkname, tlog_transaction FROM ".$dbref."_top_log 
				INNER JOIN assist_".$cmpcode."_timekeep ON tlog_tkid = tkid WHERE tlog_topid = '".$obj_id."' AND tlog_yn = 'Y' ORDER BY tlog_id DESC";
	displayAuditLog($log_sql,array('date'=>"tlog_date",'user'=>"tlog_tkname",'action'=>"tlog_transaction"));

?>
</body></html>