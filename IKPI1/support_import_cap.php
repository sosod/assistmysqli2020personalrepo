<?php 
$page_title = "Capital Projects";
$import_section = "CAP";
$head_section = "CAP";
$get_headings = array("CAP","CAP_R");
include("support_import_headings.php");
include("inc/header.php"); 
//arrPrint($mheadings);
set_time_limit(1800);

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > ".$page_title, $self);


//GET ALL LIST VALUES
$listoflists = array(
	'dir'=>				array('name'=>"Directorate",'data'=>array()),
	'subdir'=>			array('name'=>"Sub-Directorate",'data'=>array()),
	'list_gfs'=>		array('name'=>"GFS Classification",'data'=>array()),
	/*'list_munkpa'=>		array('name'=>"Municipal KPA",'data'=>array()),
	'list_natkpa'=>		array('name'=>"National KPA",'data'=>array()),
	'list_natoutcome'=>	array('name'=>"National Outcome",'data'=>array()),
	'list_idp_kpi'=>	array('name'=>"Departmental SDBIP: IDP Objective",'data'=>array()),
	'list_idp_top'=>	array('name'=>"Top Layer: IDP Objective",'data'=>array()),*/
	'list_area'=>		array('name'=>"Area",'data'=>array()),
	'list_wards'=>		array('name'=>"Ward",'data'=>array()),
	'list_fundsource'=>	array('name'=>"Funding Source",'data'=>array())
);
$wa = array(
	'capital_wards'=>array(),
	'capital_area'=>array(),
	'capital_fundsource'=>array()
);
foreach($listoflists as $tbl => $l) {
	$rs = getRS("SELECT * FROM ".$dbref."_".$tbl." WHERE active = true");
	while($row = mysql_fetch_assoc($rs)) {
		if($tbl!="list_calctype") {
			$listoflists[$tbl]['data'][$row['id']] = $row;
		} else {
			$listoflists[$tbl]['data'][$row['code']] = $row;
		}
		switch($tbl) {
			case "list_owner": $owner[strtolower(decode($row['value']))] = $row['id']; break;
			case "list_wards": 
				if(strtolower($row['value'])=="all") {
					$wa['capital_wards']['ALL'] = $row['id'];
				} else {
					$wa['capital_wards'][$row['code']] = $row['id'];
				}
				break;
			case "list_area":	$wa['capital_area'][$row['id']] = $row['id']; break;
			case "list_fundsource":	$wa['capital_fundsource'][$row['id']] = $row['id']; break;
		} 
	}
}
//arrPrint($listoflists['list_area']);
//arrPrint($wa['area']);
//arrPrint($listoflists['list_wards']);
//arrPrint($wa['wards']);
function valError($v,$e) {
	return $v."<br /><b>-> ".$e."</b>";
}
function valList($v,$e) {
	return $v."<br />=> ".$e;
}

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_capital";
		$r_sql[] = "TRUNCATE ".$dbref."_capital_results";
		$r_sql[] = "TRUNCATE ".$dbref."_capital_wards";
		$r_sql[] = "TRUNCATE ".$dbref."_capital_area";
		$r_sql[] = "TRUNCATE ".$dbref."_capital_forecast";
		$r_sql[] = "TRUNCATE ".$dbref."_capital_fundsource";
		foreach($r_sql as $sql) {	
			//$mnr = db_update($sql);
			$rs = getRS($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '".$import_section."'");
		$import_status[$import_section] = false;
		$result = array("ok","All $page_title tables successfully reset.");
		logImport($import_log_file, $page_title." > Reset tables", $self);
		break;
}

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		.ireset {
			background-color: #CC0001;
			color: #ffffff;
			line-height: 1.5em;
		}
		.dark-info {
			background: #FE9900 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		.dark-error {
			background: #CC0001 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		th.ignite { background-color: #555555; }
		th.doc { background-color: #999999; }
		</style>
<?php
logImport($import_log_file, $page_title." > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
$onscreen = "";
switch($act) {
	case "VIEW": case "RESET": case "GENERATE":
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).prop("id");
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=<?php echo $import_section; ?>'; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=RESET'; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").prop("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
			?>
			<form id=list action="<?php echo $self; ?>" method=post enctype="multipart/form-data">
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to the Lists sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information.&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php if(!$import_status[$import_section]) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Select the CSV file and click the Import button. &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php } ?>
				</table>
			</form>
			<?php displayGoBack("support_import.php"); 
			
			drawCSVColumnsGuidelines("cap_id",$target_text);
			
		break;	//switch act case view/reset/generate
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>$page_title Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
		$onscreen.= "<p class=centre>Any errors highlighted in <span class=dark-error>red</span> are errors that prevent the import (fatal errors).<br />The \"Accept\" button will not be available until all of these errors have been corrected.</p>";
		$onscreen.= "<p class=centre>Any cells highlighted in <span class=dark-info>orange</span> are where the value associated with the given list reference does not match the value given in the next cell.<br />These errors do not prevent the import (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Please remember, if you get any \"Too long\" errors, that any non-alphanumeric characters in text fields are converted a special code to allow them to be uploaded to the database.
		<br />This code is generally 6 characters long and counts towards the overall length of the text field.</p>
		<p class=center><span class=iinform>Please note:</span> The top heading row displays the headings from the module as per Setup > Defaults > Headings.
		<br />The second heading row displays the headings from the import file.
		<br />Please ensure that they match up to ensure that the correct data is being imported into the correct field.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			echo "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
		}
		$onscreen.= "<form id=save method=post action=\"$self\">";
		$onscreen.= "<input type=hidden name=act value=ACCEPT />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, $page_title." > ERROR > $file doesn't exist", $self);
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, $page_title." > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, $page_title." > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value=".(isset($filen) ? $filen : $_REQUEST['f'])." />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);

			if(count($data)>2) {
				$doc_headings = $data[0];
				$doc_headings2 = $data[1];
				unset($data[0]); unset($data[1]);
				$columns[0] = array("Ignite","Sub-Directorate [R]","","","GFS Classification [R]","","Mun CP Ref","IDP Number","Vote Number","Project name [R]","Project Description","Funding source [R]","Planned Start Date [R]","Planned Completion Date [R]","Actual Start Date","Ward [R]","Area [R]","&nbsp;","July 2011","August 2011","September 2011","October 2011","November 2011","December 2011","January 2012","February 2012","March 2012","April 2012","May 2012","June 2012","Total","2011/2012","","2012/2013","","2013/2014","","2014/2015","","2015/2016","");
				$columns[1] = array("Ref","Ignite","Directorate","List","Ignite","List","40 characters","40 characters","40 characters","200 characters","65000 characters","Ignite ref ;","YYYY/MM/DD","YYYY/MM/DD","YYYY/MM/DD","Mun. Ref separated ;","Ignite ref separated by ;","&nbsp;","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","&nbsp;","CRR","Other","CRR","Other","CRR","Other","CRR","Other","CRR","Other");
				//$columns[0] = array("Ignite","Directorate","","PMS Ref","Reporting Requirement","GFS Classification","","National Outcome","","National KPA","","MTAS Indicator","IDP Objective","","Municipal KPA","","KPI","Unit of Measurement","Risk","Wards","Area","Program Driver","Baseline","POE","Past Year Performance","KPI Calculation Type","","KPI Target Type","","Annual Target","Revised Target","","Q1","Q2","Q3","Q4","","2012/2013","2013/2014","2014/2015","2015/2016");
				//$columns[1] = array("Ref","Dir Ref","Dir Name","40 characters","Y / N","GFS Ref","GFS Value","Ref","Value","Ref","Value","200 characters","Ref","Value","Ref","Value","200 characters","200 characters","200 characters","Mun Ref ;","Ignite Ref ;","Value","200 characters","200 characters","100 characters","Code","Value","Ref","Value","Number","Number","","Number","Number","Number","Number","","Number","Number","Number","Number");
				$colspanned[2] = array(4);
				$colspanned[3] = array(1);
				/*$cells = array(
					0=>'cap_id',
					1=>'cap_subid',
					4=>'cap_gfsid',
					6=>'cap_cpref',
					7=>'cap_idpref',
					8=>'cap_vote',
					9=>'cap_name',
					10=>'cap_descrip',
					11=>'fundsource',
					12=>'cap_planstart',
					13=>'cap_planend',
					14=>'cap_actualstart',
					15=>'wards',
					16=>'area',
					18=>'target_1',
					19=>'target_2',
					20=>'target_3',
					21=>'target_4',
					22=>'target_5',
					23=>'target_6',
					24=>'target_7',
					25=>'target_8',
					26=>'target_9',
					27=>'target_10',
					28=>'target_11',
					29=>'target_12',
					31=>'forecast_1_1',
					33=>'forecast_2_1',
					35=>'forecast_3_1',
					37=>'forecast_4_1',
					39=>'forecast_5_1',
					32=>'forecast_1_2',
					34=>'forecast_2_2',
					36=>'forecast_3_2',
					38=>'forecast_4_2',
					40=>'forecast_5_2'
				);*/
				$listinfo = array(
					'cap_subid' 		=> array('tbl' => "subdir", 'other' => 3, 'dir'=>2),
					'cap_gfsid' 		=> array('tbl' => "list_gfs", 'other' => 5),
					'capital_wards'				=> array('tbl' => "list_wards"),
					'capital_area'				=> array('tbl' => "list_area"),
					'capital_fundsource'		=> array('tbl' => "list_fundsource")
				);
				
				$onscreen.="<table>";
					/*$onscreen.="<tr><th>?</td>";
						foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1) ? 3 : $colspan;
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan >".$c."</th>";
							}
						}
					$onscreen.="</tr>";
					$onscreen.="<tr><th>?</td>";
						foreach($columns[1] as $cell => $c) {
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan >".$c."</th>";
							}
						}*/
					//$onscreen.= "<tr><th rowspan=2 style='vertical-align: middle;'> ? </td>";
						/*foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1) ? 3 : $colspan;
							if(strlen($c)>0 || $cell>30) {
								$onscreen.= "<th colspan=$colspan class=ignite >".$c."</th>";
							}
						}*/
						$onscreen.=getImportHeadings();
					/*$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1) ? 3 : $colspan;
							if(strlen($c)>0 || $cell>30) {
								$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
							}
						}
					$onscreen.="</tr>";*/
					//arrPrint($cells);
					$t_sql = array();
					$r_sql = array();
					$f_sql = array();
					$refs = array();
					$values = array();
					$all_valid = true;
					$error_count = 0;
					$info_count = 0;
					foreach($data as $i => $d) {
						$targettype = 0;
						$valid = true;
						$row = array();
						$display = array();
						$val = array();
						foreach($cells as $c => $fld) {
							$v = $d[$c];
							$val[$c][0] = 1;
							//DATA VALIDATION HERE
							switch($fld) {
								case "cap_id":		//IGNITE REF
									$v = $v*1;
									if(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(in_array($v,$refs)) {	//duplicate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Duplicate reference");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = $v;
										$refs[] = $v;
									}
									break;
								case "cap_subid":		//DIRECTORATE
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$dir = $listinfo[$fld]['dir'];

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$val[$dir] = array(1,$d[$dir]);
									}
									break;
								case "cap_gfsid":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
									}
									break;
								case "cap_cpref":		//PMS REF
								case "cap_idpref":
								case "cap_vote":
								case "cap_name":
								case "cap_descrip":
									$len = $mheadings[$head_section][$fld]['c_maxlen'];
									$type = $mheadings[$head_section][$fld]['h_type'];
									if(($mheadings[$head_section][$fld]['i_required']+$mheadings[$head_section][$fld]['c_required'])>0 && strlen(trim(code($v))) == 0) {			//required field
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(strlen(trim(code($v)))> $len && $type!="TEXT") {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim(code($v)))." chars (max ".$len." chars)".$fld);
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = trim(code($v));
									}
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
									$v = trim($v);
									if($fld!="cap_actualstart" && strlen($v)==0) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif($fld=="cap_actualstart" && strlen($v)==0) {
										$val[$c][1] = "";
										$row[$fld] = 0;
									} else {
										if(strpos($v,"/")!==false) {
											$v2 = explode("/",$v);
										} elseif(strpos($v,"-")!==false) {
											$v2 = explode("-",$v);
										} else {
											$v2=array();
										}
										if(count($v2)!=3) {
											$valid = 0; $error_count++;
											$val[$c][0] = 0;
											$val[$c][1] = valError($v,"Invalid date format - YYYY/MM/DD");
										} elseif(strlen($v2[0])!=4 || !checkIntRef($v2[0]) || strlen($v2[1]) <1 || strlen($v2[1]) > 2 || !checkIntRef($v2[1]) || strlen($v2[2]) <1 || strlen($v2[2]) > 2 || !checkIntRef($v2[2]) ) {
											$valid = 0; $error_count++;
											$val[$c][0] = 0;
											$val[$c][1] = valError($v,"Invalid date format - YYYY/MM/DD");
										} else {
											$v2[0]*=1;
											$v2[1]*=1;
											$v2[2]*=1;
											if($v2[1]<1 || $v2[1]>12) {
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
												$val[$c][1] = valError($v,"Invalid month");
											} elseif($v2[2] < 1 || ( in_array($v2[1],array(1,3,5,7,8,10,12)) && $v2[2] > 31 ) || ($v2[1]==2 && $v2[2] > 29) || (in_array($v2[1],array(4,6,9,11)) && $v2[2] > 30) ) {
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
												$val[$c][1] = valError($v,"Invalid day");
											} else {
												if($fld=="cap_planend") {
													$mkt = mktime(23,59,59,$v2[1],$v2[2],$v2[0]);
												} else {
													$mkt = mktime(0,0,0,$v2[1],$v2[2],$v2[0]);
												}
												if($mkt==0) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1] = valError(valList($v,date("d M Y",$mkt)),"There is an error on the date, the system does not recognise it.");
												} elseif($fld=="cap_planend" && $row['cap_planstart']>$mkt) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1] = valError(valList($v,date("d M Y",$mkt)),"Start cannot be later than completion date.");
												} else {
													$val[$c][1] = valList($v,date("d M Y",$mkt));
													$row[$fld] = $mkt;
												}
											}
										}
									}
									break;
								case "target_1":
								case "target_2":
								case "target_3":
								case "target_4":
								case "target_5":
								case "target_6":
								case "target_7":
								case "target_8":
								case "target_9":
								case "target_10":
								case "target_11":
								case "target_12":
								case "forecast_1_1":
								case "forecast_2_1":
								case "forecast_3_1":
								case "forecast_4_1":
								case "forecast_5_1":
								case "forecast_6_1":
								case "forecast_1_2":
								case "forecast_2_2":
								case "forecast_3_2":
								case "forecast_4_2":
								case "forecast_5_2":
								case "forecast_6_2":
									$v = trim($v);
									if(!is_numeric($v) && strlen($v)>0) {	//invlid number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data: number only");
									} else {
										if(strlen($v)==0) {
											$v = 0;
										}
										$row[$fld] = $v;
										$val[$c][1] = $v;
										if($targettype==2 && $v <=1 && $v>0) {
											$val[$c][0]=2; $info_count++;
										}
									}
									break;
								case "capital_wards":
								case "capital_area":
								case "capital_fundsource":
									if(strlen(trim($v))==0) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} else {
										$tbl = $listinfo[$fld]['tbl'];
										$row[$fld] = array();
										$val[$c][1] = "";
										$v = strtoupper($v);
										$va = explode(";",$v);
										if($fld=="capital_wards" && in_array("ALL",$va)) {
											if(isset($wa[$fld]['ALL'])) {
												$row[$fld][] = $wa[$fld]['ALL'];
												$val[$c][1] = "All";
											} else {
												$val[$c][1] = valError("All","Not found");
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
											}
										} else {
											foreach($va as $a) {
												$a = trim($a);
												if(!checkIntRef($a)) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid reference - number only");
												} elseif(!isset($a,$wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Reference not found");
												} elseif(!checkIntRef($wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid Ignite Reference found");
												} else {
													$row[$fld][] = $wa[$fld][$a];
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").$a." => ".$listoflists[$tbl]['data'][$wa[$fld][$a]]['value'];
												}
											}
										}
									}
									break;
								default:
									$val[$c][1] = $v;
									//$row[$fld] = $v;
							}
						}
						$val['a'] = $valid;
						$onscreen.= "<tr>";
							switch($val['a']) {
								case true:
									$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									break;
								case false:
								default:
									$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
							}
							foreach($columns[1] as $cell => $c) {
								if(isset($val[$cell])) {
									$v = $val[$cell]; //echo $cell; arrPrint($v);
									switch($v[0]) {
										case 0:
											$onscreen.= "<td class=\"dark-error\">";
											break;
										case 2:
											$onscreen.= "<td class=\"dark-info\">";
											break;
										case 1:
										default:
											$onscreen.= "<td>";
									}
									$onscreen.= $val[$cell][1];
									$onscreen.= "</td>";
								} else {
									$onscreen.= "<td></td>";
								}
							}
						$onscreen.= "</tr>";
						if($valid) {
							$values[$i] = $val;
							//INSERT INTO ".$dbref."_capital (cap_id, cap_subid, 'cap_cpref', 'cap_idpref', 'cap_vote', 'cap_name', 'cap_descrip', cap_planstart, cap_planend, cap_actualstart, cap_actualend, cap_active) VALUES ...
							$t_sql[] = "(".$row['cap_id'].", ".$row['cap_subid'].", ".$row['cap_gfsid'].", '".$row['cap_cpref']."', '".$row['cap_idpref']."', '".$row['cap_vote']."', '".$row['cap_name']."', '".$row['cap_descrip']."', ".$row['cap_planstart'].", ".$row['cap_planend'].", ".$row['cap_actualstart'].", 0, true)";
							//RESULTS
							$tot_target = 0; $ytd_target = array("",0,0,0,0,0,0,0,0,0,0,0,0);
							for($z=1;$z<=12;$z++) { 
								$tot_target+= $row['target_'.$z]; 
								$ytd_target[$z] = $tot_target;
							}
							$r_sql[] = "INSERT INTO ".$dbref."_capital_results (cr_id, cr_capid, cr_timeid, cr_target, cr_ytd_target, cr_tot_target) VALUES 
										 (null,".$row['cap_id'].",1, ".$row['target_1'].", ".$ytd_target[1].", ".$tot_target."), 
										 (null,".$row['cap_id'].",2, ".$row['target_2'].", ".$ytd_target[2].", ".$tot_target."), 
										 (null,".$row['cap_id'].",3, ".$row['target_3'].", ".$ytd_target[3].", ".$tot_target."), 
										 (null,".$row['cap_id'].",4, ".$row['target_4'].", ".$ytd_target[4].", ".$tot_target."), 
										 (null,".$row['cap_id'].",5, ".$row['target_5'].", ".$ytd_target[5].", ".$tot_target."), 
										 (null,".$row['cap_id'].",6, ".$row['target_6'].", ".$ytd_target[6].", ".$tot_target."), 
										 (null,".$row['cap_id'].",7, ".$row['target_7'].", ".$ytd_target[7].", ".$tot_target."), 
										 (null,".$row['cap_id'].",8, ".$row['target_8'].", ".$ytd_target[8].", ".$tot_target."), 
										 (null,".$row['cap_id'].",9, ".$row['target_9'].", ".$ytd_target[9].", ".$tot_target."), 
										(null,".$row['cap_id'].",10,".$row['target_10'].", ".$ytd_target[10].", ".$tot_target."), 
										(null,".$row['cap_id'].",11,".$row['target_11'].", ".$ytd_target[11].", ".$tot_target."), 
										(null,".$row['cap_id'].",12,".$row['target_12'].", ".$ytd_target[12].", ".$tot_target.")";
							//FORECAST YEARS
							$r_sql[] = "INSERT INTO ".$dbref."_capital_forecast (cf_id, cf_capid, cf_listid, cf_active, cf_crr, cf_other) VALUES 
										(null,".$row['cap_id'].",1,true,".$row['forecast_1_1'].",".$row['forecast_1_2'].")
										, (null,".$row['cap_id'].",3,true,".$row['forecast_3_1'].",".$row['forecast_3_2'].")
										, (null,".$row['cap_id'].",4,true,".$row['forecast_4_1'].",".$row['forecast_4_2'].")
										, (null,".$row['cap_id'].",5,true,".$row['forecast_5_1'].",".$row['forecast_5_2'].")
										, (null,".$row['cap_id'].",6,true,".$row['forecast_6_1'].",".$row['forecast_6_2'].")
										";
							//WARDS
							$r_sql[] = "INSERT INTO ".$dbref."_capital_wards (cw_id, cw_capid, cw_listid, cw_active) VALUES	
							(null,".$row['cap_id'].",".implode(",true),(null,".$row['cap_id'].",",$row['capital_wards']).",true)";
							//AREA
							$r_sql[] = "INSERT INTO ".$dbref."_capital_area (ca_id, ca_capid, ca_listid, ca_active) VALUES	
							(null,".$row['cap_id'].",".implode(",true),(null,".$row['cap_id'].",",$row['capital_area']).",true)";
							//FUNDSOURCE
							$r_sql[] = "INSERT INTO ".$dbref."_capital_fundsource (cs_id, cs_capid, cs_listid, cs_active) VALUES	
							(null,".$row['cap_id'].",".implode(",true),(null,".$row['cap_id'].",",$row['capital_fundsource']).",true)";
						} else {
							$all_valid = false;
						}
						//arrPrint($row);
					}
					
					
				$onscreen.="</table>";
				
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre>There are $error_count fatal error(s).<br />There are $info_count non-fatal error(s).</p>";
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit id=submit_me /> <input type=button id=reset_me class=idelete value=Reject /><p>";
					if(!$all_valid || $error_count > 0) {
						$onscreen.= "<script>$(function() {
							$(\"#submit_me\").prop(\"disabled\",true);
							$(\"#submit_me\").removeClass(\"isubmit\");
							$('#reset_me').click(function() { document.location.href = 'support_import_cap.php'; });
						});</script>";
					}
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
				} else {
					$z_sql = array();
					$z=0;
					$z_sql[$z] = "INSERT INTO ".$dbref."_capital (cap_id, cap_subid, cap_gfsid, cap_cpref, cap_idpref, cap_vote, cap_name, cap_descrip, cap_planstart, cap_planend, cap_actualstart, cap_actualend, cap_active) VALUES ";
					$a = 0;
					foreach($t_sql as $t) {
						if($a>=50) {
							$z++;
							$z_sql[$z] = "INSERT INTO ".$dbref."_capital (cap_id, cap_subid, cap_gfsid, cap_cpref, cap_idpref, cap_vote, cap_name, cap_descrip, cap_planstart, cap_planend, cap_actualstart, cap_actualend, cap_active) VALUES ";
							$a=0;
						} elseif($a>0) {
							$z_sql[$z].=",";
						}
						$z_sql[$z].=$t;
						$a++;
					}
					foreach($z_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
						//echo "<P>".$id;
					}
					foreach($r_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
						//echo "<P>".$id;
					}
					db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
					echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=Capital+Projects+imported.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, $page_title." > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
		break;	//switch act case accept
}	//switch act
?>

</body>
</html>