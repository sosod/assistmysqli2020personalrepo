<?php 
$page_type = isset($_REQUEST['t']) && strtoupper($_REQUEST['t'])=="TOP" ? "TOP" : "DEPT";
$page_title = ($page_type=="TOP" ? "Top Layer" : "Departmental")." Administrators";
$log_title = ($page_type=="TOP" ? "Top Layer" : "Departmental")." administrator";
$log_section = $page_type!="TOP" ? "ADMIN" : "TADM";
$section = $page_type!="TOP" ? "ADMIN" : "TOP_ADMIN";
include("inc/header.php"); 

if($page_type=="TOP") {
	switch($page_id) {
	case "owner":	$type = "T_OWN"; break;
	case "dir":
	default:		$type = "TOP";
	}
	$report_types = array('dir'=>"TOP",'own'=>"T_OWN");
} else {
	switch($page_id) {
	case "subdir":	$type = "SUB"; break;
	case "owner":	$type = "OWN"; break;
	case "dir":
	default:		$type = "DIR";
	}
	$report_types = array('dir'=>"DIR",'sub'=>"SUB",'own'=>"OWN");
}

include("setup_defaults_admins_".$page_id.".php");
?>
</body>
</html>