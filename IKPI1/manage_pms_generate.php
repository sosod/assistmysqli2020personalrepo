<?php
$onscreen_header = "false";
$section = "KPI";
$get_lists = true;
$get_active_lists = false;
$get_open_time = false;
include("inc/header.php");
//arrPrint($_REQUEST);

$head = $mheadings['KPI'];

ini_set('max_execution_time',1800); 

/*** GET REQUEST ***/
$fields = array();
if($_REQUEST['format']=="agreement") {
	$order = array('kpi_natkpaid'=>1,'kpi_idpid'=>2,'kpi_value'=>3,'kpi_unit'=>4,'kpi_baseline'=>5,'kpi_perfstd'=>6);
	$export_head = array('kpi_natkpaid'=>"National KPA",'kpi_idpid'=>"IDP Objective",'kpi_value'=>"KPI Name",'kpi_unit'=>"KPI Definition",'kpi_baseline'=>"Baseline",'kpi_perfstd'=>"Performance Standard");
	foreach($order as $i=>$l) { $fields[] = $i; }
} else {
	$order = array('kpi_value'=>1,'kpi_unit'=>2,'kpi_perfstd'=>3,'kpi_outcome'=>4,'kpi_poe'=>5);
	$export_head = array('kpi_value'=>"KPI Name",'kpi_unit'=>"KPI Definition",'kpi_perfstd'=>"Performance Standard",'kpi_outcome'=>"Outcome",'kpi_poe'=>"POE");
	foreach($order as $i=>$l) { $fields[] = $i; }
}
$r_calc = "ytd";
$krsum = "N";
$filter = array();
$filter[$_REQUEST['groupby']][0] = $_REQUEST[$_REQUEST['groupby']];
$groupby = "X";
$output = "csv";
$report_time = array(1,12);

/* GROUP BY */
$group = array();
if($groupby != "X") {
} else {
	$group["oX"] = array('id'=>"oX",'value'=>"",'objects'=>array());
}

/* OBJECT SQL */
$sql_fields = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "LIST":
			if($fld=="kpi_natkpaid") {
				$sql_fields[] = $h['h_table'].".code as ".$fld;
			} else {
				$sql_fields[] = $h['h_table'].".value as ".$fld;
			}
			if($fld!="dir") {
				$sql_fields[] = "o.".$fld." as o".$fld;
			} else {
				$sql_fields[] = "dir.id as odir";
			}
			break;
		default: $sql_fields[] = "o.".$fld;
	}
}

$object_sqla[0] = "SELECT o.kpi_id as obj_id, ".implode(", ",$sql_fields);
$object_sqla[1] = "FROM ".$dbref."_kpi o
					INNER JOIN ".$dbref."_subdir as subdir ON o.kpi_subid = subdir.id AND subdir.active = true
					INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id AND dir.active = true";
					foreach($mheadings[$section] as $fld => $h) {
						if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
							$a = $h['h_table']=="calctype" ? "code" : "id";
							$object_sqla[1].= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
						}
					}
$object_sqla[1].= " WHERE o.kpi_active = true";
$my_sort = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "TOP":
		case "CAP":
		case "BOOL":
			break;
		case "LIST":
			if($fld==$_REQUEST['groupby']) {
				$object_sqla[1].=" AND ".$h['h_table'].".id IN (".implode(",",$filter[$fld][0]).") ";
			}
			break;
		default:
			break;
	}
}
$object_sqla[2]=" ORDER BY o.kpi_id";
$rs = getRS(implode(" ",$object_sqla));
$objects = array();
while($row = mysql_fetch_assoc($rs)) {
	$objects[$row['obj_id']] = $row; 
	if($groupby!="X") {
	} else {
		$group['oX']['objects'][$row['obj_id']] = "Y";
	}
}
/* RESULTS */
$r_sql = "SELECT * FROM ".$dbref."_".$table_tbl."_results WHERE ".$r_table_fld."timeid <= ".$report_time[1]." AND ".$r_table_fld.$table_id."id IN (SELECT ".$table_fld."id ".$object_sqla[1].") ORDER BY ".$r_table_fld.$table_id."id, ".$r_table_fld."timeid";
$results = mysql_fetch_alls2($r_sql,"".$r_table_fld.$table_id."id","".$r_table_fld."timeid");
	foreach($objects as $obj_id => $obj) {
		$res = $results[$obj_id];
		$obj_tt = $obj["o".$table_fld.'targettype'];
		$obj_ct = $obj["o".$table_fld.'calctype'];
		for($i=1;$i<=12;$i+=3) {
			$values = array('target'=>array(),'actual'=>array());
			$j = $obj_ct == "CO" ? 1 : $i;
			for($t=$j;$t<=$i+2;$t+=$time_period_size) {
				$tp = "ALL";
				$values['target'][$t] = $res[$t][$fld_target];
				$values['actual'][$t] = $res[$t][$fld_actual];
				$r = KPIcalcResult($values,$obj_ct,$report_time,$tp);
				$results[$obj_id][$t]['r'] = $r;
				//echo "<P>".$obj_id; arrPrint($r);
			}
			/*$t = $i;
			$r = KPIcalcResult($values,$obj_ct,$report_time,"ALL");
			$results[$obj_id][$i]['r'] = $r;*/
//			echo "<P>".$obj_id; arrPrint($r);
		}
		//if($obj_ct=="CO") { arrPrint($results[$obj_id]); }
	}
//arrPrint($results);

/*** START OUTPUT ***/
$c2 = count($mheadings[$r_section]);
foreach($mheadings[$r_section] as $fld => $h) {
	switch($h['h_type']) {
		case "NUM":
			if(!in_array("results",$fields)) { $c2--; }
			break;
		case "TEXT":
			if(!in_array($fld,$fields)) { $c2--; }
			break;
	}
}
if(in_array("results",$fields)) { $c2++; }
$fc = 0;
$fc++;
			foreach($head as $fld => $h) { 				if(in_array($fld,$fields)) { 
$fc++;
			} 			}
			for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
$fc+=$c2;
			}
			//Overall
			if($c2>0 && $report_time[0]!=$report_time[1] && $r_calc=="captured") {
$fc+=3;
			}
$echo = "";
switch($output) {
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	for($c=1;$c<$c2;$c++) { $cellz[10].="\"\","; }
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22]['OVERALL'] = "\"";	//ptd normal cell
	$cella[22]['NUM'] = "\""; //result cell
	$cella[22]['DOUBLE'] = "\""; //result cell
	$cella[22]['TEXT'] = "\""; //result cell
	$cella[22]['R'][0] = "\""; //result cell
	$cella[22]['R'][1] = "\""; //result cell
	$cella[22]['R'][2] = "\""; //result cell
	$cella[22]['R'][3] = "\""; //result cell
	$cella[22]['R'][4] = "\""; //result cell
	$cella[22]['R'][5] = "\""; //result cell
	$cellz[22]['R'][0] = "\","; //close result cell
	$cellz[22]['R'][1] = "\","; //close result cell
	$cellz[22]['R'][2] = "\","; //close result cell
	$cellz[22]['R'][3] = "\","; //close result cell
	$cellz[22]['R'][4] = "\","; //close result cell
	$cellz[22]['R'][5] = "\","; //close result cell
	$rowa = "";
	$pagea = "";
	$rowz = "\r\n";
	$table[1] = "";
	$table[9] = "\r\n";
	$pagez = "";
	$newline = chr(10);
	break;
}
$totals = array();
foreach($result_settings as $i => $r) { $totals[$i] = 0; }
//START PAGE
$echo = $pagea;
$echo.= $table[1];
//GROUP BY 
$head = $export_head;
foreach($group as $i => $g) {
	$g_objects = $g['objects'];
	if(count($g_objects)>0) {
		$groupresults = array();
		foreach($result_settings as $r) { $groupresults[$r['r']]=0; }
		if(strlen($g['value'])>0) {
			$echo.= $table[9].$gcell[1].ASSIST_HELPER::decode($g['value']).$gcell[9].$table[1];
		}
		//Main heading
		$echo.= $rowa;
			$echo .= $cella[12]."Ref".$cellz[12];
			foreach($head as $fld => $h) {
				$echo .= $cella[12].ASSIST_HELPER::decode($h).$cellz[12];
			}
			if($_REQUEST['format']=="agreement") {
				for($t=1;$t<=4;$t+=1) {
					$echo.=$cella[10]."Target ".$t.$cellz[10];
				}
				$echo.=$cella[11]."Weight".$cellz[11];
				$echo.=$cella[11]."Comment".$cellz[11];
			}
		$echo .= $rowz;
		//Time heading
		foreach($g_objects as $obj_id => $g_obj) {
			$obj = $objects[$obj_id];
			$echo .= $rowa.$cella[2].$obj_id.$cellz[2];
			foreach($head as $fld => $h) {
					$echo.=$cella[2];
					switch($h['h_type']) {
						case "WARDS":
						case "AREA":
							break;
						case "TOP":
						case "CAP":
							break;
						case "LIST":
						default:
							$echo.= str_replace(chr(10),$newline,(isset($obj[$fld]) ? ASSIST_HELPER::decode($obj[$fld]) : ""));
							break;
					}
					$echo.=$cellz[2];
			}
			if($_REQUEST['format']=="agreement") {
				/** MONTHLY RESULTS **/
				$res = $results[$obj_id];
				$obj_tt = $obj["o".$table_fld.'targettype'];
				$obj_ct = $obj["o".$table_fld.'calctype'];
				for($t=3;$t<=12;$t+=3) {
					$r = $res[$t]['r'];
					$echo.=$cella[22]['NUM'].number_format($r['target'],0,".","").$cellz[2];
				}
					//$echo.=$cella[22]['NUM'].$obj_ct.$cellz[2];
					//for($t=1;$t<=12;$t+=1) {
					//	$echo.=$cella[22]['NUM'].$res[$t]['kr_target'].$cellz[2];
					//}
			}
			$echo.=$rowz;
		} //foreach g['objects']
	} //if count(g['objects']) > 0
} //foreach group

$echo .= $table[9];
$echo.= $pagez;

if($output == "csv") { $ext = ".csv"; $content = 'text/plain'; } else { $ext = ".xls"; $content = 'application/ms-excel'; }
switch($output) {
case "csv":
//case "excel":
	$path = $modref."/reports";
	$sys_filename = "PMS_".date("Ymd_His").$ext;
	saveEcho($path,$sys_filename,$echo);
	$usr_filename = "PMS_report_".date("Ymd_His",$today).$ext;
	downloadFile2($path, $sys_filename, $usr_filename, $content);
	break;
default:
	echo "<table border=1>".str_replace('"',"<td>",str_replace('",',"",str_replace('","',"<td>",str_replace("\r\n","<tr>",$echo))));
	break;
}
?>