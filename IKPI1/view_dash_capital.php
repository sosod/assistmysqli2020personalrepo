<?php
	//GET DATA
		$time_id = date("m") + ( (date("m")<6) ? 6 : -2);
		$filter_to = $time_id;
		
		$test_sql = "SELECT cr_timeid, sum( cr_actual ) AS actual FROM ".$dbref."_".$table_tbl."_results 
					WHERE cr_timeid <= ".$time_id." GROUP BY cr_timeid ORDER BY cr_timeid DESC";
		//$rs = getRS($test_sql);
		$rs = $helperObj->mysql_fetch_all($test_sql);
		//while($row = mysql_fetch_assoc($rs)) {
foreach ($rs as $row){
			if($row['actual']>0) { $filter_to = $row['cr_timeid']; break; }
		}
//		mysql_close();

		$dir = array('budget'=>0,'actual'=>0);
		$results_sql = "SELECT cap_subid as sub, sum(cr_ytd_target) as budget, sum(cr_ytd_actual) as actual FROM ".$dbref."_".$table_tbl."_results
						INNER JOIN ".$dbref."_".$table_tbl." ON cap_id = cr_capid AND cap_active = true
						INNER JOIN ".$dbref."_subdir ON id = cap_subid AND active = true AND dirid = ".$graph_id."
						WHERE cr_timeid = ".$filter_to." 
						GROUP BY cap_subid";
		$results = $helperObj->mysql_fetch_all_fld($results_sql,"sub");
		$sub_chartData = array();
		foreach($results as $i => $r) {
			$dir['actual']+= $r['actual'];
			$dir['budget']+= $r['budget'];
				$sval = explode(" ",$helperObj->decode($helperObj->decode($lists['subdir'][$i]['value'])));
				$s_name = "";
				$s_n = "";
				foreach($sval as $sv) {
					if(strlen($s_n.$sv)>20) {
						$s_name.=$s_n."\\n";
						$s_n = "";
					}
					$s_n.=" ".$sv;
				}
				$s_name.=" ".$s_n;

			$sub_chartData[] = "{text:\"".$s_name."\",budget:".$r['budget'].",actual:".$r['actual']."}";
		}
	if(array_sum($dir)>0) {
		if($dir['budget']>$dir['actual']) { $max = $dir['budget']; } else { $max = $dir['actual']; }
		//$max = round($max,-3);
		$max+=1000000;
		//arrPrint($sub_chartData);
		$graph_width = count($sub_chartData)*120+50;
		
		?>
        <script type="text/javascript">
        var chart;
 
        var dir_chartData = [
				{text:"",budget:<?php echo $dir['budget']; ?>,actual:<?php echo $dir['actual']; ?>}];
        var sub_chartData = [
				//{text:"Manager",budget:2000000,actual:1500000},
				//{text:"Another Sub",budget:2100000,actual:2109000},
				//{text:"Last Sub",budget:200000,actual:250000}
				<?php echo implode(",",$sub_chartData); ?>
			];
 
			$(document).ready(function() {
				dir_chart = new AmCharts.AmSerialChart();
					dir_chart.dataProvider = dir_chartData;
					dir_chart.categoryField = "text";
					dir_chart.rotate = true;
					dir_chart.marginLeft = 30;
					dir_chart.marginBottom = 50;
					dir_chart.marginRight = 30;
					dir_chart.plotAreaBorderAlpha = 0.2;
					dir_chart.angle = 45;
					dir_chart.depth3D = 15;
				sub_chart = new AmCharts.AmSerialChart();
					sub_chart.dataProvider = sub_chartData;
					sub_chart.categoryField = "text";
					sub_chart.columnSpacing = 1;
					sub_chart.columnWidth = 0.9;
					sub_chart.marginLeft = 60;
					sub_chart.marginTop = 10;
					sub_chart.plotAreaBorderAlpha = 0.2;
					//chart.rotate = true;
					sub_chart.fontSize = 8.5;
					sub_chart.marginBottom = 50;
					//sub_chart.fontSize = 10;
					sub_chart.angle = 45;
					sub_chart.depth3D = 15;
				
				var graph = new AmCharts.AmGraph();
					graph.title = "Budget";
					graph.valueField = "budget";
					graph.type = "column";
					graph.lineAlpha = 0;
					graph.fillAlphas = 1;
					graph.lineColor = "#FE9900";
				var graphS = new AmCharts.AmGraph();
					graphS.title = "Budget";
					graphS.valueField = "budget";
					graphS.type = "column";
					graphS.lineAlpha = 0;
					graphS.fillAlphas = 1;
					graphS.lineColor = "#FE9900";
					sub_chart.addGraph(graphS);
					dir_chart.addGraph(graph);
				var graph = new AmCharts.AmGraph();
					graph.title = "Actual";
					//graph.labelText="[[value]]";
					graph.valueField = "actual";
					graph.type = "column";
					graph.lineAlpha = 0;
					graph.fillAlphas = 1;
					graph.lineColor = "#009900";
					sub_chart.addGraph(graph);
					dir_chart.addGraph(graph);

				var valAxis = new AmCharts.ValueAxis();
					//valAxis.stackType = "regular";
					valAxis.gridAlpha = 0.1;
					valAxis.axisAlpha = 0;
					sub_chart.addValueAxis(valAxis);
					dir_chart.addValueAxis(valAxis);

				sub_chart.categoryAxis.gridAlpha = 0.5;
				sub_chart.categoryAxis.axisAlpha = 0;
				sub_chart.categoryAxis.gridPosition = "start";
				

 
				sub_chart.write("sub");
				dir_chart.write("dir");
				
			});
	    </script>
<div align=center>
	<table class=noborder><tr class=no-highlight>
		<td class="center noborder"><div align=center><span style="font-weight: bold;"><?php echo $lists['dir'][$graph_id]['value']; ?></span>
			<div id="dir" style="width:<?php echo ($graph_width < 300 ? 300 : $graph_width); ?>px; height:250px; background-color:#ffffff;"></div>
		</div></td>
	</tr><tr class=no-highlight>
		<td class="center noborder"><div align=center><span style="font-weight: bold;"><?php echo $head_sub; ?>s</span>
			<div id="sub" style="width:<?php echo ($graph_width < 250 ? 250 : $graph_width); ?>px; height:400px; background-color:#ffffff;"></div>
		</div></td>
	</tr><tr class=no-highlight>
		<td colspan=2 class=noborder>
		<div align=center><table>
		<thead>
			<tr class=no-highlight>
				<td class="right bottom" style="border-top: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff;"><span style="margin-top: 0px;font-style: italic; font-size: 6.5pt; line-height: 7pt">Year-To-Date as at <?php echo $time[$filter_to]['display_full']; ?></span></td>
				<th class=time2 width=120>Budget</th>
				<th class=time3 width=120>Actual</th>
			</tr>
		</thead><tbody>
		<?php
			foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
				echo "<tr>";
					echo "<td style=\"font-weight: bold;\">".$s['value']."</td>";
					echo "<td class=\"right time2\">".(isset($results[$s_id]) ? number_format($results[$s_id]['budget'],2) : "-")."</td>";
					echo "<td class=\"right time3\">".(isset($results[$s_id]) ? number_format($results[$s_id]['actual'],2) : "-")."</td>";
				echo "</tr>";
			}
		?>
		</tbody><tfoot>
			<tr>
				<td class="total right"><?php echo $lists['dir'][$graph_id]['value']; ?>:</td>
				<td class="time2 right"><?php echo number_format($dir['budget'],2); ?></td>
				<td class="time3 right"><?php echo number_format($dir['actual'],2); ?></td>
			</tr>
		</tfoot>
		</table></div></td>
	</tr></table>
</div>
<?php 
	} else {
		echo "<p>No data to display for this ".$head_dir."</p>";
	}
displayGoBack("view_dash.php","Go Back"); ?>