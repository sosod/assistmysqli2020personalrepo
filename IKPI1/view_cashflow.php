<?php 
$section = "CF";
			$get_lists = true;
			$get_active_lists = false;
			$locked_column = true;

include("inc/header.php");
//arrPrint($_REQUEST);
//Filter settings
$filter = array();
	//WHEN
	if(isset($_REQUEST['filter_when'])) {
		$filter_when = $_REQUEST['filter_when'];
	} else {
		$filter_to = $current_time_id;
		$filter_when = array($filter_to,$filter_to);
	}
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


//arrPrint($filter);
//arrPrint($mheadings);


if(!$import_status[$section]) {
	die("<p>Monthly Cashflow has not yet been created.  Please try again later.</p>");
} else {

	$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
		INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT c.cf_subid FROM ".$dbref."_cashflow c
		INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE cf_active = true ) WHERE d.active = true";
	$valid_dir_sub = $helperObj->mysql_fetch_all_fld2($dir_sql,"dir","subdir");


	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = "<h2 style=\"margin-top: 0px;\">";
		$head.= $time[$filter['when'][0]]['display_full'];
	$head.="</h2>";
	$filter['who'] = drawViewFilter(true,false,true,1,true,$filter,$head);
	//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
	//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);


	//SQL
	$object_sql = "SELECT c.* FROM ".$dbref."_cashflow c
	INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true
	".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
	INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
	WHERE cf_active = true ".($filter['who'][0]=="S" && $helperObj->checkIntRef($filter['who'][1]) ? " AND cf_subid = ".$filter['who'][1] : "");
	
	$results_sql = "SELECT r.* FROM ".$dbref."_cashflow_results r
	INNER JOIN ".$dbref."_cashflow c ON r.cr_cfid = c.cf_id AND c.cf_active = true
	WHERE cr_timeid <= ".$filter['when'][0]." AND cr_cfid IN (SELECT c.cf_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
	
	$results = $helperObj->mysql_fetch_all_fld2($results_sql,"cr_cfid","cr_timeid");
	$rheadings = "CF_R";
	

	
	$object_sql.= " ORDER BY dir.sort, subdir.sort, c.cf_sort";
	
	include("view_table.php");

	
	
}	//if import_status['import_Section'] == false else	
?>
<p>&nbsp;</p>
</body>
</html>