<?php 
$section = "RS";
$get_lists = true;
$get_active_lists = false;
$locked_column = true;
include("inc/header.php"); 

//arrPrint($mheadings);

//Filter settings
$filter = array();
	//WHEN
	if(isset($_REQUEST['filter_when'])) {
		$f_when = $_REQUEST['filter_when'];
		$filter_when = array(
			0=>($f_when[0]<=$f_when[1] ? $f_when[0] : $f_when[1]),
			1=>($f_when[0]<=$f_when[1] ? $f_when[1] : $f_when[0])
		);
	} else {
		$filter_to = $current_time_id;
		$filter_from = ($filter_to > 4) ? $filter_to - 2 : 1;
		$filter_when = array($filter_from,$filter_to);
	}
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


//arrPrint($filter);


if(!$import_status[$section]) {
	die("<p>Revenue By Source has not yet been loaded.  Please try again later.</p>");
} else {

	$valid_dir_sub = array();


	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = "<h2 style=\"margin-top: 0px;\">";
	if($filter['when'][0]==$filter['when'][1]) {
		$head.= $time[$filter['when'][0]]['display_full'];
	} else {
		$head.= $time[$filter['when'][0]]['display_full']." - ".$time[$filter['when'][1]]['display_full'];
	}
	$head.="</h2>";
	$filter['who'] = drawViewFilter(false,false,true,2,false,$filter,$head);
	//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
	//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);


	//SQL
	$object_sql = "SELECT r.* FROM ".$dbref."_revbysrc r
	WHERE r.rs_active = true ORDER BY r.rs_sort";
	
	$results_sql = "SELECT r.* FROM ".$dbref."_revbysrc_results r
	INNER JOIN ".$dbref."_revbysrc rs ON r.rr_rsid = rs.rs_id AND rs.rs_active = true
	WHERE rr_timeid <= ".$filter['when'][1];
	
	$results = $helperObj->mysql_fetch_all_fld2($results_sql,"rr_rsid","rr_timeid");
	$fld_target = "rr_budget";
	$fld_actual = "rr_actual";
	

	
	//$object_sql.= " LIMIT 50";
	
	include("view_table.php");

	
	
}	//if import_status['import_Section'] == false else	
?>
<p>&nbsp;</p>
</body>
</html>