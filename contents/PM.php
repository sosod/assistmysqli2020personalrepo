<?php
function echoLink($link) {
	global $mloc;
	echo "<img src='pics/bullet_w.gif'><a href=\"$mloc/".strtolower($link['3'])."\">".$link['2']."</a>&nbsp;";
}
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
***************/
//VARIABLES
$dbref               = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc                = $menurow['modlocation'];
$_SESSION['modref']  = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['dbref']   = $dbref;
$width               = strlen($menurow['modtext'])*7.8;

/** REQUIRED CODE **/
//CONTENTS TITLE WITH LINK TO MAIN PAGE
//echo "<td width=$width style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: bold;\"><a href='$mloc/main.php' style=\"text-decoration: none; color: #ffffff\">".$menurow['modtext'].":</a>&nbsp;</td>";
//CONTENTS
//echo "<td style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: normal;\">&nbsp;";
/** END REQUIRED **/
/**
Get the menu
**/
$sql = "SELECT id, parent_id, name, url, client_terminology, ignite_terminology FROM ".$dbref."_menu WHERE parent_id = 0 AND status & 1 = 1  
        ORDER BY sort
       ";
$rs  = getRS($sql);
$menudata = array();
while($row = mysql_fetch_assoc($rs)) 
{
	if( strpos($row['name'], ".") > 0)
	{
		$location = "main.php?controller=".substr($row['name'], 0, strpos($row['name'],"."))."&parent=".$row['id']."&folder=".$row['name'];		
	} else {
		$location = "main.php?controller=".$row['url']."&parent=".$row['id']."&folder=".$row['name'];
	}
	
	$menudata[$row['id']] = array($row['id'] ,
							$row['parent_id'] ,
							($row['client_terminology'] == "" ? $row['ignite_terminology'] : $row['client_terminology'] ) ,
							 $location
							); 
}
$userRes = getRS("SELECT * FROM ".$dbref."_user_setting WHERE user_id = '".$_SESSION['tid']."' ");
$user   = array();
while($userRow = mysql_fetch_assoc($userRes))
{
   $user['status'] = $userRow['status'];
}
if($_SESSION['tid'] == '0000')
{
     if($tkuser == "admin") 
     {	//if ignite assist administrator
	     //display the link to +Setup
	     echoLink($menudata[4]);
	     echo "<script type=text/javascript> parent.main.location.href = '/PM/main.php?controller=setup&action=defaults&parent=4&folder=setup/';</script>";
     }
} else {
   if(!empty($user))
   {
     foreach($menudata as $id => $menu)
     {
        switch($id)
        {
          case 4:
             if(($user['status'] & 1024) == 1024)
             {
               echoLink($menu);      
             }               
          break;
          case 5:
            if(($user['status'] & 8) == 8)
            {
               echoLink($menu);
            }
          break;
          case 3:
            if(($user['status'] & 128) == 128)
            {
               echoLink($menu);
            }
          break;          
          default:
           echoLink($menu);	
          break;
        
        }	
     }          
     //echo "<script type=text/javascript> parent.main.location.href = '/PM/setup/';</script>";       
   } 
}
?>
 
