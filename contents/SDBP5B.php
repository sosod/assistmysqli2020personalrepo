<?php
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
***************/
/** 
Get User Access
**/

$user_access_fields = array(
	'module'=>"Module<br />Admin",
	'kpi'=>"KPI<br />Admin",
	'finance'=>"Finance<br />Admin",
	'toplayer'=>"Top Layer<br />Admin",
	'assurance'=>"Assurance<br />Provider",
	'second'=>"Secondary<br />Time Period",
	'setup'=>"Setup",
);

//require_once("../SDBP4/inc/inc.php");
function getUserAccess($tkid) {
	$user = array('access'=>array(),'manage'=>array());
	global $dbref;
	
	/* Setup > User Access */
	global $user_access_fields;
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true AND tkid = '$tkid'";
	$a = mysql_fetch_all($sql);
	if(count($a)>0) {
		$a = $a[0];
	} else {
		foreach($user_access_fields as $fld => $h) {
			$a[$fld] = false;
		}
	}
	$user['access'] = $a;
	/* Setup > Administrators */
	$a = array();
	$sql = "SELECT * FROM ".$dbref."_user_admins WHERE active = true and tkid = '$tkid' ORDER BY type, ref";
	$manage = mysql_fetch_all_fld2($sql,"type","ref");
	$user['manage'] = $manage;
	
	return $user;
}
$my_access = getUserAccess($tkid);

/**
Get the menu
**/

$menudata = array(	//0=>id, 1=>display, 2=>link
	'view' => array(1,"View","view.php"),
	'manage' => array(2,"Manage","manage.php"),
//	'search' => array(5,"Search","search.php"),
//	'glossary' => array(5,"Glossary","glossary.php"),
	'admin' => array(7,"Admin","admin.php"),
	'report' => array(3,"Report","report.php"),
	'setup' => array(4,"Setup","setup.php"),
	'support' => array(6,"Support","support.php"),
);

if($tkuser == "admin") {	//if ignite assist administrator
	//display the link to +Setup
	echoContentsLink($menudata['setup']);
} else {
	//development useraccess code
	foreach($menudata as $key => $m) {
		$echo = false;
		switch($key) {
			case "setup":		if($my_access['access']['setup'] || $my_access['access']['module'] || $_SESSION['ia_support']==1 ) { $echo = true; } break;
			case "manage":	if(count($my_access['manage'])>0 || $my_access['access']['module'] || $my_access['access']['finance'] || $my_access['access']['assurance']) { $echo = true; } break;
			case "admin":	if( $my_access['access']['module'] || $my_access['access']['kpi'] || $my_access['access']['toplayer'] ) { $echo = true; } break;
			case "support":	if($_SESSION['ia_support']>0) { $echo = true; } break;
			default: $echo = true; break;
		}
		if($echo) {	echoContentsLink($m);	}
	}
	//look in the _useraccess table where user = '$tkid'
}
?>
