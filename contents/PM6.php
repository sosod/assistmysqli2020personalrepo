<?php
require_once("module/autoloader.php");


$menuObject = new PM6_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new PM6_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);
/* User Access Rules */
//NEW displays always so that user can approve their scorecard/staff scorecard - local pages must manage user access as needed
//MANAGE always displays
//All other access must be managed here || locally as needed
if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = true;
    	if(
			($m['section']=="assessment" && $useraccess['assessment']!=true)	//access to scorecard
			||
    		($m['section']=="trigger" && $useraccess['trigger']!=true)	//access to assessment
			||
			($m['section']=="report" && $useraccess['report']!=true)
			||
			($m['section']=="setup" && $useraccess['setup']!=true)
			||
			($m['section']=="admin" && $useraccess['admin']!=true)
		) {
    		 $valid8 = false; 
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>