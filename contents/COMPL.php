<?php
function echoLink($link) {
	global $mloc;
	echo "<img src='pics/bullet_w.gif'><a href=\"$mloc/".strtolower($link['3'])."\">".$link['2']."</a>&nbsp;";
}
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
***************/
//VARIABLES
$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc = $menurow['modlocation'];
$_SESSION['modref'] = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['dbref'] = $dbref;
$width = strlen($menurow['modtext'])*7.8;

/** REQUIRED CODE **/
//CONTENTS TITLE WITH LINK TO MAIN PAGE
//echo "<td width=$width style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: bold;\"><a href='$mloc/main.php' style=\"text-decoration: none; color: #ffffff\">".$menurow['modtext'].":</a>&nbsp;</td>";
//CONTENTS
//echo "<td style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: normal;\">&nbsp;";
/** END REQUIRED **/
/**
Get the menu
**/
$sql = "SELECT id , parent_id , name , client_terminology, ignite_terminology FROM  ".$dbref."_menu WHERE parent_id = 0 AND status > 0" ;
$rs = getRS($sql);
$menudata = array();
while( $row = mysql_fetch_assoc( $rs ) ) {
	if( $row['name'] == "new")
	{
	  $row['name'] = "new/legislation.php";		
	} else if($row['name'] == "manage") { 
	  $row['name'] = "manage/view.php";		
	} else if( $row['name'] == "admin") {
	  $row['name'] = "admin/events.php";
	} else if ($row['name']=="support") {
	  $row['name'] = "support/edit_deliverables.php";
	}

	$menudata[$row['id']] = array( $row['id'] ,
							 $row['parent_id'] ,
							 ($row['client_terminology'] == "" ? $row['ignite_terminology'] : $row['client_terminology'] ) ,
							 $row['name'],
						    ); 
}
//get user access settings
$userAccess = array();	
$resource   = getRs("SELECT id, user_id, status FROM ".$dbref."_useraccess WHERE user_id = '".$_SESSION['tid']."'");
$userAccess = mysql_fetch_assoc($resource);
if($tkuser == "admin") {
	//if ignite assist administrator
	//display the link to +Setup
	echoLink($menudata[7]);
	echo "<script type=text/javascript> parent.main.location.href = '/COMPL/setup/';</script>";
} else {
	
	$legAdmin = array();
	$admins   = "";
	//get users assigned to be legislation administrators
	$q = "SELECT admin, id  FROM ".$dbref."_legislation ";
	$res = getRs( $q ); 	
	while($row = mysql_fetch_assoc($res))
	{		
		$legAdmin[$row['id']] = $row['admin'];
		$admins       		 .= $row['admin'].",";
	}
	$admins   = explode(",", rtrim($admins,","));
 
	foreach($menudata as $id => $menu)
	{
		switch($id)
		{
		    case 1 :
		     //if user has access to create or import legislation , then show the New link
		     if((($userAccess['status'] & 1024) == 1024) ||  (($userAccess['status'] & 4) == 4))
		     {
		       echoLink($menu);
		     }
		    break;
		    case 6 :
		     //show the admin link if the user has access to admin or is adminstrator of the legislations
			if((($userAccess['status'] & 8) == 8) || (in_array($_SESSION['tid'],  $admins)))
			{
			  echoLink($menu);	
			} 
		    break;
		    case 7 :
               //user access setting to view and do the setup
               if(($userAccess['status'] & 512) == 512)
               {
                 echoLink($menu);
               }
		    break;
              default: 
				if($menu['3']=="support/edit_deliverables.php") {
					if($isSupportUser===true) {
						echoLink($menu);
					}
				} else {
					echoLink($menu);
				}
              break;		   
		};
		
		/*
		if( $id == 35)
		{
			if(($userAccess['status'] & 8) == 8)
			{
				echoLink( $menu );	
			} else {
				if( in_array($_SESSION['tid'],  $admins))
				{
				   echoLink( $menu );	
				}	
			}
		} else if( $id == 3) {
			//user access setting to view and do the setup
			if(($userAccess['status'] & 512) == 512 )
			{
			  echoLink( $menu );
			}
		} else if($id == 4){
			//user access to view and create the reports
			if(($userAccess['status'] & 128) == 128)
			{
			  echoLink( $menu );	
			}
		} else if($id == 1){}{
			echoLink( $menu );			
		}*/
	}
if($isSupportUser===true) 
{
	//echoLink(array('2'=>"Support",'3'=>"support"));
}
	//look in the _useraccess table where user = '$tkid'
/*	$sql = "SELECT * FROM ".$dbref."_useraccess WHERE user = '$tkid'";
	include("inc_db_con.php");
		$useraccess = mysql_fetch_assoc($rs);
	mysql_close($con);
	//if(no record is found) display an error message => "Please contact your administrator to setup your user access"
	if(!isset($useraccess['id'])) {
		echo "Invalid user access.  Please contact your administrator to setup your user access.";
	} else {	//display contents depending on useraccess
		//if(user is module_admin or has create access) 
		
		if($useraccess['module_admin']==1 || $useraccess['create_risks']==1) {
			//display link to +New
			echoLink($menudata[1]);
		}
		//display link to +Manage
		echoLink($menudata[2]);
		//if(user is module_admin or has report access) 
		if($useraccess['module_admin']==1 || $useraccess['report']==1) {
			//display link to +Report
			echoLink($menudata[3]);
		}
		//if(user is module_admin or has setup access)
		if($useraccess['module_admin']==1 || $useraccess['setup']==1) {
			//display link to +Setup
			echoLink($menudata[4]);
		}
		//display link to +Glossary
			echoLink($menudata[5]);
		//display link to +Search
			echoLink($menudata[6]);
	}
	*/
}
?>
 
