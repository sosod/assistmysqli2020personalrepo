<?php
//error_reporting(-1);
require_once("module/autoloader.php");

$menuObject = new SDBP6_MENU($menurow['modref']);
$menu = $menuObject->getContentsBar();

$userObject = new SDBP6_USERACCESS($menurow['modref']);
$useraccess = $userObject->getMyUserAccess();

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = false;
    	if(isset($useraccess[$m['section']]) && $useraccess[$m['section']]==true) {
    		 $valid8 = true;
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>