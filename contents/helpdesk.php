<?php
//2019 Annual Maintenance #AA-297
require_once("module/autoloader.php");

$menuObject = new HELPDESK_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new HELPDESK_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);

/* Hide the "+New" menu item if the current user is the support user - Start */
/*
function isThisSupport(){
    $client_db = new ASSIST_MODULE_HELPER();

    $sql = 'SELECT * FROM assist_' . $_SESSION['cc'] . '_timekeep ';
    $sql .= 'WHERE tkid = \''.$_SESSION['tid'] . '\' ';
    $user = $client_db->mysql_fetch_one($sql);

    $is_support = false;
    if($user['tkuser'] == 'support'){
        $is_support = true;
    }

    return $is_support;
}

$support_user = isThisSupport();
*/

$support_user = $me->isSupportUser();

if($support_user == true){
    unset($menu['new']);
}
/* Hide the "+New" menu item if the current user is the support user - End */

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = true;
    	if(
			($m['section']=="new" && $useraccess['new']!=true)
			||
    		($m['section']=="report" && $useraccess['report']!=true)
			||
			($m['section']=="setup" && $useraccess['setup']!=true)
			||
			($m['section']=="admin" && $useraccess['admin']!=true)
		) {
    		 $valid8 = false; 
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>
<!-- <u><b>!!!!! USER ACCESS SET - NEEDS TESTING!!!!!!!!</b></u> -->