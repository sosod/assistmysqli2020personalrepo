<?php 
//include("RAPS/class/raps_access.php");
//include("RAPS/class/raps_helper.php");
//include("RAPS/class/raps.php");

//include("module/autoloader.php");
$me = new RAPS();

$menu = array(
	'new'	=> array(1,"New","new.php"),
	'manage'=> array(2, "Manage","manage.php"),
	'report'=> array(3, "Report","report.php"),
	'setup'	=> array(5,"Setup","setup.php"),
	'admin'	=> array(5,"Admin","admin.php"),
);


if(!$me->assist->isAdminUser() && $me->ra->canAdd()) {
	echoContentsLink($menu['new']);
}
if(!$me->assist->isAdminUser()) {
	echoContentsLink($menu['manage']);
	echoContentsLink($menu['report']);
}
if($me->ra->canSetup() || $me->assist->isAdminUser()) {
	echoContentsLink($menu['setup']);
}
if(!$me->assist->isAdminUser() && $me->ra->accessToAdmin()) {
	echoContentsLink($menu['admin']);
}
?>