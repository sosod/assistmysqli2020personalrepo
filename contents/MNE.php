<?php
//require_once("/CNTRCT/class/cntrct.php");
//require_once("/CNTRCT/class/cntrct_contract.php");
//require_once("/CNTRCT/class/cntrct_useraccess.php");
//require_once("/CNTRCT/class/cntrct_menu.php");
require_once("module/autoloader.php");

//$path = realpath('CNTRCT/class'); 
//foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $filename) {
//	echo "$filename\n";
//} 

$menuObject = new CNTRCT_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new CNTRCT_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = true;
    	if(
			($m['section']=="new" && $useraccess['new']!=true)
			||
    		($m['section']=="report" && $useraccess['report']!=true)
			||
			($m['section']=="setup" && $useraccess['setup']!=true)
			||
			($m['section']=="admin" && $useraccess['admin']!=true)
		) {
    		 $valid8 = false; 
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>
<!-- <u><b>!!!!! USER ACCESS SET - NEEDS TESTING!!!!!!!!</b></u> -->