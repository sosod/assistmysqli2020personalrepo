<?php
$report = "N";
//$modref = "DHP10";

$dbref = "assist_".$cmpcode."_".strtolower($_SESSION['modref']);
$modref = $_SESSION['modref'];
include($_SESSION['modlocation']."/inc_admin.php");

$menudata = array(	//0=>id, 1=>display, 2=>link
	'view' => array(1,"View","view.php"),
	'admin' => array(2,"Admin","admin.php"),
	'report' => array(3,"Report","report.php"),
	'setup' => array(4,"Setup","setup.php"),
);

if($tkid != "0000" && $access[0]=="Y") {  
	if($access['view']=="Y" || $access['dash']=="Y") { echoContentsLink($menudata['view']); } 
    if($access['capture']=="Y" || $access['activate']=="Y" || $access['dash']=="Y") { echoContentsLink($menudata['admin']); } 
	if($access['report']=="Y" || $access['dash']=="Y") { echoContentsLink($menudata['report']); } 
}

//if($setup[0][0] == $tkid || $tkid == "0000" || ($tkuser == "support" && $tkname == "Ignite Support"))
if($setup[0][0] == $tkid || $tkid == "0000" || $_SESSION['ia_support']) {
	echoContentsLink($menudata['setup']);
    if($tkid == "0000") {
        ?><script type=text/javascript>
        parent.main.location.href="<?php echo $_SESSION['modlocation']; ?>/setup.php?ACT0=A";
        </script>
        <?php
    }
}

?>