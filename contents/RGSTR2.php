<?php
require_once("module/autoloader.php");

$menuObject = new RGSTR2_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new RGSTR2_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = true;
    	if(
			($m['section']=="new" && $useraccess['new']!=true)
			||
    		($m['section']=="report" && $useraccess['report']!=true)
			||
			($m['section']=="setup" && $useraccess['setup']!=true)
			||
			($m['section']=="admin" && $useraccess['admin']!=true)
		) {
    		 $valid8 = false; 
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>
<!-- <u><b>!!!!! USER ACCESS SET - NEEDS TESTING!!!!!!!!</b></u> -->