<?php
//error_reporting(-1);

require_once("module/autoloader.php");

$client_db = new ASSIST_MODULE_HELPER();

$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc = $menurow['modlocation'];
$actname = "";
$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '".$menurow['modref']."' AND refid = 1";

$rs = $client_db->mysql_fetch_one($sql);

if(count($rs)>0) {
	$actname = $rs['value'];
}

if(strlen($actname)==0) {
	$sql = "INSERT INTO assist_".$cmpcode."_setup (ref,refid,value,comment,field) VALUES ('".$menurow['modref']."',1,'Action','Name of action','')";
	$rs = getRS($sql);
	$actname = "Action";
}
$_SESSION['modref'] = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['actname'] = $actname;
$_SESSION['dbref'] = "assist_".strtolower($cmpcode)."_".strtolower($_SESSION['modref']);

$menuObject = new PDP2_MENU();
$menu = $menuObject->getContentsBar();

//echo '<pre style="font-size: 18px">';
//echo '<p>$menu</p>';
//print_r($menu);
//echo '</pre>';

//$menu = array(
//	'new' => array(1,"New","new.php"),
//	'view' => array(2, "View","view.php"),
//	'report' => array(3, "Report","report.php"),
//	'setup'=>array(5,"Setup","setup.php?TA0=A"),
//	'graph' => array(4, "Graphs","graphs.php"),
//	'profile' => array(6,"My Profile","profile.php")
//);

if($tkuser != "admin")
{
	echoContentsLinkFormatted($menu['new']);
	echoContentsLinkFormatted($menu['view']);
    $sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."' AND yn = 'Y'";
    $row = $client_db->mysql_fetch_one($sql);
    
    if($row['act'] > 89 || $row['view'] > 89)
    {
		echoContentsLinkFormatted($menu['report']);
		echoContentsLinkFormatted($menu['graphs']);
    }
}
else
{
	echoContentsLinkFormatted($menu['setup']);
}

$sql = "SELECT * FROM assist_".$cmpcode."_setup s WHERE s.value = '".$tkid."' AND s.ref = '".$menurow['modref']."' AND s.refid = 0";
$rs = $client_db->mysql_fetch_all($sql);
$h = count($rs);
if(($h > 0 || $cmpcode == "fin0001" || $cmpcode == "currie1" || ($cmpcode=="iassist" && ($tkuser=="janet" || $tkuser=="rklein"))) && $tkuser != "admin")
{
	echoContentsLinkFormatted($menu['setup']);
}

if($tkuser != "admin" && isset($menu['profile']))
{
	echoContentsLinkFormatted($menu['profile']);
}
?>
