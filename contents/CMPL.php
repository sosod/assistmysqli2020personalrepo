<?php
function echoLink($link) {
	global $mloc;
	echo "<img src='pics/bullet_w.gif'><a href=\"$mloc/".strtolower($link['3'])."\">".$link['2']."</a>&nbsp;&nbsp;";
}
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
***************/
//VARIABLES
$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc = $menurow['modlocation'];
$_SESSION['modref'] = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['dbref'] = $dbref;
$width = strlen($menurow['modtext'])*7.8;

/** REQUIRED CODE **/
//CONTENTS TITLE WITH LINK TO MAIN PAGE
//echo "<td width=$width style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: bold;\"><a href='$mloc/main.php' style=\"text-decoration: none; color: #ffffff\">".$menurow['modtext']."</a>:&nbsp;</td>";
//CONTENTS
//echo "<td style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: normal;\">&nbsp;";
/** END REQUIRED **/

/**
Get the menu
**/
$sql = "SELECT id , parent_id , name , client_terminology , ignite_terminology, type FROM  ".$dbref."_menu WHERE parent_id = 0 " ;
$rs = getRS($sql);
$menudata = array();
while( $row = mysql_fetch_assoc( $rs ) ) {

	if( $_SESSION['tid'] != "0001")
	{
		if(trim($row['id']) != "3" && trim($row['id']) != "21") {
			$menudata[$row['id']] = array( $row['id'] ,
										   $row['parent_id'] ,
										   ($row['client_terminology'] == "" ? $row['ignite_terminology'] : $row['client_terminology'] ) ,
										   ($row['type'] == "new" ? "new/new_deliverable.php"  : $row['type']),
										  ); 	
			}				  
	} else {
		$menudata[$row['id']] = array( $row['id'] ,
									   $row['parent_id'] ,
									   ($row['client_terminology'] == "" ? $row['ignite_terminology'] : $row['client_terminology'] ) ,
									   ($row['type'] == "new" ? "new/new_legislation.php"  : $row['type']),
									  ); 
	}
}
foreach( $menudata as $index => $value ) 
{
	echoLink( $value );
	//echo echoLink( $value[3] );
}
/*
if($tkuser == "admin") {	//if ignite assist administrator
	//display the link to +Setup
	echoLink($menudata[4]);
} else {
	// look to see see if the user is responsible for a directorate
	//$sql = "SELECT * FROM ".$dbref."_directorate_structure WHERE user_responsible = '$tkid' ";
	//include("inc_db_con.php");
		//$respo_directorate = mysql_fetch_assoc($rs);
	mysql_close($con);
	
	//look in the _useraccess table where user = '$tkid'
	$sql = "SELECT * FROM ".$dbref."_useraccess WHERE user = '$tkid'";
	$rs = getRS($sql);
		$useraccess = mysql_fetch_assoc($rs);
	mysql_close($con);
	//if(no record is found) display an error message => "Please contact your administrator to setup your user access"
	if(!isset($useraccess['id'])) {
		echo "Invalid user access.  Please contact your administrator to setup your user access.";
	} else {	//display contents depending on useraccess
		//if(user is module_admin or has create access) 
		
	if($useraccess['module_admin']==1 || $useraccess['create_risks']==1 || $useraccess['risk_manager']==1 || (isset($respo_directorate) && !empty($respo_directorate)) ) 		{
			//display link to +New
			echoLink($menudata[1]);
		}
		//display link to +Manage
		echoLink($menudata[2]);
		//if(user is module_admin or has report access) 
		if($useraccess['module_admin']==1 || $useraccess['report']==1) {
			//display link to +Report
			echoLink($menudata[3]);
		}
		//if(user is module_admin or has setup access)
		if($useraccess['module_admin']==1 || $useraccess['setup']==1) {
			//display link to +Setup
			echoLink($menudata[4]);
		}		
		//display link to +Glossary
			echoLink($menudata[5]);
		//display link to +Search
			echoLink($menudata[6]);
	}
}
*/
?>
 