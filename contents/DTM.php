<?php
require_once("module/autoloader.php");

$menuObject = new DTM_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new DTM_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($useraccess);
//echo '</pre>';

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
        $valid8 = true;
        if(
            ($m['section']=="new" && $useraccess['new']!=true)
            ||
            ($m['section']=="report" && $useraccess['report']!=true)
            ||
            ($m['section']=="setup" && $useraccess['setup']!=true)
            ||
            ($m['section']=="admin" && $useraccess['admin']!=true)
        ) {
            $valid8 = false;
        }
        if($valid8) {
            echoContentsLinkFormatted($m);
        }
    }
}
else
{
    echoContentsLinkFormatted($menu['setup']);
}


?>
<!-- <u><b>!!!!! USER ACCESS SET - NEEDS TESTING!!!!!!!!</b></u> -->