<?php
function echoLink($link) {
    global $mloc;
    echo "<img src='pics/bullet_w.gif'><a href=\"$mloc/".strtolower($link['3'])."\">".$link['2']."</a>&nbsp;&nbsp;";
}
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
 ***************/
//VARIABLES
$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc = $menurow['modlocation'];
$_SESSION['modref'] = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['dbref'] = $dbref;
$width = strlen($menurow['modtext'])*7.8;

/** REQUIRED CODE **/
//CONTENTS TITLE WITH LINK TO MAIN PAGE
//echo "<td width=$width style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: bold;\"><a href='$mloc/main.php' style=\"text-decoration: none; color: #ffffff\">".$menurow['modtext']."</a>:&nbsp;</td>";
//CONTENTS
//echo "<td style=\"text-align: left; color: #ffffff; font-family: arial; font-size: 10pt; font-weight: normal;\">&nbsp;";
/** END REQUIRED **/

/**
Get the menu
 **/

$sql      = "SELECT id, parent_id, name, client_terminology, folder, action_file FROM  ".$dbref."_menu WHERE parent_id = 0 ORDER BY  position asc" ;
$rs       = getRS($sql);
$menudata = array();
while( $row = mysql_fetch_assoc( $rs ) ) {
    $toPage = "";
    if( $row['folder'] == "manage" )
    {
        $toPage = "manage/update_actions.php";
    } else if($row['folder'] == "new") {
        $toPage = "new/index.php";
    } else {
        $toPage = (empty($row['action_file']) ? $row['folder'] : $row['folder']."/".$row['action_file'].".php");
    }

    $menudata[$row['id']] = array($row['id'], $row['parent_id'], ($row['client_terminology'] == "" ? $row['name'] : $row['client_terminology']), $toPage);
}
mysql_close();
//arrPrint($menudata);
if($tkuser=="admin")
{
    echoLink( $menudata[7] );
    echo "<script type=text/javascript>parent.main.location.href = 'SCORECARD/setup'; </script>	";
} else {
    $sql 		= "SELECT * FROM ".$dbref."_user WHERE user_id = '".$tkid."'";
    $rs  		= getRS($sql);
    $useraccess = mysql_fetch_assoc($rs);
    foreach($menudata as $index => $value)
    {
        //echo " user status ".$useraccess['status'];
        switch ($index)
        {
            case 2: //setup
              if((($useraccess['status'] & 256) == 256)) echoLink( $menudata[2] );
            break;
            case 4://reports
                if( (($useraccess['status'] & 64) == 64) ) echoLink( $menudata[4] );
            break;
            case 7://admin
                if( (($useraccess['status'] & 2048) == 2048) ) echoLink( $menudata[7] );
                break;
            case 1:
                //if user is allowed to create scorecard
                if( ($useraccess['status'] & 4) == 4 ){
                        $menudata[1][3] = "new/index.php";
                } else {
                    //if user is not allowed to create scorecard bt allowed to create deliverable ,
                    // then jump to add heading page
                    if( ($useraccess['status'] & 8) == 8 ) {
                        $menudata[1][3] = "new/deliverable.php";
                    } else {
                        // else jump to the actions page
                        $menudata[1][3] = "new/action.php";
                    }
                }
                echoLink( $menudata[1] );
                break;
            default:
                echoLink( $value );
        }

    }
}
?>
 