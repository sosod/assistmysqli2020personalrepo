<?php
/**** NOTES ****
1. if tkid = 0000 and/or tkuser =admin then display link to setup
2. if tkid is module_admin then display all links
3. otherwise display links according to user access (tkid)
4. don't use <td> to display links - it affects the rest of the title - use '+text  '
***************/
//VARIABLES
$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
$mloc = $menurow['modlocation'];
$_SESSION['modref'] = strtoupper($menurow['modref']);
$_SESSION['modtext'] = $menurow['modtext'];
$_SESSION['dbref'] = $dbref;
/**
Get the menu
**/

$sql = "SELECT id , parent_id , name , client_name , folder, viewby FROM  ".$dbref."_menu WHERE parent_id = 0" ;
$rs = getRS($sql);
$menudata = array();
while( $row = mysql_fetch_assoc( $rs ) ) {
	$menudata[$row['id']] = array( $row['id'] ,
							       ($row['client_name'] == "" ? $row['name'] : $row['client_name'] ) ,
							       ($row['folder'] == "manage" ? "manage/view.php" :  $row['folder']) ,
							        $row['viewby']
								  ); 
}
mysql_close();
if($tkuser == "admin") {	//if ignite assist administrator
	//display the link to +Setup
	echoContentsLink($menudata[4]);
	echo "<script type=text/javascript> parent.main.location.href = '/CASE/defaults/';</script>";
} else {
	//look in the _useraccess table where user = '$tkid'
	$sql        = "SELECT * FROM ".$dbref."_useraccess WHERE user = '$tkid'";
	$rs         = getRS($sql);
    $useraccess = mysql_fetch_assoc($rs);
	mysql_close();
	//if(no record is found) display an error message => "Please contact your administrator to setup your user access"
	if(!isset($useraccess['id'])) {
		echo "Invalid user access.  Please contact your administrator to setup your user access.";
	} else {	//display contents depending on useraccess
		//if(user is module_admin or has create access) 
		
		if($useraccess['module_admin']==1 || $useraccess['create_risks']==1) {
			//display link to +New
			echoContentsLink($menudata[1]);
		}
		//display link to +Manage
		echoContentsLink($menudata[2]);
		//if(user is module_admin or has report access) 
		if($useraccess['module_admin']==1 || $useraccess['report']==1) {
			//display link to +Report
			echoContentsLink($menudata[3]);
		}
		//if(user is module_admin or has setup access)
		if($useraccess['module_admin']==1 || $useraccess['setup']==1) 
		{
			//display link to +Setup
			echoContentsLink($menudata[4]);
		}
		if($useraccess['view_all']==1 || $useraccess['edit_all'] ==1)
		{
		    //display Admin tab, where user is update all, and edit all
		    if(isset($menudata[25]))
		    {
		       echoContentsLink($menudata[25]);
		    } else if(isset($menudata[26])){
		       echoContentsLink($menudata[26]);
		    }
		}
		
		//display link to +Glossary
			echoContentsLink($menudata[5]);
		//display link to +Search
			echoContentsLink($menudata[6]);
	}
}
?>
 
