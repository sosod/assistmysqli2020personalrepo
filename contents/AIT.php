<?php
//error_reporting(-1);
require_once("module/autoloader.php");


$menuObject = new AIT_MENU;
$menu = $menuObject->getContentsBar();

$userObject = new AIT_USERACCESS();
$useraccess = $userObject->getMyUserAccess();

//arrPrint($useraccess);

if(!$me->isAdminUser()) {
    foreach($menu as $key=>$m) {
    	$valid8 = true;
    	if(
			($m['section']=="setup" && $useraccess['setup']!=true)
			||
			($m['section']=="admin" && $useraccess['admin']!=true)
		) {
    		 $valid8 = false; 
		} 
		if($valid8) {
        	echoContentsLinkFormatted($m);
		}
    }
}
else
{ 
    echoContentsLinkFormatted($menu['setup']);
}


?>
<!-- <u><b>!!!!! USER ACCESS SET - NEEDS TESTING!!!!!!!!</b></u> -->