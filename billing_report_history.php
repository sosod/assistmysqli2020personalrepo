<?php
//error_reporting(-1);

function fileError() {	
	echo "<p>Error!  The report you have requested is not available.";
}

$cmpcode = "";
require_once 'inc_db.php';
require_once 'inc_db_conn.php';

$get_session = false;

$hist_id = $_REQUEST['h'];

if(strlen($hist_id)==0 || !is_numeric($hist_id) || $hist_id<1) {
	fileError();
} else {

	$sql = "SELECT * FROM assist_billing_report_history WHERE id = ".$hist_id;
	$hist = mysql_fetch_one($sql,"A");

	if(count($hist)==0 || !isset($hist['filename'])) {
		fileError();
	} else {
		$file = explode("/",$hist['filename']);
		$file = $file[count($file)-1];
		$file = "reports/files/".$file;
		if(!file_exists($file)) {
			fileError();
		} else {
			include($file);
		}
	}
}

?>