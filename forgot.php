<?php

$default_site_code = "actionassist";
session_destroy();
session_start();
        header('Expires: 0');
        header('Cache-Control: private');
        header('Pragma: cache');
//error_reporting(-1);

require_once("library/class/autoload.php");
$alogin = new ASSIST_LOGIN(array(),false);
require 'inc_status.php';

$mdb = new ASSIST_DB("master");
$mods = $mdb->mysql_fetch_all("SELECT * FROM www_modules WHERE active = 1 ORDER BY code");
  $sections = array();

  foreach($mods as $m) {
	$descrip = strip_tags($m['blurb']);
	$o = $descrip;
	$descrip = substr($descrip,0,120);
	if($descrip!=$o) { $descrip.="..."; }
	
	$sections[]= array(
		'link'=>"/modules/view_module.php?code=".$m['code'],
		'name'=>$m['function'],
		'desc'=>$descrip,
	);
  }
  $random = array();
  $random_module = rand( 0, count( $sections ) - 1 );
  $random[] = $random_module;
while(in_array($random_module, $random)) { $random_module = rand(0,count($sections)-1); }
  $random[] = $random_module;
while(in_array($random_module, $random)) { $random_module = rand(0,count($sections)-1); }
  $random[] = $random_module;


$site_code = strlen($_SERVER['QUERY_STRING'])>0 ? $_SERVER['QUERY_STRING'] : $default_site_code;

$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE site_code = '".$site_code."' AND active = 1";
$site = $mdb->mysql_fetch_one($sql);

if(!isset($site['site_name'])) {
	$site_code = $default_site_code;
	$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE r.id = 1 ";
	$site = $mdb->mysql_fetch_one($sql);
}

$site_name = $site['site_name'];
$site_logo = "pics/bp_logos/".$site['assist_logo'];
if(!file_exists($site_logo)) {
	$site_logo = "pics/bp_logos/actionassist.png";
}
$site_logo = "/".$site_logo;
//if($assiststatus!="U") {
	$sql = "SELECT cmpcode, cmpreseller, cmpname FROM assist_company WHERE cmpstatus = 'Y' ORDER BY cmpcode";
	$rows = $mdb->mysql_fetch_all($sql); //$rs = $mdb->db_query($sql); //AgetRS($sql);
	$cmp = array();
//	while($row = mysql_fetch_assoc($rs)) {
	foreach($rows as $row) {
		$cmp[$row['cmpcode']] = $row;
	}
	unset($rs);
	
	$sql = "SELECT * FROM assist_reseller WHERE active = 1 ORDER BY cmpcode";
	$rows = $mdb->mysql_fetch_all($sql); //$rs = $mdb->db_query($sql);
	//$rs = AgetRS($sql);
	$res = array();
	foreach($rows as $row) {//while($row = mysql_fetch_assoc($rs)) {
		$res[$row['cmpcode']] = array();
		$a = explode("|",$row['help_contact']);
		$d = array();
		foreach($a as $b) {
			$c = explode("_",$b);
			$d[] = "<br />&nbsp;&nbsp;&nbsp;&nbsp;<span class=i>".$c[0]."</span>: ".($c[0]=="Email" ? "<a href=mailto:".$c[1]." class=fc>" : "").$c[1].($c[0]=="Email" ? "</a>" : "");
//			$d[] = $c[1];
		}
		$res[$row['cmpcode']] = implode("",$d);
//		$res[$row['cmpcode']] = implode(" or ",$d);
	}
	unset($rs);
//}

?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Ignite4u.co.za</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen.css?ac=3" media="screen,projector" />
    <link rel="stylesheet" type="text/css" href="/pics/frontpage/include/print.css?ac=1" media="print" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie.css?ac=1" /><![endif]-->
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie6.css?ac=1" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie7.css?ac=1" /><![endif]-->
    <!--[if IE 8]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie8.css?ac=1" /><![endif]-->    
<script type ="text/javascript" src="library/js/assistform.js"></script>
<script type ="text/javascript" src="library/js/assisthelper.js"></script>
<script type ="text/javascript" src="library/js/assiststring.js"></script>
<link href="/library/jquery-ui-1.10.0/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery-ui.min.js"></script>	
	
    <script type="text/javascript" src="/pics/frontpage/include/ignite.js?ac=1"></script>
	
    <!--[if IE]><script type="text/javascript" src="/pics/frontpage/include/ignite-ie.js?ac=1"></script><![endif]-->
	<script type="text/javascript" src="/pics/frontpage/include/js/jquery.lightbox-0.5.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/pics/frontpage/include/css/jquery.lightbox-0.5.css" />	

<style type=text/css>
.notice { font-weight: bold; text-decoration: none; }
.center { text-align: center; }
.required { color: white; background-color: red; }
.b { font-weight: bold; }
.u { text-decoration: underline; }

#tbl_social td {
	font-size: 8pt;
	text-align: center;
	vertical-align: center;
}
.section_module {
	border: 1px solid #bcbcbc;
	margin: 10px auto;
	padding: 5px;
	width: 80%;
	font-size: 90%;
	background-color: #ffffff;
}
.section_module h4 {
	font-weight: bold;
	color: #555555;
	font-size: 110%;
	margin-bottom: 3px;
}
#active_user_count {
	padding-right: 5px;
	position: absolute;
	bottom: 10px;
	right: 5px;
	border: 0px dashed #990000;
}
#active_user_count p {
	font-size: 85%;
	font-style: italic;
	color: #555555;
}
#header-top-row { border: 0px dashed #fe9900; }
#header { border: 0px dashed #009900; }

//.noborder { border: 2px dashed #009900; }
//table.noborder th , table.noborder td { border: 2px dashed #009900; }
</style> 
  </head>
  <body>
    <div id="site-frame">
      <div id="header" >
 	<a id="logo" href="/"><img src="<?php echo $site_logo; ?>" alt="<?php echo $site_name; ?>" /></a>
	  <div style="" id=header-top-row>

</div>
<div id=active_user_count><p><?php include "inc_count_active_users.php"; ?></p></div>
</div>
      <div id="content-frame">
        
 
      
        <img id="column-divider-top" src="/pics/frontpage/images/bg-hr-column.png" alt="divider" />
<div id="column-wrapper">
  <div id="content-column">
    <div class="spacer">
      <h1>Forgot Your Password?</h1>
			
			
	  <div id=login style='margin-bottom: 20px;'>
	  
				<table style="padding: 5px; border: 2px solid #034C96; margin: 0 auto;"><tr><td style="padding: 5px; ">
<?php 
if($assiststatus=="U") { 
	echo "<p class=center>".$site_name." is currently undergoing maintenance.<br />Please try again later.</p><input type=hidden id=u />";
} else {
	$auser = new ASSIST_USER();
	$securityQs = $auser->getSecurityQuestions();
?>
<table><tr><td><form name=frm_forgot><input type=hidden name=site_code value='<?php echo $site_code; ?>' />
						<table style="padding: 5px" class=noborder>
							<tr>
								<td colspan=2 style="font-size: 10pt; line-height: 14pt; text-align: center; font-weight: bold; color: #000066; ">Reset your password:</td>
							</tr>
							<tr>
								<td style='font-weight: bold;'>User:</td>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=user_name id=user_name class='req' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Company Code:</th>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=cmpcode id=cmpcode class='req' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Security Question:</th>
								<td class=noborder style="padding: 5px"><select id=security_question name=security_question><?php
								foreach($securityQs as $i => $s) {
									echo "<option ".($i==1 ? "selected" : "")." value=".$i.">".$s."</option>";
								}
								?></select></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Answer:</th>
								<td class=noborder style="padding: 5px"><input type=text name=security_answer id=security_answer /></td>
							</tr>
							<tr>
								<td style='text-align: center; font-size: 8pt;'></td>
								<td  class="noborder" style="padding: 5px; text-align: center"><input type=button id=btn_forgot class=isubmit value="Request new password" style='font-weight: bold; color: #009900; padding: 2px 8px 2px 8px;' /></td>
							</tr>
						</table>
				</form>
			</td></tr></table>
						
<?php }
?>
				</td></tr></table></form>
	  
	  </div>
	  <script type=text/javascript>
		$(function() {
			var site_code = "<?php echo $site_code; ?>";
			$("#btn_forgot").click(function() {
				var dta = AssistForm.serialize($("form[name=frm_forgot]"));
				//alert(dta);
				var result = AssistHelper.doAjax("forgot_process.php",dta);
				//console.log(result);
				$("<div />",{title:"Password Reset",html:AssistHelper.getHTMLResult(result[0],result[1],"")}).dialog({
					close: function() {
							if(result[0]=="ok") {
								document.location.href = "index.php?<?php echo $_SERVER['QUERY_STRING']; ?>";
							}
					},
					buttons: [ { text: "Ok", click: function() { $( this ).dialog( "close" ); } } ] 
				}).css("font-size","75%");
			});
		});
	  </script>
	  
	  
	</div>
  </div>
<div id="side-column">
  <div class="spacer">

<div class=section>
     <span style='float: right; margin-top: -5px'> <a href="http://www.actionassist.co.za/modules/overview.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=100px /></a></span>
      <h3><?php echo $site_name; ?> Modules</h3>
<p><?php echo $site_name; ?> can help your organisation in:</p>
<?php foreach($random as $random_module) { ?>
<div class=section_module>
<?php
echo "<h4>".$sections[$random_module]['name']."</h4>
<p>".$sections[$random_module]['desc']."</p>";
?>
</div>
<?php } ?>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Divider" class="divider" />
</div>
    <div class="section">
     <span style='float: right'> <a href="http://www.actionassist.co.za/faq.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=100px /></a></span>
      <h3>Frequently Asked Questions</h3>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Divider" class="divider" />
    </div>
	
	
<?php if(isset($_SERVER['HTTPS'])) { ?><div id="thawteseal" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
<div style='float:right'><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=assist.ignite4u.co.za&amp;size=S&amp;lang=en"></script></div>
</div><?php } ?>	
	
	
	
	
  </div>
</div>
<!--[if IE 6]><br class="clear"/><![endif]-->
</div>
<img id="column-divider-bottom" src="/pics/frontpage/images/bg-hr-column.png" alt="divider" />

      </div><!-- #content-frame -->
      <div id="footer">
        <div id="footer-top-row">
          <a href="http://www.actionassist.co.za/" style='float:right'>www.actionassist.co.za</a>
        </div>
        <div id="footer-middle-row">
          <a href="http://www.actionassist.co.za/terms.php">Terms &amp; Conditions</a> |          
          <a href="http://www.actionassist.co.za/privacy.php">Privacy Policy</a> |
          <a href="http://www.actionassist.co.za/legal.php">Legal</a> |
          <a href="http://www.actionassist.co.za/IRS - Promotion of Access to Information 20111206.pdf">PAIA Manual</a>
        </div>        
		<div>Action iT (Pty) Ltd is an authorised Financial Services Provider (FSP no. 40003).</div>
        <div id="footer-bottom-row"><p>&copy; <?php echo date("Y"); ?> Action iT (Pty) Ltd. All Rights Reserved.</p></div>        
      </div><!-- #footer -->  
    </div><!-- #site-frame -->    
		
       
