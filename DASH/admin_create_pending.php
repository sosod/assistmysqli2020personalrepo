<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreateProgress.php");
?>
<script type=text/javascript>
function cancelDash(r) {
    if(!isNaN(parseInt(r)))
    {
        if(confirm("Are you sure you wish to cancel dashboard "+r+"?")==true)
        {
            document.location.href = "admin_create_pending.php?ref="+r+"&cancelact=Cancel";
        }
    }
    else
    {
        alert("An error occurred.  Please reload the page and try again.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Admin - Incomplete New Dashboards</b></h1>
<?php displayResult($result); ?>
<table cellpadding=3 cellspacing=0 width=100%>
    <tr height=30>
        <th width=40>Ref</th>
        <th>Dashboard Name</th>
        <th width=150>Last Modified</th>
        <th width=170>Last Step Completed</th>
        <th width=120>&nbsp;</th>
    </tr>
<?php
    $sql = "SELECT * FROM ".$dbref."_dashboard WHERE dashstatus LIKE 'P_%' AND dashowner = '$tkid'";
    include("inc_db_con.php");
        while($dash = mysql_fetch_array($rs))
        {
            $ref = $dash['dashid'];
            $status = $dash['dashstatus'];
            $name = $dash['dashname'];
            $sql2 = "SELECT * FROM ".$dbref."_dashboard_status WHERE statdashid = $ref ORDER BY statid DESC LIMIT 1";
            include("inc_db_con2.php");
                $stat = mysql_fetch_array($rs2);
            mysql_close($con2);
            $lastmod = $stat['statmoddate'];
            if(strlen($lastmod)==0) { $lastmod = $dash['dashadddate']; }
            
            //status
            $status = strFn("explode",$status,"_","");
            $status = strFn("explode",$status[1],"-","");
            if(count($status)==1)
            {
                $status = "New";
                $next = "admin_create1-1.php?ref=".$ref;
            }
            else
            {
                $p1 = $status[0];
                $p2 = $status[1];
                $status = $p1.".".$p2." ".$process[$p1][$p2]['txt'];
                $next = "admin_create".$process[$p1][$p2]['next'].".php?ref=".$ref;
            }
            ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader style="border-bottom-color: #ffffff;"><?php echo($ref); ?></td>
        <td style="font-weight: bold; padding-left: 10px;"><?php echo($name); ?></td>
        <td width=150  style="text-align:center;"><?php echo($lastmod); ?></td>
        <td width=170><?php echo($status); ?></td>
        <td width=120 ><input type=button value=Continue id=<?php echo("move".$ref); ?> onclick="document.location.href = '<?php echo($next); ?>';"> <input type=button value=Cancel onclick="cancelDash(<?php echo($ref); ?>);"></td>
    </tr>
            <?php
//                echo("<script type=text/javascript>document.getElementById('move".$ref."').disabled = true;</script>");
        }
    mysql_close($con);
?>
</table>
<?php
$urlback = "admin.php";
include("inc_goback.php");
?>


</body>

</html>
