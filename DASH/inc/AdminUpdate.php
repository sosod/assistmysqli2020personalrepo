<?php
switch($_SERVER['PHP_SELF'])
{
    case "/DASH/admin_update_draw3.php":
        $result = getUpdate();
        $data = $result[0];
        $values = $result[1];
        $result = array();
        break;
    case "/DASH/admin_update_draw4.php":
        $result = setUpdate();
        break;
}


function getUpdate() {
    global $variables;
    global $dbref;
    global $cmpcode;
    $data = array();

//    print_r($variables);
    
    $ref = $variables['ref'];
    $timeid = $variables['timeid'];
    $frameid = $variables['frame'];
    
    if(!checkIntRef($ref) || !checkIntRef($timeid) || !(strlen($frameid)>0))
    {
        die("An error has occurred.  Please go back and try again.");
    }
    else
    {
        $dloc = $variables['dloc'];
        $dtime = $variables['dtime'];
        $dval = $variables['dval'];
        $dfn = $variables['dfn'];
        $d = count($dloc);
        for($d2=0;$d2<$d;$d2++)
        {
            $loc = strFn("explode",$dloc[$d2],"-","");
            $time = $dtime[$d2];
            $fn = $dfn[$d2];
            $val = $dval[$d2];
            $data[$loc[0]][$loc[1]][$loc[2]][$loc[3]][$loc[4]] = array(
                "val"=>$val,
                "time"=>$time,
                "fn"=>$fn
            );
            if(is_numeric($val))
            {
                $values[$loc[0]][$loc[1]][$loc[2]][$loc[3]][$loc[4]] = $val;
            }
        }
    }
    return array($data,$values);
}


function setUpdate() {
    global $variables;
    global $dbref;
    global $cmpcode;
    global $tkid;
    $result[0] = "error";
    $result[1] = "An error has occurred.";
    $result[2] = $variables['ref'];

    //print_r($variables);

    $ref = $variables['ref'];
    
    
    if(!checkIntRef($ref))
    {
        die("An error has occurred.  Please go back and try again.");
    }
    else
    {
        $dataid = $variables['dataid'];
        $action = $variables['action'];
        $sql = "UPDATE ".$dbref."_dashboard_data SET datastatus = '$action' WHERE dataid IN (".strFn("implode",$dataid,",","").")";
        include("inc_db_con.php");
        $mar = mysql_affected_rows($con);
        if($mar>0)
        {
            if($action=="Y") {
                $result[0] = "check";
                $result[1] = "Data successfully updated.";
                $comment = code($variables['comment']);
//                echo(":".$comment.":");
                if(strlen($comment)>0) {
                    $timeid = $variables['timeid'];
                    $frame = $variables['frame'];
                    $f = strFn("explode",$frame,"_","");
                    $fr = getSection($f[0],$ref,$f[1]);
                    $dfid = $fr[0]['dfid'];
//                    echo($timeid.":".$dfid.":".$frame.":");
//                    print_r($f); echo(":"); print_r($fr);
                    if(checkIntRef($timeid) && checkIntRef($dfid)) {
                        $sql = "INSERT INTO ".$dbref."_dashboard_comments (dcdashid, dctimeid, dcdfid, dcdate, dctkid, dccomment) ";
                        $sql.= "VALUES ($ref, $timeid, $dfid, now(), '$tkid', '$comment')";
//                        echo($sql);
                        include("inc_db_con.php");
                    }
                }
            } else {
                $result[0] = "error";
                $result[1] = "Update rejected.";
            }
        }
        else
        {
            $result[0] = "error";
            $result[1] = "An error has occurred.  Please go back and try again.";
        }
    }
    return $result;
}
?>
