<?php
function getNAWard() {
    global $cmpcode;
    global $dbref;
    
    $mnr = 0;
    $sql = "SELECT * FROM ".$dbref."_list_wards WHERE yn = 'Y' AND value = 'N/A'";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
        $naward = mysql_fetch_array();
    mysql_close($con);
    
    if($mnr==0)
    {
        $naward = array();
        $sql = "INSERT INTO ".$dbref."_list_wards (value, yn) VALUES ('N/A','Y')";
        include("inc_db_con.php");
        $naward['id'] = mysql_insert_id($con);
        $naward['value'] = "N/A";
        $naward['yn'] = "Y";
    }
    
    return $naward;
}

function getCaptureFreq() {
    global $cmpcode;
    global $dbref;
    $types = array();
    $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE yn = 'Y' AND listid = 1 ORDER BY sort, value";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $code = $row['id'];
        $types[$code] = $row;
    }
    mysql_close($con);

    return $types;
}

function getTime($id) {
    global $cmpcode;
    global $dbref;
    $types = array();
    $sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ";
    if(checkIntRef($id)) { $sql.= "AND id = $id "; }
    $sql.= "ORDER BY sort";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $code = $row['id'];
        $types[$code] = $row;
    }
    mysql_close($con);

    return $types;
}

function getTypes() {
    global $cmpcode;
    global $dbref;
    $types = array();
    $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE yn = 'Y' AND listid = 3 ORDER BY sort, value";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $code = $row['code'];
        $types[$code] = $row;
    }
    mysql_close($con);

    return $types;
}

function getFunctions() {
    global $cmpcode;
    global $dbref;
    $types = array();
    $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE yn = 'Y' AND listid = 2 ORDER BY sort, value";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $code = $row['code'];
        $types[$code] = $row;
    }
    mysql_close($con);

    return $types;
}

function getMultiFn() {
    global $cmpcode;
    global $dbref;
    $types = array();
    $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE yn = 'Y' AND listid = 2 AND comment = 'MULTI' ORDER BY sort, value";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $code = $row['code'];
        $types[$code] = $row['id'];
    }
    mysql_close($con);

    return $types;
}

function getDashboard($ref) {
    global $cmpcode;
    global $dbref;

    $sql = "SELECT * FROM ".$dbref."_dashboard WHERE dashid = $ref";
    include("inc_db_con.php");
        $dash = mysql_fetch_array($rs);
    mysql_close($con);
    return $dash;
}

function getRecords($ref) {
    global $cmpcode;
    global $dbref;
    global $recid;
    $recs = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_records WHERE recdashid = $ref AND recyn = 'Y' ";
    if(checkIntRef($recid)) { $sql.= "AND recid = $recid "; }
    $sql.= "ORDER BY recsort, rectxt, recid";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $recs[] = $row;
        }
    mysql_close($con);
    return $recs;

}

function getRecord($recid) {
    global $cmpcode;
    global $dbref;
    $recs = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_records WHERE recid = $recid AND recyn = 'Y' ";
    include("inc_db_con.php");
        $recs = mysql_fetch_array($rs);
    mysql_close($con);
    return $recs;

}


function getFields($ref) {
    global $cmpcode;
    global $dbref;
    //global $fldid;
    $recs = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_fields WHERE flddashid = $ref AND fldyn = 'Y' ";
    //if(checkIntRef($fldid)) { $sql.= " AND fldid <> $fldid "; }
    $sql.= "ORDER BY fldsort, fldtxt, fldid";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $recs[] = $row;
        }
    mysql_close($con);
    return $recs;

}

function getField($fldid) {
    global $cmpcode;
    global $dbref;
//    global $recid;
    $recs = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_fields WHERE fldid = $fldid AND fldyn = 'Y' ";
    include("inc_db_con.php");
        $recs = mysql_fetch_array($rs);
    mysql_close($con);
    return $recs;

}


function getVariables($ref) {
    global $cmpcode;
    global $dbref;

    $vars = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_variables WHERE vardashid = $ref AND varfnsrc <> 'SUB' AND varfnsrc <> 'DIR' AND varfnsrc <> 'SUBF' AND varfnsrc <> 'DIRF' ";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $varid = $row['varid'];
        $recid = $row['varrecid'];
        $fldid = $row['varfldid'];
        $vtype = $row['vartype'];
        $vunit = $row['varunit'];
        $vfn = $row['varfn'];
        $vfnsrc = $row['varfnsrc'];

        $vars[$recid][$fldid] = array(
            "varid"=>$varid,
            "vartype"=>$vtype,
            "varunit"=>$vunit,
            "varfn"=>$vfn,
            "varfnsrc"=>$vfnsrc
        );
    }
    mysql_close($con);
    $sql = "SELECT * FROM ".$dbref."_dashboard_variables WHERE vardashid = $ref AND (varfnsrc LIKE 'SUB%' OR varfnsrc LIKE 'DIR%') ";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $varid = $row['varid'];
        $recid = $row['varfnsrc'];
        $fldid = $row['varfldid'];
        $vtype = $row['vartype'];
        $vunit = $row['varunit'];
        $vfn = $row['varfn'];
        $vfnsrc = $row['varfnsrc'];

        $vars[$recid][$fldid] = array(
            "varid"=>$varid,
            "vartype"=>$vtype,
            "varunit"=>$vunit,
            "varfn"=>$vfn,
            "varfnsrc"=>$vfnsrc
        );
    }
    mysql_close($con);
    return $vars;
}

function getVariablesData($ref) {
    global $cmpcode;
    global $dbref;

    $vars = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_variables WHERE vardashid = $ref AND varfnsrc <> 'SUB' AND varfnsrc <> 'DIR' AND varfnsrc <> 'SUBF' AND varfnsrc <> 'DIRF' ";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $varid = $row['varid'];
        $recid = $row['varrecid'];
        $fldid = $row['varfldid'];
        $vtype = $row['vartype'];
        $vunit = $row['varunit'];
        $vfn = $row['varfn'];
        $vfnsrc = $row['varfnsrc'];

        $vars[$recid][$fldid] = array(
            "varid"=>$varid,
            "vartype"=>$vtype,
            "varunit"=>$vunit,
            "varfn"=>$vfn,
            "varfnsrc"=>$vfnsrc
        );
    }
    mysql_close($con);
    return $vars;
}

function getVariablesTotals($ref) {
    global $cmpcode;
    global $dbref;

    $vars = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_variables WHERE vardashid = $ref AND (varfnsrc LIKE 'SUB%' OR varfnsrc LIKE 'DIR%') ";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $varid = $row['varid'];
        $recid = $row['varfnsrc'];
        $fldid = $row['varfldid'];
        $vtype = $row['vartype'];
        $vunit = $row['varunit'];
        $vfn = $row['varfn'];
        $vfnsrc = $row['varfnsrc'];

        $vars[$recid][$fldid] = array(
            "varid"=>$varid,
            "vartype"=>$vtype,
            "varunit"=>$vunit,
            "varfn"=>$vfn,
            "varfnsrc"=>$vfnsrc
        );
    }
    mysql_close($con);
    return $vars;
}


function getVariablesFn($varid) {
    global $dbref;
    global $cmpcode;

    $fid = array();
        $sql = "SELECT * FROM ".$dbref."_dashboard_variables_fn WHERE fnvarid = $varid ORDER BY fnsort";
        include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $v++;
            $fid[$v] = $row['fnforeignid'];
        }
        mysql_close($con);
    return $fid;
}


function getDataMonth($ref,$time,$precision,$timedate) {   //ref = int, time = array(start,end), precision = str
    global $cmpcode;
    global $dbref;

    $data = array();
    
    if(strlen($timedate)>0)
    {
        $dt = strFn("explode",$timedate,"-","");
        $tday = $dt[0]*1;
        $tmon = $dt[1]*1;
        $tyear = $dt[2];
    }

//    $sql = "SELECT * FROM ".$dbref."_dashboard_data WHERE datastatus = 'Y' AND datadashid = $ref AND datatimeid >= ".$time[0]." AND datatimeid <= ".$time[1];
$sql = "SELECT d.*, v.vartype, v.varunit ";
$sql.= "FROM ".$dbref."_dashboard_data d ";
$sql.= ", ".$dbref."_dashboard_variables v ";
$sql.= "WHERE datadashid = $ref ";
$sql.= "AND vardashid = datadashid ";
$sql.= "AND varrecid = datarecid ";
$sql.= "AND varfldid = datafldid ";
$sql.= "AND datatimeid >= ".$time[0]." ";
$sql.= "AND datatimeid <= ".$time[1];
if(strlen($timedate)>0) {
    $sql.= " AND datatimeday = $tday AND datatimemonth = $tmon AND datatimeyear = $tyear ";
}
//echo($sql);
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
//        echo("<P>".$row['dataid']."<br>");
//        print_r($row);
        $d = $row['datadirid'];
        $s = $row['datasubid'];
        $f = $row['dataframeid'];
        $r = $row['datarecid'];
        $l = $row['datafldid'];
        $val = $row['datavalue'];
        $timeid = $row['datatimeid'];
        $vt = $row['vartype'];
        if($vt=="NUM" || $vt == "PERC" || $vt == "R")
        {
            $data[$timeid][$d][$s][$f][$r][$l]['value'] = $data[$timeid][$d][$s][$f][$r][$l]['value'] + $val;
        }
        else
        {
            if($vt=="DT" || $vt=="LT")
            {
                $data[$timeid][$d][$s][$f][$r][$l]['value'].= ",".$val;
            }
            else
            {
                if(strlen($data[$timeid][$d][$s][$f][$r][$l]['value'])>0)
                {
                    $data[$timeid][$d][$s][$f][$r][$l]['value'].= "<br>";
                }
                $data[$timeid][$d][$s][$f][$r][$l]['value'].=strFn("str_replace",$val,chr(10),"<br>");
            }
        }
//        $data[$timeid][$d][$s][$f][$r][$l]['vt'] = $vt;
//        echo("<BR>".$data[$timeid][$d][$s][$f][$r][$l]['vt']."-".$data[$timeid][$d][$s][$f][$r][$l]['value']);
        
        if($precision=="exact")
        {
/*            $td = $row['datatimeday'];
            $tm = $row['datatimemonth'];
            $ty = $row['datatimeyear'];
            $data[$timeid][$d][$s][$f][$r][$l]['td'] = $td;
            $data[$timeid][$d][$s][$f][$r][$l]['tm'] = $tm;
            $data[$timeid][$d][$s][$f][$r][$l]['ty'] = $ty;*/
        }
    }
    mysql_close($con);
    return $data;
}


function getKPI($dash,$dir,$sub,$frame,$rec,$fld) {
    global $dbref;
    global $cmpcode;

    $sql = "SELECT k.* FROM ".$dbref."_dashboard_kpi dk, assist_".$cmpcode."_sdp09_kpi k WHERE ";
            $sql.= " dkdashid = $dash ";
            $sql.= " AND dkdirid = $dir AND dksubid = $sub ";
            $sql.= " AND dkframeid = $frame ";
            $sql.= " AND dkrecid = $rec AND dkfldid = $fld ";
            $sql.= " AND dkkpiid = kpiid AND dkyn = 'Y' AND kpiyn = 'Y'";
            if($sub > 0)
            {
                $sql.= " AND kpisubid = $sub ";
            }
            include("inc_db_con.php");
                $kpirow = mysql_fetch_array($rs);
            mysql_close($con);
    return $kpirow;
}

function getAdmins($ref,$type,$foreignid) {
    global $dbref;
    global $cmpcode;
    $tk = array();

    if(checkIntRef($ref))
    {
        $sql = "SELECT tkid,tkname,tksurname,daprimary FROM ".$dbref."_dashboard_admins, ".$dbref."_dashboard_frame, assist_".$cmpcode."_timekeep ";
        $sql.= " WHERE tkstatus = 1 ";
        $sql.= " AND tkid = datkid AND dayn = 'Y' AND datype = '$type' ";
        $sql.= " AND daframeid = dfid AND dfyn = 'Y' AND dfdashid = $ref AND dftype = '$type' AND dfforeignid = $foreignid ";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $p = $row['daprimary'];
                $tk[$p] = $row;
            }
        mysql_close($con);
    }

    return $tk;
}

function getDirectorates() {
    global $dbref;
    global $cmpcode;
    
    $list = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_list_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $i = $row['dirid'];
            $list[$i] = $row;
        }
    mysql_close($con);
    
    return $list;
}

function getSubDirectorates() {
    global $dbref;
    global $cmpcode;

    $list = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_list_dirsub WHERE subyn = 'Y' ORDER BY subsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $i = $row['subdirid'];
            $z = $row['subid'];
            $list[$i][$z] = $row;
        }
    mysql_close($con);

    return $list;
}

function getWards() {
    global $dbref;
    global $cmpcode;

    $list = array();
    $sql = "SELECT * FROM ".$dbref."_list_wards WHERE yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $i = $row['id'];
            $list[$i] = $row;
        }
    mysql_close($con);

    return $list;
}

function getTowns() {
    global $dbref;
    global $cmpcode;

    $list = array();
    $sql = "SELECT * FROM ".$dbref."_list_towns WHERE yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $i = $row['id'];
            $list[$i] = $row;
        }
    mysql_close($con);

    return $list;
}

function getDashAdmins($type,$ref) {
    global $dbref;
    global $cmpcode;
    $tk = array();

    if(checkIntRef($ref))
    {
        $sql = "SELECT tkid,tkname,tksurname,daprimary,dfid FROM ".$dbref."_dashboard_admins, ".$dbref."_dashboard_frame, assist_".$cmpcode."_timekeep ";
        $sql.= " WHERE tkstatus = 1 ";
        $sql.= " AND tkid = datkid AND dayn = 'Y' AND datype = '$type' ";
        $sql.= " AND daframeid = dfid AND dfyn = 'Y' AND dfdashid = $ref AND dftype = '$type' ";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $p = $row['daprimary'];
                $df = $row['dfid'];
                $tk[$df][$p] = $row;
            }
        mysql_close($con);
    }

    return $tk;
}

function getSections($type,$ref) {
    global $dbref;
    global $cmpcode;
    $list = array();

    if(checkIntRef($ref))
    {
        switch($type)
        {
            case "D":
                $sql = "SELECT dfid, dfforeignid id, dirtxt txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, assist_".$cmpcode."_list_dir ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = dirid ";
                $sql.= "AND diryn = 'Y'";
                $sql.= " ORDER BY dirsort";
                break;
            case "S":
                $sql = "SELECT dfid, dfforeignid id, subtxt txt, subdirid  FROM ".$dbref."_dashboard_frame, assist_".$cmpcode."_list_dirsub ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = subid ";
                $sql.= "AND subyn = 'Y'";
                $sql.= " ORDER BY subsort";
                break;
            case "T":
                $sql = "SELECT dfid, dfforeignid id, value txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, ".$dbref."_list_towns ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = id ";
                $sql.= "AND yn = 'Y'";
                $sql.= " ORDER BY value";
                break;
            case "W":
                $sql = "SELECT dfid, dfforeignid id, value txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, ".$dbref."_list_wards ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = id ";
                $sql.= "AND yn = 'Y'";
                $sql.= " ORDER BY value";
                break;
        }
        include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            if($type == "S")
            {
                $list[$row['subdirid']][] = $row;
            }
            else
            {
                $list[] = $row;
            }
        }
        mysql_close($con);
    }
    return $list;
}

function getSection($type,$ref,$id) {
    global $dbref;
    global $cmpcode;
    $list = array();

    if(checkIntRef($ref) && checkIntRef($id))
    {
        switch($type)
        {
            case "D":
                $sql = "SELECT dfid, dfforeignid id, dirtxt txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, assist_".$cmpcode."_list_dir ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = dirid ";
                $sql.= "AND diryn = 'Y' AND dirid = $id";
                $sql.= " ORDER BY dirsort";
                break;
            case "S":
                $sql = "SELECT dfid, dfforeignid id, subtxt txt, subdirid  FROM ".$dbref."_dashboard_frame, assist_".$cmpcode."_list_dirsub ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = subid ";
                $sql.= "AND subyn = 'Y' AND subid = $id";
                $sql.= " ORDER BY subsort";
                break;
            case "T":
                $sql = "SELECT dfid, dfforeignid id, value txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, ".$dbref."_list_towns ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = id ";
                $sql.= "AND yn = 'Y' AND id = $id";
                $sql.= " ORDER BY value";
                break;
            case "W":
                $sql = "SELECT dfid, dfforeignid id, value txt, 0 as subdirid FROM ".$dbref."_dashboard_frame, ".$dbref."_list_wards ";
                $sql.= "WHERE dfdashid = $ref AND dfyn = 'Y' AND dftype = '$type' ";
                $sql.= "AND dfforeignid = id ";
                $sql.= "AND yn = 'Y' AND id = $id";
                $sql.= " ORDER BY value";
                break;
        }
        include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            if($type == "S")
            {
                $list[$row['subdirid']][] = $row;
            }
            else
            {
                $list[] = $row;
            }
        }
        mysql_close($con);
    }
    return $list;
}

function getList() {
    global $dbref;
    global $cmpcode;
    $list = array();
            $sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE yn = 'Y' ORDER BY value";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $list[] = $row;
                }
            mysql_close($con);
    return $list;
}


function getLt($i) {
    global $dbref;
    global $cmpcode;
    $list = array();
    if(checkIntRef($i)) {
            $sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE yn = 'Y' AND id = $i ORDER BY value";
            include("inc_db_con.php");
                $list = mysql_fetch_array($rs);
            mysql_close($con);
    }
    return $list;
}



function getComments($dashid,$timeid,$dfid) {
    global $dbref;
    global $cmpcode;
    $comments = array();

    $sql = "SELECT * FROM ".$dbref."_dashboard_comments WHERE dcdashid = $dashid AND dctimeid = $timeid AND dcdfid = $dfid ORDER BY dcdate DESC";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $comments[] = $row;
    }
    mysql_close($con);

    return $comments;
}
 ?>
