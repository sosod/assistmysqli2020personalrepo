<?php
$step = $variables['step'];
$stepact = $variables['stepact'];
if($stepact=="save")
{
    switch($step)
    {
        case "2-1":
            saveStep21();
            break;
        case "2-2":
            saveStep22();
            break;
        case "2-3":
            $nextaction = $variables['nextaction'];
            if($nextaction!="edit") {
                $result = saveStep23();
            } else {
                $recid = $variables['recid'];
            }
            break;
        default:
            $result[0] = "error";
            $result[1] = "Nothing to do";
            break;
    }
}



function saveStep21() {
    global $dbref;
    global $cmpcode;
    global $variables;
    
    $ref = $variables['ref'];
    $next = $variables['next'];
    $old = $variables['oldstep'];
        changeStatus($ref,$old,"E_2-1"); //dashid,statold,statnew
}

function saveStep22() {
    global $cmpcode;
    global $dbref;
    global $variables;
    global $tkid;
    $ref = $variables['ref'];
    $action = $variables['nextaction'];
                        $res[0] = "error";
                        $res[1] = "An error occurred. Please try again.";
                        $res[2] = $ref;
    if(checkIntRef($ref))
    {
        $recsort = $variables['recsort']+1;
        $ddid = $variables['ddid'];
        foreach($ddid as $d)
        {
            if(checkIntRef($d))
            {
                $recsort++;
                $sql = "INSERT INTO ".$dbref."_dashboard_records (recdashid,rectxt,recfn,recyn,recsort) ";
                $sql.= "SELECT $ref, value, 'N', 'Y', $recsort FROM ".$dbref."_list_dropdowns_values WHERE id = $d ";
                include("inc_db_con.php");
            }
        }
        changeStatus($ref,"E_2-1","E_2-2"); //dashid,statold,statnew
        $res[0] = "check";
        $res[1] = "Records saved.";
        $res[2] = $ref;
    }
    else
    {
        $res[0] = "error";
        $res[1] = "An error has occurred.";
        $res[2] = $ref;
    }
    return $res;
}

function saveStep23() {
    global $cmpcode;
    global $dbref;
    global $variables;
    global $tkid;
    $ref = $variables['ref'];
    $action = $variables['nextaction'];
                        $res[0] = "error";
                        $res[1] = "An error occurred. Please try again.";
                        $res[2] = $ref;

    if(checkIntRef($ref))
    {
        $dash = getDashboard($ref);
        if($dash['dashstatus']!="E_2-3") { //not on refresh
            switch($action)
            {
                case "delete":
                    $recid = $variables['recid'];
                    if(checkIntRef($recid))
                    {
                        $sql = "UPDATE ".$dbref."_dashboard_records SET recyn = 'N' WHERE recid = $recid AND recdashid = $ref";
                        include("inc_db_con.php");
                        $mar = mysql_affected_rows($con);
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "Record deleted subcessfully.";
                            $res[2] = $ref;
                        }
                        else
                        {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                        }
                    } else {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                    }
                    break;
                case "edit":
                    break;
                case "editsave":
                    $recid = $variables['recid'];
                    if(checkIntRef($recid))
                    {
                        $rect = $variables['rn'];
                        $recf = $variables['fn'];
                        $sql = "UPDATE ".$dbref."_dashboard_records SET rectxt = '$rect', recfn = '$recf' WHERE recid = $recid AND recdashid = $ref ";
                        include("inc_db_con.php");
                        $mar = mysql_affected_rows($con);
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "Record $recid updated.";
                            $res[2] = $ref;
                        }
                        else
                        {
                            $res[0] = "error";
                            $res[1] = "An error occurred while attempting to update record $recid.  No change was made.";
                            $res[2] = $ref;
                        }
                    } else {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                    }
                    break;
                default:
                    $rn = $variables['rn'];
                    $rf = $variables['fn'];
                    $recsort = $variables['recsort'];
                    $res[0] = "check";
                    $res[1] = "Records saved.";
                    $res[2] = $ref;
                    for($r=0;$r<count($rn);$r++)
                    {
                        $rectxt = $rn[$r];
                        $recfn = $rf[$r];
                        if(strlen($rectxt)>0)
                        {
                            if(strlen($recfn)==0) { $recfn = "N"; }
                            $rectxt = code($rectxt);
                            $sql = "INSERT INTO ".$dbref."_dashboard_records (recdashid,rectxt,recfn,recyn,recsort) VALUES ";
                            $sql.= "($ref,'$rectxt','$recfn','Y',$recsort)";
                            include("inc_db_con.php");
                            $recsort++;
                        }
                    }
                    break;
            }
        } else {
            $res = array();
        }
    }
    else
    {
        $res[0] = "error";
        $res[1] = "An error has occurred.";
        $res[2] = $ref;
    }

    if($action=="preview" && $dash['dashstatus']!="E_2-3")
    {
        $statold = $variables['oldstep'];
        changeStatus($ref,$statold,"E_2-3"); //dashid,statold,statnew
    }
    
    return $res;
}
?>
