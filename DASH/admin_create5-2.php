<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreate5.php");
include("inc/AdminCreateProgress.php");
$step = "5-2";
$page = array("next"=>"6-1","step"=>"2","title"=>"Preview");

?>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 5.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);


echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
echo("<form name=frm id=frm action=\"admin_create_process.php\" method=post>");
echo("<input type=hidden name=step value=\"5-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");
?><div align=center><?php

//print_r($kpis);

previewKPI();
?>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type=button value="<-- Go Back" onclick="document.location.href = 'admin_create5-1.php?ref=<?php echo($ref); ?>';"> <input type=hidden name=acttype value="" id=actt><select name=act><option selected value=move>Continue creation</option><option value=pause>Pause dashboard creation</option></select> <input type="button" value="Accept" onclick="moveOn('Y');"> <input type=button value="Reject" onclick="moveOn('N');"> <input type=button value=Cancel onclick="moveOn('C');"> <input type=button value="Preview" onclick="previewDash(<?php echo($ref); ?>);"></td>
	</tr>
</table>
</form>
<?php

        $stepprogress = setProgress(5);
        $totalprogress = setProgress(0);
        displayProgress("Step 5 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
