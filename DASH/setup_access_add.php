<?php
include("inc_head_setup.php");
include("inc_admin.php");
?>
<h1><b><?php echo($modtitle); ?>: Setup - User Access</b></h1>
<p>Processing...</p>
<?php
//GET VARIABLES PASSED BY FORM
$act = $_POST['act'];
$start = $_POST['s'];
if(!is_numeric($start) || strlen($start)==0) { $start = 0; }
$tid = $_POST['tid'];
$d = $_POST['d'];
$a = $_POST['a'];
$u = $_POST['u'];
$v = $_POST['v'];
$r = $_POST['r'];
if($act=="single")
{
                $tk = $tid;
                $uv = $v;
                $ur = $r;
                $ua = $a;
                $ud = $d;
                $uu = $u;
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, dash, activate, capture, view, report) VALUES ";
                $sql.= "('$tk','Y','$ud','$ua','$uu','$uv','$ur')";
                include("inc_db_con.php");
                    $tsql = $sql;
                    $trans = "Added user access for user ".$tk;
                    include("inc_transaction_log.php");
                $res = "User added to ".$modtitle;
}
else
{
    if($act == "all")
    {
        $tc = count($tid);
        $vc = count($v);
        $rc = count($r);
        $ac = count($a);
        $dc = count($d);
        $uc = count($u);
        $c = $tc+$dc+$vc+$rc+$ac+$uc;
        $c = $c/6;
        if($c!=$tc || $c != $dc || $c != $vc || $c != $rc || $c != $ac || $c != $uc || $c != round($c))
        {
            $res = "ERROR - Incomplete data";
        }
        else
        {
            $steps = count($tid);
            for($i=0;$i<$steps;$i++)
            {
                $tk = $tid[$i];
                $uv = $v[$i];
                $ur = $r[$i];
                $ua = $a[$i];
                $ud = $d[$i];
                $uu = $u[$i];
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, dash, activate, capture, view, report) VALUES ";
                $sql.= "('$tk','Y','$ud','$ua','$uu','$uv','$ur')";
                            include("inc_db_con.php");
                                $tsql = $sql;
                                $trans = "Added user access for user ".$tk;
                                include("inc_transaction_log.php");
            }
            $res = "Users added to ".$modtitle;
        }
    }
    else
    {
        $res = "ERROR - Unknown error.";
    }
}

?>
<script type=text/javascript>
var res = "<?php echo($res); ?>";
res = escape(res);
document.location.href = "setup_access.php?a=access&s=<?php echo($start); ?>&r="+res;
</script>
</body></html>
