<?php
include("inc_head.php");
include("inc/Admin.php");
$frequency = getCaptureFreq();
$time = getTime(0);
//print_r($setup);
$variables = $_REQUEST;
$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}
if($access['dash']!="Y") {
    die("You do not have permission to view this page.");
}
?>
<script type=text/javascript>
function goDash() {
    var ref = document.getElementById('ref').value;
    var act = document.getElementById('action').value;
    if(!isNaN(parseInt(ref)))
    {
        if(act == "X" || (act != "E" && act != "U" && act != "D" && act != "A"))
        {
            alert("Please select an action before clicking the Go button.");
        }
        else
        {
            switch(act)
            {
                case "A":
                    document.location.href = "admin_edit_users.php?dash=Y&ref="+ref;
                    break;
                case "D":
                    if(confirm("Are you sure you wish to delete this dashboard?")==true)
                    {
                        document.location.href = "admin_dash_delete.php?ref="+ref;
                    }
                    break;
                case "E":
                    document.location.href = "admin_edit2-1.php?ref="+ref;
                    break;
                case "U":
                    document.location.href = "admin_update_draw1.php?ref="+ref+"&dash=Y";
                    break;
            }
        }
    }
    else
    {
        alert("A problem occurred.  Please go back and try again.");
    }
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Dashboard Details</h1>
<table cellpadding=3 cellspacing=0 width=600>
	<tr height=21>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr height=21>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr height=21>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr height=21>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
<?php    $dashstatus = $dash['dashstatus'];
    if($dashstatus=="Y")
    {
        $status = "Active";
    }
    else
    {
        $ds = strFn("substr",$dashstatus,0,1);
        switch($ds)
        {
            case "E":
                $status = "Inactive - Edit";
                break;
            case "P":
                $status = "Inactive - New";
                break;
            default:
                $status = "Unknown";
                break;
        }
    } ?>
	<tr height=21>
		<td class="tdheaderl b-bottom-w b-top-w">Status:</td>
		<td><?php echo($status); ?></td>
	</tr>
</table>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td style="text-align:center">
            <select id=action>
                <option selected value=X>--- SELECT ---</option>
			<?php if($dashstatus == "Y") { ?>                
				<option value=E>Edit Structure</option>
                <option value=U>Update Data</option>  
			<?php } ?>
<?php if($dashstatus=="Y" || $ds == "E") { ?>                <option value=A>Update Admins</option><?php } ?>
                <option value=D>Delete Dashboard</option>
            </select><input type=hidden value="<?php echo($ref); ?>" id=ref>
            <input type=button value="  Go  " onclick="goDash();">
        </td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_dash.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
