<?php
include("inc_head_setup.php");
include("inc/setup.php");
include("inc_admin.php");
?>
<script type=text/javascript>
function delDir(i,tk,txt,d) {
    if(escape(i)==i && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you want to delete administrator '"+tk+"' from "+txt+"?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            var a = "admin";
            var act = "del"
            var dirid = d;
            var dir = i;
            document.location.href = "setup_admin_config.php?a="+a+"&act="+act+"&dirid="+dirid+"&dir="+dir;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
</script>
<?php
if(strlen($dirid)==0) { $dirid = $_GET['d']; }
    $sql = "SELECT * FROM assist_".$cmpcode."_list_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $dir = mysql_fetch_array($rs);
    mysql_close();
?>
<h1><?php echo($modtitle); ?>: Setup - Directorate ~ Administrators</h1>
<?php displayResult($result); ?>
<table cellpadding=3 cellspacing=0>
	<tr height=27>
		<th width=270>Directorate</td>
		<th width=330 colspan=2>Administrators</td>
	</tr>
		  <?php
/*            $sql = "SELECT a.id, t.tkname, t.tksurname ";
            $sql.= "FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t, ".$dbref."_list_users u ";
            $sql.= "WHERE a.yn = 'Y' AND a.type = 'D' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$dirid;
            $sql.= " AND u.tkid = t.tkid AND (u.dash = 'Y' OR u.activate = 'Y' OR u.capture = 'Y')";
            $sql.= " ORDER BY t.tkname, t.tksurname";
*/
            $sql = userList("D",$dirid,"L");
            include("inc_db_con.php");
                $rnum = mysql_num_rows($rs);
                if($rnum==0) { $rowspan = 1; } else { $rowspan = $rnum; }
                ?>
    <tr>
		<td rowspan=<?php echo($rowspan); ?> valign=top><b><?php echo($dir['dirtxt']); ?></b></td>
		
                <?php
                if(mysql_num_rows($rs)==0)
                {
                    echo("<td width=300 valign=top style=\"padding-left: 10px; border-left: 1px solid #dedede;\" height=26>No Administrators.</td>");
                    echo("<td  align=center valign=top><input type=button value=\" Add \" onclick=\"document.location.href = 'setup_admin_add.php?d=".$dirid."&s=0';\"></td>");
                }
                else
                {
                    $d=1;
                    while($row = mysql_fetch_array($rs))
                    {
                        if($d>1) { echo("<tr>"); }
                        $d++;
                        $id2 = $row['id'];
                        $val2 = $row['tkname']." ".$row['tksurname'];
                        $delvar = str_replace("'","\'",$val2);
                        $delsub = str_replace("'","\'",$dir['dirtxt']);
                        echo("<td width=300 valign=top style=\"padding-left: 10px; border-left: 1px solid #dedede;\">".$val2."</td>");
                        echo("<td width=30 align=center valign=top><input type=button value=\" Del \" onclick=\"delDir(".$id2.",'".$delvar."','".$delsub."',".$dirid.");\"></td>");
                        echo("</tr>");
                    }
                }
            mysql_close();
            ?>
	<tr height=27>
		<th>Sub-Directorates</td>
		<th colspan=2>Administrators</td>
	</tr>
		  <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_list_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con.php");
                while($sub = mysql_fetch_array($rs))
                {
                    $subid = $sub['subid'];
//            $sql2 = "SELECT a.id, t.tkname, t.tksurname FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t WHERE a.yn = 'Y' AND a.type = 'S' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$subid;
/*            $sql2 = "SELECT a.id, t.tkname, t.tksurname ";
            $sql2.= "FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t, ".$dbref."_list_users u ";
            $sql2.= "WHERE a.yn = 'Y' AND a.type = 'S' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$subid;
            $sql2.= " AND u.tkid = t.tkid AND (u.dash = 'Y' OR u.activate = 'Y' OR u.capture = 'Y')";
            $sql2.= " ORDER BY t.tkname, t.tksurname";
*/
            $sql2 = userList("S",$subid,"L");
//            echo("<P>$sql2");
            include("inc_db_con2.php");
                $rnum = mysql_num_rows($rs2);
                if($rnum==0) { $rowspan = 1; } else { $rowspan = $rnum; }
                ?>
    <tr>
		<td rowspan=<?php echo($rowspan); ?> valign=top><b><?php echo($sub['subtxt']); ?></b></td>

                <?php
                if(mysql_num_rows($rs2)==0)
                {
                    echo("<td valign=top style=\"padding-left: 10px; border-left: 1px solid #dedede;\" height=27>No Administrators.</td>");
                    echo("<td  align=center valign=top><input type=button value=\" Add \" onclick=\"document.location.href = 'setup_admin_add.php?d=".$dirid."&s=".$sub['subid']."';\"></td>");
                }
                else
                {
                    $d=1;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $id2 = $row2['id'];
                        $val2 = $row2['tkname']." ".$row2['tksurname'];
                        if($d>1) { echo("<tr>"); }
                        $d++;
                        $delvar = str_replace("'","\'",$val2);
                        $delsub = str_replace("'","\'",$sub['subtxt']);
                        echo("<td valign=top style=\"padding-left: 10px; border-left: 1px solid #dedede;\">".$val2."</td>");
                        echo("<td  align=center valign=top><input type=button value=\" Del \" onclick=\"delDir(".$id2.",'".$delvar."','".$delsub."',".$dirid.");\"></td>");
                        echo("</tr>");
                    }
                }
            mysql_close($con2);
            ?>
<?php
                }
            mysql_close();
            ?>
</table>
<p><input type=button value="Add New" onclick="document.location.href = 'setup_admin_add.php?d=<?php echo($dirid); ?>';"></p>
<?php
$urlback = "setup_admin.php";
include("inc_goback.php");

displayChangeLog("S_DIR",$dirid,$urlback);

?>
<p>&nbsp;</p>
</body>
</html>
