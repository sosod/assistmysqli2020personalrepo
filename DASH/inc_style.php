<?php
include("../styles/style_green.php");
?>
<style type="text/css">
body { font-size: 62.5%; }
h1 { color: #006600; }
h2 { color: #006600; }
h3 { color: #006600; }
h4 { color: #006600; margin-bottom: 10px; text-decoration: underline;}
table {
    border-collapse: collapse;
    border: 1px solid #ababab;
}
table td {
    border: 1px solid #ababab;
	color: #000000;
	font-weight: normal;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 10pt;
	text-align: left;
}
table th {
    border: 1px solid #ffffff;
	color: #ffffff;
	text-align: center;
	background-color: #006600;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
.thlight {
    border: 1px solid #ababab;
	color: #000000;
	text-align: center;
	background-color: #ccffcc;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
.tdlog {
	color: #555555;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7.5pt;
	line-height: 10pt;
    border-left: 1px solid #cecece;
    border-right: 1px solid #cecece;
    border-bottom: 1px solid #ababab;
}
.tdloghead {
	color: #ffffff;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7.5pt;
	line-height: 10pt;
    border-left: 1px solid #cecece;
    border-right: 1px solid #cecece;
    border-bottom: 1px solid #ababab;
    background-color: #909090;
    font-weight: bold;
    text-align: center;
}
.level1 {
	color: #ffffff;
	background-color: #006600;
	font-weight: bold;
	font-size: 8.5pt;
	line-height: 12pt;
	padding-left: 5px;
	text-align: left;
}
.level2 {
	color: #006600;
	background-color: #ccffcc;
	font-weight: bold;
	font-size: 8.5pt;
	line-height: 12pt;
	padding-left: 10px;
	text-align: left;
}
.level3 {
	color: #000000;
	background-color: #dedede;
	font-weight: bold;
	font-size: 8.5pt;
	line-height: 12pt;
	padding-left: 20px;
	font-style: italic;
	text-align: left;
}
.level1total {
	color: #ffffff;
	background-color: #666666;
	font-weight: bold;
	font-size: 8.5pt;
	line-height: 12pt;
	text-align: right;
}
.level2total {
	color: #006600;
	background-color: #cccccc;
	font-weight: bold;
	font-size: 8.5pt;
	line-height: 12pt;
	text-align: right;
}
.record {
    padding-left: 40px;
	text-align: left;
}
.total {
    padding-left: 40px;
    font-weight: bold;
	text-align: left;
}
.varrecord {
}
.vartotal {
    align: center;
    font-weight: bold;
}
.field {
    padding: 0px 3px 0px 3px;
    text-align: center;
    height: 23;
    background-color: #ffffff;
}
.target {
    background-color: #edffed;
}
.b-left-w {
    border-left-color: #ffffff;
}
.b-left-g {
    border-left-color: #ababab;
}
.b-right-w {
    border-right-color: #ffffff;
}
.b-right-g {
    border-right-color: #ababab;
}
.b-top-w {
    border-top-color: #ffffff;
}
.b-top-g {
    border-top-color: #ababab;
}
.b-bottom-w {
    border-bottom-color: #ffffff;
}
.b-bottom-g {
    border-bottom-color: #ababab;
}
.txt {
    padding-left: 7px;
}
.blank { background-color: #ffffff; }
.disabled { background-color: #dddddd; }
.lft { text-align: left; }
.noborder { border: 0px solid #ffffff; }
.tdhover { background-color: #deffde; }
.tdhover2 { background-color: #ccffcc; }
.required { background-color: #ddffdd; }

</style>
