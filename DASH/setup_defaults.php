<?php
include("inc_head_setup.php");
include("inc/setup.php");
include("inc_admin.php");
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function saveYN(ref) {
    var val = document.getElementById(ref).value;
    var url = "setup_defaults.php?a=default&r="+ref+"&v="+val;
    document.location.href = url;
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Defaults</b></h1>
<?php displayResult($result); ?>
<form name=update method=post action=setup.php>
<table cellpadding=3 cellspacing=0 width=610>
<!--    <tr height=50>
        <th class=lft>Use Branches?</th>
        <td class=tdmain>Allow users to capture data per Branch?</td>
        <td class=tdbut width=170 align=right><select id=d_1><?php echo(selectYesNo($setup[1][0])); ?></select> <input type=button value=Save id=1 onclick="saveYN('d_1');">&nbsp;</td>
    </tr> -->
    <tr height=50>
        <th class=lft>Use Projects?</th>
        <td class=tdmain>Allow users to capture data per Project?</td>
        <td class=tdbut align=right><select id=d_2><?php echo(selectYesNo($setup[2][0])); ?></select> <input type=button value=Save id=2 onclick="saveYN('d_2');">&nbsp;</td>
    </tr>
<!--     <tr height=50>
        <th class=lft>Reporting Years</th>
        <td class=tdmain>Configure Reporting Years.</td>
        <td class=tdbut align=right><input type=button value=Configure id=4>&nbsp;</td>
    </tr> -->
</table>
</form>
<script type=text/javascript>
//butKill(4,4);
</script>
<?php
$urlback = "setup.php";
include("inc_goback.php");

displayChangeLog("S_DEF",0,$urlback);
?>
</body>
</html>
