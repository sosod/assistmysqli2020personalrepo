<?php
include("inc_head_setup.php");
include("inc_admin.php");

//GET EDIT VARIABLES
$result = "";
$start = $_GET['s'];
$aid = $_GET['id'];
$type = $_GET['type'];
$d = $_GET['d'];
$a = $_GET['a'];
$u = $_GET['u'];
$v = $_GET['v'];
$r = $_GET['r'];

if($type == "edit")    //IF EDIT TYPE = EDIT
{
    //GET TRANSACTION LOG INFO
    $sql = "SELECT * FROM ".$dbref."_list_users WHERE id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    //PERFORM EDIT UPDATE
    $sql = "UPDATE ".$dbref."_list_users SET dash = '".$d."', activate = '".$a."', capture = '".$u."', view = '".$v."', report = '".$r."' WHERE id = ".$aid;
    include("inc_db_con.php");
    //SET TRANSACTION LOG VALUES
    $tsql = $sql;
    $told = "UPDATE ".$dbref."_list_users SET dash = '".$row['dash']."', activate = '".$row['activate']."', capture = '".$row['capture']."', view = '".$row['view']."', report = '".$row['report']."' WHERE id = ".$aid;
    $trans = "Updated user ".$aid;
    //PERFORM TRANSACTION LOG UPDATE
    include("inc_transaction_log.php");
    $result = "Updated user access.";
    echo("<script type=text/javascript>document.location.href = 'setup_access.php?a=access&s=".$start."&r=".$result."';</script>");
}
else
{
    if($type == "d")    //IF EDIT TYPE = DELETE
    {
        //UPDATE LIST-ACCESS AND SET USER TO N
        $sql = "UPDATE ".$dbref."_list_users SET yn = 'N' WHERE id = ".$aid;
        include("inc_db_con.php");
            //Set transaction log sql value for delete
            $tsql = $sql;
            $told = "UPDATE ".$dbref."_list_users SET yn = 'Y' WHERE id = ".$aid;;
            $trans = "Removed user access for ".$aid.".";
            include("inc_transaction_log.php");
        $result = "Deleted user access from $modtitle successfully.";
        echo("<script type=text/javascript>document.location.href = 'setup_access.php?a=access&s=".$start."&r=".$result."';</script>");
    }
}

?>
<script language=JavaScript>
function delUser(id,s,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to delete access to <?php echo($modtitle); ?> for "+tkn+"."))
    {
        document.location.href = "setup_access_edit.php?id="+id+"&s="+s+"&type=d";
    }
}

function Validate(me) {
    return true;
}
</script>
<h1><?php echo($modtitle); ?>: Setup - User Access ~ Edit</h1>
<form name=edit method=get action=setup_access_edit.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=type value=edit><input type=hidden name=id value=<?php echo($aid);?>>
<input type=hidden name=s value=<?php echo($start); ?>>
<table border="1" id="table1" cellspacing="0" cellpadding="4" width=690>
	<tr>
		<th rowspan=2 width=30>ID</th>
		<th rowspan=2 width=260>User</th>
		<th colspan=5>Access</th>
	</tr>
	<tr>
		<th width=80>Dashboard<br>Admin</th>
		<th width=80>Create</th>
		<th width=80>Capture</th>
		<th width=80>View<Br>All</th>
		<th width=80>Report</th>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT u.*, t.tkname, t.tksurname ";
    $sql .= "FROM ".$dbref."_list_users u ";
    $sql .= ", assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE u.yn = 'Y' AND t.tkid = u.tkid AND u.id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $tkn = $row['tkname']." ".$row['tksurname'];
    mysql_close($con);
                $d = "<select name=d>".selectYesNo($row['dash'])."</select>";
                $a = "<select name=a>".selectYesNo($row['activate'])."</select>";
                $u = "<select name=u>".selectYesNo($row['capture'])."</select>";
                $v = "<select name=v>".selectYesNo($row['view'])."</select>";
                $r = "<select name=r>".selectYesNo($row['report'])."</select>";
?>
        <tr valign=middle>
            <td><?php echo($row['tkid']); ?></td>
            <td ><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td align=center><?php echo($d); ?></td>
            <td align=center><?php echo($a); ?></td>
            <td align=center><?php echo($u); ?></td>
            <td align=center><?php echo($v); ?></td>
            <td align=center><?php echo($r); ?></td>
        </tr>
        <tr>
            <td colspan=7 class="tdgeneral"><input type=submit value=" Save "> <input type=button value=Delete onclick="delUser(<?php echo($aid.",".$start) ;?>,'<?php echo(str_replace(" ","_",$tkn)) ;?>')"> <input type=reset></td>
        </tr>
</table>
</form>
<?php
$urlback = "setup_access.php";
include("inc_goback.php");
?>

</body></html>
