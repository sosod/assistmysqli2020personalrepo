<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreate1.php");
include("inc/AdminCreateProgress.php");

$page = array("next"=>"2-0","step"=>"6","title"=>"Preview");

?>
<script type=text/javascript>
function moveOn(actt) {
    if(actt.length==1)
    {
        document.getElementById('actt').value = actt;
        document.forms['frm'].submit();
    }
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 1.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);

$frames = $dash['dashwards'] + $dash['dashtowns'];
if($frames<0)
{
    $sql = "INSERT INTO ".$dbref."_dashboard_frame (dfdashid,dfforeignid,dfyn,dftype) VALUES ($ref,".$naward['id'].",'Y','W')";
    include("inc_db_con.php");
    $sql = "UPDATE ".$dbref."_dashboard SET dashwards = 3 WHERE dashid = $ref";
    include("inc_db_con.php");
}

echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
echo("<form name=frm id=frm onsubmit=\"return Validate(this);\" action=\"admin_create_process.php\" method=post>");
echo("<input type=hidden name=step value=\"1-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
?>
<div align=center>
<?php
previewDash($ref);
?>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type=hidden name=acttype value="" id=actt><select name=act><option selected value=move>Continue creation</option><option value=pause>Pause dashboard creation</option></select> <input type="button" value="Accept" onclick="moveOn('Y');"> <input type=button value=Cancel onclick="moveOn('C');"> <input type=button value="Preview" onclick="previewDash(<?php echo($ref); ?>);"></td>
	</tr>
</table>
</form>
<?php
        $stepprogress = setProgress(1);
        $totalprogress = setProgress(0);
        displayProgress("Step 1 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
