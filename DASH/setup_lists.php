<?php
include("inc_head_setup.php");
include("inc/setup.php");
?>
<script type=text/javascript>
function Validate(me) {
    if(me.list.value.length>0)
    {
        return true;
    }
    else
    {
        alert("Please enter the name of the list.");
        return false;
    }
}
</script>
<h1><?php echo($modtitle); ?>: Setup - Lists</h1>
<?php displayResult($result); ?>
<form name=add method=get action=setup_lists.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=a value=lists><input type=hidden name=act value=add>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th width=30>Ref</th>
        <th width=420>List Name</th>
        <th width=100># of Values</th>
        <th width=50>&nbsp;</th>
    </tr>
<?php
$cid = array();
$sql = "SELECT listid, count(listid) as cid FROM ".$dbref."_list_dropdowns_values WHERE yn = 'Y' GROUP BY listid";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $cid[$row['listid']] = $row['cid'];
}
mysql_close($con);
$row = array();
$sql = "SELECT l.id, l.value FROM ".$dbref."_list_dropdowns l WHERE l.yn = 'Y' ORDER BY l.value";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $count = $cid[$row['id']];
        if(strlen($count)==0) { $count = 0; }
?>
    <tr height=30>
        <td class=tdheader style="border-bottom: 1px solid #ffffff;"><?php echo($row['id']); ?></td>
        <td style="padding-left: 7px;"><?php echo($row['value']); ?></td>
        <td align=center><?php echo($count); ?></td>
        <td align=center><input type=button value=" Edit " onclick="document.location.href = 'setup_lists_edit.php?i=<?php echo($row['id']); ?>';"></td>
    </tr>
<?php
    }
mysql_close($con);
?>
    <tr height=30>
        <td class=tdheader>&nbsp;</td>
        <td style="padding-left: 7px;" colspan=2><input type=text size=50 maxlength=100 name=list></td>
        <td align=center><input type=submit value=" Add "></td>
    </tr>
</table>
</form>
<?php
$urlback = "setup.php";
include("inc_goback.php");

displayChangeLog("S_LISTS","X",$urlback);

?>
</body>
</html>
