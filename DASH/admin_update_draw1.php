<?php
include("inc_head.php");
include("inc/Admin.php");
$frequency = getCaptureFreq();
$time = getTime(0);
//print_r($setup);
$variables = $_REQUEST;
$ref = $variables['ref'];
$dashadmin = $variables['dash'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}
//echo("<P>"); print_r($dash);
if(checkIntRef($variables['timeid']))
{
    $t = $time[$variables['timeid']];
?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('.datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/images/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:false,
                    changeYear:false,
                    minDate: new Date(<?php echo(date("Y",$t['sval'])); ?>, <?php echo(date("m",$t['sval'])-1); ?>, <?php echo(date("d",$t['sval'])); ?>),
                    maxDate: new Date(<?php echo(date("Y",$t['eval'])); ?>, <?php echo(date("m",$t['eval'])-1); ?>, <?php echo(date("d",$t['eval'])); ?>)
                });

			});

</script>
<?php
}
?>
<script type=text/javascript>
function changeTime(me) {
    document.location.href = "admin_update_draw1.php?ref=<?php echo($ref); ?>&timeid="+me.value;
}
function Validate() {
    var valid8 = "false";
    var me = document.forms['frmn'];
    var ref = me.ref.value;
    var timeid = me.timeid.value;
    var frme = me.frame.value;
//    alert(timeid=="X");
    if(isNaN(parseInt(ref)))
    {
        alert("An error has occurred.  Please go back and try again.");
    }
    else
    {
        if(timeid == "X")
        {
            alert("Please select a time period (Step 2).");
        }
        else
        {
            if(frme=="X")
            {
                alert("Please selecte a frame to update (Step 3).");
            }
            else
            {
                if(document.getElementById('imp').checked==true) { me.action = "admin_update_excel1.php"; }
                me.submit();
            }
        }
    }
    //alert(valid8);
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Update a Dashboard</h1>
<h2>1. Dashboard Details</h2>
<form name=frmn id=frm action="admin_update_draw2.php" method=post language=jscript>
<input type=hidden name=ref value="<?php echo($ref);?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<h2>2. Select Time Period</h2>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl" width=200 height=27>
<?php
switch($dash['dashfrequency'])
{
    case 4:     //monthly
        echo("Month Ending:</td><td width=400> <select name=timeid><option selected value=X>--- SELECT ---</option>");
        foreach($time as $t)
        {
            echo("<option value=".$t['id'].">".date("d F Y",$t['eval'])."</option>");
        }
        echo("</select>");
        break;
    case 5:     //2-monthly
        $x = 2;
        echo("$x Months Ending:</td><td width=400> <select name=timeid><option selected value=X>--- SELECT ---</option>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
                echo("<option value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 6:     //3-monthly
        $x = 3;
        echo("Quarter Ending:</td><td width=400> <select name=timeid><option selected value=X>--- SELECT ---</option>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
                echo("<option value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 7:     //4-monthly
        $x = 4;
        echo("$x Months Ending:</td><td width=400> <select name=timeid><option selected value=X>--- SELECT ---</option>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
                echo("<option value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 8:     //6-monthly
        $x = 6;
        echo("$x Months Ending:</td><td width=400> <select name=timeid><option selected value=X>--- SELECT ---</option>");
        for($t=1;$t<13;$t++)
        {
            if($t/$x == ceil($t/$x))
            {
                $tim = $time[$t];
                echo("<option value=".$tim['id'].">".date("d F Y",$tim['eval'])."</option>");
            }
        }
        echo("</select>");
        break;
    case 9:     //12-monthly
        echo("Year Ending:</td><td width=400> ");
               $tim = $time[12];
                echo(date("d F Y",$tim['eval'])."<input type=hidden name=timeid value=12>");
        break;
    default:  //1 Daily,2 Weekly,3 fortnightly
        if(checkIntRef($variables['timeid']))
        {
            echo("Month:</td><td width=400> ".date("F Y",$time[$variables['timeid']]['eval']));
            echo("<input type=hidden name=timeid value=\"".$variables['timeid']."\"></td></tr><tr><td  class=\"tdheaderl b-top-w b-bottom-w\" width=200>");
            switch($dash['dashfrequency'])
            {
                case 1:
                    echo("Day: </td><td><input type=text name=timedate size=9 readonly=readonly class=datepicker value=\"".date("d-m-Y",$t['sval'])."\">"); // to <input type=text class=datepicker size=9 readonly=readonly value=\"".date("d-m-Y",$t['sval'])."\" >");
                    break;
                case 2:
                    echo("Week: </td><td>N/A");
                    break;
                case 3:
                    echo("Fortnight: </td><td>N/A");
                    break;
            }
        }
        else
        {
            echo("Month:</td><td width=400> <select name=timeid onchange=\"changeTime(this);\"><option selected value=X>--- SELECT ---</option>");
            foreach($time as $t)
            {
                echo("<option value=".$t['id'].">".date("d F Y",$t['eval'])."</option>");
            }
            echo("</select>");
        }
//                    echo("Daily/Weekly/Fortnightly: </td><td>IN PROGRESS");
        break;
}
?>
</td></tr></table>
<h2>3. Select Frame</h2>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Frame: </td>
        <td width=400> <select name=frame id=frme><option selected value=X>--- SELECT ---</option>
<?php
$echo = "";
            $sql = "SELECT dirid, dirtxt FROM assist_".$cmpcode."_list_dir WHERE diryn = 'Y' ";
            $sql.= "AND (dirid IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame, ".$dbref."_dashboard_admins WHERE dftype = 'D' AND dfid = daframeid AND datkid = '$tkid' AND dayn = 'Y' AND dfyn = 'Y' AND dfdashid = $ref) ";
            if($dash['dashowner'] == $tkid || $dashadmin == "Y")
            {
                $sql.= " OR ";
                $sql.= "dirid IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame WHERE dftype = 'D' AND dfyn = 'Y' AND dfdashid = $ref AND dfid NOT IN (SELECT daframeid FROM ".$dbref."_dashboard_admins WHERE dayn = 'Y'))";
            }
            $sql.= ") ORDER BY dirsort";
//$echo.="<P>".$sql;
//            echo($sql);
            include("inc_db_con.php");
            $mnr = mysql_num_rows($rs);
            if($mnr==0)
            {
            }
            else
            {
                echo("<option value=X>--- DIRECTORATES ---</option>");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['dirid'];
                    $val = $row['dirtxt'];
                    echo("<option value=D_".$id.">$val</option>");
                }
            }
            mysql_close($con);

            $sql = "SELECT subid, subtxt FROM assist_".$cmpcode."_list_dirsub WHERE subyn = 'Y' ";
            $sql.= "AND (subid IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame, ".$dbref."_dashboard_admins WHERE dftype = 'S' AND dfid = daframeid AND datkid = '$tkid' AND dayn = 'Y' AND dfyn = 'Y' AND dfdashid = $ref) ";
            if($dash['dashowner'] == $tkid || $dashadmin == "Y")
            {
                $sql.= " OR ";
                $sql.= "subid IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame WHERE dftype = 'S' AND dfyn = 'Y' AND dfdashid = $ref AND dfid NOT IN (SELECT daframeid FROM ".$dbref."_dashboard_admins WHERE dayn = 'Y'))";
            }
            $sql.= ") AND subdirid IN (SELECT dirid FROM assist_".$cmpcode."_list_dir WHERE diryn = 'Y') ORDER BY subdirid, subsort";
//            echo($sql);
//$echo.="<P>".$sql;
            include("inc_db_con.php");
            $mnr = mysql_num_rows($rs);
            if($mnr==0)
            {
            }
            else
            {
                echo("<option value=X>--- SUB-DIRECTORATES ---</option>");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['subid'];
                    $val = $row['subtxt'];
                    echo("<option value=S_".$id.">$val</option>");
                }
            }
            mysql_close($con);

         if($setup[1][0]=="Y" && $dash['dashtowns']>0) { 
            $sql = "SELECT id, value FROM ".$dbref."_list_towns WHERE yn = 'Y' ";
            $sql.= "AND (id IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame, ".$dbref."_dashboard_admins WHERE dftype = 'T' AND dfid = daframeid AND datkid = '$tkid' AND dayn = 'Y' AND dfyn = 'Y' AND dfdashid = $ref) ";
            if($dash['dashowner'] == $tkid || $dashadmin == "Y")
            {
                $sql.= " OR ";
                $sql.= "id IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame WHERE dftype = 'T' AND dfyn = 'Y' AND dfdashid = $ref AND dfid NOT IN (SELECT daframeid FROM ".$dbref."_dashboard_admins WHERE dayn = 'Y'))";
            }
            $sql.= ") ORDER BY value";
//$echo.="<P>".$sql;
//            echo($sql);
            include("inc_db_con.php");
            $mnr = mysql_num_rows($rs);
            if($mnr==0)
            {
            }
            else
            {
                echo("<option value=X>--- TOWNS ---</option>");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option value=T_".$id.">$val</option>");
                }
            }
            mysql_close($con);
        }

        if($setup[2][0]=="Y" && $dash['dashwards']>0) { 
            $sql = "SELECT id, value FROM ".$dbref."_list_wards WHERE yn = 'Y' ";
            $sql.= "AND (id IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame, ".$dbref."_dashboard_admins WHERE dftype = 'W' AND dfid = daframeid AND datkid = '$tkid' AND dayn = 'Y' AND dfyn = 'Y' AND dfdashid = $ref) ";
            if($dash['dashowner'] == $tkid || $dashadmin == "Y")
            {
                $sql.= " OR ";
                $sql.= "id IN (SELECT dfforeignid FROM ".$dbref."_dashboard_frame WHERE dftype = 'W' AND dfyn = 'Y' AND dfdashid = $ref AND dfid NOT IN (SELECT daframeid FROM ".$dbref."_dashboard_admins WHERE dayn = 'Y'))";
            }
            $sql.= ") ORDER BY value";
//$echo.="<P>".$sql;
//            echo($sql);
            include("inc_db_con.php");
            $mnr = mysql_num_rows($rs);
            if($mnr==0)
            {
            }
            else
            {
                echo("<option value=X>--- WARDS ---</option>");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option value=W_".$id.">$val</option>");
                }
            }
            mysql_close($con);
        } ?>
        </td>
    </tr>
</table>
<?php //echo($echo); ?>
<h2>4. Choose Update Method</h2>
<table cellpadding=3 cellspacing=0 width=600 class=noborder>
	<tr>
		<td class="noborder"><input type=radio name=output value=onscreen id=ons checked><label for=ons> Onscreen Form</label></td>
    </tr>
	<tr>
		<td class=noborder><input type=radio name=output value=import id=imp><label for=imp> Import/Export</label></td>
    </tr>
</table>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td style="text-align:center"><input type=hidden value="<?php echo($dashadmin); ?>" name=dash><input type=button value="  Update  " onclick="Validate();"></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_update.php";
include("inc_goback.php");
?>
<script type=text/javascript>//document.getElementById('imp').disabled = true;</script>
<p>&nbsp;</p>
</body>
</html>
