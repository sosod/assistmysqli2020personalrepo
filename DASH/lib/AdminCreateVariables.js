function changeType(me,i) {
    var fld = "u-"+i;
    var targ = document.getElementById(fld);
    switch(me.value)
    {
        case "NUM":
            targ.value = "";
            targ.disabled = false;
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        case "LT":
            targ.value = "N/A";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
    if(me.value != "LT")
    {
            fld = "ut-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "inline";
            fld = "ul-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "none";
    }
    else
    {
            fld = "ut-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "none";
            fld = "ul-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "inline";
    }
}
function setType(me,i) {
    var fld = "u-"+i;
    var targ = document.getElementById(fld);
    switch(me.value)
    {
        case "NUM":
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        case "LT":
            targ.value = "N/A";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
    if(me.value != "LT")
    {
            fld = "ut-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "inline";
            fld = "ul-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "none";
    }
    else
    {
            fld = "ut-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "none";
            fld = "ul-"+i;
            targ = document.getElementById(fld);
            targ.style.display = "inline";
    }
}


function viewBlock() {
    var valid8 = true;
    if(document.getElementById('recfn').style.display == "block") { valid8 = false; }
    if(document.getElementById('fldfn').style.display == "block") { valid8 = false; }
    if(document.getElementById('subfn').style.display == "block") { valid8 = false; }
    if(document.getElementById('dirfn').style.display == "block") { valid8 = false; }
    if(document.getElementById('subffn').style.display == "block") { valid8 = false; }
    if(document.getElementById('dirffn').style.display == "block") { valid8 = false; }
    
    return valid8;
}


var multifn = "_|SUM|SAVE|COUNT|MIN|MAX|_";
var dblfn = "_|VAR|PERC|AVE|PROD|_";
var sglfn = "_|AVAT|LVAT|EVAT|IVAT|EDAY|DAYE|_";

var recfnvar = new Array('r-yn','r-fn','r-rsum','r-r1','r-r2','r-t','r-u');
var recfndef = new Array('Y','SUM','','X','X','NUM','');
var fldfnvar = new Array('f-yn','f-fn','f-fsum','f-f1','f-f2','f-t','f-u');
var fldfndef = new Array('Y','SUM','','X','X','NUM','');
var subfldfnvar = new Array('sf-yn','sf-fn','sf-fsum','sf-f1','sf-f2','sf-t','sf-u');
var subfldfndef = new Array('Y','SUM','','X','X','NUM','');
var dirfldfnvar = new Array('df-yn','df-fn','df-fsum','df-f1','df-f2','df-t','df-u');
var dirfldfndef = new Array('Y','SUM','','X','X','NUM','');
var arSelected = new Array();
function getMultiple(ob)
{
    var len = ob.length;
    for(l=0;l<len;l++) {
        if(ob.options[l].selected) {
            arSelected.push(ob.options[l].value);
        }
    }
}
function setupRecFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            var rt = fnvars[6];
            var ru = fnvars[7];
            if(multifn.indexOf("|"+rfn+"|")>0)
            {
                var rsum2 = fnvars[3].split("-");
                var rsum = new Array();
                var r2;
                for(r=0;r<rsum2.length;r++)
                {
                    r2 = rsum2[r];
                    rsum[r2] = "Y";
                }
                var x=document.getElementById("r-rsum");
                var xv;
                for (s=0;s<x.length;s++)
                {
                    xv = x.options[s].value;
                    if(rsum[xv]=="Y") { x.options[s].selected = true; }
                }
            }
            else
            {
                document.getElementById('r-r1').value = fnvars[4];
                if(!isNaN(parseInt(fnvars[5])))
                    document.getElementById('r-r2').value = fnvars[5];
                else
                    document.getElementById('r-r2').value = 'X';
            }
            document.getElementById('r-fn').value = rfn;
            document.getElementById('r-t').value = rt;
            document.getElementById('r-u').value = ru;

        }
        document.getElementById('recfnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('recfn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';
        
    }
    changeRecFn("N");
}
function saveRecFn() {
    var result = "";
    var yn = document.getElementById('r-yn').value;
    if(yn=="Y") {
        for(r in recfnvar)
        {
            if(recfnvar[r]!="r-rsum") {
                result = result + "|_|" + document.getElementById(recfnvar[r]).value;
            } else {
                getMultiple(document.getElementById(recfnvar[r]));
                result = result + "|_|" + arSelected.join("-");
                arSelected = new Array();
            }
            document.getElementById(recfnvar[r]).disabled = false;
        }
                result = result + "|_|REC";
    } else {
        result = "N";
    }
    var recfnv = document.getElementById('recfnv').value;
    document.getElementById('recfnv').value = "";
    var fnv = recfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('recfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelRecFn() {
        for(r in recfnvar)
        {
            document.getElementById(recfnvar[r]).disabled = false;
            document.getElementById(recfnvar[r]).value = recfndef[r];
        }

    var recfnv = document.getElementById('recfnv').value;
    document.getElementById('recfnv').value = "";
    var fnv = recfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('recfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useRecFn(me) {
    if(me.value=="N") {
        for(r in recfnvar)
        {
            if(recfnvar[r]!="r-yn")
                document.getElementById(recfnvar[r]).disabled = true;
        }
    } else {
        for(r in recfnvar)
        {
            if(recfnvar[r]!="r-yn")
                document.getElementById(recfnvar[r]).disabled = false;
        }
        changeRecFn();
    }
}
function changeRecType(me) {
    var targ = document.getElementById('r-u');
    switch(me.value)
    {
        case "NUM":
            targ.value = "";
            targ.disabled = false;
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
}
function changeRecFn(yn) {
    var me = document.getElementById('r-fn');
    var targt = document.getElementById('r-t');
    var targu = document.getElementById('r-u');
    switch(me.value)
    {
        case "PERC":
            targu.value = "%";
            targu.disabled = true;
            targt.value = "PERC";
            targt.disabled = true;
            break;
        default:
            if(yn == "Y") {
                targu.value = "";
                targt.value = "NUM";
            }
            targu.disabled = false;
            targt.disabled = false;
    }
    var mev = "|"+me.value+"|";
    if(multifn.indexOf(mev)>0)
    {
        document.getElementById('r-rsum').disabled = false;
        document.getElementById('r-r1').disabled = true;
        document.getElementById('r-r2').disabled = true;
    }
    else
    {
        document.getElementById('r-rsum').disabled = true;
        document.getElementById('r-r1').disabled = false;
        if(dblfn.indexOf(me.value)>0)
            document.getElementById('r-r2').disabled = false;
        else
            document.getElementById('r-r2').disabled = true;
    }
}







function setupFldFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            var rt = fnvars[6];
            var ru = fnvars[7];
            if(multifn.indexOf("|"+rfn+"|")>0)
            {
                var rsum2 = fnvars[3].split("-");
                var rsum = new Array();
                var r2;
                for(r=0;r<rsum2.length;r++)
                {
                    r2 = rsum2[r];
                    rsum[r2] = "Y";
                }
                var x=document.getElementById("f-fsum");
                var xv;
                for (s=0;s<x.length;s++)
                {
                    xv = x.options[s].value;
                    if(rsum[xv]=="Y") { x.options[s].selected = true; }
                }
            }
            else
            {
                document.getElementById('f-f1').value = fnvars[4];
                if(!isNaN(parseInt(fnvars[5])))
                    document.getElementById('f-f2').value = fnvars[5];
                else
                    document.getElementById('f-f2').value = "X";
            }
            document.getElementById('f-fn').value = rfn;
            document.getElementById('f-t').value = rt;
            document.getElementById('f-u').value = ru;

        }
        document.getElementById('fldfnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('fldfn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';
    }
    changeFldFn("N");
}
function saveFldFn() {
    var result = "";
    var yn = document.getElementById('f-yn').value;
    if(yn=="Y") {
        for(f in fldfnvar)
        {
            if(fldfnvar[f]!="f-fsum") {
                result = result + "|_|" + document.getElementById(fldfnvar[f]).value;
            } else {
                getMultiple(document.getElementById(fldfnvar[f]));
                result = result + "|_|" + arSelected.join("-");
                arSelected = new Array();
            }
        }
                result = result + "|_|FLD";
    }
    else
    {
        result = "N";
    }
    var fldfnv = document.getElementById('fldfnv').value;
    document.getElementById('fldfnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('fldfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelFldFn() {
        for(f in fldfnvar)
        {
            document.getElementById(fldfnvar[f]).disabled = false;
            document.getElementById(fldfnvar[f]).value = fldfndef[f];
        }

    var fldfnv = document.getElementById('fldfnv').value;
    document.getElementById('fldfnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fldfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useFldFn(me) {
    if(me.value=="N") {
        for(f in fldfnvar)
        {
            if(fldfnvar[f]!="f-yn")
                document.getElementById(fldfnvar[f]).disabled = true;
        }
    } else {
        for(f in fldfnvar)
        {
            if(fldfnvar[f]!="f-yn")
                document.getElementById(fldfnvar[f]).disabled = false;
        }
        changeFldFn();
    }
}
function changeFldType(me) {
    var targ = document.getElementById('f-u');
    switch(me.value)
    {
        case "NUM":
            targ.value = "";
            targ.disabled = false;
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
}
function changeFldFn(yn) {
    var me = document.getElementById('f-fn');
    var targt = document.getElementById('f-t');
    var targu = document.getElementById('f-u');
    switch(me.value)
    {
        case "PERC":
            targu.value = "%";
            targu.disabled = true;
            targt.value = "PERC";
            targt.disabled = true;
            break;
        default:
            if(yn=="Y") {
                targu.value = "";
                targt.value = "NUM";
            }
            targu.disabled = false;
            targt.disabled = false;
    }
    var mev = "|"+me.value+"|";
    if(multifn.indexOf(mev)>0)
    {
        document.getElementById('f-fsum').disabled = false;
        document.getElementById('f-f1').disabled = true;
        document.getElementById('f-f2').disabled = true;
    }
    else
    {
        document.getElementById('f-fsum').disabled = true;
        document.getElementById('f-f1').disabled = false;
        if(dblfn.indexOf(me.value)>0)
            document.getElementById('f-f2').disabled = false;
        else
            document.getElementById('f-f2').disabled = true;
    }
}







function setupSubFFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            var rt = fnvars[6];
            var ru = fnvars[7];
            if(multifn.indexOf("|"+rfn+"|")>0)
            {
                var rsum2 = fnvars[3].split("-");
                var rsum = new Array();
                var r2;
                for(r=0;r<rsum2.length;r++)
                {
                    r2 = rsum2[r];
                    rsum[r2] = "Y";
                }
                var x=document.getElementById("sf-fsum");
                var xv;
                for (s=0;s<x.length;s++)
                {
                    xv = x.options[s].value;
                    if(rsum[xv]=="Y") { x.options[s].selected = true; }
                }
            }
            else
            {
                document.getElementById('sf-f1').value = fnvars[4];
                if(!isNaN(parseInt(fnvars[5])))
                    document.getElementById('sf-f2').value = fnvars[5];
                else
                    document.getElementById('sf-f2').value = "X";
            }
            document.getElementById('sf-fn').value = rfn;
            document.getElementById('sf-t').value = rt;
            document.getElementById('sf-u').value = ru;

        }
        document.getElementById('subffnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('subffn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';
    }
    changeSubFFn("N");
}
function saveSubFFn() {
    var result = "";
    var yn = document.getElementById('sf-yn').value;
    if(yn=="Y") {
        for(f in subfldfnvar)
        {
            if(subfldfnvar[f]!="sf-fsum") {
                result = result + "|_|" + document.getElementById(subfldfnvar[f]).value;
            } else {
                getMultiple(document.getElementById(subfldfnvar[f]));
                result = result + "|_|" + arSelected.join("-");
                arSelected = new Array();
            }
        }
                result = result + "|_|SUBF";
    }
    else
    {
        result = "N";
    }
    var fldfnv = document.getElementById('subffnv').value;
    document.getElementById('subffnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('subffn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelSubFFn() {
        for(f in subfldfnvar)
        {
            document.getElementById(subfldfnvar[f]).disabled = false;
            document.getElementById(subfldfnvar[f]).value = subfldfndef[f];
        }

    var fldfnv = document.getElementById('subffnv').value;
    document.getElementById('subffnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('subffn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useSubFFn(me) {
    if(me.value=="N") {
        for(f in subfldfnvar)
        {
            if(subfldfnvar[f]!="f-yn")
                document.getElementById(subfldfnvar[f]).disabled = true;
        }
    } else {
        for(f in subfldfnvar)
        {
            if(subfldfnvar[f]!="f-yn")
                document.getElementById(subfldfnvar[f]).disabled = false;
        }
        changeSubFFn();
    }
}
function changeSubFType(me) {
    var targ = document.getElementById('sf-u');
    switch(me.value)
    {
        case "NUM":
            targ.value = "";
            targ.disabled = false;
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
}
function changeSubFFn(yn) {
    var me = document.getElementById('sf-fn');
    var targt = document.getElementById('sf-t');
    var targu = document.getElementById('sf-u');
    switch(me.value)
    {
        case "PERC":
            targu.value = "%";
            targu.disabled = true;
            targt.value = "PERC";
            targt.disabled = true;
            break;
        default:
            if(yn=="Y") {
                targu.value = "";
                targt.value = "NUM";
            }
            targu.disabled = false;
            targt.disabled = false;
    }
    var mev = "|"+me.value+"|";
    if(multifn.indexOf(mev)>0)
    {
        document.getElementById('sf-fsum').disabled = false;
        document.getElementById('sf-f1').disabled = true;
        document.getElementById('sf-f2').disabled = true;
    }
    else
    {
        document.getElementById('sf-fsum').disabled = true;
        document.getElementById('sf-f1').disabled = false;
        if(dblfn.indexOf(me.value)>0)
            document.getElementById('sf-f2').disabled = false;
        else
            document.getElementById('sf-f2').disabled = true;
    }
}






var subfnvar = new Array('s-yn','s-fn','s-sum','s-t','s-u');
var subfndef = new Array('Y','SUM','X','NUM','');
function setupSubFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            var rt = fnvars[4];
            var ru = fnvars[5];
            document.getElementById('s-sum').value = fnvars[3];
            
            document.getElementById('s-fn').value = rfn;
            document.getElementById('s-t').value = rt;
            document.getElementById('s-u').value = ru;

        }
        document.getElementById('subfnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('subfn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';

    }
}
function saveSubFn() {
    var result = "";
    var yn = document.getElementById('s-yn').value;
    if(yn=="Y") {
        for(r in subfnvar)
        {
            result = result + "|_|" + document.getElementById(subfnvar[r]).value;
            document.getElementById(subfnvar[r]).disabled = false;
        }
                result = result + "|_|SUB";
    } else {
        result = "N";
    }
    var subfnv = document.getElementById('subfnv').value;
    document.getElementById('subfnv').value = "";
    var fnv = subfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('subfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelSubFn() {
        for(r in subfnvar)
        {
            document.getElementById(subfnvar[r]).disabled = false;
            document.getElementById(subfnvar[r]).value = subfndef[r];
        }

    var subfnv = document.getElementById('subfnv').value;
    document.getElementById('subfnv').value = "";
    var fnv = subfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('subfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useSubFn(me) {
    if(me.value=="N") {
        for(r in subfnvar)
        {
            if(subfnvar[r]!="s-yn")
                document.getElementById(subfnvar[r]).disabled = true;
        }
    } else {
        for(r in subfnvar)
        {
            if(subfnvar[r]!="s-yn")
                document.getElementById(subfnvar[r]).disabled = false;
        }
    }
}
function changeSubType(me) {
    var targ = document.getElementById('s-u');
    switch(me.value)
    {
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        default:
            targ.value = "";
            targ.disabled = false;
    }
}















var dirfnvar = new Array('d-yn','d-fn');
var dirfndef = new Array('Y','SUM');
function setupDirFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            document.getElementById('d-fn').value = rfn;

        }
        document.getElementById('dirfnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('dirfn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';

    }
}
function saveDirFn() {
    var result = "";
    var yn = document.getElementById('d-yn').value;
    if(yn=="Y") {
        for(r in dirfnvar)
        {
            result = result + "|_|" + document.getElementById(dirfnvar[r]).value;
            document.getElementById(dirfnvar[r]).disabled = false;
        }
                result = result + "|_|DIR";
    } else {
        result = "N";
    }
    var dirfnv = document.getElementById('dirfnv').value;
    document.getElementById('dirfnv').value = "";
    var fnv = dirfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('dirfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-w ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelDirFn() {
        for(r in dirfnvar)
        {
            document.getElementById(dirfnvar[r]).disabled = false;
            document.getElementById(dirfnvar[r]).value = dirfndef[r];
        }

    var dirfnv = document.getElementById('dirfnv').value;
    document.getElementById('dirfnv').value = "";
    var fnv = dirfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('dirfn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useDirFn(me) {
    if(me.value=="N") {
        for(r in dirfnvar)
        {
            if(dirfnvar[r]!="d-yn")
                document.getElementById(dirfnvar[r]).disabled = true;
        }
    } else {
        for(r in dirfnvar)
        {
            if(dirfnvar[r]!="d-yn")
                document.getElementById(dirfnvar[r]).disabled = false;
        }
    }
}





function setupDirFFn(i,recid,fldid,me) {
    if(viewBlock()==false)
    {
        alert("Please complete the current function setup by\nclicking either the 'Save changes' button or the 'Cancel' button.");
    }
    else
    {
        var fld = "fn-"+i;
        var current = document.getElementById(fld).value;
        if(current.length > 0 && current != "N")
        {
            fnvars = current.split('|_|');
            var rfn = fnvars[2];
            var rt = fnvars[6];
            var ru = fnvars[7];
            if(multifn.indexOf("|"+rfn+"|")>0)
            {
                var rsum2 = fnvars[3].split("-");
                var rsum = new Array();
                var r2;
                for(r=0;r<rsum2.length;r++)
                {
                    r2 = rsum2[r];
                    rsum[r2] = "Y";
                }
                var x=document.getElementById("df-fsum");
                var xv;
                for (s=0;s<x.length;s++)
                {
                    xv = x.options[s].value;
                    if(rsum[xv]=="Y") { x.options[s].selected = true; }
                }
            }
            else
            {
                document.getElementById('df-f1').value = fnvars[4];
                if(!isNaN(parseInt(fnvars[5])))
                    document.getElementById('df-f2').value = fnvars[5];
                else
                    document.getElementById('df-f2').value = "X";
            }
            document.getElementById('df-fn').value = rfn;
            document.getElementById('df-t').value = rt;
            document.getElementById('df-u').value = ru;

        }
        document.getElementById('dirffnv').value = i+"-"+recid+"-"+fldid;
        document.getElementById('dirffn').style.display = "block";
        document.getElementById(me).style.color = '#ffffff';
        document.getElementById(me).style.backgroundColor = '#006600';
    }
    changeDirFFn("N");
}
function saveDirFFn() {
    var result = "";
    var yn = document.getElementById('df-yn').value;
    if(yn=="Y") {
        for(f in dirfldfnvar)
        {
            if(dirfldfnvar[f]!="df-fsum") {
                result = result + "|_|" + document.getElementById(dirfldfnvar[f]).value;
            } else {
                getMultiple(document.getElementById(dirfldfnvar[f]));
                result = result + "|_|" + arSelected.join("-");
                arSelected = new Array();
            }
        }
                result = result + "|_|DIRF";
    }
    else
    {
        result = "N";
    }
    var fldfnv = document.getElementById('dirffnv').value;
    document.getElementById('dirffnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('fn-'+fnv[0]).value = result;
    document.getElementById('dirffn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
    var ui = "s-"+fnv[0];
    if(yn=="Y")
        document.getElementById(ui).className = 'ui-icon ui-icon-w ui-icon-check';
    else
        document.getElementById(ui).className = 'ui-icon ui-icon-circle-minus';
}
function cancelDirFFn() {
        for(f in dirfldfnvar)
        {
            document.getElementById(dirfldfnvar[f]).disabled = false;
            document.getElementById(dirfldfnvar[f]).value = dirfldfndef[f];
        }

    var fldfnv = document.getElementById('dirffnv').value;
    document.getElementById('dirffnv').value = "";
    var fnv = fldfnv.split("-");
    var me = "b-"+fnv[0];
    document.getElementById('dirffn').style.display = "none";
    document.getElementById(me).style.color = '#000000';
    document.getElementById(me).style.backgroundColor = '';
}
function useDirFFn(me) {
    if(me.value=="N") {
        for(f in dirfldfnvar)
        {
            if(dirfldfnvar[f]!="f-yn")
                document.getElementById(dirfldfnvar[f]).disabled = true;
        }
    } else {
        for(f in dirfldfnvar)
        {
            if(dirfldfnvar[f]!="f-yn")
                document.getElementById(dirfldfnvar[f]).disabled = false;
        }
        changeDirFFn();
    }
}
function changeDirFType(me) {
    var targ = document.getElementById('df-u');
    switch(me.value)
    {
        case "NUM":
            targ.value = "";
            targ.disabled = false;
            break;
        case "R":
            targ.value = "R";
            targ.disabled = true;
            break;
        case "PERC":
            targ.value = "%";
            targ.disabled = true;
            break;
        default:
            targ.value = "N/A";
            targ.disabled = true;
    }
}
function changeDirFFn(yn) {
    var me = document.getElementById('df-fn');
    var targt = document.getElementById('df-t');
    var targu = document.getElementById('df-u');
    switch(me.value)
    {
        case "PERC":
            targu.value = "%";
            targu.disabled = true;
            targt.value = "PERC";
            targt.disabled = true;
            break;
        default:
            if(yn=="Y") {
                targu.value = "";
                targt.value = "NUM";
            }
            targu.disabled = false;
            targt.disabled = false;
    }
    var mev = "|"+me.value+"|";
    if(multifn.indexOf(mev)>0)
    {
        document.getElementById('df-fsum').disabled = false;
        document.getElementById('df-f1').disabled = true;
        document.getElementById('df-f2').disabled = true;
    }
    else
    {
        document.getElementById('df-fsum').disabled = true;
        document.getElementById('df-f1').disabled = false;
        if(dblfn.indexOf(me.value)>0)
            document.getElementById('df-f2').disabled = false;
        else
            document.getElementById('df-f2').disabled = true;
    }
}




function changeKPI(i) {
    document.getElementById(i).className = "";
}
