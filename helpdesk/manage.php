<?php require_once('inc_header.php'); ?>
<?php require_once('manage_page_header.php'); ?>
<?php if($show_detailed){ ?>
    <?php require_once('manage_page_detailed_view.php'); ?>
<?php }else{ ?>
    <?php require_once('manage_page_simple_view.php'); ?>
<?php } ?>
<?php require_once('inc_footer.php'); ?>
