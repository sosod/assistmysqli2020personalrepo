<?php $display_navigation_buttons = false; ?>
<?php require_once('inc_header.php'); ?>

<?php
/*
 * ************************************************************************************************************************
 * ************ THIS IS WHERE THE REFCTORED CODE IS GOING LIVE UNTIL I SEND IT ALL TO THE CLASS DIRECTORY - START *********
 * ************************************************************************************************************************/


/*
 * ************************************************************************************************************************
 * ************ THIS IS WHERE THE REFCTORED CODE IS GOING LIVE UNTIL I SEND IT ALL TO THE CLASS DIRECTORY - END *********
 * ************************************************************************************************************************/

/*
 * ************************************************************************************************************************
 * ********************* THIS IS A HACK FOR THE UNASSIGNED REQUESTS ISSUE, TO BE REMOVED AFTER REFACTOR - START *************
 * ************************************************************************************************************************/
function getCurrentCompInfo($helper){
    /*
     * These are the company details
     * Including:
     * Company information
     * Preferred way of handling the helpdesk request (settings)
    */

    $sql = 'SELECT * FROM helpdesk_companies ';

    // Company Type
    $sql .= 'INNER JOIN helpdesk_company_types ';
    $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

    // Company helpdesk settings
    $sql .= 'INNER JOIN helpdesk_company_settings ';
    $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

    $sql .= 'INNER JOIN helpdesk_admin_management ';
    $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

    $sql .= 'INNER JOIN helpdesk_request_settings ';
    $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

    // Clauses
    $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
    $company_info = $helper->mysql_fetch_one($sql);

    return $company_info;

}

function getCurrentCompChildren($company_info, $helper){
    //Find all of the companies that this company is a parent of
    $sql = 'SELECT * FROM helpdesk_companies ';

    // Company Type
    $sql .= 'INNER JOIN helpdesk_company_types ';
    $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

    // Company helpdesk settings
    $sql .= 'INNER JOIN helpdesk_company_settings ';
    $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

    $sql .= 'INNER JOIN helpdesk_admin_management ';
    $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

    $sql .= 'INNER JOIN helpdesk_request_settings ';
    $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

    // Clauses
    $sql .= 'WHERE helpdesk_companies.hdc_parent = ' . $company_info['hdc_id'];

    $children = $helper->mysql_fetch_all_by_id($sql, 'hdc_id');

    return $children;
}

function thisArrayIsPopulated($returned_array){
    $thisArrayIsPopulated = false;
    if(isset($returned_array) && is_array($returned_array) && count($returned_array) > 0){
        $thisArrayIsPopulated = true;
    }
    return $thisArrayIsPopulated;
}

function showThePot($company_info){
    $pot = false;
    if(isset($company_info) && is_array($company_info) && count($company_info) > 0){
        if(($company_info['hcs_settings_id'] == 3) && ($company_info['hcs_admin_man_id'] == 3)){
            $pot = true;
        }
    }
    return $pot;
}

function getUnassignedInternalRequests($company_info, $helper, $unassigned){
    //This query gets all of the internal requests that haven't been claimed yet
    $sql = 'SELECT * FROM helpdesk_request ';

    $sql .= 'WHERE helpdesk_request.hdr_id IN ';
    $sql .= '(';//Internal select - START

    $sql .= 'SELECT hdr_id FROM helpdesk_request ';

    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

    $sql .= 'AND helpdesk_request.hdr_add_status <> 6 ';//Request is not cancelled

    //USER TYPE STUFF HACK #1
    if((int)$company_info['hdc_type'] == 1){//Client
        $sql .= 'AND helpdesk_users.user_type = 1 ';//Client User
    }elseif((int)$company_info['hdc_type'] == 2){//Reseller
        $sql .= 'AND helpdesk_users.user_type = 4 ';//Reseller User
    }

    $sql .= ') ';//Internal select - END

    $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
    $sql .= '(';//Internal select - START

    $sql .= 'SELECT hdr_id FROM helpdesk_request ';
    $sql .= 'INNER JOIN helpdesk_request_users ';
    $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

    //USER TYPE STUFF HACK #2
    if((int)$company_info['hdc_type'] == 1){//Client
        $sql .= 'AND helpdesk_users.user_type = 2 ';//Client Admin User
    }elseif((int)$company_info['hdc_type'] == 2){//Reseller
        $sql .= 'AND helpdesk_users.user_type = 3 ';//Reseller Admin User
    }

    $sql .= ')';//Internal select - END

    $internal_unassigned = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');

    if(isset($internal_unassigned) && is_array($internal_unassigned) && count($internal_unassigned) > 0){
        $unassigned = array_merge($unassigned, $internal_unassigned);
    }

    return $unassigned;
}


function getUnassignedRequestsFromChildren($company_info, $children, $current_user, $helper, $unassigned){
    $children_with_admin_id_array = array();
    $children_without_admin_id_array = array();
    foreach($children as $key => $val){
        $children_id_array[$key] = $val['hdc_id'];

        if(($val['hcs_admin_man_id'] == 2 && $val['hcs_settings_id'] == 2) || ($val['hcs_admin_man_id'] == 3 && $val['hcs_settings_id'] == 3)){
            // These settings require admin involvement
            $children_with_admin_id_array[$key] = $val['hdc_id'];
        }elseif($val['hcs_admin_man_id'] == 1 && $val['hcs_settings_id'] == 1) {
            // This is an auto escalation - Which means it comes from a client,
            // seeing that we said that resellers MUST have at least one admin
            $children_without_admin_id_array[$key] = $val['hdc_id'];
        }else{
            //This should never happen
        }
    }

    if(thisArrayIsPopulated($children_with_admin_id_array)){
        // These settings require admin involvement

        //This query gets all of the external requests that haven't been claimed yet
        $sql = 'SELECT * FROM helpdesk_request ';

        $sql .= 'WHERE ';
        $sql .= '(';//Group Internal selects - START
        $sql .= 'helpdesk_request.hdr_id IN ';

        // The add_user is s client_admin_user
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_with_admin_id_array) . ') ';

        $sql .= 'AND helpdesk_request.hdr_add_status <> 6 ';//Request is not cancelled

        if($current_user['hdrut_value'] == 'reseller_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
        }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
        }

        $sql .= ') ';//Internal select - END

        $sql .= 'OR helpdesk_request.hdr_id IN ';

        // The client_admin_user is an interested party, and his status is escalated
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_with_admin_id_array) . ') ';
        if($current_user['hdrut_value'] == 'reseller_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
        }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
        }
        $sql .= 'AND helpdesk_request_users.hdru_role = 2 ';
        $sql .= 'AND helpdesk_request_users.hdru_status = 9 ';
        $sql .= ') ';//Internal select - END
        $sql .= ') ';//Group Internal selects - END


        //No Entry for it in this company
        $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
        $sql .= '(';//Internal select - START

        $sql .= 'SELECT hdr_id FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$company_info['hdc_cc'].'\' ';

        $sql .= ')';//Internal select - END

        $unassigned_child = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');

        if(isset($unassigned_child) && is_array($unassigned_child) && count($unassigned_child) > 0){
            $unassigned = array_merge($unassigned, $unassigned_child);
        }
    }

    if(thisArrayIsPopulated($children_without_admin_id_array)) {
        // This is an auto escalation - Which means it comes from a client, seeing that we said that resellers MUST have admins

        //This query gets all of the external requests that haven't been claimed yet
        $sql = 'SELECT * FROM helpdesk_request ';

        $sql .= 'WHERE helpdesk_request.hdr_id IN ';

        // The add_user is s client_user
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_without_admin_id_array) . ') ';
        $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_user\' ';
        $sql .= ') ';//Internal select - END

        //No Entry for it in this company
        $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
        $sql .= '(';//Internal select - START

        $sql .= 'SELECT hdr_id FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

        $sql .= ')';//Internal select - END

        $unassigned_child = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');

        if(isset($unassigned_child) && is_array($unassigned_child) && count($unassigned_child) > 0){
            $unassigned = array_merge($unassigned, $unassigned_child);
        }
    }

    return $unassigned;
}

//This time I'm making it return the requests themselves, I'll make it work better in the refactored rendintion
function displayPot($company_info, $helper, $current_user){
    $unassigned = array();
    if($current_user['hdrut_value'] == 'client_admin_user' || $current_user['hdrut_value'] == 'reseller_admin_user'){
        $unassigned = getUnassignedInternalRequests($company_info, $helper, $unassigned);
    }

    if($current_user['hdrut_value'] == 'reseller_admin_user' || $current_user['hdrut_value'] == 'iassist_admin_user'){
        //Start by finding all of the companies that this company is a parent of
        $children = getCurrentCompChildren($company_info, $helper);
        if(thisArrayIsPopulated($children)){
            $unassigned = getUnassignedRequestsFromChildren($company_info, $children, $current_user, $helper, $unassigned);
        }

    }

    return $unassigned;
}
/*
 * ************************************************************************************************************************
 * ********************* THIS IS A HACK FOR THE UNASSIGNED REQUESTS ISSUE, TO BE REMOVED AFTER REFACTOR - END *************
 * ************************************************************************************************************************/
?>

<?php
//echo '<pre>';
//echo '<p>PRINTHERE</p>';
//print_r($requests);
//echo '</pre>';

////Use the id from the get variable to search for
//echo '<pre>';
//echo '<p>PRINTHERE</p>';
//print_r($_GET);
//echo '</pre>';


$sql = 'SELECT * FROM helpdesk_users ';
$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
$sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'] . ' ';
$sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
$user = $helper->mysql_fetch_one($sql);


$sql = 'SELECT * FROM helpdesk_users ';
$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\' ';
$sql .= 'AND helpdesk_users.user_type = 5 ';
$users = $helper->mysql_fetch_all_by_id($sql, 'user_id');

$process = false;

//"Done" SIMPLE STATUS
$simple_status[4]['status_name'] = 'Done';
$simple_status[4]['status_collection'][1] = $status[5];
$simple_status[4]['status_collection'][2] = $status[6];
$simple_status[4]['status_collection'][3] = $status[7];
$simple_status[4]['status_collection'][4] = $status[8];

$status_ids = array();
foreach($simple_status[4]['status_collection'] as $key => $val) {
    $status_ids[$val['hlus_id']] = $val['hlus_id'];
}

if(true){
    $sql = 'SELECT * FROM helpdesk_request ';
    //inner join heldesk_request / user table on hdrid to find the role of assigned user
    $sql .= 'INNER JOIN helpdesk_request_users ';
    $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

    $sql .= 'INNER JOIN helpdesk_list_user_roles ';
    $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

    $sql .= 'WHERE helpdesk_users.user_id IN ('. implode(', ', array_keys($users)) .') ';
    $sql .= 'AND (helpdesk_list_user_roles.hlur_id = 1 OR helpdesk_list_user_roles.hlur_id = 2) ';
    $sql .= 'AND helpdesk_request_users.hdru_status IN (' . implode(', ', $status_ids) . ') ';
}

$assigned_requests = $helper->mysql_fetch_all($sql);

$requests = $assigned_requests;
if($process){
    // This query returns duplicates because of the entries in the intermediary table,
    // , and as such the following code is going to resolve the issue
    $requests = $helper->mysql_fetch_all($sql);
    $processed_requests = array();
    if(is_array($requests) && count($requests) > 0){
        foreach($requests as $key2 => $val2){
            if(!array_key_exists($val2['hdr_id'], $processed_requests )){
                if($val2['hlur_value'] == 'assigned_user'){
                    $processed_requests[] = $val2;
                }
            }
        }
        unset($requests);
        $requests = $processed_requests;
    }
}
