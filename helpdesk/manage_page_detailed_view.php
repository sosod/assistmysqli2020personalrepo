<?php
//Mock Up
$table_width = 600;

/*
 * Alright, The following are functions that I use in this page that I built when I started building this module.
 * Now, these functions are the beginning phase in my refactoring the functionality,
 * and moving all of the business logic to the classes, where they belong. */

function displayList($section, $helper) {
    $status = getStatuses($helper);
    $current_user = getCurrentHelpdeskUser($helper);

    /***********************************/
    //Weird spaghetti code
    //This section is going to handle two types of "statuses" in the current cue
    if($section == 'current_cue'){
        //That's "All Requests" that i have to action in the current queue
        $all_statuses = array(1, 2, 4, 9);

        $sql = 'SELECT * FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        $sql .= 'WHERE helpdesk_users.user_id = '. $current_user['user_id'] .' ';
        $sql .= 'AND ((';

        ///----------------------
        $sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';

        $sql .= 'AND ';
        $sql .= '(';

        $count = 1;
        foreach($all_statuses as $key => $val){
            $sql .= 'helpdesk_request_users.hdru_status = ' . $val ;
            if($count < count($all_statuses)){
                $sql .= ' ';
                $sql .= 'OR ';
            }

            $count++;
        }

        $sql .= ')';
        ///------------------------

        $sql .= ') OR (';
        $sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';

        $sql .= 'AND ';
        $sql .= 'helpdesk_request_users.hdru_status = 3';
        $sql .= '))';
        $all_my_requests = $helper->mysql_fetch_all($sql);

        //Status Count Code (DUPLICATE)
        if(thisArrayIsPopulated($all_my_requests)){
            $status_count = count($all_my_requests);
        }else{
            $status_count = 0;
        }

        $requests_assigned_to_me = '<li><a href="manage_view.php" data-status="nonsense"  data-section="all_mine" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>All Mine (' . $status_count .')</a></li>';

        //And those that have been resolved to me from Above
        $sql = 'SELECT * FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        $sql .= 'WHERE helpdesk_users.user_id = '. $current_user['user_id'] .' ';
        $sql .= 'AND ';
        $sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';
        $sql .= 'AND ';
        $sql .= 'helpdesk_request_users.hdru_status = 9';

        $resolved = $helper->mysql_fetch_all($sql);

        //Status Count Code (DUPLICATE)
        if(thisArrayIsPopulated($resolved)){
            $status_count = count($resolved);
        }else{
            $status_count = 0;
        }

        $requests_assigned_to_me.= '<li><a href="manage_view.php" data-status="nonsense"  data-section="resolved_to_me" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>Resolved to Me (' . $status_count .')</a></li>';

    }
    /***********************************/


    //New Refactored Code - Start
    If($section == 'my_requests'){
        $sql = 'SELECT COUNT(*) AS status_count, hdr_add_status FROM helpdesk_request ';
        $sql .= 'WHERE hdr_add_userid = ' . $current_user['user_id'] . ' ';
        $sql .= 'GROUP BY hdr_add_status ';
        $request = $helper->mysql_fetch_all_by_id($sql, 'hdr_add_status');

        unset($status[10]);
        unset($status[11]);
        unset($status[12]);
    }elseif($section == 'current_cue'){
        $sql = 'SELECT COUNT(*) AS status_count, hdru_status FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        $sql .= 'WHERE helpdesk_users.user_id = '. $current_user['user_id'] .' ';
        $sql .= 'AND ';
        $sql .= '(';
        if($current_user['hdrut_value'] == 'client_admin_user'){
            $sql .= '(helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 1) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 2)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 3)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 3)';
        }elseif($current_user['hdrut_value'] == 'reseller_admin_user'){

            $sql .= '(helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 1) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 3)';
        }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){
            $sql .= '(helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 1) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 3)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 3)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 2)';

        }
        $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 3)';

        $sql .= ') ';
        $sql .= 'GROUP BY hdru_status ';
        $request = $helper->mysql_fetch_all_by_id($sql, 'hdru_status');

        $company_info = getCurrentCompInfo($helper);
        if(!showThePot($company_info)){
            unset($status[11]);
            unset($status[12]);
        }

    }elseif($section == 'escalated'){
        $sql = 'SELECT COUNT(*) AS status_count, hdru_status FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        if($current_user['hdrut_value'] == 'client_admin_user'){
            $sql .= 'WHERE helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';//Reseller admin user

            $sql .= 'AND ';
            $sql .= '(';
            $sql .= '(helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 1) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 3)';
            $sql .= ')';


            $sql .= 'AND helpdesk_request.hdr_id IN ';
            $sql .= '(';
            $sql .= 'SELECT hdr_id FROM helpdesk_request ';
            $sql .= 'INNER JOIN helpdesk_request_users ';
            $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

            $sql .= 'INNER JOIN helpdesk_users ';
            $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

            $sql .= 'INNER JOIN helpdesk_companies ';
            $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

            $sql .= 'INNER JOIN helpdesk_user_types ';
            $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

            $sql .= 'INNER JOIN helpdesk_list_user_roles ';
            $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

            $sql .= 'WHERE helpdesk_users.user_id = '.$current_user['user_id'].' ';
            $sql .= 'AND (helpdesk_list_user_roles.hlur_id = 2 || helpdesk_list_user_roles.hlur_id = 1)';
            $sql .= ')';
        }elseif($current_user['hdrut_value'] == 'reseller_admin_user'){
            $sql .= 'WHERE helpdesk_user_types.hdrut_value = \'iassist_admin_user\' ';//IASSIST admin user
            $sql .= 'AND ';
            $sql .= '(';
            $sql .= '(helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 1) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 2) ';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 1 AND hdr_escalation_level = 3)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 3)';
            $sql .= 'OR (helpdesk_list_user_roles.hlur_id = 2 AND hdr_escalation_level = 2)';
            $sql .= ')';


            $sql .= 'AND helpdesk_request.hdr_id IN ';
            $sql .= '(';
            $sql .= 'SELECT hdr_id FROM helpdesk_request ';
            $sql .= 'INNER JOIN helpdesk_request_users ';
            $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

            $sql .= 'INNER JOIN helpdesk_users ';
            $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

            $sql .= 'INNER JOIN helpdesk_list_user_roles ';
            $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

            $sql .= 'WHERE helpdesk_users.user_id = '.$current_user['user_id'].' ';
            $sql .= 'AND (helpdesk_list_user_roles.hlur_id = 2 || helpdesk_list_user_roles.hlur_id = 1)';
            $sql .= ')';
        }

        $sql .= 'GROUP BY hdru_status ';

        $request = $helper->mysql_fetch_all_by_id($sql, 'hdru_status');


        //get parent company information
        $company_info = getCurrentCompInfo($helper);
        $sql = 'SELECT * FROM helpdesk_companies ';
        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';
        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_id = ' . $company_info['hdc_parent'] . ' ';
        $parent_company_info = $helper->mysql_fetch_one($sql);

        if(!showThePot($parent_company_info)){
            unset($status[11]);
            unset($status[12]);
        }
    }



    //Build the list
    $list = '';
    if(isset($requests_assigned_to_me) && is_string($requests_assigned_to_me) && !empty($requests_assigned_to_me)){
        $list .= '<div style="text-decoration: underline;">Currently On My plate</div>';
        $list .= '<ul style="margin-bottom: 2.5em">';
        $list .= $requests_assigned_to_me;
        $list.= '</ul>';
    }

    $simple_status = array();

    //"New" SIMPLE STATUS
    $simple_status[1]['status_name'] = 'New';
    $simple_status[1]['status_collection'][1] = $status[1];
    $simple_status[1]['status_collection'][2] = $status[2];

    //"In Progress" SIMPLE STATUS
    $simple_status[2]['status_name'] = 'In Progress';
    $simple_status[2]['status_collection'][1] = $status[3];
    $simple_status[2]['status_collection'][2] = $status[4];
    if($section == 'my_requests'){
        $simple_status[2]['status_collection'][3] = $status[5];
    }
    $simple_status[2]['status_collection'][4] = $status[9];
    if(isset($status[10])){
        $simple_status[2]['status_collection'][4] = $status[10];
    }

    if(isset($status[11]) && isset($status[12])){
        $simple_status[3]['status_name'] = 'Transfers';
        $simple_status[3]['status_collection'][1] = $status[11];
        $simple_status[3]['status_collection'][2] = $status[12];
    }

    //"Done" SIMPLE STATUS
    $simple_status[4]['status_name'] = 'Done';
    if($section != 'my_requests'){
        $simple_status[4]['status_collection'][1] = $status[5];
    }
    $simple_status[4]['status_collection'][2] = $status[6];
    $simple_status[4]['status_collection'][3] = $status[7];
    $simple_status[4]['status_collection'][4] = $status[8];



//    echo '<pre style="font-size: 18px">';
//    echo '<p>SIMPLE STATUS</p>';
//    print_r($simple_status);
//    echo '</pre>';

    foreach($simple_status as $key => $val) {
        $status_count = 0;

        $list .= '<div style="text-decoration: underline;">' . $val['status_name'] . '</div>';
        $list .= '<ul style="margin-bottom: 2.5em">';
        foreach($val['status_collection'] as $key2 => $val2){
            $status_id = $val2['hlus_id'];
            $status_name = $val2['hlus_value'];

            $status_id_is_an_index_in_request_array = (thisArrayIsPopulated($request) ? array_key_exists($status_id, $request) : false );

            //Status Count For the Internal Requests
            if(thisArrayIsPopulated($request) && $status_id_is_an_index_in_request_array){
                $status_count = $request[$status_id]['status_count'];
            }else{
                $status_count = 0;
            }

            $list.= '<li><a href="manage_view.php" data-status="' . $status_id . '"  data-section="' . $section . '" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>' . $status_name. ' ('.$status_count.')</a></li>';

        }
        $list.= '</ul>';

    }

    return $list;
    //New Refactored Code - END
}

function displaySupportList($status, $helper){
    $status = $status;
    $helper = $helper;

    $company_info = getCurrentCompInfo($helper);

    //Can you do this with one query?
    //Turns Out You can
    //This following sql statement shows the requests that originate from the current company
    $sql = 'SELECT COUNT(*) AS status_count, hdr_add_status FROM helpdesk_request ';
    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
    $sql .= 'INNER JOIN helpdesk_user_types ';
    $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';
    $sql .= 'GROUP BY hdr_add_status ';
    $request = $helper->mysql_fetch_all_by_id($sql, 'hdr_add_status');

    /*Requests from children --- currently Adapted for the Reseller ---> Client Relationship*/
    if($company_info['hdct_value'] == 'reseller' || $company_info['hdct_value'] == 'iassist'){
        //This query gets all of the external requests from the clients that haven't been claimed yet

        //Start by finding all of the companies that this company is a parent of
        $children = getCurrentCompChildren($company_info, $helper);

        if(thisArrayIsPopulated($children)){

            $children_id_array = array();
            foreach($children as $key => $val){
                $children_id_array[$key] = $val['hdc_id'];
            }

            //This query gets all of the requests that came from the children of the current company, that have been actioned
            $sql = 'SELECT COUNT(*) AS status_count, hdru_status FROM helpdesk_request ';

            /*Change these inner joins to return the add_user when I'm done - START*/
            $sql .= 'INNER JOIN helpdesk_request_users ';
            $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

            $sql .= 'INNER JOIN helpdesk_users ';
            $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

            $sql .= 'INNER JOIN helpdesk_companies ';
            $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

            $sql .= 'INNER JOIN helpdesk_list_user_roles ';
            $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';
            /*Change these inner joins to return the add_user when I'm done - END*/

            $sql .= 'WHERE helpdesk_request.hdr_id IN ';

            // Originates from this child
            $sql .= '(';//Internal select - START
            //sql goes here
            $sql .= 'SELECT hdr_id FROM helpdesk_request ';
            $sql .= 'INNER JOIN helpdesk_users ';
            $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
            $sql .= 'INNER JOIN helpdesk_companies ';
            $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
            $sql .= 'INNER JOIN helpdesk_user_types ';
            $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
            $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_id_array) . ')';
            $sql .= ') ';//Internal select - END

            //Responsibility is in this company
            $sql .= 'AND helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

            $sql .= 'GROUP BY hdru_status ';

            $requests_from_children = $helper->mysql_fetch_all_by_id($sql, 'hdru_status');
        }

    }

    //Build the list
    $list = '<ul>';

    foreach($status as $key => $val) {
        $status_id = $val['hlus_id'];
        $status_name = $val['hlus_value'];

        $status_id_is_an_index_in_request_array = (thisArrayIsPopulated($request) ? array_key_exists($status_id, $request) : false );

        //Status Count For the Internal Requests
        if(thisArrayIsPopulated($request) && $status_id_is_an_index_in_request_array){
            $internal_status_count = $request[$status_id]['status_count'];
        }else{
            $internal_status_count = 0;
        }

        $status_id_is_an_index_in_request_from_children_array = (thisArrayIsPopulated($requests_from_children) ? array_key_exists($status_id, $requests_from_children) : false );

        //Status Count For the External Requests from this companies children
        if(thisArrayIsPopulated($requests_from_children) && $status_id_is_an_index_in_request_from_children_array){
            $children_status_count = $requests_from_children[$status_id]['status_count'];
        }else{
            $children_status_count = 0;
        }

        $status_count = $internal_status_count + $children_status_count;

        $list.= '<li><a href="manage_view.php" data-status="' . $status_id .'"  data-section="support_user" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>' . $status_name. ' ('.$status_count.')</a></li>';

    }

    $list.= '</ul>';
    return $list;
}

function getStatuses($helper) {
    $sql = 'SELECT * FROM helpdesk_list_user_status';
    $status = $helper->mysql_fetch_all_by_id($sql, 'hlus_id');
    return $status;
}

function isThisSupport(){
    $client_db = new ASSIST_MODULE_HELPER();

    $sql = 'SELECT * FROM assist_' . $_SESSION['cc'] . '_timekeep ';
    $sql .= 'WHERE tkid = \''.$_SESSION['tid'] . '\' ';
    $user = $client_db->mysql_fetch_one($sql);

    $is_support = false;
    if($user['tkuser'] == 'support'){
        $is_support = true;
    }

    return $is_support;
}

function getCurrentCompInfo($helper){
    /*
     * These are the company details
     * Including:
     * Company information
     * Preferred way of handling the helpdesk request (settings)
    */

    $sql = 'SELECT * FROM helpdesk_companies ';

    // Company Type
    $sql .= 'INNER JOIN helpdesk_company_types ';
    $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

    // Company helpdesk settings
    $sql .= 'INNER JOIN helpdesk_company_settings ';
    $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

    $sql .= 'INNER JOIN helpdesk_admin_management ';
    $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

    $sql .= 'INNER JOIN helpdesk_request_settings ';
    $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

    // Clauses
    $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
    $company_info = $helper->mysql_fetch_one($sql);

    return $company_info;

}

function getCurrentCompChildren($company_info, $helper){
    //Find all of the companies that this company is a parent of
    $sql = 'SELECT * FROM helpdesk_companies ';

    // Company Type
    $sql .= 'INNER JOIN helpdesk_company_types ';
    $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

    // Company helpdesk settings
    $sql .= 'INNER JOIN helpdesk_company_settings ';
    $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

    $sql .= 'INNER JOIN helpdesk_admin_management ';
    $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

    $sql .= 'INNER JOIN helpdesk_request_settings ';
    $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

    // Clauses
    $sql .= 'WHERE helpdesk_companies.hdc_parent = ' . $company_info['hdc_id'];

    $children = $helper->mysql_fetch_all_by_id($sql, 'hdc_id');

    return $children;
}

function thisArrayIsPopulated($returned_array){
    $thisArrayIsPopulated = false;
    if(isset($returned_array) && is_array($returned_array) && count($returned_array) > 0){
        $thisArrayIsPopulated = true;
    }
    return $thisArrayIsPopulated;
}

function showThePot($company_info){
    $pot = false;
    if(isset($company_info) && is_array($company_info) && count($company_info) > 0){
        if(($company_info['hcs_settings_id'] == 3) && ($company_info['hcs_admin_man_id'] == 3)){
            $pot = true;
        }
    }
    return $pot;
}

function getUnassignedInternalRequests($company_info, $helper, $unassigned){
    //This query gets all of the internal requests that haven't been claimed yet
    $sql = 'SELECT * FROM helpdesk_request ';

    $sql .= 'WHERE helpdesk_request.hdr_id IN ';
    $sql .= '(';//Internal select - START

    $sql .= 'SELECT hdr_id FROM helpdesk_request ';

    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

    $sql .= 'AND helpdesk_request.hdr_add_status <> 6 ';//Request is not cancelled
    $sql .= 'AND helpdesk_request.hdr_add_status <> 8 ';//Request is not closed/Terminated

    //USER TYPE STUFF HACK #1
    if((int)$company_info['hdc_type'] == 1){//Client
        $sql .= 'AND helpdesk_users.user_type = 1 ';//Client User
    }elseif((int)$company_info['hdc_type'] == 2){//Reseller
        $sql .= 'AND helpdesk_users.user_type = 4 ';//Reseller User
    }

    $sql .= ') ';//Internal select - END

    $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
    $sql .= '(';//Internal select - START

    $sql .= 'SELECT hdr_id FROM helpdesk_request ';
    $sql .= 'INNER JOIN helpdesk_request_users ';
    $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

    $sql .= 'INNER JOIN helpdesk_users ';
    $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

    //USER TYPE STUFF HACK #2
    if((int)$company_info['hdc_type'] == 1){//Client
        $sql .= 'AND helpdesk_users.user_type = 2 ';//Client Admin User
    }elseif((int)$company_info['hdc_type'] == 2){//Reseller
        $sql .= 'AND helpdesk_users.user_type = 3 ';//Reseller Admin User
    }

    $sql .= ')';//Internal select - END

    $internal_unassigned = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');
    if(isset($internal_unassigned) && is_array($internal_unassigned) && count($internal_unassigned) > 0){
        $unassigned = array_merge($unassigned, $internal_unassigned);
    }

    return $unassigned;
}


function getUnassignedRequestsFromChildren($company_info, $children, $current_user, $helper, $unassigned){
    $children_with_admin_id_array = array();
    $children_without_admin_id_array = array();
    foreach($children as $key => $val){
        $children_id_array[$key] = $val['hdc_id'];

        if(($val['hcs_admin_man_id'] == 2 && $val['hcs_settings_id'] == 2) || ($val['hcs_admin_man_id'] == 3 && $val['hcs_settings_id'] == 3)){
            // These settings require admin involvement
            $children_with_admin_id_array[$key] = $val['hdc_id'];
        }elseif($val['hcs_admin_man_id'] == 1 && $val['hcs_settings_id'] == 1) {
            // This is an auto escalation - Which means it comes from a client,
            // seeing that we said that resellers MUST have at least one admin
            $children_without_admin_id_array[$key] = $val['hdc_id'];
        }else{
            //This should never happen
        }
    }

    if(thisArrayIsPopulated($children_with_admin_id_array)){
        // These settings require admin involvement

        //This query gets all of the external requests that haven't been claimed yet
        $sql = 'SELECT * FROM helpdesk_request ';

        $sql .= 'WHERE ';
        $sql .= '(';//Group Internal selects - START
        $sql .= 'helpdesk_request.hdr_id IN ';

        // The add_user is s client_admin_user
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_with_admin_id_array) . ') ';

        $sql .= 'AND helpdesk_request.hdr_add_status <> 6 ';//Request is not cancelled
        $sql .= 'AND helpdesk_request.hdr_add_status <> 8 ';//Request is not closed/Terminated

        if($current_user['hdrut_value'] == 'reseller_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
        }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
        }

        $sql .= ') ';//Internal select - END

        $sql .= 'OR helpdesk_request.hdr_id IN ';

        // The client_admin_user is an interested party, and his status is escalated
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_with_admin_id_array) . ') ';
        if($current_user['hdrut_value'] == 'reseller_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
        }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){
            $sql .= 'AND helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
        }
        $sql .= 'AND helpdesk_request_users.hdru_role = 2 ';
        $sql .= 'AND helpdesk_request_users.hdru_status = 9 ';
        $sql .= ') ';//Internal select - END
        $sql .= ') ';//Group Internal selects - END


        //No Entry for it in this company
        $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
        $sql .= '(';//Internal select - START

        $sql .= 'SELECT hdr_id FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$company_info['hdc_cc'].'\' ';

        $sql .= ')';//Internal select - END

        $unassigned_child = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');

        if(isset($unassigned_child) && is_array($unassigned_child) && count($unassigned_child) > 0){
            $unassigned = array_merge($unassigned, $unassigned_child);
        }
    }

    if(thisArrayIsPopulated($children_without_admin_id_array)) {
        // This is an auto escalation - Which means it comes from a client, seeing that we said that resellers MUST have admins

        //This query gets all of the external requests that haven't been claimed yet
        $sql = 'SELECT * FROM helpdesk_request ';

        $sql .= 'WHERE helpdesk_request.hdr_id IN ';

        // The add_user is s client_user
        $sql .= '(';//Internal select - START
        $sql .= 'SELECT hdr_id FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_id IN ('.implode(', ', $children_without_admin_id_array) . ')';
        $sql .= 'AND helpdesk_user_types.hdrut_value = \'client_user\' ';
        $sql .= ') ';//Internal select - END

        //No Entry for it in this company
        $sql .= 'AND helpdesk_request.hdr_id NOT IN ';
        $sql .= '(';//Internal select - START

        $sql .= 'SELECT hdr_id FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \'' . $company_info['hdc_cc'] . '\' ';

        $sql .= ')';//Internal select - END

        $unassigned_child = $helper->mysql_fetch_all_by_id($sql, 'hdr_id');
        if(isset($unassigned_child) && is_array($unassigned_child) && count($unassigned_child) > 0){
            $unassigned = array_merge($unassigned, $unassigned_child);
        }
    }

    return $unassigned;
}

function displayPot($company_info, $helper, $current_user){
    $unassigned = array();
    if($current_user['hdrut_value'] == 'client_admin_user' || $current_user['hdrut_value'] == 'reseller_admin_user'){
        $unassigned = getUnassignedInternalRequests($company_info, $helper, $unassigned);
    }

    if($current_user['hdrut_value'] == 'reseller_admin_user' || $current_user['hdrut_value'] == 'iassist_admin_user'){
        //Start by finding all of the companies that this company is a parent of
        $children = getCurrentCompChildren($company_info, $helper);
        if(thisArrayIsPopulated($children)){
            $unassigned = getUnassignedRequestsFromChildren($company_info, $children, $current_user, $helper, $unassigned);
        }

    }

    //Status Count Code (DUPLICATE)
    if(thisArrayIsPopulated($unassigned)){
        $status_count = count($unassigned);
    }else{
        $status_count = 0;
    }

    $list = '<ul>';
    $list .= '<li><a href="manage_view.php" data-status="unassigned" data-section="the_pot" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>Unassigned (' . $status_count . ')</a></li>';
    $list .= '</ul>';

    return $list;
}

$status = getStatuses($helper);

$support_user = isThisSupport();

$company_info = getCurrentCompInfo($helper);

$pot = showThePot($company_info);

?>

<?php if($current_user){// If the user exists in the helpdesk db ?>
    <table width="<?php echo $table_width ?>">
        <tr>
            <?php if($current_user['hdc_cc'] != 'iassist'){  ?>
                <th width="200">My Helpdesk Requests</th>
            <?php } ?>
            <?php if($current_user['hdrut_value'] == 'client_admin_user' || $current_user['hdrut_value'] == 'reseller_admin_user' || $current_user['hdrut_value'] == 'iassist_admin_user'){//Is this an admin user ?>
                <th width="200">Current Queue</th>
                <?php if($current_user['hdc_cc'] != 'iassist'){  ?>
                    <th width="200">Escalated</th>
                <?php } ?>
                <?php if($pot){  ?>
                    <th width="200">Unassigned</th>
                <?php } ?>
            <?php } ?>
        </tr>
        <tr>
            <?php if($current_user['hdc_cc'] != 'iassist'){  ?>
                <!-- My Requests -->
                <td class="top">
                    <?php echo displayList('my_requests', $helper); ?>
                </td>
            <?php } ?>

            <?php if($current_user['hdrut_value'] == 'client_admin_user' || $current_user['hdrut_value'] == 'reseller_admin_user' || $current_user['hdrut_value'] == 'iassist_admin_user'){//Is this an admin user ?>
                <!-- Escalated Here -->
                <td class="top">
                    <?php echo displayList('current_cue', $helper); ?>
                </td>

                <?php if($current_user['hdc_cc'] != 'iassist'){  ?>
                    <!-- Escalated Upwards -->
                    <td class="top">
                        <?php echo displayList('escalated', $helper); ?>
                    </td>
                <?php } ?>

                <?php if($pot){  ?>
                    <!-- The Pot To Be Claimed From -->
                    <td class="top">
                        <?php echo displayPot($company_info, $helper, $current_user); ?>
                    </td>
                <?php } ?>
            <?php } ?>
        </tr>
    </table>
<?php }elseif($support_user){ ?>
    <table width="<?php echo $table_width ?>">
        <tr>
            <th width="200">All Requests</th>
        </tr>
        <tr>
            <td class="top">
                <?php echo displaySupportList($status, $helper); ?>
            </td>
        </tr>
    </table>
<?php }else{ ?>
    <p>You are currently not associated with any Helpdesk Requests</p>
<?php } ?>


<?php
/*
 * This is where I'll be testing code for the time being before I move it all the way to the functions*/

?>


<?php
//echo '<pre style="font-size: 18px">';
//echo '<p>COMPANY INFORMATION</p>';
//print_r($company_info);
//echo '</pre>';
//
//$contact_details = $helper->getAnEmail($_SESSION['tid'], $_SESSION['cc']);
//
//echo '<pre style="font-size: 18px">';
//echo '<p>CONTACT INFO</p>';
//var_dump($contact_details);
//echo '</pre>';
?>

<script type="text/javascript">
    $(function(){

        $('ul li a').click(function(e){
            e.preventDefault();
            console.log('Done clicked this');
            document.location.href = 'manage_view.php?status=' + $(this).data('status') + '&section=' + $(this).data('section');
        });
    });
</script>
