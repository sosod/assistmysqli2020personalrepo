<?php require_once('inc_header.php'); ?>
<?php ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r']) ? $_REQUEST['r'] : array())); ?>
<?php

$section = "NEW";
$sub_page = "NEW";

$button_icon = "OPEN";
$button_label = "Continue";
$button_url = "new_create.php?";
$button_text = false;

//Check if the current user is the add user or the assigned user to determine the actions
$sql = 'SELECT * FROM helpdesk_users ';
$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
$sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'].' ';
$sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'].'\'';
$user = $helper->mysql_fetch_one($sql);

//This get's the information for this request so that I can populate the table  with the topic and module
$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

$sql .= 'INNER JOIN helpdesk_list_topic ';
$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

$sql .= 'INNER JOIN helpdesk_list_module ';
$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

$sql .= 'INNER JOIN helpdesk_users ';
$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

$sql .= 'WHERE helpdesk_request.hdr_id = '.$_GET['hdrid'];

$request = $helper->mysql_fetch_one($sql);


if(is_null($request)) {//This should only happen when the reseller requests a new client db
	$sql = 'SELECT * FROM helpdesk_request ';
	$sql .= 'INNER JOIN helpdesk_list_topic ';
	$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

	$sql .= 'INNER JOIN helpdesk_companies ';
	$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

	$sql .= 'INNER JOIN helpdesk_user_types ';
	$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

	$sql .= 'WHERE helpdesk_request.hdr_id = '.$_GET['hdrid'];

	$request = $helper->mysql_fetch_one($sql);
}

//Get Topics and modules
//Is this user in our helpdesk database\
$is_hd_user = false;

//Get Topics
$listObject = new HELPDESK_LIST('topic');
$topics = $listObject->getActiveListItemsFormattedForSelect();

if($_SESSION['ia_reseller'] != 1) {//Only give resellers the option to request new client dbs
	unset($topics[6]);
}

//Get Topics
$listObject->changeListType('module');
$modules = $listObject->getActiveListItemsFormattedForSelect();

$full_module_list = $listObject->getActiveListItems();
?>

<form name="edit_request" method="post" enctype="multipart/form-data">

	<input type="hidden" id="user_tk_id" name="user_tk_id" value="<?php echo $request['user_tkid']; ?>">
	<input type="hidden" id="user_comp_cc" name="user_comp_cc" value="<?php echo $request['hdc_cc']; ?>">
	<input id="user_id" class="user_id" name="user_id" type="hidden" value="<?php echo $user['user_id']; ?>">
	<input type="hidden" id="hdrid" name="hdrid" value="<?php echo $request['hdr_id']; ?>">

	<table id="tbl_action" class="form">
		<tbody>
		<tr class="">
			<th>Request By:</th>
			<td><?php echo $request['user_name']; ?></td>
		</tr>
		<tr>
			<th id="th_helpdesk_request_topicid">Topic:</th>
			<td>
				<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_topicid', 'name' => 'hdr_topicid', 'required' => true, 'unspecified' => false, 'options' => $topics)); ?>
			</td>
		</tr>
		<tr>
			<th id="th_helpdesk_request_module">Module:</th>
			<td id="dynamic_modules">
				<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_moduleid', 'name' => 'hdr_moduleid', 'required' => true, 'unspecified' => false, 'options' => $modules)); ?>
			</td>
		</tr>
		<tr>
			<th id="th_helpdesk_request_description">Description:</th>
			<td>
				<?php $js .= $displayObject->drawFormField('TEXT', array('id' => 'hdrup_description', 'name' => 'hdrup_description', 'required' => true)); ?>
			</td>
		</tr>
		<tr>
			<th id="th_hdr_description">Attachments:</th>
			<td>
				<?php
				$something_else = $displayObject->getAttachForDisplay($request['hdr_attachment'], false, 'REQUEST', $request['hdr_id'], true);
				echo $something_else;
				//                    $js .= $something_else['js'];
				$js .= $displayObject->getAttachmentDownloadJS('REQUEST', $request['hdr_id']);
				?>

			</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<input id="btn_submit" type="button" value="Submit" data-hdrid="<?php echo $request['hdr_id']; ?>"
					   class="isubmit i-am-the-submit-button">
				<input type="reset" value="Reset" name="B2">
			</td>
		</tr>
		</tbody>
	</table>
</form>

<?php

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($_SESSION);
//echo '</pre>';

?>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$('#btn_submit').click(function () {
//            console.log($('form').serialize());
			HelpdeskHelper.processObjectForm($('form[name="edit_request"]'), 'REQUEST.HDR_EDIT', 'manage_view_object.php?hdrid=' + $(this).data('hdrid') + '&');
		});

		/*
		 * What follows is the dynamic module functionality
		 * Currently commented out with the intention of becoming
		 * fully functional when I get back from Dhamma Pataka
		 * */

		$('#hdr_topicid').change(function (e) {
			e.preventDefault();
			var value = $(this).val();
			var user_tkid = $('#user_tk_id').val();
			var comp_code = $('#user_comp_cc').val();
			var dta = {topic_id: value, user_tkid: user_tkid, cc: comp_code};
			// console.log(value);
			// console.log(dta);

			var page_action = 'REQUEST.GETMODULES';
			var url = 'inc_controller.php?action=' + page_action;
			var result = AssistHelper.doAjax(url, dta);

//            console.log(result);

			$('#dynamic_modules').empty().html(result)
		});
	});
</script>

<?php require_once('inc_footer.php'); ?>

