<?php require_once('inc_header.php'); ?>
<h2>Companies</h2>
<?php
$hdc_object = new HELPDESK_COMPANY();
$current_company_info = $hdc_object->getActiveCompanyInformation($_SESSION['cc']);

$isReseller = false;
if($current_company_info['hdct_value'] == 'reseller'){
    $isReseller = true;
}

//Get every registered company
$db = new ASSIST_DB('master');
$sql = 'SELECT cmpcode, cmpname, cmp_is_demo FROM assist_company ';
$sql .= 'WHERE cmpcode <> \'BLANK\' AND cmp_is_demo <> 1 ';
$sql .= ($isReseller ? 'AND cmpreseller = \'' . $current_company_info['hdc_cc'] . '\' AND cmpcode <> \'' . $current_company_info['hdc_cc'] . '\' ' : '' );
$ait_companies = $db->mysql_fetch_all($sql);

$sql = 'SELECT * FROM helpdesk_companies ';
$sql .= ($isReseller ? 'WHERE hdc_parent = \'' . $current_company_info['hdc_id'] . '\' ' : '' );
$helpdesk_companies = $helper->mysql_fetch_all_by_id($sql, 'hdc_cc');

$show_manage_button = false;
?>

<form name="companies" method="post" action="manage.php" enctype="multipart/form-data">
    <table class="list " id="paging_obj_list_view" width="100%">
        <tbody>
        <tr id="head_row">
            <th>Company Name:</th>
            <th>Company Code:</th>
            <th>ADD/REMOVE:</th>
            <th></th>
        </tr>
        <?php foreach($ait_companies as $key => $val){ ?>
            <tr>
                <td><?php echo $val['cmpname']; ?></td>
                <td><?php echo $val['cmpcode']; ?></td>
                <td class="center">
                    <?php if(strtolower($val['cmpcode']) != 'iassist'){ ?>
                        <?php if((array_key_exists($val['cmpcode'], $helpdesk_companies) && (int)$helpdesk_companies[$val['cmpcode']]['hdc_active'] == 1) || (array_key_exists(strtolower($val['cmpcode']), $helpdesk_companies) && (int)$helpdesk_companies[strtolower($val['cmpcode'])]['hdc_active'] == 1)  || (array_key_exists(strtoupper($val['cmpcode']), $helpdesk_companies) && (int)$helpdesk_companies[strtoupper($val['cmpcode'])]['hdc_active'] == 1)){ ?>
                            <?php $show_manage_button = true; ?>
                            <input type="submit" data-action="del" data-cc="<?php echo $val['cmpcode']; ?>"  value="Delete from Helpdesk" ref="3" class="btn_company_change">
                        <?php }else{ ?>
                            <input type="submit" data-action="add" data-cc="<?php echo $val['cmpcode']; ?>"  value="Add to Helpdesk" ref="3" class="btn_company_change">
                        <?php } ?>
                    <?php }else{ ?>
                        <?php $show_manage_button = true; ?>
                    <?php } ?>
                </td>
                <td class="center">
                    <?php if($show_manage_button == true){ ?>
                        <input type="submit" data-cc="<?php echo $val['cmpcode']; ?>"  value="Manage" ref="3" class="company_manage">
                    <?php } ?>
                </td>
            </tr>
            <?php $show_manage_button = false; ?>
        <?php } ?>

        </tbody>
    </table>
</form>


<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>
        $('.btn_company_change').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var value = [];
            value["cc"] = $(this).data('cc');
            var dta = {hdc_cc : value["cc"]};
            console.log(value);
            console.log(dta);

            var button_action = $(this).data('action');

            var action;
            if(button_action == 'add'){
                action = 'COMPANY.SIMPLEADD';
            }else{
                action = 'COMPANY.DELETE';
            }

            var page_action = action;
            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'setup_defaults_helpdesk_access.php?');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });

        $('.company_manage').click(function(e){
            e.preventDefault();
            document.location.href = 'setup_defaults_manage_admins.php?company_cc=' + $(this).data('cc');
        });
    });
</script>

<?php require_once('inc_footer.php'); ?>
