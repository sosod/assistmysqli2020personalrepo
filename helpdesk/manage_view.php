<?php $display_navigation_buttons = false; ?>
<?php require_once('inc_header.php'); ?>
<?php
//Status
$sql = 'SELECT * FROM helpdesk_list_user_status ';
if($_GET['status'] != 'nonsense' && $_GET['status'] != 'unassigned'){
    if(isset($_GET['section']) && ($_GET['section'] == 'my_requests' || $_GET['section'] == 'current_cue')){
        $sql .= 'WHERE helpdesk_list_user_status.hlus_id = '. $_GET['status'] . ' ';
    }
}
$status = $helper->mysql_fetch_all_by_id($sql, 'hlus_id');
?>
<?php
if(isset($_GET['table_view'])){
    require_once('manage_view_page_simple_view.php');
}elseif(isset($_GET['view_all'])){
    require_once('manage_view_page_view_all.php');
}elseif(isset($_GET['view_all_complete'])){
    require_once('manage_view_page_view_all_complete.php');
}else{
    require_once('manage_view_page_detailed_view.php');
}
?>
<table class="list " id="paging_obj_list_view" width="100%">
    <tbody>
    <tr id="head_row">
        <th>Ref</th>
        <th>Owner</th>
        <th>Description</th>
        <th>Date</th>
        <th>Status</th>
        <th>Assigned User</th>
        <th></th>
    </tr>
    <?php foreach($requests as $key => $val){ ?>
        <?php
        //Find add_user for this specific request
        $sql = 'SELECT * FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'WHERE helpdesk_request.hdr_id = '. $val['hdr_id'] . ' ';
        $sql .= 'AND hdr_add_userid = '. $val['hdr_add_userid'] .' ';
        $add_user = $helper->mysql_fetch_one($sql);

        //Find the Assigned user for this specific request
        $sql = 'SELECT * FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        $sql .= 'WHERE helpdesk_request.hdr_id = '. $val['hdr_id'] . ' ';
        $sql .= 'AND ';
        $sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';
        $assigned_user = $helper->mysql_fetch_one($sql);

        //Find the Current user for this specific request
        $sql = 'SELECT * FROM helpdesk_request ';
        $sql .= 'INNER JOIN helpdesk_request_users ';
        $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_list_user_roles ';
        $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

        $sql .= 'WHERE helpdesk_request.hdr_id = '. $val['hdr_id'] . ' ';
        $sql .= 'AND ';
        $sql .= 'helpdesk_users.user_id = '. (isset($user) && is_array($user) && count($user) > 0 ? $user['user_id'] : $val['hdr_add_userid'] ) .' ';
        $current_user = $helper->mysql_fetch_one($sql);

        //Determine which status to use
        if($user['user_id'] == $add_user['user_id']){
            $status_to_use = $val['hdr_add_status'];
        }elseif(isset($assigned_user) && is_array($assigned_user) && count($assigned_user) > 0 && ($user['user_id'] == $assigned_user['user_id'] || (isset($users) && is_array($users) && count($users) > 0  && in_array($assigned_user['user_id'], array_keys($users))))){
            $status_to_use = $assigned_user['hdru_status'];
        }elseif(isset($current_user) && is_array($current_user) && count($current_user) > 0){//not the assigned user
            $status_to_use = $current_user['hdru_status'];
        }else{//not assigned to anyone
            $status_to_use = $val['hdr_add_status'];
        }

        $hdr_description = $val['hdr_description'];
        $hdr_description = str_replace(chr(10),"<br/>", $hdr_description);
        ?>
    <tr>
        <td class=""><?php echo strtoupper($add_user['hdc_cc']). '/HDR'. $val['hdr_id']; ?></td>
        <td class=""><?php echo $add_user['user_name'] . ' (' .$add_user['hdc_name'] .  ')'; ?></td>
        <td class=""><?php echo $hdr_description; ?></td>
        <td class=""><?php echo $val['hdr_add_date']; ?></td>
        <td class=""><?php echo $status[$status_to_use]['hlus_value']; ?></td>
        <td class=""><?php echo ($assigned_user['user_name'] != '' ? $assigned_user['user_name'] . ' (' . $assigned_user['hdc_name'].  ')' : 'Currently Unassigned' ); ?></td>
        <td class="center"><input type="button" value="View" ref="3" data-hdrid="<?php echo $val['hdr_id']; ?>" data-status="<?php echo $_GET['status']; ?>" data-section="<?php echo (isset($_GET['section']) ? $_GET['section'] : 'undefined' ); ?>" data-table_view="<?php echo (isset($_GET['table_view']) ? $_GET['table_view'] : 'undefined' ); ?>" data-view_all="<?php echo (isset($_GET['view_all']) ? $_GET['view_all'] : 'undefined' ); ?>" data-view_all_complete="<?php echo (isset($_GET['view_all_complete']) ? $_GET['view_all_complete'] : 'undefined' ); ?>" class="btn_list"></td>
    </tr>
    <?php } ?>
    </tbody>
</table>


<script type="text/javascript">
    $(function(){

        $('.btn_list').click(function(e){
            e.preventDefault();
            console.log('Go to the single');

            var redirect_url = 'manage_view_object.php?hdrid=' + $(this).data('hdrid') + '&status=' + $(this).data('status');

            if($(this).data('status') != 'undefined'){
                redirect_url += '&section=' + $(this).data('section');
            }

            if($(this).data('table_view') != 'undefined'){
                redirect_url += '&table_view=' + $(this).data('table_view');
            }

            if($(this).data('view_all') != 'undefined'){
                redirect_url += '&view_all=' + $(this).data('view_all');
            }

            if($(this).data('view_all_complete') != 'undefined'){
                redirect_url += '&view_all_complete=' + $(this).data('view_all_complete');
            }

            document.location.href = redirect_url;
        });
    });
</script>

<?php require_once('inc_footer.php'); ?>
