<?php require_once('inc_header.php'); ?>
<?php
/*
 * These are the company details
 * Including:
 * Company information
 * Admin users, if any
 * Preferred way of handling the helpdesk request (settings)
*/

if(isset($_GET['company_cc'])){
    $company_cc = $_GET['company_cc'];
}else{
    $company_cc = $_SESSION['cc'];
}

$sql = 'SELECT * FROM helpdesk_companies ';

// Company Type
$sql .= 'INNER JOIN helpdesk_company_types ';
$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

// Company helpdesk settings
$sql .= 'INNER JOIN helpdesk_company_settings ';
$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

$sql .= 'INNER JOIN helpdesk_admin_management ';
$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

$sql .= 'INNER JOIN helpdesk_request_settings ';
$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

// Clauses
$sql .= 'WHERE helpdesk_companies.hdc_cc = \''. $company_cc . '\'';
$company_info = $helper->mysql_fetch_one($sql);

$client_db = new ASSIST_MODULE_HELPER('client', $company_cc);
?>

<?php if(isset($company_info) && is_array($company_info) && count($company_info) > 0){ ?>
    <h2>Company Name: <?php echo $company_info['hdc_name']; ?></h2>

    <?php
    ASSIST_HELPER::displayResult(
        isset($_REQUEST['r']) ? $_REQUEST['r'] : array()
    );
    ?>

    <p><strong>Admin Management:</strong> <?php echo $company_info['hdam_value']; ?></p>

    <p><strong>Helpdesk Setting:</strong> <?php echo $company_info['hdrs_value']; ?></p>

    <div id=div_layout>
        <?php
        $sql = 'SELECT * FROM helpdesk_company_user_access ';
        $company_user_access_values = $helper->mysql_fetch_all_by_id($sql, 'hcua_id');
        ?>
        <h5>Helpdesk User Access:</h5>
        <table>
            <input type="hidden" id="current_login_company_code" name="current_login_company_code" value="<?php echo strtolower($_SESSION['cc']) ?>">
            <input type="hidden" id="current_manage_company_code" name="current_manage_company_code" value="<?php echo strtolower($company_info['hdc_cc']) ?>">
            <input type="hidden" id="current_user_access_value" name="current_user_access_value" value="<?php echo $company_info['hcs_hcua_id'] ?>">
            <?php foreach($company_user_access_values as $key => $val){ ?>
            <tr>
                <td class=b style="width: 200px"><strong style="color: <?php echo ($company_info['hcs_hcua_id'] == $val['hcua_id'] ? 'green' : 'black') ?>"><?php echo $val['hcua_value'] ?></strong></td>
                <td><input type="radio" class="heldesk_user_access" name="heldesk_user_access" value="<?php echo $val['hcua_id'] ?>" <?php echo ($company_info['hcs_hcua_id'] == $val['hcua_id'] ? 'checked' : '') ?>></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="2" style="text-align: center"><input type="submit" class="isubmit" id="heldesk_user_access_submit" name="heldesk_user_access_submit" value="Save"></td>
            </tr>
        </table>
    </div>

    <?php if($company_info['hdam_id'] != 1 && $company_info['hdrs_id'] != 1){//If there is an admin and the the settings aren't ?>
        <?php

        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'] . ' ';
        $sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
        $user = $helper->mysql_fetch_one($sql);


        //Get the admins of this company to display them in the table
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        //Admin users
        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_user_admin_types ';
        $sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

        $sql .= 'INNER JOIN helpdesk_admin_types ';
        $sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$company_info['hdc_cc'] . '\'';
        $company_admins = $helper->mysql_fetch_all($sql);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINTHERE</p>';
//        print_r($company_admins);
//        echo '</pre>';

        $sql = 'SELECT * FROM helpdesk_companies ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_useraccess ';
        $sql .= 'ON  helpdesk_useraccess.ua_user_id = helpdesk_users.user_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$company_info['hdc_cc'] . '\'';

        $user_access = $helper->mysql_fetch_all_by_id($sql, 'user_id');

//        echo '<pre style="font-size: 18px">';
//        echo '<p>USER ACCESS</p>';
//        var_dump($user_access);
//        echo '</pre>';

        $show_admin_user_view = false;
        if($_SESSION['ia_reseller'] == 1 && $company_info['hdct_value'] != 'client'){
            $show_admin_user_view = true;
        }
        ?>
        <?php if(count($company_admins) > 0){ ?>
            <table class="list " id="paging_obj_list_view">
                <tbody>
                <tr id="head_row">
                    <th style="width: 200px;">Admin User Name</th>
                    <th>Remove Admin User</th>
                    <th>Grant/Revoke Setup Access</th>
                    <?php if($show_admin_user_view == true){ ?>
                    <th>View Admin User</th>
                    <?php } ?>
                </tr>

                <?php foreach($company_admins as $key => $val){ ?>
                    <tr>
                        <td class=""><?php echo $val['user_name']; ?></td>
                        <?php if($val['user_id'] != $user['user_id']){ ?>
                        <td class="center"><input type="button" value="Delete" ref="3" data-userid="<?php echo $val['user_id']; ?>" class="btn_admin_del"></td>
                        <?php
                            $user_id = $val['user_id'];
                            if(isset($user_access[$user_id]) && $user_access[$user_id]['ua_setup'] == '1'){
                                $value = 'Revoke Setup Access';
                                $action = 'revoke_access';
                            }else{
                                $value = 'Grant Setup Access';
                                $action = 'grant_access';
                            }
                        ?>
                        <td class="center"><input type="button" value="<?php echo $value; ?>" ref="4" data-action="<?php echo $action; ?>" data-userid="<?php echo $val['user_id']; ?>" class="btn_setup_access"></td>
                    <?php }else{ ?>
                        <?php $show_notification = true; ?>
                            <td class="center">*</td>
                            <td class="center">*</td>
                    <?php } ?>
                    <?php if($show_admin_user_view == true){ ?>
                        <td class="center"><input type="button" value="View" ref="3" data-userid="<?php echo $val['user_id']; ?>" class="btn_admin_view"></td>
                    <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    <?php } ?>


    <?php
        if(isset($show_notification) && $show_notification == true){
            ASSIST_HELPER::displayResult(array("info","* = One cannot remove oneself as an admin user on the Helpdesk system, nor can one revoke one's own access to setup."));
        }
    ?>
    <br/>
    <br/>


    <?php
    $test_array = array('0002', '0003', '0004', '0005', '0007');

    $sql = 'SELECT * FROM assist_'. strtolower($company_info['hdc_cc']).'_timekeep ';
    $sql .= 'WHERE tkstatus = \'1\' ';
    $sql .= 'AND tkid <> \'0000\' ';
    $sql .= 'AND tkuser <> \'support\' ';

    if(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0){


        $blank_array = array();

        for($i = 0; $i <  count($company_admins); $i++){
            $blank_array[] = $company_admins[$i]['user_tkid'];

        }
        $sql .= 'AND tkid NOT IN (\'' . implode('\',\'', $blank_array) . '\') ';
    }


    $users = $client_db->mysql_fetch_all($sql);

//    echo '<pre style="font-size: 18px">';
//    echo '<p>PRINTHERE</p>';
//    print_r($users);
//    echo '</pre>';

    $user_list = array();

    foreach($users as $key => $val){
        if($val['tkuser'] != 'support'){
            $user_list[$val['tkid']] = $val['tkname'] . ' ' . $val['tksurname'];
        }
    }
    ?>
    <form name="add_admin" method="post" enctype="multipart/form-data">
        <table id="tbl_action" class="form">
            <tbody>
            <!-- Decide On An Action To Take - START -->
            <tr>
                <th id="th_helpdesk_request_topicid">Users:</th>

                <td>
                    <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'helpdesk_user', 'name' => 'helpdesk_user', 'required' => true, 'unspecified' => false,  'options' => $user_list));?>
                </td>
                <input id="helpdesk_company" class="helpdesk_company" name="helpdesk_company" type="hidden" value="<?php echo $company_info['hdc_cc']; ?>">
            </tr>
            <!-- Decide On An Action To Take - END -->
            <tr>
                <th></th>
                <td>
                    <input id="btn_submit" type="button" value="Add Admin" data-hdrid="user_id" class="isubmit i-am-the-submit-button">
                </td>
            </tr>
            </tbody>
        </table>
    </form>

    <?php
//    echo '<pre style="font-size: 18px">';
//    echo '<p>COMPANY INFORMATION</p>';
//    print_r($company_info);
//    echo '</pre>';
//
//    $contact_details = $helper->getAnEmail($_SESSION['tid'], $_SESSION['cc']);
//
//    echo '<pre style="font-size: 18px">';
//    echo '<p>CONTACT INFO</p>';
//    print_r($contact_details);
//    echo '</pre>';
    ?>
<?php }else{ ?>
    <?php
    /*
     * This company doesn't exist in the helpdesk db,
     * so here's a button to initialise it,
     * so that we have initial admin management and request settings
     * */
//    echo '<pre style="font-size: 18px">';
//    echo '<p>PRINTHERE</p>';
//    print_r($_SESSION);
//    echo '</pre>';


    $sql = 'SELECT * FROM helpdesk_companies ';
    $sql .= 'WHERE helpdesk_companies.hdc_cc = \'';
    if($_SESSION['ia_reseller'] == 1){
        $sql .= 'iassist';
        $info_message = 'This company cannot be registered in the helpdesk system until the parent company (IASSIST) is registered in the helpdesk system first';
    }elseif($_SESSION['ia_reseller'] == 0){
        $sql .= $_SESSION['ia_cmp_reseller'];
        $info_message = 'This company cannot be registered in the helpdesk system until the parent company ('. $_SESSION['bpa_details']['cmpname'] .') is registered in the helpdesk system first';
    }
    $sql .= '\'';

    $parent_comp = $helper->mysql_fetch_one($sql);
    if($_SESSION['cc'] == 'iassist' || (isset($parent_comp) && is_array($parent_comp) && count($parent_comp) > 0)){
        ?>
        <h3>This company has not been registered in the helpdesk system yet, click the "Register Company" button to do so.</h3>
        <form name="create_company" method="post" enctype="multipart/form-data">
            <input id="btn_submit" type="button" value="Register Company" class="btn_list">
        </form>
    <?php }else{ ?>
        <h3><?php echo $info_message; ?></h3>
    <?php } ?>

<?php } // end else?>


<?php
////Test email softwarer from locahost
//$emailer = new ASSIST_EMAIL();
//$to = 'mosnubian@gmail.com';
//$emailer->setRecipient($to);
//$sub = 'Localhost Email Test';
//$emailer->setSubject($sub);
//$message = 'This is the message you should be getting';
//$emailer->setBody($message);
//$emailer->sendEmail();
?>

<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>

        //Create the current company because it does not exist in the helpdesk db
        $('form[name="create_company"] #btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="create_company"]'), 'COMPANY.ADD', 'setup_defaults_manage_admins.php?company_cc=<?php echo $company_cc; ?>&')
        });

        $('form[name="add_admin"] #btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="add_admin"]'), 'USERS.ADDADMINUSER', 'setup_defaults_manage_admins.php?company_cc=<?php echo $company_cc; ?>&')
        });

        $('.btn_admin_del').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var value = [];
            value["user_id"] = $(this).data('userid');
            var dta = {user_id : value["user_id"]};
            console.log(value);
            console.log(dta);

            var page_action = 'USERS.DEMOTEADMINUSER';
            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'setup_defaults_manage_admins.php?company_cc=<?php echo $company_cc; ?>&');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });

        $('.btn_admin_view').click(function(e){
            e.preventDefault();
            document.location.href = 'setup_defaults_view_admin.php?user_id=' + $(this).data('userid');
        });


        $('.btn_setup_access').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var user_id = $(this).data('userid');
            var dta = {user_id : user_id};

            var action = $(this).data('action');

            var page_action;
            if(action == 'grant_access'){
                page_action = 'USERS.GRANTSETUPACCESS';
            }else{
                page_action = 'USERS.REVOKESETUPACCESS';
            }

            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'setup_defaults_manage_admins.php?company_cc=<?php echo $company_cc; ?>&');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });

        $('.heldesk_user_access').change(function(e){
            e.preventDefault();

            //If the current selection is equal to the current setting, then disable the submit
            //Also disable the submit if this is the current company and the value is "3"
            if(($('#current_login_company_code').val() == $('#current_manage_company_code').val() && $("input[name='heldesk_user_access']:checked"). val() == 3) || $('#current_user_access_value').val() == $("input[name='heldesk_user_access']:checked"). val()){
                console.log('Disable the submit button');
                $('#heldesk_user_access_submit').prop('disabled', true);
            }else{
                //Else enable submit
                console.log('Enable the submit button');
                $('#heldesk_user_access_submit').prop('disabled', false);
            }

        });

        $('.heldesk_user_access').trigger('change');

        $('#heldesk_user_access_submit').click(function(e){
            e.preventDefault();
            AssistHelper.processing();
            var value = [];
            value["heldesk_user_access"] = $("input[name='heldesk_user_access']:checked"). val();
            value["company_code"] = $('#current_manage_company_code').val();
            var dta = {heldesk_user_access : value["heldesk_user_access"], company_code : value["company_code"]};
            console.log(value);
            console.log(dta);

            var page_action = 'COMPANY.SAVEHELPDESKUSERACCESSCHANGE';
            var url = 'inc_controller.php?action='+page_action;
            var result = AssistHelper.doAjax(url, dta);

            if(result[0]=="ok") {
//                console.log('what in the world have you done');
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1], 'setup_defaults_manage_admins.php?company_cc=<?php echo $company_cc; ?>&');
//                location.reload();
            } else {
                AssistHelper.finishedProcessing(result[0],result[1]);
            }

        });

    });
</script>
<?php require_once('inc_footer.php'); ?>

