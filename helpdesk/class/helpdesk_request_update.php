<?php

class HELPDESK_REQUEST_UPDATE extends HELPDESK
{


    private $table_name = "helpdesk_request_updates";
    private $table_field = "hdrup_";
    protected $id_field = "id";
    protected $status_field = "system_status";
    protected $attachment_field = "attachment";


    private $system_log = array();
    private $user_changes;
    private $emails;


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT_UPDATE";
    const TABLE = "request_updates";
    const TABLE_FLD = "hdrup_";
    const REFTAG = "HDR";
    const LOG_TABLE = "object";

    public function __construct()
    {
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix() . $this->id_field;
        $this->status_field = $this->getTableFieldPrefix() . $this->status_field;
        $this->attachment_field = $this->getTableFieldPrefix() . $this->attachment_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField()
    {
        return self::TABLE_FLD;
    }

    public function getTableName()
    {
        return $this->getDBRef() . "_" . self::TABLE;
    }

    public function getRootTableName()
    {
        return $this->getDBRef();
    }

    public function getRefTag()
    {
        return self::REFTAG;
    }

    public function getMyObjectType()
    {
        return self::OBJECT_TYPE;
    }

    public function getMyLogTable()
    {
        return self::LOG_TABLE;
    }

    public function getApproverFieldName()
    {
        return $this->approver_field;
    }

    public function getTableFieldPrefix()
    {
        return $this->table_field;
    }

    public function __destruct()
    {
        parent::__destruct();
    }

}