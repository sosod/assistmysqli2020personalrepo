<?php

/**
 *
 *
 * Created on: 2 July 2016
 * Authors: Janet Currie
 *
 */
class HELPDESK_REQUEST extends HELPDESK {


	private $table_name = "helpdesk_request";
	private $table_field = "hdr_";
	protected $id_field = "id";
	protected $status_field = "system_status";
	protected $attachment_field = "attachment";


	private $system_log = array();
	private $user_changes;
	private $emails;


	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "OBJECT";
	const TABLE = "request";
	const TABLE_FLD = "hdr_";
	const REFTAG = "HDR";
	const LOG_TABLE = "object";

	public function __construct() {
		parent::__construct();
		$this->id_field = $this->getTableFieldPrefix().$this->id_field;
		$this->status_field = $this->getTableFieldPrefix().$this->status_field;
		$this->attachment_field = $this->getTableFieldPrefix().$this->attachment_field;
	}

	/*************************************
	 * GET functions, Copied by TM
	 * */

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getRootTableName() {
		return $this->getDBRef();
	}

	public function getRefTag() {
		return self::REFTAG;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getApproverFieldName() {
		return $this->approver_field;
	}

	public function getTableFieldPrefix() {
		return $this->table_field;
	}


	//Add new helpdesk to the helpdesk db
	public function addObject($var) {
		$attachment = (isset($var['has_attachments']) ? $var['has_attachments'] : '');
		unset($var['has_attachments']);
		unset($var['attachments']);

		//Get user id
		$hdr_user = new HELPDESK_USERS();
		$add_user = $hdr_user->addObject($_SESSION['tid'], $_SESSION['tkn'], $_SESSION['cc']);

		if(is_array($add_user) && array_key_exists('user_id', $add_user)) {
			$uid = $add_user['user_id'];
		}

		$insert_into_helpdesk_company = false;
		if(isset($var['hdrc_company_id'])) {
			if($var['hdrc_company_id'] != 'X' && (int)$var['hdrc_company_id'] > 0) {
				$insert_into_helpdesk_company = true;
				$hdrc_company_id = (int)$var['hdrc_company_id'];
			}
			unset($var['hdrc_company_id']);
		}

		$insert_helpdesk_company_module_link = false;
		if(isset($var['hdr_moduleid'])) {
			if($var['hdr_moduleid'] != 'X') {
				$imploded_key = explode('_', $var['hdr_moduleid']);
				if(isset($imploded_key) && is_array($imploded_key) && count($imploded_key) > 0) {
					$var['hdr_moduleid'] = $imploded_key[0];
					if($imploded_key[1] == 'companyMod') {
						$insert_helpdesk_company_module_link = true;
						$modid = $imploded_key[2];
					}
				}
			} else {
				unset($var['hdr_moduleid']);
			}

		}

		//Put information into db
		$var[$this->getTableField().'add_userid'] = $uid;
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;//new
		$var[$this->getTableField().'system_status'] = HELPDESK::ACTIVE;

//		if(is_resource($this->get_db_resource()) && get_resource_type($this->get_db_resource()) == 'mysql link') {
//			$var[$this->getTableField().'description'] = mysql_real_escape_string($var[$this->getTableField().'description']);
//		} else {
//			$var[$this->getTableField().'description'] = mysqli_escape_string($this->get_db_resource(), $var[$this->getTableField().'description']);
//		}
		$var[$this->getTableField().'attachment'] = '';

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id > 0) {

			if($insert_into_helpdesk_company == true) {
				$hdrc_insert_vars = array();
				$hdrc_insert_vars['hdrc_company_id'] = $hdrc_company_id;
				$hdrc_insert_vars['hdrc_hdr_id'] = $id;

				$hdrc_table_ref = 'helpdesk_request_company';

				$sql = "INSERT INTO ".$hdrc_table_ref." SET ".$this->convertArrayToSQL($hdrc_insert_vars);
				$hdrc_id = $this->db_insert($sql);


				if(!($hdrc_id > 0)) {
					return array('error', 'Something has gone wrong with the launch of this request. Please contact Action iT to have this issue resolved', '$hdrc_id');
				}

			}

			if($insert_helpdesk_company_module_link == true) {

				$sql = 'SELECT * FROM helpdesk_list_company_module ';
				$sql .= 'WHERE helpdesk_list_company_module.hlcm_hdc_id = '.$add_user['hdc_id'].' ';
				$sql .= 'AND helpdesk_list_company_module.hlcm_modid = '.$modid.' ';
				$sql .= 'ORDER BY hlcm_modtext ';
				$company_module = $this->mysql_fetch_one($sql);


				$hrcm_insert_vars = array();
				$hrcm_insert_vars['hrcm_hdr_id'] = $id;
				$hrcm_insert_vars['hrcm_hlcm_id'] = $company_module['hlcm_id'];

				$hrcm_table_ref = 'helpdesk_request_company_modules';

				$sql = "INSERT INTO ".$hrcm_table_ref." SET ".$this->convertArrayToSQL($hrcm_insert_vars);
				$hrcm_id = $this->db_insert($sql);

				if(!($hrcm_id > 0)) {
					return array('error', 'Something has gone wrong with the launch of this request. Please contact Action iT to have this issue resolved', '$hrcm_id');
				}
			}

			//I'm going to need the hdrid to identify the users that need to be contacted by email
			$hdrid = $id;

			//This Company or The One Above
			if(($add_user['hdrut_value'] == 'client_user') || ($add_user['hdrut_value'] == 'reseller_user')) {
				$cc = $add_user['hdc_cc'];

			} elseif($add_user['hdrut_value'] == 'client_admin_user') {
				$cc = strtolower($_SESSION['bpa_details']['cmpcode']);

			} elseif($add_user['hdrut_value'] == 'reseller_admin_user') {
				$cc = 'iassist';
			}

			//Get the Company Information
			$sql = 'SELECT * FROM helpdesk_companies ';

			$sql .= 'INNER JOIN helpdesk_company_settings ';
			$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

			$sql .= 'INNER JOIN helpdesk_admin_management ';
			$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

			$sql .= 'INNER JOIN helpdesk_request_settings ';
			$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

			$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
			$company = $this->mysql_fetch_one($sql);

			//What this company's admin management
			if($company['hdam_id'] == 1) {// No Admin
				if($company['hdrs_id'] == 1) {//Auto Escalate
					/*
                     * This is pretty much only going to be experienced
                     * by client_users, because we're not giving resellers the option of not having
                     * admin users, so the $cc becomes the cc of the reseller and we run the same query again
                     * to find out if the reseller has one admin to assign this request to, or many that would
                     * have to claim the request from a pot
                     * */
					if($add_user['hdrut_value'] == 'client_user') {
						$cc = strtolower($_SESSION['bpa_details']['cmpcode']);

						//Get the Company Information
						$sql = 'SELECT * FROM helpdesk_companies ';

						$sql .= 'INNER JOIN helpdesk_company_settings ';
						$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

						$sql .= 'INNER JOIN helpdesk_admin_management ';
						$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

						$sql .= 'INNER JOIN helpdesk_request_settings ';
						$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

						$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
						$reseller = $this->mysql_fetch_one($sql);

						if($reseller['hdam_id'] == 2) {// One Admin
							if($reseller['hdrs_id'] == 2) {//To the one admin user
								//Get admin of this company
								$admin_user = $hdr_user->getAdminUser($cc);
							}
						} elseif($reseller['hdam_id'] == 3) {// Multi Admins
							if($reseller['hdrs_id'] == 3) {// Into a Pot
								//Find the admin users and send them a message
								$sql = 'SELECT * FROM helpdesk_companies ';

								// Company Type
								$sql .= 'INNER JOIN helpdesk_company_types ';
								$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

								// Company helpdesk settings
								$sql .= 'INNER JOIN helpdesk_company_settings ';
								$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_admin_management ';
								$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

								$sql .= 'INNER JOIN helpdesk_request_settings ';
								$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

								//Admin users
								$sql .= 'INNER JOIN helpdesk_users ';
								$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_user_types ';
								$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

								$sql .= 'INNER JOIN helpdesk_user_admin_types ';
								$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

								$sql .= 'INNER JOIN helpdesk_admin_types ';
								$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

								// Clauses
								$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
								$company_admins = $this->mysql_fetch_all_by_id($sql, 'user_id');


								//10 January 2018 Admin Assignment HACK - Updated on 06 June 2018
								$admin_user_id_array = array();
								foreach($company_admins as $key => $val) {
									$admin_user_id_array[$val['user_id']] = $val['user_id'];
								}

								// 10 January 2018 - New hack dealing with company admin assignments - Updated on 06 June 2018
								$sql = 'SELECT * FROM helpdesk_company_admin_assignment ';
								$sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_hdc_id = \''.$add_user['hdc_id'].'\' ';
								$sql .= 'AND helpdesk_company_admin_assignment.hcaa_user_id IN ('.implode(', ', $admin_user_id_array).') ';
								$child_assigned_admin = $this->mysql_fetch_all_by_id($sql, 'hcaa_user_id');

								if(isset($child_assigned_admin) && is_array($child_assigned_admin) && count($child_assigned_admin) > 0) {
									if(count($child_assigned_admin) == 1) {
										//Assign to the one admin user
										foreach($admin_user_id_array as $key => $val) {
											if(array_key_exists($val, $child_assigned_admin)) {
												$admin_user = $company_admins[$val];
												break;
											}
										}
									} else {
										//Don't assign it to anyone, and send an email to the assigned admin users
										$admin_users_to_loop_through = $company_admins;
										unset($company_admins);
										$company_admins = array();
										foreach($admin_user_id_array as $key => $val) {
											$company_admins[$val] = $admin_users_to_loop_through[$val];
										}
									}
								}
							}
						}
					}
				}
			} elseif($company['hdam_id'] == 2) {// One Admin
				if($company['hdrs_id'] == 2) {//To the one admin user
					//Get admin of this company
					$admin_user = $hdr_user->getAdminUser($cc);
				}
			} elseif($company['hdam_id'] == 3) {// Multi Admins

				if($company['hdrs_id'] == 3) {// Into a Pot
					//Find the admin users and send them a message
					$sql = 'SELECT * FROM helpdesk_companies ';

					// Company Type
					$sql .= 'INNER JOIN helpdesk_company_types ';
					$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

					// Company helpdesk settings
					$sql .= 'INNER JOIN helpdesk_company_settings ';
					$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

					$sql .= 'INNER JOIN helpdesk_admin_management ';
					$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

					$sql .= 'INNER JOIN helpdesk_request_settings ';
					$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

					//Admin users
					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'INNER JOIN helpdesk_user_admin_types ';
					$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

					$sql .= 'INNER JOIN helpdesk_admin_types ';
					$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

					// Clauses
					$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
					$company_admins = $this->mysql_fetch_all_by_id($sql, 'user_id');


					//10 January 2018 Admin Assignment HACK - Updated on 06 June 2018
					$admin_user_id_array = array();
					foreach($company_admins as $key => $val) {
						$admin_user_id_array[$val['user_id']] = $val['user_id'];
					}

					// 10 January 2018 - New hack dealing with company admin assignments - Updated on 06 June 2018
					$sql = 'SELECT * FROM helpdesk_company_admin_assignment ';
					$sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_hdc_id = \''.$add_user['hdc_id'].'\' ';
					$sql .= 'AND helpdesk_company_admin_assignment.hcaa_user_id IN ('.implode(', ', $admin_user_id_array).') ';
					$child_assigned_admin = $this->mysql_fetch_all_by_id($sql, 'hcaa_user_id');

					if(isset($child_assigned_admin) && is_array($child_assigned_admin) && count($child_assigned_admin) > 0) {
						if(count($child_assigned_admin) == 1) {
							//Assign to the one admin user
							foreach($admin_user_id_array as $key => $val) {
								if(array_key_exists($val, $child_assigned_admin)) {
									$admin_user = $company_admins[$val];
									break;
								}
							}
						} else {
							//Don't assign it to anyone, and send an email to the assigned admin users
							$admin_users_to_loop_through = $company_admins;
							unset($company_admins);
							$company_admins = array();
							foreach($admin_user_id_array as $key => $val) {
								$company_admins[$val] = $admin_users_to_loop_through[$val];
							}
						}
					}
				}

			}

			// 3. Assign the helpdesk request to them in the db
			if(isset($admin_user) && is_array($admin_user) && array_key_exists('user_id', $admin_user)) {
				$admin_uid = $admin_user['user_id'];
				$user_role = 1;// Assigned
				$user_status = 1;// New
				$id = $this->helpdeskRequestAssignment($id, $admin_uid, $user_role, $user_status);

				$assignment = $id;
			}


			//This get's the information for this request so that I can populate the table  with the topic and module
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_list_module ';
			$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdrid.' ';

			$add_user = $this->mysql_fetch_one($sql);


			if(!isset($add_user) || !is_array($add_user) || is_null($add_user) || $add_user === false || count($add_user) == 0) {//This should only happen when the reseller requests a new client db
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_list_topic ';
				$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdrid.' ';

				$add_user = $this->mysql_fetch_one($sql);
			}

			$add_user['hdr_attachment'] = (is_string($attachment) && strlen($attachment) > 0 ? serialize($_FILES['attachments']) : $add_user['hdr_attachment']);

			if(isset($assignment) && is_int($assignment) && $assignment > 0) {
				$success = true;

				// Send confirmation email to the add_user
				$message = $add_user['user_name'].', You have successfully launched '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' to '.$admin_user['user_name'].'.';

				$this->emails[0]['from'] = 'AiT Helpdesk';
				$this->emails[0]['to'] = $this->getAnEmail($add_user['user_tkid'], $add_user['hdc_cc']);
				$this->emails[0]['subject'] = strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' successfully launched';
				$this->emails[0]['message'] = $message;

				// Send notification email to the assigned user
				$message = $admin_user['user_name'].', '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' has been logged to you by '.$add_user['user_name'].', and can be found in the "Current Queue.';
				$this->emails[1]['from'] = 'AiT Helpdesk';
				$this->emails[1]['to'] = $this->getAnEmail($admin_user['user_tkid'], $admin_user['hdc_cc']);
				$this->emails[1]['subject'] = 'New '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' received';
				$this->emails[1]['message'] = $message;

				$this->processEmails($add_user);
			} elseif(count($company_admins) > 0) {
				$success = true;

				// Send confirmation email to the add_user, index is certain to be zero
				$message = $add_user['user_name'].', You have successfully launched '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.', which is currently unassigned, but an admin user will claim responsibility shortly.';
				$this->emails[0]['from'] = 'AiT Helpdesk';
				$this->emails[0]['to'] = $this->getAnEmail($add_user['user_tkid'], $add_user['hdc_cc']);
				$this->emails[0]['subject'] = strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' successfully launched';
				$this->emails[0]['message'] = $message;

				$count = 1;

				foreach($company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$message = $val['user_name'].', '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' has been logged by '.$add_user['user_name'].', is waiting for an admin user to claim responsibility, and can be found in the "Unassigned" section of your helpdesk.';
					$this->emails[$count]['from'] = 'AiT Helpdesk';
					$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
					$this->emails[$count]['subject'] = 'New '.strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid.' received, pending claim.';
					$this->emails[$count]['message'] = $message;

					$count++;
				}

				$this->processEmails($add_user);
			} else {
				$success = false;
			}
		} else {
			$success = false;
		}


		//Return the relevant message upon success or failure
		if($success) {
			$result = array(
				0 => "ok",
				1 => strtoupper($add_user['hdc_cc']).'/'.$this::REFTAG.$hdrid." has been successfully created.",
				'object_id' => $hdrid,
			);


			return $result;
		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	public function processEmails($request) {
		if(isset($this->emails) && is_array($this->emails) && count($this->emails) > 0) {
			$request_details_table = '';
			$request_details_table .= '<p style="margin-bottom:5px"> Here are the details of this request: </p>';
			$request_details_table .= '<table style="border: 1px solid black; border-collapse: collapse; width: 50%; text-align: left; padding: 0px">';

			$table_rows = array();
			foreach($request as $key => $val) {

				if($key == 'hdr_id') {
					$th = 'Ref:';
					$index = 0;
				} elseif($key == 'user_name') {
					$th = 'Owner:';
					$index = 1;
				} elseif($key == 'hdr_add_date') {
					$th = 'Date:';
					$index = 2;
				} elseif($key == 'topic_value') {
					$th = 'Topic: ';
					$index = 3;
				} elseif($key == 'hdr_moduleid') {
					$th = 'Module:';

					if(isset($request['module_value'])) {
						$module = $request['module_value'];
					} else {
						$module = 'Not Applicable';
					}

					$index = 4;
				} elseif($key == 'hdr_description') {
					$th = 'Description:';
					$index = 5;
				} elseif($key == 'hdr_attachment') {
					$th = 'Attachments:';
					$index = 6;
				}

				if(isset($th) && !is_null($th) && is_string($th)) {
					if($th == 'Module:') {
						$value = $module;
					} elseif($th == 'Attachments:') {
						$value = 'No Attachments';
						if(strlen($val) > 0) {
							$unserialized_val = unserialize($val);
							$attachment_count = (isset($unserialized_val['name']) ? count($unserialized_val['name']) : count($unserialized_val));
							$value = $attachment_count.' Attachment'.($attachment_count > 1 ? 's' : '').' available on the system. Please log into Action Assist to access '.($attachment_count > 1 ? 'them' : 'it').'.';
						}
					} elseif($th == 'Ref:') {
						$hdr_id = $request['hdr_id'];
						$value = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id;
					} else {
						$value = ASSIST_HELPER::decode($val);
					}

					//Table Row - Start
					$one_table_row = '<tr>';
					$one_table_row .= '<th style="border: 1px solid black; border-collapse: collapse; width: 30%;">';
					$one_table_row .= $th;
					$one_table_row .= '</th>';
					$one_table_row .= '<td style="border: 1px solid black; border-collapse: collapse; width: 70%;">';
					$one_table_row .= $value;
					$one_table_row .= '</td>';
					$one_table_row .= '</tr>';
					//Table Row - Start

					//add the table into the array
					$table_rows[$index] = $one_table_row;

					$th = false;
				}
			}

			ksort($table_rows);

			foreach($table_rows as $key => $val) {
				$request_details_table .= $val;
			}

			$request_details_table .= '</table>';
			$request_details_table .= '<br/>';
			$auto_generated_message = 'This is an auto generated email. Please log on to Action Assist to make any updates.';
			$request_details_table .= '<p><strong>'.$auto_generated_message.'</strong></p>';


			$mailer = new ASSIST_EMAIL();

			foreach($this->emails as $key => $val) {
				$email_body = '<p>'.$this->decode($val['message']).'</p>';

				if(isset($val['custom_message_text']) && is_string($val['custom_message_text'])) {
					$email_body .= '<p style="margin:2.5px; font-weight: bold">'.$val['user_name'].' has added the following message to the update:</p>';
					$email_body .= '<p style="margin:2.5px">'.nl2br($val['custom_message_text']).'</p>';
				}

				$email_body .= $request_details_table;

				$email_body = str_replace(chr(10), "<br/>", $email_body);//New line to spread the message out nicely

				$mailer->setContentType('HTML');
				$mailer->setRecipient($val['to']);
				$mailer->setBCC('helpdesk_cc@actionassist.co.za');
				$mailer->setSubject($val['subject']);
				$mailer->setBody($email_body);
				$mailer->sendEmail();
			}

		}
	}

	public function helpdeskRequestAssignment($hdrid, $hdru_userid, $user_role, $status) {

		// 3. Enter the hdrid, the user_id, the role_id, and the status_id into the hdr/user intermediary table
		$sql = 'INSERT INTO helpdesk_request_users ';
		$sql .= '(hdru_hdrid, hdru_userid, hdru_role, hdru_status)';
		$sql .= 'VALUES ('.$hdrid.', '.$hdru_userid.', '.$user_role.', '.$status.')';
		$id = $this->db_insert($sql);

		// 4. Return true for success, or false for failure
		if($id > 0) {
			return $id;
		} else {
			return $id;
		}
	}

	public function helpdeskRequestEscalation($hdr_id) {
		// Escalate current helpdesk request to the user that's one level above this level
		$hdr_user = new HELPDESK_USERS();

		// 1. Get the helpdesk request and all the information about it from other tables
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdr_id.' ';

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdr_id.' ';

			$request = $this->mysql_fetch_one($sql);
		}

		//Get information on the user assigned to the request, which will help us in identifying where to escalate to
		$sql = 'SELECT * FROM helpdesk_request ';
		$sql .= 'INNER JOIN helpdesk_request_users ';
		$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

		$sql .= 'INNER JOIN helpdesk_list_user_status ';
		$sql .= 'ON helpdesk_list_user_status.hlus_id = helpdesk_request_users.hdru_status ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';


		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';


		$sql .= 'INNER JOIN helpdesk_list_user_roles ';
		$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdr_id.' ';
		$sql .= 'AND helpdesk_request_users.hdru_role = 1';

		$assigned = $this->mysql_fetch_one($sql);

		if(count($assigned) > 0) {
			/*
             * Current assigned user, you know, the person who's actually escalating this request,
             * becomes an interested party,
             * with a status of escalated
             * */
			$sql = 'UPDATE helpdesk_request_users ';
			$sql .= 'SET hdru_status = 9, hdru_role = 2 ';
			$sql .= 'WHERE  hdru_hdrid = '.$hdr_id.' ';
			$sql .= 'AND  hdru_userid = '.(int)$assigned['user_id'];
			$id = $this->db_update($sql);

			if($id > 0) {
				$sql = 'SELECT * FROM helpdesk_list_user_status ';
				$sql .= 'WHERE hlus_id = 1';
				$new_status = $this->mysql_fetch_one($sql);
				$this->system_log[] = 'hdru_status has changed from '.$assigned['hlus_value'].' to '.$new_status['hlus_value'].' & hdru_role has changed from assigned_user to interested_party in helpdesk_request_users for the user '.$assigned['user_name'];

				/*
                 * The Helpdesk Request status either becomes or stays as escalated,
                 * and the escalation level increases by one
                 * */
				if($request['hdr_escalation_level'] == 1) {
					$new_esc_lvl = 2;
				} elseif($request['hdr_escalation_level'] == 2) {
					$new_esc_lvl = 3;
				}
				/*
                 * Escalation level is now 2, and the status is now 9 (Escalated)
                */
				$sql = 'UPDATE helpdesk_request ';
				$sql .= 'SET hdr_add_status = 9, hdr_escalation_level = '.$new_esc_lvl.' ';
				$sql .= 'WHERE  hdr_id = '.$hdr_id.' ';
				$id = $this->db_update($sql);

				if($id > 0) {
					//Success

					$this->system_log[] = 'hdr_escalation_level has changed from '.$request['hdr_escalation_level'].' to '.$new_esc_lvl.' in helpdesk_request';

					// Check if the current assigned user is a client_admin_user or a reseller_admin_user
					if($assigned['hdrut_value'] == 'client_admin_user') {
						$cc = strtolower($_SESSION['bpa_details']['cmpcode']);

					} elseif($assigned['hdrut_value'] == 'reseller_admin_user') {
						$cc = 'iassist';

					}

					//Get the Company Information
					$sql = 'SELECT * FROM helpdesk_companies ';

					$sql .= 'INNER JOIN helpdesk_company_settings ';
					$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

					$sql .= 'INNER JOIN helpdesk_admin_management ';
					$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

					$sql .= 'INNER JOIN helpdesk_request_settings ';
					$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

					$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
					$company = $this->mysql_fetch_one($sql);

					//What this company's admin management
					if($company['hdam_id'] == 1) {// No Admin
						if($company['hdrs_id'] == 1) {//Auto Escalate
							/*
                             * Remember when I said that this would only be experienced by client_users?
                             *
                             * Nothings changed so far so this here should never happen
                             *
                             * But, You never know, so I'll hold onto to this section just in case*/

						}
					} elseif($company['hdam_id'] == 2) {// One Admin
						if($company['hdrs_id'] == 2) {//To the one admin user
							//Get admin of this company
							$admin_user = $hdr_user->getAdminUser($cc);
						}
					} elseif($company['hdam_id'] == 3) {// Multi Admins

						if($company['hdrs_id'] == 3) {// Into a Pot
							//Find the admin users and send them a message
							$sql = 'SELECT * FROM helpdesk_companies ';

							// Company Type
							$sql .= 'INNER JOIN helpdesk_company_types ';
							$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

							// Company helpdesk settings
							$sql .= 'INNER JOIN helpdesk_company_settings ';
							$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

							$sql .= 'INNER JOIN helpdesk_admin_management ';
							$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

							$sql .= 'INNER JOIN helpdesk_request_settings ';
							$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

							//Admin users
							$sql .= 'INNER JOIN helpdesk_users ';
							$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

							$sql .= 'INNER JOIN helpdesk_user_types ';
							$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

							$sql .= 'INNER JOIN helpdesk_user_admin_types ';
							$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

							$sql .= 'INNER JOIN helpdesk_admin_types ';
							$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

							// Clauses
							$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc.'\'';
							$company_admins = $this->mysql_fetch_all($sql);


							//10 January 2018 Admin Assignment HACK - Updated on 06 June 2018
							$admin_user_id_array = array();
							foreach($company_admins as $key => $val) {
								$admin_user_id_array[$val['user_id']] = $val['user_id'];
							}

							// 10 January 2018 - New hack dealing with company admin assignments -  - Updated on 06 June 2018
							$sql = 'SELECT * FROM helpdesk_company_admin_assignment ';
							$sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_hdc_id = \''.$assigned['hdc_id'].'\' ';
							$sql .= 'AND helpdesk_company_admin_assignment.hcaa_user_id IN ('.implode(', ', $admin_user_id_array).') ';
							$child_assigned_admin = $this->mysql_fetch_all_by_id($sql, 'hcaa_user_id');

							if(isset($child_assigned_admin) && is_array($child_assigned_admin) && count($child_assigned_admin) > 0) {
								if(count($child_assigned_admin) == 1) {
									//Assign to the one admin user
									foreach($admin_user_id_array as $key => $val) {
										if(array_key_exists($val, $child_assigned_admin)) {
											$admin_user = $company_admins[$val];
											break;
										}
									}
								} else {
									//Don't assign it to anyone, and send an email to the assigned admin users
									$admin_users_to_loop_through = $company_admins;
									unset($company_admins);
									$company_admins = array();
									foreach($admin_user_id_array as $key => $val) {
										$company_admins[$val] = $admin_users_to_loop_through[$val];
									}
								}
							}
						}
					}

					// 3. Assign the helpdesk request to them in the db
					if(isset($admin_user) && is_array($admin_user) && array_key_exists('user_id', $admin_user)) {
						$admin_uid = $admin_user['user_id'];
						$user_role = 1;// Assigned
						$user_status = 1;// New
						$id = $this->helpdeskRequestAssignment($hdr_id, $admin_uid, $user_role, $user_status);

						$assignment = $id;
					}

					//Find the user below this one
					if($assigned['hdrut_value'] == 'reseller_admin_user') {
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdr_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';

						$interested_user = $this->mysql_fetch_one($sql);
					}

					if(isset($interested_user) && is_array($interested_user) && count($interested_user) > 0) {
						$user_below = $interested_user;
					} else {
						//The user below is the add_user
						//Find add_user for this specific request
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
						$sql .= 'WHERE helpdesk_request.hdr_id = '.$hdr_id.' ';
						$add_user = $this->mysql_fetch_one($sql);

						$user_below = $add_user;
					}

					if(isset($assignment) && is_int($assignment) && $assignment > 0) {
						/*
                         * Put this escalation in the log
                         * */
						$this->system_log[] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has now been assigned to '.$admin_user['user_name'];
						$this->user_changes = $assigned['user_name'].' (former assigned_user) has escalated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' to '.$admin_user['user_name'].' (current assigned_user)';


						/*
                         * Send email to the user below to let them know that this request has now been escalated upward
                         * */
						$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.': '.$this->user_changes.'.';
						$this->emails[0]['from'] = 'AiT Helpdesk';
						$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
						$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has been escalated upward';
						$this->emails[0]['message'] = $message;


						/*
                         * Send an email to the current user, who escalated this request and let him/her know that
                         * the request has been successfully escalated upward
                         * */
						$message = $assigned['user_name'].', you have successfully escalated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' to '.$admin_user['user_name'].'.';
						$this->emails[1]['from'] = 'AiT Helpdesk';
						$this->emails[1]['to'] = $this->getAnEmail($assigned['user_tkid'], $assigned['hdc_cc']);
						$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' successfully escalated';
						$this->emails[1]['message'] = $message;

						/*
                         * Let the user above the current one, the new assigned user, know that this request has been logged to him
                         * */
						$message = $admin_user['user_name'].', '.$assigned['user_name'].' has escalated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' to you, which you\'ll find in the "Current Queue" section of the helpdesk.';
						$this->emails[2]['from'] = 'AiT Helpdesk';
						$this->emails[2]['to'] = $this->getAnEmail($admin_user['user_tkid'], $admin_user['hdc_cc']);
						$this->emails[2]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has been escalated to '.$admin_user['user_name'];
						$this->emails[2]['message'] = $message;

						return true;
					} elseif(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0) {
						/*
                         * Put this escalation in the log
                         * */
						$this->system_log[] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has now been assigned to and is now unassigned';
						$this->user_changes = $assigned['user_name'].' (former assigned_user) has escalated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' upward, which is now waiting to be claimed';


						/*
                         * Send email to the user below to let them know that this request has now been escalated upward
                         * */
						$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.': '.$this->user_changes.'.';
						$this->emails[0]['from'] = 'AiT Helpdesk';
						$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
						$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has been escalated upward';
						$this->emails[0]['message'] = $message;


						/*
                         * Send an email to the current user, who escalated this request and let him/her know that
                         * the request has been successfully escalated upward
                         * */
						$message = 'You have successfully escalated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' upward.  It is currently unassigned, but an admin user will claim responsibility shortly.';
						$this->emails[1]['from'] = 'AiT Helpdesk';
						$this->emails[1]['to'] = $this->getAnEmail($assigned['user_tkid'], $assigned['hdc_cc']);
						$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' successfully escalated';
						$this->emails[1]['message'] = $message;

						/*
                         * Let the users above this level know that there's an unassigned HDR ready to be claimed
                         * */
						$count = 2;

						foreach($company_admins as $key => $val) {
							// Send notification email to the admins of this company, notifying them of the new request
							$message = $val['user_name'].', '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has been escalated by '.$assigned['user_name'].', is waiting for an admin user to claim responsibility, and can be found in the "Unassigned" section of your helpdesk.';
							$this->emails[$count]['from'] = 'AiT Helpdesk';
							$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
							$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$hdr_id.' has been escalated, pending claim.';
							$this->emails[$count]['message'] = $message;

							$count++;
						}

						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}

			} else {
				return false;
			}

		} else {
			/*
             * There is no assigned user, which is weird, because only the assigned can initiate this function
             * GRACEFUL FAIL*/
			return false;
		}
	}


	public function updateObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);


//        return $var;

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$action = (is_string($action) && strlen($action) > 2 ? $action : (int)$action);
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;

//			if(is_resource($this->get_db_resource()) && get_resource_type($this->get_db_resource()) == 'mysql link') {
//				$the_actual_desc = mysql_real_escape_string($var['hdrup_description']);
//			} else {
//				$the_actual_desc = mysqli_escape_string($this->get_db_resource(), $var['hdrup_description']);
//			}
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		if($action > 0 && $action != 9 && $action < 10) {

			if($request['hdr_escalation_level'] == 1) {//Change both the hdr table and the hdr/user intermediary
				/*
                 * Change both the hdr table and the hdr/user intermediary table,
                 * because there are currently only two users at play here, which are
                 * the add_user = (client_user || client_admin_user || reseller_user || reseller_admin_user)
                 * and the assigned user = (client_admin_user || reseller_admin_user || iassist_admin_user)
                 * */
				$update_values[$this->getTableField().'add_status'] = $action;
				$sql = 'UPDATE '.$this->getTableName().' ';
				$sql .= 'SET hdr_add_status = '.$action.' ';
				$sql .= ' WHERE  hdr_id = '.$request_id.' ';
				$sql .= 'AND hdr_add_userid = '.$request['hdr_add_userid'].' ';
				$id = $this->db_update($sql);

				if($id > 0) {
					$sql = 'SELECT * FROM helpdesk_list_user_status ';
					$sql .= 'WHERE hlus_id = '.$request['hdr_add_status'];
					$old_status = $this->mysql_fetch_one($sql);

					$sql = 'SELECT * FROM helpdesk_list_user_status ';
					$sql .= 'WHERE hlus_id = '.$action;
					$new_status = $this->mysql_fetch_one($sql);

					$this->system_log[] = 'hdr_add_status has changed from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'].' in helpdesk_request';
					/*
                     * Now that we've changed the status at add_user level,
                     * we're going to change the assigned_user's status to match the the add_user status in the request table
                     */
					$sql = 'SELECT * FROM helpdesk_request ';

					$sql .= 'INNER JOIN helpdesk_request_users ';
					$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
					$sql .= 'AND helpdesk_request_users.hdru_role = 1';// assigned_user

					$assigned_user = $this->mysql_fetch_one($sql);

					if(isset($assigned_user) && is_array($assigned_user) && count($assigned_user) > 0) {
						$sql = 'UPDATE helpdesk_request_users ';
						$sql .= 'SET hdru_status = '.$action.' ';
						$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
						$sql .= 'AND  hdru_userid = '.(int)$assigned_user['user_id'];
						$id = $this->db_update($sql);
					}

					if($id > 0) {
						$success = true;

						$this->system_log[] = 'hdru_status has changed from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'].' in helpdesk_request_users';

						/*
                         * Now, let's identify who made the update so that we know who to notify about the change,
                         * And help us to structure the log
                         * */
						if($user['user_id'] == $request['hdr_add_userid']) {
							$this->user_changes = $request['user_name'].' (add_user) has changed the status from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'];

							$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' changes';
							if($action == 4 && $request['hdr_add_status'] == 3) {//Reply
								$this->user_changes = $request['user_name'].' has replied to the request for more information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = 'Reply to request for information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
							} elseif($action == 4 && $request['hdr_add_status'] != 3) {//Rejection
								$this->user_changes = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
							} elseif($action == 7) {//Confirm Resolution
								$this->user_changes = $request['user_name'].' has confirmed resolution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = $subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' confirmed resolution by '.$user['user_name'];
							} elseif($action == 6) {//cancellation
								$this->user_changes = $request['user_name'].' has cancelled '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' has been cancelled by '.$user['user_name'];
							}


							if(isset($assigned_user) && is_array($assigned_user) && count($assigned_user) > 0) {
								// Notify the assigned_user of the change
								$message = $assigned_user['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

								$this->emails[0]['from'] = 'AiT Helpdesk';
								$this->emails[0]['to'] = $this->getAnEmail($assigned_user['user_tkid'], $assigned_user['hdc_cc']);
								$this->emails[0]['subject'] = $subject;
								$this->emails[0]['message'] = $message;

								if($description) {
									$this->emails[0]['custom_message_text'] = $the_actual_desc;
									$this->emails[0]['user_name'] = $user['user_name'];
								}
							} else {
								//Get the multi admins and send the assigned user email
								//Find the admin users and send them a message
								$sql = 'SELECT * FROM helpdesk_companies ';

								// Company Type
								$sql .= 'INNER JOIN helpdesk_company_types ';
								$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

								// Company helpdesk settings
								$sql .= 'INNER JOIN helpdesk_company_settings ';
								$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_admin_management ';
								$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

								$sql .= 'INNER JOIN helpdesk_request_settings ';
								$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

								//Admin users
								$sql .= 'INNER JOIN helpdesk_users ';
								$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_user_types ';
								$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

								$sql .= 'INNER JOIN helpdesk_user_admin_types ';
								$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

								$sql .= 'INNER JOIN helpdesk_admin_types ';
								$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';
								// Clauses
								$sql .= 'WHERE helpdesk_companies.hdc_id = '.$request['hdc_parent'].' ';
								$company_admins = $this->mysql_fetch_all($sql);

								if(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0) {
									$count = 0;
									foreach($company_admins as $key => $val) {
										// Send notification email to the admins of this company, notifying them of the new request
										$message = $val['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

										$this->emails[$count]['from'] = 'AiT Helpdesk';
										$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
										$this->emails[$count]['subject'] = $subject;
										$this->emails[$count]['message'] = $message;

										if($description) {
											$this->emails[$count]['custom_message_text'] = $the_actual_desc;
											$this->emails[$count]['user_name'] = $user['user_name'];
										}

										$count++;
									}
								}
							}

						} elseif($user['user_id'] == $assigned_user['user_id']) {
							$this->user_changes = $assigned_user['user_name'].' has changed the status from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'];


							$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' changes';
							if($action == 4 && $request['hdr_add_status'] == 3) {//Reclaim
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' reclaimed by '.$user['user_name'];
							} elseif($action == 8) {//Close
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' closed by '.$user['user_name'];
							} elseif($action == 5) {//Close
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' marked as resolved by '.$user['user_name'];

								// This where the resolution type insert should happen if it exists
								// We might have to reassess this process when resellers get to have resolution types as well
								if(isset($var['hdrup_res_type']) && is_numeric($var['hdrup_res_type']) && (int)$var['hdrup_res_type'] > 0) {
									$hdrup_res_type = $var['hdrup_res_type'];
									$hdr_restype_link_id = $this->setResoultionType($request, $hdrup_res_type);

									if(!is_numeric($hdr_restype_link_id)) {
										return array('error', 'Something went wrong with the resolution type process');
									}
								}
							}

							// Notify the add_user of the change
							$message = $request['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

							$this->emails[0]['from'] = 'AiT Helpdesk';
							$this->emails[0]['to'] = $this->getAnEmail($request['user_tkid'], $request['hdc_cc']);
							$this->emails[0]['subject'] = $subject;
							$this->emails[0]['message'] = $message;

							if($description) {
								$this->emails[0]['custom_message_text'] = $the_actual_desc;
								$this->emails[0]['user_name'] = $user['user_name'];
							}
						}
					} else {
						$success = false;
					}
				} else {
					$success = false;
				}
			} elseif($action == 5) {
				/*
                     * Assign this helpdesk request to the user that's one level below this admin user
                     * */
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';


				if($user['hdrut_value'] == 'iassist_admin_user') {
					$sql .= 'AND  helpdesk_user_types.hdrut_value = \'reseller_admin_user\'';
				} elseif($user['hdrut_value'] == 'reseller_admin_user') {
					$sql .= 'AND  helpdesk_user_types.hdrut_value = \'client_admin_user\'';
				}

				$admin_user = $this->mysql_fetch_one($sql);


				// Admin_user_below exists in hdru table
				if(isset($admin_user) && is_array($admin_user) && count($admin_user) > 0 && $user['hdrut_value'] != 'client_admin_user') {
					$hdru = true;

					$hdru_role = 2;

				} else {
					$sql = 'SELECT * FROM helpdesk_request ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';

					if($user['hdrut_value'] == 'iassist_admin_user') {
						$sql .= 'AND  helpdesk_user_types.hdrut_value = \'reseller_admin_user\'';
					} elseif($user['hdrut_value'] == 'reseller_admin_user') {
						$sql .= 'AND  (helpdesk_user_types.hdrut_value = \'client_admin_user\' || helpdesk_user_types.hdrut_value = \'reseller_user\')';
					} elseif($user['hdrut_value'] == 'client_admin_user') {
						$sql .= 'AND  helpdesk_user_types.hdrut_value = \'client_user\'';
					}

					$add_user_below = $this->mysql_fetch_one($sql);

					$hdru = false;

					$hdru_role = 1;

					//If this does yield a result, then we're dealing with one of those situations where the adduser is more than one level down
				}

				//Update the reseller admin user's or iassist admin user status to resolved, and set this user to an interested party
				/*
                 * Update the current admin user's status to resolved,
                 * and set this user's role to interested_party
                 * */
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = '.$action.', hdru_role = '.$hdru_role.' ';
				$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
				$id = $this->db_update($sql);

				// this is where the resolution type code goes


				$sql = 'SELECT * FROM helpdesk_list_user_status ';
				$sql .= 'WHERE hlus_id = '.$action;
				$new_status = $this->mysql_fetch_one($sql);

				if($id > 0) {
					if($hdru) {
						$this->system_log[] = $user['user_id'].' has become an interested_party, and the hdru_status is '.$new_status['hlus_value'].' in helpdesk_request_users';
						//Update reseller_admin_user, or client_admin_user to assigned user
						$sql = 'UPDATE helpdesk_request_users ';
						$sql .= 'SET hdru_role = 1 ';
						$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
						$sql .= 'AND  hdru_status = 9 ';
						$sql .= 'AND  hdru_role = 2 ';
						$sql .= 'AND  hdru_userid = '.(int)$admin_user['user_id'].'';
						$id = $this->db_update($sql);


						if($id > 0) {
							$success = true;
							$this->system_log[] = $user['user_id'].' has become an assigned_user for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
						} else {
							//Failed
							$success = false;
						}
					} else {
						$this->system_log[] = 'hdru_status is '.$new_status['hlus_value'].' in helpdesk_request_users '.$user['user_name'];

						$update_values[$this->getTableField().'add_status'] = $action;
						$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
						$sql .= ' WHERE  hdr_id = '.$request_id;
						$id = $this->db_update($sql);

						if($id > 0) {
							$success = true;
							$this->system_log[] = 'hdr_add_status is '.$new_status['hlus_value'].' in helpdesk_request for add_user';
						} else {
							// FAiled
							$success = false;
						}
					}
					$this->user_changes = $user['user_name'].' has Marked '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' as Resolved';

					if(count($admin_user) > 0) {
						//Send email to admin user
						$message = $admin_user['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

						$this->emails[0]['from'] = 'AiT Helpdesk';
						$this->emails[0]['to'] = $this->getAnEmail($admin_user['user_tkid'], $admin_user['hdc_cc']);
						$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' marked as resolved'.
							$this->emails[0]['message'] = $message;

						if($description) {
							$this->emails[0]['custom_message_text'] = $the_actual_desc;
							$this->emails[0]['user_name'] = $user['user_name'];
						}

						//Send email to user below
					} else {
						//Send email to add_user
						$message = $request['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

						$this->emails[0]['from'] = 'AiT Helpdesk';
						$this->emails[0]['to'] = $this->getAnEmail($request['user_tkid'], $request['hdc_cc']);
						$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' marked as resolved'.
							$this->emails[0]['message'] = $message;

						if($description) {
							$this->emails[0]['custom_message_text'] = $the_actual_desc;
							$this->emails[0]['user_name'] = $user['user_name'];
						}
					}
				} else {
					// Failure
					$success = false;
				}
			} elseif($action == 4) {
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'WHERE helpdesk_request_users.hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND helpdesk_request_users.hdru_status = 10';
				$reply_check = $this->mysql_fetch_one($sql);

				if(is_array($reply_check) && count($reply_check) > 0) {// This is a reply to a request for more information, or a reclaimation
					/*
                * Is the current user the add_user*/
					if($user['user_id'] == $request['hdr_add_userid']) {
						if($request['hdr_escalation_level'] == 1) {// Reply
							//Change the HDR status to 4
							$update_values[$this->getTableField().'add_status'] = $action;
							$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
							$sql .= ' WHERE  hdr_id = '.$request_id;
							$id = $this->db_update($sql);

							if($id > 0) {
								$this->system_log[] = 'hdr_add_status is '.$action.' in helpdesk_request';
								//Change the hdru status to 4
								$sql = 'UPDATE helpdesk_request_users ';
								$sql .= 'SET hdru_status = '.$action.' ';
								$sql .= 'WHERE hdru_hdrid = '.$request_id;
								$id = $this->db_update($sql);

								if($id > 0) {
									$this->system_log[] = 'hdru_status is now '.$action.' in helpdesk_request_users';
									$this->user_changes = $user['user_name'].' has replied to the request for more information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
									//Success
									$success = true;

									$message = $this->user_changes;

									$this->emails[0]['from'] = 'AiT Helpdesk';
									$this->emails[0]['to'] = $this->getAnEmail($reply_check['user_tkid'], $reply_check['hdc_cc']);
									$this->emails[0]['subject'] = 'Reply to request for information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
									$this->emails[0]['message'] = $message;

									if($description) {
										$this->emails[0]['custom_message_text'] = $the_actual_desc;
										$this->emails[0]['user_name'] = $user['user_name'];
									}
								} else {
									// Failure
									$success = false;
								}
							} else {
								// Failure
								$success = false;
							}
						} else {// Reply
							$update_values[$this->getTableField().'add_status'] = 9;
							$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
							$sql .= ' WHERE  hdr_id = '.$request_id;
							$id = $this->db_update($sql);

							if($id > 0) {
								$sql = 'SELECT * FROM helpdesk_list_user_status ';
								$sql .= 'WHERE hlus_id = '.$action;
								$new_status = $this->mysql_fetch_one($sql);
								$this->system_log[] = 'hdr_add_status is '.$new_status['hlus_value'].' in helpdesk_request';


								//Change the hdru status to 4
								$sql = 'UPDATE helpdesk_request_users ';
								$sql .= 'SET hdru_status = '.$action.' ';
								$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
								$sql .= 'AND hdru_userid = '.(int)$reply_check['user_id'];
								$id = $this->db_update($sql);

								if($id > 0) {
									$this->system_log[] = 'hdru_status is '.$new_status['hlus_value'].' in helpdesk_request_users';
									$this->user_changes = $user['user_name'].' has replied to the request for more information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

									//Send an email of the reply to the admin user that asked for information HERE
									$message = $reply_check['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

									$this->emails[0]['from'] = 'AiT Helpdesk';
									$this->emails[0]['to'] = $this->getAnEmail($reply_check['user_tkid'], $reply_check['hdc_cc']);
									$this->emails[0]['subject'] = 'Reply to request for information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
									$this->emails[0]['message'] = $message;

									if($description) {
										$this->emails[0]['custom_message_text'] = $the_actual_desc;
										$this->emails[0]['user_name'] = $user['user_name'];
									}

									//Success
									$success = true;
								} else {
									//Failure
									$success = false;
								}
							} else {
								//Failure
								$success = false;
							}
						}
					} elseif($user['user_id'] == $reply_check['user_id']) {//Reclaim
						if($request['hdr_escalation_level'] == 1 || $request['hdr_add_status'] == 3) {
							//Change the HDR status to 4
							if($request['hdr_escalation_level'] == 1) {
								$update_values[$this->getTableField().'add_status'] = $action;
							} else {
								$update_values[$this->getTableField().'add_status'] = 9;
							}
							$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
							$sql .= ' WHERE  hdr_id = '.$request_id;
							$id = $this->db_update($sql);

							$this->system_log[] = 'hdr_add_status is '.$update_values[$this->getTableField().'add_status'].' in helpdesk_request';

							//Get add user and inform them via email that the assigned admin_user has reclaimed the HDR in question
							$message = $request['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

							$this->emails[0]['from'] = 'AiT Helpdesk';
							$this->emails[0]['to'] = $this->getAnEmail($request['user_tkid'], $request['hdc_cc']);
							$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' reclaimed by '.$reply_check['user_name'];
							$this->emails[0]['message'] = $message;

							if($description) {
								$this->emails[0]['custom_message_text'] = $the_actual_desc;
								$this->emails[0]['user_name'] = $user['user_name'];
							}
						} else {
							//First let's find the admin_user that this has bee returned to
							$sql = 'SELECT * FROM helpdesk_request ';
							$sql .= 'INNER JOIN helpdesk_request_users ';
							$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

							$sql .= 'INNER JOIN helpdesk_users ';
							$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

							$sql .= 'INNER JOIN helpdesk_companies ';
							$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

							$sql .= 'INNER JOIN helpdesk_user_types ';
							$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

							$sql .= 'INNER JOIN helpdesk_list_user_roles ';
							$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

							$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
							$sql .= 'AND ';
							$sql .= 'helpdesk_request_users.hdru_status = 3 ';
							$returned_user = $this->mysql_fetch_one($sql);

							//Change the hdru status to 9
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = 9 ';
							$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
							$sql .= 'AND hdru_status = 3';
							$id = $this->db_update($sql);

							$this->system_log[] = 'hdru_status is back to escalated in helpdesk_request_users';
							$this->user_changes = $reply_check['user_name'].' has reclaimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
							//Get admin_user and inform them via email that the assigned admin_user has reclaimed the HDR in question
							$message = $returned_user['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

							$this->emails[0]['from'] = 'AiT Helpdesk';
							$this->emails[0]['to'] = $this->getAnEmail($returned_user['user_tkid'], $returned_user['hdc_cc']);
							$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Reclaimed by '.$reply_check['user_name'];
							$this->emails[0]['message'] = $message;

							if($description) {
								$this->emails[0]['custom_message_text'] = $the_actual_desc;
								$this->emails[0]['user_name'] = $user['user_name'];
							}
						}

						if($id > 0) {
							//Change the hdru status to 4
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = '.$action.' ';
							$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
							$sql .= 'AND hdru_userid = '.(int)$reply_check['user_id'];
							$id = $this->db_update($sql);

							if($id > 0) {
								$this->system_log[] = 'hdru_status is now In Progress in helpdesk_request_users';
								$this->user_changes = $user['user_name'].' has reclaimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								//Success
								$success = true;
							} else {
								//Failure
								$success = false;
							}
						} else {
							//Failure
							$success = false;
						}

					} else {
						// Change current_user hdru_status to 9
						$sql = 'UPDATE helpdesk_request_users ';
						$sql .= 'SET hdru_status = 9 ';
						$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
						$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
						$id = $this->db_update($sql);

						if($id > 0) {
							$this->system_log[] = 'hdru_status is now Escalated in helpdesk_request_users';

							//Assign this request to the higher level user, and update their status to 4 (In progress)
							//Change the hdru status to 4
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = '.$action.' ';
							$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
							$sql .= 'AND hdru_userid = '.(int)$reply_check['user_id'];
							$id = $this->db_update($sql);


							if($id > 0) {
								$this->system_log[] = 'hdru_status is now In Progress in helpdesk_request_users';
								$this->user_changes = $user['user_name'].' has replied to the request for more information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

								//Send an email to the assigned user and let know that the HDR in question is now theirs again
								$message = $reply_check['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

								$this->emails[0]['from'] = 'AiT Helpdesk';
								$this->emails[0]['to'] = $this->getAnEmail($reply_check['user_tkid'], $reply_check['hdc_cc']);
								$this->emails[0]['subject'] = 'Reply to request for information for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$this->emails[0]['message'] = $message;

								if($description) {
									$this->emails[0]['custom_message_text'] = $the_actual_desc;
									$this->emails[0]['user_name'] = $user['user_name'];
								}

								//Success
								$success = true;
							} else {
								//Failure
								$success = false;
							}
						} else {
							//Failure
							$success = false;
						}

					}
				} else {//Either a reject or plain marking as in progress
					/*
                     * Is the current user the add_user
                     * */
					if((int)$user['user_id'] == (int)$request['hdr_add_userid']) {
						if((int)$request['hdr_escalation_level'] == 1) {// Rejection
							//Change the HDR status to 4
							$update_values[$this->getTableField().'add_status'] = $action;
							$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
							$sql .= ' WHERE  hdr_id = '.$request_id;
							$affected = $this->db_update($sql);

							//Change the hdru status to 4
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = '.$action.' ';
							$sql .= 'WHERE hdru_hdrid = '.$request_id;
							$affected = $this->db_update($sql);
						} else {// Rejection
							$update_values[$this->getTableField().'add_status'] = 9;
							// Change hdr status back to 9
							$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
							$sql .= ' WHERE  hdr_id = '.$request_id;
							$id = $this->db_update($sql);

							if($id > 0) {
								$sql = 'SELECT * FROM helpdesk_list_user_status ';
								$sql .= 'WHERE hlus_id = '.$action;
								$new_status = $this->mysql_fetch_one($sql);
								$this->system_log[] = 'hdr_add_status is '.$new_status['hlus_value'].' in helpdesk_request';

								// Find the users attached to this request in the hdru table
								if(($request['hdrut_value'] == 'client_user') || ($request['hdrut_value'] == 'client_admin_user') || ($request['hdrut_value'] == 'reseller_user')) {//is client_user, client_admin_user, reseller_user

									//Assign to the reseller_admin_user
									$sql = 'SELECT * FROM helpdesk_request ';

									$sql .= 'INNER JOIN helpdesk_request_users ';
									$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

									$sql .= 'INNER JOIN helpdesk_users ';
									$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

									$sql .= 'INNER JOIN helpdesk_companies ';
									$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

									$sql .= 'INNER JOIN helpdesk_user_types ';
									$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

									$sql .= 'WHERE  hdr_id = '.$request_id.' ';
									$sql .= 'AND  helpdesk_user_types.hdrut_value = \'reseller_admin_user\'';
									$reseller = $this->mysql_fetch_one($sql);

									if(isset($reseller) && is_array($reseller) && count($reseller) > 0) {// Is there a reseller involved in this request


										//Is there an iassist_admin above this one attached to this request
										$sql = 'SELECT * FROM helpdesk_request ';

										$sql .= 'INNER JOIN helpdesk_request_users ';
										$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

										$sql .= 'INNER JOIN helpdesk_users ';
										$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

										$sql .= 'INNER JOIN helpdesk_companies ';
										$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

										$sql .= 'INNER JOIN helpdesk_user_types ';
										$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

										$sql .= 'WHERE  hdr_id = '.$request_id.' ';

										$sql .= 'AND  helpdesk_user_types.hdrut_value = \'iassist_admin_user\'';

										$iassist_admin = $this->mysql_fetch_one($sql);

										if(count($iassist_admin) > 0) {
											$hdru_status = 9;
										} else {
											$hdru_status = $action;
										}

										//Check for client_admin_user
										$sql = 'SELECT * FROM helpdesk_request ';

										$sql .= 'INNER JOIN helpdesk_request_users ';
										$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

										$sql .= 'INNER JOIN helpdesk_users ';
										$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

										$sql .= 'INNER JOIN helpdesk_companies ';
										$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

										$sql .= 'INNER JOIN helpdesk_user_types ';
										$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

										$sql .= 'WHERE  hdr_id = '.$request_id.' ';
										$sql .= 'AND  helpdesk_user_types.hdrut_value = \'client_admin_user\'';
										$client_admin = $this->mysql_fetch_one($sql);

										if(count($client_admin) > 0) {
											//Then return the status back to escalated, and make them an interested party once more
											$sql = 'UPDATE helpdesk_request_users ';
											$sql .= 'SET hdru_status = 9, hdru_role = 2 ';
											$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
											$sql .= 'AND hdru_userid = '.(int)$client_admin['user_id'];
											$id = $this->db_update($sql);

											if($id > 0) {
												$this->system_log[] = 'hdru_status has become Escalated in helpdesk_request_users for '.$client_admin['user_name'].' and the user role is now the assigned user again';
												$this->user_changes = $user['user_name'].' has rejected the resolution recieved from '.$client_admin['user_name'];
											}
										}

										//Assign this request to the higher level user, and update their status
										$sql = 'UPDATE helpdesk_request_users ';
										$sql .= 'SET hdru_status = '.$hdru_status.', hdru_role = 1 ';
										$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
										$sql .= 'AND hdru_userid = '.(int)$reseller['user_id'];
										$id = $this->db_update($sql);


										if($id > 0) {
											$this->system_log[] = 'hdru_status has become In Progress in helpdesk_request_users for '.$reseller['user_name'].' and the user role is now the assigned user again';
											$this->user_changes = $user['user_name'].' has rejected the resolution recieved from '.(count($client_admin) > 0 ? $client_admin['user_name'] : $reseller['user_name']);

											//Success
											$success = true;

											//send email to reseller admin user
											$message = $reseller['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

											$this->emails[0]['from'] = 'AiT Helpdesk';
											$this->emails[0]['to'] = $this->getAnEmail($reseller['user_tkid'], $reseller['hdc_cc']);
											$this->emails[0]['subject'] = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
											$this->emails[0]['message'] = $message;

											if($description) {
												$this->emails[0]['custom_message_text'] = $the_actual_desc;
												$this->emails[0]['user_name'] = $user['user_name'];
											}

											if(isset($client_admin) && is_array($client_admin) && count($client_admin) > 0) {
												$message = $client_admin['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;
												$this->emails[1]['from'] = 'AiT Helpdesk';
												$this->emails[1]['to'] = $this->getAnEmail($client_admin['user_tkid'], $client_admin['hdc_cc']);
												$this->emails[1]['subject'] = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
												$this->emails[1]['message'] = $message;

												if($description) {
													$this->emails[1]['custom_message_text'] = $the_actual_desc;
													$this->emails[1]['user_name'] = $user['user_name'];
												}
											}
											//send email to client admin user
										} else {
											$success = false;
										}

									} else {
										//Assign to the client_admin_user
										$sql = 'SELECT * FROM helpdesk_request ';

										$sql .= 'INNER JOIN helpdesk_request_users ';
										$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

										$sql .= 'INNER JOIN helpdesk_users ';
										$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

										$sql .= 'INNER JOIN helpdesk_companies ';
										$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

										$sql .= 'INNER JOIN helpdesk_user_types ';
										$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

										$sql .= 'WHERE  hdr_id = '.$request_id.' ';
										$sql .= 'AND  helpdesk_user_types.hdrut_value = \'client_admin_user\'';
										$client = $this->mysql_fetch_one($sql);

										if(count($client) > 0) {
											//Change the hdru status to 4
											$sql = 'UPDATE helpdesk_request_users ';
											$sql .= 'SET hdru_status = '.$action.' ';
											$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
											$sql .= 'AND hdru_userid = '.(int)$client['user_id'];
											$id = $this->db_update($sql);

											if($id > 0) {
												$success = true;

												//send email to client admin user
												$message = $client['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

												$this->emails[0]['from'] = 'AiT Helpdesk';
												$this->emails[0]['to'] = $this->getAnEmail($client['user_tkid'], $client['hdc_cc']);
												$this->emails[0]['subject'] = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
												$this->emails[0]['message'] = $message;

												if($description) {
													$this->emails[0]['custom_message_text'] = $the_actual_desc;
													$this->emails[0]['user_name'] = $user['user_name'];
												}
											} else {
												//Failure
												$success = false;
											}
										}
									}
								} elseif($request['hdrut_value'] == 'reseller_admin_user') {//reseller_admin_user
									//Assign to the iassist_admin_user
									$sql = 'SELECT * FROM helpdesk_request ';

									$sql .= 'INNER JOIN helpdesk_request_users ';
									$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

									$sql .= 'INNER JOIN helpdesk_users ';
									$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

									$sql .= 'INNER JOIN helpdesk_companies ';
									$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

									$sql .= 'INNER JOIN helpdesk_user_types ';
									$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

									$sql .= 'WHERE  hdr_id = '.$request_id.' ';
									$sql .= 'AND  helpdesk_user_types.hdrut_value = \'iassist_admin_user\'';
									$iassist = $this->mysql_fetch_one($sql);

									if(count($iassist) > 0) {
										//Change the hdru status to 4
										$sql = 'UPDATE helpdesk_request_users ';
										$sql .= 'SET hdru_status = '.$action.' ';
										$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
										$sql .= 'AND hdru_userid = '.(int)$iassist['user_id'];
										$id = $this->db_update($sql);

										if($id > 0) {
											$success = true;

											//send email to iassist admin user
											$message = $iassist['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

											$this->emails[0]['from'] = 'AiT Helpdesk';
											$this->emails[0]['to'] = $this->getAnEmail($iassist['user_tkid'], $iassist['hdc_cc']);
											$this->emails[0]['subject'] = $request['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
											$this->emails[0]['message'] = $message;

											if($description) {
												$this->emails[0]['custom_message_text'] = $the_actual_desc;
												$this->emails[0]['user_name'] = $user['user_name'];
											}
										} else {
											//Failure
											$success = false;
										}
									}
								}
							} else {
								//Failure
								$success = false;
							}
						}
					} else {
						//what's the current user's user_type, as which type of admin is he
						$user_type = $user['hdrut_value'];

						//Is there an admin above this one attached to this request
						$sql = 'SELECT * FROM helpdesk_request ';

						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE  hdr_id = '.$request_id.' ';
						if($user_type == 'client_admin_user') {
							$sql .= 'AND  helpdesk_user_types.hdrut_value = \'reseller_admin_user\'';
						} elseif($user_type == 'reseller_admin_user') {
							$sql .= 'AND  helpdesk_user_types.hdrut_value = \'iassist_admin_user\'';
						} elseif($user_type == 'iassist_admin_user') {
							$sql .= 'AND  helpdesk_user_types.hdrut_value = \'this_doesnt_exist\'';
						}

						$admin = $this->mysql_fetch_one($sql);

						if(isset($admin) && is_array($admin) && count($admin) > 0) {


							if($user_type == 'iassist_admin_user') {
								return array('error', 'This is going down the wrong path');
							}

							// Change current_user hdru_status to 9, Update the user_role of the current user to interested_party
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = 9, hdru_role = 2 ';
							$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
							$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
							$id = $this->db_update($sql);

							if((int)$id > 0) {
								$this->system_log[] = 'hdru_status has become in escalated in helpdesk_request_users for '.$user['user_name'].' and the user role is now back to interested party';

								if($admin['hdrut_value'] == 'reseller_admin_user') {
									//Is there an iassist_admin above this one attached to this request
									$sql = 'SELECT * FROM helpdesk_request ';

									$sql .= 'INNER JOIN helpdesk_request_users ';
									$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

									$sql .= 'INNER JOIN helpdesk_users ';
									$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

									$sql .= 'INNER JOIN helpdesk_companies ';
									$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

									$sql .= 'INNER JOIN helpdesk_user_types ';
									$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

									$sql .= 'WHERE  hdr_id = '.$request_id.' ';

									$sql .= 'AND  helpdesk_user_types.hdrut_value = \'iassist_admin_user\'';

									$iassist_admin = $this->mysql_fetch_one($sql);

									if(count($iassist_admin) > 0) {
										$hdru_status = 9;
									} else {
										$hdru_status = $action;
									}
								} elseif($admin['hdrut_value'] == 'iassist_admin_user') {
									$hdru_status = $action;
								}


								//Assign this request to the higher level user, and update their status to 4 (In progress)
								$sql = 'UPDATE helpdesk_request_users ';
								$sql .= 'SET hdru_status = '.$hdru_status.', hdru_role = 1 ';
								$sql .= 'WHERE hdru_hdrid = '.$request_id.' ';
								$sql .= 'AND hdru_userid = '.(int)$admin['user_id'];
								$id = $this->db_update($sql);


								if($id > 0) {
									$this->system_log[] = 'hdru_status has become In Progress in helpdesk_request_users for '.$admin['user_name'].' and the user role is now the assigned user again';
									$this->user_changes = $user['user_name'].' has rejected the resolution recieved from '.$admin['user_name'];

									$message = $admin['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

									$this->emails[0]['from'] = 'AiT Helpdesk';
									$this->emails[0]['to'] = $this->getAnEmail($admin['user_tkid'], $admin['hdc_cc']);
									$this->emails[0]['subject'] = $user['user_name'].' has rejected the solution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
									$this->emails[0]['message'] = $message;

									if($description) {
										$this->emails[0]['custom_message_text'] = $the_actual_desc;
										$this->emails[0]['user_name'] = $user['user_name'];
									}

									//Success
									$success = true;
								}
							} else {
								//Failure
								$success = false;
							}

						} else {
							// Change current_user hdru_status to 4, because he's the one working on it
							$sql = 'UPDATE helpdesk_request_users ';
							$sql .= 'SET hdru_status = '.$action.' ';
							$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
							$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
							$id = $this->db_update($sql);

							if($id > 0) {
								$success = true;

								$sql = 'SELECT * FROM helpdesk_list_user_status ';
								$sql .= 'WHERE hlus_id = '.$action;
								$new_status = $this->mysql_fetch_one($sql);
								$this->system_log[] = 'hdru_status is now '.$new_status['hlus_value'].' in helpdesk_request_users for '.$user['user_name'];
								$this->user_changes = $user['user_name'].' is now working on '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

								//Find the user below this one
								if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
									$sql = 'SELECT * FROM helpdesk_request ';
									$sql .= 'INNER JOIN helpdesk_request_users ';
									$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

									$sql .= 'INNER JOIN helpdesk_users ';
									$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

									$sql .= 'INNER JOIN helpdesk_companies ';
									$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

									$sql .= 'INNER JOIN helpdesk_list_user_roles ';
									$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

									$sql .= 'INNER JOIN helpdesk_user_types ';
									$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

									$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
									$sql .= 'AND ';
									$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
									$sql .= 'AND ';
									if($user['hdrut_value'] == 'iassist_admin_user') {
										$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
									} elseif($user['hdrut_value'] == 'reseller_admin_user') {
										$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
									}

									$interested_user = $this->mysql_fetch_one($sql);
								}

								if(isset($interested_user) && is_array($interested_user)) {
									$user_below = $interested_user;
								} else {
									//The user below is the add_user
									//Find add_user for this specific request
									$sql = 'SELECT * FROM helpdesk_request ';
									$sql .= 'INNER JOIN helpdesk_users ';
									$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
									$sql .= 'INNER JOIN helpdesk_companies ';
									$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
									$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';

									$add_user = $this->mysql_fetch_one($sql);

									$user_below = $add_user;
								}

								//Is the user below the add_user... If yes then this code will never execute anyway because that would mean that the request is escalation level 1
								//Which I've already taken care of somewhere up top
								if($user_below['user_id'] == $request['hdr_add_userid']) {
									//Change the hdr_add_status to 4 as well
									$update_values[$this->getTableField().'add_status'] = $action;
									$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
									$sql .= ' WHERE  hdr_id = '.$request_id;
									$id = $this->db_update($sql);
								}

								$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

								$this->emails[0]['from'] = 'AiT Helpdesk';
								$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
								$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' is In Progress';
								$this->emails[0]['message'] = $message;

								if($description) {
									$this->emails[0]['custom_message_text'] = $the_actual_desc;
									$this->emails[0]['user_name'] = $user['user_name'];
								}


							} else {
								$success = false;
							}
						}

					}
				}
			} elseif($action == 6 || $action == 7 || $action == 8) {
				/*
                 * This section handles final closures
                 * and we shan't be able to modify this request once
                 * it passes through this section,
                 * because this is where requests go to die*/
				// 6 Cancellation
				// 7 Confirmed Completion
				// 8 Forced Close
				$update_values[$this->getTableField().'add_status'] = $action;
				$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
				$sql .= ' WHERE  hdr_id = '.$request_id;
				$id = $this->db_update($sql);

				if($id > 0) {
					// 1. Change status to completed at client admin user level, reseller admin user level, and iassist
					$sql = 'UPDATE helpdesk_request_users ';
					$sql .= 'SET hdru_status = '.$action.' ';
					$sql .= 'WHERE hdru_hdrid = '.$request_id;
					$id = $this->db_update($sql);

					if($id > 0) {
						if($user['user_id'] == $request['hdr_add_userid']) {
							if($action == 6) {
								$this->system_log[] = 'hdr_add_status has changed to Cancelled in helpdesk_request';
								$this->system_log[] = 'hdru_status has changed to Cancelled in helpdesk_request_users for all users';
								$this->user_changes = $user['user_name'].' has confirmed resolution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' cancelled by '.$user['user_name'];
							} elseif($action == 7) {
								$this->system_log[] = 'hdr_add_status has changed from Marked As Resolved to Completed in helpdesk_request';
								$this->system_log[] = 'hdru_status has changed from Marked As Resolved to Completed in helpdesk_request_users for all users';
								$this->user_changes = $user['user_name'].' has confirmed resolution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
								$subject = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' confirmed resolution by '.$user['user_name'];
							}

							//find all the admins involved and notify them of change
							$sql = 'SELECT * FROM helpdesk_request ';
							$sql .= 'INNER JOIN helpdesk_request_users ';
							$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

							$sql .= 'INNER JOIN helpdesk_users ';
							$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

							$sql .= 'INNER JOIN helpdesk_companies ';
							$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

							$sql .= 'INNER JOIN helpdesk_list_user_roles ';
							$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

							$sql .= 'INNER JOIN helpdesk_user_types ';
							$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

							$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';

							$all_admins = $this->mysql_fetch_all($sql);

							if(isset($all_admins) && is_array($all_admins) && count($all_admins) > 0) {
								$count = 0;
								foreach($all_admins as $key => $val) {
									$message = $val['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

									$this->emails[$count]['from'] = 'AiT Helpdesk';
									$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
									$this->emails[$count]['subject'] = $subject;
									$this->emails[$count]['message'] = $message;

									$count++;
								}
							} else {
								//Get the multi admins and send the assigned user email
								//Find the admin users and send them a message
								$sql = 'SELECT * FROM helpdesk_companies ';

								// Company Type
								$sql .= 'INNER JOIN helpdesk_company_types ';
								$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

								// Company helpdesk settings
								$sql .= 'INNER JOIN helpdesk_company_settings ';
								$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_admin_management ';
								$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

								$sql .= 'INNER JOIN helpdesk_request_settings ';
								$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

								//Admin users
								$sql .= 'INNER JOIN helpdesk_users ';
								$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

								$sql .= 'INNER JOIN helpdesk_user_types ';
								$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

								$sql .= 'INNER JOIN helpdesk_user_admin_types ';
								$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

								$sql .= 'INNER JOIN helpdesk_admin_types ';
								$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';
								// Clauses
								$sql .= 'WHERE helpdesk_companies.hdc_id = '.$request['hdc_parent'].' ';
								$company_admins = $this->mysql_fetch_all($sql);

								if(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0) {
									$count = 0;
									foreach($company_admins as $key => $val) {
										// Send notification email to the admins of this company, notifying them of the new request
										$message = $val['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

										$this->emails[$count]['from'] = 'AiT Helpdesk';
										$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
										$this->emails[$count]['subject'] = $subject;
										$this->emails[$count]['message'] = $message;

										if($description) {
											$this->emails[$count]['custom_message_text'] = $the_actual_desc;
											$this->emails[$count]['user_name'] = $user['user_name'];
										}

										$count++;
									}
								}
							}
						} else {
							if($action == 8) {
								$this->system_log[] = 'hdr_add_status has changed from Marked As Resolved to Closed in helpdesk_request';
								$this->system_log[] = 'hdru_status has changed from Marked As Resolved to Closed in helpdesk_request_users for all users';
								$this->user_changes = $user['user_name'].' has confirmed resolution for '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
							}

							//First of all, notify the add_user
							$message = $request['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

							$this->emails[0]['from'] = 'AiT Helpdesk';
							$this->emails[0]['to'] = $this->getAnEmail($request['user_tkid'], $request['hdc_cc']);
							$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' closed by '.$user['user_name'];
							$this->emails[0]['message'] = $message;


							//Then find all the admin users involved aside from the current one and notify them
							$sql = 'SELECT * FROM helpdesk_request ';
							$sql .= 'INNER JOIN helpdesk_request_users ';
							$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

							$sql .= 'INNER JOIN helpdesk_users ';
							$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

							$sql .= 'INNER JOIN helpdesk_companies ';
							$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

							$sql .= 'INNER JOIN helpdesk_list_user_roles ';
							$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

							$sql .= 'INNER JOIN helpdesk_user_types ';
							$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

							$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
							$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';

							$all_admins = $this->mysql_fetch_all($sql);

							if(isset($all_admins) && is_array($all_admins) && count($all_admins) > 0) {
								$count = 1;
								foreach($all_admins as $key => $val) {
									$message = $val['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;
									$this->emails[$count]['from'] = 'AiT Helpdesk';
									$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
									$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' closed by '.$user['user_name'];
									$this->emails[$count]['message'] = $message;

									$count++;
								}
							}
						}


						$success = true;
					} else {
						$success = false;
					}
				} else {
					$success = false;
				}
			} else {
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = '.$action.' ';
				$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
				$id = $this->db_update($sql);

				if($id > 0) {

					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_request_users ';
					$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

					$sql .= 'INNER JOIN helpdesk_list_user_status ';
					$sql .= 'ON helpdesk_list_user_status.hlus_id = helpdesk_request_users.hdru_status ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';


					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';


					$sql .= 'INNER JOIN helpdesk_list_user_roles ';
					$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$action.' ';
					$sql .= 'AND helpdesk_request_users.hdru_userid = '.$user['user_id'];
					$old_status = $this->mysql_fetch_one($sql);

					$sql = 'SELECT * FROM helpdesk_list_user_status ';
					$sql .= 'WHERE hlus_id = '.$action;
					$new_status = $this->mysql_fetch_one($sql);

					$this->system_log[] = 'hdru_status has changed from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'].' in helpdesk_request_users';
					$this->user_changes = $user['user_name'].' has changed the status from '.$old_status['hlus_value'].' to '.$new_status['hlus_value'];

					//Find the user below this one
					if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
						$sql .= 'AND ';
						if($user['hdrut_value'] == 'iassist_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
						} elseif($user['hdrut_value'] == 'reseller_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
						}

						$interested_user = $this->mysql_fetch_one($sql);
					}

					if(isset($interested_user) && is_array($interested_user)) {
						$user_below = $interested_user;
					} else {
						//The user below is the add_user
						//Find add_user for this specific request
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$add_user = $this->mysql_fetch_one($sql);

						$user_below = $add_user;
					}

					$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

					$this->emails[0]['from'] = 'AiT Helpdesk';
					$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
					$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' change by '.$user['user_name'];
					$this->emails[0]['message'] = $message;

					if($description) {
						$this->emails[0]['custom_message_text'] = $the_actual_desc;
						$this->emails[0]['user_name'] = $user['user_name'];
					}

					$success = true;
				} else {
					$success = false;
				}
			}
		} elseif($action == 9) {
			/*
             * Escalate this request upward
             * */
			$id = $this->helpdeskRequestEscalation($request_id);

			if($id == true) {
				$success = true;
			} else {
				$success = false;
			}
		} elseif($action > 10) {
			//This is a return
			$reception_id = $action - 10;

			//Find out if the user the being returned to is an add user or an interested party
			// So that we know which table to affect
			if($reception_id == $request['hdr_add_userid']) {
				//This is the add user
				$sql = 'UPDATE '.$this->getTableName().' ';
				$sql .= 'SET hdr_add_status = 3 ';
				$sql .= 'WHERE  hdr_id = '.$request_id;
				$id = $this->db_update($sql);

				if($id > 0) {
					$this->system_log[] = 'hdr_add_status is now Returned on '.$this->getTableName();
				}
			} else {
				//This is one of the ineterested parties
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = 3 ';
				$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.$reception_id;
				$id = $this->db_update($sql);

				if($id > 0) {
					$this->system_log[] = 'hdru_status is now Returned on helpdesk_request_users for this intersted party';
				}
			}

			if($id > 0) {
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = 10 ';
				$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
				$id = $this->db_update($sql);

				if($id > 0) {
					$success = true;

					//Find the user below this one
					if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
						$sql .= 'AND ';
						if($user['hdrut_value'] == 'iassist_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
						} elseif($user['hdrut_value'] == 'reseller_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
						}
						$sql .= 'AND ';
						$sql .= 'helpdesk_users.user_id = '.$reception_id.' ';

						$interested_user = $this->mysql_fetch_one($sql);
					}

					if(isset($interested_user) && is_array($interested_user)) {
						$user_below = $interested_user;
					} else {
						//The user below is the add_user
						//Find add_user for this specific request
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_users.user_id = '.$reception_id.' ';
						$add_user = $this->mysql_fetch_one($sql);

						$user_below = $add_user;
					}

					//Logs
					$this->system_log[] = 'hdru_status is now Waiting on helpdesk_request_users where hdru_userid is '.$user['user_id'];
					$this->user_changes = $user['user_name'].' has returned '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$user_below['user_name'].' for more information';

					$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

					$this->emails[0]['from'] = 'AiT Helpdesk';
					$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
					$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' returned for More Information';
					$this->emails[0]['message'] = $message;

					if($description) {
						$this->emails[0]['custom_message_text'] = $the_actual_desc;
						$this->emails[0]['user_name'] = $user['user_name'];
					}
				}
			} else {
				$success = false;
			}


		} elseif(is_string($action) && $action == 'claim') {
			$user_role = 1;// Assigned
			$user_status = 2;// Confirmed
			$id = $this->helpdeskRequestAssignment($request_id, $user['user_id'], $user_role, $user_status);

			if($id > 0) {
				$success = true;
				$this->system_log[] = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and is now the assigned user, and the status is now confirmed on helpdesk_request_users';
				$this->user_changes = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

				//Find out who logged, or escalated the request and notifify them of the claim
				//Find the user below this one
				if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_request_users ';
					$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

					$sql .= 'INNER JOIN helpdesk_list_user_roles ';
					$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
					$sql .= 'AND ';
					$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
					$sql .= 'AND ';
					if($user['hdrut_value'] == 'iassist_admin_user') {
						$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
					} elseif($user['hdrut_value'] == 'reseller_admin_user') {
						$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
					}

					$interested_user = $this->mysql_fetch_one($sql);
				}

				if(isset($interested_user) && is_array($interested_user)) {
					$user_below = $interested_user;
				} else {
					//The user below is the add_user
					//Find add_user for this specific request
					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
					$add_user = $this->mysql_fetch_one($sql);

					$user_below = $add_user;
				}
				$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

				$this->emails[0]['from'] = 'AiT Helpdesk';
				$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
				$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
				$this->emails[0]['message'] = $message;

				if($description) {
					$this->emails[0]['custom_message_text'] = $the_actual_desc;
					$this->emails[0]['user_name'] = $user['user_name'];
				}

				// Notify the other admin users that the current has claimed responsibility for this request
				//Find the admin users and send them a message
				$sql = 'SELECT * FROM helpdesk_companies ';

				// Company Type
				$sql .= 'INNER JOIN helpdesk_company_types ';
				$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

				// Company helpdesk settings
				$sql .= 'INNER JOIN helpdesk_company_settings ';
				$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

				$sql .= 'INNER JOIN helpdesk_admin_management ';
				$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

				$sql .= 'INNER JOIN helpdesk_request_settings ';
				$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

				//Admin users
				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'INNER JOIN helpdesk_user_admin_types ';
				$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

				$sql .= 'INNER JOIN helpdesk_admin_types ';
				$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

				// Clauses
				$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'].'\'';
				$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';
				$company_admins = $this->mysql_fetch_all($sql);

				$count = 1;

				foreach($company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$message = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
					$this->emails[$count]['from'] = 'AiT Helpdesk';
					$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
					$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
					$this->emails[$count]['message'] = $message;

					$count++;
				}
			} else {
				$success = false;
			}
		} elseif($action == 0) {
			//'This is just a plain update'
			$this->user_changes = $user['user_name'].' has added an update to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
			$success = true;

			if($user['user_id'] == $request['hdr_add_userid']) {
				//Find the assigned user and notify them, if they exist
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$sql .= 'AND ';
				$sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';

				$assigned = $this->mysql_fetch_one($sql);

				$send_to_user = $assigned;
			} else {
				//Find this user's role
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$sql .= 'AND ';
				$sql .= 'helpdesk_users.user_id = '.$user['user_id'].' ';
				$this_user_role = $this->mysql_fetch_one($sql);

				if($this_user_role['hlur_id'] == 2) {//interested party
					//Find assigned user and email them
					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_request_users ';
					$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

					$sql .= 'INNER JOIN helpdesk_list_user_roles ';
					$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
					$sql .= 'AND ';
					$sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';

					$assigned = $this->mysql_fetch_one($sql);
					$send_to_user = $assigned;
				} elseif($this_user_role['hlur_id'] == 1) {//assigned
					//find the user below and notify
					if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
						$sql .= 'AND ';
						if($user['hdrut_value'] == 'iassist_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
						} elseif($user['hdrut_value'] == 'reseller_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
						}

						$interested_user = $this->mysql_fetch_one($sql);
					}

					if(isset($interested_user) && is_array($interested_user)) {
						$user_below = $interested_user;
					} else {
						//The user below is the add_user
						//Find add_user for this specific request
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$add_user = $this->mysql_fetch_one($sql);
						$user_below = $add_user;
					}


					if(isset($this_user_role['hdru_status']) && $this_user_role['hdru_status'] == 10) {//Check if the HDR is awaiting reply ||| THIS IS A HACK
						//Send the message to the person that it's been returned to
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_request_users.hdru_status = 3 ';

						$return_user = $this->mysql_fetch_one($sql);

						if(!(isset($return_user) && is_array($return_user) && count($return_user) > 0)) {//Check that the return user is an admin
							$sql = 'SELECT * FROM helpdesk_request ';
							$sql .= 'INNER JOIN helpdesk_users ';
							$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
							$sql .= 'INNER JOIN helpdesk_companies ';
							$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
							$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
							$add_user = $this->mysql_fetch_one($sql);
							$return_user = $add_user;
						}

						$user_below = $return_user;
					}

					$send_to_user = $user_below;
				}
			}

			$this->user_changes = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' updated by '.$user['user_name'];


			$message = $send_to_user['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($send_to_user['user_tkid'], $send_to_user['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' updated by '.$user['user_name'];
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." has been successfully updated.",
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	// I'm going to do this process using the fundamentals of clean code
	public function setResoultionType($request, $hdrup_res_type) {
		if($this->requestAlreadyHasResolutionType($request)) {
			$id = 0;
			if($this->newResolutionTypeIsDifferentFromPrevious($request, $hdrup_res_type)) {
				$id = $this->replaceOldResolutionTypeWithNewOne($request, $hdrup_res_type);
			}
		} else {
			$id = $this->createRequestResolutionTypeLink($request, $hdrup_res_type);
		}

		return $id;
	}

	public function requestAlreadyHasResolutionType($request) {
		$request_res_type_link = $this->getRequestResolutionTypeLink($request);

		$request_already_has_resolution_type_linked = false;
		if(isset($request_res_type_link) && is_array($request_res_type_link) && count($request_res_type_link) > 0) {
			$request_already_has_resolution_type_linked = true;
		}

		return $request_already_has_resolution_type_linked;
	}

	public function newResolutionTypeIsDifferentFromPrevious($request, $hdrup_res_type) {
		$request_res_type_link = $this->getRequestResolutionTypeLink($request);

		$newResolutionTypeIsDifferentFromPrevious = false;
		if($hdrup_res_type != $request_res_type_link['hrrt_hlrt_id']) {
			$newResolutionTypeIsDifferentFromPrevious = true;
		}

		return $newResolutionTypeIsDifferentFromPrevious;
	}

	public function getRequestResolutionTypeLink($request) {
		$sql = 'SELECT * FROM helpdesk_request_resolution_type ';
		$sql .= 'INNER JOIN helpdesk_list_resolution_type ';
		$sql .= 'ON helpdesk_list_resolution_type.hlrt_id = helpdesk_request_resolution_type.hrrt_hlrt_id ';
		$sql .= 'WHERE helpdesk_request_resolution_type.hrrt_hdr_id = '.$request['hdr_id'];
		$request_res_type_link = $this->mysql_fetch_one($sql);
		return $request_res_type_link;
	}

	public function replaceOldResolutionTypeWithNewOne($request, $hdrup_res_type) {
		$sql = 'UPDATE helpdesk_request_resolution_type ';
		$sql .= 'SET hrrt_hlrt_id = '.$hdrup_res_type.' ';
		$sql .= 'WHERE  hrrt_hdr_id = '.$request['hdr_id'].' ';
		$id = $this->db_update($sql);

		return $id;
	}

	public function createRequestResolutionTypeLink($request, $hdrup_res_type) {
		$sql = 'INSERT INTO helpdesk_request_resolution_type (hrrt_hdr_id,hrrt_hlrt_id) ';
		$sql .= 'VALUES ('.$request['hdr_id'].', ';
		$sql .= $hdrup_res_type.')';
		$id = $this->db_insert($sql);

		return $id;
	}

	public function claimAndProcessObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$action = (is_string($action) && strlen($action) > 2 ? $action : (int)$action);
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//FIRST THINGS FIRST - LET'S DO THE CLAIM
		$user_role = 1;// Assigned
		$user_status = 2;// Confirmed
		$id = $this->helpdeskRequestAssignment($request_id, $user['user_id'], $user_role, $user_status);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and is now the assigned user, and the status is now confirmed on helpdesk_request_users';
			$this->user_changes = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
		} else {
			$success = false;
		}

		//NOW LET'S FIGURE OUT IF THERE'S MORE TO DO WITH IT
		if($action == 4) {
			// Change current_user hdru_status to 4, because he's the one working on it
			$sql = 'UPDATE helpdesk_request_users ';
			$sql .= 'SET hdru_status = '.$action.' ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
			$id = $this->db_update($sql);

			if($id > 0) {
				$success = true;

				$sql = 'SELECT * FROM helpdesk_list_user_status ';
				$sql .= 'WHERE hlus_id = '.$action;
				$new_status = $this->mysql_fetch_one($sql);
				$this->system_log[] = 'hdru_status is now '.$new_status['hlus_value'].' in helpdesk_request_users for '.$user['user_name'];
				$this->user_changes = $user['user_name'].' is now working on '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

				//Find the user below this one
				if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_request_users ';
					$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

					$sql .= 'INNER JOIN helpdesk_list_user_roles ';
					$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

					$sql .= 'INNER JOIN helpdesk_user_types ';
					$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
					$sql .= 'AND ';
					$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
					$sql .= 'AND ';
					if($user['hdrut_value'] == 'iassist_admin_user') {
						$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
					} elseif($user['hdrut_value'] == 'reseller_admin_user') {
						$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
					}

					$interested_user = $this->mysql_fetch_one($sql);
				}

				if(isset($interested_user) && is_array($interested_user)) {
					$user_below = $interested_user;
				} else {
					//The user below is the add_user
					//Find add_user for this specific request
					$sql = 'SELECT * FROM helpdesk_request ';
					$sql .= 'INNER JOIN helpdesk_users ';
					$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
					$sql .= 'INNER JOIN helpdesk_companies ';
					$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
					$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';

					$add_user = $this->mysql_fetch_one($sql);

					$user_below = $add_user;
				}

				//Is the user below the add_user... If yes then this code will never execute anyway because that would mean that the request is escalation level 1
				//Which I've already taken care of somewhere up top
				if($user_below['user_id'] == $request['hdr_add_userid']) {
					//Change the hdr_add_status to 4 as well
					$update_values[$this->getTableField().'add_status'] = $action;
					$sql = 'UPDATE '.$this->getTableName().' SET '.$this->convertArrayToSQL($update_values);
					$sql .= ' WHERE  hdr_id = '.$request_id;
					$id = $this->db_update($sql);
				}

				$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

				$this->emails[0]['from'] = 'AiT Helpdesk';
				$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
				$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' is In Progress';
				$this->emails[0]['message'] = $message;

				if($description) {
					$this->emails[0]['custom_message_text'] = $the_actual_desc;
					$this->emails[0]['user_name'] = $user['user_name'];
				}


			} else {
				$success = false;
			}
		} elseif($action == 9) {
			/*
             * Escalate this request upward
             * */
			$id = $this->helpdeskRequestEscalation($request_id);

			if($id == true) {
				$success = true;
			} else {
				$success = false;
			}
		} elseif($action > 10) {
			//This is a return
			$reception_id = $action - 10;

			//Find out if the user the being returned to is an add user or an interested party
			// So that we know which table to affect
			if($reception_id == $request['hdr_add_userid']) {
				//This is the add user
				$sql = 'UPDATE '.$this->getTableName().' ';
				$sql .= 'SET hdr_add_status = 3 ';
				$sql .= 'WHERE  hdr_id = '.$request_id;
				$id = $this->db_update($sql);

				if($id > 0) {
					$this->system_log[] = 'hdr_add_status is now Returned on '.$this->getTableName();
				}
			} else {
				//This is one of the ineterested parties
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = 3 ';
				$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.$reception_id;
				$id = $this->db_update($sql);

				if($id > 0) {
					$this->system_log[] = 'hdru_status is now Returned on helpdesk_request_users for this intersted party';
				}
			}

			if($id > 0) {
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = 10 ';
				$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
				$sql .= 'AND  hdru_userid = '.(int)$user['user_id'];
				$id = $this->db_update($sql);

				if($id > 0) {
					$success = true;

					//Find the user below this one
					if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_request_users ';
						$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

						$sql .= 'INNER JOIN helpdesk_list_user_roles ';
						$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

						$sql .= 'INNER JOIN helpdesk_user_types ';
						$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
						$sql .= 'AND ';
						if($user['hdrut_value'] == 'iassist_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
						} elseif($user['hdrut_value'] == 'reseller_admin_user') {
							$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
						}
						$sql .= 'AND ';
						$sql .= 'helpdesk_users.user_id = '.$reception_id.' ';

						$interested_user = $this->mysql_fetch_one($sql);
					}

					if(isset($interested_user) && is_array($interested_user)) {
						$user_below = $interested_user;
					} else {
						//The user below is the add_user
						//Find add_user for this specific request
						$sql = 'SELECT * FROM helpdesk_request ';
						$sql .= 'INNER JOIN helpdesk_users ';
						$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
						$sql .= 'INNER JOIN helpdesk_companies ';
						$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
						$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
						$sql .= 'AND ';
						$sql .= 'helpdesk_users.user_id = '.$reception_id.' ';
						$add_user = $this->mysql_fetch_one($sql);

						$user_below = $add_user;
					}

					//Logs
					$this->system_log[] = 'hdru_status is now Waiting on helpdesk_request_users where hdru_userid is '.$user['user_id'];
					$this->user_changes = $user['user_name'].' has returned '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$user_below['user_name'].' for more information';

					$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

					$this->emails[0]['from'] = 'AiT Helpdesk';
					$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
					$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' returned for More Information';
					$this->emails[0]['message'] = $message;

					if($description) {
						$this->emails[0]['custom_message_text'] = $the_actual_desc;
						$this->emails[0]['user_name'] = $user['user_name'];
					}
				}
			} else {
				$success = false;
			}

		} else {
			$this->system_log[] = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and is now the assigned user, and the status is now confirmed on helpdesk_request_users';
			$this->user_changes = $user['user_name'].' has Claimed '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

			//Find out who logged, or escalated the request and notifify them of the claim
			//Find the user below this one
			if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$sql .= 'AND ';
				$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
				$sql .= 'AND ';
				if($user['hdrut_value'] == 'iassist_admin_user') {
					$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
				} elseif($user['hdrut_value'] == 'reseller_admin_user') {
					$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
				}

				$interested_user = $this->mysql_fetch_one($sql);
			}

			if(isset($interested_user) && is_array($interested_user)) {
				$user_below = $interested_user;
			} else {
				//The user below is the add_user
				//Find add_user for this specific request
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$add_user = $this->mysql_fetch_one($sql);

				$user_below = $add_user;
			}
			$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			// Notify the other admin users that the current has claimed responsibility for this request
			//Find the admin users and send them a message
			$sql = 'SELECT * FROM helpdesk_companies ';

			// Company Type
			$sql .= 'INNER JOIN helpdesk_company_types ';
			$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

			// Company helpdesk settings
			$sql .= 'INNER JOIN helpdesk_company_settings ';
			$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

			$sql .= 'INNER JOIN helpdesk_admin_management ';
			$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

			$sql .= 'INNER JOIN helpdesk_request_settings ';
			$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

			//Admin users
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'INNER JOIN helpdesk_user_admin_types ';
			$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

			$sql .= 'INNER JOIN helpdesk_admin_types ';
			$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

			// Clauses
			$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'].'\'';
			$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';
			$company_admins = $this->mysql_fetch_all($sql);

			$count = 1;

			foreach($company_admins as $key => $val) {
				// Send notification email to the admins of this company, notifying them of the new request
				$message = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
				$this->emails[$count]['from'] = 'AiT Helpdesk';
				$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
				$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
				$this->emails[$count]['message'] = $message;

				$count++;
			}
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." has been successfully claimed and processed.",
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}


	public function assignObjectToAdmin($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$assignment_user_id = str_replace('_assign_request_to_', '', $action);
		$assignment_user_id = (int)$assignment_user_id;
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$assignment_user_id;
		$assigned_user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//FIRST THINGS FIRST - LET'S DO THE ASSIGNMENT
		$user_role = 1;// Assigned
		$user_status = 1;// New
		$id = $this->helpdeskRequestAssignment($request_id, $assigned_user['user_id'], $user_role, $user_status);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has assigned '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$assigned_user['user_name'];
			$this->user_changes = $user['user_name'].' has assigned '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$assigned_user['user_name'];

			//Find the user below this one
			if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_list_user_roles ';
				$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$sql .= 'AND ';
				$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
				$sql .= 'AND ';
				if($user['hdrut_value'] == 'iassist_admin_user') {
					$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
				} elseif($user['hdrut_value'] == 'reseller_admin_user') {
					$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
				}

				$interested_user = $this->mysql_fetch_one($sql);
			}

			if(isset($interested_user) && is_array($interested_user)) {
				$user_below = $interested_user;
			} else {
				//The user below is the add_user
				//Find add_user for this specific request
				$sql = 'SELECT * FROM helpdesk_request ';
				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
				$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
				$add_user = $this->mysql_fetch_one($sql);

				$user_below = $add_user;
			}
			$message = $user_below['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' assigned to '.$assigned_user['user_name'];
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}


			//Send an email to the assigned user
			$this->emails[1]['from'] = 'AiT Helpdesk';
			$this->emails[1]['to'] = $this->getAnEmail($assigned_user['user_tkid'], $assigned_user['hdc_cc']);
			$this->emails[1]['subject'] = 'New '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' assigned to you by '.$user['user_name'];
			$this->emails[1]['message'] = $message;

			if($description) {
				$this->emails[1]['custom_message_text'] = $the_actual_desc;
				$this->emails[1]['user_name'] = $user['user_name'];
			}

			// Notify the other admin users that the current has claimed responsibility for this request
			//Find the admin users and send them a message
			$sql = 'SELECT * FROM helpdesk_companies ';

			// Company Type
			$sql .= 'INNER JOIN helpdesk_company_types ';
			$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

			// Company helpdesk settings
			$sql .= 'INNER JOIN helpdesk_company_settings ';
			$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

			$sql .= 'INNER JOIN helpdesk_admin_management ';
			$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

			$sql .= 'INNER JOIN helpdesk_request_settings ';
			$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

			//Admin users
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'INNER JOIN helpdesk_user_admin_types ';
			$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

			$sql .= 'INNER JOIN helpdesk_admin_types ';
			$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

			// Clauses
			$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'].'\'';
			$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].', '.$assigned_user['user_id'].') ';
			$company_admins = $this->mysql_fetch_all($sql);

			if(isset($company_admins) && is_array($company_admins) && count($company_admins) > 0) {
				$count = 2;
				foreach($company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$message = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' assigned to '.$assigned_user['user_name'].' by '.$user['user_name'];
					$this->emails[$count]['from'] = 'AiT Helpdesk';
					$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
					$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' claimed by '.$user['user_name'];
					$this->emails[$count]['message'] = $message;

					$count++;
				}
			}
		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." has been successfully assigned to ".$assigned_user['user_name'],
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	public function transferObjectToAnotherAdmin($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$transfer_user_id = str_replace('_transfer_request_to_', '', $action);
		$transfer_user_id = (int)$transfer_user_id;
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		//get the transfer user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$transfer_user_id;
		$transfer_user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//01. FIRST THINGS FIRST - LET'S SEND THE TRANSFER REQUEST
		$user_role = 3;// Pending Assigned User
		$user_status = 11;// New Transfer
		$id = $this->helpdeskRequestAssignment($request_id, $transfer_user['user_id'], $user_role, $user_status);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has transferred '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$transfer_user['user_name'];
			$this->user_changes = $user['user_name'].' has transferred '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$transfer_user['user_name'];

			$message = $transfer_user['user_name'].', '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' has been transferred to you.';

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($transfer_user['user_tkid'], $transfer_user['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request';
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//02. THEN CHANGE THE STATUS FOR THE CURRENT USER TO PENDING TRANSFERS
			$user_status = 12;// Pending Transfer

			//This is one of the ineterested parties
			$sql = 'UPDATE helpdesk_request_users ';
			$sql .= 'SET hdru_status = '.$user_status.' ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_userid = '.$user['user_id'];
			$id = $this->db_update($sql);


			if($id > 0) {
				$success = true;

				$message = $user['user_name'].', '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' has been transferred to '.$transfer_user['user_name'].'.';

				//Send an email to the assigned user
				$this->emails[1]['from'] = 'AiT Helpdesk';
				$this->emails[1]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
				$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request sent to '.$transfer_user['user_name'];
				$this->emails[1]['message'] = $message;

				if($description) {
					$this->emails[1]['custom_message_text'] = $the_actual_desc;
					$this->emails[1]['user_name'] = $user['user_name'];
				}

			} else {
				$success = false;
			}

		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => "You have successfully sent a transfer request for ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." to ".$transfer_user['user_name'],
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	public function reclaimPendingTransferObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$transfer_user_id = str_replace('_transfer_request_to_', '', $action);
		$transfer_user_id = (int)$transfer_user_id;
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);


		//Get the transfer initiator
		$sql = 'SELECT * FROM helpdesk_request_users ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_role = 3';
		$pending_transfer_user_connection = $this->mysql_fetch_one($sql);

		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$pending_transfer_user_connection['hdru_userid'];
		$pending_transfer_user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//01. Change the status for the transfer initiator to in-progress if this is an escalated request, otherwise make the status reflect the add_status
		//Find the user below this one
		if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_list_user_roles ';
			$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$sql .= 'AND ';
			$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
			$sql .= 'AND ';
			if($user['hdrut_value'] == 'iassist_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
			} elseif($user['hdrut_value'] == 'reseller_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
			}

			$interested_user = $this->mysql_fetch_one($sql);
		}

		if(isset($interested_user) && is_array($interested_user)) {
			$user_below = $interested_user;
		} else {
			//The user below is the add_user
			//Find add_user for this specific request
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$add_user = $this->mysql_fetch_one($sql);

			$user_below = $add_user;
		}


		$user_status = 4;

		if($this->userBelowIsAdminUserInPipeline($user_below) && !$this->statusIsEscalated($user_below['hdru_status'])) {
			$user_status = $user_below['hdru_status'];
		} elseif(!$this->statusIsEscalated($user_below['hdr_add_status'])) {
			$user_status = $user_below['hdr_add_status'];
		}

		$sql = 'UPDATE helpdesk_request_users ';
		$sql .= 'SET hdru_status = '.$user_status.' ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_userid = '.$user['user_id'];
		$id = $this->db_update($sql);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has cancelled the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and has reclaimed responsibility for it. Transfer is no longer pending.';
			$this->user_changes = $user['user_name'].' has cancelled the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and has reclaimed responsibility for it. Transfer is no longer pending.';

			//03.1 Tell the transfer initiator that the transfer has been cancelled (Current User)
			$message = $user['user_name'].', you\'ve cancelled the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and reclaimed responsibility for it.';

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request Cancelled successfully';
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//02. Get rid of the pending transfer user from the HDRU table
			$user_role = 3;// Pending Assigned User

			//This is one of the ineterested parties
			$sql = 'DELETE FROM helpdesk_request_users ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_role = '.$user_role.' ';
			$sql .= 'AND  hdru_userid = '.$pending_transfer_user['user_id'];
			$id = $this->db_query($sql);


			if($id > 0) {
				$success = true;

				$message = $pending_transfer_user['user_name'].', '.$user['user_name'].' has cancelled the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and reclaimed responsibility for it.';

				//03.2 Tell the current user that they've rejected the transfer
				$this->emails[1]['from'] = 'AiT Helpdesk';
				$this->emails[1]['to'] = $this->getAnEmail($pending_transfer_user['user_tkid'], $pending_transfer_user['hdc_cc']);
				$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request cancelled.';
				$this->emails[1]['message'] = $message;

				if($description) {
					$this->emails[1]['custom_message_text'] = $the_actual_desc;
					$this->emails[1]['user_name'] = $user['user_name'];
				}

			} else {
				$success = false;
			}

		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => "You have cancelled the transfer of ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." successfully. ",
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	private function userBelowIsAdminUserInPipeline($user_below) {
		$userBelowIsAdminUserInPipeline = false;
		if((isset($user_below) && is_array($user_below) && count($user_below) > 0) && (isset($user_below['hlur_id']) && (int)$user_below['hlur_id'] == 2)) {
			$userBelowIsAdminUserInPipeline = true;
		}
		return $userBelowIsAdminUserInPipeline;
	}

	private function statusIsEscalated($status) {
		$statusIsEscalated = false;
		if((int)$status == 9) {
			$statusIsEscalated = true;
		}
		return $statusIsEscalated;
	}

	public function rejectObjectTransfer($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$transfer_user_id = str_replace('_transfer_request_to_', '', $action);
		$transfer_user_id = (int)$transfer_user_id;
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);


		//Get the transfer initiator
		$sql = 'SELECT * FROM helpdesk_request_users ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_role = 1';
		$transfer_initiator_connection = $this->mysql_fetch_one($sql);

		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$transfer_initiator_connection['hdru_userid'];
		$transfer_initiator = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//01. Change the status for the transfer initiator to in-progress if this is an escalated request, otherwise make the status reflect the add_status
		//Find the user below this one
		if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_list_user_roles ';
			$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$sql .= 'AND ';
			$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
			$sql .= 'AND ';
			if($user['hdrut_value'] == 'iassist_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
			} elseif($user['hdrut_value'] == 'reseller_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
			}

			$interested_user = $this->mysql_fetch_one($sql);
		}

		if(isset($interested_user) && is_array($interested_user)) {
			$user_below = $interested_user;
		} else {
			//The user below is the add_user
			//Find add_user for this specific request
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$add_user = $this->mysql_fetch_one($sql);

			$user_below = $add_user;
		}


		$user_status = (isset($user_below['hdr_add_status']) && is_numeric($user_below['hdr_add_status']) ? $user_below['hdr_add_status'] : 4);
		$sql = 'UPDATE helpdesk_request_users ';
		$sql .= 'SET hdru_status = '.$user_status.' ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_userid = '.$transfer_initiator['user_id'];
		$id = $this->db_update($sql);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has rejected the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and returned it to '.$transfer_initiator['user_name'];
			$this->user_changes = $user['user_name'].' has rejected the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and returned it to '.$transfer_initiator['user_name'];

			//03.1 Tell the transfer initiator that the transfer has been rejected
			$message = $transfer_initiator['user_name'].', '.$user['user_name'].' has rejected the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and returned it to you.';

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($transfer_initiator['user_tkid'], $transfer_initiator['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request Rejection';
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//02. Get rid of the user who rejected the transfer from the HDRU table
			$user_role = 3;// Pending Assigned User

			//This is one of the ineterested parties
			$sql = 'DELETE FROM helpdesk_request_users ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_role = '.$user_role.' ';
			$sql .= 'AND  hdru_userid = '.$user['user_id'];
			$id = $this->db_query($sql);


			if($id > 0) {
				$success = true;

				$message = $user['user_name'].', you\'ve rejected the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' and returned it to '.$transfer_initiator['user_name'].'.';

				//03.2 Tell the current user that they've rejected the transfer
				$this->emails[1]['from'] = 'AiT Helpdesk';
				$this->emails[1]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
				$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request rejection successfully.';
				$this->emails[1]['message'] = $message;

				if($description) {
					$this->emails[1]['custom_message_text'] = $the_actual_desc;
					$this->emails[1]['user_name'] = $user['user_name'];
				}

			} else {
				$success = false;
			}

		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => "You have rejected the transfer of ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.", it is no longer in your queue. ",
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

	}

	public function acceptObjectTransfer($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);


		//Get the transfer initiator
		$sql = 'SELECT * FROM helpdesk_request_users ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_role = 1';
		$transfer_initiator_connection = $this->mysql_fetch_one($sql);

		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$transfer_initiator_connection['hdru_userid'];
		$transfer_initiator = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//01. Change the status for the transfer initiator to in-progress if this is an escalated request, otherwise make the status reflect the add_status
		//Find the user below this one
		if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_list_user_roles ';
			$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$sql .= 'AND ';
			$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
			$sql .= 'AND ';
			if($user['hdrut_value'] == 'iassist_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
			} elseif($user['hdrut_value'] == 'reseller_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
			}

			$interested_user = $this->mysql_fetch_one($sql);
		}

		if(isset($interested_user) && is_array($interested_user)) {
			$user_below = $interested_user;
		} else {
			//The user below is the add_user
			//Find add_user for this specific request
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$add_user = $this->mysql_fetch_one($sql);

			$user_below = $add_user;
		}

		//01. Change the status for this user to the one they've chosen
		$action_status = ($action == '_accept_transfer_and_confirm' ? 2 : 4);

		$sql = 'UPDATE helpdesk_request_users ';
		$sql .= 'SET hdru_status = '.$action_status.', ';
		$sql .= 'hdru_role = 1 ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_userid = '.$user['user_id'];
		$id = $this->db_update($sql);

		if($id > 0) {
			$success = true;
			//04.1 Tell the current user that they've accepted the transfer
			$this->system_log[] = $user['user_name'].' has successfully accepted the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' from '.$transfer_initiator['user_name'];
			$this->user_changes = $user['user_name'].', you have successfully accepted the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' from '.$transfer_initiator['user_name'];

			//03.1 Tell the transfer initiator that the transfer has been rejected
			$message = $user['user_name'].', you have successfully accepted the transfer of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' from '.$transfer_initiator['user_name'];

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Request Accepted';
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//02. Check the user below and see if the status therein should reflect the one chosen by the current admin user
			if($user_below['user_id'] == $request['hdr_add_userid']) {
				$sql = 'UPDATE helpdesk_request ';
				$sql .= 'SET hdr_add_status = '.$action_status.' ';
				$sql .= 'WHERE  hdr_id = '.$request_id.' ';
				$sql .= 'AND  hdr_add_userid = '.$request['user_id'];
				$id = $this->db_update($sql);
				if($id > 0) {
					$success = true;
				} else {
					$success = false;
				}
			}
			//04.3 Tell the user below that the request has been transferred and the status has changed
			$message = $user_below['user_name'].', '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' has been transferred by '.$transfer_initiator['user_name'].' to '.$user['user_name'].' who is now the Admin responsible for it';

			$this->emails[1]['from'] = 'AiT Helpdesk';
			$this->emails[1]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
			$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transferred to '.$user['user_name'].' by '.$transfer_initiator['user_name'];
			$this->emails[1]['message'] = $message;

			if($description) {
				$this->emails[1]['custom_message_text'] = $the_actual_desc;
				$this->emails[1]['user_name'] = $user['user_name'];
			}

			//03. Get rid of the user who initiated the transfer from the HDRU table
			$sql = 'DELETE FROM helpdesk_request_users ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_userid = '.$transfer_initiator['user_id'];
			$id = $this->db_query($sql);

			if($id > 0) {
				$success = true;

				//04.2 Tell the transfer initiator that the request has been successfully transferred
				$message = $transfer_initiator['user_name'].', you have successfully transferred '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' to '.$user['user_name'].' who is now responsible for it';

				$this->emails[2]['from'] = 'AiT Helpdesk';
				$this->emails[2]['to'] = $this->getAnEmail($transfer_initiator['user_tkid'], $transfer_initiator['hdc_cc']);
				$this->emails[2]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transfer Successful';
				$this->emails[2]['message'] = $message;

				if($description) {
					$this->emails[2]['custom_message_text'] = $the_actual_desc;
					$this->emails[2]['user_name'] = $user['user_name'];
				}
			} else {
				$success = false;
			}

		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		$updated = false;
		if($success === true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//04. Let everyone involved know what's going on
				//Process Emails
				$this->processEmails($request);
			}

		}

		//Return the relevant message upon success or failure
		if($updated === true) {
			$result = array(
				0 => "ok",
				1 => "You have accepted the transfer of ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.", you are now the assigned user. ",
				'object_id' => $request_id,
			);
			return $result;
		} else {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	public function seizeAndAssumeResponsibilityOfObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$action = $var['hdr_action'];
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$update_values = array();


		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);


		//Get the current assigned user
		$sql = 'SELECT * FROM helpdesk_request_users ';
		$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
		$sql .= 'AND  hdru_role = 1';
		$current_assigned_user_connection = $this->mysql_fetch_one($sql);

		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$current_assigned_user_connection['hdru_userid'];
		$current_assigned_user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//01. Change the status for the transfer initiator to in-progress if this is an escalated request, otherwise make the status reflect the add_status
		//Find the user below this one
		if($user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user') {
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_list_user_roles ';
			$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$sql .= 'AND ';
			$sql .= 'helpdesk_list_user_roles.hlur_id = 2 ';
			$sql .= 'AND ';
			if($user['hdrut_value'] == 'iassist_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'reseller_admin_user\' ';
			} elseif($user['hdrut_value'] == 'reseller_admin_user') {
				$sql .= 'helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
			}

			$interested_user = $this->mysql_fetch_one($sql);
		}

		if(isset($interested_user) && is_array($interested_user)) {
			$user_below = $interested_user;
		} else {
			//The user below is the add_user
			//Find add_user for this specific request
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';
			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$add_user = $this->mysql_fetch_one($sql);

			$user_below = $add_user;
		}

		//01. Change the status for this user to the one that the current assigned user has, INSERT
		$action_status = ((int)$current_assigned_user_connection['hdru_status'] == 1 ? 2 : $current_assigned_user_connection['hdru_status']);

		$user_role = 1;//Assigned User
		$user_status = $action_status;
		$id = $this->helpdeskRequestAssignment($request_id, $user['user_id'], $user_role, $user_status);
		//////***************************************************************************************

		if($id > 0) {
			$success = true;
			//04.1 Tell the current user that they've accepted the transfer
			$this->system_log[] = $user['user_name'].' has successfully seized and assumed responsibility of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'.';
			$this->user_changes = $user['user_name'].' has successfully seized and assumed responsibility of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'.';

			//03.1 Tell the transfer initiator that the transfer has been rejected
			$message = $user['user_name'].', you have successfully seized and assumed responsibility of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' from '.$current_assigned_user['user_name'];

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
			$this->emails[0]['subject'] = 'Successful Seizure of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//02. Check the user below and see if the status therein should reflect the one chosen by the current admin user
			if($user_below['user_id'] == $request['hdr_add_userid']) {
				$sql = 'UPDATE helpdesk_request ';
				$sql .= 'SET hdr_add_status = '.$action_status.' ';
				$sql .= 'WHERE  hdr_id = '.$request_id.' ';
				$sql .= 'AND  hdr_add_userid = '.$request['user_id'];
				$id = $this->db_update($sql);
				if($id > 0) {
					$success = true;
				} else {
					$success = false;
				}
			}
			//04.3 Tell the user below that the request has been transferred and the status has changed
			$message = $user_below['user_name'].', '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' has been transferred from '.$current_assigned_user['user_name'].' to '.$user['user_name'].' who is now the Admin responsible for it';

			$this->emails[1]['from'] = 'AiT Helpdesk';
			$this->emails[1]['to'] = $this->getAnEmail($user_below['user_tkid'], $user_below['hdc_cc']);
			$this->emails[1]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Transferred to '.$user['user_name'].' from '.$current_assigned_user['user_name'];
			$this->emails[1]['message'] = $message;

			if($description) {
				$this->emails[1]['custom_message_text'] = $the_actual_desc;
				$this->emails[1]['user_name'] = $user['user_name'];
			}

			//03. Get rid of the user record who this user is taking the request from in the HDRU table
			$sql = 'DELETE FROM helpdesk_request_users ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$sql .= 'AND  hdru_userid = '.$current_assigned_user['user_id'];
			$id = $this->db_query($sql);

			if($id > 0) {
				$success = true;

				//04.2 Tell the transfer initiator that the request has been successfully transferred
				$message = $current_assigned_user['user_name'].', '.$user['user_name'].' has successfully seized and assumed responsibility of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'. You are no longer responsible for it';

				$this->emails[2]['from'] = 'AiT Helpdesk';
				$this->emails[2]['to'] = $this->getAnEmail($current_assigned_user['user_tkid'], $current_assigned_user['hdc_cc']);
				$this->emails[2]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Seized by '.$user['user_name'];
				$this->emails[2]['message'] = $message;

				if($description) {
					$this->emails[2]['custom_message_text'] = $the_actual_desc;
					$this->emails[2]['user_name'] = $user['user_name'];
				}
			} else {
				$success = false;
			}

		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		$updated = false;
		if($success === true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//04. Let everyone involved know what's going on
				//Process Emails
				$this->processEmails($request);
			}

		}

		//Return the relevant message upon success or failure
		if($updated === true) {
			$result = array(
				0 => "ok",
				1 => "You have seized and assumed responsibility of ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.", and you are now the assigned user. ",
				'object_id' => $request_id,
			);
			return $result;
		} else {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}
	}

	public function silentlyTerminateObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);

		//Parse everything that needs to be parsed
		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];


		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		//Put information into db
		$var[$this->getTableField().'add_date'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'escalation_level'] = 1;
		$var[$this->getTableField().'add_status'] = 1;

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		$user_status = 8;//'terminated' = 'closed'

		//Make the request terminated in the helpdesk table
		$sql = 'UPDATE helpdesk_request ';
		$sql .= 'SET hdr_add_status = '.$user_status.' ';
		$sql .= 'WHERE  hdr_id = '.$request_id.' ';
		$id = $this->db_update($sql);

		if($id > 0) {
			$success = true;
			$this->system_log[] = $user['user_name'].' has terminated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;
			$this->user_changes = $user['user_name'].' has terminated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id;

			//03.1 Tell the transfer initiator that the transfer has been rejected
			$message = $user['user_name'].', You have successfully terminated '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'.';

			$this->emails[0]['from'] = 'AiT Helpdesk';
			$this->emails[0]['to'] = $this->getAnEmail($user['user_tkid'], $user['hdc_cc']);
			$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' Successfully Terminated';
			$this->emails[0]['message'] = $message;

			if($description) {
				$this->emails[0]['custom_message_text'] = $the_actual_desc;
				$this->emails[0]['user_name'] = $user['user_name'];
			}

			//HACK FO THE UNCLAIMED HDR'S - BEGIN
			//Check if the current user is involved in this HDR, and add them as the assigned if not XXXX
			//Find this user's role
			$sql = 'SELECT * FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_list_user_roles ';
			$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id.' ';
			$sql .= 'AND ';
			$sql .= 'helpdesk_users.user_id = '.$user['user_id'].' ';
			$this_user_role = $this->mysql_fetch_one($sql);

			if(!(isset($this_user_role) && is_array($this_user_role) && count($this_user_role) > 0)) {
				//FIRST THINGS FIRST - LET'S DO THE CLAIM
				$user_role = 1;// Assigned
				$id = $this->helpdeskRequestAssignment($request_id, $user['user_id'], $user_role, $user_status);
			}
			//HACK FO THE UNCLAIMED HDR'S - END


			//Make the request terminated in the hdru table
			$sql = 'UPDATE helpdesk_request_users ';
			$sql .= 'SET hdru_status = '.$user_status.' ';
			$sql .= 'WHERE  hdru_hdrid = '.$request_id.' ';
			$id = $this->db_update($sql);
		} else {
			$success = false;
		}

		//Return the relevant message upon success or failure
		if($success == true) {

			$log_time = false;
			if(isset($var['ht_amount']) && $var['ht_amount'] > 0) {
				$this->user_changes .= '. '.$var['ht_amount'].' minutes logged to this request.';
				$log_time = true;
			}

			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user_id;

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//This is where we make an insert into the helpdesk_time table, if there is a time to insert, otherwise just carry on as normal
				if($log_time == true) {
					$updated = false;

					//Enter the amount of time that's been spent on this request in the helpdesk_time table
					$sql = 'INSERT INTO helpdesk_time (ht_hdru_id,ht_amount) ';
					$sql .= 'VALUES ('.$id.', ';
					$sql .= $var['ht_amount'].')';
					$id = $this->db_insert($sql);

					if($id > 0) {
						$updated = true;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => "You have successfully terminated ".strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'.',
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

	}

	public function getModules($var) {

		//If $var['cc'] && $var['user_tkid'] are set then this call is coming from an edit instead of a new launch
		$cc = (isset($var['cc']) ? $var['cc'] : $_SESSION['cc']);
		$tkid = (isset($var['user_tkid']) ? $var['user_tkid'] : $_SESSION['tid']);

		$int_parsed_topic_id = (int)$var['topic_id'];

		/*
 * Get list of all modules from ignite4u man_modules,
 * and get the ones that aren't in the helpdesk system in there
 * - START
 * */
		$master_db_obj = new ASSIST_MODULE_HELPER('master');

		$sql = 'SELECT * FROM man_modules ';
		$sql .= 'WHERE ((mod_status & '.MAN_HELPER::INACTIVE.') <> '.MAN_HELPER::INACTIVE.' AND (mod_status & '.MAN_HELPER::MODULE_ARCHIVED.') <> '.MAN_HELPER::MODULE_ARCHIVED.' AND (mod_status & '.MAN_HELPER::ACTIVE.') = '.MAN_HELPER::ACTIVE.') ';
		$sql .= 'ORDER BY mod_generic_name, mod_loc ';

		$all_ignite_modules = $master_db_obj->mysql_fetch_all_by_id($sql, 'mod_id');

		$client_db = new ASSIST_MODULE_HELPER('client', $cc);
		$listObject = new HELPDESK_LIST('module');

		$full_module_list = $listObject->getActiveListItems();

		foreach($full_module_list as $key => $val) {
			foreach($all_ignite_modules as $key2 => $val2) {
				if($val['shortcode'] == $val2['mod_loc']) {
					unset($all_ignite_modules[$key2]);
				}
			}
		}

//Then insert all of the modules that aren't in the system, if any remain
		if(isset($all_ignite_modules) && is_array($all_ignite_modules) && count($all_ignite_modules) > 0) {
			foreach($all_ignite_modules as $key => $val) {
				$var = array();
				$var['value'] = $val['mod_generic_name'];
				$var['shortcode'] = $val['mod_loc'];
				$var['status'] = HELPDESK::ACTIVE;

				$listObject->addListItem($var);
			}
			$full_module_list = $listObject->getActiveListItems();
		}
		/*
         * Get list of all modules from ignite4u man_modules,
         * and get the ones that aren't in the helpdesk system in there
         * - END
         * */


		/*
         * Get list of all modules that this company has access to and the names thereof,
         * and get the ones that aren't in the helpdesk system in there
         * - START
         * */
		$sql = 'SELECT * FROM assist_menu_modules ';
		$sql .= 'WHERE modyn = \'Y\' ';
		$sql .= 'ORDER BY modtext ';
		$company_modules = $client_db->mysql_fetch_all($sql);

		$hd_company_obj = new HELPDESK_COMPANY();
		$company_info = $hd_company_obj->getActiveCompanyInformation($_SESSION['cc']);

		$sql = 'SELECT * FROM helpdesk_list_company_module ';
		$sql .= 'WHERE hlcm_hdc_id = '.$company_info['hdc_id'].' ';
		$sql .= 'ORDER BY hlcm_modtext ';
		$helpdesk_company_modules = $this->mysql_fetch_all($sql);

		foreach($helpdesk_company_modules as $key => $val) {
			foreach($company_modules as $key2 => $val2) {
				if($val['hlcm_modid'] == $val2['modid']) {
					unset($company_modules[$key2]);
				}
			}
		}

		if(isset($company_modules) && is_array($company_modules) && count($company_modules) > 0) {
			foreach($company_modules as $key => $val) {
				$var = array();
				$var['hlcm_hdc_id'] = $company_info['hdc_id'];

				foreach($full_module_list as $key2 => $val2) {
					if($val['modlocation'] == $val2['shortcode']) {
						$var['hlcm_hlm_id'] = $val2['id'];
						break;
					} else {
						$var['hlcm_hlm_id'] = 0;
					}
				}

				$var['hlcm_modid'] = $val['modid'];
				$var['hlcm_modref'] = $val['modref'];
				$var['hlcm_modtext'] = $val['modtext'];

				$sql = 'INSERT INTO helpdesk_list_company_module ';
				$sql .= '(hlcm_hdc_id, hlcm_hlm_id, hlcm_modid, hlcm_modref, hlcm_modtext)';
				$sql .= 'VALUES ('.$var['hlcm_hdc_id'].', '.$var['hlcm_hlm_id'].', '.$var['hlcm_modid'].', \''.$var['hlcm_modref'].'\', \''.$var['hlcm_modtext'].'\')';
				$id = $this->db_insert($sql);
			}
			$sql = 'SELECT * FROM helpdesk_list_company_module ';
			$sql .= 'WHERE hlcm_hdc_id = '.$company_info['hdc_id'].' ';
			$sql .= 'ORDER BY hlcm_modtext ';
			$helpdesk_company_modules = $this->mysql_fetch_all($sql);
		}

		/*
         * Get list of all modules that this company has access to and the names thereof,
         * and get the ones that aren't in the helpdesk system in there
         * - END
         * */

		//Client Admin User Hack For PWC - As Requested by JC during meeting on 8 August 2017 - Start
		//Get the current user record if it's a client_admin_user, from this company
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_tkid = \''.$_SESSION['tid'].'\' ';
		$sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'].'\' ';
		$sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
		$user = $this->mysql_fetch_one($sql);

		$show_every_module_for_client_admin_user = false;
		if(isset($user) && is_array($user) && count($user) > 0) {
			$show_every_module_for_client_admin_user = true;
		}

		//Client Admin User Hack For PWC - As Requested by JC during meeting on 8 August 2017 - END

		$hd_user_obj = new HELPDESK_USERS();
		$current_user = $hd_user_obj->getCurrentHelpdeskUser();

		if(!(isset($current_user) && is_array($current_user) && count($current_user) > 0)) {
			$current_user = $hd_user_obj->addObject($_SESSION['tid'], $_SESSION['tkn'], $_SESSION['cc']);
		}

		/*  TOPICS
         *  [1] => Request New Module
         *  [2] => Report Bug
            [3] => Request Custom Module Functionality
            [4] => Request Access To Module
            [5] => Other
            [6] => Request new client db
         * */

		$sql = 'SELECT * FROM assist_menu_modules ';
		$sql .= 'WHERE modyn = \'Y\' ';
		$sql .= 'ORDER BY modtext ';
		$company_modules = $client_db->mysql_fetch_all($sql);

		$show_company_modules = false;
		$show_generic_modules = false;
		$show_helpdesk_module = false;
		$show_unspecified_module = false;
		if($int_parsed_topic_id == 1) {
			//Show all generic modules
			$show_generic_modules = true;

		} elseif(($current_user['hdrut_value'] == 'client_user' || $current_user['hdrut_value'] == 'reseller_user') && ($int_parsed_topic_id == 2 || $int_parsed_topic_id == 3 || $int_parsed_topic_id == 7)) {
			//Show all of my modules + Helpdesk && Unspecified
			$show_company_modules = true;
			$show_helpdesk_module = true;
			$show_unspecified_module = true;


			/*This section should return modules that the current user already has access to*/
			$sql = 'SELECT * FROM assist_menu_modules ';
			$sql .= 'INNER JOIN assist_'.$cc.'_menu_modules_users ';
			$sql .= 'ON assist_'.$cc.'_menu_modules_users.usrmodid = assist_menu_modules.modid ';
			$sql .= 'WHERE assist_menu_modules.modyn = \'Y\' ';
			$sql .= 'AND assist_'.$cc.'_menu_modules_users.usrtkid = \''.$tkid.'\' ';
			$sql .= 'ORDER BY modtext ';

			$current_user_modules = $client_db->mysql_fetch_all($sql);

			$company_modules = $current_user_modules;

		} elseif(($current_user['hdrut_value'] == 'client_user' || $current_user['hdrut_value'] == 'reseller_user') && $int_parsed_topic_id == 4) {
			//All company modules not in my modules
			$show_company_modules = true;

			/*This section should return modules that the current user already has access to*/
			$sql = 'SELECT * FROM assist_menu_modules ';
			$sql .= 'INNER JOIN assist_'.$cc.'_menu_modules_users ';
			$sql .= 'ON assist_'.$cc.'_menu_modules_users.usrmodid = assist_menu_modules.modid ';
			$sql .= 'WHERE assist_menu_modules.modyn = \'Y\' ';
			$sql .= 'AND assist_'.$cc.'_menu_modules_users.usrtkid = \''.$tkid.'\' ';
			$sql .= 'ORDER BY modtext ';

			$current_user_modules = $client_db->mysql_fetch_all_by_id($sql, 'modid');

			foreach($current_user_modules as $key => $val) {
				if(isset($company_modules[$key])) {
					unset($company_modules[$key]);
				}
			}

			$mod_ids = array();
			foreach($company_modules as $key => $val) {
				$mod_ids[$key] = $key;
			}

			$sql = 'SELECT * FROM assist_menu_modules ';
			$sql .= 'WHERE modyn = \'Y\' ';
			$sql .= 'AND modid IN ('.implode(',', $mod_ids).') ';
			$sql .= 'ORDER BY modtext ';

			$company_modules = $client_db->mysql_fetch_all($sql);

		} elseif($current_user['hdrut_value'] == 'client_admin_user') {
			//All company modules + Helpdesk
			$show_company_modules = true;
			$show_helpdesk_module = true;

			if($int_parsed_topic_id == 2 || $int_parsed_topic_id == 3) {
				// + Unspecified
				$show_unspecified_module = true;
			}

		} else {
			//All Company Modules + All Generic Modules
			$show_company_modules = true;
			$show_generic_modules = true;

		}

		$html = '<select id="hdr_moduleid" name="hdr_moduleid" required="1" req="1" unspecified="">';
		$html .= '<option value="X">--- SELECT ---</option>';
		if($show_company_modules == true) {
			$helpdesk_company_modules_to_loop_through = array();

			foreach($helpdesk_company_modules as $key => $val) {
				foreach($company_modules as $key2 => $val2) {
					if($val['hlcm_modid'] == $val2['modid']) {
						$helpdesk_company_modules_to_loop_through[$val['hlcm_id']]['id'] = $val['hlcm_hlm_id'].'_companyMod_'.$val['hlcm_modid'];
						$helpdesk_company_modules_to_loop_through[$val['hlcm_id']]['value'] = $val2['modtext'];
						$helpdesk_company_modules_to_loop_through[$val['hlcm_id']]['shortcode'] = $val2['modlocation'];
					}
				}
			}

			$html .= '<option value="X">--- COMPANY MODULES ---</option>';
			foreach($helpdesk_company_modules_to_loop_through as $key => $val) {
				$html .= '<option value="'.$val['id'].'" full="Complaints Assist" list_num="0">'.$val['value'].' ['.$val['shortcode'].']'.'</option>';
			}
		}

		if($show_generic_modules == true) {
			$sql = 'SELECT * FROM man_modules ';
			$sql .= 'WHERE ((mod_status & '.MAN_HELPER::INACTIVE.') <> '.MAN_HELPER::INACTIVE.' AND (mod_status & '.MAN_HELPER::MODULE_ARCHIVED.') <> '.MAN_HELPER::MODULE_ARCHIVED.' AND (mod_status & '.MAN_HELPER::ACTIVE.') = '.MAN_HELPER::ACTIVE.') ';
			$sql .= 'ORDER BY mod_generic_name, mod_loc ';

			$all_ignite_modules = $master_db_obj->mysql_fetch_all_by_id($sql, 'mod_id');

			$helpdesk_generic_modules_to_loop_through = array();

			foreach($full_module_list as $key => $val) {
				if($val['shortcode'] != 'S' && $val['shortcode'] != 'TK' /*|| $val['shortcode'] != 'SYSTEM REPORTS'*/) {
					foreach($all_ignite_modules as $key2 => $val2) {
						if($val['shortcode'] == $val2['mod_loc']) {
							$helpdesk_generic_modules_to_loop_through[$val['id']]['id'] = $val['id'].'_genericMod_0';
							$helpdesk_generic_modules_to_loop_through[$val['id']]['value'] = $val2['mod_generic_name'];
							$helpdesk_generic_modules_to_loop_through[$val['id']]['shortcode'] = $val2['mod_loc'];
						}
					}
				}
			}

			$html .= '<option value="X">--- GENERIC MODULES ---</option>';
			foreach($helpdesk_generic_modules_to_loop_through as $key => $val) {
				$html .= '<option value="'.$val['id'].'" full="Complaints Assist" list_num="0">'.$val['value'].' ['.$val['shortcode'].']'.'</option>';
			}

		}

		if($show_helpdesk_module == true) {
			$html .= '<option value="'.$full_module_list[17]['id'].'_genericMod_0'.'" full="Complaints Assist" list_num="0">'.$full_module_list[17]['value'].' ['.$full_module_list[17]['shortcode'].']'.'</option>';
		}

		if($show_unspecified_module == true) {
			$html .= '<option value="'.$full_module_list[18]['id'].'_genericMod_0'.'" full="Complaints Assist" list_num="0">'.$full_module_list[18]['value'].' ['.$full_module_list[18]['shortcode'].']'.'</option>';
		}

		if($current_user['hdrut_value'] != 'client_user' && $current_user['hdrut_value'] != 'reseller_user') {
			//Add Master, User Man, And System Reports Once It's added
			$html .= '<option value="'.$full_module_list[31]['id'].'_genericMod_0'.'" full="Complaints Assist" list_num="0">'.$full_module_list[31]['value'].' ['.$full_module_list[31]['shortcode'].']'.'</option>';
			$html .= '<option value="'.$full_module_list[34]['id'].'_genericMod_0'.'" full="Complaints Assist" list_num="0">'.$full_module_list[34]['value'].' ['.$full_module_list[34]['shortcode'].']'.'</option>';
		}

		$html .= '</select>';

		return $html;
	}

	public function checkActiveRequests($user) {
		$sql = 'SELECT * FROM helpdesk_request ';
		$sql .= 'INNER JOIN helpdesk_request_users ';
		$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_list_user_roles ';
		$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_users.user_id = '.$user['user_id'].' ';

		$active_req = $this->mysql_fetch_all($sql);

		if(isset($active_req) && is_array($active_req) && count($active_req) > 0) {
			$count = count($active_req);
		} else {
			$count = 0;
		}

		return $count;
	}

	public function hdrEdit($var) {

		$description = false;
		if($var['hdrup_description'] != '') {
			$description = true;
			$the_actual_desc = $var['hdrup_description'];
		}

		$request_id = (int)$var['hdrid'];
		$user_id = (int)$var['user_id'];

		$update_values = array();

		//get the current user
		$sql = 'SELECT * FROM helpdesk_users ';
		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
		$sql .= 'WHERE helpdesk_users.user_id = '.$user_id;
		$user = $this->mysql_fetch_one($sql);

		/*
         * Get the actual helpdesk request and
         * all of the information about the user who launched it
         * */
		$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

		$sql .= 'INNER JOIN helpdesk_list_topic ';
		$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

		$sql .= 'INNER JOIN helpdesk_list_module ';
		$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

		$request = $this->mysql_fetch_one($sql);


		if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
			$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value FROM helpdesk_request ';
			$sql .= 'INNER JOIN helpdesk_list_topic ';
			$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request.hdr_id = '.$request_id;

			$request = $this->mysql_fetch_one($sql);
		}

		//Update HDR with the topic and module values
		$sql = 'UPDATE helpdesk_request ';
		$sql .= 'SET hdr_topicid = '.$var['hdr_topicid'];
		if(isset($var['hdr_moduleid'])) {
			$sql .= ', hdr_moduleid = '.$var['hdr_moduleid'];

			$get_modules = true;
		}
		//if escalation level 1, then check if it's new and if it is then change it to confirmed
		if($request['hdr_escalation_level'] == 1 && $request['hdr_add_status'] == 1) {
			$sql .= ', hdr_add_status = 2';
		}
		$sql .= ' ';
		$sql .= 'WHERE  hdr_id = '.$request['hdr_id'].' ';
		$id = $this->db_update($sql);

		if(isset($id) && is_int($id) & $id > 0) {
			//Success
			$success = true;

			//Get Topics
			$listObject = new HELPDESK_LIST('topic');
			$topics = $listObject->getActiveListItemsFormattedForSelect();

			if(isset($get_modules) && $get_modules == true) {
				//Get Modules
				$listObject->changeListType('module');
				$modules = $listObject->getActiveListItemsFormattedForSelect();
			}


			/*    TKT    */
			$this->system_log[] = $user['user_name'].' has edited the details of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' in the helpdesk_request table. The topics is now '.$topics[$var['hdr_topicid']].(isset($get_modules) && $get_modules == true ? ' and the module is now '.$modules[$var['hdr_moduleid']] : '').'.';
			$this->user_changes = $user['user_name'].' has edited '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.'. The topics is now '.$topics[$var['hdr_topicid']].(isset($get_modules) && $get_modules == true ? ' and the module is now '.$modules[$var['hdr_moduleid']] : '').'.';;


			//Check if it's new for the current admin user
			$sql = 'SELECT * FROM helpdesk_request ';

			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE helpdesk_request_users.hdru_hdrid = '.$request['hdr_id'].' ';
			$sql .= 'AND helpdesk_request_users.hdru_status = 1 ';
			$sql .= 'AND helpdesk_users.user_id = '.$user['user_id'].' ';

			$assigned_user = $this->mysql_fetch_one($sql);

			if(isset($assigned_user) && is_array($assigned_user) && count($assigned_user) > 0) {
				$sql = 'UPDATE helpdesk_request_users ';
				$sql .= 'SET hdru_status = 2 ';
				$sql .= 'WHERE hdru_hdrid = '.(int)$assigned_user['hdr_id'].' ';
				$sql .= 'AND  hdru_userid = '.(int)$assigned_user['user_id'];
				$id = $this->db_update($sql);

				if(isset($id) && is_int($id) && $id > 0) {
					$this->system_log[] = $user['user_name'].' has changed the status of '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' From New to Confirmed.';

					//Success
					$success = true;
				} else {
					//Failure
					$success = false;
				}
			}
		} else {
			//Failure
			$success = false;
		}


		//Enter all this information in the update table
		//Return the relevant message upon success or failure
		if($success) {
			$update_log_entry = array();
			$update_log_entry['hdrup_hdrid'] = $request_id;
			$update_log_entry['hdrup_response'] = $var['hdrup_description'];
			$update_log_entry['hdrup_attachment'] = (isset($var['attachment']) ? serialize($var['attachment']) : '');
			$update_log_entry['hdrup_datetime'] = 'GETDATE()';
			$update_log_entry['hdrup_system_changes'] = serialize($this->system_log);
			$update_log_entry['hdrup_user_changes'] = $this->user_changes;
			$update_log_entry['hdrup_userid'] = $user['user_id'];

			//Enter all the update information into the update/log table
			$sql = 'INSERT INTO helpdesk_request_updates (hdrup_hdrid,hdrup_response,hdrup_attachment,hdrup_datetime, hdrup_system_changes, hdrup_user_changes, hdrup_userid) ';
			$sql .= 'VALUES ('.$update_log_entry['hdrup_hdrid'].', ';
			$sql .= '\''.$update_log_entry['hdrup_response'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_attachment'].'\', ';
			$sql .= 'NOW(), ';
			$sql .= '\''.$update_log_entry['hdrup_system_changes'].'\', ';
			$sql .= '\''.$update_log_entry['hdrup_user_changes'].'\', ';
			$sql .= $update_log_entry['hdrup_userid'].')';
			$id = $this->db_insert($sql);

			if($id > 0) {
				$updated = true;

				//Setup the emails
				//Get add user and inform them via email that the assigned admin_user has edited the HDR in question
				$message = $request['user_name'].', the following change has been made to '.strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.': '.$this->user_changes;

				$this->emails[0]['from'] = 'AiT Helpdesk';
				$this->emails[0]['to'] = $this->getAnEmail($request['user_tkid'], $request['hdc_cc']);
				$this->emails[0]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' edited by '.$user['user_name'];
				$this->emails[0]['message'] = $message;

				if($description) {
					$this->emails[0]['custom_message_text'] = $the_actual_desc;
					$this->emails[0]['user_name'] = $user['user_name'];
				}

				//Check if there are other admins involved and notify them of the change if they exist
				$sql = 'SELECT * FROM helpdesk_request ';

				$sql .= 'INNER JOIN helpdesk_request_users ';
				$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

				$sql .= 'INNER JOIN helpdesk_users ';
				$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

				$sql .= 'INNER JOIN helpdesk_companies ';
				$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

				$sql .= 'INNER JOIN helpdesk_user_types ';
				$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

				$sql .= 'WHERE helpdesk_request_users.hdru_hdrid = '.$request['hdr_id'].' ';

				$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';

				$involved_admins = $this->mysql_fetch_one($sql);

				if(isset($involved_admins) && is_array($involved_admins) && count($involved_admins) > 0) {
					$count = 1;

					foreach($involved_admins as $key => $val) {
						// Send notification email to the admins of this company, notifying them of the new request
						$this->emails[$count]['from'] = 'AiT Helpdesk';
						$this->emails[$count]['to'] = $this->getAnEmail($val['user_tkid'], $val['hdc_cc']);
						$this->emails[$count]['subject'] = strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id.' edited by '.$user['user_name'];
						$this->emails[$count]['message'] = $message;

						if($description) {
							$this->emails[$count]['custom_message_text'] = $the_actual_desc;
							$this->emails[$count]['user_name'] = $user['user_name'];
						}

						$count++;
					}
				}

				//Process Emails
				$this->processEmails($request);
			} else {
				// Failure
				$updated = false;
			}

		} elseif(!$success) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

		//Return the relevant message upon success or failure
		if($updated) {
			$result = array(
				0 => "ok",
				1 => strtoupper($request['hdc_cc']).'/'.$this::REFTAG.$request_id." has been successfully edited.",
				'object_id' => $request_id,
			);
			return $result;
		} elseif(!$updated) {
			return array("error", "Testing: ".$sql." @ ".__LINE__);
		}

	}

	public function __destruct() {
		parent::__destruct();
	}


}

?>
