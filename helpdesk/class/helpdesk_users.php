<?php

class HELPDESK_USERS extends HELPDESK
{

    private $table_name = "helpdesk_users";
    private $table_field = "hdr_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "request";
    const TABLE_FLD = "user_";
    const REFTAG = "HDR";
    const LOG_TABLE = "object";

    public function __construct(){
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix().$this->id_field;
        $this->status_field = $this->getTableFieldPrefix().$this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getApproverFieldName() {
        return $this->approver_field;
    }
    public function getTableFieldPrefix() { return $this->table_field; }


    //Tshegofatso Mosidi
    public function addObject($tid, $tkn, $cc) {

        //Check if user exists in the helpdesk db
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_tkid = '.$tid . ' ';
        $sql .= 'AND helpdesk_companies.hdc_cc = \''.$cc . '\'';
        $user = $this->mysql_fetch_one($sql);

        //Does this user exist in the helpdesk db
        if(!isset($user) || !is_array($user) || is_null($user) || $user===false || count($user) == 0) {//No: Create then return user
            //Get user information from the company db

            // 1. Get information on whether this is a  client or a reseller
            $sql = 'SELECT * FROM helpdesk_companies ';
            // Company Type
            $sql .= 'INNER JOIN helpdesk_company_types ';
            $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

            $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
            $company = $this->mysql_fetch_one($sql);

            if($company['hdct_value'] == 'client'){
                $user_type = 1;//client_user
            }elseif($company['hdct_value'] == 'reseller'){
                $user_type = 4;//reseller_user
            }

            $user_id = $tid;
            $user_name = $tkn;
            $user_company = $company['hdc_id'];


            //Enter information in the helpdesk user table
            $var = array();
            $var[$this->getTableField() . 'tkid'] = $user_id;
            $var[$this->getTableField() . 'name'] = $user_name;
            $var[$this->getTableField() . 'type'] = $user_type;
            $var[$this->getTableField() . 'company'] = $user_company;

            $sql = "INSERT INTO helpdesk_users SET " . $this->convertArrayToSQL($var);
            $id = $this->db_insert($sql);
            if ($id > 0) {
                //return the user that we've just added onto the helpdesk system
                $sql = 'SELECT * FROM helpdesk_users ';
                $sql .= 'INNER JOIN helpdesk_companies ';
                $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
                $sql .= 'INNER JOIN helpdesk_user_types ';
                $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
                $sql .= 'WHERE user_id = '. $id;
                $user = $this->mysql_fetch_one($sql);

                return $user;
            }
            return array("error", "Testing: " . $sql);
        }else{//Yes: Return existing user
            return $user;
        }
    }

    function getCurrentHelpdeskUser(){
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'INNER JOIN helpdesk_table_view ';
        $sql .= 'ON helpdesk_table_view.htv_id = helpdesk_users.user_table_view ';
        $sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'] . ' ';
        $sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
        $user = $this->mysql_fetch_one($sql);
        return $user;
    }

    // Functions cloned from the helpdesk_admin module - START
    public function addAdminObject($var) {

        //Check if user exists in the helpdesk db
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_tkid = '.$var['helpdesk_user'] . ' ';
        $sql .= 'AND helpdesk_companies.hdc_cc = \''.$var['helpdesk_company'] . '\'';
        $user = $this->mysql_fetch_one($sql);

        //Does this user exist in the helpdesk db
        if((isset($user) && is_array($user) && count($user) == 0) || $user === false || $user == false) {//No: Create user and make them an admin
            //Get user information from the company db

            // 1. Get information on whether this is a  client or a reseller
            $sql = 'SELECT * FROM helpdesk_companies ';
            // Company Type
            $sql .= 'INNER JOIN helpdesk_company_types ';
            $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

            $sql .= 'WHERE helpdesk_companies.hdc_cc = \''. $var['helpdesk_company'] . '\'';
            $company = $this->mysql_fetch_one($sql);

            if($company['hdct_value'] == 'client'){
                $user_type = 2;//client_user
            }elseif($company['hdct_value'] == 'reseller'){
                $user_type = 3;//reseller_user
            }elseif($company['hdct_value'] == 'iassist'){
                $user_type = 5;//reseller_user
            }

            $sql = 'SELECT tkid, tkname, tksurname FROM assist_'.strtolower($company['hdc_cc']).'_timekeep ';
            $sql .='WHERE tkid = \''.$var['helpdesk_user'].'\'';
            $client_db = new ASSIST_MODULE_HELPER('client', $company['hdc_cc']);
            $user_details = $client_db->mysql_fetch_one($sql);

            if(count($user_details) > 0){
                $user_tkid = $var['helpdesk_user'];
                $user_name = $user_details['tkname'] . ' ' . $user_details['tksurname'];
                $user_company = $company['hdc_id'];

                //Enter information in the helpdesk user table
                unset($var);
                $var = array();
                $var[$this->getTableField() . 'tkid'] = $user_tkid;
                $var[$this->getTableField() . 'name'] = $user_name;
                $var[$this->getTableField() . 'type'] = $user_type;
                $var[$this->getTableField() . 'company'] = $user_company;

                $sql = "INSERT INTO helpdesk_users SET " . $this->convertArrayToSQL($var);
                $id = $this->db_insert($sql);
                if ($id > 0) {
                    $user_id = $id;

                    //Make them a regular admin
                    $admin_type = 1; // Regular Admin
                    $sql = 'INSERT INTO helpdesk_user_admin_types ';
                    $sql .= '(hduat_userid, hduat_admin_type_id)';
                    $sql .= 'VALUES (' . $id . ', ' . $admin_type . ')';
                    $id = $this->db_insert($sql);
                }
            }

            $event = 'created';

            //user log values
            $log_user_id = $user_id;
            $log_user_tkid = $user_tkid;
            $log_user_tkname = $user_name;
            $log_user_transaction = $user_name . ' has been created as a helpdesk user, and assigned the rank of admin user, by: ' . $_SESSION['tkn'];

        }else{//Yes: Make This user an admin user
            // 1. Get information on whether this is a  client or a reseller
            $sql = 'SELECT * FROM helpdesk_companies ';
            // Company Type
            $sql .= 'INNER JOIN helpdesk_company_types ';
            $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

            $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'] . '\'';
            $company = $this->mysql_fetch_one($sql);

            if($company['hdct_value'] == 'client'){
                $user_type = 2;//client_user
            }elseif($company['hdct_value'] == 'reseller'){
                $user_type = 3;//reseller_user
            }elseif($company['hdct_value'] == 'iassist'){
                $user_type = 5;//reseller_user
            }

            $sql = 'UPDATE helpdesk_users ';
            $sql .= 'SET user_type = ' . $user_type . ' ';
            $sql .= 'WHERE user_id = ' . $user['user_id'];
            $id = $this->db_update($sql);

            if ($id > 0) {
                //Make them a regular admin
                $admin_type = 1; // Regular Admin
                $sql = 'INSERT INTO helpdesk_user_admin_types ';
                $sql .= '(hduat_userid, hduat_admin_type_id)';
                $sql .= 'VALUES (' . $user['user_id'] . ', ' . $admin_type . ')';
                $id = $this->db_insert($sql);
            }

            $event = 'updated';

            $sql = 'SELECT tkid, tkname, tksurname FROM assist_'.strtolower($company['hdc_cc']).'_timekeep ';
            $sql .='WHERE tkid = \''.$var['helpdesk_user'].'\'';
            $client_db = new ASSIST_MODULE_HELPER('client', $company['hdc_cc']);
            $user_details = $client_db->mysql_fetch_one($sql);

            if(count($user_details) > 0){
                $user_tkid = $var['helpdesk_user'];
                $user_name = $user_details['tkname'] . ' ' . $user_details['tksurname'];
                $user_company = $company['hdc_id'];
            }


            //user log values
            $log_user_id = $user['user_id'];
            $log_user_tkid = $user_tkid;
            $log_user_tkname = $user_name;
            $log_user_transaction = $user['user_name'] . ' has been assigned the rank of admin user, by: ' . $_SESSION['tkn'];
        }


        //Update the admin management and the settings
        if($id){
            $company_obj = new HELPDESK_COMPANY();
            $id = $company_obj->updateSettings(strtolower($company['hdc_cc']));
            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['hd_user_id'] = $log_user_id;
            $log_values['date_time'] = 'NOW()';
            $log_values['tkid'] = $log_user_tkid;
            $log_values['tkname'] = $log_user_tkname;
            $log_values['user_change'] = $log_user_transaction;

            $sql = 'INSERT INTO helpdesk_users_log (hd_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['hd_user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "Admin User: " . (isset($user_name) ? $user_name : $user['user_name'] ) . " has been successfully assigned the role of admin user",
                    'object_id' => $id,
                );
                return $result;
            }
        }else{
            return array("error", "Testing: " . $sql);
        }

    }

    public function deleteAdminObject($var) {
        //This is more of a demotion than a deletion

        //Check if user is from client or reseller
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';
        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
        $sql .= 'WHERE helpdesk_users.user_id = '.$var['user_id'];

        $user = $this->mysql_fetch_one($sql);

        $helpdesk = new HELPDESK_REQUEST();

        $open_requests = $helpdesk->checkActiveRequests($user);

        if(isset($open_requests) && is_int($open_requests) && $open_requests == 0){
            if(strtolower($user['hdct_value']) == 'client'){
                $user_type = 1;//client_user
            }elseif(strtolower($user['hdct_value']) == 'reseller'){
                $user_type = 4;//reseller_user
            }elseif(strtolower($user['hdct_value']) == 'iassist'){
                $user_type = 5;//reseller_user
            }

            if($user_type == 5){
                $sql = 'DELETE FROM helpdesk_users ';
                $sql .= 'WHERE user_id = ' . $user['user_id'];
            }else{
                $sql = 'UPDATE helpdesk_users ';
                $sql .= 'SET user_type = ' . $user_type . ' ';
                $sql .= 'WHERE user_id = ' . $user['user_id'];
            }
            $id = $this->db_update($sql);

            if ($id > 0) {
                //Delete their admin user type, they don't have one anymore
                $sql = 'DELETE FROM helpdesk_user_admin_types ';
                $sql .= 'WHERE hduat_userid = ' . $user['user_id'];
                $id = $this->db_update($sql);
                $diditwork = $id;
            }

            //Update the admin management and the settings
            if($diditwork > 0){
                $company = new HELPDESK_COMPANY();
                $id = $company->updateSettings(strtolower($user['hdc_cc']));
                //user log values
                $log_user_id = $user['user_id'];
                $log_user_transaction = $user['user_name'] . ' has been successfully deleted as an admin user, by: ' . $_SESSION['tkn'];

                //Log the addition or promotion to admin user
                $log_values = array();
                $log_values['hd_user_id'] = $log_user_id;
                $log_values['date_time'] = 'NOW()';
                $log_values['user_change'] = $log_user_transaction;

                $sql = 'INSERT INTO helpdesk_users_log (hd_user_id,date_time,user_change) ';
                $sql .= 'VALUES ('.$log_values['hd_user_id'].', ';
                $sql .= 'NOW(), ';
                $sql .= '\''.$log_values['user_change'].'\')';
                $id = $this->db_insert($sql);

                if(isset($id) && is_int($id) && $id > 0){
                    $result = array(
                        0 => "ok",
                        1 => "Admin User: " . $user['user_name'] . " has been successfully deleted as an admin user",
                        'object_id' => $user['user_id'],
                    );
                    return $result;
                }
            }else{
                return array("error", "Testing: " . $sql);
            }
        }elseif(isset($open_requests) && is_int($open_requests) && $open_requests > 0){
            $result = array(
                0 => "ok",
                1 => "Admin User: " . $user['user_name'] . " cannot be deleted, because " . $user['user_name'] . " has active requests on the system",
                'object_id' => $user['user_id'],
            );
            return $result;
        }
    }
    // Functions cloned from the helpdesk_admin module - END

    //Got these ones from DTM - START
    public function grantSetupAccess($var) {
        //get the current user details, so that we can identify the person who gave this user access to setup
        $sql = 'SELECT * FROM helpdesk_users ';
        $sql .= 'WHERE helpdesk_users.user_tkid = \''.$_SESSION['tid'] . '\' ';
        $current_user_info = $this->mysql_fetch_one($sql);

        $access_insert = array();
        $access_insert['ua_user_id'] = $var['user_id'];
        $access_insert['ua_insertuser'] = $current_user_info['user_id'];

        $dtm_user_access = new HELPDESK_USERACCESS();
        $return_value = $dtm_user_access->addObject($access_insert);


        if($return_value[0] == 'ok'){
            $sql = 'SELECT * FROM helpdesk_users ';
            $sql .= 'WHERE helpdesk_users.user_id = ' . $var['user_id'] . ' ';
            $affected_user_info = $this->mysql_fetch_one($sql);

            //user log values
            $log_user_id = $affected_user_info['user_id'];
            $log_comp_change = $affected_user_info['user_name'] . ' has been successfully granted setup access in the Helpdesk system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['user_id'] = $log_user_id;
            $log_values['user_change'] = $log_comp_change;

            $sql = 'INSERT INTO helpdesk_users_log (hd_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $affected_user_info['user_name'] . " has been successfully granted setup access.",
                    'object_id' => $affected_user_info['user_id'],
                );
                return $result;
            }
        }else{
            return $return_value;
        }

    }

    public function revokeSetupAccess($var) {
        $dtm_user_access = new HELPDESK_USERACCESS();

        $return_value = $dtm_user_access->removeObject($var);

        if($return_value[0] == 'ok'){
            $sql = 'SELECT * FROM helpdesk_users ';
            $sql .= 'WHERE helpdesk_users.user_id = ' . $var['user_id'] . ' ';
            $affected_user_info = $this->mysql_fetch_one($sql);

            //user log values
            $log_user_id = $affected_user_info['user_id'];
            $log_comp_change = $affected_user_info['user_name'] . ' has had their setup access successfully revoked in the Helpdesk system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['user_id'] = $log_user_id;
            $log_values['user_change'] = $log_comp_change;

            $sql = 'INSERT INTO helpdesk_users_log (hd_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);


            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $affected_user_info['user_name'] . " no longer has setup access.",
                    'object_id' => $affected_user_info['user_id'],
                );
                return $result;
            }
        }else{
            return $return_value;
        }
    }
    //Got these ones from DTM - END

    //For situations where there's only one admin user
    public function getAdminUser($cc) {
        $sql = 'SELECT * FROM helpdesk_users ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_user_admin_types ';
        $sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

        $sql .= 'INNER JOIN helpdesk_admin_types ';
        $sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc . '\' ';
        $sql .= 'AND helpdesk_admin_types.hdat_admin_type = \'regular_admin\'';
        $user = $this->mysql_fetch_one($sql);

        return $user;
    }

    public function getAdminUserOnDuty($cc) {

    }

    public function getPrimaryAdminUser($cc) {

    }

    public function saveDefaultView($var) {
        $page_id = $var['page_id'];

        $sql = 'SELECT * FROM helpdesk_table_view ';
        $sql .= 'WHERE helpdesk_table_view.htv_id = '. $page_id . ' ';
        $view = $this->mysql_fetch_one($sql);

        $user = $this->getCurrentHelpdeskUser();

        $sql = 'UPDATE helpdesk_users ';
        $sql .= 'SET user_table_view = ' . $page_id . ' ';
        $sql .= 'WHERE user_id = ' . $user['user_id'];
        $id = $this->db_update($sql);

        //user log values
        $log_user_id = $user['user_id'];
        $log_user_transaction = $user['user_name'] . ', has successfully saved the ' . $view['htv_name'] . ' view as their default table view.';


        //Update the admin management and the settings
        if($id){
            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['hd_user_id'] = $log_user_id;
            $log_values['user_change'] = $log_user_transaction;

            $sql = 'INSERT INTO helpdesk_users_log (hd_user_id,date_time,user_change) ';
            $sql .= 'VALUES ('.$log_values['hd_user_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['user_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => " You have successfully saved the " . $view['htv_name'] . " view as your default table view.",
                    'object_id' => $id,
                );
                return $result;
            }else{
                return array("error", "Testing: " . $sql);
            }
        }else{
            return array("error", "Testing: " . $sql);
        }

    }

    public function isCurrentUserAnAdminUser($cc, $tkid){
        $sql = 'SELECT * FROM helpdesk_users ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_user_admin_types ';
        $sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

        $sql .= 'INNER JOIN helpdesk_admin_types ';
        $sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc . '\' ';
        $sql .= 'AND helpdesk_users.user_tkid = \''. $tkid . '\' ';
        $user = $this->mysql_fetch_one($sql);

        $isCurrentUserAnAdminUser = false;
        if(!((isset($user) && is_array($user) && count($user) == 0) || $user === false || $user == false)) {
            $isCurrentUserAnAdminUser = true;
        }

        return $isCurrentUserAnAdminUser;
    }

	public function getCompanyUsers($cc){
		//[SD] AA-645 Helpdesk Reporting - 12/07/21
		$sql = 'SELECT * FROM helpdesk_companies ';

		//Admin users
		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

		// Clauses
		$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$cc . '\'';
		//$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'] . ') ';

		$raw_users = $this->mysql_fetch_all($sql);;

		$users_formatted_for_select = array();
		foreach($raw_users as $key => $val){
			$users_formatted_for_select[$val['user_id']] = $val['user_name']  ;
		}

		return $users_formatted_for_select;


	}


	public function getUsersWithRequests(){
    	//[SD] AA-645 Helpdesk Reporting - 12/07/21
    	//get all users
		$sql = 'SELECT * FROM helpdesk_users ';

		$sql .= 'INNER JOIN helpdesk_companies ';
		$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

		$sql .= 'INNER JOIN helpdesk_request ';
		$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

		$sql .= 'WHERE helpdesk_companies.hdc_type <> 3 ';

		$sql .= 'ORDER BY helpdesk_companies.hdc_name ASC ';
		$raw_users = $this->mysql_fetch_all($sql);

		$users_formatted_for_select = array();
		foreach($raw_users as $key => $val){
			$users_formatted_for_select[$val['user_id']] = $val['user_name'] ." (".$val['hdc_name'].")" ;
		}

		return $users_formatted_for_select;
	}


}