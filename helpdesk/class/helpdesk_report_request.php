<?php

class HELPDESK_REPORT_REQUEST extends HELPDESK_REPORT {

	//CONSTRUCT
	private $date_format = "DATETIME";
	private $my_class_name = "HELPDESK_REPORT_REQUEST";
	private $my_quick_class_name = "HELPDESK_QUICK_REPORT";
	
	protected $me;
	protected $s=1;
	
	protected $object_type = "DEPTKPI";
	protected $table_name = "kpi";
	protected $id_field = "kpi_id";
	protected $name_field = "kpi_name";
	protected $reftag = "D";
	protected $deadline_field;
	protected $date_completed_field;
	protected $action_date_completed_field;
	

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Departmental KPI Report";
	
	
	


	public function __construct($p) {	
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		$this->me = new HELPDESK_REQUEST();
		$this->object_type = $this->me->getMyObjectType();
		$this->table_name = $this->me->getTableName();
		$this->reftag = $this->me->getRefTag();
		$this->deadline_field = false;//$this->me->getDeadlineFieldName();
		$this->date_completed_field = $this->me->getDateCompletedFieldName();
//		$this->name_field = $this->me->getNameFieldName();
		//$this->default_report_title = $this->me->replaceAllNames("|".$this->me->getMyObjectName()."|")." Report";
		$this->default_report_title = "Helpdesk Report";
		if($p=="fixed") {
			$this->titles = array('financial_year'=>"Financial Year");
			$this->allowfilter['financial_year']=true;
		}
//		$resultSetupObject = new SDBP6_SETUP_RESULTS();
//		$temp_result_categories = $resultSetupObject->getResultOptions();
//		$this->result_categories = array();
//		foreach($temp_result_categories as $i => $rc) {
//			$key = $rc['code'];
//			$this->result_categories[$key] = array(
//				'text'=>$rc['value'],
//				'code'=>$rc['code'],
//				'color'=>$rc['color']
//			);
//		}
//		ASSIST_HELPER::arrPrint($this->result_categories);
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}


	protected function getFieldDetails() {
		$this->allowchoose = array();
		$this->default_selected = array();
		$this->allowfilter = array();
		$this->types = array(
			'result'				=> "RESULT",
		);
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array();
		$this->allowsortby = array(
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array();
		$this->s = 1;

        //Fields that I'ma show are
        $fields = array();

        $db_field = 'ref';
        $fields[$db_field]['field_name'] = 'Ref';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;


        $db_field = 'owner';
        $fields[$db_field]['field_name'] = 'Owner';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'date';
        $fields[$db_field]['field_name'] = 'Launch Date';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'topic';
        $fields[$db_field]['field_name'] = 'Topic';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field= 'module';
        $fields[$db_field]['field_name'] = 'Module';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field= 'launched_on_behalf_of';
        $fields[$db_field]['field_name'] = 'On Behalf Of';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'description';
        $fields[$db_field]['field_name'] = 'Description';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'status';
        $fields[$db_field]['field_name'] = 'Status';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'assigned_user';
        $fields[$db_field]['field_name'] = 'Assigned User';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'attachment';
        $fields[$db_field]['field_name'] = 'Attachments';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        /**************************************************************************************************************/

        $db_field = 'acknowledgement_date';
        $fields[$db_field]['field_name'] = 'Acknowledgement Date';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'acknowledgement_duration';
        $fields[$db_field]['field_name'] = 'Acknowledgement Duration';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'resolution_date';
        $fields[$db_field]['field_name'] = 'Resolution Date';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        $db_field = 'resolution_duration';
        $fields[$db_field]['field_name'] = 'Resolution Duration';
        $fields[$db_field]['field_type'] = 'TEXT';
        $fields[$db_field]['allowchoose'] = true;
        $fields[$db_field]['allowfilter'] = false;
        $fields[$db_field]['sortposition'] = $this->s; $this->s++;
        $fields[$db_field]['allowsortby'] = false;
        $fields[$db_field]['allowgroupby'] = false;

        /**************************************************************************************************************/

		foreach($fields as $field => $field_info) {
			$fld = $field;
            $items = array();
            $this->data[$fld] = $items;

            $this->titles[$fld] = $field_info['field_name'];
            $this->types[$fld] = $field_info['field_type'];

            $this->allowfilter[$fld] = $field_info['allowfilter'];
            $this->allowchoose[$fld] = $field_info['allowchoose'];
            $this->sortposition[$fld] = $field_info['sortposition'];
            $this->allowsortby[$fld] = $field_info['allowsortby'];
            $this->allowgroupby[$fld] = $field_info['allowgroupby'];
		}

//		echo '<pre style="font-size: 18px">';
//		echo '<p>TITLES</p>';
//		print_r($this->titles);
//		echo '</pre>';
//
//		echo '<pre style="font-size: 18px">';
//		echo '<p>DATA</p>';
//		print_r($this->data);
//		echo '</pre>';

        /******************************************FILTERS*************************************************************/

		//[SD] AA-645 Helpdesk Reporting - 12/07/21 - Add filter by assigned user
		//Assigned user
		$field_name = 'assigned_user';
		$title_text = 'Assigned user';
		$this->titles[$field_name] = $title_text;
		$this->types[$field_name] = "LIST";
		$this->allowchoose[$field_name] = false;
		$this->allowfilter[$field_name] = true;
		$this->allowgroupby[$field_name] = false;
		$this->allowsortby[$field_name] = false;

		$hdc_object = new HELPDESK_USERS();

		$items = $hdc_object->getCompanyUsers($this->cmp_code);
		$this->data[$field_name] = $items;


        //Company
        $field_name = 'company_code';
        $title_text = 'Company';
        $this->titles[$field_name] = $title_text;
        $this->types[$field_name] = "LIST";
        $this->allowchoose[$field_name] = false;
        $this->allowfilter[$field_name] = true;
        $this->allowgroupby[$field_name] = false;
        $this->allowsortby[$field_name] = false;

        $hdc_object = new HELPDESK_COMPANY();

        $items = $hdc_object->getAllClientCompaniesFormattedForSelect();
        $this->data[$field_name] = $items;


		//[SD] AA-645 Helpdesk Reporting - 13/07/21 - add filter by DATE
		//Owner
		$field_name = 'date_filter';
		$title_text = 'Date logged';
		$this->titles[$field_name] = $title_text;
		$this->types[$field_name] = "DATE";
		$this->allowchoose[$field_name] = false;
		$this->allowfilter[$field_name] = true;
		$this->allowgroupby[$field_name] = false;
		$this->allowsortby[$field_name] = false;


		//[SD] AA-645 Helpdesk Reporting - 12/07/21 - add filter by owner
		//Owner
		$field_name = 'owner_filter';
		$title_text = 'Owner';
		$this->titles[$field_name] = $title_text;
		$this->types[$field_name] = "LIST";
		$this->allowchoose[$field_name] = false;
		$this->allowfilter[$field_name] = true;
		$this->allowgroupby[$field_name] = false;
		$this->allowsortby[$field_name] = false;

		$hdc_object = new HELPDESK_USERS();

		$items = $hdc_object->getUsersWithRequests();
		$this->data[$field_name] = $items;


        //Status
        $field_name = 'status_filter';
        $title_text = 'Status';
        $this->titles[$field_name] = $title_text;
        $this->types[$field_name] = "LIST";
        $this->allowchoose[$field_name] = false;
        $this->allowfilter[$field_name] = true;
        $this->allowgroupby[$field_name] = false;
        $this->allowsortby[$field_name] = false;

        $items = array();
        $items['all'] = 'All';
        $items['open_and_in_progress'] = 'Open/In Progress';
        $items['completed'] = 'Completed';
        $this->data[$field_name] = $items;

	}
	
	private function processHeadings($headObject,$head,$headings) {
				$fld = $head['field'];
				$this->titles[$fld] = ($head['parent_id']>0?$headings[$head['parent_id']]['name']." - ":"").$head['name'];
				if($headObject->isListField($head['type'])) {
					$this->types[$fld] = "LIST";
				} elseif($headObject->isTextField($head['type'])) {
					$this->types[$fld] = "TEXT";
				} else {
					$this->types[$fld] = $head['type'];
				}
				$this->allowfilter[$fld] = !in_array($head['type'],$this->bad_filter_types);
				$this->allowchoose[$fld] = true;
				$this->sortposition[$fld] = $this->s; $this->s++;
				$this->allowsortby[$fld] = ($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types));
				if(!in_array($head['type'],$this->bad_graph_types) && (($head['parent_id']==0) && (!in_array($head['type'],$this->bad_sort_types)))) {
					$this->allowgroupby[$fld] = true;
				} else {
					$this->allowgroupby[$fld] = false;
				}
	}
	
	protected function getFieldData() {
		$this->data['result'] = $this->getResultOptions();
	}

	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();

        $report_title = $this->default_report_title;

		$this->report->setReportTitle($report_title);
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
//		foreach($this->result_categories as $key=>$r) {
//            $this->report->setResultCategoryWithDescription($key,$r['text'],$r['color'], $r['description']);
//		}
		$this->report->setRows($rows);

//		$this->report->setResultRows($results_rows);
//		$this->report->setOverallResultRows($overall_result_rows);
//		$this->report->setChangeLogRows($change_log_rows);

		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings($this->result_fields['bottom']);
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(strtolower($this->getMyObjectType()));
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r[$this->me->getIDFieldName()];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	

	
	private function getRows() {
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);

		$id_field = $this->me->getIDFieldName();
		if(strlen($sql)>0) {
			$rows = $this->me->mysql_fetch_all_fld($sql,$id_field);

            $sql = 'SELECT * FROM helpdesk_list_user_status ';
            $status = $this->me->mysql_fetch_all_by_id($sql, 'hlus_id');

            $filters = $_REQUEST['filter'];

            $company_code = '';
            foreach($filters['company_code'] as $key => $cc){
                if($cc != "X"){
                    $company_code = $cc;
                }
            }

            $hdc_object = new HELPDESK_COMPANY();
            $all_active_companies = $hdc_object->getAllCompaniesRAW();

            $sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_request_company.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

            $sql .= 'INNER JOIN helpdesk_list_topic ';
            $sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

            $sql .= 'INNER JOIN helpdesk_list_module ';
            $sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

            $sql .= 'INNER JOIN helpdesk_users ';
            $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

            $sql .= 'INNER JOIN helpdesk_companies ';
            $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

            $sql .= 'INNER JOIN helpdesk_request_company ';
            $sql .= 'ON helpdesk_request_company.hdrc_hdr_id =helpdesk_request.hdr_id ';

            $sql .= 'INNER JOIN helpdesk_user_types ';
            $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'INNER JOIN helpdesk_request_users ';
			$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

            $sql .= 'WHERE helpdesk_companies.hdc_type = 2 ';

            if(strlen($company_code) > 0){
                $company_information = $hdc_object->getCompanyInformation($company_code);
                $sql .= 'AND helpdesk_request_company.hdrc_company_id = ' . $company_information['hdc_id'] . ' ';
            }

			if (count($filters['assigned_user']) > 0) {//[SD] AA-645 Helpdesk Reporting - 12/07/21 - add filter by assigned user
				$user_id = implode(',',$filters['assigned_user']);
				if($user_id != "X") {
					$sql .= 'AND helpdesk_request_users.hdru_userid IN (' . $user_id . ') ';
				}
			}

			if (count($filters['owner_filter']) > 0) {//[SD] AA-645 Helpdesk Reporting - 12/07/21 - add filter by owner
				$owner_id = implode(',',$filters['owner_filter']);
				if($owner_id != "X") {
					$sql .= 'AND helpdesk_request.hdr_add_userid IN (' . $owner_id . ') ';
				}
			}

			if (count($filters['date_filter']) > 0) {//[SD] AA-645 Helpdesk Reporting - 13/07/21 - add filter by date logged
				if(!empty($filters['date_filter']['from']) && !empty($filters['date_filter']['to'])) {
					$sql .= 'AND helpdesk_request.hdr_add_date BETWEEN  "' . date("Y-m-d H:i:s",strtotime($filters['date_filter']['from'])) . '" AND "'.date("Y-m-d H:i:s",strtotime($filters['date_filter']['to'])).'" ';
				}
			}

            $hdrs_launched_on_behalf_of = $this->me->mysql_fetch_all_fld($sql,$id_field);

            if(isset($hdrs_launched_on_behalf_of) && is_array($hdrs_launched_on_behalf_of) && count($hdrs_launched_on_behalf_of) > 0){
                foreach($hdrs_launched_on_behalf_of as $hdr_id => $hdr_data){
                    $rows[$hdr_id] =  $hdr_data;
                }
                ksort($rows);
            }

            $status_filter = '';
            foreach($filters['status_filter'] as $key => $filtered_status){
                if($filtered_status != "X" && $filtered_status != "all"){
                    $status_filter = $filtered_status;
                }
            }

            foreach($rows as $key => $r) {

//                $the_id_i_want_printed = 153;
//                if((int)$key == $the_id_i_want_printed){
//                    echo '<pre style="font-size: 18px">';
//                    echo '<p>Row: ' . $the_id_i_want_printed . '</p>';
//                    print_r($r);
//                    echo '</pre>';
//                }

                $sql = 'SELECT * FROM helpdesk_request ';

                $sql .= 'INNER JOIN helpdesk_request_users ';
                $sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

                $sql .= 'INNER JOIN helpdesk_users ';
                $sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

                $sql .= 'INNER JOIN helpdesk_companies ';
                $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

                $sql .= 'INNER JOIN helpdesk_list_user_roles ';
                $sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';


                $sql .= 'WHERE  helpdesk_request_users.hdru_hdrid = ' . $r['hdr_id'] . ' ';

                $involved_users = $this->me->mysql_fetch_all_by_id($sql, 'hdc_type');

                if(isset($involved_users[3])){
                    $assigned_user_index = 3;
                }elseif(isset($involved_users[2])){
                    $assigned_user_index = 2;
                }elseif(isset($involved_users[1])){
                    $assigned_user_index = 1;
                }else{
                    $assigned_user_index = 0;
                }

                $acknowledgement_user_index = 0;
                if(isset($involved_users[2]) && strtolower($involved_users[2]['hdc_cc']) == 'ait0001'){
                    $acknowledgement_user_index = 2;
                }

                if((isset($involved_users[2]) && strtolower($involved_users[2]['hdc_cc']) != 'ait0001') && isset($involved_users[3])){
                    $acknowledgement_user_index = 3;
                }

                $start_date_time_to_use = $r['hdr_add_date'];
                if(isset($involved_users[2]) && strtolower($involved_users[2]['hdc_cc']) != 'ait0001' && (int)$involved_users[2]['hdru_status'] == 9){
                    $sql = 'SELECT * FROM helpdesk_request_updates ';

                    $sql .= 'INNER JOIN helpdesk_users ';
                    $sql .= 'ON helpdesk_users.user_id = helpdesk_request_updates.hdrup_userid ';

                    $sql .= 'INNER JOIN helpdesk_companies ';
                    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

                    $sql .= 'INNER JOIN helpdesk_user_types ';
                    $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

                    $sql .= 'WHERE  helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                    $sql .= 'AND helpdesk_request_updates.hdrup_userid = ' . $involved_users[2]['user_id'] . ' ';
                    $sql .= 'AND  helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                    $sql .= 'AND helpdesk_request_updates.hdrup_user_changes LIKE \'%escalated%\' ';

                    $escalation_update = $this->me->mysql_fetch_one($sql);
                    $start_date_time_to_use = $escalation_update['hdrup_datetime'];

                    if(!isset($involved_users[3])){
                        $assigned_user_index = 0;
                    }
                }

//                echo '<pre style="font-size: 18px">';
//                echo '<p>$involved_users</p>';
//                print_r($involved_users);
//                echo '</pre>';

                if($assigned_user_index != 0){
                    $status_to_use = $involved_users[$assigned_user_index]['hdru_status'];

                    $sql = 'SELECT * FROM helpdesk_request_updates ';
                    $sql .= 'WHERE helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                    $sql .= 'AND helpdesk_request_updates.hdrup_userid = ' . $involved_users[($acknowledgement_user_index == 0 ? $assigned_user_index : $acknowledgement_user_index )]['user_id'] . ' ';
                    $sql .= 'AND (helpdesk_request_updates.hdrup_system_changes LIKE \'%New to%\' OR helpdesk_request_updates.hdrup_system_changes LIKE \'%Claim%\') ';
                    $sql .= 'ORDER BY helpdesk_request_updates.hdrup_datetime ASC ';

                    $acknowledgement = $this->me->mysql_fetch_one($sql);

                    /****************          ***************************/
                    /*
                     * Tshegofatso Mosidi - 26 November 2020 - AA-514
                     * This is a hack for those HDRs that have been handled by another admin in the same company as the current assigned
                     * user that is no longer involved in the HDR as a result of a transfer or a snatch
                     * */
                    //

                    if($acknowledgement===false){//$acknowledgement has nothing in it

                        $sql = 'SELECT * FROM helpdesk_users ';
                        $sql .= 'WHERE helpdesk_users.user_company = ' . $involved_users[$assigned_user_index]['user_company'] . ' ';
                        $sql .= 'AND helpdesk_users.user_type = ' . $involved_users[$assigned_user_index]['user_type'] . ' ';
                        $all_the_other_admins = $this->me->mysql_fetch_all_by_id($sql, 'user_id');

                        $admin_id_array = array();
                        foreach($all_the_other_admins as $id => $user_record){
                            $admin_id_array[$id] = $id;
                        }

                        $sql = 'SELECT * FROM helpdesk_request_updates ';
                        $sql .= 'WHERE helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                        $sql .= 'AND helpdesk_request_updates.hdrup_userid IN ('.implode(', ', $admin_id_array) . ') ';
                        $sql .= 'AND (helpdesk_request_updates.hdrup_system_changes LIKE \'%New to%\' OR helpdesk_request_updates.hdrup_system_changes LIKE \'%Claim%\') ';
                        $sql .= 'ORDER BY helpdesk_request_updates.hdrup_datetime ASC ';

                        $acknowledgement = $this->me->mysql_fetch_one($sql);

                        /****************          ***************************/
                        /*
                         * Tshegofatso Mosidi - 26 November 2020 - AA-514
                         * This is a hack within a hack for those HDRs that have been handled by another admin from another company of the same level [WHY!!!]
                         * */
                        //
                        if($acknowledgement===false){
                            $sql = 'SELECT * FROM helpdesk_request_updates ';
                            $sql .= 'WHERE helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                            $sql .= 'AND (helpdesk_request_updates.hdrup_system_changes LIKE \'%New to%\' OR helpdesk_request_updates.hdrup_system_changes LIKE \'%Claim%\') ';
                            $evey_acknowledgement_update = $this->me->mysql_fetch_all($sql);

                            $every_user_who_claimed_this_hdr_by_id = array();
                            foreach($evey_acknowledgement_update as $the_key => $values){
                                $every_user_who_claimed_this_hdr_by_id[$values['hdrup_userid']] = $values['hdrup_userid'];
                            }

                            if(count($every_user_who_claimed_this_hdr_by_id) > 0){
                                $sql = 'SELECT * FROM helpdesk_users ';

                                $sql .= 'INNER JOIN helpdesk_companies ';
                                $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

                                $sql .= 'WHERE helpdesk_users.user_id IN ('.implode(', ', $every_user_who_claimed_this_hdr_by_id) . ') ';

                                $every_user_who_claimed_this_hdr = $this->me->mysql_fetch_all_by_id($sql, 'user_id');

                                $acknowledgement_user_from_lateral_company = array();
                                foreach($every_user_who_claimed_this_hdr as $uid => $user_info){
                                    if($user_info['hdc_type'] == $involved_users[$assigned_user_index]['hdc_type']){
                                        $acknowledgement_user_from_lateral_company = $user_info;
                                        continue;
                                    }
                                }

                                if(count($acknowledgement_user_from_lateral_company) > 0){
                                    $sql = 'SELECT * FROM helpdesk_request_updates ';
                                    $sql .= 'WHERE helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                                    $sql .= 'AND helpdesk_request_updates.hdrup_userid = ' . $acknowledgement_user_from_lateral_company['user_id'] . ' ';
                                    $sql .= 'AND (helpdesk_request_updates.hdrup_system_changes LIKE \'%New to%\' OR helpdesk_request_updates.hdrup_system_changes LIKE \'%Claim%\') ';
                                    $sql .= 'ORDER BY helpdesk_request_updates.hdrup_datetime ASC ';
                                    $acknowledgement = $this->me->mysql_fetch_one($sql);
                                }
                            }
                        }
                    }

                    $sql = 'SELECT * FROM helpdesk_request_updates ';

                    $sql .= 'INNER JOIN helpdesk_users ';
                    $sql .= 'ON helpdesk_users.user_id = helpdesk_request_updates.hdrup_userid ';

                    $sql .= 'INNER JOIN helpdesk_companies ';
                    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

                    $sql .= 'INNER JOIN helpdesk_user_types ';
                    $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

                    $sql .= 'WHERE  helpdesk_request_updates.hdrup_hdrid = ' . $r['hdr_id'] . ' ';
                    $sql .= 'AND helpdesk_request_updates.hdrup_userid = ' . $involved_users[$assigned_user_index]['user_id'] . ' ';
                    $sql .= 'AND helpdesk_request_updates.hdrup_user_changes LIKE \'%Resolved%\' ';
                    $sql .= 'ORDER BY helpdesk_request_updates.hdrup_datetime DESC ';

                    $resolutions = $this->me->mysql_fetch_all($sql);

                    //Now for the durations in between
                    //$acknowledgement_duration
                    $acknowledgement_duration = 'N/A';
                    if(count($acknowledgement) > 0){
                        $start_date_time = $start_date_time_to_use;
                        $end_date_time = $acknowledgement['hdrup_datetime'];

                        $acknowledgement_duration = $this->getDifferenceBetweenDateTimesInHoursAndMinutes($start_date_time, $end_date_time);
                    }

                    //$resolutions_duration
                    $resolutions_duration = 'N/A';
                    if(count($acknowledgement) > 0 && count($resolutions) > 0){
                        $start_date_time = $acknowledgement['hdrup_datetime'];
                        $end_date_time = $resolutions[0]['hdrup_datetime'];

                        $resolutions_duration = $this->getDifferenceBetweenDateTimesInHoursAndMinutes($start_date_time, $end_date_time);
                    }

                }else{
                    $status_to_use = $r['hdr_add_status'];

                    $acknowledgement = array();
                    $acknowledgement_duration = 'N/A';
                    $resolutions = array();
                    $resolutions_duration = 'N/A';
                }

//                echo '<pre style="font-size: 18px">';
//                echo '<p>$resolutions</p>';
//                print_r($resolutions);
//                echo '</pre>';


                $db_field = 'ref';
                $final_rows[$key][$db_field] =  strtoupper($r['hdc_cc']). '/' . $this->me->getRefTag(). $r['hdr_id'];

                $db_field = 'owner';
                $final_rows[$key][$db_field] =  $r['user_name'] . ' (' .$r['hdc_name'] .  ')';

                $db_field = 'date';
                $final_rows[$key][$db_field] =  $r['hdr_add_date'];

                $db_field = 'topic';
                $final_rows[$key][$db_field] =  $r['topic_value'];

                $db_field= 'module';
                $final_rows[$key][$db_field] =  $r['module_value'];

                $db_field= 'launched_on_behalf_of';
                $final_rows[$key][$db_field] =  (isset($r['hdrc_company_id']) ? $all_active_companies[$r['hdrc_company_id']]['hdc_name'] . ' (' . strtoupper($all_active_companies[$r['hdrc_company_id']]['hdc_cc']) . ') ' : 'N/A' );

                $db_field = 'description';
                $hdr_description = $r['hdr_description'];
                $hdr_description = str_replace(chr(10),"<br/>", $hdr_description);
                $final_rows[$key][$db_field] =  $hdr_description;

                $db_field = 'status';
                $final_rows[$key][$db_field] =  $status[$status_to_use]['hlus_value'];

                $db_field = 'assigned_user';
                $final_rows[$key][$db_field] =  ($assigned_user_index != 0 ? $involved_users[$assigned_user_index]['user_name'] . ' (' .$involved_users[$assigned_user_index]['hdc_name'] .  ')' : 'N/A' );

                //DEAL WITH THIS LATER
                $db_field = 'attachment';
                $attachment = $r['hdr_attachment'];
                if(is_string($attachment) && strlen($attachment) > 0){
                    $f = unserialize($attachment);
                    $d = "";
                    if(isset($f) && is_array($f) && count($f) > 0){
                        foreach($f as $att_key => $val){
                            $d .= "+" . $val['original_filename'] . "\n";
                        }
                    }
                }else{
                    $d = "";
                }
                $final_rows[$key][$db_field] = $d;

                /**************************************************************************************************************/

                $db_field = 'acknowledgement_date';
                $final_rows[$key][$db_field] =  (count($acknowledgement) > 0 ? $acknowledgement['hdrup_datetime'] : 'N/A' );

                $db_field = 'acknowledgement_duration';
                $final_rows[$key][$db_field] =  $acknowledgement_duration;//launch date to $acknowledgement date

                $db_field = 'resolution_date';
                $final_rows[$key][$db_field] =  (count($resolutions) > 0 ? $resolutions[0]['hdrup_datetime'] : 'N/A' );

                $db_field = 'resolution_duration';
                $final_rows[$key][$db_field] =  $resolutions_duration;//$acknowledgement date to $resolutions_duration

                $allocateToAGroup = true;
                if(strlen($status_filter) > 0){
                    //"Done" SIMPLE STATUS
                    $simple_status[4]['status_name'] = 'Done';
                    $simple_status[4]['status_collection'][1] = $status[5];
                    $simple_status[4]['status_collection'][2] = $status[6];
                    $simple_status[4]['status_collection'][3] = $status[7];
                    $simple_status[4]['status_collection'][4] = $status[8];

                    $status_ids = array();
                    foreach($simple_status[4]['status_collection'] as $skey => $sval) {
                        $status_ids[$sval['hlus_id']] = $sval['hlus_id'];
                    }

                    //$items['open_and_in_progress'] = 'Open/In Progress';
                    //$items['completed'] = 'Completed';

                    if($status_filter == 'open_and_in_progress'){
                        if(array_key_exists($status_to_use, $status_ids)){
                            unset($final_rows[$key]);
                            $allocateToAGroup = false;
                        }
                    }else{
                        if(!array_key_exists($status_to_use, $status_ids)){
                            unset($final_rows[$key]);
                            $allocateToAGroup = false;
                        }
                    }
                }

                if($allocateToAGroup === true){
                    $this->allocateToAGroup($r);
                }
            }
		}

//		echo '<pre style="font-size: 18px">';
//		echo '<p>FINAL ROWS</p>';
//		print_r($final_rows);
//		echo '</pre>';

		return $final_rows;
	}

    private function getDifferenceBetweenDateTimesInHoursAndMinutes($start_date_time, $end_date_time){
        //set business start and end hours
        $businessStartHour = 8; //8 AM
        $businessEndHour = 17; //5 PM

        //set weekend days
        $arrWeekendDays = array(6,0); //numeric representations of Saturday (6) and Sunday (0)

        //set start and end dates and times
        //2-11-2018 3 PM
        $start_date_time_object = new DateTime($start_date_time);

//        $date = $start_date_time_object->format('m/d/Y');
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>$date</p>';
//        print_r($date);
//        echo '</pre>';
//
//        $time = $start_date_time_object->format('H:i:s');
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>$time</p>';
//        print_r($time);
//        echo '</pre>';
        
        $startHour = $start_date_time_object->format('H');
        $startMinute = $start_date_time_object->format('i');
        $startSecond = $start_date_time_object->format('s');
        $startMonth = $start_date_time_object->format('m');
        $startDay = $start_date_time_object->format('d');
        $startYear = $start_date_time_object->format('Y');

        //5-11-2018 11 AM
        $end_date_time_object = new DateTime($end_date_time);
        $endHour = $end_date_time_object->format('H');
        $endMinute = $end_date_time_object->format('i');
        $endSecond = $end_date_time_object->format('s');
        $endMonth = $end_date_time_object->format('m');
        $endDay = $end_date_time_object->format('d');
        $endYear = $end_date_time_object->format('Y');

        //create UNIX timestamps
        $startTime = mktime($startHour, $startMinute, $startSecond, $startMonth, $startDay, $startYear);
        $endTime = mktime($endHour, $endMinute, $endSecond, $endMonth, $endDay, $endYear);

        //ensure $endTime is greater than $startTime
        if($startTime >= $endTime){
            //invalid start and end datetimes
            die("Invalid start and end datetimes.");
        }

        //calculate eligible seconds from partial time on first and last day
        $totalSeconds = 0;

        $currentTime = mktime(0, 0, 0, $startMonth, $startDay, $startYear); //beginning of $startTime day
        $lastFullDay = mktime(0, 0, 0, $endMonth, $endDay, $endYear); //beginning of $endTime day

        $startingBusinessTime = mktime($businessStartHour, 0, 0, $startMonth, $startDay, $startYear);
        $endingBusinessTime = mktime($businessEndHour, 0, 0, $endMonth, $endDay, $endYear);

        if($startTime < $startingBusinessTime){
            $startTime = $startingBusinessTime;
        }
        if($endTime > $endingBusinessTime){
            $endTime = $endingBusinessTime;
        }

        if($currentTime == $lastFullDay){
            //$startTime and $endTime occur on the same day
            if($endTime > $startTime){
                $totalSeconds += ($endTime - $startTime);
            }
        }else{
            //$startTime and $endTime do not occur on the same day
            $startingBusinessTime = mktime($businessStartHour, 0, 0, $endMonth, $endDay, $endYear);
            $endingBusinessTime = mktime($businessEndHour, 0, 0, $startMonth, $startDay, $startYear);
            if($endingBusinessTime > $startTime){
                $totalSeconds += ($endingBusinessTime - $startTime);
            }
            if($endTime > $startingBusinessTime){
                $totalSeconds += ($endTime - $startingBusinessTime);
            }
        }

        //calculate eligible seconds from all full days in between start day and end day
        $fullDayBusinessSeconds = (($businessEndHour - $businessStartHour) * 3600);

        //set $currentTime to beginning of first full day
        $nextDay = $currentTime + (26 * 3600); //add 26 hours to $currentTime to get into the next day, compensating for possible daylight savings
        $currentTime = mktime(0, 0, 0, date('n', $nextDay), date('j', $nextDay), date('Y', $nextDay));

        while($currentTime < $lastFullDay){
            //determine if $currentTime is a weekday
            if(!in_array(date('w', $currentTime), $arrWeekendDays)){
                //it's a business day, add all business seconds to $totalSeconds
                $totalSeconds += $fullDayBusinessSeconds;
            }
            //increment $currentTime to beginning of next day
            $nextDay = $currentTime + (26 * 3600); //add 26 hours to $currentTime to get into the next day, compensating for possible daylight savings
            $currentTime = mktime(0, 0, 0, date('n', $nextDay), date('j', $nextDay), date('Y', $nextDay));
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>SHOW DATE CONVERSION</p>';
//        echo '<p>';
//        echo "Total eligible time between start time and end time: " . $totalSeconds . " seconds (" . $this->convertSecToTime($totalSeconds) . ")";
//        echo '</p>';
//        echo '</pre>';

        $interval = $this->convertSecToTime($totalSeconds);

        $intervalHour = $interval->format('%H');
        $intervalMinute = $interval->format('%i');
        $intervalSecond = $interval->format('%s');
        $intervalMonth = $interval->format('%m');
        $intervalDays = $interval->format('%a');

//        echo '<pre style="font-size: 18px">';
//        echo '<p>$intervalDays</p>';
//        print_r($intervalDays);
//        echo '<p>$intervalHour</p>';
//        print_r($intervalHour);
//        echo '<p>$intervalMinute</p>';
//        print_r($intervalMinute);
//        echo '<p>$intervalSecond</p>';
//        print_r($intervalSecond);
//        echo '</pre>';

        $days_in_hours = $intervalDays * 24;
        $minutes_in_hours = $intervalMinute / 60;
        $seconds_in_hours = $intervalSecond / 60 / 60;

        $total_hours = $days_in_hours + $intervalHour + $minutes_in_hours + $seconds_in_hours;

        $formatted_total_hours = number_format((float)$total_hours, 2, '.', '');

        return $formatted_total_hours . ' hours';
        //return $interval->format('%a days, %h hours, %i minutes and %s seconds');
    }

    private function convertSecToTime($sec){
        $date1 = new DateTime("@0");
        $date2 = new DateTime("@$sec");
        $interval =  date_diff($date1, $date2);

        // convert into Days, Hours, Minutes
        //return $interval->format('%a days, %h hours, %i minutes and %s seconds');

        return $interval;
    }
	
	
	protected function setSQL($db,$filter=array()) {
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
//        $group_by = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";

//        echo '<pre style="font-size: 18px">';
//        echo '<p>$filters</p>';
//        print_r($filters);
//        echo '</pre>';

        $company_code = '';
        foreach($filters['company_code'] as $key => $cc){
            if($cc != "X"){
                $company_code = $cc;
            }
        }

        $sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

        $sql .= 'INNER JOIN helpdesk_list_topic ';
        $sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

        $sql .= 'INNER JOIN helpdesk_list_module ';
        $sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

        $sql .= 'INNER JOIN helpdesk_companies ';
        $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'INNER JOIN helpdesk_request_users ';
		$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_type = 1 ';

        if(strlen($company_code) > 0){
            $sql .= 'AND helpdesk_companies.hdc_cc = \'' . $company_code . '\' ';
        }

		if (count($filters['assigned_user']) > 0) {//[SD] AA-645 Helpdesk Reporting - 12/07/21 - add filter by assigned user
			$user_id = implode(',',$filters['assigned_user']);
			if($user_id != "X") {
				$sql .= 'AND helpdesk_request_users.hdru_userid IN (' . $user_id . ') ';
			}
		}

		if (count($filters['owner_filter']) > 0) {//[SD] AA-645 Helpdesk Reporting - 12/07/21 - add filter by owner
			$owner_id = implode(',',$filters['owner_filter']);
			if($owner_id != "X") {
				$sql .= 'AND helpdesk_request.hdr_add_userid IN (' . $owner_id . ') ';
			}
		}

		if (count($filters['date_filter']) > 0) {//[SD] AA-645 Helpdesk Reporting - 13/07/21 - add filter by date logged
			if(!empty($filters['date_filter']['from']) && !empty($filters['date_filter']['to'])) {
				$sql .= 'AND helpdesk_request.hdr_add_date BETWEEN  "' . date("Y-m-d H:i:s",strtotime($filters['date_filter']['from'])) . '" AND "'.date("Y-m-d H:i:s",strtotime($filters['date_filter']['to'])).'" ';
			}
		}

//        echo '<pre style="font-size: 18px">';
//        echo '<p>$sql</p>';
//        print_r($sql);
//        echo '</pre>';

		return $sql;
	}
	

}

?>