<?php

/**
 *
 *
 * Created on: 2 July 2016
 * Authors: Janet Currie
 *
 */
class HELPDESK_COMPANY extends HELPDESK
{


    private $table_name = "helpdesk_companies";
    private $table_field = "hdc_";
    protected $id_field = "id";
    protected $status_field = "system_status";


    private $system_log = array();
    private $user_changes;
    private $emails;


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "OBJECT";
    const TABLE = "companies";
    const TABLE_FLD = "hdc_";
    const REFTAG = "HDRC";
    const LOG_TABLE = "object";

    public function __construct()
    {
        parent::__construct();
        $this->id_field = $this->getTableFieldPrefix() . $this->id_field;
        $this->status_field = $this->getTableFieldPrefix() . $this->status_field;
    }

    /*************************************
     * GET functions, Copied by TM
     * */

    public function getTableField()
    {
        return self::TABLE_FLD;
    }

    public function getTableName()
    {
        return $this->getDBRef() . "_" . self::TABLE;
    }

    public function getRootTableName()
    {
        return $this->getDBRef();
    }

    public function getRefTag()
    {
        return self::REFTAG;
    }

    public function getMyObjectType()
    {
        return self::OBJECT_TYPE;
    }

    public function getMyLogTable()
    {
        return self::LOG_TABLE;
    }

    public function getApproverFieldName()
    {
        return $this->approver_field;
    }

    public function getTableFieldPrefix()
    {
        return $this->table_field;
    }



    public function addObject($var)
    {
        unset($var['attachments']);

        $cc = $var['hdc_cc'];
        $company_information = $this->getCompanyInformation($cc);

        if(isset($company_information) && is_array($company_information) && count($company_information) > 0){
            $sql = 'UPDATE helpdesk_companies ';
            $sql .= 'SET hdc_active = 1 ';
            $sql .= 'WHERE hdc_cc = \''. $cc . '\'';
            $update_id = $this->db_update($sql);

            if ($update_id > 0) {
                $success = true;
                $message_text = 'activated';

                $comp_id = $company_information['hdc_id'];
            } else {
                $success = false;
            }

        }else{
            $db = new ASSIST_DB('master');
            $sql = 'SELECT * FROM assist_company ';
            $sql .= 'WHERE cmpcode = \'' . $cc . '\' ';
            $ignite_company = $db->mysql_fetch_one($sql);

            //Put information into db
            $var[$this->getTableField() . 'name'] = $ignite_company['cmpname'];
            $var[$this->getTableField() . 'cc'] = $ignite_company['cmpcode'];

            if($ignite_company['cmpcode'] == 'IASSIST' || $ignite_company['cmpcode'] == 'PASSIST'){
                $type = 3;//Reseller
                $parent = 0;//IASSIST AND PASSIST

                $var[$this->getTableField() . 'cc'] = 'iassist';

            }elseif($ignite_company['cmpreseller'] == 'IASSIST'){
                $type = 2;//Reseller
                $sql = 'SELECT * FROM helpdesk_companies ';
                $sql .= 'WHERE helpdesk_companies.hdc_cc = \'iassist\'';
                $parent_comp = $this->mysql_fetch_one($sql);

                if(count($parent_comp) > 0){
                    $parent = $parent_comp['hdc_id'];//IASSIST
                }

            }elseif(!($ignite_company['cmpreseller'] == 'IASSIST')){
                $type = 1;//Client

                $sql = 'SELECT * FROM helpdesk_companies ';
                $sql .= 'WHERE helpdesk_companies.hdc_cc = \''. strtolower($ignite_company['cmpreseller']) . '\'';
                $parent_comp = $this->mysql_fetch_one($sql);

                if(count($parent_comp) > 0){
                    $parent = $parent_comp['hdc_id'];//RESELLER
                }
            }

            $var[$this->getTableField() . 'type'] = $type;
            $var[$this->getTableField() . 'parent'] = $parent;
            $var[$this->getTableField() . 'active'] = 1;


            $var[$this->getTableField() . 'admin_management'] = 1;//new

            $sql = "INSERT INTO " . $this->getTableName() . " SET " . $this->convertArrayToSQL($var);
            $id = $this->db_insert($sql);
            if ($id > 0) {
                $comp_id = $id;

                $admin_man = 1; // No Admin
                $settings = 1; // Auto Escalate

                $sql = 'INSERT INTO helpdesk_company_settings ';
                $sql .= '(hcs_company_id, hcs_admin_man_id, hcs_settings_id)';
                $sql .= 'VALUES (' . $id . ', ' . $admin_man . ', ' . $settings . ')';
                $id = $this->db_insert($sql);

                // 4. Return true for success, or false for failure
                if ($id > 0) {
                    $success = true;

                    $company_information = $this->getActiveCompanyInformation($cc);
                    $message_text = 'created';
                } else {
                    $success = false;
                }

            } else {
                $success = false;
            }

        }




        //Return the relevant message upon success or failure
        if ($success) {
            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = $company_information['hdc_name'] . ' has been successfully ' . $message_text . ' in the helpdesk system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO helpdesk_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $id . " has been successfully added to the Helpdesk system.",
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }
    }


    public function updateSettings($cc)
    {
        $company_information = $this->getActiveCompanyInformation($cc);


        //Get the admins of this company to display them in the table
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        //Admin users
        $sql .= 'INNER JOIN helpdesk_users ';
        $sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_user_types ';
        $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

        $sql .= 'INNER JOIN helpdesk_user_admin_types ';
        $sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

        $sql .= 'INNER JOIN helpdesk_admin_types ';
        $sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''. $cc . '\'';
        $company_admins = $this->mysql_fetch_all($sql);

        if(count($company_admins) == 0){
            $admin_man = 1; // One Admin
            $settings = 1; // To One Admin
        }elseif(count($company_admins) == 1){
            $admin_man = 2; // No Admin
            $settings = 2; // Auto Escalate
        }elseif(count($company_admins) > 1){
            $admin_man = 3; // Multi Admin
            $settings = 3; // Pot
        }

        $sql = 'UPDATE helpdesk_company_settings ';
        $sql .= 'SET ';
        $sql .= 'hcs_admin_man_id = ' . $admin_man . ', ';
        $sql .= 'hcs_settings_id = ' . $settings . ' ';
        $sql .= 'WHERE hcs_company_id = ' . $company_information['hdc_id'] . ' ';
        $id = $this->db_update($sql);

        return $id;
    }

    public function updateObject($var)
    {
    }

    //I know we call it a deletion, but it's really a deactivation
    public function deleteObject($var) {
        if(isset($var['attachments'])){
            unset($var['attachments']);
        }

        $cc = $var['hdc_cc'];
        $company_information = $this->getActiveCompanyInformation($cc);

        //Deactivate the company
        $sql = 'UPDATE helpdesk_companies ';
        $sql .= 'SET hdc_active = 0 ';
        $sql .= 'WHERE hdc_id = ' . $company_information['hdc_id'];
        $id = $this->db_update($sql);

        //Return true for success, or false for failure
        if ($id > 0) {
            $comp_id = $company_information['hdc_id'];
            $success = true;
        } else {
            $success = false;
        }

        //Return the relevant message upon success or failure
        if ($success) {
            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = $company_information['hdc_name'] . ' has been successfully deactivated in the Helpdesk system, by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO helpdesk_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => "" . $this->getObjectName($this->getMyObjectType()) . " " . $this->getRefTag() . $id . " has been successfully deactivated.",
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }

    }

    public function saveHelpdeskUserAccessChange($var){
        $heldesk_user_access_id = $var['heldesk_user_access'];

        $company_code = $var['company_code'];
        $company_information = $this->getActiveCompanyInformation($company_code);

        $sql = 'UPDATE helpdesk_company_settings ';
        $sql .= 'SET ';
        $sql .= 'hcs_hcua_id = ' . $heldesk_user_access_id . ' ';
        $sql .= 'WHERE hcs_company_id = ' . $company_information['hdc_id'] . ' ';
        $id = $this->db_update($sql);

        //Return true for success, or false for failure
        if ($id > 0) {
            $comp_id = $company_information['hdc_id'];
            $success = true;
        } else {
            $success = false;
        }

        //Return the relevant message upon success or failure
        if ($success) {
            $company_user_access_values = $this->getHelpdeskCompanyUserAccessChange();

            //user log values
            $log_comp_id = $comp_id;
            $log_comp_change = 'Helpdesk User Access for ' . $company_information['hdc_name'] . ' has been successfully changed to "' . $company_user_access_values[$heldesk_user_access_id]['hcua_value'] . '", by: ' . $_SESSION['tkn'];

            //Log the addition or promotion to admin user
            $log_values = array();
            $log_values['comp_id'] = $log_comp_id;
            $log_values['comp_change'] = $log_comp_change;

            $sql = 'INSERT INTO helpdesk_company_log (comp_id,datetime,comp_change) ';
            $sql .= 'VALUES ('.$log_values['comp_id'].', ';
            $sql .= 'NOW(), ';
            $sql .= '\''.$log_values['comp_change'].'\')';
            $id = $this->db_insert($sql);

            if(isset($id) && is_int($id) && $id > 0){
                $result = array(
                    0 => "ok",
                    1 => $log_comp_change,
                    'object_id' => $id,
                );
                return $result;
            }
        } elseif (!$success) {
            return array("error", "Testing: " . $sql);
        }

    }

    public function getHelpdeskCompanyUserAccessChange(){
        $sql = 'SELECT * FROM helpdesk_company_user_access ';
        $company_user_access_values = $this->mysql_fetch_all_by_id($sql, 'hcua_id');
        return $company_user_access_values;
    }


    public function getCompanyInformation($cc){
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''. $cc . '\' ';
        $company_info = $this->mysql_fetch_one($sql);

        return $company_info;
    }

    public function userHasAccessToHelpdesk($company_code, $tkid){
        //Determine what the company user access is
        //if setting is "All Users" then return true

        //Elseif setting is "Admin Users Only", then determine if we are working with an admin user

        ///else  return false
        //
        $userHasAccessToHelpdesk = false;
        if(isCompanyInHelpdesk($company_code)){
            $user_obj = new HELPDESK_USERS();
            $company_information = $this->getActiveCompanyInformation($company_code);
            if($company_information['hcs_hcua_id'] == 1 || ($company_information['hcs_hcua_id'] == 2 && $user_obj->isCurrentUserAnAdminUser($company_code, $tkid))){
                $userHasAccessToHelpdesk = true;
            }
        }

        return $userHasAccessToHelpdesk;

    }

    public function getActiveCompanyInformation($cc){
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        // Clauses
        $sql .= 'WHERE helpdesk_companies.hdc_cc = \''. $cc . '\' ';
        $sql .= 'AND helpdesk_companies.hdc_active = 1';
        $company_info = $this->mysql_fetch_one($sql);

        return $company_info;
    }

    public function isCompanyInHelpdesk($cc){

        $companyInQuestion = $this->getActiveCompanyInformation($cc);

        $isCompanyInHelpdesk = false;
        if(isset($companyInQuestion) && is_array($companyInQuestion) && count($companyInQuestion) > 0){
            $isCompanyInHelpdesk = true;
        }
        return $isCompanyInHelpdesk;
    }


    public function getAllCompanies(){
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_active = 1 ';
        $sql .= 'ORDER BY helpdesk_companies.hdc_name ASC';

        $raw_companies = $this->mysql_fetch_all_by_id($sql, 'hdc_cc');

//        echo '<pre style="font-size: 18px">';
//        echo '<p>ALL COMPANIES - RAW</p>';
//        print_r($raw_companies);
//        echo '</pre>';

        //Get every registered company
        $db = new ASSIST_DB('master');
        $sql = 'SELECT cmpcode, cmpname, cmp_is_demo FROM assist_company ';
        $sql .= 'WHERE cmpcode <> \'BLANK\' AND cmp_is_demo <> 1 ';
        $sql .= 'AND (cmpstatus = \'Y\' OR cmpstatus = \'S\') ';
        $sql .= 'ORDER BY cmpname ASC';
        $ait_companies = $db->mysql_fetch_all($sql);

        $resellers = array();
        $clients = array();
        foreach($ait_companies as $key => $val){
            if(array_key_exists($val['cmpcode'], $raw_companies)){
                $company_id = $raw_companies[$val['cmpcode']]['hdc_id'];
                $company_code = $val['cmpcode'];
            }elseif(array_key_exists(strtolower($val['cmpcode']), $raw_companies)){
                $company_id = $raw_companies[strtolower($val['cmpcode'])]['hdc_id'];
                $company_code = strtolower($val['cmpcode']);
            }else{
                $company_id = 0;
            }

            if($company_id != 0){

                if($raw_companies[$company_code]['hdct_value'] == 'reseller'){
                    $resellers[$company_id] = $raw_companies[$company_code];
                }elseif($raw_companies[$company_code]['hdct_value'] == 'client'){
                    $clients[$company_id] = $raw_companies[$company_code];
                }else{

                }
            }
        }

//        foreach($raw_companies as $key => $val){
//            if($val['hdct_value'] == 'reseller'){
//                $resellers[$key] = $val;
//            }elseif($val['hdct_value'] == 'client'){
//                $clients[$key] = $val;
//            }else{
//
//            }
//        }


        $companies_data_structure = array();

        //ksort($resellers);

        foreach($resellers as $r_key => $r_val){
            $companies_data_structure[$r_key]['info'] = $r_val;

            //ksort($clients);
            foreach($clients as $c_key => $c_val){
                if((int)$c_val['hdc_parent'] == $r_key){
                    $companies_data_structure[$r_key]['children'][$c_key] = $c_val;

                    unset($clients[$c_key]);
                }
            }
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>ALL COMPANIES - STRUCTURED</p>';
//        print_r($companies_data_structure);
//        echo '</pre>';

        return $companies_data_structure;

    }

    public function getAllCompaniesRAW()
    {
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_active = 1 ';
        $sql .= 'ORDER BY helpdesk_companies.hdc_name ASC';

        $raw_companies = $this->mysql_fetch_all_by_id($sql, 'hdc_id');

        return $raw_companies;
    }

    public function getAllClientCompaniesFormattedForSelect(){
        $sql = 'SELECT * FROM helpdesk_companies ';

        // Company Type
        $sql .= 'INNER JOIN helpdesk_company_types ';
        $sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

        // Company helpdesk settings
        $sql .= 'INNER JOIN helpdesk_company_settings ';
        $sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

        $sql .= 'INNER JOIN helpdesk_admin_management ';
        $sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

        $sql .= 'INNER JOIN helpdesk_request_settings ';
        $sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

        $sql .= 'WHERE helpdesk_companies.hdc_active = 1 ';
        $sql .= 'AND helpdesk_company_types.hdct_value = \'client\'';

        $raw_companies = $this->mysql_fetch_all_by_id($sql, 'hdc_id');

        $companies_formatted_for_select = array();
        foreach($raw_companies as $key => $val){
            $companies_formatted_for_select[$val['hdc_cc']] = $val['hdc_name'];
        }

        return $companies_formatted_for_select;

    }

    public function assignAdminToChildCompany($var) {
        $user_id = $var['user_id'];
        unset($var['user_id']);

        $child_comp_id_array = array();
        $child_comp_value_array = array();
        $last_str_pos = 0;

        foreach($var as $key => $val){
            $key_string = $key;

            $strpos = strrpos($key_string, '_id_');

            $last_str_pos = $strpos;
            if($strpos > 0){
                $new_key_string = trim($key_string,"hdc_id_");
                $child_comp_value_array[(int)$new_key_string] = $val;
                $child_comp_id_array[(int)$new_key_string] = (int)$new_key_string;
            }
        }

        $sql = 'SELECT * FROM helpdesk_company_admin_assignment ';
        $sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_user_id = ' . $user_id . ' ';
        $sql .= 'AND helpdesk_company_admin_assignment.hcaa_hdc_id IN (' . implode(', ', $child_comp_id_array) . ') ';
        $company_assignments = $this->mysql_fetch_all_by_id($sql, 'hcaa_hdc_id');

        $delete_id_array = array();
        $insert_id_array = array();
        foreach($child_comp_value_array as $key => $val){
            if(array_key_exists($key, $company_assignments) && (int)$val == 0){
                //Delete the record
                $delete_id_array[$key] = $key;
            }elseif(!array_key_exists($key, $company_assignments) && (int)$val == 1){
                //Add the record
                $insert_id_array[$key] = $key;
            }
        }

        if(isset($delete_id_array) && is_array($delete_id_array) && count($delete_id_array) > 0){
            $sql = 'DELETE FROM helpdesk_company_admin_assignment ';
            $sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_user_id = ' . $user_id . ' ';
            $sql .= 'AND helpdesk_company_admin_assignment.hcaa_hdc_id IN (' . implode(', ', $delete_id_array) . ') ';
            $deletion = $this->db_query($sql);
        }

        if(isset($insert_id_array) && is_array($insert_id_array) && count($insert_id_array) > 0) {
            $sql = 'INSERT INTO helpdesk_company_admin_assignment (hcaa_hdc_id,hcaa_user_id) ';
            $sql .= 'VALUES ';
            $count = 1;
            foreach($insert_id_array as $key => $val){
                $sql .= '(' . $val . ', ' . $user_id . ')';
                $sql .= ($count < count($insert_id_array) ? ',' : '');
                $count++;
            }
            $insertion = $this->db_query($sql);
        }

        $assignment_changes = false;

        if((isset($deletion) && $deletion != false) || (isset($insertion) && $insertion != false)){
            $assignment_changes = true;
        }

//        $debug = array(
//            0 => "ok",
//            1 => "has been successfully deactivated.",
//            'var_array' => $var['hdc_id_22'],
//            'user_id' => $user_id,
//            'last_one' => $last_str_pos,
//            'child_comp_value_array' => $child_comp_value_array,
//            'object_id' => $user_id,
//            'inserts' => $insert_id_array,
//            'delete' => $delete_id_array,
//        );
//        return $debug;

        //Return the relevant message upon success or failure
        if ($assignment_changes == true) {
            $result = array(
                0 => "ok",
                1 => " Assignment changes have been made successfully.",
                'object_id' => $user_id,
            );
        } else{
            $result = array(
                0 => "Info",
                1 => "There were no changes to be found.",
                'object_id' => $user_id,
            );
        }
        return $result;

    }

    public function __destruct()
    {
        parent::__destruct();
    }


}

?>
