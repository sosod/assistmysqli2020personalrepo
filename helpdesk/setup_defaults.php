<?php


require_once("inc_header.php");

/*
 * MANAGE COMPANIES
 * */

$hdc_object = new HELPDESK_COMPANY();
$current_company_info = $hdc_object->getActiveCompanyInformation($_SESSION['cc']);

//echo '<pre style="font-size: 18px">';
//echo '<p>CURRENT COMPANY INFORMATION</p>';
//print_r($current_company_info);
//echo '</pre>';

if($current_company_info['hdc_cc'] == 'iassist'){
	$iassist_info = $current_company_info;
}else{
	$iassist_info = $hdc_object->getActiveCompanyInformation('iassist');
}

if(!($current_company_info['hdct_value'] == 'client')){
    $all_companies = $hdc_object->getAllCompanies();

    if($current_company_info['hdct_value'] == 'reseller'){
    	$all_companies_temp_container = $all_companies;
    	unset($all_companies);
		$all_companies = array();
        $all_companies[$current_company_info['hdc_id']] = $all_companies_temp_container[$current_company_info['hdc_id']];
    }

?>
	<style type="text/css">
		#div_left, #div_right {
			padding: 0px 20px 20px 20px;
			width: 45%;
			height: 100%;
			min-height: 350px;
		}
		#div_container {
			padding: 0px 10px 0px 10px;
		}
	</style>
	<div id=div_container>
		<div id="<?php echo (!($current_company_info['hdct_value'] == 'client') ? 'div_right' : 'div_left');?>">
			<h2>Manage Companies And Users</h2>
			<table class="dialog-form middle" >
				<tr>
					<th>Company Name</th>
					<th>Company Code</th>
					<th>Company Type</th>
					<th>Company Parent</th>
					<th></th>
				</tr>
				<?php foreach($all_companies as $r_key => $r_val){ ?>
					<tr class="th2">
						<td><?php echo $r_val['info']['hdc_name'] ?></td>
						<td><?php echo strtoupper($r_val['info']['hdc_cc']) ?></td>
						<td><?php echo ucfirst($r_val['info']['hdct_value']) ?></td>
						<td><?php echo $iassist_info['hdc_name'] ?></td>
						<td><span class=float><input type=button value=Manage data-cc="<?php echo $r_val['info']['hdc_cc'] ?>" class=btn_manage id=columns /></span></td>
					</tr>
					<?php if(isset($r_val['children'])){ ?>
						<?php foreach($r_val['children'] as $c_key => $c_val){ ?>
							<tr>
								<td><?php echo $c_val['hdc_name'] ?></td>
								<td><?php echo strtoupper($c_val['hdc_cc']) ?></td>
								<td><?php echo ucfirst($c_val['hdct_value']) ?></td>
								<td><?php echo $r_val['info']['hdc_name'] ?></td>
								<td><span class=float><input type=button value=Manage data-cc="<?php echo $c_val['hdc_cc'] ?>" class=btn_manage id=columns /></span></td>
							</tr>
						<?php } ?>
					<?php } ?>
				<?php } ?>

			</table>
		</div>


		<?php if($_SESSION['cc'] == 'iassist'){  ?>
			<div id=div_left>
				<h2><?php echo $current_company_info['hdc_name']; ?>: Settings</h2>
				<table class="dialog-form middle form" >
					<?php if(false){ ?>
					<tr>
						<th >Preferences:</th>
						<td>Customise the module settings.<span class=float><input type=button value=Configure class=btn_setup id=preferences /></span></td>
					</tr>
					<tr>
						<th >Naming:</th>
						<td>Configure the naming convention for the various objects and activities.<span class=float><input type=button value=Configure class=btn_setup id=names /></span></td>
					</tr>
					<tr>
						<th >Menus:</th>
						<td>Configure the naming convention for the menus.<span class=float><input type=button value=Configure class=btn_setup id=menu /></span></td>
					</tr>
					<tr>
						<th>Headings:</th>
						<td>Configure the naming convention for the field headings.<span class=float><input type=button value=Configure class=btn_setup id=headings /></span></td>
					</tr>
					<tr>
						<th>List Columns:</th>
						<td>Configure the columns to be displayed on the list pages.<span class=float><input type=button value=Configure class=btn_setup id=columns /></span></td>
					</tr>
					<?php } ?>
					<tr>
						<th>Manage Admins:</th>
						<td>Functionality From the Helpdesk Admin Module.<span class=float><input type=button value=Configure class=btn_setup id=manage_admins /></span></td>
					</tr>

					<tr>
						<th>Manage Companies:</th>
						<td>Manage the access that organizations have to the Helpdesk System.<span class=float><input type=button value=Configure class=btn_setup id=helpdesk_access /></span></td>
					</tr>

				</table>
			</div>
		<?php  }elseif($current_company_info['hdct_value'] == 'reseller'){ ?>
			<div id=div_left>
				<h2><?php echo $current_company_info['hdc_name']; ?>: Settings</h2>
				<table class="dialog-form middle form" >
					<tr>
						<th>Manage Admins:</th>
						<td>Functionality From the Helpdesk Admin Module.<span class=float><input type=button value=Configure class=btn_setup id=manage_admins /></span></td>
					</tr>

					<tr>
						<th>Manage Companies:</th>
						<td>Manage the access that organizations have to the Helpdesk System.<span class=float><input type=button value=Configure class=btn_setup id=helpdesk_access /></span></td>
					</tr>

				</table>
			</div>
		<?php  } ?>
	</div> <!-- end div container -->

	<div style="position:absolute;bottom:5px;right:5px;">
		<?php
		//ASSIST_HELPER::displayResult(array("error","NOTE TO DEVELOPER:<br />* = to be tested once module finalised;<br />++ = coding to be finalised once Travel Agent side finalised;"))
		?>
	</div>
	<script type=text/javascript>
		$(function() {
			$("h2").css("margin-top","15px");
			$("span.float").css("margin-left","10px");

			$("input:button").button().css("font-size","75%");
			$("input:button.btn_manage").click(function() {
				document.location.href = 'setup_defaults_manage_admins.php?company_cc=' + $(this).data('cc');
			});

			$("input:button").button().css("font-size","75%");
			$("input:button.btn_setup").click(function() {
				document.location.href = "setup_defaults_"+$(this).prop("id")+".php";
			});


			$("#div_right").css("float","right");
			$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
			$("table.form").css({"width":"100%"});
			$(window).resize(function() {
				var x = 0;
				$("#div_right").children().each(function() {
					x+=AssistString.substr($(this).css("height"),0,-2)*1;
				});
				$("#div_container").css("height",(x+50)+"px");
			});
			$(window).trigger("resize");
		});
	</script>
<?php }else{ ?>
	<script>
		$(function() {
			document.location.href = 'setup_defaults_manage_admins.php?company_cc=<?php echo $current_company_info['hdc_cc']; ?>';
		});
	</script>
<?php } ?>
