<?php $display_navigation_buttons = false; ?>
<?php require_once('inc_header.php'); ?>
<?php
//    echo '<pre>';
//    echo '<p>GET</p>';
//    print_r($_GET);
//    echo '</pre>';

$requestObject = new HELPDESK_REQUEST();
echo '<h2>'.$requestObject->getObjectName($requestObject->getMyObjectType()).'</h2>';

ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r']) ? $_REQUEST['r'] : array()));

//This get's the information for this request so that I can populate the table  with the topic and module
$sql = 'SELECT helpdesk_request.*, helpdesk_companies.*, helpdesk_users.*, helpdesk_list_topic.value AS topic_value, helpdesk_list_module.value AS module_value FROM helpdesk_request ';

$sql .= 'INNER JOIN helpdesk_list_topic ';
$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

$sql .= 'INNER JOIN helpdesk_list_module ';
$sql .= 'ON helpdesk_list_module.id = helpdesk_request.hdr_moduleid ';

$sql .= 'INNER JOIN helpdesk_users ';
$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

$sql .= 'WHERE helpdesk_request.hdr_id = '.$_GET['hdrid'];

$request = $helper->mysql_fetch_one($sql);


if(!isset($request) || !is_array($request) || is_null($request) || $request === false || count($request) == 0) {//This should only happen when the reseller requests a new client db
	$sql = 'SELECT * FROM helpdesk_request ';
	$sql .= 'INNER JOIN helpdesk_list_topic ';
	$sql .= 'ON helpdesk_list_topic.id = helpdesk_request.hdr_topicid ';

	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_id = helpdesk_request.hdr_add_userid ';

	$sql .= 'INNER JOIN helpdesk_companies ';
	$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

	$sql .= 'INNER JOIN helpdesk_user_types ';
	$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

	$sql .= 'WHERE helpdesk_request.hdr_id = '.$_GET['hdrid'];

	$request = $helper->mysql_fetch_one($sql);
}

//echo '<pre style="font-size: 18px">';
//echo '<p>REQUEST</p>';
//print_r($request);
//echo '</pre>';

//Find the Assigned user for this specific request
$sql = 'SELECT * FROM helpdesk_request ';
$sql .= 'INNER JOIN helpdesk_request_users ';
$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

$sql .= 'INNER JOIN helpdesk_users ';
$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

$sql .= 'INNER JOIN helpdesk_list_user_roles ';
$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

$sql .= 'WHERE helpdesk_request.hdr_id = '.$_GET['hdrid'].' ';
$sql .= 'AND ';
$sql .= 'helpdesk_list_user_roles.hlur_id = 1 ';
$assigned_user = $helper->mysql_fetch_one($sql);

//Check if the current user is the add user or the assigned user to determine the actions
$sql = 'SELECT * FROM helpdesk_users ';
$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
$sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'].' ';
$sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'].'\'';
$user = $helper->mysql_fetch_one($sql);

//echo '<pre style="font-size: 18px">';
//echo '<p>USER</p>';
//print_r($user);
//echo '</pre>';

$actions = array();

$show_time_field = true;
$assigned_req = true;
$is_pipeline_user = true;
if($user['user_id'] == $request['hdr_add_userid']) {

	$show_time_field = false;

	if($request['hdr_add_status'] == 5) {// Marked As Resolved
		$actions[4] = 'Incorrect Solution';
		$actions[7] = 'Confirm as Completed';
	} elseif($request['hdr_add_status'] == 3) {//Returned
		$actions[4] = 'Reply';
	} elseif($request['hdr_add_status'] == 6 || $request['hdr_add_status'] == 7 || $request['hdr_add_status'] == 8) {// Cancelled, Completed, Closed
		$actions[0] = 'Do Nothing';
	} else {
		$actions[0] = 'Update';
		$actions[6] = 'Cancel';
	}

	$current_status = $request['hdr_add_status'];

} elseif($user['user_id'] != $request['hdr_add_userid'] && ($user['hdrut_value'] == 'client_admin_user' || $user['hdrut_value'] == 'reseller_admin_user' || $user['hdrut_value'] == 'iassist_admin_user')) {//if client admin user

	//Show the option to escalate if this admin user is not from iassist
	$show_esc_opt = true;
	if($user['hdrut_value'] == 'iassist_admin_user') {
		$show_esc_opt = false;
	}

	//Find out what the status is for the current user so that we can dtermine the actions available to them
	$sql = 'SELECT * FROM helpdesk_request_users ';

	$sql .= 'INNER JOIN helpdesk_list_user_roles ';
	$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';
	$sql .= 'WHERE hdru_userid = '.$user['user_id'].' ';
	$sql .= 'AND hdru_hdrid = '.$_GET['hdrid'];
	$user_roles = $helper->mysql_fetch_one($sql);

	//Check if there's a record for the users in this company for this request
	$sql = 'SELECT * FROM helpdesk_request_users ';

	$sql .= 'INNER JOIN helpdesk_list_user_roles ';
	$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';

	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

	$sql .= 'INNER JOIN helpdesk_companies ';
	$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

	$sql .= 'WHERE hdc_cc = \''.$_SESSION['cc'].'\' ';
	$sql .= 'AND hdru_hdrid = '.$_GET['hdrid'];
	$request_user_records = $helper->mysql_fetch_one($sql);


//    echo '<pre style="font-size: 18px">';
//    echo '<p>$user_roles</p>';
//    var_dump($user_roles);
//    echo '</pre>';
//
//    echo '<pre style="font-size: 18px">';
//    echo '<p>$request_user_records</p>';
//    var_dump($request_user_records);
//    echo '</pre>';


	if(
		((is_null($user_roles) && is_null($request_user_records)) || ($user_roles === false && $request_user_records === false))
		||
		((is_null($user_roles) && !is_null($request_user_records) && $user['user_id'] != $request_user_records['user_id']) || ($user_roles === false && $request_user_records !== false && $user['user_id'] != $request_user_records['user_id']))
	) {
		$is_pipeline_user = false;
	}

	/*
	 * The following piece of code determines the users involved with the current request
	 * below the current user, which is going to be very important for the returning of
	 * requests for more information
	 * */
	// This is the add_user
	$involved_users = array();
	$involved_users[0]['user_name'] = $request['user_name'];
	$involved_users[0]['user_id'] = $request['user_id'];
	$involved_users[0]['hdc_name'] = $request['hdc_name'];

	// Interested parties and assigned
	$sql = 'SELECT * FROM helpdesk_request ';

	$sql .= 'INNER JOIN helpdesk_request_users ';
	$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

	$sql .= 'INNER JOIN helpdesk_companies ';
	$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

	$sql .= 'INNER JOIN helpdesk_list_user_roles ';
	$sql .= 'ON helpdesk_list_user_roles.hlur_id = helpdesk_request_users.hdru_role ';


	$sql .= 'WHERE  helpdesk_request_users.hdru_hdrid = '.$_GET['hdrid'].' ';
	$sql .= 'AND  helpdesk_request_users.hdru_userid != '.$user['user_id'];

	$involved_users_below = $helper->mysql_fetch_all($sql);

	$user_index = 1;
	if(count($involved_users_below) > 0) {
		foreach($involved_users_below as $key => $val) {
			$involved_users[$user_index]['user_name'] = $val['user_name'];
			$involved_users[$user_index]['user_id'] = $val['user_id'];
			$involved_users[$user_index]['hdc_name'] = $val['hdc_name'];
			$user_index++;
		}
	}

	//what's the current user's user_type, as which type of admin is he
	$user_type = $user['hdrut_value'];

	//Is there an admin above this one attached to this request
	$sql = 'SELECT * FROM helpdesk_request ';

	$sql .= 'INNER JOIN helpdesk_request_users ';
	$sql .= 'ON helpdesk_request_users.hdru_hdrid = helpdesk_request.hdr_id ';

	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_id = helpdesk_request_users.hdru_userid ';

	$sql .= 'INNER JOIN helpdesk_companies ';
	$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

	$sql .= 'INNER JOIN helpdesk_user_types ';
	$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

	$sql .= 'WHERE  hdr_id = '.$_GET['hdrid'].' ';
	if($user_type == 'client_admin_user') {
		$sql .= 'AND  helpdesk_user_types.hdrut_value = \'reseller_admin_user\'';
	} elseif($user_type == 'reseller_admin_user') {
		$sql .= 'AND  helpdesk_user_types.hdrut_value = \'iassist_admin_user\'';
	} elseif($user_type == 'iassist_admin_user') {
		$sql .= 'AND  helpdesk_user_types.hdrut_value = \'this_doesnt_exist\'';
	}

	$escalated = $helper->mysql_fetch_one($sql);

	if(!isset($escalated) || !is_array($escalated)) {
		$escalated = array();
	}

	/*
	 * NEW TRANSFER CODE STUFF == BEGIN
	 * */
	//Are there other admins in this company
	$sql = 'SELECT * FROM helpdesk_companies ';

	// Company Type
	$sql .= 'INNER JOIN helpdesk_company_types ';
	$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

	// Company helpdesk settings
	$sql .= 'INNER JOIN helpdesk_company_settings ';
	$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

	$sql .= 'INNER JOIN helpdesk_admin_management ';
	$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

	$sql .= 'INNER JOIN helpdesk_request_settings ';
	$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

	//Admin users
	$sql .= 'INNER JOIN helpdesk_users ';
	$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

	$sql .= 'INNER JOIN helpdesk_user_types ';
	$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

	$sql .= 'INNER JOIN helpdesk_user_admin_types ';
	$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

	$sql .= 'INNER JOIN helpdesk_admin_types ';
	$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

	// Clauses
	$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'].'\'';
	$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';
	$other_company_admins = $helper->mysql_fetch_all($sql);

	$transferable = false;
	if(isset($other_company_admins) && is_array($other_company_admins) && count($other_company_admins) > 0) {
//        echo '<pre style="font-size: 18px">';
//        echo '<p>OTHER ADMINS</p>';
//        print_r($other_company_admins);
//        echo '</pre>';
		$transferable = true;
	}


	/*
	 * NEW TRANSFER CODE STUFF == END
	 * */


	$show_edit_button = false;

	//Admin User
	if((count($escalated) == 0 && $user_roles['hlur_value'] == 'assigned_user' && $user_roles['hdru_status'] != 12) || (count($escalated) > 0 && $user_roles['hlur_value'] == 'interested_party') && $user_roles['hdru_status'] != 5) {//If current user is the assigned user
		if($user_roles['hdru_status'] == 1) {//New
			$actions[2] = 'Confirm';
			$actions[4] = 'Confirm and work on it';
			if($show_esc_opt) {
				$actions[9] = 'Escalate';
			}

			//Put the users into the action array
			foreach($involved_users as $key => $val) {
				$return = 10;
				$actions[$return + $val['user_id']] = 'Return to '.$val['user_name'].' ('.$val['hdc_name'].')';
			}

			if($transferable) {
				//Put them in the action array
				foreach($other_company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$actions['_transfer_request_to_'.$val['user_id']] = 'Transfer to '.$val['user_name'].' ('.$val['hdc_name'].')';
				}
			}

		} elseif($user_roles['hdru_status'] == 2) {//Confirmed
			$actions[4] = 'Work on it';
			if($show_esc_opt) {
				$actions[9] = 'Escalate';
			}

			//Put the users into the action array
			foreach($involved_users as $key => $val) {
				$return = 10;
				$actions[$return + $val['user_id']] = 'Return to '.$val['user_name'].' ('.$val['hdc_name'].')';
			}

			if($transferable) {
				//Put them in the action array
				foreach($other_company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$actions['_transfer_request_to_'.$val['user_id']] = 'Transfer to '.$val['user_name'].' ('.$val['hdc_name'].')';
				}
			}

			//If Iassist, set up an option to terminate
			if($user['hdrut_value'] == 'iassist_admin_user') {
				$actions['_terminate_request_silently_'] = 'Silent Termination';
			}

		} elseif($user_roles['hdru_status'] == 3) {//Returned
			$actions[4] = 'Reply';
		} elseif($user_roles['hdru_status'] == 4) {// In Progress
			$actions[0] = 'Update';
			$actions[5] = 'Mark As Resolved';
			if($show_esc_opt) {
				$actions[9] = 'Escalate';
			}

			//Put the users into the action array
			foreach($involved_users as $key => $val) {
				$return = 10;
				$actions[$return + $val['user_id']] = 'Return to '.$val['user_name'].' ('.$val['hdc_name'].')';
			}

			if($transferable) {
				//Put them in the action array
				foreach($other_company_admins as $key => $val) {
					// Send notification email to the admins of this company, notifying them of the new request
					$actions['_transfer_request_to_'.$val['user_id']] = 'Transfer to '.$val['user_name'].' ('.$val['hdc_name'].')';
				}
			}

		} elseif($user_roles['hdru_status'] == 5) {// Marked as resolved
			$actions[8] = 'Close';
		} elseif(($user_roles['hdru_status']) == 6 || ($user_roles['hdru_status'] == 7) || ($user_roles['hdru_status'] == 8)) {// Cancelled, Completed, Closed
			$actions[-1] = 'Do Nothing';
		} elseif($user_roles['hdru_status'] == 9) {// Escalated
			$actions[0] = 'Update';
		} elseif($user_roles['hdru_status'] == 10) {//Waiting
			$actions[4] = 'Reclaim';
			$actions[0] = 'Update';
		}

		$show_edit_button = true;
	} elseif(count($escalated) > 0 && $user_roles['hlur_value'] == 'assigned_user') {//Resolved at reseller admin user level
		if($user_roles['hdru_status'] == 9) {// Escalated
			if($escalated['hdru_status'] == 5) {//Marked as resolved
				$actions[4] = 'Incorrect Solution';
				$actions[5] = 'Confirm Resolution';
			} else {
				$actions[0] = 'Do Nothing';
			}
		} elseif($user_roles['hdru_status'] == 5) {
			$actions[8] = 'Close';
		}
	} elseif(($user_roles['hlur_value'] == 'interested_party') && $user_roles['hdru_status'] == 5 && $request['hdr_add_status'] == $user_roles['hdru_status']) {
		$actions[8] = 'Close';
	} elseif(($user_roles['hlur_value'] == 'interested_party') && $user_roles['hdru_status'] == 5 && $request['hdr_add_status'] != $user_roles['hdru_status']) {
		$actions[-1] = 'Do Nothing';
	} elseif($user_roles['hlur_value'] == 'interested_party' && $user_roles['hdru_status'] == 9 && count($escalated) == 0) {//Escalated but unclaimed
		$actions[0] = 'Update';
	} elseif(($user_roles['hlur_value'] == 'pending_transfer_user') && $user_roles['hdru_status'] == 11) {//Pending Transfer User with a status of New Transfer
		$actions['_reject_transfer_'] = 'Reject Transfer';
		$actions['_accept_transfer_and_confirm'] = 'Accept Transfer';
		$actions['_accept_transfer_and_work_on_it'] = 'Accept Transfer and Work on it';
	} elseif(($user_roles['hlur_value'] == 'assigned_user') && (int)$user_roles['hdru_status'] == 12) {//Assigned User with a status of Pending Transfer
		$actions['_reclaim_pending_transfer_'] = 'Reclaim Pending Transfer';
	} elseif(strtolower($_SESSION['cc']) == "iassist" && $is_pipeline_user === false && !(is_null($request_user_records) || $request_user_records === false)) {
		$actions['_seize_and_assume_responsibility_'] = 'Seize and Assume Responsibility';
	} else {//This is unclaimed
		$assigned_req = false;
		$actions[2] = 'Claim / Confirm Reception';
		$actions[4] = 'Claim and Work on it';
		if($user['hdrut_value'] != 'iassist_admin_user') {
			$actions[9] = 'Claim and Escalate';
		}


		//Put the users into the action array
		foreach($involved_users as $key => $val) {
			$return = 10;
			$actions[$return + $val['user_id']] = 'Claim and Return to '.$val['user_name'].' ('.$val['hdc_name'].')';
		}


		//Find all the other admin users and set up the functionality for an assignment
		//Find the admin users and send them a message
		$sql = 'SELECT * FROM helpdesk_companies ';

		// Company Type
		$sql .= 'INNER JOIN helpdesk_company_types ';
		$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

		// Company helpdesk settings
		$sql .= 'INNER JOIN helpdesk_company_settings ';
		$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

		$sql .= 'INNER JOIN helpdesk_admin_management ';
		$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

		$sql .= 'INNER JOIN helpdesk_request_settings ';
		$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

		//Admin users
		$sql .= 'INNER JOIN helpdesk_users ';
		$sql .= 'ON helpdesk_users.user_company = helpdesk_companies.hdc_id ';

		$sql .= 'INNER JOIN helpdesk_user_types ';
		$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

		$sql .= 'INNER JOIN helpdesk_user_admin_types ';
		$sql .= 'ON helpdesk_user_admin_types.hduat_userid = helpdesk_users.user_id ';

		$sql .= 'INNER JOIN helpdesk_admin_types ';
		$sql .= 'ON helpdesk_admin_types.hdat_id = helpdesk_user_admin_types.hduat_admin_type_id ';

		// Clauses
		$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$user['hdc_cc'].'\'';
		$sql .= 'AND helpdesk_users.user_id NOT IN ('.$user['user_id'].') ';
		$company_admins = $helper->mysql_fetch_all($sql);

		//Put them in the action array
		foreach($company_admins as $key => $val) {
			// Send notification email to the admins of this company, notifying them of the new request
			$actions['_assign_request_to_'.$val['user_id']] = 'Assign to '.$val['user_name'].' ('.$val['hdc_name'].')';
		}

		//If Iassist, set up an option to terminate silently
		if($user['hdrut_value'] == 'iassist_admin_user') {
			$actions['_terminate_request_silently_'] = 'Silent Termination';
		}
	}

	if($user_roles === false && isset($request_user_records)) {
		$current_status = $request_user_records['hdru_status'];
	} else {
		$current_status = $user_roles['hdru_status'];
	}

}

//Status
$sql = 'SELECT * FROM helpdesk_list_user_status ';
$sql .= 'WHERE helpdesk_list_user_status.hlus_id = '.(!isset($current_status) ? '1' : $current_status).' ';
$status = $helper->mysql_fetch_one($sql);

//Check if this request was launched on behalf of another company
$sql = 'SELECT * FROM helpdesk_request_company ';

$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_request_company.hdrc_company_id ';
$sql .= 'WHERE helpdesk_request_company.hdrc_hdr_id = '.$_GET['hdrid'].' ';

$helpdesk_request_company = $helper->mysql_fetch_one($sql);

$is_this_request_for_another_company = false;
if(isset($helpdesk_request_company) && is_array($helpdesk_request_company) && count($helpdesk_request_company) > 0) {
	$is_this_request_for_another_company = true;
}

//Is the request dead
$dead_request = false;
if($request['hdr_add_status'] == 6 || $request['hdr_add_status'] == 7 || $request['hdr_add_status'] == 8) {// Cancelled, Completed, Closed
	$dead_request = true;
}
?>
<?php if(($dead_request === false /*&& $is_pipeline_user === true*/) || ($dead_request === false /*&& (is_null($request_user_records) || $request_user_records===false)*/)) {//Don't show the update form for dead requests ?>
	<div id="div_right" class="float" style="margin-left: 15%;">
		<form name="update_request" method="post" enctype="multipart/form-data">
			<input type="hidden" id="hdrid" name="hdrid" value="<?php echo $_GET['hdrid']; ?>">
			<input type="hidden" id="object_id" name="object_id" value="<?php echo $_GET['hdrid']; ?>">
			<input type="hidden" id="assigned_req" name="assigned_req"
				   value="<?php echo($assigned_req == true ? 'true' : 'false'); ?>">
			<input id="user_id" class="user_id" name="user_id" type="hidden" value="<?php echo $user['user_id']; ?>">
			<table id="tbl_action" class="form">
				<tbody>
				<!-- Update Form - START -->
				<tr>
					<th id="th_hdrup_description">Update Text:</th>
					<td>
						<?php $js .= $displayObject->drawFormField('TEXT', array('id' => 'hdrup_description', 'name' => 'hdrup_description', 'required' => true, 'req' => true, 'unspecified' => false)); ?>
					</td>
				</tr>

				<tr>
					<th>Attach Document(s):</th>
					<td id="attach_containers">
						<?php $js .= $displayObject->drawFormField('ATTACH', array('id' => 'hdrup_attachment', 'name' => 'hdrup_attachment', 'required' => false, 'action' => 'REQUEST.UPDATE', 'page_direct' => 'manage_view_object.php?hdrid='.$request['hdr_id'].'&')); ?>
					</td>
				</tr>
				<!-- Update Form - END -->

				<?php if($show_time_field == true) { ?>
					<!-- Log the amount of time spent - START -->
					<tr>
						<th>Time Spent:</th>
						<td>
							<input id="ht_amount" name="ht_amount" type="number" min="0"/> Minutes
						</td>
					</tr>
					<!-- Log the amount of time spent - END -->
				<?php } ?>

				<!-- Decide On An Action To Take - START -->
				<tr>
					<th id="th_helpdesk_request_topicid">Action:</th>
					<td>
						<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdrup_action', 'name' => 'hdr_action', 'required' => true, 'req' => true, 'unspecified' => false, 'options' => $actions)); ?>
					</td>
				</tr>
				<!-- Decide On An Action To Take - END -->
				<?php if(strtoupper($_SESSION['cc']) == 'IASSIST') { ?>
					<?php
					//RESOLUTION TYPE CODE
					$sql = 'SELECT * FROM helpdesk_list_resolution_type ';
					$resolution_type = $helper->mysql_fetch_all($sql);

					$res_type_array = array();
					foreach($resolution_type as $key => $val) {
						$res_type_array[$val['hlrt_id']] = $val['hlrt_name'];
					}
					?>
					<!-- Decide On An Action To Take - START -->
					<tr class="res_type_tr">
						<th id="th_helpdesk_request_topicid">Resolution Type:</th>
						<td>
							<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdrup_res_type', 'name' => 'hdrup_res_type', 'required' => true, 'req' => true, 'unspecified' => false, 'options' => $res_type_array)); ?>
						</td>
					</tr>
					<!-- Decide On An Action To Take - END -->
				<?php } ?>

				<tr>
					<th></th>
					<td>
						<input id="btn_submit" type="button" value="Submit" data-hdrid="<?php echo $_GET['hdrid']; ?>"
							   class="isubmit i-am-the-submit-button">
						<input type="reset" value="Reset" name="B2">
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>
<?php } ?>

<div id="div_left">
	<table id="tbl_action" class="form">
		<tbody>
		<!-- Helpdesk request information  - START -->
		<tr class="">
			<th>Ref:</th>
			<td><?php echo strtoupper($request['hdc_cc']).'/HDR'.$request['hdr_id']; ?><input type="hidden" id="hdrid"
																							  name="hdrid"
																							  value="<?php echo $_GET['hdrid']; ?>">
			</td>
			<input id="user_id" class="user_id" name="user_id" type="hidden" value="<?php echo $user['user_id']; ?>">
		</tr>
		<tr>
			<th id="th_hdr_topic">Owner:</th>
			<td>
				<?php echo $request['user_name'].' ('.$request['hdc_name'].')'; ?>
			</td>
		</tr>
		<tr>
			<th id="th_hdr_topic">Date:</th>
			<td>
				<?php echo $request['hdr_add_date']; ?>
			</td>
		</tr>
		<tr>
			<th id="th_hdr_topic">Topic:</th>
			<td>
				<?php echo(isset($request['topic_value']) ? $request['topic_value'] : $request['value']); ?>
			</td>
		</tr>
		<tr>
			<th id="th_hdr_module">Module:</th>
			<td>
				<?php echo(isset($request['module_value']) ? $request['module_value'] : (isset($request['hdr_topicid']) && (int)$request['hdr_topicid'] == 6 ? 'New Client DB' : 'Request For Information')); ?>
			</td>
		</tr>

		<?php if($is_this_request_for_another_company == true) { ?>
			<tr>
				<th id="th_hdr_module">On Behalf Of:</th>
				<td>
					<?php echo $helpdesk_request_company['hdc_name']; ?>
				</td>
			</tr>
		<?php } ?>

		<tr>
			<th id="th_hdr_description">Description:</th>
			<td>
				<?php
				$hdr_description = $request['hdr_description'];
				$hdr_description = str_replace(chr(10), "<br/>", $hdr_description);
				?>
				<?php echo $hdr_description; ?>
			</td>
		</tr>

		<tr>
			<th id="th_hdr_description">Attachments:</th>
			<td>
				<!--                --><?php //echo $request['hdr_attachment']; ?>
				<?php
				$something_else = $displayObject->getAttachForDisplay($request['hdr_attachment'], false, 'REQUEST', $request['hdr_id'], true);
				echo $something_else;
				//                    $js .= $something_else['js'];
				$js .= $displayObject->getAttachmentDownloadJS('REQUEST', $request['hdr_id']);
				?>

			</td>
		</tr>

		<tr>
			<th id="th_hdr_description">Status:</th>
			<td>
				<?php echo $status['hlus_value']; ?>
			</td>
		</tr>

		<?php if($requestObject->requestAlreadyHasResolutionType($request)) { ?>
			<tr>
				<th id="th_hdr_description">Resolution Type:</th>
				<td>
					<?php
					$res_type_link = $requestObject->getRequestResolutionTypeLink($request);
					echo $res_type_link['hlrt_name'];
					?>
				</td>
			</tr>
		<?php } ?>

		<tr>
			<th id="th_hdr_description">Assigned User:</th>
			<td>
				<?php echo($assigned_user['user_name'] != '' ? $assigned_user['user_name'].' ('.$assigned_user['hdc_name'].')' : 'Currently Unassigned'); ?>
			</td>
		</tr>
		<!-- Helpdesk request information  - END -->

		<!-- Total Time Logged So Far - START -->
		<?php if($show_time_field == true) { ?>
			<?php
			$sql = 'SELECT SUM(ht_amount) AS total_time FROM helpdesk_request_updates ';

			$sql .= 'INNER JOIN helpdesk_users ';
			$sql .= 'ON helpdesk_users.user_id = helpdesk_request_updates.hdrup_userid ';

			$sql .= 'INNER JOIN helpdesk_companies ';
			$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

			$sql .= 'INNER JOIN helpdesk_time ';
			$sql .= 'ON helpdesk_time.ht_hdru_id = helpdesk_request_updates.hdrup_id ';

			$sql .= 'INNER JOIN helpdesk_user_types ';
			$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

			$sql .= 'WHERE  helpdesk_request_updates.hdrup_hdrid = '.$_GET['hdrid'].' ';
			$sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'].'\' ';
			$total_time_logged = $helper->mysql_fetch_all($sql);

//            echo '<pre style="font-size: 18px">';
//            echo '<p>PRINTHERE</p>';
//            print_r($total_time_logged);
//            echo '</pre>';
			?>
			<?php if((isset($total_time_logged) && is_array($total_time_logged) && count($total_time_logged) > 0) && $total_time_logged[0]['total_time'] != null) { ?>
				<tr>
					<th id="th_ht_amount">Time Spent:</th>
					<td>
						<?php echo $total_time_logged[0]['total_time'].' Minutes'; ?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
		<!-- Total Time Logged So Far  - END -->

		</tbody>
	</table>

	<?php if((isset($show_edit_button) && $show_edit_button == true) && $dead_request == false && false) { ?>
		<div id="edit_hdr" class="edit_hdr" style="margin-top: 5px">
			<input type="button" value="EDIT HDR<?php echo $_GET['hdrid']; ?>" ref="3"
				   data-hdrid="<?php echo $_GET['hdrid']; ?>" class="btn_edit">
		</div>
	<?php } ?>
</div>


<div id=div_error class=div_frm_error>

</div>

<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		//Form Submit
		$('#btn_submit').click(function () {
			console.log($('form').serialize());


			var count = 0;
			$('input:file').each(function () {
				if ($(this).val().length > 0) {
					count++;
				}
			});

			//Change redirect address depending on action - 17 january 2018
			var helpdesk_action = $('#hdrup_action').val();
			var redirect_address = 'manage_view_object.php?hdrid=' + $(this).data('hdrid') + '&';
			if (helpdesk_action == 5) {
				redirect_address = 'manage.php?';
			}

			//Claim and Multi-Step Process - 17 January 2018
			var controller_action = 'REQUEST.UPDATE';
			if ($('#assigned_req').val() == 'false') {
				controller_action = 'REQUEST.CLAIMANDPROCESS';

				var assign_index_of = helpdesk_action.indexOf('assign_request_to');
				if (assign_index_of > 0) {
					controller_action = 'REQUEST.ASSIGNTOADMIN';
					redirect_address = 'manage.php?';
				}
			}

			var assign_index_of = helpdesk_action.indexOf('transfer_request_to');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.TRANSFERTOADMIN';
				redirect_address = 'manage.php?';
			}

			var assign_index_of = helpdesk_action.indexOf('reclaim_pending_transfer_');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.RECLAIMPENDINGTRANSFER';
			}

			var assign_index_of = helpdesk_action.indexOf('reject_transfer');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.REJECTTRANSFER';
				redirect_address = 'manage.php?';
			}

			var assign_index_of = helpdesk_action.indexOf('accept_transfer');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.ACCEPTANDPROCESS';
			}

			var assign_index_of = helpdesk_action.indexOf('terminate_request_silently');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.SILENTTERMINATION';
				redirect_address = 'manage.php?';
			}

			var assign_index_of = helpdesk_action.indexOf('seize_and_assume_responsibility');
			if (assign_index_of > 0) {
				controller_action = 'REQUEST.SEIZEANDASSUMEREPOSIBILITY';
			}

			if (count > 0) {
				HelpdeskHelper.processObjectFormWithAttachment($('form[name="update_request"]'), controller_action, redirect_address)
			} else {
				HelpdeskHelper.processObjectForm($('form[name="update_request"]'), controller_action, redirect_address)
			}
		});


		$('#hdrup_action').change(function (e) {
			if ($('.res_type_tr')) {
				$('#hdrup_res_type').removeAttr("req");
				$('.res_type_tr').hide();
			}

			if ($('#hdrup_action').val() == 5) {
				if ($('.res_type_tr')) {
					$('.res_type_tr').show();
					$('#hdrup_res_type').attr("req", "1");
				}
			}
		});

		if ($('.res_type_tr')) {
			$('#hdrup_res_type').removeAttr("req");
			$('.res_type_tr').hide();
		}


		//Edit Redirect
		$('.btn_edit').click(function (e) {
			e.preventDefault();
			console.log('Go to the edit page');
			document.location.href = 'manage_view_object_edit.php?hdrid=' + $(this).data('hdrid');
		});
	});
</script>

<?php require_once('inc_footer.php'); ?>

<?php
//This is the request history
$sql = 'SELECT * FROM helpdesk_request_updates ';

$sql .= 'INNER JOIN helpdesk_users ';
$sql .= 'ON helpdesk_users.user_id = helpdesk_request_updates.hdrup_userid ';

$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';

$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';

$sql .= 'WHERE  helpdesk_request_updates.hdrup_hdrid = '.$_GET['hdrid'].' ';
$sql .= ' ORDER BY helpdesk_request_updates.hdrup_datetime DESC';
$history = $helper->mysql_fetch_all($sql);


//echo '<pre style="font-size: 18px">';
//echo '<p>HISTORY</p>';
//print_r($history);
//echo '</pre>';

?>

<h3 style="clear: both">Activity Log</h3>

<?php if(count($history) > 0) { ?>
	<table>
		<tr>
			<th width="200">User:</th>
			<th width="200">Company:</th>
			<th width="200">What Happened:</th>
			<th width="200">Date:</th>
			<th width="200">Message Text:</th>
			<th width="200">Attachment:</th>

			<!-- This displays the log of what actually happens at the database level - This is not to be displayed to the user, so get rid  of it when it's time to go live -->
			<!--        <th width="200">What Changed - DEV ONLY</th>-->
			<!-- This displays the log of what actually happens at the database level - This is not to be displayed to the user, so get rid  of it when it's time to go live -->
		</tr>

		<?php $js = ''; ?>
		<?php foreach($history as $key => $val) { ?>
			<tr>
				<td class="top">
					<?php echo $val['user_name']; ?>
				</td>

				<td class="top">
					<?php echo $val['hdc_name']; ?>
				</td>

				<td class="top">
					<?php
					$user_changes = $val['hdrup_user_changes'];
					$user_changes = str_replace('assigned_user', 'assigned user', $user_changes);
					?>
					<?php echo $user_changes; ?>
				</td>

				<td class="top">
					<?php echo $val['hdrup_datetime']; ?>
				</td>

				<td class="top">
					<?php
					//JC - catch for accidental double character encodings in the logs
					$update_description = ASSIST_HELPER::decode($val['hdrup_response']);
					$update_description = str_replace(chr(10), "<br />", $update_description);

					echo $update_description; ?>
				</td>

				<td class="top">
					<?php
					$something_else_again = $displayObject->getAttachForDisplay($val['hdrup_attachment'], false, 'REQUEST_UPDATE', $val['hdrup_id'], true);
					echo $something_else_again;
					//                    $js .= $something_else['js'];
					$js .= $displayObject->getAttachmentDownloadJS('REQUEST_UPDATE', $val['hdrup_id']);
					?>
				</td>


				<!-- This displays the log of what actually happens at the database level - This is not to be displayed to the user, so get rid  of it when it's time to go live -->
				<!--            <td class="top">-->
				<!--                <ol>-->
				<!--                --><?php
				//                    $system_changes = unserialize($val['hdrup_system_changes']);
				//                    if(is_array($system_changes) && count($system_changes) > 0){
				//                        foreach($system_changes as $key => $val){
				//                            echo '<li>'. $val .'</li>';
				//                        }
				//                    }
				//                ?>
				<!--                </ol>-->
				<!--            </td>-->
				<!-- This displays the log of what actually happens at the database level - This is not to be displayed to the user, so get rid  of it when it's time to go live -->

			</tr>
		<?php }//Endfor ?>
	</table>
<?php } else {
	echo '<p>No historic activity to display.</p>';
}//Endif ?>

<script type="text/javascript">
	<?php echo $js; ?>
</script>