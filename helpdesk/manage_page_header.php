<?php

ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r'])? $_REQUEST['r']: array()));

function getCurrentHelpdeskUser($helper){
    $sql = 'SELECT * FROM helpdesk_users ';
    $sql .= 'INNER JOIN helpdesk_companies ';
    $sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
    $sql .= 'INNER JOIN helpdesk_user_types ';
    $sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
    $sql .= 'INNER JOIN helpdesk_table_view ';
    $sql .= 'ON helpdesk_table_view.htv_id = helpdesk_users.user_table_view ';
    $sql .= 'WHERE helpdesk_users.user_tkid = '.$_SESSION['tid'] . ' ';
    $sql .= 'AND helpdesk_companies.hdc_cc = \''.$_SESSION['cc'] . '\'';
    $user = $helper->mysql_fetch_one($sql);
    return $user;
}

$current_user = getCurrentHelpdeskUser($helper);

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($current_user);
//echo '</pre>';

$show_detailed = true;

        //03. Determine whether the the button to make this the default view should appear or not


?>

<?php if($current_user){// If the user exists in the helpdesk db ?>
    <?php if(!($current_user['hdrut_value'] == 'client_admin_user' || $current_user['hdrut_value'] == 'reseller_admin_user' || $current_user['hdrut_value'] == 'iassist_admin_user')){//Is this an admin user ?>
        <?php
            //Show the buttons and start making decisions
            $show_default_save = false;

            //01. Then Lets have a look at whether there is a get variable with the either of the options available
            if(isset($_GET['table_view'])){
                //02. Show the requested table view
                $table_view = (int)$_GET['table_view'];

//                echo '<pre style="font-size: 18px">';
//                echo '<p>PRINTHERE</p>';
//                print_r($_GET['table_view']);
//                echo '</pre>';
            }else{
                //02. Show the preferred table_view
                $table_view = (int)$current_user['htv_id'];
            }

            if($table_view != 1){
                $show_detailed = false;
            }

            if($table_view != (int)$current_user['htv_id']){
                //Show the button
                $show_default_save = true;
            }
        ?>
        <div id="m1" style="font-size: 8pt; font-weight: bold;" class="ui-buttonset">
            <input type="radio" id="24" name="radiom1" value="manage.php?table_view=1" <?php echo ($show_detailed ? 'checked="checked"' : '' ) ?>  class="ui-helper-hidden-accessible">
            <label for="24" class="<?php echo ($show_detailed ? 'ui-state-active ' : '' ) ?>ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false">
                <span class="ui-button-text">&nbsp;&nbsp;Detailed View&nbsp;&nbsp;</span>
            </label>
            &nbsp;&nbsp;
            <input type="radio" id="25" name="radiom1" value="manage.php?table_view=2" <?php echo (!$show_detailed ? 'checked="checked"' : '' ) ?> class="ui-helper-hidden-accessible">
            <label for="25" class="<?php echo (!$show_detailed ? 'ui-state-active ' : '' ) ?>ui-button ui-widget ui-state-default ui-button-text-only" role="button" aria-disabled="false">
                <span class="ui-button-text">&nbsp;&nbsp;Simple View&nbsp;&nbsp;</span>
            </label>
            &nbsp;&nbsp;
            &nbsp;&nbsp;
            &nbsp;&nbsp;

            <?php if($show_default_save){// If the user exists in the helpdesk db ?>
            <input type="radio" id="27" name="radiom1" value="<?php echo $table_view ?>" class="ui-helper-hidden-accessible">
            <label for="27" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right" role="button" aria-disabled="false">
                <span class="ui-button-text">&nbsp;&nbsp;Make This My Default View&nbsp;&nbsp;</span>
            </label>
            <?php } ?>
        </div>
        <br/>
    <?php }elseif($current_user['hdrut_value'] == 'iassist_admin_user'){ ?>
        <div id="m1" style="font-size: 8pt; font-weight: bold;" class="ui-buttonset">
            <input type="radio" id="24" name="radiom1" value="manage_view.php?status=nonsense&view_all=true" <?php echo ($show_detailed ? 'checked="checked"' : '' ) ?>  class="ui-helper-hidden-accessible">
            <label for="24" class="<?php echo ($show_detailed ? 'ui-state-active ' : '' ) ?>ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false">
                <span class="ui-button-text">&nbsp;&nbsp;View All Open Requests&nbsp;&nbsp;</span>
            </label>
            &nbsp;
            &nbsp;
            <input type="radio" id="25" name="radiom1" value="manage_view.php?status=nonsense&view_all_complete=true" <?php echo ($show_detailed ? 'checked="checked"' : '' ) ?>  class="ui-helper-hidden-accessible">
            <label for="25" class="<?php echo ($show_detailed ? 'ui-state-active ' : '' ) ?>ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" role="button" aria-disabled="false">
                <span class="ui-button-text">&nbsp;&nbsp;View All Complete Requests&nbsp;&nbsp;</span>
            </label>
        </div>
        <br/>
    <?php } ?>
    <script type=text/javascript>
        $(function() {
            $("#m1").buttonset();
            $("#m1 input[type='radio']").click( function(e) {
                e.preventDefault();
                var value = $(this).val();

                if($.isNumeric(value)){
                    saveDefaultView(value);
                }else{
                    document.location.href = value;
                }

            }).children('.ui-button-hover').hover(
                function() {
                    var h = $(this).attr('id');
                    alert(h);
                    if(h.length>0) {
                        AssistHelper.showHelp(h);
                    }
                },
                function() {
                    AssistHelper.hideHelp();
                }
            );


            function saveDefaultView(value){
                var page_id = value;
                var dta = {page_id : page_id};

                var action = 'USERS.SAVEDEFAULTVIEW';

                var url = 'inc_controller.php?action='+action;

                var result = AssistHelper.doAjax(url, dta);

                var redirect_url = 'manage.php?r[]=' + result[0] + '&r[]=' + result[1] + '&';
                document.location.href = redirect_url;
            }
        });
    </script>
<?php } ?>
