<script type="text/javascript">
    $(function(){

        $('a.breadcrumb').click(function(e){
            e.preventDefault();
            var redirect_url = $(this).attr('href');

            if(redirect_url == 'manage_view.php'){
                redirect_url = 'manage_view.php?status=' + $(this).data('status');

                if($(this).data('status') != 'undefined'){
                    redirect_url += '&section=' + $(this).data('section');
                }

                if($(this).data('table_view') != 'undefined'){
                    redirect_url += '&table_view=' + $(this).data('table_view');
                }

                if($(this).data('view_all') != 'undefined'){
                    redirect_url += '&view_all=' + $(this).data('view_all');
                }

                if($(this).data('view_all_complete') != 'undefined'){
                    redirect_url += '&view_all_complete=' + $(this).data('view_all_complete');
                }

            }else{}
            document.location.href = redirect_url;

        });
    });
</script>
</body>
</html>