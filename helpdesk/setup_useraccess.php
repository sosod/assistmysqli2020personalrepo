<?php
require_once("inc_header.php");

$userObject = new HELPDESK_USERACCESS();
$user_access_fields = $userObject->getUserAccessFields();
$user_access_defaults = $userObject->getUserAccessDefaults();
$user_access_types = $userObject->getUserAccessTypes();
$user_access_sources = $userObject->getUserAccessSources();

$users_not_set = $userObject->getUsersWithNoAccess();
$active_users = $userObject->getActiveUsers();
$js = "";


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_add>
	<table class=list id=tbl_useraccess>
		<tr>
			<th></th>
			<th>User</th>
			<?php
			foreach($user_access_fields as $fld => $name){
				echo "<th>".str_replace(" ","<br />",$name)."</th>";
			}
			?>
			<th></th>
		</tr> 
	<?php if(count($users_not_set)>0) { ?>
		<tr>
			<td></td>
			<td><select name=ua_tkid><option value=X selected>--- SELECT ---</option><?php
			foreach($users_not_set as $id=>$name){
				echo "<option value='".$id."'>".$name."</option>";
			}
			?></select></td>
			<?php 
			foreach($user_access_fields as $fld=>$name){
				echo "<td>";
				$type = isset($user_access_types[$fld]) ? $user_access_types[$fld] : "BOOL_BUTTON";
				$user_access_types[$fld] = $type;
				switch($type) {
					case "LIST":
						$options = array('id'=>$fld,'name'=>$fld,'options'=>$user_access_sources[$fld],'unspecified'=>false);
						break;
					case "BOOL_BUTTON":
					default:
						$options = array('id'=>$fld,'yes'=>1,'no'=>0,'form'=>"vertical");
						break;
				}
				$js.= $displayObject->drawFormField($type,$options,$user_access_defaults[$fld]);
				echo "</td>";
			} 
			?>
			<td><button id=btn_add class='btn_green format_btn'>Add</button></td>
		</tr>
	<?php
	}
	foreach($active_users as $ua) {
		echo "
		<tr>
			<td>".$ua['tkid']."</td>
			<td>".$ua['name']."</td>";
		foreach($user_access_fields as $fld => $name) {
			switch($user_access_types[$fld]) {
				case "LIST":
					echo "<td>".$ua[$fld]."</td>";
					break;
				case "BOOL_BUTTON":
				default:
					echo "<td style='text-align: center'>".$helper->getDisplayIcon(($ua[$fld]==1?"ok":"error"))."</td>";
					break;
			}
		}
		echo "
			<td><button class='format_btn btn_edit' id='".$ua['ua_id']."' >Edit</button></td>
		</tr>";
	}
	?>
	</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"user",array('section'=>"USER")); ?></td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		<?php echo $js; ?>
		$("#tbl_useraccess tr").not("tr:first").next().find("span.ui-icon").css("margin","0 auto");
		//$("span.ui-icon").css("margin","0 auto");
		$("#btn_add").click(function(){
			var err_msg = new Array();
			var err = false;
			AssistHelper.processing();
			if($("select[name=ua_tkid]").val()=="X") {
				err_msg.push("Please select a user.");
				err = true;
			} 
			if(parseInt($("table#tbl_useraccess select[name=ua_benefit_category]").val())==0 && parseInt($("table#tbl_useraccess input#ua_traveller").val())!=0) {
				err_msg.push("A Traveller must have a Benefit Category assigned to them.");
				err = true;
			} 
			if(parseInt($("table#tbl_useraccess input#ua_create_own").val())!=0 && parseInt($("table#tbl_useraccess input#ua_traveller").val())==0) {
				err_msg.push("A user cannot create their own Travel Requests unless they are also a Traveller.");
				err = true;
			} 
			if(err) {
				var err_result = "The following errors have occurred:<ul><li>"+AssistString.implode("</li><li>",err_msg)+"</li></ul>";
				var result = ["error",err_result];
				AssistHelper.finishedProcessing(result[0],result[1]);
			} else {
				var dta = AssistForm.serialize($("form[name=frm_add]"));
				//console.log(dta);
				//alert(dta);
				//var result = ["error","testing"];
				var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Add",dta);
				//console.log(result);
				if(result[0]=="ok") {
					document.location.href = "setup_useraccess.php?r[]="+result[0]+"&r[]="+result[1];
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
			return false;
		});
	});
</script>
<div id=dlg_edit title="Edit User">
	<h1 style="margin-top:3px;">Edit: <span id=spn_edit></span></h1>
	<form name=frm_edit>
		<input type=hidden value="" id=ua_id name=ua_id />
		<table class=dialog-form id=tbl_edit>
			<?php 
			foreach($user_access_fields as $fld=>$name){
				echo "<tr><th>".str_ireplace(" ", "&nbsp;", $name).":</th>";
				echo "<td>";
				//$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"edit_".$fld,'name'=>$fld,'yes'=>1,'no'=>0,'form'=>""),$user_access_defaults[$fld]);
				$type = isset($user_access_types[$fld]) ? $user_access_types[$fld] : "BOOL_BUTTON";
				$user_access_types[$fld] = $type;
				switch($type) {
					case "LIST":
						$options = array('id'=>"edit_".$fld,'name'=>$fld,'options'=>$user_access_sources[$fld],'unspecified'=>false);
						break;
					case "BOOL_BUTTON":
					default:
						$options = array('id'=>"edit_".$fld,'name'=>$fld,'yes'=>1,'no'=>0,'form'=>"horizontal");
						break;
				}
				$js.= $displayObject->drawFormField($type,$options,$user_access_defaults[$fld]);
				echo "</td>
				</tr>";
			} 
			?>
		</table>
</div>
<script type="text/javascript">
	$(function() {
		
		var win = AssistHelper.getWindowSize();
		//console.log(win);
		var h = win.height;
		//console.log(h);
		var th = ($("table#tbl_edit").css("height"));
		th = ((th.substr(0,th.length-2))*1)+150;
		if(h>th) {
			h = th;
		}
		
		//$("table#tbl_edit").css("border","0px solid #ffffff");
		//$("table#tbl_edit th").css("border","0px solid #ffffff");
		//$("table#tbl_edit td").css("border","0px solid #ffffff");
		//$("table#tbl_edit tr").hover(function() {
		//	$(this).find("th, td").css("background-color","#f0f0f0");
		//},function() {
		//	$(this).find("th, td").css("background-color","");
		//});
		$("#dlg_edit").dialog({
			width: "550px",
			height: (h-10),
			autoOpen: false,
			modal: true,
			buttons:[{
				text:"Save Changes",
				icons: {primary:"ui-icon-disk"},
				click:function() {
					AssistHelper.processing();
					var err = false;
					var err_msg = new Array();

					if($("table#tbl_edit select[name=ua_tkid]").val()=="X") {
						err_msg.push("Please select a user.");
						err = true;
					} 
					if(parseInt($("table#tbl_edit select[name=ua_benefit_category]").val())==0 && parseInt($("table#tbl_edit input#edit_ua_traveller").val())!=0) {
						err_msg.push("A Traveller must have a Benefit Category assigned to them.");
						err = true;
					} 
					if(parseInt($("table#tbl_edit input#edit_ua_create_own").val())!=0 && parseInt($("table#tbl_edit input#edit_ua_traveller").val())==0) {
						err_msg.push("A user cannot create their own Travel Requests unless they are also a Traveller.");
						err = true;
					} 
					if(err) {
						var err_result = "The following errors have occurred:<ul><li>"+AssistString.implode("</li><li>",err_msg)+"</li></ul>";
						var result = ["error",err_result];
						AssistHelper.finishedProcessing(result[0],result[1]);
					} else {
						var dta = AssistForm.serialize($("form[name=frm_edit]"));
						//console.log(dta);
						var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Edit",dta);
						if(result[0]=="ok") {
							document.location.href = "setup_useraccess.php?r[]="+result[0]+"&r[]="+result[1];
						} else {
							AssistHelper.finishedProcessing(result[0],result[1]);
						}
					}
				}
			},{
				text:"Cancel",
				click: function(){
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id","dlg_edit");
		AssistHelper.formatDialogButtons($("#dlg_edit"),0,AssistHelper.getDialogSaveCSS());
		AssistHelper.formatDialogButtonIcon($("#dlg_edit"),0,"green");
		AssistHelper.formatDialogButtons($("#dlg_edit"),1,AssistHelper.getCloseCSS());
		/*$("#dlg_edit").dialog({
			autoOpen: false
			modal:true,
			autoOpen:false,
			width: "550px",
		});*/
		$("button.btn_edit").button({
			icons:{primary: "ui-icon-pencil"}
		}).click(function(e) {
			e.preventDefault();
			var i = $(this).prop("id");
			$("#dlg_edit #ua_id").val(i);
			var tk = $(this).parent().parent().find("td:eq(1)").html();
			//$("#dlg_edit #td_user").html(tk);
			$("#dlg_edit #spn_edit").html(tk);
			
			var data = AssistHelper.doAjax("inc_controller.php?action=UserAccess.getAUserAccess","id="+i+"&process=0");
			//console.log(data);
			$("form[name=frm_edit]").find("select, input").each(function() {
				var i = $(this).prop("id");
				if(i!="ua_id") {
					i = AssistString.substr(i,5);
					//console.log(i+" = "+data[i]);
					if($(this).is("select")) {
						$(this).val(data[i]);
					} else {
						if(data[i]==1) {
							$("table#tbl_edit #edit_"+i+"_yes").trigger("click");
						} else {
							$("table#tbl_edit #edit_"+i+"_no").trigger("click");
						}
					}
				}
			});
			
			$("#dlg_edit").dialog("open");			
			//var i = 0;
			/*$(this).parent().parent().find("td:gt(1)").each(function() {
				if($(this).find("span").hasClass("ui-icon-check")) {
					$("#tbl_edit tr:eq("+i+") td button.btn_yes").trigger("click");
				} else {
					$("#tbl_edit tr:eq("+i+") td button.btn_no").trigger("click");
				}
				i++;
			});*/
			//AssistHelper.focusDialogButtons($("#dlg_edit"),1);
			//AssistHelper.blurDialogButtons($("#dlg_edit"),1);
		});
		
		
		$("button#btn_add").button({
			icons:{primary: "ui-icon-circle-plus"}
		})
		.click(function() {
			//nothing needed here - form submit kicks in
		});
	});
</script>