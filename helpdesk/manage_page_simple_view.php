<?php
//Mock Up
$table_width = 600;

function displayList($helper) {
    $status = getStatuses($helper);
    $current_user = getCurrentHelpdeskUser($helper);

//    echo '<pre style="font-size: 18px">';
//    echo '<p>REAL STATUSES</p>';
//    print_r($status);
//    echo '</pre>';

    $simple_status = array();
    //"Awaiting My Response" SIMPLE STATUS
    $simple_status[0]['status_name'] = 'Awaiting My Response';
    $simple_status[0]['status_collection'][1] = $status[3];
    $simple_status[0]['status_collection'][2] = $status[5];

    //"New" SIMPLE STATUS
    $simple_status[1]['status_name'] = 'New';
    $simple_status[1]['status_collection'][1] = $status[1];
    $simple_status[1]['status_collection'][2] = $status[2];

    //"In Progress" SIMPLE STATUS
    $simple_status[2]['status_name'] = 'In Progress';
    $simple_status[2]['status_collection'][1] = $status[3];
    $simple_status[2]['status_collection'][2] = $status[4];
    $simple_status[2]['status_collection'][3] = $status[5];
    $simple_status[2]['status_collection'][4] = $status[9];

    //"Done" SIMPLE STATUS
    $simple_status[4]['status_name'] = 'Done';
    $simple_status[4]['status_collection'][2] = $status[6];
    $simple_status[4]['status_collection'][3] = $status[7];
    $simple_status[4]['status_collection'][4] = $status[8];


//    echo '<pre style="font-size: 18px">';
//    echo '<p>SIMPLE STATUSES</p>';
//    print_r($simple_status);
//    echo '</pre>';


    $sql = 'SELECT COUNT(*) AS status_count, hdr_add_status FROM helpdesk_request ';
    $sql .= 'WHERE hdr_add_userid = ' . $current_user['user_id'] . ' ';
    $sql .= 'GROUP BY hdr_add_status ';
    $request = $helper->mysql_fetch_all_by_id($sql, 'hdr_add_status');

//    echo '<pre style="font-size: 18px">';
//    echo '<p>REQUEST</p>';
//    print_r($request);
//    echo '</pre>';

    //Build the list
    $list = '<ul>';
    foreach($status as $key => $val) {
        $status_id = $val['hlus_id'];
        $status_name = $val['hlus_value'];

        $status_id_is_an_index_in_request_array = (thisArrayIsPopulated($request) ? array_key_exists($status_id, $request) : false );

        //Status Count For the Internal Requests
        if(thisArrayIsPopulated($request) && $status_id_is_an_index_in_request_array){
            $status_count = $request[$status_id]['status_count'];
        }else{
            $status_count = 0;
        }

        $list.= '<li><a href="manage_view.php" data-status="' . $status_id . '"  data-section="my_requests" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>' . $status_name. ' ('.$status_count.')</a></li>';

    }
    $list.= '</ul>';


    $list = '<ul>';
    foreach($simple_status as $key => $val) {
        $status_count = 0;

        foreach($val['status_collection'] as $key2 => $val2){
            $status_id = $val2['hlus_id'];
            $status_name = $val2['hlus_value'];

            $status_id_is_an_index_in_request_array = (thisArrayIsPopulated($request) ? array_key_exists($status_id, $request) : false );

            //Status Count For the Internal Requests
            if(thisArrayIsPopulated($request) && $status_id_is_an_index_in_request_array){
                $status_count += $request[$status_id]['status_count'];
            }else{
                $status_count += 0;
            }

        }

        $list.= '<li><a href="manage_view.php" data-status="' . $key . '" data-table_view="simple" ' . ((int)$status_count == 0 ? 'style="pointer-events: none; color: black;"' : '') . '>' . $val['status_name']. ' ('.$status_count.')</a></li>';

    }
    $list.= '</ul>';
    return $list;
}

function getStatuses($helper) {
    $sql = 'SELECT * FROM helpdesk_list_user_status';
    $status = $helper->mysql_fetch_all_by_id($sql, 'hlus_id');
    return $status;
}

function thisArrayIsPopulated($returned_array){
    $thisArrayIsPopulated = false;
    if(isset($returned_array) && is_array($returned_array) && count($returned_array) > 0){
        $thisArrayIsPopulated = true;
    }
    return $thisArrayIsPopulated;
}

$status = getStatuses($helper);

?>

<?php if($current_user){// If the user exists in the helpdesk db ?>
    <table width="<?php echo $table_width ?>">
        <tr>
            <th width="200">My Helpdesk Requests</th>
        </tr>
        <tr>
            <td class="top">
                <?php echo displayList($helper); ?>
            </td>
        </tr>
    </table>
<?php }else{ ?>
    <p>You are currently not associated with any Helpdesk Requests</p>
<?php } ?>


<script type="text/javascript">
    $(function(){

        $('ul li a').click(function(e){
            e.preventDefault();
            console.log('Done clicked this');
            document.location.href = 'manage_view.php?status=' + $(this).data('status') + '&table_view=' + $(this).data('table_view');
        });
    });
</script>
