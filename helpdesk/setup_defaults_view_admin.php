<?php require_once('inc_header.php'); ?>
<?php
$user_id = $_GET['user_id'];

//Check if user exists in the helpdesk db
$sql = 'SELECT * FROM helpdesk_users ';
$sql .= 'INNER JOIN helpdesk_companies ';
$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
$sql .= 'INNER JOIN helpdesk_user_types ';
$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
$sql .= 'WHERE helpdesk_users.user_id = '. $user_id . ' ';
$user = $helper->mysql_fetch_one($sql);

//echo '<pre style="font-size: 18px">';
//echo '<p>USER INFORMATION</p>';
//print_r($user);
//echo '</pre>';

$sql = 'SELECT * FROM assist_'. strtolower($user['hdc_cc']).'_timekeep ';
$sql .= 'WHERE tkstatus = \'1\' ';
$sql .= 'AND tkid = \'' . $user['user_tkid'] . '\' ';

$client_db = new ASSIST_MODULE_HELPER('client', strtolower($user['hdc_cc']));

$tk_details = $client_db->mysql_fetch_one($sql);

?>

<h2><?php echo $user['user_name']; ?> User Information</h2>
<table id="tbl_action" class="form">
    <tr>
        <th id="th_helpdesk_request_topicid">User Name:</th>
        <td>
            <?php echo $user['user_name']; ?>
        </td>
    </tr>
    <tr>
        <th id="th_helpdesk_request_topicid">Rank:</th>
        <td>
            <?php echo ($user['hdrut_value'] == 'reseller_admin_user' ? 'Reseller Admin User' : 'Another Type of User'); ?>
        </td>
    </tr>
    <tr>
        <th id="th_helpdesk_request_topicid">User ID:</th>
        <td>
            <?php echo $user['user_tkid']; ?>
        </td>
    </tr>
    <tr>
        <th id="th_helpdesk_request_topicid">Email:</th>
        <td>
            <?php echo $tk_details['tkemail']; ?>
        </td>
    </tr>
    <tr>
        <th id="th_helpdesk_request_topicid">Mobile:</th>
        <td>
            <?php echo $tk_details['tkmobile']; ?>
        </td>
    </tr>
    <tr>
        <th id="th_helpdesk_request_topicid">Tel:</th>
        <td>
            <?php echo $tk_details['tktel']; ?>
        </td>
    </tr>
</table>


<?php
//Find all of the companies that this company is a parent of
$sql = 'SELECT * FROM helpdesk_companies ';

// Company Type
$sql .= 'INNER JOIN helpdesk_company_types ';
$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

// Company helpdesk settings
$sql .= 'INNER JOIN helpdesk_company_settings ';
$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

$sql .= 'INNER JOIN helpdesk_admin_management ';
$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

$sql .= 'INNER JOIN helpdesk_request_settings ';
$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

// Clauses
$sql .= 'WHERE helpdesk_companies.hdc_parent = ' . $user['hdc_id'] . ' ';
$sql .= 'AND helpdesk_companies.hdc_active = 1 ';
$sql .= 'ORDER BY helpdesk_companies.hdc_name ASC';

$helpdesk_companies = $helper->mysql_fetch_all_by_id($sql, 'hdc_cc');


//echo '<pre style="font-size: 18px">';
//echo '<p>CHILDREN</p>';
//print_r($children);
//echo '</pre>';

$hdc_object = new HELPDESK_COMPANY();
$current_company_info = $hdc_object->getActiveCompanyInformation($_SESSION['cc']);

$isReseller = false;
if($current_company_info['hdct_value'] == 'reseller'){
    $isReseller = true;
}

//Get every registered company
$db = new ASSIST_DB('master');
$sql = 'SELECT cmpcode, cmpname, cmp_is_demo FROM assist_company ';
$sql .= 'WHERE cmpcode <> \'BLANK\' AND cmp_is_demo <> 1 ';
$sql .= 'AND (cmpstatus = \'Y\' OR cmpstatus = \'S\') ';
$sql .= 'ORDER BY cmpname ASC';
$ait_companies = $db->mysql_fetch_all($sql);

$child_company_id_array = array();
foreach($ait_companies as $key => $val){
    if(array_key_exists($val['cmpcode'], $helpdesk_companies)){
        $company_id = $helpdesk_companies[$val['cmpcode']]['hdc_id'];
    }elseif(array_key_exists(strtolower($val['cmpcode']), $helpdesk_companies)){
        $company_id = $helpdesk_companies[strtolower($val['cmpcode'])]['hdc_id'];
    }else{
        $company_id = 0;
    }

    if($company_id != 0){
        $child_company_id_array[$company_id] = $company_id;
    }
}

if(isset($child_company_id_array) && is_array($child_company_id_array) && count($child_company_id_array) > 0){
    $sql = 'SELECT * FROM helpdesk_company_admin_assignment ';
    $sql .= 'WHERE helpdesk_company_admin_assignment.hcaa_user_id = ' . $user['user_id'] . ' ';
    $sql .= 'AND helpdesk_company_admin_assignment.hcaa_hdc_id IN (' . implode(', ', $child_company_id_array) . ') ';
    $company_assignments = $helper->mysql_fetch_all_by_id($sql, 'hcaa_hdc_id');
}

//echo '<pre style="font-size: 18px">';
//echo '<p>COMPANY ASSIGNMENTs</p>';
//print_r($company_assignments);
//echo '</pre>';

?>

<h2>Child Company Assignments</h2>

<form name="admin_to_company_assignments" method="post" enctype="multipart/form-data">
    <table id="tbl_action">
        <tbody>
        <tr class="">
            <th>Company Name</th>
            <th>Company Code</th>
            <th>Assign <?php echo $user['user_name']; ?> To This Company</th>
            <input id="user_id" class="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>">
        </tr>
        <?php foreach($helpdesk_companies as $key => $value){ ?>
            <?php
                $name_and_id = 'hdc_id_' . $value['hdc_id'];
                $bool_button_value = (isset($company_assignments[$value['hdc_id']]) && is_array($company_assignments[$value['hdc_id']]) ? true : false);
            ?>
        <tr>
            <td><?php echo $value['hdc_name']; ?></td>
            <td><?php echo strtoupper($value['hdc_cc']); ?></td>
            <td><?php $js .= $displayObject->drawFormField('BOOL_BUTTON', array('id' => $name_and_id, 'name' => $name_and_id, 'required' => false), $bool_button_value); ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="3">
                <input id="btn_submit" type="button" value="Apply Changes" class="isubmit i-am-the-submit-button">
            </td>
        </tr>
        </tbody>
    </table>
</form>


<script type="text/javascript">
    $(function(){
        <?php echo $js; ?>

        $('#btn_submit').click(function(){
            console.log($('form').serialize());
            HelpdeskHelper.processObjectForm($('form[name="admin_to_company_assignments"]'), 'COMPANY.ASSIGNADMINTOCHILDCOMP', 'setup_defaults_view_admin.php?user_id=' + <?php echo $user_id; ?> + '&');
        });
    });
</script>


