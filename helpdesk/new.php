<?php require_once('inc_header.php'); ?>
<?php ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r']) ? $_REQUEST['r'] : array())); ?>
<?php
function getCurrentCompInfo($helper) {
	/*
	 * These are the company details
	 * Including:
	 * Company information
	 * Preferred way of handling the helpdesk request (settings)
	*/

	$sql = 'SELECT * FROM helpdesk_companies ';

	// Company Type
	$sql .= 'INNER JOIN helpdesk_company_types ';
	$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

	// Company helpdesk settings
	$sql .= 'INNER JOIN helpdesk_company_settings ';
	$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

	$sql .= 'INNER JOIN helpdesk_admin_management ';
	$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

	$sql .= 'INNER JOIN helpdesk_request_settings ';
	$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

	// Clauses
	$sql .= 'WHERE helpdesk_companies.hdc_cc = \''.$_SESSION['cc'].'\'';
	$company_info = $helper->mysql_fetch_one($sql);

	return $company_info;

}

function getCurrentCompChildren($company_info, $helper) {
	//Find all of the companies that this company is a parent of
	$sql = 'SELECT * FROM helpdesk_companies ';

	// Company Type
	$sql .= 'INNER JOIN helpdesk_company_types ';
	$sql .= 'ON helpdesk_company_types.hdct_id = helpdesk_companies.hdc_type ';

	// Company helpdesk settings
	$sql .= 'INNER JOIN helpdesk_company_settings ';
	$sql .= 'ON helpdesk_company_settings.hcs_company_id = helpdesk_companies.hdc_id ';

	$sql .= 'INNER JOIN helpdesk_admin_management ';
	$sql .= 'ON helpdesk_admin_management.hdam_id = helpdesk_company_settings.hcs_admin_man_id ';

	$sql .= 'INNER JOIN helpdesk_request_settings ';
	$sql .= 'ON helpdesk_request_settings.hdrs_id = helpdesk_company_settings.hcs_settings_id ';

	// Clauses
	$sql .= 'WHERE helpdesk_companies.hdc_parent = '.$company_info['hdc_id'];

	$children = $helper->mysql_fetch_all_by_id($sql, 'hdc_id');

	return $children;
}

function thisArrayIsPopulated($returned_array) {
	$thisArrayIsPopulated = false;
	if(isset($returned_array) && is_array($returned_array) && count($returned_array) > 0) {
		$thisArrayIsPopulated = true;
	}
	return $thisArrayIsPopulated;
}

?>
<?php

$section = "NEW";
$sub_page = "NEW";

$button_icon = "OPEN";
$button_label = "Continue";
$button_url = "new_create.php?";
$button_text = false;

//Get Topics
$listObject = new HELPDESK_LIST('topic');
$topics = $listObject->getActiveListItemsFormattedForSelect();

$hd_company_obj = new HELPDESK_COMPANY();
$company_info = $hd_company_obj->getActiveCompanyInformation($_SESSION['cc']);

$hd_user_obj = new HELPDESK_USERS();
$current_user = $hd_user_obj->getCurrentHelpdeskUser();

$show_children = true;
if(!($company_info['hdct_value'] == 'reseller' && $current_user['hdrut_value'] == 'reseller_admin_user')) {//Only give resellers the option to request new client dbs
	unset($topics[6]);

	$show_children = false;
}

if($company_info['hdct_value'] == 'reseller') {
	unset($topics[7]);
}

//Get Modules
$listObject->changeListType('module');
$modules = $listObject->getActiveListItemsFormattedForSelect();

if($show_children == true) {
	$children = getCurrentCompChildren($company_info, $helper);
	if(thisArrayIsPopulated($children)) {
		$children_prepped_for_select = array();
		$children_prepped_for_select[0] = 'N/A';
		foreach($children as $key => $val) {
			$children_prepped_for_select[$key] = $val['hdc_name'];
		}
	}
}
?>

<form name="newrequest" method="post" enctype="multipart/form-data">
	<table id="tbl_action" class="form">
		<tbody>
		<tr class="">
			<th>Request By:</th>
			<td><?php echo $_SESSION['tkn']; ?></td>
		</tr>
		<tr>
			<th id="th_helpdesk_request_topicid">Topic:</th>
			<td>
				<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_topicid', 'name' => 'hdr_topicid', 'required' => true, 'req' => true, 'unspecified' => false, 'options' => $topics)); ?>
			</td>
		</tr>
		<tr>
			<th id="th_helpdesk_request_module">Module:</th>
			<td id="dynamic_modules">
				<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdr_moduleid', 'name' => 'hdr_moduleid', 'required' => true, 'req' => true, 'unspecified' => false, 'options' => $modules)); ?>
			</td>
		</tr>
		<?php if($show_children == true && thisArrayIsPopulated($children_prepped_for_select)) { ?>
			<tr>
				<th id="th_helpdesk_request_company">On Behalf Of:</th>
				<td>
					<?php $js .= $displayObject->drawFormField('LIST', array('id' => 'hdrc_company_id', 'name' => 'hdrc_company_id', 'required' => false, 'unspecified' => false, 'options' => $children_prepped_for_select)); ?>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<th id="th_helpdesk_request_description">Description:</th>
			<td>
				<?php $js .= $displayObject->drawFormField('TEXT', array('id' => 'hdr_description', 'name' => 'hdr_description', 'required' => true, 'req' => true)); ?>
			</td>
		</tr>
		<tr>
			<th>Attach Document(s):</th>
			<td id="attach_containers">
				<?php $js .= $displayObject->drawFormField('ATTACH', array('id' => 'hdr_attachment', 'name' => 'hdr_attachment', 'required' => false, 'action' => 'REQUEST.ADD', 'page_direct' => 'new.php?')); ?>
			</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<input id="btn_submit" type="button" value="Submit" class="isubmit i-am-the-submit-button">
				<input type="reset" value="Reset" name="B2">
			</td>
		</tr>
		</tbody>
	</table>
</form>

<div id=div_error class=div_frm_error>

</div>

<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$('#btn_submit').click(function () {
			//console.log($('form').serialize());
			var count = 0;
			var attachments = false;
			$('input:file').each(function () {

				if ($(this).val().length > 0) {
//                    console.log('Yes there is an attachment:')
//                    console.log($(this).val().length);

					attachments = true;

					count++;
				} else {
//                    console.log('No, there is no attachment:')
//                    console.log($(this).val().length);
				}
			});
			if (attachments == true) {
				HelpdeskHelper.processObjectFormWithAttachment($('form[name="newrequest"]'), 'REQUEST.ADD', 'new.php?');
			} else {
				HelpdeskHelper.processObjectForm($('form[name="newrequest"]'), 'REQUEST.ADD', 'new.php?');
			}

		});

		/*
		* What follows is the dynamic module functionality
		* Currently commented out with the intention of becoming
		* fully functional when I get back from Dhamma Pataka
		* */

		$('#hdr_topicid').change(function (e) {
			e.preventDefault();
			var value = $(this).val();
			var dta = {topic_id: value};
			// console.log(value);
			// console.log(dta);

			var page_action = 'REQUEST.GETMODULES';
			var url = 'inc_controller.php?action=' + page_action;
			var result = AssistHelper.doAjax(url, dta);

//            console.log(result);
			if (value != 6 && value != 8) {
				$('#dynamic_modules').empty().html(result)
			} else {
				$('#dynamic_modules').empty();
			}

		});
	});
</script>

<?php
////If $var['cc'] && $var['user_tkid'] are set then this call is coming from an edit instead of a new launch
//$var = array();
//$cc = (isset($var['cc']) ? $var['cc'] : $_SESSION['cc'] );
//$tkid = (isset($var['user_tkid']) ? $var['user_tkid'] : $_SESSION['tid'] );
//
///*
// * Get list of all modules from ignite4u man_modules,
// * and get the ones that aren't in the helpdesk system in there
// * - START
// * */
//
////getAllActiveModulesFromIgite4U() - start
//$master_db_obj = new ASSIST_MODULE_HELPER('master');
//
//$sql = 'SELECT * FROM man_modules ';
//$sql .= 'WHERE ((mod_status & ' . MAN_HELPER::INACTIVE . ') <> ' . MAN_HELPER::INACTIVE . ' AND (mod_status & ' . MAN_HELPER::MODULE_ARCHIVED . ') <> ' . MAN_HELPER::MODULE_ARCHIVED . ' AND (mod_status & ' . MAN_HELPER::ACTIVE . ') = ' . MAN_HELPER::ACTIVE . ') ';
//$sql .= 'ORDER BY mod_generic_name, mod_loc ';
//
//$all_ignite_modules = $master_db_obj->mysql_fetch_all_by_id($sql, 'mod_id');
////getAllActiveModulesFromIgite4U() - end
//
//$client_db = new ASSIST_MODULE_HELPER('client', $cc);
//$listObject = new HELPDESK_LIST('module');
//
////getAllActiveModulesInHelpdeskSystem() - start
//$full_module_list = $listObject->getActiveListItems();
////getAllActiveModulesInHelpdeskSystem() - end
//
////determineModulesFromIgite4uThatAreNotInTheHelpdeskSystem() - start
//foreach($full_module_list as $key => $val){
//    foreach($all_ignite_modules as $key2 => $val2){
//        if($val['shortcode'] == $val2['mod_loc']){
//            unset($all_ignite_modules[$key2]);
//        }
//    }
//}
////determineModulesFromIgite4uThatAreNotInTheHelpdeskSystem() - end
//
////insertModulesFromIgite4uThatAreNotInTheHelpdeskSystem() - start
////Then insert all of the modules that aren't in the system, if any remain
//if(isset($all_ignite_modules) && is_array($all_ignite_modules) && count($all_ignite_modules) > 0 ){
//    foreach($all_ignite_modules as $key => $val){
//        $var = array();
//        $var['value'] = $val['mod_generic_name'];
//        $var['shortcode'] = $val['mod_loc'];
//        $var['status'] = MAN_HELPER::ACTIVE;
//
//        $listObject->addListItem($var);
//    }
//    $full_module_list = $listObject->getActiveListItems();
//}
////insertModulesFromIgite4uThatAreNotInTheHelpdeskSystem() - end
//
///* Find the modules that are in the helpdesk that are not in the list of active ignite4u modules*/
//
///*
// * Get list of all modules from ignite4u man_modules,
// * and get the ones that aren't in the helpdesk system in there
// * - END
// * */
//
//
///*
// * Get list of all modules that this company has access to and the names thereof,
// * and get the ones that aren't in the helpdesk system in there
// * - START
// * */
//$sql = 'SELECT * FROM assist_menu_modules ';
//$sql .= 'WHERE modyn = \'Y\' ';
//$company_modules = $client_db->mysql_fetch_all($sql);
//
//$hd_company_obj = new HELPDESK_COMPANY();
//$company_info = $hd_company_obj->getActiveCompanyInformation($_SESSION['cc']);
//
//$sql = 'SELECT * FROM helpdesk_list_company_module ';
//$sql .= 'WHERE hlcm_hdc_id = ' . $company_info['hdc_id'] . ' ';
//$sql .= 'ORDER BY hlcm_modtext ';
//$helpdesk_company_modules = $helper->mysql_fetch_all($sql);
//
////echo '<pre style="font-size: 18px">';
////echo '<p>$helpdesk_company_modules</p>';
////print_r($helpdesk_company_modules);
////echo '</pre>';
//
//foreach($helpdesk_company_modules as $key => $val){
//    foreach($company_modules as $key2 => $val2){
//        if($val['hlcm_modid'] == $val2['modid']){
//            unset($company_modules[$key2]);
//        }
//    }
//}
//
//if(isset($company_modules) && is_array($company_modules) && count($company_modules) > 0 ){
//    foreach($company_modules as $key => $val){
//        $var = array();
//        $var['hlcm_hdc_id'] = $company_info['hdc_id'];
//
//        foreach($full_module_list as $key2 => $val2){
//            if($val['modlocation'] == $val2['shortcode']){
//                $var['hlcm_hlm_id'] = $val2['id'];
//                break;
//            }else{
//                $var['hlcm_hlm_id'] = 0;
//            }
//        }
//
//        $var['hlcm_modid'] = $val['modid'];
//        $var['hlcm_modref'] = $val['modref'];
//        $var['hlcm_modtext'] = $val['modtext'];
//
//        $sql = 'INSERT INTO helpdesk_list_company_module ';
//        $sql .= '(hlcm_hdc_id, hlcm_hlm_id, hlcm_modid, hlcm_modref, hlcm_modtext)';
//        $sql .= 'VALUES (' . $var['hlcm_hdc_id'] . ', ' . $var['hlcm_hlm_id'] . ', ' . $var['hlcm_modid'] . ', \'' . $var['hlcm_modref'] . '\', \'' . $var['hlcm_modtext'] . '\')';
//        $id = $helper->db_insert($sql);
//    }
//    $sql = 'SELECT * FROM helpdesk_list_company_module ';
//    $sql .= 'WHERE hlcm_hdc_id = ' . $company_info['hdc_id'] . ' ';
//    $sql .= 'ORDER BY hlcm_modtext ';
//    $helpdesk_company_modules = $helper->mysql_fetch_all($sql);
//}
//
///*
// * Get list of all modules that this company has access to and the names thereof,
// * and get the ones that aren't in the helpdesk system in there
// * - END
// * */
//
///************************************************************************************************************************/
///************************************************************************************************************************/
///************************************************************************************************************************/
//
//$int_parsed_topic_id = 4;
//
////Client Admin User Hack For PWC - As Requested by JC during meeting on 8 August 2017 - Start
////Get the current user record if it's a client_admin_user, from this company
//$sql = 'SELECT * FROM helpdesk_users ';
//$sql .= 'INNER JOIN helpdesk_companies ';
//$sql .= 'ON helpdesk_companies.hdc_id = helpdesk_users.user_company ';
//$sql .= 'INNER JOIN helpdesk_user_types ';
//$sql .= 'ON helpdesk_user_types.hdrut_id = helpdesk_users.user_type ';
//$sql .= 'WHERE helpdesk_users.user_tkid = \'' . $_SESSION['tid'] . '\' ';
//$sql .= 'AND helpdesk_companies.hdc_cc = \'' . $_SESSION['cc'] . '\' ';
//$sql .= 'AND helpdesk_user_types.hdrut_value = \'client_admin_user\' ';
//$user = $helper->mysql_fetch_one($sql);
//
//$show_every_module_for_client_admin_user = false;
//if(isset($user) && is_array($user) && count($user) > 0){
//    $show_every_module_for_client_admin_user = true;
//}
//
////Client Admin User Hack For PWC - As Requested by JC during meeting on 8 August 2017 - END
//
//$hd_user_obj = new HELPDESK_USERS();
//$current_user = $hd_user_obj->getCurrentHelpdeskUser();
//
//if(!(isset($current_user) && is_array($current_user) && count($current_user) > 0)){
//    $current_user = $hd_user_obj->addObject($_SESSION['tid'], $_SESSION['tkn'], $_SESSION['cc']);
//}
//
///*  TOPICS
// *  [1] => Request New Module
// *  [2] => Report Bug
//    [3] => Request Custom Module Functionality
//    [4] => Request Access To Module
//    [5] => Other
//    [6] => Request new client db
//    [7] => Content Support
// * */
//
//$sql = 'SELECT * FROM assist_menu_modules ';
//$sql .= 'WHERE modyn = \'Y\' ';
//$sql .= 'ORDER BY modtext ';
//$company_modules = $client_db->mysql_fetch_all($sql);
//
////echo '<pre style="font-size: 18px">';
////echo '<p>$company_modules</p>';
////print_r($company_modules);
////echo '</pre>';
//
//$show_company_modules = false;
//$show_generic_modules = false;
//$show_helpdesk_module = false;
//$show_unspecified_module = false;
//if($int_parsed_topic_id == 1){
//    //Show all generic modules
//    $show_generic_modules = true;
//
//}elseif(($current_user['hdrut_value'] == 'client_user' || $current_user['hdrut_value'] == 'reseller_user') && ($int_parsed_topic_id == 2 || $int_parsed_topic_id == 3)){
//    //Show all of my modules + Helpdesk && Unspecified
//    $show_company_modules = true;
//    $show_helpdesk_module = true;
//    $show_unspecified_module = true;
//
//
//    /*This section should return modules that the current user already has access to*/
//    $sql = 'SELECT * FROM assist_menu_modules ';
//    $sql .= 'INNER JOIN assist_'. $cc .'_menu_modules_users ';
//    $sql .= 'ON assist_'. $cc .'_menu_modules_users.usrmodid = assist_menu_modules.modid ';
//    $sql .= 'WHERE assist_menu_modules.modyn = \'Y\' ';
//    $sql .= 'AND assist_'. $cc .'_menu_modules_users.usrtkid = \''. $tkid .'\' ';
//    $sql .= 'ORDER BY modtext ';
//
//    $current_user_modules = $client_db->mysql_fetch_all($sql);
//
//    $company_modules = $current_user_modules;
//
//}elseif(($current_user['hdrut_value'] == 'client_user' || $current_user['hdrut_value'] == 'reseller_user') && $int_parsed_topic_id == 4){
//    //All company modules not in my modules
//    $show_company_modules = true;
//
//    /*This section should return modules that the current user already has access to*/
//    $sql = 'SELECT * FROM assist_menu_modules ';
//    $sql .= 'INNER JOIN assist_'. $cc .'_menu_modules_users ';
//    $sql .= 'ON assist_'. $cc .'_menu_modules_users.usrmodid = assist_menu_modules.modid ';
//    $sql .= 'WHERE assist_menu_modules.modyn = \'Y\' ';
//    $sql .= 'AND assist_'. $cc .'_menu_modules_users.usrtkid = \''. $tkid .'\' ';
//    $sql .= 'ORDER BY modtext ';
//
//    $current_user_modules = $client_db->mysql_fetch_all_by_id($sql, 'modid');
//
//    foreach($current_user_modules as $key => $val){
//        if(isset($company_modules[$key])){
//            unset($company_modules[$key]);
//        }
//    }
//
//    $mod_ids = array();
//    foreach($company_modules as $key => $val){
//        $mod_ids[$key] = $key;
//    }
//
//    $sql = 'SELECT * FROM assist_menu_modules ';
//    $sql .= 'WHERE modyn = \'Y\' ';
//    $sql .= 'AND modid IN (' . implode(',',$mod_ids) . ') ';
//    $sql .= 'ORDER BY modtext ';
//
//    $company_modules = $client_db->mysql_fetch_all($sql);
//
//}elseif($current_user['hdrut_value'] == 'client_admin_user'){
//    //All company modules + Helpdesk
//    $show_company_modules = true;
//    $show_helpdesk_module = true;
//
//    if($int_parsed_topic_id == 2 || $int_parsed_topic_id == 3){
//        // + Unspecified
//        $show_unspecified_module = true;
//    }
//
//}else{
//    //All Company Modules + All Generic Modules
//    $show_company_modules = true;
//    $show_generic_modules = true;
//
//}
//
//$html = '<select id="hdr_moduleid" name="hdr_moduleid" required="1" unspecified="">';
//$html .= '<option value="X">--- SELECT ---</option>';
//if($show_company_modules == true){
//    $helpdesk_company_modules_to_loop_through = array();
//
//    foreach($helpdesk_company_modules as $key => $val){
//        foreach($company_modules as $key2 => $val2){
//            if($val['hlcm_modid'] == $val2['modid']){
//                $helpdesk_company_modules_to_loop_through[$val['hlcm_id']]['id'] = $val['hlcm_hlm_id'] . '_companyMod_' . $val['hlcm_modid'];
//                $helpdesk_company_modules_to_loop_through[$val['hlcm_id']]['value'] = $val2['modtext'];
//            }
//        }
//    }
//
//    $html .= '<option value="X">--- COMPANY MODULES ---</option>';
//    foreach($helpdesk_company_modules_to_loop_through as $key => $val){
//        $html .= '<option value="'.$val['id'].'" full="Complaints Assist" list_num="0">'.$val['value'].'</option>';
//    }
//}
//
//if($show_generic_modules == true){
//    $sql = 'SELECT * FROM man_modules ';
//    $sql .= 'WHERE ((mod_status & ' . MAN_HELPER::INACTIVE . ') <> ' . MAN_HELPER::INACTIVE . ' AND (mod_status & ' . MAN_HELPER::MODULE_ARCHIVED . ') <> ' . MAN_HELPER::MODULE_ARCHIVED . ' AND (mod_status & ' . MAN_HELPER::ACTIVE . ') = ' . MAN_HELPER::ACTIVE . ') ';
//    $sql .= 'ORDER BY mod_generic_name, mod_loc ';
//
//    $all_ignite_modules = $master_db_obj->mysql_fetch_all_by_id($sql, 'mod_id');
//
//    $helpdesk_generic_modules_to_loop_through = array();
//
//    foreach($full_module_list as $key => $val){
//        if($val['shortcode'] != 'S' && $val['shortcode'] != 'TK' /*|| $val['shortcode'] != 'SYSTEM REPORTS'*/){
//            foreach($all_ignite_modules as $key2 => $val2){
//                if($val['shortcode'] == $val2['mod_loc']){
//                    $helpdesk_generic_modules_to_loop_through[$val['id']]['id'] = $val['id'] . '_genericMod_0';
//                    $helpdesk_generic_modules_to_loop_through[$val['id']]['value'] = $val2['mod_generic_name'];
//                }
//            }
//        }
//    }
//
//    $html .= '<option value="X">--- GENERIC MODULES ---</option>';
//    foreach($helpdesk_generic_modules_to_loop_through as $key => $val){
//        $html .= '<option value="'.$val['id'].'" full="Complaints Assist" list_num="0">'.$val['value'].'</option>';
//    }
//
//}
//
//if($show_helpdesk_module == true){
//    $html .= '<option value="'.$full_module_list[17]['id'] . '_genericMod_0' . '" full="Complaints Assist" list_num="0">'.$full_module_list[17]['value'].'</option>';
//}
//
//if($show_unspecified_module == true){
//    $html .= '<option value="'.$full_module_list[18]['id'] . '_genericMod_0' .'" full="Complaints Assist" list_num="0">'.$full_module_list[18]['value'].'</option>';
//}
//
//if($current_user['hdrut_value'] != 'client_user' && $current_user['hdrut_value'] != 'reseller_user'){
//    //Add Master, User Man, And System Reports Once It's added
//    $html .= '<option value="'.$full_module_list[31]['id'] . '_genericMod_0' .'" full="Complaints Assist" list_num="0">'.$full_module_list[31]['value'].'</option>';
//    $html .= '<option value="'.$full_module_list[34]['id'] . '_genericMod_0' .'" full="Complaints Assist" list_num="0">'.$full_module_list[34]['value'].'</option>';
//}
//
//$html .= '</select>';

?>

<?php require_once('inc_footer.php'); ?>

