<?php
    include("inc_ignite.php");
    include("inc_admin.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();


$cpid = $_GET['c'];

$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_projects WHERE id = ".$cpid;
include("inc_db_con.php");
    $cprow = mysql_fetch_array($rs);
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE id = ".$cprow['cateid'];
include("inc_db_con.php");
    $caterow = mysql_fetch_array($rs);
mysql_close();
    $catedept = $caterow['deptid'];
    
if($moncash == "Y")
{
    $mbudget = 0;
    $mactual = 0;
    for($t=1;$t<13;$t++)
    {
        $mbudget = $mbudget + $cprow['m'.$t.'budget'];
        $mactual = $mactual + $cprow['m'.$t.'actual'];
    }
}

?>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009 - Edit</b></h1>
<form name=newcp action=update_cp_fin_process.php method=post onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=c value=<?php echo($cpid); ?>>
<table cellpadding=3 cellspacing=0 border=1>
    <tr class=tdgeneral>
        <td><b>Capital Project Number:</b></td>
        <td><?php echo($cprow['capnum']);?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
<?php
$sql2 = "SELECT headshort FROM assist_".$cmpcode."_sdbip_capital_headings WHERE field = 'pms'";
include("inc_db_con2.php");
    $row = mysql_fetch_array($rs2);
mysql_close($con2);
if(strlen($row['headshort'])>0)
{
        ?>
		<td><b><?php echo($row['headshort']); ?>:</b></td>
        <?php
}
else
{
        ?>
		<td><b>PMS Ref:</b></td>
        <?php
}
?>
        <td><?php echo($cprow['pmsref']);?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Department:</b></td>
        <td>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE id = ".$catedept." ORDER BY sort";
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            echo($row['value']);
        mysql_close();
        ?>
        &nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Category:</b></td>
        <td>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE id = ".$caterow['id']." AND deptid = ".$catedept;
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            echo($row['value']." (".$row['code'].")");
        mysql_close();
        ?>
        &nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Description:</b></td>
        <td><?php echo($cprow['project']);?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Wards:</b></td>
        <td><?php echo($cprow['wards']);?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Funding Source:</b></td>
        <td><?php echo($cprow['fundsource']);?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=tot1>
        <td>&nbsp;&nbsp;<i>Budget:</i></td>
    <?php if($moncash != "Y") { ?>
        <td>R <input type=text size=5 style="text-align: right" name=budget value="<?php echo($cprow['budget']);?>">&nbsp;<input type=hidden size=5 style="text-align: right" name=budgetold value="<?php echo($cprow['budget']);?>"></td>
    <?php } else { ?>
        <td>R <?php echo(number_format($mbudget,2)); ?><input type=hidden size=5 style="text-align: right" name=budget value="<?php echo($mbudget);?>"></td>
    <?php } ?>
    </tr>
    <tr class=tdgeneral id=tot2>
        <td>&nbsp;&nbsp;<i>Total Spend:</i></td>
    <?php if($moncash != "Y") { ?>
        <td>R <input type=text size=5 style="text-align: right" name=totalspend value="<?php echo($cprow['totalspend']);?>">&nbsp;<input type=hidden size=5 style="text-align: right" name=totalspendold value="<?php echo($cprow['totalspend']);?>"></td>
    <?php } else { ?>
        <td>R <?php echo(number_format($mactual,2)); ?><input type=hidden size=5 style="text-align: right" name=totalspend value="<?php echo($mactual);?>"></td>
    <?php } ?>
    </tr>
    <tr class=tdgeneral>
        <td><b>Start Date:</b></td>
        <td><?php echo(date("d F Y",$cprow['startdate']));?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>End Date:</b></td>
        <td><?php echo(date("d F Y",$cprow['enddate']));?>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td colspan=2><?php if($moncash != "Y") { ?><input type=submit value=Update> <input type=reset><?php } else { ?><i>To update financial information on Capital Projects,<br>please contact Ignite at helpdesk@ignite4u.co.za or 0829 IGNITE.</i><?php } ?>&nbsp;</td>
    </tr>
</table>

</form>
</body>

</html>
