<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
//GET VARIABLES
$cpfield = $_POST['cpfield'];
$dptfilter = $_POST['dpt'];
$ctgfilter = $_POST['ctg'];
$wrdfilter = $_POST['wrd'];
$fsfilter = $_POST['fs'];
$rpsort = $_POST['rpsort'];
$rpsort = explode("_",$rpsort);
$csvfile = $_POST['csvfile'];
//CONVERT FROM array[#] = text TO array[text] = Y
foreach($cpfield as $c)
{
    $cparray[$c] = "Y";
}
if($cparray['mb']=="Y")
{
    $monthtype = $_POST['monthtype'];
}
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();
/*echo("<p>fld ");
print_r($farray);
echo("<P>dpt ".$dptfilter);
echo("<P>ntk ".$ntkfilter);
echo("<P>mnk ".$mnkfilter);
echo("<P>prg ".$prgfilter);
echo("<P>drv ".$drvfilter);
echo("<P>wrd ".$wrdfilter);
echo("<P>srt ".$rpsort);
*/
//GET SDBIP HEADINGS
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_headings ORDER BY sort";
include("inc_db_con.php");
    $h = 0;
    $head[$h]['head'] = "Department";
    $head[$h]['field'] = "dept";
    $h++;
    while($row = mysql_fetch_array($rs))
    {
        $head[$h]['head'] = $row['headshort'];
        $head[$h]['field'] = $row['field'];
        $hdsort[$row['id']] = $row['field'];
        $h++;
    }
mysql_close();
//SET CP SQL VARIABLE
$sql = "SELECT p.capnum";
$sql.= ", d.value as dept";
$sql.= ", c.value as cate";
$sql.= ", p.pmsref as pms";
$sql.= ", p.project as descrip";
$sql.= ", p.wards";
$sql.= ", p.fundsource as fund";
$sql.= ", p.startdate as start";
$sql.= ", p.enddate as end";
if($moncash == "Y")
{
    $sql.= ", p.m1budget as m1b";
    $sql.= ", p.m1actual as m1a";
    $sql.= ", p.m2budget as m2b";
    $sql.= ", p.m2actual as m2a";
    $sql.= ", p.m3budget as m3b";
    $sql.= ", p.m3actual as m3a";
    $sql.= ", p.m4budget as m4b";
    $sql.= ", p.m4actual as m4a";
    $sql.= ", p.m5budget as m5b";
    $sql.= ", p.m5actual as m5a";
    $sql.= ", p.m6budget as m6b";
    $sql.= ", p.m6actual as m6a";
    $sql.= ", p.m7budget as m7b";
    $sql.= ", p.m7actual as m7a";
    $sql.= ", p.m8budget as m8b";
    $sql.= ", p.m8actual as m8a";
    $sql.= ", p.m9budget as m9b";
    $sql.= ", p.m9actual as m9a";
    $sql.= ", p.m10budget as m10b";
    $sql.= ", p.m10actual as m10a";
    $sql.= ", p.m11budget as m11b";
    $sql.= ", p.m11actual as m11a";
    $sql.= ", p.m12budget as m12b";
    $sql.= ", p.m12actual as m12a";
}
else
{
    $sql.= ", p.budget";
    $sql.= ", p.totalspend as spend";
}
$sql.= " FROM assist_".$cmpcode."_sdbip_capital_projects p";
$sql.= ", assist_".$cmpcode."_sdbip_capital_category c";
$sql.= ", assist_".$cmpcode."_sdbip_dept d";
$sql.= " WHERE p.yn = 'Y'";
$sql.= " AND p.cateid = c.id";
$sql.= " AND c.deptid = d.id";
$sql.= " AND c.yn = 'Y'";
switch($dptfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($dptfilter)>0)
        {
            $sql.= " AND c.deptid = ".$dptfilter;
        }
        break;
}
switch($ctgfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($ctgfilter)>0)
        {
            $sql.= " AND c.id = ".$ctgfilter;
        }
        break;
}
switch($wrdfilter)
{
    case "X":
        break;
    case "blank":
        $sql.= " AND p.wards = ''";
        break;
    default:
        $sql.= " AND p.wards = '".$wrdfilter."'";
        break;
}
switch($fsfilter)
{
    case "X":
        break;
    case "blank":
        $sql.= " AND p.fundsource = ''";
        break;
    default:
        $sql.= " AND p.fundsource = '".$fsfilter."'";
        break;
}
$rp = $rpsort[0];
if($rp == "0" || strlen($hdsort[$rpsort[1]])==0)
{
    $sql.= " ORDER BY d.sort, cate, capnum, pms, descrip, wards, fund, ";
    if($moncash != "Y")
    {
        $sql.="budget, spend, ";
    }
    $sql.="start, end";
}
else
{
    $sql.= " ORDER BY ".$hdsort[$rpsort[1]].", d.sort, cate, capnum, pms, descrip, wards, fund, ";
    if($moncash != "Y")
    {
        $sql.="budget, spend, ";
    }
    $sql.="start, end";
}
include("inc_db_con.php");














if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{
        //CREATE HEADER ROW
        $fdata = "\"\"";
        foreach($head as $hd)
        {
            if($cparray[$hd['field']] == "Y") {$fdata .= ",\"".html_entity_decode($hd['head'], ENT_QUOTES, "ISO-8859-1")."\"";}
        }
        if($moncash=="Y" && ($cparray['mb']=="Y" || $cparray['ma']=="Y"))
        {
                if($monthtype != "ytd")
                {
                    $mx = 12;
                    $diff = $today - $fdate;
                    $mv = date("n",$diff);
                }
                else
                {
                    $diff = $today - $fdate;
                    $mx = date("n",$diff);
                    $mv = $mx;
                }
//                $mt = date("n");
$mt = 12;
//                $mf = date("n",$fdate);
$mf = 1;
                $yf = date("Y",$fdate);
                $m2 = 0;
                for($m=0;$m<$mx;$m++)
                {
                    $m2++;
                    $mc = $mf + $m;
                    if($mc > 12)
                    {
                        $mc = $mc - 12;
                        $yc = $yf + 1;
                    }
                    else
                    {
                        $yc = $yf;
                    }
                    $fdata .= ",\"".date("M-Y",mktime(12,0,0,$mc,1,$yc))."\"";
                    if($cparray['mb']=="Y" && $cparray['ma']=="Y")// && $m < $mv)
                    {
                        $fdata.= ",\"\"";
                    }
                }
                    $fdata .= ",\"Total\"";
                    $fdata .= "\r\n";
                    $fdata .= "\"\"";
                    foreach($head as $hd)
                    {
                        if($cparray[$hd['field']] == "Y") {$fdata .= ",\"\"";}
                    }
                for($m=0;$m<$mx;$m++)
                {
                    if($cparray['mb']=="Y")
                    { $fdata .= ",\"Budget\""; }
                    if($cparray['ma']=="Y")// && $m < $mv)
                    { $fdata .= ",\"Actual\""; }
                }
                    if($cparray['mb']=="Y")
                    { $fdata .= ",\"Budget\""; }
                    if($cparray['ma']=="Y")
                    { $fdata .= ",\"Actual\""; }
                    if($cparray['mb']=="Y" && $cparray['ma']=="Y")
                    { $fdata .= ",\"Balance\""; }
        }
    //LOOP TROUGH QUERY RESULTS AND ADD DETAILS TO ROW IF array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        if($moncash!="Y")
        {
            $fdata .= "\r\n";
            $fdata .= "\"\"";
            $finance['spend'] = $row['spend'];
            $finance['budget'] = $row['budget'];
            $finance['balance'] = $finance['budget'] - $finance['spend'];
            foreach($head as $hd)
            {
                if($cparray[$hd['field']] == "Y")
                {
                    if($hd['field']=="balance" || $hd['field']=="spend" || $hd['field']=="budget")
                    {
                        $fdata .= ",\"".number_format($finance[$hd['field']],2)."\"";
                    }
                    else
                    {
                        if($hd['field']=="start" || $hd['field']=="end")
                        {
                            $fdata .= ",\"".date("d M Y",$row[$hd['field']])."\"";
                        }
                        else
                        {
                            $str = $row[$hd['field']];
                            $str = html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
                            $str = str_replace("&#39","&#39;",$str);
                            $str = str_replace("&#35","&#35;",$str);
                            $str = str_replace("&#235","&#235;",$str);
                            $str = str_replace("&#246","&#246;",$str);
                            $fdata .= ",\"".html_entity_decode($str, ENT_QUOTES, "ISO-8859-1")."\"";
                        }
                    }
                }
            }
        }
        else
        {
            $fdata .= "\r\n";
            $fdata .= "\"\"";
            foreach($head as $hd)
            {
                if($cparray[$hd['field']] == "Y")
                {
                    if($hd['field']!="balance" || $hd['field']!="spend" || $hd['field']!="budget")
                    {
                        if($hd['field']=="start" || $hd['field']=="end")
                        {
                            $fdata .= ",\"".date("d M Y",$row[$hd['field']])."\"";
                        }
                        else
                        {
                            $str = $row[$hd['field']];
                            $str = html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
                            $str = str_replace("&#39","&#39;",$str);
                            $str = str_replace("&#35","&#35;",$str);
                            $str = str_replace("&#235","&#235;",$str);
                            $str = str_replace("&#246","&#246;",$str);
                            $fdata .= ",\"".html_entity_decode($str, ENT_QUOTES, "ISO-8859-1")."\"";
                        }
                    }
                }
            }
                $tota = 0;
                $totb = 0;
                for($m=0;$m<$mx;$m++)
                {
                    $mz = $m+1;
                    $fa = "m".$mz."a";
                    $fb = "m".$mz."b";
                    $fina = $row[$fa];
                    $finb = $row[$fb];
                    $tota = $tota + $fina;
                    $totb = $totb + $finb;
                    if($cparray['mb']=="Y")
                    { $fdata .= ",\"".number_format($finb,2)."\""; }
                    if($cparray['ma']=="Y")// && $m < $mv)
                    { $fdata .= ",\"".number_format($fina,2)."\""; }
                }
                $totbal = $totb - $tota;
                    if($cparray['mb']=="Y")
                    { $fdata .= ",\"".number_format($totb,2)."\""; }
                    if($cparray['ma']=="Y")
                    { $fdata .= ",\"".number_format($tota,2)."\""; }
                    if($cparray['mb']=="Y" && $cparray['ma']=="Y")
                    { $fdata .= ",\"".number_format($totbal,2)."\""; }
        }
    }
    mysql_close();
    //WRITE DATA TO FILE
    $filename = "../files/".$cmpcode."/sdbip_cp_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "sdbip_cp_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
//    echo($fdata);
















}
else    //ELSE OUTPUT IS ONSCREEN DISPLAY
{
















//SET DISPLAY OF HTML HEAD & BODY
$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>SDBIP Assist 2008/9: Departmental Capital Project Report</b></h1>";
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    $display .= "<p>There are no Capital Projects that meet your criteria.<br>Please go back and select different criteria.</p>";
}
//ELSE
else
{
    //CREATE TABLE AND SET TDHEADER
    $display .= "<table border=1 cellspacing=0 cellpadding=2>	<tr class=tdheader>";
        if($moncash=="Y" && ($cparray['mb']=="Y" || $cparray['ma']=="Y"))
        {
            $hrows = "2";
        }
        else
        {
            $hrows = "1";
        }
        foreach($head as $hd)
        {
            if($cparray[$hd['field']] == "Y") {$display .= "<td rowspan=".$hrows.">".$hd['head']."</td>";}
        }
        if($moncash=="Y" && ($cparray['mb']=="Y" || $cparray['ma']=="Y"))
        {
            if($cparray['mb']=="Y" && $cparray['ma']=="Y")
            {
                $mcols = 2;
                $totmcols = 3;
            }
            else
            {
                $mcols = 1;
                $totmcols = 1;
            }
                if($monthtype != "ytd")
                {
                    $mx = 12;
                    $diff = $today - $fdate;
                    $mv = date("n",$diff);
                }
                else
                {
                    $diff = $today - $fdate;
                    $mx = date("n",$diff);
                    $mv = $mx;
                }
                //$mt = date("n");
                $mf = date("n",$fdate);
                $mt = 12;
                //$mf = 12;
                $yf = date("Y",$fdate);
                $m2 = 0;
                for($m=0;$m<$mx;$m++)
                {
                    $m2++;
                    $mc = $mf + $m;
                    if($mc > 12)
                    {
                        $mc = $mc - 12;
                        $yc = $yf + 1;
                    }
                    else
                    {
                        $yc = $yf;
                    }
                    $display.= "<td colspan=";
                    //if($m>=$mv) { $display.="1"; } else { $display.=$mcols;}
                    $display.=$mcols;
                    $display.=">".date("M-Y",mktime(12,0,0,$mc,1,$yc))."</td>";
                }
                    $display.= "<td colspan=".$totmcols.">Total</td>";
        	$display .= "</tr>";
        	$display .= "<tr class=tdheader>";
                for($m=0;$m<$mx;$m++)
                {
                    if($cparray['mb']=="Y")
                    { $display.= "<td>Budget</td>"; }
                    if($cparray['ma']=="Y")// && $m < $mv)
                    { $display.= "<td>Actual</td>"; }
                }
                    if($cparray['mb']=="Y")
                    { $display.= "<td>Budget</td>"; }
                    if($cparray['ma']=="Y")
                    { $display.= "<td>Actual</td>"; }
                    if($cparray['mb']=="Y" && $cparray['ma']=="Y")
                    { $display.= "<td>Balance</td>"; }
        }
	$display .= "</tr>";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        if($moncash != "Y")
        {
            $finance['spend'] = $row['spend'];
            $finance['budget'] = $row['budget'];
            $finance['balance'] = $finance['budget'] - $finance['spend'];
            $display .= "<tr class=tdgeneral valign=top>";
            foreach($head as $hd)
            {
                if($cparray[$hd['field']] == "Y")
                {
                    if($hd['field']=="balance" || $hd['field']=="spend" || $hd['field']=="budget")
                    {
                        $display .= "<td align=right>".number_format($finance[$hd['field']],2)."</td>";
                    }
                    else
                    {
                        if($hd['field']=="start" || $hd['field']=="end")
                        {
                            $display .= "<td align=center>".date("d M Y",$row[$hd['field']])."&nbsp;</td>";
                        }
                        else
                        {
                            $display .= "<td>".$row[$hd['field']]."&nbsp;</td>";
                        }
                    }
                }
            }
        }
        else    //IF MONCASH = N
        {
            $display .= "<tr class=tdgeneral valign=top>";
            foreach($head as $hd)
            {
                if($cparray[$hd['field']] == "Y")
                {
                    if($hd['field']!="balance" || $hd['field']!="spend" || $hd['field']!="budget")
                    {
                        if($hd['field']=="start" || $hd['field']=="end")
                        {
                            $display .= "<td align=center>".date("d M Y",$row[$hd['field']])."&nbsp;</td>";
                        }
                        else
                        {
                            $display .= "<td>".$row[$hd['field']]."&nbsp;</td>";
                        }
                    }
                }
            }
                $tota = 0;
                $totb = 0;
                for($m=0;$m<$mx;$m++)
                {
                    $mz = $m+1;
                    $fa = "m".$mz."a";
                    $fb = "m".$mz."b";
                    $fina = $row[$fa];
                    $finb = $row[$fb];
                    $tota = $tota + $fina;
                    $totb = $totb + $finb;
                    if($cparray['mb']=="Y")
                    {
                        if($finb!=0)
                        { $display.= "<td align=right>".number_format($finb,2)."</td>"; }
                        else
                        { $display.= "<td align=right>-</td>"; }
                    }
                    if($cparray['ma']=="Y")// && $m < $mv)
                    {
                        if($fina!=0)
                        { $display.= "<td align=right>".number_format($fina,2)."</td>"; }
                        else
                        { $display.= "<td align=right>-</td>"; }
                    }
                }
                $totbal = $totb - $tota;
                    if($cparray['mb']=="Y")
                    { $display.= "<td align=right><b>".number_format($totb,2)."</b></td>"; }
                    if($cparray['ma']=="Y")
                    { $display.= "<td align=right><b>".number_format($tota,2)."</b></td>"; }
                    if($cparray['mb']=="Y" && $cparray['ma']=="Y")
                    { $display.= "<td align=right><b>".number_format($totbal,2)."</b></td>"; }

        }
    }
    $display .= "</table>";
}
mysql_close();
$display .= "</body></html>";
//WRITE DISPLAY TO SCREEN
echo($display);

}   //ENDIF CSVFILE = Y

?>

