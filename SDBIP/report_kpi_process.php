<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
//GET VARIABLES
$field = $_POST['field'];
$dptfilter = $_POST['dpt'];
$ntkfilter = $_POST['ntk'];
$mnkfilter = $_POST['mnk'];
$prgfilter = $_POST['prg'];
$drvfilter = $_POST['drv'];
$wrdfilter = $_POST['wrd'];
$rpsort = $_POST['rpsort'];
$csvfile = $_POST['csvfile'];
//CONVERT FROM array[#] = text TO array[text] = Y
foreach($field as $f)
{
    $farray[$f] = "Y";
}
/*echo("<p>fld ");
print_r($farray);
echo("<P>dpt ".$dptfilter);
echo("<P>ntk ".$ntkfilter);
echo("<P>mnk ".$mnkfilter);
echo("<P>prg ".$prgfilter);
echo("<P>drv ".$drvfilter);
echo("<P>wrd ".$wrdfilter);
echo("<P>srt ".$rpsort);
*/
//GET SDBIP HEADINGS
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_headings";
include("inc_db_con.php");
    $h = 0;
    while($row = mysql_fetch_array($rs))
    {
        $head[$h]['head'] = $row['headshort'];
        $head[$h]['field'] = $row['field'];
        $h++;
    }
mysql_close();
//GET TIME/RESULT DETAILS
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $t = 0;
    while($row = mysql_fetch_array($rs))
    {
        $time[$t]['e'] = $row['value'];
        $time[$t]['s'] = $row['start'];
        $t++;
    }
mysql_close();
//SET SQL VARIABLE
            $sql = "SELECT m.value munkpa";
            $sql.= ", m.code munkpac";
            $sql.= ", d.value dept";
            $sql.= ", n.code natkpac";
            $sql.= ", n.value natkpa";
            $sql.= ", a.value progval";
            $sql.= ", i.kpiid";
            $sql.= ", i.kpicapnum capnum";
            $sql.= ", i.kpivalue kpival";
            $sql.= ", t.value kpitype";
            $sql.= ", i.kpiunit unitmeasure";
            $sql.= ", i.kpiward wards";
            $sql.= ", i.kpidriver driver";
            $sql.= ", i.kpibaseline baseline";
            $sql.= ", i.kpitarget target";
            $sql.= ", i.kpiperfweight pweight";
            $sql.= ", i.kpicapprojid cpid";
            $sql.= ", i.kpizeroyn zeroyn";
            $sql.= ", r.*";

            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i";
            $sql.= ", assist_".$cmpcode."_sdbip_list_type t";
            $sql.= ", assist_".$cmpcode."_sdbip_list_kpa n";
            $sql.= ", assist_".$cmpcode."_sdbip_list_kpa m";
            $sql.= ", assist_".$cmpcode."_sdbip_prog a";
            $sql.= ", assist_".$cmpcode."_sdbip_kpi_result r";
            $sql.= ", assist_".$cmpcode."_sdbip_dept d";
            $sql.= ", assist_".$cmpcode."_sdbip_list_time l";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND a.deptid = d.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND a.typeid <> 1";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND r.krtimeid = l.id";
switch($dptfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($dptfilter)>0)
        {
            $sql.= " AND a.deptid = ".$dptfilter;
        }
        break;
}
switch($ntkfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($ntkfilter)>0)
        {
            $sql.= " AND n.id = ".$ntkfilter;
        }
        break;
}
switch($mnkfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($mnkfilter)>0)
        {
        $sql.= " AND m.id = ".$mnkfilter;
        }
        break;
}
switch($prgfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($prgfilter)>0)
        {
        $sql.= " AND a.id = ".$prgfilter;
        }
        break;
}
switch($drvfilter)
{
    case "ALL":
        break;
    default:
        if(strlen($drvfilter)>0)
        {
        $sql.= " AND i.kpidriver = '".$drvfilter."'";
        }
        break;
}
switch($wrdfilter)
{
    case "X":
        break;
    case "blank":
        $sql.= " AND i.kpiward = ''";
        break;
    default:
        if(strlen($wrdfilter)>0)
        {
        $sql.= " AND i.kpiward = '".$wrdfilter."'";
        }
        break;
}
$sort = array();
$sort[0] = "d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[1] = "m.value, d.sort, a.typeid, n.value, a.value, i.kpiid, l.sort";
$sort[2] = "n.value, d.sort, a.typeid, m.value, a.value, i.kpiid, l.sort";
$sort[3] = "a.value, d.sort, a.typeid, n.value, m.value, i.kpiid, l.sort";
$sort[4] = "i.kpicapnum, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[5] = "i.kpivalue, d.sort, a.typeid, n.value, m.value, a.value, l.sort";
$sort[6] = "t.value, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[7] = "i.kpiunit, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[8] = "i.kpiward, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[9] = "i.kpidriver, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[10] = "i.kpibaseline, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[11] = "i.kpitarget, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sort[12] = "i.kpiperfweight, d.sort, a.typeid, n.value, m.value, a.value, i.kpiid, l.sort";
$sql.= " ORDER BY ".$sort[$rpsort];

//echo("<P>".$sql);

/*
//Set transaction log
$tsql2 = $sql;
$tsql = str_replace("'","|",$tsql2);
$tref = 'SDBIP';
$trans = "Generated departmental KPI report to view ";
if($farray['adddate'] == "Y") {$trans .= ", Date added";}
if($farray['tkid'] == "Y") {$trans .= ", Person tasked";}
if($farray['adduser'] == "Y") {$trans .= ", Task owner";}
if($farray['topicid'] == "Y") {$trans .= ", Task topic";}
if($farray['action'] == "Y") {$trans .= ", Task instructions";}
if($farray['deliver'] == "Y") {$trans .= ", Task deliverables";}
if($farray['deadline'] == "Y") {$trans .= ", Task deadline";}
$trans .= ", Task status; Report output as ";
if($csvfile == "Y") {$trans .= "CSVfile.";} else {$trans .= "onscreen display.";}
include("inc_transaction_log.php");

//RUN SQL QUERY
$sql = $tsql2;
*/

include("inc_db_con.php");


if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{
        //CREATE HEADER ROW
        if($farray['dept'] == "Y") { $fdata = "\"Dept\""; } else { $fdata = "\"\""; }
        foreach($head as $hd)
        {
            if($farray[$hd['field']] == "Y") {$fdata .= ",\"".html_entity_decode($hd['head'], ENT_QUOTES, "ISO-8859-1")."\"";}
        }
        if($farray['time'] == "Y")
        {
            foreach($time as $tm)
            {
                $fdata .= ",\"".date("d-M-Y",$tm['e'])."\",\"\",\"\",\"\"";
            }
            $fdata .= "\r\n";
            $fdata .= "\"\"";
            foreach($head as $hd)
            {
                if($farray[$hd['field']] == "Y") {$fdata .= ",\"\"";}
            }
            foreach($time as $tm)
            {
                $fdata .= ",\"Target\",\"Actual\",\"R\",\"Comment\"";
            }
        }

    //LOOP TROUGH QUERY RESULTS AND ADD DETAILS TO ROW IF array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $fdata .= "\r\n";
//        $fdata .= "\"\"";
        if($farray['dept'] == "Y") { $fdata.= "\"".$row['dept']."\""; } else { $fdata.= "\"\""; }
        foreach($head as $hd)
        {
            $str = $row[$hd['field']];
            $str = html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
            $str = str_replace("&#39","&#39;",$str);
            $str = str_replace("&#35","&#35;",$str);
            $str = str_replace("&#235","&#235;",$str);
            $str = str_replace("&#246","&#246;",$str);
            if($farray[$hd['field']] == "Y") {$fdata .= ",\"".html_entity_decode($str, ENT_QUOTES, "ISO-8859-1")."\"";}
        }
        if($farray['time'] == "Y")
        {
            $krr = "";
            $t = 1;
            foreach($time as $tm)
            {
                if($t>1)
                {
                    $row = mysql_fetch_array($rs);
                }
                $krt = $row['krtarget'];
                $kra = $row['kractual'];
                $krc = $row['krcomment'];
                $kr0 = $row['zeroyn'];
                if($kr0 == "N")
                {
                    if($krt>0)
                    {
                        if($kra>0)
                        {
                            $krr = $kra / $krt;
                        }
                        else
                        {
                            $krr = "";
                        }
                    }
                    else
                    {
                        if($kra>0)
                        {
                            $krr = 1;
                        }
                        else
                        {
                            $krr = "";
                        }
                    }
                }
                else
                {
                    $krr = $kr0;
                }
                if(strlen($krr)>0)
                {
                    if($krr<0.75)
                    {
                        $krr = "R";
                    }
                    else
                    {
                        if($krr<1)
                        {
                            $krr = "O";
                        }
                        else
                        {
                            $krr = "G";
                        }
                    }
                }
                $fdata .= ",\"".number_format($krt,0)."\"";
                $fdata .= ",\"".number_format($kra,0)."\"";
                $fdata .= ",\"".$krr."\"";
                $str = $krc;
                $str = html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
                $str = str_replace("&#39","&#39;",$str);
                $str = str_replace("&#35","&#35;",$str);
                $str = str_replace("&#235","&#235;",$str);
                $str = str_replace("&#246","&#246;",$str);
                $fdata .= ",\"".html_entity_decode($str, ENT_QUOTES, "ISO-8859-1")."\"";
                $t++;
            }
        }
        else
        {
            $t = 1;
            foreach($time as $tm)
            {
                if($t>1)
                {
                    $row = mysql_fetch_array($rs);
                }
                $t++;
            }
        }

    }
    mysql_close();
    //WRITE DATA TO FILE
    $filename = "../files/".$cmpcode."/sdbip_kpi_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "sdbip_kpi_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
}
else    //ELSE OUTPUT IS ONSCREEN DISPLAY
{
//SET DISPLAY OF HTML HEAD & BODY
$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>SDBIP Assist 2008/9: Departmental KPI Report</b></h1>";
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    $display .= "<p>There are no KPIs that meet your criteria.<br>Please go back and select different criteria.</p>";
}
//ELSE
else
{
    //CREATE TABLE AND SET TDHEADER
    $display .= "<table border=1 cellspacing=0 cellpadding=2>	<tr class=tdheader>";
    if($farray['time'] == "Y")
    {
        $hrows = "2";
    }
    else
    {
        $hrows = "1";
    }
        if($farray['dept'] == "Y") {$display .= "<td rowspan=".$hrows.">Dept</td>";}
        foreach($head as $hd)
        {
            if($farray[$hd['field']] == "Y") {$display .= "<td rowspan=".$hrows.">".$hd['head']."</td>";}
        }
        if($farray['time'] == "Y")
        {
            foreach($time as $tm)
            {
                $display .= "<td colspan=4>".date("d-M-Y",$tm['e'])."</td>";
            }
        	$display .= "</tr><tr class=tdheader>";
            foreach($time as $tm)
            {
                $display .= "<td>Target</td>";
                $display .= "<td>Actual</td>";
                $display .= "<td>R</td>";
                $display .= "<td>Comment</td>";
            }

        }

	$display .= "</tr>";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $display .= "<tr class=tdgeneral valign=top>";
        if($farray['dept'] == "Y") {$display .= "<td>".$row['dept']."&nbsp;</td>";}
        foreach($head as $hd)
        {
            if($farray[$hd['field']] == "Y") {$display .= "<td>".$row[$hd['field']]."&nbsp;</td>";}
        }
        if($farray['time'] == "Y")
        {
            $krr = "";
            $krstyle = "";
            $t = 1;
            foreach($time as $tm)
            {
                if($t>1)
                {
                    $row = mysql_fetch_array($rs);
                }
                $krt = $row['krtarget'];
                $kra = $row['kractual'];
                $krc = $row['krcomment'];
                $kr0 = $row['zeroyn'];
                if($kr0 == "N")
                {
                    if($krt>0)
                    {
                        if($kra>0)
                        {
                            $krr = $kra / $krt;
                        }
                        else
                        {
                            $krr = "&nbsp;";
                            $krstyle = "";
                        }
                    }
                    else
                    {
                        if($kra>0)
                        {
                            $krr = 1;
                        }
                        else
                        {
                            $krr = "&nbsp;";
                            $krstyle = "";
                        }
                    }
                }
                else
                {
                    $krr = $kr0;
                }
                if($krr != "&nbsp;")
                {
                    if($krr<0.75)
                    {
                        $krr = "R";
                        $krstyle = "style=\"background-color: #CC0001; color: #CC0001;\"";
                    }
                    else
                    {
                        if($krr<1)
                        {
                            $krr = "O";
                            $krstyle = "style=\"background-color: #FE9900; color: #FE9900;\"";
                        }
                        else
                        {
                            $krr = "G";
                            $krstyle = "style=\"background-color: #006600; color: #006600;\"";
                        }
                    }
                }
                $display .= "<td align=center>".number_format($krt,0)."</td>";
                $display .= "<td align=center>".number_format($kra,0)."</td>";
                $display .= "<td align=center ".$krstyle.">".$krr."</td>";
                $display .= "<td align=center>".str_replace(chr(10),"<br>",$krc)."&nbsp;</td>";
                $t++;
            }
        }
        else
        {
            $t = 1;
            foreach($time as $tm)
            {
                if($t>1)
                {
                    $row = mysql_fetch_array($rs);
                }
                $t++;
            }
        }
        $display .= "</tr>";

    }
    $display .= "</table>";
}
mysql_close();
$display .= "</body></html>";
//WRITE DISPLAY TO SCREEN
echo($display);


}   //ENDIF CSVFILE = Y

?>

