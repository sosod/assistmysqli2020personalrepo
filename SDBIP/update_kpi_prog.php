<?php include("inc_ignite.php");

$act = $_GET['act'];

switch($act)
{
    case "add":
        $val = $_GET['addtxt'];
        $val = str_replace("'","&#39",$val);
        $t = $_GET['t'];
        $d = $_GET['d'];
        $sql = "INSERT INTO assist_".$cmpcode."_sdbip_prog SET value = '".$val."', yn = 'Y', typeid = ".$t.", deptid = ".$d;
        include("inc_db_con.php");
        break;
    case "edit":
        $val = $_GET['v'];
        $val = str_replace("'","&#39",$val);
        $i = $_GET['i'];
        $d = $_GET['d'];
        $sql = "UPDATE assist_".$cmpcode."_sdbip_prog SET value = '".$val."' WHERE id = ".$i." AND deptid = ".$d;
        include("inc_db_con.php");
        break;
    case "del":
        $i = $_GET['i'];
        $d = $_GET['d'];
        $sql = "UPDATE assist_".$cmpcode."_sdbip_prog SET yn = 'N' WHERE id = ".$i." AND deptid = ".$d;
        include("inc_db_con.php");
        break;
    default:
        break;
}




 ?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
var addtxt = me.value;
if(addtxt.length > 0)
    return true;
else
    return false;
}

function editTop(id,val,d) {
    while(val.indexOf("_")>0)
    {
        val = val.replace("_"," ");
    }
    while(val.indexOf("|")>0)
    {
        val = val.replace("|","'");
    }
    if(confirm("Warning: Editing an obj/prog will affect all KPIs that are associated with the obj/prog.\n\nAre you sure you wish to continue editing '"+val+"'?") == true)
    {
        var editprog = prompt("Please edit the obj/prog:",val);
        //alert(editprog);
        document.location.href = "update_kpi_prog.php?i="+id+"&act=edit&v="+editprog+"&d="+d;
    }
}

function delTop(id,val,d) {
    while(val.indexOf("_")>0)
    {
        val = val.replace("_"," ");
    }
    while(val.indexOf("|")>0)
    {
        val = val.replace("|","'");
    }
    if(confirm("Warning: Deleting an obj/prog will affect all KPIs that are associated with the obj/prog.\n\nAre you sure you wish to delete '"+val+"'?") == true)
    {
        //alert(editprog);
        document.location.href = "update_kpi_prog.php?i="+id+"&act=del&d="+d;
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Setup - Update Objectives/Programmes</b></h1>
<p>&nbsp;</p>
<?php
$deptid = $_GET['d'];
?>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader>ID</td>
        <td class=tdheader>Objective/Programme</td>
        <td class=tdheader>KPIs linked<br>to Obj/Prog</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=4><span class=fc><b>Capital Project Objectives/Programmes</b></span></td>
    </tr>
<form name=add method=get action=update_kpi_prog.php onsubmit="return Validate(this);" language=jscript><input type=hidden value=add name=act>
    <tr>
        <td class=tdheader>&nbsp;</td>
        <td class=tdgeneral colspan=2><input type=text name=addtxt maxlength=255 size=50><input type=hidden value=2 name=t><input type=hidden name=d value=<?php echo($deptid); ?>></td>
        <td class=tdheader><input type=submit value=Add></td>
    </tr>
</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT p.id, p.value, 0 as kc FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= " WHERE p.id NOT IN (SELECT p.id FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= ", assist_".$cmpcode."_sdbip_kpi k WHERE p.yn = 'Y' AND p.deptid = ".$deptid;
$sql.= " AND p.typeid = 2 AND p.id = k.kpiprogid GROUP BY p.id)";
$sql.= " AND p.yn = 'Y' AND p.deptid = ".$deptid." AND p.typeid = 2";
$sql.= " UNION";
$sql.= " SELECT p2.id, p2.value, count(kpiid) as kc";
$sql.= " FROM assist_".$cmpcode."_sdbip_prog p2 , assist_".$cmpcode."_sdbip_kpi k";
$sql.= " WHERE p2.yn = 'Y' AND p2.deptid = ".$deptid." AND p2.typeid = 2 AND p2.id = k.kpiprogid";
$sql.= " GROUP BY p2.id, p2.value";
$sql.= " ORDER BY value";

/*$sql = "SELECT p.id, p.value, count(kpiid) as kc ";
$sql.= " FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= " , assist_".$cmpcode."_sdbip_kpi k";
$sql.= " WHERE p.yn = 'Y' ";
$sql.= " AND p.deptid = ".$deptid." ";
$sql.= " AND p.typeid = 2 ";
$sql.= " AND p.id = k.kpiprogid ";
$sql.= " GROUP BY p.id, p.value";
$sql.= " ORDER BY p.value";*/
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $val = $row['value'];
    $val = str_replace(" ","_",$val);
    $val = str_replace("&#39","|",$val);
?>
    <tr>
        <td class=tdheader><?php echo($row['id']); ?></td>
        <td class=tdgeneral><?php echo($row['value']); ?></td>
        <td class=tdgeneral align=center><?php echo($row['kc']); ?></td>
        <td class=tdheader><?php echo("<input type=button value=Edit onclick=editTop(".$row["id"].",'".$val."',".$deptid.")> <input type=button value=Del onclick=delTop(".$row["id"].",'".$val."',".$deptid.")>"); ?></td>
    </tr>
<?php
}
mysql_close();
?>
    <tr>
        <td class=tdgeneral colspan=4><span class=fc><b>Operational Objectives/Programmes</b></span></td>
    </tr>
<form name=add2 method=get action=update_kpi_prog.php onsubmit="return Validate(this);" language=jscript><input type=hidden value=add name=act>
    <tr>
        <td class=tdheader>&nbsp;</td>
        <td class=tdgeneral colspan=2><input type=text name=addtxt maxlength=255 size=50><input type=hidden value=3 name=t><input type=hidden name=d value=<?php echo($deptid); ?>></td>
        <td class=tdheader><input type=submit value=Add></td>
    </tr>
</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT p.id, p.value, 0 as kc FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= " WHERE p.id NOT IN (SELECT p.id FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= ", assist_".$cmpcode."_sdbip_kpi k WHERE p.yn = 'Y' AND p.deptid = ".$deptid;
$sql.= " AND p.typeid = 3 AND p.id = k.kpiprogid GROUP BY p.id)";
$sql.= " AND p.yn = 'Y' AND p.deptid = ".$deptid." AND p.typeid = 3";
$sql.= " UNION";
$sql.= " SELECT p2.id, p2.value, count(kpiid) as kc";
$sql.= " FROM assist_".$cmpcode."_sdbip_prog p2 , assist_".$cmpcode."_sdbip_kpi k";
$sql.= " WHERE p2.yn = 'Y' AND p2.deptid = ".$deptid." AND p2.typeid = 3 AND p2.id = k.kpiprogid";
$sql.= " GROUP BY p2.id, p2.value";
$sql.= " ORDER BY value";
/*$sql = "SELECT p.id, p.value, count(kpiid) as kc ";
$sql.= " FROM assist_".$cmpcode."_sdbip_prog p";
$sql.= " , assist_".$cmpcode."_sdbip_kpi k";
$sql.= " WHERE p.yn = 'Y' ";
$sql.= " AND p.deptid = ".$deptid." ";
$sql.= " AND p.typeid = 3 ";
$sql.= " AND p.id = k.kpiprogid ";
$sql.= " GROUP BY p.id, p.value";
$sql.= " ORDER BY p.value";*/
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $val = $row['value'];
    $val = str_replace(" ","_",$val);
    $val = str_replace("&#39","|",$val);
?>
    <tr>
        <td class=tdheader><?php echo($row['id']); ?></td>
        <td class=tdgeneral><?php echo($row['value']); ?></td>
        <td class=tdgeneral align=center><?php echo($row['kc']); ?></td>
        <td class=tdheader><?php echo("<input type=button value=Edit onclick=editTop(".$row["id"].",'".$val."',".$deptid.")> <input type=button value=Del onclick=delTop(".$row["id"].",'".$val."',".$deptid.")>"); ?></td>
    </tr>
<?php
}
mysql_close();
?>
</table>
</body>

</html>
