<?php
    include("inc_ignite.php");
    include("inc_admin.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var deptid = me.deptid.value;
    var cateid = me.cateid.value;
    var capnum = me.capnum.value;
    var project = me.project.value;
    var budgr = me.budget.value;
    var sday = me.sday.value;
    var smon = me.smon.value;
    var syear = me.syear.value;
    var eday = me.eday.value;
    var emon = me.emon.value;
    var eyear = me.eyear.value;
    var valid8 = 0;
    var err = "";

    if(capnum.length==0)
    {
        alert("Please enter a Capital Project Number.");
    }
    else
    {
        if(deptid=="X")
        {
            alert("Please select a valid Department.");
        }
        else
        {
            if(cateid=="X")
            {
                alert("Please select a valid Project Category.");
            }
            else
            {
                if(project.length==0)
                {
                    alert("Please enter a Project Description.");
                }
                else
                {
                    if(budgr.length==0|| isNaN(parseInt(budgr)) || parseInt(budgr)==0)
                    {
                        alert("Please enter a valid budget (numbers only).");
                    }
                    else
                    {
                        if(sday.length==0 || sday.length>2 || isNaN(parseInt(sday)) || parseInt(sday)==0 || syear.length<2 || syear.length>4 || isNaN(parseInt(syear)) || parseInt(syear)==0)
                        {
                            alert("Please enter a valid start date.");
                        }
                        else
                        {
                            if(eday.length==0 || eday.length>2 || isNaN(parseInt(eday)) || parseInt(eday)==0 || eyear.length<2 || eyear.length>4 || isNaN(parseInt(eyear)) || parseInt(eyear)==0)
                            {
                                alert("Please enter a valid end date.");
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}

function changeDept(me) {
    var dept = me.value;
    var defsel;
    var sel;
    //alert(cate[dept]);
    document.newcp.cateid.options.length=0
    if (dept!="X")
    {
        for (i=0; i<cate[dept].length; i++)
        {
            if(i==0)
            {
                defsel = true;
                sel = true;
            }
            else
            {
                defsel = false;
                sel = false;
            }
            document.newcp.cateid.options[document.newcp.cateid.options.length]=new Option(cate[dept][i].split("|")[0], cate[dept][i].split("|")[1],defsel,sel)
        }
    }
    else
    {
        document.newcp.cateid.options[0]=new Option("Please select a department first", "X", true, true)
    }
}

function delCP(c) {
    if(confirm("Are you sure you want to delete this capital project?\n\nThis will also delete the associated departmental KPI.")==true)
    {
        var url = "update_cp_del.php?c="+c;
        //alert(url);
        document.location.href = url;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php

$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();


$cpid = $_GET['c'];

$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_projects WHERE id = ".$cpid;
include("inc_db_con.php");
    $cprow = mysql_fetch_array($rs);
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE id = ".$cprow['cateid'];
include("inc_db_con.php");
    $caterow = mysql_fetch_array($rs);
mysql_close();
    $catedept = $caterow['deptid'];
    
    
    
if($moncash == "Y")
{
    $mbudget = 0;
    $mactual = 0;
    for($t=1;$t<13;$t++)
    {
        $mbudget = $mbudget + $cprow['m'.$t.'budget'];
        $mactual = $mactual + $cprow['m'.$t.'actual'];
    }
}
?>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009 - Edit</b></h1>
<form name=newcp action=update_cp_edit_process.php method=post onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=c value=<?php echo($cpid); ?>>
<table cellpadding=3 cellspacing=0 border=1>
    <tr class=tdgeneral>
        <td><b>Capital Project Number:</b></td>
        <td><input type=text size=15 name=capnum value="<?php echo($cprow['capnum']);?>"></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>PMS Ref:</b></td>
        <td><input type=text size=15 name=pmsref value="<?php echo($cprow['pmsref']);?>"></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Department:</b></td>
        <td><select name=deptid onchange=changeDept(this)><option value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
        include("inc_db_con.php");
        $dr = 0;
        while($row = mysql_fetch_array($rs))
        {
            $deptrow[$dr] = $row;
            if($catedept == $row['id'])
            {
                echo("<option selected value=".$row['id'].">".$row['value']."</option>");
            }
            else
            {
                echo("<option value=".$row['id'].">".$row['value']."</option>");
            }
            $dr++;
        }
        mysql_close();
        ?>
        </select></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Category:</b></td>
        <td><select name=cateid>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE yn = 'Y' AND deptid = ".$catedept;
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                if($row['id']==$caterow['id'])
                {
                    echo("<option selected value=".$row['id'].">".$row['value']." (".$row['code'].")</option>");
                }
                else
                {
                    echo("<option value=".$row['id'].">".$row['value']." (".$row['code'].")</option>");
                }
            }
        mysql_close();
        ?>
        </select></td>
        <script language=JavaScript>
        var cate=new Array();
        cate[0]="";
        <?php
    foreach($deptrow as $dept)
    {
        $d = $dept['id'];
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE yn = 'Y' AND deptid = ".$d;
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $carr = "cate[".$d."] = [\"".$row['value']." (".$row['code'].")|".$row['id']."\"";
            while($row = mysql_fetch_array($rs))
            {
                $carr.= ", \"".$row['value']." (".$row['code'].")|".$row['id']."\"";
            }
        mysql_close();
        $carr.= "];";
        echo($carr);
    }
        ?>
        </script>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Description:</b></td>
        <td><input type=text size=50 name=project value="<?php echo($cprow['project']);?>"></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Wards:</b></td>
        <td><input type=text size=15 name=wards value="<?php echo($cprow['wards']);?>"></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Funding Source:</b></td>
        <td><input type=text size=15 name=fundsource value="<?php echo($cprow['fundsource']);?>"></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Budget:</b></td>
    <?php if($moncash != "Y") { ?>
        <td>R <input type=text size=5 style="text-align: right" name=budget value="<?php echo($cprow['budget']);?>"></td>
    <?php } else { ?>
        <td>R <?php echo(number_format($mbudget,2)); ?><input type=hidden size=5 style="text-align: right" name=budget value="<?php echo($mbudget);?>"></td>
    <?php } ?>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Total Spend:</b></td>
        <td>R <?php echo(number_format($mactual,2));?></td>
        <td><i>&nbsp;</i></td>
    </tr>
    <?php
    $smon = date("m",$cprow['startdate']);
    ?>
    <tr class=tdgeneral>
        <td><b>Start Date:</b></td>
        <td><input type=text size=3 name=sday value="<?php echo(date("d",$cprow['startdate']));?>"><select name=smon><?php
        for($m=1;$m<13;$m++)
        {
            $dt = mktime(0,0,0,$m,1,date("Y"));
            if($m == $smon)
            {
                echo("<option selected value=".$m.">".date("M",$dt)."</option>");
            }
            else
            {
                echo("<option value=".$m.">".date("M",$dt)."</option>");
            }
        }
        ?></select><input type=text size=4 name=syear value="<?php echo(date("Y",$cprow['startdate']));?>"></td>
        <td><i>* required</i></td>
    </tr>
    <?php
    $emon = date("m",$cprow['enddate']);
    ?>
    <tr class=tdgeneral>
        <td><b>End Date:</b></td>
        <td><input type=text size=3 name=eday value="<?php echo(date("d",$cprow['enddate']));?>"><select name=emon><?php
        for($m=1;$m<13;$m++)
        {
            $dt = mktime(0,0,0,$m,1,date("Y"));
            if($m == $emon)
            {
                echo("<option selected value=".$m.">".date("M",$dt)."</option>");
            }
            else
            {
                echo("<option value=".$m.">".date("M",$dt)."</option>");
            }
        }
        ?></select><input type=text size=4 name=eyear value="<?php echo(date("Y",$cprow['enddate']));?>"></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td colspan=3><input type=submit value=Update> <input type=reset> <input type=button value=Delete onclick=delCP(<?php echo($cpid); ?>)></td>
    </tr>
</table>

</form>
</body>

</html>
