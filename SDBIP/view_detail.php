<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>SDBIP</title>
</head>

<body>

				<p align="center"><b>
				<font face="Arial" size="5" color="#CC0000">Performance Management 2008/9<br>
				</font><font face="Arial" size="4" color="#CC0000">MUNICIPALITY</font><font face="Arial" size="5" color="#CC0000"><br>
				</font><font face="Arial" color="#CC0000">DEPARTMENT</font></b></p>
	<table border="1" id="table4" cellspacing="0" cellpadding="3">
	<tr>
		<td><b><font face="Arial" size="2">National KPA:</font></b></td>
		<td><font face="Arial" size="2">FV</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Strategy / Programme:</font></b></td>
		<td><font face="Arial" size="2">Financial Performance</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Capital Prog. Number:&nbsp;</font></b><font size="2" face="Arial">
		</font>
		</td>
		<td><font face="Arial" size="2">&nbsp;</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Key Performance Indicator:</font></b></td>
		<td><font face="Arial" size="2">Revenue</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Type:</font></b></td>
		<td><font face="Arial" size="2">Process</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">KPI: Definition / <br>
		Unit of Measurement:</font></b></td>
		<td><font face="Arial" size="2">Revenue collected in line or exceeding 
		budget&nbsp;</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Wards:</font></b></td>
		<td><font face="Arial" size="2">&nbsp;</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Program Driver:</font></b></td>
		<td><font face="Arial" size="2">Municipal Manager</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Baseline:</font></b></td>
		<td><font face="Arial" size="2">95%</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Target Unit:</font></b></td>
		<td><font face="Arial" size="2">% of revenue billed</font></td>
	</tr>
	<tr>
		<td><b><font face="Arial" size="2">Performance Weighting:</font></b></td>
		<td><font face="Arial" size="2">&nbsp;</font></td>
	</tr>
</table>
<p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>

<table border="1" id="table5" cellpadding=2 cellspacing=0>
	<tr>
		<td colspan="3" align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">30 Sep 
		2008</font></b></td>
		<td colspan="3" align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">31 Dec 
		2008</font></b></td>
		<td colspan="3" align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">31 Mar 
		2009</font></b></td>
		<td colspan="3" align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">30 June 
		2009</font></b></td>
	</tr>
	<tr>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">R</font></b></td>
	</tr>
	<tr>
		<td valign=center align=center ><font face="Arial" size="2">47</font></td>
		<td align="center"><input type="text" name="T49" size="3" value="37"></td>
		<td align="center" bgcolor="#008000">
		<font face="Arial" size="1" color="#008000">G</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T61" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T51" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T52" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center ><font face="Arial" size="2">27</font></td>
		<td align="center"><input type="text" name="T53" size="3" value="23"></td>
		<td align="center" bgcolor="#FF9900">
		<font face="Arial" size="1" color="#FF9900">O</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T62" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T55" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T56" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center ><font face="Arial" size="2">39</font></td>
		<td align="center"><input type="text" name="T57" size="3" value="27"></td>
		<td align="center" bgcolor="#CC0000">
		<font face="Arial" size="1" color="#CC0000">R</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T63" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T59" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T60" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center ><font face="Arial" size="2">0</font></td>
		<td align="center"><input type="text" name="T2" size="3" value="0"></td>
		<td align="center" bgcolor="#008000">
		<font face="Arial" size="1" color="#008000">G</font></td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T47" size="3" value="6"></td>
		<td align="center" bgcolor="#FF9900">
		<font face="Arial" size="1" color="#FF9900">O</font></td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T64" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T48" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
</table>

<p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>

	<table border="1" id="table6" cellspacing="0" cellpadding="3">
	<tr>
		<td colspan="2"><b><font face="Arial" size="4">Comments:</font></b></td>
	</tr>
	<tr>
		<td valign="top"><font face="Arial" size="2"><b>30 September 2008:</b></font></td>
		<td><textarea rows="3" name="S30" cols="30"></textarea></td>
	</tr>
	<tr>
		<td valign="top"><font face="Arial" size="2"><b>31 December 2008:</b></font></td>
		<td><textarea rows="3" name="S34" cols="30"></textarea></td>
	</tr>
	<tr>
		<td valign="top"><font face="Arial" size="2"><b>31 March 2008:</b></font></td>
		<td><textarea rows="3" name="S35" cols="30"></textarea></td>
	</tr>
	<tr>
		<td valign="top"><font face="Arial" size="2"><b>30 June 2008:</b></font></td>
		<td><textarea rows="3" name="S36" cols="30"></textarea></td>
	</tr>
	</table>
				<p><input type="button" value="Update" name="B3" onclick="document.location.href='main.php';"></p>

</body>

</html>