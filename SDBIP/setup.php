<?php
    include("inc_ignite.php");

$result = "";
$sadm = $_POST['sadm'];
$a = $_POST['a'];
if(strlen($sadm) > 0 && $a == "admin")
{
    //GET SETUP ADMIN DATA
    $sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 0";
    include("inc_db_con.php");
    $u = mysql_num_rows($rs);
    if($u == 0)
    {
        //IF NO RESULT THEN CREATE RECORD
        $sql2 = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'SDBIP', refid = 0, value = '".$sadm."', comment = 'Setup administrator', field = ''";
    }
    else
    {
        //IF RESULT THEN UPDATE RECORD
        $sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '".$sadm."' WHERE ref = 'SDBIP' AND refid = 0";
    }
    mysql_close();
    //RUN SQL CREATED ABOVE
    $sql = $sql2;
    include("inc_db_con.php");
        $tsql = $sql;
        $tref = "SDBIP";
        $trans = "SDBIP setup administrator updated.";
        include("inc_transaction_log.php");
    $sql = "";
    $result = "<p><i>SDBIP Assist Setup Administrator update complete.</i></p>";

}


    include("inc_admin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>SDBIP Assist: Setup</b></h1>
<?php echo($result); ?>
<ul>
    <form name=admin method=post action=setup.php>
        <li>SDBIP Assist Administrator: <select name=sadm>
            <?php
                if($setupadmin == "0000")
                {
                    echo("<option selected value=0000>Ignite Assist Administrator</option>");
                }
                else
                {
                    echo("<option value=0000>Ignite Assist Administrator</option>");
                }
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid <> '0000' AND u.usrtkid = t.tkid AND u.usrmodref = 'SDBIP' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    if($id == $setupadmin)
                    {
                        echo("<option selected value=".$id.">".$tk."</option>");
                    }
                    else
                    {
                        echo("<option value=".$id.">".$tk."</option>");
                    }
                }
                mysql_close();
            ?>
        </select>&nbsp;<input type=hidden name=a value=admin><input type=submit value=Update></li>
    </form>
<?php //    <li><a href=setup_dept.php>Departments</a></li>?>
    <li><a href=setup_users.php?t=3>Departmental Administrators</a><br>
    <i>These are the people responsible for adding new KPIs and updating the KPI results for the specified department.</i></li>
    <li><a href=setup_users.php?t=1>Financial Administrators</a><br>
    <i>These are the people responsible for updating the financial results for the municipality.</i></li>
    <li><a href=setup_users.php?t=2>Capital Projects Administrators</a><br>
    <i>These are the people responsible for adding and updating the capital projects for the municipality.</i></li>
    <li><a href=setup_users.php?t=5>KPI Administrators</a><br>
    <i>These are the people responsible for deleting KPIs and editing all KPI details (including targets) for the municipality.</i></li>
</ul>
<?php
$helpfile = "../help/SDBIP_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</body>

</html>
