<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009</b></h1>
<p style="margin-top: -12px; margin-bottom: 0px"><label id=label1 for=lbl>Loading, please wait...</label><input type=hidden name=lbl></p>
<?php
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();


//CHECK WHETHER THERE ARE ANY CAPITAL PROJECTS TO DISPLAY
$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id ";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
mysql_close();

if($cp > 0)     //IF THERE ARE CPs TO DISPLAY
{
    //GET LIST OF DEPARTMENTS
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $d = 0;
        while($row = mysql_fetch_array($rs))
        {
            $dept[$d] = $row;
            $d++;
        }
    mysql_close();
?>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr class=tdheader>
		<td>No.</td>
		<td>PMS<br>Ref.</td>
		<td>Project</td>
		<td>Wards</td>
		<td>Funding<br>Source</td>
<?php if($moncash == "N") { ?>
		<td>Budget</td>
<?php //		<td>Progress</td> ?>
		<td>Total<br>Spend</td>
		<td>Balance</td>
<?php } ?>
		<td>Start<br>Date</td>
		<td>End<br>Date</td>
	</tr>
	<?php
    foreach($dept as $deptrow)
    {
    ?>
	<tr class=tdheaderbluel>
		<td colspan="12"><?php echo($deptrow['value']);?></td>
	</tr>
    <?php
        $sql = "SELECT c.id as cid, p.id as pid, c.*, p.* FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
        $sql.= "assist_".$cmpcode."_sdbip_capital_category c ";
        $sql.= "WHERE p.yn = 'Y' ";
        $sql.= "AND c.yn = 'Y' ";
        $sql.= "AND p.cateid = c.id ";
        $sql.= "AND c.deptid = ".$deptrow['id']." ORDER BY c.value";
        include("inc_db_con.php");
            $cateid2 = 0;
            $cpdept = mysql_num_rows($rs);  //CHECK WHETHER THERE ARE CPs FOR THIS DEPT
            if($cpdept > 0)     //IF THERE ARE CPs FOR THIS DEPT
            {
                while($row = mysql_fetch_array($rs))
                {
                    $cateid1 = $row['cid'];
                    if($cateid1 != $cateid2)
                    {
    ?>
	<tr class=tdheadergreenl>
		<td colspan="12"><?php echo($row['value']); ?></td>
	</tr>
                <?php
                        $cateid2 = $cateid1;
                    }
                    $budg = $row['budget'];
                    $tots = $row['totalspend'];
                    $rem = $budg - $tots;
                    $rem = number_format($rem,0);
                ?>
	<tr class=tdgeneral valign=top>
		<td align=center><?php echo($row['capnum']); ?></td>
		<td align=center><?php echo($row['pmsref']); ?>&nbsp;</td>
		<td><?php echo("<a href=update_cp_edit.php?c=".$row['pid'].">".$row['project']); ?></a>&nbsp;</td>
		<td align=center><?php echo($row['wards']); ?>&nbsp;</td>
		<td align=center><?php echo($row['fundsource']); ?>&nbsp;</td>
<?php if($moncash == "N") { ?>
		<td align=right>R <?php echo(number_format($row['budget'],0)); ?></span></td>
<?php //		<td><?php echo($row['project']); &nbsp;</td>?>
		<td align=right>R <?php echo(number_format($row['totalspend'],0)); ?></span></td>
		<td align=right><?php if($rem < 0){ echo("<font color=red><b>"); } echo("R ".$rem); ?></b></font></span></td>
<?php } ?>
		<td align=center><?php echo(date("d M Y",$row['startdate'])); ?></td>
		<td align=center><?php echo(date("d M Y",$row['enddate'])); ?></td>
	</tr>
	<?php
                }       //WHILE ROW FETCH
            }       //IF NO CPs FOR THIS DEPT
            else
            {
            ?>
            	<tr class=tdgeneral>
            		<td colspan="12">This department currently has no capital projects.</td>
            	</tr>
            <?php
            }
        mysql_close();
    }       //FOREACH DEPT
    ?>
</table>
<?php
}
else        //IF NO CAPITAL PROJECTS TO DISPLAY
{
    echo("<p>There are no capital projects to display.</p>");
}
?>
<script language=JavaScript>
label1.innerText="";
</script>
</body>

</html>
