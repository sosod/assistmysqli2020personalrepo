<?php
    include("inc_ignite.php");

    $a = $_GET['a'];
    $i = $_GET['i'];
    $d = $_GET['d'];
    
    switch($a) {
        case "add":
            $sql = "INSERT INTO assist_".$cmpcode."_sdbip_dept SET value = '".$d."', yn = 'Y'";
            include("inc_db_con.php");
                $tsql = $sql;
                $tref = "SDBIP";
                $trans = "Added new SDBIP department.";
                include("inc_transaction_log.php");
            break;
        case "del":
            $sql = "UPDATE assist_".$cmpcode."_sdbip_dept SET yn = 'N' WHERE id = ".$i;
            include("inc_db_con.php");
                $tsql = $sql;
                $tref = "SDBIP";
                $trans = "Removed SDBIP department ".$i.".";
                include("inc_transaction_log.php");
            break;
        case "edit":
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE id = ".$i;
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                $d0 = $row['value'];
            mysql_close();
            $sql = "UPDATE assist_".$cmpcode."_sdbip_dept SET value = '".$d."' WHERE id = ".$i;
            include("inc_db_con.php");
                $tsql = $sql;
                $tref = "SDBIP";
                $trans = "Edited SDBIP department ".$i." - changed value from ".$d0." to ".$d.".";
                include("inc_transaction_log.php");
            break;
        default:
            break;
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
    function delDept(i,d) {
        while(d.indexOf("_") > 0)
        {
            d = d.replace("_"," ");
        }
        while(d.indexOf("&#39") > 0)
        {
            d = d.replace("&#39","'");
        }
        if(confirm("Are you sure you want to delete department '"+d+"'?")==true)
        {
            document.location.href = "setup_dept.php?a=del&i="+i;
        }
    }

    function editDept(i,d) {
        while(d.indexOf("_") > 0)
        {
            d = d.replace("_"," ");
        }
        while(d.indexOf("&#39") > 0)
        {
            d = d.replace("&#39","'");
        }
        if(confirm("Are you sure you want to edit department '"+d+"'?")==true)
        {
            d2 = prompt("Please enter the new department name:",d);
            document.location.href = "setup_dept.php?a=edit&i="+i+"&d="+d2;
        }
    }

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>SDBIP Assist: Setup - Departments</b></h1>
<?php echo($result); ?>
<form name=adddept action=setup_dept.php method=get>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader>Department</td>
        <td class=tdheader>Action</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $id = $row['id'];
        $val = $row['value'];
        $val2 = str_replace(" ","_",$val);
?>
    <tr>
        <td class=tdgeneral><?php echo($val); ?></td>
        <td class=tdgeneral><input type=button value=Edit onclick="editDept('<?php echo($id);?>','<?php echo($val2); ?>')"> <input type=button value=Del onclick="delDept('<?php echo($id);?>','<?php echo($val2); ?>')"></td>
    </tr>
<?php
    }
mysql_close();
?>
    <tr>
        <td class=tdgeneral><input type=text name=d size=30 maxlength=50><input type=hidden name=a value=add></td>
        <td class=tdgeneral><input type=submit value=Add></td>
    </tr>
</table>
</form>
<?php
$helpfile = "../help/SDBIP_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</body>

</html>
