<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009</b></h1>

<ul style="margin-top: 30px">
<?php

$ctot = 0;

$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id ";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
    $ctot = $ctot+$cp;
mysql_close();
if($cp>0)
{
?>
    <li><a href=cp_view.php>Capital Projects for 2008/2009</a></li>
<?php
}

$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();

if($moncash=="Y")
{
$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id ";
$sql.= "AND (m1budget > 0 ";
$sql.= "  OR m1actual > 0 ";
$sql.= "  OR m2budget > 0 ";
$sql.= "  OR m2actual > 0 ";
$sql.= "  OR m3budget > 0 ";
$sql.= "  OR m3actual > 0 ";
$sql.= "  OR m4budget > 0 ";
$sql.= "  OR m4actual > 0 ";
$sql.= "  OR m5budget > 0 ";
$sql.= "  OR m5actual > 0 ";
$sql.= "  OR m6budget > 0 ";
$sql.= "  OR m6actual > 0 ";
$sql.= "  OR m7budget > 0 ";
$sql.= "  OR m7actual > 0 ";
$sql.= "  OR m8budget > 0 ";
$sql.= "  OR m8actual > 0 ";
$sql.= "  OR m9budget > 0 ";
$sql.= "  OR m9actual > 0 ";
$sql.= "  OR m10budget > 0 ";
$sql.= "  OR m10actual > 0 ";
$sql.= "  OR m11budget > 0 ";
$sql.= "  OR m11actual > 0 ";
$sql.= "  OR m12budget > 0 ";
$sql.= "  OR m12actual > 0) ";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
    $ctot = $ctot+$cp;
mysql_close();
if($cp>0)
{
?>
    <li style="margin-top: 15px"><a href=cp_view_monthly.php>Capital Projects for 2008/2009 - Monthly Cashflow</a></li>
<?php
}
}

$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_3year p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id ";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
    $ctot = $ctot+$cp;
mysql_close();
if($cp>0)
{
?>
    <li style="margin-top: 15px"><a href=cp_3year.php>3-Year Capital Project Program</a></li>
<?php
}

if($ctot==0)
{
    echo("<li>No Capital Project details to display.</li>");
}
?>
</ul>

</body>

</html>
