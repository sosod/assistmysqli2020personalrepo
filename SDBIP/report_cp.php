<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist 2008/9: Report</b></h1>
<form name=kpireport method=post action=report_cp_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the headings you want in your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table1" cellpadding="3" cellspacing="0">
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="cpfield[]" value="dept"></td>
		<td class="tdgeneral">Department</td>
	</tr>
<?php
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_headings WHERE fullyn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['field'] == "cate") { $catehead = $row['headfull']; }
        if(($row['field'] != "budget" && $row['field'] != "spend" && $row['field'] != "balance") || $moncash != "Y")
        {
?>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="cpfield[]" value="<?php echo($row['field']); ?>"></td>
		<td class="tdgeneral"><?php echo($row['headfull']); ?></td>
	</tr>
<?php
        }
    }
mysql_close();
if($moncash=="Y")
{
?>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="cpfield[]" value="mb"></td>
		<td class="tdgeneral">Monthly Budgets <select name=monthtype><option selected value=ytd>Year-to-date</option><option value=all>All months</option></select></td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="cpfield[]" value="ma"></td>
		<td class="tdgeneral">Monthly Actuals</td>
	</tr>
<?php
}
?>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3">
	<tr>
		<td class="tdgeneral">Department:</td>
		<td class="tdgeneral"><select size="1" name="dpt" onchange=changeDept(this)>
			<option selected value=ALL>All departments</option>
			<?php
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
                $dt = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $deptid[$dt] = $row['id'];
                    $dt++;
                    $id = $row['id'];
                    $value = str_replace("&#39","'",$row['value']);
                    echo("<option value=\"".$id."\">".$value."</option>");
                }
            mysql_close();
            ?>
		</select></td>
	</tr>
	<script language=JavaScript>
    var cpcate = new Array();
    cpcate[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, p.id, p.value FROM";
            $sql.= " assist_".$cmpcode."_sdbip_capital_category p";
            $sql.= " WHERE p.yn = 'Y'";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " ORDER BY value";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."cpcate[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                $value = $row['id']."_|_".str_replace("&#39","'",$row['value']);
                $echo.= $value;
                while($row = mysql_fetch_array($rs))
                {
                    $value = $row['id']."_|_".str_replace("&#39","'",$row['value']);
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo);
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral"><?php echo($catehead); ?>:</td>
		<td class="tdgeneral"><select size="1" name="ctg">
			<option selected value=ALL>All categories</option>
			<option value=ALL>Please specify the department</option>
		</select></td>
	</tr>
	<script language=JavaScript>
    var kpiward = new Array();
    kpiward[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, k.wards FROM";
            $sql.= " assist_".$cmpcode."_sdbip_capital_projects k,";
            $sql.= " assist_".$cmpcode."_sdbip_capital_category p";
            $sql.= " WHERE k.cateid = p.id";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " GROUP BY k.wards";
            $sql.= " ORDER BY deptid, wards";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."kpiward[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                        if(strlen($row['wards'])>0)
                        {
                            $value = $row['wards']."_|_".str_replace("&#39","'",$row['wards']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= $value;

                while($row = mysql_fetch_array($rs))
                {
                        if(strlen($row['wards'])>0)
                        {
                            $value = $row['wards']."_|_".str_replace("&#39","'",$row['wards']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo);
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral">Wards:</td>
		<td class="tdgeneral"><select size="1" name="wrd">
			<option selected value=X>Any ward</option>
			<option value=X>Please specify the department</option>
		</select></td>
	</tr>
	<script language=JavaScript>
    var kpifs = new Array();
    kpifs[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, k.fundsource FROM";
            $sql.= " assist_".$cmpcode."_sdbip_capital_projects k,";
            $sql.= " assist_".$cmpcode."_sdbip_capital_category p";
            $sql.= " WHERE k.cateid = p.id";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " GROUP BY k.fundsource";
            $sql.= " ORDER BY deptid, fundsource";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."kpifs[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                        if(strlen($row['fundsource'])>0)
                        {
                            $value = $row['fundsource']."_|_".str_replace("&#39","'",$row['fundsource']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= $value;

                while($row = mysql_fetch_array($rs))
                {
                        if(strlen($row['fundsource'])>0)
                        {
                            $value = $row['fundsource']."_|_".str_replace("&#39","'",$row['fundsource']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo);
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral">Funding Source:</td>
		<td class="tdgeneral"><select size="1" name="fs">
			<option selected value=X>Any source</option>
			<option value=X>Please specify the department</option>
		</select></td>
	</tr>
</table>
</div>
<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center>Sort by:</td>
		<td class="tdgeneral"><select name=rpsort><option selected value=0>Department</option>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_headings WHERE fullyn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if(($row['field'] != "budget" && $row['field'] != "spend" && $row['field'] != "balance") || $moncash != "Y")
        {
            echo("<option value=cp_".$row['id'].">".$row['headshort']."</option>");
        }
    }
mysql_close();
?>
        </select></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" id=csvn value="N" checked></td>
		<td class="tdgeneral"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" id=csvy value="Y"></td>
		<td class=tdgeneral><label for=csvy>Save to file (Microsoft Excel file)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>5. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>

<script language=JavaScript>
function changeDept(me) {
    var deptid = me.value;
    if(deptid!="ALL")
    {
        var i2;
        var optval;
        //CATE
        var ctg = cpcate[deptid].split("++|++");
        document.kpireport.ctg.length=1;
        i2 = ctg.length+1;
        var c=0;
        for (i=1; i<i2; i++)
        {
            c = i-1;
            optval = ctg[c].split("_|_");
            document.kpireport.ctg.options[i] = new Option(optval[1],optval[0]);
        }
        //WARDS
        var wrd = kpiward[deptid].split("++|++");
        document.kpireport.wrd.length=1;
        i2 = wrd.length+1;
        var w=0;
        for (i=1; i<i2; i++)
        {
            w = i-1;
            optval = wrd[w].split("_|_");
            document.kpireport.wrd.options[i] = new Option(optval[1],optval[0]);
        }
        //FUNDSOURCE
        var fs = kpifs[deptid].split("++|++");
        document.kpireport.fs.length=1;
        i2 = fs.length+1;
        var w=0;
        for (i=1; i<i2; i++)
        {
            w = i-1;
            optval = fs[w].split("_|_");
            document.kpireport.fs.options[i] = new Option(optval[1],optval[0]);
        }
    }
    else
    {
        document.kpireport.ctg.length=1;
        document.kpireport.wrd.length=1;
        document.kpireport.fs.length=1;
        document.kpireport.ctg.options[1] = new Option('Please specify the department','ALL');
        document.kpireport.wrd.options[1] = new Option('Please specify the department','X');
        document.kpireport.fs.options[1] = new Option('Please specify the department','X');
    }
}
</script>
</body>

</html>
