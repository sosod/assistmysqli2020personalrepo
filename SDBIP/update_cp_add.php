<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var deptid = me.deptid.value;
    var cateid = me.cateid.value;
    var capnum = me.capnum.value;
    var project = me.project.value;
    var budgr = me.budgr.value;
    var totr = me.totr.value;
    var sday = me.sday.value;
    var smon = me.smon.value;
    var syear = me.syear.value;
    var eday = me.eday.value;
    var emon = me.emon.value;
    var eyear = me.eyear.value;
    var valid8 = 0;
    var err = "";
    
    if(capnum.length==0)
    {
        alert("Please enter a Capital Project Number.");
    }
    else
    {
        if(deptid=="X")
        {
            alert("Please select a valid Department.");
        }
        else
        {
            if(cateid=="X")
            {
                alert("Please select a valid Project Category.");
            }
            else
            {
                if(project.length==0)
                {
                    alert("Please enter a Project Description.");
                }
                else
                {
                    if(budgr.length==0|| isNaN(parseInt(budgr)))
                    {
                        alert("Please enter a valid budget (numbers only).");
                    }
                    else
                    {
                        if(totr.length==0|| isNaN(parseInt(totr)))
                        {
                            me.totr.value = 0;
                        }
                        if(sday.length==0 || sday.length>2 || isNaN(parseInt(sday)) || parseInt(sday)==0 || syear.length<2 || syear.length>4 || isNaN(parseInt(syear)) || parseInt(syear)==0)
                        {
                            alert("Please enter a valid start date.");
                        }
                        else
                        {
                            if(eday.length==0 || eday.length>2 || isNaN(parseInt(eday)) || parseInt(eday)==0 || eyear.length<2 || eyear.length>4 || isNaN(parseInt(eyear)) || parseInt(eyear)==0)
                            {
                                alert("Please enter a valid end date.");
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}
function changeDept(me) {
    var dept = me.value;
    var defsel;
    var sel;
    //alert(cate[dept]);
    document.newcp.cateid.options.length=0
    if (dept!="X")
    {
        for (i=0; i<cate[dept].length; i++)
        {
            if(i==0)
            {
                defsel = true;
                sel = true;
            }
            else
            {
                defsel = false;
                sel = false;
            }
            document.newcp.cateid.options[document.newcp.cateid.options.length]=new Option(cate[dept][i].split("|")[0], cate[dept][i].split("|")[1],defsel,sel)
        }
    }
    else
    {
        document.newcp.cateid.options[0]=new Option("Please select a department first", "X", true, true)
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009 - Add new</b></h1>
<form name=newcp action=update_cp_add_process.php method=post onsubmit="return Validate(this);" language=jscript>
<table cellpadding=3 cellspacing=0 border=1>
    <tr class=tdgeneral>
        <td><b>Capital Project Number:</b></td>
        <td><input type=text size=15 name=capnum></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>PMS Ref:</b></td>
        <td><input type=text size=15 name=pmsref></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Department:</b></td>
        <td><select name=deptid onchange=changeDept(this)><option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
        include("inc_db_con.php");
        $dr = 0;
        while($row = mysql_fetch_array($rs))
        {
            $deptrow[$dr] = $row;
            echo("<option value=".$row['id'].">".$row['value']."</option>");
            $dr++;
        }
        mysql_close();
        ?>
        </select></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Category:</b></td>
        <td><select name=cateid><option selected value=X>Please select a department first</option></select></td>
        <script language=JavaScript>
        var cate=new Array();
        cate[0]="";
        <?php
    foreach($deptrow as $dept)
    {
        $d = $dept['id'];
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_capital_category WHERE yn = 'Y' AND deptid = ".$d;
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $carr = "cate[".$d."] = [\"".$row['value']." (".$row['code'].")|".$row['id']."\"";
            while($row = mysql_fetch_array($rs))
            {
                $carr.= ", \"".$row['value']." (".$row['code'].")|".$row['id']."\"";
            }
        mysql_close();
        $carr.= "];";
        echo($carr);
    }
        ?>
        </script>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Project Description:</b></td>
        <td><input type=text size=50 name=project></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Wards:</b></td>
        <td><input type=text size=15 name=wards></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Funding Source:</b></td>
        <td><input type=text size=15 name=fundsource></td>
        <td>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Budget:</b></td>
        <td>R <input type=text size=5 style="text-align: right" name=budgr></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Total Spend:</b></td>
        <td>R <input type=text size=5 value=0 style="text-align: right" name=totr></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>Start Date:</b></td>
        <td><input type=text size=3 value=1 name=sday><select name=smon><?php
        for($m=1;$m<13;$m++)
        {
            $dt = mktime(0,0,0,$m,1,date("Y"));
            if($m == 1)
            {
                echo("<option selected value=".$m.">".date("M",$dt)."</option>");
            }
            else
            {
                echo("<option value=".$m.">".date("M",$dt)."</option>");
            }
        }
        ?></select><input type=text size=4 name=syear></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td><b>End Date:</b></td>
        <td><input type=text size=3 value=1 name=eday><select name=emon><?php
        for($m=1;$m<13;$m++)
        {
            $dt = mktime(0,0,0,$m,1,date("Y"));
            if($m == 1)
            {
                echo("<option selected value=".$m.">".date("M",$dt)."</option>");
            }
            else
            {
                echo("<option value=".$m.">".date("M",$dt)."</option>");
            }
        }
        ?></select><input type=text size=4 name=eyear></td>
        <td><i>* required</i></td>
    </tr>
    <tr class=tdgeneral>
        <td colspan=3><input type=submit value=Add> <input type=reset></td>
    </tr>
</table>

</form>
</body>

</html>
