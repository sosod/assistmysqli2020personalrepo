<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009</b></h1>
<?php
//CHECK WHETHER THERE ARE ANY CAPITAL PROJECTS TO DISPLAY
$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_3year p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id ";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
mysql_close();

if($cp > 0)     //IF THERE ARE CPs TO DISPLAY
{
    //GET LIST OF DEPARTMENTS
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $d = 0;
        while($row = mysql_fetch_array($rs))
        {
            $dept[$d] = $row;
            $d++;
        }
    mysql_close();
    //GET LIST OF IDP
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_idp WHERE yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $idp[$row['id']] = $row['value'];
        }
    mysql_close();

?>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr class=tdheader>
		<td rowspan=2>No.</td>
		<td rowspan=2>IDP Link</td>
		<td rowspan=2>Project</td>
		<td rowspan=2>Vote Number</td>
		<td rowspan=2>Wards</td>
		<td rowspan=2>Funding<br>Source</td>
		<td rowspan=2>Strategy</td>
<?php
    //GET LIST OF Years
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_capyear WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $y=0;
        while($row = mysql_fetch_array($rs))
        {
            $y++;
            echo("<td colspan=2>".$row['value']."</td>");
        }
    mysql_close();

?>
	</tr>
	<tr class=tdheader>
<?php
    //GET LIST OF Years
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_capfund WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $fd[$row['id']] = $row['value'];
        }
    mysql_close();
    for($y2=0;$y2<$y;$y2++)
    {
        foreach($fd as $f)
        {
            echo("<td>".$f."</td>");
        }
    }
?>
	</tr>
	<?php
    foreach($dept as $deptrow)
    {
        $cols = 7 + ($y*2);
    ?>
	<tr class=tdheaderbluel>
		<td colspan="<?php echo($cols);?>"><?php echo($deptrow['value']);?></td>
	</tr>
    <?php
        $sql = "SELECT c.id as cid, p.id as pid, c.*, p.*, y.* FROM assist_".$cmpcode."_sdbip_capital_3year p, ";
        $sql.= "assist_".$cmpcode."_sdbip_capital_category c ";
        $sql.= ", assist_".$cmpcode."_sdbip_capital_3year_fund y ";
        $sql.= ", assist_".$cmpcode."_sdbip_list_capyear l ";
        $sql.= ", assist_".$cmpcode."_sdbip_list_capfund f ";
        $sql.= "WHERE p.yn = 'Y' ";
        $sql.= "AND y.progid = p.id ";
        $sql.= "AND y.yrid = l.id ";
        $sql.= "AND y.fundid = f.id ";
        $sql.= "AND c.yn = 'Y' ";
        $sql.= "AND p.cateid = c.id ";
        $sql.= "AND c.deptid = ".$deptrow['id'];
        $sql.= " ORDER BY p.capcode, p.capint, p.id, l.sort, f.sort";
        include("inc_db_con.php");
            $cateid2 = 0;
            $cpdept = mysql_num_rows($rs);  //CHECK WHETHER THERE ARE CPs FOR THIS DEPT
            if($cpdept > 0)     //IF THERE ARE CPs FOR THIS DEPT
            {
                while($row = mysql_fetch_array($rs))
                {
                    $cateid1 = $row['cid'];
                    if($cateid1 != $cateid2)
                    {
    ?>
	<tr class=tdheadergreenl>
		<td colspan="<?php echo($cols); ?>"><?php echo($row['value']); ?></td>
	</tr>
                <?php
                        $cateid2 = $cateid1;
                    }
                ?>
	<tr class=tdgeneral valign=top>
		<td align=center><?php echo($row['capnum']); ?></td>
		<td align=center><?php echo($idp[$row['idpid']]); ?>&nbsp;</td>
		<td><?php echo($row['project']); ?></a>&nbsp;</td>
		<td align=center><?php echo($row['votenum']); ?>&nbsp;</td>
		<td align=center><?php echo($row['wards']); ?>&nbsp;</td>
		<td align=center><?php echo($row['funding']); ?>&nbsp;</td>
		<td align=center><?php echo($row['strategy']); ?>&nbsp;</td>
    <?php
    $r = 1;
    for($y2=0;$y2<$y;$y2++)
    {
        foreach($fd as $f)
        {
            if($r>1)
            {
                $row = mysql_fetch_array($rs);
            }
            $r++;
            if($row['rands']>0)
            {
                echo("<td align=right>".number_format($row['rands'],0)."</td>");
            }
            else
            {
                echo("<td align=right>-</td>");
            }
        }
    }
    ?>
	</tr>
	<?php
                }       //WHILE ROW FETCH
            }       //IF NO CPs FOR THIS DEPT
            else
            {
            ?>
            	<tr class=tdgeneral>
            		<td colspan="12">This department currently has no capital projects.</td>
            	</tr>
            <?php
            }
        mysql_close();
    }       //FOREACH DEPT
    ?>
</table>
<?php
}
else        //IF NO CAPITAL PROJECTS TO DISPLAY
{
    echo("<p>There are no capital projects to display.</p>");
}
?>
</body>

</html>
