<?php
    include("inc_ignite.php");
	$style[1] = "style=\"background-color: #ffcccc; color: #000000;\"";
	$style[2] = "style=\"background-color: #ffeaca; color: #000000;\"";
	$style[3] = "style=\"background-color: #ceffde; color: #000000;\"";
	$style[4] = "style=\"background-color: #ccccff; color: #000000;\"";
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function changeMun(me) {
    var targetid = "munadd";
    var target = document.getElementById( targetid )
    var mun = me.value;
    if(mun == "ADD")
    {
        target.style.display = "inline";
    }
    else
    {
        target.style.display = "none";
    }
}
function changeProg(me) {
    var targetid = "progadd";
    var target = document.getElementById( targetid )
    var prog = me.value;
    if(prog == "ADD")
    {
        target.style.display = "inline";
    }
    else
    {
        target.style.display = "none";
    }
}

function clearProg(me,meval0) {
    var meval1 = me.value;
    if(meval1 == meval0)
        me.value="";
}

function Validate(me) {
    var munkpa = me.munkpa.value;
    var natkpa = me.natkpa.value;
    var progval = me.progval.value;
    var kpival = me.kpival.value;
    var unitmeasure = me.unitmeasure.value;
    var pdriver = me.driver.value;
    var ktarget = me.target.value;

    if(munkpa.length ==0 || munkpa == "X")
    {
        alert("Please select the relevant Municipal KPA.");
    }
    else
    {
        if(natkpa.length==0 || natkpa == "X")
        {
            alert("Please select the relevant National KPA.");
        }
        else
        {
            if(progval.length==0 || progval == "X")
            {
                alert("Please select the relevant Objective/Programme.");
            }
            else
            {
                var reqfld = "";
                if(kpival.length == 0)
                {
                    reqfld = "- Key Performance Indicator";
                }
                if(unitmeasure.length == 0)
                {
                    if(reqfld.length>0)
                    {
                        reqfld = reqfld+"\n";
                    }
                    reqfld = reqfld+"- KPI Definition / Unit of Measurement";
                }
                if(pdriver.length == 0)
                {
                    if(reqfld.length>0)
                    {
                        reqfld = reqfld+"\n";
                    }
                    reqfld = reqfld+"- Program Driver";
                }
                if(ktarget.length == 0)
                {
                    if(reqfld.length>0)
                    {
                        reqfld = reqfld+"\n";
                    }
                    reqfld = reqfld+"- Target Unit";
                }
                if(reqfld.length>0)
                {
                    alert("Please complete the following required fields:\n"+reqfld);
                }
                else
                {
                    return true;
                }
            }
        }
    }
//    alert("end");
    return false;
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP Assist 2008/9</h1><form name=addkpi action=update_kpi_add_process.php method=post onsubmit="return Validate(this);" language=jscript>
<?php
//$kpiid = $_GET['i'];
$deptid = $_GET['d'];
//if(strlen($kpiid) == 0)
//{
    $kpiid = 0;
    $kpirow['cpid'] = 0;
//}
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_headings WHERE fullyn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $h=0;
        while($row = mysql_fetch_array($rs))
        {
            $heading[$h] = $row;
            $h++;
        }
    mysql_close();
?>
<?php echo("<input type=hidden name=d value=".$deptid."><input type=hidden name=capnum value=".$kpirow['cpid'].">"); ?>
<?php
if($kpirow['cpid'] != 0)
{
    echo("<h3>KPI Details</h3>");
}
?>
<table border="1" id="table4" cellspacing="0" cellpadding="3" width=600>
<?php
foreach($heading as $head)
{
    if($head['field'] != "capnum")
    {
        echo("<tr class=tdgeneral valign=top>");
        switch($head['field'])
        {
            case "munkpa":
                echo("<td><b>".$head['headfull'].":</b></td>");
               	$form = "<select name=munkpa onchange=\"changeMun(this)\"><option selected value=X>--- SELECT ---</option>";
                $form.= "<option value=ADD>Add New</option>";
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_kpa WHERE yn = 'Y' AND type='M' ORDER BY value";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $form.= "<option value=".$row['id'].">".$row['value']." (".$row['code'].")</option>";
                    }
                mysql_close();
                $form.= "</select>";
                $form.= "<span id=munadd><br><input type=text size=40 name=munnew value=\"Enter Municipal KPA here...\" onfocus='clearProg(this,\"Enter Municipal KPA here...\")'> (<input type=text size=10 value=\"Short code\" name=munewcode onfocus='clearProg(this,\"Short code\")'>)</span>";
                echo("<td>".$form."&nbsp;</td>");
                if($head['required']=="R")
                {
                    echo("<td><i>(required)</i></td>");
                }
                else
                {
                    echo("<td>&nbsp;</td>");
                }
                break;
            case "natkpa":
                echo("<td><b>".$head['headfull'].":</b></td>");
                $form = "<select name=natkpa><option selected value=X>--- SELECT ---</option>";
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_kpa WHERE yn = 'Y' AND type='N' ORDER BY value";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $form.= "<option value=".$row['id'].">".$row['value']." (".$row['code'].")</option>";
                    }
                mysql_close();
                $form.= "</select>";
                echo("<td>".$form."&nbsp;</td>");
                if($head['required']=="R")
                {
                    echo("<td><i>(required)</i></td>");
                }
                else
                {
                    echo("<td>&nbsp;</td>");
                }
                break;
            case "progval":
                echo("<td><b>".$head['headfull'].":</b></td>");
                echo("<td><select name=progval onchange=changeProg(this)><option selected value=X>--- SELECT ---</option>");
                echo("<option value=ADD>Add New</option>");
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_prog WHERE yn = 'Y' AND deptid = ".$deptid." AND typeid = 3 ORDER BY value";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                    }
                mysql_close();
                echo("</select>");
                echo("<span id=progadd><br><input type=text size=50 name=prognew value=\"Enter ".$head['headfull']." here...\" onfocus='clearProg(this,\"Enter ".$head['headfull']." here...\")'></span>");
                echo("&nbsp;</td>");
                if($head['required']=="R")
                {
                    echo("<td><i>(required)</i></td>");
                }
                else
                {
                    echo("<td>&nbsp;</td>");
                }
                break;
            case "kpitype":
                echo("<td><b>".$head['headfull'].":</b></td>");
                echo("<td><select name=kpitype><option selected value=X>--- SELECT ---</option>");
                $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_type WHERE yn = 'Y' AND id <> 'X'";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                    }
                mysql_close();
                echo("</select>&nbsp;</td>");
                if($head['required']=="R")
                {
                    echo("<td><i>(required)</i></td>");
                }
                else
                {
                    echo("<td>&nbsp;</td>");
                }
                break;
            default:
                echo("<td><b>".$head['headfull'].":</b></td>");
                echo("<td><input type=text name=".$head['field']." maxlength=".$head['maxlen']);
                if($head['maxlen']>50)
                {
                    echo(" size=50");
                }
                else
                {
                    if($head['maxlen']>20)
                    {
                        echo(" size=25");
                    }
                    else
                    {
                        echo(" size=15");
                    }
                }
                echo(">&nbsp;</td>");
                if($head['required']=="R")
                {
                    echo("<td><i>(required)</i></td>");
                }
                else
                {
                    echo("<td>&nbsp;</td>");
                }
                break;
        }
        echo("</tr>");
    }
}

?>
</table>
<?php
if($kpirow['cpid'] != 0)
{
?>
    <table border="1" cellspacing="0" cellpadding="3" width=600>
        <tr class=tdgeneral>
            <td width=150><b>Project Category:</b></td>
            <td><?php echo($caterow['value']); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>PMS Ref:</b></td>
            <td><?php echo($cprow['pmsref']); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Project Description:</b></td>
            <td><?php echo($cprow['project']); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Funding Source:</b></td>
            <td><?php echo($cprow['fundsource']); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Budget:</b></td>
            <td>&nbsp;R <?php echo(number_format($cprow['budget'])); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Total Spend:</b></td>
            <td>&nbsp;R <?php echo(number_format($cprow['totalspend'])); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Balance:</b></td>
            <td>&nbsp;R <?php echo(number_format($cpbal)); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>Start Date:</b></td>
            <td><?php echo(date("d F Y",$cprow['startdate'])); ?>&nbsp;</td>
        </tr>
        <tr class=tdgeneral>
            <td><b>End Date:</b></td>
            <td><?php echo(date("d F Y",$cprow['enddate'])); ?>&nbsp;</td>
        </tr>
    </table>
<?php
}       //END IF CAPITAL PROJECT
?>
<p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
<table border=1 cellpadding=3 cellspacing=0 width=600>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_time t WHERE t.yn = 'Y' ORDER BY t.sort";
include("inc_db_con.php");
    $s = 1;
    while($row = mysql_fetch_array($rs))
    {
?>
    <tr class=tdgeneral valign=top>
        <td width=100 <?php echo($style[$s]); ?>><b><?php echo(date("d-M-Y",$row['value'])); ?></b></td>
        <td width=75 <?php echo($style[$s]); ?>><b>Target:</b></td>
        <td><input type=text size=10 name=krt[] value=0>% <i>(Numbers only)</i>&nbsp;</td>
    </tr>
    <?php
        $s++;
        if($s > 4){ $s=1; }
    }
mysql_close();
    ?>
    <tr class=tdgeneral valign=top>
        <td colspan=3><i>Note: At least one target is required</i></td>
    </tr>
</table>
<p><input type=submit value=Submit> <input type=reset></p>
</form>
<script language=JavaScript>
var targetid = "munadd";
var target = document.getElementById( targetid )
target.style.display = "none";
var targetid = "progadd";
var target = document.getElementById( targetid )
target.style.display = "none";
</script>
<?php
//}
?>
</body>

</html>
