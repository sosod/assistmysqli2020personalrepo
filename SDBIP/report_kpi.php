<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist 2008/9: Report</b></h1>
<form name=kpireport method=post action=report_kpi_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the headings you want in your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table1" cellpadding="3" cellspacing="0">
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="dept"></td>
		<td class="tdgeneral">Department</td>
	</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_headings WHERE fullyn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['field'] == "progval")
            $progval = $row['headfull'];
?>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="<?php echo($row['field']); ?>"></td>
		<td class="tdgeneral"><?php echo($row['headfull']); ?></td>
	</tr>
<?php
    }
mysql_close();
?>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="time"></td>
		<td class="tdgeneral">Targets and Actual Results</td>
	</tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3">
	<tr>
		<td class="tdgeneral">Department:</td>
		<td class="tdgeneral"><select size="1" name="dpt" onchange=changeDept(this)>
			<option selected value=ALL>All departments</option>
			<?php
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
                $dt = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $deptid[$dt] = $row['id'];
                    $dt++;
                    $id = $row['id'];
                    $value = str_replace("&#39","'",$row['value']);
                    echo("<option value=\"".$id."\">".$value."</option>");
                }
            mysql_close();
            ?>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">National KPA:</td>
		<td class="tdgeneral"><select size="1" name="ntk">
			<option selected value=ALL>All KPAs</option>
			<?php
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_kpa WHERE yn = 'Y' AND type = 'N' ORDER BY value";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $value = str_replace("&#39","'",$row['value'])." (".$row['code'].")";
                    echo("<option value=\"".$id."\">".$value."</option>");
                }
            mysql_close();
            ?>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">Municipal KPA:</td>
		<td class="tdgeneral"><select size="1" name="mnk">
			<option selected value=ALL>All KPAs</option>
			<?php
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_kpa WHERE yn = 'Y' AND type = 'M' ORDER BY value";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $value = str_replace("&#39","'",$row['value'])." (".$row['code'].")";
                    echo("<option value=\"".$id."\">".$value."</option>");
                }
            mysql_close();
            ?>
		</select></td>
	</tr>
	<script language=JavaScript>
    var kpiprog = new Array();
    kpiprog[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, p.id, p.value FROM";
            $sql.= " assist_".$cmpcode."_sdbip_prog p";
            $sql.= " WHERE p.yn = 'Y'";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " ORDER BY value";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."kpiprog[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                $v = str_replace("&#39","'",$row['value']);
                $v = str_replace("\"","\\\"",$v);
                $value = $row['id']."_|_".$v;
                $echo.= $value;
                while($row = mysql_fetch_array($rs))
                {
                    $v = str_replace("&#39","'",$row['value']);
                    $v = str_replace("\"","\\\"",$v);
                    $value = $row['id']."_|_".$v;
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo.";");
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral"><?php echo($progval); ?>:</td>
		<td class="tdgeneral"><select size="1" name="prg">
			<option selected value=ALL>All programmes</option>
			<option value=ALL>Please specify the department</option>
		</select></td>
	</tr>
	<script language=JavaScript>
    var kpidriver = new Array();
    kpidriver[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, k.kpidriver FROM";
            $sql.= " assist_".$cmpcode."_sdbip_kpi k,";
            $sql.= " assist_".$cmpcode."_sdbip_prog p";
            $sql.= " WHERE k.kpiprogid = p.id";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " GROUP BY k.kpidriver";
            $sql.= " ORDER BY deptid, kpidriver";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."kpidriver[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                        if(strlen($row['kpidriver'])>0)
                        {
                            $value = $row['kpidriver']."_|_".str_replace("&#39","'",$row['kpidriver']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= $value;

                while($row = mysql_fetch_array($rs))
                {
                        if(strlen($row['kpidriver'])>0)
                        {
                            $value = $row['kpidriver']."_|_".str_replace("&#39","'",$row['kpidriver']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo);
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral">Program Driver:</td>
		<td class="tdgeneral"><select size="1" name="drv">
			<option selected value=ALL>All program drivers</option>
			<option value=ALL>Please specify the department</option>
		</select></td>
	</tr>
	<script language=JavaScript>
    var kpiward = new Array();
    kpiward[0] = "";
        <?php
        foreach($deptid as $did)
        {
            $sql = "SELECT p.deptid, k.kpiward FROM";
            $sql.= " assist_".$cmpcode."_sdbip_kpi k,";
            $sql.= " assist_".$cmpcode."_sdbip_prog p";
            $sql.= " WHERE k.kpiprogid = p.id";
            $sql.= " AND p.deptid = ".$did;
            $sql.= " GROUP BY k.kpiward";
            $sql.= " ORDER BY deptid, kpiward";
            include("inc_db_con.php");
                $echo = "";
                $echo = chr(10)."kpiward[".$did."]=\"";
                $row = mysql_fetch_array($rs);
                        if(strlen($row['kpiward'])>0)
                        {
                            $value = $row['kpiward']."_|_".str_replace("&#39","'",$row['kpiward']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= $value;

                while($row = mysql_fetch_array($rs))
                {
                        if(strlen($row['kpiward'])>0)
                        {
                            $value = $row['kpiward']."_|_".str_replace("&#39","'",$row['kpiward']);
                        }
                        else
                        {
                            $value = "blank_|_Blank / Unspecified";
                        }
                    $echo.= "++|++".$value;
                }
                $echo.= "\"";
            mysql_close();
            echo($echo);
        }
        ?>
    </script>
	<tr>
		<td class="tdgeneral">Wards:</td>
		<td class="tdgeneral"><select size="1" name="wrd">
			<option selected value=X>Any ward</option>
			<option value=X>Please specify the department</option>
		</select></td>
	</tr>
</table>
</div>
<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center>Sort by:</td>
		<td class="tdgeneral"><select name=rpsort><option selected value=0>Department</option>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_sdbip_headings WHERE fullyn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        echo("<option value=".$row['id'].">".$row['headshort']."</option>");
    }
mysql_close();
?>
        </select></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="N" checked id=csvn></td>
		<td class="tdgeneral"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="Y" id=csvy></td>
		<td class=tdgeneral><label for=csvy>Save to file (Microsoft Excel file)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>5. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>

<script language=JavaScript>
function changeDept(me) {
    var deptid = me.value;
    if(deptid!="ALL")
    {
        var i2;
        var optval;
        //PROG
        var prg = kpiprog[deptid].split("++|++");
        document.kpireport.prg.length=1;
        i2 = prg.length+1;
        var p=0;
        for (i=1; i<i2; i++)
        {
            p = i-1;
            optval = prg[p].split("_|_");
            document.kpireport.prg.options[i] = new Option(optval[1],optval[0]);
        }
        //DRIVER
        var drv = kpidriver[deptid].split("++|++");
        document.kpireport.drv.length=1;
        i2 = drv.length+1;
        var d=0;
        for (i=1; i<i2; i++)
        {
            d = i-1;
            optval = drv[d].split("_|_");
            document.kpireport.drv.options[i] = new Option(optval[1],optval[0]);
        }
        //WARDS
        var wrd = kpiward[deptid].split("++|++");
        document.kpireport.wrd.length=1;
        i2 = wrd.length+1;
        var w=0;
        for (i=1; i<i2; i++)
        {
            w = i-1;
            optval = wrd[w].split("_|_");
            document.kpireport.wrd.options[i] = new Option(optval[1],optval[0]);
        }
    }
    else
    {
        document.kpireport.prg.length=1;
        document.kpireport.drv.length=1;
        document.kpireport.wrd.length=1;
        document.kpireport.prg.options[1] = new Option('Please specify the department','ALL');
        document.kpireport.drv.options[1] = new Option('Please specify the department','ALL');
        document.kpireport.wrd.options[1] = new Option('Please specify the department','X');
    }
}
</script>
</body>

</html>
