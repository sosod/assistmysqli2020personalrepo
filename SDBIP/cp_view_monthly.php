<?php
    include("inc_ignite.php");
    $display = "";
$display = $_GET['d'];
if(strlen($display)==0)
{
    $display = "mon";
}

$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();

$cols=12;

if($display=="mon")
{
    $cols = 2+(12*3);
}
else
{
        $cols = 2+(12*3)+9;
}
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009</b></h1>
<p style="margin-top: -12px; margin-bottom: 0px"><label id=label1 for=lbl>Loading, please wait...</label><input type=hidden name=lbl></p>
<span id=show><form name=disp method=get action=cp_view_monthly.php style="margin-top: -10px">
<p><b>Display: <select name=d>
<option <?php if($display=="mon") { echo("selected"); } ?> value=mon>Summary - All months</option>
<option <?php if($display=="all") { echo("selected"); } ?> value=all>All columns - All months</option>
<option <?php if($display=="montd") { echo("selected"); } ?> value=montd>Summary - Year to date</option>
<option <?php if($display=="alltd") { echo("selected"); } ?> value=alltd>All columns - Year to date</option>
</select></b> <input type=submit value="Go" class=button style="padding: 2px 3px 2px 4px; border: solid 1px;"></p>
</form></span>
<script language=JavaScript>
    document.getElementById('show').style.display = "none";
</script>
<?php
$dbtot = 0;
$datot = 0;
$munbtot = 0;
$munatot = 0;
//CHECK WHETHER THERE ARE ANY CAPITAL PROJECTS TO DISPLAY
$cpdept = 0;
$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
mysql_close();

if($cp > 0)     //IF THERE ARE CPs TO DISPLAY
{
    //GET LIST OF DEPARTMENTS
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $d = 0;
        while($row = mysql_fetch_array($rs))
        {
            $dept[$d] = $row;
            $d++;
        }
    mysql_close();
?>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
<?php $tdhead = "	<tr class=tdheader>";
$tdhead.= "			<td>No.</td>";
		if($display == "all" || $display=="alltd")
        {
$sql2 = "SELECT headshort FROM assist_".$cmpcode."_sdbip_capital_headings WHERE field = 'pms'";
include("inc_db_con2.php");
    $row = mysql_fetch_array($rs2);
mysql_close($con2);
if(strlen($row['headshort'])>0)
{
    $tdhead.= "		<td>".$row['headshort']."</td>";
}
else
{
    $tdhead.= "		<td>PMS<br>Ref.</td>";
}

} //display
$tdhead.= "			<td>Project</td>";
        if($display == "all" || $display == "alltd")
        {
$tdhead.= "		<td>Wards</td>";
$tdhead.= "		<td>Funding<br>Source</td>";
$tdhead.= "		<td>Budget</td>";
$tdhead.= "		<td>Progress</td>";
$tdhead.= "		<td>Total<br>Spend</td>";
$tdhead.= "		<td>Balance</td>";
$tdhead.= "		<td>Start<br>Date</td>";
$tdhead.= "		<td>End<br>Date</td>";
        } //if display=all
$tdhead.= "		<td>Cashflow</td>";
                $mt = date("n");
                $mf = date("n",$fdate);
                $yf = date("Y",$fdate);
                $m2 = 0;
                if($display == "mon" || $display == "all")
                {
                    $mx = 12;
                }
                else
                {
                    $diff = $today - $fdate;
                    $mx = date("n",$diff);
                }
                for($m=0;$m<$mx;$m++)
                {
                    $m2++;
                    $mc = $mf + $m;
                    if($mc > 12)
                    {
                        $mc = $mc - 12;
                        $yc = $yf + 1;
                    }
                    else
                    {
                        $yc = $yf;
                    }
                    $tdhead.= "<td>".date("M-Y",mktime(12,0,0,$mc,1,$yc))."</td>";
                }
$tdhead.= "	</tr>";
echo($tdhead);
	$d=0;
    foreach($dept as $deptrow)
    {
        $c=0;
        if($d>0)
        {
            if($cpdept>0)
            {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Total:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Total:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mbtot[$m]==0) { echo("-"); } else { echo(number_format($mbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($matot[$m]==0) { echo("-"); } else { echo(number_format($matot[$m],0)); } ?></i></b></td>
<?php
        }
?>
	</tr>
	<?php
            }
        }
    if($d>0)
    {
    ?>
	<tr class=tdgeneral bgcolor=#cde7ff>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Total for <u><?php echo($deptval); ?></u>:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($dbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($datot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($dbtot-$datot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Total for <u><?php echo($deptval); ?></u>:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mdbtot[$m]==0) { echo("-"); } else { echo(number_format($mdbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#cde7ff>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($mdatot[$m]==0) { echo("-"); } else { echo(number_format($mdatot[$m],0)); } ?></i></b></td>
<?php
        }
    }
        $d++;

?>
	</tr>
	<tr class=tdheaderbluel>
		<td colspan=<?php echo($cols); ?>><?php echo($deptrow['value']);?></td>
	</tr>
    <?php
        $dbtot = 0;
        $datot = 0;
        for($m=1;$m<$mx2;$m++)
        {
            $mdbtot[$m] = 0;
            $mdatot[$m] = 0;
        }
        $sql = "SELECT c.id as cid, p.id as pid, c.*, p.* FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
        $sql.= "assist_".$cmpcode."_sdbip_capital_category c ";
        $sql.= "WHERE p.yn = 'Y' ";
        $sql.= "AND c.yn = 'Y' ";
        $sql.= "AND p.cateid = c.id ";
        $sql.= "AND c.deptid = ".$deptrow['id']." ORDER BY c.id";
        include("inc_db_con.php");
            $cateid2 = 0;
            $cbtot = 0;
            $catot = 0;
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            $mbtot[$m] = 0;
            $matot[$m] = 0;
        }

            $cpdept = mysql_num_rows($rs);  //CHECK WHETHER THERE ARE CPs FOR THIS DEPT
            if($cpdept > 0)     //IF THERE ARE CPs FOR THIS DEPT
            {
                while($row = mysql_fetch_array($rs))
                {
                    $cateid1 = $row['cid'];
                    if($cateid1 != $cateid2)
                    {
                        if($c>0)
                        {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Total:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Total:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mbtot[$m]==0) { echo("-"); } else { echo(number_format($mbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($matot[$m]==0) { echo("-"); } else { echo(number_format($matot[$m],0)); } ?></i></b></td>
<?php
        }
?>
	</tr>
    <?php
                        }
                        $c++;
    ?>
	<tr class=tdheadergreenl>
		<td colspan=<?php echo($cols); ?>><?php echo($row['value']); ?></td>
	</tr>
                <?php
                    $cval = $row['value'];
                        $cbtot = 0;
                        $catot = 0;
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            $mbtot[$m] = 0;
            $matot[$m] = 0;
        }

                        $cateid2 = $cateid1;
                    }
                    if($moncash!="Y")
                    {
                        $budg = $row['budget'];
                        $tots = $row['totalspend'];
                    }
                    else
                    {
                        $budg = 0;
                        $tots = 0;
                        for($c=1;$c<13;$c++)
                        {
                            $fldb = "m".$c."budget";
                            $flda = "m".$c."actual";
                            $budg = $budg + $row[$fldb];
                            $tots = $tots + $row[$flda];
                        }
                    }
                    $cbtot = $cbtot + $budg;
                    $catot = $catot + $tots;
                    $dbtot = $dbtot + $budg;
                    $datot = $datot + $tots;
                    $munbtot = $munbtot + $budg;
                    $munatot = $munatot + $tots;
                    $rem = $budg - $tots;
                    $rem = number_format($rem,0);
                ?>
	<tr class=tdgeneral valign=top>
		<td align=center rowspan=2><?php echo($row['capnum']); ?>&nbsp;</td>
		<?php if($display=="all" || $display=="alltd") { ?><td align=center rowspan=2><?php echo($row['pmsref']); ?>&nbsp;</td><?php } ?>
		<?php
        $sql2 = "SELECT i.kpiid";
        $sql2.= " FROM assist_".$cmpcode."_sdbip_kpi i";
        $sql2.= " WHERE i.kpicapprojid = ".$row['pid'];
        include("inc_db_con2.php");
            $rc = mysql_num_rows($rs2);
            if($rc > 0)
            {
                $row2 = mysql_fetch_array($rs2);
                $url = "<a href=view.php?i=".$row2['kpiid'].">";
            }
            else
            {
                $url = "";
            }
        mysql_close($rs2);
        $row2 = "";
        ?>
		<td rowspan=2><?php echo($url.$row['project']); ?></a>&nbsp;</td>
<?php if($display=="all" || $display=="alltd") { ?>
        <td rowspan=2 align=center><?php echo($row['wards']); ?>&nbsp;</td>
		<td rowspan=2 align=center><?php echo($row['fundsource']); ?>&nbsp;</td>
		<td rowspan=2 align=right><?php echo(number_format($budg,0)); ?></span></td>
		<?php
        $sql2 = "SELECT r.krcomment";
        $sql2.= " FROM assist_".$cmpcode."_sdbip_kpi i,";
        $sql2.= " assist_".$cmpcode."_sdbip_kpi_result r,";
        $sql2.= " assist_".$cmpcode."_sdbip_list_time t";
        $sql2.= " WHERE i.kpiid = r.krkpiid";
        $sql2.= " AND r.krtimeid = t.id";
        $sql2.= " AND i.kpicapprojid = ".$row['pid'];
        $sql2.= " AND r.krcomment <> ''";
        $sql2.= " ORDER BY t.start DESC";
        include("inc_db_con2.php");
            $cc = mysql_num_rows($rs2);
            if($cc > 0)
            {
                $row2 = mysql_fetch_array($rs2);
                echo("<td rowspan=2>".str_replace(chr(10),"<br>",$row2['krcomment'])."&nbsp;</td>");
            }
            else
            {
                echo("<td rowspan=2>&nbsp;</td>");
            }
        mysql_close($rs2);
        ?>
		<td rowspan=2 align=right><?php echo(number_format($tots,0)); ?></span></td>
		<td rowspan=2 align=right><?php if($rem < 0){ echo("<font color=red><b>"); } echo("R ".$rem); ?></b></font></span></td>
		<td rowspan=2 align=center><?php echo(date("d M Y",$row['startdate'])); ?></td>
		<td rowspan=2 align=center><?php echo(date("d M Y",$row['enddate'])); ?></td>
<?php } ?>
		<td align=right bgcolor=E7F8E8><u>Budget:</u></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            $b = "m".$m."budget";
            $b = $row[$b];
            $mbtot[$m] = $mbtot[$m]+$b;
            $mdbtot[$m] = $mdbtot[$m]+$b;
            $mmunbtot[$m] = $mmunbtot[$m]+$b;
            ?>
            <td align=right bgcolor=E7F8E8><?php if($b==0) { echo("-"); } else { echo(number_format($b,0)); } ?></td>
<?php
        }
?>
	</tr>
	<tr class=tdgeneral valign=top>
		<td align=right bgcolor=FaFaFA><i><u>Actual:</u></i></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            $a = "m".$m."actual";
            $a = $row[$a];
            $matot[$m] = $matot[$m]+$a;
            $mdatot[$m] = $mdatot[$m]+$a;
            $mmunatot[$m] = $mmunatot[$m]+$a;
            ?>
            <td align=right bgcolor=FAFAFA><i><?php if($a==0) { echo("-"); } else { echo(number_format($a,0)); } ?></i></td>
<?php
        }
?>
    </tr>
	<?php
                }       //WHILE ROW FETCH
                $cperr = "N";
            }       //IF NO CPs FOR THIS DEPT
            else
            {
                $cperr = "Y";
            ?>
            	<tr class=tdgeneral>
            		<td colspan=<?php echo($cols); ?>>This department currently has no capital projects.</td>
            	</tr>
            <?php
            }
        mysql_close();
        $deptval = $deptrow['value'];
    }       //FOREACH DEPT
    if($cperr == "N")
    {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Total:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Total:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mbtot[$m]==0) { echo("-"); } else { echo(number_format($mbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($matot[$m]==0) { echo("-"); } else { echo(number_format($matot[$m],0)); } ?></i></b></td>
<?php
        }
?>
	</tr>
    <?php
    }
    ?>

	<tr class=tdgeneral bgcolor=#cde7ff>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Total for <u><?php echo($deptrow['value']); ?></u>:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($dbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($datot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($dbtot-$datot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Total for <u><?php echo($deptrow['value']); ?></u>:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mdbtot[$m]==0) { echo("-"); } else { echo(number_format($mdbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#cde7ff>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($mdatot[$m]==0) { echo("-"); } else { echo(number_format($mdatot[$m],0)); } ?></i></b></td>
<?php
        }
?>
	</tr>
<tr class=tdgeneral>
    <td colspan=<?php echo($cols); ?>>&nbsp;</td>
</tr>
	<tr class=tdgeneral bgcolor=#ffcdcd>
   	<?php if($display=="all" || $display=="alltd")
       {
       ?>
        <td colspan=5 align=right rowspan=2><b>Grand Total for <u><?php echo($cmpname); ?></u>:</b></td>
        <td align=right rowspan=2><b><?php echo(number_format($munbtot,0)); ?></b></td>
        <td align=right rowspan=2>&nbsp;</td>
        <td align=right rowspan=2><b><?php echo(number_format($munatot,0)); ?></b></td>
        <td align=right rowspan=2><b><?php echo(number_format(($munbtot-$munatot),0)); ?></b></td>
        <td align=right rowspan=2 colspan=2>&nbsp;</td>
        <?php
        }
        else
        {
       ?>
        <td colspan=2 align=right rowspan=2><b>Grand Total for <u><?php echo($cmpname); ?></u>:</b></td>
        <?php
        }
        ?>
        <td align=right ><b><u>Budget:</u></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><?php if($mmunbtot[$m]==0) { echo("-"); } else { echo(number_format($mmunbtot[$m],0)); } ?></b></td>
<?php
        }
?>
	</tr>
   	<tr class=tdgeneral bgcolor=#ffcdcd>
        <td align=right ><b><i><u>Actual:</u></i></b></td>
<?php
    $mx2 = $mx+1;
        for($m=1;$m<$mx2;$m++)
        {
            ?>
            <td align=right><b><i><?php if($mmunatot[$m]==0) { echo("-"); } else { echo(number_format($mmunatot[$m],0)); } ?></i></b></td>
<?php
        }
?>
	</tr>

</table>
<?php
}
else        //IF NO CAPITAL PROJECTS TO DISPLAY
{
    echo("<p>There are no capital projects to display.</p>");
}
?>

<script language=JavaScript>
label1.innerText="";
document.getElementById('show').style.display = "inline";
</script>

</body>

</html>
