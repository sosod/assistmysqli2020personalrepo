<?php
    include("inc_ignite.php");

$style['R'] = " style=\"background-color: #ccccff; color: #000000;\"";
$style['O'] = " style=\"background-color: #ceffde; color: #000000;\"";
$style['C'] = " style=\"background-color: #ffeaca; color: #000000;\"";

$field = $_POST['field'];
$dptfilter = $_POST['dptfilter'];
$datefilter = $_POST['datefilter'];
$amon = $_POST['amon'];
$zmon = $_POST['zmon'];

foreach($field as $f)
{
    $farray[$f] = "Y";
}

$csvfile = $_POST['csvfile'];

if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{

//GET DEPT TOTALS
$sql = "SELECT d.id, f.month, sum(f.rb) rb, sum(f.ob) ob, sum(f.cb) cb, sum(f.ra) ra, sum(f.oa) oa, sum(f.ca) ca ";
$sql.= "FROM assist_".$cmpcode."_sdbip_dept d, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_cashflow f ";
$sql.= "WHERE d.id = c.deptid ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND c.id = f.cateid";
if($dptfilter != "ALL")
{
    $sql.= " AND d.id = ".$dptfilter;
}
switch($datefilter)
{
    case "ALL":
        $mon1 = 1;
        $mon2 = 13;
        break;
    case "SOME":
        $sql.= " AND f.month >= ".$amon." AND f.month <= ".$zmon;
        $mon1 = $amon;
        $mon2 = $zmon + 1;
        break;
    case "YTD":
        $ytdb = $_POST['ytdb'];
        $mon1 = 1;
        $mon2 = $ytdb + 1;
        $sql.= " AND f.month <= ".$ytdb;
        break;
    default:
        $mon1 = 1;
        $mon2 = 13;
        break;
}
$sql.= " GROUP BY d.id, f.month ";
$sql.= "ORDER BY d.id, f.month";
include("inc_db_con.php");
//$deptot = array();
    while($row = mysql_fetch_array($rs))
    {
        $deptot[$row['id']][$row['month']]['rb'] = $row['rb'];
        $deptot[$row['id']][$row['month']]['ob'] = $row['ob'];
        $deptot[$row['id']][$row['month']]['cb'] = $row['cb'];
        $deptot[$row['id']][$row['month']]['ra'] = $row['ra'];
        $deptot[$row['id']][$row['month']]['oa'] = $row['oa'];
        $deptot[$row['id']][$row['month']]['ca'] = $row['ca'];
    }
mysql_close();

//GET FIRST DATE OF YEAR
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();


$fmon = date("n",$fdate);
$fyr = date("Y",$fdate);

//START OF DISPLAY CODE

    $fdata = "\"SDBIP Assist 2008/9: Financial Report\"";
	$fdata .= "\r\n";
$fdata.="\"\",\"\",\"\",";
$cols = 0;
$tcols = 0;
if($farray['rb']=="Y"){ $cols++; }
if($farray['ob']=="Y"){ $cols++; }
if($farray['cb']=="Y"){ $cols++; }
if($cols>0)
{
    $fdata .= "\"Budget\"";
    for($c=1;$c<$cols;$c++)
    {
        $fdata .= ",\"\"";
    }
    $tcols = 1;
}
$cols = 0;
if($farray['ra']=="Y"){ $cols++; }
if($farray['oa']=="Y"){ $cols++; }
if($farray['ca']=="Y"){ $cols++; }
if($cols>0)
{
    if($tcols == 1)
    {
        $fdata .= ",";
    }
    $fdata .= "\"Actual\"";
    for($c=1;$c<$cols;$c++)
    {
        $fdata .= ",\"\"";
    }
    $tcols=1;
}
$cols = 0;
if($farray['rv']=="Y"){ $cols++; }
if($farray['ov']=="Y"){ $cols++; }
if($farray['cv']=="Y"){ $cols++; }
if($cols>0)
{
    if($tcols == 1)
    {
        $fdata.=",";
    }
    $fdata .= "\"Variance\"";
    for($c=1;$c<$cols;$c++)
    {
        $fdata .= ",\"\"";
    }
}
$fdata.="\r\n";
$fdata.="\"Month\",\"Department\",\"Division\"";
$c = 0;
if($farray['rb']=="Y"){ $fdata.= ",\"Revenue\""; }
if($farray['ob']=="Y"){ $fdata.= ",\"Opex\"";  }
if($farray['cb']=="Y"){ $fdata.= ",\"Capex\"";  }
if($farray['ra']=="Y"){ $fdata.= ",\"Revenue\"";  }
if($farray['oa']=="Y"){ $fdata.= ",\"Opex\"";  }
if($farray['ca']=="Y"){ $fdata.= ",\"Capex\"";  }
if($farray['rv']=="Y"){ $fdata.= ",\"Revenue\""; }
if($farray['ov']=="Y"){ $fdata.= ",\"Opex\"";  }
if($farray['cv']=="Y"){ $fdata.= ",\"Capex\"";  }
//$heading = implode(",",$head);
//$fdata.= $heading;
$fdata.="\r\n";

for($mon=$mon1;$mon<$mon2;$mon++)
{
    $grandtot['rb'] = 0;
    $grandtot['ra'] = 0;
    $grandtot['ob'] = 0;
    $grandtot['oa'] = 0;
    $grandtot['cb'] = 0;
    $grandtot['ca'] = 0;
//    if($mon!=1)
//    	$fdata .= "\r\n";
    $dispmon = $fmon+$mon-1;
    if($dispmon > 12)
    {
        $dispyear = $fyr+1;
        $dispmon = $dispmon - 12;
    }
    else
    {
        $dispyear = $fyr;
    }
    $dispdate = getdate(mktime(0,0,1,$dispmon,1,$dispyear));

//$fdata .= "\"".date("F Y",$dispdate[0])."\"\r\n";



    $sql = "SELECT d.value dept, c.value cate, c.id cid, d.id d FROM ";
    $sql.= "assist_".$cmpcode."_sdbip_dept d, ";
    $sql.= "assist_".$cmpcode."_sdbip_finance_category c ";
    $sql.= "WHERE d.id = c.deptid ";
    $sql.= "AND d.yn = 'Y' ";
    $sql.= "AND c.yn = 'Y' ";
    if($dptfilter != "ALL")
    {
        $sql.= " AND d.id = ".$dptfilter;
    }
    $sql.= " ORDER BY d.sort, c.sort";
    include("inc_db_con.php");
        $dept1 = "";
        while($row = mysql_fetch_array($rs))
        {
            $dept2 = $row['dept'];
            $fdata .= "\"".date("F Y",$dispdate[0])."\",";
            $fdata .= "\"".$dept2."\",\"".$row['cate']."\"";
            $sql2 = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_cashflow WHERE cateid = ".$row['cid']." AND month = ".$mon;
            include("inc_db_con2.php");
                if(mysql_num_rows($rs2) > 0)
                {
                    $row2 = mysql_fetch_array($rs2);
                    $rb = $row2['rb'];
                    $ra = $row2['ra'];
                    $rv = $rb - $ra;
                    $ob = $row2['ob'];
                    $oa = $row2['oa'];
                    $ov = $ob - $oa;
                    $cb = $row2['cb'];
                    $ca = $row2['ca'];
                    $cv = $cb - $ca;

                    if($farray['rb']=="Y") { $fdata .= ",".$rb; }
                    if($farray['ob']=="Y") { $fdata .= ",".$ob; }
                    if($farray['cb']=="Y") { $fdata .= ",".$cb; }
                    if($farray['ra']=="Y") { $fdata .= ",".$ra; }
                    if($farray['oa']=="Y") { $fdata .= ",".$oa; }
                    if($farray['ca']=="Y") { $fdata .= ",".$ca; }
                    if($farray['rv']=="Y") { $fdata .= ",".$rv; }
                    if($farray['ov']=="Y") { $fdata .= ",".$ov; }
                    if($farray['cv']=="Y") { $fdata .= ",".$cv; }
                }
                else
                {
                    if($farray['rb']=="Y") { $fdata .= ",0"; }
                    if($farray['ob']=="Y") { $fdata .= ",0"; }
                    if($farray['cb']=="Y") { $fdata .= ",0"; }
                    if($farray['ra']=="Y") { $fdata .= ",0"; }
                    if($farray['oa']=="Y") { $fdata .= ",0"; }
                    if($farray['ca']=="Y") { $fdata .= ",0"; }
                    if($farray['rv']=="Y") { $fdata .= ",0"; }
                    if($farray['ov']=="Y") { $fdata .= ",0"; }
                    if($farray['cv']=="Y") { $fdata .= ",0"; }
                }
            $fdata .= "\r\n";
            mysql_close($con2);
        }
    mysql_close();

/*    $grandtot['rv'] = $grandtot['rb'] - $grandtot['ra'];
    $grandtot['ov'] = $grandtot['ob'] - $grandtot['oa'];
    $grandtot['cv'] = $grandtot['cb'] - $grandtot['ca'];


$display .= "<tr class=tdheaderl>";
$display .= "<td>Total</td>";

    if($farray['rb']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['rb']>0){$display .= number_format($grandtot['rb']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['ob']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ob']>0){$display .= number_format($grandtot['ob']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['cb']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['cb']>0){$display .= number_format($grandtot['cb']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['ra']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ra']>0){$display .= number_format($grandtot['ra']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['oa']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['oa']>0){$display .= number_format($grandtot['oa']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['ca']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ca']>0){$display .= number_format($grandtot['ca']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['rv']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['rv']>0){$display .= number_format($grandtot['rv']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['ov']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ov']>0){$display .= number_format($grandtot['ov']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

    if($farray['cv']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['cv']>0){$display .= number_format($grandtot['cv']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }

$display .= "</tr>";

$display .= "</table>";
*/
}

//$display .= "</body></html>";

//echo($display);

    //WRITE DATA TO FILE
    $filename = "../files/".$cmpcode."/sdbip_fin_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "sdbip_fin_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
    //echo($fdata);
}
else    //ELSE IF CSVFILE
{
//GET DEPT TOTALS
$sql = "SELECT d.id, f.month, sum(f.rb) rb, sum(f.ob) ob, sum(f.cb) cb, sum(f.ra) ra, sum(f.oa) oa, sum(f.ca) ca ";
$sql.= "FROM assist_".$cmpcode."_sdbip_dept d, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_cashflow f ";
$sql.= "WHERE d.id = c.deptid ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND c.id = f.cateid";
if($dptfilter != "ALL")
{
    $sql.= " AND d.id = ".$dptfilter;
}
switch($datefilter)
{
    case "ALL":
        $mon1 = 1;
        $mon2 = 13;
        break;
    case "SOME":
        $sql.= " AND f.month >= ".$amon." AND f.month <= ".$zmon;
        $mon1 = $amon;
        $mon2 = $zmon + 1;
        break;
    case "YTD":
        $ytdb = $_POST['ytdb'];
        $mon1 = 1;
        $mon2 = $ytdb + 1;
        $sql.= " AND f.month <= ".$ytdb;
        break;
    default:
        $mon1 = 1;
        $mon2 = 13;
        break;
}
$sql.= " GROUP BY d.id, f.month ";
$sql.= "ORDER BY d.id, f.month";
include("inc_db_con.php");
//$deptot = array();
    while($row = mysql_fetch_array($rs))
    {
        $deptot[$row['id']][$row['month']]['rb'] = $row['rb'];
        $deptot[$row['id']][$row['month']]['ob'] = $row['ob'];
        $deptot[$row['id']][$row['month']]['cb'] = $row['cb'];
        $deptot[$row['id']][$row['month']]['ra'] = $row['ra'];
        $deptot[$row['id']][$row['month']]['oa'] = $row['oa'];
        $deptot[$row['id']][$row['month']]['ca'] = $row['ca'];
    }
mysql_close();

//GET FIRST DATE OF YEAR
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();


$fmon = date("n",$fdate);
$fyr = date("Y",$fdate);

//START OF DISPLAY CODE

$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>SDBIP Assist 2008/9: Financial Report</b></h1>";

for($mon=$mon1;$mon<$mon2;$mon++)
{
    $grandtot['rb'] = 0;
    $grandtot['ra'] = 0;
    $grandtot['ob'] = 0;
    $grandtot['oa'] = 0;
    $grandtot['cb'] = 0;
    $grandtot['ca'] = 0;
    if($mon!=1)
        $display .= "<p style=\"page-break-after: always; margin: 10 10 10 10; line-height: 0px\">&nbsp;</p>";
    $dispmon = $fmon+$mon-1;
    if($dispmon > 12)
    {
        $dispyear = $fyr+1;
        $dispmon = $dispmon - 12;
    }
    else
    {
        $dispyear = $fyr;
    }
    $dispdate = getdate(mktime(0,0,1,$dispmon,1,$dispyear));

$display .= "<table border=1 cellpadding=3 cellspacing=0>";
$display .= "    <tr class=tdgeneral>";
$display .= "        <td rowspan=3 align=center>&nbsp;</td>";
$display .= "        <td colspan=9 align=center><h3 class=fc style=\"margin-top: 10px; margin-bottom -10px; line-height: 10px\">".date("F Y",$dispdate[0])."</h3></td>";
$display .= "    </tr>";
$display .= "    <tr class=tdheader>";
$cols = 0;
$tcols = 0;
if($farray['rb']=="Y"){ $cols++; }
if($farray['ob']=="Y"){ $cols++; }
if($farray['cb']=="Y"){ $cols++; }
if($cols>0){ $display .= "<td colspan=".$cols." width=33%>Budget</td>"; }
$tcols = $cols;
$cols = 0;
if($farray['ra']=="Y"){ $cols++; }
if($farray['oa']=="Y"){ $cols++; }
if($farray['ca']=="Y"){ $cols++; }
if($cols>0){ $display .= "<td colspan=".$cols." width=33%>Actual</td>"; }
$tcols = $tcols+$cols;
$cols = 0;
if($farray['rv']=="Y"){ $cols++; }
if($farray['ov']=="Y"){ $cols++; }
if($farray['cv']=="Y"){ $cols++; }
if($cols>0){ $display .= "<td colspan=".$cols." width=34%>Variance</td>"; }
$tcols = $tcols+$cols;
$wdt = 100/$tcols;
$display .= "</tr>";
$display .= "    <tr class=tdgeneral align=center style=\"font-weight: bold; text-decoration: underline\">";
 if($farray['rb']=="Y"){ $display .= "<td width=".$wdt."% ".$style['R']."><b>Revenue</b></td>"; } 
 if($farray['ob']=="Y"){ $display .= "<td width=".$wdt."% ".$style['O']."><b>Opex</b></td>"; } 
 if($farray['cb']=="Y"){ $display .= "<td width=".$wdt."% ".$style['C']."><b>Capex</b></td>"; } 
 if($farray['ra']=="Y"){ $display .= "<td width=".$wdt."% ".$style['R']."><b>Revenue</b></td>"; } 
 if($farray['oa']=="Y"){ $display .= "<td width=".$wdt."% ".$style['O']."><b>Opex</b></td>"; } 
 if($farray['ca']=="Y"){ $display .= "<td width=".$wdt."% ".$style['C']."><b>Capex</b></td>"; } 
 if($farray['rv']=="Y"){ $display .= "<td width=".$wdt."% ".$style['R']."><b>Revenue</b></td>"; } 
 if($farray['ov']=="Y"){ $display .= "<td width=".$wdt."% ".$style['O']."><b>Opex</b></td>"; } 
 if($farray['cv']=="Y"){ $display .= "<td width=".$wdt."% ".$style['C']."><b>Capex</b></td>"; } 

$display .= "    </tr>";

    $sql = "SELECT d.value dept, c.value cate, c.id cid, d.id d FROM ";
    $sql.= "assist_".$cmpcode."_sdbip_dept d, ";
    $sql.= "assist_".$cmpcode."_sdbip_finance_category c ";
    $sql.= "WHERE d.id = c.deptid ";
    $sql.= "AND d.yn = 'Y' ";
    $sql.= "AND c.yn = 'Y' ";
if($dptfilter != "ALL")
{
    $sql.= " AND d.id = ".$dptfilter;
}
    $sql.= " ORDER BY d.sort, c.sort";
    include("inc_db_con.php");
        $dept1 = "";
        while($row = mysql_fetch_array($rs))
        {
            $dept2 = $row['dept'];
            if($dept1 != $dept2)
            {
                $display .= "<tr class=tdgeneral><td><b>".$dept2."</b></td>";
                $rb = $deptot[$row['d']][$mon]['rb'];
                $ra = $deptot[$row['d']][$mon]['ra'];
                $rv = $ra - $rb;
                $ob = $deptot[$row['d']][$mon]['ob'];
                $oa = $deptot[$row['d']][$mon]['oa'];
                $ov = $ob - $oa;
                $cb = $deptot[$row['d']][$mon]['cb'];
                $ca = $deptot[$row['d']][$mon]['ca'];
                $cv = $cb - $ca;
                $grandtot['rb'] = $grandtot['rb'] + $rb;
                $grandtot['ra'] = $grandtot['ra'] + $ra;
                $grandtot['ob'] = $grandtot['ob'] + $ob;
                $grandtot['oa'] = $grandtot['oa'] + $oa;
                $grandtot['cb'] = $grandtot['cb'] + $cb;
                $grandtot['ca'] = $grandtot['ca'] + $ca;
                
 if($farray['rb']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;<b>";
        if($rb!=0){$display .= number_format($rb); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ob']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;<b>";
        if($ob!=0){$display .= number_format($ob); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['cb']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;<b>";
        if($cb!=0){$display .= number_format($cb); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ra']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;<b>";
        if($ra!=0){$display .= number_format($ra); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['oa']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;<b>";
        if($oa!=0){$display .= number_format($oa); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ca']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;<b>";
        if($ca!=0){$display .= number_format($ca); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['rv']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;<b>";
        if($rv!=0){if($rv<0){$display .= "<font color=red><b>".number_format($rv)."</b></font>";}else{$display .= number_format($rv);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ov']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;<b>";
        if($ov!=0){if($ov<0){$display .= "<font color=red><b>".number_format($ov)."</b></font>";}else{$display .= number_format($ov);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['cv']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;<b>";
        if($cv!=0){if($cv<0){$display .= "<font color=red><b>".number_format($cv)."</b></font>";}else{$display .= number_format($cv);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
                
                $display .= "</tr>";
                $dept1 = $dept2;
            } //if dept ids don't match

            $display .= "<tr class=tdgeneral align=right><td align=left>".$row['cate']."</td>";
            $sql2 = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_cashflow WHERE cateid = ".$row['cid']." AND month = ".$mon;
            include("inc_db_con2.php");
                if(mysql_num_rows($rs2) > 0)
                {
                    $row2 = mysql_fetch_array($rs2);
                    $rb = $row2['rb'];
                    $ra = $row2['ra'];
                    $rv = $ra - $rb;
                    $ob = $row2['ob'];
                    $oa = $row2['oa'];
                    $ov = $ob - $oa;
                    $cb = $row2['cb'];
                    $ca = $row2['ca'];
                    $cv = $cb - $ca;
                    
 if($farray['rb']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;";
        if($rb!=0){$display .= number_format($rb); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ob']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;";
        if($ob!=0){$display .= number_format($ob); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['cb']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;";
        if($cb!=0){$display .= number_format($cb); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ra']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;";
        if($ra!=0){$display .= number_format($ra); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['oa']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;";
        if($oa!=0){$display .= number_format($oa); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ca']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;";
        if($ca!=0){$display .= number_format($ca); }else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['rv']=="Y")
    {
        $display .= "<td align=right ".$style['R'].">&nbsp;";
        if($rv!=0){if($rv<0){$display .= "<font color=red><b>".number_format($rv)."</b></font>";}else{$display .= number_format($rv);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['ov']=="Y")
    {
        $display .= "<td align=right ".$style['O'].">&nbsp;";
        if($ov!=0){if($ov<0){$display .= "<font color=red><b>".number_format($ov)."</b></font>";}else{$display .= number_format($ov);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
 if($farray['cv']=="Y")
    {
        $display .= "<td align=right ".$style['C'].">&nbsp;";
        if($cv!=0){if($cv<0){$display .= "<font color=red><b>".number_format($cv)."</b></font>";}else{$display .= number_format($cv);}}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
                    
                }
                else
                {
                    
if($farray['rb']=="Y"){ $display .= "<td ".$style['R'].">&nbsp;-</td>"; }
if($farray['ob']=="Y"){ $display .= "<td ".$style['O'].">&nbsp;-</td>"; }
if($farray['cb']=="Y"){ $display .= "<td ".$style['C'].">&nbsp;-</td>"; }
if($farray['ra']=="Y"){ $display .= "<td ".$style['R'].">&nbsp;-</td>"; }
if($farray['oa']=="Y"){ $display .= "<td ".$style['O'].">&nbsp;-</td>"; }
if($farray['ca']=="Y"){ $display .= "<td ".$style['C'].">&nbsp;-</td>"; }
if($farray['rv']=="Y"){ $display .= "<td ".$style['R'].">&nbsp;-</td>"; }
if($farray['ov']=="Y"){ $display .= "<td ".$style['O'].">&nbsp;-</td>"; }
if($farray['cv']=="Y"){ $display .= "<td ".$style['C'].">&nbsp;-</td>"; }
                }
            $display .= "</tr>";
            mysql_close($con2);
        }
    mysql_close();

    $grandtot['rv'] = $grandtot['ra'] - $grandtot['rb'];
    $grandtot['ov'] = $grandtot['ob'] - $grandtot['oa'];
    $grandtot['cv'] = $grandtot['cb'] - $grandtot['ca'];


$display .= "<tr class=tdheaderl>";
$display .= "<td>Total</td>";

    if($farray['rb']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['rb']!=0){$display .= number_format($grandtot['rb']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['ob']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ob']!=0){$display .= number_format($grandtot['ob']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['cb']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['cb']!=0){$display .= number_format($grandtot['cb']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['ra']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ra']!=0){$display .= number_format($grandtot['ra']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['oa']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['oa']!=0){$display .= number_format($grandtot['oa']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['ca']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ca']!=0){$display .= number_format($grandtot['ca']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['rv']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['rv']!=0){$display .= number_format($grandtot['rv']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['ov']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['ov']!=0){$display .= number_format($grandtot['ov']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    }
    
    if($farray['cv']=="Y")
    {
        $display .= "<td align=right>&nbsp;<b>";
        if($grandtot['cv']!=0){$display .= number_format($grandtot['cv']);}else{$display .= "&nbsp;-";}
        $display .= "</b></td>";
    } 
    
$display .= "</tr>";

$display .= "</table>";

}

$display .= "</body></html>";

echo($display);
}   //END IF CSVFILE
?>
