<?php
    include("inc_ignite.php");
    
$style['R'] = " style=\"background-color: #ccccff; color: #000000;\"";
$style['O'] = " style=\"background-color: #ceffde; color: #000000;\"";
$style['C'] = " style=\"background-color: #ffeaca; color: #000000;\"";

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function hide() {
    var target = document.getElementById( 'load' );
    target.style.display = "none";
}
function Validate() {
	if(confirm("The update script can take some time to run.  Please be patient.")==true)
		return true;
	else
		return false;
}
lblper = 0;
lblln = "";
subname = "";
target = "";
s = 1;
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type="text/css">
.lab1
{
    font-weight: bold;
    color: 006600;
}
.lab2
{
    background-color: #cc0001;
    color: #cc0001;
}
.lab3
{
    background-color: #006600;
    color: #006600;
}
</style><base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Financial Information 2008/2009</b></h1>
<h2>Monthly Cashflow</h2>
<span id=load><center>
<table border=0 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border-style: solid; border-width: 0px" width=350>
    <tr>
        <td class=tdgeneral colspan=2 align=center><label id=label1 for=lbl>Loading, please wait...&nbsp;<input type=hidden name=lbl></td>
    </tr>
    <tr>
        <td class=tdgeneral align=right width=300><table cellpadding=0 cellspacing=0 width=252 style="border-collapse: collapse; border-style: solid; border-width: 1px;">
            <tr>
                <td class=tdgeneral style="line-height: 90%"><label id=label3 for=lbl class=lab2>|</label></td>
            </tr>
            </table>
        </td>
        <td class=tdgeneral width=50><label id=label2 for=lbl>0%</label>&nbsp;</td>
    </tr>
</table>
</center></span>
<form name=fincash action=update_fin_cash_dept_process.php method=post onsubmit="return Validate();" language=jscript>
<?php
//GET DEPT TOTALS
$sql = "SELECT d.id, f.month, sum(f.rb) rb, sum(f.ob) ob, sum(f.cb) cb, sum(f.ra) ra, sum(f.oa) oa, sum(f.ca) ca ";
$sql.= "FROM assist_".$cmpcode."_sdbip_dept d, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_cashflow f ";
$sql.= "WHERE d.id = c.deptid ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND c.id = f.cateid ";
$sql.= "GROUP BY d.id, f.month ";
$sql.= "ORDER BY d.id, f.month";
include("inc_db_con.php");
//$deptot = array();
    while($row = mysql_fetch_array($rs))
    {
        $deptot[$row['id']][$row['month']]['rb'] = $row['rb'];
        $deptot[$row['id']][$row['month']]['ob'] = $row['ob'];
        $deptot[$row['id']][$row['month']]['cb'] = $row['cb'];
        $deptot[$row['id']][$row['month']]['ra'] = $row['ra'];
        $deptot[$row['id']][$row['month']]['oa'] = $row['oa'];
        $deptot[$row['id']][$row['month']]['ca'] = $row['ca'];
    }
mysql_close();

//GET FIRST DATE OF YEAR
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();


$fmon = date("n",$fdate);
$fyr = date("Y",$fdate);
$tmon = date("n",$today);
$tyr = date("Y",$today);
$m = 0;
for($mon=1;$mon<13;$mon++)
{
    set_time_limit(30);
    $grandtot['rb'] = 0;
    $grandtot['ra'] = 0;
    $grandtot['ob'] = 0;
    $grandtot['oa'] = 0;
    $grandtot['cb'] = 0;
    $grandtot['ca'] = 0;
    if($mon!=1)
        echo("<p style=\"page-break-after: always; margin: 10 10 10 10; line-height: 0px\">&nbsp;</p>");
    $dispmon = $fmon+$mon-1;
    if($dispmon > 12)
    {
        $dispyear = $fyr+1;
        $dispmon = $dispmon - 12;
    }
    else
    {
        $dispyear = $fyr;
    }
    $dispdate = mktime(0,0,1,$dispmon,1,$dispyear);
    $todaydate = mktime(0,0,1,$tmon,1,$tyr);
    //$dispdate[0]=$fdate;
    if($dispdate > $todaydate)
    {
    }
    else
    {
        $m++;
?>
<table border=1 cellpadding=3 cellspacing=0>
    <tr class=tdgeneral>
        <td rowspan=3 align=center>&nbsp;</td>
        <td colspan=9 align=center><h3 class=fc style="margin-top: 10px; margin-bottom -10px; line-height: 10px"><?php echo(date("F Y",$dispdate)); ?></h3></td>
    </tr>
    <tr class=tdheader>
        <td colspan=3 width=210>Budget</td>
        <td colspan=3 width=210>Actual</td>
    </tr>
    <tr class=tdgeneral align=center style="font-weight: bold; text-decoration: underline">
        <td width=70<?php echo($style['R']); ?>><b>Revenue</b></td>
        <td width=70<?php echo($style['O']); ?>><b>Opex</b></td>
        <td width=70<?php echo($style['C']); ?>><b>Capex</b></td>
        <td width=70<?php echo($style['R']); ?>><b>Revenue</b></td>
        <td width=70<?php echo($style['O']); ?>><b>Opex</b></td>
        <td width=70<?php echo($style['C']); ?>><b>Capex</b></td>
    </tr>
<?php

    $sql = "SELECT d.value dept, c.value cate, c.id cid, d.id d FROM ";
    $sql.= "assist_".$cmpcode."_sdbip_dept d, ";
    $sql.= "assist_".$cmpcode."_sdbip_finance_category c ";
    $sql.= "WHERE d.id = c.deptid ";
    $sql.= "AND d.yn = 'Y' ";
    $sql.= "AND c.yn = 'Y' ";
    $sql.= "ORDER BY d.sort, c.sort";
    include("inc_db_con.php");
        $dept1 = "";
        while($row = mysql_fetch_array($rs))
        {
            $dept2 = $row['dept'];
            if($dept1 != $dept2)
            {
                echo("<tr class=tdgeneral><td><b>".$dept2."</b></td>");
                ?>
                <td align=right<?php echo($style['R']); ?>>&nbsp;</td>
                <td align=right<?php echo($style['O']); ?>>&nbsp;</td>
                <td align=right<?php echo($style['C']); ?>>&nbsp;</td>
                <td align=right<?php echo($style['R']); ?>>&nbsp;</td>
                <td align=right<?php echo($style['O']); ?>>&nbsp;</td>
                <td align=right<?php echo($style['C']); ?>>&nbsp;</td>
                <?php
                echo("</tr>");
                $dept1 = $dept2;
            }
            echo("<tr class=tdgeneral align=right><td align=left>".$row['cate']."<input type=hidden name=id".$mon."[] size=3 value=".$row['cid']."></td>");
            $sql2 = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_cashflow WHERE cateid = ".$row['cid']." AND month = ".$mon;
            include("inc_db_con2.php");
                if(mysql_num_rows($rs2) > 0)
                {
                    $row2 = mysql_fetch_array($rs2);
                    $rb = $row2['rb'];
                    $ra = $row2['ra'];
                    $ob = $row2['ob'];
                    $oa = $row2['oa'];
                    $cb = $row2['cb'];
                    $ca = $row2['ca'];
                    ?>
                    <td align=right<?php echo($style['R']); ?>>&nbsp;<?php if($rb>0){echo(number_format($rb));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['O']); ?>>&nbsp;<?php if($ob>0){echo(number_format($ob));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['C']); ?>>&nbsp;<?php if($cb>0){echo(number_format($cb));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['R']); ?>><?php echo("R <input type=text size=10 name=ra".$mon."[] value=".$ra." style=\"text-align: right;\">"); ?></b></td>
                    <td align=right<?php echo($style['O']); ?>><?php echo("R <input type=text size=10 name=oa".$mon."[] value=".$oa." style=\"text-align: right;\">"); ?></b></td>
                    <td align=right<?php echo($style['C']); ?>><?php echo("R <input type=text size=10 name=ca".$mon."[] value=".$ca." style=\"text-align: right;\">"); ?></b></td>
                    <?php
                }
                else
                {
                    ?>
                    <td<?php echo($style['R']); ?>>&nbsp;-</td>
                    <td<?php echo($style['O']); ?>>&nbsp;-</td>
                    <td<?php echo($style['C']); ?>>&nbsp;-</td>
                    <td<?php echo($style['R']); ?>>&nbsp;-</td>
                    <td<?php echo($style['O']); ?>>&nbsp;-</td>
                    <td<?php echo($style['C']); ?>>&nbsp;-</td>
                    <?php
                }
            echo("</tr>");
            mysql_close($rs2);
        }
    mysql_close();
$subname = "sub".$mon;
?>
    <tr class=tdgeneral>
        <td colspan=7><input type=hidden name=m value=<?php echo($m);?>><input type=submit name="<?php echo($subname); ?>" value=Submit*> <input type=reset value=Reset*> *Will update/reset all months</td>
    </tr>
</table>
<?php
    if($mon != 12)
    {
//        echo("<p style=\"page-break-before: always\">&nbsp;</p>");
    }
            ?>
<script language=JavaScript>
    subname = "sub"+s;
    target = document.getElementById( subname );
    if(target != null)
    {
        target.disabled = true;
    }
    s++;
lblper = lblper + 8;
label2.innerText=lblper+"%";
for(l=1;l<5;l++)
{
    label3.innerText=label3.innerText+"|";
}
</script>
            <?php
    }
}
?>
</form>

<script language=JavaScript>
label2.innerText="100%";
label3.innerText="||||||||||||||||||||||||||||||||||||||||||||||||||";
label1.innerText = "Loading complete.  Thank you for your patience.";
label1.className = 'lab1';
label3.className = 'lab3';
for(s=1;s<13;s++)
{
    subname = "sub"+s;
    target = document.getElementById( subname );
    if(target != null)
    {
        target.disabled = false;
    }
}
//var t = setTimeout("return hide();",5000);
</script>

</body>

</html>
