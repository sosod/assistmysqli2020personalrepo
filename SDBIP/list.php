<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>SDBIP</title>
</head>

<body>
				<p align="center"><b>
				<font face="Arial" size="5" color="#CC0000">Performance Management 2008/9<br>
				</font><font face="Arial" size="4" color="#CC0000">MUNICIPALITY</font><font face="Arial" size="5" color="#CC0000"><br>
				</font><font face="Arial" color="#CC0000">DEPARTMENT</font></b></p>

<table border="1" width="100%" id="table2" cellpadding=2 cellspacing=0>
	<tr>
		<td rowspan="2" align="center"><b>
		<font face="Arial" size="2">Strategy / 
		Programme</font></b></td>
		<td rowspan="2" align="center"><b><font face="Arial" size="2">
		C.P.<br>
		No.</font></b></td>
		<td rowspan="2" align="center"><b><font face="Arial" size="2">Key 
		Performance Indicator</font></b></td>
		<td rowspan="2" align="center"><b><font face="Arial" size="2">KPI: 
		Definition / Unit of Measurement</font></b></td>
		<td rowspan="2" align="center"><b><font face="Arial" size="2">Program 
		Driver</font></b></td>
		<td rowspan="2" align="center"><b><font face="Arial" size="2">Target 
		Unit</font></b></td>
		<td colspan="3" align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">30 Sep 
		2008</font></b></td>
		<td colspan="3" align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">31 Dec 
		2008</font></b></td>
		<td colspan="3" align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">31 Mar 
		2009</font></b></td>
		<td colspan="3" align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">30 June 
		2009</font></b></td>
	</tr>
	<tr>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#FFCCCC"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#FFEACA"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#CEFFDE"><b><font face="Arial" size="2">R</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">Target %</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">Actual %</font></b></td>
		<td align="center" bgcolor="#CCCCFF"><b><font face="Arial" size="2">R</font></b></td>
	</tr>
	<tr>
		<td valign=center align=center colspan="18" >
		<p align="left"><input type="button" value="Update" name="B3"></td>
	</tr>
	<tr>
		<td valign=center align=center rowspan="3" ><font face="Arial" size="2">
		Financial Performance</font></td>
		<td valign=center align=center >&nbsp;</td>
		<td valign=center align=left ><a href=main2.php><u>
		<font face="Arial" size="2" color="#CC0000">Revenue</font></u></a></td>
		<td valign=center align=left ><font face="Arial" size="2">Revenue 
		collected in line or exceeding budget</font></td>
		<td valign=center align=center ><font face="Arial" size="2">Municipal 
		manager</font></td>
		<td valign=center align=center ><font face="Arial" size="2">% of revenue 
		billed</font></td>
		<td valign=center align=center ><font face="Arial" size="2">47</font></td>
		<td align="center"><input type="text" name="T49" size="3" value="37"></td>
		<td align="center" bgcolor="#008000">
		<font face="Arial" size="1" color="#008000">G</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T61" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T51" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T52" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center >&nbsp;</td>
		<td valign=center align=left ><a href=main2.php><u>
		<font face="Arial" size="2" color="#CC0000">Capital 
		Expenditure</font></u></a></td>
		<td valign=center align=left ><font face="Arial" size="2">All capital 
		projects budgeted for implemented</font></td>
		<td valign=center align=center ><font face="Arial" size="2">Municipal 
		manager</font></td>
		<td valign=center align=center ><font face="Arial" size="2">% of budget 
		spent</font></td>
		<td valign=center align=center ><font face="Arial" size="2">27</font></td>
		<td align="center"><input type="text" name="T53" size="3" value="23"></td>
		<td align="center" bgcolor="#FF9900">
		<font face="Arial" size="1" color="#FF9900">O</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T62" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T55" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T56" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center >&nbsp;</td>
		<td valign=center align=left ><a href=main2.php><u>
		<font face="Arial" size="2" color="#CC0000">Operational 
		Performance</font></u></a></td>
		<td valign=center align=left ><font face="Arial" size="2">Expenditure 
		within budget</font></td>
		<td valign=center align=center ><font face="Arial" size="2">Municipal 
		manager</font></td>
		<td valign=center align=center ><font face="Arial" size="2">R's 
		collected and spent</font></td>
		<td valign=center align=center ><font face="Arial" size="2">39</font></td>
		<td align="center"><input type="text" name="T57" size="3" value="27"></td>
		<td align="center" bgcolor="#CC0000">
		<font face="Arial" size="1" color="#CC0000">R</font></td>
		<td align="center"><font face="Arial" size="2">50</font></td>
		<td align="center"><input type="text" name="T63" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">75</font></td>
		<td align="center"><input type="text" name="T59" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">100</font></td>
		<td align="center"><input type="text" name="T60" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center ><font face="Arial" size="2">Improve 
		offices</font></td>
		<td valign=center align=center ><font face="Arial" size="2">110</font></td>
		<td valign=center align=left ><a href=main2.php><u>
		<font face="Arial" size="2" color="#CC0000">Building 
		additional offices</font></u></a></td>
		<td valign=center align=left ><font face="Arial" size="2">Building 7 
		new offices</font></td>
		<td valign=center align=center ><font face="Arial" size="2">Strategy 
		manager</font></td>
		<td valign=center align=center ><font face="Arial" size="2"># of offices 
		built</font></td>
		<td valign=center align=center ><font face="Arial" size="2">0</font></td>
		<td align="center"><input type="text" name="T2" size="3" value="0"></td>
		<td align="center" bgcolor="#008000">
		<font face="Arial" size="1" color="#008000">G</font></td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T47" size="3" value="6"></td>
		<td align="center" bgcolor="#FF9900">
		<font face="Arial" size="1" color="#FF9900">O</font></td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T48" size="3"></td>
		<td align="center">&nbsp;</td>
		<td align="center"><font face="Arial" size="2">7</font></td>
		<td align="center"><input type="text" name="T48" size="3"></td>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td valign=center align=center colspan="18" >
		<p align="left"><input type="button" value="Update" name="B3"></td>
	</tr>
</table>

</body>

</html>