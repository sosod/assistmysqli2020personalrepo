<?php
    include("inc_ignite.php");
    
$style['R'] = " style=\"background-color: #ccccff; color: #000000;\"";
$style['RH'] = " style=\"background-color: #cccccc; color: #000000;\"";
$style['O'] = " style=\"background-color: #ceffde; color: #000000;\"";
$style['OH'] = " style=\"background-color: #cccccc; color: #000000;\"";
$style['C'] = " style=\"background-color: #ffeaca; color: #000000;\"";
$style['CH'] = " style=\"background-color: #cccccc; color: #000000;\"";
$style['TOT'] = " style=\"background-color: #666666; color: #FFFFFF;\"";

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Financial Information 2008/2009</b></h1>
<h2>Monthly Cashflow</h2>
<?php
//GET DEPT TOTALS
$sql = "SELECT d.id, f.month, sum(f.rb) rb, sum(f.ob) ob, sum(f.cb) cb, sum(f.ra) ra, sum(f.oa) oa, sum(f.ca) ca ";
$sql.= "FROM assist_".$cmpcode."_sdbip_dept d, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_cashflow f ";
$sql.= "WHERE d.id = c.deptid ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND c.id = f.cateid ";
$sql.= "GROUP BY d.id, f.month ";
$sql.= "ORDER BY d.id, f.month";
include("inc_db_con.php");
//$deptot = array();
    while($row = mysql_fetch_array($rs))
    {
        $deptot[$row['id']][$row['month']]['rb'] = $row['rb'];
        $deptot[$row['id']][$row['month']]['ob'] = $row['ob'];
        $deptot[$row['id']][$row['month']]['cb'] = $row['cb'];
        $deptot[$row['id']][$row['month']]['ra'] = $row['ra'];
        $deptot[$row['id']][$row['month']]['oa'] = $row['oa'];
        $deptot[$row['id']][$row['month']]['ca'] = $row['ca'];
    }
mysql_close();

//GET FIRST DATE OF YEAR
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
include("inc_db_con.php");
    $setup = mysql_fetch_array($rs);
    $fdate = $setup['value'];
mysql_close();


$fmon = date("n",$fdate);
$fyr = date("Y",$fdate);

for($mon=1;$mon<13;$mon++)
{
    $grandtot['rb'] = 0;
    $grandtot['ra'] = 0;
    $grandtot['ob'] = 0;
    $grandtot['oa'] = 0;
    $grandtot['cb'] = 0;
    $grandtot['ca'] = 0;
    if($mon!=1)
        echo("<p style=\"page-break-after: always; margin: 10 10 10 10; line-height: 0px\">&nbsp;</p>");
    $dispmon = $fmon+$mon-1;
    if($dispmon > 12)
    {
        $dispyear = $fyr+1;
        $dispmon = $dispmon - 12;
    }
    else
    {
        $dispyear = $fyr;
    }
    $dispdate = getdate(mktime(0,0,1,$dispmon,1,$dispyear));

    //$dispdate[0]=$fdate;
?>
<table border=1 cellpadding=3 cellspacing=0>
    <tr class=tdgeneral>
        <td rowspan=3 align=center>&nbsp;</td>
        <td colspan=9 align=center><h3 class=fc style="margin-top: 10px; margin-bottom -10px; line-height: 10px"><?php echo(date("F Y",$dispdate[0])); ?></h3></td>
    </tr>
    <tr class=tdheader>
        <td colspan=3 width=210>Budget</td>
        <td colspan=3 width=210>Actual</td>
        <td colspan=3 width=210>Variance</td>
    </tr>
    <tr class=tdgeneral align=center style="font-weight: bold; text-decoration: underline">
        <td width=70<?php echo($style['R']); ?>><b>Revenue</b></td>
        <td width=70<?php echo($style['O']); ?>><b>Opex</b></td>
        <td width=70<?php echo($style['C']); ?>><b>Capex</b></td>
        <td width=70<?php echo($style['R']); ?>><b>Revenue</b></td>
        <td width=70<?php echo($style['O']); ?>><b>Opex</b></td>
        <td width=70<?php echo($style['C']); ?>><b>Capex</b></td>
        <td width=70<?php echo($style['R']); ?>><b>Revenue</b></td>
        <td width=70<?php echo($style['O']); ?>><b>Opex</b></td>
        <td width=70<?php echo($style['C']); ?>><b>Capex</b></td>
    </tr>
<?php

    $sql = "SELECT d.value dept, c.value cate, c.id cid, d.id d FROM ";
    $sql.= "assist_".$cmpcode."_sdbip_dept d, ";
    $sql.= "assist_".$cmpcode."_sdbip_finance_category c ";
    $sql.= "WHERE d.id = c.deptid ";
    $sql.= "AND d.yn = 'Y' ";
    $sql.= "AND c.yn = 'Y' ";
    $sql.= "ORDER BY d.sort, c.sort";
    include("inc_db_con.php");
        $dept1 = "";
        while($row = mysql_fetch_array($rs))
        {
            $dept2 = $row['dept'];
            if($dept1 != $dept2)
            {
                echo("<tr class=tdgeneral ".$style['RH']."><td><b>".$dept2."</b></td>");
                $rb = $deptot[$row['d']][$mon]['rb'];
                $ra = $deptot[$row['d']][$mon]['ra'];
                $rv = $ra - $rb;
                $ob = $deptot[$row['d']][$mon]['ob'];
                $oa = $deptot[$row['d']][$mon]['oa'];
                $ov = $ob - $oa;
                $cb = $deptot[$row['d']][$mon]['cb'];
                $ca = $deptot[$row['d']][$mon]['ca'];
                $cv = $cb - $ca;
                $grandtot['rb'] = $grandtot['rb'] + $rb;
                $grandtot['ra'] = $grandtot['ra'] + $ra;
                $grandtot['ob'] = $grandtot['ob'] + $ob;
                $grandtot['oa'] = $grandtot['oa'] + $oa;
                $grandtot['cb'] = $grandtot['cb'] + $cb;
                $grandtot['ca'] = $grandtot['ca'] + $ca;
                ?>
                <td align=right<?php echo($style['RH']); ?>>&nbsp;<b><?php if($rb!=0){echo(number_format($rb));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['OH']); ?>>&nbsp;<b><?php if($ob!=0){echo(number_format($ob));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['CH']); ?>>&nbsp;<b><?php if($cb!=0){echo(number_format($cb));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['RH']); ?>>&nbsp;<b><?php if($ra!=0){echo(number_format($ra));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['OH']); ?>>&nbsp;<b><?php if($oa!=0){echo(number_format($oa));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['CH']); ?>>&nbsp;<b><?php if($ca!=0){echo(number_format($ca));}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['RH']); ?>>&nbsp;<b><?php if($rv!=0){if($rv<0){echo("<font color=red><b>".number_format($rv)."</b></font>");}else{echo(number_format($rv));}}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['OH']); ?>>&nbsp;<b><?php if($ov!=0){if($ov<0){echo("<font color=red><b>".number_format($ov)."</b></font>");}else{echo(number_format($ov));}}else{echo("&nbsp;-");} ?></b></td>
                <td align=right<?php echo($style['CH']); ?>>&nbsp;<b><?php if($cv!=0){if($cv<0){echo("<font color=red><b>".number_format($cv)."</b></font>");}else{echo(number_format($cv));}}else{echo("&nbsp;-");} ?></b></td>
                <?php
                echo("</tr>");
                $dept1 = $dept2;
            }
            echo("<tr class=tdgeneral align=right><td align=left>".$row['cate']."</td>");
            $sql2 = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_cashflow WHERE cateid = ".$row['cid']." AND month = ".$mon;
            include("inc_db_con2.php");
                if(mysql_num_rows($rs2) > 0)
                {
                    $row2 = mysql_fetch_array($rs2);
                    $rb = $row2['rb'];
                    $ra = $row2['ra'];
                    $rv = $ra - $rb;
                    $ob = $row2['ob'];
                    $oa = $row2['oa'];
                    $ov = $ob - $oa;
                    $cb = $row2['cb'];
                    $ca = $row2['ca'];
                    $cv = $cb - $ca;
                    ?>
                    <td align=right<?php echo($style['R']); ?>>&nbsp;<?php if($rb!=0){echo(number_format($rb));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['O']); ?>>&nbsp;<?php if($ob!=0){echo(number_format($ob));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['C']); ?>>&nbsp;<?php if($cb!=0){echo(number_format($cb));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['R']); ?>>&nbsp;<?php if($ra!=0){echo(number_format($ra));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['O']); ?>>&nbsp;<?php if($oa!=0){echo(number_format($oa));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['C']); ?>>&nbsp;<?php if($ca!=0){echo(number_format($ca));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['R']); ?>>&nbsp;<?php if($rv!=0){if($rv<0){echo("<font color=red>".number_format($rv)."</b></font>");}else{echo(number_format($rv));}}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['O']); ?>>&nbsp;<?php if($ov!=0){if($ov<0){echo("<font color=red>".number_format($ov)."</b></font>");}else{echo(number_format($ov));}}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right<?php echo($style['C']); ?>>&nbsp;<?php if($cv!=0){if($cv<0){echo("<font color=red>".number_format($cv)."</b></font>");}else{echo(number_format($cv));}}else{echo("&nbsp;-");} ?></b></td>
                    <?php
                }
                else
                {
                    ?>
                    <td<?php echo($style['R']); ?>>&nbsp;-</td>
                    <td<?php echo($style['O']); ?>>&nbsp;-</td>
                    <td<?php echo($style['C']); ?>>&nbsp;-</td>
                    <td<?php echo($style['R']); ?>>&nbsp;-</td>
                    <td<?php echo($style['O']); ?>>&nbsp;-</td>
                    <td<?php echo($style['C']); ?>>&nbsp;-</td>
                    <td<?php echo($style['R']); ?>>&nbsp;-</td>
                    <td<?php echo($style['O']); ?>>&nbsp;-</td>
                    <td<?php echo($style['C']); ?>>&nbsp;-</td>
                    <?php
                }
            echo("</tr>");
            mysql_close($rs2);
        }
    mysql_close();
    
    $grandtot['rv'] = $grandtot['ra'] - $grandtot['rb'];
    $grandtot['ov'] = $grandtot['ob'] - $grandtot['oa'];
    $grandtot['cv'] = $grandtot['cb'] - $grandtot['ca'];

?>
<tr class=tdheaderl <?php echo($style['TOT']); ?>>
<td>Total</td>
                    <td align=right>&nbsp;<?php if($grandtot['rb']!=0){echo(number_format($grandtot['rb']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['ob']!=0){echo(number_format($grandtot['ob']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['cb']!=0){echo(number_format($grandtot['cb']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['ra']!=0){echo(number_format($grandtot['ra']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['oa']!=0){echo(number_format($grandtot['oa']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['ca']!=0){echo(number_format($grandtot['ca']));}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['rv']!=0){if($grandtot['rv']<0){echo("<font color=#ff8b8e><b>".number_format($grandtot['rv'])."</b></font>");}else{echo(number_format($grandtot['rv']));}}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['ov']!=0){if($grandtot['ov']<0){echo("<font color=#ff8b8e><b>".number_format($grandtot['ov'])."</b></font>");}else{echo(number_format($grandtot['ov']));}}else{echo("&nbsp;-");} ?></b></td>
                    <td align=right>&nbsp;<?php if($grandtot['cv']!=0){if($grandtot['cv']<0){echo("<font color=#ff8b8e><b>".number_format($grandtot['cv'])."</b></font>");}else{echo(number_format($grandtot['cv']));}}else{echo("&nbsp;-");} ?></b></td>
</tr>
</table>
<?php
    if($mon != 12)
    {
//        echo("<p style=\"page-break-before: always\">&nbsp;</p>");
    }
}
?>
</body>

</html>
