<?php
    include("inc_ignite.php");
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 3";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $row = mysql_fetch_array($rs);
            $totaltimeperiods = $row['value'];
        }
        else
        {
            $totaltimeperiods = 4;
        }
    mysql_close();
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
    $d = $_GET['d'];
    if(strlen($d) > 0)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE id = ".$d;
        include("inc_db_con.php");
            $deptrow = mysql_fetch_array($rs);
        mysql_close();
        ?>
        <h1 class=fc><b>SDBIP Assist: <?php echo($deptrow['value']); ?></b></h1>
        <p style="margin-top: -12px; margin-bottom: 15px"><a href=#fin>Financial Performance</a> | <a href=#cap>Capital Projects</a> | <a href=#op>Operational KPIs</a> | <a href=#add>Additional KPIs Added During the Year</a></p>
        <p style="margin-top: -12px; margin-bottom: 0px"><label id=label1 for=lbl>Loading, please wait...</label><input type=hidden name=lbl></p>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_headings WHERE shortyn = 'Y' ORDER BY sort";
        include("inc_db_con.php");
            $h = 0;
            while($row = mysql_fetch_array($rs))
            {
                $headings[$h] = $row;
                $h++;
            }
        mysql_close();
        ?>
        <table border=1 cellpadding=3 cellspacing=0>
            <tr class=tdheader style="background-color: #ffffff; color: #000000">
        <?php
            foreach($headings as $head)
            {
                echo("<td rowspan=2>".$head['headshort']."</td>");
            }
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_list_time WHERE yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
                $t = 0;
                $s = 1;
                while($row = mysql_fetch_array($rs))
                {
                    $time[$t] = $row;
                    switch($s) {
                        case 1:
                            $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                            $s = 2;
                            break;
                        case 2:
                            $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                            $s = 3;
                            break;
                        case 3:
                            $style = "style=\"background-color: #ceffde; color: #000000;\"";
                            $s = 4;
                            break;
                        case 4:
                            $style = "style=\"background-color: #ccccff; color: #000000;\"";
                            $s = 1;
                            break;
                    }
                    $time[$t]['style'] = $style;
                    echo("<td colspan=3 ".$style.">".date("d-M-Y",$row['value'])."</td>");
                    $t++;
                }
            mysql_close();
            echo("</tr><tr class=tdheader style=\"color: #000000;\">");
            foreach($time as $period)
            {
                $style = $period['style'];
                echo("<td ".$style.">Target</td><td ".$style.">Actual</td><td ".$style.">R</td>");
            }
            $cols = count($headings) + (count($time)*3);
            ?>
            </tr>
<!--FINANCIAL PERFORMANCE STARTS HERE-->
<?php
            $sql = "SELECT count(i.kpiid) c ";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_type t, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa n, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa m, ";
            $sql.= " assist_".$cmpcode."_sdbip_prog a, ";
            $sql.= " assist_".$cmpcode."_sdbip_kpi_result r,";
            $sql.= " assist_".$cmpcode."_sdbip_list_time e ";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 1";
            $sql.= " AND a.deptid = ".$d;
            $sql.= " AND r.krtimeid = e.id";
            $sql.= " ORDER BY i.kpiid, e.sort";
            include("inc_db_con.php");
            $frow = mysql_fetch_array($rs);
            mysql_close();
            $fmnr = $frow['c'] * 1;
            if($fmnr > 0)
            {
?>
            <tr class=tdheaderl style="background-color: #606060; color: #ffffff">
                <?php
                echo("<td colspan=".$cols."><a name=fin></a>Financial Performance</td>");
                ?>
            </tr>
            <?php
            //GET FINANCIAL DATA PER DEPT PER MONTH
$sql = "SELECT d.id, f.month, sum(f.rb) rb, sum(f.ob) ob, sum(f.cb) cb, sum(f.ra) ra, sum(f.oa) oa, sum(f.ca) ca ";
$sql.= "FROM assist_".$cmpcode."_sdbip_dept d, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_finance_cashflow f ";
$sql.= "WHERE d.id = c.deptid ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND c.id = f.cateid ";
$sql.= "AND d.id = ".$d;
$sql.= " GROUP BY d.id, f.month ";
$sql.= "ORDER BY d.id, f.month";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $mont = $row['month'];
        $fin[$mont]['rb'] = $row['rb'];
        $fin[$mont]['ob'] = $row['ob'];
        $fin[$mont]['cb'] = $row['cb'];
        $fin[$mont]['ra'] = $row['ra'];
        $fin[$mont]['oa'] = $row['oa'];
        $fin[$mont]['ca'] = $row['ca'];
    }
mysql_close();
$fkpiarry[1] = "r";
$fkpiarry[2] = "c";
$fkpiarry[3] = "o";
$tper = $t;   //TIME PERIODS
$mtp = 12/$tper;  //MONTHS PER TIME PERIOD
$s=1;
            //GET KPI DETAILS
            $sql = "SELECT m.code munkpa,";
            $sql.= " m.value munkpaval,";
            $sql.= " n.code natkpa,";
            $sql.= " n.value natkpaval,";
            $sql.= " a.value progval,";
            $sql.= " i.kpiid,";
            $sql.= " i.kpicapnum capnum,";
            $sql.= " i.kpivalue kpival,";
            $sql.= " t.value kpitype,";
            $sql.= " i.kpiunit unitmeasure,";
            $sql.= " i.kpiward wards,";
            $sql.= " i.kpidriver driver,";
            $sql.= " i.kpibaseline baseline,";
            $sql.= " i.kpitarget target,";
            $sql.= " i.kpiperfweight pweight,";
            $sql.= " r.*";

//            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_type t, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa n, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa m, ";
            $sql.= " assist_".$cmpcode."_sdbip_prog a, ";
            $sql.= " assist_".$cmpcode."_sdbip_kpi_result r,";
            $sql.= " assist_".$cmpcode."_sdbip_list_time e ";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 1";
            $sql.= " AND a.deptid = ".$d;
            $sql.= " AND r.krtimeid = e.id";
            $sql.= " ORDER BY i.kpiid, e.sort";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No financial performance KPIs found.</td>");
                    echo("</tr>");
                }
                else
                {
                    $a = 0;
                    while($row = mysql_fetch_array($rs))
                    {
                        $a++;
                        echo("<tr class=tdgeneral>");
/*                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/
                            foreach($headings as $head)
                            {
                                switch($head['field'])
                                {
                                    case "munkpa":
                                        echo("<td align=center><a title=\"".$row['munkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."&nbsp;</a></td>");
                                        break;
                                    case "natkpa":
                                        echo("<td align=center><a title=\"".$row['natkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."&nbsp;</a></td>");
                                        break;
                                    case "capnum":
                                        echo("<td>&nbsp;</td>");
                                        break;
                                    case "kpival":
                                        echo("<td><a href=view.php?i=".$row['kpiid']."&d=".$d."&t=".$fkpiarry[$a].">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    default:
                                        echo("<td>".$row[$head['field']]."&nbsp;</td>");
                                        break;
                                }
                            }
                            $tp = 1;
                            $z = 1;
                            foreach($time as $period)
                            {
                                $b = $fkpiarry[$a]."b";
                                $c = $fkpiarry[$a]."a";
                                $kresult[$b] = 0;
                                $kresult[$c] = 0;
                                for($m=1;$m<($mtp+1);$m++)
                                {
                                    $kresult[$b] = $kresult[$b]+$fin[$z][$b];
                                    $kresult[$c] = $kresult[$c]+$fin[$z][$c];
                                    $z++;
                                }
                                if($tp != 1)
                                {
                                    $row = mysql_fetch_array($rs);
                                }
                                switch($s) {
                                    case 1:
                                        $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                                        $s = 2;
                                        break;
                                    case 2:
                                        $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                                        $s = 3;
                                        break;
                                    case 3:
                                        $style = "style=\"background-color: #ceffde; color: #000000;\"";
                                        $s = 4;
                                        break;
                                    case 4:
                                        $style = "style=\"background-color: #ccccff; color: #000000;\"";
                                        $s = 1;
                                        break;
                                }
                                //$krtarget = $row['krtarget'];
                                //$kractual = $row['kractual'];
                                $krtarget = $kresult[$b];
                                $kractual = $kresult[$c];
                                echo("<td align=center ".$style.">".number_format($krtarget,2)."</td>");
                                if($kractual != 0)
                                {
                                    $result = round($kractual / $krtarget * 100);
                                    if($result < 74)
                                    {
                                        $result = "<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>";
                                    }
                                    else
                                    {
                                        if($result < 100)
                                        {
                                            $result = "<td align=center style=\"background-color: #FE9900; color: #FE9900;\">O</td>";
                                        }
                                        else
                                        {
                                            $result = "<td align=center style=\"background-color: #006600; color: #006600;\">G</td>";
                                        }
                                    }
                                    echo("<td align=center ".$style.">".number_format($kractual,2)."</td>");
                                    echo($result);
                                }
                                else
                                {
                                    if($period['eint']>$today)
                                    {
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                    }
                                    else
                                    {
                                        if($krtarget>0)
                                        {
                                            echo("<td align=center ".$style.">0</td>");
                                            echo("<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>");
                                        }
                                        else
                                        {
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                        }
                                    }
                                }
                                $tp++;
                            }
                            $s=1;
                        echo("</tr>");
                    }
                }
            mysql_close();
            }
            ?>
<!--CAPITAL PROJECTS STARTS HERE-->
            <tr class=tdheaderl style="background-color: #606060; color: #ffffff">
                <?php
                echo("<td colspan=".$cols."><a name=cap></a>Capital Projects</td>");
                ?>
            </tr>
            <?php
$s = 1;
            $sql = "SELECT m.code munkpa,";
            $sql.= " m.value munkpaval,";
            $sql.= " n.code natkpa,";
            $sql.= " n.value natkpaval,";
            $sql.= " a.value progval,";
            $sql.= " i.kpiid,";
            $sql.= " i.kpicapnum capnum,";
            $sql.= " i.kpivalue kpival,";
            $sql.= " t.value kpitype,";
            $sql.= " i.kpiunit unitmeasure,";
            $sql.= " i.kpiward wards,";
            $sql.= " i.kpidriver driver,";
            $sql.= " i.kpibaseline baseline,";
            $sql.= " i.kpitarget target,";
            $sql.= " i.kpiperfweight pweight,";
            $sql.= " r.*";

//            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_type t, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa n, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa m, ";
            $sql.= " assist_".$cmpcode."_sdbip_prog a, ";
            $sql.= " assist_".$cmpcode."_sdbip_kpi_result r,";
            $sql.= " assist_".$cmpcode."_sdbip_list_time e ";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 2";
            $sql.= " AND a.deptid = ".$d;
            $sql.= " AND r.krtimeid = e.id";
            $sql.= " ORDER BY i.kpiid, e.sort";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No capital projects found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
/*                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/
                            foreach($headings as $head)
                            {
                                switch($head['field'])
                                {
                                    case "munkpa":
                                        echo("<td align=center><a title=\"".$row['munkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "natkpa":
                                        echo("<td align=center><a title=\"".$row['natkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "capnum":
                                        echo("<td>".$row[$head['field']]."&nbsp;</td>");
                                        break;
                                    case "kpival":
                                        echo("<td><a href=view.php?i=".$row['kpiid'].">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    default:
                                        echo("<td>".$row[$head['field']]."&nbsp;</td>");
                                        break;
                                }
                            }
/*            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, assist_".$cmpcode."_sdbip_list_type t, assist_".$cmpcode."_sdbip_list_kpa n, assist_".$cmpcode."_sdbip_prog a, assist_".$cmpcode."_sdbip_kpi_result r";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 2";
            $sql.= " AND a.deptid = ".$d;
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No capital projects found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
                            echo("<td>".$row['value']."</td>");
                            echo("<td>".$row['kpicapnum']."</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/                            $tp = 1;
                            if($s==1)
                            {
                                $krtarget2 = 0;
                            }
                            foreach($time as $period)
                            {
                                if($tp != 1)
                                {
                                    $row = mysql_fetch_array($rs);
                                }
                                switch($s) {
                                    case 1:
                                        $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                                        $s = 2;
                                        break;
                                    case 2:
                                        $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                                        $s = 3;
                                        break;
                                    case 3:
                                        $style = "style=\"background-color: #ceffde; color: #000000;\"";
                                        $s = 4;
                                        break;
                                    case 4:
                                        $style = "style=\"background-color: #ccccff; color: #000000;\"";
                                        $s = 1;
                                        break;
                                }
                                $krtarget = $row['krtarget'];
                                $kractual = $row['kractual'];
                                if($krtarget == 0 && $kractual > 0 && $s > 0)
                                {
                                    $krtarget = $krtarget2;
                                }
                                if($krtarget == 0 && $kractual == 0)
                                {
                                    $krtarget = "&nbsp;";
                                }
                                echo("<td align=center ".$style.">".round($krtarget)."</td>");
                                if($kractual != 0)
                                {
                                    if($krtarget > 0)
                                    {
                                        $result = round($kractual / $krtarget * 100);
                                    }
                                    else
                                    {
                                        $result = 101;
                                    }
                                    if($result < 74)
                                    {
                                        $result = "<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>";
                                    }
                                    else
                                    {
                                        if($result < 100)
                                        {
                                            $result = "<td align=center style=\"background-color: #FE9900; color: #FE9900;\">O</td>";
                                        }
                                        else
                                        {
                                            $result = "<td align=center style=\"background-color: #006600; color: #006600;\">G</td>";
                                        }
                                    }
                                    echo("<td align=center ".$style.">".round($row['kractual'])."</td>");
                                    echo($result);
                                }
                                else
                                {
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
                                    if($period['eint']>$today)
                                    {
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                    }
                                    else
                                    {
                                        if($krtarget>0)
                                        {
                                            echo("<td align=center ".$style.">0</td>");
                                            echo("<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>");
                                        }
                                        else
                                        {
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                        }
                                    }
                                }
                                $tp++;
                                $krtarget2 = $krtarget;
                            }
                            $s = 1;
                        echo("</tr>");
                    }
                }
            mysql_close();
            ?>
<!--OPERATIONAL PERFORMANCE STARTS HERE-->
            <tr class=tdheaderl style="background-color: #606060; color: #ffffff">
                <?php
                echo("<td colspan=".$cols."><a name=op></a>Operational Performance</td>");
                ?>
            </tr>
            <?php
            $s=1;
            $sql = "SELECT m.code munkpa,";
            $sql.= " m.value munkpaval,";
            $sql.= " n.code natkpa,";
            $sql.= " n.value natkpaval,";
            $sql.= " a.value progval,";
            $sql.= " i.kpiid,";
            $sql.= " i.kpicapnum capnum,";
            $sql.= " i.kpivalue kpival,";
            $sql.= " t.value kpitype,";
            $sql.= " i.kpiunit unitmeasure,";
            $sql.= " i.kpiward wards,";
            $sql.= " i.kpidriver driver,";
            $sql.= " i.kpibaseline baseline,";
            $sql.= " i.kpitarget target,";
            $sql.= " i.kpiperfweight pweight,";
            $sql.= " r.*";

//            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_type t, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa n, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa m, ";
            $sql.= " assist_".$cmpcode."_sdbip_prog a, ";
            $sql.= " assist_".$cmpcode."_sdbip_kpi_result r,";
            $sql.= " assist_".$cmpcode."_sdbip_list_time e ";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 3";
            $sql.= " AND a.deptid = ".$d;
            $sql.= " AND r.krtimeid = e.id";
            $sql.= " AND i.kpiadduser = 'IA'";
            $sql.= " AND i.kpiadddate = '0'";
            $sql.= " ORDER BY i.kpiid, e.sort";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No operational KPIs found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
/*                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/
                            foreach($headings as $head)
                            {
                                switch($head['field'])
                                {
                                    case "munkpa":
                                        echo("<td align=center><a title=\"".$row['munkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "natkpa":
                                        echo("<td align=center><a title=\"".$row['natkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "capnum":
                                        echo("<td>&nbsp;</td>");
                                        break;
                                    case "kpival":
                                        echo("<td><a href=view.php?i=".$row['kpiid'].">".$row[$head['field']]."&nbsp;</a></td>");
                                        break;
                                    default:
                                        echo("<td>".$row[$head['field']]."&nbsp;</td>");
                                        break;
                                }
                            }
/*            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, assist_".$cmpcode."_sdbip_list_type t, assist_".$cmpcode."_sdbip_list_kpa n, assist_".$cmpcode."_sdbip_prog a, assist_".$cmpcode."_sdbip_kpi_result r";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 3";
            $sql.= " AND a.deptid = ".$d;
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No operational performance KPIs found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/                            $tp = 1;
/*                            foreach($time as $period)
                            {
                                if($tp != 1)
                                {
                                    $row = mysql_fetch_array($rs);
                                }
                                switch($s) {
                                    case 1:
                                        $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                                        $s = 2;
                                        break;
                                    case 2:
                                        $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                                        $s = 3;
                                        break;
                                    case 3:
                                        $style = "style=\"background-color: #ceffde; color: #000000;\"";
                                        $s = 4;
                                        break;
                                    case 4:
                                        $style = "style=\"background-color: #ccccff; color: #000000;\"";
                                        $s = 1;
                                        break;
                                }
                                $krtarget = $row['krtarget'];
                                $kractual = $row['kractual'];
                                echo("<td align=center ".$style.">".$krtarget."</td>");
                                if($kractual != 0)
                                {
                                    $result = round($kractual / $krtarget * 100);
                                    if($result < 74)
                                    {
                                        $result = "<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>";
                                    }
                                    else
                                    {
                                        if($result < 100)
                                        {
                                            $result = "<td align=center style=\"background-color: #FE9900; color: #FE9900;\">R</td>";
                                        }
                                        else
                                        {
                                            $result = "<td align=center style=\"background-color: #006600; color: #006600;\">R</td>";
                                        }
                                    }
                                    echo("<td align=center ".$style.">".$row['kractual']."</td>");
                                    echo($result);
                                }
                                else
                                {
                                    echo("<td align=center ".$style.">&nbsp;</td>");
                                    echo("<td align=center ".$style.">&nbsp;</td>");
                                }
                                $tp++;
                            }*/
                            if($s==1)
                            {
                                $krtarget2 = 0;
                            }
                            foreach($time as $period)
                            {
                                if($tp != 1)
                                {
                                    $row = mysql_fetch_array($rs);
                                }
                                switch($s) {
                                    case 1:
                                        $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                                        $s = 2;
                                        break;
                                    case 2:
                                        $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                                        $s = 3;
                                        break;
                                    case 3:
                                        $style = "style=\"background-color: #ceffde; color: #000000;\"";
                                        $s = 4;
                                        break;
                                    case 4:
                                        $style = "style=\"background-color: #ccccff; color: #000000;\"";
                                        $s = 1;
                                        break;
                                }
                                $krtarget = $row['krtarget'];
                                $kractual = $row['kractual'];
                                if($krtarget == 0 && $kractual > 0 && $s > 0)
                                {
                                    $krtarget = $krtarget2;
                                }
                                if($krtarget == 0 && $kractual == 0)
                                {
                                    $krtarget = "&nbsp;";
                                }
                                echo("<td align=center ".$style.">".round($krtarget)."</td>");
                                if($kractual != 0)
                                {
                                    if($krtarget > 0)
                                    {
                                        $result = round($kractual / $krtarget * 100);
                                    }
                                    else
                                    {
                                        $result = 101;
                                    }
                                    if($result < 74)
                                    {
                                        $result = "<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>";
                                    }
                                    else
                                    {
                                        if($result < 100)
                                        {
                                            $result = "<td align=center style=\"background-color: #FE9900; color: #FE9900;\">R</td>";
                                        }
                                        else
                                        {
                                            $result = "<td align=center style=\"background-color: #006600; color: #006600;\">R</td>";
                                        }
                                    }
                                    echo("<td align=center ".$style.">".round($row['kractual'])."</td>");
                                    echo($result);
                                }
                                else
                                {
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
                                    if($period['eint']>$today)
                                    {
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                    }
                                    else
                                    {
                                        if($krtarget>0)
                                        {
                                            echo("<td align=center ".$style.">0</td>");
                                            echo("<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>");
                                        }
                                        else
                                        {
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                        }
                                    }
                                }
                                $tp++;
                                $krtarget2 = $krtarget;
                            }
                            $s=1;
                        echo("</tr>");
                    }
                }
            mysql_close();
            ?>
<!--ADDITIONAL OPERATIONAL PERFORMANCE DURING THE YEAR STARTS HERE-->
            <tr class=tdheaderl style="background-color: #606060; color: #ffffff">
                <?php
                echo("<td colspan=".$cols."><a name=add></a>Additional KPIs Added During the Year</td>");
                ?>
            </tr>
            <?php
            $s=1;
            $sql = "SELECT m.code munkpa,";
            $sql.= " m.value munkpaval,";
            $sql.= " n.code natkpa,";
            $sql.= " n.value natkpaval,";
            $sql.= " a.value progval,";
            $sql.= " i.kpiid,";
            $sql.= " i.kpicapnum capnum,";
            $sql.= " i.kpivalue kpival,";
            $sql.= " t.value kpitype,";
            $sql.= " i.kpiunit unitmeasure,";
            $sql.= " i.kpiward wards,";
            $sql.= " i.kpidriver driver,";
            $sql.= " i.kpibaseline baseline,";
            $sql.= " i.kpitarget target,";
            $sql.= " i.kpiperfweight pweight,";
            $sql.= " r.*";

//            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_type t, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa n, ";
            $sql.= " assist_".$cmpcode."_sdbip_list_kpa m, ";
            $sql.= " assist_".$cmpcode."_sdbip_prog a, ";
            $sql.= " assist_".$cmpcode."_sdbip_kpi_result r,";
            $sql.= " assist_".$cmpcode."_sdbip_list_time e ";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpimunkpaid = m.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 3";
            $sql.= " AND a.deptid = ".$d;
            $sql.= " AND r.krtimeid = e.id";
            $sql.= " AND i.kpiadduser <> 'IA'";
            $sql.= " AND i.kpiadddate <> '0'";
            $sql.= " ORDER BY i.kpiid, e.sort";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No additional KPIs found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
/*                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/
                            foreach($headings as $head)
                            {
                                switch($head['field'])
                                {
                                    case "munkpa":
                                        echo("<td align=center><a title=\"".$row['munkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "natkpa":
                                        echo("<td align=center><a title=\"".$row['natkpaval']."\" style=\"text-decoration: underline; color: #000000\">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    case "capnum":
                                        echo("<td>&nbsp;</td>");
                                        break;
                                    case "kpival":
                                        echo("<td><a href=view.php?i=".$row['kpiid'].">".$row[$head['field']]."</a>&nbsp;</td>");
                                        break;
                                    default:
                                        echo("<td>".$row[$head['field']]."&nbsp;</td>");
                                        break;
                                }
                            }
/*            $sql = "SELECT a.*, i.*, n.value nkpa, t.value tkpi, r.*";
            $sql.= " FROM assist_".$cmpcode."_sdbip_kpi i, assist_".$cmpcode."_sdbip_list_type t, assist_".$cmpcode."_sdbip_list_kpa n, assist_".$cmpcode."_sdbip_prog a, assist_".$cmpcode."_sdbip_kpi_result r";
            $sql.= " WHERE a.id = i.kpiprogid";
            $sql.= " AND i.kpinatkpaid = n.id";
            $sql.= " AND i.kpiid = r.krkpiid";
            $sql.= " AND i.kpitypeid = t.id";
            $sql.= " AND a.yn = 'Y'";
            $sql.= " AND i.kpiyn = 'Y'";
            $sql.= " AND a.typeid = 3";
            $sql.= " AND a.deptid = ".$d;
            include("inc_db_con.php");
                if(mysql_num_rows($rs)==0)
                {
                    echo("<tr class=tdgeneral>");
                        echo("<td colspan=".$cols.">No additional KPIs found.</td>");
                    echo("</tr>");
                }
                else
                {
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<tr class=tdgeneral>");
                            echo("<td>".$row['value']."</td>");
                            echo("<td>&nbsp;</td>");
                            echo("<td><a href=view.php?i=".$row['kpiid'].">".$row['kpivalue']."</a></td>");
                            echo("<td>".$row['kpiunit']."</td>");
                            echo("<td align=center>".$row['kpidriver']."</td>");
                            echo("<td align=center>".$row['kpitarget']."</td>");
*/                            $tp = 1;
                            if($s==1)
                            {
                                $krtarget2 = 0;
                            }
                            foreach($time as $period)
                            {
                                if($tp != 1)
                                {
                                    $row = mysql_fetch_array($rs);
                                }
                                switch($s) {
                                    case 1:
                                        $style = "style=\"background-color: #ffcccc; color: #000000;\"";
                                        $s = 2;
                                        break;
                                    case 2:
                                        $style = "style=\"background-color: #ffeaca; color: #000000;\"";
                                        $s = 3;
                                        break;
                                    case 3:
                                        $style = "style=\"background-color: #ceffde; color: #000000;\"";
                                        $s = 4;
                                        break;
                                    case 4:
                                        $style = "style=\"background-color: #ccccff; color: #000000;\"";
                                        $s = 1;
                                        break;
                                }
                                $krtarget = $row['krtarget'];
                                $kractual = $row['kractual'];
                                if($krtarget == 0 && $kractual > 0 && $s > 0)
                                {
                                    $krtarget = $krtarget2;
                                }
                                if($krtarget == 0 && $kractual == 0)
                                {
                                    $krtarget = "&nbsp;";
                                }
                                echo("<td align=center ".$style.">".$krtarget."</td>");
                                if($kractual != 0)
                                {
                                    if($krtarget > 0)
                                    {
                                        $result = round($kractual / $krtarget * 100);
                                    }
                                    else
                                    {
                                        $result = 101;
                                    }
/*                                if($krtarget == 0)
                                {
                                    $krtarget = "&nbsp;";
                                }
                                echo("<td align=center ".$style.">".$krtarget."</td>");
                                if($kractual != 0)// && $krtarget != 0)
                                {
                                    $result = round($kractual / $krtarget * 100);
*/                                    if($result < 74)
                                    {
                                        $result = "<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>";
                                    }
                                    else
                                    {
                                        if($result < 100)
                                        {
                                            $result = "<td align=center style=\"background-color: #FE9900; color: #FE9900;\">R</td>";
                                        }
                                        else
                                        {
                                            $result = "<td align=center style=\"background-color: #006600; color: #006600;\">R</td>";
                                        }
                                    }
                                    echo("<td align=center ".$style.">".$row['kractual']."</td>");
                                    echo($result);
                                }
                                else
                                {
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
//                                    echo("<td align=center ".$style.">&nbsp;</td>");
                                    if($period['eint']>$today)
                                    {
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                        echo("<td align=center ".$style.">&nbsp;</td>");
                                    }
                                    else
                                    {
                                        if($krtarget>0)
                                        {
                                            echo("<td align=center ".$style.">0</td>");
                                            echo("<td align=center style=\"background-color: #CC0001; color: #CC0001;\">R</td>");
                                        }
                                        else
                                        {
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                            echo("<td align=center ".$style.">&nbsp;</td>");
                                        }
                                    }
                                }
                                $tp++;
                                $krtarget2 = $krtarget;
                            }
                            $s=1;
                        echo("</tr>");
                    }
                }
            mysql_close();
            ?>
        </table>
<script language=JavaScript>
label1.innerText="";
</script>
        <?php

    }
    else
    {
        ?>
        <h1 class=fc><b>SDBIP Assist</b></h1>
        <p>Please select a department from the list above.</p>
        <?php
    }
?>




</body>

</html>
