<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist</b></h1>
<form name=log><input type=hidden name=dir value=<?php echo($cmpcode); ?>></form>
<div align=center>
<p><b>Revenue Collected (Year-to-date)</b></p>
<!-- amcolumn script-->
  <script type="text/javascript" src="/amcolumn/swfobject.js"></script>
	<div id="fin_rev">
		<strong>You need to upgrade your Flash Player</strong>
	</div>

	<script type="text/javascript">
        var cc = document.log.dir.value;
		// <![CDATA[
		var so = new SWFObject("/amcolumn/amcolumn.swf", "amcolumn", "600", "400", "8", "#FFFFFF");
		//so.addVariable("path", "/amcolumn/");
		so.addVariable("settings_file", escape("/files/"+cc+"/SDBIP/fin_rev_settings.xml")); // you can set two or more different settings files here (separated by commas)
		so.addVariable("data_file", escape("/files/"+cc+"/SDBIP/fin_rev_data.xml"));
//	so.addVariable("chart_data", "");                                          // you can pass chart data as a string directly from this file
//	so.addVariable("chart_settings", "");                                      // you can pass chart settings as a string directly from this file
//	so.addVariable("additional_chart_settings", "");                           // you append some chart settings to the loaded ones
//  so.addVariable("loading_settings", "LOADING SETTINGS");                    // you can set custom "loading settings" text here
  so.addVariable("loading_data", "LOADING DATA");                            // you can set custom "loading data" text here
    so.addVariable("preloader_color", "#000000");
		so.write("fin_rev");
		// ]]>
	</script>
<!-- end of amcolumn script -->
<p>&nbsp;</p>
<p><b>Operational Expenditure (Year-to-date)</b></p>
<!-- amcolumn script-->
  <script type="text/javascript" src="/amcolumn/swfobject.js"></script>
	<div id="fin_opex">
		<strong>You need to upgrade your Flash Player</strong>
	</div>

	<script type="text/javascript">
        var cc = document.log.dir.value;
		// <![CDATA[
		var so = new SWFObject("/amcolumn/amcolumn.swf", "amcolumn", "600", "400", "8", "#FFFFFF");
		//so.addVariable("path", "/amcolumn/");
		so.addVariable("settings_file", escape("/files/"+cc+"/SDBIP/fin_opex_settings.xml")); // you can set two or more different settings files here (separated by commas)
		so.addVariable("data_file", escape("/files/"+cc+"/SDBIP/fin_opex_data.xml"));
//	so.addVariable("chart_data", "");                                          // you can pass chart data as a string directly from this file
//	so.addVariable("chart_settings", "");                                      // you can pass chart settings as a string directly from this file
//	so.addVariable("additional_chart_settings", "");                           // you append some chart settings to the loaded ones
//  so.addVariable("loading_settings", "LOADING SETTINGS");                    // you can set custom "loading settings" text here
  so.addVariable("loading_data", "LOADING DATA");                            // you can set custom "loading data" text here
    so.addVariable("preloader_color", "#000000");
		so.write("fin_opex");
		// ]]>
	</script>
<!-- end of amcolumn script -->
<p>&nbsp;</p>
<p><b>Capital Expenditure (Year-to-date)</b></p>
<!-- amcolumn script-->
  <script type="text/javascript" src="/amcolumn/swfobject.js"></script>
	<div id="fin_capex">
		<strong>You need to upgrade your Flash Player</strong>
	</div>

	<script type="text/javascript">
        var cc = document.log.dir.value;
		// <![CDATA[
		var so = new SWFObject("/amcolumn/amcolumn.swf", "amcolumn", "600", "400", "8", "#FFFFFF");
		//so.addVariable("path", "/amcolumn/");
		so.addVariable("settings_file", escape("/files/"+cc+"/SDBIP/fin_capex_settings.xml")); // you can set two or more different settings files here (separated by commas)
		so.addVariable("data_file", escape("/files/"+cc+"/SDBIP/fin_capex_data.xml"));
//	so.addVariable("chart_data", "");                                          // you can pass chart data as a string directly from this file
//	so.addVariable("chart_settings", "");                                      // you can pass chart settings as a string directly from this file
//	so.addVariable("additional_chart_settings", "");                           // you append some chart settings to the loaded ones
//  so.addVariable("loading_settings", "LOADING SETTINGS");                    // you can set custom "loading settings" text here
  so.addVariable("loading_data", "LOADING DATA");                            // you can set custom "loading data" text here
    so.addVariable("preloader_color", "#000000");
		so.write("fin_capex");
		// ]]>
	</script>
<!-- end of amcolumn script -->

</div>
<p>&nbsp;</p>
</body>

</html>
