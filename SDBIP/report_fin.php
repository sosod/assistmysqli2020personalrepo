<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist 2008/9: Report</b></h1>
<form name=finreport method=post action=report_fin_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the headings you want in your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table1" cellpadding="3" cellspacing="0">
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="rb"></td>
		<td class="tdgeneral">Revenue - Budget</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="ra"></td>
		<td class="tdgeneral">Revenue - Actual</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="rv"></td>
		<td class="tdgeneral">Revenue - Variance</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="ob"></td>
		<td class="tdgeneral">Operational Expenditure - Budget</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="oa"></td>
		<td class="tdgeneral">Operational Expenditure - Actual</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="ov"></td>
		<td class="tdgeneral">Operational Expenditure - Variance</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="cb"></td>
		<td class="tdgeneral">Capital Expenditure - Budget</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="ca"></td>
		<td class="tdgeneral">Capital Expenditure - Actual</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="cv"></td>
		<td class="tdgeneral">Capital Expenditure - Variance</td>
	</tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3">
	<tr>
		<td class="tdgeneral">Department:</td>
		<td class="tdgeneral"><select size="1" name="dptfilter">
			<option selected value=ALL>All departments</option>
			<?php
            $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
                $dt = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $deptid[$dt] = $row['id'];
                    $dt++;
                    $id = $row['id'];
                    $value = str_replace("&#39","'",$row['value']);
                    echo("<option value=\"".$id."\">".$value."</option>");
                }
            mysql_close();
            ?>
		</select></td>
	</tr>
    <?php
		$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
		include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
        mysql_close();
		$mmon = date("n",$row['value']);
		$myear = date("Y",$row['value']);
        $tmon = date("n");
        $tyear = date("Y");
        if($tyear == $myear)
        {
            $ytdb = $tmon - $mmon + 1;
        }
        else
        {
            $ytdb = $tmon - $mmon + 13;
        }
    ?>
	<tr>
		<td class="tdgeneral" valign=top>Time period:</td>
		<td class="tdgeneral">
        <input type=radio name=datefilter value=YTD checked> Year-to-date<input type=hidden name=ytdb value=<?php echo($ytdb); ?>><br>
        <input type=radio name=datefilter value=ALL> Full year<br>
		<input type=radio name=datefilter value=SOME> from <select name=amon onchange=dtFilter()>
        <?php
//        date("d F Y H:i",$row['value'];
		for($m=0;$m<12;$m++)
		{
            //$mdate = getdate(mktime(0,0,0,$m,1,0));
            //$mmon2 = date("n",$mdate[0]);
            $mmon2 = $mmon + $m;
            if($mmon2 > 12)
            {
                $mmon2 = $mmon2-12;
                $myear2 = $myear+1;
            }
            else
            {
                $myear2 = $myear;
            }
            $mdate = getdate(mktime(0,0,0,$mmon2,1,$myear2));
            $msel = date("M-y",$mdate[0]);
            if($mmon == $mmon2)
            {
                echo("<option selected value=".($m+1).">".$msel."</option>");
            }
            else
            {
                echo("<option value=".($m+1).">".$msel."</option>");
            }
		}
        ?>
        </select> to <select name=zmon onchange=dtFilter()>
		<?php
		$tdate = mktime(0,0,0,date("n"),1,date("Y"));
		for($m=0;$m<12;$m++)
		{
            $mmon2 = $mmon + $m;
            if($mmon2 > 12)
            {
                $mmon2 = $mmon2-12;
                $myear2 = $myear+1;
            }
            else
            {
                $myear2 = $myear;
            }
            $mdate = getdate(mktime(0,0,0,$mmon2,1,$myear2));
            $msel = date("M-y",$mdate[0]);
            if($mdate[0] == $tdate)
            {
                echo("<option selected value=".($m+1).">".$msel."</option>");
            }
            else
            {
                echo("<option value=".($m+1).">".$msel."</option>");
            }
		}
        ?>
        </select>
        </td>
	</tr>
</table>
</div>
<h3 class=fc>3. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="N" checked id=csvn></td>
		<td class="tdgeneral"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="Y" id=csvy></td>
		<td class=tdgeneral><label for=csvy>Save to file (Microsoft Excel file)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>
<script language=Javascript>
function dtFilter() {
    //alert(document.finreport.datefilter[2].checked);
    document.finreport.datefilter[2].checked=true;
}
</script>
</body>

</html>
