<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist - Help</b></h1>
<p>&nbsp;</p>
<ul>
    <li><a href=sdbip_help_colours_ie.pdf target=_blank>Printing colours on the SDBIP report (in Internet Explorer)</a><br>&nbsp;</li>
    <li><u>To email an SDBIP report follow these steps:</u><ol>
        <li>Select the report you want on the SDBIP: Report page.</li>
        <li>Select the columns you want to display and filters you want to apply to the report.</li>
        <li>Before clicking the "Generate report" button, select "Save to file (Microsoft Excel file)" just above the "Generate report" button.</li>
        <li>Click the "Generate report" button and save the file when prompted.</li>
        <li>Apply any formatting you want to to the file in Microsoft Excel.</li>
        <li>Attach the file to the email as you would any other document.</li>
    </ol></li>
</ul>
</body>

</html>
