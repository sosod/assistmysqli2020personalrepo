<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Capital Projects 2008/2009</b></h1>
<p style="margin-top: -12px; margin-bottom: 0px"><label id=label1 for=lbl>Loading, please wait...</label><input type=hidden name=lbl></p>
<?php
$munbtot = 0;
$munatot = 0;
$dbtot = 0;
$datot = 0;
$moncash = "N";
$sql = "DESCRIBE assist_".$cmpcode."_sdbip_capital_projects";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['Field'] == "m1budget")
        {
            $moncash = "Y";
        }
    }
mysql_close();

//CHECK WHETHER THERE ARE ANY CAPITAL PROJECTS TO DISPLAY
$sql = "SELECT count(p.project) as cp ";
$sql.= "FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
$sql.= "assist_".$cmpcode."_sdbip_capital_category c, ";
$sql.= "assist_".$cmpcode."_sdbip_dept d ";
$sql.= "WHERE p.yn = 'Y' ";
$sql.= "AND c.yn = 'Y' ";
$sql.= "AND d.yn = 'Y' ";
$sql.= "AND p.cateid = c.id ";
$sql.= "AND c.deptid = d.id";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $cp = $row['cp'];
mysql_close();

if($cp > 0)     //IF THERE ARE CPs TO DISPLAY
{
    //GET LIST OF DEPARTMENTS
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_dept WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $d = 0;
        while($row = mysql_fetch_array($rs))
        {
            $dept[$d] = $row;
            $d++;
        }
    mysql_close();
?>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr class=tdheader>
		<td>No.</td>
		<?php
$sql2 = "SELECT headshort FROM assist_".$cmpcode."_sdbip_capital_headings WHERE field = 'pms'";
include("inc_db_con2.php");
    $row = mysql_fetch_array($rs2);
mysql_close($con2);
if(strlen($row['headshort'])>0)
{
        ?>
		<td><?php echo($row['headshort']); ?></td>
        <?php
}
else
{
        ?>
		<td>PMS<br>Ref.</td>
        <?php
}
?>
		<td>Project</td>
		<td>Wards</td>
		<td>Funding<br>Source</td>
		<td>Budget</td>
		<td>Progress</td>
		<td>Total<br>Spend</td>
		<td>Balance</td>
		<td>Start<br>Date</td>
		<td>End<br>Date</td>
	</tr>
	<?php
	$d=0;
    foreach($dept as $deptrow)
    {
        $c=0;
        if($d>0)
        {
            if($cpdept>0)
            {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td colspan=5 align=right rowspan=1><b>Total:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
	<?php
            }
    ?>
   	<tr class=tdgeneral bgcolor=#cde7ff>
        <td colspan=5 align=right rowspan=1><b>Total for <u><?php echo($deptval); ?></u>:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($dbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($datot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($dbtot-$datot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
	<?php

        }
        
        $d++;
    ?>
	<tr class=tdheaderbluel>
		<td colspan="12"><?php echo($deptrow['value']);?></td>
	</tr>
    <?php
    $dbtot = 0;
    $datot = 0;
        $sql = "SELECT c.id as cid, p.id as pid, c.*, p.* FROM assist_".$cmpcode."_sdbip_capital_projects p, ";
        $sql.= "assist_".$cmpcode."_sdbip_capital_category c ";
        $sql.= "WHERE p.yn = 'Y' ";
        $sql.= "AND c.yn = 'Y' ";
        $sql.= "AND p.cateid = c.id ";
        $sql.= "AND c.deptid = ".$deptrow['id']." ORDER BY c.id";
        include("inc_db_con.php");
            $cateid2 = 0;
            $cbtot = 0;
            $catot = 0;
            $cpdept = mysql_num_rows($rs);  //CHECK WHETHER THERE ARE CPs FOR THIS DEPT
            if($cpdept > 0)     //IF THERE ARE CPs FOR THIS DEPT
            {
                while($row = mysql_fetch_array($rs))
                {
                    $cateid1 = $row['cid'];
                    if($cateid1 != $cateid2)
                    {
                        if($c>0)
                        {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td colspan=5 align=right rowspan=1><b>Total:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
	<?php
                        }
                        $c++;
    ?>
	<tr class=tdheadergreenl>
		<td colspan="12"><?php echo($row['value']); ?></td>
	</tr>
                <?php
                    $cval = $row['value'];
                        $cbtot = 0;
                        $catot = 0;
                        $cateid2 = $cateid1;
                    }
                    if($moncash!="Y")
                    {
                        $budg = $row['budget'];
                        $tots = $row['totalspend'];
                    }
                    else
                    {
                        $budg = 0;
                        $tots = 0;
                        for($c=1;$c<13;$c++)
                        {
                            $fldb = "m".$c."budget";
                            $flda = "m".$c."actual";
                            $budg = $budg + $row[$fldb];
                            $tots = $tots + $row[$flda];
                        }
                    }
                    $cbtot = $cbtot + $budg;
                    $catot = $catot + $tots;
                    $dbtot = $dbtot + $budg;
                    $datot = $datot + $tots;
                    $munbtot = $munbtot + $budg;
                    $munatot = $munatot + $tots;
                    $rem = $budg - $tots;
                    $rem = number_format($rem,0);
                ?>
	<tr class=tdgeneral valign=top>
		<td align=center><?php echo($row['capnum']); ?>&nbsp;</td>
		<td align=center><?php echo($row['pmsref']); ?>&nbsp;</td>
		<?php
        $sql2 = "SELECT i.kpiid";
        $sql2.= " FROM assist_".$cmpcode."_sdbip_kpi i";
        $sql2.= " WHERE i.kpicapprojid = ".$row['pid']." AND kpiyn = 'Y'";
        include("inc_db_con2.php");
            $rc = mysql_num_rows($rs2);
            if($rc > 0)
            {
                $row2 = mysql_fetch_array($rs2);
                $url = "<a href=view.php?i=".$row2['kpiid'].">";
            }
            else
            {
                $url = "";
            }
        mysql_close($rs2);
        $row2 = "";
        ?>
		<td><?php echo($url.$row['project']); ?></a>&nbsp;</td>
		<td align=center><?php echo($row['wards']); ?>&nbsp;</td>
		<td align=center><?php echo($row['fundsource']); ?>&nbsp;</td>
		<td align=right><?php echo(number_format($budg,0)); ?></span></td>
		<?php
        $sql2 = "SELECT r.krcomment";
        $sql2.= " FROM assist_".$cmpcode."_sdbip_kpi i,";
        $sql2.= " assist_".$cmpcode."_sdbip_kpi_result r,";
        $sql2.= " assist_".$cmpcode."_sdbip_list_time t";
        $sql2.= " WHERE i.kpiid = r.krkpiid";
        $sql2.= " AND r.krtimeid = t.id";
        $sql2.= " AND i.kpicapprojid = ".$row['pid'];
        $sql2.= " AND r.krcomment <> ''";
        $sql2.= " ORDER BY t.start DESC";
        include("inc_db_con2.php");
            $cc = mysql_num_rows($rs2);
            if($cc > 0)
            {
                $row2 = mysql_fetch_array($rs2);
                echo("<td>".str_replace(chr(10),"<br>",$row2['krcomment'])."&nbsp;</td>");
            }
            else
            {
                echo("<td>&nbsp;</td>");
            }
        mysql_close($rs2);
        ?>
		<td align=right><?php echo(number_format($tots,0)); ?></span></td>
		<td align=right><?php if($rem < 0){ echo("<font color=red><b>"); } echo("R ".$rem); ?></b></font></span></td>
		<td align=center><?php echo(date("d M Y",$row['startdate'])); ?></td>
		<td align=center><?php echo(date("d M Y",$row['enddate'])); ?></td>
	</tr>
	<?php
                }       //WHILE ROW FETCH
                $cperr = "N";
            }       //IF NO CPs FOR THIS DEPT
            else
            {
                $cperr = "Y";
            ?>
            	<tr class=tdgeneral>
            		<td colspan="12">This department currently has no capital projects.</td>
            	</tr>
            <?php
            }
        mysql_close();
        $deptval = $deptrow['value'];
    }       //FOREACH DEPT
    if($cperr == "N")
    {
    ?>
   	<tr class=tdgeneral bgcolor=#CCFFCC>
        <td colspan=5 align=right rowspan=1><b>Total:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($cbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($catot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($cbtot-$catot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
	<?php
    ?>
   	<tr class=tdgeneral bgcolor=#cde7ff>
        <td colspan=5 align=right rowspan=1><b>Total for <u><?php echo($deptval); ?></u>:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($dbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($datot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($dbtot-$datot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
	<?php
    }
    ?>
<tr class=tdgeneral>
    <td colspan=11>&nbsp;</td>
</tr>
	<tr class=tdgeneral bgcolor=#ffcdcd>
        <td colspan=5 align=right rowspan=1><b>Grand Total for <?php echo($cmpname); ?>:</b></td>
        <td align=right rowspan=1><b><?php echo(number_format($munbtot,0)); ?></b></td>
        <td align=right rowspan=1>&nbsp;</td>
        <td align=right rowspan=1><b><?php echo(number_format($munatot,0)); ?></b></td>
        <td align=right rowspan=1><b><?php echo(number_format(($munbtot-$munatot),0)); ?></b></td>
        <td align=right rowspan=1 colspan=2>&nbsp;</td>
	</tr>
</table>
<?php
}
else        //IF NO CAPITAL PROJECTS TO DISPLAY
{
    echo("<p>There are no capital projects to display.</p>");
}
?>
<script language=JavaScript>
label1.innerText="";
</script>

</body>

</html>
