<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP Assist: Financial Information 2008/2009</b></h1>
<h2 class=fc>Revenue by Source</h2>
<table cellpadding=3 cellspacing=0 border=1>
<tr class=tdgeneral><td colspan=14>
<h3 class=fc>Monthly Revenue Projection</h3>
</td></tr>
    <tr class=tdheader>
        <td>Income Source</td>
        <?php
        //GET DATE OF FIRST OF YEAR
        $sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $fdate = $row['value'];
        mysql_close();
        //DRAW COLUMNS
        $mon = date("n",$fdate);
        $year = date("Y",$fdate);
        
        echo("<td>".date("M-y",$fdate)."</td>");
        
        for($f=1;$f<12;$f++)
        {
            $mon = $mon + 1;
            if($mon > 12)
            {
                $year = $year+1;
                $mon = 1;
            }
            $f2date = mktime(0,0,1,$mon,1,$year);
            echo("<td>".date("M-y",$f2date)."</td>");
        }
        ?>
        <td>Total</td>
    </tr>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_revbysource WHERE yn = 'Y' AND sort < 70 ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left><b>".$row['value']."</b></td>");
            $total = 0;
            for($f=1;$f<13;$f++)
            {
                $field = "m".$f."bill";
                if($row[$field] == 0)
                {
                    echo("<td>&nbsp;-</td>");
                }
                else
                {
                    $coltot[$f] = $coltot[$f] + $row[$field];
                    $total = $total + $row[$field];
                    echo("<td>&nbsp;".number_format($row[$field],0)."</td>");
                }
            }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
            echo("</tr>");
        }
    mysql_close();
    echo("<tr class=tdheader valign=top align=right>");
    echo("<td align=left><b>OPERATING INCOME GENERATED</b></td>");
    $total = 0;
    for($f=1;$f<13;$f++)
    {
//        $coltot[$f] = $coltot[$f] + $row[$field];
        $total = $total + $coltot[$f];
        echo("<td>&nbsp;<b>".number_format($coltot[$f],0)."</b></td>");
    }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
    echo("</tr>");
    
    
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_revbysource WHERE yn = 'Y' AND sort > 70 ORDER BY sort";
    include("inc_db_con.php");
    if(mysql_num_rows($rs) > 0)
    {
        while($row = mysql_fetch_array($rs))
        {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left><b>".$row['value']."</b></td>");
            $total = 0;
            for($f=1;$f<13;$f++)
            {
                $field = "m".$f."bill";
                if($row[$field] == 0)
                {
                    echo("<td>&nbsp;-</td>");
                }
                else
                {
                    $coltot[$f] = $coltot[$f] + $row[$field];
                    $total = $total + $row[$field];
                    echo("<td>&nbsp;".number_format($row[$field],0)."</td>");
                }
            }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
            echo("</tr>");
        }
    }
    else
    {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left>&nbsp;</td>");
            echo("<td align=left>&nbsp;</td>");
            echo("<td align=left>&nbsp;</td>");
            echo("</tr>");
    }
    mysql_close();
    echo("<tr class=tdheader valign=top align=right>");
    echo("<td align=left><b>DIRECT OPERATING INCOME</b></td>");
    $total = 0;
    for($f=1;$f<13;$f++)
    {
//        $coltot[$f] = $coltot[$f] + $row[$field];
        $total = $total + $coltot[$f];
        echo("<td>&nbsp;<b>".number_format($coltot[$f],0)."</b></td>");
    }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
    echo("</tr>");

$total = 0;
$coltot = array();
?><tr class=tdgeneral><td colspan=14>
<h3 class=fc>Monthly Revenue Collected</h3>
</td></tr>

    <tr class=tdheader>
        <td>Income Source</td>
        <?php
        //GET DATE OF FIRST OF YEAR
        $sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'SDBIP' AND refid = 2";
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $fdate = $row['value'];
        mysql_close();
        //DRAW COLUMNS
        $mon = date("n",$fdate);
        $year = date("Y",$fdate);

        echo("<td>".date("M-y",$fdate)."</td>");

        for($f=1;$f<12;$f++)
        {
            $mon = $mon + 1;
            if($mon > 12)
            {
                $year = $year+1;
                $mon = 1;
            }
            $f2date = mktime(0,0,1,$mon,1,$year);
            echo("<td>".date("M-y",$f2date)."</td>");
        }
        ?>
        <td>Total</td>
    </tr>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_revbysource WHERE yn = 'Y' AND sort < 70 ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left><b>".$row['value']."</b></td>");
            $total = 0;
            for($f=1;$f<13;$f++)
            {
                $field = "m".$f."actual";
                if($row[$field] == 0)
                {
                    echo("<td>&nbsp;-</td>");
                }
                else
                {
                    $coltot[$f] = $coltot[$f] + $row[$field];
                    $total = $total + $row[$field];
                    echo("<td>&nbsp;".number_format($row[$field],0)."</td>");
                }
            }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
            echo("</tr>");
        }
    mysql_close();
    echo("<tr class=tdheader valign=top align=right>");
    echo("<td align=left><b>OPERATING INCOME GENERATED</b></td>");
    $total = 0;
    for($f=1;$f<13;$f++)
    {
        if($coltot[$f] == 0)
        {
            echo("<td>&nbsp;-</td>");
        }
        else
        {
            $total = $total + $coltot[$f];
            echo("<td>&nbsp;".number_format($coltot[$f],0)."</td>");
        }
    }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
    echo("</tr>");
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbip_finance_revbysource WHERE yn = 'Y' AND sort > 70 ORDER BY sort";
    include("inc_db_con.php");
    if(mysql_num_rows($rs) > 0)
    {
        while($row = mysql_fetch_array($rs))
        {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left><b>".$row['value']."</b></td>");
            $total = 0;
            for($f=1;$f<13;$f++)
            {
                $field = "m".$f."actual";
                if($row[$field] == 0)
                {
                    echo("<td>&nbsp;-</td>");
                }
                else
                {
                    $coltot[$f] = $coltot[$f] + $row[$field];
                    $total = $total + $row[$field];
                    echo("<td>&nbsp;".number_format($row[$field],0)."</td>");
                }
            }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
            echo("</tr>");
        }
    }
    else
    {
            echo("<tr class=tdgeneral valign=top align=right>");
            echo("<td align=left>&nbsp;</td>");
            echo("<td align=left>&nbsp;</td>");
            echo("<td align=left>&nbsp;</td>");
            echo("</tr>");
    }
    mysql_close();
    echo("<tr class=tdheader valign=top align=right>");
    echo("<td align=left><b>DIRECT OPERATING INCOME (Cash collected)</b></td>");
    $total = 0;
    for($f=1;$f<13;$f++)
    {
//        $coltot[$f] = $coltot[$f] + $row[$field];
        if($coltot[$f] == 0)
        {
            echo("<td>&nbsp;-</td>");
        }
        else
        {
            $total = $total + $coltot[$f];
            echo("<td>&nbsp;".number_format($coltot[$f],0)."</td>");
        }
    }
            if($total > 0)
            {
                echo("<td>&nbsp;<b>".number_format($total,0)."</b></td>");
            }
            else
            {
                echo("<td>&nbsp;-</td>");
            }
    echo("</tr>");

    ?>
</table>

</body>

</html>
