<?php
include("inc_ignite.php");

include("inc_admin.php");
?>
<html>
<head>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: Setup - User Access</b></h1>
<p>Processing...</p>
<?php
//GET VARIABLES PASSED BY FORM
$act = $_POST['act'];
$start = $_POST['s'];
if(!is_numeric($start) || strlen($start)==0) { $start = 0; }
$tid = $_POST['tid'];
$deptid = $_POST['deptid'];
$v = $_POST['v'];
$r = $_POST['r'];
$a = $_POST['a'];
$p = $_POST['p'];
$m = $_POST['m'];
if($act=="single")
{
                $tk = $tid;
                $dpt = $deptid;
                $uv = $v;
                $ur = $r;
                $ua = $a;
                $up = $p;
                $um = $m;
                $didarr = explode("_",$dpt);
                if($didarr[0]=="d" && is_numeric($didarr[1]))
                {
                    $sid = 0;
                    $did = $didarr[1];
                }
                else
                {
                    if($didarr[0]=="s" && is_numeric($didarr[1]))
                    {
                        $sql = "SELECT subdirid FROM assist_".$cmpcode."_list_dirsub WHERE subid = ".$didarr[1];
                        include("inc_db_con.php");
                            $sub = mysql_fetch_array($rs);
                        mysql_close($con);
                        $sid = $didarr[1];
                        $did = $sub['subdirid'];
                    }
                }
                        if(is_numeric($did) && strlen($did)>0)
                        {
                            $sql = "INSERT INTO ".$dbref."_list_users (tkid, deptid, secid, yn, v, r, a, p, m) VALUES ";
                            $sql.= "('$tk',$did,$sid,'Y','$uv','$ur','$ua','$up','$um')";
                            include("inc_db_con.php");
                                $tsql = $sql;
                                $trans = "Added user access for user ".$tk;
                                include("inc_transaction_log.php");
                        }
                $res = "User added to ".$modtitle;
}
else
{
    if($act == "all")
    {
        $tc = count($tid);
        $dc = count($deptid);
        $vc = count($v);
        $rc = count($r);
        $ac = count($a);
        $pc = count($p);
        $mc = count($m);
        $c = $tc+$dc+$vc+$rc+$ac+$pc+$mc;
        $c = $c/7;
        if($c!=$tc && $c != $dc && $c != $vc && $c != $rc && $c != $ac && $c != $pc && $c != $mc && $c != round($c))
        {
            $res = "ERROR - Incomplete data";
        }
        else
        {
            $steps = count($tid);
            for($i=0;$i<$steps;$i++)
            {
                $tk = $tid[$i];
                $dpt = $deptid[$i];
                $uv = $v[$i];
                $ur = $r[$i];
                $ua = $a[$i];
                $up = $p[$i];
                $um = $m[$i];
                $didarr = explode("_",$dpt);
                if($didarr[0]=="d" && is_numeric($didarr[1]))
                {
                    $sid = 0;
                    $did = $didarr[1];
                }
                else
                {
                    if($didarr[0]=="s" && is_numeric($didarr[1]))
                    {
                        $sql = "SELECT subdirid FROM assist_".$cmpcode."_list_dirsub WHERE subid = ".$didarr[1];
                        include("inc_db_con.php");
                            $sub = mysql_fetch_array($rs);
                        mysql_close($con);
                        $sid = $didarr[1];
                        $did = $sub['subdirid'];
                    }
                }
                        if(is_numeric($did) && strlen($did)>0)
                        {
                            $sql = "INSERT INTO ".$dbref."_list_users (tkid, deptid, secid, yn, v, r, a, p, m) VALUES ";
                            $sql.= "('$tk',$did,$sid,'Y','$uv','$ur','$ua','$up','$um')";
                            include("inc_db_con.php");
                                $tsql = $sql;
                                $trans = "Added user access for user ".$tk;
                                include("inc_transaction_log.php");
                        }
            }
            $res = "Users added to ".$modtitle;
        }
    }
    else
    {
        $res = "ERROR - Unknown error.";
    }
}

?>
<script type=text/javascript>
var res = "<?php echo($res); ?>";
res = escape(res);
document.location.href = "setup_access.php?s=<?php echo($start); ?>&r="+res;
</script>
</body></html>
