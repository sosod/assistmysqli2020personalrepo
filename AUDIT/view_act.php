<?php
include("inc_ignite.php");
include("inc_raps.php");

//GET STATUS ID
$sid = $_GET['s'];
$rt = $_GET['t'];
//GET STATUS TEXT
    if($sid == "ALL")
    {
        $statusheading = "All incomplete $modcall";
        $status = 0;
        $statsql = "SELECT actid aid, actref actid, actassignedto, actassignedby, actadddate, CONCAT_WS(' - ', d.dirtxt,l.value) portfolio, p.value priority, s.value status, actactiondate, actdeadline, acttext1, acttext2, s.id statid ";
        $statsql.= "FROM ".$dbref."_action a, ".$dbref."_list_portfolio l, ".$dbref."_list_priority p, ".$dbref."_list_status s, assist_".$cmpcode."_list_dir d ";
        $statsql.= "WHERE a.actstatusid = s.id ";
        if($rt=="my")
        {
            $statsql.= "AND a.actassignedto = '".$tkid."' ";
        }
        $statsql.= "AND a.actstatusid <> 'CL' AND a.actstatusid <> 'CN' ";
        $statsql.= "AND a.actportfolioid = l.id ";
        $statsql.= "AND d.dirid = l.deptid ";
        $statsql.= "AND a.actpriorityid = p.id";
    }
    else
    {
        $sql = "SELECT value, heading FROM ".$dbref."_list_status WHERE id = '".$sid."'";
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $statusheading = $row['heading'];
            $status = $row['value'];
        mysql_close();
        $statsql = "SELECT actid aid, actref actid, actassignedto, actassignedby, actadddate, CONCAT_WS(' - ', d.dirtxt,l.value) portfolio, p.value priority, s.value status, actactiondate, actdeadline, acttext1, acttext2, s.id statid ";
        $statsql.= "FROM ".$dbref."_action a, ".$dbref."_list_portfolio l, ".$dbref."_list_priority p, ".$dbref."_list_status s, assist_".$cmpcode."_list_dir d ";
        $statsql.= "WHERE a.actstatusid = s.id ";
        if($rt=="my")
        {
            $statsql.= "AND a.actassignedto = '".$tkid."' ";
        }
        $statsql.= "AND a.actstatusid = '".$sid."' ";
        $statsql.= "AND a.actportfolioid = l.id ";
        $statsql.= "AND d.dirid = l.deptid ";
        $statsql.= "AND a.actpriorityid = p.id";
    }
    
$heading = array();
$sql = "SELECT * FROM ".$dbref."_headings WHERE headyn = 'Y' AND head$rt > 0 ORDER BY head$rt";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $heading[] = $row;
    }
mysql_close($con);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function viewAct(id) {
    if(id > 0 && !isNaN(parseInt(id)))
    {
        document.location.href="view_act_update.php?i="+id;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: View - <?php echo($statusheading); ?></b></h1>
<p style="font-size: 6pt; margin: 0 0 0 0;line-height: 6pt;">&nbsp;</p>
<?php
//GET LIST OF TASKS THAT ARE WITHIN STATUS
$sql = $statsql;
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0)
{
?>
<table cellspacing="0" cellpadding="4" width=100% style="margin-right: 10px; margin-left: 10px;">
	<tr height=30>
	<?php foreach($heading as $head) { ?>
		<th><?php echo($head['headshort']); ?></th>
    <?php } ?>
		<th width=40>&nbsp;</th>
	</tr>
<?php
while($row = mysql_fetch_array($rs))
{
//echo("<P>");
//print_r($row);
?>
	<tr valign=top>
	<?php
    foreach($heading as $head)
    {
        $fld = $head['headfield'];
        $val = $row[$fld];
        $align="left";
        switch($fld)
        {
            case "actid":
                if(strlen($val)==0) { $val = $row['aid']; }
                $align="center";
                break;
            case "actassignedto":
                $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $val = $row2['tkname']." ".$row2['tksurname'];
                break;
            case "actassignedby":
                $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $val = $row2['tkname']." ".$row2['tksurname'];
                break;
            case "actactiondate":
                $val = date("d-M-Y",$val);
                $align="center";
                break;
            case "actdeadline":
                if($row['statid']=="ON")
                {
                    $val = "N/A";
                }
                else
                {
                    if($val < $today) { $val2 = "<span class=fc style=\"font-weight: bold;\">"; } else { $val2 = "<span color=#000000>"; }
                    $val = $val2.date("d-M-Y",$val)."</span>";
                }
                $align="center";
                break;
            default:
                $val = str_replace(chr(10),"<br>",$val);
                if($fld=="priority" || $fld == "status") { $align="center"; }
                break;
        }
    ?>
		<td <?php echo("align=".$align.">".$val); ?></td>
    <?php
    }
    ?>
		<td align=center><input type="button" value="View" name="B4" onclick="viewAct(<?php echo($row['aid']); ?>)"></td>
	</tr>
<?php
}
}//IF MYSQL_NUM_ROWS > 0
else
{
    echo("<p>There are currently no $modcall to view.</p>");
}
mysql_close();
?>
</table>

<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=# onclick="document.location.href = 'view.php';" class=grey>Go back</a></p>
</body>

</html>
