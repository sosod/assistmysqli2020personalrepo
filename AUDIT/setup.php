<?php
include("inc_ignite.php");

//GET SETUP ADMIN TKID
$ta0 = $_POST['ACT0'];
//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(strlen($ta0) > 0)
{
    //GET CURRENT SETUP ADMIN TKID
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '".$modref."' AND refid = 0";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ta0z = $row['value'];
    mysql_close();
    $sql = "";
    //UPDATE ASSIST-SETUP TABLE
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0."' WHERE ref = '".$modref."' AND refid = 0";
    include("inc_db_con.php");
        //Set transaction log values for this update
        $trans = "Updated $tref Setup Administrator to ".$ta0;
    $sql = "";

    //PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
        $tsql = $sql;
        $told = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0z."' WHERE ref = '".$modref."' AND refid = 0";
        $tref = $modref;
        include("inc_transaction_log.php");

    $result = "<p><i>Administrator update complete.</i></p>";
}
else
{
    $result = "";
}

include("inc_admin.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b><?php echo($modtitle); ?>: Setup</b></h1>
<?php if(strlen($result)>0) { ?>
<div class="ui-widget">
    <div class="ui-state-highlight ui-corner-all" style="margin: 30px 0px 10px 10px; padding: 0 .3em;">
        <p><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>
        <?php echo($result); ?></p>
    </div>
</div>
<?php } else { ?>
<p>&nbsp;</p>
</p>
<?php } ?>
<ul>
<form name=update method=post action=setup.php>
<li>Setup Administrator: <select name=ACT0>
<?php
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($setupadmn == "0000")
{
?>
    <option selected value=0000>Ignite Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Ignite Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = '".$modref."' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $setupadmn)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close();

?>
</select> <input type=submit value=Update><br>&nbsp;</li>

<li><a href=setup_access.php>User Access</a><br>&nbsp;</li>

<li><a href=setup_portfolio.php>Update Portfolios</a><br>&nbsp;</li>
</ul>
</form>
</body>

</html>
