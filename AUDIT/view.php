<?php
include("inc_ignite.php");
include("inc_admin.php");

//COUNT NUMBER OF TASKS PER STATUS FOR THE CURRENT USER
$sql = "SELECT s.id, count(a.actid) AS tcount FROM ".$dbref."_list_status s, ".$dbref."_action a ";
$sql .= "WHERE a.actstatusid = s.id AND a.actassignedto = '".$tkid."' AND s.yn = 'Y' AND s.id <> 'CN' ";
$sql .= "GROUP BY s.id";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $id = $row['id'];
    $tc = $row['tcount'];
    $tstat[$id] = $tc;
}
mysql_close();
$tstat['ALL'] = $tstat['NW'] + $tstat['IP'] + $tstat['ON'];

$sql = "SELECT s.id, count(a.actid) AS tcount FROM ".$dbref."_list_status s, ".$dbref."_action a ";
$sql .= "WHERE a.actstatusid = s.id AND s.yn = 'Y' AND s.id <> 'CN' ";
$sql .= "GROUP BY s.id";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $id = $row['id'];
    $tc = $row['tcount'];
    $tstat2[$id] = $tc;
}
mysql_close();
$tstat2['ALL'] = $tstat2['NW'] + $tstat2['IP'] + $tstat2['ON'];

$status = array();
$sql = "SELECT id, heading FROM ".$dbref."_list_status WHERE yn = 'Y' AND id <> 'CN' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $status[] = $row;
    }
mysql_close($con);

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: View</b></h1>
<p style="font-size: 6pt; margin: 0 0 0 0;line-height: 6pt;">&nbsp;</p>
<?php if($jobadmin['r'] == "Y" || $tstat['ALL'] > 0 || $jobadmin['v']=="Y")
{
?>
<table cellpadding=3 cellspacing=0 style="margin-left: 10px;">
<?php
$th = "";
$td = "";

if($jobadmin['r'] == "Y" || $tstat['ALL'] > 0) {
    $th.="<th width=250>My $modcallheads</th>";
    $td.="<td><ul class=ul2>";
    foreach($status as $ts)
    {
        $tc = $tstat[$ts['id']];
        if(strlen($tc)==0 || !is_numeric($tc)) { $tc = 0; }
        $td.="<li>";
        if($tc>0) { $td.="<a href=view_act.php?t=my&s=".$ts['id'].">"; }
        $td.=$ts['heading']."</a> ($tc)</li>";
    }
        $tc = $tstat['ALL'];
        if(strlen($tc)==0 || !is_numeric($tc)) { $tc = 0; }
        $td.="<li>";
        if($tc>0) { $td.="<a href=view_act.php?t=my&s=ALL>"; }
        $td.="All Incomplete $modcallheads</a> ($tc)</li>";
    $td.="</ul></td>";
}

if($jobadmin['v']=="Y") {
    $th.="<th width=250>All $modcallheads</th>";
    $td.="<td><ul class=ul2>";
    foreach($status as $ts)
    {
        $tc = $tstat2[$ts['id']];
        if(strlen($tc)==0 || !is_numeric($tc)) { $tc = 0; }
        $td.="<li>";
        if($tc>0) { $td.="<a href=view_act.php?t=all&s=".$ts['id'].">"; }
        $td.=$ts['heading']."</a> ($tc)</li>";
    }
        $tc = $tstat2['ALL'];
        if(strlen($tc)==0 || !is_numeric($tc)) { $tc = 0; }
        $td.="<li>";
        if($tc>0) { $td.="<a href=view_act.php?t=all&s=ALL>"; }
        $td.="All Incomplete $modcallheads</a> ($tc)</li>";
    $td.="</ul></td>";
}


?>
    <tr height=30>
        <?php echo($th); ?>
    </tr>
    <tr valign=top>
        <?php echo($td); ?>
    </tr>
</table>
<?php
}
else
{
    echo("<p>You do not have permission to view $modcall.");
}


?>
</body>

</html>
