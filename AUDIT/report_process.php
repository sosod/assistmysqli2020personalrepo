<?php
include("inc_ignite.php");
include("inc_admin.php");

function getLog($aid,$adate) {
    global $dbref;
    global $cmpcode;
    $sql2 = "SELECT * FROM ".$dbref."_log WHERE logactid = $aid AND logdate <> $adate ORDER BY logid DESC LIMIT 1";
    include("inc_db_con2.php");
        $logrow = mysql_fetch_array($rs2);
        $logrow['mnr'] = mysql_num_rows($rs2);
    mysql_close($con2);
    return $logrow;
}

$heading = getHead('R');
$headings = $heading['s'];
unset($heading['s']);
$newhead['headfield'] = "logdate";
$newhead['headshort'] = "Latest Update Date";
$newhead['headtype'] = "L";
$heading[] = $newhead;
$newhead['headfield'] = "duration";
$newhead['headshort'] = "Duration";
$newhead['headtype'] = "L";
$heading[] = $newhead;

//GET VARIABLES
$field = $_POST['field'];
//$udf = $_POST['udf'];
//GET FILTERS
$filter = array();
foreach($heading as $head)
{
    $fld = $head['headfield'];
    $filter[$fld]['h'] = $fld;
    $filter[$fld]['f'] = $_POST[$fld.'filter'];
    $filter[$fld]['t'] = $_POST[$fld.'filtertype'];
}
//GET OUTPUT METHOD
$csvfile = $_POST['csvfile'];

//CONVERT FROM array[#] = text TO array[text] = Y
foreach($field as $f)
{
    $farray[$f] = "Y";
}

//SET SQL VARIABLE
$sql = "SELECT actid aid";
$sql.= ", actref actid";
$sql.= ", actassignedto";
$sql.= ", actassignedby";
$sql.= ", actadddate";
$sql.= ", CONCAT_WS(' - ', d.dirtxt,l.value) portfolio";
$sql.= ", p.value priority";
$sql.= ", s.value status";
$sql.= ", actstate";
$sql.= ", actactiondate";
$sql.= ", actdeadline";
$sql.= ", acttext1";
$sql.= ", acttext2";
$sql.= ", s.id statid ";
$sql.= "FROM ".$dbref."_action a, ".$dbref."_list_portfolio l, ".$dbref."_list_priority p, ".$dbref."_list_status s, assist_".$cmpcode."_list_dir d ";
$sql.= "WHERE a.actstatusid = s.id ";
$sql.= "AND a.actportfolioid = l.id ";
$sql.= "AND d.dirid = l.deptid ";
$sql.= "AND a.actpriorityid = p.id ";
foreach($heading as $head)
{
    $fld = $head['headfield'];
    $ff = $filter[$fld]['f'];
    $ft = $filter[$fld]['t'];
    switch($fld)
    {
        case "portfolio":
            if($ff != "ALL" && strlen($ff) > 0 && is_numeric($ff)) { $sql.= "AND l.id = $ff "; }
            break;
        case "priority":
            if($ff != "ALL" && strlen($ff) > 0 && is_numeric($ff)) { $sql.= "AND p.id = $ff "; }
            break;
        case "status":
            switch($ff)
            {
                case 1: //incomplete & ongoing
                    $sql.= "AND (s.id = 'IP' OR s.id = 'NW' OR s.id = 'ON') ";
                    break;
                case 2: //incomplete only
                    $sql.= "AND (s.id = 'IP' OR s.id = 'NW') ";
                    break;
                case 3: //ongoing only
                    $sql.= "AND (s.id = 'ON') ";
                    break;
                case 4: //completed only
                    $sql.= "AND (s.id = 'CL') ";
                    break;
                case 5: //all
                    $sql.= "AND s.id <> 'CN' ";
                    break;
            }
            break;
        case "actactiondate":
            $from = $ff[0];
            $to = $ff[1];
            if(strlen($from)>0)
            {
                $from = explode("-",$from);
                $actdatefrom = mktime(12,0,0,$from[1],$from[0],$from[2]);
                $sql.= "AND a.actactiondate >= ".$actdatefrom." ";
            }
            if(strlen($to)>0)
            {
                $to = explode("-",$to);
                $actdateto = mktime(12,0,0,$to[1],$to[0],$to[2]);
                $sql.= "AND a.actactiondate <= ".$actdateto." ";
            }
            break;
        default:
            if($fld == "actid" || $fld == "acttext1" || $fld == "acttext2")
            {
                if(strlen($ff)>0)
                {
                    switch($ft)
                    {
                        case 1: //exact
                            $sql.= "AND ".$fld." LIKE '%".code($ff)."%' ";
                            break;
                        case 2: //all
                            $sql1 = "";
                            $ffarr = explode(" ",$ff);
                            if(count($ffarr)>0)
                            {
                                $sql.= "AND (";
                                foreach($ffarr as $fa)
                                {
                                    if(strlen($sql1)>0)
                                    {
                                        $sql1.= " AND ";
                                    }
                                    $sql1.= $fld." LIKE '%".$fa."%' ";
                                }
                                $sql.= $sql1.") ";
                            }
                            break;
                        case 3: //any
                            $sql1 = "";
                            $ffarr = explode(" ",$ff);
                            if(count($ffarr)>0)
                            {
                                $sql.= "AND (";
                                foreach($ffarr as $fa)
                                {
                                    if(strlen($sql1)>0)
                                    {
                                        $sql1.= " OR ";
                                    }
                                    $sql1.= $fld." LIKE '%".$fa."%' ";
                                }
                                $sql.= $sql1.") ";
                            }
                            break;
                    }
                }
            }
            else
            {
                if($fld == "actassignedto" || $fld == "actassignedby")
                {
                    if($ff!= "ALL") { $sql.= "AND ".$fld." = '".$ff."' "; }
                }
            }
            break;
    }
}
$sql .= " ORDER BY a.actid";
include("inc_db_con.php");


if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    ?><script type=text/javascript>
    alert("There are no <?php echo($modcall); ?> that meet your criteria.\nPlease go back and select different criteria.");
    document.location.href = "report.php";
    </script><?php
}
//ELSE
else
{
    $display = "";
    //CREATE TABLE AND SET TDHEADER
//            $fdata = "\"Ref\"";
//            if($farray['adddate'] == "Y") {$fdata .= ",\"Assigned on\"";}
        foreach($heading as $head)
        {
            $fld = $head['headfield'];
            if($farray[$fld]=="Y")
            {
                if(strlen($display)>0) { $display.=","; }
                $display.= "\"".$head['headshort']."\"";
            }
        }
	$display .= "\r\n";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $aid = $row['aid'];
        $actadddate = $row['actadddate'];
        if($farray['logupdate']=="Y" || $farray['logdate']=="Y" || $farray['duration']=="Y")
        {
            $logrow = getLog($aid,$actadddate);
        }
        $line = "";
        foreach($heading as $head)
        {
            $fld = $head['headfield'];
            if($farray[$fld]=="Y")
            {
                if(strlen($line)>0) { $line.= ","; }
                $val = $row[$fld];
                $align="left";
                switch($fld)
                {
                    case "actadddate":
                        $val = date("d-M-Y H:i:s",$val);
                        $align="center";
                        break;
                    case "actid":
                        if(strlen($val)==0) { $val = $row['aid']; }
                        $val = decode($val);
                        $align="center";
                        break;
                    case "actassignedto":
                        $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $val = $row2['tkname']." ".$row2['tksurname'];
                        $val = decode($val);
                        break;
                    case "actassignedby":
                        $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $val = $row2['tkname']." ".$row2['tksurname'];
                        $val = decode($val);
                        break;
                    case "actactiondate":
                        $val = date("d-M-Y",$val);
                        $align="center";
                        break;
                    case "actdeadline":
                        if($row['statid']=="ON")
                        {
                            $val = "N/A";
                        }
                        else
                        {
                            $val = date("d-M-Y",$val);
                        }
                        $align="center";
                        break;
                    case "logupdate":
                        if($logrow['mnr']>0)
                        {
                            $val = decode($logrow['logupdate']);
                        }
                        break;
                    case "logdate":
                        if($logrow['mnr']>0)
                        {
                            $val = date("d-M-Y H:i:s",$logrow['logdate']);
                            $align="center";
                        }
                        break;
                    case "duration":
                        $val = "";
                        if($row['statid']=="CL" && $logrow['mnr']>0)
                        {
                            $updatedate = $logrow['logdate'];
                            $adddate = $actadddate;
                            if(strlen($updatedate)>0 && is_numeric($updatedate) && strlen($adddate)>0 && is_numeric($adddate))
                            {
                                $duration = $updatedate - $adddate;
                                $durdays = floor($duration/86400);
                                $durhours = floor(($duration-($durdays*86400))/3600);
                                $durmins = floor(($duration - ($durdays*86400 + $durhours*3600))/60);
                                $dursecs = $duration - ($durdays*86400 + $durhours*3600 + $durmins*60);
                                if($durhours<10) { $durhours = "0".$durhours; }
                                if($durmins<10) { $durmins = "0".$durmins; }
                                if($dursecs<10) { $dursecs = "0".$dursecs; }
                                //$val = $durdays;
                                //if($durdays>1) { $val.=" days "; } else { $val.= " day "; }
                                $durhours = $durhours+($durdays*24);
                                $val.= $durhours.":".$durmins.":".$dursecs;
                            }
                        }
                        break;
                    default:
                        $val = decode($val);
                        if($fld == "status")
                        {
                            if($row['statid']=="IP")
                            {
                                $val.= " (".$row['actstate']."%)";
                            }
                        }
                        break;
                }
                $line.= "\"$val\"";
            }
        }
        $display.= $line."\r\n";
    }
}
mysql_close($con);
//echo($display);
    //WRITE DATA TO FILE
    $filename = "../files/".$cmpcode."/".$repref."_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = $repref."_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$display."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
}
else    //ELSE OUTPUT IS ONSCREEN DISPLAY
{
//SET DISPLAY OF HTML HEAD & BODY
?><html><link rel=stylesheet href=/default.css type=text/css>
<?php include("inc_style.php"); ?>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: Report</b></h1>
<?php
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    ?><p>There are no <?php echo($modcall); ?> that meet your criteria.<br>Please go back and select different criteria.</p><?php
}
//ELSE
else
{
    $display = "";
    //CREATE TABLE AND SET TDHEADER
    $display.= "<table cellspacing=0 cellpadding=3>";
        $display.= chr(10)."    <tr>";
        foreach($heading as $head)
        {
            $fld = $head['headfield'];
            if($farray[$fld]=="Y")
            {
                $display.= chr(10)."        <th>".$head['headshort']."</th>";
            }
        }
	$display .= chr(10)."    </tr>";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $aid = $row['aid'];
        $actadddate = $row['actadddate'];
        if($farray['logupdate']=="Y" || $farray['logdate']=="Y" || $farray['duration']=="Y")
        {
            $logrow = getLog($aid,$actadddate);
        }
        $display.= chr(10)."    <tr valign=top>";
        foreach($heading as $head)
        {
            $fld = $head['headfield'];
            if($farray[$fld]=="Y")
            {
                $val = $row[$fld];
                $align="left";
                switch($fld)
                {
                    case "actadddate":
                        $val = date("d-M-Y H:i:s",$val);
                        $align="center";
                        break;
                    case "actid":
                        if(strlen($val)==0) { $val = $row['aid']; }
                        $align="center";
                        break;
                    case "actassignedto":
                        $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $val = $row2['tkname']." ".$row2['tksurname'];
                        break;
                    case "actassignedby":
                        $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$val'";
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $val = $row2['tkname']." ".$row2['tksurname'];
                        break;
                    case "actactiondate":
                        $val = date("d-M-Y",$val);
                        $align="center";
                        break;
                    case "actdeadline":
                        if($row['statid']=="ON")
                        {
                            $val = "N/A";
                        }
                        else
                        {
                            if($val < $today) { $val2 = "<span class=fc style=\"font-weight: bold;\">"; } else { $val2 = "<span color=#000000>"; }
                            $val = $val2.date("d-M-Y",$val)."</span>";
                        }
                        $align="center";
                        break;
                    case "logupdate":
                        if($logrow['mnr']>0)
                        {
                            $val = str_replace(chr(10),"<br>",$logrow['logupdate']);
                        }
                        break;
                    case "logdate":
                        if($logrow['mnr']>0)
                        {
                            $val = date("d-M-Y H:i:s",$logrow['logdate']);
                            $align="center";
                        }
                        break;
                    case "duration":
                        $val = "&nbsp;";
                        if($row['statid']=="CL" && $logrow['mnr']>0)
                        {
                            $updatedate = $logrow['logdate'];
                            $adddate = $actadddate;
                            if(strlen($updatedate)>0 && is_numeric($updatedate) && strlen($adddate)>0 && is_numeric($adddate))
                            {
                                $duration = $updatedate - $adddate;
                                $durdays = floor($duration/86400);
                                $durhours = floor(($duration-($durdays*86400))/3600);
                                $durmins = floor(($duration - ($durdays*86400 + $durhours*3600))/60);
                                $dursecs = $duration - ($durdays*86400 + $durhours*3600 + $durmins*60);
                                if($durhours<10) { $durhours = "0".$durhours; }
                                if($durmins<10) { $durmins = "0".$durmins; }
                                if($dursecs<10) { $dursecs = "0".$dursecs; }
                                $val = $durdays;
                                if($durdays>1) { $val.=" days "; } else { $val.= " day "; }
                                $val.= $durhours.":".$durmins.":".$dursecs;
                            }
                        }
                        break;
                    default:
                        $val = str_replace(chr(10),"<br>",$val);
                        if($fld == "status")
                        {
                            if($row['statid']=="IP")
                            {
                                $val.= " (".$row['actstate']."%)";
                            }
                        }
                        break;
                }
                $display.= chr(10)."        <td align=$align > $val </td>";
            }
        }
        $display.= chr(10)."    </tr>";
    }
    $display .= "</table>";
}
mysql_close($con);
//WRITE DISPLAY TO SCREEN
echo($display);
?>
</body></html>
<?php
}   //ENDIF CSVFILE = Y

//arrprint($heading);
//arrprint($field);
//arrprint($filter);
//echo("<P>".$sql);
?>

