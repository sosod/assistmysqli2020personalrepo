<?php include("inc_ignite.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: New <?php echo($modcallhead); ?></b></h1>
<?php
$head = array();
$headings = array();
$sql = "SELECT * FROM ".$dbref."_headings WHERE headyn = 'Y' AND headtype = 'A' ORDER BY headsort";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $head[] = $row;
    $headings[$row['headfield']] = $row['headfull'];
}
mysql_close($con);

foreach($head as $hd)
{
    $field[$hd['headfield']] = $_POST[$hd['headfield']];
}

$field['actassignedby'] = $tkid;

$actdate = $field['actactiondate'];
$adate = explode("-",$actdate);
$field['actactiondate'] = mktime(12,0,0,$adate[1],$adate[0],$adate[2]);
if($field['status']!="ON")
{
    $deaddate = $field['actdeadline'];
    $ddate = explode("-",$deaddate);
    $field['actdeadline'] = mktime(12,0,0,$ddate[1],$ddate[0],$ddate[2]);
}
else
{
    $field['actdeadline'] = 0;
}
$field['actid'] = htmlentities($field['actid'],ENT_QUOTES,"ISO-8859-1");
$field['acttext1'] = htmlentities($field['acttext1'],ENT_QUOTES,"ISO-8859-1");
$field['acttext2'] = htmlentities($field['acttext2'],ENT_QUOTES,"ISO-8859-1");


//echo("<P>");
//print_r($field);


//GET STATE FOR GIVEN STATUS
$sql = "SELECT state FROM ".$dbref."_list_status WHERE id = '".$field['status']."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$field['state'] = $row['state'];
mysql_close();

//ADD RECORD TO RES
$sql = "INSERT INTO ".$dbref."_action SET ";
$sql .= "actref = '".$field['actid']."', ";
$sql .= "actassignedto = '".$field['actassignedto']."', ";
$sql .= "actassignedby = '".$field['actassignedby']."', ";
$sql .= "actpriorityid = '".$field['priority']."', ";
$sql .= "actportfolioid = '".$field['portfolio']."', ";
$sql .= "acttext1 = '".$field['acttext1']."', ";
$sql .= "acttext2 = '".$field['acttext2']."', ";
$sql .= "actstatusid = '".$field['status']."', ";
$sql .= "actstate = '".$field['state']."', ";
$sql .= "actdeadline = '".$field['actdeadline']."', ";
$sql .= "actactiondate = '".$field['actactiondate']."', ";
$sql .= "actadddate = '".$today."', ";
$sql .= "actadduser = '".$tkid."' ";



include("inc_db_con.php");
$actid = mysql_insert_id($con);
    //LOG
    $tsql = $sql;
    $trans = "Added new ".$modcallhead." ".$actid;
    include("inc_transaction_log.php");
if(strlen($field['actid'])==0 && $actid > 0)
{
    $sql = "UPDATE ".$dbref."_action SET actref = '".$actid."' WHERE actid = ".$actid;
    include("inc_db_con.php");
}

//ADD RECORD TO LOG
$logupdate = "New ".$modcallhead." added.".chr(10)."Instructions: ".$field['acttext1'];
$sql = "INSERT INTO ".$dbref."_log SET ";
$sql .= "logdate = '".$today."', ";
$sql .= "logacttkid = '".$tkid."', ";
$sql .= "logupdatetkid = '".$tkid."', ";
$sql .= "logupdate = '".$logupdate."', ";
$sql .= "logstatusid = '".$field['status']."', ";
$sql .= "logstate = ".$field['state'].", ";
$sql .= "logactid = '".$actid."', ";
$sql .= "logdeadline = '".$field['actdeadline']."'";
include("inc_db_con.php");
$logid = mysql_insert_id($con);
    //LOG
    $trans = "Added ".$modcallhead." log record ".$logid.".";
    $tsql = $sql;
    include("inc_transaction_log.php");



    $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$field['actassignedto']."'";
    include("inc_db_con.php");
        $actassignedto = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT CONCAT_WS(' - ',d.dirtxt,p.value) AS value FROM ".$dbref."_list_portfolio p, assist_".$cmpcode."_list_dir d WHERE p.deptid = d.dirid AND p.id = ".$field['portfolio'];
    include("inc_db_con.php");
        $portfolio = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT value FROM ".$dbref."_list_priority WHERE id = ".$field['priority'];
    include("inc_db_con.php");
        $priority = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT value FROM ".$dbref."_list_status WHERE id = '".$field['status']."'";
    include("inc_db_con.php");
        $status = mysql_fetch_array($rs);
    mysql_close();

$to = $actassignedto['tkemail'];
if(strlen($to)>0)
{
$subject = "New $modcallheadfull";
$message = "";
$message.= "A new $modcallheadfull (".$field['actid'].") has beeen assigned to you.".chr(10).chr(10);
$message.= "$modcallhead details:".chr(10);
$message.= $headings['actid'].": ".decode($field['actid']).chr(10);
$message.= $headings['actactiondate'].": ".mydate($field['actactiondate'],0).chr(10);
$message.= $headings['portfolio'].": ".$portfolio['value'].chr(10);
$message.= $headings['priority'].": ".decode($priority['value']).chr(10);
if($field['status'] != "ON" && $field['actdeadline'] != "N/A") {
    $message.= $headings['actdeadline'].": ".mydate($field['actdeadline'],0).chr(10);
}
$message.= $headings['acttext1'].": ".decode($field['acttext1']).chr(10);
$message.= $headings['acttext2'].": ".decode($field['acttext2']).chr(10).chr(10);
$message.= "Please login on Ignite Assist to update this $modcal.";

//echo("<P>$subject");
//echo("<P>$message");
$from = "no-reply@ignite4u.co.za";
$header = "From: ".$from."\r\nReply-to: ".$from;
mail($to,$subject,$message,$header);
}


echo("<p>Success!  The new ".$modcallhead." has been added.</p>");
echo("<p>&nbsp;</p>");
echo("<p><img src=/pics/tri_left.gif align=absmiddle > <a href=new.php class=grey>Go back</a></p>");
?>
</body>

</html>
