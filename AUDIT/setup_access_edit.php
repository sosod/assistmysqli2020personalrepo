<?php
include("inc_ignite.php");

include("inc_admin.php");
//GET EDIT VARIABLES
$result = "";
$start = $_GET['s'];
$aid = $_GET['id'];
$type = $_GET['type'];
$deptid = $_GET['deptid'];
$v = $_GET['v'];
$r = $_GET['r'];
$a = $_GET['a'];
$e = $_GET['e'];
$m = $_GET['m'];

if($type == "edit")    //IF EDIT TYPE = EDIT
{
                $didarr = explode("_",$deptid);
                if($didarr[0]=="d" && is_numeric($didarr[1]))
                {
                    $sid = 0;
                    $did = $didarr[1];
                }
                else
                {
                    if($didarr[0]=="s" && is_numeric($didarr[1]))
                    {
                        $sql = "SELECT subdirid FROM assist_".$cmpcode."_list_dirsub WHERE subid = ".$didarr[1];
                        include("inc_db_con.php");
                            $sub = mysql_fetch_array($rs);
                        mysql_close($con);
                        $sid = $didarr[1];
                        $did = $sub['subdirid'];
                    }
                }

    //GET TRANSACTION LOG INFO
    $sql = "SELECT * FROM ".$dbref."_list_users WHERE id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    //PERFORM EDIT UPDATE
    $sql = "UPDATE ".$dbref."_list_users SET v = '".$v."', r = '".$r."', a = '".$a."', p = '".$e."', m = '".$m."', deptid = ".$did.", secid = ".$sid." WHERE id = ".$aid;
    include("inc_db_con.php");
    //SET TRANSACTION LOG VALUES
    $tsql = $sql;
    $told = "UPDATE ".$dbref."_list_users SET v = '".$row['v']."', r = '".$row['r']."', a = '".$row['a']."', p = '".$row['e']."', m = '".$row['m']."', deptid = ".$row['deptid'].", secid = ".$row['secid']." WHERE id = ".$aid;
    $trans = "Updated user ".$aid;
    //PERFORM TRANSACTION LOG UPDATE
    include("inc_transaction_log.php");
    $result = "done";
}
else
{
    if($type == "d")    //IF EDIT TYPE = DELETE
    {
        //UPDATE LIST-ACCESS AND SET USER TO N
        $sql = "UPDATE ".$dbref."_list_users SET yn = 'N' WHERE id = ".$aid;
        include("inc_db_con.php");
            //Set transaction log sql value for delete
            $tsql = $sql;
            $told = "UPDATE ".$dbref."_list_users SET yn = 'Y' WHERE id = ".$aid;;
            $trans = "Removed user access for ".$aid.".";
            include("inc_transaction_log.php");
        $result = "done";
    }
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delUser(id,s,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to delete access to <?php echo($modtitle); ?> for "+tkn+"."))
    {
        document.location.href = "setup_access_edit.php?id="+id+"&s="+s+"&type=d";
    }
}

function Validate(me) {
    var access = me.access.value;
    var deptid = me.deptid.value;
    if(access != "X" && access.length > 0)
    {
        if(deptid != "X" && deptid.length > 0)
        {
            return true;
        }
        else
        {
            alert("Please indicate to which department this user belongs.");
        }
    }
    else
    {
        alert("Please indicate the level of access for the user.");
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: Setup - User Access</b></h1>
<?php
//echo($transaction[1]);
//print_r($taskid);
if($result != "done")   //IF NO ACTION WAS TAKEN THEN DISPLAY EDIT FORM
{
?>
<form name=edit method=get action=setup_access_edit.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=type value=edit><input type=hidden name=id value=<?php echo($aid);?>>
<input type=hidden name=s value=<?php echo($start); ?>>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<th rowspan=2>ID</th>
		<th rowspan=2>User</th>
		<th rowspan=2>Department</th>
		<th colspan=5>Access</th>
	</tr>
	<tr>
		<th >View All</th>
		<th >Receive</th>
		<th >Add</th>
		<th >Report</th>
		<th >Receive&nbsp;Email<br>Updates</th>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT u.*, t.tkname, t.tksurname ";
    $sql .= "FROM ".$dbref."_list_users u ";
    $sql .= ", assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE u.yn = 'Y' AND t.tkid = u.tkid AND u.id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $tkn = $row['tkname']." ".$row['tksurname'];
    mysql_close();
        switch ($row['v'])
        {
            case "Y":
                $v = "<select name=v><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $v = "<select name=v><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $v = "<select name=v><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['r'])
        {
            case "Y":
                $r = "<select name=r><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $r = "<select name=r><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $r = "<select name=r><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['a'])
        {
            case "Y":
                $a = "<select name=a><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $a = "<select name=a><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $a = "<select name=a><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['p'])
        {
            case "Y":
                $e = "<select name=e><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $e = "<select name=e><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $e = "<select name=e><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }
        switch ($row['m'])
        {
            case "Y":
                $m = "<select name=m><option selected value=Y>Yes</option><option value=N>No</option></select>";
                break;
            case "N":
                $m = "<select name=m><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
            default:
                $m = "<select name=m><option value=Y>Yes</option><option selected value=N>No</option></select>";
                break;
        }

?>
        <tr valign=top>
            <td><?php echo($row['tkid']); ?></td>
            <td ><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td ><select name=deptid>
            <?php
                $sql2 = "SELECT * FROM assist_".$cmpcode."_list_dir WHERE diryn = 'Y' ORDER BY dirtxt";
                include("inc_db_con2.php");
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $id = $row2['dirid'];
                        $val = $row2['dirtxt'];
                        echo("<option ");
                        if($row['deptid']==$id && $row['secid']==0) { echo(" selected "); }
                        echo("value=d_".$id.">".$val."</option>");
/*                        $sql3 = "SELECT * FROM assist_".$cmpcode."_list_dirsub WHERE subyn = 'Y' AND subdirid = ".$id." ORDER BY subtxt";
                        include("inc_db_con3.php");
                            while($row3 = mysql_fetch_array($rs3))
                            {
                                $sid = $row3['subid'];
                                $sval = $row3['subtxt'];
                                echo("<option ");
                                if($row['deptid']==$id && $row['secid']==$sid) { echo(" selected "); }
                                echo("value=s_".$sid."> - ".$sval."</option>");
                            }
                        mysql_close($con3);
*/                    }
                mysql_close($con2);
            ?>
        </select>
            <td align=center><?php echo($v); ?></td>
            <td align=center><?php echo($r); ?></td>
            <td align=center><?php echo($a); ?></td>
            <td align=center><?php echo($e); ?></td>
            <td align=center><?php echo($m); ?></td>
        </tr>
<?php
//    mysql_close();
?>

        <tr>
            <td colspan=8 class="tdgeneral"><input type=submit value=" Save "> <input type=button value=Delete onclick="delUser(<?php echo($aid.",".$start) ;?>,'<?php echo(str_replace(" ","_",$tkn)) ;?>')"> <input type=reset></td>
        </tr>
</table>
</form>
<?php
}
?>
<form name=result>
<input type=hidden name=res value=<?php echo($result); ?>>
</form>
<script language=JavaScript>
var res = document.result.res.value;
var s = <?php echo($start); ?>;
//IF ACTION WAS TAKEN (result = done) THEN REDIRECT
if(res == "done")
{
    document.location.href="setup_access.php?s="+s;
}
</script>
</body>

</html>
