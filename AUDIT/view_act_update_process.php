<?php
include("inc_ignite.php");
include("inc_admin.php");


 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: Update</b></h1>
<?php
//GET DATA FROM FORM
$logdate = $today;
$logupdate = $_POST['logupdate'];
$logstatusid = $_POST['logstatusid'];
$logstate = $_POST['logstate'];
$actid = $_POST['actid'];


//REMOVE ' FROM UPDATE
$logupdate = code($logupdate);

//GET TASK DETAILS
$sql = "SELECT * FROM ".$dbref."_action WHERE actid = ".$actid;
include("inc_db_con.php");
$action = mysql_fetch_array($rs);
mysql_close();

//GET STATE FOR GIVEN STATUS IF NOT IP
if($logstatusid != "IP")
{
    $sql = "SELECT state FROM ".$dbref."_list_status WHERE id = '".$logstatusid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $logstate = $row['state'];
    mysql_close();
}
else
{
    if(strlen($logstate)==0 || !is_numeric($logstate))
    {
        $logstate = $action['actstate'];
    }
    if(strlen($logstate)==0 || !is_numeric($logstate))
    {
        $logstate = 0;
    }
}


//UPDATE TA_TASK RECORD
$sql = "UPDATE ".$dbref."_action SET ";
$sql .= "actstatusid = '".$logstatusid."', ";
$sql .= "actstate = ".$logstate." WHERE ";
$sql .= "actid = ".$actid;
$sqltask = $sql;
include("inc_db_con.php");



//ADD RECORD TO TA_LOG
$sql = "INSERT INTO ".$dbref."_log SET ";
$sql .= "logdate = ".$today.", ";
$sql .= "logacttkid = '".$tkid."', ";
$sql .= "logupdatetkid = '".$tkid."', ";
$sql .= "logupdate = '".$logupdate."', ";
$sql .= "logstatusid = '".$logstatusid."', ";
$sql .= "logstate = ".$logstate.", ";
$sql .= "logactid = ".$actid.", ";
$sql .= "logdeadline = ".$action['actdeadline'];
$sqllog = $sql;
include("inc_db_con.php");

//GET LOG ID
$logid = mysql_insert_id();

//CMP LOG UPDATE TA_TASK
$trans = "Updated $tref action record $actid.";
$tsql = $sqltask;
$told = implode("|",$action);
include("inc_transaction_log.php");

//CMP LOG NEW TA_LOG
$tsql = $sqllog;
$trans = "Added $tref log record $logid.";
include("inc_transaction_log.php");



    //Get tasktkid details
    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as tkn FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$action['actassignedto']."'";
    include("inc_db_con.php");
        $actassignedto = mysql_fetch_array($rs);
    mysql_close();
    //Get topic details
    $sql = "SELECT CONCAT_WS(' - ',d.dirtxt,p.value) AS value FROM ".$dbref."_list_portfolio p, assist_".$cmpcode."_list_dir d WHERE p.deptid = d.dirid AND p.id = ".$action['actportfolioid'];
    include("inc_db_con.php");
        $portfolio = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT value FROM ".$dbref."_list_priority WHERE id = ".$action['actpriorityid'];
    include("inc_db_con.php");
        $priority = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT value FROM ".$dbref."_list_status WHERE id = '".$logstatusid."'";
    include("inc_db_con.php");
        $status = mysql_fetch_array($rs);
    mysql_close();
    
$emails = array();
$sql = "SELECT DISTINCT t.tkemail FROM ".$dbref."_list_users u, assist_".$cmpcode."_timekeep t WHERE u.yn = 'Y' AND u.m = 'Y' AND t.tkemail <> '' AND t.tkstatus = 1 AND t.tkid = u.tkid";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $emails[] = $row['tkemail'];
    }
mysql_close($con);
$headings = array();
$sql = "SELECT headfull, headfield FROM ".$dbref."_headings WHERE headyn = 'Y'";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $headings[$row['headfield']] = $row['headfull'];
    }
mysql_close($con);

$to = implode(",",$emails);
//echo("<P>$to");
$subject = "Update to $modcallheadfull";
$message = "";
$message.= "$modcallheadfull ".$action['actref']." has beeen updated by ".$actassignedto['tkn'].".".chr(10).chr(10);
$message.= "$modcallhead details:".chr(10);
$message.= $headings['actid'].": ".decode($action['actref']).chr(10);
$message.= $headings['actactiondate'].": ".mydate($action['actactiondate'],0).chr(10);
$message.= $headings['portfolio'].": ".$portfolio['value'];
$message.= chr(10);
if($logstatusid != "ON") {
    $message.= $headings['actdeadline'].": ".mydate($action['actdeadline'],0).chr(10);
}
$message.= $headings['priority'].": ".decode($priority['value']).chr(10);
$message.= $headings['acttext1'].": ".decode($action['acttext1']).chr(10);
$message.= $headings['acttext2'].": ".decode($action['acttext2']).chr(10).chr(10);
$message.= "Update details:".chr(10);
$message.= $headings['status'].": ".decode($status['value']);
if($logstatusid=="IP") { $message.= " ($logstate%)"; }
$message.= chr(10);
$message.= "Update: ".decode($logupdate).chr(10).chr(10);
$message.= "Please login on Ignite Assist to view all updates to this $modcal.";

//echo("<P>$subject");
//echo("<P>$message");
$from = "no-reply@ignite4u.co.za";
$header = "From: ".$from."\r\nReply-to: ".$from;
mail($to,$subject,$message,$header);

echo("<P>Success!  Update complete.</p>");
echo("<p>&nbsp;</p>");
echo("<p><img src=/pics/tri_left.gif align=absmiddle > <a href=# onclick=\"document.location.href = 'view.php';\" class=grey>Go back</a></p>");
?>

</body>

</html>
