<?php
include("../styles/".$modref.".php");
?>
<style type="text/css">
.blank { background-color: #ffffff; }
.disabled { background-color: #dddddd; }
.tdhover { background-color: #e1e1e1; }
table {
    border-collapse: collapse;
    border: 1px solid #ababab;
}
table td {
    border: 1px solid #ababab;
	color: #000000;
	font-weight: normal;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 10pt;
}
table th {
    border: 1px solid #ffffff;
	color: #ffffff;
	text-align: center;
	background-color: #fe9900;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
.noborder {
    border: 1px solid #ffffff;
}
</style>
		<link type="text/css" href="lib/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="lib/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="lib/jquery-ui-1.7.2.custom.min.js"></script>
<script language=JavaScript>
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
            var pbar = 0;
            var pb2 = 0;
			function incProg(pbar) {
                pbar = parseInt(pbar);
                lbl2.innerText=pbar+"%";
                pb2 = pbar * 2;
                document.getElementById('tbl3').width = pb2;
                document.getElementById('lbl2').innerText = pbar+'%';
			}

			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
$(document).ready(function(){
                $(".date-input").datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                });
});
</script>
