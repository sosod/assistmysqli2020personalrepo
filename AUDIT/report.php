<?php include("inc_ignite.php");
include("inc_admin.php");

function filterDisp($htitle,$filter) {
	echo(chr(10)."    <tr>");
		echo(chr(10)."        <th width=200 style=\"text-align: left;\">$htitle:</th>");
		echo(chr(10)."        <td width=400>$filter</td>");
	echo(chr(10)."    </tr>");
}

function textFilter($fld,$size) {
    $filter = "<input type=text name=".$fld."filter value=\"\" size=$size> ";
    $filter.= "<select name=".$fld."filtertype>";
    $filter.= "<option select value=1>Match exact phrase</option>";
    $filter.= "<option select value=2>Match all words</option>";
    $filter.= "<option select value=3>Match any words</option>";
    $filter.= "</select>";
    return $filter;
}

$heading = getHead('R');
$headings = $heading['s'];
unset($heading['s']);
$newhead['headfield'] = "logdate";
$newhead['headfull'] = "Date of latest update";
$newhead['headtype'] = "L";
$heading[] = $newhead;
$newhead['headfield'] = "duration";
$newhead['headfull'] = "Duration";
$newhead['headtype'] = "L";
$heading[] = $newhead;

$headcount = count($heading);

 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
            function cmDate(me) {
                if(me.value.length>0)
                {
                    var fld = "cmdto";
                    var target = document.getElementById(fld);
                    if(target.value.length == 0)
                    {
                        target.value = me.value;
                    }
                }
            }
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
		body {
          font-size: 62.5%;
        }
		</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($modtitle); ?>: Report</b></h1>
<form name=taskreport method=post action=report_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the details you want in your report:</h3>
<div style="margin-left: 17px">
<table cellpadding="3" cellspacing="0" width=600>
<tr><td valign=top width=300>
<table cellpadding="3" cellspacing="0" class=noborder>
    <?php
    $c = 0;
    $c2 = ceil($headcount/2);
    for($c=0;$c<$c2;$c++)
    {
        $head = array();
        $head = $heading[$c];
        if(count($head)>0)
        {
    ?>
	<tr>
		<td class="noborder" align=center><input type="checkbox" checked name="field[]" <?php echo("value=\"".$head['headfield']."\" id=c".$c); ?>></td>
		<td class="noborder"><?php echo("<label for=c".$c.">".$head['headfull']); ?></label></td>
	</tr>
	<?php
        }
    }
    ?>
</table>
</td><td valign=top width=300>
<table cellpadding="3" cellspacing="0" class=noborder>
<?php
    $c2 = $c;
    for($c=$c2;$c<$headcount;$c++)
    {
        $head = array();
        $head = $heading[$c];
        if(count($head)>0)
        {
    ?>
	<tr>
		<td class="noborder" align=center><input type="checkbox" checked name="field[]" <?php echo("value=\"".$head['headfield']."\" id=c".$c); ?>></td>
		<td class="noborder"><?php echo("<label for=c".$c.">".$head['headfull']); ?></label></td>
	</tr>
	<?php
        }
    }
?>
</table></td></tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table cellspacing="0" cellpadding="3" width=600>
<?php
foreach($heading as $head)
{
    $htype = $head['headtype'];
    $fld = $head['headfield'];
    switch($htype)
    {
        case "L":
            break;
        case "U":
            break;
        default:
            $filter = "";
            $htitle = $head['headfull'];
            switch($fld)
            {
                case "actid":
                    $filter = textFilter($fld,10);;
                    filterDisp($htitle,$filter);
                    break;
                case "actassignedto":
                    $add = array();
                    $sql = "SELECT DISTINCT actassignedto FROM ".$dbref."_action";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $add[] = $row['actassignedto'];
                    }
                    mysql_close($con);
                    $ac = count($add);
                    for($a=0;$a<$ac;$a++)
                    {
                        if(strlen($add[$a])>0)
                        {
                            $add[$a] = "'".$add[$a]."'";
                        }
                        else
                        {
                            unset($add[$a]);
                        }
                    }
                    if(count($add)>0)
                    {
                        $addtkid = implode(",",$add);
                        $filter = "<select size=1 name=".$fld."filter>";
                        $filter.= "<option selected value=ALL>All users</option>";
                        $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_users a WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkid IN ($addtkid) ORDER BY t.tkname, t.tksurname";
                        include("inc_db_con.php");
                        while($row = mysql_fetch_array($rs))
                        {
                            $filter.= "<option value=".$row['tkid'].">".$row['name']."</option>";
                        }
                        mysql_close($con);
                        $filter.="</select>";
                        filterDisp($htitle,$filter);
                    }
                    break;
                case "actassignedby":
                    $add = array();
                    $sql = "SELECT DISTINCT actassignedby FROM ".$dbref."_action";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $add[] = $row['actassignedby'];
                    }
                    mysql_close($con);
                    $ac = count($add);
                    for($a=0;$a<$ac;$a++)
                    {
                        if(strlen($add[$a])>0)
                        {
                            $add[$a] = "'".$add[$a]."'";
                        }
                        else
                        {
                            unset($add[$a]);
                        }
                    }
                    if(count($add)>0)
                    {
                        $addtkid = implode(",",$add);
                        $filter = "<select size=1 name=".$fld."filter>";
                        $filter.= "<option selected value=ALL>All users</option>";
                        $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_users a WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkid IN ($addtkid) ORDER BY t.tkname, t.tksurname";
                        include("inc_db_con.php");
                        while($row = mysql_fetch_array($rs))
                        {
                            $filter.= "<option value=".$row['tkid'].">".$row['name']."</option>";
                        }
                        mysql_close($con);
                        $filter.="</select>";
                        filterDisp($htitle,$filter);
                    }
                    break;
                case "portfolio":
                    $filter = "<select size=1 name=".$fld."filter>";
                    $filter.= "<option selected value=ALL>All portfolios</option>";
                    $sql = "SELECT DISTINCT p.id, CONCAT_WS(' - ',d.dirtxt,p.value) as portval ";
                    $sql.= " FROM ".$dbref."_list_portfolio p, assist_".$cmpcode."_list_dir d, ".$dbref."_action a ";
                    $sql.= " WHERE p.id = a.actportfolioid AND p.deptid = d.dirid ";
                    $sql.= " ORDER BY d.dirsort, p.value";
                    include("inc_db_con.php");
                        while($row = mysql_Fetch_array($rs))
                        {
                            $filter.="<option value=".$row['id'].">".$row['portval']."</option>";
                        }
                    mysql_close($con);
                    $filter.=" </select>";
                    filterDisp($htitle,$filter);
                    break;
                case "priority":
                    $filter = "<select size=1 name=".$fld."filter>";
                    $filter.= "<option selected value=ALL>All</option>";
                    $sql = "SELECT DISTINCT p.id, p.value portval ";
                    $sql.= " FROM ".$dbref."_list_priority p, ".$dbref."_action a ";
                    $sql.= " WHERE p.id = a.actpriorityid ";
                    $sql.= " ORDER BY p.value";
                    include("inc_db_con.php");
                        while($row = mysql_Fetch_array($rs))
                        {
                            $filter.="<option value=".$row['id'].">".$row['portval']."</option>";
                        }
                    mysql_close($con);
                    $filter.=" </select>";
                    filterDisp($htitle,$filter);
                    break;
                case "status":
                    $filter = "<select size=1 name=".$fld."filter>";
                    $filter.= "<option selected value=1>Incomplete & ongoing</option>";
                    $filter.= "<option value=2>Incomplete only</option>";
                    $filter.= "<option value=3>Ongoing only</option>";
                    $filter.= "<option value=4>Completed only</option>";
                    $filter.= "<option value=5>All resolutions</option>";
                    $filter.= "</select>";
                    filterDisp($htitle,$filter);
                    break;
                case "actactiondate":
                    $filter = "From <input type=text name=actactiondatefilter[] id=cmdfrom class=date-input readonly=readonly size=10> to <input type=text name=actactiondatefilter[] id=cmdto class=date-input readonly=readonly size=10>";
                    filterDisp($htitle,$filter);
                    break;
                case "acttext1":
                    $filter = textFilter($fld,40);;
                    filterDisp($htitle,$filter);
                    break;
                case "acttext2":
                    $filter = textFilter($fld,40);;
                    filterDisp($htitle,$filter);
                    break;
                default:
                    break;
            }
            break;
    }
}
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '$tref' ORDER BY udfisort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
?>
	<tr>
		<th><?php echo($row['udfivalue']); ?>:</th>
		<td>
        <?php
            switch($row['udfilist'])
            {
                case "Y":
                    echo("<select size=1 name=\"udf".$row['udfiid']."filter\">");
                    echo("<option selected value=ALL>All</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                        }
                    mysql_close($rs2);
                    echo("</select>");
                    break;
                default:
                    echo("<input type=text name=\"udf".$row['udfiid']."filter\">");
                    break;
            }
        ?>
        </td>
	</tr>
<?php
        }
    mysql_close();
?>
</table>
</div>
<h3 class=fc>3. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" class=noborder>
	<tr>
		<td class=noborder align=center><input type="radio" name="csvfile" value="N" checked id=disp></td>
		<td class=noborder><label for=disp>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=noborder align=center><input type="radio" name="csvfile" value="Y" id=csv></td>
		<td class=noborder ><label for=csv>Save to file (Microsoft Excel file)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>


</body>

</html>
