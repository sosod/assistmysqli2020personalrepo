<?php 
//error_reporting(-1);
require_once ("../../module/autoloader.php");
$ah = new ASSIST_MODULE_HELPER();
$cmpcode = $ah->getCmpCode();//$_SESSION['cc'];
$dbref = $ah->getDBRef();//$_SESSION['dbref'];
//require_once("../../inc_db.php");
//require_once("../../inc_db_conn.php");
//require_once("../../inc_assist.php");

$data = $_REQUEST;
//echo "abc<br>";
$docObj = new DOC($data['modref']);
$form = $docObj->getDocForm($data);
$form1 = str_replace("\r\n",'',$form);
$form2 = str_replace("\t",'',$form1);
echo json_encode($form2,JSON_UNESCAPED_SLASHES);//JSON_UNESCAPED_SLASHES flag to prevent backslashes that break the page
//echo ($form);
//echo "<br>";
return;
$id = isset($_REQUEST['d']) ? $_REQUEST['d'] : 0;
if(strlen($id)==0 || !is_numeric($id) || $id <=0) {
	$data = array('id'=>$id,'result'=>false);
} else {
	if(isset($_REQUEST['DOC']))	{
		$docResult = $docObj->deleteActionDoc($id,$_REQUEST['modref']);
		ASSIST_HELPER::arrPrint($docResult);
	}else{
		$doc = $ah->mysql_fetch_one("SELECT * FROM ".$dbref."_task_attachments WHERE id = ".$id);
	
		//update task_attachment record
		$mar = $ah->db_update("UPDATE ".$dbref."_task_attachments SET file_location = 'deleted' WHERE id = ".$id);
		if($mar>0) {
			//move document
			$ah->checkFolder("deleted");
			$old = "../../files/$cmpcode/".$_SESSION['modref']."/".(strlen($doc['file_location'])>0 ? $doc['file_location']."/" : "").$doc['system_filename'];
			$new = "../../files/$cmpcode/deleted/".$_SESSION['modref']."_".date("YmdHis")."_".$doc['system_filename'];
			if(file_exists($old)) {
				copy($old,$new);
				if(file_exists($new)) {
					unlink($old);
				}
			}
			//update log
			$task = $ah->mysql_fetch_one("SELECT * FROM ".$dbref."_task WHERE taskid = ".$doc['taskid']);
			$logdate = time();
			$sql = "INSERT INTO ".$dbref."_log 
					(logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype) 
					VALUES 
					(null, $logdate, '".$_SESSION['tid']."', 'Deleted attachment: ".$doc['original_filename']."', ".$task['taskstatusid'].", ".$task['taskstate'].", $logdate, 'N', '".$_SESSION['tid']."', ".$doc['taskid'].", '', 'E')";
			$ah->db_insert($sql);
	
			$data = array('id'=>$id,'result'=>true);
		} else {
			$data = array('id'=>$id,'result'=>false);
		}
	}
}
echo json_encode($data);

  
/*
function getList($var) {
	global $dbref;
	global $cmpcode;

	switch($var['field']) {
	case "taskadduser":
		$sql = "SELECT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as value
				FROM assist_".$cmpcode."_timekeep tk
				INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
				  ON tk.tkid = mmu.usrtkid
				  AND mmu.usrmodref = '".$_SESSION['modref']."'
				INNER JOIN ".$dbref."_list_access a
				  ON a.tkid = tk.tkid
				  AND (a.act > 20 
				    OR a.tkid = '".$_SESSION['tid']."'
				  )
				WHERE tk.tkstatus = 1";
		break;
	case "tasktopicid":
		$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
		break;
	case "taskurgencyid":
		$sql = "SELECT * FROM ".$dbref."_list_urgency WHERE yn = 'Y' ORDER BY value";
		break;
	}
	$data = mysql_fetch_all($sql);
	return $data;
}  


*/
  ?>