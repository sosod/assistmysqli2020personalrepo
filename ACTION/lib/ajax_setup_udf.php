<?php
require_once ("../../module/autoloader.php");
$ah = new ASSIST_MODULE_HELPER();
$cmpcode = $ah->getCmpCode();//$_SESSION['cc'];
$dbref = $ah->getDBRef();//$_SESSION['dbref'];
//require_once("../../inc_db.php");
//require_once("../../inc_db_conn.php");
require_once("../lib/setup_udf_log.php");


$headings = array(
	'udfivalue' => array(
		'head'	=> "Field Name",
		'type'	=> "text",
		'list'	=> array(),
	),
	'udfilist' => array(
		'head'	=> "Field Type",
		'type'	=> "list",
		'list'	=> array(
			'Y'	=> "List",
			'T'	=> "Small Text",
			'M'	=> "Large Text",
			'N'	=> "Number",
			'D'	=> "Date",
		),
	),
	'udfidefault' => array(
		'head'	=> "Default Value",
		'type'	=> "text",
		'list'	=> array(),
	),
	'udfirequired' => array(
		'head'	=> "Required",
		'type'	=> "list",
		'list'	=> array(
			0	=> "No",
			1	=> "Yes",
		),
	),
	'udfiobject' => array(
		'head'	=> "Link Object",
		'type'	=> "list",
		'list'	=> array(
			'action'	=> isset($_SESSION['actname']) ? $_SESSION['actname'] : "Action",
			'update'	=> "Update",
		),
	),
	'udfilinkfield' => array(
		'head'	=> "Link List",
		'type'	=> "list",
		'list'	=> array(
			''				=> "N/A",
			'tasktopicid'	=> "Topic",
			'taskstatusid'	=> "Status",
		),
	),
	'udfilinkref' => array(
		'head'	=> "Link List Item",
		'type'	=> "db",
		'list'	=> array(),
	),
);

$data = array();
switch($_REQUEST['act']) {
case "getList":
	$tbl = $_REQUEST['t'];
	$data = getList($tbl);
	break;
case "getUDF":
	$id = $_REQUEST['i'];
	$data = getUDF($id);
	break;
case "ADD":
	$data = addUDF($_REQUEST);
	break;
case "EDIT":
	$data = editUDF($_REQUEST);
	break;
case "DELETE":
	$data = delUDF($_REQUEST);
	break;
}
//echo "abc";
//$data = array('id'=>0,'value'=>"TEST");
echo json_encode($data);


  
  
  
function getList($tbl) {
	global $dbref,$ah;
	
	switch($tbl) {
	case "tasktopicid":
		$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn ='Y' ORDER BY value";
		break;
	case "taskstatusid":
		$sql = "SELECT pkey as id, value, yn FROM ".$dbref."_list_status WHERE yn ='Y' AND id NOT IN ('CN','NW') ORDER BY sort, value";
		break;
	}
	//$result = getRS($sql);
	$rows = $ah->mysql_fetch_all($sql);
	$data = array();
	//while($array = mysql_fetch_assoc($result)) {
	foreach($rows as $array){
		$data[] = $array;
	}

	return $data;

}
  

function getUDF($id) {
	global $ah;
	$cmpcode = $ah->getCmpCode();
	$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$id;
	$data = $ah->mysql_fetch_one($sql);
	$data['udfivalue'] = $ah->decode($data['udfivalue']);
	$data['udfidefault'] = $ah->decode($data['udfidefault']);
	return $data;

}  

  
function addUDF($var) {
	global $ah;
	$cmpcode = $ah->getCmpCode();
	$udfiref = strtoupper($ah->getModRef());
	$udficustom = "Y";
	$udfisort = 9999;
	$udfiyn = 'Y';
	
	$sql = "INSERT INTO assist_".$cmpcode."_udfindex 
			(udfiid,udfivalue,udfilist,udfiref,udficustom,udfisort,udfiyn,udfirequired,udfidefault,udfiobject,udfilinkfield,udfilinkref) 
		   VALUES 
		   (null,'".$ah->code($var['udfivalue'])."','".$var['udfilist']."','".$udfiref."','".$udficustom."','".$udfisort."','".$udfiyn."','".$var['udfirequired']."','".$ah->code($var['udfidefault'])."','".$var['udfiobject']."','".$var['udfilinkfield']."','".$var['udfilinkref']."')";
	$id = $ah->db_insert($sql);
	$var['udfivalue'] = $ah->code($var['udfivalue']);
	$var['udfidefault'] = $ah->code($var['udfidefault']);
	$var['udfiref'] = $udfiref;
	$var['udficustom'] = $udficustom;
	$var['udfisort'] = $udfisort;
	$var['udfiyn'] = $udfiyn;
	$flds = array_keys($var);
	logMe($id,serialize($flds),"Added new UDF \'".$var['udfivalue']."\' (Ref: ".$id.").",serialize(array()),serialize($var),"MAIN","C",$sql,"udf");
	
	$data = array('id'=>$id,'value'=>$var['udfivalue']);
	
	return $data;
}
  
   
function editUDF($var) {
	global $headings,$ah;
	$cmpcode = $_SESSION['cc'];
	$udfiid = $var['udfiid'];
	if(is_numeric($udfiid) && strlen($udfiid)>0 && $udfiid>0) {	
		$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$udfiid;
		$old = $ah->mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET
				udfivalue = '".$ah->code($var['udfivalue'])."',
				udfilist = '".$var['udfilist']."',
				udfirequired = '".$var['udfirequired']."',
				udfidefault = '".$ah->code($var['udfidefault'])."',
				udfiobject = '".$var['udfiobject']."',
				udfilinkfield = '".$var['udfilinkfield']."',
				udfilinkref = '".$var['udfilinkref']."'
				WHERE udfiid = ".$udfiid; 
		$mar = $ah->db_update($sql);
		if($mar>0) {
			$var['udfivalue'] = $ah->code($var['udfivalue']);
			$var['udfidefault'] = $ah->code($var['udfidefault']);
			$trans = "Edited UDF \'".$var['udfivalue']."\' (Ref: ".$udfiid."):";
			foreach($var as $f => $v) {
				if(isset($headings[$f]) && $v != $old[$f]) {
					$trans.="<br />- Updated ".$headings[$f]['head'];
					if($headings[$f]['type']=="text") {
						$trans.= " to \'$v\' from \'".$old[$f]."\'.";
					} elseif($f=="udfilinkref") {
						$linkref = getLinkItem($var['udfilinkfield'],$v,$old['udfilinkfield'],$old[$f]);
						$trans.= " to \'".$linkref['new']."\' from \'".$linkref['old']."\'.";
					} else {
						$trans.= " to \'".$headings[$f]['list'][$v]."\' from \'".$headings[$f]['list'][$old[$f]]."\'.";
					}
				}
			}
			logMe($udfiid,serialize(array_keys($var)),$trans,serialize($old),serialize($var),"MAIN","E",$sql,"udf");
		}
		$data = array('mar'=>$mar,'id'=>$udfiid,'value'=>$var['udfivalue']);
	} else {
		$data = array('mar'=>-1,'id'=>$udfiid,'value'=>$var['udfivalue']);
	}
	return $data;
}
  
  
function delUDF($var) {
	global $ah;
	$cmpcode = $ah->getCmpCode();
	$udfiid = $var['i'];
	if(is_numeric($udfiid) && strlen($udfiid)>0 && $udfiid>0) {	
		$sql = "SELECT udfivalue FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$udfiid;
		$row = $ah->mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET
				udfiyn = 'N'
				WHERE udfiid = ".$udfiid; 
		$mar = $ah->db_update($sql);
		if($mar>0) {
			$trans = "Deleted UDF \'".$row['udfivalue']."\' (Ref: ".$udfiid.").";
			logMe($udfiid,"udfiyn",$trans,"Y","N","MAIN","D",$sql,"udf");
		}
		$data = array('mar'=>$mar,'id'=>$udfiid,'value'=>$ah->decode($row['udfivalue']));
	} else {
		$data = array('mar'=>-1);
	}
	return $data;
}
  
  
function getLinkItem($n_f,$n,$o_f,$o) {
	global $ah;
	$cmpcode = strtolower($ah->getCmpCode());
	$udfiref = $_SESSION['modref'];
	$dbref = "assist_".$cmpcode."_".strtolower($udfiref);
	$data = array('old'=>"N/A",'new'=>"N/A");
	$field = $n_f;
	$ref = $n;
	$fld = "new";
	if($ref>0) {
		switch($field) {
		case "tasktopicid":
			$sql = "SELECT value FROM ".$dbref."_list_topic WHERE id = ".$ref;
			$row = $ah->mysql_fetch_one($sql);
			$data[$fld] = $row['value'];
			break;
		case "taskstatusid":
			$sql = "SELECT value FROM ".$dbref."_list_status WHERE pkey = ".$ref;
			$row = $ah->mysql_fetch_one($sql);
			$data[$fld] = $row['value'];
			break;
		default:
			break;
		}
	}
		
	$field = $o_f;
	$ref = $o;
	$fld = "old";
	if($ref>0) {
		switch($field) {
		case "tasktopicid":
			$sql = "SELECT value FROM ".$dbref."_list_topic WHERE id = ".$ref;
			$row = $ah->mysql_fetch_one($sql);
			$data[$fld] = $row['value'];
			break;
		case "taskstatusid":
			$sql = "SELECT value FROM ".$dbref."_list_status WHERE pkey = ".$ref;
			$row = $ah->mysql_fetch_one($sql);
			$data[$fld] = $row['value'];
			break;
		default:
			break;
		}
	}

	return $data;
}
  
  
  
  
  ?>