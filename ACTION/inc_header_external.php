<?php 
// require_once "inc_ignite.php"; 
//
require_once ("../module/autoloader.php");
$actname = isset($_SESSION['actionmenu']['TASK']) ? ucwords(strtolower($_SESSION['actionmenu']['TASK']['modlocation'])) : "Action";
//echo $actname;
$ah = new ASSIST_HELPER();
$ah2 = new ASSIST_MODULE_HELPER();
$modref = $modloc;
$cmpcode = $ah2->getCmpCode();
$adb = new ASSIST_DB("client");
$tkid = $ah2->getUserID(); 
require_once "inc_external.php";
require_once 'inc_ta_external.php';
//echo "Passed assist DB";

//UDF DEFAULTS
$udf_options = array(
	'udfilist' => array(
		'Y' => array('txt'=>"List",'type'=>"select",'align'=>"left"),
		'T' => array('txt'=>"Small Text",'type'=>"text",'align'=>"left"),
		'M' => array('txt'=>"Large Text",'type'=>"textarea",'align'=>"left"),
		'N' => array('txt'=>"Number",'type'=>"number",'align'=>"right"),
		'D' => array('txt'=>"Date",'type'=>"date",'align'=>"center"),
		'CUSTOMER' => array('txt'=>"Customer",'type'=>"select",'align'=>"left"),
		'SUPPLIER' => array('txt'=>"Supplier",'type'=>"select",'align'=>"left"),
	),
	'udfirequired' => array(
		0 => "No",
		1 => "Yes",
	),
	'udfiobject' => array(
		'action' => $actname,
		'update' => "Update",
	),
	'udfilinkfield' => array(
		'all'		=> array("tasktopicid","taskstatusid"),
		'action'	=> array("tasktopicid"),
		'update'	=> array("tasktopicid","taskstatusid"),
		'headings'	=> array(),
		'tables'	=> array(
			'tasktopicid'	=> array(
				'tbl'		=> "list_topic",
				'id'			=> "pkey",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
			'taskstatusid'	=> array(
				'tbl'		=> "list_status",
				'id'			=> "id",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
		),
	),
);
if(isset($get_udf_link_headings) && $get_udf_link_headings) {
	$sql = "SELECT headingshort as display, field FROM ".$dbref."_list_display WHERE yn = 'Y' AND field IN ('".implode("','",$udf_options['udfilinkfield']['all'])."')";
		$udf_options['udfilinkfield']['headings'] = mysql_fetch_all_fld($sql,"field");
}
//GET VALID UDFS
$udf_index = array('action'=>array('ids'=>array(),'index'=>array()),'update'=>array('ids'=>array(),'index'=>array()));
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y'";
//echo $sql;
$rs = $adb->mysql_fetch_all($sql);
foreach($rs as $row) {
	$obj = strlen($row['udfiobject'])>0 ? $row['udfiobject'] : "action";
	$udf_index[$obj]['ids'][] = $row['udfiid'];
	$udf_index[$obj]['index'][$row['udfiid']] = $row;
}
//ASSIST_HELPER::arrPrint($rs);
unset($rs);


$headings = array();
$headings['action'] = array(
	'taskadddate'	=> "Created On",
	'taskadduser'	=> "Assigned By",
	'tasktkid'		=> "Assigned To",
	'tasktopicid'	=> "Topic",
	'taskurgencyid'	=> "Priority",
	'taskstatusid'	=> "Status",
	'taskdeadline'	=> "Deadline",
	'taskaction'	=> ucfirst($actname)." Instructions",
	'taskdeliver'	=> ucfirst($actname)." Deliverables",
	'taskattach'	=> "Attachment Details"
);
$headings['update'] = array(
	'logdate'		=> "Date logged",
	'logtkid'		=> "Logged By",
	'logupdate'		=> "Message",
	'logstatusid'	=> "Status",
	'logactdate'	=> "Date of Activity",
	'logattach'		=> "Attachment details",
);

if(!isset($page) || implode("_",$page)!="report_process") {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Action4u.co.za</title>
</head>
		<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>
		<link href="/library/jquery-ui-1.8.24/css/jquery-ui.css" rel="stylesheet" type="text/css" />

		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui-timepicker-addon-0.9.8.js"></script>		
		<link href="/library/jquery/css/jquery-timepicker.css" rel="stylesheet" type="text/css" />

		<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
		<script type ="text/javascript" src="/library/js/assiststring.js"></script>
		<script type ="text/javascript" src="/library/js/assistarray.js"></script>
		<script type ="text/javascript" src="/library/js/assistform.js"></script>
		<script type ="text/javascript" src="/assist.js"></script>

		<link rel="stylesheet" href="/assist.css" type="text/css" />
<?php 
	include("inc_css.php"); 
	include("inc_js.php"); 
?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php

if(!isset($page)) {
	$page = explode("/",$_SERVER['PHP_SELF']);
	$page = $page[count($page)-1];
	if(strpos($page,".php")!==false) {
		$page = substr($page,0,-4);
	}
	$page = explode("_",$page);
}
$available_menu = array('setup');

if(in_array($page[0],$available_menu)) {
	$menu = array();
	switch($page[0]) {
	case "setup":
		$menu['defaults'] = array(
			'id'=>"defaults",
			'url'=>"setup.php",
			'active'=>(!isset($page[1]) || $page[1]!="access" ? true : false),
			'display'=>"Defaults"
		);
		$menu['user'] = array(
			'id'=>"user",
			'url'=>"setup_access.php",
			'active'=>(isset($page[1]) && $page[1]=="access" ? true : false),
			'display'=>"User Access"
		);
		break;
	}
	$level = 1;
	echoNavigation($level,$menu);
}	//end if in available menu

if(isset($title)) {
	echo "<h1 id=h_title>";
	$new_title = array();
	foreach($title as $t) {
		$nt = "";
		if(isset($t['url']) && strlen($t['url'])>0) {
			$nt.="<a href='".$t['url']."' class=breadcrumb>";
		}
		$nt.=$t['txt']."</a>";
		$new_title[] = $nt;
	}
	echo implode(" >> ",$new_title);
	echo "</h1>";
}

}	//end not if report_process
?>