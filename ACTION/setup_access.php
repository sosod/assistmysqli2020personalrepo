<?php
//include("inc_ignite.php");
$page = array("setup","access");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"User Access"),
);

require_once "inc_header.php";


include("inc_ta.php");

$result = "<p>&nbsp;</p>";
//GET VARIABLES PASSED BY FORM
if(isset($_REQUEST['atkid']) && isset($_REQUEST['access'])) {
	$atkid = $_POST['atkid'];
	$access = $_POST['access'];
	//IF THE VARIABLES WERE PASSED CORRECTLY THEN PERFORM UPDATE
	if($atkid != "X" && $access != "X" && strlen($access) > 0)
	{
		//CHECK IF NEW USER EXISTS IN ACCESS TABLE ALREADY
		$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$atkid."' AND yn = 'Y'";
		//include("inc_db_con.php");
		$trow = $ah->db_get_num_rows($sql);
		if($trow > 0)
		{
			//IF THE USER WAS FOUND IN THE ACCESS TABLE - ERROR
			$err = "Y";
			$result = "<p>Error - the requested action could not be completed as the user already exists in ".ucfirst($actname)." Assist.</p>";
			//SET TRANSACTION LOG VALUES
			$row = $ah->mysql_fetch_one($sql);
			$tsql = implode("|",$row);
			$trans = "Error - User ".$atkid." could not be added to $modref as user already had access.";
		}
		else
		{
			//ELSE SET VARIABLE TO CONTINUE
			$err = "N";
			$result = "<p>Success!  User added to ".ucfirst($actname)." Assist.</p>";
		}
		//mysql_close();
		//IF RECORD WAS NOT FOUND THEN PERFORM UPDATE
		if($err == "N")
		{
			$sql = "INSERT INTO ".$dbref."_list_access SET tkid = '".$atkid."', view = ".$access.", act = ".$access.", yn = 'Y'";
			//include("inc_db_con.php");
			$ah->db_insert($sql);
			//SET TRANSACTION LOG VALUES FOR UPDATE
			$tsql = $sql;
			$trans = "User ".$atkid." added to $modref.";
		}
		//PERFORM TRANSACTION LOG UPDATE
		$tref = "$modref";
		//include("inc_transaction_log.php");
	}
}
?>
<script language=JavaScript>
function editUser(id) {
    document.location.href="setup_access_edit.php?id="+id;
}
</script>
<?php echo($result); ?>
<table>
	<tr>
		<th>ID</th>
		<th>User</th>
		<th>Access</th>
		<th>&nbsp;</th>
	</tr>
<?php
    //GET SETUP ADMIN DETAILS AND DISPLAY
    $sql = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$taadmin."'";
    $row = $ah->mysql_fetch_one($sql);
	if(isset($row['tkname'])) {
		$taadminname = $row['tkname']." ".$row['tksurname'];
?>
		<tr>
			<td><?php echo($taadmin); ?></td>
			<td><?php echo($taadminname); ?></td>
			<td colspan=2>Task Assist Administrator</td>
		</tr>
<?php
	}
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.act, r.ruletext, a.id ";
    $sql .= "FROM ".$dbref."_list_access a
		INNER JOIN ".$dbref."_list_access_rules r
			ON r.ruleint = a.act AND r.rulefn = 'act' AND r.ruleint < 100
		INNER JOIN assist_".$cmpcode."_timekeep t
			ON t.tkid = a.tkid AND t.tkstatus = 1
		INNER JOIN assist_".$cmpcode."_menu_modules_users mmu 
			ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'";
    $sql .= "WHERE yn = 'Y'
			ORDER BY t.tkname, t.tksurname, a.id ASC";
    //$rs = getRS($sql);
    //while($row = mysql_fetch_array($rs)) {
	$users = $ah->mysql_fetch_all_fld($sql,"tkid");
	$got = array_keys($users); $got[] = $taadmin;
	foreach($users as $row) {
        echo "	<tr>
					<td>".$row['tkid']."</td>
					<td>".$row['tkname']." ".$row['tksurname']."</td>
					<td>".$row['ruletext']."</td>
					<td class=center><input type=button value=Edit onclick='editUser(".$row['id'].")'></td>
				</tr>";
    }

//IF A USER HAS ACCESS TO TA BUT IS NOT IN THE ABOVE RESULT THEN DISPLAY THE ADD FORM
$sql = "SELECT t.tkid, t.tkname, t.tksurname
		FROM assist_".$cmpcode."_timekeep t
		, assist_".$cmpcode."_menu_modules_users m 
		WHERE t.tkid = m.usrtkid 
		AND usrmodref = '$modref' "
		.(count($got)>0 ? "AND t.tkid NOT IN (".implode(",",$got).")" : "");
$new = $ah->mysql_fetch_all($sql);
if(count($new) > 0) {
?>
<form method=POST action=setup_access.php>
	<tr>
		<td>&nbsp;</td>
		<td><select name=atkid>
                <option selected value=X>--- SELECT ---</option>
                <?php
                    foreach($new as $row) {
                        echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                    }
                ?>
            </select>
        </td>
		<td>Add/Update/View&nbsp;
            <select name=access>
                <option selected value=X>--- SELECT ---</option>
                <option value=20>own <?php echo ucfirst($actname);?>s only</option>
                <option value=90>all <?php echo ucfirst($actname);?>s</option>
            </select>
        </td>
		<td><input type=submit value=Add class=isubmit /></td>
	</tr>
</form>
<?php
}
?>
</table>
</body>

</html>
