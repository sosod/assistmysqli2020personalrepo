<?php

class ACTION extends ASSIST_MODULE_HELPER {

	const OBJECT_NAME = "action";
	protected $dbref;
	protected $cmpcode;
	protected $modref;
	
	public function __construct() {
		parent::__construct();
		$this->cmpcode = $_SESSION['cc'];
		$this->dbref = $_SESSION['dbref'];	
		$this->modref = $_SESSION['modref'];	
	}
	public function getEmptyTableNotice() {
		return "No ".self::OBJECT_NAME."s found to display.";
	}
	
	public function getAction($id){
		$task = $this->mysql_fetch_one("SELECT * FROM ".$this->dbref."_task WHERE taskid = '$id'");
		if(isset($task['taskid'])){
			return $task;
		}else{
			return array("error","Couldn't get task",$task);
		}
	}

	public function setActionModRef($mr="") {
		if(strlen($mr)==0) {
			$mr = $this->modref;
		}
		$_SESSION['modref'] = $mr;
		$this->setDBRef($mr);
	}
}



?>