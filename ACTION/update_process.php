<?php
error_reporting(-1);
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$get_udf_link_headings = false;

$redirect_me = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";

require_once 'inc_header.php';

echo "<p>Starting update...";
?>
<script type="text/javascript" >
	$(function() {
		AssistHelper.processing();
	});
</script>
<?php
//arrPrint($_REQUEST);
$logdate = $ah->getToday();
$logtkid = $tkid;
$logupdate = $ah->code($_REQUEST['logupdate']);
$logstatusid = $_REQUEST['logstatusid'];
$logstate = $_REQUEST['logstatusid']==1 ? 100 : $_REQUEST['logstate'];
$logactdate = strtotime($_REQUEST['logactdate']);
$logemail = "Y";
$logsubmittkid = $tkid;
$logtaskid = $_REQUEST['logtaskid'];
$logtasktkid = "";
$logtype = "U";
echo "<p>1. Updating log.</p>";
$sql = "INSERT INTO ".$dbref."_log
		(logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype)
		VALUES
		(null, $logdate, '$logtkid', '$logupdate', $logstatusid, $logstate, $logactdate, '$logemail', '$logsubmittkid', $logtaskid, '$logtasktkid', '$logtype')";
//echo "<P>".$sql;
$logid = $ah->db_insert($sql);
//echo "<P>".$logid;
$sql = "UPDATE ".$dbref."_task SET taskstatusid = $logstatusid , taskstate = $logstate WHERE taskid = $logtaskid ";
$ah->db_update($sql);

//echo "<h3>udfs</h3>";
echo "<p>2. Updating User Defined Fields.</p>";
$udf_types = isset($_REQUEST['udf_types']) ? $_REQUEST['udf_types'] : "";
$udftypes = unserialize($udf_types);
$udf_email = array();
$udf = isset($_REQUEST['udf']) ? $_REQUEST['udf'] : array();
foreach($udf as $key => $r) {
	$udfs = array();
	if(strlen($r)>0 && (!in_array($key,$udftypes['Y']) || $ah->checkIntRef($r))) {
		if(in_array($key,$udftypes['D'])) { $r = strtotime($r); }
		$udfs[$key] = "(null,$key,'".$ah->code($r)."',$logid,'".$_SESSION['modref']."')";
	}
	if(count($udfs)>0) {
		$sql = "INSERT INTO assist_".$cmpcode."_udf VALUES ".implode(",",$udfs);
		$ah->db_insert($sql);
		//echo "<P>".$sql;
	}
	if(strlen($r)>0) {	$udf_email[$key] = $r; }
}



echo "<P>3. Uploading attachments.</p>";
//SSIST_HELPER::arrPrint($_FILES);
$modRefs = $tempReq = $_REQUEST;
$metaFields = array();

for($x=0; $x<count($_REQUEST);$x++){
	if(stripos(key($_REQUEST),"docModRef")===false){
		unset($modRefs[key($_REQUEST)]);
	}else{
		if(current($_REQUEST)!=="DefaultMod"){
    		$metaFields[] = array_slice($tempReq,$x+1,7,true);
		}else{
			$metaFields[] = array();
		}
	}
	next($_REQUEST);
}

foreach($metaFields as $indx=>$arr){
	foreach($arr as $k=>$vl){
		$tmp = explode("->",$k);
		unset($arr[$k]);
		$arr[$tmp[0]]=$vl;
	}
	$metaFields[$indx]=$arr;
}
//SSIST_HELPER::arrPrint(array("metafields"));
//SSIST_HELPER::arrPrint($metaFields);
//SSIST_HELPER::arrPrint(array("modrefs"));
//SSIST_HELPER::arrPrint($modRefs);
//SSIST_HELPER::arrPrint(array("task instruction test"));
$attachments = "";
$attach_count = 0;
$taskaction = "";
if(isset($_FILES)) {
	$cntr = 0;
    foreach($modRefs as $val){
    	if($val == "DefaultMod"){
        	$folder = "../files/$cmpcode/".$_SESSION['modref']."/update";
			$ah->checkFolder($_SESSION['modref']."/update");
            if($_FILES['attachments']['error'][$cntr] == 0) {
                $original_filename = $ah->code($_FILES['attachments']['name'][$cntr]);
                $ext = $ah->code(substr($original_filename, strrpos($original_filename, '.') + 1));
                $sql = "INSERT INTO ".$dbref."_task_attachments (taskid,logid,original_filename,system_filename,file_location) VALUES ($logtaskid,$logid,'$original_filename','','update')";
                $docid = $ah->db_insert($sql);
				$system_filename = $logtaskid."_".$docid."_".date("YmdHis").".$ext";
				$full_path = $folder."/".$system_filename;
				move_uploaded_file($_FILES['attachments']['tmp_name'][$cntr], $full_path);
				$ah->db_update("UPDATE ".$dbref."_task_attachments SET system_filename = '$system_filename' WHERE id = $docid");
				$attachments.= $original_filename."<br />";
            }else{
            	//error
            }
    	}else{
            if($_FILES['attachments']['error'][$cntr] == 0) {
        		$doc = new DOC($val);
				if(strlen($taskaction)<=0){
					$sql = "SELECT taskaction FROM ".$dbref."_task WHERE taskid = '".$logtaskid."'";
					$taskaction = $doc->mysql_fetch_one_value($sql,"taskaction");
					////SSIST_HELPER::arrPrint($taskaction);
				}
        		$modTexts = $doc->getDocInstancesForSelect();
				$result[$cntr] = $doc->addActionDocWithMeta($actname, $taskaction, $logid, $_FILES['attachments']['name'][$cntr], $_FILES['attachments']['tmp_name'][$cntr],$metaFields[$cntr],true);
				if($result[$cntr][0]=="ok"){
					$attachments.= $_FILES['attachments']['name'][$cntr]." (Stored in ".$modTexts[$val].")<br />";
				}else{
					//error uploading doc
				}
            }else{
            	//error
            }
    	}
    	$cntr ++;
		$attach_count ++;
    }
}


$task = getTask($logtaskid);


/*
$sql = "SELECT tasktkid as id, tkemail, CONCAT_WS(' ',tkname,tksurname) as name
		FROM ".$dbref."_task_recipients
		INNER JOIN assist_".$cmpcode."_timekeep ON tkid = tasktkid AND tkstatus = 1 AND tkemail <>  ''
		WHERE taskid = ".$logtaskid;
$rs = getRS($sql);
$recipients = array('ids'=>array(),'rec'=>array());
while($row = mysql_fetch_assoc($rs)) {
	$recipients['ids'][] = $row['id'];
	$recipients['rec'][$row['id']] = decode($row['name'])." <".$row['tkemail'].">";
}
unset($rs);
arrPrint($recipients);
if(count($recipients['ids'])>1) {// || ( count($recipients)==1 && !in_array($task['taskadduser'],$recipients['ids']) )) {*/
$recipients = array('ids'=>array(),'rec'=>array());
//get task owner details
if($task['taskadduser']!=$tkid) {
	$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = ".$task['taskadduser']." AND tkstatus = 1 AND tkemail <> ''";
	$row = $ah->mysql_fetch_one($sql);
	if(isset($row['tkemail'])) {
		$recipients['rec'][] = array('name'=>$row['name'],'email'=>$row['tkemail']);
		$recipients['ids'][] = $row['tkid'];
	}
}
//get other recipient details
$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name
		FROM assist_".$cmpcode."_timekeep
		INNER JOIN ".$dbref."_task_recipients
		  ON tkid = tasktkid
		  AND taskid = ".$logtaskid."
		  AND tasktkid <> '".$tkid."'
		  AND tasktkid <> '".$task['taskadduser']."'
		WHERE tkstatus = 1 AND tkemail <> ''";
$rows = $ah->mysql_fetch_all($sql);
if(count($rows)>0) {
	foreach($rows as $row) {
		//$recipients['rec'][] = $row['name']." <".$row['tkemail'].">";
		$recipients['rec'][] = array('name'=>$row['name'],'email'=>$row['tkemail']);
		$recipients['ids'][] = $row['tkid'];
	}
}

//arrPrint($recipients);

if(count($recipients['rec'])>0) {
	echo "<P>4. Sending notifications</p>";
	$userFrom = $adb->mysql_fetch_one("SELECT CONCAT(tkname,' ',tksurname) as name, tkemail as email FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."' AND tkstatus = 1 AND tkemail <> ''");
	if(!is_array($userFrom) || !isset($userFrom['email']) || strlen($userFrom['email'])==0) {
		$userFrom = false;
	}
	$sql = "SELECT *
			FROM assist_".$cmpcode."_udfindex
			 LEFT OUTER JOIN assist_".$cmpcode."_udf
			 ON udfindex = udfiid
			 AND udfref = '".strtoupper($_SESSION['modref'])."'
			 AND udfnum = $logtaskid
			WHERE udfiobject = 'action'
			AND udfiyn = 'Y'
			AND udfiref = '".strtoupper($_SESSION['modref'])."'
			AND (
			  udfilinkfield = ''
			  OR (udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid'].")
			)
			ORDER BY udfisort, udfivalue";
	$task_udfs = $ah->mysql_fetch_all($sql);
	//get local udfs
	$udf_lists = getUDFListItems("");
	//get udfs from master modules
	$masters = array("CUSTOMER","SUPPLIER","EMPLOYEE");
	foreach($masters as $master_key) {
		$masterObject = new MASTER_EXTERNAL($master_key);
		$udf_lists[$master_key] = $masterObject->getListForExternalModule(false);
		unset($masterObject);
	}
	//reset mod ref back to action module's ref
	$actionObject->setActionModRef($local_modref);


	$subject = $ah->decode($_SESSION['modtext']." $actname $logtaskid Updated");
$message = $_SESSION['modtext']." $actname $logtaskid has been updated by ".$_SESSION['tkn'].".\n
Update details
Message: ".str_replace(chr(10),chr(10)."   ",$ah->decode($logupdate))."
Status: ".$task['status']." (".$logstate."%)
Date of Activity: ".date("d-M-Y H:i",$logactdate);
	foreach($udf_email as $key => $r) {
		$message.="\n".$udf_index['update']['index'][$key]['udfivalue'].": ";
		switch($udf_index['update']['index'][$key]['udfilist']) {
		case "N": $message.=number_format($r,2,".",","); break;
		case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
		case "Y":
			$message.=(isset($udf_lists[$key][$r]) ? $ah->decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]");
			break;
		case "CUSTOMER":
		case "SUPPLIER":
		case "EMPLOYEE":
				$message.=(isset($udf_lists[$tu['udfilist']][$r]) ? $ah->decode($udf_lists[$tu['udfilist']][$r]) : "[Unspecified]");
				break;
		default: $message.=$r;
		}
	}
$message.=($attach_count>0 ? "\n".$attach_count." file(s) were attached to this update.  Please access the $actname on Assist in order to view the attachment(s)." : "")."

$actname details
Created: ".date("d-M-Y H:i:s",$task['taskadddate'])."
Topic: ".$task['topic']."
Deadline: ".date("d-M-Y",$task['taskdeadline'])."
Instructions: ".str_replace(chr(10),chr(10)."   ",$ah->decode($task['taskaction']))."
Deliverables: ".str_replace(chr(10),chr(10)."   ",$ah->decode($task['taskdeliver']));
	foreach($task_udfs as $tu) {
		$message.="\n".$tu['udfivalue'].": ";
		if(strlen($tu['udfvalue'])==0 || ($tu['udfilist']=="Y" && !$ah->checkIntRef($tu['udfvalue']))) {
			$message.="[Unspecified]";
		} else {
			$r = $tu['udfvalue'];
			$key = $tu['udfindex'];
			switch($tu['udfilist']) {
			case "N": $message.=number_format($r,2,".",","); break;
			case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
			case "Y":
				$message.=(isset($udf_lists[$key][$r]) ? $ah->decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]");
				break;
			case "CUSTOMER":
			case "SUPPLIER":
			case "EMPLOYEE":
				$message.=(isset($udf_lists[$tu['udfilist']][$r]) ? $ah->decode($udf_lists[$tu['udfilist']][$r]) : "[Unspecified]");
				break;
			default: $message.=$r;
			}
		}
	}
$message.="

Please log onto Assist to view the full history of this $actname.";

	$emailObject = new ASSIST_EMAIL($recipients['rec'],$subject,$message,"TEXT","","",$userFrom);
	$emailObject->sendEmail();
}
echo "<P>Update complete!</p>";
if(isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0) {
	if($redirect_me=="dashboard" || $_REQUEST['src']=="home") {
		echo "
		<script type=text/javascript>
			parent.header.$('#backHome').trigger('click');
			$('#backHome', window.parent.header.document).click();
		</script>";
	} else {
		//echo "<br><br>Going home - we're done!!";
		//echo $message;

		echo "<script type=text/javascript>document.location.href = 'view_list.php?get_details=1';</script>";
		//echo "<script type=text/javascript>document.location.href = '".$_REQUEST['src']."';</script>";
	}
}


?>