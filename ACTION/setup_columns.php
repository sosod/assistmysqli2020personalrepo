<?php 

$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('txt'=>"List Columns"),
);
$page = array("setup","columns");
$get_udf_link_headings = true;
require_once 'inc_header.php';

if(isset($_REQUEST['r'])) { $ah->displayResult($_REQUEST['r']); }

$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : die("<p>An error has occurred.  Please go back and try again.</p>");
switch($section) {
case "my":	
case "all":	$title = ucfirst($section)." ".ucfirst($actname)."s"; break;
case "own":	$title = ucfirst($actname)."s Owned"; break;
case "home": $title = "Home Page Action List"; break;
}


updateUDFDisplayList();


$sql_start = "SELECT a.*, u.udfivalue
				FROM `".$dbref."_list_display` a
				LEFT OUTER JOIN assist_".$cmpcode."_udfindex u
				ON a.field = u.udfiid
				AND u.udfiyn = 'Y'
				WHERE a.yn = 'Y'
				AND ( u.udfiobject = 'action' OR a.type = 'S' ) ";




$display = array('N'=>array(),'Y'=>array());
foreach($display as $key => $d) {
	$sql = $sql_start." AND a.".$section."disp = '$key' AND a.".$section."yn = 'Y' ORDER BY a.".$section."sort";
	//$rs = getRS($sql);
	$rows = $ah->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
	foreach($rows as $row){
		if($row['type']=="U") { $row['headingfull'] = $row['udfivalue']; }
		$display[$key][] = $row;
	}
	unset($rs);
}


?>


	
	<style>
	#sortable1, #sortable2 { list-style-type: none; margin: 0; padding: 0; float: left; margin-right: 10px; list-style-image: url(); width: 100% }
	#sortable1 li, #sortable2 li { margin: 0 5px 5px 5px; padding: 5px; }
	.ui-state-highlight { height: 15px; }
	li { cursor: hand; }
	</style>
	<script>
	$(function() {
		$( "#sortable1, #sortable2" ).sortable({
			connectWith: ".connectedSortable",
			placeholder: "ui-state-highlight"
		}).disableSelection();
		$("tr").unbind("mouseenter mouseleave");
	});
	</script>

<p>To add a column, click and drag the column from the "Columns Available" box to the "Columns to Display" box.
<br>To remove a column, click and drag the column from the "Columns to Display" box to the "Columns Available" box.
<br>To change the order in which the columns display, click and drag each column into it's desired position in the "Columns to Display" box.</p>

<table class=noborder id=tbl_list>
	<thead>
		<tr>
			<th >Columns Available</th>
			<td  class=noborder>&nbsp;&nbsp;</td>
			<th >Columns To Display</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class=noborder>
<ul id="sortable1" class="connectedSortable">
<?php foreach($display['N'] as $d) { 
	echo "<li class=\"ui-state-default\" id=\"".$d['id']."\">".$d['headingfull']."&nbsp;</li>";
 } ?>
</ul>
			</td>
			<td class=noborder>&nbsp;&nbsp;</td>
			<td class=noborder>
<ul id="sortable2" class="connectedSortable">
<?php foreach($display['Y'] as $d) { 
	echo "<li class=\"ui-state-default\"  id=\"".$d['id']."\">".$d['headingfull']."&nbsp;</li>";
 } ?>
</ul>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan=3 class="noborder right"><input type=button value=Save class=isubmit /></td>
		</tr>
	</tfoot>
</table>
<form name=save_columns action=setup_columns_process.php method=post>
<input type=hidden name=section value=<?php echo $section; ?> />
<input type=hidden value="" name=dN id=dN />
<input type=hidden value="" name=dY id=dY />
</form>
<script type=text/javascript>
$(document).ready(function() {
	var left = $("#tbl_list th:first").width();
	var right = $("#tbl_list th:last").width();
	if(left > right) { $("#tbl_list th:last").width(left); var width = left; } else { $("#tbl_list th:first").width(right); var width = right; }
	$(".sortable1, .sortable2").css("width",width+"px");
	
	$("input:button").click(function() {
		var dN = "";
		var dY = "";
		$("#sortable1 li").each(function() {
			dN=dN+"_"+$(this).attr("id");
		});
		$("#sortable2 li").each(function() {
			dY=dY+"_"+$(this).attr("id");
		});
		if(dY.length==0) {
			alert("Please select at least one column to display.");
		} else {
			$("form[name=save_columns] #dN").val(dN.substring(1));
			$("form[name=save_columns] #dY").val(dY.substring(1));
			$("form[name=save_columns]").submit();
		}
	});
	$("#h_title").append(" >> <?php echo $title; ?>");
});
</script>




<p><img src=/pics/tri_left.gif align=absmiddle > <a href=setup.php class=grey>Go back</a></p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	</body>
</html>
