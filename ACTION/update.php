<?php
//error_reporting(-1);
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$view_type = isset($_REQUEST['act']) ? $_REQUEST['act'] : "";
$get_udf_link_headings = false;

$redirect_me = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";

require_once 'inc_header.php';

$src = explode("/",$_SERVER['HTTP_REFERER']);
//arrPrint($src);
if(substr($src[count($src)-1],0,4)=="link") {
	$return_addr = "home";
} else {
	$return_addr = $src[count($src)-1];
}
function drawHistoryRow($log) {
	global $update_udfs,$ah;
	global $history_attachments;
	global $history_udfs;
	global $update_udf_lists;
	$cmpcode = $_SESSION['cc'];
	$modref = $_SESSION['modref'];
	
	$logid = $log['logid'];
	$log_udf = isset($history_udfs[$logid]) ? $history_udfs[$logid] : array();
	$log_attach = isset($history_attachments[$logid]) ? $history_attachments[$logid] : array();
	$file_location = "../files/".$cmpcode."/".$_SESSION['modref']."/";
	foreach($log_attach as $i => $l) {
		if(isset($l['storage_module'])){
			$path = "..".$l['system_filename'];
			if(!file_exists($path) && $l['docyn']!== "O") {
				unset($log_attach[$i]);
			}
		}else if(strlen($l['file_location'])>0) {
			$path = $file_location.$l['file_location']."/"; 
			if(!file_exists($path.$l['system_filename'])) {
				unset($log_attach[$i]);
			}
		} else {
			$path = $file_location; 
			if(!file_exists($path.$l['system_filename'])) {
				unset($log_attach[$i]);
			}
		}
	}
	
		echo "<tr>
				<td colspan=2 style='padding: 0 0 0 0;'>
					<table id=log_".$logid." width=99% class=noborder>
						<tr>
							<th width=50 rowspan=2>".date("d M Y H:i",$log['logactdate'])."</th>
							<td >
								<p style='margin-top: 0px;'>".str_replace(chr(10),"<br />",$ah->decode($log['logupdate']))."</p>";
		if(count($log_attach)>0) {
			echo "
							<p><span class=b>Attachments:</span>";
			foreach($log_attach as $a) {
				if(isset($a['storage_module'])){
					if($a['docyn'] == "O"){
						echo "<br />+ <em>".$a['original_filename']." - deleted from ".$a["storage_name"]."</em>". '<img src="/pics/icons/folder_full.gif" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;" title="(Stored in '.$a["storage_name"].')">';
					}else{						
						echo "<br />+ <a href='download2.php?i=".$a['id']."&DOC=true&storage=".$a['storage_module']."' title='(Stored in ".$a['storage_name'].")'>".$a['original_filename']."</a>". '<img src="/pics/icons/folder_full.gif" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;" title="(Stored in '.$a["storage_name"].')">';
					}	
				}else{
					echo "<br />+ <a href='download2.php?i=".$a['id']."'>".$a['original_filename']."</a>";
				}
			}
			echo "</p>";
		}
        //ASSIST_HELPER::arrPrint($update_udfs);
		echo "
								<table class=noborder id=tbl_udf>";
								foreach($update_udfs as $u) {
									if(isset($log_udf[$u['udfiid']]) && ( ($u['udfilist']=="Y" && $ah->checkIntRef($log_udf[$u['udfiid']]['udfvalue'])) || (strlen($log_udf[$u['udfiid']]['udfvalue'])>0) )) {
										$lv = isset($log_udf[$u['udfiid']]['udfvalue']) ? $log_udf[$u['udfiid']]['udfvalue'] : "";
										echo "
											<tr>
												<td class='b right' >".$u['udfivalue'].":</td>
												<td>";
												switch($u['udfilist']) {
												case "Y":
													echo (isset($lv) && $ah->checkIntRef($lv) && isset($update_udf_lists[$u['udfiid']][$lv]['udfvvalue'])) ? $update_udf_lists[$u['udfiid']][$lv]['udfvvalue'] : "N/A" ;
													break;
												case "M":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "D":
													if(strlen($lv)>0) { 
														if(is_numeric($lv)) { echo date("d M Y",$lv); } else { echo $lv; }
													} else {
														echo "N/A";
													}
													
//													echo strlen($lv)>0 && is_numeric($lv) ? date("d M Y",$lv) : "N/A";
													break;
												case "T":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "N":
													echo strlen($lv)>0 && is_numeric($lv) ? number_format($lv,2) : "N/A";
													break;
											    case "CUSTOMER":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("CUSTOMER");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
                                                case "SUPPLIER":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("SUPPLIER");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
                                                case "EMPLOYEE":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("EMPLOYEE");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
												}
											echo "</td>
											</tr>";
									}
								}
						echo	"</table>
		</td>
							<td  class='right'>
								<p style='margin-top: 0px;'><span class=b>Status:</span> ".$log['status']."<br />(".$log['logstate']."% completed)</p>
								<p><span class=b>Date Logged:</span><br />".date("d M Y H:i",$log['logdate'])."</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
}


//GET USER ACCESS PERMISSIONS
$tact = getUserAccess();

//GET TASK ID
$taskid = $_REQUEST['i'];
if($ah->checkIntRef($taskid)) {
	//GET TASK DETAILS INTO ARRAY TASK
//	$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
	$task = getTask($taskid);
	
	//HISTORY INFORMATION
		$sql = "SELECT l.*, stat.value as status 
				FROM ".$dbref."_log l
				INNER JOIN ".$dbref."_list_status stat
				  ON l.logstatusid = stat.pkey
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U','E','D') 
				ORDER BY logactdate DESC, logdate DESC";
		$history = $ah->mysql_fetch_all_fld($sql,"logid");
		$sql = "SELECT l.* 
				FROM ".$dbref."_log l
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U') 
				ORDER BY logactdate DESC, logdate DESC
				LIMIT 1";
		$last = $ah->mysql_fetch_all($sql);
		$last_log_id = isset($last[0]['logid']) ? $last[0]['logid'] : false;
		$sql = "SELECT a.* 
				FROM ".$dbref."_task_attachments a
				INNER JOIN ".$dbref."_log l
				  ON a.logid = l.logid
				  AND l.logtaskid = ".$taskid;
		$sql .=	" ORDER BY l.logactdate DESC, l.logdate DESC";
		$history_attachments = $ah->mysql_fetch_all_fld2($sql,"logid", "id");
		//ASSIST_HELPER::arrPrint($sql);
		$doc = new DOC();
		$docModules = $doc->getDocInstancesForSelect();
		if(count($docModules)>0){
			$histAtts = $doc->getHistoryDocsForAction($taskid,$dbref."_log");
			//echo "<br>Default history</br>";
			//ASSIST_HELPER::arrPrint($history_attachments);
			foreach($histAtts as $k=>$v){
				if(isset($history_attachments[$k])){
					foreach($v as $docid=>$data){
						$history_attachments[$k][$docid] = $data;
					}
				}else{
					$history_attachments[$k] = $v;
				}
			}
		}
		//echo "<br>All history</br>";
		//ASSIST_HELPER::arrPrint($history_attachments);
		$sql = "SELECT u.*, l.logid 
				FROM assist_".$cmpcode."_udf u
				INNER JOIN assist_".$cmpcode."_udfindex i
				  ON u.udfindex = i.udfiid
				  AND i.udfiobject = 'update'
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = '".$modref."'
				INNER JOIN ".$dbref."_log l
				  ON l.logid = u.udfnum
				  AND l.logtaskid = ".$taskid."
				WHERE u.udfref = '".$modref."'";
		$history_udfs = $ah->mysql_fetch_all_fld2($sql,"logid", "udfindex");
		
	//UPDATE INFORMATION	
		$sql = "SELECT * FROM ".$dbref."_list_status WHERE yn = 'Y' AND id NOT IN ('CN','NW','ON') ORDER BY sort";
		$status = $ah->mysql_fetch_all_fld($sql,"pkey");
		
		$sql = "SELECT * 
				FROM assist_".$cmpcode."_udfindex 
				WHERE udfiref = '".$modref."' 
				AND udfiyn = 'Y' 
				AND udfiobject = 'update' 
				AND (
						udfilinkref = 0 
					OR 
						udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid']."
					OR 
						udfilinkfield = 'taskstatusid'
				)
				ORDER BY udfisort, udfivalue";
		$update_udfs = $ah->mysql_fetch_all($sql);
		$update_udf_lists = getUDFListItems("update");

	
} else {
	die("<p>An error occurred while trying to access the requested ".$actname.".  Please go back and try again.</p>");
}

//$last = array_pop($history_udfs);
$k = array_keys($history);
$last = $last_log_id!== false && isset($history_udfs[$last_log_id]) ? $history_udfs[$last_log_id] : array();

if($_SERVER['REMOTE_ADDR']=="105.233.22.235") {
//	arrPrint($k);
//	arrPrint($history);
//	arrPrint($last);
}

/*
//SET REMINDER
$tremind = $_GET['remind'];
if(strlen($tremind) > 0) {
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE ".$dbref."_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
    $tsql = $sql;
    $tref = "TA";
    $trans = "Set reminder ".$tr." for task ".$taskid.".";
    include("inc_transaction_log.php");
}
*/

?>
<style type=text/css>
[type=file] { margin-bottom: 5px; }
#tbl_udf, #tbl_udf td { padding: 2px 1px 2px 1px; }
.metaFileIn{
	background-color : #edcfaa;
}
.metaFileOk{
	background-color : #aaedc0;
}
</style>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>
<table width=100% id=tbl_container>
	<tr><td class=td_container style='padding-right: 30px' width=50%>
        <h2 class=fc><?php echo ucfirst($actname);?> Status</h2>
        <table width=95% id=tbl_object><tbody>
            <tr>
                <th width=140>Reference:</b></th>
				<td><?php echo $task['taskid']; ?></td>
            </tr>
            <tr>
                <th width=140>Created By:</b></th>
				<td><?php echo $task['adduser']; ?></td>
            </tr>
			<tr>
				<th>Created On:</th>
				<td><?php echo date("d-M-Y H:i:s",$task['taskadddate']); ?></td>
			</tr>
            <tr>
                <th>Assigned To:</th>
                <td><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN ".$dbref."_task_recipients r ON (u.tkid=r.tasktkid)
                WHERE r.taskid= '".$task['taskid']."' ";
                    // echo $sql;
                    //$rs = getRS($sql);
					$rows = $ah->mysql_fetch_all($sql);
                    $assignees = "";
                    //while($row = mysql_fetch_array($rs)) {
                    foreach($rows as $row){
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    unset($rs);
                ?></td>
            </tr>
            <tr>
                <th>Topic:</td>
				<td><?php echo $task['topic']; ?></td>
            </tr>
            <tr>
                <th>Priority:</th>
				<td><?php echo $task['urgency']; ?></td>
            </tr>
            <tr>
                <th>Status:</th>
				<td><?php echo $task['status'].($task['taskstate']>0 && $task['taskstate']<100 ? " (".$task['taskstate']."%)" : ""); ?></td>
            </tr>
            <tr>
                <th>Deadline:</th>
                <td><?php echo date("d-M-Y",$task['taskdeadline']); ?></td>
            </tr>
            <tr>
				<th></b><?php echo ucfirst($actname);?> Instructions:</b></th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskaction']);
                ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Deliverables:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskdeliver']);
                ?></td>
            </tr>
<?php
if($cmpcode == "fin0001" || $cmpcode == "janet12") {
	$sql = "SELECT * FROM ".$dbref."_task_preset WHERE preset_taskid = $taskid AND active = true"; 
	//$rs = getRS($sql);
	$rows = $ah->mysql_fetch_all($sql);
		if($ah->db_get_num_rows($sql)>0) { ?>
				<tr>
					<th>Source <?php echo ucfirst($actname);?>:</th>
					<td><ul>
						<?php
						//while($row = mysql_fetch_array($rs)) {
						foreach($rows as $row){
							echo "<li><a href=view_all_update.php?i=".$row['source_taskid']." target=_blank>$actname #".$row['source_taskid']."</a></li>";
						}
					?></ul></td>
				</tr>
	<?php }
	//unset($rs);
}
?>
<!--<tr id="firstdoc">

    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
</tr>
<tr>
    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
</tr>-->


            <?php
            if($task['topic'] == 'Travel Requests' && $topicyn = 'N') {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                $udfi = $ah->mysql_fetch_one($sql);
                if(strlen($udfi['udfiid'])>0) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                    //$rs2 = getRS($sql2);
                    $udf = $ah->db_get_num_rows($sql2);
                    $row2 = $ah->mysql_fetch_one($sql2);
                    ?>
            <tr>
                <th>Travel Request Ref.:</th>
                <td><?php
					if($udf>0) {
						echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
					}
					else {
						echo("N/A");
					}
				?>&nbsp;</td>
            </tr>
                    <?php
                }
            }
            $sql = "SELECT * 
					FROM assist_".$cmpcode."_udfindex 
					WHERE udfiref = '".$modref."' 
					AND udfiyn = 'Y' 
					AND udfiobject = 'action' 
					AND (udfilinkref = 0 OR udfilinkref = ".$task['tasktopicid'].")
					ORDER BY udfisort, udfivalue";
            //$rs = getRS($sql);
			$rows = $ah->mysql_fetch_all($sql);
            //while($row = mysql_fetch_array($rs)) {
						foreach($rows as $row){
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                //$rs2 = getRS($sql2);

					$udf = $ah->db_get_num_rows($sql2);
					$row2 = $ah->mysql_fetch_one($sql2);
                ?>
            <tr>
                <th><?php echo($row['udfivalue']); ?>:</th>
                <td><?php
                        switch($row['udfilist']) {
                            case "Y":
                                if($ah->checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    //$rs3 = getRS($sql2);
										$row3 = $ah->mysql_fetch_one($sql2);
										echo $row3['udfvvalue'];
                                    unset($rs3);
                                } else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo $ah->decode($row2['udfvalue']);
                                else
                                    echo "N/A";
                                break;
                            case "CUSTOMER":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("CUSTOMER");
                                    //ASSIST_HELPER::arrPrint($obj->getObjectNameForExternalModule($row2['udfvalue']));
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            case "SUPPLIER":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("SUPPLIER");
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            case "EMPLOYEE":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("EMPLOYEE");
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",$ah->decode($row2['udfvalue'])));
                                break;
                        }
                ?>&nbsp;</td>
            </tr>
                <?php
            }
			unset($rs2);
			//Doc attachments
			$doc = new DOC();
			$docModules = $doc->getDocInstancesForSelect();
			//ASSIST_HELPER::arrPrint($docModules);
			if(count($docModules)>0){
				$docAtts = $doc->getDocsForAction($taskid);
			}else{
				$docAtts = array();
			}
			//ASSIST_HELPER::arrPrint($docAtts);

            $sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE (att.file_location = 'action' OR att.file_location = 'edit') AND att.taskid = ".$taskid;
            //$rs = getRS($sql);
			$rows = $ah->mysql_fetch_all($sql);
            if($ah->db_get_num_rows($sql) > 0 || count($docAtts) > 0) {
                echo "<tr>
				<th>Attachment(s):</th>
				<td>";
$tattach = array();
                //Original attachments
                //while($row = mysql_fetch_assoc($rs)) {
                foreach($rows as $row){
					//ASSIST_HELPER::arrPrint($row);
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
					$row['file_location'] = $row['file_location'] == "edit" ? "action" : $row['file_location'];
                    $file = "../files/$cmpcode/".$_SESSION['modref']."/".(strlen($row['file_location'])>0 ? $row['file_location']."/" : "").$row['system_filename'];
                    if(file_exists($file)) {
						$tattach[] =  "+ <a href='download2.php?i=".$row['id']."'>".$row['original_filename']."</a>";
					}
                }
				$file = null;
				foreach($docAtts as $val){
                	$file = "..".$val['system_filename'];///files/$cmpcode/".$_SESSION['modref']."/".(strlen($row['file_location'])>0 ? $row['file_location']."/" : "").$row['system_filename'];
					if(file_exists($file)){
						$tattach[] =  "+ <a href='download2.php?i=".$val['id']."&DOC=true&storage=".$val['storage_module']."' title='(Stored in ".$val['storage_name'].")'>".$val['original_filename']."</a>". '<img src="/pics/icons/folder_full.gif" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;" title="(Stored in '.$val["storage_name"].')">';// <em>(Doc Management System)</em>";
					}else if($val['docyn']=="N"){
						$tattach[] =  "+ <em>".$val['original_filename']." - deleted from ".$val["storage_name"]."</em>". '<img src="/pics/icons/folder_full.gif" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;" title="(Stored in '.$val["storage_name"].')">';						
					}
				}
                echo implode("<br />",$tattach)."</td></tr>";
            }

            if(($tkid == $task['taskadduser'] || $view_type == "ALL") && $task['topic'] != 'Travel Requests') { ?>
                <tr><th></th><td class=right><input type="button" value="Edit"  onclick="redirect(<?php echo($taskid);?>);"/></td></tr>
           <?php } ?>
        </tbody></table>
        <?php
		$ah->displayGoBack();
if(count($history)>0) {
?>
	<table id=tbl_history width=95%>
	<?php
	echo "	<tr>
				<th colspan=2>History</th>
			</tr>";
	foreach($history as $h) {
		drawHistoryRow($h);
	}
	
	?>
	</table>
<?php
	$ah->displayGoBack();
} 
?>
</td><td class=td_container width=50%><div class=float>
<?php
if($task['taskstatusid']>2) {	//not completed or cancelled		
		echo "<h2>".ucfirst($actname)." Updates</h2>
		<form name=update id='updateForm' action=update_process.php method=post  enctype='multipart/form-data'>
		<input type=hidden name=redirect value=".$redirect_me." />
		<input type=hidden name=logtaskid value=".$taskid." />
		<input type=hidden name=src value='".$return_addr."' />
		<table id=tbl_updates>
			<tr>
				<th colspan=2>Update</th>
			</tr>
			<tr class=no-highlight>
				<td class=center colspan=2>
					<textarea rows=6 cols=70 name=logupdate></textarea>
				</td>
			</tr>
			<tr>
				<td class=left style='border-right: 0px solid #ababab; border-bottom: 0px;'>
					<p><span class=b>Status:</span> <select name=logstatusid>";
					foreach($status as $s) {
						echo "<option value=".$s['pkey']." s=".$s['state'].">".$s['value']."</option>";
					}
		echo "
					</select></p>
					<p><input type=text name=logstate id=logstate size=5 value=".($task['taskstate'])." style='text-align: right; margin-left: 45px' />% complete </p>
					<p><span class=b>Date of Activity:</span> <input type=text name=logactdate value='".date("d M Y H:i")."' class=datetime /></p>
				</td>
				<td class=right  style='border-left: 0px solid #ababab; border-bottom: 0px;'>
					<input type=button id='attachmentButton' value='Choose Attachments' />
				</td>
			</tr>";
			if(count($update_udfs)>0) {
				$udftypes = array('Y'=>array(),'D'=>array());
				echo "
					<tr><td colspan=2 style='border-top: 0px;'>
						<table id=tbl_update_udf>";
				foreach($update_udfs as $u) {
					$ui = $u['udfiid'];
					$uv = $last[$ui]['udfvalue'];
					if(isset($udftypes[$u['udfilist']])) { $udftypes[$u['udfilist']][] = $u['udfiid']; }
					$class = $u['udfiobject']." ".(strlen($u['udfilinkfield'])>0 ? $u['udfilinkfield']." ".$u['udfilinkref'] : "");
					echo "
						<tr class='udf $class'>
							<td class='b right'>".$u['udfivalue'].":</td>
							<td>";
						switch($u['udfilist']) {
						case "Y":
							$x = isset($update_udf_lists[$ui][$uv]);
							echo "<select name=udf[".$ui."]><option value=0 ".($x ? "" : "selected")." >N/A</option>";
							foreach($update_udf_lists[$ui] as $v) {
								echo "<option value=".$v['udfvid']."  ".($uv==$v['udfvid'] ? "selected" : "").">".$v['udfvvalue']."</option>";
							}
							echo "</select>";
							break;
						case "M":
							echo "<textarea name=udf[".$u['udfiid']."] rows=5 cols=60>".decode($uv)."</textarea>";
							break;
						case "D":
							echo "<input name=udf[".$u['udfiid']."] type=text size=11 class=jdate2012 value='";
							if(strlen($uv)>0) {
								if(is_numeric($uv)) {
									echo date("d-M-Y",$uv);
								} else {
									echo $uv;
								}
							} else {
								echo "";
							}
							echo "' />";
							break;
						case "T":
							echo "<input name=udf[".$u['udfiid']."] type=text size=60 value='".decode($uv)."' />";
							break;
						case "N":
							echo "<input name=udf[".$u['udfiid']."] type=text size=15 class=number value='".$uv."' /> <span class=i style='' id=lbl_".$u['udfiid'].">(numbers only)</span>";
							break;
						}
					echo "</td>
						</tr>";
				}
				echo "	</table>
					</td></tr>";
			}
			if(isset($udftypes)){
				$hiddenUDF = '<input type=hidden name=udf_types value='.serialize($udftypes).' />';
			}else{
				$hiddenUDF = '';
			}
			echo "
			<tr>
				<td class=center colspan=2>".$hiddenUDF."<input type=button value='Save Update' class=isubmit /><input type=reset /></td>
			</tr>
		</table>
		</form>";
	$ah->displayGoBack();
}//end taskstatusid if
		?></div>
</td></tr>
</table>
<div hidden id='attachmentPopup'>
	<table id='tbl_attach' width='100%' class='noborder' style='padding:8px;'>
		<tr>
			<td class=noborder>
				<input type=file name=attachments[] size=30 style='margin-top: 0px;' />
				<br>
				<div class='moduleSelect'></div>
			</td>
		</tr>
		<tr hidden>
			<td class=center id='insertAttachData'>&nbsp;</td>			
		</tr>
	</table>
	<a href='javascript:void(0)' id='attachlink' style='margin-top: 5px;'>Attach another file</a>
</div>
<div hidden id='metadataPopup'></div>
<script type=text/javascript>
$(function() {
	window.oneClicked = false;
	$("tr").off('mouseenter mouseleave');
	$("td").addClass("top");
	$("table.noborder td").addClass("noborder");
	$("table#tbl_container, table#tbl_container td.td_container").addClass("noborder");
	$("#tbl_object th").addClass("left");
	$("#tbl_update_udf, #tbl_update_udf td").addClass("noborder");
	$("#tbl_updates select[name=logstatusid]").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_updates input[name=logstate]").val(<?php echo $task['taskstate']; ?>).blur(function() {
		var v = parseInt($(this).val());
		if(v==100) {
			$("#tbl_updates select[name=logstatusid]").val(1);
			$(this).attr("disabled","disabled");
		}
	});
	var previous_status;
	var previous_progress;
	$("#tbl_updates select[name=logstatusid]").focus(function() {
		previous_status = $(this).val();
		previous_progress = $("#tbl_updates input[name=logstate]").val();
	});
	$("#tbl_updates select[name=logstatusid]").change(function() {
		var v = $(this).val();
		if(v==1) {
			$("#tbl_updates input:text[name=logstate]").val(100);
			$("#tbl_updates input:text[name=logstate]").attr("disabled","true");
		} else {
			$("#tbl_updates input:text[name=logstate]").removeAttr("disabled");
			var s = $("#tbl_updates select[name=logstatusid] option:selected").attr("s");
			if(!isNaN(parseInt(s)) && s > 0	) {
				$("#tbl_updates input:text[name=logstate]").val(s);
			} else if(parseInt(previous_status)==1){
				$("#tbl_updates input:text[name=logstate]").val(99);
			}
		}
		$("#tbl_update_udf tr.taskstatusid").each(function() {
			if($(this).hasClass(v)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});

	$("form[name=update] input:button.isubmit").click(function() {
		var form = "form[name=update]";
		$(form+" textarea,"+form+" select,"+form+" input").removeClass("required");
		var err = false;
		var logupdate = $(form+" textarea[name=logupdate]").val();
		if(logupdate.length==0) {
			$(form+" textarea[name=logupdate]").addClass("required");
			err = true;
		}
		var logstatusid = $(form+" select[name=logstatusid]").val();
		if(logstatusid.length==0) {
			$(form+" select[name=logstatusid]").addClass("required");
			err = true;
		}
		var logstate = $(form+" input[name=logstate]").val();
		if(logstate.length==0) {
			$(form+" input[name=logstate]").addClass("required");
			err = true;
		}
		var logactdate = $(form+" input[name=logactdate]").val();
		if(logactdate.length==0) {
			$(form+" input[name=logactdate]").addClass("required");
			err = true;
		}
		$("#tbl_update_udf input:text").each(function() {
			var u = 0;
			if($(this).hasClass("number")) {
				u = $(this).val();
				if(u.length>0 && (isNaN(parseFloat(u)) || !isFinite(u))) {
					$(this).addClass("required");
					$("#lbl_"+$(this).attr("name")).css("color","#cc0001");
					err = true;
				}
			}
		});
		if(!err) {
			$clone = $("#insertAttachData");
			var d = new Date();
			$unique = d.getTime();
			$("#updateForm").find("input:hidden").clone(true).appendTo($clone);
			var selects = $("#tbl_updates").find("select");
			$(selects).each(function(i) {
			    var select = this;
			    $(select).clone().appendTo($clone);
			    $clone.find("select").eq(i).val($(select).val());
			});
			$("#tbl_updates").find("input:text, textarea").each(function(){
			    var select = this;
				$me = $(select).clone(true).appendTo($clone);
			});
			$("#tbl_attach").wrap("<form id='attachForm' action='update_process.php' method='post' enctype='multipart/form-data' />");
			$("#attachForm").submit();
		} else {
			alert("Please complete the required fields as highlighted in red.");
		}
	});
	
	$("#attachmentButton").button().click(function(){
		$("#attachmentPopup").dialog({
			title:"Choose attachments to upload",
			hide: "fade",
			show: "fade",
			width: 550,
			height: 400,
			buttons:{
				Done : function(){
					validateUploads();
				}
			}
		});
	});
	
	function validateUploads(){
		colouriseUI();
		var er = 0;
		$("#attachmentPopup").find("input:file").each(function(){
			if($(this).hasClass("metaFileIn")){
				$(this).parent().effect("highlight");
				er ++;
			}
		});
		if(er > 0){
			alert("Files in yellow do not have their details filled in \n Please choose a location for each of them, and enter their details.");
		}else{
			$("#attachmentPopup").dialog("close");
		}
	}
	
	$("input:file").change(function(){
		colouriseUI();
	});
	
	$(document).on("click","input:file",function(event){
		window.oneClicked = true;
		if(typeof(window.modChooserCount) === 'undefined'){
			window.modChooserCount = 0;
		}
		if($(this).prop("files").length == 0){
			if(typeof(window.modChooser) !== 'undefined'){
				var html = window.modChooser;
				html = html.replace(/docModRef[0-99]/g,"docModRef"+window.modChooserCount);
			}else{
				$(this).addClass("metaFileOk");
				var html = "";
				<?php
					$echo = '<div id=docModuleInfo><br>';
					$docModules = $doc->getDocInstancesForSelect();
					$icon = mysqli_real_escape_string($ah->getConn(),$doc->getDisplayIconAsDiv("error"," float:right;"));
					$echo.= $icon;
					$uni = time();
					if(count($docModules)>0){
						$echo.= '<em>Where would you like this attachment to be stored?</em><br>';
						$echo .= '<div id=\"docModuleChooser'."-".$uni.'\" class=\"radioSet\">';
						$echo .= '<input type=\"radio\" checked id=\"defaultMod'."-".$uni.'\" value=\"DefaultMod\" name=\"docModRef'."-".$uni.'\"><label for=\"defaultMod'."-".$uni.'\">Default Location</label>';
						foreach($docModules as $key=>$val){
							$echo .= '<input type=\"radio\" id=\"'.$key.'-'.$uni.'\" value=\"'.$key.'\" name=\"docModRef'."-".$uni.'\"><label for=\"'.$key.'-'.$uni.'\">'.$val.'</label>';
						}
						$echo .=  '</div>';
					}else if(count($docModules)<=0){
						$echo .= '<em>Storing file in the default location. </em><input hidden name=\"docModRef'."-".$uni.'\" value=DefaultMod />';
					}
					$echo .= '<div class=\"hiddenInsertPoint\" hidden>&nbsp;</div></div>';
				 ?>
				html += "<?php echo $echo; ?>";
				html = html.replace(/docModRef/g,"docModRef"+window.modChooserCount);
				window.modChooser = html;
			}
			$(this).siblings("div.moduleSelect").html(html);
			//console.log($(this).siblings("moduleSelect"));
			window.modChooserCount ++;
			$("#attachmentPopup div.ui-icon-closethick").on("click",closeAttachRow);
		}
	});
	
	function closeAttachRow(){
		//console.log($(this).parents("td.top").first().children("input").val());
		$(this).parents("td.noborder").children("input").val("");
		$(this).parents("div.moduleSelect").empty();
		checkFileInputs();
		colouriseUI();
		//window.modChooserCount --;
	}
	
	function checkFileInputs(){
		var fileInputs = $("#tbl_attach").find("input:file");
		$(fileInputs).each(function(i){
			//console.log(i);
			//console.log($(this).val());
			if(i > 0 && $(this).val().length <= 0){
				//console.log($(this).parents("tr"));
				$(this).parents("tr").first().empty();
			}
		});
	}
	
	$('#attachlink').click(function(){
		$("#tbl_attach").append("<tr><td class=noborder><input type=file name=attachments[] size=30 style='margin-top: 0px;' /><div class='moduleSelect'></div></td></tr>");
		if(typeof(window.modChooserCount) === 'undefined'){
			if(window.oneClicked){
				window.modChooserCount = 0;				
			}else{
				window.modChooserCount = 1;								
			}
		}else{
			window.modChooserCount ++;
		}
	});
	
	$(document).on("click",".radioSet input:radio",function(event){
		var id = $(this).attr("id").split("-")[0];
		if(id !== "defaultMod"){
			$radiobtn = $(this);
			var d = new Date();
			$unique = $radiobtn.val()+d.getTime();
			var title = $(this).next().text();
			var fl = $(this).parents("td").first().children("input").prop("files")[0].name;
			var my_post = "fl="+fl+"&modref="+id;
			$.ajax({                                      
				url: 'lib/ajax_new_task.php', 		  
				type: 'POST',		  
				data: my_post,		  
				dataType: 'json' 
			}).fail(function(){
				alert("Something went wrong! Please try that again");
			}).done(function( data ) {
				$("#attachmentPopup").dialog("close");
				//console.log("Finished!");
				$("#metadataPopup").html(data);
				$("#metadataPopup .jdate2012").datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-M-yy',
                    changeMonth:true,
                    changeYear:true		
                });
				$table = $("#tbl_upload");
				$table1 = $("#tbl_attach");
				$("#metadataPopup").dialog({
					title: "Uploading into <em>"+title+"</em>",
					hide: "fade",
					show: "fade",
					width: 550,
					height: 400,
					buttons: {
						Back : function(){
							$(this).dialog("close");
							$("#attachmentPopup").dialog("open");
						},
						Save : function(){
							var err = new Array();
							var my_tag = "";
							var name = "";
							var val = "";
							var fld = "";
							$("#tbl_upload .i_am_required").removeClass("required");
							$("#tbl_upload").find(".i_am_required").each(function() {
								my_tag = $(this).get(0);
								my_tag = $(my_tag).prop("tagName").toString();
								name = $(this).attr("name");
								val = $(this).val();
								if(my_tag.toUpperCase()=="TEXTAREA") {
									val = val+$(this).text();
								}
								if(!val || val.length==0 || (my_tag=="SELECT" && (val=="X" || val=="0"))) {
									if(name=="tasktkid[]") { name="tasktkid"; }
									fld = $(this).parents("td").prev().text();
									fld = fld.toString();
									if(fld.charAt(fld.length-1)==":") {
										fld = fld.substr(0,fld.length-1);
									}
									err.push("- "+fld);
									$(this).addClass("required");
								}
							});
							if(err.length > 0) {
								alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
							} else {
								$clone = $radiobtn.parent("div").next();
								//$clone.show("flip");
								$clone.html("Hiyaah!");
								var selects = $table.find("select");
								$(selects).each(function(i) {
								    var select = this;
								    $(select).clone().appendTo($clone);
								    $clone.find("select").eq(i).val($(select).val()).attr("name",$clone.find("select").eq(i).attr("name")+"->"+$unique);
								});
								$table.find("input:text, textarea").each(function(){
								    var select = this;
									$me = $(select).clone(true).appendTo($clone);
									$me.attr("name",$me.attr("name")+"->"+$unique);
								});
								$(this).dialog("close");
								$("#attachmentPopup").dialog("open");
							}
						}
					}
				});
			});
		}else{
			colouriseUI();
		}
	});
	
	$("#attachmentPopup").on("dialogopen",function(event,ui){
		colouriseUI();
	});
	
	function colouriseUI(){
		$("#attachmentPopup").find("input:file").each(function(){
			//console.log(typeof($(this).prop("files")[0]));
			var typecheck = $(this).prop("files")[0];
			if(typeof(typecheck) !== "undefined"){
				$modSel = $(this).siblings("div");
				$radioSet = $modSel.find("div.radioSet");
				if($radioSet.length > 0){
					$chosenLocation = $radioSet.find("input:radio:checked").val();
					if($chosenLocation == "DefaultMod"){
						$(this).removeClass();	
						$(this).addClass("metaFileOk");
					}else{
						$hiddenMetadata = $modSel.find("div.hiddenInsertPoint");
						if($hiddenMetadata.html().length >= 10){						
							$(this).removeClass();	
							$(this).addClass("metaFileOk");
						}else{						
							$(this).removeClass();	
							$(this).addClass("metaFileIn");
						}
					}
				}else{
					//No document modules on the system
					$(this).removeClass();	
					$(this).addClass("metaFileOk");
				}
			}else{
				$(this).removeClass();	
			}
		});
	}
	
});
</script>		

    </body>

</html>
