<?php include("inc_ignite.php");

function validateDate($d) {	//EXPECTED FORMAT DD-MM-YYYY
	global $ah;
						if(strpos($d,"-")>0 || strpos($d,"/")>0) {
							if(strpos($d,"/")>0) {
								$d2 = strFn("explode",$d,"/","");
							} else {
								$d2 = strFn("explode",$d,"-","");
							}
							if(count($d2)==3) {
								if($ah->checkIntRef($d2[0]) && $ah->checkIntRef($d2[1]) && $ah->checkIntRef($d2[2])) {	//is numeric & > 0
									if($d2[1]<=12) {	//validate month
										if($d2[0]<=31) {	//validate day
											$d = date("d-m-Y",mktime(12,0,0,$d2[1],$d2[0],$d2[2]));
										} elseif($d2[2]<=31) {	//swop day and year
											$d = date("d-m-Y",mktime(12,0,0,$d2[1],$d2[2],$d2[0]));
										} else {
											$d = "";
										}
									} else {	
										$d = "";
									}
								}
							} else {
								$d = "";
							}
						} else {
							$d = "";
						}
		return $d;
}
function validateNum($d) {
	if(strpos($d,",")>0) { $d = strFn("str_replace",$d,",",""); }
	if(strpos($d," ")>0) { $d = strFn("str_replace",$d," ",""); }
	return $d;
}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>
        <script type ="text/javascript">
            $(function() {
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                })
            });
            $(function() {
                $('.dateme').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                })
            });

        </script>
</head>
<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.required { color: #ffffff; font-weight: bold; background-color: #cc0001; }
.met { color: #000000; font-weight: normal; background-color: #ffffff; }
.disabled { background-color: #777777; }
.enabled { background-color: #009900; }
.notfound { background-color: #ffffff; color: #CC0001; text-decoration: underline; font-weight: bold; }
</style>
<script type=text/javascript>
function Validate(me) {
	return false;
}
function Verify(me,t) {
	//alert(t);
	switch(t) {
		case "txt":
			if(me.value.length>0) 
				me.className = 'met';
			else
				me.className = 'required';
			break;
		case "d":
			if(me.value.length>0) 
				me.className = 'date-type met';
			else
				me.className = 'date-type required';
			break;
		case "sel":
			if(me.value.length>0 && me.value != "X") 
				me.className = 'met';
			else
				me.className = 'required';
			break;
		case "tk":
			if(me.selectedIndex < 0) {
				me.className = 'required';
			} else {
				me.className = 'met';
				for(i=0;i<me.options.length;i++) {
					me.options[i].className = '';
				}
			}
			break;
	}
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5 style="font-size:62.5%;">
<?php
	echo "<h1 class=fc>".$modtext.": New ".$actname." - Validate import</h1>";
	echo "<p>Any required fields which are incomplete, will be highlighted in <span class=required>RED</span>.  If you click the Accept button while a required field is incomplete, the $actname will not be created.</p>";
	echo "<form name=import id=imp action=new_multiple_import_process.php method=post>";
	echo "<input type=hidden name=act value=SAVE>";
/**************** IMPORT FILE **************************/
if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
		switch($_FILES["ifile"]["error"])
		{
			case 2:
				echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
				break;
			case 4:
				echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
				break;
			default:
				echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
				break;
		}
		die();
} else { //ELSE IF NO ERROR WITH UPLOAD FILE
		$ext = substr($_FILES['ifile']['name'],-3,3);
		if(strtolower($ext)!="csv") {	//CHECK EXTENSION TYPE
			die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
		} else {
			$filename = $modref."-IMPORT_".substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$ah->getToday()).".csv";
			$fileloc = "../files/".$cmpcode."/".$filename;
			//UPLOAD UPLOADED FILE
			set_time_limit(180);
			copy($_FILES["ifile"]["tmp_name"], $fileloc);
		}
}

if(!file_exists($fileloc)) {
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
}

/******************* GET LISTS ****************************/
$checks = array();
//USERS
$users = array();
$usersid = array();
$sql = "SELECT t.tkid, t.tkname, t.tksurname, a.view FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users u";
$sql.= " WHERE t.tkstatus = 1 AND t.tkid = a.tkid AND a.yn = 'Y' AND t.tkid = u.usrtkid AND u.usrmodref = '".strtoupper($modref)."'";
$sql.= " ORDER BY t.tkname, t.tksurname";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$id = $row['tkid'];
	$n = $ah->decode($row['tkname']." ".$row['tksurname']);
	$tasktkid[$id] = array('id'=>$id,'value'=>$n);
	$checks['tasktkid'][strFn("str_replace",strtolower($n)," ","")] = $id;
	if($row['view']>20) {
		$tau[$id] = array('id'=>$id,'value'=>$n);
		$checks['taskadduser'][strFn("str_replace",strtolower($n)," ","")] = $id;
	}
}
//mysql_close($con);
//PRIORITY
$lists = array();
$sql = "SELECT * FROM ".$dbref."_list_urgency WHERE yn = 'Y'";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$lists['taskurgencyid'][$row['id']] = $row;
	$checks['taskurgencyid'][strFn("str_replace",strtolower($ah->decode($row['value']))," ","")] = $row['id'];
}
//mysql_close($con);
//STATUS
//TOPIC
$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y'  ORDER BY value";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$lists['tasktopicid'][$row['id']] = $row;
	$checks['tasktopicid'][strFn("str_replace",strtolower($ah->decode($row['value']))," ","")] = $row['id'];
}
//mysql_close($con);
/*** UDFs ***/
$sql = "SELECT v.* FROM assist_".$cmpcode."_udfvalue v, assist_".$cmpcode."_udfindex u WHERE u.udfiref = '".strtoupper($modref)."' AND ( udfiobject = '' OR udfiobject = 'action' ) AND u.udfiyn = 'Y' AND u.udfiid = v.udfvindex AND v.udfvyn = 'Y'";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$lists[$row['udfvindex']][$row['udfvid']] = $row;
	$checks[$row['udfvindex']][strFn("str_replace",strtolower($ah->decode($row['udfvvalue']))," ","")] = $row['udfvid'];
}
//mysql_close($con);
/*** HEADINGS ***/
$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND type = 'S'";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$heading[$row['field']] = $row;
}
//mysql_close($con);
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".strtoupper($modref)."' AND ( udfiobject = '' OR udfiobject = 'action' ) AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$row['type'] = "U";
	$heading[$row['udfiid']] = $row;
}
//mysql_close($con);

/****************** GET DATA FROM FILE ***********************/
//GET DATA FROM FILE
set_time_limit(180);
$file = fopen($fileloc,"r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>0 && !empty($tmpdata)) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($file);
unset($data[0]);	//remove row 1 = heading
unset($data[1]);	//remove row 2 = formatting guidelines

/****************** DISPLAY DATA ****************************/
echo "<form name=import id=imp action=new_multiple_import_process.php method=post>";
echo "<input type=hidden name=act value=SAVE>";
echo "<table cellpadding=3 cellspacing=0>";
//HEADINGS
	echo "<tr>";
	foreach($heading as $h) {
		echo "<th>";
		switch($h['type']) {
			case "U": echo $h['udfivalue']; break;
			default: echo $h['headingfull'];
		}
		echo "</th>";
	}
	echo "</tr>";
//DATA
$count = 0;
if(empty($data)) {
	$actionObject = new ACTION();
	echo "<tr>
<td colspan='9' style='min-width: 450px'>".$actionObject->getEmptyTableNotice()."</td>
</tr>";
} else {
	foreach($data as $dta) {

		echo "<tr>";
		$csv = 0;
		/*if (is_array($dta)){
			$d = array_key_exists($csv,$dta) ? $dta[$csv] : "";
		}else{
			$d = isset($csv,$dta) ? $dta[$csv] : "";
		}
		$chk = strFn("str_replace", strtolower($ah->decode($d)), " ", "");*/
		//$chk_wrd = explode(",",$chk);
		foreach($heading as $h) {
			if (is_array($dta)){
				$d = array_key_exists($csv,$dta) ? $dta[$csv] : "";
			}else{
				$d = isset($csv,$dta) ? $dta[$csv] : "";
			}
			$chk = strFn("str_replace", strtolower($ah->decode($d)), " ", "");
			$ht = $h['type'];
			echo "<td>";
			switch($ht) {
				case "U":    //UDF
					$hf = $h['udfiid'];
					switch($h['udfilist']) {
						case "Y":
							$l = $lists[$hf];
							$c = $checks[$hf];
							if(strlen($d) > 0) {
								$chk = strFn("str_replace", strtolower($ah->decode($d)), " ", "");
								if(isset($c[$chk])) {
									$cid = $c[$chk];
								} else {
									$cid = "X";
								}
							} else {
								$cid = "X";
							}
							echo "<select name=" . $hf . "[]><option value=X";
							if($cid == "X") {
								echo " selected";
							}
							echo ">--- SELECT ---</option>";
							foreach($l as $m) {
								echo "<option value=" . $m['udfvid'];
								if($cid == $m['udfvid']) {
									echo " selected";
								}
								echo ">" . ASSIST_HELPER::decode($m['udfvvalue']) . "</option>";
							}
							echo "</select>";
							if($cid == "X" && strlen($d) > 0) {
								echo "<br /><span class=notfound>Value not found:</span><br />&nbsp;&nbsp;$d";
							}
							break;
						case "D":
							$d = validateDate($d);
							echo "<input type='text' size='10' value='$d' name='" . $hf . "[]' class='dateme'>";
							break;
						case "T":
							echo "<input type=text size=25 value=\"$d\" name=\"" . $h['udfiid'] . "[]\">";
							break;
						case "M":
							echo "<textarea name=\"" . $h['udfiid'] . "[]\" rows=3 cols=20>$d</textarea>";
							break;
						case "N":
							$d = validateNum($d);
							echo "<input type=text size=15 name=\"" . $h['udfiid'] . "[]\" value=\"$d\"><br /><span style=\"font-size: 6.5pt; line-height: 7pt; font-style: italic;\">Numbers only</span>";
							break;
					}
					break;
				default:
					$hf = $h['field'];
					switch($hf) {
						case "tasktopicid":
							$l = $lists[$hf];
							$c = $checks[$hf];
							$d = strtolower($d);
							if(isset($c[$d])) {
								$cid = $c[$d];
							} else {
								$cid = "X";
							}
							echo "<select name=" . $hf . "[] id=" . $hf . $count . " onchange=\"Verify(this,'sel');\"><option value=X";
							if($cid == "X") {
								echo " selected";
							}
							echo ">--- SELECT ---</option>";
							foreach($l as $m) {
								echo "<option value=" . $m['id'];
								if($cid == $m['id']) {
									echo " selected";
								}
								echo ">" . ASSIST_HELPER::decode($m['value']) . "</option>";
							}
							/*if($cid == "NEW") { 
								echo "<option value=\"NEW".$count."\" selected>NEW - ".$d."</option>"; 
								$lists[$hf]['NEW'.$count]['id'] = "NEW".$count;
								$lists[$hf]['NEW'.$count]['value'] = "NEW - ".$d;
								$checks[$hf][strFn("str_replace",strtolower(decode($d))," ","")] = "NEW".$count;
								echo "<input type=hidden name=newtopic[] value=\"NEW".$count."|_|".$d."\">";
							}*/
							echo "</select>";
							if($cid == "X") {
								$error[] = array('sel', $hf . $count);
								if(strlen($chk) > 0) {
									echo "<br /><span class=notfound>Value not found:</span><br />&nbsp;&nbsp;$d";
								}
							}
							break;
						case "taskurgencyid":
							if(strlen($d) == 0) {
								$d = "Normal";
							}
							$l = $lists[$hf];
							$c = $checks[$hf];
							$chk = strFn("str_replace", strtolower($ah->decode($d)), " ", "");
							if(isset($c[$chk])) {
								$cid = $c[$chk];
							} else {
								$cid = "X";
							}
							echo "<select name=" . $hf . "[] id=" . $hf . $count . " onchange=\"Verify(this,'sel');\"><option value=X";
							if($cid == "X") {
								echo " selected";
							}
							echo ">--- SELECT ---</option>";
							foreach($l as $m) {
								echo "<option value=" . $m['id'];
								if($cid == $m['id']) {
									echo " selected";
								}
								echo ">" . ASSIST_HELPER::decode($m['value']) . "</option>";
							}
							echo "</select>";
							if($cid == "X") {
								$error[] = array('sel', $hf . $count);
							}
							break;
						case "taskaction":
							echo "<textarea name=" . $hf . "[] rows=3 cols=25 ";
							if(strlen($d) == 0) {
								echo " class=required";
							}
							echo " onchange=\"Verify(this,'txt');\">" . $d . "</textarea>";
							break;
						case "taskstatusid":
							if(strtolower($ah->decode($d)) == "on-going" || strtolower($ah->decode($d)) == "ongoing" || strtolower($ah->decode($d)) == "on going") {
								echo "New <input type=hidden size=5 value=5 name=" . $hf . "[]>";
							} else {
								echo "New <input type=hidden size=5 value=4 name=" . $hf . "[]>";
							}
							break;
						case "taskdeadline":    //EXPECTED FORMAT DD-MM-YYYY
							$d = validateDate($d);
							echo "<input type='text' name='" . $hf . "[]' size='10' id=\"" . $hf . $count . "\" readonly='readonly' value=\"$d\"/ class='date-type' onchange=\"Verify(this,'d');\">";
							if(strlen($d) == 0) {
								$error[] = array('d', $hf . $count);
							}
							break;
						case "taskadddate":
							echo date("d-M-Y");
							$csv--;
							$d = "";
							break;
						case "taskadduser":
							if(strlen($d) > 0) {
								$c = $checks[$hf];
								$chk = strFn("str_replace", strtolower($ah->decode($d)), " ", "");
								if(isset($c[$chk])) {
									$cid = $c[$chk];
								} else {
									$cid = "X";
								}
							} else {
								$cid = $tkid;
							}
							echo "<select name=" . $hf . "[] onchange=\"Verify(this,'sel');\" id=\"" . $hf . $count . "\" ><option value=X";
							if($cid == "X") {
								echo " selected";
							}
							echo ">--- SELECT ---</option>";
                            $tau = isset($tau) ? $tau : array();
							foreach($tau as $m) {
								echo "<option value=" . $m['id'];
								if($cid == $m['id']) {
									echo " selected";
								}
								echo ">" . ASSIST_HELPER::decode($m['value']) . "</option>";
							}
							echo "</select>";
							if($cid == "X") {
								echo "<br /><span class=notfound>User&nbsp;not&nbsp;identified:</span><br />" . $d;
								$error[] = array('sel', $hf . $count);
							}
							break;
						case "tasktkid":    //task recipient
							$notfound = array();
							$cid = count($cid) > 0 ? $cid : array();
							if(strlen($d) > 0) {
								$c = $checks[$hf];
								$usrs = strFn("explode", $d, ";","");
								foreach($usrs as $u) {
									$chk = strFn("str_replace", strtolower($ah->decode($u)), " ", "");
									if(isset($c[$chk])) {
										$cid[$c[$chk]] = "Y";
									} else {
										$notfound[] = $u;
									}
								}
							} else {
								$cid[$tkid] = "Y";
							}
							echo "<select name=" . $hf . $count . "[] id=\"" . $hf . $count . "\" multiple size=5 onchange=\"Verify(this,'tk');\">";
                            $tasktkid = isset($tasktkid) ? $tasktkid : array();
							foreach($tasktkid as $m) {
								echo "<option value=" . $m['id'];
								if(array_key_exists($m['id'],$cid) && $cid[$m['id']] == "Y") {
									echo " selected";
								}
								echo ">" . ASSIST_HELPER::decode($m['value']) . "</option>";
							}
							echo "</select>";
							if(count($notfound) > 0) {
								echo "<br /><span class=notfound>Users&nbsp;not&nbsp;identified:</span>";
							}
							foreach($notfound as $nf) {
								echo "<br />" . $nf;
							}
							$error[] = array('tk', $hf . $count);
							break;
						default:
							echo "<textarea name=" . $hf . "[] rows=3 cols=25>" . $d . "</textarea>";
							break;
					}
					break;
			}
            $csv++;
			//RESET VARIABLES
			$d = "";
			$chk = "";
			$cid = "";
			echo "</td>";
		}
		echo "</tr>";
		$count++;
	}
}
echo "</table>";
if($count>0) {
		echo "<p style=\"text-align:center;\"><input type=hidden name=count value=$count><input type=submit value=\"  Accept  \" style=\"color: #FFFFFF; padding: 10 10 10 10;\" id=accept class=enabled></p>";
		echo "<script type=text/javascript>";
		echo "var targ;";
		foreach($error as $e) {
			echo "targ = document.getElementById('".$e[1]."');";
			echo "Verify(targ,'".$e[0]."');";
		}
		echo "</script>";
}
?>
</body>
</html>