<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<script type=text/javascript>
function Validate(me) {
	if(me.ifile.value.length > 0) {
			return true;
	} else {
		alert("Please select the import file by clicking on the \"Browse\" button.");
	}
	return false;
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
	echo "<h1 class=fc>".$modtext.": New ".$actname."</b></h1>";
	echo "<form name=import id=imp action=new_multiple_import.php method=post onsubmit=\"return Validate(this);\" language=jscript enctype=\"multipart/form-data\">";
	echo "<input type=hidden name=act value=CHECK>";
	echo "<ol>";
	/*<li>Ensure that all of the <a href=setup_lists.php>lists</a> include those list items in the Top Level:
		<ul>
			<li><a href=setup_dir.php>Directorates</a></li>
			<li><a href="setup_lists_config.php?l=munkpa">Municipal KPA</a></li>
			<li><a href="setup_lists_config.php?l=gfs">GFS Classification</a></li>
			<li><a href="setup_lists_config.php?l=wards">Wards</a></li>
			<li><a href="setup_lists_config.php?l=area">Area</a></li>
		</ul>
	</li>*/
	echo "<li>Generate an import Template: <input type=button value=\"  Go  \" onclick=\"document.location.href = 'new_multiple_generate.php';\"></li>";
	echo "<li>Copy the data into the template.  Please note the following:";
		echo "<ul>";
			echo "<li>Keep the template in CSV format.</li>";
			echo "<li>The first 2 rows will be ignored as headings.</li>";
			echo "<li>Required fields are marked with a *.</li>";
			echo "<li>Note the formatting guidelines given in Row 2.</li>";
		echo "</ul>";
	echo "</li>";
	echo "<li>Import the updated template: <input type=file name=ifile id=ife> <input type=submit value=Import></li>";
	echo "<li>Review the data as imported by the system and click \"Accept\" to finalise the import.<br />Any lines with import errors will be highlighted in <span style=\"color: #CC0001\">RED</span>.  Until all errors are attended to the \"Accept\" button will not be available.  <br />You <u>must</u> click the \"Accept\" button to finalise the import process.</li>";
echo "</ol>";
echo "</form>";
echo "<p>&nbsp;</p>";
?>
</body>
</html>