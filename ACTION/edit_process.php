<?php
//error_reporting(-1);
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Edit"),
);
$page = array("edit");
$get_udf_link_headings = false;
require_once("inc_header.php");
//SSIST_HELPER::arrPrint($_REQUEST);
//SSIST_HELPER::arrPrint($_FILES);
$modRefs = $_REQUEST;
foreach($_REQUEST as $index=>$value){
	if(stripos($index,"docModRef")===false){
		unset($modRefs[$index]);
	}
}
//SSIST_HELPER::arrPrint($modRefs);
?>
<script type="text/javascript" >
	$(function() {
		AssistHelper.processing();
	});
</script>
<?php
$taskid = $_REQUEST['taskid'];

    //this is just to maintain the code in the form it is...
    $tref = $ah->getModRef();
    $udfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    //include("inc_db_con.php");
	$rows = $ah->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs)) {
	foreach($rows as $row){
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        //include("inc_db_con2.php");
        $udf = $ah->db_get_num_rows($sql2);
        $row2 = $ah->mysql_fetch_one($sql2);
        //mysql_close($con2);
        $udfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if($ah->checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    //include("inc_db_con2.php");
                    if($ah->db_get_num_rows($sql2)>0) {
                        $row3 = $ah->mysql_fetch_one($sql2);
                    }
                    //mysql_close($con2);
                }
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                //include("inc_db_con2.php");
			$rows = $ah->mysql_fetch_all($sql2);
                //while($row2 = mysql_fetch_array($rs2)) {
			foreach($rows as $row2){
                    if($row3['udfvid'] == $row2['udfvid'])
                        $udfFields[$row['udfivalue']] = $row2['udfvvalue'];
                }
                //mysql_close($con2);
                break;
            case "T":
			case "M":
			case "D":
                $udfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
                break;
        }
    }//crazy code ends here..

    //get the original task record before editing.
    $sql = "SELECT * FROM ".$dbref."_task WHERE taskid=$taskid"; $linenumber = __LINE__;
    //include("inc_db_con.php");
    $linenumber = 0;
    $row = $ah->mysql_fetch_one($sql);
    $taskstate = $row['taskstate'];
    if($_POST['taskstatusid'] == 4) {
        $taskstate = 0;
    }else if($_POST['taskstatusid'] == 1) {
        $taskstate = 100;
    }
    /*
     * If the Task Status is changed from �On-going?? to something else the taskstate field must be set to 0.
    */
    if($row['taskstatusid'] == 5 && $_POST['taskstatusid'] != 5) {
        $taskstate = 0;
    }
    /*
     * If the Task status is changed from �Complete?? to any other status then the taskstate field must be returned to the previous state
     * (as per the most recent _log record before the one that closed the task)
    */
    $statusid = $_POST['taskstatusid'];
    if($row['taskstatusid'] == 1 && $_POST['taskstatusid'] != 1) {
        $sql = "SELECT * FROM ".$dbref."_log WHERE logtaskid=$taskid ORDER BY logdate DESC LIMIT 2";
        //include("inc_db_con.php");
		$rs = $ah->db_query($sql);
        mysqli_data_seek($rs, 1);
        $rowR = $ah->mysql_fetch_one($sql);
        $taskstate = $rowR['logstate'];
    }
    $taskdeadline = strtotime($_POST['datepicker']);
    $taskdeliver = $ah->code($_POST['taskdeliver']);
    $taskaction = $ah->code($_POST['taskaction']);
    $sql = "UPDATE ".$dbref."_task SET tasktkid='',tasktopicid=".$_POST['tasktopicid'].",
        taskurgencyid=".$_POST['taskurgencyid'].",taskstatusid=".$_POST['taskstatusid'].",taskdeadline='".$taskdeadline."',
          taskaction='".$taskaction."',taskdeliver='".$taskdeliver."',taskstate=$taskstate
              WHERE taskid=".$_POST['taskid'];
    //include("inc_db_con.php");
	$ah->db_update($sql);
    /*updating recipients
     * first get the original assignees into array,then delete them from the recipients table,
     * insert the new recipients
    */
    //get the old assignees to a task,this will be used later when updating somewhere below.

    $oldRecipients = array();
    $sql = "SELECT * FROM ".$dbref."_task_recipients WHERE taskid=".$_POST['taskid'];
    //include("inc_db_con.php");
	$rows = $ah->mysql_fetch_all($sql);
    //while($rowO = mysql_fetch_array($rs)) {
	foreach($rows as $rowO){
        array_push($oldRecipients,$rowO['tasktkid']);
    }

    //get the new assignees to the task,this will be used later when updating somewhere below.
    $newRecipients = array();
    if(is_array($_POST['tasktkid'])) {
        $newRecipients = $_POST['tasktkid'];
    }else {
        $newRecipients[] = $_POST['tasktkid'];
    }
    //remove the old recipients to this task.
    $sql = "DELETE FROM ".$dbref."_task_recipients WHERE taskid=".$_POST['taskid'];
    //include("inc_db_con.php");
	$ah->db_query($sql);

    //insert the new recipients for this task
    $sql = "INSERT INTO ".$dbref."_task_recipients (taskid,tasktkid) VALUES ";
    foreach($newRecipients as $value) {
        $sql .= "($taskid,'$value'),";
    }
    $sql = substr($sql, 0,-1);
    //include("inc_db_con.php");
	$ah->db_insert($sql);

    $taskaction = $_POST['taskaction'];
    $taskdeliver = $_POST['taskdeliver'];
    $logdate = $ah->getToday();
    $logtkid = $tkid;
    $updator = $ah->getUserName();//getTK($tkid, $cmpcode, 'tkn');

    $updates = "";
    $usersChanges ="";
    $logupdate = "The ".strtolower($actname)." has been edited by <i>$updator</i>.<br/>";
    /*construct logupdate bits for the changes in recipients
      * making use of @newRecipients array and @oldRecipients array
    */

    $removedUsers = array_diff($oldRecipients,$newRecipients);
    if(!empty($removedUsers)) {
        $usersChanges .= "The following user(s) were removed from working on ".strtolower($actname)." id ".$taskid."<br />";
        foreach($removedUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".$ah->getAUserName($tasktkid)."</i><br />";
        }
    }
    $newUsers = array_diff($newRecipients,$oldRecipients);
    if(!empty($newUsers)) {
        $usersChanges .= "The following user(s) were added to work on ".strtolower($actname)." id ".$taskid."<br />";
        foreach($newUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".$ah->getAUserName($tasktkid)."</i><br />";
        }
    }
    //send emails to all affected users
    $toEmails = array();
    foreach ($newRecipients as $key => $value) {
        //if(isset($_POST['sendEmail'])) {
        //    array_push($toEmails,$value);
        //}else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
       // }
    }
    foreach ($oldRecipients as $key => $value) {
      //  if(isset($_POST['sendEmail'])) {
      //      array_push($toEmails,$value);
     //   }else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
       // }
    }
    if(isset($_POST['sendEmail'])){
            array_push($toEmails,$tkid);
        }
    if(strtolower($cmpcode) != 'wcc0001' && !empty($toEmails)) {
        $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=";
        for ($i = 0; $i < count($toEmails);$i++) {
            if($i < count($toEmails) - 1)
                $sql .= "'".$toEmails[$i]."' OR t.tkid=";
            else
                $sql .= "'".$toEmails[$i]."'";
        }
        //include("inc_db_con.php");
		$rows = $ah->mysql_fetch_all($sql);
        $strTo = "";
        //while($rowRusers = mysql_fetch_array($rs)) {
		foreach($rows as $rowRusers){
            $strTo .= $rowRusers['tkemail'].",";
        }
        $strToEmails = substr($strTo,0,-1);

        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=".$row['taskadduser'];
        //include("inc_db_con.php");
        $rowadduser = $ah->mysql_fetch_one($sql);
        $from = $rowadduser['tkemail'];
        $sql = "SELECT * FROM ".$dbref."_list_urgency u WHERE id=".$row['taskurgencyid'];
        //include("inc_db_con.php");
        $rowurg = $ah->mysql_fetch_one($sql);

        $sql = "SELECT * FROM ".$dbref."_list_topic t WHERE id=".$row['taskurgencyid'];
        //include("inc_db_con.php");
        $rowtopic = $ah->mysql_fetch_one($sql);
        $userFrom = $rowadduser['tkname']." ".$rowadduser['tksurname'];
        $subject = "Task with id ".$taskid." on Ignite Assist edited.";

		$message = "";
        $message .= $rowadduser['tkname']." ".$rowadduser['tksurname']." has made some changes on ".strtolower($actname)." with id ".$taskid.":\n";
        $message .= "Prority: ".$rowurg['value']."\n";
        $message .= "Topic: ".$rowtopic['value']."\n";
        $taskaction = preg_replace('/(&#39)/', "'", stripslashes($taskaction));
        $taskdeliver = preg_replace('/(&#39)/', "'", stripslashes($taskdeliver));
        //$taskaction = html_entity_decode(stripslashes($taskaction));
       // $taskdeliver = html_entity_decode(stripslashes($taskdeliver));
        $message .= ucfirst($actname)." Instructions:\n".$taskaction."\n";
        $message .= ucfirst($actname)." Deliverables:".(empty($taskdeliver) ? "N/A" : "\n".$taskdeliver)."\n";
        $message .= "Deadline: ".date("d F Y",$taskdeadline)."\n";
        $message .= "Please log onto Assist in order to update this ".strtolower($actname).".\n";
        $message = nl2br($message);
        //Send email
if(strtoupper($cmpcode)!="IASSIST") {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: no-reply@ignite4u.co.za' . "\r\n";
//        $headers .= 'From: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
} else {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: no-reply@ignite4u.co.za' . "\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
}
//        ini_set("sendmail_from",$from);
if(strtoupper($cmpcode)!="IASSIST") {
        mail($strToEmails,$ah->decode($subject),$message,$headers);
}
        // echo ( mail($strToEmails,$subject,$message,$headers) ? "success" : "failed");
    }
    if($row['tasktopicid'] != $_POST['tasktopicid']) {
        $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id=".$row['tasktopicid'];
        //include("inc_db_con.php");
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id=".$_POST['tasktopicid'];
        //include("inc_db_con.php");
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />Task Topic changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
    }
    if($row['taskurgencyid'] != $_POST['taskurgencyid']) {
        $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE id=".$row['taskurgencyid'];
        //include("inc_db_con.php");
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE id=".$_POST['taskurgencyid'];
        //include("inc_db_con.php");
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />".ucfirst($actname)." Priority changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
    }
    if($row['taskaction'] != $taskaction) {
        if($row['taskaction'])
            $updates .= "<br />".ucfirst($actname)." instructions changed from  <i> ".$row['taskaction']." </i>  to <i>$taskaction</i>";
        else
            $updates .= "<br />".ucfirst($actname)." instructions changed to <i>$taskaction</i>";
    }
    if($row['taskdeliver'] != $taskdeliver) {
        if($row['taskdeliver'])
            $updates .= "<br />".ucfirst($actname)." deliverables changed from  <i> ".$row['taskdeliver']." </i>  to <i>$taskdeliver</i>";
        else
            $updates .= "<br />".ucfirst($actname)." deliverables changed to <i>$taskdeliver</i>";
    }

    if($row['taskstatusid'] != $_POST['taskstatusid']) {
        $sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey=".$row['taskstatusid'];
        //include("inc_db_con.php");
        $row0 = $ah->mysql_fetch_one($sql);
        $taskstate0 = $row0['state'];
        $sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey=".$_POST['taskstatusid'];
        //include("inc_db_con.php");
        $row1 = $ah->mysql_fetch_one($sql);
        $taskstate1 = $row0['state'];
        $updates .= "<br />".ucfirst($actname)." status has been changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
        //$updates .= "<br />Task state changed from $taskstate0 to $taskstate1";
    }
    if($row['taskdeadline'] != $taskdeadline) {
        $updates .= "<br />".ucfirst($actname)." deadline changed from ".date("d-M-Y",$row['taskdeadline'])." to ".date("d-M-Y",$taskdeadline);
    }

    //uploads
    $original_filename = "";
    $system_filename = "";
    $attachments = "";
    $modRefs = $tempReq = $_REQUEST;
	$metaFields = array();
	
	for($x=0; $x<count($_REQUEST);$x++){
		if(stripos(key($_REQUEST),"docModRef")===false){
    		unset($modRefs[key($_REQUEST)]);
    	}else{
    		//$vl = $tempReq[$index];
    		//$metaFields[] = array($_REQUEST[$index],next($_REQUEST),next($_REQUEST),next($_REQUEST),next($_REQUEST),next($_REQUEST));
    		//array_
    		//echo $x;
    		if(current($_REQUEST)!=="DefaultMod"){	    			
	    		$metaFields[] = array_slice($tempReq,$x+1,6,true);
    		}else{
    			$metaFields[] = array();
    		}
    	}
		next($_REQUEST);
	}
	
	foreach($metaFields as $indx=>$arr){
		foreach($arr as $k=>$vl){
			$tmp = explode("->",$k);
			unset($arr[$k]);
			$arr[$tmp[0]]=$vl;
		}
		$metaFields[$indx]=$arr;
	}
	
	//SSIST_HELPER::arrPrint(array("metafields"));			
	//SSIST_HELPER::arrPrint($metaFields);			
	//ASSIST_HELPER::arrPrint(array("modrefs"));
	//ASSIST_HELPER::arrPrint($modRefs);
	$defaultAttachIDs = array();
	
    if(isset($_FILES)) {
    	if(count($modRefs)==0){
    		$modRefs = array("noDocMods"=>"DefaultMod");
    	}
    	$cntr = 0;	
        foreach($modRefs as $val){
        	if($val == "DefaultMod"){
	        	$folder = "../files/$cmpcode/".$_SESSION['modref']."/action";
				$ah->checkFolder($_SESSION['modref']."/action");
	            if($_FILES['attachments']['error'][$cntr] == 0) {
	                $original_filename = $_FILES['attachments']['name'][$cntr];
	                $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
	                $sql = "INSERT INTO ".$dbref."_task_attachments (taskid,logid,original_filename,system_filename,file_location) VALUES ($taskid,0,'$original_filename','','edit')";
	                $docid = $ah->db_insert($sql);
					$system_filename = $taskid."_".$docid."_".date("YmdHis").".$ext";
					$full_path = $folder."/".$system_filename;
					move_uploaded_file($_FILES['attachments']['tmp_name'][$cntr], $full_path);
					$ah->db_update("UPDATE ".$dbref."_task_attachments SET system_filename = '$system_filename' WHERE id = $docid");
					$attachments.= $original_filename."<br />";
					$defaultAttachIDs[] = $docid;
	            }else{
	            	//error
	            }     		
        	}else{
	            if($_FILES['attachments']['error'][$cntr] == 0) {
	        		$doc = new DOC($val);
	        		$modTexts = $doc->getDocInstancesForSelect();
					$result[$cntr] = $doc->addActionDocWithMeta($actname, $taskaction, $taskid, $_FILES['attachments']['name'][$cntr], $_FILES['attachments']['tmp_name'][$cntr],$metaFields[$cntr]);
					if($result[$cntr][0]=="ok"){
						$attachments.= $_FILES['attachments']['name'][$cntr]." (Stored in ".$modTexts[$val].")<br />";						
					}else{
						//error uploading doc
					}
	            }else{
	            	//error
	            }     		
        	}	
        	$cntr ++;
        }   
    }
    if($attachments != "") {
        $updates .= "<br />".ucfirst($actname)." Attachment(s):<br /> $attachments";
    }
    if($usersChanges != "") {
        $updates = $usersChanges.$updates;
    }
    if($updates != "") {
        $logupdate .= " The following changes were made: <br />$updates";
    }

    $logupdate = addslashes(htmlentities($logupdate));
    //echo $updates;die;
    $logstatusid = $_POST['taskstatusid'];
    $logstate = $taskstate;
    $logemail = 'N';
    $logsubmittkid = $tkid;
    $logtaskid = $_POST['taskid'];
    $logtasktkid = '';
    $sql = "INSERT INTO ".$dbref."_log
            (logdate,logtkid,logupdate,logstatusid,logstate,logemail,logsubmittkid,logtaskid,logtasktkid,logactdate,logtype) VALUES
            ($logdate,$logtkid,'$logupdate',$logstatusid,$logstate,'$logemail','$logsubmittkid',$logtaskid,'$logtasktkid','$logdate','E')";
    //include("inc_db_con.php");
    $logid = $ah->db_insert($sql);//mysql_insert_id();

    //&*!@!12891edw&*7821
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    
    //include("inc_db_con.php");
    $udf = $ah->db_get_num_rows($sql);//mysql_num_rows($rs);
    if($udf>0) {
        $u = 0;
        $rows = $ah->mysql_fetch_all($sql);
        //while($row = mysql_fetch_array($rs)) {
		foreach($rows as $row){
            $udfindex[$u] = $row['udfiid'];
            $u++;
        }
    }

    //mysql_close();

	//Post-log creation modification of *_task_attachments file IF associated with a 'default' attachment
	if(count($defaultAttachIDs)>0){
		$sql = "UPDATE ".$dbref."_task_attachments SET logid = '$logid' WHERE id IN (".implode(", ",$defaultAttachIDs).")";
		//echo $sql;
		$logLink = $ah->db_update($sql);
	}else{
		$logLink = "No defaultAttachIDS";
	}
    //echo $logLink;
    
    //GET UDF DATA
    if($udf>0) {
        foreach($udfindex as $udfi) {
            $udfval[$udfi] = htmlspecialchars($_POST[$udfi],ENT_QUOTES);//str_replace("'","&339",$_POST[$udfi]);
        }
    }
    
    //$udfidArr = array();
    //echo '<pre>';
   // print_r($_POST);die;
    foreach($_POST as $key => $value) {
        if(is_int($key)) {
            $sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfindex=$key AND udfnum=".$_POST['taskid']." AND udfref='".$modref."'";
            //include("inc_db_con.php");
            if($ah->db_get_num_rows($sql) > 0){
                $sql = "UPDATE assist_".$cmpcode."_udf SET";
                $sql.= " udfvalue='".$_POST[$key]."' WHERE udfindex=$key AND udfnum=".$_POST['taskid']." AND udfref='".strtoupper($modref)."'";
                //include("inc_db_con.php");
				$ah->db_update($sql);
            }else{
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=$key,udfvalue='".$_POST[$key]."',udfnum=".$_POST['taskid'].",udfref='".strtoupper($modref)."'";
                //include("inc_db_con.php");
				$ah->db_insert($sql);
            }
           /*
            if($key == -1) {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                 $udfindex = $row['udfiid'];
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=$udfindex,udfvalue='".$_POST[$key]."',udfnum=".$_POST['taskid'].",udfref='".$modref."'";
                include("inc_db_con.php");
            }else {
                $sql = "UPDATE assist_".$cmpcode."_udf SET";
                $sql.= " udfvalue = '".$value."' WHERE udfid=$key";               
                include("inc_db_con.php");
            }*/
        }
    }
    
    $afterUdfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    //include("inc_db_con.php");
	$rows = $ah->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs)) {
	foreach($rows as $row){
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        //include("inc_db_con2.php");
        $udf = $ah->db_get_num_rows($sql2);
        $row2 = $ah->mysql_fetch_one($sql2);
        //mysql_close($con2);
        $afterUdfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if($ah->checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    //include("inc_db_con2.php");
                    if($ah->db_get_num_rows($sql2)>0) {
                        $row3 = $ah->mysql_fetch_one($sql2);
                        //echo($row3['udfvvalue']);
                    }
                    //mysql_close($con2);
                }
                //echo("<select name=".($row2['udfid'] == "" ? "-1" : $row2['udfid'])."><option value=X>---SELECT---</option>");
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                //include("inc_db_con2.php");
				$rows2 = $ah->mysql_fetch_all($sql2);
                //while($row2 = mysql_fetch_array($rs2)) {
				foreach($rows2 as $row2){
                    //echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                    if($row3['udfvid'] == $row2['udfvid'])
                        $afterUdfFields[$row['udfivalue']] = $row2['udfvvalue'];
                }
                //mysql_close($con2);
                //echo("</select>");
                break;
            case "T":
			case "D":
			case "M":
                $afterUdfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
                break;
        }
    }
    $logupdate = "";
    foreach($udfFields as $key => $value) {
        if($value != $afterUdfFields[$key])
            $logupdate .= $key." changed ".($value != null ? "from " : "" )."<i>".$value."</i> to <i>".$afterUdfFields[$key]."</i><br />";
    }
    $sql = "UPDATE ".$dbref."_log SET logupdate=CONCAT_WS('<br />',logupdate,'$logupdate') WHERE logid=$logid";
    //echo $sql;die;
    //include("inc_db_con.php");
	$ah->db_update($sql2);
    /*$referral_page = $_POST['referal_page'];
        if(strlen($referral_page)==0 || !file_exists($referral_page.".php")) { $referral_page = "view"; }	//Due to error found in some browsers
    echo "<script type='text/javascript'>document.location.href='$referral_page.php?i='+$taskid</script>";*/
	if(isset($_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'])) {
		$url = "view_list.php?get_details=1";
	} else {
		$url = "view.php";
	}
	//echo "<br><strong>".$url."</strong><br>";
	echo "<script type='text/javascript'>document.location.href='$url';</script>";
?>