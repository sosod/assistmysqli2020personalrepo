<?php
$title = array(
	array('txt'=>"Advanced Search"),
);
$page = array("search");
$get_udf_link_headings = false;
require_once("inc_header.php");



$view_type = $_REQUEST['act'];
switch($view_type) {
case "MY":	$title = "My ".$actname."s"; break;
case "OWN":	$title = "".$actname."s I Assigned"; break;
case "ALL":	$title = "All ".$actname."s"; break;
}

$tact = getUserAccess();

?>
        <form name=search method=post action=view_list.php>
			<!-- <input type=hidden name=s value='<?php echo $_REQUEST['s']; ?>' /> -->
			<input type=hidden name=act value='<?php echo $_REQUEST['act']; ?>' />
			<input type=hidden name=orderby value='<?php echo isset($_REQUEST['orderby']) ? $_REQUEST['orderby'] : "taskdeadline"; ?>' /> 
			<input type=hidden name=search[type] value='ADVANCED' />
            <table id=tbl_action>
                <tr>
                    <th>Created On:</th>
                    <td><input type=text size=10 class=jdate2012 name="taskadddate[from]" readonly="readonly" /> - <input type=text size=10 class=jdate2012 name="taskadddate[to]" readonly="readonly" /></td>
                </tr>
                <tr>
                    <th>Assigned By:</th>
                    <td><?php 
					switch($view_type) {
					case "MY":
					case "ALL":
						$sql = "SELECT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as value
								FROM assist_".$cmpcode."_timekeep tk
								INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
								  ON tk.tkid = mmu.usrtkid
								  AND mmu.usrmodref = '".$_SESSION['modref']."'
								INNER JOIN ".$dbref."_list_access a
								  ON a.tkid = tk.tkid
								  AND (a.act > 20 
									OR a.tkid = '".$_SESSION['tid']."'
								  )
								WHERE tk.tkstatus = 1";
						$taskaddusers = $ah->mysql_fetch_all($sql);
						echo "<select name=taskadduser[] multiple size=5>";
							foreach($taskaddusers as $ta) {
								echo chr(10)."<option value=".$ta['id'].">".$ta['value']."</option>";
							}
						echo "</select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>";
						break;
					case "OWN":
						echo $tkname;
						break;
					}
					?></td>
                </tr>
                <tr>
                    <th>Assign To:</th>
                    <td><?php 
					switch($view_type) {
					case "OWN":
					case "ALL":
						$sql = "SELECT DISTINCT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as value
								FROM assist_".$cmpcode."_timekeep tk
								INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
								  ON tk.tkid = mmu.usrtkid
								  AND mmu.usrmodref = '".$_SESSION['modref']."'
								INNER JOIN ".$dbref."_task_recipients tr
								  ON tk.tkid = tr.tasktkid
								WHERE tk.tkstatus = 1";
						$tasktkid = $ah->mysql_fetch_all($sql);
						echo "<select name=tasktkid[] multiple size=5>";
							foreach($tasktkid as $ta) {
								echo chr(10)."<option value=".$ta['id'].">".$ta['value']."</option>";
							}
						echo "</select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>";
						break;
					case "MY":
						echo $tkname;
						break;
					}
					?></td>
                </tr>
                <tr>
                    <th>Topic:</th>
                    <td>
                        <select name="tasktopicid[]" id=tasktopicid multiple size=5>
<?php
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                            //$rs = getRS($sql);
							$rows = $ah->mysql_fetch_all($sql);
                            //while($row = mysql_fetch_array($rs)) {
                            foreach($rows as $row){
                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                            }
                            //unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Priority:</th>
                    <td>
                        <select name="taskurgencyid[]" multiple size=3><?php
							$sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
                            //$rs = getRS($sql);
							$rows = $ah->mysql_fetch_all($sql);
                            //while($row = mysql_fetch_array($rs)) {
                            foreach($rows as $row){
                                echo "<option value=".$row['id'].">".$row['value']."</option>";
                            }
                            //unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td>
                        <select name="taskstatusid[]" multiple size=5><?php
							$sql = "SELECT * FROM ".$dbref."_list_status WHERE id <> 'CN' ORDER BY sort";
                            //$rs = getRS($sql);
							$rows = $ah->mysql_fetch_all($sql);
                            //while($row = mysql_fetch_array($rs)) {
                            foreach($rows as $row){
                                echo "<option value=".$row['pkey'].">".$row['value']."</option>";
                            }
                            //unset($rs);
                            ?>
                        </select>
						<br /><span class=note>Ctrl + Click to select multiple options</span>
                    </td>
                </tr>
                <tr>
                    <th>Deadline:</th>
                    <td><input type=text size=10 class=jdate2012 name="taskdeadline[from]" readonly="readonly" /> - <input type=text size=10 class=jdate2012 name="taskdeadline[to]" readonly="readonly" /></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><input type=text name='taskaction[search]' value='' /> <select name='taskaction[type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><input type=text name='taskdeliver[search]' value='' /> <select name='taskdeliver[type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select></td>
                </tr>

<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') ORDER BY udfisort, udfivalue";
//$rs = getRS($sql);
$rows = $ah->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
	$class = $row['udfiobject']." ".(strlen($row['udfilinkfield'])>0 ? $row['udfilinkfield']." ".$row['udfilinkref'] : "");
	echo "<tr class='udf $class'>
		<th>".$row['udfivalue'].":</th>
		<td>";
	switch($row['udfilist']) {
	case "Y":
		$sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
		//$rs2 = getRS($sql2);
		$rows2 = $ah->mysql_fetch_all($sql2);
		echo "<select name='udf[".$row['udfiid']."][]' size=5 multiple><option value=0>[Blank]</option>";
		//while($row2 = mysql_fetch_array($rs2)) {
		foreach($rows2 as $row2){
			echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
		}
		unset($rs2);
		echo "</select><br /><span class=note>Ctrl + Click to select multiple options</span>";
		break;
	case "T":
	case "M":
		echo "<input type=text name='udf[".$row['udfiid']."][search]' /> <select name='udf[".$row['udfiid']."][type]'><option selected value=ANY>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select>";
		break;
	case "D":
		echo "<input class='jdate2012'  type='text' name='udf[".$row['udfiid']."][from]' size='10' readonly='readonly' /> - <input class='jdate2012'  type='text' name='udf[".$row['udfiid']."][to]' size='10' readonly='readonly' />";
		break;
	case "N":
		 echo "<input type='text' name='udf[".$row['udfiid']."][from]' size='15' class=numb /> - <input type='text' name='udf[".$row['udfiid']."][to]' size='15' class=numb />&nbsp;<br /><span class=note>Only numeric values are allowed.</span>";
		break;
	case "CUSTOMER":
        $obj = new MASTER_EXTERNAL("CUSTOMER");
        echo "<select name=udf[".$row['udfiid']."][] size=5 multiple>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;
    case "SUPPLIER":
        $obj = new MASTER_EXTERNAL("SUPPLIER");
        echo "<select name=udf[".$row['udfiid']."][] size=5 multiple>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;
    case "EMPLOYEE":
        $obj = new MASTER_EXTERNAL("EMPLOYEE");
        echo "<select name=udf[".$row['udfiid']."][] size=5 multiple>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;

	default:
		echo "<input type=text name='udf[".$row['udfiid']."]' size='50'>";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th><?php echo $actname; ?> Attachments:</th>
                    <td><select name=attach>
						<option selected value=X>Doesn't matter</option>
						<option value=1>Must have attachments</option>
						<option value=0>Must not have attachments</option>
					</select></td>
                </tr>
                <tr>
					<th></th>
                    <td>
                        <input type="submit" value="Search" class=isubmit />
                        <input type="reset" value="Reset" name="B2" />
					</td>
                </tr>
            </table>
        </form>
<script type=text/javascript>
$(function() {
	$("th").addClass("left").addClass("top");
	$("td").addClass("top");
	$("tr").off("mouseenter mouseleave");
	$("#h_title").prepend("<?php echo "<a href=view_list.php?s=".$_REQUEST['s']."&act=".$_REQUEST['act']." class=breadcrumb>".$title."</a> >> "; ?>");
	$(".note").css("color","#fe9900").css("font-style","italic").css("font-size","6.5pt");
	
	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});
	
});
</script>
<?php $ah->displayGoBack(); ?>
    </body>
</html>

