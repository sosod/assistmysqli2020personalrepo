<!--
/*
 * Read README file first.
 *
 * This example shows how to create a simple pie chart with a few configuration
 * directives and a connected HTML table as legend.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
*/
-->
<?php

include("inc_ignite.php");



$sql = "SELECT s.pkey,s.value,s.heading FROM
        assist_".$cmpcode."_ta_list_status s LEFT OUTER JOIN assist_".$cmpcode."_ta_task t ON
	(s.pkey=t.taskstatusid)  WHERE s.yn='Y' GROUP BY s.pkey ORDER BY sort";
//include("inc_db_con.php");
$rows = $ah->mysql_fetch_all($sql);
$allStatuses["my"] = array();
$allStatuses["own"] = array();
$allStatuses["all"] = array();
$statuses = array();
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
    $allStatuses["my"][$row['pkey']] = array("title"=>$row['heading'],'count'=>0,'value'=>$row['value']);
    $statuses[$row['pkey']] = $row['heading'];
}
$allStatuses["all"] = $allStatuses["own"] = $allStatuses["my"];

$sql = "SELECT s.pkey,COUNT(t.taskid) AS cnt, s.heading
FROM assist_".$cmpcode."_ta_list_status s 
    INNER JOIN  assist_".$cmpcode."_ta_task t ON (s.pkey=t.taskstatusid)
    INNER JOIN  assist_".$cmpcode."_ta_task_recipients r ON (t.taskid=r.taskid)
    WHERE  r.tasktkid = '".$tkid."' AND s.yn = 'Y'  GROUP BY s.pkey";
//include("inc_db_con.php");
$rows = $ah->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
    $allStatuses["my"][$row['pkey']]['count'] = $row['cnt'];
}

$sql = "SELECT s.pkey,s.value,s.heading,COUNT(t.taskid) AS cnt
FROM assist_".$cmpcode."_ta_list_status s
LEFT OUTER JOIN assist_".$cmpcode."_ta_task t ON (s.pkey=t.taskstatusid) WHERE s.yn='Y' AND t.taskadduser='".$tkid."' GROUP BY s.pkey";
//include("inc_db_con.php");
$rows = $ah->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
    $allStatuses["own"][$row['pkey']]['count'] = $row['cnt'];
}
$sql = "SELECT s.pkey, COUNT(t.taskid) AS cnt, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t
WHERE t.taskstatusid = s.pkey AND s.yn = 'Y'
GROUP BY s.pkey";
//include("inc_db_con.php");

$rows = $ah->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
    $allStatuses["all"][$row['pkey']]['count'] = $row['cnt'];
}
//mysql_close($con);
$xml = "<?xml version='1.0' encoding='UTF-8'?>
        <chart>
            <series>
                <value xid='my'>My Tasks</value>
                <value xid='own'>Tasks I've assigned</value>
                <value xid='all'>All Tasks</value>
            </series>";
$xml .= "<graphs>";
foreach($statuses as $pkey => $value){
    $xml .= "<graph gid='".$pkey."' title='".$value."'>";
     $xml .= "<value xid='my'>".$allStatuses['my'][$pkey]['count']."</value>";
     $xml .= "<value xid='own'>".$allStatuses['own'][$pkey]['count']."</value>";
     $xml .= "<value xid='all'>".$allStatuses['all'][$pkey]['count']."</value>";
    $xml .= "</graph>";
}
$xml .= "</graphs></chart>";
file_put_contents("data.xml", $xml);
?>
<script type="text/javascript" src="../lib/amcharts-php-0.2.1/amcharts/amcolumn_1.6.4.2/amcolumn/swfobject.js"></script>
<div id="flashcontent">
    <strong>You need to upgrade your Flash Player</strong>
</div>

<script type="text/javascript">
    // <![CDATA[
    var so = new SWFObject("../lib/amcharts-php-0.2.1/amcharts/amcolumn_1.6.4.2/amcolumn/amcolumn.swf", "amcolumn", "520", "380", "8", "#FFFFFF");
    so.addVariable("path", "../lib/amcharts-php-0.2.1/amcharts/amcolumn_1.6.4.2/amcolumn/");
    so.addVariable("settings_file", encodeURIComponent("amcolumn_settings.xml"));
    so.addVariable("data_file", encodeURIComponent("data.xml"));
    so.addVariable("preloader_color", "#999999");
    so.write("flashcontent");
    // ]]>
</script>
<!-- end of amcolumn script -->


