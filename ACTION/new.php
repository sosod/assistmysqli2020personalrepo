<?php
$title = array(
	array('url'=>"new.php",'txt'=>"New"),
);
$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new");
$get_udf_link_headings = false;
require_once("inc_header.php");
$doc = new DOC();

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

//GET USER ACCESS - IF 20 THEN NEW TASKS ONLY TO SELF ELSE CAN ASSIGN TO OTHERS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."' AND yn='Y'";
//include("inc_db_con.php");
//$row = mysql_fetch_array($rs);
$row = $ah->mysql_fetch_one($sql);
$tact = isset($row['act']) ? $row['act'] : 20;
//mysql_close();
?>
<!-- <script type="text/javascript" src="js/new_task.js"></script> -->
        <style type="text/css">
			.metaFileIn{
				background-color : #edcfaa;
			}
			.metaFileOk{
				background-color : #aaedc0;
			}
		</style>
        
        <script type ="text/javascript">
            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                })
            });

        function Validate(tform){
            var form = document.forms['newtask'];
            var count = 0;
            var message = '';
            for(var i = 0; i < form.tasktkid.length;i++){
                if(form.tasktkid.options[i].selected){
                    count++;
                }
            }
            if(count == 0){
                if(form.elements['tact']){
                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }else{
                    message += "Please assign this task to at least 1 user.\n";
                }

            }
            if(tform.taskurgencyid.value == "X"){
                message += "Please select the task priority.\n";
            }
            if(tform.taskstatusid.value == "X"){
                message += "Please select task status.\n";
            }
            if(tform.datepicker.value == ""){
                message += "Please select task deadline date.\n";
            }
            if(tform.tasktopicid.value == "X"){
                message += "Please select task topic.\n";
            }
            if(tform.taskaction.value == ""){
                message += "Please enter task details.\n"
            }
            if(message != '')
            {
                alert(message);
                return false;
            }
            return true;
        }
    </script>
<?php $ah->displayResult($result); ?>
<p><input type=button value="Import <?php echo $actname; ?>" onclick="document.location.href = 'new_multiple.php';"></p>
        <form name=newtask method=post action=new_process.php enctype="multipart/form-data">
            <table id=tbl_action>
                <tr>
                    <th>Assigned By:</th>
                    <td><?php echo(isset($tkname) ? $tkname : $ah->getUserName()); ?>
				    	<input type=hidden size=5 name=redirect value=<?php echo $redirect; ?>>
                    	<input type=hidden size=5 name=taskadduser value=<?php echo($tkid); ?>>
                	</td>
                </tr>
                <tr>
                    <th id=th_tasktkid>Assign To:</th>
                    <td>
                        <?php 
/* INVEST1 TWEAK:
Added: March 2012
If the database is INVEST1 and it is not the investment admin logged in, then tasks can only be assigned to the investment admin.
*/
if(strtoupper($_SESSION['cc'])=="INVEST1" && $_SESSION['tid']!=$mod_admin) {
	$sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = $mod_admin";
	$row = $ah->mysql_fetch_one($sql);
	$mod_name = $row['name'];
	echo $mod_name."<input type=hidden name=tasktkid[] value='$mod_admin' />";
} else {
                        if($tact == 20) { //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                            echo($tkname."<input type=hidden size=5 name=tasktkid[] value=".$tkid." id=tasktkid>");
                           echo("<input type='hidden' name='tact' id='tact' value='$tact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
									FROM assist_".$cmpcode."_timekeep t
									INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
									  ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($modref)."'
									LEFT OUTER JOIN ".$dbref."_list_access a
									  ON t.tkid = a.tkid AND a.yn = 'Y'
									WHERE t.tkid <> '0000' AND t.tkuser <> 'support' AND t.tkstatus = 1
									ORDER BY t.tkname, t.tksurname";  
                            //$rs = getRS($sql);
							$rows = $ah->mysql_fetch_all($sql);
                            $size = $ah->db_get_num_rows($sql);
							//if($size>1) {
                            ?>
                        <select  id="tasktkid" class=i_am_required name="tasktkid[]" multiple="multiple" size="<?php echo($size > 10 ? 10 : $size);?>">
                            <!--<option value=X>--- SELECT ---</option>-->
                                <?php
                                //while($row = mysql_fetch_array($rs)) {
								foreach($rows as $row){
                                    echo("<option ".($size==1 ? "selected" : "")." name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                unset($rs);
                                ?>
                        </select><br /><i>Ctrl + left click to select multiple users</i><?php
							/*} else {
								$row = mysql_fetch_assoc($rs);
								echo $row['name']."<input type=hidden name=tasktkid[] value='".$row['tkid']."' />";
							}*/
                        }
}	//invest1 tweak
                        ?>
                    </td>
                </tr>
                <tr>
                    <th id=th_tasktopicid>Topic:</th>
                    <td>
                        <select name="tasktopicid" id=tasktopicid class=i_am_required>
                            <option value=0>--- SELECT ---</option>
<?php
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                            //$rs = getRS($sql);
							$rows = $ah->mysql_fetch_all($sql);
							//while($row = mysql_fetch_array($rs)) {
							foreach($rows as $row){
                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                            }
                            //unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskurgencyid>Priority:</th>
                    <td>
                        <select name="taskurgencyid" class=i_am_required>
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
							$rows = $ah->mysql_fetch_all($sql);
							//while($row = mysql_fetch_array($rs)) {
							foreach($rows as $row){
                                echo("<option ".($row['id']== 2 ? "selected='selected'" : '')." value=".$row['id'].">".$row['value']."</option>");
                            }
                            unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskstatusid>Status:</th>
                    <td>
                        <select name="taskstatusid"  class=i_am_required>
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM ".$dbref."_list_status WHERE (pkey = 4 OR pkey = 5) AND yn = 'Y' ORDER BY sort";
							$rows = $ah->mysql_fetch_all($sql);
							//while($row = mysql_fetch_array($rs)) {
							foreach($rows as $row){
                                if($row['pkey'] == "4") {
                                    echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                }
                                else {
                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                }
                            }
                            unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskdeadline>Deadline:</th>
                    <td><input type=text size=15 class='jdate2012 i_am_required' name=taskdeadline readonly=readonly /></td>
                </tr>
                <tr>
                    <th id=th_taskaction><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><textarea rows="7" name="taskaction" cols="50" class=i_am_required></textarea></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><textarea rows="7" name="taskdeliver" cols="50"></textarea></td>
                </tr>

<?php
//error_reporting(-1);
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') ORDER BY udfisort, udfivalue";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row){
	$class = $row['udfiobject']." ".(strlen($row['udfilinkfield'])>0 ? $row['udfilinkfield']." ".$row['udfilinkref'] : "");
	echo "<tr class='udf $class'>
		<th>".$row['udfivalue'].":</th>
		<td>";
	switch($row['udfilist']) {
	case "Y":
		echo "<select name=".$row['udfiid']."><option selected value=0>---SELECT---</option>";
		$sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
		$rows2 = $ah->mysql_fetch_all($sql2);
		//while($row = mysql_fetch_array($rs)) {
		foreach($rows2 as $row2){
			echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
		}
		unset($rs2);
		echo "</select>";
		break;
	case "T":
		echo "<input type=text name=".$row['udfiid']." size=50 />";
		break;
	case "M":
		echo "<textarea name=".$row['udfiid']." rows=5 cols=40></textarea>";
		break;
	case "D":
		echo "<input class='jdate2012'  type='text' name='".$row['udfiid']."' size='15' readonly='readonly' />";
		break;
	case "N":
		 echo "<input type='text' name='".$row['udfiid']."' size='15' class=numb />&nbsp;<br /><span class=i style='font-size: 7pt;'>Only numeric values are allowed.</span>";
		break;
	case "CUSTOMER":
        $obj = new MASTER_EXTERNAL("CUSTOMER");
        echo "<select name=".$row['udfiid']."><option selected value=0>---SELECT---</option>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;
    case "SUPPLIER":
        $obj = new MASTER_EXTERNAL("SUPPLIER");
        echo "<select name=".$row['udfiid']."><option selected value=0>---SELECT---</option>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;
    case "EMPLOYEE":
        $obj = new MASTER_EXTERNAL("EMPLOYEE");
        echo "<select name=".$row['udfiid']."><option selected value=0>---SELECT---</option>";
        foreach($obj->getListForExternalModule() as $key=>$val){
            echo("<option value=".$key.">".$val."</option>");
        }
        echo "</select>";
        break;
	default:
		echo "<input type=text name=".$row['udfiid']." size='50'>";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th>Attach Document(s):</th>
                    <td>
						<table id=tbl_attach width=100%>
							<tr>
								<td>
									<input type="file" name="attachments[]" id="attachment" size="30" />
									<div class="moduleSelect"></div>
								</td>
							</tr>		
						</table>
						<a href="javascript:void(0)" id="attachlink" style='margin-top: 5px;'>Attach another file</a>
						<?php
						/*
						<br>
						<input type="checkbox" name="storeDoc" id="storeDoc" value="1" />
						<i>Tick to put attachments in the Document Management system</i>
							$doc = new DOC();
							$docModules = $doc->getDocInstancesForSelect();
							echo "<div id=docModuleInfo hidden><br>";
							if(count($docModules)>1){
								echo '<div id="docModuleChooser">';
								foreach($docModules as $key=>$val){
									echo '<input type="radio" id="'.$key.'" value="'.$key.'" name="docModRef"><label for="'.$key.'">'.$val.'</label>';
								}
									    // <input type="radio" id="docModRef0" name="docModRef" checked><label for="docModRef0">Choice 1</label>
									    // <input type="radio" id="docModRef1" name="docModRef"><label for="docModRef1">Choice 2</label>
									    // <input type="radio" id="docModRef2" name="docModRef"><label for="docModRef2">Choice 3</label>
								echo  '</div>';
							}else if(count($docModules)==0){
								echo "<strong>No Document Management Module found! </strong>";
							}
							echo "</div>";
							*/
						?>
					</td>
                </tr>
                <tr>
					<th>Send Email:</th>
					<td>
						<input type="checkbox" name="sendEmail" id="sendEmail" value="1" />
						<i>Tick to send <?php echo strtolower($actname);?> details to yourself</i>
                    </td>
				</tr>
                <tr>
					<th></th>
                    <td>
                        <input type="button" value="Submit" class=isubmit />
                        <input type="reset" value="Reset" name="B2" />
					</td>
                </tr>
            </table>
        </form>
        <div hidden id="hiddenDocDiv"></div>
        <?php //ASSIST_HELPER::arrPrint($_SESSION); ?>

<script type=text/javascript>
$(function() {
	window.oneClicked = false;

	function reloadJs(src) {
		src = $('script[src$="' + src + '"]').attr("src");
	    $('script[src$="' + src + '"]').remove();
	    $('<script/>').attr('src', src).appendTo('body');
	}
	
	//FORM PREPARATION
	$("th").addClass("left").addClass("top");
	$("td").addClass("top");
	$("#tbl_attach tr.hover").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$("#tbl_attach, #tbl_attach td").addClass("noborder");
	
	$("#tasktopicid").change(function() {
		var v = $(this).val();
		$(".tasktopicid").each(function() {
			if($(this).hasClass(v)) {	
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	$("#tasktopicid").trigger("change");
	
	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			//$(this).css("background-color","#ffcccc").css("border","1px solid #cc0001");
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});
	
	$('#attachlink').click(function(){
		$("#tbl_attach").append("<tr><td class=noborder><input type=file name=attachments[] size=30 style='margin-top: 0px;' /><div class='moduleSelect'></div></td></tr>");
		if(typeof(window.modChooserCount) === 'undefined'){
			if(window.oneClicked){
				window.modChooserCount = 0;				
			}else{
				window.modChooserCount = 1;								
			}
		}else{
			window.modChooserCount ++;
		}
	});
	
	function validateUploads(){
		colouriseUI();
		var er = 0;
		$("#tbl_attach").find("input:file").each(function(){
			if($(this).hasClass("metaFileIn")){
				$(this).parent().effect("highlight");
				er ++;
			}
		});
		if(er > 0){
			alert("Files in yellow do not have their details filled in \n Please choose a location for each of them, and enter their details.");
		}else{
			//$("#attachmentPopup").dialog("close");
			return true;
		}
	}
	
	$("input:file").change(function(){
		colouriseUI();
	});
	
	$(document).on("click","input:file",function(event){
		window.oneClicked = true;
		//var dt = new Date();
		//var uni = dt.time();
		
		//console.log($(this).prop("files"));
		if(typeof(window.modChooserCount) === 'undefined'){
			window.modChooserCount = 0;
		}
		//alert(window.modChooserCount);
		if($(this).prop("files").length == 0){
			//alert("let's go");
			if(typeof(window.modChooser) !== 'undefined'){
				var html = window.modChooser;
				html = html.replace(/docModRef[0-99]/g,"docModRef"+window.modChooserCount);
				//alert("using old modChooser");
			}else{
				$(this).addClass("metaFileOk");
				var html = "";
				//alert('<?php //echo mysql_real_escape_string($doc->getDisplayIconAsDiv("error")); ?>');
				<?php
					$echo = '<div id=docModuleInfo><br>';
					$docModules = $doc->getDocInstancesForSelect();
					$icon = mysqli_real_escape_string($ah->getConn(),$doc->getDisplayIconAsDiv("error"," float:right;"));
					$echo.= $icon;
					if(count($docModules)>0){
						$uni = time();
						$echo.= '<em>Where would you like this attachment to be stored?</em><br>';
						$echo .= '<div id=\"docModuleChooser'."-".$uni.'\" class=\"radioSet\">';
						$echo .= '<input type=\"radio\" checked id=\"defaultMod'."-".$uni.'\" value=\"DefaultMod\" name=\"docModRef'."-".$uni.'\"><label for=\"defaultMod'."-".$uni.'\">Default Location</label>';
						foreach($docModules as $key=>$val){
							$echo .= '<input type=\"radio\" id=\"'.$key.'-'.$uni.'\" value=\"'.$key.'\" name=\"docModRef'."-".$uni.'\"><label for=\"'.$key.'-'.$uni.'\">'.$val.'</label>';
						}
						$echo .=  '</div>';
					}else if(count($docModules)<=0){
						$echo .= '<em>Storing file in the default location. </em>';
					}
					$echo .= '<div class=\"hiddenInsertPoint\" hidden>&nbsp;</div></div>';
				 ?>
				html += "<?php echo $echo; ?>";
				html = html.replace(/docModRef/g,"docModRef"+window.modChooserCount);
				window.modChooser = html;
			}
			//alert(html);
			$(this).next(".moduleSelect").html(html);
			//$("div.radioSet").last().buttonset();
			window.modChooserCount ++;
			$("div.ui-icon-closethick").button().on("click",closeAttachRow);
			colouriseUI();
		}
		
	});
	
	function resetFormElement(e) {
	  e.wrap('<form>').closest('form').get(0).reset();
	  e.unwrap();
	
	  // Prevent form submission
	  e.stopPropagation();
	  e.preventDefault();
	}
	
	function closeAttachRow(){
		//console.log($(this).parents("td.top").first().children("input").val());
		$(this).parents("td.noborder").children("input").val("");
		$(this).parents("div.moduleSelect").empty();
		checkFileInputs();
		colouriseUI();
		//window.modChooserCount --;
	}
	
	function checkFileInputs(){
		var fileInputs = $("#tbl_attach").find("input:file");
		$(fileInputs).each(function(i){
			//console.log(i);
			//console.log($(this).val());
			if(i > 0 && $(this).val().length <= 0){
				//console.log($(this).parents("tr"));
				$(this).parents("tr").first().empty();
			}
		});
		colouriseUI();
	}
	
	$(document).on("click",".radioSet input:radio",function(event){
		var id = $(this).attr("id").split("-")[0];
		if(id !== "defaultMod"){
			$radiobtn = $(this);
			var d = new Date();
			$unique = $radiobtn.val()+d.getTime();
			//var loc = $(this).parent("div").attr("id");
			var title = $(this).next().text();
			var fl = $(this).parents("td").first().children("input").prop("files")[0].name;
			
			//var metaForm = ajax_newAction("hello","loc="+loc+"&title="+title+"&fl="+fl+"&modref="+id);
			var my_post = "fl="+fl+"&modref="+id;
			$.ajax({                                      
				url: 'lib/ajax_new_task.php', 		  
				type: 'POST',		  
				data: my_post,		  
				dataType: 'json' 
			}).fail(function(){
				alert("Something went wrong! Please try that again");
			}).done(function( data ) {
				//console.log("Finished!");
				$("#hiddenDocDiv").html(data);
				$("#hiddenDocDiv .jdate2012").datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-M-yy',
                    changeMonth:true,
                    changeYear:true		
                });
				//reloadJs("assist.js");
				//$("#hiddenDocDiv .jdate2012").addClass("hasDatePicker");
				$table = $("#tbl_upload");
				$table1 = $("#tbl_attach");
				$("#hiddenDocDiv").dialog({
					title: "Uploading into <em>"+title+"</em>",
					hide: "fade",
					show: "fade",
					width: 550,
					height: 400,
					buttons: {
						Back : function(){
							$(this).dialog("close");
						},
						Save : function(){
							var err = new Array();
							var my_tag = "";
							var name = "";
							var val = "";
							var fld = "";
							$("#tbl_upload .i_am_required").removeClass("required");
							$("#tbl_upload").find(".i_am_required").each(function() {
								my_tag = $(this).get(0);
								my_tag = $(my_tag).prop("tagName").toString();
								name = $(this).attr("name");
								val = $(this).val();
								if(my_tag.toUpperCase()=="TEXTAREA") {
									val = val+$(this).text();
								}
								if(!val || val.length==0 || (my_tag=="SELECT" && (val=="X" || val=="0"))) {
									if(name=="tasktkid[]") { name="tasktkid"; }
									fld = $(this).parents("td").prev().text();
									fld = fld.toString();
									if(fld.charAt(fld.length-1)==":") {
										fld = fld.substr(0,fld.length-1);
									}
									err.push("- "+fld);
									$(this).addClass("required");
								}
							});
							if(err.length > 0) {
								alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
							} else {
								$clone = $radiobtn.parent("div").next();
								//$clone.show("flip");
								$clone.html("Hiyaah!");
								var selects = $table.find("select");
								$(selects).each(function(i) {
								    var select = this;
								    $(select).clone().appendTo($clone);
								    $clone.find("select").eq(i).val($(select).val()).attr("name",$clone.find("select").eq(i).attr("name")+"->"+$unique);
								});
								$table.find("input:text, textarea").each(function(){
								    var select = this;
									$me = $(select).clone(true).appendTo($clone);
									$me.attr("name",$me.attr("name")+"->"+$unique);
								});		
								colouriseUI();
								$(this).dialog("close");
							}
						}
					}
				});
			});
		}
		colouriseUI();
		return;
		var metaForm = ajax_newAction("hello","loc="+loc+"&title="+title+"&fl="+fl+"&modref="+id);
		console.log(metaForm);
		//return;
		var frm = $table.parents("form")[0];
		//console.log(frm);
		frm.reset();
		//document.getElementById("tbl_upload").reset();
		$table.find("#filenameInsert").html("<em>"+fl+"</em>");
	});
	
	$("form[name=newtask] input:button.isubmit").click(function() {
		var form = "form[name=newtask]";
		var err = new Array();
		var my_tag = "";
		var name = "";
		var val = "";
		var fld = "";
		$(".i_am_required").removeClass("required");
		$(".i_am_required").each(function() {
			//console.log($(this).parents("table").first().attr("id"));
			if($(this).parents("table").first().attr("id")=="tbl_upload"){
				//console.log("got an innard, sire");
			}else{
				my_tag = $(this).get(0).tagName; //attr("tagName");
				name = $(this).attr("name");
				val = $(this).val();
				if(my_tag.toUpperCase()=="TEXTAREA") {
					val = val+$(this).text();
				}
				if(!val || val.length==0 || (my_tag=="SELECT" && (val=="X" || val=="0"))) {
					if(name=="tasktkid[]") { name="tasktkid"; }
					fld = $("#th_"+name).text();
					if(fld.charAt(fld.length-1)==":") {
						fld = fld.substr(0,fld.length-1);
					}
					if($(this).parents("table").first().attr("id") !== "tbl_upload"){					
						err.push("- "+fld);
						$(this).addClass("required");
					}
				}
			}
		});
		if(err.length > 0) {
			alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
		} else {
			if(validateUploads()){
				$(form).submit();				
			}
		}
	});
	$("select.i_am_required").change(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if(!(!val || val=="X" || val=="0")) {
				$(this).removeClass("required");
			}
		}
	});
	$("textarea.i_am_required, input:text.i_am_required").keyup(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if($(this).attr("tagName")=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(val.length>0) {
				$(this).removeClass("required");
			}	
		}
	});
	
	$("#tbl_upload").on("dialogopen",function(event,ui){
		colouriseUI();
	});
	
	function colouriseUI(){
		$("#tbl_attach").find("input:file").each(function(){
			//console.log(typeof($(this).prop("files")[0]));
			var typecheck = $(this).prop("files")[0];
			if(typeof(typecheck) !== "undefined"){
				$modSel = $(this).siblings("div");
				$radioSet = $modSel.find("div.radioSet");
				if($radioSet.length > 0){
					$chosenLocation = $radioSet.find("input:radio:checked").val();
					if($chosenLocation == "DefaultMod"){
						$(this).removeClass();	
						$(this).addClass("metaFileOk");
					}else{
						$hiddenMetadata = $modSel.find("div.hiddenInsertPoint");
						if($hiddenMetadata.html().length >= 10){						
							$(this).removeClass();	
							$(this).addClass("metaFileOk");
						}else{						
							$(this).removeClass();	
							$(this).addClass("metaFileIn");
						}
					}
				}else{
					//No document modules on the system
					$(this).removeClass();	
					$(this).addClass("metaFileOk");
				}
			}else{
				$(this).removeClass();	
			}
		});
	}
	
	
});
</script>
    </body>
</html>

