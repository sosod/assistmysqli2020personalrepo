<?php
error_reporting(-1);
//print_r($_REQUEST);
$title = array(
	//array('txt'=>"View"),
	//array('txt'=>"Update"),
);
$page = array("update");
$id = $_REQUEST['id']; 
$modloc = $_REQUEST['modloc'];
$view_type = "MY";

require_once 'inc_header_external.php';
/*
$src = explode("/",$_SERVER['HTTP_REFERER']);
//arrPrint($src);
if(substr($src[count($src)-1],0,4)=="link") {
	$return_addr = "home";
} else {
	$return_addr = $src[count($src)-1];
}
*/
function drawHistoryRow($log) {
	global $update_udfs;
	global $history_attachments;
	global $history_udfs;
	global $update_udf_lists;
	global $ah2;
	$cmpcode = $_SESSION['cc'];
	$modref = $_SESSION['modref'];
	
	$logid = $log['logid'];
	$log_udf = isset($history_udfs[$logid]) ? $history_udfs[$logid] : array();
	$log_attach = isset($history_attachments[$logid]) ? $history_attachments[$logid] : array();
	$file_location = "../files/".$cmpcode."/".$_SESSION['modref']."/";
	foreach($log_attach as $i => $l) {
		if(strlen($l['file_location'])>0) { $path = $file_location.$l['file_location']."/"; } else { $path = $file_location; }
		if(!file_exists($path.$l['system_filename'])) {
			unset($log_attach[$i]);
		}
	}
	
		echo "<tr>
				<td colspan=2 style='padding: 0 0 0 0;'>
					<table id=log_".$logid." width=99% class=noborder>
						<tr>
							<th width=50 rowspan=2>".date("d M Y H:i",$log['logactdate'])."</th>
							<td >
								<p style='margin-top: 0px;'>".str_replace(chr(10),"<br />",$ah2->decode($log['logupdate']))."</p>";
		if(count($log_attach)>0) {
			echo "
							<p><span class=b>Attachments:</span>";
			foreach($log_attach as $a) {
				echo "<br />+ <a href='download2.php?i=".$a['id']."'>".$a['original_filename']."</a>";
			}
			echo "</p>";
		}
        //ASSIST_HELPER::arrPrint($update_udfs);
		echo "
								<table class=noborder id=tbl_udf>";
								foreach($update_udfs as $u) {
									if(isset($log_udf[$u['udfiid']]) && ( ($u['udfilist']=="Y" && $ah2->checkIntRef($log_udf[$u['udfiid']]['udfvalue'])) || (strlen($log_udf[$u['udfiid']]['udfvalue'])>0) )) {
										$lv = isset($log_udf[$u['udfiid']]['udfvalue']) ? $log_udf[$u['udfiid']]['udfvalue'] : "";
										echo "
											<tr>
												<td class='b right' >".$u['udfivalue'].":</td>
												<td>";
												switch($u['udfilist']) {
												case "Y":
													echo (isset($lv) && $ah2->checkIntRef($lv) && isset($update_udf_lists[$u['udfiid']][$lv]['udfvvalue'])) ? $update_udf_lists[$u['udfiid']][$lv]['udfvvalue'] : "N/A" ;
													break;
												case "M":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "D":
													if(strlen($lv)>0) { 
														if(is_numeric($lv)) { echo date("d M Y",$lv); } else { echo $lv; }
													} else {
														echo "N/A";
													}
													
//													echo strlen($lv)>0 && is_numeric($lv) ? date("d M Y",$lv) : "N/A";
													break;
												case "T":
													echo strlen($lv)>0 ? str_replace(chr(10),"<br />",$lv) : "N/A";
													break;
												case "N":
													echo strlen($lv)>0 && is_numeric($lv) ? number_format($lv,2) : "N/A";
													break;
											    case "CUSTOMER":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("CUSTOMER");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
                                                case "SUPPLIER":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("SUPPLIER");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
                                                case "EMPLOYEE":
                                                    if(isset($lv) && strlen($lv) > 0 ){
                                                        $obj = new MASTER_EXTERNAL("EMPLOYEE");
                                                        echo $obj->getObjectNameForExternalModule($lv);
                                                    }else{
                                                        echo ASSIST_HELPER::UNSPECIFIED;
                                                    }
                                                    break;
												}
											echo "</td>
											</tr>";
									}
								}
						echo	"</table>
		</td>
							<td  class='right'>
								<p style='margin-top: 0px;'><span class=b>Status:</span> ".$log['status']."<br />(".$log['logstate']."% completed)</p>
								<p><span class=b>Date Logged:</span><br />".date("d M Y H:i",$log['logdate'])."</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
}

//GET USER ACCESS PERMISSIONS
$tact = getUserAccess();
//echo "CALLED INC";

//GET TASK ID
$taskid = $_REQUEST['id'];
if($ah2->checkIntRef($taskid)) {
	//GET TASK DETAILS INTO ARRAY TASK
//	$sql = "SELECT * FROM ".$dbref."_task WHERE taskid = ".$taskid;
	$task = getTask($taskid);
	
	//HISTORY INFORMATION
		$sql = "SELECT l.*, stat.value as status 
				FROM assist_".$cmpcode."_task_log l
				INNER JOIN assist_".$cmpcode."_task_list_status stat
				  ON l.logstatusid = stat.pkey
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U','E','D') 
				ORDER BY logactdate DESC, logdate DESC";
		//echo $sql;
		$history = $ah2->mysql_fetch_all_fld($sql,"logid");
		//ASSIST_HELPER::arrPrint($history);
		$sql = "SELECT l.* 
				FROM assist_".$cmpcode."_task_log l
				WHERE l.logtaskid = ".$taskid." 
				AND l.logtype IN ('U') 
				ORDER BY logactdate DESC, logdate DESC
				LIMIT 1";
		$last = $ah2->mysql_fetch_all($sql);
		$last_log_id = isset($last[0]['logid']) ? $last[0]['logid'] : false;
		$sql = "SELECT a.* 
				FROM assist_".$cmpcode."_task_task_attachments a
				INNER JOIN assist_".$cmpcode."_task_log l
				  ON a.logid = l.logid
				  AND l.logtaskid = ".$taskid;
		$history_attachments = $ah2->mysql_fetch_all_fld2($sql,"logid", "id");
		$sql = "SELECT u.*, l.logid 
				FROM assist_".$cmpcode."_udf u
				INNER JOIN assist_".$cmpcode."_udfindex i
				  ON u.udfindex = i.udfiid
				  AND i.udfiobject = 'update'
				  AND i.udfiyn = 'Y'
				  AND i.udfiref = '".$modref."'
				INNER JOIN assist_".$cmpcode."_task_log l
				  ON l.logid = u.udfnum
				  AND l.logtaskid = ".$taskid."
				WHERE u.udfref = '".$modref."'";
		$history_udfs = $ah2->mysql_fetch_all_fld2($sql,"logid", "udfindex");
		
	//UPDATE INFORMATION	
		$sql = "SELECT * FROM assist_".$cmpcode."_task_list_status WHERE yn = 'Y' AND id NOT IN ('CN','NW','ON') ORDER BY sort";
		$status = $ah2->mysql_fetch_all_fld($sql,"pkey");
		
		$sql = "SELECT * 
				FROM assist_".$cmpcode."_udfindex 
				WHERE udfiref = '".$modref."' 
				AND udfiyn = 'Y' 
				AND udfiobject = 'update' 
				AND (
						udfilinkref = 0 
					OR 
						udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid']."
					OR 
						udfilinkfield = 'taskstatusid'
				)
				ORDER BY udfisort, udfivalue";
		$update_udfs = $ah2->mysql_fetch_all($sql);
		$update_udf_lists = getUDFListItems("update");

	
} else {
	die("<p>An error occurred while trying to access the requested ".$actname.".  Please go back and try again.</p>");
}

//$last = array_pop($history_udfs);
$k = array_keys($history);
$last = $last_log_id!== false && isset($history_udfs[$last_log_id]) ? $history_udfs[$last_log_id] : array();

if($_SERVER['REMOTE_ADDR']=="105.233.22.235") {
//	arrPrint($k);
//	arrPrint($history);
//	arrPrint($last);
}

/*
//SET REMINDER
$tremind = $_GET['remind'];
if(strlen($tremind) > 0) {
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE ".$dbref."_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
    $tsql = $sql;
    $tref = "TA";
    $trans = "Set reminder ".$tr." for task ".$taskid.".";
    include("inc_transaction_log.php");
}
*/

?>
<style type=text/css>
[type=file] { margin-bottom: 5px; }
#tbl_udf, #tbl_udf td { padding: 2px 1px 2px 1px; }
</style>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function statusPerc() {
            var stat = document.taskupdate.logstatusid.value;
            stat = parseInt(stat);
            if(stat == 3 || stat > 5)
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
                if(stat > 5 && lstat[stat] > 0)
                {
                    document.taskupdate.logstate.value = lstat[stat];
                }
                else
                {
                    document.taskupdate.logstate.value = lstat[0];
                }
            }
            else
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>
<table width=100% id=tbl_container>
	<tr><td class=td_container style='padding-right: 30px' width=50%>
        <h2 class=fc><?php echo ucfirst($actname);?> <?php echo $task['taskid']; ?></h2>
        <table width=95% id=tbl_object><tbody>
            <tr>
                <th width=140>Reference:</b></th>
				<td><?php echo $task['taskid']; ?></td>
            </tr>
            <tr>
                <th width=140>Created By:</b></th>
				<td><?php echo $task['adduser']; ?></td>
            </tr>
			<tr>
				<th>Created On:</th>
				<td><?php echo date("d-M-Y H:i:s",$task['taskadddate']); ?></td>
			</tr>
            <tr>
                <th>Assigned To:</th>
                <td><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN assist_".$cmpcode."_task_task_recipients r ON (u.tkid=r.tasktkid)
                WHERE r.taskid= '".$task['taskid']."' ";
                    // echo $sql;
                    $rs = $ah2->mysql_fetch_all($sql);
                    $assignees = "";
                    foreach($rs as $row) {
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    unset($rs);
                ?></td>
            </tr>
            <tr>
                <th>Topic:</td>
				<td><?php echo $task['topic']; ?></td>
            </tr>
            <tr>
                <th>Priority:</th>
				<td><?php echo $task['urgency']; ?></td>
            </tr>
            <tr>
                <th>Status:</th>
				<td><?php echo $task['status'].($task['taskstate']>0 && $task['taskstate']<100 ? " (".$task['taskstate']."%)" : ""); ?></td>
            </tr>
            <tr>
                <th>Deadline:</th>
                <td><?php echo date("d-M-Y",$task['taskdeadline']); ?></td>
            </tr>
            <tr>
                <th><b><?php echo ucfirst($actname);?> Instructions:</b></th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskaction']);
                ?></td>
            </tr>
            <tr>
                <th><?php echo ucfirst($actname);?> Deliverables:</th>
                <td><?php
					echo str_replace(chr(10),"<br />",$task['taskdeliver']);
                ?></td>
            </tr>
<?php
if($cmpcode == "fin0001" || $cmpcode == "janet12") {
	$sql = "SELECT * FROM assist_".$cmpcode."_task_task_preset WHERE preset_taskid = $taskid AND active = true"; 
	$rs = $ah2->mysql_fetch_all($sql);
		if(count($rs)>0) { ?>
				<tr>
					<th>Source <?php echo ucfirst($actname);?>:</th>
					<td><ul>
						<?php
						foreach($rs as $row) {
							echo "<li><a href=view_all_update.php?i=".$row['source_taskid']." target=_blank>$actname #".$row['source_taskid']."</a></li>";
						}
					?></ul></td>
				</tr>
	<?php }
	unset($rs);
}
?>
<!--<tr id="firstdoc">

    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
</tr>
<tr>
    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
</tr>-->


            <?php
            if($task['topic'] == 'Travel Requests' && $topicyn = 'N') {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                $udfi = $ah2->mysql_fetch_one($sql);
                if(strlen($udfi['udfiid'])>0) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                    $rs2 = $ah2->mysql_fetch_all($sql2);
                    $row2 = $ah2->mysql_fetch_all($rs2);
                    $udf = count($rs2);
                    ?>
            <tr>
                <th>Travel Request Ref.:</th>
                <td><?php
					if($udf>0) {
						echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
					}
					else {
						echo("N/A");
					}
				?>&nbsp;</td>
            </tr>
                    <?php
                }
            }
            $sql = "SELECT * 
					FROM assist_".$cmpcode."_udfindex 
					WHERE udfiref = '".$modref."' 
					AND udfiyn = 'Y' 
					AND udfiobject = 'action' 
					AND (udfilinkref = 0 OR udfilinkref = ".$task['tasktopicid'].")
					ORDER BY udfisort, udfivalue";
            $rs = $ah2->mysql_fetch_all($sql);
            foreach($rs as $row) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
				//echo $sql2;
                $rs2 = $ah2->mysql_fetch_one($sql2);
					$udf = count($rs2);
					$row2 = $rs2;
                ?>
            <tr>
                <th><?php echo($row['udfivalue']); ?>:</th>
                <td><?php
		            //ASSIST_HELPER::arrPrint($row);
                        switch($row['udfilist']) {
                            case "Y":
                                if($ah2->checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    $rs3 = $ah2->mysql_fetch_all($sql2);
										$row3 = $ah2->mysql_fetch_all($rs3);
										echo $row3['udfvvalue'];
                                    unset($rs3);
                                } else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo decode($row2['udfvalue']);
                                else
                                    echo "N/A";
                                break;
                            case "CUSTOMER":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("CUSTOMER");
                                    //ASSIST_HELPER::arrPrint($obj->getObjectNameForExternalModule($row2['udfvalue']));
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            case "SUPPLIER":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("SUPPLIER");
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            case "EMPLOYEE":
                                if(isset($row2['udfvalue']) && strlen($row2['udfvalue']) > 0 ){
                                    $obj = new MASTER_EXTERNAL("EMPLOYEE");
                                    echo $obj->getObjectNameForExternalModule($row2['udfvalue']);
                                }else{
                                    echo ASSIST_HELPER::UNSPECIFIED;
                                }
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",decode($row2['udfvalue'])));
                                break;
                        }
                ?>&nbsp;</td>
            </tr>
                <?php
            }
			unset($rs2);

            $sql = "SELECT * FROM  assist_".$cmpcode."_task_task_attachments AS att WHERE att.file_location <> 'deleted' AND att.taskid = ".$taskid;
            $rs = $ah2->mysql_fetch_all($sql);
            if(count($rs) > 0) {
                echo "<tr>
				<th>Attachment(s):</th>
				<td>";
$tattach = array();
                while($row = $ah2->mysql_fetch_all($rs)) {
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
                    $file = "../files/$cmpcode/".$_SESSION['modref']."/".(strlen($row['file_location'])>0 ? $row['file_location']."/" : "").$row['system_filename'];
                    if(file_exists($file)) {
						$tattach[] =  "+ <a href='download2.php?i=".$row['id']."'>".$row['original_filename']."</a>";
					}
                }
                echo implode("<br />",$tattach)."</td></tr>";
            }

            if(($tkid == $task['taskadduser'] || $view_type == "ALL") && $task['topic'] != 'Travel Requests') { ?>
                <tr><th></th><td class=right>&nbsp;</td></tr>
           <?php } ?>
        </tbody></table>
        <?php 
		//displayGoBack(); 
if(count($history)>0) {
?>
</td><td class=td_container width=50%>
	<div class=float>
		<h2 class="fc">&nbsp;</h2>
		<table id=tbl_history width=95%>
		<?php
		echo "	<tr>
					<th colspan=2>History</th>
				</tr>";
		foreach($history as $h) {
			drawHistoryRow($h);
		}
		
		?>
		</table>
	</div>
	</td></tr>
</table>
<?php 
	//displayGoBack();
} 
?>

<script type=text/javascript>
$(function() {
	$("tr").unbind('mouseenter mouseleave');
	$("td").addClass("top");
	$("table.noborder td").addClass("noborder");
	$("table#tbl_container, table#tbl_container td.td_container").addClass("noborder");
	$("#tbl_object th").addClass("left");
	$("#tbl_update_udf, #tbl_update_udf td").addClass("noborder");
	$("#tbl_updates select[name=logstatusid]").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_updates input[name=logstate]").val(<?php echo $task['taskstate']; ?>);
	$("#tbl_updates select[name=logstatusid]").change(function() {
		var v = $(this).val();
		if(v==1) {
			$("#tbl_updates input:text[name=logstate]").val(100);
			$("#tbl_updates input:text[name=logstate]").attr("disabled","true");
		} else {
			var s = $("#tbl_updates select[name=logstatusid] option:selected").attr("s");
			if(!isNaN(parseInt(s)) && s > 0) {
				$("#tbl_updates input:text[name=logstate]").val(s);
			}
		}
		$("#tbl_update_udf tr.taskstatusid").each(function() {
			if($(this).hasClass(v)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	//$("#tbl_updates select[name=logstatusid]").trigger("change");
	$("#tbl_updates #p_attach").click(function() {
		$("#tbl_updates #spn_attach_update").append("<br /><input type=file name=attachments[] />");
	});
/*
	$("form[name=update] input:button.isubmit").click(function() {
		var form = "form[name=update]";
		$(form+" textarea,"+form+" select,"+form+" input").removeClass("required");
		var err = false;
		var logupdate = $(form+" textarea[name=logupdate]").val();
		if(logupdate.length==0) {
			$(form+" textarea[name=logupdate]").addClass("required");
			err = true;
		}
		var logstatusid = $(form+" select[name=logstatusid]").val();
		if(logstatusid.length==0) {
			$(form+" select[name=logstatusid]").addClass("required");
			err = true;
		}
		var logstate = $(form+" input[name=logstate]").val();
		if(logstate.length==0) {
			$(form+" input[name=logstate]").addClass("required");
			err = true;
		}
		var logactdate = $(form+" input[name=logactdate]").val();
		if(logactdate.length==0) {
			$(form+" input[name=logactdate]").addClass("required");
			err = true;
		}
		$("#tbl_update_udf input:text").each(function() {
			var u = 0;
			if($(this).hasClass("number")) {
				u = $(this).val();
				if(u.length>0 && (isNaN(parseFloat(u)) || !isFinite(u))) {
					$(this).addClass("required");
					$("#lbl_"+$(this).attr("name")).css("color","#cc0001");
					err = true;
				}
			}
		});
		if(!err) {
			$(form).submit();
		} else {
			alert("Please complete the required fields as highlighted in red.");
		}
	});
	*/
});
</script>		

    </body>

</html>
