<?php
//include("inc_ignite.php");
require_once("inc_header.php");

function getUDFvalues($udfiid,$cmpcode)
{
global $db_name, $db_user, $db_pwd,$ah;
    $sql = "SELECT udfilist FROM assist_".$cmpcode."_udfindex WHERE udfiid=".$udfiid;

//    include 'inc_db_con.php';
//$rs = getRS($sql);
    $row = $ah->mysql_fetch_one($sql);//mysql_fetch_array($rs);
    if($row['udfilist'] == 'T' OR $row['udfilist'] == 'M')
    {
        $response['udfilist'] = $row['udfilist'];
    }
    else if($row['udfilist'] == 'Y')
    {
        $sql = "SELECT udfvid,udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex=".$udfiid;
//        include 'inc_db_con.php';
		$rows = $ah->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
		foreach($rows as $row){
            $response[$row['udfvid']] = $row['udfvvalue'];
        }
        $response['udfilist'] = 'Y';
    }
    echo json_encode($response);
    exit();
}
function getTaskedUsers($cmpcode,$dbref,$modref,$field_name,$default = "SELECT")
{ global $ah;
    $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'
                                ORDER BY t.tkname, t.tksurname";
    //include("inc_db_con.php");
	$rows = $ah->mysql_fetch_all($sql);
    $users = "<select  id='$field_name' name='$field_name'>
                <option value='X'>--- $default ---</option>";

                //while($row = mysql_fetch_array($rs)) {
                foreach($rows as $row){
                    $users .= "<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
                }

    $users .= "</select>";
    echo $users;
}
//
$udfs = "";
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiyn = 'Y' AND (udfiobject = '' OR udfiobject = 'action') ORDER BY udfisort";
//include("inc_db_con.php");
$rows = $ah->mysql_fetch_all($sql);
$num_udfs = $ah->db_get_num_rows($sql);
if($num_udfs > 0)
{
$udfs .= "<select name='udfs' id='udfs'><option value='X'>--- SELECT ---</option>";
    //while($row = mysql_fetch_array($rs)) {
    foreach($rows as $row){
    $udfs .= "<option value='".$row['udfiid']."'>".$row['udfivalue']."</option>";
    }
    $udfs .= "</select>";
}
if(isset($_POST['field']))
{
$field = $_POST['field'];

$queries = array(
'tasktopicid'=>"SELECT id,value FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value",
'taskurgencyid'=>"SELECT id,value FROM ".$dbref."_list_urgency WHERE yn='Y' ORDER BY sort",
'tasktkid'=>"SELECT DISTINCT t.tkid AS id, CONCAT_WS(' ',t.tkname,t.tksurname) AS value
FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'
ORDER BY t.tkname, t.tksurname",
'taskadduser' => "SELECT DISTINCT t.tkid AS id, CONCAT_WS(' ',t.tkname,t.tksurname) AS value
FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'
ORDER BY t.tkname, t.tksurname",
'taskstatusid'=>"SELECT id,value FROM ".$dbref."_list_status  WHERE yn =  'Y' ORDER BY sort"
);
if(is_numeric($field))
{ 
getUDFvalues($field,$cmpcode);
}
else
{
if(array_key_exists($field, $queries))
{
$sql = $queries[$field];
//include("inc_db_con.php");
	$rows = $ah->mysql_fetch_all($sql);
$result = array();
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row){
$result[] = $row;
}
echo json_encode($result);
}
}
exit();
}
else if(isset($_GET['udfiid']))
{
getUDFvalues($_GET['udfiid'],$cmpcode);
}
?>
        <script type="text/javascript">
            var lastSelectedVal = '';
            var lastSelectedText = '';
            $(function(){

                $('#graphfilter').submit(function(e){
                    if($("#groupby").val() == "X"){
                        $("#field_filter_cont").html("<span style='margin-left:5px;color:#FF0000;'>This field is required!</span>");
                        e.preventDefault();
                    }
                    if($('#udfs').val() != 'X'){
                        if($('#udfvalue').val() == 'X' || $('#udfvalue').val() == ''){
                            e.preventDefault();
                            $('<span style="margin-left:5px;color:#FF0000;">This field is required</span>').insertAfter('#udfvalue');
                        }
                    }

                });
                $('#udfs').change(function(){   
                    if($('#udfs').val() != 'X'){
                        $.getJSON("graphs.php", {udfiid:$('#udfs').val()}, function(data){
alert("got this far");
                            if(data.udfilist == 'T' || data.udfilist == 'M'){
                                $('#udf-values').html('<input type="text" name="udfvalue" id="udfvalue" size="25" />');
                            }else{
                                var content = '<select name="udfvalue" id="udfvalue"><option value="X">--- SELECT ---</option>'
                                $.each(data, function(i,item){
                                    content += "<option value='"+i+"'>"+item+"</option>";
                                });
                                content += '</select>';
                                $('#udf-values').html(content);
                            }

                        });
                        if(lastSelectedVal != '' && lastSelectedText != ''){
                            $('#groupby').append(new Option(lastSelectedText,lastSelectedVal));
                            $('#field_filter_cont').html('');
                        }
                        lastSelectedVal = $('#udfs').val();
                        lastSelectedText = $("#groupby option[value='"+$('#udfs').val()+"']").text();
                        $("#groupby option[value='"+$('#udfs').val()+"']").remove();
                    }else if($('#udfs').val() == 'X'){
                        $('#udf-values').html('');
                        if(lastSelectedVal != '' && lastSelectedText != ''){
                            $('#groupby').append(new Option(lastSelectedText,lastSelectedVal));
                        }
                        lastSelectedVal = '';
                        lastSelectedText = '';
                    }

                });

                $("#graphdatecreatedFrom").bind("focus",function(){                   
                    $("#graphexactdateopt").attr("checked","checked");
                });
                $("#graphdatecreatedTo").bind("focus",function(){
                    $("#graphexactdateopt") .attr("checked","checked");
                });
                $("#graphdeadlineFrom").bind("focus",function(){
                    $("#graphexactdeadlinedateopt").attr("checked","checked");
                });
                $("#graphdeadlineTo").bind("focus",function(){
                    $("#graphexactdeadlinedateopt") .attr("checked","checked");
                });
                $('#graphtaskurgencyid').change(function(){
                    var  taskurgencyid = $('#graphtaskurgencyid').val();

                    if(taskurgencyid != 'X')
                    {
                        $("#groupby option[value='taskurgencyid']").remove();
                    }
                    else if(taskurgencyid == 'X')
                    {
                        $('#groupby').append("<option value='taskurgencyid'>Priority</option>");
                    }
                    $('#field_filter').remove();
                });
                $('#graphtasktopicid').change(function(){
                    var tasktopicid = $('#graphtasktopicid').val();
                    if(tasktopicid != 'X')
                    {
                        $("#groupby option[value='tasktopicid']").remove();
                    }
                    else if(tasktopicid == 'X')
                    {
                        $("#groupby").append("<option value='tasktopicid'>Topic</option>");
                    }
                    $("#field_filter").remove();
                });
                $('#graphtasktkid').change(function(){
                    var tasktkid = $('#graphtasktkid').val();
                    if(tasktkid != 'X')
                    {
                        $("#groupby option[value='tasktkid']").remove();
                    }
                    else if(tasktkid == 'X')
                    {
                        $("#groupby").append("<option value='tasktkid'>Person Actioned</option>");
                    }
                    $("#field_filter").remove();
                });
                $('#graphtaskadduser').change(function(){
                    var taskadduser = $('#graphtaskadduser').val();
                    if(taskadduser != 'X')
                    {
                        $("#groupby option[value='taskadduser']").remove();
                    }
                    else if(taskadduser == 'X')
                    {
                        $('#groupby').append("<option value='taskadduser'>Action Owner</option>");
                    }
                    $("#field_filter").remove();

                });
                $('#graphdatecreatedFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdatecreatedTo').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdeadlineFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#graphdeadlineTo').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#groupby').bind('change',function(){

                    $.ajax({
                        type:"POST",
                        url:"graphs.php",
                        data:{field:$('#groupby').val()},
                        beforeSend:function(data){
                            if(data && data.overrideMimeType){
                                data.overrideMimeType("application/j-son;charset=UTF-8");
                            }
                        },
                        dataType:"json",
                        success:function(data){
                            var content = '';
                            if(data != null){
                                if(data.udfilist == 'T' || data.udfilist == 'M'){
                                    content = '<input name="field_filter" id="field_filter" size="25">';
                                }else if(data.udfilist == 'Y'){
                                    content = '<select name="field_filter" id="field_filter"><option value="X">---All---</option>';
                                    $.each(data, function(i,item){
                                        if(item != 'Y')
                                            content += "<option value='"+i+"'>"+item+"</option>";
                                    })
                                    content += '</select>';
                                }
                                else{
                                    content = '<select  name="field_filter" id="field_filter"><option value="X">---All---</option>';
                                    $.each(data, function(i,item){
                                        content += "<option value='"+item.id+"'>"+item.value+"</option>";
                                    })
                                    $('#field_filter_cont').show();
                                    content += '</select>';
                                }
                            }                            
                            $('#field_filter_cont').html(content);
                        }
                    });
                });
            });
        </script>
    </head>

    <script type="text/javascript">
        function Validate(me) {
            return true;
        }
    </script>
<style type=text/css> table, table td { border: 1px solid #FFFFFF; } </style>
        <h1 class=fc><b><?php echo $_SESSION['modtext'];?> : Report</b></h1>
        <!--Graph code starts here-->
        <div id="graphs">
            <form action="report_process_graphs.xml.php" method="post" name="graphfilter" id="graphfilter">
                <table>
                    <tr>
                        <td  colspan="2">
                            <h3 class=fc>1. Select filters to specify the data to be selected:</h3>
                        </td>
                    </tr>
                    <tr>
                        <td >Person <?php echo $actname;?>ed:</td>
                        <td>
                            <?php getTaskedUsers($cmpcode,$dbref,$modref,'graphtasktkid');?>
                        </td>
                    </tr>
                    <tr>
                        <td ><?php echo $actname;?> Owner:</td>
                        <td>
                                <?php
                                $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, ".$dbref."_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = '$modref'
                                ORDER BY t.tkname, t.tksurname";
                                //include("inc_db_con.php");
								$rows = $ah->mysql_fetch_all($sql);

    ?>
                            <select  id="graphtaskadduser" name="graphtaskadduser">
                                <option value='X'>--- SELECT ---</option>
                                    <?php
                                    //while($row = mysql_fetch_array($rs)) {
                                    foreach($rows as $row){
                                        echo("<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                    }
                                    //mysql_close($con);
    ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td >Priority:</td>
                        <td>
                            <select size="1" name="graphtaskurgencyid" id="graphtaskurgencyid">
                                <option value=X>--- SELECT ---</option>
                                    <?php
                                    $sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
                                    //include("inc_db_con.php");
									$rows = $ah->mysql_fetch_all($sql);
                                    //while($row = mysql_fetch_array($rs)) {
                                    foreach($rows as $row){
                                        echo("<option  value=".$row['id'].">".$row['value']."</option>");
                                    }
    ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td >Topic:</td>
                        <td>
                            <select size="1" name="graphtasktopicid" id="graphtasktopicid">
                                <option value=X>--- SELECT ---</option>
                                    <?php
                                    $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                                    //include("inc_db_con.php");
									$rows = $ah->mysql_fetch_all($sql);
                                    //while($row = mysql_fetch_array($rs)) {
                                    foreach($rows as $row){
                                        echo("<option value='".$row['id']."'>".$row['value']."</option>");
                                    }
//mysql_close($con);
    ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td >
                            <select name="graphtaskstatusid" id="graphtaskstatusid">
                                <option value="X">--- SELECT ---</option>
                                <!--<option value="-1">All incomplete <?php //echo $actname;?>s</option> -->
                                    <?php
                                    $sql = "SELECT * FROM ".$dbref."_list_status t WHERE yn='Y' ORDER BY sort";
                                    //include("inc_db_con.php");
									$rows = $ah->mysql_fetch_all($sql);
                                    //while($row = mysql_fetch_array($rs)) {
                                    foreach($rows as $row){
                                        echo ("<option value=".$row['pkey'].">".$row['heading']."</option>");
                                    }
    ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td  >Date created:</td>
                        <td class="tdgeneral"  nowrap>
                            <input type='radio' name='graphdatecreatedopt' value='Any' checked id='graphanydatecreatedopt'/>
                            <label for='graphanydatecreatedopt'>Any date</label><br />
                            <input type='radio' name='graphdatecreatedopt' value='Exact' id='graphexactdateopt'/>
                            <label for='graphexactdateopt'>From </label>
                            <input type='text' id='graphdatecreatedFrom' name='graphdatecreatedFrom'   readonly='readonly'/>
                            <label for='graphdatecreatedFrom'>&nbsp;To&nbsp;</label>
                            <input type='text' id="graphdatecreatedTo" name="graphdatecreatedTo"   readonly="readonly"/>
                        </td>
                    </tr>

                    <tr>
                        <td  >Deadline:</td>
                        <td class="tdgeneral"  nowrap>
                            <input type='radio' name='graphdeadlinedateopt' value='Any'  checked id='graphdeadlinedateopt' />
                            <label for='graphanydeadlinedateopt'>Any date</label><br>
                            <input type='radio' name='graphdeadlinedateopt' value='Exact' id='graphexactdeadlinedateopt' />
                            <label for='graphdeadlinedateopt'>From </label>
                            <input type='text' id="graphdeadlineFrom" name="graphdeadlineFrom"   readonly="readonly"/>
                            <label for='deadlineFrom'>&nbsp;To&nbsp;</label>
                            <input type='text' id="graphdeadlineTo" name="graphdeadlineTo"   readonly="readonly"/>
                        </td>
                    </tr>
                        <?php if($num_udfs > 0 && $udfs == "ABC")
    {?>
                    <tr>
                        <td class="tdgeneral"  nowrap>User Defined Fields (UDFs):</td>
                        <td>
                            <?php echo $udfs;?><span id="udf-values" style="margin-left:10px;" ></span>
                        </td>

                    </tr>
        <?php }?>
                    <tr>
                        <td  colspan="2" >
                            <h3 class=fc>2. Group data by:</h3>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgeneral" valign="top">
                            <select id="groupby" name="groupby" >
                                <option value="X">---SELECT---</option>
                                    <?php
//AND d.field NOT IN('taskstatusid','taskadduser')
                                    $sql2 = "SELECT * FROM ".$dbref."_list_display d WHERE d.type='S' AND d.id NOT IN (2,3,4,5,7)  ORDER BY d.allsort";
                                    //include("inc_db_con2.php");
									$rows = $ah->mysql_fetch_all($sql2);
                                    //while($row2 = mysql_fetch_array($rs2)) {
                                    foreach($rows as $row2){
                                        echo("<option value='".$row2['field']."' ".($row2['field'] == 'tasktkid' ? "selected=selected" : "").">".$row2['headingfull']."</option>");
                                    }
                                    //mysql_close($con2);
                                   /* $sql2 = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfilist != 'D' AND udfilist != 'N' AND udfiref = '$modref' AND (udfiobject = '' OR udfiobject = 'action') ORDER BY udfisort";
                                    include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        echo("<option value='".$row2['udfiid']."'>".$row2['udfivalue']."</option>");
                                    }
                                    mysql_close($con2);*/
?>
                            </select>
                        </td>
                        <td class="tdgeneral" valign="top" ><div id="field_filter_cont" >
                                 <?php getTaskedUsers($cmpcode,$dbref,$modref,'field_filter','All');?>
                            </div></td>
                    </tr>
                    <tr>
                        <td class="tdgeneral"  nowrap="nowrap">
                            <input type='radio' name="graphtype" value="3dcolumn" checked id="3dcolumn" />
                            <label for="3dcolumn">Column Chart</label><br />
                            <input type='radio' name="graphtype" value="3dbarchart" id="3dbarchart" />
                            <label for="3dbarchart">Bar Chart</label><br />
                            <input type='radio' name="graphtype" value="pie" id="simplepie" checked="checked"/>
                            <label for="simplepie">Pie Chart</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="Generate Graphs" name="btnGraph" id="btnGraph"/>
                        </td>
                    </tr>

                </table>
                <input type="hidden" name="currentPage" value="1" />
            </form>
        </div>
        <!-- graph code ends here-->
        <div style="clear:left;"></div>
    </body>
</html>
