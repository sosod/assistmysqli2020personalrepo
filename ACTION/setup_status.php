<?php include("inc_header.php"); 
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
/*** CHECK FOR NEW STATUS ***/
if(isset($_REQUEST['addtxt']) && isset($_REQUEST['addstate']) && strlen($_REQUEST['addtxt'])>0) {
	$txt = $ah->code($_REQUEST['addtxt']);
	$state = $_REQUEST['addstate'];
	$clr = isset($_REQUEST['clr']) ? $_REQUEST['clr'] : "";
	if(strlen($state)==0 || !is_numeric($state)) { $state = 0; }
	$sql = "SELECT max(sort) as sort FROM ".$dbref."_list_status WHERE yn = 'Y'";
	//include("inc_db_con.php");
		$s = $ah->mysql_fetch_one($sql);
	//mysql_close($con);
	$sort = $s['sort'];
//	$sql = "INSERT INTO ".$dbref."_list_status (id, value, sort, yn, state, heading, custom, color, pkey) VALUES ('','$txt',$sort,'Y',$state,'$txt','Y','$clr',null)";
	$sql = "INSERT INTO ".$dbref."_list_status (id, value, sort, yn, state, heading, custom, pkey) VALUES ('','$txt',$sort,'Y',$state,'$txt','Y',null)";
	//include("inc_db_con.php");
	$ar = $ah->db_insert($sql);
	if($ar>0) {
		$result = array("check","New status '$txt' created.");
			$tsql = $sql;
			$trans = "New status ".$txt." added to $modref.";
			$tref = "$modref";
			//include("inc_transaction_log.php");
		$sql = "UPDATE ".$dbref."_list_status SET sort = ".($sort+1)." WHERE pkey = 1";
		//include("inc_db_con.php");
		$ah->db_update($sql);
	} else {
		$result = array("error","New status '$txt' could not be created.");
	}
}
?>
<link rel="stylesheet" media="screen" type="text/css" href="../lib/jquery/css/colorpicker.css" />
<script type="text/javascript" src="../lib/jquery/js/colorpicker.js"></script>
<h1>Setup >> Status</h1>
<?php $ah->displayResult($result); ?>
<p><input type=button value="Display Order" onclick="document.location.href = 'setup_status_sort.php';"></p>
<table id=tbl_list>
    <tr>
        <th>Ref</th>
        <th>Status</th>
        <th>Default %</th>
<!--        <th>Color</th> -->
		<th>Assoc. <?php echo $actname; ?>s</th>
        <th>&nbsp;</th>
    </tr>
<form name=add method=post action=setup_status.php onsubmit="return ValidateStatus(this);" language=jscript>
    <tr>
        <th width=30>&nbsp;</th>
        <td width=230><input type=text name=addtxt maxlength=50 size=30  id=s2></td>
        <td width=100 class=center><input type=text name=addstate size=5 id=s1>%</td>
<!--		<td><table style="margin-left: auto; margin-right: auto;"><tr><td style="background-color: #ffffff;" width=20 id=colorSelector>&nbsp;</td></tr></table><input type="hidden" name="clr" id=clr size="6"></td> -->
		<td class=center>N/A</td>
        <td class=center><input type=submit value=Add class=isubmit /></td>
    </tr>
</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT stat.*, count(taskid) as tc 
		FROM ".$dbref."_list_status stat
		LEFT OUTER JOIN ".$dbref."_task 
		ON taskstatusid = stat.pkey
		WHERE stat.yn = 'Y' AND stat.pkey <> 2 
		GROUP BY stat.pkey
		ORDER BY stat.sort
		";
$status = $ah->mysql_fetch_all($sql);
foreach($status as $row) {
    $val = $row['value'];
            $valjs = $ah->decode($row['value']);
            $valjs = str_replace("'","\'",$valjs);
            $valjs = str_replace("\"","&#034;",$valjs);
	$color = $row['color'];

$objid = $row['pkey'];
    echo "<tr>
        <th id=pkey_".$objid.">".$row['pkey']."</th>
        <td id=val_".$objid.">".$ah->decode($row['value'])."</td>
        <td class=centre id=state_".$objid.">".($row['state']<=0 ? "" : $row['state']."%")."</td>
<!--		<td><table style=\"margin-left: auto; margin-right: auto;\"><tr><td style=\"background-color: #".$color.";\" width=20>&nbsp;</td></tr></table></td> -->
		<td class=center id=tc_".$objid.">".$row['tc']."</td>
        <td class=centre>";
			if($row['pkey'] > 5) {
				echo("<input type=button value=Edit id=".$row['pkey']." class=edit />"); 
				/*if(strtolower($cmpcode)=="fin0001") {
					echo("<input type=button value=\"Preset ".$actname."\" onclick=\"presetAct(".$row["pkey"].",'status')\">"); 
				}*/
			}
		echo "</td>
    </tr>";
}
?>
</table>
<script type=text/javascript>
			$(function(){
				$("#tbl_list tr").attr("height","30px");
				$("#tbl_list td").css("vertical-align","middle");
				$('#colorSelector').ColorPicker({
					color: '#ffffff',
					onShow: function (colpkr) {
						$(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						$(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						document.getElementById('clr').value = hex;
						document.getElementById('clr').style.backgroundColor = '#' + hex;
						document.getElementById('clr').style.color = '#FFFFFF';
						$('#colorSelector').css('backgroundColor', '#' + hex);
					},
					onSubmit: function (hsb, hex, rgb) {
						document.getElementById('clr').value = hex;
						document.getElementById('clr').style.backgroundColor = '#' + hex;
						document.getElementById('clr').style.color = '#FFFFFF';
					}
				});
			});
</script>
<div id=dlg_edit title="Edit Status">
<form name=frm_edit action=setup_status_edit_process.php method=post>
	<input type=hidden name=act value=EDIT id=act />
	<table>
		<tr>
			<th>Ref:</th>
			<td><label for=ref id=lbl_ref></label><input type=hidden name=pkey id=ref </td>
		</tr>
		<tr>
			<th>Original Status:</th>
			<td><label for=old id=lbl_old></label><input type=hidden name=old id=old value='' /></td>
		</tr>
		<tr>
			<th>Updated Status:</th>
			<td><input type=text value='' name=edittxt id=new size=50 /></td>
		</tr>
		<tr>
			<th>Original Default %:</th>
			<td><label for=oldst id=lbl_oldst></label><input type=hidden name=oldst id=oldst value='' /></td>
		</tr>
		<tr>
			<th>Updated Default %:</th>
			<td><input type=text value='' name=editstate id=newst size=5 class=right />%</td>
		</tr>
	</table>
	<p><input type=button value="Save Changes" class=isubmit /> <input type=button value=Cancel class=cancel /> <span class=float><input type=button value=Delete* class=idelete /></span></p>
	<p>*Delete function is only available to statuses without any associated <?php echo strtolower($actname."s"); ?>.</p>
</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_edit").dialog({
		"modal": true,
		"autoOpen": false,
		"width": "500px"
	});
	$("input:button.edit").click(function() {
		var i = $(this).attr("id");
		if(confirm("Are you sure you wish to edit status "+i+"?\n\nThis will affect all tasks associated with this status.")==true) { 
			$("#dlg_edit input#ref").val(i);
			$("#dlg_edit #lbl_ref").text(i);
			var tc = $("#tbl_list #tc_"+i).text();
			if(tc>0) { $("#dlg_edit input:button.idelete").removeClass("idelete").attr("disabled",true); } else { $("#dlg_edit input:button.idelete").addClass("idelete").attr("disabled",false); }
			var v = $("#tbl_list #val_"+i).text();
			$("#dlg_edit input#old").val(v);
			$("#dlg_edit #lbl_old").text(v);
			var st = $("#tbl_list #state_"+i).text();
			if(st.length>0) {
				st = st.substr(0,st.length-1);
				$("#dlg_edit input#oldst").val(st);
				$("#dlg_edit #lbl_oldst").text(st+"%");
				$("#dlg_edit input#newst").val(st);
			} else {
				$("#dlg_edit input#oldst").val("");
				$("#dlg_edit #lbl_oldst").text("");
				$("#dlg_edit input#newst").val("");
			}
			$("#dlg_edit").dialog("open");
			$("#dlg_edit input#new").val(v).focus();
		}
	});
	$("#dlg_edit table th").addClass("left");
	$("#dlg_edit input:button.cancel").click(function() { $("#dlg_edit").dialog("close"); });
	$("#dlg_edit input:button.idelete").click(function() { 
		if(confirm("Are you sure you wish to delete this status?")==true) { 
			$("#dlg_edit input#act").val("DEL");
			$("#dlg_edit form[name=frm_edit]").submit();
		}
	});
	$("#dlg_edit input:button.isubmit").click(function() { 
		if(confirm("Are you sure you wish to save the changes to this status?")==true) { 
			$("#dlg_edit input#act").val("EDIT");
			$("#dlg_edit form[name=frm_edit]").submit();
		}
	});
});
</script>

</body>

</html>
