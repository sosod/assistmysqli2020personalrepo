<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php'; include("inc_amcharts.php"); ?>
<h1 class=fc><b>Complaints Assist</b></h1>
<?php


$call = array(
	'NEW'=>0,
	'IP'=>0,
	'CL'=>0
);

$sql = "SELECT callstatusid, count(callid) as cc FROM ".$dbref."_call WHERE callstatusid <> 5 GROUP BY callstatusid";
//$rs = getRS($sql);
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
	$cs = $row['callstatusid'];
	if(in_array($cs,$new_statusid)) {
		$call['NEW']+= $row['cc'];
	} elseif(in_array($cs,$closed_statusid)) {
		$call['CL']+= $row['cc'];
	} else {
		$call['IP']+= $row['cc'];
	}
}
//mysql_close();

//arrPrint($call);

if(array_sum($call)>0) {
	$sql = "SELECT calldate FROM ".$dbref."_call LIMIT 1";
	$row = $helper->mysql_fetch_one($sql);
	drawLegendStyle();
	echo "<table class=container style=\"margin: 0 auto;\"><tr><td class=center>
	<span class=b style=\"margin: 0 auto;\">Status of all Complaints logged since <br />".date("d F Y H:i:s",$row['calldate']).".</span>";
	drawJavaGraph($result_settings,$call,"view");
	echo "</td></tr>
	<tr><td class=center>";
	drawPieLegend($call,false,0);
	echo "<p><span class=i style=\"font-size: 6.5pt;\">Graph accurate as at<br />".date("d F Y H:i:s").".</span></p>
	</td></tr></table>";
	echo "<script type=text/javascript>
	$(function() {
		$('table.container, table.container td').addClass('noborder');
	});
	</script>";
}




?>
</body></html>