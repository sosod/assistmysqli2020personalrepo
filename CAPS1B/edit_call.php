<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';

//arrPrint($_SERVER);
$src = $_SERVER['HTTP_REFERER'];
$src = explode("/",$src);
$urlback = $src[count($src)-1];
$cancel_back = substr($urlback,0,1);
switch($cancel_back) {
	case "u": $cancel_back = "update.php"; break;
	case "v": default: $cancel_back = "view.php"; break;
}

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

$callid = isset($_REQUEST['callid']) ? $_REQUEST['callid'] : 0;
$b = isset($_REQUEST['b']) ? $_REQUEST['b'] :"";
$req = "edit_call.php?callid=".$callid."&b=".$b;
$logsort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array("logdateact","DESC");


$sql = "SELECT * FROM ".$dbref."_call WHERE callid = ".$callid;
$call = $helper->mysql_fetch_one($sql);
foreach($call as $key => $c) {
	$call[$key] = ASSIST_HELPER::decode($c);
}

/*$callref = $_POST['callref'];
$calldate = $_POST['calldate'];
$calltkid = $_POST['logtkid'];
$callconname = $_POST['callconname'];
$callcontel = $_POST['callcontel'];
$callconaddress1 = $_POST['callconaddress1'];
$callconaddress2 = $_POST['callconaddress2'];
$callconaddress3 = $_POST['callconaddress3'];
$callarea = $_POST['callarea'];
$callrespid = $_POST['callrespid'];
$callurgencyid = $_POST['callurgencyid'];
$callmessage = $_POST['callmessage'];
$callid = $_POST['callid'];
$callstatusid = $_POST['callstatusid'];*/
?>
<h1>Edit a call</b></h1>
<?php $helper->displayResult($result); ?>
<h2>Complaint Details</h2>
        <form name=logcall action=edit_process.php method="post"  enctype="multipart/form-data">
            <input type="hidden" name="utype" id="utype" value="<?php $utype = isset($_REQUEST['utype']) ? $_REQUEST['utype'] : "";echo $utype; ?>" />
            <input type="hidden" name="callid" id="callid" value="<?php echo $callid; ?>" />
			<input type=hidden name=src value="<?php echo $urlback; ?>" />
            <table id=complaint width=650>
                <tr>
                    <th  width=150>Reference:</th>
                    <td  width=500>
                        <?php
                        echo($call['callref']);
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Date Logged:</th>
                    <td >
                        <?php
                        echo(date("d-M-Y H:i", $call['calldate']));
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Logged by:</th>
                    <td >
                        <?php
                        $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$call['calltkid']."'";
                        //include("inc_db_con.php");
                        //$row = mysql_fetch_array($rs);
						$row = $helper->mysql_fetch_one($sql);
                        echo($row['tkname']." ".$row['tksurname']);
                        //mysql_close();
                        ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Edited By:</th>
                    <td ><?php echo($tkname); ?><input type=hidden name=calltkid value="<?php echo($tkid); ?>"></td>
                </tr>
                <tr>
                    <th>Resident's Name:</th>
                    <td ><input type=text name=callconname size=40 maxlength=90 value="<?php echo $call['callconname']; ?>"></td>
                </tr>
                <tr>
                    <th>Resident's Telephone:</th>
                    <td ><input type=text name=callcontel size=40 maxlength=90 value="<?php echo $call['callcontel']; ?>"></td>
                </tr>
                <tr>
                    <th>Resident's Cellphone:</th>
                    <td ><input type=text name=callconcell size=40 maxlength=90 value="<?php $callconcell = isset($call['callconcell']) ?$call['callconcell'] : ""; echo $callconcell; ?>"></td>
                </tr>
                <tr>
                    <th  rowspan=3>Resident's Address:</th>
                    <td ><input type=text name=callconaddress1 size=40 maxlength=90 value="<?php echo $call['callconaddress1']; ?>"></td>
                </tr>
                <tr>
                    <td ><input type=text name=callconaddress2 size=40 maxlength=90 value="<?php echo $call['callconaddress2']; ?>"></td>
                </tr>
                <tr>
                    <td ><input type=text name=callconaddress3 size=40 maxlength=90 value="<?php echo $call['callconaddress3']; ?>"></td>
                </tr>
                <tr>
                    <th>Area/Ward/Town:</th>
                    <td ><select name=callareaid><option value=0 <?php
                            $callareaid = isset($call['callareaid']) ?$call['callareaid'] : 0;
                            echo ($callareaid==0 ? "selected" : ""); ?>>Unspecified</option><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_area WHERE yn = 'Y' OR id = ".$callareaid." ORDER BY sort,value";
                    $rows = $mydb->mysql_fetch_value_by_id($sql, "id", "value");
                    foreach($rows as $i => $a) {
                    	echo "<option value=".$i." ".($callareaid==$i ? "selected" : "").">".$a."</option>";
                    }
                     ?></select> or <input type=text name=callarea size=40 maxlength=90 value="<?php echo $call['callarea']; ?>"></td>
                </tr>

                <tr>
                    <th>Department:</th>
                    <td ><select name=calldeptid><option value=0 <?php
                            $calldeptid = isset($call['calldeptid']) ?$call['calldeptid'] : 0;
                            echo ($calldeptid==0 ? "selected" : ""); ?>>Unspecified</option><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_dept WHERE yn = 'Y' OR id = ".$calldeptid." ORDER BY sort,value";
                    $rows = $mydb->mysql_fetch_value_by_id($sql, "id", "value");
                    foreach($rows as $i => $a) {
                    	echo "<option value=".$i." ".($calldeptid==$i ? "selected" : "").">".$a."</option>";
                    }
                     ?></select></td>
                </tr>
                <tr>
                    <th>Respondant:</th>
                    <td >
                        <select name=callrespid><option value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant r, assist_".$cmpcode."_timekeep t
                                WHERE r.yn = 'Y' AND t.tkid = r.admin ORDER BY value ASC";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while ($row = mysql_fetch_array($rs)) {
                            foreach ($rows as $row){
                                $id = $row['id'];
                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
                                echo("<option value=".$id." ".($call['callrespid'] == $id ? "selected=selected" : "").">".$val."</option>");
								if($call['callrespid']==$id) { $admin = $row['tkname']." ".$row['tksurname']; }
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td >
                        <select size="1" name="callstatusid">
                            <option value='X'>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' ORDER BY sort ASC";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while ($row = mysql_fetch_array($rs)) {
                            foreach ($rows as $row){
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option value=".$id." ".($call['callstatusid'] == $id ? "selected=selected" : "").">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Urgency:</th>
                    <td ><select size="1" name="callurgencyid"><option  value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE yn = 'Y' ORDER BY sort ASC";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while ($row = mysql_fetch_array($rs)){
                            foreach ($rows as $row){
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option value=".$id." ".($call['callurgencyid'] == $id ? "selected=selected" : "").">".$val."</option>");
                            }
                            ?>
                        </select></td>
                </tr>

                <tr>
                    <th>Elecctricity - Type of Complaint:</th>
                    <td ><select name=calltypeid><option value=0 <?php
                            $calltypeid = isset($call['calltypeid']) ?$call['calltypeid'] : 0;
                            echo ($calltypeid==0 ? "selected" : ""); ?>>Unspecified</option><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_electype WHERE yn = 'Y' OR id = ".$calltypeid." ORDER BY sort,value";
                    $rows = $mydb->mysql_fetch_value_by_id($sql, "id", "value");
                    foreach($rows as $i => $a) {
                    	echo "<option value=".$i." ".($calltypeid==$i ? "selected" : "").">".$a."</option>";
                    }
                     ?></select></td>
                </tr>
                <tr>
                    <th>Complaint:</th>
                    <td >
                        <textarea rows="6" name="callmessage" cols="40"><?php echo $call['callmessage']; ?></textarea></td>
                </tr>

                <tr>
                    <th>Radio Respondant:</th>
                    <td ><input type=text name=callradio size=40 maxlength=90 value="<?php $callradio = isset($call['callradio']) ?$call['callradio'] : "";  echo $callradio; ?>"></td>
                </tr>

                <tr>
                    <th>Resident's Vehicle:</th>
                    <td ><input type=text name=callvehicle size=40 maxlength=90 value="<?php $callvehicle = isset($call['callvehicle']) ?$call['callvehicle'] : ""; echo $callvehicle; ?>"></td>
                </tr>
                <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while ($row = mysql_fetch_array($rs)) {
                            foreach ($rows as $row){
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];

                                //include("inc_db_con2.php");
                                $udf = $helper->db_get_num_rows($sql2);
                                $row2 = $helper->mysql_fetch_one($sql2);
                                //mysql_close($con2);
                ?>
                                <tr>
                                    <th><?php echo($row['udfivalue']); ?>:</th>
                            <td >
                        <?php
                                switch ($row['udfilist'])
                                {
                                    case "Y":
                                        if ($helper->checkIntRef($row2['udfvalue']))
                                        {
                                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                            //include("inc_db_con2.php");
                                            if ($helper->db_get_num_rows($sql2) > 0)
                                            {
                                                $row3 = $helper->mysql_fetch_one($sql2);
                                            }
                                            //mysql_close($con2);
                                        }
                                        echo("<select name='".$row['udfiid']."'><option value=X>---SELECT---</option>");
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                                        //include("inc_db_con2.php");
                                        $rows = $helper->mysql_fetch_all($sql2);
                                        //while ($row2 = mysql_fetch_array($rs2)){
                                        foreach ($rows as $row2){
                                            echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                        }
                                        //mysql_close($con2);
                                        echo("</select>");
                                        break;
                                    case "T":
                                        if (empty($row2['udfid']))
                                        {
                                            echo("<input type=text name='".$row['udfiid']."' value='' size='50'>");
                                        }
                                        else
                                        {
                                            echo("<input type=text name='".$row['udfiid']."' value='".$row2['udfvalue']."' size='50'>");
                                        }
                                        break;
                                    case "M":
                                        if (empty($row2['udfid']))
                                        {
                                            echo ("<textarea name='".$row['udfiid']."' rows='5' cols='40' ></textarea>");
                                        }
                                        else
                                        {
                                            echo("<textarea name='".$row['udfiid']."' rows='5' cols='40' >".$row2['udfvalue']."</textarea>");
                                        }
                                        break;
                                    case "D":
                                        echo "<input class='date-type' type='text' name='".$row['udfiid']."' value='".(empty($row2['udfid']) ? "" : $row2['udfvalue'])."' size='15' readonly='readonly'/>";
                                        break;
                                    case "N":
                                        echo "<input type='text' name='".$row['udfiid']."' value='".(empty($row2['udfid']) ? "" : $row2['udfvalue'])."' size='15' />&nbsp;<small><i>Only numeric values are allowed.</i></small>";
                                        break;
                                    default:
                                        //echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                        break;
                                }
                        ?>
                                &nbsp;</td>
                        </tr>


<?php }; ?>
                <tr>
                    <th>Attach document(s):</th>
                    <td>
						<span class=float><a href="javascript:void(0)" id="attachlinkMAIN">Attach another file</a></span>
						<div id=firstdoc><input type="file" name="attachments[]" id="attachment" size="30"/></div>
					</td>
                </tr>
                <?php
                            $sql = "SELECT * FROM  assist_".$cmpcode."_caps_attachments AS c WHERE c.callid = ".$callid;
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                ?>
                
                            <tr>
                                <th>Attachment(s):</th>
                                <td>
                        <?php
                            if (!$helper->db_get_num_rows($sql))
                            {
                                echo ("<i>No attachments found.</i>");
                            }
                            else
                            {
                                echo "<table style='border:none;'>";
                            }

                            //while ($row = mysql_fetch_assoc($rs)) {
                            foreach ($rows as $row){
                                $file = "../files/".$cmpcode."/CAPS/".$row['system_filename'];
                                if (!file_exists($file))
                                    continue;
                                $call_attach_id = $row['id'];

                                echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:void(0)' class=downloadAttach id=".$call_attach_id.">".$row['original_filename']."</a>
                       </td>
                       </tr>");
                            }
                            if ($helper->db_get_num_rows($sql) > 0)
                            {
                                echo("</table>");
                            }
                        ?>

                        </td>
                    </tr>
                    <tr>
						<th></th>
                        <td >
                            <input type="submit" value="Save Changes" name="B1" class=isubmit />
                            <input type="reset" value="Reset" name="B2">
                            <span class=float><input type=button value="Cancel Complaint" class=idelete id=cancelme /></span>       
							</td>
                    </tr>
                </table>

            </form>
			<?php $helper->displayGoBack(); ?>
            <h2 class=fc>Updates</h2>
            <table id=update  width=650>
<?php
$callstatusid = isset($callstatusid) ? $callstatusid : 0;
                            if ($callstatusid != 4 && $callstatusid != 5)
                            {
?>
                                <!-- <form name=update method=post action=view_call_update.php enctype="multipart/form-data">
                                    <tr>
                                        <th>Date</td>
                                        <th colspan=2>Update message</td>
                                    </tr>

                                    <tr>
                                        <td class=center><input type=hidden name=logdate value=<?php echo $today; ?>><?php echo(date("d-M-Y H:i", $today)); ?></td>
                                        <td colspan=2><textarea cols=80 rows=7 name=logadmintext></textarea><br>
                                            <input type=hidden name=email value=Y>
                                            <input type=hidden name=utype value=U>
                                            <input type=hidden size=1 name=logtkid value=<?php echo($calltkid); ?>>
                                            <input type=hidden size=1 name=logurgencyid value=<?php echo($callurgencyid); ?>>
                                            <input type=hidden size=1 name=logrespid value=<?php echo($callrespid); ?>>
                                            <input type=hidden size=1 name=logstatusid value=<?php echo($callstatusid); ?>>
                                            <input type=hidden size=1 name=callid value=<?php echo($callid); ?>>
											<span class=float style="margin-top: 5px"><a href="javascript:void(0)" id="attachlink">Attach another file</a></span>
											<div id=firstdocUP style="margin-top: 5px;"><input type="file" name="attachments[]" id="attachment" size="30"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class=center>
                                            <input type=submit class=isubmit value="<?php echo("Send message to ".$admin); ?>">
                                        </td></tr>
                                </form> -->
            <?php
                            }
								
								$sql = "SELECT * FROM  `".$dbref."_attachments` WHERE logid IN (
										SELECT logid FROM ".$dbref."_log WHERE logcallid =32585)";
								//$rs = getRS($sql);
                                $rows = $helper->mysql_fetch_all($sql);
								$attach = array();
								//while($row = mysql_fetch_assoc($rs)) {
								foreach ($rows as $row){
								//arrPrint($row);
									$attach[$row['logid']][] = $row;
								}
								//mysql_close();//arrPrint($attach);

                                $sql = "SELECT a.logid, a.logemail, a.logdateact, p.admin, a.logsubmittkid,  ";
                                $sql.= "a.logdate, a.logadmintext, s.value as status, a.logstatusid, ";
                                $sql.= "t.tkname as n, t.tksurname as sn, tb.tkname as nb, tb.tksurname as snb, logutype ";
                                $sql.= "FROM assist_".$cmpcode."_caps_log a, ";
                                $sql.= "assist_".$cmpcode."_caps_list_status s, ";
                                $sql.= "assist_".$cmpcode."_timekeep t, ";
                                $sql.= "assist_".$cmpcode."_timekeep tb, ";
                                $sql.= "assist_".$cmpcode."_caps_list_respondant p ";
                                $sql.= "WHERE a.logcallid = ".$callid." ";
                                $sql.= "AND a.logstatusid = s.id ";
                                $sql.= "AND a.logrespid = p.id ";
                                $sql.= "AND p.admin = t.tkid ";
                                $sql.= "AND a.logsubmittkid = tb.tkid ";
                                $sql.= "AND a.logadmintext <> '' ";
                                $sql.= "AND (a.logutype = 'U' OR a.logutype = 'A' OR a.logutype='E') ";
                                $sql.= "ORDER BY ".$logsort[0]." ".$logsort[1].", logid DESC";
								$logs = $helper->mysql_fetch_all($sql);
								if(count($logs)>0) {
									echo "<tr class=\"ui-widget ui-state-white\">
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdate></span>Date Logged</th>
											<th>Update</th>
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdateact></span>Details</th>
										</tr>";
									foreach($logs as $l) {
										$i = $l['logid'];//echo $i;
										$attachments = array();
                                        $attach_arr = isset($attach[$i]) ? $attach[$i] : array();
										foreach($attach_arr as $a) {
											if(file_exists("../files/".$cmpcode."/CAPS/".$a['system_filename'])) {
												$attachments[] = "<a href=javascript:void(0) class=downloadAttach id=".$a['id'].">".$a['original_filename']."</a>";
											}
										}
										if(count($attachments)>0) {
											$log_attach = "<br />Attachment".(count($attachments)>1 ? "s" : "").":<br />".implode("<br />",$attachments);
										} else {
											$log_attach = "";
										}
										
										if(strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):")!==false) {
											$log_text = substr($helper->decode($l['logadmintext']),0,strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):"));
										} else {
											$log_text = $helper->decode($l['logadmintext']);
										}
										//$attach_pos = strpos(decode($l['logadmintext']),"Complaint Attachment(s):");
										//$log_text = $attach_pos!==false ? "true" : "false";
										
										echo "<tr>
												<td class=center>".date("d-M-Y H:i",$l['logdate'])."</td>
												<td>".$log_text;
												//arrPrint($attachments);
                                        $style = isset($style) ? $style : "";
										echo "
												".$log_attach."</td>
												</td><td>
												<span class=b>Status: </span>
												<br /><span class=\"".$style."\">".$l['status']."</span>
												<br /><span class=b>Respondant: </span>
												<br />".$l['n']." ".$l['sn']."
												".(date("dMYHi",$l['logdate'])!=date("dMYHi",$l['logdateact']) ? "<br /><span class=b>Action taken: </span><br />".date("d-M-Y H:i",$l['logdateact']) : "")."
												</td>
											</tr>";
									}
								}
								//mysql_close();
        ?>
                            </table>
<?php
$helper->displayGoBack($urlback,"Go Back");
                            //include("inc_back.php");
?>
        <script type ="text/javascript">
            $(document).ready(function(){
				var logsort = new Array('<?php echo $logsort[0]; ?>','<?php echo $logsort[1]; ?>');
				var sorting = new Array();
				sorting['logdate'] = "logdateact";
				sorting['logdateact'] = "logdate";
				sorting['DESC'] = "ASC";
				sorting['ASC'] = "DESC";
				
				
				$("#update #logdate, #update #logdateact").click(function() {
					var url = '<?php echo $req; ?>';
					var id = $(this).attr("id");
					var newsort = new Array();
					newsort[0] = id;
					if(id==logsort[0]) {
						newsort[1] = sorting[logsort[1]];
					} else {
						newsort[1] = "DESC";
					}
					url = url+"&sort[]="+newsort[0]+"&sort[]="+newsort[1];
					document.location.href = url;
				});
				$("#update th").addClass("center");
				$("#complaint th").addClass("left");
				$("#complaint th").addClass("top");
				
                $('#attachlink').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdocUP');
                });
                $('#attachlinkMAIN').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdoc');
                });
				$("#cancelme").click(function() {
					if(confirm("Are you sure you wish to cancel this complaint?")==true) {
						url = 'view_call_update.php?utype=C&callid=<?php echo $callid; ?>&src=<?php echo $cancel_back; ?>&logstatusid=<?php echo $cancel_statusid; ?>';
						document.location.href = url;
					}
				});
            });

        </script>

    </body>

</html>
