<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';

echo "<h1>Setup >> User Defined Fields</h1>";

if(!isset($_REQUEST['act']) || !(strlen($_REQUEST['act'])>0)) {
	$result = array("error","Ignite Assist could not determine your requested action.  Please try again.");
} else {
	$i = $_REQUEST['udfiid'];
	$v = $_REQUEST['udfivalue'];
	$r = $_REQUEST['udfirequired'];
	$l = $_REQUEST['udfilist'];
	
	switch($_REQUEST['act']) {
	case "ADD":
		$sql = "INSERT INTO assist_".$cmpcode."_udfindex (udfiid,udfivalue,udfilist,udfiref,udficustom,udfisort,udfiyn,udfirequired,udfidefault) VALUES (null,'".code($v)."','$l','$modref','N',1,'Y',$r,'')";
		$i = $helper->db_insert($sql);
		if($helper->checkIntRef($i)) {
			$log = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,'$today','$tkid','$modref','Created UDF $i','".addslashes($sql)."','')";
            $helper->db_insert($log);
			$result = array("ok","UDF \'".$helper->code($v)."\' has been created successfully.");
		} else {
			$result = array("error","Ignite Assist was unable to create the UDF.  Please try again.");
		}
		break;
	case "EDIT":
		$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiid = $i";
		$old = $helper->mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET
					udfivalue = '".$helper->code($v)."',
					udfilist = '$l',
					udfirequired = $r
				WHERE udfiid = $i";
		$mar = $helper->db_update($sql);
		if($mar>0) {
			$log = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,'$today','$tkid','$modref','Edited UDF $i','".addslashes($sql)."','".serialize($old)."')";
            $helper->db_insert($log);
			$result = array("ok","UDF \'".$helper->code($v)."\' has been edited successfully.");
		} else {
			$result = array("info","No change was found to be saved.");
		}
		break;
	case "DELETE":
		$sql = "UPDATE assist_".$cmpcode."_udfindex SET udfiyn = 'N' WHERE udfiid = ".$i;
		$mar = $helper->db_update($sql);
		if($mar>0) {
			$log = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,'$today','$tkid','$modref','Deleted UDF $i','".addslashes($sql)."','')";
            $helper->db_insert($log);
			$result = array("ok","UDF ".$i." has been successfully deleted.");
		} else {	
			$result = array("info","No change was found to be saved.");
		}
		break;
	default:
		$result = array("error","Ignite Assist could not determine your requested action.  Please try again.");
	}
}

echo "
<script type=text/javascript>
	document.location.href = 'setup_udf.php?r[]=".$result[0]."&r[]=".$result[1]."';
</script>
";

?>