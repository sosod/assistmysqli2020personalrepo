<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once "inc_head.php";

$req = "update_call.php?i=".$_REQUEST['i']."&b=".(isset($_REQUEST['b']) ? $_REQUEST['b'] : "");
$logsort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array("logdateact","DESC");

?>
        <h1>View status</h1>
        <?php
        $helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
        $callid = $_REQUEST['i'];
        $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
        //include("inc_db_con.php");
        //$row = mysql_fetch_array($rs);
		$row = $helper->mysql_fetch_one($sql);
		//arrPrint($row);
        $callref = $row['callref'];
        $callurgencyid = $row['callurgencyid'];
        $callstatusid = $row['callstatusid'];
        $callrespid = $row['callrespid'];
        $callmessage = $row['callmessage'];
        $calltkid = $row['calltkid'];
        $callarea = $row['callarea'];
        $calldate = $row['calldate'];
        $callconname = $row['callconname'];
        $callcontel = $row['callcontel'];
        $callconcell = isset($row['callconcell']) ? $row['callconcell'] : "";
        $callradio = isset($row['callradio']) ? $row['callradio'] : "";
        $callvehicle = isset($row['callvehicle']) ? $row['callvehicle'] : "";
        $callconaddress1 = $row['callconaddress1'];
        $callconaddress2 = $row['callconaddress2'];
        $callconaddress3 = $row['callconaddress3'];
        $callareaid = isset($row['callareaid']) ? $row['callareaid'] : 0;
        //mysql_close();
		$sql = "SELECT * FROM ".$dbref."_list_area WHERE id = ".$callareaid; //echo $sql;
		$area = $helper->mysql_fetch_one($sql); //arrPrint($x);
		$callareaid = $area['value'];
		if(strlen($callareaid)>0) {
			if(strlen($callarea)>0) {
				$callarea= $callareaid." ($callarea)";
			} else {
				$callarea = $callareaid;
			}
		}
		$calldeptid = isset($row['calldeptid']) ? $row['calldeptid'] : 0;
        $electypeid = isset($row['calltypeid']) ? $row['calltypeid'] : 0;
        $respondantid = isset($row['callrespid']) ? $row['callrespid'] : 0;
		$sql = "SELECT * FROM ".$dbref."_list_dept WHERE id = ".$calldeptid; //echo $sql;
		$area = $helper->mysql_fetch_one($sql); //arrPrint($x);
		$calldept = $area['value'];
		$sql = "SELECT * FROM ".$dbref."_list_electype WHERE id = ".$electypeid; //echo $sql;
		$area = $helper->mysql_fetch_one($sql); //arrPrint($x);
		$calltypeid = $area['value'];
		$call_users = array($row['calltkid']);
		$sql = "SELECT * FROM ".$dbref."_list_respondant WHERE id = ".$respondantid; //echo $sql;
		$resp = $helper->mysql_fetch_one($sql); //arrPrint($x);
		$call_users[] = $resp['admin'];
		$call_users[] = $resp['hodept'];
		$call_users[] = $resp['hodiv'];
        ?>
        <h2>Current status</h2>
        <table width=650>
            <tr>
                <th  width=150>Reference:</b></th>
                <td  width=500>
                    <?php
                    echo($callref);
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Date Logged:</b></th>
                <td >
                    <?php
                    echo(date("d-M-Y H:i", $calldate));
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Logged by:</b></th>
                <td >
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$calltkid."'";
                    //include("inc_db_con.php");
                    $row = $helper->mysql_fetch_one($sql);
                    $loggedBy = $row['tkname']." ".$row['tksurname'];
                    echo($row['tkname']." ".$row['tksurname']);
                    //mysql_close();
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Resident's Name:</b></th>
                <td >
                    <?php
                    echo($callconname."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Telephone:</b></th>
                <td >
                    <?php
                    echo($callcontel."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Cellphone:</b></th>
                <td >
                    <?php
                    echo($callconcell."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Address:</b></th>
                <td >
                    <?php
                    if (strlen($callconaddress1) > 0)
                    {
                        echo($callconaddress1);
                    }
                    if (strlen($callconaddress1) > 0 && strlen($callconaddress2) > 0)
                    {
                        echo("<br>");
                    }
                    if (strlen($callconaddress2) > 0)
                    {
                        echo($callconaddress2);
                    }
                    if ((strlen($callconaddress1) > 0 || strlen($callconaddress2) > 0) && strlen($callconaddress3) > 0)
                    {
                        echo("<br>");
                    }
                    if (strlen($callconaddress3) > 0)
                    {
                        echo($callconaddress3);
                    }
                    echo("&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Area/Ward/Town:</b></th>
                <td >
                    <?php
                    echo($callarea."&nbsp;");
                    ?></td>
            </tr>
            <?php
                    //$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant WHERE id = ".$callrespid;
                    //include("inc_db_con.php");
                    //$resp = mysql_fetch_array($rs);
                    //mysql_close();
            ?>
            <tr>
                <th>Department:</b></th>
                <td >
                    <?php
                    echo($calldept."&nbsp;");
                    ?></td>
            </tr>
                    <tr>
                        <th>Dept/Division (Respondant):</b></th>
                        <td >
                    <?php
                    echo($resp['value']);
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Respondent:</b></th>
                <td >
                    <?php
                    $sql = "SELECT tk.tkname n, tk.tksurname s FROM assist_".$cmpcode."_timekeep tk WHERE tk.tkid = '".$resp['admin']."'";
                    //include("inc_db_con.php");
                    $row = $helper->mysql_fetch_one($sql);
                    echo($row['n']." ".$row['s']);
                    $admin = $row['n']." ".$row['s'];
                    //mysql_close();
                    ?> &nbsp;</td>
            </tr>
            <tr>
                <th>Urgency:</b></th>
                <td >
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE id = ".$callurgencyid;
                    //include("inc_db_con.php");
                    $row = $helper->mysql_fetch_one($sql);
                    echo($row['value']);
                    //mysql_close();
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Electricity - Type of Complaint:</b></th>
                <td >
                    <?php
                    echo($calltypeid."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Status:</b></th>
                <td >
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE id = ".$callstatusid;
                    //include("inc_db_con.php");
                    $row = $helper->mysql_fetch_one($sql);
                    echo($row['value']);
                    //mysql_close();
                    ?>&nbsp;</td>
            </tr>
                <tr>
                    <th>Complaint:</b></th>
                    <td ><?php
                        $mess = str_replace(chr(10), "<br>", $callmessage);
                        echo($mess);
                    ?>&nbsp;</td>
                        </tr>
            <tr>
                <th>Radio Respondant:</b></th>
                <td >
                    <?php
                    echo($callradio."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Vehicle Registration:</b></th>
                <td >
                    <?php
                    echo($callvehicle."&nbsp;");
                    ?></td>
            </tr>
            <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                    //include("inc_db_con.php");
                $rows = $helper->mysql_fetch_all($sql);
                    //while ($row = mysql_fetch_array($rs)){
                foreach ($rows as $row){
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];
                        //include("inc_db_con2.php");
                        $udf = $helper->db_get_num_rows($sql2);
                        $row2 = $helper->mysql_fetch_one($sql2);
                        //mysql_close($con2);
            ?>
                        <tr>
                            <th><?php echo($row['udfivalue']); ?>:</b></th>
                            <td>
                    <?php
                        if ($udf > 0)
                        {
                            switch ($row['udfilist'])
                            {
                                case "Y":
                                    if ($helper->checkIntRef($row2['udfvalue']))
                                    {
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                        //include("inc_db_con2.php");
                                        $row3 = $helper->mysql_fetch_one($sql2);
                                        echo($row3['udfvvalue']);
                                        //mysql_close($con2);
                                    }
                                    else
                                    {
                                        echo ("N/A");
                                    }
                                    break;
                                case "T":
                                    //$sql = "SELECT * FROM assist_".$cmpcode."_".
                                    if ($row2['udfvalue'])
                                        echo $row2['udfvalue'];
                                    else
                                        echo "N/A";
                                    break;
                                default:
                                    echo(str_replace(chr(10), "<br>", $row2['udfvalue']));
                                    break;
                            }
                        }
                        else
                        {
                            echo("N/A");
                        }
                    }
                    ?>
                        &nbsp;</td>
                </tr>
                    <?php
                        $sql = "SELECT * FROM  assist_".$cmpcode."_caps_attachments AS c WHERE c.callid = ".$callid;
                        //$rs = getRS($sql); //include("inc_db_con.php");
                    $rows = $helper->mysql_fetch_all($sql);
                    ?>
                <tr>
                    <th>Attachment(s):</b></th>
                    <td class=tdgeneral>
                    <?php
                        if (!$helper->db_get_num_rows($sql))
                        {
                            echo ("<i>No attachments found.</i>");
                        }
                        else
                        {
                            echo "<table style='border:none;'>";
                        }

                        //while ($row = mysql_fetch_assoc($rs)) {
                        foreach ($rows as $row){
                            $file = "../files/".$cmpcode."/CAPS/".$row['system_filename'];
                            if (!file_exists($file))
                                continue;
                            $call_attach_id = $row['id'];

                            echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:void(0)' class=downloadAttach id=".$call_attach_id.">".$row['original_filename']."</a>
                       </td>
                       </tr>");
                        }
                        if ($helper->db_get_num_rows($sql) > 0)
                        {
                            echo("</table>");
                        }
                    ?>

                            </td>
                        </tr>
<?php
                        //if ($callstatusid != 4)
                        // {
?>
<?php
//echo $tkid; arrPrint($call_users); 
if($_SESSION['ia_support'] || in_array($tkid,$call_users)) { ?>
                        <tr><form name="edit" method="post" action="">
							<th></th>
                            <td>
                                    <input type='hidden' name='logadmintext' value="<?php echo("Call cancelled by ".$tkname); ?>">
                                    <input type='hidden' name='email' value='Y'>
                                    <input type='hidden' name='utype' value='C'>
                                    <input type='hidden'  name='logtkid' value='<?php echo($calltkid); ?>'>
                                    <input type='hidden'  name='logurgencyid' value='<?php echo($callurgencyid); ?>'>
                                    <input type='hidden'  name='logrespid' value='<?php echo($callrespid); ?>'>
                                    <input type='hidden'  name='logstatusid' value='5'>
                                    <input type='hidden'  name='callid' value='<?php echo($callid); ?>'>
                                    <input type='hidden'  name='callrespid' value='<?php echo($callrespid); ?>'>
                                    <input type='hidden'  name='callconname' value='<?php echo($callconname); ?>'>
                                    <input type='hidden'  name='callcontel' value='<?php echo($callcontel); ?>'>
                                    <input type='hidden'  name='callconaddress1' value="<?php echo($callconaddress1); ?>">
                                    <input type='hidden'  name='callconaddress2' value='<?php echo($callconaddress2); ?>'>
                                    <input type='hidden'  name='callconaddress3' value='<?php echo($callconaddress3); ?>'>
                                    <input type='hidden'  name='callarea' value='<?php echo($callarea); ?>'>
                                    <input type='hidden'  name='callrespid' value='<?php echo($callrespid); ?>'>
                                    <input type='hidden'  name='callmessage' value="<?php echo($callmessage); ?>">
                                    <input type='hidden'  name='callurgencyid' value='<?php echo($callurgencyid); ?>'>
                                    <input type='hidden'  name='callstatusid' value='<?php echo($callstatusid); ?>'>
                                    <input type='hidden'  name='callref' value='<?php echo($callref); ?>'>
                                    <input type='hidden'  name='calldate' value='<?php echo($calldate); ?>'>
                                    <input type='hidden'  name='loggedBy' value='<?php echo($loggedBy); ?>'>
                                    <input type='button' name="btnEdit" value="Edit" /> 
 <?php if(isset($DONOTDISPLAYTHISBUTTON) && $DONOTDISPLAYTHISBUTTON && !in_array($callstatusid,$not_ip)) { ?><span class=float><input type='button' name="btnCancel" value='Cancel Complaint' class=idelete /></span><?php } ?>
 </td>
                            </form>
                        </tr><?php } ?>
            <?php //} ?>
                    </table>

                    <h2 class=fc>Updates</h2>
                    <table width=650 id=update>
<?php
                        if ($callstatusid != 4 && $callstatusid != 5)
                        {
?>
                            <form name=update method=post action=view_call_update.php enctype="multipart/form-data">
                                <tr>
                                    <th>Date</th>
                                    <th colspan=2>Update message</th>
                                </tr>

                                <tr>
                                    <td class=center><input type=hidden name=logdate value=<?php echo $today; ?>><?php echo(date("d-M-Y", $today)); ?></td>
                                    <td  colspan=2><textarea cols=80 rows=7 name=logadmintext></textarea><br>
                                        <input type=hidden name=email value=Y>
                                        <input type=hidden name=utype value=U>
                                        <input type=hidden size=1 name=logtkid value=<?php echo($calltkid); ?>>
                                        <input type=hidden size=1 name=logurgencyid value=<?php echo($callurgencyid); ?>>
                                        <input type=hidden size=1 name=logrespid value=<?php echo($callrespid); ?>>
                                        <input type=hidden size=1 name=logstatusid value=<?php echo($callstatusid); ?>>
                                        <input type=hidden size=1 name=callid value=<?php echo($callid); ?>>
										<span style="margin-top: 5px;" class=float><a href="javascript:void(0)" id="attachlink">Attach another file</a></span>
										<div id=firstdoc style="margin-top: 5px;"><input type="file" name="attachments[]" id="attachment" size="30"/></div>
                                    </td>
                                </tr>
                                </tr>
                                <tr>
                                    <td colspan=3 class="center">
                                        <input type=submit class=isubmit value="<?php echo("Send message to ".$admin); ?>" />
                                    </td>
								</tr>
                            </form>
            <?php
                        }
								$sql = "SELECT * FROM  `".$dbref."_attachments` WHERE logid IN (
										SELECT logid FROM ".$dbref."_log WHERE logcallid =32585)";
								//$rs = getRS($sql);
                                $rows = $helper->mysql_fetch_all($sql);
								$attach = array();
								//while($row = mysql_fetch_assoc($rs)) {
								foreach ($rows as $row){
								//arrPrint($row);
									$attach[$row['logid']][] = $row;
								}
								//mysql_close();//arrPrint($attach);

                        /*$sql = "SELECT a.logemail, a.logdateact, p.admin, a.logsubmittkid, ";
                        $sql.= "a.logdate, a.logadmintext, s.value as status, ";
                        $sql.= "t.tkname as n, t.tksurname as sn, tb.tkname as nb, tb.tksurname as snb  ";*/
                                $sql = "SELECT a.logid, a.logemail, a.logdateact, p.admin, a.logsubmittkid,  ";
                                $sql.= "a.logdate, a.logadmintext, s.value as status, a.logstatusid, ";
                                $sql.= "t.tkname as n, t.tksurname as sn, tb.tkname as nb, tb.tksurname as snb, logutype ";
                        $sql.= "FROM assist_".$cmpcode."_caps_log a, ";
                        $sql.= "assist_".$cmpcode."_caps_list_status s, ";
                        $sql.= "assist_".$cmpcode."_timekeep t, ";
                        $sql.= "assist_".$cmpcode."_timekeep tb, ";
                        $sql.= "assist_".$cmpcode."_caps_list_respondant p ";
                        $sql.= "WHERE a.logcallid = ".$callid." ";
                        $sql.= "AND a.logstatusid = s.id ";
                        $sql.= "AND a.logrespid = p.id ";
                        $sql.= "AND p.admin = t.tkid ";
                        $sql.= "AND a.logsubmittkid = tb.tkid ";
                        $sql.= "AND (a.logutype = 'U' OR a.logutype = 'A' OR a.logutype='E') ";
                        $sql.= "ORDER BY logdateact DESC, logid DESC";
								$logs = $helper->mysql_fetch_all($sql);
								if(count($logs)>0) {
									echo "<tr class=\"ui-widget ui-state-white\">
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdate></span>Date Logged</th>
											<th>Update</th>
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdateact></span>Details</th>
										</tr>";
									foreach($logs as $l) {
										$i = $l['logid'];//echo $i;
										$attachments = array();
										if(isset($attach[$i])) {
											foreach($attach[$i] as $a) {
												if(file_exists("../files/".$cmpcode."/CAPS/".$a['system_filename'])) {
													$attachments[] = "<a href=javascript:void(0) class=downloadAttach id=".$a['id'].">".$a['original_filename']."</a>";
												}
											}
										}
										if(count($attachments)>0) {
											$log_attach = "<br />Attachment".(count($attachments)>1 ? "s" : "").":<br />".implode("<br />",$attachments);
										} else {
											$log_attach = "";
										}
										
										if(strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):")!==false) {
											$log_text = substr($helper->decode($l['logadmintext']),0,strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):"));
										} else {
											$log_text = $helper->decode($l['logadmintext']);
										}
										//$attach_pos = strpos(decode($l['logadmintext']),"Complaint Attachment(s):");
										//$log_text = $attach_pos!==false ? "true" : "false";
										$style=isset($style) ? $style : "";
										echo "<tr>
												<td class=center>".date("d-M-Y H:i",$l['logdate'])."</td>
												<td>".$log_text;
												//arrPrint($attachments);
										echo "
												".$log_attach."</td>
												</td><td>
												<span class=b>Status: </span>
												<br /><span class=\"".$style."\">".$l['status']."</span>
												<br /><span class=b>Respondant: </span>
												<br />".$l['n']." ".$l['sn']."
												".(date("dMYHi",$l['logdate'])!=date("dMYHi",$l['logdateact']) ? "<br /><span class=b>Action taken: </span><br />".date("d-M-Y H:i",$l['logdateact']) : "")."
												</td>
											</tr>";
									}
								}
								//mysql_close();

?>
                        </table>
        <script type ="text/javascript">
            $(document).ready(function(){
				var logsort = new Array('<?php echo $logsort[0]; ?>','<?php echo $logsort[1]; ?>');
				var sorting = new Array();
				sorting['logdate'] = "logdateact";
				sorting['logdateact'] = "logdate";
				sorting['DESC'] = "ASC";
				sorting['ASC'] = "DESC";
				
				$("#update th").addClass("center");
				
				$("#update #logdate, #update #logdateact").click(function() {
				//$("#update #logdate").click(function() {
					var url = '<?php echo $req; ?>';
					var id = $(this).attr("id");
					var newsort = new Array();
					newsort[0] = id;
					if(id==logsort[0]) {
						newsort[1] = sorting[logsort[1]];
					} else {
						newsort[1] = "DESC";
					}
					url = url+"&sort[]="+newsort[0]+"&sort[]="+newsort[1];
					document.location.href = url;
				});

                $('#attachlink').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdoc');
                });
                $('input[name="btnCancel"]').click(function(event){                    
                    if($(event.target).attr("name") == "btnCancel")
                    {
						if(confirm("Are you sure you with to cancel this complaint?")==true) {
							$('form[name="edit"]').attr("action","view_call_update.php");
							$('form[name="edit"]').submit();
						}
                    }
                });
                $('input[name="btnEdit"]').click(function(event){
                    if($(event.target).attr("name") == "btnEdit")
                    {                        
                        $('form[name="edit"]').attr("action","edit_call.php");
                    }
                    $('input[name="utype"]').attr("value","E");
                    $('form[name="edit"]').submit();
                });
				$("#update th").addClass("center");
            })
    
        </script>
        <script type="text/javascript">
            function download(path){
                document.location.href = "download.php?path="+path;
            }
            function Validate() {
                return false;
            }
        </script>


    </body>

</html>
