<?php
//require_once '../inc_session.php';
//error_reporting(-1);
require_once '../module/autoloader.php';
$mydb = new ASSIST_DB();
$helper = new CAPS1B();
$modref = strtoupper($helper->getModRef());
$cmpcode = $helper->getCmpCode();
$tkname = $helper->getUserName();
$tkid = $helper->getUserID();
$today = $helper->getToday();
$dbref = "assist_".strtolower($cmpcode)."_caps";

$headings = array();
	$headings['callref'] = array(
		'display'=>"Reference",
		'field'=>"callref",
		'type'=>"T",
		'table'=>""
	);
	$headings['calldate'] = array(
		'display'=>"Date Logged",
		'field'=>"calldate",
		'type'=>"D",
		'table'=>""
	);
	$headings['calltkid'] = array(
		'display'=>"Logged By",
		'field'=>"calltkid",
		'type'=>"Y",
		'table'=>"assist_".$cmpcode."_timekeep"
	);
	$headings['callconname'] = array(
		'display'=>"Resident's Name",
		'field'=>"callconname",
		'type'=>"T",
		'table'=>""
	);
	$headings['callcontel'] = array(
		'display'=>"Resident's Telephone",
		'field'=>"callcontel",
		'type'=>"T",
		'table'=>""
	);
	$headings['callconcell'] = array(
		'display'=>"Resident's Cellphone",
		'field'=>"callconcell",
		'type'=>"T",
		'table'=>""
	);
	$headings['callconaddress'] = array(
		'display'=>"Resident's Address",
		'field'=>"callconaddress",
		'type'=>"T",
		'table'=>""
	);
	$headings['callareaid'] = array(
		'display'=>"Area/Ward/Town",
		'field'=>"callareaid",
		'type'=>"Y",
		'table'=>$dbref."_list_area"
	);
	$headings['calldeptid'] = array(
		'display'=>"Department",
		'field'=>"calldeptid",
		'type'=>"Y",
		'table'=>$dbref."_list_dept"
	);
	$headings['callrespid'] = array(
		'display'=>"Assigned To",
		'field'=>"callrespid",
		'type'=>"Y",
		'table'=>$dbref."_list_respondant"
	);
	$headings['callurgencyid'] = array(
		'display'=>"Urgency",
		'field'=>"callurgencyid",
		'type'=>"Y",
		'table'=>$dbref."_list_urgency"
	);
	$headings['callmessage'] = array(
		'display'=>"Complaint",
		'field'=>"callmessage",
		'type'=>"M",
		'table'=>""
	);
	$headings['callstatusid'] = array(
		'display'=>"Status",
		'field'=>"callstatusid",
		'type'=>"Y",
		'table'=>$dbref."_list_status"
	);
$udfheadings = array();
	$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".$modref."'";
	//include("inc_db_con.php");
	//while($row = mysql_fetch_assoc($rs)) {
	$udfrows = $mydb->mysql_fetch_all($sql);
	foreach($udfrows as $row) {
		$udfheadings[$row['udfiid']] = array(
			'display'=>$row['udfivalue'],
			'field'=>$row['udfiid'],
			'type'=>$row['udfilist']
		);
	}
	//mysql_close($con);
$logheadings = array();
	$logheadings['logdate'] = array(
		'display'=>"Date last updated",
		'field'=>"logdate",
		'type'=>"D",
		'table'=>""
	);
	$logheadings['logdateact'] = array(
		'display'=>"Date action was taken",
		'field'=>"logdateact",
		'type'=>"D",
		'table'=>""
	);
	$logheadings['duration'] = array(
		'display'=>"Duration",
		'field'=>"duration",
		'type'=>"TM",
		'table'=>""
	);
	$logheadings['logadmin'] = array(
		'display'=>"Most recent update",
		'field'=>"logadmin",
		'type'=>"M",
		'table'=>""
	);
$head = array_merge($headings,$logheadings,$udfheadings);
$nosort = array("duration","logdate","logdateact","logadmin","callattachment","logattachment");
$nogroup = array("callref","callconname","callcontel","callconaddress","callmessage","callattachment","logattachment");
	foreach($udfheadings as $u) { 
		if($u['type']!="Y" && $u['type'] != "D") { $nogroup[] = $u['field']; }	//can't group by udf's which aren't lists or dates
		$nosort[] = $u['field'];	//can't sort by a udf
	}	
$nofilter = array("duration","logdateact");

?>