<?php
/***********************************
Get setup values:
0. Administrator
1. Can creator close the call?
***********************************/
/***********************************
Standardisation by Sondelani Dumalisile 05 May 2021
 ***********************************/
$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'CAPS' AND refid = 0";
$helper = new CAPS1B();
//include("inc_db_con.php");
$c = $helper->db_get_num_rows($sql);
$row = $helper->mysql_fetch_one($sql);
$setupadmn = $row['value'];
//mysql_close();
if($c == 0 || strlen($setupadmn) == 0)
{
	$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'CAPS', refid = 0, value = '0000', comment = 'Setup Administrator', field = ''";
	//include("inc_db_con.php");
	$helper->db_insert($sql);
	$setupadmn = "0000";
		$tref = "CAPS";
		$tsql = $sql;
		$trans = "Added setup administrator reference";
		//include("inc_transaction_log.php");
}

$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'CAPS' AND refid = 1";

//include("inc_db_con.php");
$c = $helper->db_get_num_rows($sql);
$row = $helper->mysql_fetch_one($sql);
$capsact = $row['value'];
//mysql_close();
if($c == 0 || strlen($setupadmn) == 0)
{
	$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'CAPS', refid = 1, value = 'N', comment = 'Can owner close complaints', field = ''";
	//include("inc_db_con.php");
	$helper->db_insert($sql);
	$capsact = "N";
		$tref = "CAPS";
		$tsql = $sql;
		$trans = "Added setup reference to complaints owner closing call";
		//include("inc_transaction_log.php");
}


/*
$sql = "SELECT * FROM assist_".$cmpcode."_caps WHERE tkid = '".$tkid."' AND yn = 'Y'";
include("inc_db_con.php");
$p = mysql_num_rows($rs);
if($p > 0)
{
    $hppl = mysql_fetch_array($rs);
}
mysql_close();
*/
?>
