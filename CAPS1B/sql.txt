CREATE TABLE IF NOT EXISTS `assist_ngoni12_caps_list_display` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `headingfull` varchar(255) NOT NULL,
  `headingshort` varchar(255) NOT NULL,
  `yn` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'Use heading at all',
  `type` varchar(1) NOT NULL COMMENT 'S=Std-U=UDF',
  `mydisp` varchar(1) NOT NULL DEFAULT 'N',
  `alldisp` varchar(1) NOT NULL DEFAULT 'N',
  `owndisp` varchar(1) NOT NULL DEFAULT 'N',
  `myyn` varchar(1) NOT NULL DEFAULT 'N',
  `allyn` varchar(1) NOT NULL DEFAULT 'N',
  `ownyn` varchar(1) NOT NULL DEFAULT 'N',
  `field` varchar(45) NOT NULL,
  `mysort` int(10) unsigned NOT NULL,
  `allsort` int(10) unsigned NOT NULL,
  `ownsort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1