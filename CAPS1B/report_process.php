<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
ini_set('max_execution_time',1800); 
include("inc_ignite.php");

//SUMMARY OF RESULTS
/*function drawKRSum($what,$txt,$val,$i) {
	global $output;
	global $styler;
	if(!checkIntRef($val)) { $val = 0; }
	switch($what) {
		case "title":
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"Summary of Results\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title2 style=\"text-align: left;\">Summary of Results</td>";
			} else {
				$echo ="</table><h2>Summary of Results</h2><table cellpadding=3 cellspacing=5 width=330>";
			}
			break;
		case "title0":
			$t = "Summary of Results: ".$txt;
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"".$t."\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title4 style=\"text-align: left;\">".$t."</td>";
			} else {
				$echo ="</table><h4 style=\"margin-bottom: 5px; margin-top: 10px;\">".$t."</h4><table cellpadding=3 cellspacing=5 width=330 style=\"margin-bottom: 20px;\">";
			}
			break;
		case "sum":
			if($output=="csv") {
				$echo ="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo = "<tr><td></td><td nowrap style=\"font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$txt."</td><td nowrap style=\"text-align: left; font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$val."</td></tr>";
			} else {
				$echo = "<tr><td class=b-w>&nbsp;&nbsp;</td><td class=b-w style=\"font-weight: bold;\">".$txt."</td><td class=b-w style=\"text-align: right;font-weight: bold;\">&nbsp;".$val."</td></tr>";
				$echo.= "</table>";
			}
			break;
		default:
			if($output=="csv") {
				$echo.="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo.= "<tr><td style=\"".$styler[$i]."\"> </td><td nowrap>".$txt."</td><td nowrap style=\"text-align: left;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td class=b-w style=\"".$styler[$i]."\">&nbsp;</td><td class=b-w>".$txt."</td><td class=b-w style=\"text-align: right;\">&nbsp;".$val."</td></tr>";
			}
			break;
	}
	return $echo;
}*/

/***** GET LISTS *****/
$lists = array();
//URGENCY
$lists['callurgencyid'] = array();
$sql = "SELECT * FROM ".$dbref."_list_urgency";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
		$lists['callurgencyid'][$row['id']] = array('id'=>$row['id'],'value'=>$helper->decode($row['value']));
	}
//mysql_close($con);
//RESPONDANT
$lists['callrespid'] = array();
$sql = "SELECT id, value, CONCAT_WS(' ',tkname,tksurname) as tk FROM ".$dbref."_list_respondant INNER JOIN assist_".$cmpcode."_timekeep ON admin = tkid ORDER BY value, tkname, tksurname";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
		$lists['callrespid'][$row['id']] = array('id'=>$row['id'],'value'=>$helper->decode($row['value']." (".$row['tk'].")"));
	}
//mysql_close($con);
//CREATOR
$lists['calltkidid'] = array();
$sql = "SELECT DISTINCT tkid, CONCAT_WS(' ',tkname,tksurname) as value FROM assist_".$cmpcode."_timekeep INNER JOIN ".$dbref."_call ON calltkid = tkid ORDER BY tkname, tksurname";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
		$lists['calltkid'][$row['tkid']] = array('id'=>$row['tkid'],'value'=>$helper->decode($row['value']));
	}
//mysql_close($con);
//STATUS
$lists['callstatusid'] = array();
$sql = "SELECT * FROM ".$dbref."_list_status";
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
		$lists['callstatusid'][$row['id']] = array('id'=>$row['id'],'value'=>decode($row['value']));
	}
//mysql_close($con);
//UDFS
foreach($udfheadings as $u) {
	if($u['type']=="L" || $u['type'] == "Y") {
		$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$u['field']." ORDER BY udfvvalue";
        $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
				$lists[$u['field']][$row['udfvid']] = array('id'=>$row['udfvid'],'value'=>$row['udfvvalue']);
			}
		//mysql_close($con);
	}
}


//echo "<pre>";  print_r($lists); echo "</pre>";

//GET VARIABLES
$field = array();
$field['callref'] = "Y";
$filter = array();
$filtertype = array();
$calls = array();
$src = $_REQUEST['src'];
$group = $_REQUEST['groupby'];
$sortarr = $_REQUEST['sort'];
$output = $_REQUEST['output'];
$rhead = $_REQUEST['rhead'];
$echocount = 0;
if(count($sortarr)==0) { $sort = array(); }
//GET FIELD HEADINGS
    foreach($head as $row)
    {
		$hf = $row['field'];
		if($src=="DASH" || (isset($_REQUEST[$hf]) && $_REQUEST[$hf]=="Y")) {
			$field[$hf] = "Y";
		}
		if(!in_array($hf,$nofilter)) {
			$filter[$hf] = $_REQUEST[$hf.'filter'];
			if($row['type']=="T" || $row['type']=="M") {
				$filtertype[$hf] = $_REQUEST[$hf.'filtertype'];
			}
			if(count($sortarr)==0 && !in_array($hf,$nosort)) {
				$sort[] = $hf;
			}
		}
    }
	$field['logadmin_type'] = isset($_REQUEST['logadmin_type']) ? $_REQUEST['logadmin_type'] : "RECENT";
if($field['logadmin_type']=="ALL") { $head['logadmin']['display'] = "All updates"; }
if(count($sortarr)==0) { $sortarr = $sort; $sort = null; }
//UDFS
foreach($udfheadings as $u) {
	if($u['type']=="L" || $u['type'] == "Y") {
		//$ufilt = $filter[$u['field']][0];
		//echo "<P>".$ufilt;
		//if($ufilt=="X") { $filter[$u['field']] = array(); }
		$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$u['field']." ORDER BY udfvvalue";
        $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
				$lists[$u['field']][$row['udfvid']] = array('id'=>$row['udfvid'],'value'=>$row['udfvvalue']);
				//if($ufilt == "X") { $filter[$u['field']][] = $row['udfvid']; }
			}
		//mysql_close($con);
	}
}

//GROUP BY
$glist = array();
if(strlen($group)==0 || $group == "X") {
	$group = "X";
	$groupid = 0;
	$glist[] = array('id'=>0,'value'=>"");
} else {
	$groupid = $group;
	if($head[$group]['type']=="Y" || $head[$group]['type'] == "L") {
		$glist = $lists[$group];
	} else {
		$sql = "SELECT DISTINCT $groupid FROM ".$dbref."_call";
        $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
			if($group=="calldate") {
				$gval = date("d M Y",$row[$groupid]);
			} else {
				$gval = $row[$groupid];
			}
			$gid = strFn("str_replace",strtolower($helper->decode($gval))," ","");
			$glist[$gval] = array('id'=>$gid,'value'=>$gval);
		}
		//mysql_close($con);
	}
}		

//echo "<pre>"; print_r($glist); echo "</pre>";
		
/**************** SET REPORT SQL ************/
//SELECT
$repsql[0] = "SELECT c.*";
$repsql[1] = ", l.logdate, l.logdateact, l.logadmintext, l.logutype";
//FROM
$repsql[2] = " FROM ".$dbref."_call c";
$repsql[2].= " LEFT OUTER JOIN ".$dbref."_log l ON l.logcallid = c.callid ";
foreach($headings as $h) {
	$hf = $h['field'];
	if($h['type']== "L" || $h['type'] == "Y") {
		$repsql[2].= " INNER JOIN ".$h['table']." ".$hf." ON ";
		switch($hf) {
			case "calltkid":
				$repsql[2].= $hf.".tkid = c.".$h['field'];
				break;
			default:
				$repsql[2].= $hf.".id = c.".$h['field'];
		}
	}
}
//WHERE
$repsql[2].= " WHERE logutype NOT IN ('E','U','C','T') ";
foreach($headings as $h) {
	$where = "";
	$hf = $h['field'];
	if(array_key_exists($hf,$lists)) {					//LISTS
		switch($hf) {
			case "calltkid":
				if(strtolower($filter[$hf][0])!="x") {
					if(count($filter[$hf])>1) {
						if(count($filter[$hf])!=count($lists[$hf])) {
							$where = "(";
							$where.= $hf." = '".implode("' OR ".$hf." = '",$filter[$hf]);
							$where.= "')";
						}
					} elseif(strlen($filter[$hf][0])>0) {
						$where = $hf." = '".$filter[$hf][0]."'";
					}
				}
				break;
			default:
				if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="x") {
					if(count($filter[$hf])!=count($lists[$hf])) {
						$where = "(";
						$where.= $hf.".id = ".implode(" OR ".$hf.".id = ",$filter[$hf]);
						$where.= ")";
					}
				} elseif(strtolower($filter[$hf][0])!="x" && $helper->checkIntRef($filter[$hf][0])) {
					$where = $hf.".id = ".$filter[$hf][0];
				}
				break;
		}
	} elseif($hf=="calldate") {
			$from = $filter[$hf][0];
			$to = $filter[$hf][1];
			if(strlen($from)>0 && strlen($to)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
			} elseif(strlen($from)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = mktime(23,59,59,getMonth($from[1]),$from[0],$from[2]);
			} elseif(strlen($to)>0) {
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
				$from = mktime(0,0,0,getMonth($to[1]),$to[0],$to[2]);
			}
			if(is_numeric($from) && is_numeric($to)) {
				$where = "c.calldate >= $from AND c.calldate <= $to";
			}
	} elseif(strlen($filter[$hf])>0) {							//TEXT FIELDS
			$filter[$hf] = trim($filter[$hf]);
			switch($hf) {
				case "callconaddress":
                    switch($filtertype[$hf])
                    {
						case "X":
							break;
                        case "all":
                        case "any":
							if($filtertype[$hf] == "all") { $c = "AND"; } else { $c = "OR"; }
                            $ft = array_unique(explode(" ",$helper->code($filter[$hf])));
							for($w=1;$w<4;$w++) {
								if($w>1) { $where.=" OR "; }
								$where.= "(".$hf.$w." LIKE '%".strFn("implode",$ft,"%' $c ".$hf.$w." LIKE '%","")."%')";
								$where = str_replace("$c $hf".$w." LIKE '%%'","",$where);
							}
                            break;
                        case "exact":
                        default:
                            $where = $hf."1 LIKE '%".$helper->code($filter[$hf])."%'";
                            $where.= " OR ".$hf."2 LIKE '%".$helper->code($filter[$hf])."%'";
                            $where.= " OR ".$hf."3 LIKE '%".$helper->code($filter[$hf])."%'";
                            break;
                    }
					break;
				default:
                    switch($filtertype[$hf])
                    {
						case "X":
							break;
                        case "all":
                            $ft = array_unique(explode(" ",$helper->code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' AND $hf LIKE '%","")."%')";
							$where = str_replace("AND $hf LIKE '%%'","",$where);
                            break;
                        case "any":
                            $ft = array_unique(explode(" ",$helper->code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' OR $hf LIKE '%","")."%')";
							$where = str_replace("OR $hf LIKE '%%'","",$where);
                            break;
                        case "exact":
                            $where = $hf." LIKE '%".$helper->code($filter[$hf])."%'";
                            break;
                        default:
                            $where = $hf." LIKE '%".$helper->code($filter[$hf])."%'";
                            break;
                    }
				break;
			}
	}
	if(strlen($where)>0) {
		$repsql[2].=" AND (".$where.")";
	}
}

if(isset($_REQUEST['logtkid']) && count($_REQUEST['logtkid'])>0 && $_REQUEST['logtkid'][0]!="ANY") {
	$where = " AND l.logtkid IN ('".implode("','",$_REQUEST['logtkid']);
	/*foreach($_REQUEST['logtkid'] as $lt) {
		$lt*=1;
		$where.="','".$lt;
	}*/
	$where.="')";
	$repsql[2].=$where;
}

foreach($logheadings as $l) {
	$where = "";
	$lf = $l['field'];
	switch($lf) {
		case "logdate":
			$from = $filter[$lf][0];
			$to = $filter[$lf][1];
			if(strlen($from)>0 && strlen($to)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
			} elseif(strlen($from)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = mktime(23,59,59,getMonth($from[1]),$from[0],$from[2]);
			} elseif(strlen($to)>0) {
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
				$from = mktime(0,0,0,getMonth($to[1]),$to[0],$to[2]);
			}
			if(is_numeric($from) && is_numeric($to)) {
				$where = "l.logdate >= $from AND l.logdate <= $to";
			}
			break;
		case "logadmin":
			$filt = $filter[$lf];
			if(strlen($filt)>0) {
			}
			break;
		default:
			break;
	}
	if(strlen($where)>0) {
		$repsql[2].=" AND ".$where;
	}
}

//SQL ORDER
$repsql[3] = " AND callstatusid <> 5 ORDER BY ";
$r=0;
foreach($sortarr as $s) {
	$r++;
	if($r>1) { $repsql[3].=", "; }
	if($s=="callref") {
		$repsql[3].= " c.callid DESC";
	} elseif($s=="calltkid") {
		$repsql[3].= " calltkid.tkname ASC, calltkid.tksurname ASC";
	} elseif($s=="callconaddress") {
		$repsql[3].= " c.callconaddress1 ASC, c.callconaddress2 ASC, c.callconaddress3 ASC";
	} elseif(isset($lists[$s]) ) {
		$repsql[3].= " ".$s.".value ASC";
	} else {
		$repsql[3].= " c.".$s." ASC";
	}
}
/************* END REPORT SQL ***************/

//GET CALLs
$gcall = array();
$objects = array();
$sql = implode(" ",$repsql);
//echo "<pre>"; print_r($filter); echo "</pre>";
//include("inc_db_con.php");
//$rs = getRS($sql);
if($_SESSION['ia_support']) {
	//echo "<p>".$sql."<br />".mysql_num_rows($rs);
}
$gcalls = array();
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
		$objects[] = $row['callid'];
		if(is_numeric($groupid)) { 
			$calls[0][$row['callid']] = $row;
			$gcalls[$row['callid']] = 0;
		} elseif($head[$group]['type']=="Y" || $head[$group]['type'] == "L") {
			$calls[$row[$groupid]][$row['callid']] = $row;
			$gcalls[$row['callid']] = $row[$groupid];
		} else {
			if($groupid=="calldate") {
				$gval = date("d M Y",$row[$groupid]);
			} else {
				$gval = $row[$groupid];
			}
			$gval = strFn("str_replace",strtolower($helper->decode($gval))," ","");
			$calls[$gval][$row['callid']] = $row;
			$gcalls[$row['callid']] = $gval;
		}
	}
//mysql_close($con);

/*** GET UDFS ***/
$udfs = array();
if(count($objects)>0) {
$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfref = '$modref' AND udfnum IN (".implode(",",$objects).")";
//include("inc_db_con.php");
    $rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
	$udfs[$row['udfnum']][$row['udfindex']] = $row;
}
//mysql_close($con);
}
//OUTPUT
$echo = "";
$cspan = 1 + count($field) - 4;
switch($output) 
{
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$rowa = "";
	$rowz = "\r\n";
	if(strlen($rhead)>0) {
		$pagea = "\"".$rhead."\"\r\n";
	} else {
		$pagea = "\"Complaints Assist Report\"\r\n";
	}
	$table[1] = "";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[2] = "<td class=call>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=call style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=call style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$rowa = chr(10)."<tr>";
	$rowb = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>Complaints Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>".chr(10)." td { font-style: Calibri; font-size:11pt; } .call { border-width: thin; border-color: #000000; border-style: solid; } .head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } ".chr(10)." .title { font-size:20pt; font-style: Calibri; color:#000066; font-weight:bold; text-decoration:underline; text-align: center; } ".chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#000066; font-weight:bold; text-decoration:underline; text-align: center;} .title3 { font-size:14pt; font-style: Calibri; color:#000066; font-weight:bold;} .title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$cspan>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$cspan>";
	if(strlen($rhead)>0) {
		$pagea.= $rhead;
	} else {
		$pagea.= "Complaints Assist Report";
	}
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = chr(10);
	break;
default:
	$gcell[1] = "<h3 class=fc>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/styles/style_blue.css\" type=\"text/css\">";
	$pagea.= "<style type=text/css>table { border-width: 1px; border-style: solid; border-color: #ababab; }    table td { border: 1px solid #ababab; } .b-w { border: 1px solid #fff; }</style>";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc style=\"text-align: center;\">".$cmpname."</h1><h2 class=fc style=\"text-align: center;\">";
	if(strlen($rhead)>0) {
		$pagea.=$rhead;
	} else {
		$pagea.= "Complaints Assist Report";
	}
	$pagea.= "</b></h2>";
	$table[1] = "<table cellpadding=3 cellspacing=0 width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	break;
}

//START PAGE
$echo = $pagea;
//GROUP LOOP
foreach($glist as $g) {
//	$groupresults = array();	//reset summary/group array
	$call2 = $calls[$g['id']];	//get calls for group
//print_r($call2);
	if(count($call2)>0) {	//if there are calls for the group
		if($g['id']!=0) {	//display the group title
			$echo.= $table[9].$gcell[1].$g['value'].$gcell[9].$table[1];
		} else {
			$echo.= $table[1];
		}
		//TABLE HEADINGS
		$echo.= $rowa;
		//$echo.= $cella[1]."Ref".$cellz[1];
		foreach($head as $hrow) {
			if($field[$hrow['field']] == "Y") { 
				$echo.= $cella[1].$helper->decode($hrow['display']).$cellz[1];
			}
		}
		$echo.=$rowz;
		
	
		//TABLE ROWS
		foreach($call2 as $call)
		{
			$cid = $call['callid'];
			$yn = "Y";
			$udf = $udfs[$cid];
			foreach($udfheadings as $u) {
				if($yn == "N") { break; }
				$uf = $u['field'];
				$filt = $filter[$uf];
				$filttype = $filtertype[$uf];
				$ut = $u['type'];
				switch($ut) {
					case "L":
					case "Y":
						if($yn == "Y") {
							if($filt[0] == "X") {
							} else {
								if(!in_array($udf[$uf]['udfvalue'],$filt)) { $yn = "N"; }
							}
						}
						//if(!in_array($udf[$uf]['udfvalue'],$filt)) { $yn = "N"; }
						$call[$uf]=$udf[$uf]['udfvalue'];
						break;
					case "D":
					default:
						if(strlen($filt)>0) {
							switch($filttype)
							{
								case "X":
									break;
								case "all":
									$ft = array_unique(explode(" ",$helper->code($filt)));
									$ct = count($ft);
									$ct2 = 0;
									foreach($ft as $f) {
										if(strpos($udf[$uf]['udfvalue'],$f)===true) { $ct2++; }
									}
									if($ct!=$ct2) { $yn = "N"; }
									break;
								case "any":
									$ft = array_unique(explode(" ",$helper->code($filt)));
									$ct2 = 0;
									foreach($ft as $f) {
										//echo "::".$f."::";
										//echo ":-".$udf[$uf]['udfvalue']."-".strripos($udf[$uf]['udfvalue'],$f)."-";
										//if(strripos($udf[$uf]['udfvalue'],$f)!=true) { echo "false"; } else { echo "true"; }
										//echo "-:";
										if(strripos($udf[$uf]['udfvalue'],$f)==true) { $ct2++; }
									}
									//echo $ct2."-------";
									if($ct2==0) { $yn = "N"; }
									break;
								case "exact":
								default:
									$ft = $helper->code($filt);
									if(strpos($udf[$uf]['udfvalue'],$ft)===false) { $yn = "N"; }
									break;
							}
						}
						$call[$uf]=$udf[$uf]['udfvalue'];
						break;
				}
			}	//foreach udfheading
			//echo "<P>".$yn;
			if($yn == "Y") {
				$echo.= $rowa;
				//$echo.= $cella[2].$cid.$cellz[2];
				$head2 = array_merge($headings,$logheadings);
				foreach($head as $hrow)
				{
					$hf = $hrow['field'];
					$ht = $hrow['type'];
					if($field[$hf] == "Y") { 
						$echo.= $cella[2];
						switch($ht) {
							case "Y":
							case "L":
								$echo.=$helper->decode($lists[$hf][$call[$hf]]['value']);
								break;
							case "D":
								if(($hf!="logdate" && $hf!="logdateact") || $call['logutype']!="N") {
									$echo.= ((strlen($call[$hf])>0 && $call[$hf]!=0) ? date("d M Y H:i:s",$call[$hf]) : "");
								}
								break;
							default:
								switch($hf) {
									case "callconaddress":
										$echo.= $helper->decode($call[$hf."1"])." ";
										for($c=2;$c<4;$c++) {
											if(strlen($call[$hf.$c])>0) { $echo.=$newline.$helper->decode($call[$hf.$c]); }
										}
										break;
									case "duration":
										$duration = "";
										if(strlen($call['logdateact'])>0 && $call['calldate']<$call['logdateact']) {
											$d = $call['logdateact'] - $call['calldate'] - 7200;
											if(floor($d/60/60/24)>1) {
												$duration = floor($d/60/60/24)." days ".date("H:i:s",$d);
											} elseif(floor($d/60/60/24)>0) {
												$duration = floor($d/60/60/24)." day ".date("H:i:s",$d);
											} else {
												$duration = date("H:i:s",$d);
											}
										}
										$echo.=$duration;
										break;
									default:
										if($hf == "logadmin") { $hf.="text"; }
										if($hf!="logadmintext") {
											$echo.= strFn("str_replace",$helper->decode($call[$hf]),chr(10),$newline);
										} else {
												switch($field['logadmin_type']) {
												case "ALL":
													$sql = "SELECT l.*, CONCAT_WS(' ',tk.tkname, tk.tksurname) as tkn 
															FROM ".$dbref."_log l
															INNER JOIN assist_".$cmpcode."_timekeep tk
																ON tk.tkid = l.logtkid
															WHERE logutype NOT IN ('N','T','E') AND logcallid = ".$cid." 
															ORDER BY logid DESC";
													//$log_rs = getRS($sql);
                                                    $log_rows = $helper->mysql_fetch_all($sql);
													$log_data = array();
													//while($log_row = mysql_fetch_assoc($log_rs)) {
                                                foreach ($log_rows as $log_row){
														$lv = $log_row['logadmintext'];
														$lv = $helper->decode($lv);
														$lv = str_replace("<br>",chr(10),$lv);
														$lv = strip_tags($lv);
														$lv = str_replace(chr(10).chr(10),chr(10),$lv);
														$lv = str_replace(chr(10),$newline,$lv);
														if(strpos($lv,"Complaint Attachment(s):")!==false) {
															$lv = substr($lv,0,strpos($lv,"Complaint Attachment(s):")-1);
														}
														$l = $log_row['tkn'].": ".$lv." [".date("d M Y H:i:s",$log_row['logdateact'])."]";
														$log_data[] = $l;
													}
													$echo.= implode($newline,$log_data);
													break;
												default:
													if($call['logutype']!="N") {
														$echo.= strip_tags(strFn("str_replace",$helper->decode($call[$hf]),chr(10),$newline));
													} else {
														$echo.= "";
													}
													break;
												}
										}
										break;
								}
						}
						$echo.= $cellz[2];
					}
				}	//foreach heading and logheading
				$echo.=$rowz;
			}	//yn = y
		}	//foreach line item
		//if summary = y then display for group
		/*if($krsum == "Y" && $field['results'] == "Y" && $g['id']!=0) {
			$echo.= drawKRSum('title0',$g['value'],0,0);
			foreach($krarray as $kra) {
				if($kra['filter']=="Y") { 
					$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$groupresults[$kra['sort']],$kra['sort']);
				}
			}
			$echo.= drawKRSum('sum','Total KPIs',array_sum($groupresults),0);
		}*/
	} //end if group(kpis)>0
} //end foreach group

//if summary = y then display - on hold
/*if($krsum == "Y" && $field['results'] == "Y") {
	$echo.= drawKRSum('title','',0,0);
	foreach($krarray as $kra) {
			if($kra['filter']=="Y") { 
				$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$totals[0][$kra['sort']],$kra['sort']);
			}
	}
	$echo.= drawKRSum('sum','Total KPIs',array_sum($totals[0]),0);
}*/
	
//END PAGE
$echo.= $table[9].$pagez;

//DISPLAY OUTPUT
switch($output)
{
	case "csv":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = $report_save_path;
        $helper->checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/caps_".date("Ymd_Hi",$today).".csv";
        $newfilename = "complaints_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'text/plain';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("report_dl.php");
		break;
	case "excel":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = $report_save_path;
        $helper->checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/caps_".date("Ymd_Hi",$today).".xml";
        $newfilename = "complaints_report_".date("Ymd_Hi",$today).".xls";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'application/ms-excel';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("report_dl.php");
		break;
	default:
		echo $echo;
		break;
}
//echo $echo;
?>