<?php

class CAPS1B extends ASSIST_MODULE_HELPER {


	private $headings = array(
		'head'=>array(
			'callref'=>"Reference",
			'calldate'=>"Date Logged",
			'calltkid'=>"Logged By",
			'callconname' =>"Resident's Name",
			'callcontel'=>"Resident's Telephone",
			'callconcell'=>"Resident's Cellphone",
			'callconaddress'=>"Resident's Address",
			'callareaid'=>"Area/Ward/Town (List)",
			'callarea'=>"Area/Ward/Town (Text)",
			'calldeptid'=>"Department",
			'callrespid'=>"Assigned To",
			'callurgencyid'=>"Urgency",
			'calltypeid'=>"Electricity - Type of Complaint",
			'callmessage'=>"Complaint",
			'callradio'=>"Radio",
			'callvehicle'=>"Vehicle",
			'callstatusid'=>"Status",
			'callattachment'=>"Attachment",
		),
		'types'=>array(
			'callref'=>"REF",
			'calldate'=>"DATETIME",
			'calltkid'=>"LIST",
			'callconname'=>"SMLTEXT",
			'callcontel'=>"SMLTEXT",
			'callconcell'=>"SMLTEXT",
			'callradio'=>"SMLTEXT",
			'callvehicle'=>"SMLTEXT",
			'callconaddress'=>"TEXT",
			'callrespid'=>"LIST",
			'callurgencyid'=>"LIST",
			'callarea'=>"SMLTEXT",
			'callareaid'=>"LIST",
			'calldeptid'=>"LIST",
			'calltypeid'=>"LIST",
			'callmessage'=>"TEXT",
			'callstatusid'=>"LIST",
			'callattachment'=>"ATTACH",
		),
	);


	const REFTAG = "";

	public function __construct() {
		parent::__construct();
		//strip udf functionality for speed
/*
		//get UDF headings first
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".strtoupper($this->getModRef())."'";
		//while($row = mysql_fetch_assoc($rs)) {
			//echo $sql;
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $row) {
			$this->headings['head'][$row['udfiid']] = $row['udfivalue'];
			switch($row['udfilist']) {
				case "Y":
					$x = "UDFLIST";
					break;
				case "N":
					$x = "UDFNUM";
					break;
				case "D":
					$x = "UDFDATE";
					break;
				case "T":
				case "M":
				default:
					$x = "UDFTEXT";
					break;
			}
			$this->headings['types'][$row['udfiid']] = $x;
		}
 * */
//ASSIST_HELPER::arrPrint($this->headings);
	}


	//HEADINGS
	function getListHeadings() {
		$data = array();
		foreach($this->headings['list_view'] as $h) {
			$data[$h] = $this->headings['head'][$h];
		}
		return $data;
	}
	function getAllHeadings() {
		return $this->headings['head'];
	}
	function getHeadingTypes() {
		return $this->headings['types'];
	}


	
	public function getUDFData($key) {
		//$sql = "SELECT udfvid as id, udfvvalue as name FROM assist_".$this->getCmpCode()."_udfvalue WHERE udfvindex = ".$key." AND udfvyn = 'Y' ORDER BY udfvvalue";
		//return $this->mysql_fetch_all_by_id($sql, "id");
		return array();
	}




	function getUsedAddUsers() {
		$data = array();
		$sql = "SELECT DISTINCT calltkid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "calltkid");
		if(count($used)>0) {
			$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
					FROM assist_".$this->cc."_timekeep t
					WHERE tkid IN ('".implode("','",$used)."')
					ORDER BY t.tkname, t.tksurname";
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		}
		return $data;
	}

	/*function getUsedResponsiblePersons() {
		$data = array();
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT(t.tkname,' ',t.tksurname) as name
				FROM assist_".$this->cc."_timekeep t
				INNER JOIN ".$this->getDBRef()."_call r
				ON r.calltkid = t.tkid AND r.callstatusid <> 5
				ORDER BY t.tkname, t.tksurname";
		$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		return $data;
	}*/

	function getUsedPortfolios() {
		$data = array();
		/*$sql = "SELECT DISTINCT callrespid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "callrespid");
		if(count($used)>0) {*/
			$sql = "SELECT DISTINCT p.id, CONCAT(p.value,' (',IF(tk.tkname IS NULL,'Unspecified User',CONCAT(tk.tkname,' ',tk.tksurname)),')') as name 
					FROM `".$this->getDBRef()."_list_respondant` p 
					LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep tk 
					ON tk.tkid = p.admin
					ORDER BY p.value, tk.tkname, tk.tksurname";
					 //WHERE p.id IN ('".implode("','",$used)."')
			/*$sql = "SELECT DISTINCT p.id as id, CONCAT(d.value,' - ',p.value) as name
					FROM ".$this->getDBRef()."_list_portfolio p
					INNER JOIN ".$this->getDepartmentDBRef()." d
					ON p.deptid = d.id
					INNER JOIN ".$this->getDBRef()."_res r 
					ON r.resportfolioid = p.id AND r.resstatusid <> 'CN'
					ORDER BY ".$this->getDepartmentSortField("d").", p.value";*/
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
			//$data = array_merge(array(0=>$this->getUnspecified()),$data);
		//}
		return $data;
	}	

	function getUsedUrgency() {
		$data = array();
		/*$sql = "SELECT DISTINCT callurgencyid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "callurgencyid");
		if(count($used) > 0) {*/
			$sql = "SELECT DISTINCT u.id as id, u.value as name 
					FROM ".$this->getDBRef()."_list_urgency u 
					ORDER BY sort, value";
					//WHERE id IN (".implode(",",$used).") 
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		//}
		//$data = array_merge(array(0=>$this->getUnspecified()),$data);
		return $data;
	}

	function getUsedArea() {
		$data = array();
		/*$sql = "SELECT DISTINCT callareaid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "callareaid");
		if(count($used) > 0) {*/
				$sql = "SELECT DISTINCT u.id as id, u.value as name 
					FROM ".$this->getDBRef()."_list_area u 
					ORDER BY sort, value";
					//WHERE id IN (".implode(",",$used).") 
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		//}
		//$data = (array(0=>$this->getUnspecified()))+$data;
		return $data;
	}	

	function getUsedDept() {
		$data = array();
		/*$sql = "SELECT DISTINCT calldeptid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "calldeptid");
		if(count($used) > 0) {*/
				$sql = "SELECT DISTINCT u.id as id, u.value as name 
					FROM ".$this->getDBRef()."_list_dept u 
					 ORDER BY sort, value";
					//WHERE  id IN (".implode(",",$used).")
				$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		//}
		//$data = array_merge(array(0=>$this->getUnspecified()),$data);
		return $data;
	}	
	
	function getUsedType() {
		$data = array();
		/*$sql = "SELECT DISTINCT calltypeid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "calltypeid");
		if(count($used) > 0) {*/
			$sql = "SELECT DISTINCT u.id as id, u.value as name 
					FROM ".$this->getDBRef()."_list_electype u 
					 ORDER BY sort, value";
					//WHERE  id IN (".implode(",",$used).")
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		/*} else {
			$data = array();
		}*/
				//$data = array_merge(array(0=>$this->getUnspecified()),$data);
		return $data;
	}	
		
			
	function getUsedStatus() {
		$data = array();
		/*$sql = "SELECT DISTINCT callstatusid FROM ".$this->getDBRef()."_call WHERE callstatusid <> 5";
		$used = $this->mysql_fetch_all_by_value($sql, "callstatusid");
		if(count($used) > 0) {*/
			$sql = "SELECT s.id as id, s.value as name 
					FROM ".$this->getDBRef()."_list_status s 
					WHERE s.id <> 5 
					ORDER BY sort";
					//AND id IN (".implode(",",$used).") 
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		/*} else {
			$data = array();
		}*/
		return $data;
	}







	public function __destruct() {
		parent::__destruct();
	}

}



?>