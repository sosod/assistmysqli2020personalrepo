<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
include("inc_ignite.php"); 
include("inc.php");

$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'CAPS' AND refid = 1";
//include("inc_db_con.php");
if($helper->db_get_num_rows($sql)>0) {
	$row = $helper->mysql_fetch_one($sql);
	$calltkid_close = $row['value'];
	if(strlen($calltkid_close)==0 || ($calltkid_close != "Y" && $calltkid_close != "N")) { $calltkid_close = "N"; }
} else {
	$calltkid_close = "N";
}
//mysql_close($con);
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link href="../lib/jquery/css/blue_orange/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type ="text/javascript" src="../lib/jquery/js/jquery.min.js"></script>
<script type ="text/javascript" src="../lib/jquery/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/default.css" type="text/css">
<link rel="stylesheet" href="/styles/style_blue.css" type="text/css">
<script type=text/javascript>
function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+"s="+s+"&f="+f;
}
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
</script>
<style type=text/css>
body{
  font-size: 62.5%;
}
h1 {
	text-decoration: none;
	font-size: 14pt;
	line-height: 16pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
h2 {
	text-decoration: none;
	font-size: 12pt;
	line-height: 14pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
h3 {
	text-decoration: none;
	font-size: 10pt;
	line-height: 12pt;
	font-weight: bold;
	padding: 0 0 0 0;
	margin: 15 0 10 5;
}
.noborder {
	border: 0px solid #FFFFFF;
}
.minw {
	min-width: 700px;
}
.left {
	text-align: left;
}
.right {
	text-align: right;
}
.blank { background-color: #ffffff; }
.tdhover { background-color: #e1e1e1; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>