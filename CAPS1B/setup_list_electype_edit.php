<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';
//    include("inc_ignite.php");
 //   include("inc_hp.php");

    
$title = "Electricity - Type of Complaint";
$table = "electype";
$page = "setup_list_electype.php";
$edit_page = "setup_list_electype_edit.php";
$result = array();
$field = "calltypeid";
        
    $id = $_GET['u'];
    if(strlen($id) > 0)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_".$table." WHERE id = ".$id." ORDER BY sort, value DESC";
        //---echo($sql);
        //include("inc_db_con.php");
        $row = $helper->mysql_fetch_one($sql);
        //mysql_close();

        $sql = "SELECT count(callid) as c FROM assist_".$cmpcode."_caps_call WHERE ".$field." = ".$id." ";
        //---echo($sql);
        //include("inc_db_con.php");
        $crow = $helper->mysql_fetch_one($sql);
        //mysql_close();
        $c = $crow['c'];
    }
    else
    {
        $id = $_POST["id"];
        $val = ASSIST_HELPER::code($_POST["val"]);
        $sort = $_POST["sort"];
        $subbut = $_POST["subbut"];
    	$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_".$table." WHERE id = ".$id." ORDER BY sort, value DESC";
		$orow = $mydb->mysql_fetch_one($sql);
		$told = serialize($orow);
        if($subbut == "Del")
        {
            $sql = "UPDATE assist_".$cmpcode."_caps_list_".$table." SET yn = 'N' WHERE id = ".$id;
//            echo($sql);
            //include("inc_db_con.php");
            $helper->db_update($sql);
			//Set transaction log values for this update
			$trans = "Deleted CAPS ".$title.": ".$id;
			//PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
			$tsql = $sql;
			//$told = "Y";
			$tref = 'CAPS';
			//include("inc_transaction_log.php");
        }
        else
        {
            $sql = "UPDATE assist_".$cmpcode."_caps_list_".$table." SET value = '".$val."', sort = ".$sort." WHERE id = ".$id;
//            echo($sql);
            //include("inc_db_con.php");
            $helper->db_update($sql);
			//Set transaction log values for this update
			$trans = "Edited CAPS ".$title.": ".$id;
			//PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
			$tsql = $sql;
			//$told = "Y";
			$tref = 'CAPS';
			//include("inc_transaction_log.php");
        }
        echo "<script type=text/javascript>
        document.location.href = '".$page."?r[]=ok&r[]=Changes+saved+successfully.';
        </script>";
    }
?>
<script language=JavaScript>
function Validate() {
    if(confirm("Editing this record will affect all calls associated with it.\n\nAre you sure you wish to continue?"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function butClick(subbut) {
    document.edit.subbut.value = subbut;
}

</script>
<h1 style="margin-bottom: 20px" class=fc><b>Setup >> <?php echo $title; ?> >> Edit</b></h1>
<table>
	<tr>
		<th>ID</th>
		<th>Value</th>
	</tr>
<form name=edit method=post action=<?php echo $edit_page; ?> onsubmit="return Validate()" language=jscript>
	<input type=hidden name=sort value=99 />
	<tr>
		<td class=center><?php echo $row['id'];?><input type=hidden name=id value=<?php echo $row['id'];?>></td>
		<td class="tdgeneral"><input type=text name=val value="<?php echo ASSIST_HELPER::decode($row['value']); ?>"></td>
	</tr>
	<tr>
		<td class="tdgeneral" colspan=3><input type=hidden value="" name=subbut><input type=submit value="Save changes" class=isubmit name=but onclick="butClick('Edit')"> 
			<?php if($c == 0) {?><span class="float "><input type=submit value=Delete class=idelete name=but onclick="butClick('Del')"></span><?php } else { ?><span class=i>Calls affected: <?php echo($c); ?></span><?php } ?>
		</td>
	</tr>
</form>
</table>
<?php
    include("inc_back.php");
?>
<script type=text/javascript>
$(function() {
	$("th").addClass("center");
});
</script>
</body>

</html>
