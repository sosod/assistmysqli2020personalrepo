<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
//include("inc_ignite.php");
require_once('inc_head.php');

$result = array();

if(isset($_REQUEST['action']) && isset($_REQUEST['value'])) {
	switch($_REQUEST['action']) {
	case "CLOSE":
        //UPDATE ASSIST-SETUP TABLE
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$_REQUEST['value']."' WHERE ref = 'CAPS' AND refid = 1";
        $mnr = $helper->db_update($sql);
		if($mnr>0) {
			//Set transaction log values for this update
			$trans = "Updated CAPS owner closing YN to ".$_REQUEST['value']." from ".$capsact;
			//PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
			$tsql = $sql;
			$told = "UPDATE assist_".$cmpcode."_setup SET value = '".$capsact."' WHERE ref = 'CAPS' AND refid = 1";
			$tref = 'CAPS';
			//include("inc_transaction_log.php");
			$capsact = $_REQUEST['value'];
			$result = array("ok","Updated Closing Complaints to ".($_REQUEST['value']=="Y" ? "Yes" : "No").".");
		} else {
			$result = array("info","No change was found.");
		}
		break;
	case "ADMIN":
        //UPDATE ASSIST-SETUP TABLE
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$_REQUEST['value']."' WHERE ref = 'CAPS' AND refid = 0";
        $mnr = $helper->db_update($sql);
		if($mnr>0) {
			$sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$_REQUEST['value']."'";
			$row = $helper->mysql_fetch_one($sql);
			$new = $row['tkname']." ".$row['tksurname'];
			$sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$setupadmn."'";
			$row = $helper->mysql_fetch_one($sql);
			$old = $row['tkname']." ".$row['tksurname'];
			//Set transaction log values for this update
			$trans = "Updated Setup Administrator to ".$new." from ".$old;
			//PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
			$tsql = $sql;
			$told = "UPDATE assist_".$cmpcode."_setup SET value = '".$setupadmn."' WHERE ref = 'CAPS' AND refid = 0";
			$tref = 'CAPS';
			//include("inc_transaction_log.php");
			$capsact = $_REQUEST['value'];
			$result = array("ok","Updated Setup Administrator to ".$new.".");
			$setupadmn = $_REQUEST['value'];
		} else {
			$result = array("info","No change was found.");
		}
		break;
	}
}
?>
<h1>Setup</h1>
<?php $helper->displayResult($result); ?>
<h3>Defaults</h3>
<table>
	<tr>
		<th>Setup Administrator</th>
		<td>Who can access the Setup function?<span class=float style="margin-left: 20px;"><select id=admin_user><?php 
				if ($setupadmn == "0000") {
					$sel = "selected";
				} else {
					$sel = "";
				}
				echo "<option $sel value=0000>Ignite Assist Administrator</option>";
				$sql = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep t
						INNER JOIN assist_".$cmpcode."_menu_modules_users m 
							ON t.tkid = m.usrtkid AND m.usrmodref = 'CAPS'
						WHERE t.tkstatus = 1 AND t.tkid <> '0000' 
						ORDER BY t.tkname, t.tksurname";
				//$rs = getRS($sql);
                    $rows = $helper->mysql_fetch_all($sql);
				//while ($row = mysql_fetch_array($rs)) {
				foreach ($rows as $row){
					$tid = $row['tkid'];
					if ($tid == $setupadmn) {    
						$sel2 = "selected";
					} else {
						$sel2 = "";
					}
					echo "<option $sel2 value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>";
				}
				//mysql_close();
		?></select> <input type=button value=Save class=isubmit id=setup_admin /></span></td>
	</tr>
	<tr>
		<th>Closing Complaints</th>
		<td>Can the person who logged the complaint, close it?<span class=float style="margin-left: 20px;"><select id=close_yn><?php if($capsact!="Y") { echo "<option selected value=N>No</option><option value=Y>Yes</option>"; } else { echo "<option value=N>No</option><option selected value=Y>Yes</option>"; } ?></select> <input type=button value=Save class=isubmit id=close_call /></span></td>
	</tr>
	<tr>
		<th>Status</th>
		<td>Configure the list of statuses available. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_status /></span></td>
	</tr>
	<tr>
		<th>Respondants</th>
		<td>Configure the list of people who can respond to complaints. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_topics /></span></td>
	</tr>
<!--	<tr>
		<th>User Defined Fields</th>
		<td>Configure the user defined fields.<span class=float style="margin-left: 20px;">&nbsp;<input type=button value=Configure id=setup_udf /></span></td>
	</tr> -->
	<tr>
		<th>Data Usage Report</th>
		<td>View the data storage in use in this module.<span class=float style="margin-left: 20px;"><input type=button value=View id=setup_datareport /></span></td>
	</tr>
</table>
<h3>Lists</h3>
<table>
	<tr>
		<th>Status</th>
		<td>Configure the list of statuses available. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_status /></span></td>
	</tr>
	<tr>
		<th>Area/Ward/Town</th>
		<td>Configure the list of areas available. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_list_area class="click" /></span></td>
	</tr>
	<tr>
		<th>Department</th>
		<td>Configure the list of departments available. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_list_dept class="click" /></span></td>
	</tr>
	<tr>
		<th>Electricity - Type of Complaint</th>
		<td>Configure the list of types available. <span class=float style="margin-left: 20px;"><input type=button value=Configure id=setup_list_electype class="click" /></span></td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	$("td").addClass("middle");
	$(".click, #setup_status, #setup_topics, #setup_udf, #setup_datareport").click(function() {
		document.location.href = $(this).attr("id")+".php";
	});
	/*$("#setup_udf").click(function() {
		var action = $("#udf_action").val();
		document.location.href = 'setup_udf_'+action+'.php';
	});*/
	$("#close_call").click(function() {
		var act = $("#close_yn").val();
		document.location.href = 'setup.php?action=CLOSE&value='+act;
	});
	$("#setup_admin").click(function() {
		var act = $("#admin_user").val();
		document.location.href = 'setup.php?action=ADMIN&value='+act;
	});
});
</script>
                </body>

                </html>
