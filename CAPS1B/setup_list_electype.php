<?php
require_once 'inc_head.php';

$title = "Electricity - Type of Complaint";
$table = "electype";
$page = "setup_list_electype.php";
$edit_page = "setup_list_electype_edit.php";
$result = array();

//    include("inc_ignite.php");
//    include("inc_hp.php");

$val = ASSIST_HELPER::code(isset($_POST['val']) ? $_POST['val'] : "");
$sort = isset($_POST['sort']) ? $_POST['sort'] :0;
    
    if(strlen($val) > 0)
    {
        //CREATE SORT INT IF BLANK OR NOT NUM
        if(is_nan($sort) || $sort < 1)
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_".$table." WHERE yn = 'Y' ORDER BY sort DESC";
            //---echo($sql);
            //include("inc_db_con.php");
            $row = $helper->mysql_fetch_one($sql);
            $old = $row['sort'];
            $sort = $old + 1;
        }
        //GET ID NUMBER
/*        $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_".$table." ORDER BY id DESC";
            //---echo($sql);
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $idold = $row['id'];
        $id = $idold + 1;*/
        //ADD RECORD
        $sql = "INSERT INTO assist_".$cmpcode."_caps_list_".$table." SET value = '".$val."', sort = ".$sort.", yn = 'Y', description = '' ,status ='0' ";
            //---echo($sql);
        //include("inc_db_con.php");
        $helper->db_insert($sql);
        $result = array("ok","New ".$title." created successfully.");
			//Set transaction log values for this update
			$trans = "New CAPS ".$title.": ".$val;
			//PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
			$tsql = $sql;
			$told = "";
			$tref = 'CAPS';
			//include("inc_transaction_log.php");
		
    } else {
    	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
    }
?>
<h1 >Setup >> <?php echo $title; ?></b></h1>
<?php ASSIST_HELPER::displayResult($result); ?>
<table>
	<tr>
		<th>ID</th>
		<th>Value</th>
		<th>&nbsp;</th>
	</tr>
<form name=add method=post action=<?php echo $page; ?>>
	<input type=hidden name=sort value=99 />
	<tr>
		<td class=center>#</td>
		<td><input type="text" name="val" size="40" maxlength=90></td>
		<td><input type="submit" value="Add" name="B3" class=isubmit /></td>
	</tr>
</form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_".$table." WHERE yn = 'Y' ORDER BY sort, value ASC";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
$n = $helper->db_get_num_rows($sql);
if($n > 0)
{
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
    ?>
	<tr>
		<td class=center><?php echo $row['id'];?></td>
		<td ><?php echo $row['value'];?></td>
		<td ><?php if($row['id'] > 5) {?><input type=button value=Edit name=B4 onclick="Edit(<?php echo($row['id'])?>)"><?php }else{echo("&nbsp;");}?></td>
	</tr>
    <?php
    }
}
else
{
    echo("no rows");
}
//mysql_close();
?>
</table>
<?php $helper->displayGoBack("setup.php","Go Back"); ?>
<script type=text/javascript>
function Edit(id) {
    document.location.href = "<?php echo $edit_page; ?>?u="+id;
}
$(function() {
	$("th").addClass("center");
});
</script>

</body>

</html>
