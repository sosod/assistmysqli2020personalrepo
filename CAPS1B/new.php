<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';

?>
<h1>New Complaint</h1>
<?php
$helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
        <form name=logcall action=new_process.php method=POST onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
            <table>
                <tr>
                    <th>Your name:</th>
                    <td ><?php echo($tkname); ?><input type=hidden name=calltkid value=<?php echo($tkid);?>></td>
                </tr>
                <tr>
                    <th>Resident's Name:</th>
                    <td ><input type=text name=callconname size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Resident's Telephone:</th>
                    <td ><input type=text name=callcontel size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Resident's Cellphone:</th>
                    <td ><input type=text name=callconcell size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th rowspan=3>Resident's Address:</th>
                    <td><input type=text name=callconaddress1 size=40 maxlength=90></td>
                </tr>
                <tr>
                    <td><input type=text name=callconaddress2 size=40 maxlength=90></td>
                </tr>
                <tr>
                    <td><input type=text name=callconaddress3 size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Area/Ward/Town:</th>
                    <td><select name="callareaid"><option selected value=0>Unspecified</option>
                    	<?php
                    	$sql = "SELECT * FROM ".$mydb->getDBRef()."_list_area WHERE yn = 'Y' ORDER BY sort, value";
						$areas = $mydb->mysql_fetch_all($sql);
						foreach($areas as $a) {
							echo "<option value=".$a['id'].">".$a['value']."</option>";
						}
                    	?>
                    </select> or <input type=text name=callarea size=30 maxlength="90" />
                    	</td>
                </tr>
                <tr>
                    <th>Department:</th>
                    <td ><select name=calldeptid><option selected value=0>Unspecified</option>
                            <?php
                            $sql = "SELECT * FROM ".$mydb->getDBRef()."_list_dept WHERE yn = 'Y' ORDER BY sort, value";
                            $areas = $mydb->mysql_fetch_all($sql);
                            foreach($areas as $a) {
                                echo "<option value=".$a['id'].">".$a['value']."</option>";
                            }
                            ?>
                    </select>
                    	</td>
                </tr>
                <tr>
                    <th>Respondant:</th>
                    <td ><select name=callrespid><option selected value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * 
                            		FROM assist_".$cmpcode."_caps_list_respondant r
                            		INNER JOIN assist_".$cmpcode."_timekeep t
                            		ON  t.tkid = r.admin
                            		LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mmu
                            		ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '$modref' 
                            		WHERE r.yn = 'Y' ORDER BY value ASC";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while($row = mysql_fetch_array($rs)) {
                            foreach ($rows as $row){
                                $id = $row['id'];
								if($row['tkstatus']==1 && !is_null($row['usrmodref'])) {
									$disabled = "";
	                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
								} else {
									$disabled = "disabled=disabled";
	                                $val = $row['value']." (No active Admin)";
								}
                                echo("<option $disabled value=".$id.">".$val."</option>");
                            }
                            ?>
                            <?php
                            //Original Code removed on 9 Oct 2016 by JC - prevent assignment to departments without active admins
                            /*$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant r, assist_".$cmpcode."_timekeep t WHERE r.yn = 'Y' AND t.tkid = r.admin ORDER BY value ASC";
                            $rs = getRS($sql);
                            while($row = mysql_fetch_array($rs)) {
                                $id = $row['id'];
                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
                                echo("<option value=".$id.">".$val."</option>");
                            }*/
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <th>Urgency:</th>
                    <td ><select size="1" name="callurgencyid"><option value=0>Unspecified</option>
<?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE yn = 'Y' ORDER BY sort ASC";
                            //$rs = getRS($sql);
                            $rows = $helper->mysql_fetch_all($sql);
                            //while($row = mysql_fetch_array($rs)) {
                            foreach ($rows as $row){
                                $id = $row['id'];
                                $val = $row['value'];
                                echo("<option ".($id==2 ? "selected" : "")." value=".$id.">".$val."</option>");
                            }
                            ?>
                        </select></td>
            </tr>
                <tr>
                    <th>Electricity - Type of Complaint:</th>
                    <td ><select name=calltypeid><option selected value=0>Unspecified</option>
                    	<?php
                    	$sql = "SELECT * FROM ".$mydb->getDBRef()."_list_electype WHERE yn = 'Y' ORDER BY sort, value";
						$areas = $mydb->mysql_fetch_all($sql);
						foreach($areas as $a) {
							echo "<option value=".$a['id'].">".$a['value']."</option>";
						}
                    	?>
                    </select></td>
                </tr>
                <tr>
                    <th>Complaint:</th>
                    <td >
                        <textarea rows="6" name="callmessage" cols="40"></textarea></td>
                </tr>

                <tr>
                    <th>Radio Respondant:</th>
                    <td ><input type=text name=callradio size=40 maxlength=90></td>
                </tr>
                <tr>
                    <th>Vehicle Registration No.:</th>
                    <td ><input type=text name=callvehicle size=40 maxlength=90></td>
                </tr>

                <?php
//$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";

//echo $sql;
//$rs = getRS($sql);
//while($row = mysql_fetch_array($rs)) {
	$udfrows = array(); //remove udf functionality
	foreach($udfrows as $row) {
    ?>
                <tr>
                    <th><?php echo($row['udfivalue']); ?>:</th>
                    <td>
                    <?php
                    switch($row['udfilist']) {
                        case "Y":
                            echo("<select name=".$row['udfiid']."><option value=X>---SELECT---</option>");
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
            //include("inc_db_con2.php");
			//$rs2 = getRS($sql2);
                        $rows2 = $helper->mysql_fetch_all($sql2);
            //while($row2 = mysql_fetch_array($rs2)) {
            foreach ($rows2 as $row2){
                                        echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                    }
                                    echo("</select>");
                                    break;
                                case "T":
                                    echo("<input type=text name=".$row['udfiid']." size=50>");
                                    break;
                                case "M":
                                    echo("<textarea name=".$row['udfiid']." rows=5 cols=40></textarea>");
                                    break;
                                case "D":
                                    echo("<input class='date-type'  type='text' name='".$row['udfiid']."' size='15' readonly='readonly'/>");
                                    break;
                                case "N":
                                     echo("<input type='text' name='".$row['udfiid']."' size='15'>&nbsp;<small><i>Only numeric values are allowed.</i></small>");
                                    break;
                                default:
                                    echo("<input type=text name=".$row['udfiid']." size='50'>");
                                    break;
                            }
                            ?>
                    </td>
                </tr>


                            <?php
                        }
                        //mysql_close();
                        ?>
                <tr >
                    <th>Attach document(s):</th>
                    <td><div id="firstdoc"><input type="file" name="attachments[]" id="attachment" size="30"/></div>
					<span class=float style="margin-top: 5px;"><a href="javascript:void(0)" id="attachlink">Attach another file</a></span></td>
                </tr>
                <tr>
					<th></th>
                    <td>
                        <input type="submit" value="Submit" name="B1" class=isubmit />
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
            </table>

        </form>
		<script type=text/javascript>
		$(document).ready(function(){
			$("th").addClass("left");
			$("th").addClass("top");
			$("tr").attr("height","30");
			
                $('#attachlink').click(function(){
                    $('<div style=\"margin-top: 5px;\"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdoc');
                });
        });

			function Validate(me) {
            var cname = me.callconname.value;
            var ctel = me.callcontel.value;
            var ctel = me.callcontel.value;
            var cadd1 = me.callconaddress1.value;
            var cadd2 = me.callconaddress2.value;
            var cadd3 = me.callconaddress3.value;
            var area = me.callarea.value;
            var resp = me.callrespid.value;
            var mess = me.callmessage.value;
            var urgid = me.callurgencyid.value;

            if(cname.length == 0)
            {
                alert("Please enter the resident's name.");
            }
            else
            {
                if(ctel.length == 0)
                {
                    if(confirm("You have not entered a contact number for the resident\n\nAre you sure you wish to continue?")==true)
                    {
                        me.callcontel.value = "N/A";
                        ctel = "N/A";
                    }
                }
                if(ctel.length > 0)
                {
                    if(cadd1.length==0 && cadd2.length == 0 && cadd3.length==0)
                    {
                        if(confirm("You have not entered address details for the resident\n\nAre you sure you wish to continue?")==true)
                        {
                            me.callconaddress1.value = "N/A";
                            cadd1 = "N/A";
                        }
                    }
                    if(cadd1.length > 0 || cadd2.length > 0 || cadd3.length > 0)
                    {
                        if(resp == "X")
                        {
                            alert("Please select the respondant.");
                        }
                        else
                        {
                            if(mess.length == 0)
                            {
                                alert("Please enter a message indicating the complaint.");
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    </script>
</body></html>
