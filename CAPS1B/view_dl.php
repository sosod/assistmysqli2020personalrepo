<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
include("inc_ignite.php");

$docid = $_GET['i'];

$sql = "SELECT * FROM ".$dbref."_attachments WHERE id = $docid ";
//include("inc_db_con.php");
    $doc = $helper->mysql_fetch_one($sql);
//mysql_close($con);

$loc2 = "/files/".$cmpcode."/CAPS/".$doc['system_filename'];
$loc = "..".$loc2;
$filename = $doc['original_filename'];

if(file_exists($loc) && is_readable($loc)) {
// get the file size and send the http headers
    $size = filesize($loc);
    header('Content-Type: application/octet-stream');
    header('Content-Length: '.$size);
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    header('Content-Transfer-Encoding: binary');
// open the file in binary read-only mode
// display the error messages if the file can�t be opened
    $file = @ fopen($loc, 'r');
    if ($file) {
        // stream the file and exit the script when complete
        fpassthru($file);
    } else {
        die("An error has occurred while trying to retrieve the document.  Please go back and try again.");
    }
    
} else {
    die("An error has occurred while trying to retrieve the document.  Please go back and try again.");
}
?>
