<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';
//    include("inc_ignite.php");
 //   include("inc_hp.php");

    $id = $_GET['u'];
    if(strlen($id) > 0)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE id = ".$id." ORDER BY sort DESC";
        //---echo($sql);
        //include("inc_db_con.php");
        $row = $helper->mysql_fetch_one($sql);
        //mysql_close();

        $sql = "SELECT count(callid) as c FROM assist_".$cmpcode."_caps_call WHERE callstatusid = ".$id." ";
        //---echo($sql);
        //include("inc_db_con.php");
        $crow = $helper->mysql_fetch_one($sql);
        //mysql_close();
        $c = $crow['c'];
    }
    else
    {
        $id = $_POST["id"];
        $val = $_POST["val"];
        $sort = $_POST["sort"];
        $subbut = $_POST["subbut"];
        if($subbut == "Del")
        {
            $sql = "UPDATE assist_".$cmpcode."_caps_list_status SET yn = 'N' WHERE id = ".$id;
//            echo($sql);
            //include("inc_db_con.php");
            $helper->db_update($sql);
        }
        else
        {
            $sql = "UPDATE assist_".$cmpcode."_caps_list_status SET value = '".$val."', sort = ".$sort." WHERE id = ".$id;
//            echo($sql);
            //include("inc_db_con.php");
            $helper->db_update($sql);
        }
        header('Location: setup_status.php');
    }
?>
<script language=JavaScript>
function Validate() {
    if(confirm("Editing this record will affect all calls associated with it.\n\nAre you sure you wish to continue?"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function butClick(subbut) {
    document.edit.subbut.value = subbut;
}

</script>
<h1 style="margin-bottom: 20px" class=fc><b>Setup >> Status >> Edit</b></h1>
<p>Please note that you may only delete a status once there are no calls associated with that status.</p>
<table>
	<tr>
		<th>ID</th>
		<th>Value</th>
		<th>Display Order</th>
	</tr>
<form name=edit method=post action=setup_status_edit.php onsubmit="return Validate()" language=jscript>
	<tr>
		<td class=center><?php echo $row['id'];?><input type=hidden name=id value=<?php echo $row['id'];?>></td>
		<td class="tdgeneral"><input type=text name=val value="<?php echo $row['value'];?>"></td>
		<td class="tdgeneral"><input type=text name=sort value=<?php echo $row['sort'];?>></td>
	</tr>
	<tr>
		<td class="tdgeneral" colspan=3><input type=hidden value="" name=subbut><input type=submit value="Save changes" class=isubmit name=but onclick="butClick('Edit')"> 
			<?php if($c == 0) {?><span class="float "><input type=submit value=Delete class=idelete name=but onclick="butClick('Del')"></span><?php } else { ?><span class=i>Calls affected: <?php echo($c); ?></span><?php } ?>
		</td>
	</tr>
</form>
</table>
<?php
    include("inc_back.php");
?>
<script type=text/javascript>
$(function() {
	$("th").addClass("center");
});
</script>
</body>

</html>
