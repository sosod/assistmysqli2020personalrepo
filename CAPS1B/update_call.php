<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
$jquery_date = true;
require_once 'inc_head.php';

$urlback = isset($_REQUEST['b']) ? $_REQUEST['b'] : "update.php?sec=my&st=ip";

$req = "update_call.php?i=".$_REQUEST['i']."&b=".$_REQUEST['b'];
$logsort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array("logdateact","DESC");

$src = explode("/",$_SERVER['HTTP_REFERER']);
$src = $src[count($src)-1];

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
?>

        <h1>Update Complaint</h1>
        <?php
		
        $callid = $_REQUEST['i'];
        $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
        /*include("inc_db_con.php");
        $row = mysql_fetch_array($rs);*/
		$row = $helper->mysql_fetch_one($sql);
        $callref = $row['callref'];
        $callurgencyid = $row['callurgencyid'];
        $callstatusid = $row['callstatusid'];
        $callrespid = $row['callrespid'];
        $callmessage = $row['callmessage'];
        $calltkid = $row['calltkid'];
        $callarea = $row['callarea'];
        $calldate = $row['calldate'];
        $callconname = $row['callconname'];
        $callcontel = $row['callcontel'];
        $callconcell = $row['callconcell'];
        $callconaddress1 = $row['callconaddress1'];
        $callconaddress2 = $row['callconaddress2'];
        $callconaddress3 = $row['callconaddress3'];
        $callradio = $row['callradio'];
        $callvehicle = isset($row['callvehicle']) ? $row['callvehicle'] : 0 ;
        $callareaid = isset($row['callareaid']) ? $row['callareaid'] : 0;
        $calldeptid = isset($row['calldeptid']) ? $row['calldeptid'] : 0;
        $calltypeid = isset($row['calltypeid']) ? $row['calltypeid'] : 0;
        //mysql_close();
        ?>
        <h2>Current status</h2>
        <table>
            <tr>
                <th  width=150>Reference:</th>
                <td  >
                    <?php
                    echo($callref);
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Date Logged:</th>
                <td >
                    <?php
                    echo(date("d-M-Y H:i", $calldate));
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Logged by:</th>
                <td >
                    <?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$calltkid."'";
                    //include("inc_db_con.php");
                    $row = $helper->mysql_fetch_one($sql);
                    $loggedBy = $row['tkname']." ".$row['tksurname'];
                    echo($row['tkname']." ".$row['tksurname']);
                    //mysql_close();
                    ?>&nbsp;</td>
            </tr>
            <tr>
                <th>Resident's Name:</th>
                <td >
                    <?php
                    echo($callconname."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Telephone:</th>
                <td >
                    <?php
                    echo($callcontel."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Cellphone:</th>
                <td >
                    <?php
                    echo($callconcell."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Resident's Address:</th>
                <td >
                    <?php
                    if (strlen($callconaddress1) > 0)
                    {
                        echo($callconaddress1);
                    }
                    if (strlen($callconaddress1) > 0 && strlen($callconaddress2) > 0)
                    {
                        echo("<br>");
                    }
                    if (strlen($callconaddress2) > 0)
                    {
                        echo($callconaddress2);
                    }
                    if ((strlen($callconaddress1) > 0 || strlen($callconaddress2) > 0) && strlen($callconaddress3) > 0)
                    {
                        echo("<br>");
                    }
                    if (strlen($callconaddress3) > 0)
                    {
                        echo($callconaddress3);
                    }
                    echo("&nbsp;");
                    ?></td>
                    </tr>
                    <tr>
                        <th>Area/Ward/Town:</th>
                        <td >
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_area WHERE id = ".$callareaid;
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['value']);
                                //mysql_close();
                                ?>&nbsp;</td>
                    </tr>
                        <tr>
                            <th>Department:</th>
                            <td >
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_dept WHERE id = ".$calldeptid;
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['value']);
                                //mysql_close();
                                ?>&nbsp;</td>
                        </tr>
                        <?php
                        $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant WHERE id = ".$callrespid;
                        //include("inc_db_con.php");
                       // $resp = mysql_fetch_array($rs);
                        //mysql_close();
                        $resp = $helper->mysql_fetch_one($sql);
						
                        ?>
            <tr>
                <th>Dept/Division (Respondant):</th>
                <td >
                	<span class=float style='margin-left: 15px;margin-top: 10px'>
                        <form name=transfer method=post action=update_call_transfer.php>
                        <input type=hidden name=callid value=<?php echo($callid); ?>>
                	
                            <?php
                            if ($callstatusid != 4)
                            {
                            /* CODE REMOVED ON 09 OCTOBER 2016 BY JC - prevent selection of respondants with no active admins assigned   
							 * $sql = "SELECT * 
                                		FROM assist_".$cmpcode."_caps_list_respondant r
                                		INNER JOIN assist_".$cmpcode."_timekeep t
                                		ON t.tkid = r.admin 
                                		WHERE r.yn = 'Y' 
                                		ORDER BY value ASC";
                                //include("inc_db_con.php");*/
                                
                            $sql = "SELECT * 
                            		FROM assist_".$cmpcode."_caps_list_respondant r
                            		INNER JOIN assist_".$cmpcode."_timekeep t
                            		ON  t.tkid = r.admin
                            		LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mmu
                            		ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '$modref' 
                            		WHERE r.yn = 'Y' ORDER BY value ASC";
                                
                                $list = $helper->mysql_fetch_all($sql);
                                if (count($list) > 1) {
                            ?><select name=callrespid><option selected value=X>--- SELECT ---</option><?php
                                    foreach($list as $row)
                                    {
/*                                       CODE REMOVED ON 09 OCTOBER 2016 BY JC - prevent selection of respondants with no active admins assigned 
 * 										$id = $row['id'];
                                        $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
                                        echo("<option");
                                        if ($id == $callrespid)
                                        {
                                            echo(" selected");
                                        }
                                        echo(" value=".$id.">".$val."</option>");*/
                                $id = $row['id'];
								$selected = "";
								if($row['tkstatus']==1 && !is_null($row['usrmodref'])) {
									$disabled = "";
	                                $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
									if ($id == $callrespid) {
										$selected = "selected";
									}
								} else {
									$disabled = "disabled=disabled";
	                                $val = $row['value']." (No active Admin)";
								}
                                echo("<option $disabled $selected value=".$id.">".$val."</option>");
                                                                                
                                    }
                            ?></select><input type=submit value=Transfer style='margin-left: 5px;'><?php
                                }
                            }
                            ?>
                        &nbsp;</form></span>
                        <?php   echo($resp['value']);  ?></td>
                        </tr>
                        <tr>
                            <th>Assigned Respondent:</th>
                            <td >
                                <?php
                                $sql = "SELECT tk.tkname n, tk.tksurname s FROM assist_".$cmpcode."_timekeep tk WHERE tk.tkid = '".$resp['admin']."'";
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['n']." ".$row['s']);
                                $admin = $row['n']." ".$row['s'];
                                //mysql_close();
                                ?> &nbsp;</td>
                        </tr>
                        <tr>
                            <th>Electricity - Type of Complaint:</th>
                            <td >
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_electype WHERE id = ".$calltypeid;
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['value']);
                                //mysql_close();
                                ?>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Urgency:</th>
                            <td >
                                <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_urgency WHERE id = ".$callurgencyid;
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['value']);
                                //mysql_close();
                                ?>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td >
                        <?php
                                $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE id = ".$callstatusid;
                                //include("inc_db_con.php");
                                $row = $helper->mysql_fetch_one($sql);
                                echo($row['value']);
                                //mysql_close();
                        ?>&nbsp;</td>
                                </tr>
                        <tr>
                               <th>Message:</th>
                                    <td ><div width=500px><?php
                                $mess = str_replace(chr(10), "<br>", $callmessage);
                                echo($mess);
                        ?>&nbsp;</div></td>
                        </tr>
            <tr>
                <th>Radio Respondant:</th>
                <td >
                    <?php
                    echo($callradio."&nbsp;");
                    ?></td>
            </tr>
            <tr>
                <th>Vehicle Registration No.:</th>
                <td >
                    <?php
                    echo($callvehicle."&nbsp;");
                    ?></td>
            </tr>
                        <?php
                        //strip udf functionality for speed
                    //$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                    //include("inc_db_con.php");
                    //while ($row = mysql_fetch_array($rs))
                    $udfrows = array();
					foreach($udfrows as $row)
                    {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$callid." AND udfindex = ".$row['udfiid'];
                        //include("inc_db_con2.php");
                        $udf = $helper->db_get_num_rows($sql2);
                        $row2 = $helper->mysql_fetch_one($sql2);
                        //mysql_close($con2);
            ?>
                        <tr>
                            <th><?php echo($row['udfivalue']); ?>:</th>
                            <td>
                    <?php
                        if ($udf > 0)
                        {
                            switch ($row['udfilist'])
                            {
                                case "Y":
                                    if ($helper->checkIntRef($row2['udfvalue']))
                                    {
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                        //include("inc_db_con2.php");
                                        $row3 = $helper->mysql_fetch_one($sql2);
                                        echo($row3['udfvvalue']);
                                        //mysql_close($con2);
                                    }
                                    else
                                    {
                                        echo ("N/A");
                                    }
                                    break;
                                case "T":
                                    //$sql = "SELECT * FROM assist_".$cmpcode."_".
                                    if ($row2['udfvalue'])
                                        echo $row2['udfvalue'];
                                    else
                                        echo "N/A";
                                    break;
                                default:
                                    echo(str_replace(chr(10), "<br>", $row2['udfvalue']));
                                    break;
                            }
                        }
                        else
                        {
                            echo("N/A");
                        }
                    }
                    ?>
                        &nbsp;</td>
                </tr>

                                <?php
                                $sql = "SELECT * FROM  assist_".$cmpcode."_caps_attachments AS c WHERE c.callid = ".$callid;
                                //include("inc_db_con.php");
                                $rows = $helper->mysql_fetch_all($sql);
                                ?>
                        <tr>
                            <th>Attachment(s):</th>
                            <td >
                                <?php
                                if (!$helper->db_get_num_rows($sql))
                                {
                                    echo ("<i>No attachments found.</i>");
                                }
                                else
                                {
                                    echo "<table style='border:none;'>";
                                }
                                //while ($row = mysql_fetch_assoc($rs)) {
                                foreach ($rows as $row){
                                    $file = "../files/".$cmpcode."/CAPS/".$row['system_filename'];
                                    if (!file_exists($file))
                                        continue;
                                    $callattach_id = $row['id'];

                                    echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:void(0))' class=downloadAttach id=".$callattach_id.">".$row['original_filename']."</a>
                       </td>
                       <td style='border:none;'>
                            <input type='button' name='deleteAttachment' id='deleteAttachment' value='Delete' onclick='delAttachment(\"$callid\",\"$callattach_id\")' />
                       </td>
                       </tr>");
                                }
                                if ($helper->db_get_num_rows($sql) > 0)
                                {
                                    echo("</table>");
                                }
                                ?>

                                    </td>
                                </tr>
<?php if ($callstatusid != 4)
                                { ?>
                                    <tr><form name="edit" method="post" action=""><input type=hidden name=b value="<?php echo $urlback; ?>">
										<th></th>
                                        <td  >
                                            
                                            <input type=hidden name=logadmintext value="<?php echo("Call cancelled by ".$tkname); ?>">
                                            <input type=hidden name=email value=Y>
                                            <input type=hidden name=utype value=C>
                                            <input type=hidden size=1 name=logtkid value=<?php echo($tkid); ?>>
                                            <input type=hidden size=1 name=logurgencyid value=<?php echo($callurgencyid); ?>>
                                            <input type=hidden size=1 name=logrespid value=<?php echo($callrespid); ?>>
                                            <input type=hidden size=1 name=logstatusid value=5>
                                            <input type=hidden size=1 name=callid value=<?php echo($callid); ?>>

                                            <input type=hidden  name=logtkid value=<?php echo($calltkid); ?>>
                                            <input type=hidden  name=callid value=<?php echo($callid); ?>>
                                            <input type=hidden  name=callconname value="<?php echo($callconname); ?>">
                                            <input type=hidden  name=callcontel value="<?php echo($callcontel); ?>">
                                            <input type=hidden  name=callconaddress1 value="<?php echo($callconaddress1); ?>">
                                            <input type=hidden  name=callconaddress2 value="<?php echo($callconaddress2); ?>">
                                            <input type=hidden  name=callconaddress3 value="<?php echo($callconaddress3); ?>">
                                            <input type=hidden  name=callarea value="<?php echo($callarea); ?>">
                                            <input type=hidden  name=callrespid value=<?php echo($callrespid); ?>>
                                            <input type=hidden  name=callmessage value="<?php echo($callmessage); ?>">
                                            <input type=hidden  name=callurgencyid value=<?php echo($callurgencyid); ?>>
                                            <input type=hidden  name=callstatusid value=<?php echo($callstatusid); ?>>
                                           <input type=hidden  name=callref value=<?php echo($callref); ?>>
                               <input type=hidden  name=calldate value=<?php echo($calldate); ?>>
                               <input type=hidden  name=loggedBy value=<?php echo($loggedBy); ?>>
                                            <input type=button name="btnEdit" value="Edit"  />
                                            <?php if(isset($DONOTDIPLAYTHISBUTTON) && $DONOTDIPLAYTHISBUTTON) { ?><span class=float><input type=button name="btnCancel" value="Cancel Complaint" class=idelete /></span><?php } ?>
                                            
                                                    </td>
                                                    </form>
                                                    </tr>
<?php } ?>
                            </table>
<?php if($src!="main_login.php") { $helper->displayGoBack($urlback); } ?>
                            <a name=update></a><h2>Updates</h2>
							<?php $helper->displayResult($result); ?>
                            <table id=update>
<?php
                                if ($callstatusid != 4 && $callstatusid != 5)
                                {
?>
                                    <form name=update method=post action=update_call_process.php enctype="multipart/form-data">
                                        <tr>
                                            <th>Date</th>
                                            <th width=300px>Update message</th>
                                            <th >Details</th>
                                        </tr>
                                        <tr>
                                            <td class=center rowspan=2><input type=hidden name=logdate value=<?php echo $today; ?>><?php echo(date("d-M-Y H:i", $today)); ?></td>
                            <td ><textarea cols=55 rows=7 name=logadmintext></textarea>
                                <input type=hidden name=email value=Y><input type=hidden name=b value="<?php echo $urlback; ?>">
                                <input type=hidden name=utype value=A>
                                <input type=hidden size=1 name=logtkid value=<?php echo($tkid); ?>>
                                <input type=hidden size=1 name=logurgencyid value=<?php echo($callurgencyid); ?>>
                                <input type=hidden size=1 name=logrespid value=<?php echo($callrespid); ?>>
                                <input type=hidden size=1 name=logstatusid value=<?php echo($callstatusid); ?>>
                                <input type=hidden size=1 name=callid value=<?php echo($callid); ?>>
                            </td>
                            <td ><table>
                            	<tr>
                            		<td class=b>Call status:</td>
                            		<td><select name=logstatusid>
<?php
                                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' AND id > 2 ORDER BY sort ASC";
                                    //include("inc_db_con.php");
                                    $rows = $helper->mysql_fetch_all($sql);
                                    //while ($row = mysql_fetch_array($rs)) {
                                    foreach ($rows as $row){
                                        $id = $row['id'];
                                        $val = $row['value'];
                                        if ($id == $callstatusid)
                                        {
                                            echo("<option selected value=".$id.">".$val."</option>");
                                        }
                                        else
                                        {
                                            echo("<option value=".$id.">".$val."</option>");
                                        }
                                    }
                                    //mysql_close();
?>
                                </select></td>
                            	</tr>
                            	<tr>
                            		<td class=b>Updated by:</td>
                            		<td><?php echo $tkname; ?></td>
                            	</tr>
                            	<tr>
                            		<td class=b>(On behalf of) Respondant:</td>
                            		<td><?php
                            $sql = "SELECT * 
                            		FROM assist_".$cmpcode."_caps_list_respondant r
                            		INNER JOIN assist_".$cmpcode."_timekeep t
                            		ON  t.tkid = r.admin AND t.tkstatus = 1
                            		INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
                            		ON mmu.usrtkid = t.tkid AND mmu.usrmodref = '$modref' 
                            		WHERE r.yn = 'Y' ORDER BY value ASC";
									$list = $helper->mysql_fetch_all($sql);
                        ?><select name=logrespid><option selected value=X>--- SELECT ---</option><?php
                                        //while ($row = mysql_fetch_array($rs))
                                        foreach($list as $row)
                                        {
                                            $id = $row['id'];
                                            $val = $row['tkname']." ".$row['tksurname']." (".$row['value'].")";
                                            echo("<option");
                                            if ($id == $callrespid)
                                            {
                                                echo(" selected");
                                            }
                                            echo(" value=".$id.">".$val."</option>");
                                        }
                        ?></select></td>
                            	</tr>
                            	<tr>
                            		<td class=b>Date action was taken:</td>
                            		<td><input type=text readonly=readonly name=actiondate value="<?php echo date("d-M-Y H:i"); ?>" id=actdate /><br />
                            			<small><i>(DD MMM YYY HH:mm)</i></small>
                            		</td>
                            	</tr>
                            </table><!--
                                <b>Call status:</b><br>
                                <select name=logstatusid>
<?php
                                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_status WHERE yn = 'Y' AND id > 2 ORDER BY sort ASC";
                                    //include("inc_db_con.php");
                                $rows = $helper->mysql_fetch_all($sql);
                                    //while ($row = mysql_fetch_array($rs)){
                                    foreach ($rows as $row){
                                        $id = $row['id'];
                                        $val = $row['value'];
                                        if ($id == $callstatusid)
                                        {
                                            echo("<option selected value=".$id.">".$val."</option>");
                                        }
                                        else
                                        {
                                            echo("<option value=".$id.">".$val."</option>");
                                        }
                                    }
                                    //mysql_close();
?>
                                </select><br>
                                <b>Respondent:</b><br>
                        <?php
                                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant r, assist_".$cmpcode."_timekeep t WHERE r.yn = 'Y' AND t.tkid = r.admin ORDER BY value ASC";
									$list = $helper->mysql_fetch_all($sql);
                                    //include("inc_db_con.php");
                                    //if (mysql_num_rows($rs) > 1)
                                    if(count($list)>0)
                                    {
                        ?><select name=logrespid><option selected value=X>--- SELECT ---</option><?php
                                        //while ($row = mysql_fetch_array($rs))
                                        foreach($list as $row)
                                        {
                                            $id = $row['id'];
                                            $val = $row['value']." (".$row['tkname']." ".$row['tksurname'].")";
                                            echo("<option");
                                            if ($id == $callrespid)
                                            {
                                                echo(" selected");
                                            }
                                            echo(" value=".$id.">".$val."</option>");
                                        }
                        ?></select><br><?php
                                    }
                                    else
                                    {
                                        echo($admin."<input type=hidden name=logrespid value=".$callrespid.">");
                                    }
                                    //mysql_close();
                        ?>
                                <b>Date action was taken:</b><br><small><i>(DD MMM YYY HH:mm)</i></small><br />
								<input type=text readonly=readonly name=actiondate value="<?php echo date("d-M-Y H:i"); ?>" id=actdate />
                               <!-- <br /><input type="text" name="dday" id=dd size="3" value="<?php echo(date("d")); ?>"> <select size="1" name="dmon" id=dm>
<?php
                                    $nmon = date("n");
                                    $months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                                    $m = 0;
                                    $m2 = 1;
                                    while ($m <= 11)
                                    {
                                        if ($m2 == $nmon)
                                        {
                                            echo("<option selected value=".$m2.">".$months[$m]."</option>");
                                        }
                                        else
                                        {
                                            echo("<option value=".$m2.">".$months[$m]."</option>");
                                        }
                                        $m++;
                                        $m2++;
                                    }
?>
                                                </select> <input type="text" id=dy name="dyear" size="6" value=<?php echo(date("Y")); ?>> <input type="text" name="dhr" id=dh size="3" value="<?php echo(date("H")); ?>" class=right>:<input type="text" name="dmin" id=di size="3" value="<?php echo(date("i")); ?>"> -->
                                            </td>
                                        </tr>
                                        <tr >
                                            <td colspan="2" align="right">
												<div id=firstdocWS><input type="file" name="attachments[]" id="attachment" size="30"/></div>
												<div style='margin-top: 15x; margin-left: 5px;'><a href="javascript:void(0)" id="attachlinkWS">Attach another file</a></div>
											</td>
                                        </tr>
                                        <tr>
                                            <td class=center colspan=3>
                                                <input type=submit value="Update complaint" class=isubmit />
                                            </td>
                                        </tr>
                                    </form>
            <?php
                                }
								
								$sql = "SELECT * FROM  `".$dbref."_attachments` WHERE logid IN (
										SELECT logid FROM ".$dbref."_log WHERE logcallid =32585)";
								//$rs = getRS($sql);
                                $rows = $helper->mysql_fetch_all($sql);
								$attach = array();
								//while($row = mysql_fetch_assoc($rs)) {
								foreach ($rows as $row){
								//arrPrint($row);
									$attach[$row['logid']][] = $row;
								}
								//mysql_close();//arrPrint($attach);

                                $sql = "SELECT a.logid, a.logemail, a.logdateact, p.admin, a.logsubmittkid,  ";
                                $sql.= "a.logdate, a.logadmintext, s.value as status, a.logstatusid, ";
                                $sql.= "t.tkname as n, t.tksurname as sn, tb.tkname as nb, tb.tksurname as snb, logutype ";
                                $sql.= "FROM assist_".$cmpcode."_caps_log a, ";
                                $sql.= "assist_".$cmpcode."_caps_list_status s, ";
                                $sql.= "assist_".$cmpcode."_timekeep t, ";
                                $sql.= "assist_".$cmpcode."_timekeep tb, ";
                                $sql.= "assist_".$cmpcode."_caps_list_respondant p ";
                                $sql.= "WHERE a.logcallid = ".$callid." ";
                                $sql.= "AND a.logstatusid = s.id ";
                                $sql.= "AND a.logrespid = p.id ";
                                $sql.= "AND p.admin = t.tkid ";
                                $sql.= "AND a.logsubmittkid = tb.tkid ";
                                $sql.= "AND a.logadmintext <> '' ";
                                $sql.= "AND (a.logutype = 'U' OR a.logutype = 'A' OR a.logutype='E') ";
                                $sql.= "ORDER BY ".$logsort[0]." ".$logsort[1].", logid DESC";
								$logs = $helper->mysql_fetch_all($sql);
								if(count($logs)>0) {
									echo "<tr class=\"ui-widget ui-state-white\">
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdate></span>Date Logged</th>
											<th>Update</th>
											<th><span class=\" ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: right; cursor: hand;\" id=logdateact></span>Details</th>
										</tr>";
									foreach($logs as $l) {
										$i = $l['logid'];//echo $i;
										$attachments = array();
										$attach_arr = isset($attach[$i]) ? $attach[$i] : array();
										foreach($attach_arr as $a) {
											if(file_exists("../files/".$cmpcode."/CAPS/".$a['system_filename'])) {
												$attachments[] = "<a href=javascript:void(0) class=downloadAttach id=".$a['id'].">".$a['original_filename']."</a>";
											}
										}
										if(count($attachments)>0) {
											$log_attach = "<br />Attachment".(count($attachments)>1 ? "s" : "").":<br />".implode("<br />",$attachments);
										} else {
											$log_attach = "";
										}
										
										if(strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):")!==false) {
											$log_text = substr(decode($l['logadmintext']),0,strpos($helper->decode($l['logadmintext']),"Complaint Attachment(s):"));
										} else {
											$log_text = $helper->decode($l['logadmintext']);
										}
										//$attach_pos = strpos(decode($l['logadmintext']),"Complaint Attachment(s):");
										//$log_text = $attach_pos!==false ? "true" : "false";
										
										echo "<tr>
												<td class=center>".date("d-M-Y H:i",$l['logdate'])."</td>
												<td>".$log_text;
												//arrPrint($attachments);
                                        $style = isset($style) ? $style : "";
										echo "
												".$log_attach."</td>
												</td><td>
												<span class=b>Status: </span>
												<br /><span class=\"".$style."\">".$l['status']."</span>
												<br /><span class=b>Respondant: </span>
												<br />".$l['n']." ".$l['sn']."
												".(date("dMYHi",$l['logdate'])!=date("dMYHi",$l['logdateact']) ? "<br /><span class=b>Action taken: </span><br />".date("d-M-Y H:i",$l['logdateact']) : "")."
												</td>
											</tr>";
									}
								}
								//mysql_close();
								
								

?>
                                </table>
<?php if($src!="main_login.php") { $helper->displayGoBack($urlback); } ?>

        <script type ="text/javascript">
            $(document).ready(function() {
				$("#update th").addClass("center");

				var logsort = new Array('<?php echo $logsort[0]; ?>','<?php echo $logsort[1]; ?>');
				var sorting = new Array();
				sorting['logdate'] = "logdateact";
				sorting['logdateact'] = "logdate";
				sorting['DESC'] = "ASC";
				sorting['ASC'] = "DESC";
				
				
				$("#update #logdate, #update #logdateact").click(function() {
					var url = '<?php echo $req; ?>';
					var id = $(this).attr("id");
					var newsort = new Array();
					newsort[0] = id;
					if(id==logsort[0]) {
						newsort[1] = sorting[logsort[1]];
					} else {
						newsort[1] = "DESC";
					}
					url = url+"&sort[]="+newsort[0]+"&sort[]="+newsort[1];
					document.location.href = url;
				});
               
			   
                $('#attachlinkWS').click(function(){
                    $('<div style="margin-top: 5px;"><input type="file" name=attachments[] size="30"/></div>').insertAfter('#firstdocWS');
                });
                $('input[name="btnCancel"]').click(function(event){
                   
                   if($(event.target).attr("name") == "btnCancel")
                    {
						if(confirm("Are you sure you wish to cancel this complaint?")==true) {
							$('form[name="edit"]').attr("action","update_call_process.php");
							$('form[name="edit"]').submit();
						}
                    }
                });
                $('input[name="btnEdit"]').click(function(event){
                    
                    if($(event.target).attr("name") == "btnEdit")
                    {
                        $('form[name="edit"]').attr("action","edit_call.php");
						$('input[name="utype"]').attr("value","E");
						$('form[name="edit"]').submit();
                        
                    }
                });
				$("#actdate").datetimepicker({
					timeFormat: 'hh:mm',
					dateFormat: 'dd-M-yy',
					maxDate: '<?php echo date("d-M-Y"); ?>',
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    changeMonth:true,
                    changeYear:true		
				});
            });

            function Validate() {
                return false;
            }
            function delAttachment(callid,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_attachment.php?callid="+callid+"&attachment_id="+attachment_id;
                }
                return false;
            }

            function download(path){
                document.location.href = "download.php?path="+path;
            }

        </script>

    </body>

</html>
