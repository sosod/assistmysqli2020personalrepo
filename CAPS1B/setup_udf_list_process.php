<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
require_once 'inc_head.php';

$vindex = $_REQUEST['udfvindex'];

if(!isset($_REQUEST['act']) || !(strlen($_REQUEST['act'])>0)) {
	$result = array("error","Ignite Assist could not determine your requested action.  Please try again.");
} else {

	switch($_REQUEST['act']) {
	case "ADD":
		//GET DETAILS
		$value = $helper->code($_REQUEST['udfvvalue']);
		$yn = "Y";

		if(strlen($value)>0 && $helper->checkIntRef($vindex)) {

			//ADD TO UDFVALUE
			$sql = "INSERT INTO assist_".$cmpcode."_udfvalue SET ";
			$sql.= "  udfvvalue = '".$value."'";
			$sql.= ", udfvindex = '".$vindex."'";
			$sql.= ", udfvcode = ''";
			$sql.= ", udfvcomment = ''";
			$sql.= ", udfvyn = '".$yn."'";
			$udfvid = $helper->db_insert($sql); //mysql_insert_id();
			if($helper->checkIntRef($udfvid)) {
				$tref = "CAPS";
				$trans = "Created new List Item ".$udfvid." for UDF ".$id;
				$tsql = $sql;
				//include("inc_transaction_log.php");
				$result = array("ok","UDF list item \'".($value)."\' has been created successfully.");
			} else {
				$result = array("error","Ignite Assist was unable to create the UDF list item.  Please try again.");
			}
		} else {
				$result = array("error","Ignite Assist was unable to create the UDF list item.  Please try again.");
		}
		break;
	case "EDIT":
		$i = $_REQUEST['id'];
		$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = $i";
		$old = $helper->mysql_fetch_one($sql);
		$value = $helper->code($_REQUEST['udfvvalue']);
		$sql = "UPDATE assist_".$cmpcode."_udfvalue SET
					udfvvalue = '".$value."'
				WHERE udfvid = $i";
		$mar = $helper->db_update($sql);
		if($mar>0) {
			$log = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,'$today','$tkid','$modref','Edited UDF list item $i','".addslashes($sql)."','".serialize($old)."')";
            $helper->db_insert($log);
			$result = array("ok","UDF list item \'".$value."\' has been edited successfully.");
		} else {
			$result = array("info","No change was found to be saved.");
		}
		break;
	case "DELETE":
		$i = $_REQUEST['id'];
		$sql = "UPDATE assist_".$cmpcode."_udfvalue SET
					udfvyn = 'N'
				WHERE udfvid = $i";
		$mar = $helper->db_update($sql);
		if($mar>0) {
			$log = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,'$today','$tkid','$modref','Deleted UDF list item $i','".addslashes($sql)."','')";
            $helper->db_insert($log);
			$result = array("ok","UDF list item \'".$i."\' has been deleted successfully.");
		} else {
			$result = array("info","No change was found to be saved.");
		}
		break;
	}
		
		

}


echo "
<script type=text/javascript>
	document.location.href = 'setup_udf_list.php?i=".$vindex."&r[]=".$result[0]."&r[]=".$result[1]."';
</script>
";

?>