<?php
include("inc_ignite.php");
$local_helper = new CAPS1B();
?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        
    </head>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Update Complaint</b></h1>
        <?php
		$b = $_REQUEST['b'];
        $logadmintext = $_POST['logadmintext'];
        $email = $_POST['email'];
        $utype = $_POST['utype'];
        $logtkid = $_POST['logtkid'];
        $logurgencyid = $_POST['logurgencyid'];
        $logrespid = $_POST['logrespid'];
        $logstatusid = $_POST['logstatusid'];
        $callid = $_POST['callid'];
        $doneyn = "N";
        /*$dday = $_POST['dday'];
        $dmon = $_POST['dmon'];
        $dyear = $_POST['dyear'];
        $dhr = $_POST['dhr'];
        $dmin = $_POST['dmin'];*/
		$actdate = $_REQUEST['actiondate'];
		$dateact = strlen($actdate)>0 ? strtotime($actdate) : $today;

        if($utype != "C" && strlen($logadmintext)==0) {
            ?>
        <input type=hidden id=i value=<?php echo($callid); ?>>
        <script type="text/javascript">
            alert("Please enter an Update Message.");
            var i = document.getElementById('i').value;
            if(i.length>0)
            {
                var url = "update_call.php?i="+i;
            }
            else
            {
                var url = "update.php";
            }
            document.location.href = url;
        </script>
            <?php
        }
        else {

            /*if(strlen($dday)>0 && strlen($dday)<3 && strlen($dmon)>0 && strlen($dyear)==4 && strlen($dhr)>0 && strlen($dhr)<3 && strlen($dmin)>0 && strlen($dmin)<3 && is_numeric($dday) && is_numeric($dmon) && is_numeric($dyear) && is_numeric($dhr) && is_numeric($dmin)) {
                $dateact = mktime($dhr,$dmin,1,$dmon,$dday,$dyear);
            }
            else {
                $dateact = $today;
            }*/

            $char1[0] = "'";
            $char2[0] = "&#39";

            $marr = explode($char1[0],$logadmintext);
            $logadmintext = implode($char2[0],$marr);


//GET TO EMAIL
            $sql = "SELECT tk.tkemail, tk.tkid FROM assist_".$cmpcode."_timekeep tk, assist_".$cmpcode."_caps_list_respondant p WHERE tk.tkid = p.admin AND p.id = ".$logrespid;
            //include("inc_db_con.php");
            $row = $local_helper->mysql_fetch_one($sql);
            $to = $row['tkemail']; //respondant
            $logtkid = $row['tkid'];
            //mysql_close();
            $sql = "";
            $row = "";

            $from = "no-reply@ignite4u.co.za"; //user


            switch ($utype) {
                case "C":
                //SET SQL statements
                    $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET callstatusid = ".$logstatusid." WHERE callid = ".$callid;
                    $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                    $sqllog .= "SET logdate = '".$today."', ";
                    $sqllog .= "logtkid = '".$logtkid."', ";
                    $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                    $sqllog .= "logrespid = ".$logrespid.", ";
                    $sqllog .= "logadmintext = '".$logadmintext."', ";
                    $sqllog .= "logstatusid = ".$logstatusid.", ";
                    $sqllog .= "logemail = '".$email."', ";
                    $sqllog .= "logcallid = ".$callid.", ";
                    $sqllog .= "logsubmitdate = '".$today."', ";
                    $sqllog .= "logutype = '".$utype."', ";
                    $sqllog .= "logdateact = '".$dateact."', ";
                    $sqllog .= "logsubmittkid = '".$tkid."'";
                    //Update database
                    $sql = "";
                    $sql = $sqllog;
                    //include("inc_db_con.php");
                    $logid = $local_helper->db_insert($sql);
                    $sql = "";
                    $sql = $sqlcall;
                    //include("inc_db_con.php");
                    $local_helper->db_update($sql);
                    //Send email
                    $sql = "SELECT * FROM assist_".$cmpcode."_caps_call WHERE callid = ".$callid;
                    //include("inc_db_con.php");
                    $row = $local_helper->mysql_fetch_one($sql);
                    //mysql_close();
                    $callmess = explode(chr(10),$row['callmessage']);
                    $callmessage = implode("<br>",$callmess);
                    $subject = "Complaint cancelled";
                    $message = "<font face=arial>";
                    $message .= "<p><small>Complaint ".$row['callref']." has been cancelled.<br>";
                    $message .= "The original complaint was: <br>";
                    $message .= "<i>".$callmessage."</i></p>";
                    $message .= "</small></font>";
                    $header = "From: no-reply@ignite4u.co.za\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
                    //mail($to,$subject,$message,$header);
                    $mail = new ASSIST_EMAIL($to,$subject,$message,"HTML");
                    $mail->sendEmail();
                    $header = "";
                    $message = "";

                    $doneyn = "C";
                    break;
                case "A":
                //SET SQL statements
                    $sqlcall = "UPDATE assist_".$cmpcode."_caps_call SET callstatusid = ".$logstatusid." WHERE callid = ".$callid;
                    $sqllog = "INSERT INTO assist_".$cmpcode."_caps_log ";
                    $sqllog .= "SET logdate = '".$today."', ";
                    $sqllog .= "logtkid = '".$logtkid."', ";
                    $sqllog .= "logurgencyid = ".$logurgencyid.", ";
                    $sqllog .= "logrespid = ".$logrespid.", ";
                    $sqllog .= "logadmintext = '".$logadmintext."', ";
                    $sqllog .= "logstatusid = ".$logstatusid.", ";
                    $sqllog .= "logemail = '".$email."', ";
                    $sqllog .= "logcallid = ".$callid.", ";
                    $sqllog .= "logsubmitdate = '".$today."', ";
                    $sqllog .= "logutype = '".$utype."', ";
                    $sqllog .= "logdateact = '".$dateact."', ";
                    $sqllog .= "logsubmittkid = '".$tkid."'";
                    //Update database
                    $sql = "";
                    $sql = $sqllog;
                    //include("inc_db_con.php");
                    $logid = $local_helper->db_insert($sql);
                    $tref = "CAPS";
                    $tsql = $sql;
                    $trans = "Updated call log for call ".$callid;
                    //include("inc_transaction_log.php");
                    $sql = "";
                    $sql = $sqlcall;
                    //include("inc_db_con.php");
                    $local_helper->db_update($sql);
                    $tref = "CAPS";
                    $tsql = $sql;
                    $trans = "Updated call status for call ".$callid;
                    //include("inc_transaction_log.php");
					$result = array("ok","Complaint successfully updated.");
                    //Send email
                    /*        $upmess = explode(chr(10),$logusertext);
        $upmessage = implode("<br>",$upmess);
        $subject = "Complaint updated";
        $message = "<font face=arial>";
        $message .= "<p><small>Complaint ".$callref." has submitted an update to call ".$callid.".<br>";
        $message .= "The update message is: <br>";
        $message .= "<i>".$logusertext."</i></p>";
        $message .= "</small></font>";
        $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
        mail($to,$subject,$message,$header);
        $header = "";
        $message = "";
                    */
                    $doneyn = "U";
                    break;
                default:
                    $doneyn = "C";
                    break;

            }
//uploads
            $original_filename = "";
            $system_filename = "";
            $attachments = "";
            //print_r($_FILES);die;
            if(isset($_FILES)) {
                $folder = "../files/$cmpcode/CAPS";
                /* if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
                $local_helper->checkFolder("CAPS");
$file = 0;
                foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                    //print_r($tmp_name);die;
                    if($_FILES['attachments']['error'][$key] == 0) {
                        $original_filename = $_FILES['attachments']['name'][$key];
                        $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                        $parts = explode(".", $original_filename);
                        $file++;
                        $system_filename = $logid."_".$file."_".date("YmdHi").".$ext";
                        $full_path = $folder."/".$system_filename;
                        move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                        $sql = "INSERT INTO assist_".$cmpcode."_caps_attachments (callid,logid,original_filename,system_filename)
                                VALUES (0,$logid,'$original_filename','$system_filename')";
                        //include("inc_db_con.php");
                        $local_helper->db_insert($sql);
                        $filename = "../files/".$cmpcode."/CAPS/".$system_filename;
//                        $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
                    }
                }

            }
            $logupdate="";
/*            if($attachments != '') {
                $logupdate .= "<br />Complaint Attachment(s):<br /> $attachments";
                $logupdate = addslashes(htmlentities($logupdate));
                //$sql = "UPDATE assist_".$cmpcode."_caps_log SET logadmintext=CONCAT_WS('\n',logadmintext,'$logupdate') WHERE logid=$logid";
                //include("inc_db_con.php");
            }*/

            ?>
        <form name=update>
            <input type=hidden name=done value=<?php echo($doneyn); ?>>
            <input type=hidden name=callid value=<?php echo($callid); ?>>
        </form>
        <script language=JavaScript>
            var doneyn = document.update.done.value;
            var callid = document.update.callid.value;
            //alert(doneyn);
            if(doneyn == "C")
            {
                document.location.href = "update.php";
            }
            else
            {
				var b = escape('<?php echo $b; ?>');
                document.location.href = "update_call.php?b="+b+"&i="+callid+"&r[]=<?php echo $result[0]; ?>&r[]=<?php echo $result[1]; ?>#update";
            }
        </script>
            <?php
        }   //if text entered
        ?>
    </body>
</html>
