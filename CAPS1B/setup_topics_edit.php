<?php
/**
 * from inc_ignite.php
 * @var CAPS1B $helper
 */
    //include("inc_ignite.php");
	require_once 'inc_head.php';


    $id = $_GET['id'];
if(strlen($id)==0)
{
    $id = $_POST['id'];
    $val = $_POST['val'];
    $admin = $_POST['admin'];
    $hodept = $_POST['hodept'];
    $hodiv = $_POST['hodiv'];

    if(strlen($val) > 0 && $admin != "X" && strlen($id) > 0)
    {
        //CREATE SORT INT IF BLANK OR NOT NUM
        //ADD RECORD
        $sql = "UPDATE assist_".$cmpcode."_caps_list_respondant SET value = '".$val."', admin = '".$admin."', hodept = '".$hodept."', hodiv = '".$hodiv."', yn = 'Y' WHERE id = ".$id;
            //---echo($sql);
        //include("inc_db_con.php");
        $helper->db_update($sql);
            $tref = "CAPS";
            $tsql = $sql;
            $trans = "Edited respondant ".$id.".";
            //include("inc_transaction_log.php");
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
    }
}
else
{
    $type = $_GET['t'];
    if($type == "del")
    {
        $sql = "UPDATE assist_".$cmpcode."_caps_list_respondant SET yn = 'N' WHERE id = ".$id;
            //---echo($sql);
        //include("inc_db_con.php");
        $helper->db_update($sql);
            $tref = "CAPS";
            $tsql = $sql;
            $trans = "Removed respondant ".$id.".";
            //include("inc_transaction_log.php");
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
    }
}
?>
<script type=text/javascript>
function delResp(id) {
    if(confirm("Are you sure you wish to delete this respondant?")==true)
    {
        document.location.href = "setup_topics_edit.php?id="+id+"&t=del";
    }
}
</script>
<h1 >Setup >> Respondants >> Edit</h1>

<table>
	<tr>
		<th>ID</th>
		<th>Dept/Division</th>
		<th>Respondant</th>
		<th>Head of Dept</th>
		<th>Head of Division</th>
	</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_caps_list_respondant WHERE id = ".$id." ORDER BY value ASC";
//include("inc_db_con.php");
$n = $helper->db_get_num_rows($sql);
if($n > 0)
{
    $row = $helper->mysql_fetch_one($sql);
    $resp = $row['admin'];
    $hod = $row['hodept'];
    $hov = $row['hodiv'];
}
else
{
        echo("<script language=JavaScript>document.location.href='setup_topics.php';</script>");
}
//mysql_close();
?>
<form name=add method=post action=setup_topics_edit.php>
	<tr>
		<th><?php echo($id); ?><input type=hidden name=id value=<?php echo($id); ?>></th>
		<td><input type="text" name="val" size="20" value="<?php echo($row['value']);?>" maxlength=50></td>
		<td><select name=admin><option value=X>--- SELECT ---</option>
        <?php
$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000'  ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$rows_2 = $helper->mysql_fetch_all($sql2);
//while($row2 = mysql_fetch_array($rs2)) {
foreach ($rows_2 as $row2){
    $tid = $row2['tkid'];
    if($resp == $tid)
    {
        echo("<option selected value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
}
//mysql_close($rs2);
        ?>
        </select></td>
		<td><select name=hodept>
        <?php
$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid = '".$hod."' ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$d = $helper->db_get_num_rows($sql2);
//mysql_close($rs2);
if($d>0)
{
    echo("<option value=X>--- SELECT ---</option>");
}
else
{
    echo("<option selected value=X>--- SELECT ---</option>");
}

$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000'  ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$rows = $helper->mysql_fetch_all($sql2);
//while($row2 = mysql_fetch_array($rs2)) {
foreach ($rows as $row2){
    $tid = $row2['tkid'];
    if($hod == $tid)
    {
        echo("<option selected value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
}
//mysql_close($rs2);
        ?>
        </select></td>
		<td ><select name=hodiv>
        <?php
$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid = '".$hov."' ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$v = $helper->db_get_num_rows($sql2);
//mysql_close($rs2);
if($v>0)
{
    echo("<option value=X>--- SELECT ---</option>");
}
else
{
    echo("<option selected value=X>--- SELECT ---</option>");
}

$sql2 = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000'  ORDER BY tkname, tksurname";
//include("inc_db_con2.php");
$rows2 = $helper->mysql_fetch_all($sql2);
//while($row2 = mysql_fetch_array($rs2)) {
foreach ($rows2 as $row2){
    $tid = $row2['tkid'];
    if($hov == $tid)
    {
        echo("<option selected value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$row2['tkid'].">".$row2['tkname']." ".$row2['tksurname']."</option>");
    }
}
//mysql_close();
        ?>
        </select></td>
	</tr>
 <tr>
    <td colspan=5><input type=submit value="Save Changes" class=isubmit /> <span class=float><input type=button value=Delete onclick=delResp(<?php echo($id); ?>) class=idelete /></span></td>
</tr>
</form>
</table>
<?php $helper->displayGoBack("setup_topics.php","Go Back"); ?>
<script type=text/javascript>
$(function() {
	$("th").addClass("center");
});
</script>
</body>

</html>
