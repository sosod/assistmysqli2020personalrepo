<?php 
/**
 * Modifications to accommodate / check adjustments to auto logout process as a result of auto saving while capturing PM6 scoresheets
 * AA-232 - JC - 16 March 2021
 */
$result = array("ok");
$now = time();
switch($_REQUEST['action']) {
	case "extend":
		$_SESSION['session_timeout']['expiration'] = $now+$_SESSION['session_timeout']['setting'];
		$_SESSION['session_timeout']['current'] = $now;
		break;
	case "check":
		$expire = $_SESSION['session_timeout']['expiration'];
		if($now>$expire) {
			$result = array("logout");
		} else {
			$result = array("valid",($expire-$now),date("d F Y H:i:s",$expire));
		}
		break;
	case "timeout":
	default:
		$result = array("logout");
		break;
}
echo json_encode($result);
?>