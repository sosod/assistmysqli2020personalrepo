<?php
require_once 'inc_head_local.php';
error_reporting(-1);
//cateid is the module reference
$cateid = isset($_REQUEST['c'])?$_REQUEST['c']:"";
$sort = isset($_REQUEST['s'])?$_REQUEST['s']:"title";
$start= isset($_REQUEST['start'])?$_REQUEST['start']:"";
$f = isset($_REQUEST['f'])?$_REQUEST['f']:"";
$fldr_view = isset($_REQUEST['fld'])?$_REQUEST['fld']:"";
$find1 = html_entity_decode($f,ENT_QUOTES,"ISO-8859-1");
$find = explode(" ",$find1);
$mDocObj = new MDOC();
$js = "";

/*if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if($sort == "d") {
    $orderby = "docdate DESC, doctitle ASC";
} else {
    $orderby = "doctitle ASC, docdate DESC";
}*/
//if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if(strlen($sort)>3) { // == "d") {
    if($sort=="doctitle") {
        $orderby = "doctitle ASC, docdate DESC";
    } else {
        $orderby = $sort." DESC, doctitle ASC";
    }
} else {
    $orderby = "doctitle ASC, docdate DESC";
}


if(strlen($orderby)==0) { $orderby = "doctitle ASC, docdate DESC"; }

//Folders are main folders for each MODULE
$folders = $mDocObj->getFolders();
//ASSIST_HELPER::arrPrint($folders);
$catetitle = isset($folders[$cateid]['cattitle'])?$folders[$cateid]['cattitle']:"title";
$cate['catetitle'] = isset($folders[$cateid]['catsubtitle'])?$folders[$cateid]['catsubtitle']:"";

?>

<h1><a href=main.php?t=v class=breadcrumb>View</a> >> <?php echo $catetitle; ?></h1>

<?php if($catetitle!=$cate['catetitle']) { echo "<span style=\"float: left; margin-top: -15px;\"><h2>".$cate['catetitle']."</h2>"; } else { echo "<span style=\"float: left;\">"; } ?>
<form name=veiw action=view.php method=get><input type=hidden name=c value=<?php echo($cateid); ?>>
    <table cellpadding=5 cellspacing=0 width=650 style="margin-bottom: 10px;">
        <tr>
            <td width=250 style="border-right: 0px;">Sort by: <select id=sort><option selected value='title'>Document Title</option><option value='date'>Date Added</option><option value='expiry'>Expiry Date</option></select> <input type=button value=Go onclick="sortby();">&nbsp;</td>
            <td width=150 style="border-right: 0px; border-left: 0px;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Documents" onclick="document.location.href = 'view.php?c=<?php echo($cateid); ?>';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;"><!-- Search: <input type=hidden name=s value=0><input type=text width=15 name=f> <input type=submit value=" Go "> --></td>
        </tr>
    </table></form>
<?php


//Folders are main folders for each MODULE
$rs = $folders;
//ASSIST_HELPER::arrPrint($rs);
?>
    <table width=650 style="border-color: #fff;margin:-5 5 10 5;">
    <?php
    $ref = $start;
	//ASSIST_HELPER::arrPrint($rs);
	//foreach /*Folder*/----------
    foreach($rs as $key=>$item)
    {
    	
	//catmodref is the MODULE reference
	$catmodref = $item['catmodref'];
	$sharedBool = false;
	if(stripos(($item['name']),"(Shared)") !== false){
		$dc = $mDocObj->getUserDocuments($catmodref,$sort,$mDocObj->getUserID());
		$sharedBool = true;
		$_REQUEST['shr'] = $mDocObj->getUserID();
	}else{
	    $dc = $mDocObj->getDocuments($catmodref,$sort);
		$sharedBool = false;
	}
	
	//ASSIST_HELPER::arrPrint($dc);
    $m = count($dc);
	if($m>0) {
		$icon = "/pics/icons/folder_full.gif";
		$testItem = reset($dc);
		//Here we are, with a SHARED item
		if($sharedBool){
			$real = $mDocObj->getObjectFolders($testItem['modref'],$_REQUEST['shr']);
			//$real[] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			$SHAREmodref = $testItem['modref'];
			//$real = array();
		}else{
			$real = $mDocObj->getObjectFolders($item['catmodref']);
			$SHAREmodref = $catmodref;
		}
		//ASSIST_HELPER::arrPrint($real);
		$empty = false;
	} else {
		$icon = "/pics/icons/folder_empty.gif";
		$real = array();
		$empty = true;
	}
        $ref++;
		$shr = isset($_REQUEST['shr'])?"shr=".$_REQUEST['shr']:"";
        $url = strlen($cateid)>0 && $cateid == $catmodref ?"view.php"."?".$shr:"view.php?c=".$item['catmodref']."&".$shr;
        
        ?>
		<tr>
            <td style="border-color:#fff;">
            	<a href="<?php echo($url); ?>" style="text-decoration:underline;color: #000000"><img src="<?php echo($icon); ?>" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;"><?php echo($item['cattitle']); ?></a>
				 <?php
					 if($cateid == $item['catmodref']){
							if(!$empty){
								echo "<table width=650 style='border-color: #ffffff;'>";		 	
						 		foreach($real as $fldr){
				 					//echo "ITEM: ".ASSIST_HELPER::arrPrint($fldr);
						        	$fldr_also = strlen($fldr_view)<=0 || $fldr['type']."_".$fldr['id'] != $fldr_view ? "&fld=".$fldr['type']."_".$fldr['id'] : "";
						        	$fldr_url = "view.php?c=".$catmodref.$fldr_also."&".$shr;
					 				echo "<tr id='".$fldr['type']."_".$fldr['id']."'><td style='border-left-color:white; border-top-color:white; border-bottom-color:white;' width=8%>&nbsp;</td><td colspan=2><a href='$fldr_url' style='text-decoration:underline;color: #000000'><img src='/pics/icons/folder_full.gif' style='border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;'>".$fldr['title']."</a></td></tr>";
					 				if(strlen($fldr_view)>0){
					 					//$docs = $mDocObj->getDocumentsForView($item['catmodref'], implode("|",$fldr));
					 					$docs = $mDocObj->getDocumentsForView($SHAREmodref, $fldr['type']."|".$fldr['id']);
										//ASSIST_HELPER::arrPrint(array($SHAREmodref, $fldr['type']."|".$fldr['id']));
										//ASSIST_HELPER::arrPrint($docs);
										foreach($docs as $index=>$item){
											$document_object_string = $item['type_in']."_".$item['id_in'];
											if($fldr_view == $document_object_string){		
												$docid = $index;
												$floc2 = "../files/".$mDocObj->getCmpCode()."/".$item['location'];	
												echo "<tr>";
								                echo "<td style='border-top-color:#ffffff;border-left-color:#ffffff;border-bottom-color:#ffffff;'>&nbsp;</td><td class='mdoc_td' style='border-right-color:#ffffff;'>
								                			<a href='../MDOC/class/inc_attachment_controller.php?action=MDOC.GET_ATTACH&object_id=$docid&activity=' target='_blank'>";
								                $icon = "/pics/icons/".$item['icon'];
								                $icon2 = "../pics/icons/".$item['icon'];
								                if(file_exists($icon2)) {
								                    echo "<img src=$icon style=\"vertical-align: middle; margin: 2 4 2 10;border-width:1px; border-color: #FFFFFF;\" class=a>";
								                }
								                if(strlen($item['title']) > 0) {
								                    echo $item['title'];
								                } else {    //IF THERE IS TEXT IN THE TITLE
								                    echo "Untitled" ;
								                }
								                echo "</a>"." (".number_format(filesize($floc2)/1024,0)."kb)";
								                $type = isset($item['type']) && strlen($item['type'])>0 ? $item['type'] : "";
								                if(strlen($item['content'])>0) {
								                    echo "<ul><span style=\"font-style:italic;font-size:8pt;\">".$item['content']."</span></ul>";
								                }
								                echo "<div style=\"margin-left: 20px; font-size: 7pt;\">".$type."</div>";
								                echo "</td>";
								                //Download button
								                echo "<td class=\"mdoc_tools\" width=185 style=\"border-left-color:white; text-align: right; font-size: 7pt; font-style: italic\">";
								                echo "<input class='mdoc_download' type='button' value='Download' ref='$docid' />";
												//Metainfo 1
								        		echo "<div class='metaHolder'>&nbsp;";
								        		echo "<div class='meta'>";
								                $d1 = $item['doc_uploaded_date'];
								                $d2 = $item['doc_modified_date'];
								                echo "<br />Added on ".$d1;
								                $aname = isset($item['doc_uploader']) && strlen($item['doc_uploader'] > 0) ? $mDocObj->getAUserName($item['doc_uploader']) : "Unknown";
								                echo "<br />by ".$aname."";
								                if($d1!=$d2 && $d2 > $d1) {
									                $dname = isset($item['doc_modifier']) && strlen($item['doc_modifier'] > 0) ? $mDocObj->getAUserName($item['doc_modifier']) : "Unknown";
								                    echo "<br />Modified on ".$d2;
								                    echo "<br />by ".$dname."";
								                }
								        		echo "</div>";
								                //
								                //Metainfo 2
								        		echo "<div class='xmeta' style='display:none'>";
								                $date = isset($item['date'])?$item['date']."<br />":"Unknown <br />";
								                $exp = isset($item['expiry'])?$item['expiry']."<br />":"Unknown <br />";
								                echo "<br />Document date: ".$date;
								                echo "<br />Document expiry: ".$exp;
								        		echo "</div>";
								        		echo "</div>";
								                echo "</td>";
								                echo "</tr>";
											}
										}
									}else{
										$docs = array();
									}
								}
							echo"</table>";
							$js .= "$('.mdoc_download').button().click(function(){
			                   var myref = $(this).attr('ref');
			                   document.location.href = '../MDOC/class/inc_attachment_controller.php?action=MDOC.GET_ATTACH&object_id='+myref+'&activity='; 
			                });";
						}else{
							echo "<p>No documents to display.</p>";
						}
					 }
				 ?>           	
        	</td>
        </tr>
        <?php
    }
    ?>
    </table>
<?php



?>
</span>
<script type=text/javascript>
$(document).ready(function() {
	<?php echo $js; ?>
	$("#tbl_doc, #tbl_doc td").addClass("noborder");
	$("a").hover(
		function(){ $(this).css("color","#FE9900"); },
		function(){ $(this).css("color","#000000"); }
	);
	$(".a").hover(
		function(){ $(this).css("border-color","#FE9900"); },
		function(){ $(this).css("border-color","#FFFFFF"); }
	);
	$("#sort").val("<?php echo $sort ?>");
	$('.mdoc_tools').hover(function(){
		$(this).find('.xmeta').show();
		$(this).find('.meta').hide('fade');
	},function(){
		$(this).find('.xmeta').hide();
		$(this).find('.meta').show('fade');
	});
});
function sortby() {
    var c = '<?php echo($cateid); ?>';
    var shr = '<?php echo("&".$shr); ?>';
    var s = document.getElementById('sort').value;
    document.location.href = "view.php?c="+c+"&s="+s+shr;
}
</script>
<?php
//viewComments($cateid);
?>
<style type='text/css'>
.metaHolder {
		height:80px; 
		overflow:hidden;
	}
</style>
</body>
</html>
