<?php
require_once 'inc_head_local.php';
error_reporting(-1);
$mDocObj = new MDOC();
$type = isset($_REQUEST['t'])?$_REQUEST['t']:"V";
$start = isset($_REQUEST['s'])?$_REQUEST['s']:"";
$f = isset($_REQUEST['f'])?$_REQUEST['f']:"";
$find1 = html_entity_decode($f,ENT_QUOTES,"ISO-8859-1");
$find = explode(" ",$find1);
if(strlen($start)==0 || (!is_numeric($start) && $start != "e")) { $start = 0; }
$limit = 20;
$max = $mDocObj->getFolderCount();
$pages = ceil($max/$limit);
if(!is_numeric($start)) {
    $start = $max - ($max - ($limit*($pages-1)));
}
$page = ($start / $limit) + 1;
$rs = $mDocObj->getFolders();
//ASSIST_HELPER::arrPrint($rs);

/*
$sql = "SELECT count(catetitle) as ct ".$sqlcate;
foreach($find as $needle)
{
        $needle = str_replace("'","&#39",$needle);
        $sql.= " AND c.catetitle LIKE '%".$needle."%'";
}
$sql.=" ORDER BY catetitle";
include("inc_db_con.php");
    $mr = mysql_fetch_array($rs);
mysql_close($con);
$max = $mr['ct'];
*/
?>
<script type=text/javascript>
function goNext(s,f) {
    f = escape(f);
    document.location.href = "main.php?t=V&s="+s+"&f="+f;
}
</script>
<style type=text/css>
table td {
	border-color: #ffffff;
}

#tbl_filter, #tbl_filter td {
	border-color: #ababab;
}
</style>
<h1>View</h1>
<form name=veiw action=main.php method=get>
    <table width=650 style="margin-bottom: 10px;" id=tbl_filter>
        <tr>
            <td width=250 style="border-right: 0px;"><input type=button id=b1 value="|<" onclick="goNext(0,'<?php echo($find1)?>');"> <input type=button id=b2 value="<" onclick="goNext(<?php echo($start-$limit); ?>,'<?php echo($find1)?>');"> Page <?php echo($page); ?>/<?php echo($pages); ?> <input type=button value=">" id=b3 onclick="goNext(<?php echo($start+$limit); ?>,'<?php echo($find1)?>');"> <input type=button value=">|" id=b4 onclick="goNext('e','<?php echo($find1)?>');"></td>
            <td width=150 style="border-right: 0px; border-left: 0px;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Categories" onclick="document.location.href = 'main.php?t=V';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;"><!-- Search: <input type=hidden name=s value=0><input type=hidden name=t value=V><input type=text width=15 name=f> <input type=submit value=" Go "> --></td>
        </tr>
    </table>
</form>
<?php
//echo $sqlcate;

/*
$sql = "SELECT * ".$sqlcate;
foreach($find as $needle)
{
    $needle = str_replace("'","&#39",$needle);
    $sql.= " AND c.catetitle LIKE '%".$needle."%'";
}
$sql.=" ORDER BY catetitle LIMIT ".$start.", ".$limit;
include("inc_db_con.php");
$c = mysql_num_rows($rs);
*/
$c = count($rs);
if($c == 0)
{
    echo("<p>There are no documents to display.</p>");
}
else
{
    ?>
    <table width=650 style="border-color: #ffffff;">
    <?php
    $ref = $start;
    foreach($rs as $key=>$val)
    {

	if(stripos(($val['name']),"(Shared)") !== false){
		$shr = $mDocObj->getUserID(); 
		$dc = $mDocObj->getUserDocuments($val['catmodref'],"",$shr);
	}else{
		$shr = '';
	    $dc = $mDocObj->getDocuments($val['catmodref']);
	}
    
    //ASSIST_HELPER::arrPrint($dc);
	if(count($dc)>0) {
		$icon = "/pics/icons/folder_full.gif";
	} else {
		$icon = "/pics/icons/folder_empty.gif";
	}
        $ref++;
        $url = "view.php?c=".$val['catmodref']."&shr=".$shr;
        
        ?>
		<tr>
            <td style=""><a href="<?php echo($url); ?>" style="text-decoration:underline;color: #000000"><img src="<?php echo($icon); ?>" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;"><?php echo($val['cattitle']); ?></a></td>
        </tr>
        <?php
    }
    ?>
    </table>
    <?php
}
//mysql_close();
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(document).ready(function() {
	$("a").hover(
		function(){ $(this).css("color","#FE9900"); },
		function(){ $(this).css("color","#000000"); }
	);
});
<?php if($start==0) {?>document.getElementById('b1').disabled = true; document.getElementById('b2').disabled = true; <?php } ?>
<?php if($start == ($max - ($max - ($limit*($pages-1)))) || $pages == 0) { ?>document.getElementById('b3').disabled = true; document.getElementById('b4').disabled = true;<?php } ?>
</script>
</body>

</html>
