<?php
    require 'inc_head.php';
	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
?>
<h1>Setup >> Document Type</h1>
<?php displayResult($result); ?>
<form method=post action=setup_list_doctype_process.php id=frm_add>
<input type=hidden name=act value=ADD />
	<table>
		<tr>
			<th>Ref</th>
			<th>Type</th>
			<th></th>
		</tr>
		<tr>
			<th></th>
			<td><input type=text value="" name=value size=50 maxlength=200 id=add_val /></td>
			<td class=center><input type=button value=Add class=isubmit /></td>
		</tr>
	<?php
	$sql = "SELECT * FROM ".$dbref."_list_type WHERE yn = 'Y' ORDER BY value";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		echo "
		<tr>
			<th>".$row['id']."</th>
			<td id=td_".$row['id'].">".decode($row['value'])."</td>
			<td><input type=button value=Edit class=edit id=".$row['id']." /></td>
		</tr>";
	}
	mysql_close();
	?>
	</table>
</form>
<script type=text/javascript>
$(function() {
	$("#tbl_edit th").addClass("left");
	$("#dlg_edit").dialog({
		autoOpen: false,
		modal: true,
		height: 250,
		width: 450
	});
	$("#frm_add .isubmit").click(function() {
		var v = $("#add_val").val();
		if(v.length>0) {
			$("#frm_add").submit();
		} else {
			alert("Please enter the document type you wish to add.");
		}
	});
	$(".edit").click(function() {
		var i = $(this).prop("id");
		var v = $("#td_"+i).html();
		$("#dlg_edit #ref").val(i);
		$("#dlg_edit #value").val(v);
		$("#dlg_edit #td_ref").html(i);
		$("#dlg_edit #td_orig").html(v);
		$("#dlg_edit").dialog("open");
	});
	$("#frm_edit .isubmit").click(function() {
		var v = $("#dlg_edit #value").val();
		if(v.length>0) {
			$("#frm_edit").submit();
		} else {
			alert("Please enter the new Document Type.");
		}
	});
	$("#frm_edit .idelete").click(function() {
		if(confirm("Are you sure you wish to delete this Document Type?")==true) {
			$("#dlg_edit #act").val("DELETE");
			var v = $("#dlg_edit #value").val();
			if(v.length==0) {
				$("#dlg_edit #value").val($("#dlg_edit #td_orig").attr("innerText"));
			}
			$("#frm_edit .isubmit").trigger("click");
		} else {
			$("#dlg_edit").dialog("close");
		}
	});
});
</script>
<div id=dlg_edit title="Edit Document Type">
<h2>Edit Document Type</h2>
<form method=post action=setup_list_doctype_process.php id=frm_edit>
	<input type=hidden name=act value=EDIT id=act />
	<input type=hidden name=i value="" id=ref />
	<table id=tbl_edit>
		<tr>
			<th>Ref:</th>
			<td id=td_ref></td>
		</tr>
		<tr>
			<th>Original:</th>
			<td id=td_orig></td>
		</tr>
		<tr>
			<th>New:</th>
			<td><input type=text name=value id=value value="" size=50 maxlength=200 /></td>
		</tr>
	</table>
	<p><input type=submit value="Save Changes" class=isubmit /> <input type=reset /> <span class=float><input type=button value="Delete Type" class=idelete /></span></p>
</div>
</body>
</html>