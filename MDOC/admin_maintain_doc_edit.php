<?php
    require 'inc_head.php';
	$result = array();
	
if(isset($_REQUEST['act'])) {
	$docid = $_REQUEST['d'];
	
	$olddoc = mysql_fetch_one("SELECT * FROM ".$dbref."_content WHERE docid = ".$_REQUEST['d']);
	$newdoc = $_REQUEST;
	
	$src = $newdoc['src'];

	$changes = array();
	$fields = array("docdate","doctitle","doccateid","doccontent","doctypeid","doclistid");
	$head = array(
		'docdate' => "Document Date",
		'doctitle' => "Document Title",
		'doccateid' => "Category",
		'doccontent' => "Document Contents",
		'doctypeid' => "Document Format",
		'doclistid' => "Document Type"
	);
	$logs = array();
	foreach($fields as $f) {
		$o = $olddoc[$f];
		if($f=="docdate") {
			$n = strtotime($newdoc[$f]." 12:00:00");
		} else {
			$n = $newdoc[$f];
		}
		if($o!=$n) {	
			$changes[] = $f." = '".$n."'";
			$logs[] = " - ".$head[$f]." to '".$newdoc[$f]."' from '".decode($olddoc[$f])."'";
		}
	}
	
	if(count($changes)>0) {
		$changes[] = "docmoduser = '$tkid'";
		$changes[] = "docmoddate = '$today'";
		$sql = "UPDATE ".$dbref."_content SET ".implode(", ",$changes)." WHERE docid = ".$_REQUEST['d'];
		$mar = db_update($sql);
		if($mar>0) {
			$logtext = code("Document ".$docid." has been edited by ".$_SESSION['tku']." as follows: <br />".implode("<br />",$logs));
			logAct($logtext,$sql,"DOC",$docid);
			$result = array("ok","Changes saved to document ".$docid.".");
		} else {
			$result = array("info","No change was found to be saved for document ".$docid.".");
		}
	} else {
		$result = array("info","No change was found to be saved for document ".$docid.".");
	}






} elseif(isset($_REQUEST['d']) && checkIntRef($_REQUEST['d'])) {

	$src = $_SERVER['HTTP_REFERER'];

	$doc = mysql_fetch_one("SELECT * FROM ".$dbref."_content WHERE docid = ".$_REQUEST['d']);
	$catid = checkIntRef($doc['doccateid']) ? $doc['doccateid'] : "X";

	//arrPrint($doc);
	
	function subFunction($sub,$level,$title) {
		global $categories;
		global $access;
		global $my_access;
		global $tkid;
		global $catid;
		global $catedisplay;

		foreach($sub as $c) {
			if($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']])) {
					$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
					$echo.= "<option ".($c['cateid']==$catid ? "selected" : "")." value=".$c['cateid'].">".$text."</option>"; //."</span></p>";
			}
			$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
			if(count($sub)>0) {
				$cut = checkIntRef($catedisplay) ? $catedisplay : 20;
				$sub_title = $title.(strlen($title)>0 ? " >> " : "").substr($c['catetitle'],0,$cut).(strlen($c['catetitle'])>$cut ? "..." : "");
				//$sub_title = $title.(strlen($title)>0 ? " >> " : "").$c['catetitle'];
				$echo.=subFunction($sub,$level+1,$sub_title,$eliminated,$displayed);
			}
		}
		return $echo;
	}

	/** GET CATEGORIES **/
	$categories = array();
	$cate_count = 0;
	$cate_owner = array();

	$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$categories[$row['catesubid']][] = $row;
		$cate_count++;
		if($row['cateowner']==$tkid) {
			$cate_owner[$row['cateid']] = "Y";
		}
	}
	mysql_close();

	/** USER ACCESS **/
	$my_access = array();
	if($access['docadmin']!="Y") {
		$sql = "SELECT * FROM ".$dbref."_categories_users 
				INNER JOIN ".$dbref."_categories
				  ON cucateid = cateid AND cateyn = 'Y'
				WHERE cuyn = 'Y' AND cutkid = '$tkid' AND (cutype = 'UP' OR cutype = 'MAIN')";
		$rs = getRS($sql);
		while($row = mysql_fetch_assoc($rs)) {
			$my_access[$row['cucateid']] = "Y";
			if(isset($cate_owner[$row['cucateid']])) { unset($cate_owner[$row['cucateid']]); }
		}
		mysql_close();
		if(count($cate_owner)>0) {
			foreach($cate_owner as $i => $y) {
				$my_access[$i] = $y;
			}
		}
		$total_cate = count($my_access);
	} else {
		$total_cate = $cate_count;
	}

	//arrPrint($doc);

	?>
	<h1><a href=admin.php class=breadcrumb>Admin</a> >> Maintain >> Edit Document</h1>
	<p>Fields marked with a * are required.</p>
	<form id=frm_upload method=post action=admin_maintain_doc_edit.php enctype="multipart/form-data" onsubmit="return Validate();" language=jscript>
	<input type=hidden name=src value="<?php echo code($src); ?>" />
	<input type=hidden name=act value=SAVE />
	<input type=hidden name=d value=<?php echo $_REQUEST['d']; ?> />
	<table  id=tbl_upload><tbody>
		<tr>
			<th>Document Title:*</th>
			<td><input type=text name=doctitle size=50 maxlength=200 value="<?php echo decode($doc['doctitle']); ?>" /></td>
		</tr>
		<tr>
			<th>Document Contents:</th>
			<td><textarea name=doccontent rows=3 cols=50><?php echo decode($doc['doccontent']); ?></textarea></td>
		</tr>
		<tr>
			<th>Category:*</th>
			<td><select name=doccateid>            
				<option <?php echo $catid=="X" ? "selected":"";?> value=X>--- SELECT ---</option>
				<?php echo subFunction($categories[0],0,""); ?>
			</select></td>
		</tr>
		<tr>
			<th>Document Type:*</th>
			<td><select name=doclistid>
				<?php
				echo "<option value=0 ".(!checkIntRef($doc['doclistid']) ? "selected" : "").">Unspecified</option>";
				$sql = "SELECT * FROM ".$dbref."_list_type WHERE yn = 'Y' ORDER BY value";
				$rs = getRS($sql);
				while($row = mysql_fetch_assoc($rs)) {
					echo "<option value=".$row['id']." ".($doc['doclistid']==$row['id'] ? "selected" : "").">".$row['value']."</option>";
				}
				mysql_close();
			?></select></td>
		</tr>
		<tr>
			<th>Document Date:*</th>
			<td><input type=text name=docdate readonly=readonly id=datepicker1 class=jdate2012 value="<?php echo(date("d-M-Y",$doc['docdate'])); ?>"></td>
		</tr>
		<tr>
			<th>Document:*</th>
			<td><?php echo $doc['docfilename']; ?></td>
		</tr>
		<tr>
			<th>Document Format:*</th>
			<td><select name=doctypeid>
				<?php
				echo "<option value=X ".(!checkIntRef($doc['doctypeid']) ? "selected" : "").">--- SELECT ---</option>";
				$sql = "SELECT * FROM ".$dbref."_list_doctype WHERE yn = 'Y'";
				$doctype = mysql_fetch_all($sql);
					//while($row = mysql_fetch_array($rs))
					foreach($doctype as $row) {
						$id = $row['id'];
						$val = $row['value'];
						echo("<option value=$id ".($doc['doctypeid']==$id ? "selected" : "")."> $val </option>");
					}
				//mysql_close($con);
				?>
			</select></td>
		</tr>
		<tr>
			<th></th>
			<td><input type=submit value="Save Changes" class=isubmit /> <input type=reset></th>
		</tr>
	</tbody></table>
	</form>
	<script type=text/javascript>
	$(document).ready(function() {
		$("#tbl_upload th").addClass("left");
	});
	</script>
<?php

} else {

	$result = array("error","An error occurred while trying to identify the document to be edited.  Please try again.");

}

if(count($result)==2) {
	echo "<script type=text/javascript>
			document.location.href = '".$src."&r[]=".$result[0]."&r[]=".$result[1]."';
		</script>";
}

?>
</body></html>