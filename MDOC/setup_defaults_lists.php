<?php
require_once("inc_head_local.php");

//ASSIST_HELPER::arrPrint($_REQUEST);
?>
<h1>Setup >> Lists</h1>
<script type="text/javascript" >
$(document).ready(function() {
    

	//$("table.tbl-container, table.tbl-container td:first").addClass("noborder");
	//$("table.tbl-container tr").find("td:first").addClass("noborder");
	//$("table.tbl-subcontainer").parent("td").addClass("noborder");
	//$("table.sub_table").addClass("noborder").find("td").addClass("noborder");
	$("table.tbl-container").addClass("noborder");
	$("table.tbl-container:not(.not-max)").css("width","100%");
	$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
	$("table.th2").find("th").addClass("th2");
	$("table tr.th2").find("th").addClass("th2");
	$("table.tbl_audit_log").css("width","100%").find("td").removeClass("noborder");
	//$("h2").css("margin-top","0px");
	$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");



	//FINANCE specific formatting
	$("table.finance").addClass("form");
	$("table.finance th.DEFAULT").css({"background-color":"#FFFFFF","color":"#000000","border":"1px solid #ababab"});
	
	$("table.finance th.SUB_TOTAL").addClass("th2");
	$("table.finance td.SUB_TOTAL").css({"font-weight":"bold"});
	
	$("table.finance th.TOTAL").parent().addClass("total");
	//$("table.finance td.TOTAL").addClass("th2");
	
	$("table.finance-list tr:gt(0)").find("td:gt(0)").addClass("right");

	$("button.abutton").children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px"});




	//TREE VIEW SETTINGS
	$("table.tbl-tree tr:eq(2)").find("td:first").prop("width","500px");

});

</script>
<style type="text/css">
	.noborder { border: 1px dashed #ffffff; }
	table.tbl_audit_log { width: 100%; }
	table.attach { border: 0px dashed #009900; }
	table.attach td { 
		border: 0px dashed #009900;
		vertical-align: middle;
		padding: 1px; 
	}
	.button_class, .ui-widget.button_class {
		font-size: 75%;
		padding: 1px;
	}
	tr.total th {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	.abutton {
		font-size: 90%;
		padding: 0px;
		font-weight: bold;
	}
	
	table.tbl-tree {
		border: 0px solid #fe9900;
	}
	table.tbl-tree th {
		border: 0px solid #fe9900;
		color: #000099;
		background-color: #ffffff;
		font-size: 150%;
		line-height: 155%;
		font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
	}
	table.tbl-tree td {
		border: 0px solid #fe9900;
	}
	table.tbl-tree td.cate-pos, table.tbl-tree tr.cate-pos td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 115%;
		background-color: #9494d4;
		color: #000000;
	}
	table.tbl-tree td.td-line-break, table.tbl-tree tr.td-line-break td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
		font-size: 0.5em;
		line-height: 0.5em;
		padding: 1px;
		background-color: #FFFFFF;
	}

	table.tbl-tree td.grand-parent, table.tbl-tree tr.grand-parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 125%;
	}
	table.tbl-tree td.parent, table.tbl-tree tr.parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
	}
	table.tbl-tree td.sub-parent, table.tbl-tree tr.sub-parent td {
		font-weight: normal;
		text-decoration: none;
		font-style: italic;
	}
	table.tbl-tree td.child, table.tbl-tree tr.child td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	.white-border {
		border-color: #FFFFFF;
	}
	
	table.tbl-tree tr.grand-parent td.td-button, table.tbl-tree tr.grand-parent td.count {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	
	button:disabled {
		cursor: not-allowed; 
	}
	
	.disabled-button {
		color: #777777;
		border: 1px solid #777777;
		background-color: #efefef;
	}
 
.ui-button-state-ok, .ui-widget-content .ui-button-state-ok, .ui-widget-header .ui-button-state-ok  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #009933;
	cursor: pointer; 
}
.ui-button-state-ok a, .ui-widget-content .ui-button-state-ok a,.ui-widget-header .ui-button-state-ok a { 
	color: #363636; 
	padding: 0px;
	cursor: pointer; 
}

.ui-button-state-info, .ui-widget-content .ui-button-state-info, .ui-widget-header .ui-button-state-info  {border: 0px solid #ffffff; background: url(); color: #fe9900; }
.ui-button-state-info a, .ui-widget-content .ui-button-state-info a,.ui-widget-header .ui-button-state-info a { color: #fe9900; }

.ui-button-state-default, .ui-widget-content .ui-button-state-default, .ui-widget-header .ui-button-state-default  {border: 1px solid #888888; background: url() ; color: #888888; }
.ui-ui-button-state-default a, .ui-widget-content .ui-button-state-default a,.ui-widget-header .ui-button-state-default a { color: #363636; }

.ui-state-error-button, .ui-widget-content .ui-state-error-button, .ui-widget-header .ui-state-error-button  {border: 0px solid #cc0001; background: url() ; color: #cd0a0a; }
.ui-state-error-button a, .ui-widget-content .ui-state-error-button a,.ui-widget-header .ui-state-error-button a { color: #363636; }


.ui-state-icon-button, .ui-widget-content .ui-state-icon-button, .ui-widget-header .ui-state-icon-button  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #363636;
	padding: 0px;
	margin: 0px;  
}
.ui-state-icon-button a, .ui-widget-content .ui-state-icon-button a,.ui-widget-header .ui-state-icon-button a { color: #363636; }

.ui-state-focus-icon-button {
	border: 0px solid #ffffff; 
	background: #ffffff; 
	color: #FE9900;
	padding: 0px; 
	margin: 0px;
}

.ui-button-state-error .ui-icon {
	background-image: url(/library/images/ui-icons_cc0001_256x240.png);
}
.ui-button-state-ok .ui-icon {
	background-image: url(/library/images/ui-icons_009900_256x240.png);
}
.ui-button-state-blue .ui-icon {
	background-image: url(/library/images/ui-icons_000099_256x240.png);
}
.ui-button-state-black .ui-icon {
	background-image: url(/library/images/ui-icons_222222_256x240.png);
}
.ui-button-state-info .ui-icon {
	background-image: url(/library/images/ui-icons_fe9900_256x240.png);
}

.ui-button-state-default .ui-icon {
	background-image: url(/library/images/ui-icons_888888_256x240.png);
}

	
</style>
<?php

$headingObject = new MDOC_HEADINGS();
$displayObject = new MDOC_DISPLAY();
$js = "";

//Setting the url of the page sans queries
$noparams = explode("&",$_SERVER['REQUEST_URI']);
$pageonly = explode("/",$noparams[0]);
$thispage = $pageonly[2];

$list_id = $_REQUEST['l'];

$listObj = new MDOC_LIST($list_id);
//echo $listObj->getListTable();
$fields = $listObj->getFieldNames();  //ASSIST_HELPER::arrPrint($fields);
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();
$list_data = $listObj->getListItemsForSetup();
$cat_sups = array();	

//ASSIST_HELPER::arrPrint($fields);


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>

<!--
<div id=div_error class=div_frm_error>
	
</div>


<h3 class=idelete>Development notes for list page</h3>
<ul>
<li>Assessment Frequency - only rename and deactivate/restore, no add or delete </li> 
<li>Contract, Deliverable, Action Status - where status & MDOC::SYSTEM_DEFAULT==MDOC::SYSTEM_DEFAULT => only rename client_name no delete/deactivate</li> 
<li>Deliverable Status - no add, only rename</li> 
<li>Service Provider - no add/edit if category list is empty</li>
</ul>
-->

<h2><?php echo $headingObject->getAListHeading($list_id); ?></h2>
<?php


/****************
var_dump($types);
var_dump($fields);
var_dump($items);
echo"<br>";
echo"<br>";
*****************/

/**************
 * PROGRAMMING TO BE DONE
 * 
 * get fields from listobject
 * get all list items from list object
 * 
 * draw table
 *    heading row to be determined by fields list
 *    second row = add form = to be determined by fields list
 *    all list items to follow as individual rows with Edit button 
 * 
 * draw div for use in edit dialog
 * 
 * populate controller 
 *   - on button click, get the list item details from list object and populate edit dialog
 *   - on edit dialog save button click, update list items details, if result[0] = ok then reload page with result else display error
 * 
 * if contract_suppplier
 * 		validate that contract_supplier_category has been populated first.
 */
?>
<div class="horscroll">
<table class='tbl-container not-max'><tr><td>
<form name=frm_list>
<input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
<input type="hidden" id="sort" name="sort" value="99" />
<table class=list id=master_list>
    <thead>
        <tr><?php
        	foreach($fields as $fld=>$val){
        		echo"<th>$val</th>";
        	}
    	?>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        //Add list
        	if($list_id !== "assessment_frequency" && $list_id !== "deliverable_status"){
        		echo "<tr id=\"add_row\">";
        		foreach($fields as $fld=>$name){
        			echo"<td >";
					//echo $fld;
					if($fld == "status"){
						echo "<div class=center>Active</div><input type=\"hidden\" name='$fld' id='$fld' value=" . MDOC::ACTIVE . " />";
					}else if($fld == "list_num"){
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld, 'options'=>$cat_sups,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
					}else if($fld == "colour"){
						echo "<input type=\"hidden\" name='$fld' id=\"add_colour\" value=\"#FFFFFF\" />";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>'clr_'.$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
					}else{
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
					}
        			echo"</td>";
					
        		}
				echo "<td class=center><input type=button name=btn_add value=Add /></td></tr>";
        	}

    	?>
        <?php
			foreach($list_data as $key=>$val){
				echo"<tr id=\"tr_".$val['id']."\" ".((($val['status'] & MDOC::INACTIVE)==MDOC::INACTIVE) ? "class=inact" : "")." >";
				foreach($fields as $fld=>$head){
					$valkey = $fld;
					$valval = $val[$fld];	
					switch($valkey){
						case "sort":
							break;
						case "status":
							if(($valval & MDOC::SYSTEM_DEFAULT)==MDOC::SYSTEM_DEFAULT){
								echo "<td sys=1 class=\"center\">System Default";
							}else if(($valval & MDOC::ACTIVE)==MDOC::ACTIVE){
								echo "<td class=\"center\">Active";
							}else if(($valval & MDOC::INACTIVE)==MDOC::INACTIVE){
								echo "<td class=\"center\">Inactive";
							}else{
								echo "<td class=\"center\">";
							}
							
								echo "</td>";
							break;
						case "list_num":
							if(isset($cat_sups[$valval])){
								echo "<td fld=\"". $valkey ."\">". $cat_sups[$valval]."</td>";
							} else {
								echo "<td fld=\"". $valkey ."\">".$helper->getUnspecified()."</td>";
							}
							break;
						case "colour":
							echo "<td style=\"background-color:".$valval."\" fld=\"". $valkey ."\"></td>";
							break;
						default:	
							echo "<td fld=\"". $valkey ."\">".$valval."</td>";
						//$js .=" rows['".$val['id']."']['". $valkey ."'] = ". $valval ." ; ";
					}
				}
				if(($val['status']& MDOC::ACTIVE)==MDOC::ACTIVE){
				    echo "<td class=center><input type=button value=Edit id='".$val['id']."' class='btn_edit' can_delete='".($val['can_delete']==true ? 1 : 0)."' /></td>";
				}else{
				    echo "<td class=center><input type=button value=Restore id='".$val['id']."' class='btn_restore' /></td>";
				}
				echo "</tr>";
			}	   
	   ?>

    </tbody>
</table>
</form>
</td></tr><tr><td>
	<?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup.php'),"setup",array('section'=>$list_id)); ?>
</td></tr>
</table>
</div>
<div id=div_dialog title="Edit">
    <h1>Edit form</h1>
    <form name=edit_frm>
    <input type="hidden" name="id" id="ref" value=0 />
    <input type="hidden" name="list_id" id="list_id" value=<?php echo $list_id; ?> />
    <table id=ed_frm class=form>
    	<?php
    	
    	foreach($fields as $fld=>$name){
    		echo"<tr>
    				<th>$name</th>
    				<td fld=edit_td_".$fld." id=\"edit_td_".$fld."\">";
					if($fld == "status"){
    					echo"Active";
					}else if($fld == "list_num"){
						echo"<p class=\"for_display\"></p>";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$fld, 'name'=>$fld, 'options'=>$cat_sups));
					}else{
						echo"<p class=\"for_display\"></p>";
						$js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld));
					}
				echo"</td>";
			echo"</tr>";
    	}

?>
        
    </table>
  	</form>
  	<?php ASSIST_HELPER::displayResult(array("info","Items can only be deleted if they are not in use elsewhere within in the module.")); ?>
</div>

<div id="div_dialog_restore" title="Restore">
    	<p id="restoration_p"></p>
</div>

<script type="text/javascript">
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);
//Thanks to Erick Petru
function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

$(function () {
    <?php 
    /*****************
	 * echo stored $js strings here
	 */
    	echo  $js ;
    ?>
    	
	//Autoscrolling tables if they exceed screen width
/*	$(window).resize(function() { 
		var w= AssistHelper.getWindowSize();
		var wi = w["width"];
		var wid = wi-30;
		var widt = ""+wid+"px";
		$(".horscroll").css({"width":widt});
		console.log($(".horscroll").get(0).scrollWidth +" : "+ $(".horscroll").get(0).clientWidth);
		if($(".horscroll").get(0).scrollWidth > $(".horscroll").get(0).clientWidth) { 
			$(".horscroll").css("border","1px solid #dedede"); 
		} else {
			$(".horscroll").css("border","0px solid #dedede"); 
		}
	});
	$(window).trigger("resize");*/
	
    $("#div_dialog").dialog({
        modal: true,
        autoOpen: false,
        width: "auto",
        buttons: [{
           text: "Save Changes",
           click: function(){
			var okay = true;
           	var id = $(this).attr("num");
			$("#ed_frm td textarea").each(function(){
				var lngth = $(this).val();
				var howlong = lngth.length;
				
				if(howlong == undefined || howlong == 0){
					okay = false; 
				}
			});
				
			$("#ed_frm td input").each(function(){
				if(!$(this).hasClass("color")){
					var length = parseInt($(this).val());
					if(length == 0){
						okay = false;
					}
				}
			});
				
			if($("#edit_list_num").val() == "X"){
				okay = false;
			}
			if(okay != true){
				alert("Please fill in all of the required fields");
			}else{
				//All changes are within bounds
				AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=edit_frm]"));
				var lid = $("#list_id").val();
				console.log(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=MDOC_LIST.editObject", dta);
				if(result[0]=="ok") {
	//				console.log(result);
					document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
	//				console.log(result);
				}
				
				
			}
			//$(this).dialog("close");
           }   
        },{
           text: "Deactivate",
           click: function(){
           		var id = $(this).attr("num");
                if(confirm("Are you sure you wish to deactivate this item?")==true) {
                    var lid = $("#list_id").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Deactivate","id="+id+"&list_id="+lid);
					if(result[0]=="ok") {
						document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
                }
                $(this).dialog("close");
           }   
        },{
           text: "Restore",
           click: function(){
               alert("restoring!!");
                $(this).dialog("close");
           }   
        },{
            text: "Delete",
            click: function(){
           		var id = $(this).attr("num");
                if(confirm("Are you sure you wish to remove this item?")==true) {
                    var lid = $("#list_id").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Delete","id="+id+"&list_id="+lid);
			//		alert(id+"(id),"+lid+"(lid).");
			//		console.log(result);
					if(result[0]=="ok") {
						document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
                }
                $(this).dialog("close");
           } 
        },{
            text: "Cancel",
            click: function() {
                $(this).dialog("close");
            }
        }]
    });
    
    $("#div_dialog_restore").dialog({
        modal: true,
        autoOpen: false,
        width: "auto",
        buttons: [{
           text: "Restore",
           click: function(){
				var qid = $(this)[0].firstChild.getAttribute("num");
				var id = parseInt(qid);
				var lid = $("#list_id").val();
				var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Restore","id="+id+"&list_id="+lid);
			if(result[0]=="ok") {
				document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
				//$(this).dialog("close");
           }
        },{
            text: "Cancel",
            click: function() {
                $(this).dialog("close");
            }
        }]
    });
    AssistHelper.formatDialogButtons($("#div_dialog"),0,AssistHelper.getDialogSaveCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getRedCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),2,AssistHelper.getGreenCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getRedCSS());
    AssistHelper.formatDialogButtons($("#div_dialog"),4,AssistHelper.getCloseCSS());
    AssistHelper.formatDialogButtons($("#div_dialog_restore"),0,AssistHelper.getGreenCSS());
    AssistHelper.formatDialogButtons($("#div_dialog_restore"),1,AssistHelper.getCloseCSS());
    
    AssistHelper.hideDialogTitlebar("id", "div_dialog_restore");
    
    $("input[name='btn_add']").click(function() {
		var okay = true;
		
		$("#master_list tr#add_row").find("input:text, textarea, select").removeClass("required").each(function(){
			var require_me = $(this).attr("require_me");
			require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
			if(require_me==true && $(this).val().length==0) {
				okay = false
				$(this).addClass("required");
			}
		});
		
		if($("#list_num").val() == "X" || $("#list_num").val() == "0"){
			okay = false;	
			$("#list_num").addClass("required");
		}
		if(okay != true){
			alert("Please fill in all of the required fields as highlighted.");
		}else{
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_list]")); 
			//console.log(dta);
			var result = AssistHelper.doAjax("inc_controller.php?action=MDOC_LIST.addObject",dta);
			//console.log(result);
		//alert(result);
			if(result[0]=="ok") {
				document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
		//		alert("complete! WP");
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
    });
    
    $("input:button.btn_restore").click(function() {
        var i = $(this).prop("id");
    		
	    $('#div_dialog_restore').html("<p num="+i+" >Are you sure you want to restore item "+i+"?</p>");
	    $('#div_dialog_restore').dialog("open");
    });
    	
    	
    	
    	
    $("input:button.btn_edit").click(function() {
	    AssistHelper.formatDialogButtons($("#div_dialog"),2,AssistHelper.getDisplayCSS("hidden"));
        var i = $(this).prop("id");
        var can_delete = $(this).attr("can_delete"); 
        var sys = Array();
        if($("#tr_"+i+" td.center").attr("sys") == 1){
		    sys[i] = true;
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("hidden"));
		    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("hidden"));
		} else if(can_delete==0) {
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("hidden"));
        }else{
		    AssistHelper.formatDialogButtons($("#div_dialog"),3,AssistHelper.getDisplayCSS("inline"));
		    AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("inline"));
        }
		$("#tr_"+i+" td").each(function(){
			var h = $(this).html();
			var x = $(this).text();
			var f = $(this).attr("fld");
			$("#div_dialog #edit_td_"+f+" p").hide();
			
		//	console.log(f);
			if(f == "id"){
				$('#div_dialog #ref').val(i);	
				$('#div_dialog #edit_td_id').html(i);	
			}else if(sys[i] == true && f != "client_name"){
				$("#div_dialog #edit_"+f).hide();
				$("#div_dialog input.color").hide();
				$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #lbl_edit_"+f).hide();
				$("#div_dialog #edit_td_"+f+" br").hide();
				$("#div_dialog #edit_td_"+f+" p").show();
				$("#div_dialog #edit_td_"+f+" p").html(h);
				if(f == "colour"){
					$("#div_dialog #edit_td_"+f+" p").html("<div style='height:13px; width:100%; padding-bottom:1px; padding-top:1px; background-color:"+$(this).css("background-color")+"'></div>");
				}
			}else if(f == "colour"){
				var this_col_rgb = $(this).css("background-color");
				var this_col = rgb2hex(this_col_rgb);
				$("#div_dialog input.color").css("background-color",this_col);
				$("#div_dialog input.color").show();
				$("#div_dialog #edit_td_"+f+" p").html("<input type='hidden' id='secret_colour' name='colour' value='"+this_col+"' />");
			} else if(f=="list_num") {
				$("#div_dialog #edit_td_"+f+" br").show();
				$("#div_dialog #edit_"+f).show();
				$("#div_dialog #lbl_edit_"+f).show();
				$("#div_dialog #edit_td_"+f+" p").hide();
				//$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #edit_"+f).keyup();
				var x = 0;
				$("#div_dialog #edit_"+f+" option").each(function() {
					if($(this).prop("text")==h) {
						x = $(this).prop("value");
					}
				});
				$("#div_dialog #edit_"+f).val(x);
			}else{
				$("#div_dialog #edit_td_"+f+" br").show();
				$("#div_dialog #edit_"+f).show();
				$("#div_dialog #lbl_edit_"+f).show();
				$("#div_dialog #edit_td_"+f+" p").hide();
				$("#div_dialog #edit_"+f).val(h);
				$("#div_dialog #edit_"+f).keyup();
			}
			
			
			
		}); 
		<?php if ($list_id == "assessment_frequency"){ ?>
			AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getDisplayCSS("inline"));
			AssistHelper.formatDialogButtons($("#div_dialog"),1,AssistHelper.getRedCSS());
		<?php	} ?>
        //for each i, make an array with key=>heading and val=>value, with arbitrary numbers of headings and values.
       //console.log(rows);
	    $("#div_dialog").attr("num",i);
        $("#div_dialog").dialog("open");
	
	//change value of hidden input on colour picker change
    	$("#div_dialog input.color").change(function(){
    		var colour = "#" + $(this).val();
    		$("#secret_colour").val(colour);
    	});    
    });
    	$("#master_list input.color").change(function(){
    		var addcolour = "#" + $(this).val();
    		$("#add_colour").val(addcolour);
    	});    
    
    //Check for population of service provider categories
    //if("<?php echo $list_id;?>" == "contract_supplier" && <?php echo(count($cat_sups)); ?> == 0){
    	//$(".tbl-container").hide();
    	//var error_html = "<p>The following error needs to be attended to before any Service Providers can be added:</p> <ul><li>Supplier Categories need to be set up</li></ul><p>Please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>";
    	//$('#div_error').html(error_html).dialog({modal: true,dialogClass:"dlg_frm_error"}).addClass('ui-corner-all').css({"border":"2px solid #cc0001","background-color":"#fefefe"});
		//AssistHelper.hideDialogTitlebar('id','div_error');
		//AssistHelper.hideDialogCSS('class','dlg_frm_error');
		//$('#div_error #btn_error').button().click(function() {
		//	$('#div_error').dialog("close");
		//}).blur().parent('p').addClass('float');
		//$('html, body').animate({scrollTop:0});
	<?php 
	if(isset($fields['list_num']) && count($cat_sups)==0) {
echo "$(\"#master_list\").find(\"input:button\").each(function() {
			$(this).prop(\"disabled\",true);
		});";
	}
		?>
    //}
    $("input.color").attr("spellcheck", "false");
    

});
</script>
<script type="text/javascript" src="../library/jquery-plugins/jscolor/jscolor.js"></script>
<style>
	.for_display{
		margin:0px;
		padding:0px;
	}
	/*.horscroll { 
        overflow-x: auto;
        overflow-y: hidden;
    	white-space:nowrap;
  } */
</style>