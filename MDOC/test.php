<?php
error_reporting(-1);
$js = "";
require_once ("../module/autoloader.php");


$s = array(
	"/library/js/assist.ui.paging.js",
	"/library/jquery-plugins/jscolor/jscolor.js",
);
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

ASSIST_HELPER::arrPrint($_REQUEST);
ASSIST_HELPER::arrPrint($_FILES);

$displayObject = new ASSIST_MODULE_DISPLAY();

?>
<style type="text/css" >
	div {
		border: 2px dashed #FE7700;
		background-color: #007EFB;
	}
	div.new-div {
		border: 1px solid #000099;
		background-color: #0099FF;
	}
</style>
<h1>Testing centralised document management upload forms</h1>
<hr />
<form name=frm_test action=test.php method=POST language="jscript" enctype="multipart/form-data">
	<p>Attachments: </p>
	<div class=doc_lists></div>
	<p><input type=button value='Add Attachment' id=btn_add /></p>
	<div id=all_doc_divs>
	</div>
		<div class=div_doc></div>
	<p><input type=button value=Save id=btn_save /></p>
</form>


<hr />


<div id=dlg_new_doc>
	<table>
		<tr>
			<th>Title:</th>
			<td><input type=text name=title[] /></td>
		</tr>
		<tr>
			<th>Date:</th>
			<td><input type=text name=ddate[] class='should-be-datepicker' /></td>
		</tr>
		<tr>
			<th>File:</th>
			<td><input type=file name=attach[] class='file-names' /></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	$(function(){
		<?php echo $js; ?>
		var dd = 0;
		
		var attach_html = $("div#dlg_new_doc").html();
		//$("div.div_doc:first").html(attach_html);
		
		$("#btn_add").click(function(e) {
			e.preventDefault();
			if($("div.div_doc:last").html().length==0) {
				$me = $("div.div_doc:last");
			} else {
				$("div.div_doc:last").after($("<div />",{class:"div_doc"}));
				$me = $("div.div_doc:last");
			}
			$me.html(attach_html);
			$me.children("input:text.should-be-datepicker").each(function() {
				$(this).prop("id","dd_"+dd); 
				//$(this).datepicker({changeYear:true,showOn:"both",buttonImage: "/library/jquery/css/calendar.gif"});
			});
			$me.find(".file-names").each(function() {
				$(this).prop("id","df_"+dd);
			});
			//$me.find(".btn-hide").each(function() {
				//$(this).prop("id","bh_"+dd);
			//});
			makeDP();
			bindDoms();
			//$("input:file #df_"+dd).on("click",changeAttach($("input:file #df_"+dd))); 
			$me.dialog({modal:true,buttons:[{text:"Hide",click:function(){
				if($("div#all_doc_divs div").length==0) {
					var nextindex = 0;
				} else {
					var nextindex = $("div#all_doc_divs div:last").index()+1;
				}
				$("div#all_doc_divs").append($("<div />",{id:"doc_"+nextindex,class:"new-div"}));
				$(this).find("input").each(function() {
					$this = $(this);
					$("div#all_doc_divs div:last").append($this);
				});
				//console.log($frm[0].val());
				//$(this).dialog("close");
			}}]});
			dd++;
		});
		//$("#btn_add").trigger("click");
		
		$("#btn_save").click(function(e){
			e.preventDefault();
			
			console.log(AssistForm.serialize($("form[name=frm_test]")));
			//$("form[name=frm_test]").submit();
		});
		
		/*$("input:file").on("change",function() {
		 * 	
		 */
		function changeAttach($me) {
			//console.log('changeAttach'+$me.prop("id"));
			var t = $me.val();
			$("div.doc_lists").append("<p>"+t+" <input type=button value=Edit /></p>");
		};
		
		/*$('input:text').bind('click',".should-be-datepicker", function(){
			$(this).datepicker({changeYear:true,showOn:"both",buttonImage: "/library/jquery/css/calendar.gif"});
			$(this).datepicker("show");
			//alert($(this).prop("id"));
			console.log('click');
		});*/
		function makeDP() {
			$("input:text.should-be-datepicker").datepicker("destroy");
			$("input:text.should-be-datepicker").datepicker({changeYear:true,showOn:"both",buttonImage:"/library/jquery/css/calendar.gif"});
		}
		function bindDoms() {
			$("body").off("change","input:file.file-names");
			//$("body").off("click",".btn-hide");
			$("body").on("change","input:file.file-names",function() {
				var t = $(this).val();
				$("div.doc_lists").append("<p>"+t+" <input type=button value=Edit class=btn-edit /></p>");
				$("div.doc_lists input:button.btn-edit").each(function() {
					var i = $(this).index();
					console.log('edit btn '+i);
					$(this).prop("id","btn_edit_"+i);
				});
			});
			$("body").on("click","div.doc_lists input:button.btn-edit",function() {
				
			});
			//$("body").on("click",".btn-hide",function() {
				//$(this).parent().parent().parent().parent().dialog("close");//hide();
			//});
		}
	});
</script>