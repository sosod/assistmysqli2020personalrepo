<?php
    require 'inc_head.php';
$levels_of_children = 5;
if(isset($_REQUEST['r'])) { $result = $_REQUEST['r']; $result[1] = stripslashes($result[1]); }
	
$users = array();
$viewers = array();
$nonview = array();
$sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname, l.view FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u, ".$dbref."_list_users l ";
$sql.= "WHERE t.tkid = u.usrtkid AND u.usrmodref = '".$tref."' AND t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid = l.tkid AND l.yn = 'Y' ";
$sql.= " ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    $u = 0;
    $v = 0;
    $n = 0;
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            $users[$id] = array($id,$val);
            //$u++;
            if($row['view']=="Y") {
                $viewers[$id] = array($id,$val);
               // $v++;
            } else {
                $nonview[$id] = array($id,$val);
               // $n++;
            }
        }
    mysql_close($con);
//    print_r($users);
if(count($users)>10) { $sc = 10; } else { $sc = count($users); }
if(count($nonview)>10) { $nv = 10; } else { $nv = count($nonview); }

?>
<style type=text/css>
table th { vertical-align: top; }
table tr { height: 30px; }
table th.disabled { background-color: #999999; }
</style>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> Create Category</b></h1>
<?php if(isset($result)) { displayResult($result); } ?>
<form name=addcate method=post action=admin_create_process.php>
<?php
/*foreach($users as $vr)
{
    echo("<input type=hidden name=cateview[] value='".$vr[0]."'>");
    echo("<input type=hidden name=cateupload[] value='".$vr[0]."'>");
}*/

?>
<h2>Category Details</h2>
<table width=600><tbody>
    <tr height=30>
        <th width=150 style="text-align:left;">Category Name:</th>
        <td><input type=text name=catetitle size=50 maxlength=200 id=ct></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category Structure:</th>
        <td><div id=child class=b><span id=spn_parent>Parent: <select name=catesubid[0] id=parent>
            <option selected value=0>None</option>
<?php
    $sql = "SELECT * FROM ".$dbref."_categories WHERE catesubid = 0 AND cateyn = 'Y' ORDER BY catetitle";
    $categories = mysql_fetch_all($sql);
        foreach($categories as $row)
        {
            $id = $row['cateid'];
            $val = $row['catetitle'];
            echo("<option value='$id'>$val</option>");
        }
    mysql_close($con);
?>
        </select></span>
		<?php for($i=1;$i<=$levels_of_children;$i++) {
			echo "
				<span id=spn_child_".$i." class=spn_child><br />Level ".$i.": <select name=catesubid[$i] id=child_".$i." class=child level=$i ><option selected value=X>None</option></select></span>
			";
		} ?>
		</div><span id=sub_text class=i style="font-size: 7pt;"><span class=iinform>Please note:</span> View Access (below) can only be set for categories without a parent category.  Any sub-category will automatically inherit its View Access settings from the parent category.</span>
		</td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category owner:</th>
        <td><?php //print_r($users); echo(count($users)); ?><select name=cateowner>
<?php
//for($u=0;$u<count($users);$u++)
foreach($users as $u)
        {
            /*$id = $users[$u][0];
            $val = $users[$u][1];*/
			$id = $u[0];
			$val = $u[1];
            if($id == $tkid) { $sel = "selected"; } else { $sel = ""; }
            echo("<option $sel value='$id'>$val</option>");
        }
?>
        </select></td>
    </tr>
	</tbody></table>
<h2 style="margin-bottom: -10px;">User Access</h2>
<p><span class=iinform>Please Note:</span> <span id=note>Category Owner automatically receives complete access.</span></p>
	<table width=600 id=user_access ><tbody>
	<tr id=tr_auto>
		<th id=tr_auto width=150 class="left">Automatic View Access:</th>
		<td colspan=2><?php
				if(count($viewers)>0) {
					echo "<ul id=ul_view>";
					foreach($viewers as $v) {
						echo "<li style=\"color: #000000;\">".$v[1]."<input type=hidden name=view[] value=".$v[0]." /></li>";
					}
					echo "</ul>";
				} 
		?></td>
	</tr>
	<tr id=tr_view>
		<th id=th_view class="left">View Access:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td id=td_view style="border-right: 0px;"><?php
			if(count($nonview) > 0) {
				echo "	<select size=".$sc." multiple name=view[] id=sel_view>";
				foreach($nonview as $n) {
					echo "<option value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can view the category and<br />all associated documents.</td>
	</tr>
	<tr>
		<th class="left">Maintain Documents:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td style="border-right: 0px;"><?php
			if(count($users) > 0) {
				echo "	<select size=".$sc." multiple name=maintain[] id=sel_maintain>";
				foreach($users as $n) {
					echo "<option value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can access the category under<br />Admin and upload, edit & delete<br />documents within the category.</td>
	</tr>
	<tr>
		<th width=150 class="left">Upload Documents<br />/ Add Comments only:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td style="border-right: 0px;"><?php
			if(count($users) > 0) {
				echo "	<select size=".$sc." multiple name=upload[] id=sel_upload>";
				foreach($users as $n) {
					echo "<option value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can only upload<br />documents or add comments<br />to the category.</td>
	</tr>
	</tbody></table>
	<table width=600 style="margin-top: 20px"><tbody>
    <tr height=30>
        <td class=center><input type=submit value=Create class=isubmit> <input type=reset></th>
    </tr>
</tbody></table>
</form>
<script type=text/javascript>
function cateAjax(id,fld) {
		$.ajax({                                      
		  url: 'inc_ajax.php', 		  type: 'POST',		  data: "i="+id+"&act=CATE",		  dataType: 'json', 
		  success: function(rows) {
			for(var i in rows) {
				var row = rows[i];
				$(fld).append("<option value="+row['cateid']+" cc="+row['cc']+">"+row['catetitle']+"</option>");
			}
		  } 
		});
}
	var levels_of_children = <?php echo $levels_of_children; ?>;
$(function() {
		$("#child .spn_child").hide();
		$("#child .sub_text").css("font-weight","normal");
		$("#ct").focus();
		$("#parent").change(function() {
			var parent = $(this).val();
			if(parseInt(parent)==0) {	//NO PARENT = MAIN CATEGORY
				$("#user_access #td_view, #ul_view li").each(function() { 
					$(this).css("color","#000000");
				} );
				$("#user_access #sel_view").each(function() { 
					$(this).attr("disabled",""); 
				} );
				$("#user_access #th_view, #user_access #th_auto").each(function() { 
					$(this).removeClass("disabled"); 
				} );
				$("#user_access #tr_view, #user_access #tr_auto").each(function() { 
					$(this).show(); 
				} );
				//$(".child").hide();
				$("#child .child").empty();
				$("#child .child").append("<option selected value=X cc=0>None</option>");
				$("#child .spn_child").hide();
			} else {					//PARENT = COPY USER ACCESS
				$("#user_access #td_view, #ul_view li").each(function() { 
					$(this).css("color","#ababab");
				} );
				$("#user_access #sel_view").each(function() { $(this).attr("disabled","true"); } );
				$("#user_access #th_view, #user_access #th_auto").each(function() { 
					$(this).addClass("disabled"); 
				} );
				$("#user_access #tr_view, #user_access #tr_auto").each(function() { 
					$(this).hide(); 
				} );
				//$("#child #spn_child_1").show();
				//$("#child #child_1").empty();
				//$("#child #child_1").append("<option selected value=X cc=0>None</option>");
				//cateAjax(parent,"#child_1");
				changeChild(1,1,parent);
			}
		});
		$("#child .child").change(function() {
			var cc = $(this).children("option:selected").attr("cc");
			var l = $(this).attr("level");
			l++;
			var v = $(this).val();
			changeChild(cc,l,v);
		});
	});
	function changeChild(cc,l,v) {
			if(l<=levels_of_children) {
				//clear any existing child selects
				for(var i = l; i<=levels_of_children; i++) {
					$("#child #child_"+i).empty();
					$("#child #child_"+i).append("<option selected value=X cc=0>None</option>");
					$("#child #spn_child_"+i).hide();
				}
				//display next level
				$("#child #spn_child_"+l).show();
			}	
			//if current level has subs, use ajax to get subs
			if(cc>0 && v!="X") {
				cateAjax(v,"#child_"+l);
			}
	}
</script>
</body>
</html>
