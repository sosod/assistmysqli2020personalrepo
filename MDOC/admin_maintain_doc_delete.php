<?php
    require 'inc_head.php';
?>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> <a href=admin_maintain.php class=breadcrumb>Maintain</a> >> Delete a Document</b></h1>
<?php
$variables = $_REQUEST;
$docid = $variables['d'];
if(checkIntRef($docid))
{
	$sql = "SELECT * FROM ".$dbref."_content WHERE docid = $docid";
	$doc = mysql_fetch_one($sql);
    $urlback = "admin_maintain_doc.php?c=".$doc['doccateid'];

    $sql = "UPDATE ".$dbref."_content SET docyn = 'N' WHERE docid = $docid ";
    $mar = db_update($sql);
    if($mar>0) {
        logAct("Deleted document $docid",$sql,"DOC",$docid);
        
	checkFolder("deleted");

        $old = $doc['doclocation'];
        $loc = strFn("explode",$old,"/","");
        $new = "/files/".$cmpcode."/deleted/deleted_".date("YmdHis")."_".$loc[count($loc)-1];
        rename("..".$old,"..".$new);
        
        echo("<h3>Success!</h3>");
        echo("<p>Document $docid has been successfully deleted.</p>");
		echo "<script type=text/javascript>
				document.location.href = '".$urlback."&r[]=ok&r[]=Document $docid has been successfully deleted.';
				</script>";
        include("inc_goback.php");
    } else {
        //die("<p>An error has occurred.  Please go back and try again.</p>");
				echo "<script type=text/javascript>
				document.location.href = '".$urlback."&r[]=error&r[]=Document $docid could not be found to be deleted.';
				</script>";

    }
} else {
		echo "<script type=text/javascript>
				document.location.href = 'admin_maintain.php?r[]=error&r[]=The document to be deleted could not be identified.  Please try again.';
				</script>";
    //die("An error has occurred.  Please go back and try again.");
}
?>
</body>
</html>
