<?php
    require 'inc_head.php';
?>
<h1 class=fc><b><?php echo($moduletitle); ?>: Admin ~ Edit a document</b></h1>
<?php
set_time_limit(1800);
$step = 1;
    $variables = $_REQUEST;
    $docid = $variables['docid'];
    $doctitle = code($variables['doctitle']);
    $doccontents = code($variables['doccontents']);
    $docdate = $variables['docdate'];
    $doccateid = $variables['doccateid'];
    $doctypeid = $variables['doctypeid'];

    //PHP Validation check
    if(checkIntRef($docid) && strlen($doctitle)>0 && checkIntRef($doccateid) && checkIntRef($doctypeid) && strlen($docdate)>0) {
        //format docdate
        $docdt1 = strFn("explode",$docdate,"/","");
        $docdt = mktime(12,0,0,$docdt1[1],$docdt1[2],$docdt1[0]);
        //first sql load
        $sql = "UPDATE ".$dbref."_content SET doctitle = '$doctitle', doccontent = '$doccontents', doccateid = $doccateid, docdate = $docdt, doctypeid = $doctypeid, docmoduser = '$tkid',docmoddate = $today WHERE docid = $docid ";
        include("inc_db_con.php");
        //display result
        $result = array("check","Success!  Your document has been successfully updated.");
        logAct("Edited document $docid",$sql,"DOC",$docid);
displayResult($result);
$urlback = "admin_maintain_doc.php?c=".$doccateid;
include("inc_goback.php");
    } else {
        die("<h2>Error</h2><p>An error has occurred - some form data appears to be missing.  Please go back and try again.</p>");
    }


?>
</body>
</html>
