<?php
$result = array();
$act = $_GET['a'];

switch($act)
{
	case "access_edit":
		$result = $_REQUEST['r'];
		break;
    case "access":
        $res = $_GET['r'];
        $start = $_GET['s'];
        $result[1] = $res;
        if(strFn("substr",$res,0,5)=="ERROR")
        {
            $result[0] = "error";
        }
        else
        {
            $result[0] = "check";
        }
        break;
    case "admin":
        $act2 = $_GET['act'];
        $dirid = $_GET['dirid'];
        $dir = $_GET['dir'];
        $tid = $_GET['tkid'];
        $result = saveDirAdmin($act2,$dir,$tid);
        break;
    case "default":
        $ref = $_GET['r'];
        $val = $_GET['v'];
        $result = saveDefault($ref,$val);
        break;
    case "lists":
        $act2 = $_GET['act'];
        $result = saveList($act2);
        break;
    case "modadmin":
        $val = $_GET['v'];
        $result = saveModAdmin($tkid);
        break;
    case "time":
        $t = $_GET['t'];
        $act2 = $_GET['act'];
        $result = saveTime($act2,$t);
        break;
    case "towns":
        $val = $_GET['w'];
        $act2 = $_GET['act'];
        $result = saveTown($act2,$val);
        break;
    case "wards":
        $val = $_GET['w'];
        $act2 = $_GET['act'];
        $result = saveWard($act2,$val);
        break;
}

function saveDefault($r,$v) {
    global $cmpcode;
    global $modref;
    global $setup;
    $ref = strFn('explode',$r,"_","");
    if(strlen($v)>0 && is_numeric($ref[1]))
    {
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '$v' WHERE ref = '$modref' AND refid = ".$ref[1];
        include("inc_db_con.php");
        $res[1] = "Updated '".$setup[$ref[1]][1]."' successfully.";
        $res[0] = "check";
        
        $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
        $lsql = $sql;
        logAct("S_DEF",0,"Y",$action,$lsql);
    }
    else
    {
        $res[1] = "Error! Please try again.";
        $res[0] = "error";
    }
    return $res;
}

function saveModAdmin($v) {
    global $cmpcode;
    global $modref;
    global $setup;
    $ref = 0;
    if(strlen($v)>0 && is_numeric($ref))
    {
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '$v' WHERE ref = '$modref' AND refid = ".$ref;
        include("inc_db_con.php");
        $res[1] = "Updated '".$setup[0][1]."' successfully.";
        $res[0] = "check";

        $action = "Updated '".$setup[0][1]."' to $v.";
        $lsql = $sql;
        logAct("S_MODADM",0,"N",$action,$lsql);
    }
    else
    {
        $res[1] = "Error! Please try again.";
        $res[0] = "error";
    }
    return $res;
}

function saveDirAdmin($act,$dir,$tid) {
    global $cmpcode;
    global $dbref;
    global $modref;
    switch($act)
    {
        case "add":
            $did = array();
            $did = strFn("explode",$dir,"_","");
            if(strlen($did[0])==1 && is_numeric($did[1]) && strlen($tid)>3)
            {
                $sql = "SELECT * FROM ".$dbref."_list_admins WHERE tkid = '$tid' AND yn = 'Y' AND type = '".$did[0]."' AND ref = ".$did[1];
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                mysql_close($con);
                if($mnr==0)
                {
                    $sql = "INSERT INTO ".$dbref."_list_admins (tkid,yn,type,ref) VALUES ('$tid','Y','".$did[0]."',".$did[1].")";
                    include("inc_db_con.php");
                    $mar = mysql_affected_rows($con);
                    if($mar > 0)
                    {
                        $res[0] = "check";
                        $res[1] = "Administrator added successfully.";
                        //SETUP LOG VALUES
                                    if($did[0]=="S")
                                    {
                                        $sql = "SELECT subdirid, subtxt FROM assist_".$cmpcode."_list_dirsub WHERE subid = ".$did[1];
                                        include("inc_db_con.php");
                                        $l = mysql_fetch_array($rs);
                                        mysql_close($con);
                                        $refd = $l['subdirid'];
                                        $refname = $l['subtxt'];
                                    }
                                    else
                                    {
                                        $sql = "SELECT dirtxt FROM assist_".$cmpcode."_list_dir WHERE dirid = ".$did[1];
                                        include("inc_db_con.php");
                                        $l = mysql_fetch_array($rs);
                                        mysql_close($con);
                                        $refd = $did[1];
                                        $refname = $l['dirtxt'];
                                    }
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$tid'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);
                        
                                    $action = "Added ".$lt['uname']." as an administrator to $refname.";
                                    $lsql = $sql;
                                    logAct("S_DIR",$refd,"Y",$action,$lsql);
                    }
                    else
                    {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                    }
                }
                else
                {
                    $res[0] = "error";
                    $res[1] = "Administrator selected already exists for the Directorate/Sub-directorate selected.";
                }
            }
            else
            {
                $res[1] = "Error! Please try again.";
                $res[0] = "error";
            }
            break;
        case "del":
            if(strlen($dir)>0 && is_numeric($dir))
            {
                $sql = "UPDATE ".$dbref."_list_admins SET yn = 'N' WHERE id = $dir";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                    $res[0] = "check";
                    $res[1] = "Administrator deleted successfully.";
                        //SETUP LOG VALUES
                                    $sql = "SELECT * FROM ".$dbref."_list_admins WHERE id = $dir";
                                    include("inc_db_con.php");
                                        $adm = mysql_fetch_array($rs);
                                    mysql_close($con);
                                    if($adm['type']=="S")
                                    {
                                        $sql = "SELECT subdirid, subtxt FROM assist_".$cmpcode."_list_dirsub WHERE subid = ".$adm['ref'];
                                        include("inc_db_con.php");
                                        $l = mysql_fetch_array($rs);
                                        mysql_close($con);
                                        $refd = $l['subdirid'];
                                        $refname = $l['subtxt'];
                                    }
                                    else
                                    {
                                        $sql = "SELECT dirtxt FROM assist_".$cmpcode."_list_dir WHERE dirid = ".$adm['ref'];
                                        include("inc_db_con.php");
                                        $l = mysql_fetch_array($rs);
                                        mysql_close($con);
                                        $refd = $adm['ref'];
                                        $refname = $l['dirtxt'];
                                    }
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$adm['tkid']."'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);

                                    $action = "Deleted ".$lt['uname']." as an administrator from $refname.";
                                    $lsql = $sql;
                                    logAct("S_DIR",$refd,"Y",$action,$lsql);
                }
                else
                {
                    $res[1] = "Error! Please try again.";
                    $res[0] = "error";
                }
            }
            else
            {
                $res[1] = "Error! Please try again.";
                $res[0] = "error";
            }
            break;
        default:
            break;
    }
    return $res;
}

function saveWard($act,$val) {
    global $cmpcode;
    global $dbref;
    global $setup;
    $val = code($val);
    switch($act)
    {
        case "add":
            if(strlen($val)>0)
            {
                $sql = "SELECT count(id) as cid FROM ".$dbref."_list_wards WHERE value = '$val' AND yn = 'Y'";
                include("inc_db_con.php");
                    $count = mysql_fetch_array($rs);
                mysql_close($con);
                if($count['cid']==0)
                {
                    $sql = "INSERT INTO ".$dbref."_list_wards (value,yn) VALUES ('$val','Y')";
                    include("inc_db_con.php");
                    $wid = mysql_insert_id($con);
                    $mar = mysql_affected_rows($con);
                    if($mar>0)
                    {
                            $res[0] = "check";
                            $res[1] = "'$val' added successfully.";
                            //LOG
                        $action = "Added $val as a new ".strtolower(strFn("substr",$setup[3][0],0,-1)).".";
                        $lsql = $sql;
                        logAct("S_WARDS",$wid,"Y",$action,$lsql);
                        $action = "Added $val (ref: $wid) as a new ".strtolower(strFn("substr",$setup[3][0],0,-1)).".";
                        $lsql = $sql;
                        logAct("S_WARDS",0,"Y",$action,$lsql);
                    }
                    else
                    {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                    }
                }
                else
                {
                        $res[1] = "Error! '$val' already exists.";
                        $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! Please try again.";
                    $res[0] = "error";
            }
            break;
        case "addu":
            $ref = $_GET['i'];
            $admins = $_GET['ad'];
            if(count($admins)>0)
            {
                $val = array_value_delete($admins,"X");
                if(count($val)>0)
                {
                    $mar = 0;
                    foreach($val as $v)
                    {
                        if(strlen($v)>0)
                        {
                            while(strlen($v)<4)
                            {
                                $v = "0".$v;
                            }
                            $sql = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) VALUES ('$v', 'Y', 'W', $ref)";
                            include("inc_db_con.php");
                            $mar1 = mysql_affected_rows($con);
                            $mar = $mar + $mar1;
                                if($mar1>0)
                                {
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$v'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);

                                    $action = "Added ".$lt['uname']." as an administrator.";
                                    $lsql = $sql;
                                    logAct("S_WARDS",$ref,"Y",$action,$lsql);
                                }
                        }
                    }
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "New users added successfully.";
                        }
                        else
                        {
                            $res[1] = "Error! Please try again.";
                            $res[0] = "error";
                        }
                }
                else
                {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
            }
            break;
        case "del":
            if(strlen($val)>0 && is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_wards SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = strFn("substr",$setup[3][0],0,-1)." deleted successfully.";
                            //LOG
                            $sql = "SELECT * FROM ".$dbref."_list_wards WHERE id = $val";
                            include("inc_db_con.php");
                                $lw = mysql_fetch_array($rs);
                            mysql_close($con);
                        $action = "Deleted ".strtolower(strFn("substr",$setup[3][0],0,-1)).".";
                        $lsql = $sql;
                        logAct("S_WARDS",$val,"Y",$action,$lsql);
                        $action = "Deleted ".strtolower(strFn("substr",$setup[3][0],0,-1))." '".$lw['value']."' (ref: $val).";
                        $lsql = $sql;
                        logAct("S_WARDS",0,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "delu":
            $ref = $_GET['i'];
            if(is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_admins SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "User deleted successfully.";
                                //LOG
                                $sql = "SELECT * FROM ".$dbref."_list_admins WHERE id = $val";
                                include("inc_db_con.php");
                                    $lw = mysql_fetch_array($rs);
                                mysql_close($con);
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$lw['tkid']."'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);

                                    $action = "Deleted ".$lt['uname']." as an administrator.";
                                    $lsql = $sql;
                                    logAct("S_WARDS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "edit":
            $ref = $_GET['i'];
            $val = code($val);
            if(strlen($val)>0 && strlen($ref)>0 && is_numeric($ref))
            {
                            $sql = "SELECT * FROM ".$dbref."_list_wards WHERE id = $ref";
                            include("inc_db_con.php");
                                $lw = mysql_fetch_array($rs);
                            mysql_close($con);
                $sql = "UPDATE ".$dbref."_list_wards SET value = '$val' WHERE id = $ref";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "'$val' updated successfully.";
                    $action = "Renamed '".$lw['value']."' to '$val'.";
                    $lsql = $sql;
                    logAct("S_WARDS",$ref,"Y",$action,$lsql);
                    $action = "Renamed '".$lw['value']."' to '$val' (ref: $ref).";
                    $lsql = $sql;
                    logAct("S_WARDS",0,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
    }
    return $res;
}

function saveTown($act,$val) {
    global $cmpcode;
    global $dbref;
    global $setup;
    $val = code($val);
/*    switch($act)
    {
        case "add":
            if(strlen($val)>0)
            {
                $sql = "SELECT count(id) as cid FROM ".$dbref."_list_towns WHERE value = '$val' AND yn = 'Y'";
                include("inc_db_con.php");
                    $count = mysql_fetch_array($rs);
                mysql_close($con);
                if($count['cid']==0)
                {
                    $sql = "INSERT INTO ".$dbref."_list_towns (value,yn) VALUES ('$val','Y')";
                    include("inc_db_con.php");
                    $mar = mysql_affected_rows($con);
                    if($mar>0)
                    {
                            $res[0] = "check";
                            $res[1] = "'$val' added successfully.";
//                    $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
//                    $lsql = $sql;
//                    logAct("S_DIR",$ref[1],"Y",$action,$lsql);
                    }
                    else
                    {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                    }
                }
                else
                {
                        $res[1] = "Error! '$val' already exists.";
                        $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! Please try again.";
                    $res[0] = "error";
            }
            break;
        case "addu":
            $ref = $_GET['i'];
            $admins = $_GET['ad'];
            if(count($admins)>0)
            {
                $val = array_value_delete($admins,"X");
                if(count($val)>0)
                {
                    $mar = 0;
                    foreach($val as $v)
                    {
                        if(strlen($v)>0)
                        {
                            while(strlen($v)<4)
                            {
                                $v = "0".$v;
                            }
                            $sql = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) VALUES ('$v', 'Y', 'T', $ref)";
                            include("inc_db_con.php");
                            $mar = $mar + mysql_affected_rows($con);
//                    $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
//                    $lsql = $sql;
//                    logAct("S_DIR",$ref[1],"Y",$action,$lsql);
                        }
                    }
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "New users added successfully.";
                        }
                        else
                        {
                            $res[1] = "Error! Please try again.";
                            $res[0] = "error";
                        }
                }
                else
                {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
            }
            break;
        case "del":
            if(strlen($val)>0 && is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_towns SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "Towns deleted successfully.";
//                    $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
//                    $lsql = $sql;
//                    logAct("S_DIR",$ref[1],"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "delu":
            if(is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_admins SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "User deleted successfully.";
//                    $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
//                    $lsql = $sql;
//                    logAct("S_DIR",$ref[1],"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "edit":
            $ref = $_GET['i'];
            $val = code($val);
            if(strlen($val)>0 && strlen($ref)>0 && is_numeric($ref))
            {
                $sql = "UPDATE ".$dbref."_list_towns SET value = '$val' WHERE id = $ref";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "'$val' updated successfully.";
//                    $action = "Updated '".$setup[$ref[1]][1]."' to $v.";
//                    $lsql = $sql;
//                    logAct("S_DIR",$ref[1],"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
    } */
    switch($act)
    {
        case "add":
            if(strlen($val)>0)
            {
                $sql = "SELECT count(id) as cid FROM ".$dbref."_list_towns WHERE value = '$val' AND yn = 'Y'";
                include("inc_db_con.php");
                    $count = mysql_fetch_array($rs);
                mysql_close($con);
                if($count['cid']==0)
                {
                    $sql = "INSERT INTO ".$dbref."_list_towns (value,yn) VALUES ('$val','Y')";
                    include("inc_db_con.php");
                    $wid = mysql_insert_id($con);
                    $mar = mysql_affected_rows($con);
                    if($mar>0)
                    {
                            $res[0] = "check";
                            $res[1] = "'$val' added successfully.";
                            //LOG
                        $action = "Added $val as a new town.";
                        $lsql = $sql;
                        logAct("S_TOWNS",$wid,"Y",$action,$lsql);
                        $action = "Added $val (ref: $wid) as a new town.";
                        $lsql = $sql;
                        logAct("S_TOWNS",0,"Y",$action,$lsql);
                    }
                    else
                    {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                    }
                }
                else
                {
                        $res[1] = "Error! '$val' already exists.";
                        $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! Please try again.";
                    $res[0] = "error";
            }
            break;
        case "addu":
            $ref = $_GET['i'];
            $admins = $_GET['ad'];
            if(count($admins)>0)
            {
                $val = array_value_delete($admins,"X");
                if(count($val)>0)
                {
                    $mar = 0;
                    foreach($val as $v)
                    {
                        if(strlen($v)>0)
                        {
                            while(strlen($v)<4)
                            {
                                $v = "0".$v;
                            }
                            $sql = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) VALUES ('$v', 'Y', 'T', $ref)";
                            include("inc_db_con.php");
                            $mar1 = mysql_affected_rows($con);
                            $mar = $mar + $mar1;
                                if($mar1>0)
                                {
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '$v'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);

                                    $action = "Added ".$lt['uname']." as an administrator.";
                                    $lsql = $sql;
                                    logAct("S_TOWNS",$ref,"Y",$action,$lsql);
                                }
                        }
                    }
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "New users added successfully.";
                        }
                        else
                        {
                            $res[1] = "Error! Please try again.";
                            $res[0] = "error";
                        }
                }
                else
                {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
                }
            }
            else
            {
                    $res[1] = "Error! No users selected.  Please try again.";
                    $res[0] = "error";
            }
            break;
        case "del":
            if(strlen($val)>0 && is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_towns SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = strFn("substr",$setup[3][0],0,-1)." deleted successfully.";
                            //LOG
                            $sql = "SELECT * FROM ".$dbref."_list_towns WHERE id = $val";
                            include("inc_db_con.php");
                                $lw = mysql_fetch_array($rs);
                            mysql_close($con);
                        $action = "Deleted town.";
                        $lsql = $sql;
                        logAct("S_TOWNS",$val,"Y",$action,$lsql);
                        $action = "Deleted town '".$lw['value']."' (ref: $val).";
                        $lsql = $sql;
                        logAct("S_TOWNS",0,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "delu":
            $ref = $_GET['i'];
            if(is_numeric($val))
            {
                $sql = "UPDATE ".$dbref."_list_admins SET yn = 'N' WHERE id = $val";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "User deleted successfully.";
                                //LOG
                                $sql = "SELECT * FROM ".$dbref."_list_admins WHERE id = $val";
                                include("inc_db_con.php");
                                    $lw = mysql_fetch_array($rs);
                                mysql_close($con);
                                    $sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as uname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$lw['tkid']."'";
                                    include("inc_db_con.php");
                                    $lt = mysql_fetch_array($rs);
                                    mysql_close($con);

                                    $action = "Deleted ".$lt['uname']." as an administrator.";
                                    $lsql = $sql;
                                    logAct("S_TOWNS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "edit":
            $ref = $_GET['i'];
            $val = code($val);
            if(strlen($val)>0 && strlen($ref)>0 && is_numeric($ref))
            {
                            $sql = "SELECT * FROM ".$dbref."_list_towns WHERE id = $ref";
                            include("inc_db_con.php");
                                $lw = mysql_fetch_array($rs);
                            mysql_close($con);
                $sql = "UPDATE ".$dbref."_list_towns SET value = '$val' WHERE id = $ref";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "'$val' updated successfully.";
                    $action = "Renamed '".$lw['value']."' to '$val'.";
                    $lsql = $sql;
                    logAct("S_TOWNS",$ref,"Y",$action,$lsql);
                    $action = "Renamed '".$lw['value']."' to '$val' (ref: $ref).";
                    $lsql = $sql;
                    logAct("S_TOWNS",0,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
    }

    return $res;
}

function saveTime($act,$t) {
    global $cmpcode;
    global $dbref;
    global $today;
    switch($act)
    {
        case "auto":
            $r = $_GET['r'];
            $c = $_GET['c'];
            if(is_numeric($r) && $r>0)
            {
                $rr = $r;
                $r = ($r * 86400) + 3600;
                $r = "eval + $r";
            }
            else
            {
                $rr = 0;
                $r = 0;
            }
            if(is_numeric($c) && $c>0)
            {
                $cc = $c;
                $c = ($c * 86400) + 3600;
                $c = "eval + $c";
            }
            else
            {
                $cc = 0;
                $c = 0;
            }
            $sql = "UPDATE ".$dbref."_list_time SET aclose = ( $c ), arem = ( $r ) WHERE active = 'Y'";
            include("inc_db_con.php");
            $mar = mysql_affected_rows($con);
            if($mar>0)
            {
                $res[0] = "check";
                $res[1] = "Time periods updated successfully.";
                    $action = "Updated auto reminder ($rr days) and auto closure events ($cc days).";
                    $lsql = $sql;
                    logAct("S_TIME",0,"Y",$action,$lsql);
            }
            else
            {
                $res[0] = "error";
                $res[1] = "Error!  No time periods were updated.";
            }
            break;
        case "close":
            $res[0] = "check";
            $res[1] = "close $t";
            if(is_numeric($t))
            {
                $sql = "UPDATE ".$dbref."_list_time SET active = 'N' WHERE id = ".$t;
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                    $res[1] = "Period $t closed successfully.";
                    $res[0] = "check";
                        //LOG
                        $sql = "SELECT eval FROM ".$dbref."_list_time WHERE id = ".$t;
                        include("inc_db_con.php");
                        $lt = mysql_fetch_array($rs);
                        mysql_close($con);
                        $action = "Closed time period $t (".date("M Y",$lt['eval']).").";
                        $lsql = $sql;
                        logAct("S_TIME",0,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "save":
            $arem = $_GET['arem'];  //dd-mm-yyyy
            $aclose = $_GET['aclose'];  //dd-mm-yyyy
            if(is_numeric($t))
            {
                if(strlen($aclose)>0)
                {
                    $ac = explode("-",$aclose);
                    $aclose = mktime(1,0,0,$ac[1],$ac[0],$ac[2]);
                    if($aclose < $today) { $aclose = 0; }
                }
                else
                {
                    $aclose = 0;
                }
                if(strlen($arem)>0)
                {
                    $ar = explode("-",$arem);
                    $arem = mktime(1,0,0,$ar[1],$ar[0],$ar[2]);
                    if($arem < $today) { $arem = 0; }
                }
                else
                {
                    $arem = 0;
                }
                $sql = "UPDATE ".$dbref."_list_time SET aclose = ".$aclose.", arem = ".$arem." WHERE id = ".$t;
                include("inc_db_con.php");
                $res[1] = "Update completed successfully.";
                $res[0] = "check";
                        //LOG
                        $sql = "SELECT eval FROM ".$dbref."_list_time WHERE id = ".$t;
                        include("inc_db_con.php");
                        $lt = mysql_fetch_array($rs);
                        mysql_close($con);
                        $action = "Updated auto reminder (";
                        if($arem>0) { $action.= date("d M Y",$arem); } else { $action.= "N/A"; }
                        $action.= ") and auto closure (";
                        if($aclose>0) { $action.= date("d M Y",$aclose); } else { $action.= "N/A"; }
                        $action.= ") events for time period $t (".date("M Y",$lt['eval']).").";
                        $lsql = $sql;
                        logAct("S_TIME",0,"Y",$action,$lsql);
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
    }
    return $res;
}

function saveList($act) {
    global $cmpcode;
    global $dbref;
    switch($act)
    {
        case "add":
            $val = $_GET['list'];
            if(strlen($val)>0)
            {
                $val = code($val);
                $sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE value = '$val' AND std = 'N' AND yn = 'Y'";
                include("inc_db_con.php");
                $mnr = mysql_num_rows($rs);
                mysql_close($con);
                if($mnr>0)
                {
                            $res[1] = "Error! List already exists.";
                            $res[0] = "error";
                }
                else
                {
                    $sql = "INSERT INTO ".$dbref."_list_dropdowns (value,yn,std) VALUES ('$val','Y','N')";
                    include("inc_db_con.php");
                    $mid = mysql_insert_id($con);
                    $mar = mysql_affected_rows($con);
                    if($mar>0)
                    {
                            $res[1] = "$val added successfully.";
                            $res[0] = "check";
                            $action = "Created new list '$val' (ref: $mid).";
                            $lsql = $sql;
                            logAct("S_LISTS",0,"Y",$action,$lsql);
                            logAct("S_LISTS",$mid,"Y",$action,$lsql);
                    }
                    else
                    {
                            $res[1] = "Error! Please try again.";
                            $res[0] = "error";
                    }
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "addv":    //add list value
            $ref = $_GET['i'];
            $val = $_GET['listv'];
            if(strlen($val)>0)
            {
                $val = code($val);
                $sql = "SELECT * FROM ".$dbref."_list_dropdowns_values WHERE value = '$val' AND listid = $ref AND yn = 'Y'";
                include("inc_db_con.php");
                $mnr = mysql_num_rows($rs);
                mysql_close($con);
                if($mnr>0)
                {
                            $res[1] = "Error! Value already exists.";
                            $res[0] = "error";
                }
                else
                {
                    $sql = "INSERT INTO ".$dbref."_list_dropdowns_values (value,yn,listid) VALUES ('$val','Y',$ref)";
                    include("inc_db_con.php");
                    $mid = mysql_insert_id($con);
                    $mar = mysql_affected_rows($con);
                    if($mar>0)
                    {
                            $res[1] = "$val added successfully.";
                            $res[0] = "check";
                        //LOG
                            $action = "Added new value '$val' (ref: $mid).";
                            $lsql = $sql;
                            logAct("S_LISTS",$ref,"Y",$action,$lsql);
                    }
                    else
                    {
                            $res[1] = "Error! Please try again.";
                            $res[0] = "error";
                    }
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "del": //delete list
            $ref = $_GET['i'];
            if(strlen($ref)>0 && is_numeric($ref))
            {
                        $sql = "SELECT value FROM ".$dbref."_list_dropdowns WHERE id = $ref";
                        include("inc_db_con.php");
                        $lv = mysql_fetch_array($rs);
                        mysql_close($con);
                $sql = "UPDATE ".$dbref."_list_dropdowns SET yn = 'N' WHERE id = $ref";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "List deleted successfully.";
                    //LOG
                            $action = "Deleted list '".$lv['value']."' (ref: $ref).";
                            $lsql = $sql;
                            logAct("S_LISTS",0,"Y",$action,$lsql);
                            logAct("S_LISTS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "delv":    //delete list value
            $ref = $_GET['i'];
            $refv = $_GET['v'];
            if(strlen($ref)>0 && is_numeric($ref) && strlen($refv)>0 && is_numeric($refv))
            {
                    $sql = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = $refv";
                    include("inc_db_con.php");
                    $lv = mysql_fetch_array($rs);
                    mysql_close($con);
                $sql = "UPDATE ".$dbref."_list_dropdowns_values SET yn = 'N' WHERE id = $refv";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "List value deleted successfully.";
                        //LOG
                            $action = "Deleted value '".$lv['value']."' (ref: $refv).";
                            $lsql = $sql;
                            logAct("S_LISTS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
            break;
        case "edit":    //edit list
            $ref = $_GET['i'];
            $val = $_GET['v'];
            if(strlen($val)>0 && is_numeric($ref))
            {
                        $sql = "SELECT value FROM ".$dbref."_list_dropdowns WHERE id = $ref";
                        include("inc_db_con.php");
                        $lv = mysql_fetch_array($rs);
                        mysql_close($con);
                $val = code($val);
                $sql = "UPDATE ".$dbref."_list_dropdowns SET value = '$val' WHERE id = $ref";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "'$val' updated successfully.";
                            $action = "Renamed list from '".$lv['value']."' to '$val' (ref: $ref).";
                            $lsql = $sql;
                            logAct("S_LISTS",0,"Y",$action,$lsql);
                            logAct("S_LISTS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
        case "editv":   //edit list value
            $ref = $_GET['i'];
            $refv = $_GET['l'];
            $val = $_GET['v'];
            if(strlen($val)>0 && is_numeric($ref) && is_numeric($refv))
            {
                    $sql = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = $refv";
                    include("inc_db_con.php");
                    $lv = mysql_fetch_array($rs);
                    mysql_close($con);
                $val = code($val);
                $sql = "UPDATE ".$dbref."_list_dropdowns_values SET value = '$val' WHERE id = $refv";
                include("inc_db_con.php");
                $mar = mysql_affected_rows($con);
                if($mar>0)
                {
                            $res[0] = "check";
                            $res[1] = "'$val' updated successfully.";
                            $action = "Renamed value from '".$lv['value']."' to '$val' (ref: $refv).";
                            $lsql = $sql;
                            logAct("S_LISTS",$ref,"Y",$action,$lsql);
                }
                else
                {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
                }
            }
            else
            {
                        $res[1] = "Error! Please try again.";
                        $res[0] = "error";
            }
            break;
    }
    return $res;
}


function userList($type,$ref,$ret){
    global $cmpcode;
    global $dbref;
    global $modref;
    $sql2 = "";
    if($ret != "CR" && $ret != "A")
    {
        switch($ret)
        {
            case "C":
                $sql2 = "SELECT count(a.id) as cid ";
                break;
            case "L":
                $sql2 = "SELECT a.id, t.tkname, t.tksurname ";
                break;
            default:
                $sql2 = "SELECT a.id, t.tkname, t.tksurname ";
                break;
        }
                $sql2.= "FROM assist_".$cmpcode."_menu_modules_users u";
                $sql2.= ", assist_".$cmpcode."_timekeep t";
                $sql2.= ", ".$dbref."_list_users l ";
                $sql2.= ", ".$dbref."_list_admins a ";
                $sql2.= "WHERE u.usrmodref = '".$modref."' ";
                $sql2.= " AND u.usrtkid = t.tkid ";
                $sql2.= " AND t.tkstatus = 1 ";
                $sql2.= " AND l.tkid = t.tkid ";
                $sql2.= " AND (l.dash = 'Y' OR l.activate = 'Y' OR l.capture = 'Y') AND l.yn = 'Y' ";
                $sql2.= " AND a.yn = 'Y' AND a.type = '$type' AND a.tkid = t.tkid ";
                $sql2.= " AND ref = ".$ref;
        if($ret=="L")
        {
            $sql2.= " ORDER BY t.tkname, t.tksurname";
        }
    }
    else
    {
        switch($ret)
        {
            case "A":
                $sql2 = "SELECT t.tkid, t.tkname, t.tksurname ";
                $sql2.= "FROM assist_".$cmpcode."_menu_modules_users u";
                $sql2.= ", assist_".$cmpcode."_timekeep t";
                $sql2.= ", ".$dbref."_list_users a ";
                $sql2.= "WHERE u.usrmodref = '".$modref."' ";
                $sql2.= " AND u.usrtkid = t.tkid ";
                $sql2.= " AND t.tkstatus = 1 ";
                $sql2.= " AND a.tkid = t.tkid ";
                $sql2.= " AND (a.dash = 'Y' OR a.activate = 'Y' OR a.capture = 'Y') AND a.yn = 'Y' ";
                if(checkIntRef($ref) && strlen($type)==1)
                {
                    $sql2.= " AND t.tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE type = '$type' AND ref = $ref AND yn = 'Y') ";
                }
                $sql2.= "ORDER BY t.tkname, t.tksurname";
                break;
            case "CR":
                $sql2 = "SELECT a.ref, count(a.id) as cid ";
                $sql2.= "FROM assist_".$cmpcode."_menu_modules_users u";
                $sql2.= ", assist_".$cmpcode."_timekeep t";
                $sql2.= ", ".$dbref."_list_users l ";
                $sql2.= ", ".$dbref."_list_admins a ";
                $sql2.= "WHERE u.usrmodref = '".$modref."' ";
                $sql2.= " AND u.usrtkid = t.tkid ";
                $sql2.= " AND t.tkstatus = 1 ";
                $sql2.= " AND l.tkid = t.tkid ";
                $sql2.= " AND (l.dash = 'Y' OR l.activate = 'Y' OR l.capture = 'Y') AND l.yn = 'Y' ";
                $sql2.= " AND a.yn = 'Y' AND a.type = '$type' AND a.tkid = t.tkid ";
                $sql2.= " GROUP BY a.ref";
                break;
        }
    }
    return $sql2;
}

?>
