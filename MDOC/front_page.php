<?php

function MDOCfrontpage($m,$menu,&$dc) {
	$helper = new ASSIST_MODULE_HELPER();    
	$cmpcode = $_SESSION['cc'];
	$tkid = $_SESSION['tid'];
	$dbref = "assist_".$cmpcode."_".strtolower($m['ref']);
	$modref = strtolower($m['ref']);
	$modloc = "MDOC";
	$echo = "";
	$display_folders = isset($_SESSION['USER_PROFILE'][3]['field5'][strtoupper($modref)]) ? $_SESSION['USER_PROFILE'][3]['field5'][strtoupper($modref)] : 1;
	
	$user_access = mysql_fetch_one("SELECT * FROM ".$dbref."_list_users WHERE tkid = '$tkid' AND yn = 'Y' ORDER BY id DESC LIMIT 1");
	if(!is_array($user_access) || count($user_access)==0) { 
		$user_access = array('id'=>0,'tkid'=>$tkid,'docadmin'=>"N",'addcate'=>"N",'delcate'=>"N",'view'=>"N"); 
	}
				$echo = "<h2 class=center style=\"margin: 0px;\">".$m['title']."</h2><br>";
				if(count($menu)>1) {
					$echo.= "<span class=float style=\"margin: 5px 10px 5px 0px;\"><select id=docmenu>";
					foreach($menu as $md) {
						$echo.= "<option ".($md['ref']==$m['ref'] ? "selected" : "")." value=".$md['ref'].">".decode($md['title'])."</option>";
					}
					$echo.= "</select></span>";
				}
			//Disabling buttons
			/*
			$echo.= "	<table class=noborder width=100% style=\"padding: 0;\" id=tbl_doc_list>
						<tr>
							<td style=\"padding: 2px; padding-left: 20px\" class=\"noborder\"><input type=button value=Upload act=goto_mod action=upload modref=".$modref." modloc=".$modloc." /></td>
							<td style=\"padding: 2px;\" class=\"center noborder\"><input type=button class=\"doc_comment no_click\" value=Comment act=goto_mod action=comment modref=".$modref." modloc=".$modloc." /></td>
							<td style=\"padding: 2px; padding-right: 20px;\" class=\"right noborder\"><input type=button value=Maintain act=goto_mod action=admin modref=".$modref." modloc=".$modloc." /></td>
						</tr>";
            */
			if($display_folders!=0) {
				$echo .="	<tr>
							<td colspan=3 style=\"padding: 2px;\" class=noborder>";
							/*
							if($user_access['docadmin']=="Y" || $user_access['view']=="Y") {
								$sql = "SELECT DISTINCT cate.* 
										FROM ".$dbref."_categories cate 
										WHERE cate.catesubid = 0 AND cate.cateyn = 'Y' ORDER BY cate.catetitle";			
							} else {
								$sql = "SELECT DISTINCT cate.* 
										FROM ".$dbref."_categories cate 
										INNER JOIN ".$dbref."_categories_users cu
										ON cu.cucateid = cate.cateid AND cutkid = '$tkid' AND cuyn = 'Y' AND cutype = 'VIEW'
										WHERE cate.catesubid = 0 AND cate.cateyn = 'Y' ORDER BY cate.catetitle";			
							}
							$cates = mysql_fetch_all($sql);
                            */
                            //
                            $data = array();
                            $db = new ASSIST_DB();
                            $assistObj = new ASSIST();
                            
                            $sql = "SELECT mm.modref, mm.modlocation, mm.modtext 
                                    FROM assist_menu_modules mm 
                                    ".($helper->isAdminUser() ? "" : "INNER JOIN assist_".$helper->getCmpCode()."_menu_modules_users mmu
                                      ON mm.modref = mmu.usrmodref
                                      AND mmu.usrtkid = '".$helper->getUserID()."'")."
                                    WHERE mm.modyn = 'Y' ".($helper->isAdminUser() ? " OR mm.modadminyn = 'Y' " : "")." ORDER BY mm.modtext";
                                    
                            $data = $db->mysql_fetch_all_by_id($sql,"modref");
                            foreach($data as $key=>$val){
                                if($assistObj->isMdocModule($val['modlocation'])){
                                    $docFolders[$val['modref']]=array("cattitle"=>$val['modtext'],"catmodref"=>$val['modref'],"catyn"=>"Y","catsubid"=>0,"catowner"=>"0000");
                                }
                            }
                            $cates = $docFolders;
                            //
							$echo.= "<ul class=sortablefolders>"; //if($display_folders!=0) {
							foreach($cates as $modref=>$ct) {
								$dc++;
								$echo.= "<li id=".$ct['catmodref']." class=\"doclink no_hover\" action=view act=goto_mod modref='MDOC' modloc=".$modloc.">";
									$echo.= "<img src=/pics/icons/folder.png /><br />".$ct['cattitle'];
								$echo.= "</li>";
							} //}
				$echo.= "				</ul>
							</td>
						</tr>";
			}
			$echo.= "	</table>";
	
	
	
	
	
	/*if($dc==0 && $display_folders!=) {
		$echo = false;
	}*/
	return $echo;
}


function MDOCcomment($m) {
	return "";
};
/*
 * global $cmpcode, $tkid;
	$dbref = "assist_".$cmpcode."_".strtolower($m['ref']);
	$modref = strtolower($m['ref']);
$echo = "";
/** USER ACCESS **/
/*
$sql2 = "SELECT * FROM ".$dbref."_list_users WHERE tkid = '".$tkid."' AND yn = 'Y' ORDER BY id DESC LIMIT 1";
$access = mysql_fetch_one($sql2);
$my_access = array();
$main_access = array();
if($access['docadmin']!="Y") {
	$sql = "SELECT * FROM ".$dbref."_categories_users 
			INNER JOIN ".$dbref."_categories
			  ON cucateid = cateid AND cateyn = 'Y'
			WHERE cuyn = 'Y' AND cutkid = '$tkid' AND (cutype = 'UP' OR cutype = 'MAIN')";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$my_access[$row['cucateid']] = "Y";
		if($row['cutype']=="MAIN") { $main_access[$row['cucateid']] = "Y"; }
	}
	mysql_close();
}

/** GET CATEGORIES **/
/*
$categories = array();
$cate_count = 0;
$cate_owner = array();
if($access['docadmin']=="Y" || count($my_access) > 0) {
	$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$categories[$row['catesubid']][] = $row;
		$cate_count++;
		if($row['cateowner']==$tkid) {
			$cate_owner[$row['cateid']] = "Y";
			if(!isset($main_access[$row['cateid']])) { $main_access[$row['cateid']] = "Y"; }
			if(!isset($my_access[$row['cateid']])) { $my_access[$row['cateid']] = "Y"; }
		}
	}
	mysql_close();
}

	$echo.= chr(10)."<div id=\"div_doc_dialog\" class=doc_dialog title=\"Add Comment\">";
		$echo.= "<form name=addcomm method=post action=/DOC/frontpage_comment.php>";
			$echo.= "<input type=hidden name=act id=act value=SAVE_COMM />";
			$echo.= "<input type=hidden name=modref  value=".strtolower($m['ref'])." />";
			$echo.= "<p class=b>Category:<br /><select name=cateid class=no_change><option selected value=X>--- SELECT ---</option>";
			$echo.= MDOCoptionFunction($categories[0],0,"",$categories,$access,$my_access);
			$echo.= "</select></p>";
			$echo.= "<p class=b>Comment:<br /><textarea name=commtext id=commtext rows=10 cols=60></textarea></p>";
			$echo.= "<p><input type=button value=Save class=\"isubmit no_click\" /> <input type=button value=Cancel id=doc_dialog_cancel class=no_click /></p>";
		$echo.= "</form>";
	$echo.= "</div>
	<script type=text/javascript>
	$(function() {
		$(\".doc_dialog\").dialog({
			autoOpen: false,
			width: 450,
			modal: true,
			open: function() {
				$('select[name=cateid]').removeClass('required');
				$('textarea[name=commtext]').removeClass('required');
			}
		});
		$(\"#doc_dialog_cancel\").click(function() { $(\".doc_dialog\").dialog('close'); });
		$('.doc_dialog form[name=addcomm] input:button.isubmit').click(function() {
				$('select[name=cateid]').removeClass('required');
				$('textarea[name=commtext]').removeClass('required');
			var ci = $('select[name=cateid]').val();
			var ct = $('textarea[name=commtext]').val();
			var err = \"\";
			//var my_err = false;
			if(ci.length==0 || ci==0 || ci=='X') {
				$('select[name=cateid]').addClass('required');
				err = \"- Category\";
			}
			if(ct.length==0) {
				$('textarea[name=commtext]').addClass('required');
				if(err.length>0) { err = err+'\\n'; }
				err = err + '- Comment text';
			}
			if(err.length>0) {
				alert(\"Please complete the required fields as highlighted:\\n\"+err);
			} else {
				$('.doc_dialog form[name=addcomm]').submit();
			}
		});
	});
	</script>";



	return $echo;
}
*/

function MDOCoptionFunction($sub,$level,$title,$categories,$access,$my_access) {
	//global $categories;
	//global $access;
	//global $my_access;
	global $tkid;
	$echo = "";
	//arrPrint($sub);
/*
	foreach($sub as $c) {
		if($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']])) {
				$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
				$echo.= "<option value=".$c['cateid'].">".$text."</option>"; //."</span></p>";
		}
		$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
		if(count($sub)>0) {
			$echo.=MDOCoptionFunction($sub,$level+1,(strlen($title)>0 ? $title." >> " : "").$c['catetitle'],$categories,$access,$my_access);
		}
	}*/
	return $echo;
}


?>