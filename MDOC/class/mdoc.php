<?php

class MDOC extends MDOC_HELPER {
    
	
	const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;
    const REFTAG = "MDOC";
	
	public function __construct() {
		parent::__construct();
        $this->table_ref = $this->getDBRef();
		$this->has_target = false;
		//$this->headingsObj = new MDOC_HEADINGS();
	}

	public function getRefTag() { return self::REFTAG; }
	public function hasTarget() {
		return $this->has_target;
	}
	public function getMenuModulesFormattedForSelect($ids="") {
		return $this->formatRowsForSelect($this->getFolders());
	}
	/*
	public function getActiveOwners($ids="") {
		$sql = "SELECT subid as id, CONCAT(dirtxt,' - ',subtxt) as name , dirtxt as parent, subtxt as child
				FROM ".$this->getChildTable()."
				INNER JOIN ".$this->getParentTable()."
				  ON subdirid = dirid AND diryn = 'Y' 
				WHERE subyn = 'Y' ";
		if(is_array($ids)) {
			$sql.=" AND subid IN (".implde(",",$ids).")";
		} elseif(strlen($ids)>0) {
			$sql.= " AND subid = $ids ";
		}
		$sql.= "
				ORDER BY dirsort,subsort";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}
	*/ 
	protected function formatRowsForSelect($rows,$name="name") {
		$data = array();
		foreach($rows as $key => $r) {
			if(!isset($r[$name])){
				//ASSIST_HELPER::arrPrint($rows);
				$data[$key] = $r["value"];
			}else{
				$data[$key] = $r[$name];
			}
		}
		return $data;
	}
	
	
    public function getFolders(){
        $assistObj = new ASSIST();
        $allFolders = $assistObj->getMenuModules();
        //  ASSIST_HELPER::arrPrint($allFolders);
		$sharedOnes = $this->getSpecialAccessForUser($this->getUserID(),true);
		$temps = array();
		foreach($sharedOnes as $a=>$b){
			$array = $assistObj->getModuleDataFromCode($b);
			$array[$b]['modtext'].=' - (Shared)';
			$temps[$b] = $array;
			//$data[$b] = $assistObj->getModuleDataFromCode($b);
		}
		$processed = array_map(function($a) {  return array_pop($a); }, $temps);
        $allFolders = array_merge($processed,$allFolders);
        foreach($allFolders as $key=>$val){
            if($assistObj->isMdocModule($val['modlocation'])){
                $docFolders[$val['modref']]=array("name"=>$val['modtext'],"cattitle"=>$val['modtext'],"catmodref"=>$val['modref'],"catyn"=>"Y","catsubid"=>0,"catowner"=>"0000");
            }
        }
		//$docFolders[]=$sharedOnes;
        return $docFolders;
    }
    
    public function getFolderCount(){
        $assistObj = new ASSIST();
        $allFolders = $assistObj->getMenuModules();
        return count($allFolders);
    }
    
    public function getDocuments($folder,$sort=""){
        $sql = "SELECT doctype.value as type, doctype.yn as type_status, format.value as filetype, format.yn as filetype_status, format.icon as icon, mdoc.doc_content as content, mdoc.doc_modref as modref, mdoc.doc_id as id, mdoc.doc_title as title, mdoc.doc_date as date, mdoc.doc_expiry as expiry, mdoc.doc_format_id as format, CONCAT(mdoc.doc_location,'/',mdoc.doc_filename) as location FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc ";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_doctype format ON format.id = mdoc.doc_format_id";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_type doctype ON doctype.id = mdoc.doc_type_id";
        $sql .= " WHERE mdoc.doc_modref = '$folder' AND (mdoc.doc_status & ".MDOC::ACTIVE.")=".MDOC::ACTIVE;
        $sql .= strlen($sort)>0?" ORDER BY ".$sort:"";
        $contents = $this->mysql_fetch_all_by_id($sql,"id");
        return $contents;
    }
	
    public function getUserDocuments($folder,$sort="",$userid){
        $sql = "SELECT doctype.value as type, doctype.yn as type_status, format.value as filetype, format.yn as filetype_status, format.icon as icon, mdoc.doc_content as content, mdoc.doc_modref as modref, mdoc.doc_id as id, mdoc.doc_title as title, mdoc.doc_date as date, mdoc.doc_expiry as expiry, mdoc.doc_format_id as format, CONCAT(mdoc.doc_location,'/',mdoc.doc_filename) as location, mdoc.doc_extra_access as extra_access FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc ";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_doctype format ON format.id = mdoc.doc_format_id";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_type doctype ON doctype.id = mdoc.doc_type_id";
        $sql .= " WHERE mdoc.doc_modref = '$folder' AND mdoc.doc_extra_access LIKE '%".$userid."%' AND (mdoc.doc_status & ".MDOC::ACTIVE.")=".MDOC::ACTIVE;
        $sql .= strlen($sort)>0?" ORDER BY ".$sort:"";
        $contents = $this->mysql_fetch_all_by_id($sql,"id");
		//echo $sql;
        return $contents;
    }
	
    public function getDocumentsForObject($module,$object_type,$object_id){
        $sql = "SELECT doctype.value as type, doctype.yn as type_status, format.value as filetype, format.yn as filetype_status, format.icon as icon, mdoc.doc_uploaded_date, mdoc.doc_modified_date, mdoc.doc_uploader, mdoc.doc_modifier, mdoc.doc_content as content, mdoc.doc_modref as modref, mdoc.doc_id as id, mdoc.doc_title as title, mdoc.doc_date as date, mdoc.doc_expiry as expiry, mdoc.doc_format_id as format, mdoc.doc_location as location, mdoc.doc_filename as file FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc ";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_doctype format ON format.id = mdoc.doc_format_id";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_type doctype ON doctype.id = mdoc.doc_type_id";
        $sql .= " WHERE mdoc.doc_modref = '$module' AND mdoc.doc_object_type = '$object_type' AND mdoc.doc_object_id = '$object_id' AND (mdoc.doc_status & ".MDOC::ACTIVE.")=".MDOC::ACTIVE;
        //$sql .= strlen($sort)>0?" ORDER BY ".$sort:"";
        //echo $sql;
        $contents = $this->mysql_fetch_all_by_id($sql,"id");
        //return $sql;
        return $contents;
    }

        public function getDocumentsForObjectFolder($module,$object_type){
        $sql = "SELECT doctype.value as type, doctype.yn as type_status, format.value as filetype, format.yn as filetype_status, format.icon as icon, mdoc.doc_content as content, mdoc.doc_uploader, mdoc.doc_modifier, mdoc.doc_uploaded_date, mdoc.doc_modified_date, mdoc.doc_modref as modref, mdoc.doc_id as id, mdoc.doc_title as title, mdoc.doc_date as date, mdoc.doc_expiry as expiry, mdoc.doc_format_id as format, mdoc.doc_location as location, mdoc.doc_filename as file FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc ";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_doctype format ON format.id = mdoc.doc_format_id";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_type doctype ON doctype.id = mdoc.doc_type_id";
        $sql .= " WHERE mdoc.doc_modref = '$module' AND mdoc.doc_object_type = '$object_type' AND (mdoc.doc_status & ".MDOC::ACTIVE.")=".MDOC::ACTIVE;
        $contents = $this->mysql_fetch_all_by_id($sql,"id");
        return $contents;
    }

        public function getDocumentsForView($module,$specifier_string){
        $specifier_string = explode("|",$specifier_string);	
        $object_type = $specifier_string[0];
        $object_id = $specifier_string[1];
        $sql = "SELECT doctype.value as type, doctype.yn as type_status, format.value as filetype, format.yn as filetype_status, format.icon as icon, mdoc.doc_content as content, mdoc.doc_uploader, mdoc.doc_modifier, mdoc.doc_uploaded_date, mdoc.doc_modified_date, mdoc.doc_modref as modref, mdoc.doc_id as id, mdoc.doc_title as title, mdoc.doc_date as date, mdoc.doc_expiry as expiry, mdoc.doc_format_id as format, mdoc.doc_location as location, mdoc.doc_filename as file FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc ";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_doctype format ON format.id = mdoc.doc_format_id";
        $sql .= " LEFT OUTER JOIN assist_".$this->getCmpCode()."_mdoc_list_type doctype ON doctype.id = mdoc.doc_type_id";
        $sql .= " WHERE mdoc.doc_modref = '$module' AND mdoc.doc_object_type = '$object_type' AND mdoc.doc_object_id = '$object_id' AND (mdoc.doc_status & ".MDOC::ACTIVE.")=".MDOC::ACTIVE;
        $contents = $this->mysql_fetch_all_by_id($sql,"id");
        foreach($contents as $con=>$data){
        	$contents[$con]['type_in']=$object_type;
	        $contents[$con]['id_in']=$object_id;
        }
        return $contents;
    }
    
	public function getObjectFolders($module,$shared=''){
		/*	
		$sql = "SELECT DISTINCT doc_object_type as type FROM assist_".$this->getCmpCode()."_mdoc_documents WHERE doc_modref = '$module'";
		$folders = $this->mysql_fetch_all_by_value($sql,"type");
		foreach($folders as $fldr){
			$res[]=ucwords(strtolower($fldr));
		}
		return $res;
		*/
		if(strlen($shared)>0){
			$shr = " AND doc_extra_access LIKE '%".$shared."%'";
		}else{
			$shr = "";
			//$shr = " AND doc_extra_access NOT LIKE '%".$this->getUserID()."%'";
		}
		$sql = "SELECT DISTINCT doc_extra_access as extra_access, doc_object_type as type, doc_object_id as id FROM assist_".$this->getCmpCode()."_mdoc_documents WHERE doc_modref = '$module' $shr GROUP BY doc_object_id";
		//return $sql;
		$folders = $this->mysql_fetch_all($sql);
		$object_constructor = $this->getAModLocation($module);
		$object = new $object_constructor();
		foreach($folders as $key=>$fldr){
			$folders[$key]['title']=$object->getObjectNameForMdoc($fldr['type'],$fldr['id']);
		}
		return $folders;
	}
	
	public function getSpecialAccessForMdoc($objectid){
		//error_reporting(-1);
        $sql = "SELECT doc_extra_access FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc
        		WHERE mdoc.doc_id = $objectid 
        		AND mdoc.doc_status = ".$this::ACTIVE;
        $contentsRaw = $this->mysql_fetch_one_value($sql, "doc_extra_access");
        $contents = json_encode($contentsRaw);
        //return $sql;
        return $contentsRaw;
        return $contents;
    }	
		
	public function getSpecialAccessForObject($objectid){
		//error_reporting(-1);
        $sql = "SELECT doc_id as id, doc_extra_access as access FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc
        		WHERE mdoc.doc_object_id = $objectid 
        		AND mdoc.doc_modref = '".$this->getModRef()."'
        		AND mdoc.doc_extra_access <> ''
        		AND mdoc.doc_status = ".$this::ACTIVE;
        //$contentsRaw = $this->mysql_fetch_all_by_id($sql, "id");
        $contentsRaw = $this->mysql_fetch_value_by_id($sql, "id", "access");
        $contents = json_encode($contentsRaw);
        //return $sql;
        return $contentsRaw;
        return $contents;
    }
    
	public function getSpecialAccessForUser($userid,$countOnly){
		//error_reporting(-1);
        $sql = "SELECT doc_id as id, doc_extra_access as access, doc_modref as modref FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc
        		WHERE mdoc.doc_status = ".$this::ACTIVE."
        		AND LENGTH(mdoc.doc_extra_access) > 0";
        //$contentsRaw = $this->mysql_fetch_all_by_id($sql, "id");
        $contentsRaw = $this->mysql_fetch_all_by_id($sql, "id");
        if(count($contentsRaw)>0){
			$userid = "".$userid."";
        	$hits = array();
        	foreach($contentsRaw as $key=>$c){
        		$extras = json_decode($c['access']);
        		//if(strpos($userid, $c['access'])!= false){
        		//	$hits[]=$c;
        		//}
        		if(in_array($userid, $extras)){
        			if($countOnly){
	 					if(!in_array($c['modref'],$hits)){
	 						$hits[]=$c['modref'];
	 					}	       				
        			}else{
	        			$hits[]=$c['modref'];
        			}
        		}
        	}
			//return $userid;
			return $hits;
        }else{
        	return array();
        }
    }		
	
    public function getModuleDocumentTable($module,$object_type,$object_id,$page_action,$formatted_object_type,$page_direct){
        //echo ($module." ".$object_type." ".$object_id." ".$page_action." ".$formatted_object_type." ".$page_direct);
        include_once 'mdoc_list.php';
        $is_edit_page = $page_action == "EDIT" ? true : false;
        $is_add_page = $page_action == "ADD" && $object_id !== 0 && $object_id !== "0" ? true : false;
        $is_update_page = false;
        $sort = isset($_REQUEST['s'])?$_REQUEST['s']:"title";
        $js = "";
        $html = "<div id='mdoc_data_div'><form name=veiw action=\"view.php\" method=get><input type=hidden name=c value=$module />";
        
		if(!$is_edit_page){
			$myObject = new MASTER();
			$existingAccess = $this->getSpecialAccessForObject($object_id,true);
			//$html.=ASSIST_HELPER::arrPrint($existingAccess);
			$accessCount = count($existingAccess);
			if($accessCount > 0){
				$existingAccess = reset($existingAccess);
				$existingAccess = json_decode($existingAccess);
			}
			//$html.=ASSIST_HELPER::arrPrint($existingAccess);
			$html.='<table class="tbl_container noborder" style="width:650px;"><tbody><tr><td style="border-color:white;" class="">
						<table id="extra_user_access_table" class=form style="width: 645px;">
					            <tr>
					                <th width=84%>Extra User Access</th>
					                <th>
					                	<div class="help-tip" style="padding:5px; margin:auto;">
					                		<p>People listed here have access to all of the documents below - even if they are not allowed to see this '.$myObject->getObjectName($object_type).'.</p>
					                	</div>
				                	</th>
					            </tr>';
					            
			if($accessCount>0){
				$html.='
								<tr id="extra_access_existing">
					                <td colspan=2>
				                		<ul>';
										if(count($existingAccess)>0){
											foreach($existingAccess as $usr){
			                					$html.='<li>'.$this->getAUserName($usr).'</li>';
											}					
										}else{
											$html.='<li><em>No extra access has been set up.</em></li>';
										}
				$html.='               	</ul>
									</td>
					            </tr>';
			}else{
				$html.='
								<tr>
					                <td colspan=2><em>No extra access has been set up.</em></td>
		                		</tr>';
			}
			$html.='
					            <tr style="display:none;" class="extra_help_hidden">
					                <td colspan=2><em>People listed above have access to all of the documents below - even if they are not allowed to see this '.$myObject->getObjectName($object_type).'.</em></td>
					            </tr>
					        </table>
				        </td></tr></tbody></table>';
			$js.='
				$(".help-tip").on("click",function(){
					$(".extra_help_hidden").toggle(
						"slow",
						function() {
					});
				});
			';
		}
		$html.="<table cellpadding=5 cellspacing=0 width=650 style=\"margin-bottom: 10px; \">
                    <tr>";
        if($is_edit_page){
        	//draw sort
        	$html .= "<td width=250 style=\"border-right: 0px;\">Sort by: <span style='float:right'><select id=sort><option selected value='title'>Document Title</option><option value='date'>Date Added</option><option value='expiry'>Expiry Date</option></select> <input type=button value=\" Go \" onclick=\"sortby();\">&nbsp;</span></td>";
        }
        //<td width=250 align=right style=\"border-left: 0px;\"><!-- Search: <input type=hidden name=s value=0><input type=text width=15 name=f> <input type=submit value=\" Go \"> --></td>
        $html .= "</tr>
                </table></form>";
        $folders = $this->getFolders();
        //ASSIST_HELPER::arrPrint($folders);
        $html .= "<table width=650 style=\"border-color: #ffffff;margin:-5 5 10 5; display:inline-block;\">";
        $icon = "/pics/icons/folder_full.gif";
        $html .= "<tr>
                     <td colspan=2 class=\"mdoc_folder_td\" style=\"border-color:#ffffff;\"><p style=\"text-decoration:underline;color: #000000\"><img src=\"$icon\" style=\"border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;\">".$folders[$this->getModRef()]['cattitle']."</p></td>
                 </tr>";
        $folder_contents = $this->getDocumentsForObject($this->getModRef(),$object_type,$object_id);
		//echo $object_type . " ". $object_id;
        //ASSIST_HELPER::arrPrint($folder_contents); 
        //$all_docs = $this->getDocuments("CUSTOMER"); 
        //ASSIST_HELPER::arrPrint($all_docs);
        $m = count($folder_contents);
        $mtable = "N";
        foreach($folder_contents as $key=>$val){
        	$docid = $val['id'];
            $floc2 = "../files/".$this->getCmpCode()."/".$val['location'];
			//echo $floc2;
            if(file_exists($floc2)) {
                if($mtable == "N") {
                    $html .= "<table width=650 id='tbl_doc'>";
                    $mtable = "Y";
                }   //IF THE TABLE HEADING HAS BEEN CREATED
                $html .= "<tr>";
                $html .= "<td style='border-top-color:#ffffff;border-left-color:#ffffff;border-bottom-color:#ffffff;'>&nbsp;</td><td class='mdoc_td' style='border-right-color:#ffffff;'>
                			<a href='../MDOC/class/inc_attachment_controller.php?action=MDOC.GET_ATTACH&object_id=$docid&activity=' target='_blank'>";
                $icon = "/pics/icons/".$val['icon'];
                $icon2 = "../pics/icons/".$val['icon'];
                if(file_exists($icon2)) {
                    $html .= "<img src=$icon style=\"vertical-align: middle; margin: 2 4 2 10;border-width:1px; border-color: #FFFFFF;\" class=a>";
                }
                if(strlen($val['title']) > 0) {
                    $html .= $val['title'];
                } else {    //IF THERE IS TEXT IN THE TOC
                    $html .= "Untitled" ;
                }
                $html .= "</a>"." (".number_format(filesize($floc2)/1024,0)."kb)";
                $type = isset($val['type']) && strlen($val['type'])>0 ? $val['type'] : "";
                if(strlen($val['content'])>0) {
                    //$content = str_replace($val['content'],chr(10),"<br>");
                    $html .= "<ul><span style=\"font-style:italic;font-size:8pt;\">".$val['content']."</span></ul>";
                }
                $html .= "<div style=\"margin-left: 20px; font-size: 7pt;\">".$type."</div>";
                //Edit and Download buttons
                $html .= "</td>";
                $html .= "<td class='mdoc_tools' width=255 style=\"border-left-color:white; text-align: right; font-size: 7pt; font-style: italic;\">";
                if($is_edit_page){
	                $html .= "<input class='mdoc_edit' type='button' value='Edit' ref='".$val['id']."' />&nbsp;";
                }
                $html .= "<input class='mdoc_download' type='button' value='Download' ref='".$val['id']."' />";
                //Metainfo 1
        		$html .= "<div class='metaHolder'>&nbsp;";
        		$html .= "<div class='meta'>";
                $d1 = $val['doc_uploaded_date'];
                $d2 = $val['doc_modified_date'];
                $html .= "<br />Added on ".$d1;
                $aname = isset($val['doc_uploader']) && strlen($val['doc_uploader'] > 0) ? $this->getAUserName($val['doc_uploader']) : "Unknown";
                $html .= "<br />by ".$aname."";
                if($d1!=$d2 && $d2 > $d1) {
	                $dname = isset($val['doc_modifier']) && strlen($val['doc_modifier'] > 0) ? $this->getAUserName($val['doc_modifier']) : "Unknown";
                    $html .= "<br />Modified on ".$d2;
                    $html .= "<br />by ".$dname."";
                }
        		$html .= "</div>";
                //
                //Metainfo 2
        		$html .= "<div class='xmeta' style='display:none'>";
                $date = isset($val['date'])?$val['date']."<br />":"Unknown <br />";
                $exp = isset($val['expiry'])?$val['expiry']."<br />":"Unknown <br />";
                $html .= "<br />Document date: ".$date;
                $html .= "<br />Document expiry: ".$exp;
        		$html .= "</div>";
        		$html .= "</div>";
                //
                $html .= "</td>";
                $html .= "    </tr>";
            }
        }
        /*
        foreach($folders as $key=>$val){
            $dc = $this->getDocuments($val['catmodref'],$sort); 
            $m = count($dc);
            if($m>0){
                $icon = "/pics/icons/folder_full.gif";
            }else{
                $icon = "/pics/icons/folder_empty.gif";
            }
            $ref++;
            //$url = "view.php?c=".$val['catmodref'];
            $url = "#";
            $html .= "<tr>
                         <td class=\"mdoc_folder_td\" folder=\"".$val['catmodref']."\" style=\"border-color:#ffffff;\"><a href=\"$url\" style=\"text-decoration:underline;color: #000000\"><img src=\"$icon\" style=\"border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;\">".$val['cattitle']."</a></td>
                     </tr>";
            }
        */
        	if($is_edit_page || $is_add_page){
        		$html .= 
	        	"
	  				<tr class='tbl_container noborder'><td class='tbl_container noborder' style='border-color:white;' colspan=99><input type='button' style='float:right; margin-right:-5px' id='mdoc_add_btn' value='Add Document' /></td></tr>
				";
        	}else if($object_id == 0 || $object_id == "0"){
        		$html .= 
	        	"
	  				<tr class='tbl_container noborder'><td class='tbl_container noborder' style='border-color:white;' colspan=99>Please save a $formatted_object_type before adding a document.</td></tr>
				";
        	}
        	$html .= "</table>
        			</div>
            ";
            
			$logObj = new MDOC_LOG("mdoc");
			$html .= "<table class='tbl_container noborder' style='display:inline-block; width:650px;'><tr><td style='border-color:white;'>";
			$html .= $logObj->getAuditLogHTML(array("parent_id"=>$this->getModRef()."_".$object_type."-".$object_id));
			$html .= "</td></tr></table>";
			
            //including the iFrame within the dialog
            $html.="<div id='popup_dialog' style='display:none;'>
                        <iframe frameborder=0 width=520px height=570px></iframe> 
                    </div>";
			//Including the CSS rule for meta holder
			$html .= "<style type='text/css'>.metaHolder {height:80px; overflow:hidden;}</style>";
                        
            $js .= "
            	$('.mdoc_tools').hover(function(){
            		$(this).find('.xmeta').show();
            		$(this).find('.meta').hide('fade');
            	},function(){
            		$(this).find('.xmeta').hide();
            		$(this).find('.meta').show('fade');
            	});
            	
                $('.mdoc_folder_td').hover(function(){
                   $(this).css('background-color','#fafafb'); 
                },function(){
                   $(this).css('background-color','#fff'); 
                });

                $('.mdoc_td').hover(function(){
                   $(this).css('background-color','#f2f2f2'); 
                },function(){
                   $(this).css('background-color','#fff'); 
                });
                
				/*
                $('.mdoc_td:odd').each(function(){
                   $(this).css('background-color','#fafafb'); 
                });
                */
                
                $('.mdoc_edit').button().click(function(){
                   var ref = $(this).attr('ref');
                   $('#popup_dialog iframe').prop('src','../MDOC/class/mdoc_object_form.php?action=edit&raw_object_type=$object_type&page_direct=$page_direct&object_type=$formatted_object_type&object_id=$object_id&id='+ref);  
                });

                $('.mdoc_download').button().click(function(){
                   var myref = $(this).attr('ref');
                   document.location.href = '../MDOC/class/inc_attachment_controller.php?action=MDOC.GET_ATTACH&object_id='+myref+'&activity='; 
                });
                
                $('#mdoc_add_btn').button().click(function(){
                   $('#popup_dialog iframe').prop('src','../MDOC/class/mdoc_object_form.php?action=add&raw_object_type=$object_type&object_type=$formatted_object_type&object_id=$object_id&page_direct=$page_direct');  
                });
                
                function dialogFinished(icon,result){
                    if(icon !== 'ok'){
                        AssistHelper.finishedProcessing(icon,result);
                    }else{
                        AssistHelper.finishedProcessing(icon,result);
                    }
                }
            ";

            $js .= "";
            
            return array("html"=>$html,"js"=>$js);
    }
    
    public function getRawObject($id){
        $sql = "SELECT * FROM assist_".$this->getCmpCode()."_mdoc_documents WHERE doc_id='$id'";
        $results = $this->mysql_fetch_one($sql);
        return $results;
    }
    
    public function getDetailedObject($id) {
		$obj_type = "MDOC";
		//ASSIST_HELPER::arrPrint($options);
		$left_joins = array();
		$compact_view = false;
		$format_for_emails = false;
		$attachment_buttons = false;
		
		$headObject = new MASTER_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());
		
		//set up contract variables
		$c_headings = $headObject->getMainObjectHeadings("CONTRACT",($compact_view==true ? "COMPACT" : "DETAILS")); 
		$all_headings = $c_headings['rows'];
		$cObject = new MASTER_CONTRACT(); 
		$c_tblref = "C";
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_id_fld = $cObject->getIDFieldName();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_name = $cObject->getNameFieldName();
		$c_manager = $cObject->getManagerFieldName();
		$c_field = $c_tblref.".".$cObject->getTableField();
		
		$where = $cObject->getActiveStatusSQL($c_tblref);
			$id_fld = $cObject->getIDFieldName();
			$a_headings = array('rows'=>array());
			$d_headings = array('rows'=>array());
			if($format_for_emails) {
				$final_data['head'][$id_fld] = $d_headings['rows'][$id_fld];
				$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
				$final_data['head'][$c_id_fld] = $c_headings['rows'][$c_id_fld];
				$final_data['head'][$c_manager] = $c_headings['rows'][$c_manager];
				$sql ="SELECT ".$c_tblref.".".$id_fld." as id 
						, CONCAT('".$this->getRefTag()."',".$c_id.") as ".$c_id_fld."
						, CONCAT(".$c_tblref.".".$c_name.",' [".$this->getRefTag()."',".$c_id.",']') as ".$c_name."
						, ".$c_tblref.".".$c_manager."";
			} else {
				$sql = "SELECT ".$c_tblref.".*";
				$a_headings = array('rows'=>array());
				$final_data['head'] = $c_headings['rows'];
				$group_by = " GROUP BY ".$c_tblref.".".$id_fld;
			}
			$where_tblref = $c_tblref;
			$ref_tag = $this->getRefTag();
		
		$where.= " AND ".$where_tblref.".".$id_fld." = ".$id;
		//echo $where;
		
		//return $d_headings;
		$listObject = new MASTER_LIST();
		
		//$list_headings = $headObject->getAllListHeadings();
		//$list_tables = $listObject->getAllListTables(array_keys($list_headings));
		
		foreach($all_headings as $fld => $head) {
			$lj_tblref = substr($head['section'],0,1);
			if($head['type']=="LIST" && $head['parent_id']==0) { 
				$listObject->changeListType($head['list_table']);  
				$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
									AS ".$head['list_table']." 
									ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
									AND (".$head['list_table'].".status & ".MASTER::DELETED.") <> ".MASTER::DELETED;
			} elseif($head['type']=="MASTER") {
				$tbl = strtoupper($head['list_table']);
				//$masterObject = new MASTER_MASTER($fld);
				$masterObject = new $tbl();
				$tbl = $head['list_table'];
				$sql.= ", ".$fld.".".$masterObject->getFld("NAME")." as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$masterObject->getTable()." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$masterObject->getFld("ID");
			} elseif($head['type']=="USER") {
				$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
			} elseif($fld=="16") {
				$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_certificates AS c_certs ON ".$c_tblref.".".$id_fld." = c_certs.customer_c_parent";
			}
		}
		$from = " $c_table $c_tblref ";
		$sql.= " FROM ".$from.implode(" ",$left_joins);
		$sql.= " WHERE ".$where.(isset($group_by) ? $group_by : "");		
			//return $sql;
			//echo $sql; 
		$r = $this->mysql_fetch_one($sql);
			//echo json_encode($r);
			//ASSIST_HELPER::arrPrint($r);
			//echo"<h1>INCOMING:</h1>";
			//echo $r; 
			//echo"<h1>DONE</h1>";
		//Fetch the children
		$parentArr = array();
		foreach($final_data['head'] as $k => $f){
			if(is_numeric($f['field'])){
				$parentArr[]=$f['field'];
			}
		}
		//$parentArr = implode(", ",$parentArr);
		//echo $parentArr;
		$sqlCh = "SELECT * FROM ".$this->getDBRef()."_setup_headings WHERE h_parent_id IN(11,12,13,14,15,17) ORDER BY h_parent_id";
		$children = $this->mysql_fetch_all($sqlCh);
//ASSIST_HELPER::arrPrint($children); 
		foreach($children as $cid=>$c){
			$prnt = $c['h_parent_id'];	
			if(isset($final_data['head'][$prnt])){
				$c_id = $c['h_id'];
				$final_data['head'][$prnt]['sub'][$c_id] = $c;
			}
		}
//ASSIST_HELPER::arrPrint($final_data['head']); 
		$row = array();
		$displayObject = new MASTER_DISPLAY();
		
		//Process the final data to deal with numeric headings
		foreach($final_data['head'] as $fld=> $head) {
			if(is_numeric($fld)){
				
			}
		}
		//End processing
		
		
//		foreach($rows as $r) {
			//$i = $r[$id_fld]=="Ccustomer_id"?"customer_id":$r[$id_fld];
			$i = $r[$id_fld];
			foreach($final_data['head'] as $fld=> $head) {
				$val = isset($r[$fld]) ? $r[$fld] : $fld;
				//format by type
				if($headObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
					$row[$fld] = ( (!isset($r[$head['list_table']]) || is_null($r[$head['list_table']])) ? $this->getUnspecified() : $r[$head['list_table']]);
				//} elseif($head['type']=="BOOL") {
					//$row[$fld] = $displayObject->getDataField("BOOL", $val);
				} elseif($head['type']=="HEADING"){
					//echo "<h3>KID:".$fld."</h3>"; 
					$row[$fld] = $children[$fld]; 
				} elseif($head['type']=="DELIVERABLE") {
					if($r["del_type"]=='SUB') {
						if($r[$fld]==0) {
							$row[$fld] = $this->getUnspecified();
						} else {
							if(!isset($deliverable_names_for_subs)) {
								$deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($i);
								//$this->arrPrint($deliverable_names_for_subs);
							}
							$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
						}
					} else {
						unset($final_data['head'][$fld]);
					}
				} elseif($head['section']=="DELIVERABLE_ASSESS") {
					$assess_status = $r['contract_assess_type'];
					$display_me = true;
						if($cObject->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $cObject->mustIAssessQuality($assess_status); break;
								case "quantity": $display_me = $cObject->mustIAssessQuantity($assess_status); break;
								case "other": $display_me = $cObject->mustIAssessOther($assess_status); break;
							}
						}
					if($display_me) {
						$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
						if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
							$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
						}
					} else {
						unset($final_data['head'][$fld]);
						//$row[$fld] = $assess_status;
					}
				} elseif($this->isDateField($fld) || $head['type']=="DATE" ) {
					//echo "<h3>".ASSIST_HELPER::arrPrint($head)."</h3>";
					if($val == "0000-00-00 00:00:00" || $val == "0" || $val ==0){
						$row[$fld] = $this->getUnspecified();
					}else{
						$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
					}
				//format by fld
				} elseif($fld==$id_fld){//} || ($obj_type=="ACTION" && $fld==$a_id_fld) || ($obj_type=="DELIVERABLE" && $fld==$d_id_fld) || ($obj_type=="CONTRACT" && $fld==$c_id_fld)) {
					if($fld==$id_fld && !$format_for_emails) {
						//$row[$fld] = $ref_tag.$val;
						$row[$fld] = $val;
					} else {
						$row[$fld] = $val;
					}
				} elseif($head['type']=="REF") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
				} elseif($head['type']=="ATTACH") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
					$attach_display_options = array(
						'can_edit'=>false,
						'object_type'=>$obj_type,
						'object_id'=>$id,
						'buttons'=>$attachment_buttons,
					);
					$row[$fld] = $displayObject->getDataField($head['type'], $val,$attach_display_options);
				} else if(($fld == 16 || $fld == "16") ){
					
				}else {
					//$row[$fld] = $val;
					//if($val == "0000-00-00 00:00:00"){
					//	$row[$fld] = $this->getUnspecified();
					//}else{
						$row[$fld] = $displayObject->getDataField($head['type'], $val);
					//}
				}
			}
			if($format_for_emails) {
				switch($obj_type) {
					case "ACTION":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$a_name] = $row[$a_name]; 
						$sorted_row['child'][$a_owner] = $row[$a_owner]; 
						$sorted_row['child'][$a_deadline] = $row[$a_deadline]; 
						$sorted_row['parent'][$d_name] = $row[$d_name]; 
						$sorted_row['parent'][$d_owner] = $row[$d_owner]; 
						$sorted_row['parent'][$d_deadline] = $row[$d_deadline]; 
						$sorted_row['grandparent'][$c_name] = $row[$c_name]; 
						$sorted_row['grandparent'][$c_owner] = $row[$c_owner]; 
						$sorted_row['grandparent'][$c_manager] = $row[$c_manager]; 
						$sorted_row['grandparent'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$a_id_fld]; 
						$field_names['child']['name'] = $a_name; 
						$field_names['child']['owner'] = $a_owner; 
						$field_names['child']['deadline'] = $a_deadline; 
						$field_names['parent']['id'] = $row[$d_id_fld]; 
						$field_names['parent']['name'] = $d_name; 
						$field_names['parent']['owner'] = $d_owner; 
						$field_names['parent']['deadline'] = $d_deadline; 
						$field_names['grandparent']['id'] = $row[$c_id_fld]; 
						$field_names['grandparent']['name'] = $c_name;
						$field_names['grandparent']['owner'] = $c_owner; 
						$field_names['grandparent']['manager'] = $c_manager; 
						$field_names['grandparent']['deadline'] = $c_deadline; 
						break;
					case "DELIVERABLE":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$d_name] = $row[$d_name]; 
						$sorted_row['child'][$d_owner] = $row[$d_owner]; 
						$sorted_row['child'][$d_deadline] = $row[$d_deadline]; 
						$sorted_row['parent'][$c_name] = $row[$c_name]; 
						$sorted_row['parent'][$c_owner] = $row[$c_owner]; 
						$sorted_row['parent'][$c_manager] = $row[$c_manager]; 
						$sorted_row['parent'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$d_id_fld]; 
						$field_names['child']['name'] = $d_name; 
						$field_names['child']['owner'] = $d_owner; 
						$field_names['child']['deadline'] = $d_deadline; 
						$field_names['parent']['id'] = $row[$c_id_fld]; 
						$field_names['parent']['name'] = $c_name;
						$field_names['parent']['owner'] = $c_owner; 
						$field_names['parent']['manager'] = $c_manager; 
						$field_names['parent']['deadline'] = $c_deadline; 
						break;
					case "CONTRACT":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$c_name] = $row[$c_name]; 
						$sorted_row['child'][$c_owner] = $row[$c_owner]; 
						$sorted_row['child'][$c_manager] = $row[$c_manager]; 
						$sorted_row['child'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$c_id_fld];
						$field_names['child']['name'] = $c_name;
						$field_names['child']['owner'] = $c_owner; 
						$field_names['child']['manager'] = $c_manager; 
						$field_names['child']['deadline'] = $c_deadline; 
						break;
				}				
				$final_data['rows'] = $sorted_row;
				$final_data['email_fields'] = $field_names;
				//$final_data['email_full'] = $r;
			} else {
				//echo"<h1>INCOMING:</h1>";
				//ASSIST_HELPER::arrPrint($row);
				//echo"<h1>DONE</h1>";
				$final_data['rows'] = $row;
			}
		//}
		return $final_data;
	}




public function addObject($data){
        unset($data['attachments']);
        $data['doc_title'] = mysql_real_escape_string($data['doc_title']);
        $data['doc_content'] = mysql_real_escape_string($data['doc_content']);
        $data['doc_modified_date'] = date("Y-m-d H:i:s");
        $data['doc_modifier'] = $this->getUserID();
        $data['doc_uploaded_date'] = date("Y-m-d H:i:s");
        $data['doc_uploader'] = $data['doc_modifier'];
        $data['doc_status'] = MDOC::ACTIVE;
        $data['doc_date'] = date("Y-m-d H:i:s",strtotime($data['doc_date']));
        $data['doc_expiry'] = date("Y-m-d H:i:s",strtotime($data['doc_expiry']));
        $data['doc_reminder'] = date("Y-m-d H:i:s",strtotime($data['doc_reminder']));
        $sql_data = $this->convertArrayToSQL($data);  
        //return array("info",json_encode($sql_data));
        $sql_string = "INSERT INTO assist_".$this->getCmpCode()."_mdoc_documents SET $sql_data";
        $result = $this->db_insert($sql_string);
        if($this->checkIntRef($result) && $result > 0){
    		$logObj = new MDOC_LOG("mdoc");
            $inserter = $data['doc_modifier'];
            $changes['user']['raw'] = $inserter;
            $changes['user'] = $this->getAUserName($inserter);
			$changes[] = "Document '".$data['doc_title']."' [".self::REFTAG.$result."] created.";
			//$changes['doc_title']['to'] = $data['doc_title'];
			//$changes['doc_title']['from'] = $this->getUnspecified();
            $log_var = array(
                'object_id' => $result,
                'parent_id' => $this->getModRef()."_".$data['doc_object_type']."-".$data['doc_object_id'],
                'changes'   => $changes,
                'log_type'  => MDOC_LOG::CREATE,
                'status_id' => MDOC::ACTIVE,      
            );
            //$result = array("info",json_encode($log_var));
            //return $result;
            $log_result = $logObj->addObject($log_var);
            return array("ok","Document created succesfully","mdoc_id"=>$result,"modref"=>$data['doc_modref'],"object_id"=>$data['doc_object_id'],"object_type"=>$data['doc_object_type']);
        }else{
            return array("error",$result);
        }      
    }

    public function deleteObject($data){
        $id = $data['doc_id'];
        $sql['doc_modified_date'] = date("Y-m-d H:i:s");
        $sql['doc_modifier'] = $this->getUserID();
        $sql['doc_status'] = MDOC::DELETED;
        $sql_data = $this->convertArrayToSQL($sql);  
        //return array("info",json_encode($sql_data));
        $sql_string = "UPDATE assist_".$this->getCmpCode()."_mdoc_documents SET $sql_data WHERE doc_id = $id";
        $result = $this->db_update($sql_string);
        if($this->checkIntRef($result) && $result > 0){
        	$logObj = new MDOC_LOG("mdoc");
            $inserter = $this->getUserID();
            $changes['user']['raw'] = $inserter;
            $changes['user'] = $this->getAUserName($inserter);
			$changes[] = "Document '".$data['doc_title']."' [".self::REFTAG.$id."] deleted.";
            $log_var = array(
                'object_id' => $id,
                'parent_id' => $this->getModRef()."_".$data['doc_object_type']."-".$data['doc_object_id'],
                'changes'   => $changes,
                'log_type'  => MDOC_LOG::DELETE,
                'status_id' => MDOC::DELETED,      
            );
            //$result = array("info",json_encode($log_var));
            //return $result;
            $log_result = $logObj->addObject($log_var);
            return array("ok","Document removed succesfully","mdoc_id"=>$id,"modref"=>$data['doc_modref']);
        }else{
            return array("error","Document couldn't be removed: error code ".$result);
        }      
    }
    
    public function editDocument($data){
    //ASSIST_HELPER::arrPrint($data);    
        $id = $data['doc_id'];
        $data['doc_title'] = ASSIST_HELPER::code($data['doc_title']);
        $data['doc_content'] = ASSIST_HELPER::code($data['doc_content']);
		$changes['response'] = "Document '".$data['doc_title']."' [".self::REFTAG.$id."] edited.";
        //unset($data['doc_status']);    
        $obj_type = $data['doc_object_type'];
        $obj_id = $data['doc_object_id'];
        unset($data['doc_object_type']);    
        unset($data['doc_object_id']);    
        //unset($data['doc_location']);
        $data['doc_modified_date'] = date("Y-m-d H:i:s");
        $data['doc_modifier'] = $this->getUserID();
		$data['doc_uploader'] = $this->getUserID();
        $data['doc_status'] = MDOC::ACTIVE;
        $data['doc_date'] = date("Y-m-d H:i:s",strtotime($data['doc_date']));
        $data['doc_expiry'] = date("Y-m-d H:i:s",strtotime($data['doc_expiry']));
        $data['doc_reminder'] = date("Y-m-d H:i:s",strtotime($data['doc_reminder']));
        $old = $this->getRawObject($id);
        unset($old['doc_uploaded_date']);
    //ASSIST_HELPER::arrPrint($old);    
        $headingObj = new MDOC_HEADINGS();
        $headings = $headingObj->getMainObjectHeadings("FORM");
    //ASSIST_HELPER::arrPrint($headings);    
        foreach($old as $key=>$val){
    //echo $headings[$key]['name']." - ".$data[$key]." <val> ".$val;
            if($headings[$key]['type'] == "DATE" || $headings[$key]['type'] == "SYSTEM_DATE"){
                $old[$key] = date("Y-m-d H:i:s",strtotime($val));
                $data[$key] = date("Y-m-d H:i:s",strtotime($data[$key]));
            }else if($headings[$key]['type'] == "MEDVC" || $headings[$key]['type'] == "LRGVC" || $headings[$key]['type'] == "TEXT"){
                $old[$key] = ASSIST_HELPER::decode($val);
                $data[$key] = ASSIST_HELPER::decode($data[$key]);
            }
            if(isset($data[$key]) && $data[$key]!==$val){
                if($headings[$key]['type'] == "LIST"){
                    $listObject = new MDOC_LIST($headings[$key]['list_table']);
                    $changes[$key]['raw']['from'] = $val;
                    $changes[$key]['raw']['to'] = $data[$key];
                    $changes[$key]['from'] = $listObject->getAListItemName($val);
                    $changes[$key]['to'] = $listObject->getAListItemName($data[$key]);
                }else{
                    $changes[$key]['from'] = $val;
                    $changes[$key]['to'] = $data[$key];
                }
            }else{
                unset($data[$key]);
            }
        }
    //return array("info",$this->convertArrayToSQL($data));
        if(count($data)>0){
            $sql_string = $this->convertArrayToSQL($data);
            $sql = "UPDATE assist_".$this->getCmpCode()."_mdoc_documents SET $sql_string WHERE doc_id = '$id'";
            $sql = $this->db_update($sql);
            if($sql !== 0){
                $logObj = new MDOC_LOG("mdoc");
                $inserter = $this->getUserID();
                $changes['user']['raw'] = $inserter;
                $changes['user'] = $this->getAUserName($inserter);
				unset($changes['doc_status']);
                $log_var = array(
                    'object_id' => $id,
                    'parent_id' => $this->getModRef()."_".$obj_type."-".$obj_id,
                    'changes'   => $changes,
                    'log_type'  => MDOC_LOG::EDIT,
                    'status_id' => MDOC::ACTIVE,      
                );
                //$result = array("info",json_encode($log_var));
                //return $result;
                $log_result = $logObj->addObject($log_var);
                $result = array("ok","Document changes saved succesfully");
            }else{
                $result = array("error","Something went wrong whilst changing the Document. Please try again");
            }
        }else{
            $result = array("info","No changes were found to be saved");
        }    
        return $result;
    }




	/** 
	 * Returns string of selected form field
	 * 
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		$displayObj = new ASSIST_MODULE_DISPLAY();
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "STATUS":
				$data = $displayObj->getDataField($type, $val);
				break;
			case "LABEL":
				$data=$displayObj->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$displayObj->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$displayObj->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$displayObj->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$displayObj->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$displayObj->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$displayObj->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} 
				$data=$displayObj->getMultipleSelect($val2,$options,$items);
				break;
			case "DATE": //echo "date!";
				if(!is_array($options) || count($options)==0){
					//$extra['minDate'] = "0";
					$options['options'] = "";
				}else{
					$extra = $options['options'];
				}
				unset($options['options']);
				$data=$displayObj->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$displayObj->getColour($val,$options);
				break;
			case "RATING":
				$data=$displayObj->getRating($val, $options);
				break;	
			case "CURRENCY": $size = 15;
			case "PERC": 
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} 
				//ASSIST_HELPER::arrPrint($options);
                $data=$displayObj->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $displayObj->getBoolButton($val,$options);
				break;
			case "ATTACH":
				//ASSIST_HELPER::arrPrint($options);
				//$data = array('display'=>$val,'js'=>"");
			    //echo "attach: ".$val." - ".json_encode($options);
                $options['allow_multiple'] = false;
                $options['class'] = "am_required1";
				if($options['can_edit'] == false){
					$data = $displayObj->getAttachmentView($val,$options);
				}else{
					$options['can_edit'] == true;
					$data = $displayObj->getAttachmentInputOnly($val,$options);
				}
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $displayObj->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}
	

	public function setSpecialAccessForObject($objectid,$userid){
		$newExtras = json_encode($userid);	
		$sql = "UPDATE assist_".$this->getCmpCode()."_mdoc_documents SET doc_extra_access = '".$newExtras."'
				WHERE doc_object_id = $objectid 
        		AND doc_modref = '".$this->getModRef()."'
        		AND doc_status = ".$this::ACTIVE;
			
		//return $sql;	
		$result = $this->db_query($sql);		
		if($result > 0){
			return true;
		}else{
			return false;
		}	
			
		//** Here lies the stuff to select old data! **//	
		//error_reporting(-1);
        $sql = "SELECT doc_id as id, doc_extra_access as access FROM assist_".$this->getCmpCode()."_mdoc_documents mdoc
        		WHERE mdoc.doc_object_id = $objectid 
        		AND mdoc.doc_modref = '".$this->getModRef()."'
        		AND mdoc.doc_status = ".$this::ACTIVE;
        $contentsRaw = $this->mysql_fetch_one_value($sql,"access");
		if(strlen($contentsRaw) > 0){
	        $contents = json_decode($contentsRaw);
			//$newAccess 
			
			//$nuller = "FALSE";
		}else{
			//$nuller = "TRUE";
			$contents = array();
		}
        //return $userid;
        return array($nuller=>$contents);
		//$contents = array_merge($contents,$userid);
        return $sql;
    }


}



?>