<html>
<head>
    <title>MDOC Object Form</title>
</head>
<body>
<?php
    
    //-----TESTING-----//
    //echo "Hi! ID='$id' and Page Action = '$page_action'";
    
    //Fetching necessary files
    include("../inc_head.php");
    
    //Processing query string (setting up variables)
    $id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
    $page_action = isset($_REQUEST['action']) && strtoupper($_REQUEST['action']) == "EDIT"? "EDIT" : "ADD";
    $page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : "dialog";
    $formatted_object_type = $_REQUEST['object_type'];
    $raw_object_type = $_REQUEST['raw_object_type'];
    $object_id = $_REQUEST['object_id'];
    
    //Fetching data (headings)
    $headingObj = new MDOC_HEADINGS();
    $headings = $headingObj->getMainObjectHeadings("FORM");
    
    if(strlen($id)>0 && $page_action == "ADD"){
        echo "<div style='background-color:#f3f3f3; border-radius:8px; width:500px; height:200px; text-align:center'><h1>Warning</h1><p><em>MDOC ID fed in (".implode(", ",$_REQUEST).") and Page Action set to ADD!</em></p></div>";
    }else if(strlen($id)<=0 && $page_action == "EDIT"){
        echo "<div style='background-color:#f3f3f3; border-radius:8px; width:500px; height:200px; text-align:center'><h1>Warning</h1><p><em>MDOC ID not fed in (".implode(", ",$_REQUEST).") and Page Action set to EDIT!</em></p></div>";
    }else if($page_action == "EDIT"){
            //Processing the headings and turning them into html data
            $form_object_id = $id;
            $formObject = new MDOC();
            $form_object = $formObject->getRawObject($id);
            $is_edit_page = true;
            
            //-----TESTING-----//
            //ASSIST_HELPER::arrPrint($form_object);
            //echo "<br><br>";
            //ASSIST_HELPER::arrPrint($headings);
            
            //Drawing the form title
            $title = "Edit Document";
            $js = "";
            $html = "<h3>$title</h3><br>
                        <form method=post language=jscript enctype='multipart/form-data' id='mdoc_form' name='mdoc_form'>
                        <table class='form'>";
            foreach($headings as $fld=>$head){
                $val = "";
                $h_type = $head['type'];
                $h_field = $head['field'];
                if($h_type!="HEADING") {
                    $display_me = true;
                    $options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
                    if($head['required']==1) { $options['title'] = "This is a required field."; }
                    if($h_type=="SYSTEM") {
                    	$display_me = false;
                    	$hidden = true;
                        if($h_field == "doc_object_type"){
                            $val = "<input type='hidden' name='doc_object_type' value='$raw_object_type'></input>";
                        }else if($h_field == "doc_object_id"){
                            $val = "<input type='hidden' name='doc_object_id' value='$object_id'></input>";
                        }else{
                            $val = "<span style='color:red;'>".strtoupper($h_field)."</span>";
                        }
                    }else if($h_type=="SYSTEM_USER"){
                        $val = $formObject->getAUserName($form_object[$fld]);
                    }else if($h_type=="SYSTEM_DATE"){
                        $val = date("Y-m-d",strtotime($form_object[$fld]));
                    }else if($h_type=="LIST") {
                        if($display_me) {
                            $list_items = array();
                            $listObject = new MDOC_LIST($head['list_table']);
                            //ASSIST_HELPER::arrPrint($listObject);
                            //return array("display"=>$head['list_table']);
                            $list_items = $listObject->getActiveListItemsFormattedForSelect();
                            //return array("display"=>$list_items);
                            if(isset($list_items['list_num'])) {
                                $list_parent_association = $list_items['list_num'];
                                $list_items = $list_items['options'];
                                $options['list_num'] = $list_parent_association;
                            }
                            $options['options'] = $list_items;
                            if(count($list_items)==0 && $head['required']==1) {
                                $form_valid8 = false;
                                $form_error[] = "The".$head['name']." list has not been populated but is a required field.";
                                $options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
                            }
                            if($is_edit_page) {
                                $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
                            } else {
                                $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
                            }
                            unset($listObject);
                        }
                    } elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
                        $list_items = array();
                        switch($h_type) {
                            case "USER":
                                $listObject = new MASTER_USERACCESS();
                                $list_items = $listObject->getActiveUsersFormattedForSelect();
                                break; 
                            case "OWNER":
                                $listObject = new MASTER_CONTRACT_OWNER();
                                $list_items = $listObject->getActiveOwnersFormattedForSelect();
                                break;
                            case "MASTER":
                                $list_items = $formObject->getMenuModulesFormattedForSelect();
                                //Remove this filter to allow choice of module
                                foreach($list_items as $index=>$item){
                                    if($index != $formObject->getModRef()){
                                        unset($list_items[$index]);
                                    }
                                }
                                break; 
                            default:
                                echo $h_type; 
                                break;
                        }
                        $options['options'] = $list_items;
                        $h_type = "LIST";
                        if(count($list_items)==0 && $head['required']==1) {
                            $form_valid8 = false;
                            $form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
                            $options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
                        }
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
                        }
                    } elseif($h_type=="DATE") {
                        $options['options'] = array();
                        $options['class'] = "";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
                        }
                    } elseif($h_type=="CURRENCY" || $h_type=="PERC") {
                        $options['extra'] = "processCurrency";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
                        }
                    } elseif($h_type=="BOOL"){
                        $h_type = "BOOL_BUTTON";
                        $options['yes'] = 1;
                        $options['no'] = 0;
                        $options['extra'] = "boolButtonClickExtra";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
                        }
                    } elseif($h_type=="REF") {
                        if($page_action=="Add") {
                            $val = "System Generated";
                        } else {
                            $val = "<input type='hidden' name='doc_id' value='$form_object_id'></input>".$formObject->getRefTag().$form_object_id;
                        }
                    } elseif($h_type=="ATTACH") {
                    	//echo "ATTACH: ".json_encode($form_object[$fld])."<br>";
                        $options['can_edit'] = false;
                        $attachment_form = true;
                        $options['action'] = "VIEW";
                        $options['page_direct'] = $page_direct;
                        $options['object_type'] = "MDOC";
                        $options['object_id'] = $form_object_id;
                        $options['page_activity'] = "VIEW";
                        $options['buttons'] = true;
                        $val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
                        if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						$tmpval = unserialize($val);
						unset($val);
						$val[0] = $tmpval;
						$val = serialize($val);
						//echo "ATTACH: ".ASSIST_HELPER::arrPrint($val);
                    } else {
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) ? $form_object[$fld] : "";
                        } else {
                            $val = isset($head['default_value']) ? $head['default_value'] : "";
                        }
                    }
                    if($display_me) {
                        $display = $formObject->createFormField($h_type,$options,$val);
                        $js.=$display['js'];
                        if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
                            if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
                                $display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
                            }
                        }
                    }else if($hidden){
                    	$display['display'] = $val;
                    }
                } elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
                    $val = "";
                    if($h_type=="ATTACH") {
                        $val = "ATTACH";
                    } else {
                        $val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;
    
                        if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
                            $val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $formObject->getUnspecified() : $form_object[$head['list_table']]);
                        } else {
                            $v = $formObject->getDataField($head['type'], $val);
                            $val = $v['display'];
                        }
                    }
                    $display = array('display'=>$val);
    
                } else {//if($fld=="contract_supplier") {
                    if($is_edit_page) {
                        $pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
                    } else {
                        $pval = isset($head['default_value']) ? $head['default_value'] : "";
                    }//echo "<br />".$fld."::".$pval.":";
                } 
                if($display_me) {
                    $html.= "
                    <tr id=tr_".$fld.">
                        <th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
                        <td>".$display['display']."</td>
                    </tr>";
                }else if($hidden){
                    $html.= $display['display'];                	
                }
            }
            
            $html .= "</table></form>";
            $js .= $display['js'];
            echo $html;
    }else{
            $form_object_id = '';
            $formObject = new MDOC();
            $is_edit_page = false;
            //Drawing the form title
            $title = "Add Document";
            $js = "";
            $html = "<h3>$title</h3><br>
                     <form method=post language=jscript enctype='multipart/form-data' id='mdoc_form' name='mdoc_form'>
                        <table class='form'>";
            //Adding the required object info
            $html .= "<input type='hidden' name='doc_object_type' value='$raw_object_type'></input>";
            $html .= "<input type='hidden' name='doc_object_id' value='$object_id'></input>";
            foreach($headings as $fld=>$head){
                $val = "";
                $h_type = $head['type'];
                $h_field = $head['field'];
                if($h_type!="HEADING") {
                    $display_me = true;
                    $options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
                    if($head['required']==1) { $options['title'] = "This is a required field."; }
                    if($h_type=="SYSTEM") {
                        $display_me = false;
                        if($h_field == "doc_object_type"){
                            $val = "<input type='hidden' name='doc_object_type' value='$raw_object_type'></input>".$formatted_object_type;
                        }else if($h_field == "doc_object_id"){
                            $val = "<input type='hidden' name='doc_object_id' value='$object_id'></input>".$object_id;
                        }else{
                            $val = "<span style='color:red;'>".strtoupper($h_field)."</span>";
                        }
                    }elseif($h_type=="SYSTEM_DATE" || $h_type=="SYSTEM_USER") {
                        $display_me = false;
                    }else if($h_type=="LIST") {
                        if($display_me) {
                            $list_items = array();
                            $listObject = new MDOC_LIST($head['list_table']);
                            //ASSIST_HELPER::arrPrint($listObject);
                            //return array("display"=>$head['list_table']);
                            $list_items = $listObject->getActiveListItemsFormattedForSelect();
                            //return array("display"=>$list_items);
                            if(isset($list_items['list_num'])) {
                                $list_parent_association = $list_items['list_num'];
                                $list_items = $list_items['options'];
                                $options['list_num'] = $list_parent_association;
                            }
                            $options['options'] = $list_items;
                            if(count($list_items)==0 && $head['required']==1) {
                                $form_valid8 = false;
                                $form_error[] = "The".$head['name']." list has not been populated but is a required field.";
                                $options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
                            }
                            if($is_edit_page) {
                                $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
                            } else {
                                $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
                            }
                            unset($listObject);
                        }
                    } elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
                        $list_items = array();
                        switch($h_type) {
                            case "USER":
                                $listObject = new MASTER_USERACCESS();
                                $list_items = $listObject->getActiveUsersFormattedForSelect();
                                break; 
                            case "OWNER":
                                $listObject = new MASTER_CONTRACT_OWNER();
                                $list_items = $listObject->getActiveOwnersFormattedForSelect();
                                break;
                            case "MASTER":
                                $list_items = $formObject->getMenuModulesFormattedForSelect();
                                //Remove this filter to allow choice of module
                                foreach($list_items as $index=>$item){
                                    if($index != $formObject->getModRef()){
                                        unset($list_items[$index]);
                                    }
                                }
                                break; 
                            default:
                                echo $h_type; 
                                break;
                        }
                        $options['options'] = $list_items;
                        $h_type = "LIST";
                        if(count($list_items)==0 && $head['required']==1) {
                            $form_valid8 = false;
                            $form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
                            $options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
                        }
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
                        }
                    } elseif($h_type=="DATE") {
                        $options['options'] = array();
                        $options['class'] = "";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
                        }
                    } elseif($h_type=="CURRENCY" || $h_type=="PERC") {
                        $options['extra'] = "processCurrency";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
                        }
                    } elseif($h_type=="BOOL"){
                        $h_type = "BOOL_BUTTON";
                        $options['yes'] = 1;
                        $options['no'] = 0;
                        $options['extra'] = "boolButtonClickExtra";
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
                        } else {
                            $val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
                        }
                    } elseif($h_type=="REF") {
                        if(strtoupper($page_action)=="ADD") {
                            $val = "System Generated";
                        } else {
                            $val = $formObject->getRefTag().$form_object_id;
                        }
                    } elseif($h_type=="ATTACH") {
                       	$options['can_edit'] = true;
                        $attachment_form = true;
                        $options['action'] = "ADD";
                        $options['page_direct'] = "";
                        $options['object_type'] = "MDOC";
                        $options['object_id'] = $form_object_id;
                        $options['page_activity'] = "ADD";
                        $options['buttons'] = false;
                        $val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
                        if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						$tmpval = unserialize($val);
						unset($val);
						$val[0] = $tmpval;
						$val = serialize($val);
                    } else {
                        if($is_edit_page) {
                            $val = isset($form_object[$fld]) ? $form_object[$fld] : "";
                        } else {
                            $val = isset($head['default_value']) ? $head['default_value'] : "";
                        }
                    }
                    if($display_me) {
                        $display = $formObject->createFormField($h_type,$options,$val);
                        $js.=$display['js'];
                        if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
                            if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
                                $display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
                            }
                        }
                    }
                } elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
                    $val = "";
                    if($h_type=="ATTACH") {
                        $val = "ATTACH";
                    } else {
                        $val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;
    
                        if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
                            $val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $formObject->getUnspecified() : $form_object[$head['list_table']]);
                        } else {
                            $v = $formObject->getDataField($head['type'], $val);
                            $val = $v['display'];
                        }
                    }
                    $display = array('display'=>$val);
    
                } else {//if($fld=="contract_supplier") {
                    if($is_edit_page) {
                        $pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
                    } else {
                        $pval = isset($head['default_value']) ? $head['default_value'] : "";
                    }//echo "<br />".$fld."::".$pval.":";
                } 
                if($display_me) {
                    $html.= "
                    <tr id=tr_".$fld.">
                        <th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
                        <td>".$display['display']."</td>
                    </tr>";
                }
            }
            
            $html .= "</table></form>";
            $js .= $display['js'];
            echo $html;
    }
echo "<div id='div_error' style='display:none'></div>";
?>
<script type='text/javascript'>
    <?php echo $js; ?>
    $('textarea').trigger('keyup');
    window.parent.$('#popup_dialog').dialog({
                      width:560,
                      height:670,
                      modal:true,
                      buttons:{
                        Save:function(){
                            var pa = "<?php echo($page_action); ?>";
                            if (pa == 'EDIT'){
                                var res = MdocHelper.processObjectForm($('#mdoc_form'),pa,true,'<?php echo $page_direct; ?>');
                                document.write(res);
                                //console.log("refresher "+res);
                                //window.parent.$('#popup_dialog').dialog("close");
                            }else if(pa == 'ADD'){
                                var res = MdocHelper.processObjectFormWithAttachment($('#mdoc_form'),pa,'<?php echo $page_direct; ?>');
                                //console.log(res);
                                //window.parent.$('#popup_dialog').dialog("close");
                            }
                            //var dta = AssistString();
                        },
                        Delete:function(){
                            var dta = AssistForm.serialize($('#mdoc_form'));
                            var result = AssistHelper.doAjax('inc_controller.php?action=DELETE&page_direct='+'<?php echo $page_direct; ?>',dta);
                            document.write(result);
                            window.parent.$('#popup_dialog').dialog("close");
                            //$(this).dialog("close");
                        },
                        Cancel:function(){
                            window.parent.$('#popup_dialog').dialog("close");
                            //$(this).dialog("close");
                        }
                      }
                   }); 
</script>
</body>    
</html>