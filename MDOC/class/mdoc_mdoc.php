<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 23 April 2015
 * Authors: Duncan Cosser
 * 
 */
 
class MDOC_MDOC extends MDOC{
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $status_field = "_user_status";
	protected $id_field = "_id";
	protected $name_field = "_name";
	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "MDOC";
    const TABLE = "documents";
    const TABLE_FLD = "documents";
    const TABLE_USER_FLD = "documents";
    const REFTAG = "MDOC";
	const LOG_TABLE = "mdoc_log";
	/**
	 * STATUS CONSTANTS
	 */
	const CONFIRMED = 32;
	const ACTIVATED = 64;

	const FINANCE_INITIATED = 256;
	/**
	 * ASSESSMENT TYPE CONSTANTS
	 */
	const QUALITATIVE = 1024;
	const QUANTITATIVE = 2048;
	const OTHER = 4096;
    
    public function __construct($contract_id=0) {
        parent::__construct();
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->object_form_extra_js = "$('#customer_p_dob').datepicker('option','yearRange','c-100:c');";
		if($contract_id>0) {
	    	$this->object_id = $contract_id;
			$this->object_details = $this->getRawObject($contract_id);  
			//$this->arrPrint($this->object_details);
		}
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	
	
	
	public function addObject($var) {
		//Removing certificates for seperate processing
		foreach($var as  $key=>$val){
			if(strpos($key,"customer_c_")!== false){
				unset($var[$key]);
				$certs[$key]=$val;
			}
		} 
		$var['customer_c_certificate'] = count($certs)>0?1:0;
//return (array("info",$this->processCertificates($certs,17)));
		unset($var['attachments']);
		unset($var['contract_id']);	//remove incorrect contract_id from generic form
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		unset($var['customer_status_id']);	//remove incorrect contract_id from generic form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_status'] = MASTER::ACTIVE;
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
//return array("info",$sql);
		$id = $this->db_insert($sql);
		//Processing of certificates with the reference ID number from the INSERT
		if($id>0) {
			//$certs = $this->processCertificates($certs,$id);
			//if($certs['failer']==true){
				//return array("error","Customer saved, but certificate(s) not saved. Try again and then Contact your assist Administrator. Error code:".json_encode($certs['reason']));
			//}
			$changes = array(
				'response'=>"|contract| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			//$this->addActivityLog("contract", $log_var);
			$result = array(
				"info",
				"New ".$this->getContractObjectName()." has been successfully created with reference number ".$this->getRefTag().$id.".",
				"add_log"=>false,
				"object_id"=>$id
				//'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$contract_id = $var['contract_id'];
		/* New stuff for SMS notifications /*/
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		$sms = $var['sms']==1?"BOTH":"MAIL";
		unset($var['sms']);
		/*/ New stuff for SMS notifications */
		$sql = "SELECT ".$this->getStatusFieldName()." as status, contract_authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old['status'];
		$new_status = $old_status + self::CONFIRMED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| confirmed.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::CONFIRM,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog("contract", $log_var);
			//EMAIL TO NOTIFY AUTHORISER
			//$contract_authoriser = $old['contract_authoriers'];
			return array("ok","".$this->getContractObjectName()." ".self::REFTAG.$contract_id." confirmed successfully.  It has gone to UUUUU for activation.");
		} else {
			return array("error","An error occurred while trying to confirm ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	
	
	public function activateObject($var) {
		$contract_id = $var['contract_id'];
		/* New stuff for SMS notifications /*/
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		$sms = $var['sms']==1?"BOTH":"MAIL";
		unset($var['sms']);
		/*/ New stuff for SMS notifications */
		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old['status'];
		$new_status = $old_status + self::ACTIVATED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| activated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::ACTIVATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog("contract", $log_var);
			//return array("info",$note);
			return array("ok","".$this->getContractObjectName()." ".self::REFTAG.$contract_id." activated successfully. It is now available in Manage and Admin.");
		} else {
			return array("error","An error occurred while trying to activate ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
		
	public function deactivateObject($var) {
		$contract_id = $var['object_id'];

		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old_status = $this->mysql_fetch_one_value($sql,"status");
		$new_status = $old_status - self::ACTIVE + self::INACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::DEACTIVATE,
				'status_id'	=> MASTER::INACTIVE,		
			);
			$this->addActivityLog("contract", $log_var);
			return array("ok","Deactivation of ".$this->getContractObjectName()." ".self::REFTAG.$contract_id." successful. It is no longer available in Manage.");
		} else {
			return array("error","An error occurred while trying to deactivate ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	public function restoreObject($var) {
		$contract_id = $var['object_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old_status = $this->mysql_fetch_one_value($sql,"status");
		$new_status = $old_status - self::INACTIVE + self::ACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| restored.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::RESTORE,
				'status_id'	=> MASTER::ACTIVE,		
			);
			$this->addActivityLog("contract", $log_var);
			return array("ok","Restoration of ".$this->getContractObjectName()." ".self::REFTAG.$contract_id." successful. It is available in Manage.");
		} else {
			return array("error","An error occurred while trying to restore ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	
	public function deleteObject($var) {
		$contract_id = $var['object_id'];

		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old_status = $this->mysql_fetch_one_value($sql,"status");
		$new_status = $old_status - self::ACTIVE + self::DELETED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::DELETE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$this->addActivityLog("contract", $log_var);
			return array("ok","Deletion of ".$this->getContractObjectName()." ".self::REFTAG.$contract_id." successful. It is no longer available in the module.");
		} else {
			return array("error","An error occurred while trying to delete ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$progress = $var[$p_field];
		$status_id = $var[$si_field];
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$update_data = array(
			$p_field=>$progress,
			$si_field=>$status_id,
		);
		if($status_id == 3 && $progress == 100 && (strtotime($var['action_on']) != strtotime($old_data[$dc_field]))) {
			$update_data[$dc_field] = date("Y-m-d",strtotime($var['action_on']));
			$c[$dc_field] = array('to'=>date("Y-m-d",strtotime($var['action_on'])),'from'=>$old_data[$dc_field]);
		}
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		
		if($mar>0) {
			$changes = array(
				'response'=>$var['response'],
				'user'=>$this->getUserName(),
				'action_on'=>$var['action_on'],
			);
			if($old_data[$p_field] != $progress) {
				$changes[$p_field] = array('to'=>$progress,'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $status_id) {
				$listObject = new MASTER_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($status_id,$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$status_id],'from'=>$items[$old_data[$si_field]]);
			}
			if(isset($c[$dc_field])) {
				$changes[$dc_field] = $c[$dc_field];
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getUpdateAttachmentFieldName()] = $x;
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::UPDATE,
				'progress'	=> $progress,
				'status_id'	=> $status_id,		
			);
			$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
	//		ASSIST_HELPER::arrPrint($note);
	//		return array("info",$note);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");
		}
		
		return array("error","An error occurred.  Please try again.");
		
		
	}
	
	public function editObject($var,$attach=array()) {
		//unset($var['attach']);	//remove incorrect contract_id from generic form
		$object_id = $var['object_id'];
		
		if($this->checkIntRef($object_id)) {
			//$old_object = $this->getRawObject($object_id);
			$result = $this->editMyObject($var); 
		 	return $result;
		}
		return array("error","Please notify your Assist Administrator that something broke: ".json_encode($var));
		//return array("error","An error occurred.  Please try again.");
			 
		//return $result;
	}	
	
	

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField($fld="") {
    	if(strlen($fld)>0){
    		return self::TABLE_USER_FLD;
    	}else{
	    	return self::TABLE_FLD; 
    	}	
	}
    public function getTableName() { return "assist_".$this->getCmpCode()."_mdoc_".self::TABLE; }
    public function getTablePref() { return "assist_".$this->getCmpCode()."_mdoc"; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return "assist_".$this->getCmpCode()."_".self::LOG_TABLE; }
	public function getManagerFieldName() {
		return $this->manager_field;
	}

	public function getActiveSQLScript($tn = "MDOC") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."customer_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
	public function getList($section,$options=array()) {
		return $this->getMyList("MDOC", $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject("MDOC", $id,$options);
	}

	public function getSubObjects($id=0) {
		$sql = 	"";
		return $this->getDetailedObject("MDOC", $id,array("compact_view"=>true));
	}
	
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE customer_id = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		//moved to finance section
		//$csObject = new MASTER_CONTRACT_SUPPLIER();
		//$data['contract_supplier'] = $csObject->getRawObjectByContractID($obj_id);

		return $data;
	}
	/***
	 * Returns an unformatted array of an object with the Certificates loaded
	 */
	public function getRawCertObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE customer_id = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		$sql2 = "SELECT * FROM ".$this->getDBRef()."_certificates WHERE customer_c_status = 1 AND customer_c_parent = ".$obj_id;
		$data2 = $this->mysql_fetch_all($sql2);
		if(count($data2) > 0){
			$data['customer_c_certificate'] = $data2;
		}
		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}	
	
	
	public function getSummary($contract_id=0) {
		if($contract_id==0) { $contract_id = $this->object_id; }
		$delObject = new MASTER_DELIVERABLE();
			$del_id = $delObject->getIDFieldName();
			$del_status = $delObject->getStatusFieldName();
			$del_parent = $delObject->getParentFieldName();
		$actObject = new MASTER_ACTION();
			$act_id = $actObject->getIDFieldName();
			$act_status = $actObject->getStatusFieldName();
			$act_parent = $actObject->getParentFieldName();
		$sql = "SELECT D.".$delObject->getTableField()."_type as type, count(D.".$del_id.") as dels
				, AVG( D.del_progress ) as del_prog
				FROM ".$delObject->getTableName()." D 
				WHERE D.".$del_status." & ".MASTER::DELETED." <> ".MASTER::DELETED." AND D.".$del_parent." = ".$contract_id." 
				GROUP BY D.".$delObject->getTableField()."_type"; 
				//echo $sql;
		$results = $this->mysql_fetch_all_by_id($sql, "type");  //$this->arrPrint($results);
		$sql = "SELECT D.".$delObject->getTableField()."_type as type, count(A.".$act_id.") as actions
				, AVG( A.action_progress ) as action_prog
				FROM ".$delObject->getTableName()." D 
				LEFT OUTER JOIN ".$actObject->getTableName()." A ON A.".$act_parent." = D.".$del_id." AND A.".$act_status." & ".MASTER::DELETED." <> ".MASTER::DELETED." 
				WHERE D.".$del_status." & ".MASTER::DELETED." <> ".MASTER::DELETED." AND D.".$del_parent." = ".$contract_id." 
				GROUP BY D.".$delObject->getTableField()."_type"; 
				//echo $sql;
		$a_results = $this->mysql_fetch_all_by_id($sql, "type");  //$this->arrPrint($results);
				
		$types = $delObject->getAllDeliverableTypes();
		$res = array();	
		foreach($types as $t => $x){
			$d = isset($results[$t]) ? $results[$t]['dels'] : 0; 
			$a = isset($a_results[$t]) ? $a_results[$t]['actions'] : 0; 
			$ap = isset($a_results[$t]) && !is_null($a_results[$t]['action_prog']) ? $a_results[$t]['action_prog'] : 0;
			$dp = isset($results[$t]) && !is_null($results[$t]['del_prog']) ? $results[$t]['del_prog'] : 0;
			$res[$t] = array('deliverable'=>$d,'action'=>$a,'action_prog'=>$ap,'del_prog'=>$dp);
		}
		return $res;	
	}
    
     
	public function getRecipients($id,$action){
		switch (strtoupper($action)) {
			case 'SPECIAL':	
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new MASTER_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				$tkid['manager']=$manager;
				$tkid['authoriser']=$authoriser;
				$tkid['owner']=$owners;
				break;
			case 'UPDATE':
			case 'EDIT':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new MASTER_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				foreach($owners as $key=>$val){
					$tkid[]=$key;
				}
				$tkid[]=$manager;
				$tkid[]=$authoriser;
				break;
			case 'NEW':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new MASTER_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$usrxs = new MASTER_USERACCESS();
				//If they have create deliverable or action access
				$manager = $usrxs->canICreateDeliverables($res['manager'])?$res['manager']:"";
				if($res['authoriser'] != $res['manager']){
					$authoriser=$usrxs->canICreateDeliverables($res['authoriser'])?$res['authoriser']:"";
				}else{
					$authoriser="";
				};
				foreach($owners as $key=>$val){
					if($val['can_create_deliverable'] == true){
						$tkid[]=$key;
					}
				}
				$tkid[]=$manager;
				$tkid[]=$authoriser;
				break;
			case 'ACTIVATE':
			case 'DEACTIVATE':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new MASTER_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$usrxs = new MASTER_USERACCESS();
				//If they have create deliverable or action access
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				foreach($owners as $key=>$val){
					$tkid['Owner']=$key;
				}
				$tkid['Manager']=$manager;
				$tkid['Authoriser']=$authoriser;
				//Fetch all of the Action & Deliverable owners
				$sql = "SELECT del_id as id, del_owner as tkid FROM ".$this->getRootTableName()."_deliverable WHERE del_contract_id = $id";
				$dels = $this->mysql_fetch_all($sql);
				//echo json_encode($dels);
				if(count($dels)>0){
					foreach($dels as $index=>$thing){
						$del_ids[]=$thing['id'];
						$del_owns[]=$thing['tkid'];
					}
					$sql = "SELECT action_owner as tkid FROM ".$this->getRootTableName()."_action WHERE action_deliverable_id IN (".implode(",",$del_ids).")";
					$acts = $this->mysql_fetch_all($sql);
					$del_owner_fld = $this->getObjectName("Deliverable")." Owner";
					$tkid[$del_owner_fld]=$del_owns;
				}
				if(count($acts)>0){
					foreach($acts as $index=>$thing){
						$act_owns[]=$thing['tkid'];
					}
					$act_owner_fld = $this->getObjectName("Action")." Owner";
					$tkid[$act_owner_fld]=$act_owns;
				}
				break;
			case 'CONFIRM':
				$sql = "SELECT ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$authoriser = $res['authoriser'];
				$tkid['Authoriser']=$authoriser;
				break;
		}	
		return $tkid;
	}
	 
     
	/***************************
	 * STATUS SQL script generations
	 */
	 
	/**
	 * Returns status check for Customers to display on Manage > Edit page (inactive as well)
	 */
	public function getBothStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("BOTH",$t,false);
	}
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a contract
	 */ 
	public function getDeadlineDate() {
		return $this->object_details['contract_date_completed'];
	}
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	public function doIHaveSubDeliverables() {
		//echo ($this->object_details['contract_have_subdeliverables']*1)." => ".($this->object_details['contract_have_subdeliverables']*1==1);
		//$this->arrPrint($this->object_details);
		if($this->object_details['contract_have_subdeliverables']*1==1){
			return true;
		}
		return false;
	} 
	public function doIHaveDeliverables() {
		//if($this->object_details['contract_have_deliverables']*1==1){
			return true;
		//}
		//return false;
	} 
	public function mustIDoAssessment($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_do_assessment']*1; }
		if($x==1){
			return true;
		}
		return false;
	} 
	public function mustIAssignWeights($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_weights']*1; }
		if($x==1){
			return true;
		}
		return false;
	}
	public function mustIAssessQuality($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::QUALITATIVE) == self::QUALITATIVE){
			return true;
		}
		return false;
	} 
	public function mustIAssessQuantity($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::QUANTITATIVE) == self::QUANTITATIVE){
			return true;
		}
		return false;
	} 
	public function mustIAssessOther($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::OTHER) == self::OTHER){
			return true;
		}
		return false;
	} 
	public function canIConfirm($contract_id=0) {
		$delObject = new MASTER_DELIVERABLE();
		$actObject = new MASTER_ACTION();
		$sql = "SELECT D.".$delObject->getIDFieldName().", count(A.".$actObject->getIDFieldName().") as actions 
				FROM ".$delObject->getTableName()." D 
				LEFT OUTER JOIN ".$actObject->getTableName()." A
				ON A.".$actObject->getParentFieldName()." = D.".$delObject->getIDFieldName()." AND A.".$actObject->getStatusFieldName()." & ".MASTER::DELETED." <> ".MASTER::DELETED."
				WHERE D.".$delObject->getParentFieldName()." = $contract_id
				AND D.".$delObject->getStatusFieldName()." & ".MASTER::DELETED." <> ".MASTER::DELETED."
				ORDER BY count(A.".$actObject->getIDFieldName().")";
		$results = $this->mysql_fetch_all($sql);
		if(count($results)==0) {
			return false;
		} else {
			foreach($results as $d) {
				if($d['actions']==0) {
					return false;
				}
			}
		}
		return true;
	} 
	 

    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>