<?php
//error_reporting(-1);
require_once("../../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.");

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
$my_action = explode(".",$_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);


switch($class) {
	case "DISPLAY":
		$object = new MDOC_DISPLAY();
		break;
	/**
	 * Log object
	 */
	case "LOG":
		$log_table = $_REQUEST['log_table'];
		unset($_REQUEST['log_table']);
		$object = new MDOC_LOG($log_table);
		break;
	/**
	 * Setup objects
	 */
	case "USERACCESS":
		$object = new MDOC_USERACCESS();
		break;
	case "LISTS":
		$list_id = $_REQUEST['list_id'];
		unset($_REQUEST['list_id']);
		$object = new MDOC_LIST($list_id);
		break;
	case "MENU":
		$object = new MDOC_MENU();
		break;
	case "NAMES":
		$object = new MDOC_NAMES();
		break;
	case "HEADINGS":
		$object = new MDOC_HEADINGS();
		break;
	case "PROFILE":
		$object = new MDOC_PROFILE();
		break;
	/**
	 * Module objects
	 */
	case "MDOC":
		$object = new MDOC();
		break;

}
//$result[1].=":".$my_action.":";
if(isset($object)) {
	switch($activity) {
		case "GET_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = 'all';
			//$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			//ASSIST_HELPER::arrPrint($attach);
			$folder = $object->getParentFolderPath();
			$old = $folder."/".$attach['location']."/".$attach['system_filename'];
			//echo $old ."<br>";
			$new = $attach['original_filename'];
			//echo $new;
			$object->downloadFile3($old,$new);
			break;
	}
}

?>