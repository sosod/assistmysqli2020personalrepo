<?php
//error_reporting(-1);
require_once '../../module/autoloader.php';
$helper = new ASSIST_MODULE_HELPER();
$object = new MDOC();
//ASSIST_HELPER::arrPrint($_REQUEST);    
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : false;
unset($_REQUEST['action']);
$has_attachments = isset($_REQUEST['has_attachments']) ? $_REQUEST['has_attachments'] : false;
unset($_REQUEST['has_attachments']);
$page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : false;
unset($_REQUEST['page_direct']);
//$data = $_REQUEST;
$activity_log_handled_by_class = array("USERACCESS","LISTS","MENU","NAMES");
$result = array("info","I can't figure out what you want me to do.");

	//$result = array("info",$page_direct);
	//echo "<script type=text/javascript>alert('".$page_direct."');</script>";
	//return $result;

switch(strtoupper($action)){
    case "DELETE_ATTACH":
        $object_id = $_REQUEST['object_id'];
        $i = $_REQUEST['i'];
        $page_activity = $_REQUEST['activity'];
        $attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
        $folder = $object->getParentFolderPath();
        $old = $folder."/".$attach['location']."/".$attach['system_filename'];
        $dest_folder = $object->getDeletedFolder();
        if(file_exists($old)) {
            $new_path = $dest_folder."/".$object->getDeletedFileName($class,$attach['system_filename']);
            if(copy($old,$new_path)) {
                unlink($old);
            }
            //activity log
            $result = $object->deleteAttachment($object_id,$i,$page_activity);
            //$result = array("info","file_exists");
        } else {
            $result = array("error","File could not be found.  Please reload the page and try again.",$old);
        } 
        //$result = array("error",$page_activity);
        break;
    case "GETLISTTABLEHTML":
        unset($_REQUEST['options']['set']);
        $button = $_REQUEST['button'];
        $data = $object->getObject($_REQUEST['options']);
        //$result = $data;
        //$result = array(serialize($data));
        $displayObj = new CNTRCT_DISPLAY();
        $display = $displayObj->getListTableRows($data, $button);
        $result = array('display'=>$display['display'],'paging'=>$data['paging'],'spage'=>serialize($data['paging']));
        break;
    case "GET":
        //if($class=="LOG") {
        //  $result = array("ok","log.get");
        //} else {
            $result = $object->getObject($_REQUEST);
        //}
        break;
    case "ADD":
        $data = $object->addObject($_REQUEST);
           // $result = $data;
           // break;
        if($data[0]=="ok") {
            if($has_attachments) {
                $attach = processAttachments($action, $object, $data);
                //attachments aren't logged on add - only the fact that the object was created is logged
                //$data['log_var']['changes']['attachments'] = $attach['attachments'];
            //} else {
            }
            $result = $data;
            //No logging happening on add just yet
            /*
            if(!in_array($class,$activity_log_handled_by_class) && (!isset($data['add_log']) || $data['add_log']===true)) {
                $object->addActivityLog(strtolower($object->getMyLogTable()),$data['log_var']);
            }
            */
        } else {
            $result = $data;
        }
        break;
    case "SIMPLEADD":
        $result = $object->addObject($_REQUEST);
        break;
    case "SIMPLEEDIT":
        $result = $object->editObject($_REQUEST,$attach);
        break;
    case "EDIT":
        if($has_attachments) {
            $res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
            $attach = $res['attachments'];
        }
        $result = $object->editDocument($_REQUEST);
        break;
    case "UPDATE":
        if($has_attachments) {
            $res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
            $attach = $res['attachments'];
        }
        $result = $object->updateObject($_REQUEST,$attach);
        break;
    case "APPROVE":
        $result = $object->approveObject($_REQUEST);
        break;
    case "DECLINE":
        $result = $object->declineObject($_REQUEST);
        break;
    case "UNLOCK_APPROVE":
        $result = $object->unlockApprovedObject($_REQUEST);
        break;
    case "UNLOCK":
        $result = $object->unlockObject($_REQUEST);
        break;
    case "DELETE":
        $result = $object->deleteObject($_REQUEST);
        break; 
    case "DEACTIVATE":
        $result = $object->deactivateObject($_REQUEST);
        break; 
    case "RESTORE":
        $result = $object->restoreObject($_REQUEST);
        break;
    case "RECIPIENTS":
        $result = $object->getObjectRecipients($_REQUEST);
        break;
    /*
    default:
        //$result[5] = "unknown activity - using call_user_func($class , $activity , ".implode(":",$_REQUEST).")";
        $result = call_user_func(array($object, $activity),$_REQUEST);
        break; 
    case "EDIT":
        $result = $mDocObj->editDocument($data);
        break;
    */
    default:
        $result=array("error","you fed in something weird: Action='$action'");
        break;
}

//echo json_encode($result);
//return;
//echo json_encode($has_attachments);
if($has_attachments==false) {
    $page_direct.="&r[]=".$result[0]."&r[]=".$result[1];
	echo ("<script type=text/javascript>window.parent.location.href = '".$page_direct."';</script>");
    //echo json_encode($result);
    //echo $json_encode($_REQUEST);
} else {
        $page_direct .= "&r[]=".$result[0]."&r[]=".$result[1];
        echo "<script type=text/javascript>window.parent.parent.location.href = '$page_direct';</script>";
}

/*************** Attachment processing ******************/
function processAttachments($action,$object,$data) {
    $mdoc_id = $data['mdoc_id'];
    $object_id = $data['object_id'];
    $object_type = $data['object_type'];
    $modref = $data['modref'];
    //objecttypeObjectid_'mdoc'mdocid_timestamp
    $default_system_filename = $object_type.$object_id."_mdoc".$mdoc_id."_".date("YmdHis");
    $result = array();
            $original_filename = "";
            $system_filename = "";
            if(isset($_FILES)) {
                $result[] = "files are set";
                $save_folder = $object->getStorageFolder($modref);
                $object->checkFolder($save_folder);
                $result[] = $save_folder." folder check complete";
                $folder = $object->getFullFolderPath($modref);
                $result[] = $folder;
                //Check for existing attachments
                switch($action) {
                    case "ADD":
                        $i = 0;
                        $current_attach = array();
                        break;
                    case "UPDATE":
                    case "EDIT":
                        $current_attach = $object->getAttachmentDetails($mdoc_id,"all",$action);
                        if(count($current_attach)>0) {
                            $keys = array_keys($current_attach);
                            $i = max($keys);
                            $i++;
                        } else {
                            $i = 0;
                        }
                        break;
                }
                $new_attach = array();
                /*$record = $object->getUpdateRecord($obj_id,$time_id);
                if(!isset($record['attachment']['attach']) || count($record['attachment']['attach'])==0) {
                    $i = 0;
                } else {
                    foreach($record['attachment']['attach'] as $i=>$a) {
                        //
                    }
                    $i++;
                }*/
                $result[] = $i;
                //ASSIST_HELPER::arrPrint($_FILES);
                foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
                    $x = array('id'=>$key,'tmp_name'=>$tmp_name);
                    $x['error'] = $_FILES['attachments']['error'][$key];
                    if($_FILES['attachments']['error'][$key] == 0) {
                        $original_filename = $_FILES['attachments']['name'][$key];
                        $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                        $parts = explode(".", $original_filename);
                        $file = $parts[0];
                        $system_filename = $default_system_filename.".".$ext; 
                        $x['sys'] = $system_filename;
                        $full_path = $folder."/".$system_filename;
                        $x['path'] = $full_path;
                        $new_attach[$i] = array('status'=>CNTRCT::ACTIVE,'location'=>$save_folder,'system_filename'=>$system_filename,'original_filename'=>$original_filename,'ext'=>$ext);
                        $x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                        $i++;
                        $result[] = $x;
                    }
                }
                $result['attachments'] = $new_attach;
                $object->saveAttachments($action,$mdoc_id,array_merge($current_attach,$new_attach));
            }
    $object->arrPrint($result);
    return $result;
}


function getSaveFolder($object) {
    return $object->getStorageFolder();
}

function getFullFolderPath($object) {
    $folder = $object->getFullFolderPath();
    return $folder; 
}


function getDeletedFolder($object) {
    $folder = $object->getDeletedFolder();
}


function getParentFolderPath($object) {
    $folder = $object->getParentFolderPath();
    return $folder; 
}
?>