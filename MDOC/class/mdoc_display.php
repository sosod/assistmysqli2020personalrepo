<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class MDOC_DISPLAY extends ASSIST_MODULE_DISPLAY {
    
    
	protected $default_attach_buttons = true;
	
    public function __construct() {
    	$me = new MDOC();
		//$an = $me->getAllActivityNames();
		//$this->activity_names = $me->getAllActivityNames();
		$this->cmpcode = $me->getCmpCode();
		unset($me);
    	
        parent::__construct();
    }
    
	public function disableAttachButtons() { $this->default_attach_buttons = false; }
	public function enableAttachButtons() { $this->default_attach_buttons = true; }
	
	
	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */
	
	
	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		$d = $this->getDataField($type,$val,$options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 * 
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		//$myObject = new MASTER_CONTRACT();	
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val); 
				break;;
			case "CURRENCY":
				if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
				$data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "STATUS":
				$data['raw']=$val;
				$val *= 1;
				if(($val & MASTER::INACTIVE) == MASTER::INACTIVE){
					$val = "Inactive";
				}else if(($val & MASTER::ACTIVE) == MASTER::ACTIVE){
					$val = "Active";
				}else{
					//$val = $myObject->getUnspecified();
					$val = "System Generated";
				}
				$data['display'] = $val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display'=>$val,'js'=>"");
				//$data = array('display'=>$type);
		}
		return $data;
	}
	
    
	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		//echo "dFF VAL:-".$val."- ARR: "; print_r($options);
		$ff = $this->createFormField($type,$options,$val);
		//if(is_array($ff['display'])) {
			//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}
	/** 
	 * Returns string of selected form field
	 * 
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "STATUS":
				$data = $this->getDataField($type, $val);
				break;
			case "LABEL":
				$data=$this->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$this->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$this->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} 
				$data=$this->getMultipleSelect($val2,$options,$items);
				break;
			case "DATE": //echo "date!";
				if(!is_array($options) || count($options)==0){
					//$extra['minDate'] = "0";
					$options['options'] = "";
				}else{
					$extra = $options['options'];
				}
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;	
			case "CURRENCY": $size = 15;
			case "PERC": 
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			case "ATTACH"://echo "attach: ".$val;
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInput($val,$options);
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $this->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /**
	 * (ECHO) Function to display the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$data = $this->getPageFooter($left,$log_table,$var,$log_id);
		echo $data['display'];
		return $data['js'];
    }
    /**
	 * Function to create the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
		if(strlen($log_table)>0){
			$js .= "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
    }
 	
	
	
	
	
	
	
	
	
	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */
	
	
	
	
	/*****
	 * returns the details table for the object it is fed.
	 * 
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 * 
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new MASTER_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new MASTER_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new MASTER_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new MASTER_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id,false,false,false,array(),false,false);
	}
	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons="X") {
		if($attachment_buttons=="X") { $attachment_buttons = $this->default_attach_buttons; }
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button,$sub_table,$view_all_button,$button,$compact_view,$attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}
	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons=true) {
		//echo $object_type." :: ".$object_id;
		$cntrct = new MDOC();
		$listObj = new MDOC_LIST();
		$js = "";
		$echo = "";
		$myObject = new MDOC();
		$object = $myObject->getDetailedObject($object_id);

			$echo.="
			<h2>".$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					//ASSIST_HELPER::arrPrint($object['head']);
					//ASSIST_HELPER::arrPrint($object['head']);
					foreach($object['head'] as $fld => $head) {
						if(is_numeric($fld)){
							if($head['name']=='Certificates'){
								$val = "<table class=th2 width=100%>";
								if(is_array($subs['customer_c_certificate']) && count($subs['customer_c_certificate'])>0){
									foreach($subs['customer_c_certificate'] as $key=>$tmp){
										foreach($head['sub'] as $sub_head){
											$field = $sub_head['h_field'];
											$val .="<tr><th width=40%>".$sub_head['h_client']."</th><td>".(strlen($subs['customer_c_certificate'][$key][$field])>0?$subs['customer_c_certificate'][$key][$field]:$myObject->getUnspecified())."</td></tr>";
										}
									$val .= $tmp!==end($subs['customer_c_certificate'])?"<tr class='noborder'><td colspan=2>&nbsp;</td></tr>":"";
									}
								}else{
									$head['sub'] = isset($head['sub'])?$head['sub']:array();
									foreach($head['sub'] as $sub_head){
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$myObject->getUnspecified()."</td></tr>";
									}
								}	
								$val .= "</table>";
							}else{
								$val = "<table class=th2 width=100%>";
								$tmp = isset($head['sub'])?$head['sub']:array();	
						//ASSIST_HELPER::arrPrint($tmp);
								foreach($tmp as $sub_head){
									$field = $sub_head['h_field'];
									if($sub_head['h_type']=="LIST"){
			//echo $sub_head['h_table'];	
										$listObj->changeListType($sub_head['h_table']);
										$value = $subs[$field]>0?$listObj->getAListItemName($subs[$field]):$myObject->getUnspecified();
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$value."</td></tr>";
									}else if($sub_head['h_type']=="MASTER"){
										$hdlist = strtoupper($sub_head['h_table']);
										$listObject = new $hdlist();
										$value = $subs[$field]>0?$listObject->getAListItemName($subs[$field]):$myObject->getUnspecified();
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$value."</td></tr>";
									}else if($sub_head['h_type']=="BOOL"){
										$subs[$field] = isset($subs[$field])?$subs[$field]:0;
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$this->getBoolForDisplay($subs[$field])."</td></tr>";
									}else if($sub_head['h_section']=="CONTRACT_CERT"){
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".(strlen($subs[$field])>0?$subs[$field]:$myObject->getUnspecified())."</td></tr>";
									}else if($sub_head['h_type']=="ATTACH"){
										$subs[$field] = isset($subs[$field])?$subs[$field]:0;
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$this->getAttachForDisplay($subs[$field],false,"CONTRACT",$object_id)."</td></tr>";
									}else{
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".(strlen($subs[$field])>0?$subs[$field]:$myObject->getUnspecified())."</td></tr>";
									}
								}
								$val .= "</table>";
							}
						}else if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else {
							$val = $fld;
						}
						$echo.= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".(strlen($val)>0?$val:$myObject->getUnspecified())."</td>
						</tr>";
					}
					//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button)>0){
				$echo.="
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}		
			$echo.="</table>";
			
		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
	}
	
		
















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */



	
	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array()) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier'><span id='$i'>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
						<!-- <span class=float>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			} 
		$data['js'] = "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';
		
		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i." button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}
	
	
	/*****
	 * List View
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false) {
		//ASSIST_HELPER::arrPrint($child_objects['rows']);
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false) {
		//ASSIST_HELPER::arrPrint($child_objects); 
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		//$items_total = $contractObject->getPageLimit();
		//$these_items = array_chunk($child_objects['rows'], $items_total);
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button===false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager']===false) {
			$display_paging = false;
		}
		if($display_paging){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		unset($button['pager']);
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}
					
				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			//$data['display'].=ASSIST_HELPER::arrPrint($child_objects['rows']);
			$data['js'].=$rows['js'];
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		//ASSIST_HELPER::arrPrint($child_objects['rows']);
		if(count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				//$data['display'].="<tr>";
				$is_inactive = isset($obj['customer_user_status']['raw']) && (($obj['customer_user_status']['raw'] & MASTER::INACTIVE)==MASTER::INACTIVE);
				$data['display'].=$is_inactive?"<tr class='inact'>":"<tr>";
				foreach($obj as $fld=>$val) {
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
					if($is_inactive){
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='Restore' ref=".$id." class='restore_btn' /></td>" : "");
					}else{
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
					}
				$data['display'].="</tr>";
			}
		}
		//$data['js'].="$('.inactive_tr');";
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag) 
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page) 
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0) {
		//$form_object_type = "CUSTOMER";    
		$cert_counter = 0;
		$headingObject = new MASTER_HEADINGS();
		
		$th_class = "";
		$tbl_class = "";
		
		$attachment_form = false;
		
		$data = array('display'=>"",'js'=>"");
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		$head_object_type = $form_object_type;
		$fld_prefix = $formObject->getTableField()."_";
		$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,"FORM","NEW",$fld_prefix));
		$headings['tabs'][0]="General";
		foreach($headings['sub'] as $index=>$item){
			foreach($item as $key=>$val){
				$field = $val['field'];
				$headings['sub'][$index][$field]=$val;
				unset($headings['sub'][$index][$key]);
			}
			
		}
		foreach($headings['rows'] as $key=>$val){
			if(is_numeric($key) && $key!=11){
				$headings['rows'][$key]['subs']=isset($headings['sub'][$key])?$headings['sub'][$key]:"";
				$headings['tabs'][$key]=$val['name'];
			}
		}
		unset($headings['sub']);
		//return array("display"=>ASSIST_HELPER::arrPrint($headings));
		
//	ASSIST_HELPER::arrPrint($headings);
//	ASSIST_HELPER::arrPrint($formObject);
		
		$pa = ucwords($form_object_type).".".$page_action;
		$pd = $page_redirect_path;
		
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE) || (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		$is_add_page = (strrpos(strtoupper($page_action),"ADD")!==FALSE);
		$is_update_page = FALSE;
		$is_copy_page = (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		
		$copy_protected_fields = array();
		$copy_protected_heading_types = array();
		
		//return array("display"=>ASSIST_HELPER::arrPrint($headings));
		
		if($is_edit_page) {
			$form_object = $formObject->getRawCertObject($form_object_id);
			$form_activity = "EDIT";
			$echo.="<input type='hidden' name='object_id' value=".$form_object_id." />";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
			}
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);

		$js.="
				var page_action = '".$pa."';
				var page_direct = '".$pd."';
				
		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);
		$echo.="
			<div id=div_error class=div_frm_error>
				
			</div>
			".($display_form ? "<!-- <form name=frm_object method=post language=jscript enctype=\"multipart/form-data\"> -->" : "");
			//."
				//<tablee class='form $tbl_class' width=100% id=tbl_object_form_".$tbl_id.">";
			$form_valid8 = true;
			$form_error = array();
		
		//Drawing div for Tabs
		$echo.="<div id='tab_div'><ul>";
		foreach($headings['tabs'] as $key=>$val){
			$echo.="<li><a href='#tabs-".$key."'>".$val."</a></li>";	
		}
		$echo.="</ul>";
		foreach($headings['tabs'] as $key=>$value){
			//General headings	
			if($key == '0' || $key == 0){
	//ASSIST_HELPER::arrPrint($headings['rows']);
				$echo.="<div id='tabs-".$key."'>";//<form id='new_".$key."' class='new_frm_object'>
				$echo.="<table class='tbl-container'><tr><td><form name='section_".$key."' enctype='multipart/form-data' language='jscript' method='post'><table class='form'>";
				foreach($headings['rows'] as $fld=>$head){
					if(!is_numeric($fld)){
						//$echo.="<li>".$item['name']."</li>";
						
						
						//JUNGLE IS MASSIVE//
						
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING" && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($display_me) {
								$list_items = array();
								$listObject = new MASTER_LIST($head['list_table']);
								//return array("display"=>$head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//return array("display"=>$list_items);
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MASTER_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "OWNER":
									$listObject = new MASTER_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									//$listObject = new MASTER_MASTER($head['list_table']);
									$hdlist = strtoupper($head['list_table']);
									$listObject = new $hdlist();
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$data['js'].=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);

					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
					} 
					if($display_me) {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."</td>
						</tr>";
					}
						
						//JUNGLE IS DONE BEING MASSIVE//
					}
				//$echo.="</table>";
				}
				$echo.="</table></form></td></tr></table></div>";
			}else{
			//Tabbed headings
				$echo.="<div id='tabs-".$key."'>";
				$echo.="<table class='tbl-container'><tr><td><form name='section_".$key."' enctype='multipart/form-data' language='jscript' method='post'><table class='form' id=tbl_object_form_".$key.">";	
				//$echo.="<form class='new_frm_object' id='new_".$key."'><table class='tbl-container'><tr><td><table class='form' id=tbl_object_form_".$key.">";	
				foreach($headings['rows'][$key]['subs'] as $fld=>$head){
		//ASSIST_HELPER::arrPrint($headings['rows']);
					//$echo.="<h4>".$head['name']."</h4>";	
					//MASSIVE FORM PROCESSING STARTS HERE//
					
						
					$val = "";
					$h_type = $head['type'];
		//ASSIST_HELPER::arrPrint($head);
					if($h_type!="HEADING" && strtoupper($head['section'])!= 'CONTRACT_CERT' && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						//ASSIST_HELPER::arrPrint($form_object);
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($display_me) {
								$list_items = array();
								$listObject = new MASTER_LIST($head['list_table']);
								//return array("display"=>$head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//return array("display"=>$list_items);
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MASTER_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "OWNER":
									$listObject = new MASTER_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									//$listObject = new MASTER_MASTER($head['list_table']);
									$hdlist = strtoupper($head['list_table']);
									$listObject = new $hdlist();
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break;
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							//if($fld == "customer_p_dob"){
								//$options['options'] = array("yearRange"=>"'".(date('Y')-50).':'.date('Y')."'");
							//}else{
								$options['options'] = array();
							//}
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$data['js'].=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} else if(strtoupper($head['section'])== 'CONTRACT_CERT'){
						//-----------------------------------------------------CERTIFICATE STUFF----------------------------------------//
			//$form_object = new MASTER_CONTRACT();
			//$form_object = $form_object->getAObject();
			//ASSIST_HELPER::arrPrint($form_object);
						if(isset($form_object['customer_c_certificate']) && !$is_add_page){
							if(is_array($form_object['customer_c_certificate'])){
								if(count($form_object['customer_c_certificate'])>0){
								//ASSIST_HELPER::arrPrint($form_object['customer_c_certificate']);
								foreach($form_object['customer_c_certificate'] as $index=>$item){
									if($item['customer_c_status']==1){
										$display_me = true;
										$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
										if($head['required']==1) { $options['title'] = "This is a required field."; }
										if($h_type=="LIST") {
											if($display_me) {
												$list_items = array();
												$listObject = new MASTER_LIST($head['list_table']);
												//return array("display"=>$head['list_table']);
												$list_items = $listObject->getActiveListItemsFormattedForSelect();
												//return array("display"=>$list_items);
												//ASSIST_HELPER::arrPrint($list_items);
												if(isset($list_items['list_num'])) {
													$list_parent_association = $list_items['list_num'];
													$list_items = $list_items['options'];
													$options['list_num'] = $list_parent_association;
												}
												$options['options'] = $list_items;
												if(count($list_items)==0 && $head['required']==1) {
													$form_valid8 = false;
													$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
													$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
												}
												if($is_edit_page || $is_update_page) {
													$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "X";
												} else {
													$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
												}
												unset($listObject);
											}
										} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
											$list_items = array();
											switch($h_type) {
												case "USER":
													$listObject = new MASTER_USERACCESS();
													$list_items = $listObject->getActiveUsersFormattedForSelect();
													break; 
												case "OWNER":
													$listObject = new MASTER_CONTRACT_OWNER();
													$list_items = $listObject->getActiveOwnersFormattedForSelect();
													break;
												case "MASTER":
													//$listObject = new MASTER_MASTER($head['list_table']);
													$hdlist = strtoupper($head['list_table']);
													$listObject = new $hdlist();
													$list_items = $listObject->getActiveItemsFormattedForSelect();
													break;
												default:
													echo $h_type; 
													break;
											}
											$options['options'] = $list_items;
											$h_type = "LIST";
											if(count($list_items)==0 && $head['required']==1) {
												$form_valid8 = false;
												$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
												$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
											}
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && ($item[$fld]>0 || !is_numeric($item[$fld])) ? $item[$fld] : "X";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
											}
										} elseif($h_type=="DATE") {
											$options['options'] = array();
											$options['class'] = "";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 && strtotime($item[$fld])>0 ? date("d-M-Y",strtotime($item[$fld])) : "";
												if($is_update_page) {
													$options['options']['maxDate'] = "'+0D'";
												}
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
											}
										} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
											$options['extra'] = "processCurrency";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
											}
										} elseif($h_type=="BOOL"){
											$h_type = "BOOL_BUTTON";
											$options['yes'] = 1;
											$options['no'] = 0;
											$options['extra'] = "boolButtonClickExtra";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "0";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
											}
										} elseif($h_type=="REF") {
											if($page_action=="Add") {
												$val = "System Generated";
											} else {
												$val = $formObject->getRefTag().$form_object_id;
											}
										} elseif($h_type=="ATTACH") {
											$attachment_form = true;
											$options['action'] = $pa;
											$options['page_direct'] = $pd;
											$options['can_edit'] = ($is_edit_page || $is_update_page);
											$options['object_type'] = $form_object_type;
											$options['object_id'] = $form_object_id;
											$options['page_activity'] = $form_activity;
											$val = isset($item[$fld]) ? $item[$fld] : ""; 
											if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
										} else {
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) ? $item[$fld] : "";
											} else {
												$val = isset($head['default_value']) ? $head['default_value'] : "";
											}
										}
										if($display_me) {
											$display = $this->createFormField($h_type,$options,$val);
											$data['js'].=$display['js'];
											if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
												if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
													$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
												}
											}
										}
										if($display_me) {
										$echo.= $fld=='customer_c_status'?"
										<tr id=tr_".$fld."_".$index." sect='".$index."'".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
											<th class='".$th_class."' width=40%>Certificate ".$item['customer_c_certificate'].":</th>
											<td>Status: ".$item['customer_c_status']."<span class='right'><input type='button' class='edit_btn' value='Edit' ref='".$item['customer_c_certificate']."'></input></span>
											"//<td>".($fld=='customer_c_status'?"<span class='cert_number' style='float:right'>".$item['customer_c_certificate']."</span><input type='hidden' name='customer_c_certificate' value='".$item['customer_c_certificate']."' />":"")."
											."</td>
										</tr>":"";
										
										$echo.= "
											<tr class='edit_frm' field='$fld' ref=".$item['customer_c_certificate'].">
												<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
												<td>".$display['display']."</td>
											</tr>
										";
										/*
										if($cert_counter <=0){
											$echo .="<div id='edit_frm' style='background-color:green'>
												<table class='form'><tr id=tr_".$fld."_".$index." sect='".$index."'".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
													<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
														<td>".$display['display'].($fld=='customer_c_status'?"<span class='cert_number' style='float:right'>".$item['customer_c_certificate']."</span><input type='hidden' name='customer_c_certificate' value='".$item['customer_c_certificate']."' />":"")."
														</td>
													</tr>
												</table>
											</div>";
										}
										$cert_counter ++;
										*/
										}
									}
								}
								//Existing certificates: make the dialog
								//Hidden empty form, taken from normal add form				
								$display_me = true;
								$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
								if($head['required']==1) { $options['title'] = "This is a required field."; }
								if($h_type=="LIST") {
									if($display_me) {
										$list_items = array();
										$listObject = new MASTER_LIST($head['list_table']);
										//return array("display"=>$head['list_table']);
										$list_items = $listObject->getActiveListItemsFormattedForSelect();
										//return array("display"=>$list_items);
										//ASSIST_HELPER::arrPrint($list_items);
										//ASSIST_HELPER::arrPrint($head);
										if(isset($list_items['list_num'])) {
											$list_parent_association = $list_items['list_num'];
											$list_items = $list_items['options'];
											$options['list_num'] = $list_parent_association;
										}
										$options['options'] = $list_items;
										if(count($list_items)==0 && $head['required']==1) {
											$form_valid8 = false;
											$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
											$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
										}
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
										unset($listObject);
									}
								} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
									$list_items = array();
									switch($h_type) {
										case "USER":
											$listObject = new MASTER_USERACCESS();
											$list_items = $listObject->getActiveUsersFormattedForSelect();
											break; 
										case "OWNER":
											$listObject = new MASTER_CONTRACT_OWNER();
											$list_items = $listObject->getActiveOwnersFormattedForSelect();
											break;
										case "MASTER":
											//$listObject = new MASTER_MASTER($head['list_table']);
											$hdlist = strtoupper($head['list_table']);
											$listObject = new $hdlist();
											$list_items = $listObject->getActiveItemsFormattedForSelect();
											break;
										default:
											echo $h_type; 
											break;
									}
									$options['options'] = $list_items;
									$h_type = "LIST";
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								} elseif($h_type=="DATE") {
									$options['options'] = array();
									$options['class'] = "";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
									$options['extra'] = "processCurrency";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="BOOL"){
									$h_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
								} elseif($h_type=="REF") {
									if($page_action=="Add") {
										$val = "System Generated";
									}
								} elseif($h_type=="ATTACH") {
									$attachment_form = true;
									$options['action'] = $pa;
									$options['page_direct'] = $pd;
									$options['can_edit'] = ($is_edit_page || $is_update_page);
									$options['object_type'] = $form_object_type;
									$options['object_id'] = $form_object_id;
									$options['page_activity'] = $form_activity;
									$val = isset($item[$fld]) ? $item[$fld] : ""; 
									if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
								} else {
									$val = isset($head['default_value']) ? $head['default_value'] : "";
								}
								if($display_me) {
									$display = $this->createFormField($h_type,$options,$val);
									$data['js'].=$display['js'];
									if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
										if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
											$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
										}
									}
								}
								//ASSIST_HELPER::arrPrint($form_object['customer_c_certificate']);
								//ASSIST_HELPER::arrPrint($head);
								//$my_id = 
								
								 
						/*
						$echo.= "
							<tr class='edit_frm' field=$fld ref=".$form_object['customer_c_certificate'].">
								<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
								<td>".$display['display']."</td>
							</tr>
						";
						*/
								$echo .=($cert_counter <=0)?"	<table><tr>
												<th class='".$th_class."' width=40%>New Document</th>
												<td>&nbsp;<span class='right'><input type='button' class='edit_btn' value='Add'></input></span></td>
												</tr>
											</table>":"";
								$cert_counter ++;
							}
						//-----------------END if there are existing xertificates-------------------------------------/
						}else{
							//-----------------Add dialog / empty edit dialog----------------------//
							include("../MDOC/class/mdoc.php");
							$mDocObj = new MDOC();
                            //$masterDocObj = new MASTER_MDOC();
                            //echo $masterDocObj->getObjectTypeID();
                            //$obj_type = "";
                            //echo $this->cmpcode." ".$form_object_type." ".$form_object_id;
							$docData = $mDocObj->getModuleDocumentTable($this->cmpcode,$form_object_type,$form_object_id);
							$echo .= "<br>";
							$echo .= $docData['html'];
							$echo .= "<br>";
							$js .= $docData['js'];
							//Add button
							$echo .=($cert_counter <=0)?"	<table class='form'><tr>
											<th class='".$th_class."' width=40%>New Document</th>
											<td>&nbsp;<span class='right'><input type='button' class='edit_btn' value='Add'></input></span></td>
											</tr>
										</table>":"";
							$cert_counter ++;
							//Hidden empty form, taken from normal add form				
								$display_me = true;
								$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
								if($head['required']==1) { $options['title'] = "This is a required field."; }
								if($h_type=="LIST") {
									if($display_me) {
										$list_items = array();
										$listObject = new MASTER_LIST($head['list_table']);
										//return array("display"=>$head['list_table']);
										$list_items = $listObject->getActiveListItemsFormattedForSelect();
										//return array("display"=>$list_items);
										//ASSIST_HELPER::arrPrint($list_items);
										//ASSIST_HELPER::arrPrint($head);
										if(isset($list_items['list_num'])) {
											$list_parent_association = $list_items['list_num'];
											$list_items = $list_items['options'];
											$options['list_num'] = $list_parent_association;
										}
										$options['options'] = $list_items;
										if(count($list_items)==0 && $head['required']==1) {
											$form_valid8 = false;
											$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
											$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
										}
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
										unset($listObject);
									}
								} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
									$list_items = array();
									switch($h_type) {
										case "USER":
											$listObject = new MASTER_USERACCESS();
											$list_items = $listObject->getActiveUsersFormattedForSelect();
											break; 
										case "OWNER":
											$listObject = new MASTER_CONTRACT_OWNER();
											$list_items = $listObject->getActiveOwnersFormattedForSelect();
											break;
										case "MASTER":
											//$listObject = new MASTER_MASTER($head['list_table']);
											$hdlist = strtoupper($head['list_table']);
											$listObject = new $hdlist();
											$list_items = $listObject->getActiveItemsFormattedForSelect();
											break;
										default:
											echo $h_type; 
											break;
									}
									$options['options'] = $list_items;
									$h_type = "LIST";
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								} elseif($h_type=="DATE") {
									$options['options'] = array();
									$options['class'] = "";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
									$options['extra'] = "processCurrency";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="BOOL"){
									$h_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
								} elseif($h_type=="REF") {
									if($page_action=="Add") {
										$val = "System Generated";
									}
								} elseif($h_type=="ATTACH") {
									$attachment_form = true;
									$options['action'] = $pa;
									$options['page_direct'] = $pd;
									$options['can_edit'] = ($is_edit_page || $is_update_page);
									$options['object_type'] = $form_object_type;
									$options['object_id'] = $form_object_id;
									$options['page_activity'] = $form_activity;
									$val = isset($item[$fld]) ? $item[$fld] : ""; 
									if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
								} else {
									$val = isset($head['default_value']) ? $head['default_value'] : "";
								}
								if($display_me) {
									$display = $this->createFormField($h_type,$options,$val);
									$data['js'].=$display['js'];
									if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
										if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
											$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
										}
									}
								}
								if($display_me) {
								$echo.= "
								
									<tr class='edit_frm' field='$fld'>
										<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
										<td>".$display['display']."</td>
									</tr>
								";
								}
							}
						//----------------------------------------------END EDIT CERTIFICATE STUFF------------------------------------//
						}else if($is_add_page){
						//---------------------------------------------------Add CERTIFICATE STUFF------------------------------------//
						//ASSIST_HELPER::arrPrint($head);
						
							$display_me = true;
							$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
							if($head['required']==1) { $options['title'] = "This is a required field."; }
							if($h_type=="LIST") {
								if($display_me) {
									$list_items = array();
									$listObject = new MASTER_LIST($head['list_table']);
									//return array("display"=>$head['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									//return array("display"=>$list_items);
									//ASSIST_HELPER::arrPrint($list_items);
									//ASSIST_HELPER::arrPrint($head);
									if(isset($list_items['list_num'])) {
										$list_parent_association = $list_items['list_num'];
										$list_items = $list_items['options'];
										$options['list_num'] = $list_parent_association;
									}
									$options['options'] = $list_items;
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
									unset($listObject);
								}
							} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
								$list_items = array();
								switch($h_type) {
									case "USER":
										$listObject = new MASTER_USERACCESS();
										$list_items = $listObject->getActiveUsersFormattedForSelect();
										break; 
									case "OWNER":
										$listObject = new MASTER_CONTRACT_OWNER();
										$list_items = $listObject->getActiveOwnersFormattedForSelect();
										break;
									case "MASTER":
										//$listObject = new MASTER_MASTER($head['list_table']);
										$hdlist = strtoupper($head['list_table']);
										$listObject = new $hdlist();
										$list_items = $listObject->getActiveItemsFormattedForSelect();
										break; 
									default:
										echo $h_type; 
										break;
								}
								$options['options'] = $list_items;
								$h_type = "LIST";
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							} elseif($h_type=="DATE") {
								$options['options'] = array();
								$options['class'] = "";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
								$options['extra'] = "processCurrency";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							} elseif($h_type=="BOOL"){
								$h_type = "BOOL_BUTTON";
								$options['yes'] = 1;
								$options['no'] = 0;
								$options['extra'] = "boolButtonClickExtra";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							} elseif($h_type=="REF") {
								if($page_action=="Add") {
									$val = "System Generated";
								}
							} elseif($h_type=="ATTACH") {
								$attachment_form = true;
								$options['action'] = $pa;
								$options['page_direct'] = $pd;
								$options['can_edit'] = ($is_edit_page || $is_update_page);
								$options['object_type'] = $form_object_type;
								$options['object_id'] = $form_object_id;
								$options['page_activity'] = $form_activity;
								$val = isset($item[$fld]) ? $item[$fld] : ""; 
								if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
							if($display_me) {
								$display = $this->createFormField($h_type,$options,$val);
								$js.=$display['js'];
								if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
									if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
										$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
									}
								}
							}
							if($display_me) {
							$echo.= "
							<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
								<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
								<td>".$display['display']."
								</td>
							</tr>";
							}
						//-------------------------------------------------END Add CERTIFICATE STUFF----------------------------------//
						}else{
							$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : ":O";
							
							if($display_me) {
								$display = $this->createFormField($h_type,$options,$val);
								$data['js'].=$display['js'];
								if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
									if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
										$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
									}
								}
							}
						}
						
					} else if(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);

					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
					} 
					if($display_me && strtoupper($head['section'])!== 'CONTRACT_CERT') {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
					
						
					
					//MASSIVE FORM PROCESSING ENDS HERE//
				}
				//$echo.="<p>Hello $val!</p></div>";
				$echo.="</table></form></td></tr></table></div>";
			}
		}
		$echo.="</div>";
		if($is_edit_page){
			$echo.="<div id='edit_frm_container' style='display:none'><form id='popup-frm'><input type='hidden' name='customer_c_parent' id='customer_c_parent' value='$form_object_id' /><table id='append_here'></table></form></div>";
		}
		//Writing script for tabs
		$data['js'].="	//Set the tab object for later reference
					window.tabObj = $(\"#tab_div\").tabs();
					
			";
		//$echo.=ASSIST_HELPER::arrPrint($headings);
		//return array("display"=>$echo,"js"=>$js);
		
			if(!$form_valid8) {
				$data['js'].="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
				/*
				if(!$is_copy_page) {
					$echo.="
					<tr id='saver'>
						<th></th>
						<td>
							<input type=button value=\"Save ".$formObject->getObjectName($form_object_type)."\" class=isubmit id=btn_save />
							<input id='very_hidden_id' type=hidden name=object_id value=".$form_object_id." />
							".(($form_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
						</td>
					</tr>";
				}
			 */
		$data['js'].=($is_add_page)?"	
				//Preparing a blank row
					window.first_cert = true;
					window.newRow = '<tr class=\"remove_row\" style=\"border:none\"><td style=\"border:none\"></td><td style=\"border:none\">&nbsp;</td></tr>';
				//Counting new forms added
					window.cert_count = 0;
				//Doing the Array
					$('#tbl_object_form_16').find('input, select, textarea').each(function(){
						var newOne = $(this).prop('name') + '[]';	
						$(this).prop('name', newOne);
					});
				//'Add another' button
				":"";
				
		//$data['js'].=($is_edit_page)?"$('#tbl_object_form_16').append('<span id=\"another_tr\"><input type=\"button\" class=\"another_btn\" value=\"Add another\" /></span>');":"";
		if($is_edit_page){
		$data['js'].="
				var editing = false;
				$('tr.edit_frm').each(function(){
					$(this).hide();
				});
				$(document).on('click','.edit_btn',function(){
					$('#append_here').empty();
					window.clickedBtn = $(this).attr('ref');
					//alert('hit'+$(this).attr('ref'));
					if($(this).attr('ref')!='undefined' && $(this).attr('ref')!= undefined){
						//alert('existing');
						editing = $(this).attr('ref');
					}else{
						editing = false;
					}
					//alert(editing);
					$('#edit_frm_container').dialog({
						modal:true,
						width:500,
						buttons:{
							'Save':function(){
								AssistHelper.processing();
								//CntrctHelper.validateForm($('#popup-frm'));
								alert(dta);
								if(editing !== false){
									var act = 'CERT_Edit';
									$('#popup-frm').append('<input type=hidden name=customer_c_certificate value='+clickedBtn+' />');
								}else{
									var act = 'CERT_Add';
								}
								var dta = AssistForm.serialize($('#popup-frm'));
								//var result = 'inc_controller.php?action=CERTS.'+act+' | '+dta;
								var result = AssistHelper.doAjax('inc_controller.php?action=CERTS.'+act,dta);
								if(result[0]=='ok'){
									document.location.reload();
								}else{
									AssistHelper.finishedProcessing(result[0],result[1]);
								}
								//console.log(result);
								$(this).dialog('close');
							},
							'Delete':function(){
								var dta = 'customer_c_certificate='+clickedBtn;
								var result = AssistHelper.doAjax('inc_controller.php?action=CERTS.CERT_Del',dta);
								$(this).dialog('close');
								if(result[0]=='ok'){
									document.location.reload();
								}else{
									AssistHelper.finishedProcessing(result[0],result[1]);
								}
								
							},
							'Cancel':function(){
								$(this).dialog('close');
							}
						}
					});
					if(editing !== false){
						$('tr .edit_frm').each(function(){
							if($(this).attr('ref') == editing){
								$(this).clone().appendTo($('#append_here'));
							}
						});
					}else{
						//$('#append_here').empty();
						var counter = [];
						$('tr .edit_frm').each(function(){
							if($.inArray($(this).attr('field'),counter)==-1){
								counter.push($(this).attr('field'));
								$(this).clone().appendTo($('#append_here'));
							}
						});
						$('#append_here tbody tr').each(function(){
							$(this).show();
						});
						//Resetting form
						$(':input','#popup-frm')
						  .removeAttr('checked')
						  .removeAttr('selected')
						  .not(':button, :submit, :reset, :hidden, :radio, :checkbox')
						  .val('');
					}
					$('#append_here tbody tr').each(function(){
						$(this).show();
					});
				});
			";
		}
		$obj_id = $is_edit_page?$_REQUEST['object_id']:'0';
		if(!$is_edit_page){
			$is_edit_page = 0;
		}
		$data['js'].="
				window.isValid = false;
				//Next tab + save button
					$('div[role=\"tabpanel\"]').each(function(){
						if($(this).prop('id') == 'tabs-16'){
							$(this).append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\"><table class=form><tr><th width=\"40%\">&nbsp;</th><td><input type=button value=\"Save All\" class=isubmit id=btn_save /><form id=finished_editing ><input id=\"very_hidden_finished\" type=hidden name=customer_user_status value=2 /><input id=\"very_hidden_id\" type=hidden name=object_id value=".$form_object_id." /></td></tr></table></td></tr></table>');
						}else{
							var link = $(this).prop('id');
							$(this).append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\">\
											<table class=form id=\"btn_row\">\
												<tr>\
													<th width=\"40%\">&nbsp;</th>\
														<td><span style=\"float:right; margin-bottom:5px\">\
															<input type=button class=\"next_page isubmit\" link='+link+' value=\"Save and continue\" />\
															<input type=button id=\"deactivate_btn\" class=\"idelete\" value=\"Deactivate\" />\
															<input type=button id=\"delete_btn\" class=\"idelete\" value=\"Delete\" />\
														</span></td>\
													</tr>\
											</table>\
										</td></tr></table>');
						}
					});
					$('#btn_row').css('width',$('#tr_customer_id').css('width'));
					window.customerSeed = ".$obj_id.";
					$('input.next_page').click(function(){
						var myLink = $(this).attr('link');
						//var result = CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
						//var result = AssistForm.serialize($('#'+myLink).find('form'));
						//var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),'serialize');
						if(myLink == 'tabs-0' && $is_edit_page !==1){
							//First page -> Add customer
							var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),page_action,page_direct);
							AssistHelper.processing();
							AssistHelper.finishedProcessing(result[0],result[1]);
							console.log(result.object_id);
							if(typeof result.object_id !== undefined && result[0] == 'info'){
								window.customerSeed = result.object_id;
							}
							
							//alert(result.object_id);
						}else{
							//Next page -> update customer
							//alert('Hit the not-first-tab!');
							if(typeof window.customerSeed !== undefined && window.customerSeed > 0){
								$('#'+myLink).find('form').append('<input type=hidden name=object_id value='+window.customerSeed+' />');
								var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),page_action+'|ED',page_direct);
								//console.log($('#'+myLink).find('form'));
							}else{
								AssistHelper.processing();
								AssistHelper.finishedProcessing('info','Please complete the first section of the customer\'s details before continuing');
							}
						}
						//console.log(result);
						//To open the next tab	
						var nextTabIndex = $('li[role=\"tab\"][aria-controls=\"'+myLink+'\"][display!=\"none\"]').next().index();
						if(isValid){
							tabObj.tabs('option','active',nextTabIndex);
						}
					});
					
					$('#deactivate_btn').click(function(){
						//alert('DEACTIVATING');
						AssistHelper.processing();
						var result = AssistHelper.doAjax('inc_controller.php?object_id=".$obj_id."&action=CONTRACT.Deactivate','manage_view.php');
						//console.log(result);
						//alert(result);
						AssistHelper.finishedProcessing(result[0],result[1]);
					});

					$('#delete_btn').click(function(){
						//alert('DELETING');
						AssistHelper.processing();
						var result = AssistHelper.doAjax('inc_controller.php?object_id=".$obj_id."&action=CONTRACT.Delete','manage_view.php');
						AssistHelper.finishedProcessing(result[0],result[1]);
					});
					
					
					
					$('#tbl_object_form_16').append('<tr id=\"another_tr\"><th></th><td><input type=\"button\" class=\"another_btn\" value=\"Add another\" /></td></tr>');
				//Copying certificate html for later
					window.certHtml = $('#tbl_object_form_16').html();
					certHtml = certHtml.slice(0,-18);
					certHtml += '<input type=\"button\" class=\"remove_btn\" value=\"Remove\" /></td></tr></tbody><tr class=\"remove_row\" style=\"border:none\"><td style=\"border:none\"></td><td style=\"border:none\">&nbsp;</td></tr>';
					$('#tbl_object_form_16 tbody').addClass('keepme');
				//Appending the copied html to the table on button click
					$(document).on('click','.another_btn',function(){
						//Checking whether or not to add first new row
							if(first_cert === true){
								$('#tbl_object_form_16').append(newRow);
							}
							first_cert = false;
						//Appending a remove button to new ones
							$('#tbl_object_form_16').append(certHtml);
						//Counting new certificate
							cert_count ++;
							//alert(cert_count);
					});
				//Listener for remove buttons
					$(document).on('click','.remove_btn',function(){
						cert_count --;
						//console.log($(this).parents('tbody'));
						$('#tbl_object_form_16 tbody:not(:first)').remove();
						if(cert_count <= 0){
							$('.remove_row').remove();
							first_cert = true;
						}else{
							$(this).parents('.remove_row:first').remove();
						}
					});
				";
				if(!$is_add_page){
					$data['js'].="	
				//Listener for remove buttons
					$(document).on('click','.remove_btn',function(){
						cert_count --;
						//console.log($(this).parents('tbody'));
						$(this).parents('tbody').remove();
						if(cert_count <= 0){
							$('.remove_row').remove();
							first_cert = true;
						}else{
							$(this).parents('.remove_row:first').remove();
						}
					});
				//Arranging the current certificates into sub-tables
				sectNums = {sects:[]};
				//var finalHtml = '<tr><td>';
				var finalHtml = '';
				
				$('#tbl_object_form_16 tr').each(function(){
					if($(this).prop('id')!=='saver'){
						var sect = $(this).attr('sect');
						if(!$.isArray(sectNums.sects[sect])){
							sectNums.sects[sect]=new Array();
						}
						sectNums.sects[sect].push('<tr>'+$(this).html()+'</tr>');
					}
				});
	//console.log(sectNums);
				
				for(i=0;i<sectNums.sects.length;i++){
					finalHtml +='<tr class=\"noborder\"><td class=\"noborder\"><table class=\"th2\">';
					var hString = sectNums.sects[i].join('');
					finalHtml += hString;
					finalHtml +='</table></td></tr>';
				}
				//finalHtml +='</td></tr>';
				
	//console.log(finalHtml);
				$('#tbl_object_form_16 tbody').html(finalHtml);
				$('#tbl_object_form_16').addClass('tbl-container noborder');
				$('#tbl_object_form_16 td').each(function(){
					$(this).css('border','none');
				});
				//$('#tbl_object_form_16').trigger('pagecreate');
				";
				}
				$data['js'].="
				window.firstRun = true;
				$('a[href=\"#tabs-14\"]').click(function(){
					if(firstRun !== false){
						$(\"#tbl_object_form_14 tbody tr\").each(function(){
							if($(this).prop(\"id\")!=='tr_customer_bil_crm'){
								$(this).hide();
							}else{
								$(this).show();
							}
						});
					}
					firstRun = false;
				});
				
				$(\"#customer_bil_crm_yes, #customer_bil_crm_no\").click(function(){
					if($(customer_bil_crm).val() == 0){
						//alert('SAME');
						//$(this).parents(\"table\").hide();
						$(this).parents(\"tr\").siblings().each(function(){
							if($(this).prop('id')!=='saver'){
								$(this).hide();
							}
						});
					}else{
						//alert('NOT');
						$(this).parents(\"tr\").siblings().each(function(){
							$(this).show();
						});
					}
				});
				
				//Customertype should change the tab visibility
				window.prsnContent = '';
				window.orgContent = '';
				$('#customer_type').change(function(){
					if($(this).val()==1){
						//PERSON
							//tabObj.css('background-color','#eeffed');
						$('#tabs-0').find('tr[id*=\"_o_\"]').each(function(){
							$(this).hide();
						});
						$('#tabs-0').find('tr[id*=\"_p_\"]').each(function(){
							$(this).show();
						});
						$('#tabs-11').hide();
						$('#tabs-12').show();
						$('li[aria-controls=tabs-11]').hide();
						$('li[aria-controls=tabs-12]').show();
					}else if($(this).val()==2){
						//ORGANISATION		
						$('#tabs-0').find('tr[id*=\"_p_\"]').each(function(){
							$(this).hide();
						});
						$('#tabs-0').find('tr[id*=\"_o_\"]').each(function(){
							$(this).show();
						});
							//tabObj.css('background-color','#edf9ff');
						$('#tabs-12').hide();
						$('#tabs-11').show();
						$('li[aria-controls=tabs-12]').hide();
						$('li[aria-controls=tabs-11]').show();
					}
						tabObj.tabs('refresh');
				});
				$('#customer_type').trigger('change');
				
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($form_object_type)."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"
				
				
				".$js."
				
				";
		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="
				
				
				$(\"form[class=new_frm_object] select\").each(function() {
					if($(this).children(\"option\").length<=1 && !$(this).hasClass(\"required\")) {
					}
					$(this).trigger('change');
				});
$(\"#btn_save\").click(function() {
					/* NEW */
					AssistHelper.processing();
					var result = CntrctHelper.processObjectForm($('#finished_editing'),page_action,page_direct);
					AssistHelper.finishedProcessing(result[0],result[1]);
					return;
					/* NEW */
					$"."originalForm = $(\"form[name=massive_form]\");
					var f = 0;
					$"."originalForm.find('input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//console.log('EEK:'+f);
					if(f==0){
						$"."originalForm.find('input:hidden#has_attachments').val(0);
					}
					$"."form = $"."originalForm.clone(); 
					if($('#customer_type').val()==1){
						$"."form.find('div #tabs-11').find('input, select, textarea, button').remove();
					}else if($('#customer_type').val()==2){
						$"."form.find('div #tabs-12').find('input, select, textarea, button').remove();
						/*	
						for(i=0;i<$"."form.length;i++){
							if($"."form[i].id=='new_12'){
								$"."form.splice(i, 1);
							}
						}
						*/
					}
					if($(customer_bil_crm).val() == 1){
						$"."form.find('div #tabs-14').find('input, select, textarea, button').not('#customer_bil_crm').remove();
					}
				//console.log('Hello there--'+AssistForm.serialize($"."form));
				//debugger;
					$(\"input[name|='excluder']\").each(function(){
						//console.log($(this).prop('checked'));
					});
					//alert(AssistForm.serialize($"."form));";
		if($attachment_form) {
			$data['js'].="
					if(f>0) {
						/*
						$"."superForm = $"."form.find(':first');
						$"."form.find(':gt(0)').each(function(){
							$(this).find('input, select, textarea,button').each(function(){
								console.log('input  '+$(this).prop('name'));
								console.log($(this));
								$"."superForm.add($(this));
							});
						});
						console.log($"."superForm);
						CntrctHelper.processObjectFormWithAttachment($"."superForm,page_action,page_direct);
						alert('processing with attach');
						*/
						console.log($"."form);
						CntrctHelper.processObjectFormWithAttachment($"."form,page_action,page_direct,$"."originalForm);
						//alert('processing with attach');
					} else {
						$('#has_attachments').val(0);
						CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
					}
					
					";
		} else {
			$data['js'].="
				//Checking each individual element
					//var errorArr = [];
				//Making the thing do the other thing (attach hidden input with Object ID)
					$('#new_0').append('<input type=hidden name=object_id value=".$form_object_id." />');
					/*
					$"."form.each(function(){
						if(!CntrctHelper.validateForm($(this),true)){
							//alert($(this).prop('id'));
							$('li[role=tab]').each(function(){
								if($(this).attr('aria-selected')){
									CntrctHelper.validateForm($(this),false);		
								}
							});
						}
					});
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					alert('HI : Frigate = '+f);
					if(f>0) {
						alert('Doing attachment form');
						CntrctHelper.processObjectFormWithAttachment($"."form,page_action,page_direct);
					} else {
						$('#has_attachments').val(0);
						//alert(AssistForm.serialize($"."form));
						CntrctHelper.processObjectForm($"."form,page_action,page_direct);
					}
					*/
					$('#has_attachments').val(0);
					CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
					";
		}
		$data['js'].="
					console.log('end of function'+AssistForm.serialize($"."form));
					return false;
				});
				
				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}
				
				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");		
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();   
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}
				
				$data['js'].="
				$(\"#del_type\").change(function() {
					if($(this).val()==\"SUB\") {
						$(\".tr_del_type\").show();
					} else {
						$(\".tr_del_type\").hide();
					}
				});
				
				if($(\"#del_parent_id\").children(\"option\").length<=1) {
					$(\"#del_type option[value=SUB]\").prop(\"disabled\",true);
				}
				$(\"#del_type\").trigger(\"change\");
				
				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});
				
				//alert(AssistForm.serialize($(\"form[name=frm_deliverable]\")));
				
				$(\"form[name=frm_object] select\").each(function() {
					//alert($(this).children(\"option\").length);
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});
				
			";
				
				
		$data['display'] = $echo;
		return $data;		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 
	 * Returns filter for selecting parent objects according to section it is fed
	 * 
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";
				
		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;
					
					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;
			
			case 'DELIVERABLE':
				$dsp = "contracts get";
				
				break;
			
			case 'ACTION':
				$dsp = "deliverables get";
				
				break;
			
			default:
				$dsp = "Invalid arguments supplied";
				break;
		}
		
		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";
		
        return $data;
    }
	

	/** 
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 * 
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from MASTER_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from MASTER_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].= 
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>	
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';
					
												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript
					
					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}
		
		return $data;
    }
}


?>
