<?php
    require 'inc_head.php';
//    print_r($access);
$var = $_REQUEST;
if(isset($var['act'])) {
	switch($var['act']) {
		case "SAVE_COMM":
			$result = saveComment($var);
			break;
		case "DEL_COMM":
			$result = deleteComment($var);
			break;
		case "EDIT_COMM":
			$result = editComment($var);
			break;
		default:
				//$result[0] = "error";
				//$result[1] = "Action function for <b>".$var['act']."</b> not set.";
			break;
	}
} else {
	$result = array();
	//$result[0] = "error";
	//$result[1] = "Nothing done. ACT NOT SET";
}	

/** USER ACCESS **/
$my_access = array();
$main_access = array();
if($access['docadmin']!="Y") {
	$sql = "SELECT * FROM ".$dbref."_categories_users 
			INNER JOIN ".$dbref."_categories
			  ON cucateid = cateid AND cateyn = 'Y'
			WHERE cuyn = 'Y' AND cutkid = '$tkid' AND (cutype = 'UP' OR cutype = 'MAIN')";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$my_access[$row['cucateid']] = "Y";
		//if(isset($cate_owner[$row['cucateid']])) { unset($cate_owner[$row['cucateid']]); }
		if($row['cutype']=="MAIN") { $main_access[$row['cucateid']] = "Y"; }
	}
	mysql_close();
/*	if(count($cate_owner)>0) {
		foreach($cate_owner as $i => $y) {
			$my_access[$i] = $y;
		}
	}*/
	//$total_cate = count($my_access);
} else {
	//$total_cate = $cate_count;
}

/** GET CATEGORIES **/
$categories = array();
$cate_count = 0;
$cate_owner = array();
if($access['docadmin']=="Y" || count($my_access) > 0) {
	$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$categories[$row['catesubid']][] = $row;
		$cate_count++;
		if($row['cateowner']==$tkid) {
			$cate_owner[$row['cateid']] = "Y";
			if(!isset($main_access[$row['cateid']])) { $main_access[$row['cateid']] = "Y"; }
			if(!isset($my_access[$row['cateid']])) { $my_access[$row['cateid']] = "Y"; }
		}
	}
	mysql_close();
}

?>
<script type="text/javascript" src="lib/dialog.js"></script>

<h1>Admin</h1>
<?php displayResult($result);  ?>
<table width=600><tbody>
<?php if($access['addcate']=="Y" || $access['docadmin']=="Y") { ?>
    <tr>
        <th>Create</th>
        <td>Create a new category.&nbsp;<span class=float><input type=button value=Create id=3 onclick="adminGoTo('create');"></span></td>
    </tr>
<?php } ?>
<?php if(count($main_access)>0 || $access['docadmin']=="Y") { ?>
    <tr>
        <th>Maintain</th>
        <td>Edit categories, sub-categories and documents.&nbsp;<span class=float><input type=button value=Maintain id=4 onclick="adminGoTo('maintain');"></span></td>
    </tr>
<?php } ?>
<?php if(count($my_access)>0 || $access['docadmin']=="Y") { ?>
    <tr>
        <th>Upload</th>
        <td>Add a new document to a category.&nbsp;<span class=float><input type=button value=Upload id=2 onclick="adminGoTo('upload');"></span></td>
    </tr>
<?php } ?>
<?php if(count($my_access)>0 || $access['docadmin']=="Y") { ?>
    <tr>
        <th>Comments</th>
        <td>Add a new comment to a category.&nbsp;<span class=float><input type=button value=Add id=add ></span></td>
    </tr>
<?php } ?>
<?php if($access['docadmin']=="Y") { ?>
    <tr>
        <th>User Access</th>
        <td>Maintain user access to categories.&nbsp;<span class=float><input type=button value=Maintain id=user onclick="adminGoTo('user');" ></span></td>
    </tr>
<?php } ?>
</tbody></table>
<script type=text/javascript>
$(document).ready(function() {
	$("th").addClass("left");
});
</script>
<?php drawGeneralCommentDialog("admin.php"); ?>
</body>
</html>
