<?php
$ip=$_SERVER['REMOTE_ADDR'];
//echo "<p span=\"color: #ffffff;\"> $ip </p>";
$t = strtotime(date("Y-m-d H:i:s"));


/**************
 * SPECIAL NOTICE SETTINGS FOR NOTICE NOT RELATED TO MAINTENANCE
 */
$display_special_notice = true;
$special_notice_start_time = strtotime("26 March 2020 12:00:00");
$special_notice_end_time = strtotime("16 April 2020 23:59:59");
$special_notice_title = "COVID-19 & Assist";
$special_notice_text = "
<style type='text/css'>
#div_special_notice a.covid {
	color: #fe9900;
}

#div_special_notice a.covid:visited {
	color: #fe9900;
}

#div_special_notice a.covid:hover {
	color: #FE9900;
}

#div_special_notice a.covid:active {
	color: #FE9900;
}
</style>
<p>The South African Government has announced a lock down period in an effort to combat the spread of the COVID-19 virus.  During this period the Assist system will remain fully functional, including all automated functions such as sending of reminders & automatic time period closures.</p>
<p>Action iT and our business partners are committed to continue to provide support to all our clients during this time. </p>
<p>Should you require any assistance, please contact your system administrator or log a support call according to your business partner's helpdesk processes.</p>
<p>For more information on COVID-19 please visit <a href='http://sacoronavirus.co.za' class='covid'>www.sacoronavirus.co.za</a></p>
<p class='center'><a href='http://sacoronavirus.co.za' target='_blank' class='covid'><img src='pics/covid-19.png' style='border:1px solid #fe9900;' width='350px'></a></p>";












$maintenance_start_time = strtotime("26 December 2018 18:00:00");
$maintenance_end_time = strtotime("30 December 2018 18:00:00");
$maintenance_notice_start = strtotime("02 November 2018 08:00:01");
$annual_maintenance = false;
$last_sunday_of_the_month_maintenance = false;
$special_maintenance = false;
$maintenance_name = "scheduled";



//$maintenance_start_time = strtotime("1 October 2015 14:00:00");
//$maintenance_end_time = strtotime("1 October 2015 17:00:00");
//$maintenance_notice_start = strtotime("1 October 2015 00:00:01");



$current_time = time();
$preset = true;

if($maintenance_start_time<$current_time && $maintenance_end_time<$current_time) {
	if(date("D",$current_time)=="Sun") {
		$preset = false;
		$maintenance_start_time = strtotime(date("d F Y",$current_time)." 14:00:00");
		$maintenance_notice_start = strtotime(date("d F Y",$current_time)." 00:00:01");
		$sunday = $current_time;
		$next_week = $sunday+(60*60*24*7);
	} elseif(date("D",$current_time)=="Sat") {
		$sunday = $current_time+(60*60*24);
		$next_week = $sunday+(60*60*24*7);
		$maintenance_start_time = strtotime(date("d F Y",$sunday)." 14:00:00");
		$maintenance_notice_start = strtotime(date("d F Y",$current_time)." 00:00:01");
	}
	if(date("D",$current_time)=="Sun" || date("D",$current_time)=="Sat") {
		if(date("M",$sunday)!=date("M",$next_week)) {
			$preset = false;
			$last_sunday_of_the_month_maintenance = true;
			$maintenance_name = "month-end";
			$maintenance_end_time = strtotime(date("d F Y",$sunday)." 20:00:00");
		}
	}
}

if(!$preset && $maintenance_end_time<$current_time) {
	if(date("D")=="Sun") {
		$maintenance_end_time = strtotime(date("d F Y",$current_time)." 18:00:00");
	}
}

if($annual_maintenance==false && (date("D",$maintenance_start_time)!="Sun" || date("H:i",$maintenance_start_time)!="14:00")) {
	$special_maintenance = true;
	$maintenance_name = "special";
} elseif($annual_maintenance==true) {
	$maintenance_name = "year-end";
}

if($current_time>$maintenance_start_time && $current_time < $maintenance_end_time) {
	if(strpos($_SERVER['HTTP_HOST'],"localhost")!==false ||strpos($_SERVER['HTTP_HOST'],"action4udev")!==false || (date("Y-m-d")=="2019-02-10" && in_array($ip,array("127.0.0.1","169.0.117.117","my ip here")))) {
		$assiststatus = "A";
	} else {
		$assiststatus = "U";
	}
} else {
		$assiststatus = "A";
}
?>
