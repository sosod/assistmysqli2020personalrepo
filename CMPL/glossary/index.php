<?php 
include("../header.php");
$eventObj  = new Glossary();
$event     = $eventObj -> getGlossaries();
?>
<table width="80%">
	<tr>
			<th>Category</th>
			<th>Terminology</th>
			<th>Explanation</th>
	</tr>	
	<?php
		if(isset($event) && !empty($event)){
		foreach($event as $key => $value){
	?>
		<tr>
			<td><?php echo $value['category']?></td>
			<td><?php echo $value['terminology']?></td>
			<td><?php echo $value['explanation']?></td>
		</tr>
	<?php 
			}
		} else {
		?>
		<tr>
			<td colspan='3'>No glossary setup</td>
		</tr>			
		<?php 
		}
	?>
</table>

<div><?php displayGoBack("", ""); ?></div>