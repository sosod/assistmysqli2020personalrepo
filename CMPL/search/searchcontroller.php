<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		require_once("../../../library/dbconnect/".strtolower($classname).".php");
	}
}
class SearchController extends DBConnect {
	
	var $headers = array();
	
	function getLegislativeTypes()
	{
		$legtypeObj = new LegislativeTypes();
		$legtypes   = $legtypeObj -> getLegislativeTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getLegislativeCategories()
	{
		$legcatObj = new LegislativeCategories();
		$legcats   = $legcatObj -> getLegislativeCategories();
		echo json_encode( $legcats ); 
	}

	function getOrganisationSizes()
	{
		$orgsizeObj = new OrganisationSize();
		$orgsizes   = $orgsizeObj -> getOrganisationSize();
		echo json_encode( $orgsizes ); 
	}

	function getLegOrganisationTypes()
	{
		$orgtypeObj = new LegOrganisationTypes();
		$legtypes   = $orgtypeObj -> getLegOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getOrganisationTypes()
	{
		$orgtypeObj = new OrganisationTypes();
		$legtypes   = $orgtypeObj -> getOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$legtypes   = $orgtypeObj -> getOrganisationCapacity();
		echo json_encode( $legtypes ); 
	}

	function getResponsibleOrganisations()
	{
		$orgObj = new ResponsibleOrganisation();
		$orgs   = $orgObj -> getResponsibleOrganisations();
		echo json_encode( $orgs );
	}
    function getSections()
    {
		$sectionObj = new LegislationSection();
        $sections = $sectionObj -> getLegislationSections();
        echo json_encode($sections);
    }

	 function getComplianceFrequency()
    {
		$complianceObj = new ComplianceFrequency();
        $compliances = $complianceObj -> getComplianceFrequencies();
        echo json_encode($compliances);
    }
	    
    function getDirectorate()
    {
        $dir = new Directorate();
        echo json_encode( $dir ->getSubDirectorates() );
    }

	function getUsers()
	{
		$obj = new UserAccess();
		echo json_encode( $obj -> getUsers() );					
	}

	function getActionOwners()
	{
		$obj = new ActionOwner();
		echo json_encode( $obj -> getActionOwners() );					
	}	
	
	function getMainEvents()
	{
		$obj = new Events();
		echo json_encode( $obj -> getEvents() );
	}

    function getSubEvents()
	{
		$obj = new SubEvent();
		echo json_encode( $obj -> getMainSubEvents( $_POST['id']) );
	}

    function getAccountablePerson()
	{
		$obj = new AccountablePerson();
		echo json_encode( $obj -> getAccountablePersons());
	}

    function getResponsibleOwner()
	{
		$obj = new ResponsibleOwner();
		echo json_encode( $obj -> getResponsibleOwners() );
	}
	
    function getComplianceTo()
	{
		$freqObj = new ComplianceTo();
		$freq    = $freqObj -> getComplianceTo();
		echo json_encode( $freq );
	}
		
	
    function getFunctionalService()
	{
		$obj = new FunctionalService();
		echo json_encode( $obj -> getFunctionalService());
	}

	function searchLegislations()
	{
		$ligObj = new Legislation();
		$ligObj = new Legislation();
		$catObj = new LegislativeCategories();
		$capObj = new OrganisationCapacity();
		$sizeObj = new OrganisationSize();
		$typeObj = new LegOrganisationTypes();		
		$valueArr = array();

		foreach( $_POST['searchArr'] as $index => $nameValueArr)
		{			
			if($nameValueArr['name'] == "category" )
			{
				$valueArr[$nameValueArr['name']][] = $nameValueArr['value'];
			}else if($nameValueArr['name'] == "organisation_size"){
				$valueArr[$nameValueArr['name']][] = $nameValueArr['value'];
			} else if($nameValueArr['name'] == "organisation_capacity"){
				$valueArr[$nameValueArr['name']][] = $nameValueArr['value'];	
			} else if($nameValueArr['name'] == "organisation_type"){
				$valueArr[$nameValueArr['name']][] = $nameValueArr['value'];
			} else {
			   $valueArr[$nameValueArr['name']] = $nameValueArr['value'];
			}
		} 	
		$legislations = $ligObj -> advancedSearchLegislations($_POST['start'], $_POST['limit'],  $valueArr); 		
		$legislationArray = array();
		foreach($legislations as $key => $value)
		{
			foreach($value as $index => $val){
				if($index == "category" )
				{
					$legislationArray[$key]['legislation_category'] = $catObj -> getLegislativeCategory( $val );
				}
				if($index == "organisationsize"){
					$legislationArray[$key]['organisation_size'] = $sizeObj -> getOrganisationSizes( $val );
				} if($index == "organisationcapacity"){
					$legislationArray[$key]['organisation_capacity'] = $capObj -> getOrganisationCapacities( $val );
				}  if($index == "organisationtype"){
					$legislationArray[$key]['organisation_type'] = $typeObj -> getOrganisationType( $val );
				}			
				$legislationArray[$key][$index] = $val;
		    }			
		}		
		$response = array();
		$legs = $this->_sortData( $legislationArray, new LegislationColumns() );
		$totalResults = count( $legs );
		if( $totalResults == 0){
			$response = "No results were found";
		} else {
			$response = $totalResults." result".($totalResults < 2 ? "" : "s")."  found ";
		}
		echo json_encode( array("legislation" => $legs, "headers" => $this -> headers, "cols" => count($this->headers), "total" => count($legs), "text" => $response ));
	}
	
	function getLegislations()
	{
		$ligObj = new Legislation();
		$catObj = new LegislativeCategories();
		$capObj = new OrganisationCapacity();
		$sizeObj = new OrganisationSize();
		$typeObj = new LegOrganisationTypes();
		
		$categoryIds = $this->sortIds( $catObj -> searchCategories( $_POST['searchText'] ) );
		$capacityIds = $this->sortIds( $capObj -> searchCapacities( $_POST['searchText'] ) );
		$sizeIds	 = $this->sortIds( $sizeObj -> searchSizes( $_POST['searchText'] ) );		
		$typeIds 	 = $this->sortIds( $typeObj -> searchTypes( $_POST['searchText'] ) );

		$idsArray    = array("category" 			 => $categoryIds,
							 "organisation_capacity" => $capacityIds,
							 "organisation_size"	 => $sizeIds,
							 "organisation_type"	 => $typeIds
							 );
		$legislations = $ligObj -> searchLegislations($_POST['start'], $_POST['limit'], $_POST['searchText'], $idsArray); 		

		$legislationArray = array();
		foreach($legislations as $key => $value)
		{
			foreach($value as $index => $val){
				if($index == "category" )
				{
					$legislationArray[$key]['legislation_category'] = $catObj -> getLegislativeCategory( $val );
				}
				if($index == "organisationsize"){
					$legislationArray[$key]['organisation_size'] = $sizeObj -> getOrganisationSizes( $val );
				} if($index == "organisationcapacity"){
					$legislationArray[$key]['organisation_capacity'] = $capObj -> getOrganisationCapacities( $val );
				}  if($index == "organisationtype"){
					$legislationArray[$key]['organisation_type'] = $typeObj -> getOrganisationType( $val );
				}			
				$legislationArray[$key][$index] = $val;
		    }			
		}		
		$response = array();
		$legs = $this->_sortData( $legislationArray, new LegislationColumns() );
		$totalResults = count( $legs );
		if( $totalResults == 0){
			$response = "No results were found";
		} else {
			$response = $totalResults." result".($totalResults < 2 ? "" : "s")."  found ";
		}
		echo json_encode( array("legislation" => $legs, "headers" => $this -> headers, "cols" => count($this->headers), "total" => count($legs), "text" => $response ));
	}
	
	function sortIds( $idArr )
	{
		$id  = ""; 
		if( isset($idArr) && !empty($idArr)){
			foreach ($idArr as $index => $val)
			{
				$id = $val['id'];			
			}
		}
		return $id;
	}
	
	
	
	function _sortData( $data, Naming $naming )
	{
		$headers   = $naming -> sortedNaming( $naming ); 
		$returnData = array();
		foreach( $data  as $key => $value){
		   foreach( $headers as $index => $val){
			if(isset($value[$index]) || array_key_exists($index, $value)){
					$this->headers[$index]  = $val;
					$returnData[$value['ref']][$index] = $value[$index];
				}
			}
		}
		return $returnData;
	}



    function getDeliverables()
    {
        $deliObj      = new Deliverable();
        $deliverables = $deliObj -> getDeliverables($_POST['start'], $_POST['limit'], $_POST['id'], "");
        $delArr		  = array();
        foreach ($deliverables as $index => $deliverable)
        {
        	foreach( $deliverable as $key => $val)
        	{
        		if( $key == "applicable_org_type"){
        		  if($val == ""){
				  	$delArr[$index][$key] = "";
        		  } else {
        		  	$orgObj = new OrganisationTypes();
        		  	$delArr[$index][$key] = $orgObj -> getOrganisationType( $val );	
        		  }	        			        			
        		} else if($key == "main_event") {
        		  if( $val == ""){                     
        		  	$delArr[$index][$key] = "";
        		  } else {
        		  	$evenObj = new Events();
        		  	$delArr[$index][$key] = $evenObj -> getEvent( $val );
        		  }	
        		} else {
        			$delArr[$index][$key] = $val;
        		}        		
        	}        	
        }
        $dels         = $this->_sortData( $delArr, new DeliverableColumns() );	
        echo json_encode( array("deliverables" => $dels, "headers" => $this -> headers, "total" => count($dels), "cols" => count($this->headers) ));
    }
    
    function getActions()
    {
        $deliObj   = new Action();
         $actions    = $deliObj -> getActions( $_POST['start'], $_POST['limit'],$_POST['id'], "");
        $dels         = $this->_sortData( $actions, new ActionColumns());
         echo json_encode( array("actions" => $dels, "headers" => $this -> headers,"cols" => count($this->headers), "total" => $deliObj -> getTotalActions( $_POST['id'], "") ));
    }
    

	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;

		}
	}

}
$method = $_GET['action'];
$setup = new SearchController();
if( method_exists($setup, $method )){
	$setup -> $method();
}
?>