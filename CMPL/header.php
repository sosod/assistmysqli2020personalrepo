<html>
<head>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>

<script type="text/javascript" src="/library/jquery/js/common.js"></script>
<link rel="stylesheet" href="/library/jquery-ui-1.8.24/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="../../assist.css" type="text/css">
<?php
include_once("inc_ignite.php");
$_SESSION['cc'] = "cassist";
$_SESSION['dbref'] = "assist_cassist_cmpl";
$scripts = array_merge( 
						array( "json2.js", "default.js"),
						(isset($scripts) ? $scripts : array()) 
					  );
if( isset($scripts) ){
foreach($scripts as $script) {
?>
	<script language="javascript" src="../js/<?php echo $script; ?>"></script>
<?php
	}
}
$styles = array( '' );
foreach( $styles as $style) {
?>
 	<link rel="stylesheet" type="text/css" href="../css/<?php echo $style; ?>" />
<?php
}
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		require_once("../../../library/dbconnect/".strtolower($classname).".php");
	}
}
$requestUri = preg_split('[\\/]', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY);
$count 		 = count($requestUri);
$pagename 	 = "";
$basename    = ""; //foldername used to get the submenu 
foreach( $requestUri as $key => $val)
{
	if( $val == $_SESSION['modlocation'])
	{
		continue;
	} else {	
		//if the page has a php extenstion get the basename, 
		if( strpos($val,".") > 0){
			$pagename = substr($val, 0, strpos($val, "."));
			$basename = $requestUri[$count-2];
		} else {
			$basename = $val;
			$pagename = $val;
		}				
	}		
}

$ctrl 		 = new Menu();
$submenu     = $ctrl -> getSubMenu( $basename );

if($pagename == "new"){
    $pagename = "new_legislation";
}
if($pagename == "manage")
{
    $pagename = "view";
}

?>
</head>
<body topmargin="0" leftmargin="5" bottommargin="0" rightmargin="5">
<?php

if(!empty($submenu)){
	foreach( $submenu as $key => $menu ) {
	$active = "";

	//echo "Pagename => ".$pagename." client name is ".$menu['name']."<br /><br />";
	if($pagename == strtolower($menu['name'])) {
		$active = "Y";    		
	} else if( strstr($pagename, $menu['name'])){
		$active = "Y";
	} else {
		$_client_name = substr($menu['name'], strpos($menu['name'], "_")+1 );
		//echo "Pagename => ".$pagename." New client name is  is ".$_client_name."<br /><br />";	
		if($pagename == $_client_name || strstr($pagename, $_client_name)){
			$active = "Y";
		} else {
			$active = "";
		}
	}	
	if($_SESSION['tid'] != "0001" )
	{
		if( $menu['id'] != "4"){
			$mainMenu[$menu['id']] = array(
												"id" 	  => $menu['id'],
												"url" 	  => strtolower(trim($menu['name'])).".php",
												"active"  => $active,
												"display" => $menu['client_terminology']) ;	
		}
	} else {
		$mainMenu[$menu['id']] = array(
											"id" 	  => $menu['id'],
											"url" 	  => strtolower(trim($menu['name'])).".php",
											"active"  => $active,
											"display" => $menu['client_terminology']) ;
	}
	}
	echoNavigation(1,$mainMenu); 

}
?>
<h3 style="text-align:left; clear:left;">
<?php
$otherNames = array(
		"edit_finyear" 				=> "Edit Financial Year",
		"edit_cstatus" 				=> "Edit Contract Status",
		"change_cstatus" 			=> "Change Contract Status",
		"edit_dstatus"	 			=> "Edit Deliverable Status",
		"change_dstatus" 			=> "Change Deliverable Status",
		"edit_dstatus"	 			=> "Edit Deliverable Status",	
		"edit_astatus"	 			=> "Edit Action Status",
		"change_astatus" 			=> "Change Action Status",
		"edit_tstatus"   			=> "Edit Task Status",
		"edit_ctype"	 			=> "Edit Contract Type",
		"change_ctype"   			=> "Change Contract Type",		
		"change_categorystatus" 	=> "Change Category Status",
		"edit_assfreq" 				=> "Edit Assessment Frequency",
		"change_assfreq" 			=> "Change Assessment Frequency",
		"edit_suppliercategory" 	=> "Edit Supplier Category",
		"change_suppliercategory" 	=> "Change Supplier Category",		
		"change_supplier"		  	=> "Change Supplier Status",
		"edit_dweight"			  	=> "Edit Delivered Weight",
		"change_dweight" 		  	=> "Change Delivered Weight",
		"edit_oweight"			  	=> "Edit Other Weight",
		"change_oweight" 		  	=> "Change Other Weight",
		"edit_qweight"			  	=> "Edit Quality Weight",
		"change_qweight" 		  	=> "Change Quality Weight",
		"edit_smatrix"			  	=> "Edit Delivered Score Matrix",						
		"change_smatrix"		  	=> "Change Delivered Score Matrix Status",						
		"edit_oscore"			  	=> "Edit Other Score Matrix",					
		"change_oscore"		  		=> "Edit Other Score Matrix Status",						
		"edit_qscore"			    => "Edit Quality Score Matrix",						
		"change_qscore"		  		=> "Change Quality Score Matrix Status",	
		"myprofile"			  		=> "My Profile",
		"edit_accountable"    		=> "Edit Accountable Person",
		"change_accountable_status" => "Change Accountable Person Status",
		"sel_compliace_frequency"	=> "Compliance Frequency",
		"sel_applicableorg"			=> "Deliverable Organisation Types",
		"sel_respdept"				=> "Responsible Departments",
		"sel_funcservice"			=> "Functional Services",
		"sel_accountable_person"	=> "Accountable Person",
		"sel_responsiblity_owner"	=> "Responsible Owners",
		"sel_compliance"			=> "Compliance To",
		"sel_mainevent"				=> "Main Event",
		"sel_subevent"				=> "Sub Event",
		"changelegtype_status"		=> "Change Legislative Type Status"	,
		"edit_legislativetypes"		=> "Edit Legislative Types",
		"changelegcategory_status"	=> "Change Legislation Category Status",
		"edit_legislativecategories"=> "Edit Legislative Categories",
		"edit_leg_organisation_types" => "Edit Legislation Organisation Types",
		"edit_orgsize"				=> "Edit Organisation Size",
		"edit_orgcapacity"			=> "Edit Organisation Capacity",
		"changeorgcapacity_status"	=> "Change Organisation Capacity Status",
		"responsible_organisations" => "Responsible Business Patner",
		"edit_organisation"			=> "Edit Responsible Business Patner",
		"change_organisation"		=> "Change Responsible Business Patner Status",
		"change_subevent"			=> "Change Sub Event Status",
		"change_actionowner_status" => "Change Action Owner Status",
		"change_leg_organisation_types" => "Change Legislation Organisation Types",													
	);

$ctrl -> wasWhere( $requestUri, $otherNames );

?>
</h3>

