<?php 
$scripts = array("deliverable.js");
include("../header.php");
$legislationObj = new Legislation();
$legislation    = $legislationObj -> getALegislation( $_GET['id'] );  
?>
<span class="message" id="deliverable_message"></span>
<table>
	<tr>
    	<td valign="top" width="50%">
        	<h6>Legislation Details</h6>
            <table width='100%'>
                <tr>
                    <th>Name Of Legislation</th>    
                    <td><?php echo $legislation['legislation_name']; ?></td>
                </tr>
                <tr>
                    <th>Legislation Reference</th>    
                    <td><?php echo $legislation['legislation_reference']; ?></td>
                </tr>    
                <tr>
                    <th>Type of Legislation</th>    
                    <td><?php echo $legislation['legislative_type']; ?></td>
                </tr>    
                <tr>
                    <th>Legislation Category</th>    
                    <td><?php echo $legislation['legislation_category']; ?></td>
                </tr>    
                <tr>
                    <th>Organisation Size</th>    
                    <td><?php echo $legislation['organisation_size']; ?></td>
                </tr>    
                <tr>
                    <th>Legislation Date</th>    
                    <td><?php echo $legislation['legislation_date']; ?></td>
                </tr>        
                <tr>
                    <th>Legislation Number</th>    
                    <td><?php echo $legislation['legislation_number']; ?></td>
                </tr>            
                <tr>
                    <th>Organisation Type</th>    
                    <td><?php echo $legislation['organisation_type']; ?></td>
                </tr>   
                <tr>
                    <th>Organisation Capacity Level</th>    
                    <td><?php echo $legislation['organisation_capacity']; ?></td>
                </tr>            
                <tr>
                    <th>Hyperlink to act</th>    
                    <td><?php echo $legislation['hyperlink_to_act']; ?></td>
                </tr> 
                <tr>
                    <td></td>    
                    <td>
                      <input type="button" name="save" id="save" value=" Save " />
                    </td>
                </tr>                    
            </table>
        </td>
        <td>
            <table>
            	<tr>
                	<th>Ref #</th>
                	<td></td>
                </tr>
                <tr>
                    <th>Long Description of Legislation Requirement</th>    
                    <td>
                    	<textarea name="description" id="description"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>Short Description</th>    
                    <td><input type="text" name="short_description" id="short_description" value="" /></td>
                </tr>    
                <tr>
                    <th>Legislation Section</th>    
                    <td>
                        <select name="legislation_section" id="legislation_section">
                            <option value="">--legislation section--</option>
                        </select>
                    </td>
                </tr>    
                <tr>
                    <th>Legislative Frequency of Compliance</th>    
                    <td><input type="text" name="compliance_frequency" id="compliance_frequency" value="" class="datepicker" /></td>
                </tr>    
                <tr>
                    <th>Applicable Organisation Type</th>    
                    <td>
                        <select name="applicable_org_type" id="applicable_org_type">
                            <option value="">--applicable organisation type--</option>
                        </select>        
                    </td>
                </tr>    
                <tr>
                    <th>Legislative Compliance Date</th>    
                    <td>
                      <input type="text" name="compliance_date" id="compliance_date" value="" class="datepicker" />
                    </td>
                </tr>        
                <tr>
                    <th>Responsible Department</th>    
                    <td>
                        <select name="responsible" id="responsible">
                            <option value="">--responsible department--</option>
                        </select>
                    </td>
                </tr>            
                <tr>
                    <th>Functional Service</th>    
                    <td>
                        <select name="functional_service" id="functional_service">
                            <option  value="">--functional service--</option>
                        </select>        
                    </td>
                </tr>   
                <tr>
                    <th>Accountable Person</th>    
                    <td>
                        <select name="accountable_person" id="accountable_person">
                            <option  value="">--accountable person--</option>
                        </select>        
                    </td>
                </tr>            
                <tr>
                    <th>Responsible Owner</th>    
                    <td>
                        <select name="responsibility_owner" id="responsibility_owner">
                            <option value="">--responsibility owner--</option>
                        </select>        
                    </td>
                </tr> 
                <tr>
                    <th>Legislative Deadline date</th>    
                    <td>
                      <input type="text" name="legislation_deadline" id="legislation_deadline" value="" class="datepicker" />
                    </td>
                </tr>                 
                <tr>
                    <th>Action Deadline date</th>    
                    <td>
                      <input type="text" name="action_deadline" id="action_deadline" value="" class="datepicker" />
                    </td>
                </tr>                                 
                <tr>
                    <th>Reminder Date</th>    
                    <td>
                      <input type="text" name="reminder" id="reminder" value="" class="datepicker" />
                    </td>
                </tr> 
                <tr>
                    <th>Sanction</th>    
                    <td>
                      <input type="text" name="sanction" id="sanction" value="" />
                    </td>
                </tr>                                                           
                <tr>
                    <th>Reference Links</th>    
                    <td>
                      <input type="text" name="reference_link" id="reference_link" value="" />
                    </td>
                </tr> 
                <tr>
                    <th>Organisation KPI Ref</th>
                    <td>
                      <input type="text" name="org_kpi_ref" id="org_kpi_ref" value="" />
                    </td>
                </tr>                                                           
                <tr>
                    <th>Individual KPI Ref</th>    
                    <td>
                      <input type="text" name="individual_kpi_ref" id="individual_kpi_ref" value="" />
                    </td>
                </tr>   
                <tr>
                    <th>Assurance/ Proof of evidence required</th>    
                    <td>
                      <input type="text" name="assurance" id="assurance" value="" />
                    </td>
                </tr> 
                <tr>
                    <th>Compliance To</th>
                    <td>
                        <select name="compliance_to" id="compliance_to">
                            <option value="">--compliance to--</option>
                        </select>        
                    </td>
                </tr>                
                <tr>
                    <th>Main Event</th>    
                    <td>
                        <select name="main_event" id="main_event">
                            <option value="">--main event--</option>
                        </select>        
                    </td>
                </tr>              
                <tr>
                    <th>Sub Event</th>    
                    <td>
                        <select name="sub_event" id="sub_event">
                            <option value="">--sub event--</option>
                        </select>        
                    </td>
                </tr>
                <tr>
                    <th>Guidance</th>
                    <td>
                      <input type="text" name="guidance" id="guidance" value="" />
                    </td>
                </tr>
                <tr>
                    <th>Import</th>
                    <td>
                      <input type="submit" name="import" id="import" value="Import" />
                    </td>
                </tr>                 
                <tr>
                    <td></td>    
                    <td>
                      <input type="submit" name="save_deliverable" id="save_deliverable" value="Save" />
                      <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_GET['id']; ?>" />
                    </td>
                </tr>                    
            </table>        
        </td>
    </tr>
</table>