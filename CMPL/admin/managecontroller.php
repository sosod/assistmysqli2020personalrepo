<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		require_once("../../../library/dbconnect/".strtolower($classname).".php");
	}
}
class ManageController extends DBConnect {
	
	var $headers = array();
	
	function getLegislativeTypes()
	{
		$legtypeObj = new LegislativeTypes();
		$legtypes   = $legtypeObj -> getLegislativeTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getLegislativeCategories()
	{
		$legcatObj = new LegislativeCategories();
		$legcats   = $legcatObj -> getLegislativeCategories();
		echo json_encode( $legcats ); 
	}

	function getOrganisationSizes()
	{
		$orgsizeObj = new OrganisationSize();
		$orgsizes   = $orgsizeObj -> getOrganisationSize();
		echo json_encode( $orgsizes ); 
	}

	function getOrganisationTypes()
	{
		$orgtypeObj = new OrganisationTypes();
		$legtypes   = $orgtypeObj -> getOrganisationTypes();
		echo json_encode( $legtypes ); 
	}

    function getSubEvents()
	{
		$obj = new SubEvent();
		echo json_encode( $obj -> getMainSubEvents( $_POST['id']) );
	}	
	
	function getOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$legtypes   = $orgtypeObj -> getOrganisationCapacity();
		echo json_encode( $legtypes ); 
	}
	
	function getUsers()
	{
		$obj = new UserAccess();
		echo json_encode( $obj -> getUsers() );					
	}
	
	function saveLegislation()
	{
		$ligObj = new Legislation();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;		
		}
		$res 	  = $ligObj -> save( $insertdata );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Legislation saved", "error" => false, "id" => $res );
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true);
		}
		echo json_encode( $response );
	}
	
	function deleteLegislation()
	{
		$legObj  = new Legislation();
		$legislation = $legObj->getALegislation( $_POST['id'] );
		$insertdata['status']  = $legislation['status'] + 2;

		$res 	 = $legObj->updateLegislation($insertdata, $_POST['id'] ); 
		
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Legislation deleted", "error" => false, "id" => $res );
		} else {
			$response = array("text" => "Error deleting the legislation", "error" => true);
		}
		echo json_encode( $response );	
	}
	
	function editLegislation()
	{
		$legObj  = new Legislation();
		$changes = array(); 
		$legislation = $legObj->getALegislation( $_POST['id'] );
		$changes 		   = $this->_processLegislationChanges( $_POST['data'], $legislation );
		$insertdata = array();
		$res		= "";
		if( isset($changes['changeArray']) && !empty($changes['changeArray'])) {
			$changes['changeArray']['user'] = $_SESSION['tkn'];
			$changes['changeArray']['currentstatus'] = "New";
			$insertdata = array("legislation_id" => $_POST['id'], "changes" =>  base64_encode(serialize($changes['changeArray'])), "insertuser" => $_SESSION['tid'] );
			$res 	 	= $legObj->editLegislation($insertdata, $changes['update'], $_POST['id'] );
		} else {
			$res = 0;
		}		
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Legislation updated", "error" => false, "id" => $res );
		} else if($res == 0){
			$response = array("text" => "No change was made to the legislation", "error" => false);
		} else {
			$response = array("text" => "Error updating the legislation", "error" => true);
		}
		echo json_encode( $response );

	}
	
	function _processLegislationChanges( $postArr, $legislation )
	{
		$naming    = new Naming();
		$naming->getNaming();
		$changeArr  = array();
		$insertData = array();
		$updateData = array();
		$changeMessage 	   = $_SESSION['tkn'];
		$multiple 	= array("category", "organisation_size", "organisation_type", "organisation_capacity", "");

		foreach( $multiple as $field)
		{
			if( array_key_exists($field, $postArr))
			{
				if( is_array($postArr[$field]) && !empty($postArr[$field])){
					$postArr[$field] = implode(",", $postArr[$field]);
				}
			}
		}
		$legislation['responsible_organisation'] = $legislation['responsibleorganisation'];
		foreach( $postArr as $key => $value)
		{
			if( array_key_exists($key, $legislation))
			{
				if($value != $legislation[$key])
				{
					if($key == "type"){
						$typeObj  = new LegislativeTypes();
						$fromType = $typeObj->getALegislativeType( $legislation[$key] );
						$from 	  = $fromType['name'];
						$toType   = $typeObj->getALegislativeType( $value );
						$to 	  = $toType['name'];						
						
						$updateData[$key] = $value;
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";
						
					} else if($key == "category"){
						$legcat    = new LegislativeCategories();
						$from  	   = $legcat->getLegislativeCategory( $legislation[$key] );
											
						$to        		  = $legcat->getLegislativeCategory( $value );	
						$updateData[$key] = $value;
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
					} else if($key == "organisation_size"){
						$sizeObj = new OrganisationSize();
						$from    = $sizeObj->getOrganisationSizes( $legislation[$key]);
						$to  	 = $sizeObj->getOrganisationSizes( $value );				
						$updateData[$key] = $value;
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage  .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
					} else if($key == "organisation_type"){
						$otypeObj = new OrganisationTypes();
						$from     = $otypeObj->getOrganisationType( $legislation[$key]);
						$to       = $otypeObj->getOrganisationType( $value );				
						$updateData[$key] = $value;
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
					} else if($key == "organisation_capacity"){
						$capObj   = new OrganisationCapacity();
						$from     = $capObj->getOrganisationCapacities($legislation[$key] );
						$to   	  = $capObj->getOrganisationCapacities( $value );											
						$updateData[$key] = $value;
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
					} else if($key == "responsible_organisation"){
						$respoOrg = new ResponsibleOrganisation();

						$fromOrg = $respoOrg->getAResponsibleOrganisation( $legislation['responsibleorganisation'] );
						$from 	 = $fromOrg['name'];
						$toOrg   = $respoOrg->getAResponsibleOrganisation( $value );
						$to 	 = $toOrg['name'];						
						
						$updateData[$key] = $value;
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
					} else {
						
						$updateData[$key] = $value;						
						$changeArr[$key]  = array("from" => $legislation[$key], "to" => $value);						
						$changeMessage   .= $naming->setHeader($key)." has changed to ".$legislation[$key]." from ".$value."\r\n\n";						
					}
				}
			}			
		}

		return array("changeArray" => $changeArr, "message" => $changeMessage, "update" => $updateData );
	}
	
	function getLegislationEdits()
	{
		$legObj = new Legislation();
		$legislations = $legObj -> getEditLogs ( $_POST['id'] );
		$changes 	     = array();
		foreach($legislations as $key => $delArr){
			$changeMessage = $this->_extractChanges( $delArr );
			$changes[$delArr['insertdate']] = array(
													   "date" 	 => $delArr['insertdate'],
													   "changes" => $changeMessage,
													 );
		}
		echo json_encode($changes);
	}
	
	
	function getLegislations()
	{
		$ligObj = new Legislation();
		Naming::$section  = $_POST['section'];
		$options = " AND L.status & ".Legislation::CONFIRMED."  = ".Legislation::CONFIRMED."";
		if($_SESSION['tid'] != "0001"){
			$businessPatnerObj = new ResponsibleOrganisation();
			$businessPartner = $businessPatnerObj -> getBussinessPartners();
			$bpId = "";
			foreach( $businessPartner as $bIndex => $bValue)
			{
				$userIds = array();
				$userIds = explode(",", $bValue['user_id']);
				if( in_array($_SESSION['tid'], $userIds))
				{
					$bpId = $bValue['business_partner_id'];	
				}	
			}		
			$options .= " AND L.responsible_organisation = ".($bpId == "" ? 0 : $bpId);  
		}
		$legislations = $ligObj -> getLegislations($_POST['start'], $_POST['limit'], $options); 		
		$catObj = new LegislativeCategories();
		$capObj = new OrganisationCapacity();
		$sizeObj = new OrganisationSize();
		$typeObj = new LegOrganisationTypes();
		$legislationArray = array();
		$legOwners = array();
		foreach($legislations as $key => $value)
		{
			foreach($value as $index => $val){
				if($index == "category" )
				{
					$legislationArray[$key]['legislation_category'] = $catObj -> getLegislativeCategory( $val );
				}
				if($index == "organisationsize"){
					$legislationArray[$key]['organisation_size'] = $sizeObj -> getOrganisationSizes( $val );
				} if($index == "organisationcapacity"){
					$legislationArray[$key]['organisation_capacity'] = $capObj -> getOrganisationCapacities( $val );
				}  if($index == "organisationtype"){
					$legislationArray[$key]['organisation_type'] = $typeObj -> getOrganisationType( $val );
				}
				
				if($index == "business_partner")
				{
					if($_SESSION['tid'] == "0001"){
						//echo "Coming here and ffrom ddb is ".$val." and logged on is ".$_SESSION['tid']."  ---------  ";
						$legOwners[$value['ref']] =  $_SESSION['tid'];
						//echo "After assigning to ref , it becomes ".$legOwners[$value['ref']]."\r\n\n";
					} else {
						$legOwners[$value['ref']] =  $val;
					}
				} else {
					$legislationArray[$key][$index] = $val;
				}
				
		    }			
		}		
		$response = array();
//		/$legs = $this->_sortData( $legislationArray );
		$totalLeg = $ligObj->getTotalLegislation( $options );
		$legs = $this->_sortData( $legislationArray, array(), new LegislationColumns());
		echo json_encode( array("legislation" => $legs,
								"owners" 	  => $legOwners,
						 		"headers" 	  => $this -> headers ,
								"cols" 		  => count($this->headers),
								"total" 	  => $totalLeg
						 ));
	}

		
	function _sortData( $data, $optionsHeaders = array(), Naming $naming )
	{
		$headers   = $naming -> sortedNaming( $naming );
		$returnData = array();
		if(empty($data))
		{
			$this->headers = $headers;
		} else {
			foreach( $data  as $key => $value){
			   foreach( $headers as $index => $val){
				if(isset($value[$index]) || array_key_exists($index, $value)){
						$this->headers[$index]  = $val;
						$returnData[$value['ref']][$index] = $value[$index];
					}
				}
			}
		}
		return $returnData;
	}

    function updateDeliverable()
    {
        $response = array();
        $insertdata = array();
        $deliObj    = new Deliverable();
        foreach( $_POST['data'] as $key => $deliverable)
        {
                $insertdata[$key] = $deliverable;
        }
        $res     = $deliObj -> save($insertdata);
        if( $res > 0){
            $response = array("text" => "Deliverable saved", "error" => false);
        } else {
            $response = array("text" => "Error saving deliverables", "error" => true);
        }
        echo json_encode($response);
    }

    
    function deleteDeliverable()
    {
        $response = array();
        $insertdata = array();
        $deliObj    = new Deliverable();
        $deliverable = $deliObj ->getADeliverable($_POST['id']);
        $id 		= $_POST['id'];
        $insertdata['status'] = $deliverable['status'] + 2;
        //foreach( $_POST['data'] as $key => $deliverable)
        //{
           //$insertdata[$key] = $deliverable;
        //}
        $res     = $deliObj -> updateDeliverable($insertdata, $id);
        if( $res > 0){
            $response = array("text" => "Deliverable deleted", "error" => false);
        } else {
            $response = array("text" => "Error deleting deliverables", "error" => true);
        }
        echo json_encode($response);
    }
    
    function getDeliverables()
    {
        $deliObj      = new Deliverable();
        Naming::$section  = $_POST['section'];
        $deliverables = $deliObj -> getDeliverables($_POST['start'], $_POST['limit'], $_POST['id'], "");
        $delArr		  = array();
        foreach ($deliverables as $index => $deliverable)
        {
        	foreach( $deliverable as $key => $val)
        	{
        		if( $key == "applicable_org_type"){
        		  if($val == ""){
				  	$delArr[$index][$key] = "";
        		  } else {
        		  	$orgObj = new OrganisationTypes();
        		  	$delArr[$index][$key] = $orgObj -> getOrganisationType( $val );	
        		  }	        			        			
        		} else if($key == "main_event"){
        		  if( $val == ""){
        		  	$delArr[$index][$key] = "";
        		  } else {
        		  	$evenObj = new Events();
        		  	$delArr[$index][$key] = $evenObj -> getEvent( $val );
        		  }	
        		} else {
        			$delArr[$index][$key] = $val;
        		}        		
        	}        	
        }        
        $dels         = $this->_sortData( $delArr, array(), new DeliverableColumns() );
        $totalDel = $deliObj -> getTotalDeliverables($_POST['id'], "");
        
        echo json_encode( array("deliverables" => $dels, "headers" => $this -> headers, "total" => $totalDel['total'], "cols" => count($this->headers) ));
    }

    function editDeliverable()
    {
		$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();
        $insertdata  = array();
        $updatedata  = array();
        $response    = array();
        $multiple   = array("applicable_org_type", "main_event", "sub_event", "compliance_to");       
        foreach( $_POST['data'] as $key => $deliverable)
        {
            if(in_array($key, $multiple))
            {
            	if( is_array($deliverable) && !empty($deliverable)){
                	$updatedata[$key] = implode(",",$deliverable);
            	} else {
            		$updatedata[$key] = 0;
            	}
            } else{
                $updatedata[$key] = $deliverable;
            }       	
        }           
        $deliverable = $deliObj ->getADeliverable($_POST['id']);
        $changes     = $this  -> _processDeliverableChanges( $updatedata, $deliverable);
        if(isset($changes['change']) && !empty($changes['change'])){
            $changes['change']['user'] = $_SESSION['tkn'];
            $changes['change']['currentstatus'] = "New";
        	$insertdata  = array("changes" => base64_encode( serialize($changes['change']) ), "deliverable_id" => $_POST['id'], "insertuser" => $_SESSION['tid'] );
            $res         = $deliObj -> editDeliverable($_POST['id'] ,$insertdata, $updatedata );
        } else {
        	$res = 0;
        }      

        if( $res > 0){
            $response = array("text" => "Deliverable updated" , "error" => false);
        } else if($res == 0){
        	$response = array("text" => "No change was made to the deliverable" , "error" => false);
        } else {
            $response = array("text" => "Error updating deliverable" , "error" => true );
        }
        echo json_encode($response);
    }

    function getDeliverableEdits()
    {
		$delObj 	     = new Deliverable();
		$deliverableEdit = $delObj -> getEditLogs ( $_POST['id'] );
		$changes 	     = array();
		foreach($deliverableEdit as $key => $delArr){
			$changeMessage = $this->_extractChanges( $delArr );
			$changes[$delArr['insertdate']] = array(
													   "date" 	 => $delArr['insertdate'],
													   "changes" => $changeMessage,
													 );
		}
		echo json_encode($changes);
    }

    function getActions()
    {
        $deliObj   = new Action();
        Naming::$section  = $_POST['section'];
        $actions    = $deliObj -> getActions( $_POST['start'], $_POST['limit'],$_POST['id'], "");
        $dels         = $this->_sortData( $actions, array(), new ActionColumns() );
        echo json_encode( array("actions" => $dels, "headers" => $this -> headers,"cols" => count($this->headers), "total" => $deliObj -> getTotalActions( $_POST['id'], "") ));
    }
    
	function editAction()
	{
		$actionObj 	 = new Action();
		$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();
        
		$updatedata  = array();
        $insertdata  = array();
		$remindon    = (isset($_POST['reminder']) ? $_POST['reminder'] : "" );

		$action      = $actionObj -> getAction( $_POST['id'] );
        
		if( $this->_checkValidity($_POST['deadline'], $remindon)){

                $changes                = $this->_changeProcessor($_POST, $action);
                $insertdata['action_id']= $_POST['id'];
                $insertdata['changes']  = base64_encode( serialize($changes['changes']) );
				foreach( $_POST as $field => $value ){
					$updatedata[$field] = $value;
				} 

				$res 	  = $actionObj -> editActionChanges( $_POST['id'], $insertdata, $updatedata  );
				$mailText = "";
				if( $res == 1){
					$response = array("text" => "Action updated successfully <br />".$mailText , "error" => false);
				} else {
					$response = array("text" => "Error updating the action" , "error" => true);
				}
		} else {
			$response = array("error" => true, "text" => "Remind on date cannot be after than deadline date");
		}
		echo json_encode( $response );
	}
	
	function updateAction()
	{
		$actionObj 	 = new Action();
		$action      = $actionObj -> getAction( $_POST['id'] );
		$updatedata['actionstatus']  = $action['status'] + 2;
		$res = $actionObj -> updateAction( $updatedata, $_POST['id'] );
		if( $res == 1){
			$response = array("text" => "Action updated successfully <br />" , "error" => false);
		} else {
			$response = array("text" => "Error updating the action" , "error" => true);
		}
		echo json_encode($response);
	}

    function getActionEdits()
    {
		$act 		   = new Action();
		$actionEdits   = $act->getActionEdit ($_POST['id']);
		$changes 	   = array();
		foreach($actionEdits as $key => $editArr){
			$changeMessage = $this->_extractChanges( $editArr );
			$changes[$editArr['insertdate']] = array(
													   "date" 	 => $editArr['insertdate'],
													   "changes" => $changeMessage,
													 );
		}
		echo json_encode($changes);
    }

    function _processDeliverableChanges( $postArr, $deliverable)
    {    	
        $headerObj  = new Naming();
        $headerObj->getNaming();
        $updatedata = array();
        $changeMessage = "";
        $changeArray    = array();
        $changeMessage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
        //$usersGroup = array("responsible", "responsibility_owner", "compliance_to", "accountable_person");
       /* if( is_array($postArr['main_event']) && !empty($postArr['main_event'])){
        	$postArr['main_event'] = implode(",", $postArr['main_event'] ); 
        }
        if( is_array($postArr['applicable_org_type']) && !empty($postArr['applicable_org_type'])){
        	$postArr['applicable_org_type'] = implode(",", $postArr['applicable_org_type'] ); 
        } */   
        $postArr['sub_event'] 		= ($postArr['sub_event'] == "" ? 0 : $postArr['sub_event']);
        $deliverable['sub_event']	= ($deliverable['sub_event'] == "" ? 0 : $deliverable['sub_event'] ); 
        foreach( $deliverable as $key => $delVal)
        {
            if(isset($postArr[$key]) || array_key_exists($key, $postArr))
            {
            	//echo $key." --- ".$postArr[$key]." => ".$delVal." \r\n\n";
                if($postArr[$key] !== $delVal)
                {
                    if($key == "compliance_frequency") {

                        $secObj  = new ComplianceFrequency();
                        $fromSec = $secObj ->getComplianceFrequency( $delVal );
                        $from    = $fromSec['name'];

                        $toSec   = $secObj ->getComplianceFrequency( $postArr[$key] );
                        $to      = $toSec['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "responsible") {

						$dirObj      = new Directorate();
						$fromDir     = $dirObj -> getSubDirectorate( $delVal );
                        $from     	 = $fromDir['dirtxt'];

                        $toDir   	= $dirObj -> getSubDirectorate( $postArr[$key]);
                        $to       	= $toDir['dirtxt'];

                        $changeArray['responsible_department'] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader('responsible_department')." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "responsibility_owner") {

						$respoObj	= new ResponsibleOwner();
						$fromOwner  = $respoObj -> getResponsibleOwner( $delVal );
                        $from     	= $fromOwner['name'];

                        $toOwner   	= $respoObj -> getResponsibleOwner( $postArr[$key] );
                        $to       	= $toOwner['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "compliance_to") {

						$compObj    = new ComplianceTo();
							
						$from  = $compObj -> getCompliances( $delVal );
						
                        $to 		= $compObj -> getCompliances( $postArr[$key] );
             
                        $changeArray['compliance_name_given_to'] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader('compliance_name_given_to')." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "applicable_org_type") {

						$typeObj = new OrganisationTypes();
						$from  = "";
						$to    = "";
						if( $delVal == "" ) {
							$from = "";
						} else {
							$from  = $typeObj->getOrganisationType( $delVal );
						}
						
						if( $postArr[$key] == "") {
							$to = "";
						} else {
							$to = $typeObj->getOrganisationType( $postArr[$key] );	
						}
						//echo "from => ".$from." and to ".$to." \r\n\n";
						$changeArray[$key] = array("from" => $from, "to" => $to);
						$changeMessage  .= $headerObj->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";
                    } else if($key == "functional_service") {
 
                        $funcObj  = new FunctionalService();
                        $fromFunc = $funcObj -> getAFunctionalService( $delVal );
                        $from     = $fromFunc['name'];

                        $toFunc   = $funcObj -> getAFunctionalService( $postArr[$key] );
                        $to       = $toFunc['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "accountable_person") {

						$accObj     = new AccountablePerson();
                        $fromUser	= $accObj ->getAccountablePerson( $delVal );
                        $from    	= $fromUser['name'];

                        $toUser  	= $accObj ->getAccountablePerson( $postArr[$key] );
                        $to      	= $toUser['name'];
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "main_event") {
                        $eventObj  = new Events();
                        $to   = "";
                        $from = "";
						if( $delVal == ""){
							$from = "";
						} else {
                        	$from = $eventObj ->getEvent( $delVal );
						}
                        if( $postArr[$key] == ""){
                        	$to = "";
                        } else {
                       		$to = $eventObj ->getEvent( $postArr[$key] );
                        }
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "sub_event") {
                        $subEventObj  = new SubEvent();
                        $from    = $subEventObj ->getASubEvent( $delVal );

                        $to      = $subEventObj -> getASubEvent( $postArr[$key] );
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";

                    }  else {
                        $changeArray[$key] = array("from" => $deliverable[$key], "to" => $postArr[$key] );
                        $changeMessage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$deliverable[$key]."\n\n\n\n";
                        //$insertdata[$key]  = ;
                    }
                }
            }
        }      
      
        $changes = array("change" => $changeArray, "message" => $changeMessage );
        return $changes;
    }


    function _extractChanges( $updateArray )
	{
		$naming		= new Naming();
        $naming->getNaming();
		$adds		= array("user", "date_completed", "saving", "attachments");
		$changeMessage = "";
		if( isset($updateArray['changes']) && !empty($updateArray['changes']) ){
			$changesArr = unserialize( $updateArray['changes'] );
			foreach( $changesArr  as $index => $val ){
				if( in_array($index,$adds) ){
					$changeMessage .= $val."<br /><br />";
				} else if( $index == "remindon" ){
					if(is_array($val)){
						$changeMessage .= (isset($headers[$index]) ? $headers[$index] : ucwords(str_replace("_"," ", $index))) ." has changed to ".$val['to']." from ".$val['from']."<br />";
					} else {
						$changeMessage .= ucwords(str_replace("_"," ", $index))." ".$val."<br />";
					}
				} else if( $index == "response" ){
					$changeMessage .= $val."<br />";
				} else {
					if( array_key_exists("to", $val) && array_key_exists("to", $val)){
						$changeMessage .= $naming -> setHeader( $index ) ." has changed to ".$val['to']." from ".$val['from']."<br />";
					}
				}
			}
		}
		return $changeMessage;
	}

	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;

		}
	}

    function _changeProcessor( $postArr , $action)
    {
        $headerObj  = new Naming();
        $headerObj->getNaming();
		$headers    = $headerObj -> sortedNaming( $headerObj );
        $updatedata = array();
        $changeMesssage = "";
        $changeArray    = array();
        $changeArray['user'] = $_SESSION['tkn'];
        $changeMesssage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
        $changeArray['currentstatus'] = "New";
         foreach( $action as $key => $val)
            {
                if(isset($postArr[$key]) || array_key_exists($key, $postArr))
                {
                    if( $postArr[$key] != $val)
                    {
                        if($key  == "owner"){
                            $userObj = new ActionOwner();
                            $fromUser= $userObj ->getActionOwner( $val );
                            $from    = $fromUser['name'];
                            
                            $toUser  = $userObj ->getActionOwner( $postArr[$key] );
                            $to      = $toUser['name'];

                            $changeArray['action_owner'] = array("from" => $from, "to" => $to );
                            $changeMesssage   .= $headerObj -> setHeader('action_owner')." changed to ".$to." from ".$from."\n\n\n\n";
                        } else {
                            $changeArray[$key] = array("from" => $action[$key], "to" => $postArr[$key] );
                            $changeMesssage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$action[$key]."\n\n\n\n";
                            //$insertdata[$key]  = ;
                        }
                    }
                }
         }
        $changes = array("changes" => $changeArray, "message" => $changeMesssage);
        return $changes;
    }
	
	function activateLegislation()
	{
		$ligObj = new Legislation();
		$legislation = $ligObj -> getALegislation($_POST['id'] ); 
		$updatedata  = array("status" => $legislation['status'] + Legislation::ACTIVATED);
		$res 		 = $ligObj -> updateLegislation( $updatedata, $_POST['id'] );
		$response 	 = array(); 
		if( $res > 0 )
		{
			$response	= array("text" => "Legislation successfully activated" , "error" => false);
		} else if( $res == 0) {
			$response	= array("text" => "Legislation successfully activated" , "error" => false);			
		} else {
			$response	= array("text" => "Error updating the legislation" , "error" => true);
		}
		echo json_encode( $response );
		
	}    
    
	function deleteAction()
	{
		$id = $_POST['id'];
		$actionObj  = new Action(); 
		$updatedata = array("actionstatus" => 2);
		$res 	   = $actionObj -> updateAction( $updatedata, $id);
		$response  = array();
		if( $res == 1 )
		{
			$response = array("text" => "Action successfully deleted", "error" => false);
		} else if( $res == 0){
			$response = array("text" => "There was no change to the action", "error" => true);
		} else {
			$response = array("text" => "Error deleting the action", "error" => true);			
		}
		echo json_encode( $response );
	}
}
$method = $_GET['action'];
$setup = new ManageController();
if( method_exists($setup, $method )){
	$setup -> $method();
}
?>