// JavaScript Document
$(function(){
	Acts.get();	
	
	$("#save_act").click(function(){
		Acts.save();
		return false;						  
	});
	
	$("#edit_act").click(function(){
		Acts.edit();
		return false;
	});
	
	$("#update_act").click(function(){
		Acts.update();
		return false;
	});
});

Acts 		= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getActs", function( response ){
			$.each( response, function( index, act){
				Acts.display( act )					   
			})												  											  
		},"json");
	} , 
	
	display		: function( act )
	{
		$("#acts_table")
		 .append($("<tr />",{id:"tr_"+act.id})
		   .append($("<td />",{html:act.id}))
		   .append($("<td />",{html:act.name}))
		   .append($("<td />",{html:act.description}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+act.id, id:"edit_"+act.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+act.id, id:"del_"+act.id, value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((act.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<tr />")
			  .append($("<input />",{type:"button", name:"change_"+act.id, id:"change_"+act.id, value:"Change Status"}))			  					
		    )		   
		 )
		 		 
		 $("#edit_"+act.id).live("click", function(){
			document.location.href = "edit_act.php?id="+act.id;								   
			return false;								   
		  });
		 
		 $("#del_"+act.id).live("click", function(){
			var message 	 = $("#acts_message").show(); 

			if( confirm("Are you sure you want to delete this act")){
				message.html("Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateAct", { id : act.id , status : 2 }, function( response ){							  					$("#tr_"+act.id).fadeOut();																		  																																													 					message.html("");
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+act.id).live("click", function(){
			document.location.href = "changeact_status.php?id="+act.id;										 
		 });
		
	} , 
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#acts_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			message.html("Please enter the act name");
			return false;
		} else if( data.description == ""){
			message.html("Please enter the description")
			return false;
		} else {
			message.html("Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveAct", { data : data }, function( response ){		
				data.id  	= response.id; 																   
				Acts.display( data )																   
				Acts.empty( data );
				message.html(response.text);																   
			},"json");	
		}
		
		
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#acts_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			message.html("Please enter the act name");
			return false;
		} else if( data.description == ""){
			message.html("Please enter the description")
			return false;
		} else {
			message.html("Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateAct", { id : $("#actid").val(), data : data }, function( response ){		
				message.html( response.text );																   
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var message 	 = $("#acts_message").show(); 
		var status		= $("#status :selected").val();
		message.html("Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateAct", {  id : $("#actid").val(), status : status }, function( response ){																																																									 					message.html( response.text );
		}, "json");
	}
	
	
	
	
}