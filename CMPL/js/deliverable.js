// JavaScript Document
$(function(){
	if( $("#deliverableid").val() == undefined){
		Deliverable.getUsers();
		Deliverable.getOrganisationSizes();
		Deliverable.getOrganisationTypes();
		Deliverable.getFunctionalService();
		Deliverable.getMainEvents();
		Deliverable.getDepartments();
		Deliverable.getComplianceFrequency();
		Deliverable.getSections();
		Deliverable.getResponsibleOwner();		
		Deliverable.getAccountablePerson();		
		Deliverable.getComplianceTo();							
		Deliverable.getSubEvents(1);		
    }

	$("#has_actions").change(function() {
		
		if( $(this).val() == 0)
		{
			var deadlinedate = $("#legislation_deadline").val();
			$("<div />",{id:"hasactions_dialog"})
			.append($("<p />",{html:"To enable the system to track and report on the compliance process , please complete the fields below"}).addClass("ui-state").addClass("ui-state-highlight"))
			.append($("<table />",{id:"table_actioninfo"})
				.append($("<tr />")
				  .append($("<th />",{html:"Action Deadline Date:"}))
				  .append($("<td />")
					.append($("<input />",{type:"text", name:"action_deadlinedate", id:"action_deadlinedate", value:deadlinedate}))	  
				  )
				)	
				.append($("<tr />")
				  .append($("<th />",{html:"Action Owner:"}))
				  .append($("<td />")
					 .append($("<select />",{id:"action_owner", name:"action_owner"})
					   .append($("<option />",{id:"", text:"action owner"}))		 
					 )	  
				  )
				)			
				).dialog({
					autoOpen	: true, 
					modal		: true,
					width		: "500px",
					title		: "Add Action",
					buttons		: {
									"Ok"	: function()
									{
										$("#actionowner").val( $("#action_owner :selected").val() )
										$("#actiondeadlinedate").val( $("#action_deadlinedate").val() )
										$("#hasactions_dialog").dialog("destroy");
										$("#hasactions_dialog").remove();
									} , 
									"Cancel"	: function()
									{
										$("#hasactions_dialog").dialog("destroy");
										$("#hasactions_dialog").remove();
									}
								 }
				});
				
				$("#action_deadlinedate").datepicker({
					showOn			: "both",
					buttonImage 	: "/library/jquery/css/calendar.gif",
					buttonImageOnly	: true,
					changeMonth		: true,
					dateFormat 		: "dd-M"
				});	
		}
		
		Deliverable.getActionOwner();
		return false;
	});
	
	$("#recurring_type").live("click", function(){
		if( $(this).val() == "fixed"){
			$(".fixed").show();			
			$("#days").hide()
		} else {
			$(".fixed").hide();
			$("#days").show();
		}		
		return false;
	});
	
	$("#recurring").change(function() {
		if( $(this).val() == "") {  
			$("#more_details").hide();
			$(".recurr").hide();
		} else if( $(this).val() == 0) {
			$(".recurr").hide();
			$(".fixed").show();
		} else {
			$(".fixed").hide();
			$(".nonfixed").show();
			$(".recurr").show();
		}
		return false;
	});
	
	$("#compliance_date").live("change", function(){
		var dateObj = new Date();
		var year    = dateObj.getFullYear();
		$("#legislation_deadline").val( $(this).val()+"-"+year );
		return false;
	});
	
	$("#legislation_deadline").live("change", function(){
		var thisdate = $(this).val();
		if( $("#compliance_date").val() != "")
		{
			if( $("#compliance_date").val() < thisdate  )
			{
				$("<div />",{id:"correctdate_dialog"}).append($("<p />",{html:"The deadline date should not be greater / after the compliance date "+$("#compliance_date").val()}).addClass("ui-ste"))
				.dialog({
						autoOpen	: true,
						modal		: true,
						title		: "Correct Date", 
						buttons		: {
										"Ok"	: function()
										{
											$("#legislation_deadline").val( $("#compliance_date").val() )
											$("#correctdate_dialog").dialog("'destroy");
											$("#correctdate_dialog").remove();
										} 
									  }					
				});				
			}			
		}
		return false;
	});
	
	$("#save_deliverable").click(function(){
		Deliverable.save();
		return false;					  
	});

	$("#edit_deliverable").click(function(){
		Deliverable.edit();
		return false;
	});

    $("#main_event").live("change", function(){
	    Deliverable.getSubEvents( $(this).val() );
        return false;
    });

    $("#main_event_").live("change", function(){
	    Deliverable.getSubEvent( $(this).val() );
        return false;
    });
        
    
    $("#view_edit_log").live("click",function(){
       Deliverable.viewEditLog();
       return false;
    });

    $("#import").live("click",function(){
       document.location.href = "import_deliverable.php?legislationid="+$("#legislationid").val();;
       return false;
    });
    
    $("#delete").click(function(){
    	Deliverable.deleteDeliverable();
    	return false;
    });
    
	$(".idatepicker").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		dateFormat 		: "dd-M"
	});

});

Deliverable			= {
		
	getActionOwner 		: function (){
	$.getJSON("newcontroller.php?action=getActionOwners", function( users ){
		$.each( users, function( index, user){
				$("#action_owner").append($("<option />",{value:user.id, text:user.name}))									
			})													 
		});
	} ,
	getComplianceFrequency	: function(){
		$.post("newcontroller.php?action=getComplianceFrequency", function( data ){
            $.each( data, function( index, complianceFrequency){
               $("#compliance_frequency").append($("<option />",{text:complianceFrequency.name, value:complianceFrequency.id }))
            });
        },"json");
	} ,
	getComplianceTo		: function(){
		$.post("newcontroller.php?action=getComplianceTo", function( data ){
            $.each( data, function(index, compliance) {
               $("#compliance_to").append($("<option />",{text:compliance.name, value:compliance.id, selected:(compliance.id == 1 ? "selected" : "")}))
            });
        },"json");
	} ,	
	
	getSections				: function(){
		$.post("newcontroller.php?action=getSections", function( data ){
            $.each( data, function( index, section){
               $("#legislation_section").append($("<option />",{text:section.name+" - "+section.section_number, value:section.id }))
            });
        },"json");
	} ,
	
	getResponsibleOwner				: function(){
		$.post("newcontroller.php?action=getResponsibleOwner", function( data ){
            $.each( data, function( index, section){
               $("#responsibility_owner").append($("<option />",{text:section.name, value:section.id }))
            });
        },"json");
	} ,
	getAccountablePerson				: function(){
		$.post("newcontroller.php?action=getAccountablePerson", function( data ){
            $.each( data, function( index, accountable){
               $("#accountable_person").append($("<option />",{text:accountable.name, value:accountable.id, selected:(accountable.id == 1 ? "selected" : "")}))
            });
        },"json");
	} ,	
	getUsers 	: function()
	{
		$.post("newcontroller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
				$("#user").append($("<option />",{text:user.tkname, value:user.tkid}))		
				
				//$("#accountable_person").append($("<option />",{text:user.tkname, value:user.tkid}))
				
				//$("#responsibility_owner").append($("<option />",{text:user.tkname, value:user.tkid}))

				//$("#compliance_to").append($("<option />",{text:user.tkname, value:user.tkid}))
			});
		}, "json");	
	} , 	

    getDepartments:function()
    {
        $.get("newcontroller.php?action=getDirectorate", function( direData ){
        $.each( direData ,function( index, directorate ) {        		        	
                $("#responsible").append($("<option />",{text:directorate.name, value:directorate.id}));
            })
         $("#responsible option[value=1]").attr("selected", "selected");   
         
            
        }, "json");

    },

	getOrganisationSizes	: function()
	{
		$.post("newcontroller.php?action=getOrganisationSizes", function( org_sizes ){
			$.each( org_sizes, function( index, org_size){
				$("#organisation_size").append($("<option />",{text:org_size.name, value:org_size.id}))					
			});
		}, "json");	
	}, 
	
	getOrganisationTypes	: function()
	{
		$.post("newcontroller.php?action=getOrganisationTypes", function( org_types ){
			$.each( org_types, function( index, org_type ){
				$("#applicable_org_type").append($("<option />",{text:org_type.name, value:org_type.id, selected:(org_type.id == 1 ? "selected" : "" )}))
			});
		}, "json");	
	} ,
	
	getFunctionalService	: function()
	{
		$.post("newcontroller.php?action=getFunctionalService", function( functional_services ){
			$.each( functional_services, function( index, functional_service ){
				$("#functional_service").append($("<option />",{text:functional_service.name, value:functional_service.id}))
			});
		}, "json");	
	} ,

	getMainEvents	: function()
	{
		$.post("newcontroller.php?action=getMainEvents", function( main_events ){
			$.each( main_events, function( index, main_event ){
				$("#main_event").append($("<option />",{text:main_event.name, value:main_event.id}))
				$("select#main_event option[value=1]").attr("selected", "selected");
			});
		}, "json");
	} ,

	getSubEvents	: function( id )
	{
        $("#sub_event").empty();
       //$("#sub_event").append($("<option />",{html:"--sub event--", value:""}));
		$.post("newcontroller.php?action=getSubEvents",{ id : id}, function( sub_events ){
			$.each( sub_events, function( index, sub_event ){
				$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
			});
			if( id == 1){
				$("select#sub_event option[value=1]").attr("selected", "selected");
			}
		}, "json");
	} ,
	
	getSubEvent	: function( id )
	{
        $("#sub_event").empty();
        //$("#sub_event").append($("<option />",{html:"--sub event--", value:""}));
		$.post("managecontroller.php?action=getSubEvents",{ id : id}, function( sub_events ){
			$.each( sub_events, function( index, sub_event ){
				$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
			});
		}, "json");
	} ,	
		
	save					: function()
	{
		var data 				   = {};
		data.legislation           = $("#legislationid").val();
		data.description 		   = $("#description").val();
		data.short_description	   = $("#short_description").val();
		data.legislation_section   = $("#legislation_section").val();
		data.compliance_frequency  = $("#compliance_frequency  :selected").val();
		data.applicable_org_type   = $("#applicable_org_type :selected").val();
		data.compliance_date  	   = $("#compliance_date").val();
		data.functional_service    = $("#functional_service :selected").val();
		data.accountable_person    = $("#accountable_person").val();
		data.responsibility_owner  = $("#responsibility_owner :selected").val();
		data.responsible           = $("#responsible :selected").val();
		data.legislation_deadline  = $("#legislation_deadline").val();
		data.reminder 	           = $("#reminder").val();
		data.sanction              = $("#sanction").val();
		data.org_kpi_ref           = $("#org_kpi_ref").val();
		data.individual_kpi_ref    = $("#individual_kpi_ref").val();
		data.reference_link        = $("#reference_link").val();
		data.assurance             = $("#assurance").val();
		data.compliance_to         = $("#compliance_to").val();
		data.main_event            = $("#main_event :selected").val();
		data.sub_event             = $("#sub_event").val();
		data.guidance              = $("#guidance").val();
		data.recurring			     = $("#recurring :selected").val();
		data.hasActions 		   = $("#has_actions :selected").val();
		data.actiondeadlinedate    = $("#actiondeadlinedate").val();
		data.actionowner    	   = $("#actionowner").val();
		data.recurring_type		   = $("#recurring_type :selected").val(); 
       
        /*if( $("#fixed").is(":checked")){
        	data.recurring_type		   = "fixed";
        } else if( $("#start").is(":checked")){
        	data.recurring_type		   = "start";
        } else if($("#end").is(":checked")){
        	data.recurring_type		   = "end";
        }
        */
        data.recurring_period	   = $("#recurring_period").val() +" "+$("#period :selected").val()

		var valid = Deliverable.isValid( data );
		if( valid == true){
			jsDisplayResult("info", "info", "Saving   . . . <img src='../images/loaderA32.gif' />");
            $("#deliverable_message").show().html( "Saving . . . .   <img src='../images/loaderA32.gif' />" );
			$.post("../class/request.php?action=NewDeliverable.saveDeliverable", { data: data}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
				$("#display_deliverable").html("");	
				$("#display_deliverable").deliverable({editDeleverable:true, id:$("#legislationid").val()});
                $.each( data, function( index, value){
                    $("#"+index).val("");
                });
			} ,"json");		

		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	edit 				: function()
	{
        var data 				= {};
        data.legislation           = $("#legislationid").val();
        data.description 		= $("#description").val();
        data.short_description	= $("#short_description").val();
        data.legislation_section   = $("#legislation_section").val();
        data.compliance_frequency  = $("#compliance_frequency  :selected").val();
        data.applicable_org_type   = $("#applicable_org_type :selected").val();
        data.compliance_date  	= $("#compliance_date").val();
        data.functional_service    = $("#functional_service :selected").val();
        data.accountable_person    = $("#accountable_person").val();
        data.responsibility_owner  = $("#responsibility_owner :selected").val();
        data.responsible           = $("#responsible :selected").val();
        data.legislation_deadline  = $("#legislation_deadline").val();
        data.reminder 	          = $("#reminder").val();
        data.sanction              = $("#sanction").val();
        data.org_kpi_ref           = $("#org_kpi_ref").val();
        data.individual_kpi_ref    = $("#individual_kpi_ref").val();
        data.reference_link        = $("#reference_link").val();
        data.assurance             = $("#assurance").val();
        data.compliance_to         = $("#compliance_to").val();
        data.main_event            = $("#main_event_ :selected").val();
        data.sub_event             = $("#sub_event").val();
        data.guidance              = $("#guidance").val();
		  data.recurring			     = $("#recurring :selected").val();
		  data.recurring_type		  = $("#recurring_type :selected").val(); 
		  data.recurring_period	     = $("#recurring_period").val()
		  data.days_options	        = $("#recurring_period").val()
		  
        var valid = Deliverable.isValid( data );
        if( valid == true){
        	jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
            $.post("../class/request.php?action=ManageDeliverable.editDeliverable", { id:$("#deliverableid").val(),  data: data}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
            } ,"json");

        } else {
        	jsDisplayResult("error", "error", valid);	
        	//$("#deliverable_message").show().html( valid );
        }

	} , 

    viewEditLog     : function()
    {
        $.post("managecontroller.php?action=getDeliverableEdits", {id : $("#deliverableid").val()}, function( response ){
            $("#editLog").html("");
            if( $.isEmptyObject(response)){
                $("#editLog").html("There are no edit logs yet")																																																								} else {
			$("#editLog").append($("<table />",{id:"table_editlog"})
				.append($("<tr />")
				  .append($("<th />",{html:"Date"}))
				  .append($("<th />",{html:"Audit Log"}))
				  .append($("<th />",{html:"Status"}))
				)
			)
                $.each( response, function( index, editLog ){
				$("#table_editlog")
				.append($("<tr />")
				  .append($("<td />",{html:editLog.date}))
				  .append($("<td />",{html:editLog.changes}))
				  .append($("<td />",{html:editLog.status}))
				)
                })																																																						}

        }, "json");
    } ,

	update 			: function()
	{} ,
	
	deleteDeliverable			: function()
	{
		if( confirm("Are you sure you want to delete this deliverable"))
		{
			jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' />");
            $("#deliverable_message").show().html( "deleting deliverable . . . . <img src='../images/loaderA32.gif' />" );
              $.post("managecontroller.php?action=deleteDeliverable", { id:$("#deliverableid").val()}, function( response ){
  				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
            } ,"json");	
		}
	} , 
	
	isValid			: function( data )
	{

		if( data.short_description == ""){
			return "Please enter the short description ";
		} else if( data.legislation_section == "") {
			return "Please select the legislation section";
		} else if( data.compliance_frequency == "") {
			return "Please enter the legislative compliance frequency";
		} else if( $("#recurring :selected").val() == "") {
			return "Please enter the legislative compliance date";
		} else if( data.accountable_person == null ){
			return "Please select the accountable person";
		} else if( data.compliance_to == null){
			return "Please select compliance given to";
		} else if( data.main_event == null ){
			return "Please select main event";
		} else if( data.sub_event == null){
			return "Please select sub event";
		}  else {
			return true;	
		}  
		
       /*else if( data.description == ""){
			return "Please enter the description ";
		}  else if( data.applicable_org_type == ""){
			return "Please select the applicable organisation type";
		} else if( data.functional_service == ""){
			return "Please select the functional service";
		} else if( data.responsibility_owner == ""){
			return "Please select the responsibility owner";
		} else if( data.sanction == ""){
			return "Please enter the sanction";
		} else if( data.assurance == ""){
			return "Please enter assurance"
		} else if( data.compliance_to == ""){
			return "Please select compliance given to";
		} else if( data.main_event == "" ){
			return "Please select main event";
		} else if( data.sub_event == ""){
			return "Please select sub event";
		} else if( data.guidance == ""){
			return "Please enter the guidance";
		}*/ 

	}

}
