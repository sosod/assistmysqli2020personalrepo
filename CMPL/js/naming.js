// JavaScript Document
$(function(){
	Naming.get();	   
		   
});

var Naming 	= {
	
	get 	: function()
	{
		$.post("setupcontroller.php?action=getNaming", function( response ){
			$.each( response , function( index, namings ){
				Naming.display( namings );						
			});												
		},"json");		
	}	, 
	
	display	 	: function( naming )
	{
		$("#naming_table")
		  .append($("<tr />")
			.append($("<td />",{html:naming.id}))
			.append($("<td />",{html:naming.ignite_terminology}))
			.append($("<td />")
			  .append($("<input />",{type:"text", name:"name_"+naming.id, id:"name_"+naming.id, value:naming.client_terminology, size:"50"}))		  			
			)
			.append($("<td />")
			  .append($("<span />",{id:"loader_"+naming.id}))	  
			  .append($("<input />",{type:"submit", name:"update_"+naming.id, id:"update_"+naming.id, value:"Update"}))		  
			)			
		  )
		
		$("#update_"+naming.id).live("click", function(){
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateNaming", {
				  		 id 				  : naming.id , 
				   		 client_terminology   : $("#name_"+naming.id).val()
				   }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							jsDisplayResult("ok", "ok", response.text);
						}
				   }, "json");
			return false;										 
		});
	}
	
}