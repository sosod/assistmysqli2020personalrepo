$.widget("ui.deliverableactions", {
	
	options		: {
		url			: "../class/request.php?action=ManageDeliverable.getDeliverables",
		start			: 0,
		limit			: 10,
		current 		: 1,
		total			: 0,	
		tableId		: "deliactions_"+(Math.floor(Math.random(67*5))),
		id				: 0,
		section		: "manage",
		details		: false, 
		viewOnly		: false
	} , 
	
	_init		: function()
	{
		this._getDeliverables();		
	} , 
	
	_create		: function()
	{
				
	} , 
	
	_getDeliverables	: function()
	{
		var self = this;
		$(self.element).html("");
		$(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
			.css({
					position	: "absolute",
					"z-index"	: "9999",
					top			: "250px",
					left		: "350px",
					border		: "1px solid #FFFFF",
					padding		: "5px"
			})
		)
		$.post(self.options.url, {
			start			: self.options.start,
			limit			: self.options.limit,
			options 		: { id:self.options.id,section:self.options.section }
		}, function( deliverableResponse ) {
			$(self.element).html("");
			$("#loadingDiv").html("");
			$(self.element).html($("<table />",{id:self.options.tableId}))						
			if( $.isEmptyObject(deliverableResponse.deliverables)){
				$("#confirm_"+self.options.id).attr("disabled", "disabled").attr("id", "confirm_"+self.options.id+"_");

				$(self.element).append($("<p />")
					.append($("<span />",{html:"There are no deliverables for this legislation"}).addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px"}))
				)
			} else {
				self._displayPaging( self.options.tableId, deliverableResponse.columns ,deliverableResponse.total)
				self.options.total = deliverableResponse.total;
				self._headers( deliverableResponse.headers );	
				self._display( deliverableResponse.deliverables );				
			}	

		},"json")
		
	} ,
	
	_display		: function( deliverables )
	{
		var self = this;
		$.each( deliverables, function( index, deliverable){
			var tr 		= $("<tr />",{id:"tr_"+index});
			var childtr = $("<tr />",{id:"child_"+index}).css({display:(self.options.details ? "table-row" : "none")});
			tr.append($("<td />")
			    .append($("<a />",{href:"#", id:"less_"+index, title:"View Actions"})
			      .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus"))		
			    ).bind("click", function(){
					if( $("#child_"+index).is(":hidden")) {
						$("#"+index).addClass("tdhover");
						$("#child_"+index).show();
						$("#less_"+index+" span").removeClass("ui-icon-plus").addClass("ui-icon-minus")
					} else {
						$("#child_"+index).hide()
						$("#"+index).removeClass("tdhover");
						$("#less_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus")
					}
					//$("#child_"+index).toggle()
					return false;
				})		
			)
			$.each( deliverable, function( key , val){
				tr.append($("<td />",{html:val}))				
			});
			
			if(!self.options.viewOnly){
	            tr.append($("<td />")
		          .append($("<input />",{type:"button", name:"edit_deliverable_"+index, id:"edit_deliverable_"+index, value:"Edit Deliverable"}))
		          .append($("<input />",{type:"button", name:"del_deliverable_"+index, id:"del_deliverable_"+index, value:"Del"}))
	            )
			}
			
			$.getScript("../js/jquery.ui.action.js", function(){
				childtr.append($("<td />",{html:"&nbsp;"})
				    .append($("<span />").addClass("ui-icon").addClass("ui-icon-carat-1-sw"))		
				)
				.append($("<td />",{id:"td_"+index,colspan:"10"})
				   .append($("<div />",{id:"div_"+index}))		
				)
				$("#div_"+index).action({id:index, editAction:true, controller : "../new/newcontroller", thclass : "th2"});
			});
			
			
			$("#edit_deliverable_"+index).live("click", function(){
				document.location.href = "edit_deliverable.php?id="+index+"&legislationid="+self.options.id;
				return false;
			});
			
			$("#del_deliverable_"+index).live("click", function(){
				if(confirm("Are you sure you want to delete this deliverable"))
				{
					jsDisplayResult("info", "info", "Deleting deliverable  . . . <img src='../images/loaderA32.gif' />");
					$.post("../class/request.php?action=ManageDeliverable.deleteDeliverable",{
						id : index							
					}, function( response ) {
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text);	
						} else {
							jsDisplayResult("ok", "ok", response.text);	
							$("#tr_"+index).fadeOut();							
						}
					},"json");
				}
				return false;
			});
			
	    	$("#"+index).hover(function(){
				$(this).addClass("tdhover")		
			}, function(){
				$(this).removeClass("tdhover")
			});	
			
			$("#"+self.options.tableId).append( tr )
			$("#"+self.options.tableId).append( childtr )
		});
		
	} , 
	
	_headers		: function( headers )
	{
		var self = this;
		var tr   = $("<tr />");
		tr.append($("<th />",{html:"&nbsp;"}))	
		$.each( headers, function( index , head){
			tr.append($("<th />",{html:head}))			
		});
		tr.append($("<th />",{html:"&nbsp;"}).css({display:(self.options.viewOnly ? "none" : "table-cell")}))	
		$("#"+self.options.tableId).append( tr );
	} ,
    
    _displayPaging			: function( table, cols, total) {
		self = this;
		var pages;
		if( total%self.options.limit > 0) {
			pages   = Math.ceil(total/self.options.limit); 
		} else {
			pages   = Math.floor(total/self.options.limit); 
		}
		
		$("#"+table)
			.append($("<tr />")
			  .append($("<td />",{colspan:cols + 2})
				 .append($("<input />",{type:"button", name:"first", id:"first", value:"|<", disabled:(self.options.current < 2 ? 'disabled' : '' )}))		
				 .append($("<input />",{type:"button", name:"previous", id:"previous", value:"<", disabled:(self.options.current < 2 ? 'disabled' : '' )}))				
				 .append("&nbsp;&nbsp;&nbsp;")
			     .append($("<span />",{html:"Page "+self.options.current +" / "+pages, align:"center"}).css({"text-align":"center"}))
			     .append("&nbsp;&nbsp;&nbsp;")
				 .append($("<input />",{type:"button", name:"next", id:"next", value:">", disabled:((self.options.current ==pages || pages == 0) ? 'disabled' : '' )}))						
				 .append($("<input />",{type:"button", name:"last", id:"last", value:">|", disabled:((self.options.current ==pages || pages == 0) ? 'disabled' : '' )}))				 
			   )
			  .append($("<td />", {colspan:1}).css({"display":(self.options.editDeliverable ? "table-cell" : "none")}).css({display:(self.options.viewOnly ? "none" : "table-cell")}))
			  .append($("<td />", {colspan:1}).css({"display":(self.options.addAction? "table-cell" : "none")}))
			)
			$("#next").bind("click", function( e ){
				e.preventDefault();				
				self._getNext();
			});
			$("#last").bind("click",  function( e ){
				e.preventDefault();				
				self._getLast();
			});
			
			$("#previous").bind("click", function( e ){
				e.preventDefault();
				self._getPrevious( self );
			});
			
			$("#first").bind("click",  function( e ){
				e.preventDefault();
				self._getFirst();
			});	
	} , 
	_getNext  			: function() {
		var self = this;
		self.options.current = parseFloat( parseFloat(self.options.current) + 1 );	
		self.options.start   = parseFloat( self.options.current - 1) * parseFloat( self.options.limit );
		self._getDeliverables()
	},	
	_getLast  			: function() {
		var self = this;
		self.options.current = Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );			
		self._getDeliverables()
	},	
	_getPrevious    	: function() {
		var self = this;
		self.options.current  = parseFloat( self.options.current ) - 1;	
		self.options.start    = (self.options.current-1)*self.options.limit;
		self._getDeliverables();
	},	
	_getFirst  			: function() {
		var self = this;
		self.options.current  = 1;
		self.options.start    = 0;
		self._getDeliverables()
	}
});
