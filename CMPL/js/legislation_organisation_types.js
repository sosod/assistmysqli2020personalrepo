// JavaScript Document
$(function(){
	   
	 OrganisationType.get()  
	   
	$("#save_orgtype").click(function(){
		OrganisationType.save();
		return false;
	});
	
	$("#edit_orgtype").click(function(){
		OrganisationType.edit();	
		return false;
	});
	
	$("#update_orgtype").click(function(){
		OrganisationType.update();	
		return false;
	});	
});

var OrganisationType 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getLegOrganisationTypes", function( response ){
			$.each( response, function( index, orgtype){
				OrganisationType.display( orgtype )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#organisation_type_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the act name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveLegOrganisationType", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				OrganisationType.display( data )																   
				OrganisationType.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#organisation_type_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the act name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegOrganisationType", { id : $("#orgtypeid").val(), data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#organisation_type_message").show(); 
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegOrganisationType", {  id : $("#orgtypeid").val(), status : status }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {		
					jsDisplayResult("ok", "ok", response.text);
				}
		}, "json");
	} , 
	
	display		: function( orgtype )
	{
		$("#organisation_type_table")
		 .append($("<tr />",{id:"tr_"+orgtype.id})
		   .append($("<td />",{html:orgtype.id}))
		   .append($("<td />",{html:orgtype.name}))
		   .append($("<td />",{html:orgtype.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "edit_"+orgtype.id,
				  					id   : ((orgtype.status & 4) == 4 ? "edit" : "edit_"+orgtype.id),	
				  					value:"Edit"}))
			  .append($("<input />",{
				  					 type : "button",
				  					 name : "del_"+orgtype.id,
				  					 id   : ((orgtype.status & 4) == 4 ? "del" : "del_"+orgtype.id),
				  					 value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((orgtype.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "change_"+orgtype.id,
				  					id   : ((orgtype.status & 4) == 4 ? "change" : "change_"+orgtype.id),
				  					value: "Change Status"}))			  					
		    )		   
		 )
		 		 
		 if((orgtype.status & 4) === 4)
		 {
		    $("input[name='edit_"+orgtype.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+orgtype.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+orgtype.id+"']").attr('disabled', 'disabled');
		 } 		 		 
		 		 
		 $("#edit_"+orgtype.id).live("click", function(){
			document.location.href = "edit_leg_organisation_types.php?id="+orgtype.id;								   
			return false;								   
		  });
		 
		 $("#del_"+orgtype.id).live("click", function(){
			var message 	 = $("#organisation_type_message").show(); 

			if( confirm("Are you sure you want to delete this act")){
				jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateLegOrganisationType", { id : orgtype.id , status : 2 }, function( response ){														  																																													 					
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {	
						$("#tr_"+orgtype.id).fadeOut();	
						jsDisplayResult("ok", "ok", response.text);
					}
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+orgtype.id).live("click", function(){
			document.location.href = "change_leg_organisation_types.php?id="+orgtype.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
