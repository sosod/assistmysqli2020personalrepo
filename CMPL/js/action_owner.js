/**
 * 
 */
$(function(){
	ActionOwner.get();	
	
	$("#save").click(function(){
		ActionOwner.save();
		return false;						  
	});
	
	$("#edit").click(function(){
		ActionOwner.edit();
		return false;
	});
	
	$("#update").click(function(){
		ActionOwner.update();
		return false;
	});
});

ActionOwner 		= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getActionOwners", function( response ){
			$.each( response, function( index, actionowner){
				ActionOwner.display( actionowner )
			})												  											  
		},"json");
	} , 
	
	display		: function( actionowner )
	{

		$("#action_owner_table")
		 .append($("<tr />",{id:"tr_"+actionowner.id})
		   .append($("<td />",{html:actionowner.id}))
		   .append($("<td />",{html:actionowner.name}))
		   .append($("<td />",{html:actionowner.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "edit_"+actionowner.id,
				  					id    : ((actionowner.status & 4) == 4 ? "edit" : "edit_"+actionowner.id),
				  					value : "Edit"}))
			  .append($("<input />",{
				  					type   : "button",
				  					name   : "del_"+actionowner.id,
				  					id     : ((actionowner.status & 4) == 4 ? "del" : "del_"+actionowner.id),
				  					value  :"Del"}))
			)
		   .append($("<td />")
			.append($("<span />",{html:((actionowner.status & 1) == 1 ? "Active" : "Inactive")}))
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "change_"+actionowner.id,
				  					id    : ((actionowner.status & 4) == 4 ? "change" : "change_"+actionowner.id),
				  					value : "Change Status"}))
		    )		   
		 )
		 if((actionowner.status & 4) === 4)
		 {
		    $("input[name='edit_"+actionowner.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+actionowner.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+actionowner.id+"']").attr('disabled', 'disabled');
		 } 			 		 
		 $("#edit_"+actionowner.id).live("click", function(){
			document.location.href = "edit_action_owner.php?id="+actionowner.id;
			return false;								   
		  });
		 
		 $("#del_"+actionowner.id).live("click", function(){
			if( confirm("Are you sure you want to delete this action owner")){
				jsDisplayResult("info", "info", "Deleting action owner . . . <img src='../images/loaderA32.gif' />");				
					$.post("setupcontroller.php?action=updateActionOwner", { id : actionowner.id , status : 2 }, function( response ){
    					if( response.error )
    					{
    						jsDisplayResult("error", "error", response.text);
    					} else{
    						 $("#tr_"+actionowner.id).fadeOut();		
    						jsDisplayResult("ok", "ok", "Action owner deleted . . .");
    					}    
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+actionowner.id).live("click", function(){
			document.location.href = "change_actionowner_status.php?id="+actionowner.id;
		 });
		
	} , 
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#actionowner_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveActionOwner", { data : data }, function( response ){
				data.id  	= response.id;
                data.status = 1;
				ActionOwner.display( data )																   
				ActionOwner.empty( data );
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{		
					jsDisplayResult("ok", "ok", response.text);
				} 																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#action_owner_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateActionOwner", { id : $("#actid").val(), data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{		
					jsDisplayResult("ok", "ok", response.text);
				} 																   
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var message 	 = $("#action_owner_message").show();
		var status		= $("#status :selected").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateActionOwner", {  id : $("#actid").val(), status : status }, function( response ){																																																									 					
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{		
					jsDisplayResult("ok", "ok", response.text);
				} 
		}, "json");
	}
	
	
}
