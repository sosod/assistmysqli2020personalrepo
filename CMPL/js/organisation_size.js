// JavaScript Document
$(function(){

	OrganisationSize.get();
	
	$("#save_orgsize").click(function(){
		OrganisationSize.save();							  
		return false;							  
	});	   
		   
	$("#edit_orgsize").click(function(){
		OrganisationSize.edit();							  
		return false;							  
	});	   
	
	$("#update_orgsize").click(function(){
		OrganisationSize.update();							  
		return false;							  
	});	   
	
});

var OrganisationSize  	= {
	
	get			: function()
	{
		$.post("setupcontroller.php?action=getOrganisationSizes", function( response ){
			$.each( response, function( index, orgsize){
				OrganisationSize.display( orgsize )					   
			})												  											  
		},"json");
	} , 
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#organisation_size_message").show(); 
		data.name 		 = $("#name").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the organisation size name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveOrganisationSize", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				OrganisationSize.display( data )																   
				OrganisationSize.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {	
					jsDisplayResult("ok", "ok", response.text);
				}															   																   
			},"json");	
		}
	} , 
	
	update		: function()
	{
		var message = $("#organisation_size_message").show(); 
		var status	= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateOrganisationSize", {  id : $("#orgsizeid").val(), status : status }, function( response ){			
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {	
					jsDisplayResult("ok", "ok", response.text);
				}	
		}, "json");
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#organisation_size_message").show(); 
		data.name 		 = $("#name").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the organisation size name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateOrganisationSize", { id : $("#orgsizeid").val(),   data : data },function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {	
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
	} , 
	
	display		: function( orgsize )
	{
		$("#organisation_size_table")
		 .append($("<tr />",{id:"tr_"+orgsize.id})
		   .append($("<td />",{html:orgsize.id}))		   
		   .append($("<td />",{html:orgsize.name}))
		   .append($("<td />",{html:orgsize.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "edit_"+orgsize.id,
				  					id    : ((orgsize.status & 4) == 4 ? "edit" : "edit_"+orgsize.id),
				  					value : "Edit"}))
			  .append($("<input />",{
				  					type : "button",
				  					name : "del_"+orgsize.id,
				  					id   : ((orgsize.status & 4) == 4 ? "del" : "del_"+orgsize.id),
				  					value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((orgsize.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "change_"+orgsize.id,
				  					id	 : ((orgsize.status & 4) == 4 ? "change" : "change_"+orgsize.id),
				  					value:"Change Status"}))			  					
		    )		   
		 )
		 
		 if((orgsize.status & 4) === 4)
		 {
		    $("input[name='edit_"+orgsize.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+orgsize.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+orgsize.id+"']").attr('disabled', 'disabled');
		 } 		 		 
		 				 
		 		 
		 $("#edit_"+orgsize.id).live("click", function(){
			document.location.href = "edit_orgsize.php?id="+orgsize.id;								   
			return false;								   
		  });
		 
		 $("#del_"+orgsize.id).live("click", function(){
			var message 	 = $("#organisation_size_message").show(); 
			if( confirm("Are you sure you want to delete this organisation size")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=updateOrganisationSize", { id : orgsize.id , status : 2 }, function( response ){														  																																													 							   
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {	
						   $("#tr_"+orgsize.id).fadeOut();	
						jsDisplayResult("ok", "ok", response.text);
					}
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+orgsize.id).live("click", function(){
			document.location.href = "changeorgsize_status.php?id="+orgsize.id;										 
		 });	
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
