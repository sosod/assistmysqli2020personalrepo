$(function(){
	
	DeliverableColumns.get();


});

DeliverableColumns		= {
		
		get				: function()
		{
			$.getJSON("setupcontroller.php?action=getDeliverableColumns", function( responseData ){
				$("#columns").append($("<form />",{id:"deliverable_columns"}).append($("<table />",{id:"table_columns"})
				   .append($("<tr />")
					  .append($("<th/>",{html:"Column Names"}))
					  .append($("<th />",{html:"Show On Manage Pages"}))
					  .append($("<th />",{html:"Show On New Pages"}))
				   )		
				  )
				)
				$.each( responseData, function( index, deliverable){
					$("#table_columns").append($("<tr />")
					  .append($("<td />",{html:deliverable.client_terminology}))	
					  .append($("<td />")
						 .append($("<input />",{type:"checkbox", name:"manage", id:"manage_"+deliverable.id, value:deliverable.id }))	  
					  )	
					  .append($("<td />")
						 .append($("<input />",{type:"checkbox", name:"new", id:"new_"+deliverable.id, value:deliverable.id }))	  
					  )
					  .append($("<input />",{type:"hidden", id:"position", value:deliverable.id, name:"position_"+deliverable.id}))
					)	
					$("#manage_"+deliverable.id).attr("checked",((deliverable.active & 4) == 4 ? "checked" : "") )
					$("#new_"+deliverable.id).attr("checked",((deliverable.active & 8) == 8 ? "checked" : ""))
				});
				
				$("#table_columns tbody").sortable().disableSelection();
				
				$("#table_columns").append($("<tr />")
				   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
					  .append($("<input />",{type:"button", name:"save_changes", id:"save_changes", value:"Save Changes"}))	   
				   )
				)
				
				$("#save_changes").live("click", function(){
					jsDisplayResult("info", "info", "Updating deliverable columns  . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateColumns", {
						data	: $("#deliverable_columns").serializeArray()						
					}, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							jsDisplayResult("ok", "ok", response.text);
						}					
					},"json");
					return false;
				});
			});	
		} , 
		
		update			: function()
		{
			
			
			
		}
		
		
};