$(function(){
	
	ActionColumns.get();	
	$("#actioncolumns tbody").sortable({
		items	: ".disabled"		
	}).disableSelection(); 
});

ActionColumns		= {
		
	get			: function()
	{
		$.getJSON("setupcontroller.php?action=getActionColumns", function( responseData ){
			$.each( responseData, function( index, action){
				$("#actioncolumns").append($("<tr />").addClass("disabled")
				    .append($("<td />",{html:action.client_terminology}))
				    .append($("<td />")
				      .append($("<input />",{type:"checkbox", name:"manage", id:"manage_"+action.id, value:action.id}))
				    )
				    .append($("<td />")
		    		  .append($("<input />",{type:"checkbox", name:"new", id:"new_"+action.id, value:action.id}))
				    )
				    .append($("<input />",{type:"hidden", name:"position_"+action.id, id:"position", value:action.id}))
				)
				$("#new_"+action.id).attr("checked",((action.active & 8) == 8 ? "checked" : "" ))
				$("#manage_"+action.id).attr("checked",((action.active & 4) == 4 ? "checked" : "" ))
			});
			
			$("#actioncolumns").append($("<tr />")
			   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
				  .append($("<input />",{type:"button", name:"save_changes", id:"save_changes", value:"Save Changes"}))	   
			   )		
			)
			
			$("#save_changes").live("click", function(){
				jsDisplayResult("info", "info", "Updating deliverable columns  . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=updateColumns",{
					data		: $("#actioncolumns_form").serializeArray()
				},function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
					}	
				},"json");				
				return false;
			});
			
		});	
	} 	
		
		
};