/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/17/11
 * Time: 4:49 AM
 * To change this template use File | Settings | File Templates.
 */
$.widget("ui.deliverable", {
    options:    {
       addAction        : false,
       editDeliverable  : false,
       editActions      : false,
       start            : 0,
       limit            : 10,
       total            : 0,
       current		: 1,
       id               : "",
       controller	: "newcontroller",
       headers		: [],
       section		: "", 
       url 		: ""
    } ,

    _init:      function()
    {
        $this = this;
        $this._getDeliverable();
    } ,

    _getDeliverable:    function()
    {
        var self  = this;
	   $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		.css({
			position	: "absolute",
			"z-index"	: "9999",
			top		: "250px",
			left		: "350px",
			border	: "1px solid #FFFFF",
			padding	: "5px"
		})
	)	        
        $.post(self.options.url,
        {
            start	: self.options.start,
            limit	: self.options.limit,
            options 	: {section:self.options.section, id:self.options.id}
        }, function( data ){    
				$(self.element).html("");
				$("#display_deliverable").html("");           
				self.options.total =  data.total;    
		
				if($.isEmptyObject(data.deliverables)){
					$(self.element).css({"border-collapse":"collapse"})
						  .append($("<tr />")
								.append($("<td />",{colspan:data.columns+1})
									  .append($("<p />",{html:"There are no deliverables"}).addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px"}))
									)
						);	
				} else {
				$("#display_deliverable").append($("<table />",{id:"deliverable_table"}))      		
				self._displayPaging( "deliverable_table", data.columns ,data.total)	
				  self._displayHeaders( data.headers )			
				 self._displayDeliverable( data.deliverables );
				}
       },"json");
    } ,

    _displayHeaders:    function( headers )
    {
        var tr = $("<tr />",{id:"tr_headers"});
        $.each( headers,function( index, header){
			tr.append($("<th />",{html:header}))
        });
        //tr.append($("<th />",{html:"&nbsp;"}).css({"display" : $this.options.editActions ? "table-cell" : "none" }))
        tr.append($("<th />",{html:"&nbsp;"}).css({"display" : $this.options.addAction ? "table-cell" : "none" }))
        tr.append($("<th />",{html:"&nbsp;"}).css({"display" : $this.options.editDeliverable ? "table-cell" : "none" }))
        $("#deliverable_table").append( tr );
    } ,

    _displayDeliverable:function( deliverables )
    {
	
        $.each( deliverables, function( index, deliverable){
            tr = $("<tr />",{id:"tr_"+index});
            $.each( deliverable, function(i, val){
            	//if( $.inArray( i, $this.options.headers ) >= 0 ){
            	//} else {
            		tr.append($("<td />",{html:val}))
            	//}                            
            })

            tr.append($("<td />").css({"display" : $this.options.editDeliverable ? "table-cell" : "none" })
                .append($("<input />",{type:"button", name:"edit_deliverable_"+index, id:"edit_deliverable_"+index, value:"Edit Deliverable"}))
                .append($("<input />",{type:"button", name:"del_"+index, id:"del_"+index, value:"Del"}))
                .append($("<input />",{type:"button", name:"edit_actions_"+index, id:"edit_actions_"+index, value:"Edit Actions"})
                  .css({"display" : $this.options.editActions ? "table-cell" : "none" })		
                )
             )
            
            tr.append($("<td />").css({"display" : $this.options.addAction ? "table-cell" : "none" })
                .append($("<input />",{type:"button", name:"add_action_"+index, id:"add_action_"+index, value:"Add Action"}))
            )


            $("#add_action_"+index).live("click",function(){
                document.location.href = "add_action.php?id="+index;
                return false;
            });

            $("#edit_deliverable_"+index).live("click",function(){
                document.location.href = "edit_deliverable.php?id="+index;
                return false;
            });

            $("#edit_actions_"+index).live("click",function(){
                document.location.href = "edit_actions.php?id="+index;
                return false;
            });
            
            $("#del_"+index).live("click", function(){
            	if( confirm("Are you sure you want to delete this deliverable"))
            	{
                    jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
            		$.post("newcontroller.php?action=deleteDeliverable",{
            			deliverableId	: index             			
            		}, function( response ) { 
                        if( response.error )
                        {
                            jsDisplayResult("error", "error", response.text);
                        } else {
                            $("#tr_"+index).fadeOut();
                            jsDisplayResult("ok", "ok", response.text);
                        }             			
            		}, "json");
            	} 
            	return false;
            });
            
	    	$("#tr_"+index).hover(function(){
				$(this).addClass("tdhover")		
			}, function(){
				$(this).removeClass("tdhover")
			});	
            
            $("#deliverable_table").append( tr );
        });
    	$("#deliverable_table tr").hover(function(){
			$(this).addClass("tdhover")		
		}, function(){
			$(this).removeClass("tdhover")
		});        
    },
    
    _displayPaging			: function( table, cols, total ) {
		$this = this;
		var pages;
		if( $this.options.total%$this.options.limit > 0) {
			pages   = Math.ceil($this.options.total/$this.options.limit); 
		} else {
			pages   = Math.floor($this.options.total/$this.options.limit); 
		}
	
		$("#"+table)
			.append($("<tr />")
			  .append($("<td />",{colspan:cols})
				 .append($("<input />",{type:"button", name:"first", id:"first", value:"|<", disabled:($this.options.current < 2 ? 'disabled' : '' )}))		
				 .append($("<input />",{type:"button", name:"previous", id:"previous", value:"<", disabled:($this.options.current < 2 ? 'disabled' : '' )}))				
				 .append("&nbsp;&nbsp;&nbsp;")
			     .append($("<span />",{html:"Page "+$this.options.current +" / "+pages, align:"center"}).css({"text-align":"center"}))
			     .append("&nbsp;&nbsp;&nbsp;")
				 .append($("<input />",{type:"button", name:"next", id:"next", value:">", disabled:(($this.options.current ==pages || pages == 0) ? 'disabled' : '' )}))						
				 .append($("<input />",{type:"button", name:"last", id:"last", value:">|", disabled:(($this.options.current ==pages || pages == 0) ? 'disabled' : '' )}))				 
			   )
			  .append($("<td />", {colspan:1}).css({"display":($this.options.editDeliverable ? "table-cell" : "none")}))
			  .append($("<td />", {colspan:1}).css({"display":($this.options.addAction? "table-cell" : "none")}))
			)
			$("#next").bind("click", function( e ){
				e.preventDefault();				
				$this._getNext();
			});
			$("#last").bind("click",  function( e ){
				e.preventDefault();				
				$this._getLast();
			});
			
			$("#previous").bind("click", function( e ){
				e.preventDefault();
				$this._getPrevious( $this );
			});
			
			$("#first").bind("click",  function( e ){
				e.preventDefault();
				$this._getFirst();
			});	
	} , 
	_getNext  			: function() {
		var $this = this;
		$this.options.current = parseFloat( parseFloat($this.options.current) + 1 );	
		$this.options.start   = parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
		$this._getDeliverable()
	},	
	_getLast  			: function() {
		var $this = this;
		$this.options.current = Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
		$this.options.start   = parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
		$this._getDeliverable()
	},	
	_getPrevious    	: function() {
		var $this = this;
		$this.options.current  = parseFloat( $this.options.current ) - 1;	
		$this.options.start    = ($this.options.current-1)*$this.options.limit;
		$this._getDeliverable();
	},	
	_getFirst  			: function() {
		var $this = this;
		$this.options.current  = 1;
		$this.options.start    = 0;
		$this._getDeliverable()
	}
})
