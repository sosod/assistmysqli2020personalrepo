// JavaScript Document
$(function(){
	Menu.get();	   
		   
});

var Menu = {
	
	get			: function()
	{
		$.post("setupcontroller.php?action=getMenu", function( response ){
			$.each( response, function( index, menu ){
				Menu.display( menu );					   
			}); 						   
		}, "json");	
	}, 
	
	display	    : function( menu )
	{
		$("#menu_table")
		  .append($("<tr />")
			.append($("<td />",{html:menu.id}))
			.append($("<td />",{html:menu.ignite_terminology}))
			.append($("<td />")
			  .append($("<input />",{type:"text", name:"name_"+menu.id, id:"name_"+menu.id, value:menu.client_terminology}))		  			
			)
			.append($("<td />")
			  .append($("<span />",{id:"loader_"+menu.id}))	  
			  .append($("<input />",{type:"submit", name:"update_"+menu.id, id:"update_"+menu.id, value:"Update"}))		  
			)			
		  )
		
		$("#update_"+menu.id).live("click", function(){
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateMenu", {
				  		 id 				  : menu.id , 
				   		 client_terminology   : $("#name_"+menu.id).val()
				   }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							jsDisplayResult("ok", "ok", response.text);
						}
				   }, "json");
			return false;										 
		});
		
	}
	
}