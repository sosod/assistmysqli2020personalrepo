$.widget("ui.legislation", {
	
	options		: {
		addDeliverable		: false	,
		editDeliverable 	: false,
		addAction		: false,
		viewDetails		: false,
		start			: 0,
		limit 			: 10,
		total 			: 0,
		current			: 1,
		controller		: "newcontroller",
		searchText		: "",
		url 				: ".php?action=getLegislations",
		searchArr		: [],
		confirmLegislation	: false,
		activateLegislation : false,
		page			: "",
		section			: "",
		tableId			: "legislation_"+(Math.floor(Math.random(56*56) + 4)),
		user			: ""
	} , 
	
	_init		: function()
	{
		this._getLegislation();		
	} , 
	
	_create 	: function()
	{
	} , 
	
	
	_getLegislation	: function()
	{
		var self = this;
		$(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
				.css({
						position	: "absolute",
						"z-index"	: "9999",
						top			: "250px",
						left		: "350px",
						border		: "1px solid #FFFFF",
						padding		: "5px"
				})
			)		
		$.post(self.options.url, {
			start		: self.options.start,
			limit		: self.options.limit,
			options  : {
			            section    : self.options.section,
			            page       : self.options.page,
			            searchText : self.options.searchText,
			            searchArr  : self.options.searchArr
			           }
		}, function( legislationResponse ) {			
			$(self.element).html("")
			$(self.element).append($("<table />",{id:self.options.tableId}))
			$("#loadingDiv").html("");
			self.options.total = legislationResponse.total
			self._displayPaging( legislationResponse.total, legislationResponse.columns )
			self._displayHeaders( legislationResponse.headers )		
			if( $.isEmptyObject(legislationResponse.legislations) ){
				$("#"+self.options.tableId).append($("<tr />")
				  .append($("<td />",{colspan:legislationResponse.columns})
				    .append($("<p />",{html:"There are no legislations"}).addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px"}))
				  )
				).css({"border-collapse":"collapse"})
			} else {
				self._display( legislationResponse.legislations, legislationResponse.owners )				
			}
		},"json");
	}, 
	
	
	_display		: function( legislations, legOwners )
	{
		var self = this;
		$.each( legislations, function( index, legislation){
			var tr = $("<tr />")
			//loop through each legislation
			$.each( legislation, function( i, val){
				if( self.options.searchText != ""){
					if( val.indexOf(self.options.searchText) >= 0){
						tr.append($("<td />")
						    .append($("<span />",{html:val}).css({"background-color":"yellow"}))		
						)
					} else {
						tr.append($("<td />",{html:val}))
					}					
				} else {
					tr.append($("<td />",{html:val}))
				}
			})
			
			tr.append($("<td />").css({"display":(self.options.editLegislation ? "table-cell" : "none")})
			  	.append($("<input />",{
										type	: "button",
										id		:(legOwners[index] == self.options.user ?  "edit_legislation_"+index : "edit_legislation"),
										name	: "edit_legislation_"+index,
										value	: "Edit Legislation"}))
			  	.append($("<input />",{
										type	: "button",
										id		: (legOwners[index] == self.options.user ?  "edit_deliverable_"+index : "edit_deliverable"),
										name	: "edit_deliverable_"+index,
										value	: "Edit Deliverable"})
		  		   .css({"display":(self.options.editDeliverable ? "block" : "none")})
			  	)
			)
			$("#edit_deliverable_"+index).attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"))
			$("#edit_legislation_"+index).attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"))
			tr.append($("<td />").css({"display":(self.options.addDeliverable ? "table-cell" : "none")})
			  	.append($("<input />",{
										type	: "button",
										id		: (legOwners[index] == self.options.user ?  "add_deliverable_"+index : "add_deliverable"),
										name	: "add_deliverable_"+index,
										value	: "Add Deliverable"}))
			  	.append($("<br />"))
			  	.append($("<input />",{
										type	: "button", 
										id		: (legOwners[index] == self.options.user ?  "add_action_"+index : "add_action"),
										name	: "add_action_"+index,
										value   : "Add Action"})
			  	  .css({"display":(self.options.addAction ? "table-cell" : "none")})		
			  	)
			)	
			$("#add_deliverable_"+index).attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"))
			$("#add_action_"+index).attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"))
			if(self.options.section == "new")
			{
				tr.append($("<td />").css({"display":(self.options.addAction  ? "table-cell" : "none")})
				  	.append($("<input />",{
											type	: "button",
											id		: (legOwners[index] == self.options.user ?  "add_action_"+index : "add_action"),
											name	: "add_action_"+index,
											value	: "Add Action" }))				
				)			
			}
               $("#add_action_"+index).attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"))
			tr.append($("<td />").css({"display":(self.options.viewDetails ? "table-cell" : "none")})
			  	.append($("<input />",{type:"button", id:"view_details_"+index, name:"view_details_"+index, value:"View Details"}))				
			)			
				
			if( self.options.confirmLegislation )
			{
				$("#confrimlegislation").append($("<option />",{text: legislation.legislation_name, value:index}))
				$("select#confrimlegislation option[value="+index+"]").attr("disabled", (legOwners[index] == self.options.user ?  "" : "disabled"));
				$("#confrimlegislation").live("change", function(){
					
					var id = $(this).val();
					if( id == "")
					{
						$("#display_legislation").html("");
						$("#display_deliverable").html("");
						$("#displaydeliverable").html("")
						$("#display_deliverable").append($("<p />").addClass("ui-state").addClass("ui-state-highlight").css({"padding":"7px", "margin-left":"3px"})
						  .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
						  .append($("<span />",{html:"You have select an invalid, please try again"}))
						)	
					} else 
					{
						$.getScript("../js/jquery.ui.deliverableactions.js", function(){
							
							$("#display_legislation").html("");
							$("#display_deliverable").html("");
							$("#displaydeliverable").html("")
							$("#display_deliverable").deliverableactions({id:id,
							 url : "../class/request.php?action=NewDeliverable.getDeliverables"});
							$("#displaydeliverable").append($("<table />").addClass("noborder")
									   .append($("<tr />")
										 .append($("<td />").addClass("noborder")
										    .append($("<input />",{ type	 : "button",
										    						name	 : "confirm_"+id,
										    						id		 : "confirm_"+id,
										    						value	 : "Confirm and Request Activation"
										    						}))		 
										 ).bind("click",function(){
												$("<div />",{id:"dialogconfirmation_"+id, html:"You are confirming that the details of the legislation are correct"})
												 .append($("<br />"))
												 .append($("<div />",{id:"dialogmessage_"+id}))
												 .dialog({
													autoOpen	: true,
													title		: "Confirmation and Request Activation",
													modal		: true,
													buttons		: {
									"Confirm"	: function()
									{					
						 				jsDisplayResult("info", "info", "Confirming  . . . <img src='../images/loaderA32.gif' />");
										$.post("newcontroller.php?action=confirm", { id : id}, function( response ){
											if( response.error )
											{
												jsDisplayResult("error", "error", response.text);																			
											} else {
												jsDisplayResult("ok", "ok", response.text);
											}														
											$("#dialogconfirmation_"+id).dialog("destroy")
											$("#dialogconfirmation_"+id).remove();
										}, "json");
		
									} , 
									"Cancel"		: function()
									{															
										$("#dialogconfirmation_"+id).dialog("destroy")
										$("#dialogconfirmation_"+id).remove()
									}
	
										  }								
										});							
									return false;
								})	   
									   )		
									)
									/*$("#confirm_"+id).live("click",function(){
										$("<div />",{id:"dialogconfirmation_"+id, html:"You are confirming that the details of the legislation are correct"})
										 .append($("<br />"))
										 .append($("<div />",{id:"dialogmessage_"+id}))
										 .dialog({
											autoOpen	: true,
											title		: "Confirmation and Request Activation",
											modal		: true,
											buttons		: {
															"Confirm"	: function()
															{					
											    				jsDisplayResult("info", "info", "Confirming  . . . <img src='../images/loaderA32.gif' />");
																$.post("newcontroller.php?action=confirm", { id : id}, function( response ){
																	if( response.error )
																	{
																		jsDisplayResult("error", "error", response.text);																			
																	} else {
																		jsDisplayResult("ok", "ok", response.text);
																	}														
																	$("#dialogconfirmation_"+id).dialog("destroy")
																	$("#dialogconfirmation_"+id).remove();
																}, "json");
										
															} , 
															"Cancel"		: function()
															{															
																$("#dialogconfirmation_"+id).dialog("destroy")
																$("#dialogconfirmation_"+id).remove()
															}
	
								  }								
								});							
								return false;
							});*/
						});
					} // if the id is not empty
					return false;
				});
				
			}
			
		    $("#view_details_"+index).live("click", function(){
	    		document.location.href = "view_details.php?id="+index;
	    		return false;
			  });			
		
			   $("#edit_legislation_"+index).live("click", function(){
					document.location.href = "edit_legislation.php?id="+index;
					return false;
				});
		
			$("#edit_deliverable_"+index).live("click", function(){
				document.location.href = "edit_deliverables.php?id="+index;
				return false;
			});
		
			$("#add_deliverable_"+index).live("click", function(){
				document.location.href = "add_deliverable.php?id="+index;												
				return false;												
			});
			
			$("#add_action_"+index).live("click", function(){
				document.location.href = "add_actions.php?id="+index;
				return false;												
			});	
			
	    	$("#"+self.options.tableId+" tr").hover(function(){
				$(this).addClass("tdhover")		
			}, function(){
				$(this).removeClass("tdhover")
			});				
			$("#"+self.options.tableId).append( tr )
		})
		
		
	} , 
	
	_displayHeaders		: function( headers )
	{
		var self = this;
		var tr = $("<tr />")
		$.each( headers, function( index, head){
			tr.append($("<th />",{html:head}))			
		});
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.editLegislation ? "table-cell" : "none")}))
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.addDeliverable ? "table-cell" : "none")}))
		if( self.options.section != "manage") {
			tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.addAction ? "table-cell" : "none")}))
		}
		//tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.editDeliverable ? "table-cell" : "none")}))
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.viewDetails ? "table-cell" : "none")}))				
		$("#"+self.options.tableId).append( tr )
	} , 
	
	_displayPaging		: function( total, columns )
	{
		var self = this;
		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 
		} else {
			pages   = Math.floor(total/self.options.limit); 
		}
		$("#"+self.options.tableId)
			.append($("<tr />")
			  .append($("<td />",{colspan:columns})
				 .append($("<input />",{type:"button", name:"first", id:"first", value:"|<"}).css({"font-weight":"bold"}))		
				 .append($("<input />",{type:"button", name:"previous", id:"previous", value:"<"}).css({"font-weight":"bold"}))					 
				 .append("&nbsp;&nbsp;&nbsp;")
				 .append($("<span />",{html:"Page "+self.options.current +" / "+pages, align:"center"}).css({"text-align":"center"}))
				 .append("&nbsp;&nbsp;&nbsp;")
				 .append($("<input />",{type:"button", name:"next", id:"next", value:">"}).css({"font-weight":"bold"}))						
				 .append($("<input />",{type:"button", name:"last", id:"last", value:">|"}).css({"font-weight":"bold"}))				 
			   )			   
			  .append($("<td />", {colspan:1}).css({"display":(self.options.editDeliverable ? "table-cell" : "none")}))
			  .append($("<td />", {colspan:2}).css({"display":(self.options.addDeliverable ? "table-cell" : "none")}))
			  .append($("<td />", {colspan:2}).css({"display":(self.options.addAction ? (self.options.section == "manage" ? "none" : "table-cell") : "none")}))
			  .append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.viewDetails ? "table-cell" : "none")}))
			  .append($("<td />", {colspan:1}).css({"display":(self.options.section == "admin" ? "table-cell" : "none")}))
			)
			
		  if(self.options.current < 2)
		  {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
		  }
		  if((self.options.current == pages || pages == 0))
		  {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
		  }				
		  $("#next").bind("click", function( e ){
				e.preventDefault();				
				self._getNext();
		   });
			$("#last").bind("click",  function( e ){
				e.preventDefault();				
				self._getLast();
			});
			
			$("#previous").bind("click", function( e ){
				e.preventDefault();
				self._getPrevious();
			});
			
			$("#first").bind("click",  function( e ){
				e.preventDefault();
				self._getFirst();
			});	
	} ,
	_getNext  			: function() {
		var self = this;
		self.options.current = parseFloat( parseFloat(self.options.current) + 1 );	
		self.options.start   = parseFloat( self.options.current - 1) * parseFloat( self.options.limit );
		self._getLegislation()
	},	
	_getLast  			: function() {
		var self = this;
		self.options.current = Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );			
		self._getLegislation()
	},	
	_getPrevious    	: function() {
		var self = this;
		self.options.current  = parseFloat( self.options.current ) - 1;	
		self.options.start    = (self.options.current-1)*self.options.limit;
		self._getLegislation();
	},	
	_getFirst  			: function() {
		var self = this;
		self.options.current  = 1;
		self.options.start    = 0;
		self._getLegislation()
	}
	
	
});
