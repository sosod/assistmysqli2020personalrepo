// JavaScript Document
$(function(){
	   
	 SubEvents.get()  
	   
	$("#save_event").click(function(){
		SubEvents.save();
		return false;
	});
	
	$("#edit_event").click(function(){
		SubEvents.edit();	
		return false;
	});
	
	$("#update_subevent").click(function(){
		SubEvents.update();	
		return false;
	});	
});

var SubEvents 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getSubEvents", function( response ){
			$.each( response, function( index, subevent ){
				SubEvents.display( subevent )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.main_event	 = $("#main_event :selected").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the sub event name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else if( data.main_event == ""){
			jsDisplayResult("error", "error", "Please enter the main event");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveSubEvent", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				data.main_event = $("#main_event :selected").text();; 	
				SubEvents.display( data )																   
				SubEvents.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.main_event	 = $("#main_event :selected").val();		
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the sub event name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else if( data.main_event == ""){
			jsDisplayResult("error", "error", "Please enter the main event");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateSubEvent", { id : $("#eventid").val(), data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#event_message").show(); 
		var status		= $("#status :selected").val()
					jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateSubEvent", {  id : $("#eventid").val(), status : status }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}	
		}, "json");
	} , 
	
	display		: function( subevent )
	{
		$("#event_table")
		 .append($("<tr />",{id:"tr_"+subevent.id})
		   .append($("<td />",{html:subevent.id}))
		   .append($("<td />",{html:subevent.name}))
		   .append($("<td />",{html:subevent.description}))
		   .append($("<td />",{html:subevent.main_event}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "edit_"+subevent.id,
				  					id   : ((subevent.status & 4) == 4 ? "edit" : "edit_"+subevent.id),
				  					value:"Edit"}))
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "del_"+subevent.id,
				  					id    : ((subevent.status & 4) == 4 ? "del" : "del_"+subevent.id),
				  					value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((subevent.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "change_"+subevent.id,
				  					id 	  : ((subevent.status & 4) == 4 ? "change" : "change_"+subevent.id),
				  					value : "Change Status"}))			  					
		    )		   
		 )
		 if((subevent.status & 4) === 4)
		 {
		    $("input[name='edit_"+subevent.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+subevent.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+subevent.id+"']").attr('disabled', 'disabled');
		 } 	 
		 $("#edit_"+subevent.id).live("click", function(){
			document.location.href = "edit_subevent.php?id="+subevent.id;								   
			return false;								   
		  });
		 
		 $("#del_"+subevent.id).live("click", function(){
			var message 	 = $("#event_message").show(); 
			if( confirm("Are you sure you want to delete this event")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateSubEvent",{ id : subevent.id , status : 2 }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							 $("#tr_"+subevent.id).fadeOut();	
							jsDisplayResult("ok", "ok", response.text);
						}
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+subevent.id).live("click", function(){
			document.location.href = "change_subevent.php?id="+subevent.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
