// JavaScript Document
$(function(){

    var sectionid = $("#sectionid").val();
    if( sectionid == undefined){
     $("<div />",{id:"load_dialog", html:"Do you want to add a main section or sub section"})
            .dialog({
                autoOpen   : true,
                modal      : true,
                width      : "400px",
                height     : "auto",
                buttons    : {
                             "Main Section" : function()
                             {
                                $(".section_type").hide();
                                 $('body').data("type", 1)
                                $("#load_dialog").dialog("destroy")
                             } ,
                            "Sub Section"  : function()
                            {
                                
                                $('body').data("type", 0)
                                Sections.getMainSections();

                            } ,

                            "Nothing"  : function()
                            {
                                $("#load_dialog").dialog("destroy")
                            }

                         }
            })

	    Sections.get();
    }
	$("#add").click(function(){
		Sections.save();
		return false;
	});

	$("#edit").click(function(){
		Sections.edit();
		return false;
	});
		   
});

Sections	= {

    getMainSections : function()
    {
        $.post("setupcontroller.php?action=getMainSections", function( data ){
           if( $.isEmptyObject(data)){
               $("#load_dialog").html("There are no main sections available");
           } else {
               $.each( data, function( index, section){
                  $("#sections").append($("<option />",{value:section.section_number, text:section.name}))
               });
              $("#load_dialog").dialog("destroy");
           }
        },"json");
    } ,

	get				: function(){
		$.post("setupcontroller.php?action=getSections", function( data ){
            if( $.isEmptyObject( data)){
                $("#section_message").show().html("No section setup yet")
            } else {
                $.each( data, function( index, section){
                    Sections.display( section );
                });
            }
        },"json");
	} , 
	
	save			: function(){
		var data 		 = {};
		data.name        = $("#section_name").val();
		data.number		 = $("#section_number").val();		
		data.type		 = $('body').data("type");
		data.main	 	 = $("#sections :selected").val();
		data.description = $("#section_description").val();
        var valid        = Sections.isValid( data );
        if( valid == true){
		    $.post("setupcontroller.php?action=saveLegislationSection", { data : data }, function( response ){
                $("#section_message").show().html( response.text )
                    Sections.empty( data );
                    $(".tr_data").empty();
                    Sections.get();
		    }, "json");
        } else {
            $("#section_message").show().html( valid );
        }
	} ,

    display         :   function( section )
    {
        $("#section_table")
          .append($("<tr />",{id:"tr_"+section.id}).addClass("tr_data")
             .append($("<td />",{html:section.id}))
             .append($("<td />",{html:section.name}))
             .append($("<td />",{html:section.section_number}))
             .append($("<td />",{html:section.description}))
             .append($("<td />")
                .append($("<input />",{type:"button", name:"edit_"+section.id, id:"edit_"+section.id, value:"Edit"}))
                .append($("<input />",{type:"button", name:"del_"+section.id, id:"del_"+section.id, value:"Del"}))
             )
             .append($("<td />"))
             .append($("<td />")
               .append($("<input />",{type:"button", name:"change_"+section.id, id:"change_"+section.id, value:"Change Status"}))
             )
           )

        $("#edit_"+section.id).live("click", function(){
           document.location.href = "edit_section.php?id="+section.id;
        });

        $("#del_"+section.id).live("click", function(){
           if( confirm("Are you sure you want to delete this section"))
           {
                $.post("setupcontroller.php?action=updateSection", { status:2 , id : section.id }, function(){                  
                  $("#tr_"+section.id).fadeOut();
                },"json");
           }
            return false;
        });

        $("#change_"+section.id).live("click", function(){
           document.location.href = "change_section.php?id="+section.id;
        });


    } ,

	update			:	function(){
		
		
	} ,
	
	edit			: function(){
		var data 		 = {};
		data.name        = $("#section_name").val();
		data.number		 = $("#section_number").val();
		data.type		 =   $('body').data("type");
		data.main	 	 = $("#sections :selected").val();
		data.description = $("#section_description").val();
        var valid        = Sections.isValid( data );
        if( valid == true){
		    $.post("setupcontroller.php?action=updateSection", { id: $("#sectionid").val(), data : data }, function( response ){
                $("#section_message").show().html( response.text )
		    }, "json");
        } else {
            $("#section_message").show().html( valid );
        }

		
	} ,

    isValid         : function( data )
    {
        if( data.name == ""){
            return "Please enter the section name";
        } else if(data.number == ""){
            return "Please enter the section number";
        } else if( isNaN(data.number)){
            return "Please enter a valid section number";
        } else {
            return true;
        }
    } ,

    empty       : function( section)
    {
        $.each( section, function( index, field){
            $("#"+index).val("");
        });

    }
	
	
}