// JavaScript Document
$(function(){
  var actionid = $("#actionid").val();

  if( actionid == undefined){
	  Action.getOwner();
  }
  
 
  $("#authorize_action").click(function(){
	Action.authorize();					  
	return false;						  
 })
  
  $("#save").click(function(){
	Action.save();
	return false;						  
 })
  
$(".attach").live("change", function(){
	Action.uploadFile( this );		
	return false;
});
	
  $("#viewEditLog").click(function(){
	Action.viewEditLog();
	return false;
  });

  $("#viewUpdateLogs").click(function(){
	Action.viewUpdateLog() 							   
	return false;
  });
  
  $("#edit").click(function(){
	Action.edit() 
	return false;
  });
  
  $("#update").click(function(){
	Action.update();
	return false;
  });
 
  $("#delete").click(function(){
		Action.deleteAction();
		return false;
  });
  
  $("#auth_log").click(function(){
	Action.authorizationLog();
	return false;
  }); 
 
  
  $("#status").change(function(){
	if($(this).val() == "3"){
		$("#request_approval").attr("disabled", "") 
		$("#progress").val("100")
	} else{
		$("#request_approval").attr("disabled", "disabled") 		
	}
  });
  
});



var Action 	= {

	getOwner 		: function (){
		$.getJSON("newcontroller.php?action=getActionOwners", function( users ){
			$.each( users, function( index, user){
				$("#action_owner").append($("<option />",{value:user.id, text:user.name}))									
			})													 
		});
	} ,
	
	save 			: function()
	{
		var message              = $("#action_message").show();
		that 			        = this;
		that.name        = $("#action_name").val();
		that.owner              = $("#action_owner :selected").val();
		that.action_deliverable = $("#deliverable").val();
		that.deadline	        = $("#deadline").val();
		that.reminder	        = $("#reminder").val();
		if( $("#recurring").is(":checked")) {
			that.recurring	        = 1;
		}
		//valid 		            = Action.isValid( that );
		var data = { name : that.name, owner:that.owner, action_deliverable:that.action_deliverable, deadline:that.deadline, reminder:that.reminder, deliverable_id:$("#deliverableid").val(), recurring:that.recurring };
		if( $("#action_name").val() == ""){
			jsDisplayResult("error", "error", "Please enter the action name");
			return false;
		} else if ( $("#action_owner :selected").val() == ""){
			jsDisplayResult("error", "error", "Please select the action owner");
			return false;
		} else if( $("#deliverable").val() == ""){
			jsDisplayResult("error", "error", "Please enter the action deliverable");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving Action . . . .<img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=NewAction.saveAction", {data : data }, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					$("#display_action").html("");
					$("#display_action").action({editAction:true,deleteActions:true, id:$("#deliverableid").val(), controller : "newcontroller"});
					Action.emptyField( data )					
					jsDisplayResult("ok", "ok", response.text);
				}    
			},"json")
		}

		
	} , 
	
	edit		: function() {
		var message              = $("#action_message").show();
		that 			        = this;
		that.action_name        = $("#action_name").val();
		that.owner              = $("#action_owner :selected").val();
		that.action_deliverable = $("#deliverable").val();
		that.deadline	        = $("#deadline").val();
		that.reminder	        = $("#reminder").val();
		
		valid 		= Action.isValid( that );
		var data = {
					action_name	        : that.action_name,
					owner               : that.owner,
					deadline	        : that.deadline,
               action_deliverable  : that.action_deliverable,
					reminder	        : that.reminder,
					};
		if( valid == true ) {
			//editig dialog
			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' /> ");
			$.post("../class/request.php?action=ManageAction.editAction",{
				data 	: data, id: $("#actionid").val()
			}, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}    
			},"json")
		} else{
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	update		: function(){	
	
		that 			= this;
		that.response 	= $("#response").val();
		that.status		= $("#status :selected").val();
		that.progress	= $("#progress").val();
		that.remind_on  = $("#remind_on").val();
		
		valid 		= Action.isValid( that );
		var data = {
					response	  	 : that.response, 
					action_status 	 : that.status,
					progress	  	 : that.progress,
					remindon	  	 : that.remindon,
					request_approval : $("#request_approval").val(),
					action_id	  	 : $("#actionid").val(),
					contractid	  	 : $("#contractid").val()					
				};
		
		if( valid == true ) {
			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' /> ");
			$.post("../class/request.php?action=Manage.updateAction", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}  	
			},"json")
		} else{
			jsDisplayResult("error", "error",valid);
		}
	} ,
	
	deleteAction		: function()
	{
		if(confirm("Are you sure you want to delet this action"))
		{
			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' /> ");
			$.post("../class/request.php?action=ManageAction.updateAction", {id:$("#actionid").val(), status:2}, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}  		
			},"json")
		}
		
	} ,
	
	authorize			: function()
	{
		that 			= {};
		that.response 	= $("#response").val();
		that.approval	= $("#approval :selected").val();
		that.progress	= $("#progress").val();
		that.remind_on  = $("#remind_on").val();
		
		valid 		= Action.isValid( that );
		var data = {
					response	  	 : that.response, 
					approval	 	 : that.approval,
					request_approval : $("#request_approval").val(),
					action_id	  	 : $("#actionid").val()		
				};
		
		if( valid == true ) {

			jsDisplayResult("info", "info", "Updating action . . . .<img src='../images/loaderA32.gif' /> ");
			$.post("newcontroller.php?action=authorizeAction", data, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}  
			},"json")
		} else{	
			jsDisplayResult("error", "error", valid);
	
		}
	} ,
	
	authorizationLog	: function()
	{	
		
		$.post("newcontroller.php?action=getActionAuthorizations", {id : $("#actionid").val()}, function( response ){																			
			$("#display_authLogs").html("");																																					
			if( $.isEmptyObject(response)){
				$("#display_authLogs").html("There are no update logs yet")																																																										} else {
				$("#display_authLogs").append($("<table />",{id:"table_updateloga"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, updateLog ){
						
						$("#table_updateloga")
							.append($("<tr />")
								.append($("<td />",{html:updateLog.date}))
								.append($("<td />",{html:updateLog.changes}))
								.append($("<td />",{html:updateLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
	} ,
	
	
	viewUpdateLog	: function(){
		
		$.post("newcontroller.php?action=getActionUpdates", {id : $("#actionid").val()}, function( response ){																			   
			$("#actionUpdateLogs").html("");																																					
			if( $.isEmptyObject(response)){
				$("#actionUpdateLogs").html("There are no update logs yet")																																																								} else {
				$("#actionUpdateLogs").append($("<table />",{id:"table_updateloga"})
							.append($("<tr />")
								.append($("<th />",{html:"Date"}))
								.append($("<th />",{html:"Audit Log"}))
								.append($("<th />",{html:"Status"}))								
							)		
						  )
				$.each( response, function( index, updateLog ){
						
						$("#table_updateloga")
							.append($("<tr />")
								.append($("<td />",{html:updateLog.date}))
								.append($("<td />",{html:updateLog.changes}))
								.append($("<td />",{html:updateLog.status}))								
							)					   
				})																																																						}
																																																			
		}, "json");
		
	} ,
	
	viewEditLog	: function(){
	$.post("managecontroller.php?action=getActionEdits", {id : $("#actionid").val()}, function( response ){
				$("#editLog").html("");																																					
				if( $.isEmptyObject(response)){
					$("#editLog").html("There are no edit logs yet")																																																								} else {
					$("#editLog").append($("<table />",{id:"table_editlog"})
								.append($("<tr />")
									.append($("<th />",{html:"Date"}))
									.append($("<th />",{html:"Audit Log"}))
									.append($("<th />",{html:"Status"}))								
								)		
							  )
					$.each( response, function( index, editLog ){
							
							$("#table_editlog")
								.append($("<tr />")
									.append($("<td />",{html:editLog.date}))
									.append($("<td />",{html:editLog.changes}))
									.append($("<td />",{html:editLog.status}))								
								)					   
					})																																																						}
																																																				
			}, "json");
	} ,
	
	isValid 	: function( fields ){
		if( fields.action == ""){
			return "Action name is required";	
		} else if( fields.owner == ""){
			return "Please select the action owner";	
		} else if( fields.measurable == ""){
			return "Please enter the measurable";	
		} else if( fields.status == ""){
			return "Please select the status";	
		}
		return true;		
	} ,
	
	emptyField		: function(  fields )
	{
		$.each( fields, function( field, value){
		   $("#"+field).val("");		   
		});		
		$("#action_owner option[value='']").attr("selected", "selected");
		$("#deliverable").val("");
		$("#action_name").val("");
	} , 
	
	uploadFile		: function( $this )
	{
		//console.log( $this.id )
		$("#uploading")
		 .ajaxStart(function(){
			$(this).append($("<img />",{src:"../images/arrows16.gif"}))				 
		 })
		 .ajaxComplete(function(){
			//$(this).html("");				
		});
		
		$.ajaxFileUpload({
			url	 	      : "newcontroller.php?action=uploadAction",
			secureuri     : false,
			fileElementId : $this.id,
			dataType	  : "json",
			success		  : function( response , status){
				//console.log( response );
				if( typeof(response.error) != 'undefined'){
					
					if( response.error){
						$("#uploading").append( response.text+"<br />" )
					} else {
						//$("#uploading").append( data.text+"<br />" );
						$("#uploading").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									   .css({"color":"green"});
						if( $.isEmptyObject( response.filesuploaded ) ){
							$("#uploading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#uploading").append( file+"<br />" )											 
							});							
						}
													
					}
				}
				
			} , 
			error		: function(response, status, e){
				//console.log( "Ajax error "+e)
			}
		});
	}
 	
	
	
}
