// JavaScript Document
$(function(){
	var useraccessid = $("#useraccessid").val();
	if( useraccessid == undefined ){
		UserAccess.getUsers();
		UserAccess.get();
	}
	$("#add").click(function(){
		UserAccess.saveUserSettings();		   
		return false;
	});
	
	$("#edit").click(function(){
		UserAccess.editUserSettings();
		return false;					  
	});
	
});

var UserAccess = {
	
	getUsers 		: function()
	{
		$.getJSON("setupcontroller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
				$("#users").append($("<option />", {value:user.tkid, text:user.tkname+" "+user.tksurname}))						
			});												 
		});	
		
	} , 
	
	saveUserSettings	: function()
	{
		if( $("#users :selected").val() == "" ){
			$("#loading").html("Please select the user");
			return false;
		} else {
			
		  $.post("setupcontroller.php?action=saveUserAccess", {
				user			: $("#users :selected").val(),
				directorate 	: $("#directorate :selected").val(),
				sub_directorate : $("#subdirectorate :selected").val(),
				module_admin 	: $("#module_admin :selected").val(),
				contract_manager: $("#contract_manager :selected").val(),
				create_contract	: $("#create_contract :selected").val(),				
				create_deliverable: $("#create_deliverable :selected").val(),
				approval		: $("#approval :selected").val(),				
				viewall			: $("#viewall :selected").val(),				
				editall			: $("#editall :selected").val(),				
				reports			: $("#reports :selected").val(),
				assurance		: $("#assurance :selected").val(),
				setup			: $("#setup :selected").val()						
			}, function( response ){
				if( !response.error ){
					$("#loading").html( response.text )	
					$("#table_useraccess")
					 .append($("<tr />", {id:"tr_"+response.id})
					   .append($("<td />",{html:response.id}))
					   .append($("<td />",{html:$("#users :selected").text()}))
					   .append($("<td />",{html:$("#directorate :selected").text()}))
					   .append($("<td />",{html:$("#subdirectorate :selected").text()}))
					   .append($("<td />",{html:$("#module_admin :selected").text()}))					   
					   .append($("<td />",{html:$("#contract_manager :selected").text()}))
					   .append($("<td />",{html:$("#create_contract :selected").text()}))
					   .append($("<td />",{html:$("#create_deliverable :selected").text()}))
					   .append($("<td />",{html:$("#approval :selected").text()}))
					   .append($("<td />",{html:$("#viewall :selected").text()}))
					   .append($("<td />",{html:$("#editall :selected").text()}))
					   .append($("<td />",{html:$("#reports :selected").text()}))		
					   .append($("<td />",{html:$("#assurance :selected").text()}))		
					   .append($("<td />",{html:$("#setup :selected").text()}))		
					   .append($("<td />")
						 .append($("<input />",{value:"Edit", id:"edit_"+response.id, name:"edit_"+response.id, type:"submit"}))
						 .append($("<input />",{value:"Delete", id:"delete_"+response.id, name:"delete_"+response.id, type:"submit"}))						 
						)							   
					 )
				
					$("select#users option[value='']").attr("selected", "selected")
					$("select#directorate option[value='']").attr("selected", "selected")
					$("select#subdirectorate option[value='']").attr("selected", "selected")
					$("select#moduleadmin option[value='yes']").attr("selected", "selected")
					$("select#contract_manager option[value='yes']").attr("selected", "selected")
					$("select#create_contract option[value='yes']").attr("selected", "selected")
					$("select#create_deliverable option[value='yes']").attr("selected", "selected")
					$("select#approval option[value='yes']").attr("selected", "selected")
					$("select#viewall option[value='yes']").attr("selected", "selected")					
					$("select#editall option[value='yes']").attr("selected", "selected")
					$("select#reports option[value='yes']").attr("selected", "selected")
					$("select#assurance option[value='yes']").attr("selected", "selected")					
					$("select#setup option[value='yes']").attr("selected", "selected")		
					
					$("#edit_"+response.id).live("click", function(){
						document.location.href  = "edit_useraccess.php?id="+response.id;										   
						return false;										   
					});
					
					$("#delete_"+response.id).live("click", function(){
						if(confirm("Are you sure you want to delete this user settings")){
							$.post("setupcontroller.php?action=deleteUserAccess", {
								   id	  : 	response.id,
								   status : 	2048
								   }, function( response ){																						
								if( !response.error ){
									$("#loading").html( response.text )
									$("#tr_"+response.id).fadeOut();
								} else{
									$("#loading").html( response.text )	
								}				
							})
						}
						return false;										   
					});					
				} else{
					$("#loading").html( response.text )					
				}													  
		  }, "json")
			
		}	
	} , 
	
	editUserSettings	: function()
	{
		$.post("setupcontroller.php?action=editUserSettings",
			    {
					id				: $("#useraccessid").val(),
					directorate 	: $("#directorate :selected").val(),
					sub_directorate : $("#subdirectorate :selected").val(),
					module_admin 	: $("#module_admin :selected").val(),
					contract_manager: $("#contract_manager :selected").val(),
					create_contract	: $("#create_contract :selected").val(),				
					create_deliverable: $("#create_deliverable :selected").val(),
					approval		: $("#approval :selected").val(),				
					viewall			: $("#viewall :selected").val(),				
					editall			: $("#editall :selected").val(),				
					reports			: $("#reports :selected").val(),
					assurance		: $("#assurance :selected").val(),
					setup			: $("#setup :selected").val()					   
				}, function( response ){
				   if( !response.error ){
						$("#loading").html( response.text )   
				 	} else {
						$("#loading").html( response.text )						
					}
				},"json");
		
	} , 
	
	get				: function(){
		$.getJSON("setupcontroller.php?action=getUserAccess", function( response ){
			if( $.isEmptyObject(response)){
				
			} else{  
				$.each( response, function( index, useraccess ){
					UserAccess.display( useraccess );					   
				});
			}													  
		});
		
	} , 
	
	display 		: function( userAccess ){
		$("#table_useraccess")
		 .append($("<tr />")
			.append($("<td />",{html : userAccess.id }))
			.append($("<td />",{html : userAccess.user }))			
			.append($("<td />",{html :"" }))
			.append($("<td />",{html : "" }))
			.append($("<td />",{html : (userAccess.status & 1) == 1 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 2) == 2 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 4) == 4 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 8) == 8 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 16) == 16 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 32) == 32 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 64) == 64 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 128) == 128 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 256) == 256 ? "Yes" : "No" }))			
			.append($("<td />",{html : (userAccess.status & 512) == 512 ? "Yes" : "No" }))						
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"edit_"+userAccess.id, id:"edit_"+userAccess.id, value:"Edit"}))
				.append($("<input />",{type:"submit", name:"delete_"+userAccess.id, id:"delete_"+userAccess.id, value:"Delete"}))				
			)						
		  )

		 
			$("#edit_"+userAccess.id).live("click", function(){
				document.location.href  = "edit_useraccess.php?id="+userAccess.id;										   
				return false;										   
			});
			
			$("#delete_"+userAccess.id).live("click", function(){
				if(confirm("Are you sure you want to delete this user settings")){
					$.post("setupcontroller.php?action=deleteUserAccess", {
						   id     : userAccess.id,
						   status : (userAccess.status) + 2048
						   }, function( response ){																						
						if( !response.error ){
							$("#loading").html( response.text )
							$("#tr_"+response.id).fadeOut();
						} else{
							$("#loading").html( response.text )	
						}				
					})
				}
				return false;										   
			});				
	}



}