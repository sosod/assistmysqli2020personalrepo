// JavaScript Document
$(function(){
	AccountablePerson.get();	
	
	$("#save").click(function(){
		AccountablePerson.save();
		return false;						  
	});
	
	$("#edit").click(function(){
		AccountablePerson.edit();
		return false;
	});
	
	$("#update").click(function(){
		AccountablePerson.update();
		return false;
	});
});

AccountablePerson 		= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getAccountablePerson", function( response ){
			$.each( response, function( index, accountable){
				AccountablePerson.display( accountable )
			})												  											  
		},"json");
	} , 
	
	display		: function( accountable )
	{

		$("#accountable_table")
		 .append($("<tr />",{id:"tr_"+accountable.id})
		   .append($("<td />",{html:accountable.id}))
		   .append($("<td />",{html:accountable.name}))
		   .append($("<td />",{html:accountable.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					 type : "button", 
				  					 name : "edit_"+accountable.id,
				  					 id	  :((accountable.status & 4) == 4 ? "edit" : "edit_"+accountable.id),
				  					 value:"Edit"}))
			  .append($("<input />",{
				  					type :	"button",
				  					name :	"del_"+accountable.id,
				  					id	 :((accountable.status & 4) == 4 ? "del" : "del_"+accountable.id),
				  					value:  "Del"}))
			)
		   .append($("<td />")
			.append($("<span />",{html:((accountable.status & 1) == 1 ? "Active" : "Inactive")}))
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name :"change_"+accountable.id,
				  					id   :((accountable.status & 4) == 4 ? "change" : "change_"+accountable.id),
				  					value:"Change Status"}))
		    )		   
		 )
		 if((accountable.status & 4) === 4)
		 {
		    $("input[name='edit_"+accountable.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+accountable.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+accountable.id+"']").attr('disabled', 'disabled');
		 } 		
		 			 
		 $("#edit_"+accountable.id).live("click", function(){
			document.location.href = "edit_accountable.php?id="+accountable.id;
			return false;								   
		  });
		 
		 $("#del_"+accountable.id).live("click", function(){
			var message 	 = $("#accountable_message").show();

			if( confirm("Are you sure you want to delete this accountable person")){
				jsDisplayResult("info", "info", "Deleting accountable person . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=updateAccountable", { id : accountable.id , status : 2 }, function( response ){
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else{
						jsDisplayResult("ok", "ok", "Accontable person deleted . . .");
					}    
					$("#tr_"+accountable.id).fadeOut();																		  																																													 					message.html("");
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+accountable.id).live("click", function(){
			document.location.href = "change_accountable_status.php?id="+accountable.id;
		 });
		
	} , 
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#accountable_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the accountable person name . . . ");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveAccountablePerson", { data : data }, function( response ){
				data.id  	= response.id;
                data.status = 1;
				AccountablePerson.display( data )																   
				AccountablePerson.empty( data );
				message.html("");
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				} 															   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#accountable_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the accountable person name . . . ");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateAccountable", { id : $("#actid").val(), data : data }, function( response ){	
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}  
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var message 	 = $("#accountable_message").show();
		var status		= $("#status :selected").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateAccountable", {  id : $("#actid").val(), status : status }, function( response ){																																																									 					
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				} 
		}, "json");
	}
	
	
}
