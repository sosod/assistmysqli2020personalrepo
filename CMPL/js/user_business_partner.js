$(function(){
	if( $("#userbussid").val() == undefined){	
		UserBusinessPartner.getUsers();
		//UserBusinessPartner.getBusinessPartners();
		UserBusinessPartner.getUserBusinessPartners();
	}
	
	$("#assign").click(function(){
		UserBusinessPartner.save()
		return false;
	});
	
	$("#update").click(function(){
		UserBusinessPartner.update();
		return false;
	});
	
});

UserBusinessPartner		= {
	
		getUsers				: function()
		{
			$.getJSON("setupcontroller.php?action=getUnUsedUsers", function( responseData ){
				$.each( responseData, function( index, user){
					$("#users").append($("<option />",{value:user.tkid, text:user.tkname}));					
				});				
			});
	
		} , 
		
		getBusinessPartners		: function()
		{
			$.getJSON("setupcontroller.php?action=getResponsibleOrganisations", function( responseData ){
				$.each( responseData, function( index, businesspartner){
					$("#business_patners").append($("<option />",{text:businesspartner.name, value:businesspartner.id}))					
				});				
			});			
		} ,
		
		save 					: function()
		{
			var data 	   = {};
			data.user_id   = $("#users").val(); 
			data.id 	   = $("#business_patners :selected").val();
			if( data.id == ""){
				jsDisplayResult("error", "error", "Please select the business partner");
			} else if( data.user_id == ""){
				jsDisplayResult("error", "error", "Please select the users to be assigned");				
			} else {	
				jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=saveUserBusinessPartner",{data : data}, function( response ){
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else {
						jsDisplayResult("ok", "ok", response.text);
						var dis  = {};
						dis.id   =  response.id;
						$.each( data.user_id , function( i , val){
							$("select#users option[value="+val+"]").attr("text", "");
							$("select#users option[value="+val+"]").attr("value", "");						
						});
						/*
						dis.organisation = $("#business_patners :selected").text();
						dis.user		 = response.users;
						dis.status 		 = 1
						UserBusinessPartner.display( dis );*/
						UserBusinessPartner.getUserBusinessPartners();
					}    				
				},"json");
			}
		} , 
		
		update					: function()
		{
			var data 	   = {};
			data.user_id   = $("#users").val(); 
			data.id 	   = $("#userbussid").val();
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateUserBusinessPartner",{data : data}, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else{
					jsDisplayResult("ok", "ok", response.text);
				}    				
			},"json");
			
		} ,
		
		getUserBusinessPartners		: function()
		{
			$(".userbusy").remove();
			$.getJSON("setupcontroller.php?action=getUserBusinessPartner", function( responseData ){
				$.each( responseData, function( index, user_busy){
					UserBusinessPartner.display( user_busy );
				});				
			});			
		} , 
		
		display						: function(user_busy)
		{
			$("#table_business_partner").append($("<tr />",{id:"userbusy_"+user_busy.id}).addClass("userbusy")
					.append($("<td />",{html:user_busy.id}))
					.append($("<td />",{html:user_busy.organisation}))
					.append($("<td />",{html:user_busy.user}))
					.append($("<td />")
						.append($("<input />",{type:"button", name:"edit_"+user_busy.id, id:"edit_"+user_busy.id, value:"Edit"}))
						.append($("<input />",{type:"button", name:"del_"+user_busy.id, id:"del_"+user_busy.id, value:"Del"}))
					)
				)	
				
				$("#edit_"+user_busy.id).live("click", function(){
					document.location.href = "edit_user_business.php?id="+user_busy.id;
					return false;
				});
				
				$("#del_"+user_busy.id).live("click", function(){
					if(confirm("Are you sure you want to delete this user and business partnet link"))
					{
						var data 	= {};
						data.id     =  user_busy.id;
						data.status = 2;
						$.post("setupcontroller.php?action=deleteUserBusinessPartner", { data : data }, function( response ){
							if( response.error )
							{
								jsDisplayResult("error", "error", response.text);
							} else {
								jsDisplayResult("ok", "ok", response.text);
							}  
						},"json");
					}
					
					
					return false;
				});	
		}
		
};