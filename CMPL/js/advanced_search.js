$(function(){
	
	AdvancedSearch.getTypes();
	AdvancedSearch.getCategories();
	AdvancedSearch.getOrganisationSizes();
	AdvancedSearch.getOrganisationTypes();
	AdvancedSearch.getOrganisationCapacity();
	AdvancedSearch.getOrganisations();
	
	$("#go").click(function(){
		AdvancedSearch.search();
		return false;
	});
	
});

AdvancedSearch		= {
		
		
		getOrganisations 		: function()
		{
			$.post("searchcontroller.php?action=getResponsibleOrganisations", function( orgs ){
				$.each( orgs, function( index, org){
					$("#responsible_organisation").append($("<option />",{text:org.name, value:org.id}))
				})
			},"json");
		}  ,
		getTypes 	: function()
		{
			$.post("searchcontroller.php?action=getLegislativeTypes", function( types ){
				$.each( types, function( index, type){
					$("#type").append($("<option />",{text:type.name, value:type.id}))					
				});
			}, "json");	
		} , 	
		
		getCategories 	: function()
		{
			$.post("searchcontroller.php?action=getLegislativeCategories", function( categories ){
				$.each( categories, function( index, category){
					$("#category").append($("<option />",{text:category.name, value:category.id}))					
				});
			}, "json");	
		} , 
		
		getOrganisationSizes	: function()
		{
			$.post("searchcontroller.php?action=getOrganisationSizes", function( org_sizes ){
				$.each( org_sizes, function( index, org_size){
					$("#organisation_size").append($("<option />",{text:org_size.name, value:org_size.id}))					
				});
			}, "json");	
		}, 
		
		getOrganisationTypes	: function()
		{
			$.post("searchcontroller.php?action=getLegOrganisationTypes", function( org_types ){
				$.each( org_types, function( index, org_type ){
					$("#organisation_type").append($("<option />",{text:org_type.name, value:org_type.id}))
				});
			}, "json");	
		} , 
		
		getOrganisationCapacity	: function()
		{
			$.post("searchcontroller.php?action=getOrganisationCapacity", function( org_capacity ){
				$.each( org_capacity, function( index, org_capa ){
					$("#organisation_capacity").append($("<option />",{text:org_capa.name, value:org_capa.id}))					
				});
			}, "json");	
		} , 
		
		search					: function()
		{
			//jsDisplayResult("info", "info", "Searching legislation. . . <img src='../images/loaderA32.gif' />");
			$.getScript("../js/jquery.ui.legislation.js", function(){
				$("#display_legislation").legislation({url:".php?action=searchLegislations", controller:"searchcontroller", searchArr : $("#advanced_search_form").serializeArray() })		
			});			
		}
		
		
		
		
		
};