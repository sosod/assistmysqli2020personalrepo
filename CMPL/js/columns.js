// JavaScript Document
$(function(){

	Columns.get();
	
	$("#legislation_columns tbody").sortable().disableSelection();

});

Columns		= {		
	get		: function()
	{
		$.getJSON("setupcontroller.php?action=getLegislationColumns", function( responseData ){
			$.each( responseData, function( index, column){	
				$("#legislation_columns").append($("<tr />")
					.append($("<td />",{html:column.client_terminology}))
					.append($("<td />")
					  .append($("<input />",{type:"checkbox", id:"manage_"+column.id, name:"manage", value:column.id }))		
					)
					.append($("<td />")
					  .append($("<input />",{type:"checkbox", id:"new_"+column.id, name:"new", value:column.id }))		
					)
					.append($("<input />",{type:"hidden", name:"position_"+column.id, id:"position", value:column.id}))
				)
				$("#manage_"+column.id).attr("checked",((column.active & 4) == 4 ? "checked" : ""))
				$("#new_"+column.id).attr("checked",((column.active & 8) == 8 ? "checked" : ""))				
			});
			$("#legislation_columns").append($("<tr />")
			   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
				  .append($("<input />",{type:"button",name:"save_changes", id:"save_changes",value:"Save Changes" }))	   
			   )		
			)
			
			$("#save_changes").live("click", function(){
				jsDisplayResult("info", "info", "Updating columns . . . .<img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=updateColumns",{ 
						data : $("#legislation_columns_form").serializeArray()
				}, function( response ){
					if( response.error)
					{
						jsDisplayResult("error", "error", response.text);			
					} else {
						jsDisplayResult("ok", "ok", response.text);				
					}
				},"json");
				return false;
			});	
			
		});
	}  ,
	
	saveChanges				: function()
	{

		
	}
}