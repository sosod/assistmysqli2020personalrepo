// JavaScript Document
$(function(){
	   
	 LegislativeCategories.get()  
	   
	$("#save_legcat").click(function(){
		LegislativeCategories.save();
		return false;
	});
	
	$("#edit_legcat").click(function(){
		LegislativeCategories.edit();	
		return false;
	});
	
	$("#update_cat").click(function(){
		LegislativeCategories.update();	
		return false;
	});	
});

var LegislativeCategories 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getLegislativeCategories", function( response ){
			$.each( response, function( index, legcat){
				LegislativeCategories.display( legcat )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#legislative_category_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the legislative category name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveLegislativeCategory", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				LegislativeCategories.display( data )																   
				LegislativeCategories.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#legislative_category_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegislativeCategory", { id : $("#legcatid").val(), data : data }, function( response ){		
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {					
				jsDisplayResult("ok", "ok", response.text);
			}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#legislative_category_message").show(); 
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegislativeCategory", {  id : $("#legcatid").val(), status : status }, function( response ) {
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {					
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");
	} , 
	
	display		: function( legcat )
	{
		$("#legislative_category_table")
		 .append($("<tr />",{id:"tr_"+legcat.id})
		   .append($("<td />",{html:legcat.id}))
		   .append($("<td />",{html:legcat.name+" "+(legcat.status & 4) }))
		   .append($("<td />",{html:legcat.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "edit_"+legcat.id,
				  					id   : ((legcat.status & 4) == 4 ? "edit" : "edit_"+legcat.id),
				  					value:"Edit"}))
			  .append($("<input />",{
				  					type : "button",
				  					name : "del_"+legcat.id,
				  					id   : ((legcat.status & 4) == 4 ? "del" : "del_"+legcat.id),
				  					value: "Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((legcat.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "change_"+legcat.id,
				  					id    : ((legcat.status & 4) == 4 ? "change" : "change_"+legcat.id),
				  					value:"Change Status"}))			  					
		    )		   
		 )
		 if((legcat.status & 4) === 4)
		 {
		    $("input[name='edit_"+legcat.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+legcat.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+legcat.id+"']").attr('disabled', 'disabled');
		 } 
		 
		 $("#edit_"+legcat.id).live("click", function(){
			document.location.href = "edit_legislativecategories.php?id="+legcat.id;								   
			return false;								   
		  });
		 
		 $("#del_"+legcat.id).live("click", function(){
			var message 	 = $("#legislative_category_message").show(); 
			if( confirm("Are you sure you want to delete this act")){
			jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateLegislativeCategory",  { id : legcat.id , status : 2 }, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {	
						 $("#tr_"+legcat.id).fadeOut()
						jsDisplayResult("ok", "ok", response.text);
					}
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+legcat.id).live("click", function(){
			document.location.href = "changelegcategory_status.php?id="+legcat.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
