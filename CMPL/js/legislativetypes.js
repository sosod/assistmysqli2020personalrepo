// JavaScript Document
$(function(){
	   
	 LegislativeTypes.get()  
	   
	$("#save_leg").click(function(){
		LegislativeTypes.save();
		return false;
	});
	
	$("#edit_leg").click(function(){
		LegislativeTypes.edit();	
		return false;
	});
	
	$("#update_leg").click(function(){
		LegislativeTypes.update();	
		return false;
	});	
});

var LegislativeTypes 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getLegislativeTypes", function( response ){
			$.each( response, function( index, act){
				LegislativeTypes.display( act )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#acts_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the legislative type name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveLegislativeType", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				LegislativeTypes.display( data )																   
				LegislativeTypes.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}																	   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#acts_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the legislative type name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Update . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegislativeType", { id : $("#legid").val(), data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#acts_message").show(); 
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Update . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateLegislativeType", {  id : $("#legactid").val(), status : status }, function( response ){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);		
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");
	} , 
	
	display		: function( legtype )
	{
		$("#legislative_type_table")
		 .append($("<tr />",{id:"tr_"+legtype.id})
		   .append($("<td />",{html:legtype.id}))
		   .append($("<td />",{html:legtype.name}))
		   .append($("<td />",{html:legtype.description}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+legtype.id, id:"edit_"+legtype.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+legtype.id, id:"del_"+legtype.id, value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((legtype.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+legtype.id, id:"change_"+legtype.id, value:"Change Status"}))			  					
		    )		   
		 )
		 		 
		 $("#edit_"+legtype.id).live("click", function(){
			document.location.href = "edit_legislativetypes.php?id="+legtype.id;								   
			return false;								   
		  });
		 
		 $("#del_"+legtype.id).live("click", function(){
			var message 	 = $("#acts_message").show(); 

			if( confirm("Are you sure you want to delete this act")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");					$.post("setupcontroller.php?action=updateLegislativeType", { id : legtype.id , status : 2 }, function( response ){
																							  																																													 					
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {
					$("#tr_"+legtype.id).fadeOut();
					jsDisplayResult("ok", "ok", response.text);
				}
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+legtype.id).live("click", function(){
			document.location.href = "changelegtype_status.php?id="+legtype.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}