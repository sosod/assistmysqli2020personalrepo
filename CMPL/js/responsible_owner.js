// JavaScript Document
$(function(){
	ResponsibleOwner.get();	
	
	$("#save").click(function(){
		ResponsibleOwner.save();
		return false;						  
	});
	
	$("#edit").click(function(){
		ResponsibleOwner.edit();
		return false;
	});
	
	$("#update").click(function(){
		ResponsibleOwner.update();
		return false;
	});
});

ResponsibleOwner 		= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getResponsibleOwner", function( response ){
			$.each( response, function( index, act){
				ResponsibleOwner.display( act )					   
			})												  											  
		},"json");
	} , 
	
	display		: function( responsible_owner )
	{
		$("#responsible_owner_table")
		 .append($("<tr />",{id:"tr_"+responsible_owner.id})
		   .append($("<td />",{html:responsible_owner.id}))
		   .append($("<td />",{html:responsible_owner.name}))
		   .append($("<td />",{html:responsible_owner.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "edit_"+responsible_owner.id,
				  					id 	 : ((responsible_owner.status & 4) == 4 ? "edit" : "edit_"+responsible_owner.id),
				  					value: "Edit"}))
			  .append($("<input />",{
				  					 type  : "button",
				  					 name  : "del_"+responsible_owner.id,
				  					 id    : ((responsible_owner.status & 4) == 4 ? "del" : "del_"+responsible_owner.id),
				  					 value : "Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((responsible_owner.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					 type  : "button",
				  					 name  : "change_"+responsible_owner.id,
				  					 id    : ((responsible_owner.status & 4) == 4 ? "change" : "change_"+responsible_owner.id),
				  					 value : "Change Status"}))			  					
		    )		   
		 )
		 if((responsible_owner.status & 4) === 4)
		 {
		    $("input[name='edit_"+responsible_owner.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+responsible_owner.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+responsible_owner.id+"']").attr('disabled', 'disabled');
		 } 		
		 	 
		 $("#edit_"+responsible_owner.id).live("click", function(){
			document.location.href = "edit_responsible_owner.php?id="+responsible_owner.id;
			return false;								   
		  });
		 
		 $("#del_"+responsible_owner.id).live("click", function(){
			var message 	 = $("#responsible_owner_message").show();

			if( confirm("Are you sure you want to delete this responsible owner")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateResponsibleOwner", { id : responsible_owner.id , status : 2 }, function( response ){
      				if( response.error ){
    					jsDisplayResult("error", "error", response.text);		
    				} else {
    					 $("#tr_"+responsible_owner.id).fadeOut();	
    					jsDisplayResult("ok", "ok", response.text);
    				}
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+responsible_owner.id).live("click", function(){
			document.location.href = "change_responsible_owner_status.php?id="+responsible_owner.id;
		 });
		
	} , 
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#responsible_owner_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the responsible owner name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveResponsibleOwner", { data : data }, function( response ){
				data.id  	= response.id;
                data.status = 1;
				ResponsibleOwner.display( data )																   
				ResponsibleOwner.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
		
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#responsible_owner_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the responsible owner name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateResponsibleOwner", { id : $("#actid").val(), data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
		
	} , 
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} ,
	
	update			: function()
	{
		var message 	 = $("#responsible_owner_message").show();
		var status		= $("#status :selected").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateResponsibleOwner", {  id : $("#actid").val(), status : status }, function( response ){	
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
		}, "json");
	}
	
	
	
	
}
