// JavaScript Document
$(function(){
	
	
	var count = 0;
	var max   = 100;
	$(".textcounter").html( "<br />"+ max +" allowed ")
	$(".checkcounter").keyup(function(){
		var id   = this.id
		var text = $(this).val();
		count    = text.length
		if( count > max ){
			var new_text = text.substr(0, max);
			$(this).val( new_text );
			return false;
		} else {
			var left = max - count;
			$("#count_"+id).html( "<br />"+left+" characters left"  )
			//$(".textcounter").html( "<br />"+left+" characters left"  )
			return true;
		}
		
	});	
	
	$(".submenu>li").buttonset();
	
	$("textarea").not(".checkcounter").attr("cols", 60);
	$("textarea").not(".checkcounter").attr("rows", 7);	
	
	$("th").css({"text-align":"left"})	
	
	$("#link_to_page td").hover(function(){
		$(this).addClass("tdhover")		
	}, function(){
		$(this).removeClass("tdhover")
	});
	
	$(".setup_defaults").click(function(){
		var id = $(this).attr("id");		
		Defaults.loadPage( id );
		return false;
	});
	
	$(".datepicker").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		dateFormat 		: "dd-M"
	});

	 $(".logs").click(function(){
	       var id = $(this).attr("id");
	       var table = id.replace("view_", "");
	       $.post("setupcontroller.php?action=view_log", { table_name : table}, function( data ){
	            $("#log_div").html("");
	            if( $.isEmptyObject(data)) {
	                $("<div />",{id:"logs",html:"There are no audit logs "}).insertAfter("#log_div")
	            } else {

	               $("#log_div").append($("<div />",{id:"logs"}).append($("<table />",{id:"log_table"})))

	                $("#log_table").append($("<tr />")
	                    .append($("<th />",{html:"Date"}).addClass("log"))
	                    .append($("<th />",{html:"Ref"}).addClass("log"))
	                    .append($("<th />",{html:"Audit log"}).addClass("log"))
	                )
	                $.each( data, function( index, log){
	                    $("#log_table").append($("<tr />")
	                        .append($("<td />",{html:log.date}))
	                        .append($("<td />",{html:log.ref}))
	                        .append($("<td />",{html:log.changes}))
	                    )
	                });
	            }

	       },"json");

	   });


});

var Defaults =  {
	
	loadPage		: function( id )
	{
		document.location.href = id+".php";	
	}	
	
	
	
}
