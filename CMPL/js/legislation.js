// JavaScript Document
$(function(){
 	
	if( $("#legislationid").val() == undefined)
	{
		Legislation.getTypes();
		Legislation.getCategories();
		Legislation.getOrganisationSizes();
		Legislation.getOrganisationTypes();
		Legislation.getOrganisationCapacity();
		Legislation.getOrganisations();
	}

	$("#type").change(function(){
		
		var selId = $(this).val();
		if( selId != "3" )
		{		
			$("<div />",{ id:"legislationList"})
			.append($("<table />")
			  .append($("<tr />")
				 .append($("<th />",{text:"Select Legislation"}))
				 .append($("<td />")
				   .append($("<select />",{id:"legislation", name:"legislation"}))		 
				 )
			  )		
			)
			.dialog({
				autoOpen	: true,
				modal		: true, 
				title 		: "Select main legislation",
				width		: "500px",		
				buttons		: {
								 "Ok"		: function()
								 {
									var selectedLegislation = $("#legislation :selected").val();
									if( selectedLegislation == "" )
									{
										$("#legislationList").append($("<div />",{html:"You have not selected any legislation"}))									
									} else {
										$("#main_legislation").val( selectedLegislation )									
										$("#legislationList").dialog("destroy");
										$("#legislationList").remove();
									}
								 } , 
								 "Cancel"	: function()
								 {
									$("#legislationList").dialog("destroy");
									$("#legislationList").remove();
								 }
							 }
			})
			
			$.getJSON("newcontroller.php?action=getLegislationlist", function( responseList ){
				if( $.isEmptyObject(responseList))
				{
					$("#legislation").hide();
					$("#legislationList").append($("<div />",{html:"There are no legislations to select"}))
				} else {
					$("#legislation").show();
					$.each( responseList, function( i, val){
						$("#legislation").append($("<option />",{text:val.name, value:val.id}))					
					});
				}
			});
		}
		return false;
	});
	
	$("#save").click(function(){
		Legislation.save();		
		return false;					  
	});
	
	$("#edit").click(function(){
		Legislation.edit();		
		return false;						
	});
	
	$("#view_log").click(function(){
		Legislation.view_log();
		return false;
	});	
	
	$("#delete").click(function( ){
		Legislation.deleteLegislation();	
		return false;
	});
	
	$("#responsible_organisation").change(function(){
		Legislation.getBusinessUser( $(this).val() );
	});
	
});

Legislation			= {

	getBusinessUser			: function( id )
	{
		$("#business_partner").html("");
		$("#business_partner").append($("<option />",{text:"--select user--", value:""}))
		$.post("../class/request.php?action=BusinessPartnerUser.getUsers", { id :id }, function( users ){
			$.each( users, function(index, user){
				$("#business_partner").append($("<option />",{text:user.user, value:user.tkid}))				
			})			
		},"json")	
	} ,	
		
	getOrganisations 		: function()
	{
		$.post("newcontroller.php?action=getResponsibleOrganisations", function( orgs ){
			$.each( orgs, function( index, org){
				$("#responsible_organisation").append($("<option />",{text:org.name, value:org.id}))
			})
		},"json");
	}  ,
	getTypes 	: function()
	{
		$.post("newcontroller.php?action=getLegislativeTypes", function( types ){
			$.each( types, function( index, type){
				$("#type").append($("<option />",{text:type.name, value:type.id}))					
			});
		}, "json");	
	} , 	
	
	getCategories 	: function()
	{
		$.post("newcontroller.php?action=getLegislativeCategories", function( categories ){
			$.each( categories, function( index, category){
				$("#category").append($("<option />",{text:category.name, value:category.id,  selected:(category.id == 1 ? "selected" : "")}))					
			});
		}, "json");	
	} , 
	
	getOrganisationSizes	: function()
	{
		$.post("newcontroller.php?action=getOrganisationSizes", function( org_sizes ){
			$.each( org_sizes, function( index, org_size){
				$("#organisation_size").append($("<option />",{text:org_size.name, value:org_size.id, selected:(org_size.id == 1 ? "selected" : "")}))					
			});
		}, "json");	
	}, 
	
	getOrganisationTypes	: function()
	{
		$.post("newcontroller.php?action=getLegOrganisationTypes", function( org_types ){
			$.each( org_types, function( index, org_type ){
				$("#organisation_type").append($("<option />",{text:org_type.name, value:org_type.id, selected:(org_type.id == 1 ? "selected" : "")}))
			});
		}, "json");	
	} , 
	
	getOrganisationCapacity	: function()
	{
		$.post("newcontroller.php?action=getOrganisationCapacity", function( org_capacity ){
			$.each( org_capacity, function( index, org_capa ){
				$("#organisation_capacity").append($("<option />",{text:org_capa.name, value:org_capa.id, selected:(org_capa.id == 1 ? "selected" : "")}))					
			});
		}, "json");	
	} , 
	
	save					: function()
	{
		var data 				      = {};
		data.name 				      = $("#name").val();
		data.reference			      = $("#reference").val();
		data.type				      = $("#type :selected").val();
		data.category			      = $("#category").val();
		data.organisation_size 	   = $("#organisation_size").val();
		data.legislation_date  	   = $("#legislation_date").val();
		data.legislation_number	   = $("#legislation_number").val();
		data.organisation_type 	   = $("#organisation_type").val();
		data.organisation_capacity = $("#organisation_capacity").val();
		data.responsible_organisation = $("#responsible_organisation :selected").val();
		data.hyperlink_to_act	   = $("#hyperlink_to_act").val();
		data.available_to_organisation = $("#legislation_available :selected").val()
		data.business_partner	   = $("#business_partner :selected").val()
		data.parent_id 			   = $("#main_legislation").val();
		
		var valid = Legislation.isValid( data );
		
		if( valid == true){
			jsDisplayResult("info", "info", "Saving legislation. . . <img src='../images/loaderA32.gif' />");
			$.post("newcontroller.php?action=saveLegislation", { data: data}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
                $.each( data, function( index, value){
                    $("#"+index).val("");
                });
			} ,"json");		

		} else {
			jsDisplayResult("error", "error", valid);
		}
		
	} , 
	
	edit 				: function()
	{
		var data 				   = {};
		data.name 				   = $("#name").val();
		data.reference			   = $("#reference").val();
		data.type				   = $("#type :selected").val();
		data.category			   = $("#category").val();
		data.organisation_size 	   = $("#organisation_size").val();
		data.legislation_date  	   = $("#legislation_date").val();
		data.legislation_number	   = $("#legislation_number").val();
		data.organisation_type 	   = $("#organisation_type").val();
		data.organisation_capacity = $("#organisation_capacity").val();
		data.responsible_organisation = $("#responsible_organisation :selected").val();
		data.hyperlink_to_act	   = $("#hyperlink_to_act").val();
		data.available_to_organisation = $("#legislation_available :selected").val()
		data.business_partner	   = $("#business_partner :selected").val()
		//data.parent_id 			   = $("#main_legislation").val();		
		var valid = Legislation.isValid( data );

		if( valid == true){
			jsDisplayResult("info", "info", "Updating legislation. . . <img src='../images/loaderA32.gif' />");
			$.post("../class/request.php?action=ManageLegislation.editLegislation", { data: data, id:$("#legislationid").val()}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
			} ,"json");

		} else {
			jsDisplayResult("error", "error", valid);
		}
	} , 
	
	deleteLegislation	: function()
	{
		if( confirm("Are you sure you want to delete this legislation"))
		{
			jsDisplayResult("info", "info", "Deleting legislation. . . <img src='../images/loaderA32.gif' />");
			$.post("managecontroller.php?action=deleteLegislation", { id:$("#legislationid").val()}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
			} ,"json");
		}
	} ,
	
	update 			: function()
	{
		 
		 
		 
	} ,
	
	view_log			: function()
	{
        $.post("managecontroller.php?action=getLegislationEdits", {id : $("#legislationid").val()}, function( response ){
            $("#editLog").html("");
            if( $.isEmptyObject(response)){
                $("#audit_log").html("There are no edit logs yet")																																																								} else {
                $("#audit_log").append($("<table />",{id:"table_editlog"})
                            .append($("<tr />")
                                .append($("<th />",{html:"Date"}))
                                .append($("<th />",{html:"Audit Log"}))
                                .append($("<th />",{html:"Status"}))
                            )
                          )
                $.each( response, function( index, editLog ){

                        $("#table_editlog")
                            .append($("<tr />")
                                .append($("<td />",{html:editLog.date}))
                                .append($("<td />",{html:editLog.changes}))
                                .append($("<td />",{html:editLog.status}))
                            )
                })																																																						}

        }, "json");
	} , 
	
	isValid			: function( data )
	{
		var message = $("#message").show();
		if( data.name == ""){
			return "Please enter the legislation name";			
		} else if( data.reference == ""){
			return "Please enter the legislation reference number";
		} else if( data.type == ""){
			return "Please select the legislative type";
		} else if( data.legislation_date == ""){
			return "Please enter the legislation date";
		} else if( data.responsible_organisation == ""){
			return "Please select the responsible business partner";
		} else if( data.business_partner == ""){
			return "Please select the responsible business partner user";
		} else if( data.legislation_date == ""){
			return "Please enter the legislation date";
		} else {
			return true;	
		} 
		/*else if( data.category == ""){
			return "Please select the legislative category";
		} else if( data.responsible_organisation == ""){
			return "Please select the responsible organisation";
		} else if( data.legislation_date == ""){
			return "Please enter the legislation date";
		} else if( data.legislation_number == ""){
			return "Please enter the legislation number";
		} else if( data.organisatio_type == ""){
			return "Please select the organisation type";
		} else if( data.responsible_organisation == ""){
			return "Please select the responsible organisation";
		}  else {
			return true;	
		} */
		
	}
	
}
