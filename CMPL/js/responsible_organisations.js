// JavaScript Document
$(function(){
	   
	 ResponsibleOrganisation.get()  
	   
	$("#save_org").click(function(){
		ResponsibleOrganisation.save();
		return false;
	});
	
	$("#edit_org").click(function(){
		ResponsibleOrganisation.edit();	
		return false;
	});
	
	$("#update_org").click(function(){
		ResponsibleOrganisation.update();	
		return false;
	});	
});

var ResponsibleOrganisation 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getResponsibleOrganisations", function( orgs ){
			$.each( orgs, function( index, org){
				ResponsibleOrganisation.display( org )
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#responsile_org_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the responsible organisation name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the responsible organisation description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveResponsibleOrganisation", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				ResponsibleOrganisation.display( data )																   
				ResponsibleOrganisation.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#responsile_org_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the responsible organisation name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the responsible organisation description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateResponsibleOrganisation", { id : $("#orgid").val(), data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
	    var message 	 = $("#responsile_org_message").show();
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateResponsibleOrganisation", {  id : $("#orgid").val(), status : status }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
		}, "json");
	} , 
	
	display		: function( org )
	{
		$("#responsile_org_table")
		 .append($("<tr />",{id:"tr_"+org.id})
		   .append($("<td />",{html:org.id}))
		   .append($("<td />",{html:org.name}))
		   .append($("<td />",{html:org.description}))
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+org.id, id:"edit_"+org.id, value:"Edit"}))
			  .append($("<input />",{type:"button", name:"del_"+org.id, id:"del_"+org.id, value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((org.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+org.id, id:"change_"+org.id, value:"Change Status"}))			  					
		    )		   
		 )
		 		 
		 $("#edit_"+org.id).live("click", function(){
			document.location.href = "edit_organisation.php?id="+org.id;
			return false;								   
		  });
		 
		 $("#del_"+org.id).live("click", function(){
			var message 	 = $("#responsile_org_message").show();

			if( confirm("Are you sure you want to delete this organisation")){
				jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateResponsibleOrganisation", { id : org.id , status : 2 }, function( response ){	
					$("#tr_"+org.id).fadeOut();																		  																																													 					
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
					}
				}, "json");
				
			}
			return false;								   
		  });		
		 
		 $("#change_"+org.id).live("click", function(){
			document.location.href = "change_organisation.php?id="+org.id;
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}