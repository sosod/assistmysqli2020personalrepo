// JavaScript Document
$(function(){
	   
	 Events.get()  
	   
	$("#save_event").click(function(){
		Events.save();
		return false;
	});
	
	$("#edit_event").click(function(){
		Events.edit();	
		return false;
	});
	
	$("#update_event").click(function(){
		Events.update();	
		return false;
	});	
});

var Events 	= {
	
	get 		: function()
	{
		$.post("setupcontroller.php?action=getEvents", function( response ){
			$.each( response, function( index, events){
				Events.display( events )					   
			})												  											  
		},"json");
	}  ,
	
	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the events name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveEvents", { data : data }, function( response ){		
				data.id  	= response.id; 	
				data.status = 1; 	
				Events.display( data )																   
				Events.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}																   
			},"json");	
		}
	} , 
	
	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#event_message").show(); 
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the events name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateEvents", { id : $("#eventid").val(), data : data }, function( response ){		
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}															   
			},"json");	
		}
		
	} ,
	
	update		: function()
	{
		var message 	 = $("#event_message").show(); 
		var status		= $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateEvents", {  id : $("#eventid").val(), status : status }, function( response ){																																																									 					
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
		}, "json");
	} , 
	
	display		: function( events )
	{
		$("#event_table")
		 .append($("<tr />",{id:"tr_"+events.id})
		   .append($("<td />",{html:events.id}))
		   .append($("<td />",{html:events.name}))
		   .append($("<td />",{html:events.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "edit_"+events.id,
				  					id    : ((events.status & 4) == 4 ? "edit" : "edit_"+events.id),
				  					value : "Edit"}))
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "del_"+events.id,
				  					id    : ((events.status & 4) == 4 ? "del" : "del_"+events.id),
				  					value:"Del"}))			  
			)
		   .append($("<td />")
			.append($("<span />",{html:((events.status & 1) == 1 ? "Active" : "Inactive")}))		 
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "change_"+events.id,
				  					id    : ((events.status & 4) == 4 ? "change" : "change_"+events.id),
				  					value :"Change Status"}))			  					
		    )		   
		 )

		 if((events.status & 4) === 4)
		 {
		    $("input[name='edit_"+events.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+events.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+events.id+"']").attr('disabled', 'disabled');
		 } 		
		  		 
		 $("#edit_"+events.id).live("click", function(){
			document.location.href = "edit_event.php?id="+events.id;								   
			return false;								   
		  });
		 
		 $("#del_"+events.id).live("click", function(){
			var message 	 = $("#event_message").show(); 
			if( confirm("Are you sure you want to delete this event")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateEvents",{ id : events.id , status : 2 }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							 $("#tr_"+events.id).fadeOut();		
							jsDisplayResult("ok", "ok", response.text);
						}
				}, "json");	
			}
			return false;								   
		  });		
		 
		 $("#change_"+events.id).live("click", function(){
			document.location.href = "change_events.php?id="+events.id;										 
		 });
	} ,
	
	
	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");				   
		});	
	} 
	
	
	
}
