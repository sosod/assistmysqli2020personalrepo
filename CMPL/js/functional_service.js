/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/16/11
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
// JavaScript Document
$(function(){

	 FunctionalService.get()

	$("#save_freq").click(function(){
		FunctionalService.save();
		return false;
	});

	$("#edit_freq").click(function(){
		FunctionalService.edit();
		return false;
	});

	$("#update_freq").click(function(){
		FunctionalService.update();
		return false;
	});
});

var FunctionalService 	= {

	get 		: function()
	{
		$.post("setupcontroller.php?action=getFunctionalService", function( response ){
			$.each( response, function( index, freq){
				FunctionalService.display( freq )
			})
		},"json");
	}  ,

	save		: function()
	{
		var data 		 = {};
		var message 	 = $("#functional_service_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();

		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the functional service name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveFunctionalService", { data : data }, function( response ){
				data.id  	= response.id;
				data.status = 1;
				FunctionalService.display( data )
				FunctionalService.empty( data );
				if( response.error ){
					jsDisplayResult("error", "error", response.text);
				} else {	
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}
	} ,

	edit		: function()
	{
		var data 		 = {};
		var message 	 = $("#functional_service_message").show();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();

		if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the functional service name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=updateFunctionalService", { id : $("#freqid").val(), data : data }, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);
				} else {	
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");
		}

	} ,

	update		: function()
	{
		var message 	 = $("#functional_service_message").show();
		var status		 = $("#status :selected").val()
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("setupcontroller.php?action=updateFunctionalService", { id : $("#freqid").val(), status : status }, function( response ){																																																									 					
			if( response.error ){
				jsDisplayResult("error", "error", response.text);
			} else {	
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");
	} ,

	display		: function( freq )
	{
		$("#functional_service_table")
		 .append($("<tr />",{id:"tr_"+freq.id})
		   .append($("<td />",{html:freq.id}))
		   .append($("<td />",{html:freq.name}))
		   .append($("<td />",{html:freq.description}))
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "edit_"+freq.id,
				  					id   : ((freq.status & 4) == 4 ? "edit" : "edit_"+freq.id),
				  					value:"Edit"}))
			  .append($("<input />",{
				  					type  : "button",
				  					name  : "del_"+freq.id,
				  					id    : ((freq.status & 4) == 4 ? "del" : "del_"+freq.id),
				  					value:"Del"}))
			)
		   .append($("<td />")
			.append($("<span />",{html:((freq.status & 1) == 1 ? "Active" : "Inactive")}))
			)
		   .append($("<td />")
			  .append($("<input />",{
				  					type : "button",
				  					name : "change_"+freq.id,
				  					id   : ((freq.status & 4) == 4 ? "change" : "change_"+freq.id),
				  					value:"Change Status"}))
		    )
		 )

		 if((freq.status & 4) === 4)
		 {
		    $("input[name='edit_"+freq.id+"']").attr('disabled', 'disabled');
		    $("input[name='change_"+freq.id+"']").attr('disabled', 'disabled');
		    $("input[name='del_"+freq.id+"']").attr('disabled', 'disabled');
		 } 		 		 
		 	

		 $("#edit_"+freq.id).live("click", function(){
			document.location.href = "edit_functional_service.php?id="+freq.id;
			return false;
		  });

		 $("#del_"+freq.id).live("click", function(){
			var message 	 = $("#functional_service_message").show();
			if( confirm("Are you sure you want to delete this functional service")){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
					$.post("setupcontroller.php?action=updateFunctionalService", { id : freq.id , status : 2 }, function( response ){
						if( response.error ){
							jsDisplayResult("error", "error", response.text);
						} else {
							 $("#tr_"+freq.id).fadeOut();	
							jsDisplayResult("ok", "ok", response.text);
						}
				}, "json");
			}
			return false;
		  });

		 $("#change_"+freq.id).live("click", function(){
			document.location.href = "change_functional_service.php?id="+freq.id;
		 });
	} ,


	empty		: function( data )
	{
		$.each( data, function( index, value ){
			$("#"+index).val("");
		});
	}



}
