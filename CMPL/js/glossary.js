// JavaScript Document
$(function(){
		   
	Glossary .get();	   
		   
	$("#add").click(function(){
		Glossary.save();
		return false;					 
	});

	$("#edit").click(function(){
		Glossary.edit();
		return false;					 
	});
	
});

var Glossary  = {
	
	get			: function()
	{
		$.getJSON("setupcontroller.php?action=getGlossaries",function( response ){
			$.each( response, function( index, glossary){
				Glossary.display( glossary );					   
			})														 
		});	
	} , 
	
	display		: function( glossary )
	{	
		$("#table_glossary")
		 .append($("<tr />",{id:"tr_"+glossary.id})
		  .append($("<td />",{html:glossary.id}))		   
		  .append($("<td />",{html:glossary.category}))		   
		  .append($("<td />",{html:glossary.terminology}))		   
		  .append($("<td />",{html:glossary.explanation}))		   	   
		  .append($("<td />")
			.append($("<input />",{type:"submit", name:"edit_"+glossary.id, id:"edit_"+glossary.id, value:"Edit"}))		
		  )
		  .append($("<td />")
			.append($("<input />",{type:"submit", name:"del_"+glossary.id, id:"del_"+glossary.id, value:"Delete"}))		
		  ) 
		 )
		 
		 $("#edit_"+glossary.id).live("click", function(){
			document.location.href = "edit_glossary.php?id="+glossary.id;
			return false;											
		});
		 
		 $("#del_"+glossary.id).live("click", function(){
			if(confirm("Are you sure you want to delete this glossary term")){
				 jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
				 $.post("setupcontroller.php?action=updateGlossary", { 
					 	status 		: 2,
						id			: glossary.id
					}, function( response ){	
						if( response.error ){
							jsDisplayResult("error", "error", response.text);		
						} else {					
							jsDisplayResult("ok", "ok", response.text);
						}
					},"json")
					
				$("#tr_"+glossary.id).fadeOut();	
			}
			return false;											
		});
		 		 
	} ,
	
	save 		: function()
	{
		var category 	= $("#category").val();
		var terminology = $("#terminology").val();
		var explanation = $("#explanation").val();
		var message 	= $("#loading");
		var data	 	= {};
		if( category == ""){
			jsDisplayResult("error", "error", "Please enter the category");
			return false;
		} else if( terminology == ""){
			jsDisplayResult("error", "error", "Please enter the terminology");
			return false;
		} else if( explanation == ""){
			jsDisplayResult("error", "error", "Please enter the explanation");	
		} else {
		jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
		 $.post("setupcontroller.php?action=saveGlossary", { 
				category    : category,
				terminology : terminology,
				explanation : explanation
			}, function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
					$("#category").val("");
					$("#terminology").val("");
					$("#explanation").val("");
					data.category = category;
					data.terminology = terminology;		
					data.explanation = explanation;						
					data.id 		 = response.id
					Glossary.display( data )					
				}
		},"json")
	  }
		
	},
	edit 		: function()
	{
		var category 	= $("#category").val();
		var terminology = $("#terminology").val();
		var explanation = $("#explanation").val();
		var message 	= $("#loading");
		var data	 	= {};
		if( category == ""){
			jsDisplayResult("error", "error", "Please enter the category");
			return false;
		} else if( terminology == ""){
			jsDisplayResult("error", "error", "Please enter the terminology");
			return false;
		} else if( explanation == ""){
			jsDisplayResult("error", "error", "Please enter the explanation");	
		} else {
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
		 $.post("setupcontroller.php?action=updateGlossary", { 
				category    : category,
				terminology : terminology,
				explanation : explanation,
				id			: $("#glossaryid").val()
			}, function( response ){	
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json")
	  }
		
	}	
	
	
	
}