$(function(){
	
	Directorate.get();
	
	$("#add_new").click(function(){
		document.location.href = "add_new_directorate.php";
		return false;
	});
	
	$("#update").click(function(){
		Directorate.update();
		return false;
	});
	
	$("#save").click(function(){
		Directorate.save();		
		return false;
	});
	
	$("#cancel").click(function(){
		history.back();
		return false;
	});
	
	$("#assign_owner").click(function(){
		Directorate.assignOwner();
		return false;
	});
	
	$("#sub_directorate").change(function(){
		$("#action_owner_title").attr("selected", "").attr("disabled", "");
		Directorate.getActionOwners( $(this).val() ); 		
		return false;
	});
	
});


var Directorate		= {
		
		get		: function()
		{	
			$.getJSON("setupcontroller.php?action=getDirectorate", function( responseData ){
				$.each( responseData, function( index, data) {
					$("#table_directorates").append($("<tr />")
					  .append($("<td />",{html:data.id}))
					  .append($("<td />",{html:data.name}))
					  .append($("<td />",{id:"subdirectorates_"+data.id}))
					  .append($("<td />")
						 .append($("<input />",{
							 					type : "button",
							 					name : "edit_directorate_"+data.id,
							 					id   : ((data.status & 4) == 4 ? "edit" : "edit_directorate_"+data.id),
							 					value: "Edit Directorate"}))						 
						 .append($("<input />",{
							 					type  : "button",
							 					name  : "edit_title_"+data.id,
							 					id    : ((data.status & 4) == 4 ? "edit" : "edit_title_"+data.id),
							 					value : "Edit Administator Titles"}))
					  )
					)		
		                if((data.status & 4) === 4)
		                {
		                   $("input[name='edit_"+data.id+"']").attr('disabled', 'disabled');
		                   $("input[name='change_"+data.id+"']").attr('disabled', 'disabled');
		                   $("input[name='del_"+data.id+"']").attr('disabled', 'disabled');
		                } 	
					if( !$.isEmptyObject( data.sub))
					{
						$("#subdirectorates_"+data.dirid).append($("<ul />",{id:"li_"+data.dirid}))
						$.each( data.sub, function( i , val){
							$("#li_"+data.dirid).append($("<li />",{ html: val.dirtxt }))								
						});
					}	

					$("#edit_directorate_"+data.dirid).live("click", function(){
						document.location.href = "edit_directorate.php?id="+data.dirid;						
						return false;
					});
					
					$("#edit_title_"+data.dirid).live("click", function(){
						document.location.href = "edit_administator_title.php?id="+data.dirid;						
						return false;
					});

				});				
			});
		} , 
		
		save		: function()
		{
			if( $("#directorate_name").val() == "") {
				jsDisplayResult("error", "error", "Please enter the directorate name");
				return false;
			} else if( $("#primary_subdirectorate_name").val() == ""){
				jsDisplayResult("error", "error", "Please enter the subdirectorate name");
				return false;
			} else {
				jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=saveDirectorate", {
					data : $("#add_directorate_form").serializeArray()
				}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
					}		
				}, "json")
			}
			
		} , 
		
		update		: function()
		{
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=editDirectorate", {
				data 	: $("#editdirectorate_form").serializeArray()				
			}, function( response ) {
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}			
			}, "json");
		} , 
		
		assignOwner	: function()
		{
			if( $("#sub_directorate :selected").val() == "")
			{
				jsDisplayResult("error", "error", "Please select the sub directorate . .");
				return false;
			} else if( $("#action_owner_title :selected").val() == ""){
				jsDisplayResult("error", "error", "Please select action owner . .");
				return false;
			} else {
				jsDisplayResult("info", "info", "Assigning . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=assignOwner", {
					id 		: $("#directorate").val(),
					sub		: $("#sub_directorate :selected").val(),
					owner	: $("#action_owner_title :selected").val()
				}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);		
					} else {					
						jsDisplayResult("ok", "ok", response.text);
					}					
				},"json");	
				
				
				
			}
			
		} , 
		
		getActionOwners			: function( id )
		{
			$.getJSON("setupcontroller.php?action=getSubOwner",
			{
				subid : id 
			} , function( actionOwner ) {
					if($.isEmptyObject(actionOwner))
				{
					$("select#action_owner_title option[value='']").attr("selected", "selected");
				} else {
					$.each( actionOwner, function( index, val) {
						$("select#action_owner_title option[value="+val.actionowner_id+"]").attr("selected", "selected");
					});						
				}		
			});
			
		}
		
		
} 
