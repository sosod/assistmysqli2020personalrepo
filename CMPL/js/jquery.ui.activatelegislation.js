$.widget("ui.activatelegislation", {
	
	options		: {
		start			: 0,
		limit			: 10,
		current			: 1,
		total			: 0
	} , 
	
	_init				: function()
	{
		this._getActivation();
	} , 
	
	_create				: function()
	{
	
	} , 
	
	_getActivation		: function()
	{
		var self = this;
		$.getJSON("../new/newcontroller.php?action=getActivation",{
			start		: self.options.start,
			limit		: self.options.limit
		}, function( responseData ){
			$(self.element).html("")
			$(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
			    .append($("<tr />")
			       .append($("<td />").addClass("noborder")
			    	  .append($("<table />",{id:"awaiting_activation", width:"100%"})
			    		 .append($("<tr />")
			    		   .append($("<td />",{colspan:"6", html:"<h4>Legislations Awaiting Activation</h4>"}).addClass("noborder"))		 
			    		 )	  
			    	  )	   
			       )
			       .append($("<td />").addClass("noborder")
			    	  .append($("<table />",{id:"activated", width:"100%"})
			    		.append($("<tr />")
			    		  .append($("<td />",{colspan:"5", html:"<h4>Legislation activated</h4>"}).addClass("noborder"))		
			    		)	  
			    	  ) 	   
			       )
			    )		
			)				
			//$("#awaiting_activation").html("");
			//$("#activated").html("");
			self._displayHeaders( responseData.headers, "awaiting_activation" );
			self._displayHeaders( responseData.headers, "activated" );
			if( $.isEmptyObject(responseData.awaiting))
			{
				$("#awaiting_activation").append($("<tr />")
				   .append($("<td />",{colspan:"6",html:"No legislations awaiting approval"}).addClass("noborder"))		
				)
			} else {
				self._display( responseData.awaiting, "awaiting_activation")
			}
			
			if($.isEmptyObject(responseData.activated))
			{
				$("#activated").append($("<tr />",{id:"no_activated"})
					.append($("<td />",{colspan:"5",html:"No legislations activated approval"}).addClass("noborder"))		
				)				
			} else {				
				self._display( responseData.activated, "activated")
			}
		});
	} , 
	
	_display			: function( legislations, table )
	{
		var self = this;
		
		$.each(legislations, function( index, legislation){
			var tr = $("<tr />").addClass("removable")
			$.each( legislation, function(key , val){
				tr.append($("<td />",{html:val}))				
			});	
			   tr.append($("<td />").css({display:(table == "awaiting_activation" ? "table-cell" : "none")})
				   .append($("<input />",{
					   					  type		: "button",
					   					  id		: "activate_"+index,
					   					  name		: "activate_"+index,
					   					  value		: "Activate"
				   }))	   
			   )
			
			$("#activate_"+index).live("click", function(){
				$("<div />",{id:"activatedialog_"+index})
				  .append($("<p />").css({"padding":"7px"})
				    .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
					.append($("<span />",{html:"By activating this legislation, you are confirming that the deliverable and the action data captured on Compliance Master can be made available to the clients as defined"}))
				  )
				 .append($("<div />",{id:"message_"+index}))
				 .dialog({
					 	  autoOpen		: true,
					 	  modal			: true,
					 	  title			: "Activate Legislation",
					 	  buttons		: {
					 						 "Activate"		: function()
					 						 {
												jsDisplayResult("info", "info", "Activating  . . . <img src='../images/loaderA32.gif' />");
					 							$.post("managecontroller.php?action=activateLegislation", { id : index}, function( response ){								
					 								if( response.error )
					 								{
					 									jsDisplayResult("error", "error", response.text);
					 								} else {
					 									jsDisplayResult("ok", "ok", response.text);
					 								}
					 								$("#no_activated").remove();
					 								self._getActivation();
					 								$("#activatedialog_"+index).dialog("destroy");
					 								$("#activatedialog_"+index).remove();
					 							},"json");
					 						 } , 					 						 
					 						 "Cancel"		: function()
					 						 {
					 							 $("#activatedialog_"+index).dialog("destroy");
					 							 $("#activatedialog_"+index).remove();
					 						 }
				 						   }
				 })
				return false;
			});  
			   
			$("#"+table).append(tr);
		});		
	} , 
		
	_displayHeaders		: function( headers, table  )
	{
		var self = this;
		var tr   = $("<tr />").addClass("removable");
		$.each(headers,function( key, val){
			tr.append($("<th />",{html:val}))			
		});
		if(!$.isEmptyObject(headers)){
			tr.append($("<th />").css({display:(table == "awaiting_activation" ? "table-cell" : "none")}))
		}
		$("#"+table).append( tr );
		
	}
	
	
});