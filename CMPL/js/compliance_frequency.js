//javascript
$(function(){
	
	ComplianceFrequency.get();
	
	$("#save").click(function(){
		ComplianceFrequency.save();
		return false;
	});

	$("#update").click(function(){
		ComplianceFrequency.update();
		return false;
	});

	$("#update_status").click(function(){
		ComplianceFrequency.updateStatus();
		return false;
	});	
});

ComplianceFrequency	= {
	
	get			: function()
	{
		$.post("setupcontroller.php?action=getFrequencies", function( response ){
			$.each( response , function(index, data ){
				ComplianceFrequency.display( data );
			});
		}, "json");
	} ,
	
	save		: function()
	{
		var data  		 = {};
		var message 	 = $(".message").slideDown();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		
		if(data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving the compliance frequency  . . . <img src='../images/loaderA32.gif' />");
			$.post("setupcontroller.php?action=saveComplianceFrequency", data , function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);										
				} else {
					data.id 	= response.id;
					data.status = 1;
					ComplianceFrequency.display( data );	
					$("#name").val("")
					$("#description").val("")					
					jsDisplayResult("ok", "ok", response.text);
				}				
			},"json");			
		}
	
	} , 
	
	update		:function()
	{
		var data  		 = {};
		var message 	 = $(".message").slideDown();
		data.name 		 = $("#name").val();
		data.description = $("#description").val();
		data.id 		 = $("#complianceId").val();
		
		if(data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");
			return false;
		} else if( data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating the compliance frequency . . . <img src='../images/loaderA32.gif' />");			
			$.post("setupcontroller.php?action=updateComplianceFrequency", {dat : data,id :$("#complianceId").val()} , function( response ){
				if( response.error ){
					jsDisplayResult("error", "error", response.text);		
				} else {					
					jsDisplayResult("ok", "ok", response.text);
				}
			},"json");			
		}	
	} ,
	
	display		: function( complianceFrequency )
	{
		var message 	 = $(".message").slideDown();
		$("#compliance_frequency_table")
		  .append($("<tr />",{id:"tr_"+complianceFrequency.id})
			.append($("<td />",{html:complianceFrequency.id}))
			.append($("<td />",{html:complianceFrequency.name}))
			.append($("<td />",{html:complianceFrequency.description}))
			.append($("<td />")
				.append($("<input />",{type:'button', name:'edit_'+complianceFrequency.id, id:'edit_'+complianceFrequency.id, value:'Edit'}))
				.append($("<input />",{type:'button', name:'del_'+complianceFrequency.id, id:'del_'+complianceFrequency.id, value:'Del'}))
			)
			.append($("<td />",{html:((complianceFrequency.status & 1) == 1) ? "Active" : "Inactive"}))
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"change_status_"+complianceFrequency.id, id:"change_status_"+complianceFrequency.id, value:"Change Status"}))
			)
		  )
		  
		  $("#edit_"+complianceFrequency.id).live("click", function(){
			document.location.href = "edit_compliance_frequency.php?id="+complianceFrequency.id;
			return false;
		  });

		  $("#del_"+complianceFrequency.id).live("click", function(){
			if(confirm("Are you sure you want to delete this compliance frequency "))
			{
				var data 	= {};
				data.status = 2
				data.id     = complianceFrequency.id
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post("setupcontroller.php?action=updateComplianceFrequency", { dat: data, id:complianceFrequency.id}, function( response ){
					if( response.error ){
						jsDisplayResult("error", "error", response.text);
					} else {
						$("#tr_"+complianceFrequency.id).fadeOut();
						jsDisplayResult("ok", "ok", response.text);
					}
				}, "json");
			}
			return false;
		  });
		  		  
		  $("#change_status_"+complianceFrequency.id).live("click", function(){
			document.location.href = "change_compliance_frequency_status.php?id="+complianceFrequency.id;
			return false;
		  });
		  		  
		  
	} ,
	
	updateStatus		: function()
	{
		var data 	= {};
		var message 	 = $(".message").slideDown();
		data.status 	= $("#status :selected").val();
		data.id 	    = $("#complianceId").val();
		jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");
		$.post("setupcontroller.php?action=updateComplianceFrequency", { dat: data, id:$("#complianceId").val()}, function( response ){
			if( response.error ){
				jsDisplayResult("error", "error", response.text);
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}
		}, "json");			
	}

}
