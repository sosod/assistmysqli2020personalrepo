$.widget("ui.action",{
		
	options		: {
		start			: 0,
		limit			: 10,
		current			: 1,
		total			: 0,
		controller		: "manage",
        editAction  	: false,
        deleteAction	: false,
        updateAction    : false,
        id				: 0,
        section			: "manage", 
        thclass			: ""
	} , 
	
	_init		: function()
	{
		this._getActions();		
	} , 
	
	_create		: function()
	{		
	} , 
	
	_getActions	: function()
	{
		var self = this;
        $.post(self.options.controller+".php?action=getActions",{
        	start		: self.options.start,
        	limit		: self.options.limit,
        	section		: self.options.section,
        	id			: self.options.id
        } , function( actionsResponse ){
        	$(self.element).html("");
        	$(self.element).append($("<table />",{id:"table_"+self.options.id})	);
        	self._displayPaging( actionsResponse.total, actionsResponse.cols)   
        	self.options.total = actionsResponse.total;
        	self._displayHeaders( actionsResponse.headers )
        	$("#action_available").val( actionsResponse.total )
        	if( $.isEmptyObject( actionsResponse.actions ) )
        	{
        		$("#table_"+self.options.id).append($("<tr />")
        		  .append($("<td />",{colspan:actionsResponse.cols+1, html:"There are no actions"}))		
        		)
        	} else {
            	self._display( actionsResponse.actions )
        	}
        },"json");
	} , 
	
	_display	: function( actions )
	{
		var self = this;
		$.each( actions, function( index, action){
			var tr = $("<tr />",{id:"tr_"+index})
			$.each( action, function( i , val){
				tr.append($("<td />",{html:val}))				
			});
			
            tr.append($("<td />").css({"display":(self.options.editAction ? "block" : "none")})
               .append($("<input />",{type:"submit", id:"edit_"+index, name:"edit_"+index, value:"Edit" }))
               .append($("<input />",{type:"submit", id:"copy_"+index, name:"copy_"+index, value:"Copy" })
            		   .css({"display" : (self.options.section == "new" ? "block" : "none")})
               )
               .append($("<input />",{type:"submit", id:"del_"+index, name:"del_"+index, value:"Del" }))
            )

           tr.append($("<td />").css({"display":(self.options.deleteAction ? "block" : "none")})
              .append($("<input />",{type:"submit", id:"del_"+index, name:"del_"+index, value:"Delete" }))                
            )

            $("#edit_"+index).live("click", function(){
                document.location.href = "../manage/edit_action.php?id="+index;
                return false;
            });			
            
            $("#copy_"+index).live("click", function(){
                document.location.href = "copy_action.php?id="+index+"&deliverableid="+self.options.id;
                return false;
            });
            
            $("#del_"+index).live("click",function(){
            	if(confirm("Are you sure you want to delete this action")){
	        		jsDisplayResult("info", "info", "Deleting action . . . <img src='../images/loaderA32.gif' />");
            		$.post(self.options.controller+".php?action=deleteAction",{
            			id		: index
            		},function( response ){
            			if( response.error )
            			{
            				jsDisplayResult("error", "error", response.text );	
            			} else {
            				jsDisplayResult("ok", "ok", response.text );
            				$("#tr_"+index).fadeOut();
            			}           			
            		},"json")
            	}
            });
			
			$("#table_"+self.options.id).append( tr );			
		});
    	$("#table_"+self.options.id+" tr").hover(function(){
			$(this).addClass("tdhover")		
		}, function(){
			$(this).removeClass("tdhover")
		});			
	} , 
	
	_displayHeaders : function( headers )
	{
		var tr   = $("<tr />");
		var self = this;
		$.each( headers, function( index, head){
			tr.append($("<th />",{html:head}).addClass((self.options.thclass == "" ? "" : self.options.thclass )))			
		});
        tr.append($("<th />").css({"display":(self.options.editAction ? "table-cell" : "none")}).addClass((self.options.thclass == "" ? "" : self.options.thclass )));
        tr.append($("<th />").css({"display":(self.options.deleteAction ? "table-cell" : "none")}).addClass((self.options.thclass == "" ? "" : self.options.thclass )));		
        $("#table_"+self.options.id).append( tr );		
	} ,
	  _displayPaging			: function( total, cols) {
		var self = this;
		var pages;
		if( total%self.options.limit > 0) {
			pages   = Math.ceil(total/self.options.limit); 
		} else {
			pages   = Math.floor(total/self.options.limit); 
		}
		$("#table_"+self.options.id)
			.append($("<tr />")
			  .append($("<td />",{colspan:cols+1})
				 .append($("<input />",{type:"button", name:"first", id:"first", value:"|<", disabled:(self.options.current < 2 ? 'disabled' : '' )}))		
				 .append($("<input />",{type:"button", name:"previous", id:"previous", value:"<", disabled:(self.options.current < 2 ? 'disabled' : '' )}))				
				 .append("&nbsp;&nbsp;&nbsp;")
			     .append($("<span />",{html:"Page "+self.options.current +" / "+pages, align:"center"}).css({"text-align":"center"}))
			     .append("&nbsp;&nbsp;&nbsp;")
				 .append($("<input />",{type:"button", name:"next", id:"next", value:">", disabled:((self.options.current ==pages || pages == 0) ? 'disabled' : '' )}))						
				 .append($("<input />",{type:"button", name:"last", id:"last", value:">|", disabled:((self.options.current ==pages || pages == 0) ? 'disabled' : '' )}))				 
			   )
			  .append($("<td />", {colspan:1}).css({"display":(self.options.editDeliverable ? "table-cell" : "none")}))
			  .append($("<td />", {colspan:1}).css({"display":(self.options.addAction? "table-cell" : "none")}))
			)
			$("#next").bind("click", function( e ){
				e.preventDefault();				
				self._getNext();
			});
			$("#last").bind("click",  function( e ){
				e.preventDefault( self );				
				self._getLast();
			});
			
			$("#previous").bind("click", function( e ){
				e.preventDefault();
				self._getPrevious( self );
			});
			
			$("#first").bind("click",  function( e ){
				e.preventDefault();
				self._getFirst();
			});	
	} , 
	_getNext  			: function() {
		var self = this;
		self.options.current = parseFloat( parseFloat(self.options.current) + 1 );	
		self.options.start   = parseFloat( self.options.current - 1) * parseFloat( self.options.limit );
		self._getActions()
	},	
	_getLast  			: function() {
		var self = this;
		self.options.current = Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );			
		self._getActions()
	},	
	_getPrevious    	: function() {
		var self = this;
		self.options.current  = parseFloat( self.options.current ) - 1;	
		self.options.start    = (self.options.current-1)*self.options.limit;
		self._getActions();
	},	
	_getFirst  			: function() {
		var self= this;
		self.options.current  = 1;
		self.options.start    = 0;
		self._getActions()
	}			
})
