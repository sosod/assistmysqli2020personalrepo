<?php
$scripts = array("functional_service.js");
include("../header.php");
$funcObj  = new FunctionalService();
$leg     = $funcObj -> getAFunctionalService( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_freq" id="update_freq" value="Update" />
        	<input type="hidden" name="freqid" value="<?php echo $_GET['id']; ?>" id="freqid" />
        </td>
    </tr>
</table>
