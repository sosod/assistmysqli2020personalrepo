<?php 
include("../header.php");
?>
<div class="displayview">
<table border="1" cellpadding="5" cellspacing="0" id="link_to_page">
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu" id="menu" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the Naming convention for compliance module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Legislative Types:</th>
        <td>Configure the Legislative Types</td>
        <td><input type="button" name="legislative_types" id="legislative_types" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Legislative Categories:</th>
        <td>Define Legislative Categories</td>
        <td><input type="button" name="legislative_categories" id="legislative_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Legislation Compliance Frequency:</th>
        <td>Define the Legislation Compliance Frequency</td>
        <td><input type="button" name="legislation_compliance_frequency" id="legislation_compliance_frequency" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <!-- <tr>
    	<th>Legislative Frequency:</th>
        <td>Configure the Legislative Frequency</td>
        <td><input type="button" name="legislative_frequency" id="legislative_frequency" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
     <tr>
    	<th>Legislation Organisation Types:</th>
        <td>Configure the Legislation Organisation Types</td>
        <td><input type="button" name="legislation_organisation_types" id="legislation_organisation_types" value="Configure" class="setup_defaults"  /></td>
    </tr>   
    <tr>
    	<th>Legislation Organisation Size:</th>
        <td>Configure the Legislation Organisation Size</td>
        <td><input type="button" name="organisation_size" id="organisation_size" value="Configure" class="setup_defaults"  /></td>
    </tr> 
     <tr>
    	<th>Legislation Organisation Capacity:</th>
        <td>Configure the Legislation Organisation Capacity</td>
        <td><input type="button" name="organisation_capacity" id="organisation_capacity" value="Configure" class="setup_defaults"  /></td>
    </tr> 
     <tr>
    	<th>Responsible Business Partner:</th>
        <td>Configure the Responsible Business Partner</td>
        <td><input type="button" name="responsible_organisations" id="responsible_organisations" value="Configure" class="setup_defaults"  /></td>
    </tr>     
    <tr>
    	<th>Deliverable Responsible Departments:</th>
        <td>Configure the Deliverable Responsible Departments</td>
        <td><input type="button" name="directorate" id="directorate" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Functional Service:</th>
        <td>Configure the Deliverable Functional Service</td>
        <td><input type="button" name="functional_service" id="functional_service" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Responsible Owner:</th>
        <td>Define Deliverable Responsible Owner</td>
        <td><input type="button" name="responsible_owner" id="responsible_owner" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Accountable Person:</th>
        <td>Define Deliverable Accountable Person</td>
        <td><input type="button" name="accountable_person" id="accountable_person" value="Configure" class="setup_defaults"  /></td>
    </tr>	
     <tr>
    	<th>Deliverable Organisation Types:</th>
        <td>Configure the Deliverable Organisation Types</td>
        <td><input type="button" name="organisation_types" id="organisation_types" value="Configure" class="setup_defaults"  /></td>
    </tr>          
    <tr>
    	<th>Compliance To:</th>
        <td>Configure the Compliance To</td>
        <td><input type="button" name="compliance_to" id="compliance_to" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Events:</th>
        <td>Define the Events</td>
        <td><input type="button" name="events" id="events" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Sub Events:</th>
        <td>Define the Sub Events</td>
        <td><input type="button" name="sub_events" id="sub_events" value="Configure" class="setup_defaults"  /></td>
    </tr> 
    <tr>
    	<th>Action  Owner:</th>
        <td>Define Action Owner</td>
        <td><input type="button" name="action_owner" id="action_owner" value="Configure" class="setup_defaults" /></td>
    </tr>     	
    <tr>
    	<th>Glossary:</th>
        <td>Define Glossary Terms</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>        
    <tr>
    	<th>List Columns:</th>
        <td>Configure which columns dipslay on the Manage and New pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
</table>
</div>
