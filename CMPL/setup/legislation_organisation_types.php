<?php 
$scripts = array("legislation_organisation_types.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
			<table id="organisation_type_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Organisation Type Name</th>        
			        <th>Organisation Type Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_orgtype" id="save_orgtype" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("leg_organisation_types_logs", true)?></td>
	</tr>
</table>
</div>