<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
$dirObj	     = new Directorate();
$directorate = $dirObj -> fetch( $_GET['id'] );
$subdirectorates = $dirObj -> fetchSubDirectorates( $_GET['id'] );

?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="editdirectorate_form">
	<table class="noborder">
	<tr>
		<th>Name:</th>
		<td><input type="text" name="directorate_name_<?php echo $directorate['id']; ?>" id="directorate_name" value="<?php echo $directorate['name']; ?>" size="50" /></td>
	</tr>
	<tr>
		<th valign="top">Sub Directorates:</th>
		<td><table class="noborder">
		<?php 
			if( isset($subdirectorates) AND !empty($subdirectorates) ){
				foreach( $subdirectorates as $index => $sub){
		?>
			<tr>
				<td class="noborder">
				<!--  <input type="hidden" name="sub_<?php echo $sub['id']; ?>" id="subdirectorate" value="<?php echo $sub['subid']; ?>" /> -->
				 <input type="text" name="main_<?php echo $sub['id']; ?>" id="subdirectorate_name" value="<?php echo $sub['name']; ?>" size="50" /> 
				</td>
			</tr>
		<?php 
				}
			}
		?>
		</table>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>
			<input type="button" name="update" id="update" value="Update" />
			<input type="button" name="cancel" id="cancel" value="Cancel" />
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>	
	</table>
</form>