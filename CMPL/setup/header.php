<?php
include_once("inc_ignite.php");
ini_set("error_reporting", TRUE);
ini_set("display_errors", TRUE);
$_SESSION['cc'] = "cassist";
$_SESSION['dbref'] = "assist_cassist_cmpl";
$scripts = array_merge( 
						array("jquery-1.4.4.min.js", "jquery-ui-1.8.7.custom.min.js", "json2.js", "default.js"),
						(isset($scripts) ? $scripts : array()) 
					  );
if( isset($scripts) ){
foreach($scripts as $script) {
?>
	<script language="javascript" src="../js/<?php echo $script; ?>"></script>
<?php
	}
}
$styles = array( "jquery-ui-1.7.2.custom.css", "styles.css", "default.css",'jquery/css/blue/jquery-ui.css' );
foreach( $styles as $style) {
?>
 	<link rel="stylesheet" type="text/css" href="../css/<?php echo $style; ?>" />
<?php
}
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	}
}
$getBasename = preg_split('[\\/]', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY);
$controller  = $getBasename[2];
echo "Controller => ".$controller."";
//$pageTrace   = array( $getBasename[1] => $controller);
$ctrl 		 = new Menu();
$submenu     = $ctrl -> getSubMenu( $controller );
?>
<link rel="stylesheet" href="../../assist.css" type="text/css" />
<div style="clear:both;">
    <?php
	if(!empty($submenu)){
		foreach( $submenu as $key => $menu ) {
	//echo $page_name." is active ".(($page_name ==  strtolower(($link['client_name'] == "" ? $link['name'] : $link['client_name'])) )? "Y" : "")."<br /><br />";
			$mainMenu[$menu['id']] = array("id" => $menu['id'], "url" => strtolower(trim($menu['name'])).".php",'active'=> ($menuitems['client_terminology'] ? "Y" : ""),'display'=> $menu['client_terminology']) ;
		}
		echoNavigation(1,$mainMenu); 
	//$menuT[1] = array('id'=>1,'url'=>"",'active'=> "Y",'display'=>"Display Mine");
	//$menuT[2] = array('id'=>2,'url'=>"",'active'=> "",'display'=>"Display All");
	//echoNavigation(2,$menuT); 
	}
    ?>
    <h3 style="text-align:left; clear:left;">
   	<?php
	$otherNames = array(
			"edit_finyear" 				=> "Edit Financial Year",
			"edit_cstatus" 				=> "Edit Contract Status",
			"change_cstatus" 			=> "Change Contract Status",
			"edit_dstatus"	 			=> "Edit Deliverable Status",
			"change_dstatus" 			=> "Change Deliverable Status",
			"edit_dstatus"	 			=> "Edit Deliverable Status",	
			"edit_astatus"	 			=> "Edit Action Status",
			"change_astatus" 			=> "Change Action Status",
			"edit_tstatus"   			=> "Edit Task Status",
			"edit_ctype"	 			=> "Edit Contract Type",
			"change_ctype"   			=> "Change Contract Type",		
			"change_categorystatus" 	=> "Change Category Status",
			"edit_assfreq" 				=> "Edit Assessment Frequency",
			"change_assfreq" 			=> "Change Assessment Frequency",
			"edit_suppliercategory" 	=> "Edit Supplier Category",
			"change_suppliercategory" 	=> "Change Supplier Category",		
			"change_supplier"		  	=> "Change Supplier Status",
			"edit_dweight"			  	=> "Edit Delivered Weight",
			"change_dweight" 		  	=> "Change Delivered Weight",
			"edit_oweight"			  	=> "Edit Other Weight",
			"change_oweight" 		  	=> "Change Other Weight",
			"edit_qweight"			  	=> "Edit Quality Weight",
			"change_qweight" 		  	=> "Change Quality Weight",
			"edit_smatrix"			  	=> "Edit Delivered Score Matrix",						
			"change_smatrix"		  	=> "Change Delivered Score Matrix Status",						
			"edit_oscore"			  	=> "Edit Other Score Matrix",					
			"change_oscore"		  		=> "Edit Other Score Matrix Status",						
			"edit_qscore"			    => "Edit Quality Score Matrix",						
			"change_qscore"		  		=> "Change Quality Score Matrix Status",	
			"myprofile"			  		=> "My Profile",
			"edit_accountable"    		=> "Edit Accountable Person",
			"change_accountable_status" => "Change Accountable Person Status",
			"sel_compliace_frequency"	=> "Compliance Frequency",
			"sel_applicableorg"			=> "Deliverable Organisation Types",
			"sel_respdept"				=> "Responsible Departments",
			"sel_funcservice"			=> "Functional Services",
			"sel_accountable_person"	=> "Accountable Person",
			"sel_responsiblity_owner"	=> "Responsible Owners",
			"sel_compliance"			=> "Compliance To",
			"sel_mainevent"				=> "Main Event",
			"sel_subevent"				=> "Sub Event",
			"changelegtype_status"		=> "Change Legislative Type Status"	,
			"edit_legislativetypes"		=> "Edit Legislative Types",
			"changelegcategory_status"	=> "Change Legislation Category Status",
			"edit_legislativecategories"=> "Edit Legislative Categories",
			"edit_orgsize"				=> "Edit Organisation Size",
			"edit_orgcapacity"			=> "Edit Organisation Capacity",
			"changeorgcapacity_status"	=> "Change Organisation Capacity Status",
			"responsible_organisations" => "Responsible Business Patner",
			"edit_organisation"			=> "Edit Responsible Business Patner",
			"change_organisation"		=> "Change Responsible Business Patner Status",
			"change_subevent"			=> "Change Sub Event Status",
			"change_actionowner_status" => "Change Action Owner Status"														
		);
	$count = count($getBasename);

	foreach($getBasename as $key => $value){
		if(strpos($value, ".") > 0){
			$page = substr( $value, 0 ,strpos($value, ".") );
			if( isset($otherNames[$page])  ){
				echo ucwords( $getBasename[$key-1] )." >> ".$otherNames[$page];
				
			} else {
				if( strpos($page, "_") > 0){
					$page = str_replace("_"," ", $page);
				}
				echo  ucwords( $getBasename[$key-1] )." >> ".ucwords( $page );
			}
		} else if($value == "glossary"){
			echo  ucwords($getBasename[$count-1]);
		} else if($value == "search"){
			echo  ucwords($value);
		}
	}
	
	//$nameHeading = $ctr -> getMenuName( $); 
	?>
    </h3>
</div>
