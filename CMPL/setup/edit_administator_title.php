<?php 
$scripts = array("directorate.js");
include("../header.php");
$dirObj 		 = new Directorate();
$subdirectorates = $dirObj -> getSubDirectorate($_GET['id']);
$actionownerObj  = new ActionOwner();
$actionowners 	 = $actionownerObj -> getActionOwners();
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Sub Directorate:</th>
        <td>
        	<select id="sub_directorate" name="sub_directorate">
        	<option value="">--select sub-directorate--</option>
        	<?php 
        		if( isset($subdirectorates) && !empty($subdirectorates)){
        			foreach( $subdirectorates as $index => $dir){
        	?>
        		<option value="<?php echo $dir['id']; ?>"><?php echo $dir['dirtxt']; ?></option>
        	<?php 
        			}
        		}
        	?>
        	</select>
        </td>
    </tr>
    <tr>  
    	<th>Action Owner Title:</th>
        <td>
        	<select id="action_owner_title" name="action_owner_title">
        	<option value="">--select owner title--</option>
        	<?php 
        		if( isset($actionowners) && !empty($actionowners)){
        			foreach( $actionowners as $index => $owner){
			?>        			
        		<option value="<?php echo $owner['id']; ?>"><?php echo $owner['name']; ?></option>
        	<?php
        			} 		
        		}
        	?>
        	</select>
        </td>      
    </tr>
    <tr>    
        <th></th>
        <td>
        	<input type="button" name="assign_owner" id="assign_owner" value="Assign" />
        	<input type="hidden" name="directorate" value="<?php echo $_GET['id']; ?>" id="directorate" />        
        </td>        
    </tr>      
    <tr>    
        <td><?php displayGoBack("", ""); ?></td>
        <td>
        </td>        
    </tr>    
</table>
</div>