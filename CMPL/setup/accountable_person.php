<?php
$scripts = array("accountable_person.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="accountable_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Accountable Person Name</th>        
			        <th>Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td>
			    	   <input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save" id="save" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("accountable_person_logs", true); ?></td>
	</tr>
</table>

