<?php 
$scripts = array("legislation_organisation_types.js");
include("../header.php");
$legObj  = new LegOrganisationTypes();
$leg     = $legObj -> getALegOrganisationType( $_GET['id'] );   
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_orgtype" id="update_orgtype" value="Update" />
        	<input type="hidden" name="orgtypeid" value="<?php echo $_GET['id']; ?>" id="orgtypeid" />
        </td>        
    </tr>    
</table>
