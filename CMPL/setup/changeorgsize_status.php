<?php 
$scripts = array("organisation_size.js");
include("../header.php");
$orgsizeObj = new OrganisationSize();
$orgsize  	= $orgsizeObj -> getAOrganisationSize( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgsize['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($orgsize['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($orgsize['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_orgsize" id="update_orgsize" value="Update" />
        	<input type="hidden" name="orgsizeid" value="<?php echo $_GET['id']; ?>" id="orgsizeid" />
        </td>        
    </tr>    
</table>
