<?php 
$scripts = array("frequency.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
  <tr>
    <td colspan="2" class="noborder">
		<form action="">
		<table id="frequency_table">
			<tr>
		    	<th>Ref</th>       
		    	<th>Frequency Name</th>        
		        <th>Frequency </th>
		        <th></th>
		        <th></th>        
		        <th></th>        
		    </tr>
			<tr>
		    	<td>#</td>
		    	<td><input type="text" name="name" id="name" value="" /></td>        
		        <td><input type="text" id="description" name="description" value="" class="datepicker" /></td>
		        <td><input type="button" name="save_freq" id="save_freq" value="Add" /></td>
		        <td></td> 
		        <td></td>                
		    </tr>    
		</table>
		</form>    
    </td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("", ""); ?></td>
    <td class="noborder"><?php displayAuditLogLink("frequencies_logs", true)?></td>
  </tr>
</table>
