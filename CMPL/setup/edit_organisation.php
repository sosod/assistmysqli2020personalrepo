<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:20 AM
 * To change this template use File | Settings | File Templates.
 */
 $scripts = array("responsible_organisations.js");
include("../header.php");
$legObj  = new ResponsibleOrganisation();
$leg     = $legObj -> getAResponsibleOrganisation( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Organisation Type Name:</th>
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>
    </tr>
    <tr>
        <th>Organisation Type Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_org" id="edit_org" value="Edit" />
        	<input type="hidden" name="orgid" value="<?php echo $_GET['id']; ?>" id="orgid" />
        </td>
    </tr>
</table>

