<?php 
$scripts = array("legislativecategories.js");
include("../header.php");
$legObj  = new LegislativeCategories();
$leg     = $legObj -> getALegislativeCategory( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_cat" id="update_cat" value="Update" />
        	<input type="hidden" name="legcatid" value="<?php echo $_GET['id']; ?>" id="legcatid" />
        </td>        
    </tr>    
</table>
