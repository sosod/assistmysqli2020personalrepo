<?php 
$scripts = array("legislative_frequency.js");
include("../header.php");
$legObj  = new LegislativeFrequency();
$leg     = $legObj -> getALegislativeFrequency( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Legislative Frequency Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Legislative Frequency Description:</th>
        <td><input type="text" id="description" name="description" value="<?php echo $leg['description']; ?>" class="datepicker" /></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_legfreq" id="edit_legfreq" value="Edit" />
        	<input type="hidden" name="legfreqid" value="<?php echo $_GET['id']; ?>" id="legfreqid" />
        </td>        
    </tr>    
</table>
