<?php 
$scripts = array("legislativetypes.js");
include("../header.php");
$legObj  = new LegislativeTypes();
$leg     = $legObj -> getALegislativeType( $_GET['id'] );  
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_leg" id="update_leg" value="Update" />
        	<input type="hidden" name="legactid" value="<?php echo $_GET['id']; ?>" id="legactid" />
        </td>        
    </tr>    
</table>
</div>
