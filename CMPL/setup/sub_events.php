<?php 
$scripts = array("sub_event.js");
include("../header.php");
$eventObj  = new Events();
$events     = $eventObj -> getEvents();  
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="">
			<table id="event_table">
				<tr>
			    	<th>Ref #</th>       
			    	<th>Sub Event Name</th>        
			        <th>Sub Event Description </th>
			        <th>Main Event</th>        
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td>
			        	<select id="main_event" name="main_event">
			            	<option>--main event--</option>
			                <?php
								foreach( $events as $key => $event){
							?>
			                <option value="<?php echo $event['id']; ?>"><?php echo $event['name']; ?></option>
			                <?php
							}
							?>
			            </select>
			        </td>        
			        <td><input type="button" name="save_event" id="save_event" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("sub_event_logs", true)?></td>		
	</tr>
</table>
</div>
