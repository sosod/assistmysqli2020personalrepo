<?php 
$scripts = array("sub_event.js");
include("../header.php");
$eventObj  = new SubEvent();
$event     = $eventObj -> getAEvent( $_GET['id'] ); 
$eventsObj  = new Events();
$events     = $eventsObj -> getEvents();   
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $event['id']; ?></td>
    </tr>
    <tr>  
    	<th>Sub Event Name:</th>  
        <td><textarea name="name" id="name"><?php echo $event['name']; ?></textarea></td>      
    </tr>
    <tr>    
        <th>Sub Event Description:</th>
        <td><textarea id="description" name="description"><?php echo $event['description']; ?></textarea></td>
    </tr>
    <tr>    
        <th>Main Event:</th>
        <td>
        	<select id="main_event" name="main_event">
            	<option>--main event--</option>
                <?php
					foreach( $events as $key => $e){
				?>
                <option value="<?php echo $e['id']; ?>" <?php if($event['main_event'] == $e['id']) {?> selected <?php } ?>><?php echo $e['name']; ?></option>
                <?php
				 }
				?>
            </select>
        </td>
    </tr>    
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_event" id="edit_event" value="Edit" />
        	<input type="hidden" name="eventid" value="<?php echo $_GET['id']; ?>" id="eventid" />
        </td>        
    </tr>    
</table>
</div>