<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
$complianceObj  = new ComplianceFrequency();
$complianceFreq = $complianceObj -> getComplianceFrequency( $_REQUEST['id'] ); 
?>
<?php JSdisplayResultObj(""); ?>
<table id='edit_compliance_frequency_table'>
	<tr>
		<th>Ref #:</th>
		<td><?php echo $_REQUEST['id']; ?></td>
	</tr>
	<tr>
		<th>Name:</th>
		<td><input type='text' name='name' id='name' value='<?php echo $complianceFreq['name']; ?>' /></td>
	</tr>
	<tr>
		<th>Description:</th>
		<td><textarea name='description' id='description'><?php echo $complianceFreq['description']; ?></textarea></td>	
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder">
			<input type='hidden' name='complianceId' id='complianceId' value='<?php echo $_REQUEST['id']; ?>' />
			<input type='submit' id='update' value='Update' name='update' /></td>		
	</tr>
</table>