<?php 
$scripts = array("organisation_capacity.js");
include("../header.php");
$orgcapacityObj = new OrganisationCapacity();
$orgcapacity  	= $orgcapacityObj -> getAOrganisationCapacity( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgcapacity['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($orgcapacity['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($orgcapacity['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_orgcapacity" id="update_orgcapacity" value="Update" />
        	<input type="hidden" name="orgcapacityid" value="<?php echo $_GET['id']; ?>" id="orgcapacityid" />
        </td>        
    </tr>    
</table>
