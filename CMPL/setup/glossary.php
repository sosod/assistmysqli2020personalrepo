<?php
$scripts = array("glossary.js");
require_once("../header.php");
?>
<div id="displayview" style="clear:both;">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form name="form-glossary" id="form-glossary">
			<table id="table_glossary">
			    <tr>
			        <th>Ref</th>    
			        <th>Category</th>
			        <th>Terminology</th>
			        <th>Explanation</th>
			        <th></th>
			        <th></th>             
			    </tr>
			    <tr>
			        <td>#</td>    
			        <td><input type="text" name="category" id="category" value="" /></td>
			        <td><textarea name="terminology" id="terminology"></textarea></td>
			        <td><textarea name="explanation" id="explanation"></textarea></td>
			        <td><input type="submit" name="add" id="add" value="Add" /></td>
			        <td></td>                
			    </tr>        
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("glossary_logs", true)?></td>
	</tr>
</table>
</div>