<?php 
$scripts = array("legislativetypes.js");
include("../header.php");
$legObj  = new LegislativeTypes();
$leg     = $legObj -> getALegislativeType( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Legislative Type Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Legislative Type Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_leg" id="edit_leg" value="Edit" />
        	<input type="hidden" name="legid" value="<?php echo $_GET['id']; ?>" id="legid" />
        </td>        
    </tr>    
</table>
