<?php 
$scripts = array("legislativecategories.js");
include("../header.php");
$legObj  = new LegislativeCategories();
$leg     = $legObj -> getALegislativeCategory( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Legislative Category Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Legislative Category Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_legcat" id="edit_legcat" value="Edit" />
        	<input type="hidden" name="legcatid" value="<?php echo $_GET['id']; ?>" id="legcatid" />
        </td>        
    </tr>    
</table>
