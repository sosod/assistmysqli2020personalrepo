<?php 
$scripts = array("legislativetypes.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="">		
			<table id="legislative_type_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Legislative Type Name</th>        
			        <th>Legislative Type Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_leg" id="save_leg" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("legislative_types_logs", true)?></td>		
	</tr>
</table>	
</div>
