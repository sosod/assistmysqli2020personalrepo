<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
$complianceObj  = new ComplianceFrequency();
$complianceFreq = $complianceObj -> getComplianceFrequency( $_REQUEST['id'] ); 
?>
<?php JSdisplayResultObj(""); ?>
<table id='change_compliance_frequency_table'>
	<tr>
		<th>Ref #:</th>
		<td><?php echo  $_REQUEST['id']; ?></td>
	</tr>
	<tr>
		<th>Status:</th>
		<td>
			<select id='status' name='status'>
				<option value='1' <?php if(($complianceFreq['status'] & 1) == 1) { ?> selected='selected' <?php }?> >Active</option>
				<option value='0'<?php if(($complianceFreq['status'] & 1) == 0) { ?> selected='selected' <?php }?>>Inactive</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder">
			<input type='hidden' name='complianceId' id='complianceId' value='<?php echo $_REQUEST['id']; ?>' />
			<input type='submit' id='update_status' value='Update Status' name='update_status' />
		</td>		
	</tr>
</table>