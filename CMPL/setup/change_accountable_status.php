<?php
$scripts = array("accountable_person.js");
include("../header.php");
$actObj  = new AccountablePerson();
$act     = $actObj -> getAccountablePerson( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $act['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($act['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($act['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="actid" value="<?php echo $_GET['id']; ?>" id="actid" />
        </td>
    </tr>
</table>
