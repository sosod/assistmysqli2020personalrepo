<?php
$scripts = array( 'deliverable_columns.js' );
$styles = array();
$page_title = "ListColums";
require_once("../header.php");
$columns = array("1" => array
        (
            "id"  	  => 1,
            "url" 	  => "columns.php",
            "active"  => "", 
            "display" => "Legislation Columns"
        ),
    "2" => array
        (
            "id" 	  => 2,
            "url" 	  => "deliverable_columns.php",
            "active"  => "Y",
            "display" => "Deliverable Columns"
        ),
    "3" => array
        (
            "id" 	  => 3,
            "url" 	  => "action_columns.php",
            "active"  => "",
            "display" => "Action Columns"
        )       
);
echoNavigation(2,$columns); 
?>
<?php JSdisplayResultObj(""); ?>
<div id="columns"></div>
<div><?php displayGoBack("", ""); ?></div>