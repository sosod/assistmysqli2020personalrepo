<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);

$var = $_REQUEST;

$sql = "SELECT * FROM ".$dbref."_dir WHERE active = true ORDER BY dirsort";
include("inc_db_con.php");
$dir = array();
while($row = mysql_fetch_assoc($rs)) {
	$dir[] = $row;
}
mysql_close($con);

?>
<script type=text/javascript>
function Validate(me) {
	return true;
}
$(function(){
	$( "#sortable" ).sortable({
		placeholder: "ui-state-highlightsort"
	});
	$( "#sortable" ).disableSelection();
});
</script>
<h3>Order Directorates</h3>
<form name=edit method=post action=setup_dir.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=ORDER />
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th valign=top width=100>Directorates:</th>
			<td valign=top><ul id=sortable>
			<?php
				foreach($dir as $d) {
					echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$d['dirid']."\">".$d['dirtxt']."</li>";
				}
			?>
			</ul></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top><input type=submit value="Save Changes" /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
