<?php
$scripts = array("responsible_owner.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
  <tr>
    <td class="noborder" colspan="2">
		<table id="responsible_owner_table">
			<tr>
		    	<th>Ref #</th>
		    	<th>Responsible Owner Name</th>        
		        <th>Description</th>
		        <th></th>
		        <th></th>        
		        <th></th>        
		    </tr>
			<tr>
		    	<td>#</td>
		    	<td><input type="text" name="name" id="name" value="" /></td>        
		        <td><textarea id="description" name="description"></textarea></td>
		        <td><input type="button" name="save" id="save" value="Add" /></td>
		        <td></td> 
		        <td></td>                
		    </tr>    
		</table>    
    </td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("", ""); ?></td>
    <td class="noborder"><?php displayAuditLogLink("responsible_owner_logs", true)?></td>
  </tr>
</table>
