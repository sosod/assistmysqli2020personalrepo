<?php 
$scripts = array('user_business_partner.js');
include("../header.php");
$userbusinessObj = new ResponsibleOrganisation();
$business        = $userbusinessObj -> getResponsibleOrganisations();
$userbusiness    = $userbusinessObj -> getUserReponsibleOrg();
$u = array();
foreach( $userbusiness as $index => $v)
{
	$u[] = $v['business_partner_id'];
}
?>
<?php JSdisplayResultObj(""); ?>
<h4>Allocate  users to business patners</h4>
<table width="50%" id="table_business_partner" class="noborder">
	<tr>
		<th>Ref</th>
		<th>Businesss Patners</th>
		<th>Users</th>
		<th></th>
	</tr>
	<tr>
		<td>#</td>
		<td>
			<select id="business_patners" name="business_patners">		
		<?php 
			foreach( $business as $i => $val)
			{
			?>
			<option
			<?php 
				if(in_array($val['id'], $u))
				{
					?>
						disabled="disabled"
					<?php 		
				}			
			?>
			value="<?php echo $val['id']; ?>"
			><?php echo $val['name']; ?></option>		
			<?php 
			}		
		?>
			</select>
		</td>
		<td>
			<select id="users" name="users" multiple="multiple">
			</select>
		</td>
		<td>
			<input type="button" name="assign" id="assign" value=" Assign " /> 
		</td>
	</tr>
</table>