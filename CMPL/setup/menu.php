<?php 
$scripts = array("menu.js");
include("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<table id="menu_table">
			    <tr>
			        <th>Ref #</th>
			        <th>Ignite Terminology</th>            
			        <th>Client Terminology</th>                        
			        <th></th>                                    
			    </tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("menu_logs", true)?></td>		
	</tr>
</table>
</div>

