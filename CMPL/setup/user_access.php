<?php
$scripts = array("useraccess.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form>
<table id="table_useraccess">
	<tr>
    	<td>Ref</td>
    	<td>User</td>
    	<td>Directorate</td>
    	<td>Sub-directorate</td>
    	<td>Module Admin</td>                                
    	<td>Contract Manager</td>                           
    	<td>Create Contracts</td>                           
    	<td>Create Deliverable</td>                
    	<td>Approval</td>
    	<td>View All</td>                        
    	<td>Edit All</td>        
    	<td>Reports</td>        
    	<td>Assurance</td>        
    	<td>Setup</td>        
    	<td></td>        
    </tr>
	<tr>
    	<td>#</td>
    	<td>
       	<select id="users" name="users">
        	<option value="">-users-</option>
        </select>
        </td>
    	<td>
       	<select id="directorate" name="directorate">
        	<option value="">-directorates-</option>
        </select>        
        </td>
    	<td>
       	<select id="sub_directorate" name="sub_directorate">
        	<option value="">-subdirectorate-</option>
        </select>        
        </td>
    	<td>
       	<select id="module_admin" name="module_admin">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>                
        </td>                                
    	<td>
       	<select id="contract_manager" name="contract_manager">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>                      
        </td>                           
    	<td>
       	<select id="create_contract" name="create_contract">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>                    
        </td>                           
    	<td>
       	<select id="create_deliverable" name="create_deliverable">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>          
        </td>                
    	<td>
       	<select id="approval" name="approval">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>                  
        </td>
    	<td>
       	<select id="viewall" name="viewall">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>          
        </td>                        
    	<td>
       	<select id="editall" name="editall">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>         
        </td>        
    	<td>
       	<select id="reports" name="reports">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>            
        </td>        
    	<td>
       	<select id="assurance" name="assurance">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>         
        </td>        
    	<td>
       	<select id="setup" name="setup">
        	<option value="1">Yes</option>
        	<option value="0">No</option>                        
        </select>          
        </td>        
    	<td><input type="submit" name="add" id="add" value="Add" /></td>        
    </tr>    
</table>
<div><?php displayGoBack("", ""); ?></div>
</form>
</div>
