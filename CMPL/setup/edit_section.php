<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 1:37 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("sections.js");
include("../header.php");
$legObj  = new LegislationSection();
$leg     = $legObj -> getALegislationSection( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Section Name:</th>
        <td><input type="text" name="section_name" id="section_name" value="<?php echo $leg['name']; ?>" /></td>
    </tr>
    <tr>
    	<th>Section Number:</th>
        <td><input type="text" name="section_number" id="section_number" value="<?php echo $leg['section_number']; ?>" /></td>
    </tr>
    <tr>
        <th>Description of Section:</th>
        <td><textarea id="section_description" name="section_description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="sectionid" value="<?php echo $_GET['id']; ?>" id="sectionid" />
        </td>
    </tr>
</table>