<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 1:46 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("sections.js");
include("../header.php");
$legObj  = new LegislationSection();
$leg     = $legObj -> getALegislationSection( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($leg['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($leg['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_act" id="update_act" value="Update" />
        	<input type="hidden" name="sectionid" value="<?php echo $_GET['id']; ?>" id="sectionid" />
        </td>
    </tr>
</table>
