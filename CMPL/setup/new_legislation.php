<?php 
$scripts = array("legislation.js");
include("../header.php");
?>
<div class="message" id="message"></div>
<table>
	<tr>
    	<th>Name Of Legislation</th>    
    	<td><input type="text" id="name" name="name" value="" /></td>
    </tr>
	<tr>
    	<th>Legislation Reference</th>    
    	<td><input type="text" name="reference" id="reference" value="" /></td>
    </tr>    
	<tr>
    	<th>Type of Legislation</th>    
    	<td>
        	<select name="type" id="type">
            	<option>--type--</option>
            </select>
        </td>
    </tr>    
	<tr>
    	<th>Legislation Category</th>    
    	<td>
        	<select name="category" id="category"multiple="multiple">
            	<option>--category--</option>
            </select>        
			<br /><i><small>Use CTRL key to select multiple options</small></i>
        </td>
    </tr>    
	<tr>
    	<th>Organisation Size</th>    
    	<td>
        	<select name="organisation_size" id="organisation_size">
            	<option>--organisation size--</option>
            </select>        
        </td>
    </tr>    
	<tr>
    	<th>Legislation Date</th>    
    	<td> 
          <input type="text" name="legislation_date" id="legislation_date" value="" class="datepicker" />
        </td>
    </tr>        
	<tr>
    	<th>Legislation Number</th>    
    	<td>
          <input type="text" name="legislation_number" id="legislation_number" value="" />
        </td>
    </tr>            
	<tr>
    	<th>Organisation Type</th>    
    	<td>
        	<select name="organisation_type" id="organisation_type">
            	<option>--organisation type--</option>
            </select>        
        </td>
    </tr>   
	<tr>
    	<th>Organisation Capacity Level</th>    
    	<td>
        	<select name="organisation_capacity" id="organisation_capacity">
            	<option>--organisation capacity--</option>
            </select>        
        </td>
    </tr>
	<tr>
    	<th>Responsible Organisation</th>
    	<td>
        	<select name="responsible_organisation" id="responsible_organisation">
            	<option>--responsible organisation--</option>
            </select>
        </td>
    </tr>      
	<tr>
    	<th>Hyperlink to act</th>    
    	<td>
          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="" />
        </td>
    </tr> 
	<tr>
    	<td></td>    
    	<td>
          <input type="button" name="save" id="save" value=" Save " class="isubmit" />
        </td>
    </tr>                    
</table>