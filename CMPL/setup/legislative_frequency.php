<?php 
$scripts = array("legislative_frequency.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="" method="post">
			<table id="legislative_frequency_table">
				<tr>
			    	<th>Ref #</th>       
			    	<th>Legislative Frequency Name</th>        
			        <th>Frequency </th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td>
			    		<textarea name="name" id="name"></textarea>
			    	</td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_legfreq" id="save_legfreq" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("legislative_frequency_logs", true)?></td>
	</tr>
</table>
</div>
