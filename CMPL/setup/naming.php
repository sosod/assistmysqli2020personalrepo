<?php 
$scripts = array("naming.js");
include("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="">
			<table id="naming_table">
			    <tr>
			        <th>Ref #</th>
			        <th>Ignite Terminology</th>            
			        <th>Client Terminology</th>                        
			        <th></th>                                    
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("header_names_logs", true)?></td>
	</tr>
</table>
</div>