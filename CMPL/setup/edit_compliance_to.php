<?php 
$scripts = array("compliance_to.js");
include("../header.php");
$legObj  = new ComplianceTo();
$leg     = $legObj -> getAComplianceTo( $_GET['id'] );
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Compliance Given To Name:</th>
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Compliance Given To Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_freq" id="edit_freq" value="Edit" />
        	<input type="hidden" name="freqid" value="<?php echo $_GET['id']; ?>" id="freqid" />
        </td>        
    </tr>    
</table>
</div>