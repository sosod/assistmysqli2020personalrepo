<?php 
$scripts = array("organisation_capacity.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="">
			<table id="organisation_capacity_table">
				<tr>
			    	<th>Ref</th>      
			    	<th>Organisation Capacity Name</th>        
			        <th>Organisation Capacity Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>               
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_orgcapacity" id="save_orgcapacity" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>		
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("organisation_capacity_logs", true)?></td>
	</tr>
</table>
</div>
