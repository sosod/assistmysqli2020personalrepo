<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		require_once("../../../library/dbconnect/".strtolower($classname).".php");
	}
}
class SetupController extends DBConnect{

	function __construct(){
	
	}
	
	function getMenu()
	{
		$menu 		= new Menu();
		$menuItems  = $menu->getMenu();
		echo json_encode( $menuItems );
	}
	
	function updateMenu()
	{
		$id      	 = $_POST['id'];
		$updateData  = array("client_terminology" => $_POST['client_terminology']);
		$menu 	 	 = new Menu();		
		$result  	 = $menu -> updateMenu( $id, $updateData);
		echo $this -> _updateCheck( $result, "menu name");
	}	

	function getNaming()
	{
		$naming 	 = new Naming();
		$namingItems = $naming -> getNaming();
		echo json_encode( $namingItems );
	}

	function updateNaming()
	{
		$id      	 = $_POST['id'];
		$updateData  = array("client_terminology" => $_POST['client_terminology']);
		$naming	 	 = new Naming();		
		$result  	 = $naming -> updateNaming( $id, $updateData);
		echo $this -> _updateCheck( $result, " naming");
	}

	//================================================================================================================

	function saveAccountablePerson()
	{
		$act 		= new AccountablePerson();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $act -> save( $insertdata);
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Accountable person saved", "id" => $res , "error" => false );
		} else {
			$response = array("text" => "Error saving the accountable person", "error" => true);
		}
		echo json_encode( $response );
	}

	function getAccountablePerson()
	{
		$actObj = new AccountablePerson();
		$acts 	= $actObj -> getAccountablePersons();
		echo json_encode( $acts );
	}

	function updateAccountable()
	{
		$act 		= new AccountablePerson();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $act->getAccountablePerson($_POST['id']);
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status'];
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status'];
			}
		}
		$res 	  = $act -> updateAccountablePerson( $_POST['id'], $updatedata);
		$response = array();
		if( $res == 1)
		{
			$response = array("text" => "Accountable Person updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the  Accountable Person", "error" => false );
		} else {
			$response = array("text" => "Error updating the Accountable Person", "error" => true);
		}
		echo json_encode( $response );
	}

	//================================================================================================================
    function saveResponsibleOwner()
    {
        $act 		= new ResponsibleOwner();
        $insertdata = array();
        foreach( $_POST['data'] as $key => $value)
        {
            $insertdata[$key] = $value;
        }
        $insertdata['insertuser'] = $_SESSION['tid'];
        $res 	  = $act -> save( $insertdata);
        $response = array();
        if( $res > 0)
        {
            $response = array("text" => "Responsible owner saved", "id" => $res , "error" => false );
        } else {
            $response = array("text" => "Error saving the responsible owner", "error" => true);
        }
        echo json_encode( $response );
    }

    function getResponsibleOwner()
    {
        $actObj = new ResponsibleOwner();
        $acts 	= $actObj -> getResponsibleOwners();
        echo json_encode( $acts );
    }

    function updateResponsibleOwner()
    {
        $act 		= new ResponsibleOwner();
        $updatedata = array();
        $id 		= 0;
        if( isset($_POST['data'])){
            foreach( $_POST['data'] as $key => $value)
            {
                $updatedata[$key] = $value;
            }
        }
        if( isset($_POST['status'])){
            $_act   = $act->getResponsibleOwner($_POST['id']);
            if( $_POST['status'] != 2){
                $updatedata['status'] = $_POST['status'];
            } else {
                $updatedata['status'] = $_act['status'] + $_POST['status'];
            }
        }
        $res 	  = $act -> updateResponsibleOwner( $_POST['id'], $updatedata);
        $response = array();
        if( $res == 1)
        {
            $response = array("text" => "Responsible owner updated", "error" => false );
        } else if($res == 0){
            $response = array("text" => "No change was made to the responsible owner ", "error" => false );
        } else {
            $response = array("text" => "Error updating the responsible owner ", "error" => true);
        }
        echo json_encode( $response );
    }

    //================================================================================================================

	function saveActionOwner()
	{
		$act 		= new ActionOwner();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $act -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Action owner saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the action owner", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function getActionOwners()
	{
		$actObj = new ActionOwner();
		$acts 	= $actObj -> getActionOwners();
		echo json_encode( $acts );		 
	}
	
	function updateActionOwner()
	{
		$act 		= new ActionOwner();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $act->getActionOwner($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $act -> updateActionOwner( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Action owner updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the action owner", "error" => false );
		} else {		
			$response = array("text" => "Error updating the action owner", "error" => true);		
		}
		echo json_encode( $response );
	}    
    
    //================================================================================================================

	function saveAct()
	{
		$act 		= new Acts();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $act -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Act saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the act", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function getActs()
	{
		$actObj = new Acts();
		$acts 	= $actObj -> getActs();
		echo json_encode( $acts );		 
	}
	
	function updateAct()
	{
		$act 		= new Acts();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $act->getAAct($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $act -> updateAct( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Act updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the act", "error" => false );
		} else {		
			$response = array("text" => "Error updating the act", "error" => true);		
		}
		echo json_encode( $response );
	}
	//=====================================================================================================
	
	function getLegislativeTypes()
	{
		$legtypeObj = new LegislativeTypes();
		$legtypes   = $legtypeObj -> getLegislativeTypes();
		echo json_encode( $legtypes ); 
	}
	
	function saveLegislativeType()
	{
		$legtypeObj = new LegislativeTypes();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $legtypeObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Legislative type saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the legislative type", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateLegislativeType()
	{
		$legtypeObj = new LegislativeTypes();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $legtypeObj -> getALegislativeType($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $legtypeObj -> updateLegislativeType( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Legislative type updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the legislative type", "error" => false );
		} else {		
			$response = array("text" => "Error updating the legislative type", "error" => true);		
		}
		echo json_encode( $response );
	}
	// ==========================================================================================================
	
	function getLegislativeCategories()
	{
		$legcatObj = new LegislativeCategories();
		$legcats   = $legcatObj -> getLegislativeCategories();
		echo json_encode( $legcats ); 
	}
	
	function saveLegislativeCategory()
	{
		$legcatObj = new LegislativeCategories();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $legcatObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Legislative category saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the legislative category", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateLegislativeCategory()
	{
		$legtypeObj = new LegislativeCategories();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $legtypeObj -> getALegislativeCategory($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $legtypeObj -> updateLegislativeCategories( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Legislative category updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the legislative category", "error" => false );
		} else {		
			$response = array("text" => "Error updating the legislative category", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	//=========================================================================================
	
	function getOrganisationSizes()
	{
		$orgsizeObj = new OrganisationSize();
		$orgsizes   = $orgsizeObj -> getOrganisationSize();
		echo json_encode( $orgsizes ); 
	}
	
	function saveOrganisationSize()
	{
		$orgsizeObj = new OrganisationSize();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $orgsizeObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Legislative category saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the legislative category", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateOrganisationSize()
	{
		$orgsizeObj = new OrganisationSize();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $orgsizeObj -> getAOrganisationSize($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $orgsizeObj -> updateOrganisationSizes( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Legislative category updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the legislative category", "error" => false );
		} else {		
			$response = array("text" => "Error updating the legislative category", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	//====================================================================================================
	
	function getLegislativeFrequency()
	{
		$legfreqObj = new LegislativeFrequency();
		$legfreq    = $legfreqObj -> getLegislativeFrequency();
		echo json_encode( $legfreq ); 
	}
	
	function saveLegislativeFrequency()
	{
		$legfreqObj = new LegislativeFrequency();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $legfreqObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Legislative frequency saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the legislative frequency", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateLegislativeFrequency()
	{
		$legfreqObj = new LegislativeFrequency();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $legfreqObj -> getALegislativeFrequency($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $legfreqObj -> updateLegislativeFrequency( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Legislative frequency updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the legislative frequency", "error" => false );
		} else {		
			$response = array("text" => "Error updating the legislative frequency", "error" => true);		
		}
		echo json_encode( $response );
	}
	 // =======================================================================================================
	
	function getFrequency()
	{
		$freqObj = new Frequency();
		$freq    = $freqObj -> getFrequency();
		echo json_encode( $freq ); 
	}
	
	function saveFrequency()
	{
		$freqObj = new Frequency();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $freqObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Frequency saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the frequency", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateFrequency()
	{
		$freqObj = new Frequency();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $freqObj -> getAFrequency($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $freqObj -> updateFrequency( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Frequency updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the frequency", "error" => false );
		} else {		
			$response = array("text" => "Error updating the  frequency", "error" => true);		
		}
		echo json_encode( $response );
	}
	//====================================================================================================================


	function getFunctionalService()
	{
		$freqObj = new FunctionalService();
		$freq    = $freqObj -> getFunctionalService();
		echo json_encode( $freq );
	}

	function saveFunctionalService()
	{
		$freqObj = new FunctionalService();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $freqObj -> save( $insertdata);
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Functional service saved", "id" => $res , "error" => false );
		} else {
			$response = array("text" => "Error saving the functional service", "error" => true);
		}
		echo json_encode( $response );
	}

	function updateFunctionalService()
	{
		$freqObj = new FunctionalService();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $freqObj -> getAFunctionalService($_POST['id']);
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status'];
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status'];
			}
		}
		$res 	  = $freqObj -> updateFunctionalService( $_POST['id'], $updatedata);
		$response = array();
		if( $res == 1)
		{
			$response = array("text" => "Functional service updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the functional service", "error" => false );
		} else {
			$response = array("text" => "Error updating the  functional service", "error" => true);
		}
		echo json_encode( $response );
	}
	//====================================================================================================================


	function getComplianceTo()
	{
		$freqObj = new ComplianceTo();
		$freq    = $freqObj -> getComplianceTo();
		echo json_encode( $freq );
	}

	function saveComplianceTo()
	{
		$freqObj = new ComplianceTo();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $freqObj -> save( $insertdata);
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Compliance given  saved", "id" => $res , "error" => false );
		} else {
			$response = array("text" => "Error saving the frequency", "error" => true);
		}
		echo json_encode( $response );
	}

	function updateComplianceTo()
	{
		$freqObj = new ComplianceTo();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $freqObj -> getAComplianceTo($_POST['id']);
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status'];
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status'];
			}
		}
		$res 	  = $freqObj -> updateComplianceTo( $_POST['id'], $updatedata);
		$response = array();
		if( $res == 1)
		{
			$response = array("text" => "Compliance To updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the Compliance To", "error" => false );
		} else {
			$response = array("text" => "Error updating the  Compliance To", "error" => true);
		}
		echo json_encode( $response );
	}

    //=====================================================================================================

	function getOrganisationTypes()
	{
		$orgtypeObj = new OrganisationTypes();
		$legtypes   = $orgtypeObj -> getOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function saveOrganisationType()
	{
		$orgtypeObj = new OrganisationTypes();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $orgtypeObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Organisation type saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the organisation type", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateOrganisationType()
	{
		$orgtypeObj = new OrganisationTypes();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $orgtypeObj -> getAOrganisationType($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $orgtypeObj -> updateOrganisationType( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Organisation type updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the organisation type", "error" => false );
		} else {		
			$response = array("text" => "Error updating the organisation type", "error" => true);		
		}
		echo json_encode( $response );
	}
	
    //=====================================================================================================

	function getLegOrganisationTypes()
	{
		$orgtypeObj = new LegOrganisationTypes();
		$legtypes   = $orgtypeObj -> getLegOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function saveLegOrganisationType()
	{
		$orgtypeObj = new LegOrganisationTypes();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $orgtypeObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "Organisation type saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the organisation type", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateLegOrganisationType()
	{
		$orgtypeObj = new LegOrganisationTypes();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $orgtypeObj -> getALegOrganisationType($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $orgtypeObj -> updateLegOrganisationType( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "Organisation type updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the organisation type", "error" => false );
		} else {		
			$response = array("text" => "Error updating the organisation type", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	//==============================================================================================================
	

	function getOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$legtypes   = $orgtypeObj -> getOrganisationCapacity();
		echo json_encode( $legtypes ); 
	}
	
	function saveOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $orgtypeObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "organisation capacity saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the organisation capacity", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $orgtypeObj -> getAOrganisationCapacity($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $orgtypeObj -> updateOrganisationCapacity( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "organisation capacity updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the organisation capacity", "error" => false );
		} else {		
			$response = array("text" => "Error updating the organisation capacity", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	//==============================================================================================================================	
	

	function getEvents()
	{
		$eventObj = new Events();
		$legtypes   = $eventObj -> getEvents();
		echo json_encode( $legtypes ); 
	}
	
	function saveEvents()
	{
		$eventObj = new Events();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $eventObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "event saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the  event", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateEvents()
	{
		$eventObj = new Events();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $eventObj -> getAEvent($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $eventObj -> updateEvent( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "event updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the  event", "error" => false );
		} else {		
			$response = array("text" => "Error updating the event", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	// ====================================================================================================	
	

	function getSubEvents()
	{
		$eventObj = new SubEvent();
		$subevents   = $eventObj -> getSubEvents();
		echo json_encode( $subevents ); 
	}
	
	function saveSubEvent()
	{
		$eventObj = new SubEvent();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $eventObj -> save( $insertdata);
		$response = array(); 
		if( $res > 0)
		{
			$response = array("text" => "event saved", "id" => $res , "error" => false );
		} else {		
			$response = array("text" => "Error saving the  event", "error" => true);		
		}
		echo json_encode( $response );
	}
	
	function updateSubEvent()
	{
		$eventObj = new SubEvent();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $eventObj -> getAEvent($_POST['id']); 
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status']; 
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status']; 
			}
		}
		$res 	  = $eventObj -> updateEvent( $_POST['id'], $updatedata);
		$response = array(); 
		if( $res == 1)
		{
			$response = array("text" => "event updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the  event", "error" => false );
		} else {		
			$response = array("text" => "Error updating the event", "error" => true);		
		}
		echo json_encode( $response );
	}
  // ======================================get directorates ===========================================
  
	function getDirectorate()
	{
		$dirObj = new Directorate();
		$directorates = $dirObj -> getDirectorates();
		$dirData  = array();
		foreach( $directorates as $index => $dir)
		{
			$sub = $dirObj -> getSubDirectorate( $dir['id'] );
			$directorates[$index]['dirid'] = $dir['id'];	
			$directorates[$index]['sub'] = $sub;
		}		
		echo json_encode( $directorates );		
	}
	
	
	function editDirectorate()
	{
		$dirObj  = new Directorate(); 
		$mainDir = array();
		$subDir  = array();
		$res 	 = 0;
		$result	 = 0;
		foreach($_POST['data'] as $index => $valArr)
		{
			if( strstr($valArr['name'], "directorate_name"))
			{
				$id = str_replace("directorate_name_","", $valArr['name'] );
				$mainDir[$id]	= $valArr['value'];
				$res = $dirObj -> update( "dir", array("name" => $valArr['value']), "id={$id}");			
			} 			
			
			if( strstr($valArr['name'], "main"))
			{
				$id = str_replace("main_","", $valArr['name'] );
				$subDir[$id]	= $valArr['value'];
				$result += $dirObj -> update( "dirsub", array("name" => $valArr['value']), "id={$id}");				
			}
		}
		
		$response = array();
		if( $res < 0 )
		{
			$response = array("text" => "Error updating the directorates", "error" => true );				
		} else if( $res == 0 && $result == 0) {
			$response = array("text" => "No change was made to the directorates", "error" => false );
		} else {
			$response = array("text" => "Directorates updated successfully", "error" => false );		
		}
		echo json_encode( $response );	
	}
	
	function assignOwner()
	{
		$response = array();
		$dirObj  = new Directorate();
		$assignedsubOwners = $dirObj->getSubOwner( $_POST['sub'] );
		$alreadyAssign = FALSE;
		if( !empty($assignedsubOwners))
		{
			foreach( $assignedsubOwners as $index => $valArr)
			{
				if($_POST['owner'] == $valArr['actionowner_id'])
				{
					$alreadyAssign = TRUE;
				} 				
			}
		}
		if(!$alreadyAssign)
		{
			$res     = $dirObj -> insert("dir_admins", array("actionowner_id" => $_POST['owner'], "type" => "SUB" , "subdir_id" => $_POST['sub']) );
			$changesData['user'] = $_SESSION['tkn'];
			$changesData['message'] =  "Assigned action owner title Ref # ".$_POST['owner']." to sub-directorate Ref #".$_POST['sub'];
			$insertdata = array("changes" => base64_encode( serialize( $changesData ) ), "setup_id" => $_POST['sub'] , "insertuser" => $_SESSION['tid'] );
	        
			$chRes = $dirObj->insert("dir_logs", $insertdata );
	        //echo "Change res is => ".$chRes."<br />";	        
			if( $res < 0)
			{							
				$response = array("text" => "There was an error saving the directorate owners" , "error" => true );			
			} else {				
				$response = array("text" => "Assignment of action owner successfully saved", "error" => false );
			}						
		} else {
			$response = array("text" => "The action owner title has already been assigned to this sub directorate" , "error" => true );			
		}
		echo json_encode( $response );
	}
	
	function saveDirectorate()
	{
		$dirObj = new Directorate();
		$_resId = 0;
		$resId  = 0;
		foreach( $_POST['data'] as $index => $data)
		{
			
			if( strstr($data['name'], "directorate_name")){
				$resId = $dirObj -> insert("dir", array("name" => $data['value'], "status" => 1) );
				$id = $dirObj -> insertedId();										
			}
			
			if( strstr($data['name'], "sub")){
				if( $data['name'] == "primary_sub" && $data['value'] != "") {
					$_resId += $dirObj -> insert("dirsub", array("name" => $data['value'], "dirid" => $id, "status" => "5", "insertuser" => $_SESSION['tid']));
				} else {
					if( $data['value'] != "")
					{
						$_resId += $dirObj -> insert("dirsub", array("name" => $data['value'], "dirid" => $id, "status" => "1", "insertuser" => $_SESSION['tid']));						
					}					
				} 				
			}
		}
		$response = array();
		if( $resId < 0 )
		{
			$response = array("text" => "Error saving the directorates", "error" => true );				
		} else {
			$response = array("text" => "Directorates saved successfully", "error" => false );		
		}
		echo json_encode( $response );	
	}
	
	
  // ==================================================================================================
	function getUnUsedUsers()
	{
		$userObj = new UserAccess();
		$users   = $userObj -> getUsers();
		$respObj = new ResponsibleOrganisation();
		$usedUsers  = $respObj->getBussinessPartners();

		$unUsed = array();
		$used = "";
		foreach($usedUsers as $index => $bPartner)
		{
			$used  .= $bPartner['user_id'].",";
		}
		$usedIds = array();
		//if(!empty($used))
		//{
			$usedIds = explode(",", rtrim($used, ","));	
		//}
		foreach($users as $i => $user)
		{
			if(!in_array($user['tkid'], $usedIds))
			{
				$unUsed[] = $user;
			} 
		}
		echo json_encode( $unUsed );		
	}
	
	
	function getUsers()
	{
		$userObj = new UserAccess();
		$users   = $userObj -> getUsers();
		echo json_encode( $users );		
	}
	
	function deleteUserBusinessPartner()
	{
		$data 	  = $_POST['data'];
		$eventObj = new ResponsibleOrganisation();
		$res      = $eventObj -> updateUserBusinessPartner( array("status" => $data['status']) , $data['id'] );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Users business partner updated", "error" => false);
		} else {
			$response = array("text" => "Error updating business partner", "error" => true);			
		}
		echo json_encode( $response );	
	}
	
	function saveUserBusinessPartner()
	{
		$busObj = new ResponsibleOrganisation();
		$userBusinesPartner = $busObj -> getBusinessPartnetById($_POST['data']['id']);
		$data 	  = array();
		if(!empty($userBusinesPartner))
		{
			$usersSel = "";
			$dbUsers  = "";
			$dbUsers  = $userBusinesPartner['user_id']; 
			$usersSel = implode(",", $_POST['data']['user_id']);
			$_users   = $usersSel.",".$dbUsers; 
			$data  = array("business_partner_id" => $_POST['data']['id'], "user_id" => $_users );
			$res   = $busObj -> updateUserBusinessPartner( $data , $userBusinesPartner['id']);
		} else {
			$data  = array("business_partner_id" => $_POST['data']['id'], "user_id" => implode(",", $_POST['data']['user_id']) );
			$res   = $busObj -> saveUserBusinessPartner( $data );	
		}
		
		$response = array();
		if( $res > 0)
		{
			$userObj = new UserAccess();
			$users = $userObj -> getUsersByIds( $data['user_id'] );
			$userStr = "";
			foreach( $users as $i => $user)
			{
				$userStr .= $user['user'].",";
			}
			$response = array("text" => "User business partner save", "error" => false, "id" => $res, "users" => rtrim($userStr, ","));
		} else {
			$response = array("text" => "Error saving the business partner", "error" => true);			
		}
		echo json_encode( $response );
	}
	
	function updateUserBusinessPartner()
	{
		$eventObj = new ResponsibleOrganisation();
		$data 	  = array();
		//$data     = array("user_id" => implode(",", $_POST['data']['user_id']),  "business_partner_id" => $_POST['data']['business_partner_id'], "insertuser" => $_SESSION['tid'] );
		$data  = array("user_id" => implode(",", $_POST['data']['user_id']) );
		$res   = $eventObj -> updateUserBusinessPartner( $data , $_POST['data']['id']);
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "User business partner updated", "error" => false);
		} else {
			$response = array("text" => "Error updating the business partner", "error" => true);			
		}
		echo json_encode( $response );
	}
	
	function getUserBusinessPartner()
	{
		$userObj = new UserAccess();
		$users   = $userObj -> getUsers();		
		$respObj = new ResponsibleOrganisation();
		$results = $respObj -> getUserReponsibleOrg();
		$userbusinessPartners = array();
		foreach ( $results as $index => $userBus)
		{
			$userids = array();
			$userids = explode(",", $userBus['user_id']);
			$u		 = "";
			$userbusinessPartners[$userBus['id']] = array(
														 "id"			=> $userBus['id'],
														 "organisation" => $userBus['name']
													);
			foreach( $users as $k => $valArr)
			{
				if( in_array($valArr['tkid'], $userids))
				{
					$u .= $valArr['tkname'].",";
					//$userbusinessPartners[$userBus['id']]['user'] = $valArr['tkname'];							
				}
			}
			$u  = rtrim($u, ",");
			$userbusinessPartners[$userBus['id']]['user'] = $u;
		}
		
		echo json_encode( $userbusinessPartners );
	}
	
	function getResponsibleOrganisations()
	{
		$eventObj = new ResponsibleOrganisation();
		$subevents   = $eventObj -> getResponsibleOrganisations();
		echo json_encode( $subevents );
	}

	function saveResponsibleOrganisation()
	{
		$eventObj = new ResponsibleOrganisation();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;
		}
		$insertdata['insertuser'] = $_SESSION['tid'];
		$res 	  = $eventObj -> save( $insertdata);
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "organisation saved", "id" => $res , "error" => false );
		} else {
			$response = array("text" => "Error saving the organisation", "error" => true);
		}
		echo json_encode( $response );
	}

	function updateResponsibleOrganisation()
	{
		$eventObj = new ResponsibleOrganisation();
		$updatedata = array();
		$id 		= 0;
		if( isset($_POST['data'])){
			foreach( $_POST['data'] as $key => $value)
			{
				$updatedata[$key] = $value;
			}
		}
		if( isset($_POST['status'])){
			$_act   = $eventObj -> getAResponsibleOrganisation($_POST['id']);
			if( $_POST['status'] != 2){
				$updatedata['status'] = $_POST['status'];
			} else {
				$updatedata['status'] = $_act['status'] + $_POST['status'];
			}
		}
		$res 	  = $eventObj -> updateResponsibleOrganisation( $_POST['id'], $updatedata);
		$response = array();
		if( $res == 1)
		{
			$response = array("text" => "organisation updated", "error" => false );
		} else if($res == 0){
			$response = array("text" => "No change was made to the organisation", "error" => false );
		} else {
			$response = array("text" => "Error updating the organisation", "error" => true);
		}
		echo json_encode( $response );
	}

 // ===================================================================================================	
		
	function getGlossaries()
	{
		$obj = new Glossary();
		echo json_encode( $obj -> getGlossaries() );
	}
	
	function saveGlossary()
	{
		$obj = new Glossary();
		$insertdata = array();
		foreach( $_POST as $key => $value )
		{
			$insertdata[$key] = mysql_real_escape_string( $value );
		}
		if(( $res = $obj -> save( $insertdata )) >  0){
			$response = array("error"=> false, "text" => "Glossary saved", "id" => $res );
		} else {
			$response = array("error" => true, "text" => "Error saving glossary");
		}
		echo json_encode( $response );	
	}

	function updateGlossary()
	{
		$obj 		= new Glossary();
		$insertdata = array();
		$id 		= 0;
		foreach( $_POST as $key => $value )
		{
			if( $key == "id"){
				$id = $value;
			} else {
				$updatedata[$key] = mysql_real_escape_string( $value );
			}
		}
		$rs = $obj -> updateGlossary( $id, $updatedata );
		$response = array();
		if($rs == 1){
			$response = json_encode( array("error"=> false, "text" => "Updated glossary") );
		} else {
			$response = json_encode( array("error" => true, "text" => "Error updating glossary") );
		}
		echo $response;
	}	
	//==================================================================================
	
	
	function getColumns(){
		$obj  = new Naming();
		$data = $obj -> getColumns(); 
		$columns = array();

		foreach( $data as $key => $value ){
			if($value['active'] == 1){
				$columns['active'][$key] = $value;
			} else {
				$columns['inactive'][$key] = $value;	
			}
		}		
		echo json_encode($columns);
	}
	
	function saveColumnOrder(){
		$obj  = new Naming();
		parse_str( $_POST['active'], $active ); 
		parse_str( $_POST['inactive'], $inactive ); 						
		$obj -> saveColumnsOrder( $active['column'], $inactive['column']);
	}
	// ===========================================================================================================

    function getMainSections()
    {
		$sectionObj = new LegislationSection();
        $mainSections = $sectionObj -> getMainSections();
        echo json_encode($mainSections);
    }

    function getSections()
    {
		$sectionObj = new LegislationSection();
        $sections = $sectionObj -> getLegislationSections();
        echo json_encode($sections);
    }

	function saveLegislationSection(){
		$sectionObj = new LegislationSection();
		$insertdata = array();
        $response   = array();
        $section    = array();
		foreach( $_POST['data']  as $key => $value){
			if( $key == "type"){
				if( $value == 1){
					//main
                    $insertdata['name']                =  $_POST['data']['name'];
                    $insertdata['section_number']      =  $_POST['data']['number'];
                    $insertdata['description']       =  $_POST['data']['description'];
                    $section[$_POST['data']['number']] = array( "name" => $_POST['data']['name'], "description" => $_POST['data']['description'] );
				} else {
					//sub section
                    $insertdata['name']              =  $_POST['data']['name'];
                    $insertdata['parent_id']         =  $_POST['data']['main'];
                    $insertdata['section_number']    =  $_POST['data']['main'].".".$_POST['data']['number'];
                    $insertdata['description']       =  $_POST['data']['description'];
					$section[$_POST['data']['main']][$_POST['data']['number']] = array( "name" => $_POST['data']['name'], "description" => $_POST['data']['description']  );
				}
			}
		}
        $insertdata['section'] = serialize($section);
		$res = $sectionObj->save($insertdata);
        if( $res > 0){
            $response = array("text" => "Section saved", "id" => $res, "error" => false);
        } else {
            $response = array("text" => "Error saving the section", "error" => true);
        }
        echo json_encode($response);
	}

    function updateSection(){
		$sectionObj = new LegislationSection();
		$insertdata = array();
        $response   = array();
		foreach( $_POST['data']  as $key => $value){
		    //sub section
            $insertdata['name']              =  $_POST['data']['name'];
            $insertdata['section_number']    =  $_POST['data']['number'];
            $insertdata['description']       =  $_POST['data']['description'];
        }
        $res = $sectionObj -> updateLegislationSection($_POST['id'], $insertdata);
        if( $res > 0){
            $response = array("text" => "Section updated", "id" => $res, "error" => false);
        } else {
            $response = array("text" => "Error updating the section", "error" => true);
        }
        echo json_encode($response);
	}
	
	function saveComplianceFrequency()
	{
		$complianceObj = new ComplianceFrequency();
		$res 		   = $complianceObj -> save($_POST);
		$response 	   = array(); 
		if( $res > 0 )
		{
			$response = array("text" => "Compliance saved successfully", "error" => false, "id" => $res);
		} else {
			$response = array("text" => "Error saving compliance", "error" => true);		
		}
		echo json_encode( $response );
	}
		
	function getFrequencies()
	{
		$complianceObj = new ComplianceFrequency();
		$data = $complianceObj -> getComplianceFrequencies();
		echo json_encode( $data );
	}	
	
	function updateComplianceFrequency()
	{
		$complianceObj = new ComplianceFrequency();
		$id   = $_POST['id'];
		$complianceFreq = $complianceObj -> getComplianceFrequency( $id ); 
		
		$data = $_POST['dat'];
		if(isset($data['status'])){
			if($data['status'] == 1){
				$data['status'] = ($complianceFreq['status'] + $data['status']);
			} else {
				$data['status'] =  $data['status']	;
			}
		}
		$res  = $complianceObj->updateCompliance( $data, $id);
		echo $this->_updateCheck($res, "compliance frequency");
	}
	
	
	//-------------------------- get deliverable columns ------------------------
	
	function getDeliverableColumns()
	{
		$naming = new DeliverableColumns();
		$headers = $naming -> getNaming();
		echo json_encode( $headers );
	}
	
	function getLegislationColumns()
	{
		$naming = new LegislationColumns();
		$headers = $naming -> getNaming();
		echo json_encode( $headers );
	}
	
	function getActionColumns()
	{
		$naming = new ActionColumns();
		$headers = $naming -> getNaming();
		echo json_encode( $headers );
	}

	function updateColumns()
	{
		$naming = new DeliverableColumns();
		$newarray   = array();
		$managearray = array();
		$positionsarray = array();
		foreach( $_POST['data'] as $index => $valueArr)
		{
			if( $valueArr['name'] == "manage")
			{
				array_push($managearray, $valueArr['value']);				
			} 
			if( $valueArr['name'] == "new") {
				array_push( $newarray , $valueArr['value']);						
			}			
			if( strstr($valueArr['name'], "position"))
			{
				array_push($positionsarray, $valueArr['value']);
			}
		}

		$res = 0;
		foreach( $positionsarray as $index => $id)
		{
			$updatedata = array();
			if(in_array($id, $managearray))
			{
				$updatedata = array("active" => Naming::MANAGECOLUMNS, "position" => $index);
			} else {
				$updatedata = array("active" => 0, "position" => $index);
			}
			
			
			if(in_array($id, $newarray))
			{
				$updatedata = array("active" => $updatedata['active'] + Naming::NEWCOLUMNS, "position" => $index);				
			} else {
				$updatedata = array("active" => ($updatedata['active'] + 0), "position" => $index);
			}
			$res = $naming -> update("header_names", $updatedata, "id={$id}");
		}
		$response = array();
		if( $res >= 0)
		{			
			$response = array("text" => "Columns updated", "error" => false);	
		}  else {
			$response = array("text" => "Error updating columns", "error" => true);
		}
		echo json_encode( $response );
	}
	
	function getSubOwner()
	{
		$dir = new Directorate();
		echo json_encode( $dir -> getSubOwner( $_GET['subid'] ) );		
	}
	
	// ------------------- columns -------------------------------------
	
	//checks the staus of the update made to the database and returns the appropriate message
	//@return json encoded string 
	function _updateCheck( $res , $context)
	{
		if( $res == 1){
			$response = array("error" => false, "text" => "Update successfull");
		} else if( $res == 0){
			$response = array("error" => true, "text" => "No change was made");
		} else{
			$response = array("error" => true, "text" => "Error updating ".$context." ");
		}
		return json_encode( $response );
	}
	
	function view_log()
	{
        $logObj  = new Logs("","","");
        $logs    = $logObj -> viewLogs( $_POST['table_name'] );
        echo json_encode($logs);	
	}
}
$method = $_GET['action'];
$setup = new SetupController();
if( method_exists($setup, $method )){
	$setup -> $method();
}

?>
