<?php 
$scripts = array("organisation_capacity.js");
include("../header.php");
$orgcapacityObj = new OrganisationCapacity();
$orgcapacity  	= $orgcapacityObj -> getAOrganisationCapacity( $_GET['id'] );  
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgcapacity['id']; ?></td>
    </tr>   
    <tr>  
    	<th>Organisation Capacity Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $orgcapacity['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Organisation Capacity Description:</th>
        <td><textarea id="description" name="description"><?php echo $orgcapacity['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_orgcapacity" id="edit_orgcapacity" value="Edit" />
        	<input type="hidden" name="orgcapacityid" value="<?php echo $_GET['id']; ?>" id="orgcapacityid" />
        </td>        
    </tr>    
</table>
</div>
