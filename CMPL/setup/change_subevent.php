<?php 
$scripts = array("sub_event.js");
include("../header.php");
$eventObj  = new SubEvent();
$event     = $eventObj -> getAEvent( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $event['id']; ?></td>
    </tr>
    <tr>  
    	<th>Status:</th>  
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($event['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>                
            	<option value="0" <?php if(($event['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>                
            </select>
        </td>      
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update_subevent" id="update_subevent" value="Update" />
        	<input type="hidden" name="eventid" value="<?php echo $_GET['id']; ?>" id="eventid" />
        </td>        
    </tr>    
</table>
