<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/17/11
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("responsible_organisations.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
			<table id="responsile_org_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Responsible Business Patner</th>
			        <th>Description </th>
			        <th></th>
			        <th></th>
			        <th></th>
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>
			        <td><input type="text" id="description" name="description" value="" /></td>
			        <td><input type="button" name="save_org" id="save_org" value="Add" /></td>
			        <td></td>
			        <td></td>
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("organisation_logs", true)?></td>		
	</tr>
</table>
</div>
