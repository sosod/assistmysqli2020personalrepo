<?php 
$scripts = array("legislativecategories.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table id="legislative_category_table">
				<tr>
			    	<th>Ref #</th>
			    	<th>Legislative Categories Name</th>        
			        <th>Legislative Categories Description</th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_legcat" id="save_legcat" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("legislative_categories_logs", true)?></td>
	</tr>
</table>
</div>