<?php 
$scripts = array("compliance_to.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="">
			<table id="compliance_to_table">
				<tr>
			    	<th>Ref #</th>       
			    	<th>Compliance Given To Name</th>
			        <th>Compliance Given To Description </th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_event" id="save_event" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("compliance_to_logs", true)?></td>
	</tr>
</table>
</div>