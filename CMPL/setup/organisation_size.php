<?php 
$scripts = array("organisation_size.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
  <tr>
    <td colspan="2" class="noborder">
		<form method="post">
		<table id="organisation_size_table">
			<tr>
		    	<th>Ref #</th>    
		    	<th>Organisation Size Name</th>        
		        <th>Organisation Size Description</th>
		        <th></th>
		        <th></th>        
		        <th></th>        
		    </tr>
			<tr>
		    	<td>#</td>               
		    	<td><input type="text" name="name" id="name" value="" /></td>        
		        <td><textarea id="description" name="description"></textarea></td>
		        <td><input type="button" name="save_orgsize" id="save_orgsize" value="Add" /></td>
		        <td></td> 
		        <td></td>                
		    </tr>    
		</table>
		</form>    
    </td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("", ""); ?></td>
    <td class="noborder"><?php displayAuditLogLink("organisation_sizes_logs", true)?></td>
  </tr>
</table>
