<?php

function code($str) {
    return htmlentities($str,ENT_QUOTES,"ISO-8859-1");
}

function decode($str) {
    return html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
}

function checkFolder($path) {
	global $cmpcode;
	if(strlen($cmpcode)>0) {
		$me =  $_SERVER['PHP_SELF'];
		$myloc = explode("/",$me);
		if(strlen($myloc[0])==0) { unset($myloc[0]); }
		$mc = count($myloc) - 1;
		$path = "files/".$cmpcode."/".$path;
		$chk = explode("/",$path);
		$chkloc = "";
		for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
		foreach($chk as $c)
		{
			if(strlen($c)>0 && $c != "." && $c != "..") {
				$chkloc.= "/".$c;
				if(!is_dir($chkloc)) { mkdir($chkloc); }
			}
		}
	} else {
		die("An error has occurred.  Please go back and try again.");
	}
}

function saveEcho($folder, $filename, $echo) {
	global $cmpcode;
        //CHECK EXISTANCE OF STORAGE LOCATION
        checkFolder($folder);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$folder."/".$filename;
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
}


function downloadFile($old, $new, $content) {
	header("Content-type: $content");
	header("Content-disposition: attachment; filename=$new");
	readfile($old);
}
function downloadFile2($folder, $old, $new, $content) {
	global $cmpcode;
	header("Content-type: $content");
	header("Content-disposition: attachment; filename=$new");
	readfile("../files/".$cmpcode."/".$folder."/".$old);
}

function displayResult($result) {
    if(count($result)>0) {
        echo("<div class=\"ui-widget\">");
        if($result[0]=="ok") {
            echo "<div class=\"ui-state-ok ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
            echo "<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>";
		} elseif($result[0]=="info") {
            echo "<div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
            echo "<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>";
        } else {
            echo "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
            echo "<p><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span>";
        }
        echo $result[1]."</p></div></div>";
    }
}
	
function JSdisplayResultPrep($txt) {
	echo "<div id=display_result><p><span id=display_result_icon></span><span id=display_result_text>".$txt."</span></p></div></div>
			<script type=text/javascript>
			function JSdisplayResult(result,icon,txt) {
				$(\"#display_result_text\").attr(\"innerText\",\"\");
				$(\"#display_result\").addClass(\"ui-widget ui-state-\"+result);
				$(\"#display_result\").css({\"margin\":\"5px 0px 10px 0px\",\"padding\":\"0 .3em\"});
				$(\"#display_result_icon\").addClass(\"ui-icon\");
				switch(icon) {
					case \"ok\":		$(\"#display_result_icon\").addClass(\"ui-icon-check\"); break;
					case \"info\":	$(\"#display_result_icon\").addClass(\"ui-icon-info\"); break;
					case \"error\":	$(\"#display_result_icon\").addClass(\"ui-icon-closethick\"); break;
					default:		$(\"#display_result_icon\").addClass(\"ui-icon-\"+icon); break;
				}
				$(\"#display_result_icon\").css({\"float\":\"left\",\"margin-right\":\".3em\"});
				$(\"#display_result_text\").attr(\"innerText\",txt);
			}
			</script>";
}


function JSdisplayResultObj( $txt )
{
?>
	<div id="display_objresult">
		<p>
			<span id="display_result_icon"></span>
			<span id="display_result_text"><?php echo $txt; ?></span>
		</p>
	</div>
	<script type="text/javascript">

			function jsDisplayResult( result, icon, text )
			{
				$("#display_result_text").html("");			
				$("#display_objresult").attr("class", "").show();
				$("#display_result_icon").attr("class", "");
				//$("#display_objresult").addClass("message");			
				$("#display_objresult").addClass("ui-widget ui-state-"+result);
				$("#display_objresult").css({"margin":"5px 0px 10px 0px","padding":"0 .3em"});
				$("#display_result_icon").addClass("ui-icon");				
				if(text != "") {
					switch(icon) {
						case "ok":
							$("#display_result_icon").addClass("ui-icon-check"); 
							break;
						case "info":
							$("#display_result_icon").addClass("ui-icon-info"); 
							break;
						case "error":	
							$("#display_result_icon").addClass("ui-icon-closethick"); 
						break;
						default:
							$("#display_result_icon").addClass("ui-icon-"+icon); 
						break;
					}				
					$("#display_result_icon").css({"float":"left","margin-right":".3em"});
					$("#display_result_text").html( text );
					$('html, body').animate({
					    scrollTop: 0,
					    scrollLeft: 50
					}, 800);
				} else {
					window.location.hash = '#display_objresult';
					$("#display_result_text").html( "" )
				}
				
			} 

	</script>
<?php 
}


function displayGoBack($url,$txt) {
	//global $self;
	if(strlen($txt)==0) { $txt = "Go Back"; }
	if(strlen($url)>0) {
		echo "<p><a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></p>";
	} else {
		//echo "<p><a href=# onclick=\"history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=# onclick=\"history.back();\">Go Back</a></p>";
		echo "<p><a href=\"javascript:history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=\"javascript:history.back();\">Go Back</a></p>";
	}
}

function displayGoUp($url, $txt) {
	if(strlen($txt)==0) { $txt = "Back to Top"; }
	if(strlen($url)>0) {
		echo "<p><a href=$url /><img src=\"/pics/tri_up.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></p>";
	}
}

function displayAuditLog($sql,$flds) {
		/* $flds = array('date'=>,'user'=>,'action'=>); */
		$logs = mysql_fetch_all($sql);
		if(count($logs)>0) {
	echo "<table class=noborder width=707><tr><td class=\"noborder\" style=\"background-color: #FFFFFF;\">";
	echo "<span id=disp_audit_log style=\"cursor: hand;\" class=\"float color\"><img src=\"/pics/tri_down.gif\" style=\"vertical-align: middle; border-width: 0px;\"> <span style=\"text-decoration: underline;\">Display Audit Log</span></span>";
	echo "<div id=my_audit_log>";
			echo "<h3 class=log>Audit log</h3>";
			echo "<table width=700>
					<tr>
						<th class=log>Date</th>
						<th class=log>User</th>
						<th class=log>Action</th>
					</tr>";
				foreach($logs as $l) {
					echo "<tr>";
						$a=0;
						foreach($flds as $f) {
							$a++;
							if($a==1) {
								if(!is_numeric($l[$f])) { $d = strtotime($l[$f]); } else { $d = $l[$f]; }
								echo "<td class=\"log centre\">".date("d M Y H:i",$d)."</td>";
							} else {
								echo "<td class=log>".$l[$f]."</td>";
							}
						}
					echo "</tr>";
				}
			echo "</table>";
	echo "</div></td></tr></table>";
	echo "<script type=text/javascript>
			$(function() {
				$(\"#my_audit_log\").hide();
				$(\"#disp_audit_log\").click(function() {
					if($('#my_audit_log').is(':visible')) {
						$(\"#my_audit_log\").hide();
					} else {
						$(\"#my_audit_log\").show();
					}
				});
				$(\"a\").click(function() {
					var u = $(this).attr(\"id\");
					window.open(u, \"_blank\",\"status=no,toolbar=no,location=no,scrollbars=no,directories=no\");
				});
			});
		</script>";
		}
}

function echoNavigation($level,$menu) { 
	/*** variables:
		$level = 1 or 2;
		$menu[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
	***/
	$id = "m".$level;
	switch($level) {
	case 2:
		echo "<div id=m".$level." style=\"padding-top: 5px; font-size: 7pt\">";
		foreach($menu as $m) {
			$key = $m['id'];
			echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
			echo "<label for=\"$key\" style=\"background: #fafaff url();margin-right: 3px;\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
		}
		echo "</div>";
		break;
	case 1:
	default:
		echo "<div id=m".$level." style=\"font-size: 8pt; font-weight: bold;\">";
		foreach($menu as $m) {
			$key = $m['id'];
			echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
			echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
		}
		echo "</div>";
		break;
	}
	echo "<script type=text/javascript>
		  $(function() {
			$(\"#m".$level."\").buttonset();
			$(\"#m".$level." input[type='radio']\").click( function() {
			  document.location.href = $(this).val();
			});
		  });
	</script>";
}	//end drawNav($nav)

	function displayAuditLogLink( $table , $important) {
		echo "<span id=\"_disp_audit_log\" style=\"cursor: hand;\" class=\"float color\"><a href=\"#\" id=\"table_$table\" class=\"audit_log\"><img src=\"/pics/tri_down.gif\" style=\"vertical-align: middle; border-width: 0px;\"><span style=\"text-decoration: underline;\">Display Audit Log</span></a></span>";
		echo "<input type=\"hidden\" name=\"importance\" id=\"importance\" value=\"$important\" />";
		?>
		<script language="javascript">
			$(function(){				
				$(".audit_log").click(function(){
					var id   = this.id;
					var data = {};
					data.field = $(".logid").attr("id");
					data.value = $(".logid").val();
					data.table = this.id;

					$("#"+this.id+" img").attr("src", "/pics/tri_up.gif");
					
					if( $("#aduit_log_"+id).length == 0 || $("#aduit_log_"+id).is(":hidden") ){		
						$("#"+this.id+" img").attr("src", "/pics/tri_up.gif");			
						$("body").append($("<div />",{id:"aduit_log_"+id}).css({"clear":"both"})
								   .append($("<table />",{id:"table_"+id}).css({"display":"block"}))
							)
					} else {
						$("#"+this.id+" img").attr("src", "/pics/tri_down.gif");
						$("#aduit_log_"+id).remove();	
					}					
					$.post("../../library/process_log.php",{ data : data },function( auditlogData ){
						//$("#aduit_log_"+id).html("");
						//$("body").append( $("<div />",{id:"aduit_log_"+id}).css({"clear":"both"})
											//.append($("<table />",{id:"table_"+id}).css({"display":"block"}))
										//)										
						var importance  = $("#importance").val();
						
						$("#table_"+id).append($("<tr />")
							.append($("<th />",{html:"Date"}).addClass((importance == true ? "log"  : "")))
							.append($("<th />",{html:"Audit Log"}).addClass((importance == true ? "log"  : "")))
							.append($("<th />",{html:"Status"}).addClass((importance == true ? "log"  : "")))
						)
						if( $.isEmptyObject(auditlogData) ){
							$("#table_"+id).append($("<tr />")
												.append($("<td />",{colspan:3, html:"No log yet"}))
							)
						} else {
							$.each( auditlogData, function( index, values ){
								$("#table_"+id).append($("<tr />")
									.append($("<td />",{html:values.date}))
									.append($("<td />",{html:values.changes}))
									.append($("<td />",{html:values.key}))		
								)							
							});
						}

					},"json");
					return false;					
				});
			});
		</script>
		<?php
	}
?>