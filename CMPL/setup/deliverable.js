// JavaScript Document
$(function(){
	if( $("#deliverableid").val() == undefined){
	    Deliverable.getUsers();
	    Deliverable.getOrganisationSizes();
	    Deliverable.getOrganisationTypes();
	    Deliverable.getFunctionalService();
	    Deliverable.getMainEvents();
        Deliverable.getDepartments();
		Deliverable.getComplianceFrequency();
	    Deliverable.getSections();
	    Deliverable.ResponsibleOwner();		
	    Deliverable.AccountablePerson();				
    }
	$("#save_deliverable").click(function(){
		Deliverable.save();
		return false;					  
	});

	$("#edit_deliverable").click(function(){
		Deliverable.edit();
		return false;
	});

    $("#main_event").live("change", function(){
	    Deliverable.getSubEvents( $(this).val() );
        return false;
    });

    $("#view_edit_log").live("click",function(){
       Deliverable.viewEditLog();
       return false;
    });

    $("#import").live("click",function(){
       document.location.href = "import_deliverable.php?legislationid="+$("#legislationid").val();;
       return false;
    });

});

Deliverable			= {

	getComplianceFrequency	: function(){
		$.post("newcontroller.php?action=getComplianceFrequency", function( data ){
            $.each( data, function( index, complianceFrequency){
               $("#compliance_frequency").append($("<option />",{text:complianceFrequency.name, value:complianceFrequency.id }))
            });
        },"json");
	} ,
	
	getSections				: function(){
		$.post("newcontroller.php?action=getSections", function( data ){
            $.each( data, function( index, section){
               $("#legislation_section").append($("<option />",{text:section.name+" - "+section.section_number, value:section.id }))
            });
        },"json");
	} ,
	getResponsibleOwner				: function(){
		$.post("newcontroller.php?action=getResponsibleOwner", function( data ){
            $.each( data, function( index, section){
               $("#responsibility_owner").append($("<option />",{text:section.name, value:section.id }))
            });
        },"json");
	} ,
	getAccountablePerson				: function(){
		$.post("newcontroller.php?action=getAccountablePerson", function( data ){
            $.each( data, function( index, section){
               $("#accountable_person").append($("<option />",{text:section.name, value:section.id }))
            });
        },"json");
	} ,	
	getUsers 	: function()
	{
		$.post("newcontroller.php?action=getUsers", function( users ){
			$.each( users, function( index, user){
				$("#user").append($("<option />",{text:user.tkname, value:user.tkid}))		
				
				//$("#accountable_person").append($("<option />",{text:user.tkname, value:user.tkid}))
				
				//$("#responsibility_owner").append($("<option />",{text:user.tkname, value:user.tkid}))

				$("#compliance_to").append($("<option />",{text:user.tkname, value:user.tkid}))
			});
		}, "json");	
	} , 	

    getDepartments:function()
    {
        $.get("newcontroller.php?action=getDirectorate", function( direData ){
        $.each( direData ,function( index, directorate ) {
                $("#responsible").append($("<option />",{text:directorate.dirtxt, value:directorate.subid}));
            })
        }, "json");

    },

	getOrganisationSizes	: function()
	{
		$.post("newcontroller.php?action=getOrganisationSizes", function( org_sizes ){
			$.each( org_sizes, function( index, org_size){
				$("#organisation_size").append($("<option />",{text:org_size.name, value:org_size.id}))					
			});
		}, "json");	
	}, 
	
	getOrganisationTypes	: function()
	{
		$.post("newcontroller.php?action=getOrganisationTypes", function( org_types ){
			$.each( org_types, function( index, org_type ){
				$("#applicable_org_type").append($("<option />",{text:org_type.name, value:org_type.id}))
			});
		}, "json");	
	} ,
	
	getFunctionalService	: function()
	{
		$.post("newcontroller.php?action=getFunctionalService", function( functional_services ){
			$.each( functional_services, function( index, functional_service ){
				$("#functional_service").append($("<option />",{text:functional_service.name, value:functional_service.id}))
			});
		}, "json");	
	} ,

	getMainEvents	: function()
	{
		$.post("newcontroller.php?action=getMainEvents", function( main_events ){
			$.each( main_events, function( index, main_event ){
				$("#main_event").append($("<option />",{text:main_event.name, value:main_event.id}))
			});
		}, "json");
	} ,

	getSubEvents	: function( id )
	{
        $("#sub_event").empty();
        $("#sub_event").append($("<option />",{html:"--sub event--", value:""}));
		$.post("newcontroller.php?action=getSubEvents",{ id : id}, function( sub_events ){
			$.each( sub_events, function( index, sub_event ){
				$("#sub_event").append($("<option />",{text:sub_event.name, value:sub_event.id}))
			});
		}, "json");
	} ,
	
	save					: function()
	{
		var data 				   = {};
        data.legislation           = $("#legislationid").val();
		data.description 		   = $("#description").val();
		data.short_description	   = $("#short_description").val();
		data.legislation_section   = $("#legislation_section").val();
		data.compliance_frequency  = $("#compliance_frequency  :selected").val();
		data.applicable_org_type   = $("#applicable_org_type :selected").val();
		data.compliance_date  	   = $("#compliance_date").val();
		data.functional_service    = $("#functional_service :selected").val();
		data.accountable_person    = $("#accountable_person :selected").val();
		data.responsibility_owner  = $("#responsibility_owner :selected").val();
		data.responsible           = $("#responsible :selected").val();
		data.legislation_deadline  = $("#legislation_deadline").val();
		data.action_deadline       = $("#action_deadline").val();
		data.reminder 	           = $("#reminder").val();
		data.sanction              = $("#sanction").val();
		data.org_kpi_ref           = $("#org_kpi_ref").val();
		data.individual_kpi_ref    = $("#individual_kpi_ref").val();
        data.reference_link        = $("#reference_link").val();
		data.assurance             = $("#assurance").val();
		data.compliance_to         = $("#compliance_to").val();
        data.main_event            = $("#main_event").val();
        data.sub_event             = $("#sub_event").val();
        data.guidance              = $("#guidance").val();

		var valid = Deliverable.isValid( data );
		if( valid == true){

            $("#deliverable_message").show().html( "Saving . . . .   <img src='../images/loaderA32.gif' />" );
			$.post("newcontroller.php?action=saveDeliverable", { data: data}, function( response ){
				$("#deliverable_message").show().html( response.text );
                $.each( data, function( index, value){
                    $("#"+index).val("");
                });
			} ,"json");		

		} else {
			$("#deliverable_message").show().html( valid );
		}
		
	} , 
	
	edit 				: function()
	{
        var data 				   = {};
        data.legislation           = $("#legislationid").val();
        data.description 		   = $("#description").val();
        data.short_description	   = $("#short_description").val();
        data.legislation_section   = $("#legislation_section").val();
        data.compliance_frequency  = $("#compliance_frequency  :selected").val();
        data.applicable_org_type   = $("#applicable_org_type :selected").val();
        data.compliance_date  	   = $("#compliance_date").val();
        data.functional_service    = $("#functional_service :selected").val();
        data.accountable_person    = $("#accountable_person :selected").val();
        data.responsibility_owner  = $("#responsibility_owner :selected").val();
        data.responsible           = $("#responsible :selected").val();
        data.action_deadline       = $("#action_deadline").val();
        data.reminder 	           = $("#reminder").val();
        data.sanction              = $("#sanction").val();
        data.org_kpi_ref           = $("#org_kpi_ref").val();
        data.individual_kpi_ref    = $("#individual_kpi_ref").val();
        data.reference_link        = $("#reference_link").val();
        data.assurance             = $("#assurance").val();
        data.compliance_to         = $("#compliance_to").val();
        data.main_event            = $("#main_event").val();
        data.sub_event             = $("#sub_event").val();
        data.guidance              = $("#guidance").val();

        var valid = Deliverable.isValid( data );
        if( valid == true){
            $("#deliverable_message").show().html( "Updating deliverables . . . . <img src='../images/loaderA32.gif' />" );
            $.post("managecontroller.php?action=editDeliverable", { id:$("#deliverableid").val(),  data: data}, function( response ){
                $("#deliverable_message").show().html( response.text );
            } ,"json");

        } else {
            $("#deliverable_message").show().html( valid );
        }

	} , 

    viewEditLog     : function()
    {
        $.post("managecontroller.php?action=getDeliverableEdits", {id : $("#deliverableid").val()}, function( response ){
            $("#editLog").html("");
            if( $.isEmptyObject(response)){
                $("#editLog").html("There are no edit logs yet")																																																								} else {
                $("#editLog").append($("<table />",{id:"table_editlog"})
                            .append($("<tr />")
                                .append($("<th />",{html:"Date"}))
                                .append($("<th />",{html:"Audit Log"}))
                                .append($("<th />",{html:"Status"}))
                            )
                          )
                $.each( response, function( index, editLog ){

                        $("#table_editlog")
                            .append($("<tr />")
                                .append($("<td />",{html:editLog.date}))
                                .append($("<td />",{html:editLog.changes}))
                                .append($("<td />",{html:editLog.status}))
                            )
                })																																																						}

        }, "json");
    } ,

	update 			: function()
	{
		 
		 
		 
	} ,
	
	empty			: function()
	{
		
		
		
	} , 
	
	isValid			: function( data )
	{

		if( data.description == ""){
			return "Please enter the description ";
		} else if( data.short_description == ""){
			return "Please enter the short description ";
		} // else if( data.legislation_section == ""){
			//return "Please select the legislation section";
		//}
        else if( data.compliance_frequency == ""){
			return "Please select the compliance frequency";
		} else if( data.applicable_org_type == ""){
			return "Please select the applicable organisation type";
		} else if( data.compliance_date == ""){
			return "Please enter the compliance date";
		} else if( data.functional_service == ""){
			return "Please select the functional service";
		} else if( isNaN(data.accountable_person) ){
			return "Please enter valid accountable person";
		} else if( data.responsibility_owner == ""){
			return "Please select the responsibility owner";
		} else if( data.legistation_deadline == ""){
			return "Please select the legislation deadline"
		} else if( data.action_deadline == ""){
			return "Please enter the action deadline";
		} else if( data.sanction == ""){
			return "Please enter the sanction";
		} else if( org_kpi_ref == "" ){
			return "Please enter organisation kpi reference";
		} else if( data.individual_kpi_ref == ""){
			return "Please select the individual kpi reference";
		} else if( data.assurance == ""){
			return "Please enter assurance"
		} else if( data.compliance_to == ""){
			return "Please select compliance given to";
		} else if( main_event == "" ){
			return "Please select main event";
		} else if( data.sub_event == ""){
			return "Please select sub event";
		} else if( data.guidance == ""){
			return "Please enter the guidance";
		} else {
			return true;	
		} 
		
	}

}
