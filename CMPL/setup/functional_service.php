<?php 
$scripts = array("functional_service.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post">
				<table id="functional_service_table">
					<tr>
				    	<th>Ref #</th>       
				    	<th>Functional Service Name</th>
				        <th>Functional Service Description</th>
				        <th></th>
				        <th></th>        
				        <th></th>        
				    </tr>
					<tr>
				    	<td>#</td>
				    	<td><input type="text" name="name" id="name" value="" /></td>        
				        <td><textarea id="description" name="description"></textarea></td>
				        <td><input type="button" name="save_freq" id="save_freq" value="Add" /></td>
				        <td></td> 
				        <td></td>                
				    </tr>    
				</table>			
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("functional_service_logs", true)?></td>
	</tr>
</table>
</div>
