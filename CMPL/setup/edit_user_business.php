<?php 
$scripts = array('user_business_partner.js');
include("../header.php");
$userObj  = new UserAccess();
$users    = $userObj -> getUsers();
$userbusinessObj = new ResponsibleOrganisation();
$business        = $userbusinessObj -> getResponsibleOrganisations();
$userbusiness    = $userbusinessObj -> getResponsibleOrganisationUsers( $_GET['id'] );
$businessusers 	 = explode(",", $userbusiness['user_id']); 
$partner = "";
foreach( $business as $i => $val)
{
	if($userbusiness['business_partner_id'] == $val['id'])
	{
		$partner = $val['name'];		
	}
}
?>
<?php JSdisplayResultObj(""); ?>
<table width="50%" id="table_business_partner" class="noborder">
	<tr>
		<th>Ref</th>
		<td><?php echo $_GET['id']; ?></td>
	</tr>
	<tr>
		<th>Businesss Patners</th>
		<td>
			<?php echo $partner; ?>
		</td>		
	</tr>	
	<tr>
		<th>Users</th>
		<td>
			<select id="users" name="users" multiple="multiple">
				<?php 
					foreach( $users as $index => $valArr)
					{
				?>
					<option
					<?php 
						if(in_array($valArr['tkid'], $businessusers)){
					?>
						selected="selected"
					<?php 
						}
					?>
					 value="<?php echo $valArr['tkid']; ?>"><?php echo $valArr['tkname']; ?></option>
				<?php 						
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>
			<input type="hidden" name="userbussid" value="<?php echo $_GET['id']; ?>" id="userbussid" />
			<input type="button" name="update" id="update" value=" Update " /> 
		</td>
	</tr>
	<tr>
		<td><?php displayGoBack("", ""); ?></td>
		<td></td>
	</tr>
</table>