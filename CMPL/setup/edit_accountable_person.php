<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("accountable_person.js");
include("../header.php");
$actObj  = new AccountablePerson();
$act     = $actObj -> getAccountablePerson( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $act['id']; ?></td>
    </tr>
    <tr>
    	<th>Accountable Person:</th>
        <td><input type="text" name="name" id="name" value="<?php echo $act['name']; ?>" /></td>
    </tr>
    <tr>
        <th>Description:</th>
        <td><textarea id="description" name="description"><?php echo $act['description']; ?></textarea></td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="actid" value="<?php echo $_GET['id']; ?>" id="actid" />
        </td>
    </tr>
</table>
