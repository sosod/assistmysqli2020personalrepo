<?php 
$scripts = array("sections.js");
include("../header.php");
?>
<div>
<div class="message" id="section_message"></div>
<table id="section_table">
	<tr>
        <th>Ref #</th>
    	<th>Section Name</th>
    	<th>Section Number</th>
    	<!-- <th>Type of Section</th> -->
    	<th class="section_type"></th>
        <th>Description of Section</th>
        <th></th>
        <th></th>                        
    </tr>
	<tr>
        <td></td>
    	<td valign="top"><input type="text" name="section_name" id="section_name" value="" /></td>
    	<td valign="top"><input type="text" name="section_number" id="section_number" value="" /></td>
    	<!--<td>
        	<select name="type_of_section" id="type_of_section">
            	<option value="">-- section type--</option>
            	<option value="1">Main Section</option>                
            	<option value="0">Sub Section</option>                                
            </select>
        </td>
        -->
    	<td class="section_type" valign="top">
        	<select name="sections" id="sections">
            	<option value="">-- main section --</option>               
            </select>        	
        </td>
        <td>
        	<textarea name="section_description" id="section_description"></textarea>
        </td>
        <td><input type="button" name="add" id="add" value="Add" /></td>
        <td></td>                        
    </tr>    
</table>
</div>