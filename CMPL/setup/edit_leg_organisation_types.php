<?php 
$scripts = array("legislation_organisation_types.js");
include("../header.php");
$legObj  = new LegOrganisationTypes();
$leg     = $legObj -> getALegOrganisationType( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Organisation Type Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Organisation Type Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_orgtype" id="edit_orgtype" value="Edit" />
        	<input type="hidden" name="orgtypeid" value="<?php echo $_GET['id']; ?>" id="orgtypeid" />
        </td>        
    </tr>    
</table>
