<?php 
$scripts = array("events.js");
include("../header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form action="">
			<table id="event_table">
				<tr>
			    	<th>Ref #</th>       
			    	<th>Event Name</th>        
			        <th>Event Description </th>
			        <th></th>
			        <th></th>        
			        <th></th>        
			    </tr>
				<tr>
			    	<td>#</td>
			    	<td><input type="text" name="name" id="name" value="" /></td>        
			        <td><textarea id="description" name="description"></textarea></td>
			        <td><input type="button" name="save_event" id="save_event" value="Add" /></td>
			        <td></td> 
			        <td></td>                
			    </tr>    
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("events_logs", true)?></td>		
	</tr>
</table>
</div>
