<?php 
$scripts = array("functional_service.js");
include("../header.php");
$funcObj  = new FunctionalService();
$leg     = $funcObj -> getAFunctionalService( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $leg['id']; ?></td>
    </tr>
    <tr>  
    	<th>Functional Service Name:</th>
        <td><input type="text" name="name" id="name" value="<?php echo $leg['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Functional Service Description:</th>
        <td><textarea id="description" name="description"><?php echo $leg['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_freq" id="edit_freq" value="Edit" />
        	<input type="hidden" name="freqid" value="<?php echo $_GET['id']; ?>" id="freqid" />
        </td>        
    </tr>    
</table>
