<?php
$scripts = array( 'action_columns.js' );
$styles = array();
$page_title = "ListColums";
require_once("../header.php");
$columns = array("1" => array
        (
            "id"  	  => 1,
            "url" 	  => "columns.php",
            "active"  => "", 
            "display" => "Legislation Columns"
        ),
    "2" => array
        (
            "id" 	  => 2,
            "url" 	  => "deliverable_columns.php",
            "active"  => "",
            "display" => "Deliverable Columns"
        ),
    "3" => array
        (
            "id" 	  => 3,
            "url" 	  => "action_columns.php",
            "active"  => "Y",
            "display" => "Action Columns"
        )       
);
echoNavigation(2,$columns); 
?>
<?php JSdisplayResultObj(""); ?>
<form action="" id="actioncolumns_form">
<table id="actioncolumns">
	<tr>
		<th>Column Name</th>
		<th>Show On Manage Pages</th>
		<th>Show On New Pages</th>
	</tr>
</table>
</form>
<div><?php displayGoBack("", ""); ?></div>