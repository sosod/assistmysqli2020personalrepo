<?php
$scripts = array("compliance_frequency.js");
include("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form action="" method="post">
			<table id='compliance_frequency_table'>
				<tr>
					<th>Ref</th>
					<th>Name</th>
					<th>Description</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>#</td>
					<td><input type='text' name='name' id='name' value='' /></td>
					<td><textarea name='description' id='description'></textarea></td>
					<td><input type='submit' id='save' value='Add' name='save' /></td>
					<td></td>
					<td></td>		
				</tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("compliance_frequency_logs", true)?></td>
	</tr>
</table>
