<?php 
$scripts = array("organisation_size.js");
include("../header.php");
$orgsizeObj = new OrganisationSize();
$orgsize  	= $orgsizeObj -> getAOrganisationSize( $_GET['id'] );  
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $orgsize['id']; ?></td>
    </tr>  
    <tr>  
    	<th>Organisation Size Name:</th>  
        <td><input type="text" name="name" id="name" value="<?php echo $orgsize['name']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Organisation Size Description:</th>
        <td><textarea id="description" name="description"><?php echo $orgsize['description']; ?></textarea></td>
    </tr>
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit_orgsize" id="edit_orgsize" value="Edit" />
        	<input type="hidden" name="orgsizeid" value="<?php echo $_GET['id']; ?>" id="orgsizeid" />
        </td>        
    </tr>    
</table>
