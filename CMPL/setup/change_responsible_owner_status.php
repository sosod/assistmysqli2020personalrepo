<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("responsible_owner.js");
include("../header.php");
$actObj  = new ResponsibleOwner();
$act     = $actObj -> getResponsibleOwner( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $act['id']; ?></td>
    </tr>
    <tr>
    	<th>Status:</th>
        <td>
        	<select name="status" id="status">
            	<option value="">--status--</option>
            	<option value="1" <?php if(($act['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if(($act['status'] & 1) != 1) { ?> selected <?php } ?> >Inactive</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="update" id="update" value="Update" />
        	<input type="hidden" name="actid" value="<?php echo $_GET['id']; ?>" id="actid" />
        </td>
    </tr>
</table>