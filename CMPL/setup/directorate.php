<?php
$scripts = array( 'directorate.js' );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder"><input type="button" name="add_new" id="add_new" value="Add New" /></td>
		<td class="noborder"></td>
	</tr>
	<tr>
		<td colspan="2" class="noborder">
			<table id="table_directorates">
				<tr>
					<th>Ref</th>
					<th>Name</th>
					<th>Sub Directorates</th>
					<th></th>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("dir_logs", true)?></td>
	</tr>	
</table>
