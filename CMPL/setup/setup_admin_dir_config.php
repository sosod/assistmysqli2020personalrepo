<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);
$result = array();

$var = $_REQUEST;
if(isset($var['act'])) {
	switch($var['act']) {
		case "DEL":
			if(isset($var['a']) && checkIntRef($var['a'])) {
				$aid = $var['a'];
				$sql = "UPDATE ".$dbref."_dir_admins SET active = false WHERE id = $aid";
				include("inc_db_con.php");
					if(mysql_affected_rows() > 0) {
						$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction,tsql) VALUES ($today,'$tkid','$modref','Deleted (sub-)Directorate administrator $aid','".code($sql)."')";
						include("inc_db_con.php");
						$result[0] = "check";
						$result[1] = "Administrator deleted.";
					} else {
						$result[0] = "error";
						$result[1] = "An error occurred.  No change was made.  Please try again.";
					}
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred.  No change was made.  Please try again.";
			}
			break;
		case "ADD":
			$d = $var['d'];
			$usr = $var['atkid'];
			$r = $var['ref'];
			if(!isset($var['atkid']) || $var['atkid']=="X") {
				$result[0] = "error";
				$result[1] = "Please select the administrator's name from the User list.  No change was made.  Please try again.";
			} else {
				if($r=="DIR") {
					$ref = $d;
					$type = "DIR";
				} else {
					$r = explode("_",$r);
					$ref = $r[1];
					$type = "SUB";
				}
				if(!checkIntRef($ref)) {
					$result[0] = "error";
					$result[1] = "Please ensure that you select a valid Directorate / Sub-Directorate from the list available.  No change was made.  Please try again.";
				} else {
					$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE ref = $ref AND type = '$type' AND tkid = '$usr' AND active = true";
					include("inc_db_con.php");
					$mnr = mysql_num_rows($rs);
					mysql_close($con);
					if($mnr > 0) {
						$result[0] = "info";
						$result[1] = "Administrator already exists.  No change was made.";
					} else {
						$sql = "INSERT INTO ".$dbref."_dir_admins (tkid,active,type,ref) VALUES ('$usr',true,'$type',$ref)";
						include("inc_db_con.php");
						if(mysql_affected_rows() > 0) {
							$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction,tsql) VALUES ($today,'$tkid','$modref','Added (sub-)Directorate administrator ".mysql_insert_id()."','".code($sql)."')";
							include("inc_db_con.php");
							$result[0] = "check";
							$result[1] = "New administrator added.";
						} else {
							$result[0] = "error";
							$result[1] = "An error occurred.  No change was made.  Please try again.";
						}
					}
				}
			}
			break;
		default:
			break;
	}
}
$dirid = $_REQUEST['d'];
    $sql = "SELECT * FROM ".$dbref."_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $dir = mysql_fetch_array($rs);
    mysql_close();
	$opt = array();

?>
<script type=text/javascript>
function delDir(i,tk,txt,d) {
    if(escape(i)==i && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you want to delete administrator '"+tk+"' from "+txt+"?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_admin_dir_config.php?act=DEL&a="+i+"&d="+d;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
</script>
<?php
displayResColor($result);
?>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<th width=250>Directorate</th>
		<th >Administrators</th>
	</tr>
		  <?php
            $sql = "SELECT a.id, t.tkname, t.tksurname FROM ".$dbref."_dir_admins a, assist_".$cmpcode."_timekeep t WHERE a.active = true AND a.type = 'DIR' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$dirid;
            include("inc_db_con.php");
                $rnum = mysql_num_rows($rs);
                if($rnum==0) { $rowspan = 1; } else { $rowspan = $rnum; }
                ?>
    <tr>
		<td rowspan=<?php echo($rowspan); ?> ><b><?php echo($dir['dirtxt']); ?></b></td>
		
                <?php
                if(mysql_num_rows($rs)==0)
                {
                    echo "<td height=26><small><i>No Administrators.</i></small></td>";
                }
                else
                {
                    $d=1;
                    while($row = mysql_fetch_array($rs))
                    {
                        if($d>1) { echo("<tr>"); }
                        $d++;
                        $id2 = $row['id'];
                        $val2 = $row['tkname']." ".$row['tksurname'];
                        $delvar = str_replace("'","\'",$val2);
                        $delsub = str_replace("'","\'",$dir['dirtxt']);
                        echo "<td>".$val2."<input type=button value=\" Del \" style=\"float: right; margin-top: -15px; margin-right: 10px;\" onclick=\"delDir(".$id2.",'".$delvar."','".$delsub."',".$dirid.");\"></td>";
                        echo "</tr>";
                    }
                }
            mysql_close();
            ?>
	<tr>
		<th>Sub-Directorates</th>
		<th >Administrators</th>
	</tr>
		  <?php
            $sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = ".$dirid." AND active = true ORDER BY subsort";
            include("inc_db_con.php");
                while($sub = mysql_fetch_array($rs))
                {
					$opt[] = $sub;
                    $subid = $sub['subid'];
            $sql2 = "SELECT a.id, t.tkname, t.tksurname FROM ".$dbref."_dir_admins a, assist_".$cmpcode."_timekeep t WHERE a.active = true AND a.type = 'SUB' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$subid;
            include("inc_db_con2.php");
                $rnum = mysql_num_rows($rs2);
                if($rnum==0) { $rowspan = 1; } else { $rowspan = $rnum; }
                ?>
    <tr>
		<td  rowspan=<?php echo($rowspan); ?> ><b><?php echo($sub['subtxt']); ?></b></td>

                <?php
                if(mysql_num_rows($rs2)==0)
                {
                    echo "<td height=27><small><i>No Administrators.</i></small></td>";
                }
                else
                {
                    $d=1;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $id2 = $row2['id'];
                        $val2 = $row2['tkname']." ".$row2['tksurname'];
                        if($d>1) { echo("<tr>"); }
                        $d++;
                        $delvar = str_replace("'","\'",$val2);
                        $delsub = str_replace("'","\'",$sub['subtxt']);
                        echo "<td>".$val2."<input type=button value=\" Del \" style=\"float: right; margin-top: -15px; margin-right: 10px;\" onclick=\"delDir(".$id2.",'".$delvar."','".$delsub."',".$dirid.");\"></td>";
                        echo "</tr>";
                    }
                }
            mysql_close($con2);
            }
            mysql_close();
            ?>
</table>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
<h2>Add New</h2>
<form method=post action=setup_admin_dir_config.php >
<input type=hidden name=d value=<?php echo $dirid; ?> />
<input type=hidden name=act value=ADD />
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<th width=150>(Sub-)Directorate:</th>
		<td><select name=ref>
			<option selected value=DIR><?php echo $dir['dirtxt']; ?></option>
			<?php foreach($opt as $sub) {
				echo "<option value=\"SUB_".$sub['subid']."\">".$sub['subtxt']."</option>";
			} ?>
		</select></td>
	</tr>
	<tr>
		<th>User:</th>
		<td><select name=atkid>
			<option selected value=X>--- SELECT ---</option>
			<?php
			$sql = "SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep INNER JOIN assist_".$cmpcode."_menu_modules_users ON tkid = usrtkid AND usrmodref = '$modref' WHERE tkstatus = 1 ORDER BY tkname, tksurname";
			include("inc_db_con.php");
			while($row = mysql_fetch_array($rs)) {
				echo "<option value=".$row['tkid'].">".$row['name']."</option>";
			}
			mysql_close($con);
			?>
		</select></td>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td><input type=submit value=Add /></td>
	</tr>
</table>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</form>

