<?php 
$scripts = array("glossary.js");
include("../header.php");
$eventObj  = new Glossary();
$event     = $eventObj -> getGlossary( $_GET['id'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $event['id']; ?></td>
    </tr>
    <tr>  
    	<th>Glossary Name:</th>  
        <td><input type="text" name="category" id="category" value="<?php echo $event['category']; ?>" /></td>      
    </tr>
    <tr>    
        <th>Glossary Description:</th>
        <td><textarea id="terminology" name="terminology"><?php echo $event['terminology']; ?></textarea></td>
    </tr>
    <tr>    
        <th>Glossary Explanation:</th>
        <td><textarea id="explanation" name="explanation" ><?php echo $event['explanation']; ?></textarea></td>
    </tr>    
    <tr>    
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder">
        	<input type="button" name="edit" id="edit" value="Edit" />
        	<input type="hidden" name="glossaryid" value="<?php echo $_GET['id']; ?>" id="glossaryid" />
        </td>        
    </tr>    
</table>
