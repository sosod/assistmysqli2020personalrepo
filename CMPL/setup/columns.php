<?php
$scripts = array( 'columns.js' );
$styles = array();
$page_title = "ListColums";
require_once("../header.php");
$columns = array("1" => array
        (
            "id"  	  => 1,
            "url" 	  => "columns.php",
            "active"  => "Y", 
            "display" => "Legislation Columns"
        ),
    "2" => array
        (
            "id" 	  => 2,
            "url" 	  => "deliverable_columns.php",
            "active"  => "",
            "display" => "Deliverable Columns"
        ),
    "3" => array
        (
            "id" 	  => 3,
            "url" 	  => "action_columns.php",
            "active"  => "",
            "display" => "Action Columns"
        )       
);
echoNavigation(2,$columns); 
?>
<style>
	#activeColumns, #inactiveColumns
	{
		list-style-type:none;
		margin:0;
		padding:0;
		float:left;
		margin-right:10px;
	}
	#activeColumns li, #inactiveColumns	li
	{
		margin:0 5px 5px 5px;
		padding:5px;
		font-size:1.2em;
		width:120px;
	}
	#columnsContent
	{
		border:1px solid #000000;
		width:500px;
		clear:both;
		position:static;
	}
</style>
<?php JSdisplayResultObj(""); ?>
<form id="legislation_columns_form" action="" method="post">
<table id="legislation_columns">
	<tr>
		<th>Column Name</th>
		<th>Show On Manage Pages</th>
		<th>Show On New Pages</th>
	</tr>
</table>
</form>
<div><?php displayGoBack("", ""); ?></div>