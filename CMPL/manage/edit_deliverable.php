<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("deliverable.js");
include("../header.php");
$deliverableObj  = new Deliverable();
$deliverables    = $deliverableObj -> getADeliverable( $_GET['id'] );
$sectionObj      = new ComplianceFrequency();
$sections        = $sectionObj -> getComplianceFrequencies();
$typeObj         = new OrganisationTypes();
$types           = $typeObj -> sortedOrgTypes();

$accObj          = new AccountablePerson();
$accountables    = $accObj -> getAccountablePersons();
$respoObj		 = new ResponsibleOwner();
$responsibles    = $respoObj -> getResponsibleOwners();
$dirObj          = new Directorate();
$directorates 	 = $dirObj -> getDirectorates();
$subdirectorate  = $dirObj ->_getSubDirectorates();
$dirSubs = array();
foreach($directorates as $i => $directorate)
{
	if($directorate['id'] == 1)
	{
		$dirSubs[$directorate['id']] = array("id" => 1, "name" => $directorate['name']);
	} else {
		foreach($subdirectorate as $s => $subDir)
		{
			if($subDir['dirid'] == $directorate['id'] ) {
				$dirSubs[$subDir['dirid']] = array("id" =>$directorate['id'] , "name" => $directorate['name']." - ".$subDir['name']);
			}
		}
	}
}
$funObj          = new FunctionalService();
$functionalService= $funObj -> getFunctionalService();
$compObj         = new ComplianceTo();
$compliance     = $compObj -> getComplianceTo();
$eventsObj       = new Events();
$events          = $eventsObj -> getEvents();
$seventsObj      = new SubEvent();
$subevents       = $seventsObj -> getSubEvents();
$nObj = new Naming();
$nObj -> setThColuomns();
?>
<?php JSdisplayResultObj(""); ?>
<table width="70%">
    <tr>
        <th><?php echo $nObj->setHeader("deliverable_ref"); ?>#:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
        <th width="30%"><?php echo $nObj->setHeader("short_description_of_legislation"); ?>:<em>*</em></th>
        <td width="70%">
            <textarea name="short_description" id="short_description"><?php echo $deliverables['short_description']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("description_of_legislation"); ?>:</th>
        <td>
            <textarea name="description" id="description"><?php echo $deliverables['description']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislation_section"); ?>:<em>*</em></th>
        <td>
			<input type='text' name='legislation_section' id="legislation_section" value="<?php echo $deliverables['legislation_section']; ?>" />
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>:<em>*</em></th>
        <td>
            <select name="compliance_frequency" id="compliance_frequency">
                <option value="">--compliance frequency--</option>
                <?php
                    foreach( $sections as $sec => $section){
                ?>
                    <option value="<?php echo $section['id']; ?>" <?php if($section['id'] == $deliverables['compliance_frequency']){ ?> selected="selected" <?php } ?>><?php echo $section['name']; ?></option>
                <?php
                    }
                ?>
            </select>		
		<!-- <input type="text" name="compliance_frequency" id="compliance_frequency" value="<?php echo $deliverables['compliance_frequency']; ?>" class="datepicker" /> --></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("applicable_organisation_type"); ?>:</th>
        <td>
            <select name="applicable_org_type" id="applicable_org_type">
                <?php                   
                  foreach( $types as $key => $type){
                  
                ?>
                    <option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $deliverables['applicable_org_type']) { ?> selected="selected" <?php } ?>><?php echo $type['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>:<em>*</em></th>
        <td>
 			<div style="padding-top:10px;">
                      <table width="100%">
                        <tr>
                         	<td>Is the deadline date recurring within the financial year:</td>
                         <td>
		                     <select id="recurring" name="recurring">
		                      	<option value="">--recurring--</option>
		                      	<option value="1" <?php echo ($deliverables['recurring'] == 1 ?  "selected='selected'" : ""); ?>>Yes</option>
		                      	<option value="0" <?php echo ($deliverables['recurring'] == 0 ?  "selected='selected'" : ""); ?>>No</option>
		                      </select>
	                      </td>
                       </tr>
                      <tr id="days" class="recurr nonfixed" style="display:<?php echo ($deliverables['recurring'] == 1 ? "table-row" : "none" ); ?>;">
                      	<td colspan="2"><input type="text" name="recurring_period" id="recurring_period" value="<?php echo $deliverables['recurring_period']; ?>" />
                      		<select name="period" id="period">
								<option value="days" <?php echo ($deliverables['days_options'] == "days" ?  "selected='selected'" : ""); ?> >Days</option>
								<option value="weekly" <?php echo ($deliverables['days_options'] == "weekly" ?  "selected='selected'" : ""); ?> >Working Days</option>
                      		</select>
                      	</td>
                      </tr>	                      	
                    <tr class="recurr" style="display:<?php echo ($deliverables['recurring'] == 1 ? "table-row" : "none" ); ?>;; text-align:center;">
                    <td colspan="2" align="center">
                    	Days From : 
                    	<select name="recurring_type" id="recurring_type">
                    		<option value="">-- select -- </option>
                    		<option value="fixed" <?php echo ($deliverables['recurring_type'] == "fixed" ?  "selected='selected'" : ""); ?> class="fixedoption" >Fixed</option>
                    		<option value="start" <?php echo ($deliverables['recurring_type'] == "start" ?  "selected='selected'" : ""); ?> class="nonfixed">Start Of Financial Year</option>
                    		<option value="end" <?php echo ($deliverables['recurring_type'] == "end" ?  "selected='selected'" : ""); ?> class="nonfixed">End Of Financial Year</option>
                    		<option value="startofmonth"  <?php echo ($deliverables['recurring_type'] == "startofmonth" ?  "selected='selected'" : ""); ?> class="nonfixed">Start Of Month</option>
                    		<option value="endofmonth" <?php echo ($deliverables['recurring_type'] == "endofmonth" ?  "selected='selected'" : ""); ?> class="nonfixed">End Of Month</option>
                    		<option value="startofquarter" <?php echo ($deliverables['recurring_type'] == "startofquarter" ?  "selected='selected'" : ""); ?> class="nonfixed">Start Of Quarter</option>
                    		<option value="endofquarter" <?php echo ($deliverables['recurring_type'] == "endofquarter" ?  "selected='selected'" : ""); ?> class="nonfixed">End Of Quarter</option>
                    		<option value="startofsemester" <?php echo ($deliverables['recurring_type'] == "startofsemester" ?  "selected='selected'" : ""); ?> class="nonfixed">Start Of Semester</option>
                    		<option value="endofsemester" <?php echo ($deliverables['recurring_type'] == "endofsemester" ?  "selected='selected'" : ""); ?> class="nonfixed">End Of Semester</option>
                    		<option value="startofweek" <?php echo ($deliverables['recurring_type'] == "startofweek" ?  "selected='selected'" : ""); ?> class="nonfixed">Start Of Week</option>
                    		<option value="endofweek" <?php echo ($deliverables['recurring_type'] == "endofweek" ?  "selected='selected'" : ""); ?> class="nonfixed">End Of Week</option>
                    	</select>
                    </td>
                </tr>
                <tr class="fixed" style="display:<?php echo ($deliverables['recurring'] == 0 ? "table-row" : "none" ); ?>;;">
	                <td colspan="2">
	                   Fixed  <input type="text" name="compliance_date" id="compliance_date" value="<?php echo $deliverables['compliance_date']; ?>" class="datepicker" readonly="readonly"  />
	                </td>
                </tr> 											
            	</table>
   			</div>        
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("responsible_department"); ?>:</th>
        <td>
            <select name="responsible" id="responsible">
                <option value="">--responsible department--</option>
                <?php
                    foreach( $dirSubs as $key => $direc){
                ?>
                    <option value="<?php echo $direc['id']; ?>" <?php if($direc['id'] == $deliverables['responsible']){ ?> selected="selected" <?php } ?>><?php echo $direc['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("functional_service"); ?>:</th>
        <td>
            <select name="functional_service" id="functional_service">
                <option  value="">--functional service--</option>
                <?php
                    foreach( $functionalService as $key => $functional){
                ?>
                    <option value="<?php echo $functional['id']; ?>" <?php if($functional['id'] == $deliverables['functional_service']){ ?> selected="selected" <?php } ?>><?php echo $functional['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("accountable_person"); ?>:*</th>
        <td>
            <select name="accountable_person" id="accountable_person" multiple="multiple">
                <?php
					     $accPerson = explode(",", $deliverables['accountable_person']);
                    foreach( $accountables as $key => $acc){
                ?>
                    <option value="<?php echo $acc['id']; ?>" <?php if(in_array($acc['id'] ,$accPerson)){ ?> selected="selected" <?php } ?>><?php echo $acc['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("responsibility_ower"); ?>:</th>
        <td>
            <select name="responsibility_owner" id="responsibility_owner">
                <option value="">--responsibility person--</option>
                <?php
                    foreach( $responsibles as $key => $user){
                ?>
                    <option value="<?php echo $user['id']; ?>" <?php if($user['id'] == $deliverables['responsibility_owner']){ ?> selected="selected" <?php } ?>><?php echo $user['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?>:</th>
        <td>
          <input type="text" name="legislation_deadline" id="legislation_deadline" value="<?php echo $deliverables['legislation_deadline']; ?>" class="datepicker" readonly="readonly"  />
        </td>
    </tr>
    <!-- <tr>
        <th><?php echo $nObj->setHeader("reminder_date"); ?>:</th>
        <td>
          <input type="text" name="reminder" id="reminder" value="<?php echo $deliverables['reminder'] ?>" class="datepicker" />
        </td>
    </tr> -->
    <tr>
        <th><?php echo $nObj->setHeader("sanction"); ?>:</th>
        <td>
          <textarea name="sanction" id="sanction"><?php echo $deliverables['sanction'] ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("reference_link"); ?>:</th>
        <td>
          <input type="text" name="reference_link" id="reference_link" value="<?php echo $deliverables['reference_link'] ?>" />
        </td>
    </tr>
    <!-- <tr>
        <th>Organisation KPI Ref</th>
        <td>
          <input type="text" name="org_kpi_ref" id="org_kpi_ref" value="<?php echo $deliverables['org_kpi_ref'] ?>" />
        </td>
    </tr>
    <tr>
        <th>Individual KPI Ref</th>
        <td>
          <input type="text" name="individual_kpi_ref" id="individual_kpi_ref" value="<?php echo $deliverables['individual_kpi_ref'] ?>" />
        </td>
    </tr> -->
    <tr>
        <th><?php echo $nObj->setHeader("assurance"); ?>:</th>
        <td>
          <textarea name="assurance" id="assurance"><?php echo $deliverables['assurance'] ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?>:*</th>
        <td>
            <select name="compliance_to" id="compliance_to" multiple="multiple">
                <?php
                	  	$compArr = explode(",", $deliverables['compliance_to']);
                    foreach( $compliance as $key => $user){
                ?>
                    <option value="<?php echo $user['id']; ?>" <?php if(in_array($user['id'], $compArr)){ ?> selected="selected" <?php } ?>><?php echo $user['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("main_event"); ?>:<em>*</em></th>
        <td>
            <select name="main_event" id="main_event_">
                <?php
                    foreach( $events as $key => $event){
                ?>
                    <option value="<?php echo $event['id']; ?>" <?php if($event['id'] == $deliverables['main_event']){ ?> selected="selected" <?php } ?>><?php echo $event['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("sub_event"); ?>:<em>*</em></th>
        <td>
            <select name="sub_event" id="sub_event" multiple="multiple">
                <?php
                		$subEvents = explode(",", $deliverables['sub_event']);
                    foreach( $subevents as $key => $subevent){
                ?>
                    <option value="<?php echo $subevent['id']; ?>" <?php if(in_array($subevent['id'], $subEvents)){ ?> selected="selected" <?php } ?>><?php echo $subevent['name']; ?></option>
                <?php
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("guidance"); ?>:</th>
        <td>
            <textarea name="guidance" id="guidance"><?php echo $deliverables['guidance'] ?></textarea>
        </td>
    </tr>
    <tr>
        <th></th>
        <td>
          <input type="submit" name="edit_deliverable" id="edit_deliverable" value="Edit" class="isubmit" />
          <input type="button" name="delete" id="delete" value=" Delete " class="idelete" />
          <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>" />
          <input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $_GET['id']; ?>" class="logid" />
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("", ""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("deliverable_edit", false)?></td>
    </tr>
</table>
<div id="editLog"></div>
