<?php
$scripts = array("deliverable.js", "jquery.form.js");
include("../header.php");
$legislationId = $_REQUEST['legislationid'];
$nObj = new Naming();
$nObj -> setThColuomns();
$attachmentObj = new Attachment("deleverable", "import_file");
$filesAllowed   = array("csv");  
$file = "";
?>
<table class="noborder">
	<tr>
		<td class="noborder">
			<p class="ui-widget ui-state ui-state-highlight" style="padding:5px; clear:both; width:500px;">
			  <span class="ui-icon ui-icon-info" style="float:left;"></span>
			  <span>
			  		<span>Review the information.If the information is correct , click the Accept button.</span><br />
			  		<span style="margin-left:15px;"><b>No data will be imported until the final Accept button has been clicked</b></span>
			  </span>
			</p>
			<p id="output2"></p>			
		</td>
		<td class="noborder">
			<?php 
			$results =  $attachmentObj -> upload( $filesAllowed );
			if(($results['error']) || $results['error'] == TRUE)
			{
				echo '<p class="ui-widget ui-state ui-state-error" style="padding:5px; width:450px;">';
				echo '<span class="ui-icon ui-icon-closethick" style="float:left;"></span>';
				echo '<span>';
					echo '<span>'.$results['text'].'</span><br />';
				echo '</span>';			 			 
				echo '</p>';	
				displayGoBack("", "");
				return false;	
			}
			if(!($results['error']) || $results['error'] == FALSE)
			{
				echo '<p class="ui-widget ui-state ui-state-ok" style="padding:5px; width:430px;">';
				echo '<span class="ui-icon ui-icon-check" style="float:left;"></span>';
				echo '<span>';
					echo '<span>'.$results['text'].'</span><br />';
				echo '</span>';			
				$attachmentObj -> writeImported("deleverable");
				foreach( $results['files'] as $index => $file)
				{	
					$file = $index; 					
				}
				echo '</p>';	
			}
			?>		
		</td>
	</tr>
</table>
<?php 
$fileContents = $attachmentObj -> readfileContents("deleverable" ,$file );
$columnCount = 0;
$tableData = array();
$error = "";
$hasActions = false;
$acceptFile = true;
if(isset($fileContents) && !empty($fileContents))
{
	$complianceObj = new ComplianceFrequency();
	$compliances   = $complianceObj -> getComplianceList();	
	
    $typeObj = new OrganisationTypes();
    $types   = $typeObj -> getOrganisaitonTypesList();

    $respoObj     = new ResponsibleOrganisation();
    $responsibles = $respoObj-> getResponsibleOrganisationsList();
        
    $funObj = new FunctionalService();
    $functionalService = $funObj -> getFunctionServiceList();    
    
    $accountableObj  = new AccountablePerson();
    $accountables    = $accountableObj -> getAccountablePersonsList();    
    
  	$dirObj = new Directorate();
    $directorates = $dirObj -> getSubDirectoratesList();   

    $resownerObj = new ResponsibleOwner();
    $owners    	= $resownerObj -> getOwnersList();    

    $compObj      = new ComplianceTo();
    $complianceto = $compObj -> getComplianceToList();

    $eventsObj  = new Events();
    $events     = $eventsObj -> getEventsList();

    $seventsObj = new SubEvent();    
    $subevents  = $seventsObj -> SubEventsList();    

    //get the file headers     
    $headers     = $fileContents[0];
    $columnCount = count($headers); 
    foreach($headers as $key => $value)
    {
        $tableData['headers'][] = $value; 
    }
    $datatypes = $fileContents[1];
    unset($fileContents[0]);
    unset($fileContents[1]);
    
    //separating commond data types 
    $dates   	= array(  6  => "compliance_date",
    					  14 => "legislation_deadline"
    					);
    $textFields = array( 15  => "sanction",
    					 1   => "description",
    					 17  => "assurance",
    					 21  => "guidance",
    					 16  => "reference_link"
    					);
    $requiredText = array( 0 => "short_description",
    					   2 => "legislation_section"
    					  );     
    $data    = array();
    
    foreach($fileContents as $key => $value)
    {
    	$data[$key]['hasError'] 			 = 1;
    	$tableData['data'][$key]['hasError'] = 1;
    	$countTest[$key]  					 = 0; 
        $tr = "<tr>";
        if( count($value) < $columnCount ){
            $tableData['data'][$key] = "The column count does not match , there a fewer columns";
			$acceptFile 			 = false;
        } else if( count($value) > $columnCount){
            $tableData['data'][$key] = "The column count does not match , there a more columns";			
			$acceptFile 			 = false;
        } else {
        	//load the values
           foreach($value as $i => $val)
           {
           		$isRecurring = false;
                $val 		 = trim($val);
				$data[$key]['legislation'] = $_REQUEST['legislationid'];
				$data[$key]['insertuser']  = $_SESSION['tid'];
				if($i == 3){
				   //check the legislation compliance frequency validity				   
				    if( !in_array($val, $compliances)) {
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
                       $tableData['data'][$key]['compliance_frequency'] = "<p class='ui-state ui-state-error'>Legislation compliance frequency is not valid <br /><i>".$val."</i><br /></p>";
                    } else {
								$countTest[$key] = $countTest[$key] + 1;
								$new_compliances= array_flip($compliances);
								$data[$key]['compliance_frequency'] = $new_compliances[$val];
								$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
								$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
								//$tr .= "<td>".$val."</td>";
								$tableData['data'][$key]['compliance_frequency'] = $val;
                    }
				} else if($i == 4){
				  //check the validity of the deliverable applicable organisational type 
				  if(!empty($val))
				  {
						if( !in_array($val, $types)){
							$tableData['data'][$key]['applicable_org_type'] = "<p class='ui-state ui-state-error'>Organistion type ".$oValue." does not exist</p>"; 
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 0); 
						} else {
							$countTest[$key] = $countTest[$key] + 1;
							$new_types = array_flip($types);
							$data[$key]['applicable_org_type'] = $new_types[$val];
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);		                        
							$tableData['data'][$key]['applicable_org_type'] = $val;
						}				  
				  } else {
							$tableData['data'][$key]['applicable_org_type'] = "<p class='ui-state ui-state-error'>Organistion type is required</p>"; 
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 0); 
				  }
					/*
					$orgtypeArr = array_diff(explode(",", $val), array(''));
					$orgtypeStr = "";
					$orgtypeErr = "";
					$orgtypeDat  = "";   
				   
				   if(isset($orgtypeArr) && !empty($orgtypeArr))
				   {
						foreach($orgtypeArr as $oIndex => $oValue)
						{
						   $oValue = trim($oValue); 
						   if(!empty($oValue))
						   {
							  if( !in_array($oValue, $types)){
		                        $orgtypeErr = "<p class='ui-state ui-state-error'>Organistion type".$oValue." does not exist</p>"; 
		                        $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
		                        $data[$key]['hasError'] = ($data[$key]['hasError'] & 0); 
			                  } else {
										$countTest[$key] = $countTest[$key] + 1;
										$new_types = array_flip($types);
										$orgtypeDat  .= $oValue.",";
										$orgtypeStr .= $new_types[$oValue].","; 
										$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
										$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);		                        
			                  }
						   }
						}	
				   		if($orgtypeErr != "")
				   		{
						   //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>".$orgtypeErr."</p></td>";
						   $tableData['data'][$key]['applicable_org_type'] = "<p class='ui-state ui-state-error'>Organistion type <i>".$oValue."</i> does not exist</p>";				   			
				   		} else {
				   		   //$tr .= "<td>".rtrim($orgtypeDat)."</td>";
				   		   $data[$key]['applicable_org_type'] = rtrim($orgtypeStr,",");
				   		   $tableData['data'][$key]['applicable_org_type'] = rtrim($orgtypeDat, ",");
				   		}
				   } else {
					   $countTest[$key] = $countTest[$key] + 1;
                       $data[$key]['applicable_org_type'] = 1;
                       //$tr .= "<td>Unspecified</td>";
   					   $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
   					   $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
   					   $tableData['data'][$key]['applicable_org_type'] = "Unspecified";
				   }*/
				} else if($i == 5){
					//recurance type
					if(!empty($val))
					{
						if( $val == "Yes")
						{
							$isRecurring = true;
							$data[$key]['recurring'] = 1;
						} else {
							$isRecurring = false;
							$data[$key]['recurring'] = 0;
						}
						$tableData['data'][$key]['recurring'] = $val;												
					} else {
						$data[$key]['recurring'] = 0;
						$isRecurring 			 = false;
						$tableData['data'][$key]['recurring'] = No;
					}					
				} else if( $i == 7) {
					
					if(isset($value[5]) && $value[5] === "Yes")
					{
						if(empty($val) || !is_numeric($val))
						{
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['recurring_period'] = "<p class='ui-state ui-state-error'>Please enter valid number of days<br /><i>".$val."</i></p>";	
						} else {
							$data[$key]['recurring_period'] = $val;
							$tableData['data'][$key]['recurring_period'] = $val;
						}												
					} else {
							$data[$key]['recurring_period'] = "";
							$tableData['data'][$key]['recurring_period'] = "";
					}
					
				} else if($i == 8) {
					$daysOptions = array("days" => "Days", "working_days" => "Working Days");
					if(isset($value[5]) && $value[5] === "Yes")
					{
						if(!in_array($val, $daysOptions))
						{
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['days_options'] = "<p class='ui-state ui-state-error'>Invalid option <br /><i>".$val."</i></p>";	
						} else {
							$data[$key]['days_options'] = $val;
							$tableData['data'][$key]['days_options'] = $val;
						}												
					} else {
						$data[$key]['days_options'] = "";
						$tableData['data'][$key]['days_options'] = "";						
					}
				} else if($i == 9) {
					//check the validity of the recurrance types if the recurring is yes
					if(isset($value[5]) && $value[5] === "Yes")
					{
						$recurranceTypes = array("fixed" 		    => "Fixed",
														 "start" 		    => "Start Of Financial Year", 
														 "end" 	 		    => "End Of Financial Year",
								 						 "startofmonth"    => "Start Of Month",
								 						 "endofmonth" 	    => "End Of Month",
														 "startofquarter"  => "Start Of Quarter",
														 "endofquarter"	 => "End Of Quarter",
														 "startofsemester" => "Start Of Semester",
														 "endofsemester"   => "End Of Semester", 
														 "startofweek"		 => "Start Of Week", 
														 "endofweek"		 => "End Of Week"
														); 
						if( in_array($val, $recurranceTypes))
						{
							$countTest[$key] = $countTest[$key] + 1;
							$rTypes = array_flip($recurranceTypes);
							$data[$key]['recurring_type'] = $rTypes[$val];
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
							$tableData['data'][$key]['recurring_type'] = $val;	
						} else {
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
							$tableData['data'][$key]['recurring_type'] = "<p class='ui-state ui-state-error'>Invalid recurring type<br /><i>".$val."</i></p>";
						}				
					} else {
							$data[$key]['recurring_type'] = "";
							$tableData['data'][$key]['recurring_type'] = "";
					}	
				} else if($i == 10){
				   //check the validity of the deliverable responsible department if not empty
					if(!empty($val))
					{
					    if( !in_array($val, $directorates)){
	                       //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>Invalid department<br />".$val."</p></td>";
	                       $tableData['data'][$key]['responsible'] = "<p class='ui-state ui-state-error'>Invalid department<br /><i>".$val."</i></p>";
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
	                    } else {
						   $countTest[$key] = $countTest[$key] + 1;
	                       $new_directorates    = array_flip($directorates);
	                       $data[$key]['responsible'] = $new_directorates[$val];
	                       //$tr .= "<td>".$val."</td>";
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['responsibleError'] = $val;
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
	                    }
					} else {
					   $countTest[$key] = $countTest[$key] + 1;
                       $data[$key]['responsible'] = 1;
                      // $tr .= "<td>Unspecified</td>";
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['responsible'] = "Unspecified";
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
					}
				} else if($i == 11){
				   //check the validity of the deliverable functional services if not empty
				   if(!empty($val))
				   {
				   		if( !in_array($val, $functionalService)){
	                       //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>Invalid functional service<br />".$val."</p></td>";
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
	                       $tableData['data'][$key]['functional_service'] = "<p class='ui-state ui-state-error'>Invalid functional service<br /><i>".$val."</i></p>";
	                    } else {
	                       $countTest[$key] = $countTest[$key] + 1;
	                       $new_functional_service = array_flip($functionalService);
	                       $data[$key]['functional_service'] = $new_functional_service[$val];
	                      // $tr .= "<td>".$val."</td>";
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['functional_service'] = $val;
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
	                    }
				   } else {
                       $countTest[$key] = $countTest[$key] + 1;
                       $data[$key]['functional_service'] = 1;
                       //$tr .= "<td>Unspecified</td>";
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['functional_service'] = "Unspecified";
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
				   }					
				} else if($i == 12){
					//check the validity of the accountable person if not empty
					$personArr  = array_diff(explode(",", $val), array(''));
					$personStr  = ""; 
					$personErr  = "";
					$personData = "";	
					if(!empty($personArr))
					{
						foreach ($personArr as $aIndex => $aValue)
						{
							$aValue = trim($aValue);
							if(!empty($aValue))
							{
							     if( !in_array($aValue, $accountables)){
							       $personErr .= "<p class='ui-state ui-state-error'><br />".$cValue." is not valid</p>";
							       $tableData['data'][$key]['accountable_person'] = "<p class='ui-state ui-state-error'><br />".$aValue." is not valid</p>";
							       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
			                       //$tr .= "<td class='ui-widget'></td>";
			                 	} else {
								   $countTest[$key] = $countTest[$key] + 1;
			                       $new_accountable= array_flip($accountables); 	
			                   	   $personStr .= $new_accountable[$aValue].","; 	
			                   	   $personData   .= $aValue.",";
			                   	   $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);    
			                       //$tr .= "<td>".$val."</td>";
			                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
			                    }
							}		
						}
						if($personErr == "")
						{
							//$tr .= "<td class='ui-widget'><p>Compliance To errors</p> ".$compError."</td>";							//$tr .= "<td>".rtrim($compData)."</td>";
						   $data[$key]['accountable_person'] = rtrim($personStr,",");
	                       $tableData['data'][$key]['accountable_person'] = rtrim($personData, ",");
						}
						
					} else {
					   $countTest[$key] = $countTest[$key] + 1 ;
                       $data[$key]['accountable_person'] = 1;
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
                       $tableData['data'][$key]['accountable_person'] = "Unspecified";

					}						
				} else if($i == 13){
					//check the validity of the responsible person if not empty
					if(!empty($val))
					{
					    if( !in_array($val, $owners)){
	                       //$tr .= "<td class='ui-widget'><p class='ui-state ui-state-error'>Invalid Responsible Person<br />".$val."<p></td>";
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
	                       $tableData['data'][$key]['responsibility_owner'] = "<p class='ui-state ui-state-error'>Invalid Responsible Person<br /><i>".$val."</i><p>";
	                    } else {
						   $countTest[$key] = $countTest[$key] + 1;
	                       $new_owners = array_flip($owners);
	                       $data[$key]['responsibility_owner'] = $new_owners[$val];
	                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
	                       $tableData['data'][$key]['responsibility_owner'] = $val;
	                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
	                      // $tr .= "<td>".$val."</td>";
	                    }
					} else {
					   $countTest[$key] = $countTest[$key] + 1;
                       $data[$key]['responsibility_owner'] = 1;
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['responsibility_owner'] = "Unspecified";
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
                       //$tr .= "<td>Unspecified</td>";
					}
				} else if($i == 18){
					//check the validity of the deliverable compliance to if not empty
					$compArr = array_diff(explode(",", $val), array(''));
					$compString = ""; 
					$compError = "";
					$compData = "";
					if(!empty($compArr))
					{
						foreach ($compArr as $cIndex => $cValue)
						{
							$cValue = trim($cValue);
							if(!empty($cValue))
							{
							     if( !in_array($cValue, $complianceto)){
							       $compError .= "<p class='ui-state ui-state-error'><br />".$cValue." is not valid</p>";
							       $tableData['data'][$key]['compliance_to'] = "<p class='ui-state ui-state-error'><br />".$cValue." is not valid</p>";
							       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
			                       //$tr .= "<td class='ui-widget'></td>";
			                    } else {
								   $countTest[$key] = $countTest[$key] + 1 ;
			                       $new_complianceto = array_flip($complianceto);
			                   	   $compString .= $new_complianceto[$cValue].",";
			                   	   $compData   .= $cValue.",";
			                   	   $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);    
			                       //$tr .= "<td>".$val."</td>";
			                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
			                    }
							}		
						}
						if($compError == "")
						{
							//$tr .= "<td class='ui-widget'><p>Compliance To errors</p> ".$compError."</td>";							//$tr .= "<td>".rtrim($compData)."</td>";
						   $data[$key]['compliance_to'] = rtrim($compString,",");
	                       $tableData['data'][$key]['compliance_to'] = rtrim($compData, ",");
						}
						
					} else {
					   $countTest[$key] = $countTest[$key] + 1 ;
                       $data[$key]['compliance_to'] = 1;
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
                       $tableData['data'][$key]['compliance_to'] = "Unspecified";

					}
				} else if($i == 19){
				  //check the validity of the deliverable main event if not empty
					if(!empty($val))
					{
						if( !in_array($val, $events)){
							  $tableData['data'][$key]['main_event'] = "<p class='ui-state ui-state-error'>Main event ".$val." does not exist</p>";
							  $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
							  $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);	
						} else {
							$countTest[$key] = $countTest[$key] + 1;
							$new_events = array_flip($events);
							$data[$key]['main_event'] =  $new_events[$val];
							$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
							$tableData['data'][$key]['main_event'] = $val;
							$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
						}	
					} else {
						$tableData['data'][$key]['main_event'] = "<p class='ui-state ui-state-error'>Main event is required</p>";
						$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
						$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
					}			
					/*$eventArr = array_diff( explode(",", $val), array(''));
					$eventStr = "";
					$eventErr = "";
					$eventDat = "";
					
					if(!empty($eventArr))
					{
						foreach($eventArr as $eIndex => $eValue)
						{
							$eValue = trim($eValue);
							if(!empty($eValue))
							{
							   if( !in_array($eValue, $events)){
			                       $eventErr .= "<p class='ui-state ui-state-error'>Main event ".$val." does not exist</p>";
			                       $tableData['data'][$key]['main_event'] = "<p class='ui-state ui-state-error'>Main event ".$val." does not exist</p>";
			                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
			                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);	
			                    } else {
								   $countTest[$key] = $countTest[$key] + 1;
								   $new_events = array_flip($events);
			                       $eventDat .= $eValue.",";
			                       $eventStr .= $new_events[$eValue].",";
			                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
			                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
			                    }
							}								
						}
					} else {
						$eventErr .= "<p class='ui-state ui-state-error'>Main event is required</p>";
					   //$countTest[$key] = $countTest[$key] + 1;
                       //$data[$key]['main_event'] = 1;
                       //$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       //$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
                       //$tableData['data'][$key]['main_event'] = "Unspecified";
					}
					if($eventErr != "")
					{
					  $data[$key]['hasError'] = ($data[$key]['hasError'] & 0); 
					  $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
					  $tableData['data'][$key]['main_event'] = $eventErr;
					  //$tr .= "<td class='ui-widget'>".$eventErr."</td>";		
					} else {
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
					   $data[$key]['main_event'] = rtrim($eventStr,",");
                       $tableData['data'][$key]['main_event'] = rtrim($eventDat,",");
					}*/
				} else if($i == 20){
					//check the validity of the deliverable sub-events if not empty
					
					$subArr = array_diff( explode(",", $val), array(''));
					$subStr = "";
					$subErr = "";
					$subDat = "";					
					
					if(!empty($subArr))
					{
						foreach($subArr as $subIndex => $subValue )
						{
						    if( !in_array($subValue, $subevents)){
							   $subErr = "<p class='ui-state ui-state-error'>Invalid sub events<br />".$subValue."</p>";
		                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
		                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
		                    } else {
							   $countTest[$key] = $countTest[$key] + 1;
		                       $new_subevents = array_flip($subevents);
		                       $subDat .= $subValue.",";
		                       $subStr .= $new_subevents[$subValue].",";
		                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
		                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);               
		                    }	
						}										
					} else {
					   $subErr = "<p class='ui-state ui-state-error'>Sub events are required</p>";				
					}
					
					if($subErr != "")
					{
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);	
                       $tableData['data'][$key]['sub_event'] = $subErr;						                       
					} else {
                       $data[$key]['sub_event'] = rtrim($subStr,",");
                       $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       $tableData['data'][$key]['sub_event'] = rtrim($subDat, ",");
                       $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);					
					}
				} else if($i == 22){
					//check if deliverable has action or not
					if(!empty($val))
					{
					   $hasActions = false;
					   $data[$key]['has_actions'] = false;
                       $tableData['data'][$key]["has_actions"] = "No";						
					} else {
						$hasActions = true;
						$data[$key]['has_actions'] = true;
                        $tableData['data'][$key]["has_actions"] = "Yes";		
					}
				} else if( array_key_exists($i, $dates)){
					//check the validity of the dates
				    if(preg_match("/[2][0-9][0-9][1-9]\/[0-1][0-9]\/[0-3][0-9]/",$val) == FALSE){
				    	if((isset($value[5]) && $value[5] === "Yes") && $i == 6)
						{
                      		$tableData['data'][$key][$dates[$i]] = "";
                      	 	$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
                       		$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
						} else {
                      		$tableData['data'][$key][$dates[$i]] = "<p class='ui-state ui-state-error'>Pattern error(YYYY/MM/DD)<br />".$val."</p>";
                      	 	$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
                       		$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);						
						}
                    } else {
					  $newdate = date("d-M-Y", strtotime($val));	
					  $countTest[$key] = $countTest[$key] + 1;
					  if( $i == 6)
					  {
					  	$data[$key][$dates[$i]] = date("d-M", strtotime($val));;
					  } else {
					  	$data[$key][$dates[$i]] = $newdate;
					  }
					  $data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
					  $tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
                      $tableData['data'][$key][$dates[$i]] = $val."<br />(".$newdate.")";
                    }
					
				} else if( array_key_exists($i, $requiredText)){
					//check the validity of the test fields
					if( $val == "")
					{
						$tableData['data'][$key][$requiredText[$i]] = "<p class='ui-state ui-state-error'>".ucwords(str_replace("_", "", $requiredText[$i]))." is required and cannot be empty</p>";
						$data[$key]['hasError'] = ($data[$key]['hasError'] & 0);
						$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 0);
					} else {
						$countTest[$key] = $countTest[$key] + 1;
						$data[$key][$requiredText[$i]] = $val;
						$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
						$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
						$tableData['data'][$key][$requiredText[$i]] = $val;
					}
				} else {
					$countTest[$key] = $countTest[$key] + 1;
					$data[$key][$textFields[$i]] = $val;
					$data[$key]['hasError'] = ($data[$key]['hasError'] & 1);
					$tableData['data'][$key]['hasError'] = ($tableData['data'][$key]['hasError'] & 1);
					$tableData['data'][$key][$textFields[$i]] = $val;
				}
				
           }// foreach deliverables values  
        }
    } // foreach file contents 
} //end if filecontents isset

$delTable  = "<table>";
$delTable .= "<tr>";
$delTable .= "<th>&nbsp;&nbsp;</th>";
foreach($tableData['headers'] as $hIndex => $header)
{	
	$delTable .= "<th>".$header."</th>";	
}
$delTable .= "</tr>";
foreach($tableData['data'] as $dIndex => $dArr)
{
	$delTable .= "<tr>";
	if($dArr['hasError'])
	{
		//if there is no error in this column
		$delTable .= "<td class='ui-state-ok'><span class='ui-icon ui-icon-check' style='margin-right:0.3em;'></span></td>";
	} else {
		$acceptFile = false;
		$delTable .= "<td class='ui-state-error'><span class='ui-icon ui-icon-closethick' style='margin-right:0.3em;'></span></td>";
	}	
	unset($dArr['hasError']);	
	foreach($dArr as $key => $val)
	{
		$delTable .= "<td>".$val."</td>";		
	}	
	$delTable .= "</tr>";
}
$delTable .= "</table>";
?>
<form action="save_import.php" method="post" id="saveImport">
	<table class="noborder">
	  <tr class="noborder">
	    <td class="noborder">
	    	<button id="accept" <?php echo ($acceptFile ? "" : "disabled='disabled'" ); ?> class="isubmit">
	    		<span class="ui-icon ui-icon-check" style="float:left;"></span>
	    		<span>Accept</span>
	    	</button>
	    	<input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_REQUEST['legislationid']; ?>"/>
	    	<input type="hidden" name="columnCount" id="columnCount" value="<?php echo $columnCount; ?>"/>

	    </td>
		<td class="noborder" >
			 <button id="reject" <?php echo ($acceptFile ? "" : "disabled='disabled'" ); ?> class="idelete">
	    		<span class="ui-icon ui-icon-closethick" style="float:left;"></span>
	    		<span>Reject</span>
	    	</button>
		</td>    
	  </tr>
	</table>
</form>
<?php  
if( $acceptFile ){
	$_SESSION['importdata'] = $data;
	$_SESSION['importtable'] = $delTable;
}
echo $delTable;
displayGoBack("", "");
?>
