<?php
$scripts = array("jquery.ui.legislation.js");
$styles = array();
$page_title = "New Deliverable";
require_once("../header.php");
?>
<script language="javascript">
$(function(){
	$("#display_legislation").legislation({addDeliverable:true, addAction:true , url:"../class/request.php?action=ManageLegislation.getLegislations",section:"manage", user:$("#userId").val()});
})
</script>
<div id="display_legislation"></div>
<input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION['tid']; ?>" />
