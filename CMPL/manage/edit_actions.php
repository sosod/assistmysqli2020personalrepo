<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("jquery.ui.action.js");
include("../header.php");
$deliverableObj  = new Deliverable();
$deliverable    = $deliverableObj -> getADeliverable( $_GET['id'] );
$sectionObj      = new ComplianceFrequency();
$compfreq        = $sectionObj -> getSortedCompliance();
$typeObj         = new OrganisationTypes();
$types           = $typeObj -> sortedOrgTypes();

$accObj          = new AccountablePerson();
$accountables    = $accObj -> getAccountablePersons();
$respoObj		 = new ResponsibleOwner();
$responsibles    = $respoObj -> sortedOwners();
$dirObj          = new Directorate();
$directorates    = $dirObj -> getSubDirectorates();
$funObj          = new FunctionalService();
$functionalService= $funObj -> sortedFunctionService();
$compObj         = new ComplianceTo();
$compliance     = $compObj -> getComplianceTo();
$eventsObj       = new Events();
$events          = $eventsObj -> getEvents();
$seventsObj      = new SubEvent();
$subevents       = $seventsObj -> getSubEvents();


$delObj     = new NewDeliverable();

$orgtype      = $delObj->getOrganisationTypesList($_GET['id']); 	   	  
$complianceto = $delObj->getComplianceToList($_GET['id']);	  	    
$mainevent    = $delObj->getEventsList($_GET['id']);		   
$subevents    = $delObj->getSubEventList($_GET['id']);        
$accountables = $delObj->getAccountablePersonList($_GET['id']); 

$nObj = new Naming();
$nObj -> setThColuomns(); 
?>
<table class="noborder" width="100%">
	<tr>
		<td class="noborder">
           
            <table width="100%">
			   <tr>
			   	  <td colspan="2"> <h4>Deliverable Details</h4></td>
			   </tr>            
                  <tr>
                    <th>Ref #:</th>
                    <td><?php echo $deliverable['id']; ?></td>
                </tr>
               <tr>
                    <th><?php echo $nObj->setHeader("short_description_of_legislation"); ?>:</th>
                    <td><?php echo $deliverable['short_description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("description_of_legislation"); ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_section"); ?>:</th>
                    <td><?php echo $deliverable['legislation_section']; ?></td>
                </tr>				
                <tr>
                    <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>:</th>
                    <td><?php echo (empty($deliverable['compliance_frequency']) ? "" : $compfreq[$deliverable['compliance_frequency']]['name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("applicable_organisation_type"); ?>:</th>
                    <td><?php echo implode(",",$orgtype); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>:</th>
                    <td><?php echo $deliverable['compliance_date']; ?></td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("responsible_department"); ?>:</th>
                    <td><?php 
                    	$dir =  $dirObj -> fetch( $deliverable['responsible'] );
                    	$subDir = $dirObj -> getSubDirectorate($deliverable['responsible']);
                    	echo $dir['name']; 
                    	?></td>
                </tr>                
                <tr>
                    <th><?php echo $nObj->setHeader("functional_service"); ?>:</th>
                    <td><?php echo (!empty($deliverable['functional_service']) ? $functionalService[$deliverable['functional_service']]['name'] : ""); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("accountable_person"); ?>:</th>
                    <td><?php echo implode(",", $accountables);	?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("responsibility_ower"); ?>:</th>
                    <td><?php echo (!empty($deliverable['responsibility_owner']) ? $responsibles[$deliverable['responsibility_owner']]['name'] : ""); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?>:</th>
                    <td><?php echo $deliverable['legislation_deadline']; ?></td>
                </tr>
                <!-- <tr>
                    <th>Organisation KPI Ref</th>
                    <td><?php echo $deliverable['org_kpi_ref']; ?>:</td>
                </tr> --> 
                <tr>
                    <th><?php echo $nObj->setHeader("sanction"); ?>:</th>    
                    <td>
                      <?php echo $deliverable['sanction']; ?>
                    </td>
                </tr>     
                <tr>
                    <th><?php echo $nObj->setHeader("reference_link"); ?>:</th>    
                    <td><?php echo $deliverable['reference_link']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("assurance"); ?>:</th>    
                    <td><?php echo $deliverable['assurance']; ?></td>
                </tr>                                             
                <tr>
                    <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?>:</th>
                    <td><?php echo implode(",", $complianceto); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("main_event"); ?>:</th>
                    <td><?php echo implode(",",$mainevent); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("sub_event"); ?>:</th>
                    <td><?php  echo implode(",", $subevents);  ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("guidance"); ?>:</th>
                    <td><?php echo $deliverable['guidance']; ?></td>
                </tr> 
				<tr>
					<td class="noborder"><?php displayGoBack("", ""); ?></td>
					<td class="noborder"></td>
				</tr>
            </table>		
		
		<!-- <table width="100%">
            	<tr>
            		<td colspan="2"><h4>Deliverable Details</h4></td>
            	</tr>
                <tr>
                  <th>Ref #:</th>
                  <td><?php echo $_GET['id']; ?></td>
                </tr>
               <tr>
                    <th><?php echo $nObj->setHeader("short_description_of_legislation"); ?>: </th>
                    <td><?php echo $deliverable['short_description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("description_of_legislation"); ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_section"); ?>: </th>
                    <td><?php echo $deliverable['legislation_section']; ?></td>
                </tr>				
               <!--   <tr>
                    <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>:</th>
                    <td><?php echo $deliverable['compliance_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("applicable_organisation_type"); ?>:</th>
                    <td><?php echo $deliverable['applicable_org_type']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>:</th>
                    <td><?php echo $deliverable['compliance_date']; ?></td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("responsible_department"); ?>:</th>
                    <td><?php echo $deliverable['responsible']; ?></td>
                </tr>                
                <tr>
                    <th><?php echo $nObj->setHeader("functional_service"); ?>:</th>
                    <td><?php echo $deliverable['functional_service']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("accountable_person"); ?>:</th>
                    <td><?php echo $deliverable['accountable_person']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("responsibility_ower"); ?>:</th>
                    <td><?php echo $deliverable['responsiblity_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?>:</th>
                    <td><?php echo $deliverable['legistation_deadline']; ?></td>
                </tr>
                 <tr>
                    <th>Organisation KPI Ref</th>
                    <td><?php echo $deliverable['org_kpi_ref']; ?>:</td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("sanction"); ?>:</th>    
                    <td>
                      <?php echo $deliverable['sanction']; ?>
                    </td>
                </tr>     
                <tr>
                    <th><?php echo $nObj->setHeader("reference_link"); ?>:</th>    
                    <td><?php echo $deliverable['reference_link']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("assurance"); ?>:</th>    
                    <td><?php echo $deliverable['assurance']; ?></td>
                </tr>                                             
				  <tr>
                    <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?>:</th>
                    <td><?php echo $deliverable['compliance_to']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("main_event"); ?>:</th>
                    <td><?php echo $deliverable['main_event']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("sub_event"); ?>:</th>
                    <td><?php echo $deliverable['sub_event']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("guidance"); ?>:</th>
                    <td><?php echo $deliverable['guidance']; ?></td>
                </tr> 
				<tr>
					<td class="noborder"><?php displayGoBack("", ""); ?></td>
					<td class="noborder"></td>
				</tr>
            </table>-->
		</td>
		<td class="noborder">
			<script language="javascript">
			$(function(){
				$("#display_action").action({editAction:true,deleteActions:true, id:$("#deliverableid").val(), controller : "managecontroller", section:"manage"});
			})
			</script>
			<div id="display_action"></div>
			<input type="hidden" id="deliverableid" name="deliverableid" value="<?php echo $_GET['id']; ?>" />		
		</d>
	</tr>
</table>
