<?php
$scripts = array("legislation.js");
include("../header.php");
$nObj = new Naming();
$nObj -> setThColuomns();
$legObj 	 = new Legislation();
$legislation = $legObj->getALegislation( $_GET['id'] );
$legTypes    = new LegislativeTypes();
$types 		 = $legTypes->getLegislativeTypes();
$legcat  	 = new LegislativeCategories();
$categories  = $legcat ->getLegislativeCategories();
$sizeObj 	 = new OrganisationSize();
$sizes		 = $sizeObj->getOrganisationSize();
$typeObj 	 = new OrganisationTypes();
$orgTypes 	 = $typeObj->getOrganisationTypes();
$capObj 	 = new OrganisationCapacity();
$capacities  = $capObj->getOrganisationCapacity();  
$respoOrg 	 = new ResponsibleOrganisation();
$organisations = $respoOrg->getResponsibleOrganisations();

$legObj     = new NewLegislation();

$legCategories = $legObj->getLegislativeCategoriesList($legislation['ref']);
$legCapacities = $legObj->getOrganisationCapacityList( $legislation['ref'] );

$legSizes  = $legObj->getOrganisationSizeList( $legislation['ref'] );

$legOrgtypes = $legObj->getOrganisationTypesList( $legislation['ref'] );
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_name");?>:</th>    
    	<td>
	        <textarea rows="5" cols="30" id="name" name="name" class="checkcounter"><?php echo $legislation['legislation_name']; ?></textarea>
    		<span id="count_name" class="textcounter"></span>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_reference"); ?>:</th>    
    	<td><input type="text" name="reference" id="reference" value="<?php echo $legislation['legislation_reference']; ?>" /></td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_number"); ?>:</th>    
    	<td>
          <textarea rows="5" cols="30" name="legislation_number" id="legislation_number" class="checkcounter"><?php echo $legislation['legislation_number']; ?></textarea>
          <span id="count_legislation_number" class="textcounter"></span>    	
        </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_type");?>:</th>    
    	<td>
        	<select name="type" id="type">
            	<?php 
            		foreach( $types as $key => $type){
            	?>
            		<option selected="<?php if($type['id'] == $legislation['type']){ ?> selected <?php } ?>" value="<?php echo $type['id']; ?>"><?php echo $type['name']; ?></option>
            	<?php 
            		}
            	?>
            </select>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_category"); ?>:</th>    
    	<td>
        	<select name="category" id="category" multiple="multiple">
            	<?php 
            	//$legCats = explode(",",  $legislation['category']);
            	 foreach( $categories as $key => $category){
            	?>
            		<option <?php if(isset($legCategories[$category['id']])) { ?> selected="selected" <?php } ?> value="<?php echo $category['id']?>" ><?php echo $category['name']; ?></option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr>    
	<tr>
    	<th><?php echo $nObj->setHeader("organisation_size"); ?>:</th>    
    	<td>
        	<select name="organisation_size" id="organisation_size" multiple="multiple">
            	<?php
            	 //$legSize = explode(",",  $legislation['organisation_size']); 
            	 foreach( $sizes as $key => $size){
            	?>
            		<option <?php if(isset($legSizes[$size['id']])) { ?> selected="selected" <?php } ?> value="<?php echo $size['id']?>" ><?php echo $size['name']; ?></option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr> 
    <tr>   
    	<th><?php echo $nObj->setHeader("organisation_type"); ?>:</th>    
    	<td>
        	<select name="organisation_type" id="organisation_type" multiple="multiple">
            	<?php 
            	 //$legType = explode(",",  $legislation['organisation_type']); 
            	 foreach( $orgTypes as $key => $type){
            	?>
            		<option <?php if(isset($legOrgtypes[$type['id']])) { ?> selected="selected" <?php } ?> value="<?php echo $type['id']?>" ><?php echo $type['name']; ?></option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr>   
	<tr>
    	<th><?php echo $nObj->setHeader("organisation_capacity"); ?>:</th>    
    	<td>
        	<select name="organisation_capacity" id="organisation_capacity" multiple="multiple">
            	<?php 
            	 //$legCapacity = explode(",",  $legislation['organisation_capacity']); 
            	 foreach( $capacities as $key => $capacity){
            	?>
            		<option <?php if(isset($legCapacities[$capacity['id']])) { ?> selected="selected" <?php } ?> value="<?php echo $capacity['id']?>" ><?php echo $capacity['name']; ?></option>
            	<?php 
            	 }
            	?>
            </select>        
        </td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("responsible_business_patner"); ?>:</th>
    	<td>
        	<select name="responsible_organisation" id="responsible_organisation">
            	<option value="">--responsible business patner--</option>
            	<?php 
            	 foreach( $organisations as $key => $organisation){
            	?>
            		<option <?php if($organisation['id'] == $legislation['responsibleorganisation']) { ?> selected="selected" <?php } ?>" value="<?php echo $organisation['id']?>" ><?php echo $organisation['name']; ?></option>
            	<?php 
            	 }
            	?>           	
            </select>
        </td>
    </tr>      
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_date"); ?>:</th>    
    	<td> 
          <input type="text" name="legislation_date" id="legislation_date" value="<?php echo $legislation['legislation_date']; ?>" class="datepicker" readonly="readonly" />
        </td>
    </tr>                    
	<tr>
    	<th><?php echo $nObj->setHeader("hyperlink_act"); ?>:</th>    
    	<td>
          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="<?php echo $legislation['hyperlink_to_act']; ?>" />
        </td>
    </tr> 
	<tr>
    	<th></th>    
    	<td>
          <input type="button" name="edit" id="edit" value=" Edit " class="isubmit" />
          <input type="button" name="delete" id="delete" value=" Delete " class="idelete" />
          <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_GET['id']; ?>" />
          <input type="hidden" name="legislation_id" id="legislation_id" value="<?php echo $_GET['id']; ?>" class="logid" />
        </td>
    </tr>  
	<tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>    
    	<td class="noborder"><?php displayAuditLogLink("legislation_edit", false); ?></td>
    </tr>                       
</table>
<div id="audit_log"></div>
