<?php
$scripts = array("deliverable.js");
include("../header.php");
//$data = array();
$legislationId = $_REQUEST['legislationid'];
$nObj = new Naming();
$nObj -> setThColuomns(); 
?>
<table class="noborder" cellpadding="0" cellspacing="0"> 
  <tr>
    <td class="noborder"  valign="top">
     <table>
	  <tr>
	    <th>Generate CSV:</th>
	    <td>
			<form action="make_file.php" method="post" enctype="multipart/form-data">
		     <input type="submit" name="generate_csv" id="generate_csv" value="Generate" />
	    	</form>
	    </td>
	  </tr>  
	  <tr>
	  	<th>Import File:</th>
	    <td>
	    	<form action="process_import.php" method="post" enctype="multipart/form-data">  
		    	<table width="100%" class="noborder">
			        <tr>
			            <td class="noborder">
			                <input type="file" name="import_file" id="import_file" />
			            </td>
			            <td class="noborder"> 
			                <input type="submit" name="save_import" id="save_import" value="Import" class="isubmit" />
			                <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_REQUEST['legislationid']; ?>" />
			            </td>
			        </tr>    		
		    	</table>
	    	</form>
	    </td>
	  </tr> 
	  </table>    
    </td>  
  	<td class="noborder" valign="top">
  		<p class="ui-widget ui-state ui-state-highlight" style="padding:7px; margin:0px;">
  			<span class="ui-icon ui-icon-info" style="float:left;"></span>
  			<span>
  				<span>Review the information, if the information is correct , click the <b>Accept</b> button.</span><br />
  				<span style="margin-left:15px;"><b>No data will be imported until the final Accept button has been clicked</b></span>
  			</span>
  		</p>
  	</td>
  </tr>
</table>
<div><?php displayGoBack("", ""); ?></div>
<table>
    <tr>
        <th>Fields</th>
        <td><p>*All fileds marked with * are required fields</p></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("short_description_of_legislation"); ?>*</th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("description_of_legislation"); ?> </th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislation_section"); ?>*</th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>*</th>
        <td><a href="sel_compliace_frequency.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("applicable_organisation_type"); ?></th>
        <td><a href="sel_applicableorg.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>*</th>
        <td>Date(yyyy-mm-dd)</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislation_compliance_date_recurring_types"); ?>*</th>
        <td><a href="sel_recurringtype.php">Select Options</a></td>
    </tr>      
    <tr>
        <th><?php echo $nObj->setHeader("responsible_department"); ?></th>
        <td><a href="sel_respdept.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("functional_service"); ?></th>
        <td><a href="sel_funcservice.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("accountable_person"); ?><em>*</em></th>
        <td><a href="sel_accountable_person.php">Select Options</a> </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("responsibility_ower"); ?></th>
        <td><a href="sel_responsiblity_owner.php">Select Options</a> </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?></th>
        <td>Date(yyyy-mm-dd)</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("sanction"); ?> </th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("reference_link"); ?></th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("assurance"); ?></th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?><em>*</em></th>
        <td><a href="sel_compliance.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("main_event"); ?><em>*</em></th>
        <td><a href="sel_mainevent.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("sub_event"); ?><em>*</em></th>
        <td><a href="sel_subevent.php">Select Options</a></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("guidance"); ?></th>
        <td>Text</td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("do_this_deliverable_has_actions"); ?></th>
        <td>Text</td>
    </tr>    
</table>
<input type="hidden" name="legislationid" id="legislationid" value="<?php $_GET['legislationid']?>" class="idelete" />
