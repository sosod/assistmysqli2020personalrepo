<?php
$scripts = array("deliverable.js");
include("../header.php");
$legislationId = $_REQUEST['legislationid'];
$nObj = new Naming();
$nObj -> setThColuomns();
$saving = "<p>Saving the imported data . . . <img src='../images/loaderA32.gif' /></p>"; 
$dsaved   = 0;
$dunsaved = 0;
$del      = new Deliverable();
$errorSave = "";
$hasActions = true;
$multiple = array(
						"sub_event" 		=> array("t" => "subevent_deliverable", "key" => "subevent_id"),
						"compliance_to" 	=> array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
						"accountable_person"  => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
	  		);
	
foreach($_SESSION['importdata'] as $sIndex =>  $dataArr)
{
	unset($dataArr['hasError']);
	$dataCount = count($dataArr); 	
	if($dataCount == ($_REQUEST['columnCount']+2) )
	{
		$hasActions = $dataArr['has_actions'];
		unset($dataArr['has_actions']);
		$insertdata = array();
		$multipleInsert = array();
		foreach($dataArr as $dIndex => $dVal)
		{
		  if( array_key_exists($dIndex, $multiple))
		  {
		 	 $multipleInsert[$dIndex] = explode(",", $dVal);
		  } else {
			$insertdata[$dIndex] = $dVal;
		  }		
		}
		$delId =  $del->saveDeliverable($insertdata);
		if( $delId > 0)
		{
			$dsaved++;  
			if( !$hasActions )
			{
				$actionObj  = new Action();
				$action = array("action" 			  => $dataArr['short_description'],
									 "deadline" 		  => $dataArr['legislation_deadline'],
									 "action_deliverable" => $dataArr['legislation_deadline'],
									 "deliverable_id"	  => $delId,
									 "owner" 			  => "" 
									); 
				$actionId   = $actionObj -> save($action);				
			}
			//save the multiple fields into the relationship tables 
			foreach($multiple as $k => $multipleArr)
			{
			   if(isset($multipleInsert[$k]) && !empty($multipleInsert[$k]))
			   {
				foreach($multipleInsert[$k] as $index => $val)
				{
				  $dRes = $del->insert($multipleArr['t'], array("deliverable_id" => $delId, $multipleArr['key'] => $val) );
				}		
			   }
			}		
			//echo "Delivereable saved";
		} else {
			$dunsaved++;
			//echo "An error occured saving the deliverables";
		}
	} else {
		$dunsaved++;
		//$errorSave = "<p class='ui-state ui-widget' style='position:relative; margin-top:3px;'><span class='ui-state-error' style='padding:7px; font-size:1.0em;'>Column count does not match ,data = ".$dataCount." and columns = ".($columnCount+1)."</span></p>";		
	}	
}
?>
<table class="noborder">
	<tr>
		<td class="noborder">
			<?php 
				if($errorSave == ""){
			?>
			<p class="ui-widget ui-state ui-state-ok" style="padding:5px; clear:both; width:450px;">
			  <span class="ui-icon ui-icon-check" style="float:left;"></span>
			  <span>
				<?php echo $dsaved." deliverables successfully saved"; ?>
			  </span>
			</p>
			<?php 
				} else {
			?>
			<p class="ui-widget ui-state ui-state-error" style="padding:5px; clear:both; width:450px;">
			  <span class="ui-icon ui-icon-info" style="float:left;"></span>
			  <span>
				<?php echo $errorSave; ?>
			  </span>
			</p>		
			<?php 
				}
			?>
		</td>
		<td class="noborder">
			<?php 
				echo '<p class="ui-widget ui-state ui-state-error" style="padding:5px; width:450px;">';
				echo '<span class="ui-icon ui-icon-closethick" style="float:left;"></span>';
				echo '<span>';
				  echo '<span>'.$dunsaved.'  deliverables were not saved.</span><br />';
				echo '</span>';			 			 
				echo '</p>';			
			?> 
		</td>
	</tr>
</table>
<?php 
$tableImported = $_SESSION['importtable'];
unset($_SESSION['importtable']);
unset($_SESSION['importdata']);
unset($data);
echo $tableImported;
unset($saving);
?>
<div><?php displayGoBack("", ""); ?></div>
