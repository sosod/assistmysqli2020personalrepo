<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("action.js");
include("../header.php");
$delObj   = new Action();
$action  = $delObj ->getAction( $_REQUEST['id'] );
$usersObj = new ActionOwner();
$users    = $usersObj ->getActionOwners();
$nObj = new Naming();
$nObj -> setThColuomns(); 
?>
<?php JSdisplayResultObj(""); ?>
 <table>
    <tr>
        <th>Ref #:</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("action_required"); ?>:</th>
        <td>
             <textarea rows="7" cols="80" id="action_name" name="action_name"><?php echo $action['action_required']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("action_owner"); ?>:</th>
        <td>
            <select name="action_owner" id="action_owner">
                <option value="">--action owner--</option>
                <?php
                    foreach($users as $u => $user){
	                ?>
	                <option value="<?php echo $user['id'] ?>" <?php if($user['id'] == $action['owner']){ ?> selected="selected" <?php } ?>><?php echo $user['name']; ?></option>
	                <?php
	                    }
	                ?>
            </select>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("action_deliverable"); ?>:</th>
        <td>
            <textarea rows="7" cols="80" id="deliverable" name="deliverable"><?php echo $action['action_deliverable']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("action_deadline_date"); ?>:</th>
        <td>
        	<input type="text" name="deadline" id="deadline" value="<?php echo $action['action_deadline_date']; ?>" class="datepicker" readonly="readonly" />
        </td>
    </tr>
    <tr>
        <th><?php echo $nObj->setHeader("action_reminder"); ?>:</th>
        <td><input type="text" name="reminder" id="reminder" value="<?php echo $action['action_reminder']; ?>" class="datepicker" readonly="readonly" /></td>
    </tr>
    <tr>
        <th></th>
        <td>
          <input type="button" name="edit" id="edit" value=" Edit " class="isubmit" />
          <input type="button" name="delete" id="delete" value=" Delete " class="idelete" />
          <input type="hidden" name="actionid" id="actionid" value="<?php echo $_GET['id']; ?>">
           <input type="hidden" name="action_id" id="action_id" value="<?php echo $_GET['id']; ?>" class="logid">
        </td>
    </tr>
     <tr>
         <td class="noborder"><?php displayGoBack("", ""); ?></td>
         <td class="noborder"><?php displayAuditLogLink("action_edit", false)?></td>
     </tr>
</table>
<div id="editLog"></div>
