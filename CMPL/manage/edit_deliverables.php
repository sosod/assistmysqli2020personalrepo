<?php
$scripts = array("jquery.ui.deliverable.js");
include("../header.php");
$legislationObj = new Legislation();
$legislation    = $legislationObj -> getALegislation( $_GET['id'] );

$nObj = new Naming();
$nObj -> setThColuomns();
$legObj     = new NewLegislation();

$categories = $legObj->getLegislativeCategoriesList($legislation['ref']);

$capacities = $legObj->getOrganisationCapacityList( $legislation['ref'] );

$sizes  = $legObj->getOrganisationSizeList( $legislation['ref'] );

$orgtypes = $legObj->getOrganisationTypesList( $legislation['ref'] );
?>

<table class="noborder">
	<tr>
		<td class="noborder" width="45%">
		 			<table width='100%'>
		            	<tr>
		            		<td colspan="2"><h4>Legislation Details</h4></td>
		            	</tr>
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_name"); ?>: </th>    
		                    <td><?php echo $legislation['legislation_name']; ?></td>
		                </tr>
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_reference"); ?>:</th>    
		                    <td><?php echo $legislation['legislation_reference']; ?></td>
		                </tr>    
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_number"); ?>:</th>    
		                    <td><?php echo $legislation['legislation_number']; ?></td>
		                </tr>                    
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_type"); ?>:</th>    
		                    <td><?php echo $legislation['legislation_type']; ?></td>
		                </tr>    
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_category"); ?>:</th>    
		                    <td><?php echo implode(",",$categories); ?></td>
		                </tr>    
		                <tr>
		                    <th><?php echo $nObj->setHeader("organisation_size"); ?>:</th>    
		                    <td><?php echo implode(",",$sizes); ?></td>
		                </tr>         
		                <tr>
		                    <th><?php echo $nObj->setHeader("organisation_type"); ?>:</th>    
		                    <td><?php echo implode(",",$orgtypes); ?></td>
		                </tr>   
		                <tr>
		                    <th><?php echo $nObj->setHeader("organisation_capacity"); ?>:</th>    
		                    <td><?php echo implode(",",$capacities); ?></td>
		                </tr>
		                <tr>
		                    <th><?php echo $nObj->setHeader("responsible_business_patner"); ?>:</th>    
		                    <td><?php echo $legislation['responsible_organisation']; ?></td>
		                </tr>                  
		                <tr>
		                    <th><?php echo $nObj->setHeader("legislation_date"); ?>:</th>    
		                    <td><?php echo $legislation['legislation_date']; ?></td>
		                </tr>                            
		                <tr>
		                    <th><?php echo $nObj->setHeader("hyperlink_act"); ?>:</th>    
		                    <td><?php echo $legislation['hyperlink_to_act']; ?></td>
		                </tr> 
		                <tr>
		                    <td class="noborder"><?php displayGoBack("", ""); ?></td>    
		                    <td class="noborder"></td>
		                </tr>                 
		            </table>
		</td>
		<td class="noborder">
			<script language="javascript">
			$(function(){
				$("#display_deliverable").deliverable({editDeliverable:true,editActions:true, id:$("#legid").val(),url:"../class/request.php?action=NewDeliverable.getDeliverables", section:"manage"});
			})
			</script>
			<div id="display_deliverable"></div>
			<input type="hidden" id="legid" name="legid" value="<?php echo $_GET['id']; ?>" />		
		</td>
	</tr>
</table>
