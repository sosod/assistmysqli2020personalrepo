<?php
class NewLegislation extends LegislationFactory
{
            
     function __construct()
     {
         parent::__construct();
     }       
      
     function getLegislations( $start, $limit , $options = array())
     {
     
         $optionSql = "";
         $optionSql = " AND L.status & ".Legislation::CONFIRMED." <> ".Legislation::CONFIRMED."";	
         
	if($_SESSION['tid'] != "0001"){
		   $businessPatnerObj = new ResponsibleOrganisation();
		   $businessPartner = $businessPatnerObj -> getBussinessPartners();
		   $bpId = "";
		   foreach( $businessPartner as $bIndex => $bValue)
		   {
			   $userIds = array();
			   $userIds = explode(",", $bValue['user_id']);
			   if( in_array($_SESSION['tid'], $userIds))
			   {
				   $bpId = $bValue['business_partner_id'];	
			   }	
		   }		
		   $options .= " AND L.responsible_organisation = ".($bpId == "" ? 0 : $bpId); 
	   }        
	   Naming::$section  = $options['section'];
	   $legislations     = $this->legislationObj->getLegislations($start, $limit, $optionSql ); 	
	   $leg              = array();
	   $leg = $this->sortLegislation( $legislations, new LegislationColumns() );
	   $total         = $this->legislationObj->getTotalLegislation($optionSql);;
	   $leg['total']  = $total['total'];
         return $leg;
     } 
     
function saveDeliverable( $data )
 {
	 $createAction = FALSE;
		$multiple = array(
			  "sub_event" 		=> array("t" => "subevent_deliverable", "key" => "subevent_id"),
			  "compliance_to" 	=> array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
			  "accountable_person"  => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
			  );
		foreach( $data as $key => $deliverable)
		{
		  if( $key == "hasActions" && $deliverable == 0)
		  {
		  	$createAction = TRUE;
		  	$actionArr = array( 
		  			"action"	     => $data['short_description'],
		  			"action_deliverable" => $data['short_description'],
		  			"deadline" 	     => $data['actiondeadlinedate'],
		  			"owner" 	     => $data['actionowner'],	   
		  		);				  				  
		  } 
		}
        
		unset($data['hasActions']);				  
		unset($data['actiondeadlinedate']);
		unset($data['actionowner']);   

		foreach($data as $key => $deliverable)
		{
			if(array_key_exists($key, $multiple)) {
				if(is_array($deliverable) && !empty($deliverable)){
					$insertData[$key] = $deliverable;
				}
			} else {
			  $insertdata[$key] = $deliverable;
			}      		
		}    	
		$res = $this->deliverableObj->saveDeliverable($insertdata);
		if( $res > 0){
			if( $createAction )
			{
				$actionArr['deliverable_id'] = $res;
				$actionObj = new Action();        		
				$actionRes = $actionObj -> save($actionArr); 
			}
			//save the multiple fields into the relationship tables 
			foreach($multiple as $k => $multipleArr)
			{
			   if(isset($insertData[$k]) && !empty($insertData[$k]))
			   {
				foreach($insertData[$k] as $index => $val)
				{
					$dRes = $this->deliverableObj->insert($multipleArr['t'], array("deliverable_id" => $res, $multipleArr['key'] => $val) );
				}		
			   }
			}	
		$response = array("text" => "Deliverable saved", "error" => false);
		} else {
		$response = array("text" => "Error saving deliverables", "error" => true);
		}
		return $response; 
	}        
      
}
?>
