<?php
/**
	@author : admire
**/
class Action extends DBConnect
{
	/**
		@var int
	**/	
	protected $id;
	/**
		@var int
	**/	
	protected $deliverable_id;
	/**
		@var char
	**/	
	protected $name;
	/**
		@var char
	**/	
	protected $owner;
	/**
		@var char
	**/	
	protected $measurable;
	/**
		@var int
	**/	
	protected $status;
	/**
		@var int
	**/	
	protected $progress;
	/**
		@var date
	**/	
	protected $deadline;
	/**
		@var date
	**/	
	protected $remind_on;
	/**
		@var int
	**/	
	protected $active;
	/**
		@var char
	**/	
	protected $insertuser;
	/**
		@var date
	**/	
	protected $insertdate;
	
	const ACTION_APPROVED = 2;
	
	const ACTION_AWAITING = 4;
	
	const ACTION_DECLINED = 8;	
	
	function __construct()
	{
		parent::__construct();
	}
	
	function save( $data )
	{
		$res = $this -> insert("action", $data);
		return $this -> insertedId();
	}
	
	function getActions( $start, $limit, $id, $conditions )
	{
		$query = "";
		if($conditions != ""){
			$query = "AND $conditions";
		} else {
			$query = "";
		}
		$query .= " AND A.actionstatus & 2 <> 2";
		$result = $this -> get("SELECT 
								 A.id AS ref,
								 A.id AS action_ref,
								 A.action AS action_required, 
								 A.progress, 
								 A.deadline AS action_deadline_date,
								 A.status,
								 A.owner,
								 A.reminder AS action_reminder,
								 AST.name AS action_status,
								 A.actionstatus,
								 A.action_deliverable,
								 TK.name action_owner
								 FROM #_action A
								 INNER JOIN #_action_status AST ON AST.id = A.status
								 LEFT JOIN #_action_owner TK ON TK.id = A.owner
								 WHERE deliverable_id = '".$id."'
								 $query
								 LIMIT $start, $limit
								 ");								 
								 
		return $result;
	}
	
	function getTotalActions( $id , $conditions)
	{

		$result = $this -> getRow("SELECT 
								   COUNT(*) AS total
								   FROM #_action A
								   INNER JOIN #_action_status AST ON AST.id = A.status
								   LEFT JOIN #_action_owner TK ON TK.id = A.owner
								   WHERE deliverable_id = '".$id."'
								   $conditions 				 
								 ");
		return $result['total'];
	}
	
	function getAction( $id )
	{
		$result = $this -> getRow("SELECT 
								 A.id AS action_ref,
								 A.action AS action_required,
								 A.action AS action_name,
								 A.reminder,
								 A.deadline,
								 A.progress, 
								 A.deadline AS action_deadline_date,
								 A.status,
								 A.owner,
								 A.reminder AS action_reminder,
								 AST.name AS action_status,
								 A.actionstatus,
								 A.action_deliverable,
								 TK.name AS action_owner
								 FROM #_action A
								 INNER JOIN #_action_status AST ON AST.id = A.status
								 LEFT JOIN #_action_owner TK ON TK.id = A.owner
								 WHERE A.id = '".$id."'");
		return $result;
	}
	
	function getDeliverableActions( $deliverableid )
	{
		$result = $this -> get("SELECT * FROM #_actions WHERE deliverable_id = $deliverableid");
		return $result;
	}
	
	function getActionUpdate( $id )
	{
		$result = $this->get("SELECT 
							  AU.changes,
							  AU.response,
							  AU.insertdate,
							  AST.name AS status,
							  A.progress
							  FROM #_action_update AU 
							  INNER JOIN #_action A ON A.id = AU.action_id
							  INNER JOIN #_action_status AST ON AST.id = A.status
							  WHERE AU.action_id = $id
							  ORDER BY AU.insertdate DESC							  
							");

		return $result;
	}
	
	function getActionEdit( $id )
	{
		$result = $this->get("SELECT 
							  AU.changes,
							  AU.insertdate,
							  AST.name AS status,
							  A.progress
							  FROM #_action_edit AU 
							  INNER JOIN #_action A ON A.id = AU.action_id
							  INNER JOIN #_action_status AST ON AST.id = A.status
							  WHERE AU.action_id = $id
							  ORDER BY AU.insertdate DESC							  
							");
		return $result;
	}
	function getAuthorization( $id ) 
	{
		$response = $this -> get("SELECT 
								  AA.description,
								  AA.approval,
								  AA.insertdate,
								  CONCAT(TK.tkname,' ',TK.tksurname) AS changeBy,
								  M.name AS status
								  FROM #_action_authorization AA
								  INNER JOIN #_action A ON A.id = AA.action_id
								  INNER JOIN #_action_status M ON M.id = A.status
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser	
								  WHERE AA.action_id = $id							  
								  ORDER BY AA.insertdate DESC								  
								  ");							  
		return $response;
	}		
	
	function editActionChanges( $id, $insertdata, $updatedata)
	{
		$result	= "";
		$res 	= ""; 
		if(isset($updatedata['action_name'])){
			$updatedata['action'] = $updatedata['action_name'];
		}
		$updatedata['insertuser'] = $_SESSION['tid']; 
		$insertdata['insertuser'] = $_SESSION['tid']; 		
		unset($updatedata['action_name']);
		if(isset($insertdata)){
			$result = $this->insert("action_edit", $insertdata);
		}
		if(isset($updatedata)){
			$res	= $this->update("action", $updatedata, "id=$id");
		}		

		
		if( $result > 0 && $res > 0){
			return 1;
		} else if( $result > 0 && $res < 0){
			return 2;
		} else {
			return -1;
		}		
	}
	
	function updateActionChanges( $id, $insertdata, $updatedata)
	{
		$result = 0;
		$res	= 0;
		if(isset($insertdata)){
			$result = $this->insert("action_update", $insertdata);		
		}	
		if(isset($updatedata) && !empty($updatedata)){
			$updatedata['insertuser'] = $_SESSION['tid']; 
			$updatedata['insertuser'] = $_SESSION['tid']; 		
			$res  = $this->update("action", $updatedata, "id=$id");
		}	
		if( ($result > 0 && $res > 0) ){
			return 1;
		} else if( $result > 0 && $res <= 0 ){
			return 2;
		} else {
			return -1;
		}		
	}	
	
	function updateAction( $updatedata, $id)
	{
		$res	= $this->update("action", $updatedata, "id=$id");
		return $res;	
	}

	function authorizeAction( $insertdata )
	{
		$res	= $this->insert("action_authorization", $insertdata);
		return $this -> insertedId();
	}	
}
?>