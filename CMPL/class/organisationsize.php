<?php
class OrganisationSize extends DBConnect
{

   function fetchAll( $options )
   {
	   $innerJoin   = "";
	   $where = "";
	   if(!empty($options))
	   {
	     $innerJoin = "INNER JOIN #_orgsize_legislation SL ON SL.orgsize_id = OS.id";
	     $where     = " AND SL.legislation_id = $options";
	   }	   
		$results = $this->get("SELECT * FROM #_organisation_sizes OS $innerJoin WHERE 1 AND OS.status & 2 <> 2 $where");
		return $results;      
   }

	function getOrganisationSize()
	{
		$results = $this->get("SELECT * FROM #_organisation_sizes WHERE status & 2 <> 2");
		return $results;
	}
	
	function getAOrganisationSize( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_organisation_sizes WHERE id = $id");
		return $result;
	}

	function getOrganisationSizes( $ids )
	{
		$result  = $this->get("SELECT * FROM #_organisation_sizes WHERE id IN (".$ids.")");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}	
	
	function save( $insertdata )
	{
		$this->insert( "organisation_sizes", $insertdata );
		return $this->insertedId();
	}
	
	function updateOrganisationSizes( $id, $updatedata)
	{
        $data    = $this->getAOrganisationSize( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "organisation_sizes");
        $logsObj -> createLog();			
		$res = $this->update("organisation_sizes", $updatedata, "id=$id");
		return $res;
	}
	
	function searchSizes( $text )
	{
		$results = $this->get("SELECT id FROM #_organisation_sizes
		 					   WHERE name LIKE '%".$text."%'
		 					   OR description LIKE '%".$text."%'
							  ");
		return $results;	
	 }
}
?>
