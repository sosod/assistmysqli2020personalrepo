<?php
abstract class DeliverableFactory
{
	  
    protected $deliverableObj = null;	
		
   function __construct()
   {
   	$this->deliverableObj = new Deliverable();
   }	

  abstract function getDeliverables($start, $limit, $options = array()); 

  function __call( $methodname , $args )
  {
	 $classname  = substr($methodname, 3, -4);
	 $resultList = array();
	 if(class_exists($classname))
	 {
	    $classObj = new $classname();
	    $results  = $classObj -> fetchAll( $args[0] );
	    if(isset($results) && !empty($results))
	    {
	       //load a result list
	       foreach($results as $index => $val)
	       {
		  if(isset($val['name']))
		  {
		    $resultList[$val['id']] = $val['name'];
		  }
	       }    
	    }
	 }
	 return $resultList;
   }
	
  function sortDeliverables($deliverables, Naming $naming)
  {	
	$headers = $naming -> sortedNaming( $naming );	                
	$data    = array();        
	$data['deliverables'] = array();
	$data['headers']	  = array();
	foreach ($deliverables as $index => $deliverable)
	{
	  //$orgtype      = $this->getOrganisationTypesList($deliverable['ref']); 	   	  
	  $complianceto = $this->getComplianceToList($deliverable['ref']);	  	    
	  //$mainevent    = $this->getEventsList($deliverable['ref']);		   
	  $subevent     = $this->getSubEventList($deliverable['ref']);        
	  $accountables = $this->getAccountablePersonList($deliverable['ref']);        
	  foreach($headers as $key => $val)
	  {
	  	 //echo $key."\r\n\n";
	    if( $key == "applicable_organisation_type") {
	      $orgtype = "";
	      if(!empty($deliverable['applicable_org_type']))
	      {
				$orgtypeObj = new OrganisationTypes();   
				$otype      = $orgtypeObj->getAOrganisationType($deliverable['applicable_org_type']);  	      
				$orgtype   = $otype['name'];
	      }
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = $orgtype;					
	    } else if( $key == "compliance_name_given_to") {
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = implode(",", $complianceto);	    
	    } else if($key == "main_event") {
	      $mainevent = "";
	    	if(!empty($deliverable['main_event'])){
				$eventObj   = new Events();
				$event  = $eventObj->getAEvent($deliverable['main_event']);	  
				$mainevent = $event['name'];
			}
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = $mainevent;	    
	    } else if($key == "accountable_person"){
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = implode(",",$accountables);	    	    
	    } else if($key == "sub_event"){
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = implode(",",$subevent);	    	    	    
	    } else if($key == "responsible_department"){
	      $department = "";
	      if(!empty($deliverable['responsible_department']))
	      {
	      	$depObj = new Directorate();
	      	$directorates = $depObj->getDirectorateList();
				if(isset($directorates[$deliverable[$key]]))
				{
					$department = $directorates[$deliverable[$key]]['name'];
				} else {
					//if the department is not a main department , then check for it in the sub departments
					foreach($directorates as $dIndex => $dVal)
					{
						if(isset($dVal['sub'][$deliverable[$key]]))
						{
							$department = $dVal['sub'][$deliverable[$key]]['name'];
						}
					}
					
				} 
	      }
	      $data['headers'][$key]  = $val;
	      $data['deliverables'][$deliverable['ref']][$key] = $department;	    	    	    	    	
	    } else if($key == "legislation_compliance_date"){
			$data['headers'][$key]  = $val;	    
	    	if($deliverable['recurring'])
	    	{
			$recurranceTypes = array("fixed" 		    => "Fixed",
											 "start" 		    => "Start Of Financial Year", 
											 "end" 	 		    => "End Of Financial Year",
					 						 "startofmonth"    => "Start Of Month",
					 						 "endofmonth" 	    => "End Of Month",
											 "startofquarter"  => "Start Of Quarter",
											 "endofquarter"	 => "End Of Quarter",
											 "startofsemester" => "Start Of Semester",
											 "endofsemester"   => "End Of Semester", 
											 "startofweek"		 => "Start Of Week", 
											 "endofweek"		 => "End Of Week"
											); 	    	
	    	  $date = $deliverable['recurring_period']." ".$deliverable['days_options']." ".(isset($recurranceTypes[$deliverable['recurring_type']]) ? $recurranceTypes[$deliverable['recurring_type']] : "");
			  $data['deliverables'][$deliverable['ref']][$key] = $date;	    	    	 					    		
	    	} else {
			  $data['deliverables'][$deliverable['ref']][$key] = $deliverable[$key];	    	    	 	    		
	    	}
	    } else {
	    	 if($key !== "deliverable_import")
	    	 {
			 $data['headers'][$key]  = $val;
			 $data['deliverables'][$deliverable['ref']][$key] = $deliverable[$key];	    	    	 
	    	 }
	    }        		
	  }        	
   }      
   $data['columns'] = (isset($data['headers']) ? count($data['headers']) : "");  
  	return $data;
  }
  

    function _processDeliverableChanges( $postArr, $deliverable)
    {    	
        $headerObj  = new Naming();
        $headerObj->getNaming();
        $updatedata = array();
        $changeMessage = "";
        $changeArray    = array();
        $changeMessage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
        //$usersGroup = array("responsible", "responsibility_owner", "compliance_to", "accountable_person");
       /* if( is_array($postArr['main_event']) && !empty($postArr['main_event'])){
        	$postArr['main_event'] = implode(",", $postArr['main_event'] ); 
        }
        if( is_array($postArr['applicable_org_type']) && !empty($postArr['applicable_org_type'])){
        	$postArr['applicable_org_type'] = implode(",", $postArr['applicable_org_type'] ); 
        } */   
        $postArr['sub_event'] 		= ($postArr['sub_event'] == "" ? 0 : $postArr['sub_event']);
        $deliverable['sub_event']	= ($deliverable['sub_event'] == "" ? 0 : $deliverable['sub_event'] ); 
        foreach( $deliverable as $key => $delVal)
        {
            if(isset($postArr[$key]) || array_key_exists($key, $postArr))
            {
            	//echo $key." --- ".$postArr[$key]." => ".$delVal." \r\n\n";
                if($postArr[$key] !== $delVal)
                {
                    if($key == "compliance_frequency") {

                        $secObj  = new ComplianceFrequency();
                        $fromSec = $secObj ->getComplianceFrequency( $delVal );
                        $from    = $fromSec['name'];

                        $toSec   = $secObj ->getComplianceFrequency( $postArr[$key] );
                        $to      = $toSec['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "responsible") {

							$dirObj      = new Directorate();
							$fromDir     = $dirObj -> getSubDirectorate( $delVal );
							$from     	 = $fromDir['name'];

							$toDir   	= $dirObj -> getSubDirectorate( $postArr[$key]);
							$to       	= $toDir['name'];

							$changeArray['responsible_department'] = array("from" => $from, "to" => $to );
							$changeMessage   .= $headerObj -> setHeader('responsible_department')." changed to ".$to." from ".$from."\n\n\n\n";

                    } else if($key == "responsibility_owner") {

							$respoObj	= new ResponsibleOwner();
							$fromOwner  = $respoObj -> getResponsibleOwner( $delVal );
							$from     	= $fromOwner['name'];
							$toOwner   	= $respoObj -> getResponsibleOwner( $postArr[$key] );
							$to       	= $toOwner['name'];

							$changeArray[$key] = array("from" => $from, "to" => $to );
							$changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";

                    } else if($key == "compliance_to") {

							$compObj    = new ComplianceTo();
							$from  = $compObj -> getCompliances( $delVal );
							$to   = $compObj -> getCompliances( $postArr[$key] );
							$changeArray['compliance_name_given_to'] = array("from" => $from, "to" => $to );
							$changeMessage   .= $headerObj -> setHeader('compliance_name_given_to')." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "applicable_org_type") {

							$typeObj = new OrganisationTypes();
							$from  = "";
							$to    = "";
							if( $delVal == "" ) {
								$from = "";
							} else {
								$from  = $typeObj->getOrganisationType( $delVal );
							}

							if( $postArr[$key] == "") {
								$to = "";
							} else {
								$to = $typeObj->getOrganisationType( $postArr[$key] );	
							}
							//echo "from => ".$from." and to ".$to." \r\n\n";
							$changeArray[$key] = array("from" => $from, "to" => $to);
							$changeMessage  .= $headerObj->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";
                   } else if($key == "functional_service") {
 
                        $funcObj  = new FunctionalService();
                        $fromFunc = $funcObj -> getAFunctionalService( $delVal );
                        $from     = $fromFunc['name'];

                        $toFunc   = $funcObj -> getAFunctionalService( $postArr[$key] );
                        $to       = $toFunc['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                	 } else if($key == "accountable_person") {

							$accObj = new AccountablePerson();
							$from	= $accObj -> fetch( $delVal );
							$to		= $accObj -> fetch( $postArr[$key] );
		
							$changeArray[$key] = array("from" => $from, "to" => $to );
							$changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
						 } else if($key == "main_event") {
							$eventObj  = new Events();
							$to   = "";
							$from = "";
							if( $delVal == ""){
							$from = "";
							} else {
									$from = $eventObj ->getEvent( $delVal );
							}
							if( $postArr[$key] == ""){
								$to = "";
							} else {
						  		$to = $eventObj ->getEvent( $postArr[$key] );
							}
							$changeArray[$key] = array("from" => $from, "to" => $to );
							$changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                  } else if($key == "sub_event") {
                     $subEventObj  = new SubEvent();
                     $from    = $subEventObj ->getASubEvent( $delVal );

                     $to      = $subEventObj -> getASubEvent( $postArr[$key] );
                     $changeArray[$key] = array("from" => $from, "to" => $to );
                     $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";

                   }  else {
                     $changeArray[$key] = array("from" => $deliverable[$key], "to" => $postArr[$key] );
                     $changeMessage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$deliverable[$key]."\n\n\n\n";
                        //$insertdata[$key]  = ;
                    }
                }
            }
        }      
        $changes = array("change" => $changeArray, "message" => $changeMessage );
        return $changes;
    }


}
?>
