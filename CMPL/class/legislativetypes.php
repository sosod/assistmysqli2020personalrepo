<?php
class LegislativeTypes extends DBConnect
{
	
	function getLegislativeTypes()
	{
		$results = $this->get("SELECT * FROM #_legislative_types WHERE status & 2 <> 2");
		return $results;
	}
	
	function getALegislativeType( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_legislative_types WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "legislative_types", $insertdata );
		return $this->insertedId();
	}
	
	function updateLegislativeType( $id, $updatedata)
	{
        $data    = $this->getALegislativeType( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "legislative_types");
        $logsObj -> createLog();			
		$res = $this->update("legislative_types", $updatedata, "id=$id");
		return $res;
	}

}
?>