<?php
include_once("../loader.php");
spl_autoload_register("Loader::autoload");
class Controller {
	private static $instance;
	
	private function __construct()
	{
		//self::$instance  = new SetupController();
	}
	
	public static function getInstance()
	{
		if(!self::$instance)
		{
			self::$instance = new Controller();
		}
		return self::$instance;
	} 
	
	function call( $classname, $methodname, $args )
	{
	  try{
		   $params 	= $this->prepareUpdate( $args );
		   $reflection = new ReflectionMethod($classname, $methodname);
		   $results    = $reflection->invokeArgs(new $classname(), $params);
		   return $results;			
		} catch(Exception $e){
			return  array("text" => $e->getMessage(), "error" => true);	
		}
	}
	
	function prepareUpdate( $data )
	{	
		$updatedata = array();
		foreach( $data as $index => $value )
		{
			if( $index == "action")
			{
			   continue;
			} else {
				$updatedata[$index] = $value;			
			}
		}
		return $updatedata;
	}
	
	private function formatParameters( ReflectionMethod $method, $params )
	{
		//$args = 	
	}
	
}
