<?php
class LegislationColumns extends Naming
{

	function getNaming()
	{
		$sql = "";
		if( Naming::$section == "manage"){
			$sql = " AND active & ".Naming::MANAGECOLUMNS." = ".Naming::MANAGECOLUMNS."";
		} else if( Naming::$section == "new"){
			$sql = " AND active & ".Naming::NEWCOLUMNS." = ".Naming::NEWCOLUMNS."";
		}
		$results = $this->get("SELECT id, name , client_terminology, ignite_terminology, active
							   FROM #_header_names 
							   WHERE type = 'legislation'
							   $sql
							   ORDER BY position
							 ");
		return $results;		
	}

}
