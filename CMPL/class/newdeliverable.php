<?php
class NewDeliverable extends DeliverableFactory
{
	
  function __construct()
  {
     parent::__construct();
  }
  
  function getDeliverables($start, $limit, $options = array())
  {
    Naming::$section = $options['section'];
    $optionSql = "";
    if(isset($options) && !empty($options))
    {
       if(isset($options['id']) && !empty($options['id']))
       {
	   $optionSql .= " AND D.legislation = '".$options['id']."'";
       }	
    }    
    $deliverables    = $this->deliverableObj->getDeliverables($start, $limit, $optionSql );

    $data     = $this->sortDeliverables( $deliverables, new DeliverableColumns() );
    $totalDel = $this->deliverableObj->getTotalDeliverables( $optionSql );     	
    $data['total'] = $totalDel['total'];
    return $data;
  } 

 function saveDeliverable( $data )
 {
	 $createAction = FALSE;
	 $multiple = array("sub_event" 		=> array("t" => "subevent_deliverable", "key" => "subevent_id"),
				    "compliance_to" 	=> array("t" => "complianceto_deliverable", "key" => "complianceto_id"),
				    "accountable_person" => array("t" => "accountableperson_deliverable", "key" => "accountableperson_id")
		  );
		foreach( $data as $key => $deliverable)
		{
		  if( $key == "hasActions" && $deliverable == 0)
		  {
		  	$createAction = TRUE;
		  	$actionArr = array( 
		  			"action"	           => $data['short_description'],
		  			"action_deliverable" => $data['short_description'],
		  			"deadline" 	     => $data['actiondeadlinedate'],
		  			"owner" 	          => $data['actionowner'],	   
		  		);				  				  
		  } 
		}
        
		unset($data['hasActions']);				  
		unset($data['actiondeadlinedate']);
		unset($data['actionowner']);   

		foreach($data as $key => $deliverable)
		{
			if(array_key_exists($key, $multiple)) {
				if(is_array($deliverable) && !empty($deliverable)){
					$insertData[$key] = $deliverable;
				}
			} else {
			  $insertdata[$key] = $deliverable;
			}      		
		}    		
		$res = $this->deliverableObj->saveDeliverable($insertdata);
		if( $res > 0){
			if( $createAction )
			{
				$actionArr['deliverable_id'] = $res;
				$actionObj = new Action();        		
				$actionRes = $actionObj -> save($actionArr); 
			}
			//save the multiple fields into the relationship tables 
			foreach($multiple as $k => $multipleArr)
			{
			   if(isset($insertData[$k]) && !empty($insertData[$k]))
			   {
				foreach($insertData[$k] as $index => $val)
				{
					$dRes = $this->deliverableObj->insert($multipleArr['t'], array("deliverable_id" => $res, $multipleArr['key'] => $val) );
				}		
			   }
			}	
		$response = array("text" => "Deliverable saved", "error" => false);
		} else {
		$response = array("text" => "Error saving deliverables", "error" => true);
		}
		return $response; 
	}   

}
?>
