<?php
class Deliverable extends DBConnect
{
	function getDeliverables($start, $limit, $optionSql = "" )
	{
		$results = $this -> get("SELECT D.id,
										 D.id AS ref,
										 D.id AS deliverable_ref,
										 D.description AS description_of_legislation,
										 D.short_description AS short_description_of_legislation,
										 D.legislation_section,								 
										 CF.name AS compliance_frequency,
										 CF.name AS frequency_of_compliance,
										 D.compliance_date,
										 D.compliance_date AS legislation_compliance_date,
										 FC.name AS functional_service,
										 D.accountable_person,
										 RO.name AS responsibility_ower,
										 D.legislation_deadline,
										 D.legislation_deadline AS legislative_deadline_date,
										 D.days_options,
										 D.recurring_type,
										 D.recurring_period,
										 D.recurring,
										 D.reminder,
										 D.sanction,
										 D.reference_link,
										 D.assurance,
										 D.applicable_org_type AS applicable_organisation_type,
										 D.applicable_org_type,
										 D.main_event,
										 D.sub_event,
										 D.compliance_to AS compliance_name_given_to,
										 D.guidance,
										 D.insertdate,
										 D.responsible AS responsible_department
								 		 FROM #_deliverable D
										 LEFT JOIN #_functional_service FC ON FC.id = D.functional_service
										 LEFT JOIN #_compliance_frequency CF ON CF.id = D.compliance_frequency
										 LEFT JOIN  #_responsible_owner RO ON RO.id = D.responsibility_owner							 
										 LEFT JOIN  #_events ME ON ME.id = D.main_event
										 LEFT JOIN  #_sub_event SE ON SE.id = D.sub_event
										 WHERE D.status & 2 <> 2 
										 $optionSql
										 LIMIT $start, $limit
					 ");	
		return $results;
		
	}
	function getTotalDeliverables( $optionSql )
	{
		$results = $this->getRow("SELECT COUNT(*) AS total
					FROM #_deliverable D
					LEFT JOIN #_functional_service FC ON FC.id = D.functional_service
					LEFT JOIN #_compliance_frequency CF ON CF.id = D.compliance_frequency
					LEFT JOIN #_responsible_owner RO ON RO.id = D.responsibility_owner							 
					LEFT JOIN #_events ME ON ME.id = D.main_event
					LEFT JOIN #_sub_event SE ON SE.id = D.sub_event
					WHERE D.status & 2 <> 2 
					$optionSql"
		);						
		return $results;
	}
	
	function getDeliverable( $id, $conditions )
	{
		$results = $this -> getRow("SELECT D.id AS ref ,
					 D.description AS description_of_legislation,
					 D.short_description AS short_description_of_legislation,
					 D.legislation_section,								 
					 CF.name AS compliance_frequency,
					 D.applicable_org_type AS applicable_organisation_type,
					 D.applicable_org_type,
					 D.compliance_date,
					 FC.name AS functional_service,
					 D.accountable_person,
					 RO.name AS responsibility_ower,
					 D.legislation_deadline,
					 D.days_options,
					 D.reminder,
					 D.sanction,
					 D.reference_link,
					 D.assurance,
					 D.compliance_to,
					 D.main_event,
					 D.responsible AS responsible_department,
					 SE.name AS sub_event,
					 D.sub_event AS subevent,
					 D.guidance,
					 D.insertdate
			 		 FROM #_deliverable D
			 		 INNER JOIN #_legislation L ON L.id = D.legislation
					 INNER JOIN #_functional_service FC ON FC.id = D.functional_service
					 LEFT JOIN #_compliance_frequency CF ON CF.id = D.compliance_frequency
					 INNER JOIN  #_responsible_owner RO ON RO.id = D.responsibility_owner
					 LEFT JOIN  #_sub_event SE ON SE.id = D.sub_event
					 WHERE D.id = $id"
				 );		
		return $results;
	}
	
	function getADeliverable( $id )
	{
		$result = $this->getRow("SELECT * FROM #_deliverable WHERE id = $id");
		return $result;		
	}
	
	function saveDeliverable($insertdata)
	{
		$this->insert("deliverable", $insertdata);
		return $this->insertedId();
	}

	function updateDeliverable($insertdata, $id)
	{
		$res  = $this->update("deliverable", $insertdata, "id=$id");
		return $res;
	}
	
	function editDeliverable( $id, $data, $insertdata){
		$result = "";
		$res 	= "";
		if(isset($data) && !empty($data)){
			$result = $this->insert("deliverable_edit", $data );
		} else{
			$result = 1;
		}
		if(isset($insertdata) && !empty($insertdata)){
			$res  = $this->update("deliverable", $insertdata, "id=$id");		
		} else {
			$res = 1;
		}

		if( $result > 0 && $res > 0){
			return 1;
		} else if( $result > 0 && $res < 0){
			return 2;
		} else {
			return -1;
		}
	}
    
	function getEditLogs( $id ){
		$result = $this->get("SELECT
					  DE.changes,
					  DE.insertuser,
					  DE.insertdate
					  FROM #_deliverable_edit DE
					  INNER JOIN #_deliverable D ON DE.deliverable_id = D.id
					  WHERE deliverable_id = $id
					  ORDER BY DE.insertdate DESC
					  ");
		return $result;
	}
}
?>
