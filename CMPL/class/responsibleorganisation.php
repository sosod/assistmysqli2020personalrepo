<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */
 
class ResponsibleOrganisation extends DBConnect {

    function save( $insertdata )
    {
        $this->insert("organisation", $insertdata);
        return $this->insertedId();
    }

    function getResponsibleOrganisations()
    {
        $results = $this -> get("SELECT * FROM #_organisation WHERE status & 2 <> 2");
        return $results;
    }
    
    function getSortedResponsibleOrganisations()
    {
    	$compliances = $this->getResponsibleOrganisations();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = array("name" => $compliance['name'], "id" => $compliance['id'] );
		}
		return $compArr;
    }
    
    function getResponsibleOrganisationsList()
    {
    	$compliances = $this->getResponsibleOrganisations();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = $compliance['name'];
		}
		ksort($compArr);
		return $compArr;
    }

    function getAResponsibleOrganisation( $id )
    {
        $result = $this->getRow("SELECT * FROM #_organisation WHERE id = $id");
        return $result;
    }

    function updateResponsibleOrganisation( $id, $updatedata )
    {
        $data    = $this->getAResponsibleOrganisation( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "organisation");
        $logsObj -> createLog();	  	
        $res = $this->update("organisation", $updatedata, "id={$id}");
        return  $res;
    }
    
    function updateUserBusinessPartner( $data, $id )
    {
        $res = $this->update("user_businesspartner", $data, "id=$id");
		return $res; 
    }
    
    function saveUserBusinessPartner( $data )
    {
        $res = $this->insert("user_businesspartner", $data );
		return $this->insertedId();    	
    }
    
    function getResponsibleOrganisationUsers( $id )
    {
    	$results = $this->get("SELECT * FROM #_user_businesspartner WHERE business_partner_id = $id"); 
    	return $results;    	
    }

    function getUserReponsibleOrg()
    {
    	$results = $this->get("SELECT UBP.id, UBP.user_id, UBP.business_partner_id, O.name, O.description 
    						   FROM #_user_businesspartner UBP
    						   INNER JOIN #_organisation O ON O.id = UBP.business_partner_id
    						   WHERE UBP.status & 2 <> 2
    						 ");
    	return $results;
    }
	
	function getBusinessPartner( $userid )
	{
		$result = $this->get("SELECT UBP.id, UBP.user_id , UBP.business_partner_id 
							  FROM #_user_businesspartner UBP
							  WHERE UBP.user_id IN( $userid )
							 ");
		return $result;
	}
    
    function getBussinessPartners()
	{
		$results = $this->get("SELECT * FROM #_user_businesspartner");
		return $results;
	}
	
	function getBusinessPartnetById( $id )
	{	
		$result = $this->getRow("SELECT * FROM #_user_businesspartner WHERE id = $id");
		return $result;
	}
}
