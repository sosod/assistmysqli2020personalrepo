<?php 
class Frequency extends DBConnect
{
	
	function getFrequency()
	{
		$results = $this->get("SELECT * FROM #_frequency WHERE status & ".DBCOnnect::DELETE." <> ".DBConnect::DELETE."");
		return $results;
	}
	
	function getAFrequency( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_frequency WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "frequency", $insertdata );
		return $this->insertedId();
	}
	
	function updateFrequency( $id, $updatedata)
	{
    	$data    = $this->getAFrequency( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "frequency");
        $logsObj -> createLog();		
		$res = $this->update("frequency", $updatedata, "id=$id");
		return $res;
	}
}
?>