<?php
abstract class Response{

	const ACTIVE 		= 1;
	
	const DELETED  		= 2;
	
	const DEACTIVATED   = 4;
	
	const DEFAULTS      = 8;
	
	const APPROVED      = 16;
	
	protected $status;
	
	protected $headers = array(); 
	
	function output( $data )
	{		
		$response = $this->showResponse( $data );
		return  $response ;
	}

	
	function statusMessages()
	{
		$stauses = array(
				1 	=> "Activated",
				2 	=> "Deleted",
				4   => "Deactivated",
				8	=> "Default",
				16  => "Approved"		
		
		);
		return $this->status;
	}
	
	function getStatus( $status, $message = "")
	{
		if(isset($this->status[$status]))
		{
			echo $this->status[$status];
		}
	}
	
	abstract function showResponse( $data );	
}
