<?php
include_once("../loader.php");
spl_autoload_register("Loader::autoload");
class Request{
		
	public $request = array();
	
	private $serverRequest = array();
	
	private $requestMethod = "";
	
	private $responseType  = "";
	
	private $class = "";
	
	private $action = "";
	
	function __construct()
	{
		$this->request 		 = $_REQUEST;
		$this->requestMethod = $_SERVER['REQUEST_METHOD'];
		$this->serverRequest = $_SERVER['QUERY_STRING']; 	
		$this->responseType  = $_SERVER['HTTP_ACCEPT']; 	
				
	} 
	
	function getResponse()
	{
		$response = "";
		if( $this->responseType == "*/*"){
			$response = "json";
		} else{
			list($appResponse, $scriptType, $accept) = explode(",", $this->responseType);
			if(strstr($appResponse, "application"))
			{
				list($type, $method) = explode("/", $appResponse);
				$response = $method;	
			} else {
				$response = "json";
			}
		}

		return ucwords( $response )."Response";
	}
	
	function getRequest( $key )
	{
		return $this->request[$key];
	}
	
	function setRequest($key, $value)
	{
		$this->request[$key] = $value;
	}
	
	function getClassName()
	{
		return $this->class;
	}
	
	function getActionName()
	{
		return $this->action;
	}
	
	function processRequest(Response $responseBuilder)
	{
		//$responseBuilder = new JsonResponse();
		try{
			list($class, $method) = explode(".", $this->request['action'] );
			
			$controller    = Controller::getInstance();			
			$returnedData  = $controller->call( ucwords($class), $method, $this->request); 
			$response 	  = $responseBuilder->output(  $returnedData  );
		} catch( Exception $e){
			$response = $responseBuilder->showResponse(  $e  );
		}
		return $response;		
	}
	

}
$request = new Request();
$responseMethod = $request ->getResponse();
$result  = $request -> processRequest(new $responseMethod() );
echo $result;
