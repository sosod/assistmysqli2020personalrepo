<?php 
class Events extends DBConnect
{
	
	function fetchAll( $options = "")
	{
		$innerJoin = "";
		$where  = "";
		if( $options != "" )
		{
		   $innerJoin = " INNER JOIN #_event_deliverable ED ON ED.event_id = E.id";
		   $where = " AND ED.deliverable_id = '".$options."'";
		}	
		$results = $this->get("SELECT * FROM #_events E $innerJoin WHERE 1 $where AND E.status & 2 <> 2");
		return $results;	
	}
	
	
	function getEvents()
	{
		$results = $this->get("SELECT * FROM #_events WHERE status & ".DBCOnnect::DELETE." <> ".DBConnect::DELETE." ORDER BY name");
		return $results;
	}
	
	function getEvent( $id )
	{
		$sql = "";
		if(!empty($id))
		{
			$sql = " id IN(".$id.")"; 
		} else {
			$sql = " id IN(0)";
		}
		$result  = $this->get("SELECT * FROM #_events WHERE $sql");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}	
	
	
	function sortedEvents()
	{
		$evs = $this->getEvents();
		$events = array();
		foreach($evs as $k => $ev)
		{
			$events[$ev['id']] = array("name" => $ev['name'] , "id" => $ev['id']);
		}
		return $events;
	}
	
	function getEventsList()
	{
		$evs = $this->getEvents();
		$events = array();
		foreach($evs as $k => $ev)
		{
			$events[$ev['id']] = $ev['name'];
		}
		ksort($events);
		return $events;
	}
	
	function getAEvent( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_events WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "events", $insertdata );
		return $this->insertedId();
	}
	
	function updateEvent( $id, $updatedata)
	{
        $data    = $this->getAEvent( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "events");
        $logsObj -> createLog();	 		
		$res = $this->update("events", $updatedata, "id=$id");
		return $res;
	}
}
?>
