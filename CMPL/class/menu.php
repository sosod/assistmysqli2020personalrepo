<?php
/*
	@author 	: admire
*/
class Menu extends DBConnect
{
	function __construct()
	{
		parent::__construct();		
	}
	
	function getSubMenu( $type )
	{
		$results =  $this -> get("SELECT * FROM #_menu WHERE type = '".$type."' AND parent_id <> 0 ");
		return $results;
	}
	
	function getMenu()
	{
		$results = $this->get("SELECT * FROM #_menu");
		return $results;
	}
	
	function getAMenu( $id )
	{
		$result = $this->getRow("SELECT * FROM #_menu WHERE id=$id");
		return $result;		
	}
	
	function updateMenu( $id , $updateData)
	{
        $data    = $this->getAMenu( $id );
        $logsObj = new Logs( $_POST , $data, "menu");
        $logsObj -> createLog();	
		$res = $this-> update( "menu", $updateData, "id=$id");
		return $res;
	}
	
	function wasWhere( $requestArr, $otherNames )
	{
		$secondMenu = "";
		$name 		= array();
		if($_SERVER['QUERY_STRING'] == "")
		{
			unset($_SESSION['secondMenu']);
			unset($_SESSION['firstMenu']);
			unset($_SESSION['thirdMenu']);
			
			unset($_SESSION['firstName']);
			unset($_SESSION['thirdName']);
			unset($_SESSION['secondName']);
			$_SESSION['firstMenu'] = $_SERVER['REQUEST_URI'];
			$_SESSION['firstName'] =   $this->niceName( $requestArr, $otherNames);//$requestArr[2];//$this->niceName( $requestArr, $otherNames);			
		} else if(strstr($_SERVER['QUERY_STRING'],"&")){
			$_SESSION['thirdMenu'] = $_SERVER['REQUEST_URI'];			
			$_SESSION['thirdName'] =  $this->niceName( $requestArr, $otherNames);
		} else {
			unset($_SESSION['secondMenu']);
			unset($_SESSION['thirdMenu']);
			unset($_SESSION['thirdName']);
			$_SESSION['secondMenu'] = $_SERVER['REQUEST_URI'];	
			$_SESSION['secondName'] = $this->niceName( $requestArr, $otherNames);		
		}
		echo "<a href='".$_SESSION['firstMenu']."' class='breadcrumbs'>".$_SESSION['firstName']."</a>".(isset($_SESSION['secondMenu']) ? "&nbsp;>>&nbsp;<a href='".$_SESSION['secondMenu']."' class='breadcrumbs'>".$_SESSION['secondName']."</a>" : "").(isset($_SESSION['thirdMenu']) ? "&nbsp; >>&nbsp;<a href='".$_SESSION['thirdMenu']."' class='breadcrumbs'>".$_SESSION['thirdName']."</a>" : "");		
	}
	//create a nice name for the menu , 
	//Changes php files into nice name as defined in the other names array
	//if the name is not in the other menu , then it just removes the extension and makes a name out of it
	function niceName( $requestStr , $names)
	{		
		$currentMenu = "";
		foreach($requestStr as $key => $value){
		if(strpos($value, ".") > 0){
			$page = substr( $value, 0 ,strpos($value, ".") );
			if($page == "index"){
				$currentMenu = ucwords($requestStr[$key-1]);
			} else if( isset($names[$page])  ){
				$currentMenu = $names[$page]; 
			} else {
				if( strpos($page, "_") > 0){
					$page = str_replace("_"," ", $page);
				}
				$currentMenu = ucwords( $page );
			}
		} else if($value == "glossary"){
			$currentMenu = "Glossary";
		} else if($value == "search"){
            $currentMenu = "";
		}
		}
		return $currentMenu;		
	} 
		
}
?>