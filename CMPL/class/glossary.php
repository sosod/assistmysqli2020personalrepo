<?php
class Glossary extends DBConnect
{
	
	function construct()
	{
		
	}
	
	function getGlossaries()
	{
		$response = $this->get("SELECT * FROM #_glossary WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."");
		return $response;
	}
	
	function getGlossary( $id )
	{
		$response = $this->getRow("SELECT * FROM #_glossary WHERE id = $id");
		return $response;
	}
	
	function updateGlossary( $id, $data)
	{
        $dataA    = $this->getGlossary( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $dataA, "glossary");
        $logsObj -> createLog();			
		$response = $this->update("glossary", $data, "id=$id");
		return $response;	
	}	
	
	function save( $data )
	{
		$this->insert("glossary", $data);
		return $this->insertedId();
	}
}
?>