<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 8:26 PM
 * To change this template use File | Settings | File Templates.
 */
 
class ResponsibleOwner extends DBConnect{


        function getResponsibleOwners()
        {
            $results = $this->get("SELECT * FROM #_responsible_owner WHERE status & 2 <> 2 ORDER BY id");
            return $results;
        }

        function save( $data )
        {
            $res = $this->insert("responsible_owner", $data);
            return $this->insertedId();
        }

	    function sortedOwners()
	    {
	    	$compliances = $this->getResponsibleOwners();
			$compArr = array();
			foreach($compliances as $k => $compliance)
			{
				$compArr[$compliance['id']] = array("name" => $compliance['name'], "id" => $compliance['id'] );
			}
			return $compArr;
	    }    

	    function getOwnersList()
	    {
	    	$compliances = $this->getResponsibleOwners();
			$compArr = array();
			foreach($compliances as $k => $compliance)
			{
				$compArr[$compliance['id']] = $compliance['name'];
			}
			ksort($compArr);
			return $compArr;	    
	    }

        function getResponsibleOwner( $id )
        {
            $results = $this->getRow("SELECT * FROM #_responsible_owner WHERE id = '".$id."'");
            return $results;
        }

        function updateResponsibleOwner( $id, $updateData )
        {
	    	$data    = $this->getResponsibleOwner( $id );
	        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
	        $logsObj = new Logs( $postArr , $data, "responsible_owner");
	        $logsObj -> createLog();	        	
            $res = $this->update("responsible_owner", $updateData, "id=$id" );
            return $res;
        }
}
