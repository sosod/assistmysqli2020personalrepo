<?php
class LegislationSection extends DBConnect{

	function getLegislationSections(){
	  $results = $this -> get("SELECT * FROM #_legislation_section WHERE status & 2 <> 2");
	  return $results;	
	}
	
	function save( $insertdata ){
		$this -> insert( "legislation_section", $insertdata);
		return $this -> insertedId();
	}

}
?>