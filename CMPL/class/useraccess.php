<?php
class UserAccess extends DBConnect
{
	//defining user access constants
	const MODULE_ADMIN 		 = 1;//1000000000	
	const CONTRACT_MANAGER   = 2;   //00000001
	const CREATE_CONTRACT    = 4;   //00000010
	const CREATE_DELIVERABLE = 8;   //00000100
	const APPROVAL			 = 16;  //00001000
	const VIEW_ALL		     = 32;	//00010000		 
	const EDIT_ALL			 = 64;  //00100000
	const REPORTS			 = 128; //01000000
	const ASSURANCE 		 = 256; //10000000
	const SETUP		 		 = 512; //100000000	
	const DELETED 		 	 = 2048;//1000000000	
	
	function __construct()
	{
		parent::__construct();
	}

	function getUsers()
	{
		$response = $this -> get("SELECT 
								   TK.tkid,
								   CONCAT(TK.tkname, ' ', TK.tksurname) AS tkname,
								   TK.tkstatus ,
								   TK.tkemail ,
								   UA.status
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   LEFT JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user_id = TK.tkid
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'"	
								   );								   
		return $response;
	}
	
	function getAllUsers()
	{
		$users = $this->getUsers();
		$us    = array();
		foreach($users as $id => $usersArr)
		{
			$us[$usersArr['tkid']] = array("tkname" => $usersArr['tkname'], "id" => $usersArr['tkid'] );
		}
		return $us;
	}
	
	function getUserAccessSettings()
	{
		$response = $this -> get("SELECT UA.id,
								  UA.status, 
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								  FROM #_useraccess UA 
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								  WHERE status & ".UserAccess::DELETED." <> ".UserAccess::DELETED."");
		return $response;
	}
	
	function getUser( $userId ){
		$response = $this -> getRow("SELECT 
									  CONCAT(TK.tkname, ' ', TK.tksurname) AS user,
									  TK.tkemail AS email
									  FROM 
									  assist_".$_SESSION['cc']."_timekeep TK 
									  WHERE tkid  = $userId ");
		return $response;	
	}
	
	function getUserAccessSetting( $id )
	{
		$response = $this -> getRow("SELECT 
								  UA.id,
								  UA.status, 
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								  FROM #_useraccess UA 
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								  WHERE id = $id ");
		return $response;	
	}
	
	function getUserAccess()
	{
		$response = $this -> get("SELECT * FROM #_useraccess WHERE user_id = '".$_SESSION['tid']."'");
		return $response;
	}

	function save( $data )
	{
		$response = $this -> insert("useraccess", $data);
		return $this -> insertedId();
	}
	
	function updateUserAccess( $id, $data)
	{
		$response = $this -> update("useraccess", $data, "id=$id");
		return $response;
	}
	
	function getUsersByIds( $ids )
	{
		$results = $this->get("SELECT CONCAT(tkname,' ', tksurname) AS user, tkid FROM assist_".$_SESSION['cc']."_timekeep WHERE tkid IN (".$ids.")");
		return $results;
	}

}
?>