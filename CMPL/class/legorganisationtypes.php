<?php
class LegOrganisationTypes extends DBConnect
{
	
	function getLegOrganisationTypes()
	{
		$results = $this->get("SELECT * FROM #_leg_organisation_types WHERE status & 2 <> 2");
		return $results;
	}
	
	function sortedOrgTypes()
	{
		$types = $this->getLegOrganisationType();
		$appTypes = array();
		foreach($types as $k => $type)
		{
			$appTypes[$type['id']] = array("name" => $type['name'], "id" => $type['id'] );
		}
		return $appTypes;
	}
	
	function getOrganisationType( $id )
	{
		$result  = $this->get("SELECT * FROM #_leg_organisation_types WHERE id IN(".$id.")");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}	
	
	function getALegOrganisationType( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_leg_organisation_types WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "leg_organisation_types", $insertdata );
		return $this->insertedId();
	}
	
	function updateLegOrganisationType( $id, $updatedata)
	{
        $data    = $this->getALegOrganisationType( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "leg_organisation_types");
        $logsObj -> createLog();			
		$res = $this->update("leg_organisation_types", $updatedata, "id=$id");
		return $res;
	}

	function searchTypes( $text )
	{
		$results = $this->get("SELECT id
							   FROM #_leg_organisation_types 
							   WHERE name LIKE '%".$text."%'
							   OR description LIKE '%".$text."%'
							 ");
		return $results;	
	}
}
?>