<?php
Class Logs extends DBConnect
{

    protected $tableName;

    protected $postArr = array();

    protected $objectInContext;

    protected $setupId;

    function __construct( $postArr, $dataArr, $table )
    {
        parent::__construct();
        $this->postArr 		   = $postArr;
        $this->objectInContext = $dataArr;
        $this->tableName	   = $table;
    }

    function createLog()
    {
        $keyPost                = key($_POST);
        $this->setupId          = (isset($this->postArr['id']) ? isset($this->postArr['id']) : $_POST[$keyPost]);
        $this->saveLog();
    }

    function viewLogs( $table_name )
    {
        $this->tableName = $table_name;
        $results = $this -> get("SELECT setup_id, changes, insertdate FROM #_".$table_name." WHERE 1 ORDER BY insertdate DESC");
        $changes = array();
        foreach($results as $key => $updateArr){
            $changeMessage = $this->_extractChanges( $updateArr );
            $changes[$updateArr['insertdate']] = array(
                                                   "ref" 	 => $updateArr['setup_id'],
                                                   "date" 	 => $updateArr['insertdate'],
                                                   "changes" => $changeMessage
                                                 );
        }
        return $changes;
    }

    function saveLog()
    {
        $changesData = $this->_processChanges();
        $insertdata = array("changes" => base64_encode( serialize( $changesData['changes'] ) ), "setup_id" => $this->setupId , "insertuser" => $_SESSION['tid'] );
        $this->insert($this->tableName."_logs", $insertdata);
        return $this->insertedId();
    }

    function _processChanges()
    {
        $headers = new Naming();
        $headers->getNaming();
        $changes = array();
        $changeMessage = $_SESSION['tkn'];
        $changes['user'] = $changeMessage;
        //print "<pre>";
           // print_r($this->objectInContext);
           // print_r($this->postArr);
        // print "</pre>";
        foreach( $this->objectInContext as $key => $value)
        {
            if( isset($this->postArr[$key]) || array_key_exists($key, $this->postArr))
            {
                if($this->postArr[$key] != $value)
                {
                    if( $key == "status" || $key == "active"){
                        $fromStatus  = $this-> _checkStatus( $value);
                        $toStatus    = $this-> _checkStatus( $this->postArr[$key] );
                        $changeMessage = $headers ->setHeader($key)." changes to ".$toStatus." from ".$fromStatus."\r\n\n";
                        $changes[$key] = array("from" => $fromStatus, "to" => $toStatus );
                    } else {
                        $changeMessage = $headers ->setHeader($key)." changes to ".$this->postArr[$key]." from ".$value."\r\n\n";
                        $changes[$key] = array("from" => $value, "to" => $this->postArr[$key] );
                    }
                }
            }
        }
        return array("changes" => $changes, "message" => $changeMessage);
    }
	function _extractChanges( $updateArray )
	{
		$naming		= new Naming();
		$naming->getNaming();
		$adds		= array("user", "date_completed", "saving", "attachments");
		$changeMessage = "";
		if( isset($updateArray['changes']) && !empty($updateArray['changes']) ){
			$changesArr = unserialize( $updateArray['changes'] );

			foreach( $changesArr  as $index => $val ){
				if( in_array($index,$adds) ){
					$changeMessage .= $val."<br />";
				} else if( $index == "remindon" ){
					if(is_array($val)){
						$changeMessage .=  $naming ->setHeader($index)." has changed to ".$val['to']." from ".$val['from']."<br />";
					} else {
						$changeMessage .=  $naming ->setHeader($index)." ".$val."<br />";
					}
				} else if( $index == "response" ){
					$changeMessage .= $val."<br />";
				}else{
					if( isset($val['to']) && isset($val['from'])){

						$changeMessage .= $naming ->setHeader($index)." has changed to ".$val['to']." from ".$val['from']."<br />";
					}
				}
			}
		}
		return $changeMessage;
	}

    function _checkStatus( $value )
    {
        $status = "";
        if( $value == 2){
            $status = " deleted";
        } else if($value == 1){
            $status = " activated";
        } else if($value == 0){
            $status = " deactivated";
        }
        return $status;
    }

}
