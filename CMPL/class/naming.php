<?php
class Naming  extends DBConnect
{
	
	protected $headerArray;
	
	public static $section;
	//state of mange columns
	const MANAGECOLUMNS = 4;
	//state of the new columns
	const NEWCOLUMNS    = 8;
	
	function getNaming()
	{
		$results = $this -> get("SELECT id, name, client_terminology, ignite_terminology, active FROM #_header_names WHERE 1");
		$this->headerArray = $results;
		return $results;
	}	
	
	function getColumns()
	{
		$results = $this -> get("SELECT id, name, client_terminology, ignite_terminology, active FROM #_header_names");
		return $results;
	}
	
	function sortedNaming( $naming )
	{
		$names 		  = $naming -> getNaming();
		$headerNames  = array();
		foreach( $names as $key => $value)
		{
			$headerNames[$value['name']] = $value['client_terminology'];
		}
		return $headerNames;
	}
	
	function getHeaderNames()
	{
		$response = $this->get("SELECT name, client_terminology, ignite_terminology FROM #_header_names WHERE active  & 1 =  1" );
		$resData  = array();
		$this->headerArray = $response;
		foreach($response as $key => $header)
		{
			$resData[$header['name']] = ($header['client_terminology'] == "" ? $header['ignite_terminology'] : $header['client_terminology'] );
		}		

		return $resData;	
	}	
	
	function setHeader( $key )
	{
		$headerName = "";

		foreach($this->headerArray as $index => $value){
			if( trim($key) == trim($value['name']) ){
				$headerName = (isset($value['client_terminology']) ? $value['client_terminology'] : $value['ignite_terminology'] );
				break;
			} else {
				$headerName = ucwords( str_replace("_", " ", $key));
			}
		}
		
		return $headerName;
	}	
	
	function setThColuomns()
	{
		$results = $this -> get("SELECT id, name, client_terminology, ignite_terminology, active FROM #_header_names WHERE  1");		
		$this->headerArray = $results;
		//print "<pre>";
			//print_r($results);
		//print "</pre>";
		return $results;
	}
	
	function getANaming( $id )
	{
		$result = $this -> getRow("SELECT id, name, client_terminology, ignite_terminology, active FROM #_header_names WHERE id = $id");	
		return $result;
	}
	
	
	function updateNaming( $id, $updateData)
	{
        $data    = $this->getANaming( $id );
        $logsObj = new Logs( $_POST , $data, "header_names");
        $logsObj -> createLog();			
		$res = $this-> update( "header_names", $updateData, "id=$id");
		return $res;
	}

	function saveColumnsOrder( $activeColumns, $inactiveColumns) {	
		$activeArray = array();
		$resInAct	 = 0;
		$resAct		 = 0;
		foreach( $activeColumns as $key => $id ) {
			
			$resAct = $this -> update( "header_names", array( 
															 "active"	 	=> 1 ,
															 "insertuser"   => $_SESSION['tid'],
															 "position"		=> ($key + 1) 
															) ,"id = $id"); 
			}
		
		foreach( $inactiveColumns as $key => $id ) {
			if(is_numeric($id)){
				$resInAct = $this -> update( "header_names", array( 
																	"active" 		=> 0,
																	"insertuser"    => $_SESSION['tid'], 
																	) ,"id = $id"); 
			}

		}
		
	}

}
?>