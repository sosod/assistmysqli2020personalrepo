<?php
/*
	@author 	: admire
*/
class Legislation extends DBConnect
{
	protected $id;
	
	protected $name;
	
	protected $reference;
	
	protected $type;
	
	protected $category;
		
	protected $organisation_size;
	
	protected $legislation_date;
	
	protected $legislation_number;
	
	protected $organisation_type;
	
	protected $organisation_capacity;
	
	protected $hyperlink_to_act;
	
	protected $insertuser;
	
	const CONFIRMED  = 4;
	
	const ACTIVATED  = 8;
	
	
	function __construct()
	{
		$this->insertuser = $_SESSION['tid'];
		parent::__construct();
	}
	
	function save( $insertdata )
	{
		$insertdata['insertuser'] = $this->insertuser;
		$this->insert( "legislation", $insertdata );
		return $this->insertedId();
	}
	
	function getLegislations( $start, $limit, $conditions = "")
	{
		$results = $this->get("SELECT 
					L.id AS ref,
					L.name AS legislation_name,
					L.reference AS legislation_reference,
					L.legislation_date,
					L.legislation_number, 
					L.hyperlink_to_act,
					L.hyperlink_to_act AS hyperlink_act,
					LT.name AS legislation_type,			  
					L.business_partner,
					O.name AS responsible_organisation
					FROM #_legislation L 
					INNER JOIN #_legislative_types LT ON LT.id = L.type
					LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
					WHERE L.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
					$conditions
					LIMIT $start, $limit
					");
		return $results;
	} 
	
	function getTotalLegislation( $conditions )
	{
		$results = $this->getRow("SELECT COUNT(*) AS total
					  FROM #_legislation L 
					  INNER JOIN #_legislative_types LT ON LT.id = L.type
					  LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
					  WHERE L.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE."
					   $conditions
					  ");
			return $results;
	}
	
	function getALegislation( $id )
	{
		$results = $this->getRow("SELECT 
							  L.id AS ref,
							  L.type,
							  L.name AS legislation_name,
							  L.name,
							  L.reference,
							  L.reference AS legislation_reference,
							  L.legislation_date,
							  L.legislation_number, 
							  L.hyperlink_to_act,
							  LT.name AS legislation_type,
							  L.category,
							  L.organisation_size,
							  L.organisation_type,
							  L.organisation_capacity,
							  L.responsible_organisation AS responsibleorganisation,
							  O.name AS responsible_organisation,
							  L.status
							  FROM #_legislation L 
							  INNER JOIN #_legislative_types LT ON LT.id = L.type
							  LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
							  WHERE L.id = $id
							  ");						  
			return $results;
	} 
	
	function editLegislation( $insertdata, $updatedata, $id)
	{
		$res = "";
		$resEdit = "";
		$editId  = ""; 
		if(!empty($insertdata))
		{
			$resEdit = $this->insert("legislation_edit", $insertdata);
			$editId  = $this->insertedId(); 
		}
		
		if(!empty($updatedata))
		{
			$res = $this->update("legislation", $updatedata, "id=$id");
		}			
		return $res;
	}

	function updateLegislation( $updatedata, $id)
	{
		$res  = $this->update("legislation", $updatedata, "id=$id");
		return $res;
	}
	
	function getEditLogs( $id )
	{
		$results = $this->get("SELECT * FROM #_legislation_edit WHERE legislation_id = $id");		return $results;
	}
	
	function searchLegislations( $start, $limit, $text, $idsArray)
	{
		$sql = "";
		foreach( $idsArray as $key => $value )
		{
			if( $value != "")
			{
				$sql .= " OR L.".$key." LIKE  '%".$value."%' \r\n";	
			} 			
		}
				
		$results = $this->get("SELECT 
							  L.id AS ref,
							  L.name AS legislation_name,
							  L.reference AS legislation_reference,
							  L.legislation_date,
							  L.legislation_number, 
							  L.hyperlink_to_act,
							  LT.name AS legislation_type,
							  L.category,
							  L.organisation_size AS organisationsize,
							  L.organisation_type AS organisationtype,
							  L.organisation_capacity AS organisationcapacity ,							  
                              O.name AS responsible_organisation
							  FROM #_legislation L 
							  INNER JOIN #_legislative_types LT ON LT.id = L.type
							  LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
							  WHERE 1
							  AND (
							  		L.name LIKE '%".$text."%'
							  		OR L.reference LIKE '%".$text."%'
							  		OR L.legislation_date LIKE '%".$text."%'
							  		OR L.hyperlink_to_act LIKE '%".$text."%'
							  		OR L.legislation_number LIKE '%".$text."%'
							  		OR O.name LIKE '%".$text."%'
							  		OR LT.name LIKE '%".$text."%'
							  		$sql
							  )
							  LIMIT $start, $limit
							  ");
			return $results;
	}
	
	function advancedSearchLegislations( $start, $limit, $searchArray)
	{
		$sql = "";
		foreach( $searchArray as $key => $valueArr)
		{
			if( is_array($valueArr)){
				if( !empty($valueArr)){
					$sql .= " AND L.".$key." LIKE '".implode(",", $valueArr)."' \r\n";
				} 	
			} else {
				if( $valueArr != "" ){
					$sql .= " AND L.".$key." LIKE '".$valueArr."' \r\n";
				}
			}
		}	
		$results = $this->get("SELECT 
							  L.id AS ref,
							  L.name AS legislation_name,
							  L.reference AS legislation_reference,
							  L.legislation_date,
							  L.legislation_number, 
							  L.hyperlink_to_act,
							  LT.name AS legislation_type,
							  L.category,
							  L.organisation_size AS organisationsize,
							  L.organisation_type AS organisationtype,
							  L.organisation_capacity AS organisationcapacity ,							  
                              O.name AS responsible_organisation
							  FROM #_legislation L 
							  INNER JOIN #_legislative_types LT ON LT.id = L.type
							  LEFT JOIN #_organisation O ON O.id = L.responsible_organisation
							  WHERE 1
							  $sql 
							  LIMIT $start, $limit
							  ");
			return $results;		
	}
	
	function fetchAll($options = "")
	{
		$results = $this->get("SELECT * FROM #_legislation WHERE status & 1 = 1 $options");
		return $results;		
	}
	
}
?>
