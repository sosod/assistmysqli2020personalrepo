<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/16/11
 * Time: 10:26 PM
 * To change this template use File | Settings | File Templates.
 */
 
class FunctionalService extends DBConnect {

    function getFunctionalService()
	{
		$results = $this->get("SELECT * FROM #_functional_service WHERE status & 2 <> 2 ORDER BY id");
		return $results;
	}

	function sortedFunctionService()
	{
		$funcService  = $this->getFunctionalService();
		$fnService	  = array();
		foreach($funcService as $k => $func){
			$fnService[$func['id']] = array("name" => $func['name'], "id" => $func['id'] );
		}
		return $fnService;
	}
	
	function getFunctionServiceList()
	{
		$funcService  = $this->getFunctionalService();
		$fnService	  = array();
		foreach($funcService as $k => $func){
			$fnService[$func['id']] = $func['name'];
		}
		ksort($fnService);
		return $fnService;	
	}
	
	function getAFunctionalService( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_functional_service WHERE id = $id");
		return $result;
	}


	function save( $insertdata )
	{
		$this->insert( "functional_service", $insertdata );
		return $this->insertedId();
	}

	function updateFunctionalService( $id, $updatedata)
	{
    	$data    = $this->getAFunctionalService( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "functional_service");
        $logsObj -> createLog();			
		$res = $this->update("functional_service", $updatedata, "id=$id");
		return $res;
	}
}
