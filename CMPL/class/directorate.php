<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/18/11
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */

class Directorate extends DBConnect {

    
    function getDirectorate()
        {
            $defaults = new Defaults();
            $dirsub   = $defaults -> getDefaults();
            $toDir 	  = true;
            $toSub	  = false;
            foreach($dirsub as $index => $value )
            {
                if( $value['name'] == "risk_is_to_directorate" && $value['value'] == 1){
                    $toDir = true;
                }

                if( $value['name'] == "risk_is_to_subdirectorate" && $value['value'] == 1){
                    $toSub = true;
                    $toDir = false;
                }
            }
            if( $toDir ){
                $response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_dir D
                                       INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON  D.dirid = DS.subdirid
                                       WHERE DS.subhead = 'Y' AND DS.active = 1 AND D.active = 1 " );
            } else if( $toSub ){
                $response  = $this -> get("SELECT
                                           subid,
                                           CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS dirtxt,
                                           DR.active
                                           FROM ".$_SESSION['dbref']."_dir DR
                                           INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid
                                           WHERE DR.active = 1 AND DS.active = 1
                                         ");
            }


            $result = array_map("decodeDirectorate", $response);
            return $result;
        }
        
        function fetch( $id )
        {
            $response = $this->getRow("SELECT * FROM #_dir D
                                       WHERE D.id = {$id}
                                     ");
            return $response;        
        }
        
        function getDirectorates()
        {
            $response = $this->get("SELECT * FROM #_dir D
                                    WHERE D.status & 2  <> 2 
                                   ");
            return $response;
        }

        function getSubDirectorates()
        {
            $results  = $this -> get("SELECT  DR.id,
            						   DS.id AS ref,
                                       CONCAT_WS('-', DR.name, DS.name) AS dirtxt,
                                       DR.status
                                       FROM ".$_SESSION['dbref']."_dir DR
                                       INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.id = DS.dirid
                                       WHERE DR.status & 1 = 1 AND DR.status & 1 = 1
                                      ");
            $response = array();
           	foreach( $results as $key => $val)
           	{
           		if( substr($val['dirtxt'], -1 , 1) == "-")
           		{
           			$val['dirtxt'] = substr($val['dirtxt'],0, strpos($val['dirtxt'],"-"));
           		}
           		$response[$key] = $val; 
           	}
            return $response;
        }
		
        function getSubDirectoratesList()
        {
        	$results = $this->getSubDirectorates();
        	$directorates = array();
        	foreach( $results as $d => $dVal)
        	{
        		$directorates[$dVal['ref']]	 = $dVal['dirtxt'];
        	}
        	ksort($directorates);
        	return $directorates;
        }
        
        function fetchSubDirectorates( $id )
        {
        	$response = $this->get("SELECT id, name, dirid FROM #_dirsub WHERE dirid = {$id}");
        	return $response;
        }
        
        function getSubDirectorate( $id )
        {
            $response  = $this -> get("SELECT  id,
	                                       DS.name AS dirtxt,
	                                       DS.status,
	                                       DS.dirid
	                                       FROM #_dirsub DS 
	                                       WHERE DS.dirid = $id
                                      ");
            return $response;
        }

     function saveActionOwnerAssigned( $insertdata, $changes )
     {
     	$res = $dirObj -> insert("dir_admins", $insertdata );
     	
     	return $res;
     }
        
		function getSubOwner( $id )
		{
			$results = $this->get("SELECT * FROM #_dir_admins WHERE subdir_id = $id");
			return $results;
		}        
		
		function _getSubDirectorates()
		{
			$results = $this->get("SELECT id, name, dirid FROM #_dirsub WHERE 1");
			return $results;
		}
		
	function getDirectorateList()	
	{
		$directorates = $this->getDirectorates();
		$subDir       = $this->_getSubDirectorates();
		$dirData  = array();
		foreach($directorates as $index => $dir)
		{
			$dirData[$dir['id']] = array("name" => $dir['name'], "id" => $dir['id'], "status" => $dir['status'], "insertdate" => $dir['insertdate'], "insertuser" => $dir['insertuser'] );
		}

		$dirSub = array();
		foreach( $dirData as $dirId => $dir)
		{
			foreach($subDir as $index => $sub)
			{
				if($dirId == $sub['dirid'])
				{
					if($dirId != 1){
						$dirData[$dirId]['sub'][$sub['id']] = $sub; 
					}
				} 				
			}
		}						
		return $dirData;		
	}		
}
