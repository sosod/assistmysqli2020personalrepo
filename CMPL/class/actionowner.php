<?php
class ActionOwner extends DBConnect{


	function getActionOwners()
	{
		$results = $this->get("SELECT * FROM #_action_owner WHERE status & 2 <> 2 ");
		return $results;
	}

	function save( $data )
	{
		$res = $this->insert("action_owner", $data);
		return $this->insertedId();
	}


	function getActionOwner( $id )
	{
		$results = $this->getRow("SELECT * FROM #_action_owner WHERE id = '".$id."'");
		return $results;
	}

	function updateActionOwner( $id, $updateData )
	{
        $data    = $this->getActionOwner( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "action_owner");
        $logsObj -> createLog();		
		$res = $this->update("action_owner", $updateData, "id=$id" );
		return $res;
	}


}
