<?php
class ComplianceFrequency extends DBConnect
{
	
	function save( $insertdata ){
		$insertdata['insertuser'] = $_SESSION['tid'];
		$this->insert("compliance_frequency", $insertdata );
		return $this->insertedId();
	}
	
	function getSortedCompliance()
	{
		$compliances = $this->getComplianceFrequencies();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = array("name" => $compliance['name'], "id" => $compliance['id'] );
		}
		return $compArr;
	}
	
	function getComplianceList()
	{
		$compliances = $this->getComplianceFrequencies();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = $compliance['name'];
		}
		ksort($compArr);
		return $compArr;
	}
	
	function getComplianceFrequencies()
	{
		$results = $this->get("SELECT * FROM #_compliance_frequency WHERE status & 2 <> 2 ORDER BY id");
		return $results;
	}

	function getComplianceFrequency( $id )
	{
		$results = $this->getRow("SELECT * FROM #_compliance_frequency WHERE id = {$id} ");
		return $results;
	}	
	
	function updateCompliance( $insertdata, $id)
	{
        $data    = $this->getComplianceFrequency( $id );
        $postArr = (isset($_POST['dat']) ? $_POST['dat'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "compliance_frequency");
        $logsObj -> createLog();		
		$insertdata['insertuser'] = $_SESSION['tid'];
		$result = $this->update("compliance_frequency", $insertdata, "id=$id");
		return $result;
	}
}
?>