<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/16/11
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
 
class ComplianceTo extends DBConnect{
	
	function fetchAll( $options = "")
	{
		$optionSql = "";
		if($options !== "")
		{
			$innerJoin = " INNER JOIN #_complianceto_deliverable CD ON CD.complianceto_id = C.id";
			$optionSql = " AND CD.deliverable_id = '".$options."'";
		}
		$results = $this->get("SELECT * FROM #_compliance_to C $innerJoin WHERE 1 $optionSql AND C.status & 2 <> 2");
		return $results;
	}	
	

    function getComplianceTo()
	{
		$results = $this->get("SELECT * FROM #_compliance_to WHERE status & 2 <> 2");
		return $results;
	}

	function getSortedComplianceTo()
	{
		$compliances = $this->getComplianceTo();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = array("name" => $compliance['name'], "id" => $compliance['id'] );
		}
		return $compArr;
	}
		
	function getComplianceToList()
	{
		$compliances = $this->getComplianceTo();
		$compArr = array();
		foreach($compliances as $k => $compliance)
		{
			$compArr[$compliance['id']] = $compliance['name'];
		}
		ksort($compArr);
		return $compArr;	
	}
	
	function getAComplianceTo( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_compliance_to WHERE id = $id");
		return $result;
	}

	function getCompliances( $ids )
	{
		$sql   = "";
		if(!empty($ids))
		{
			$sql = " AND id IN($ids) ";
		} else {
			$sql = " AND id IN(0) ";
		}
		$results = $this->get("SELECT * FROM #_compliance_to WHERE 1 $sql");
		$res = array();
		foreach ($results as $c => $complianceto)
		{
			$res[] = $complianceto['name'];		
		}
		$res = implode(",", $res);
		return $res;
	}	

	function save( $insertdata )
	{
		$this->insert( "compliance_to", $insertdata );
		return $this->insertedId();
	}

	function updateComplianceTo( $id, $updatedata)
	{
    	$data    = $this->getAComplianceTo( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "compliance_to");
        $logsObj -> createLog();		
		$res = $this->update("compliance_to", $updatedata, "id=$id");
		return $res;
	}

}
