<?php
class ManageDeliverable extends DeliverableFactory
{

  function __construct()
  {
     parent::__construct();
  }
  
  function getDeliverables($start, $limit, $options = array())
  {
    Naming::$section = $options['section'];
    $optionSql = "";
    if(isset($options) && !empty($options))
    {
       if(isset($options['id']) && !empty($options['id']))
       {
	   $optionSql .= " AND D.legislation = '".$options['id']."'";
       }	
    }    
    $deliverables    = $this->deliverableObj->getDeliverables($start, $limit, $optionSql );
    	
    $data     = $this->sortDeliverables( $deliverables, new DeliverableColumns() );
    $totalDel = $this->deliverableObj->getTotalDeliverables( $optionSql );     	
    $data['total'] = $totalDel['total'];
    return $data;
  } 
  

	function editDeliverable($id, $data)
	{
		$user 		 = new UserAccess();
		$insertdata  = array();
		$updatedata  = array();
		$response    = array();
		$multiple   = array("sub_event", "compliance_to", "accountable_person");       
		foreach($data as $key => $deliverable)
		{
		  if(in_array($key, $multiple))
		  {
		  	if( is_array($deliverable) && !empty($deliverable)){
			 	$updatedata[$key] = implode(",",$deliverable);
		  	} else {
		  		$updatedata[$key] = 0;
		  	}
		  } else {
			 $updatedata[$key] = $deliverable;
		  }       	
		}   
       
		$deliverable = $this->deliverableObj->getADeliverable( $id );
		$changes     = $this->_processDeliverableChanges( $updatedata, $deliverable);
		if(isset($changes['change']) && !empty($changes['change'])){
		  $changes['change']['user'] = $_SESSION['tkn'];
		  $changes['change']['currentstatus'] = "New";
		  $insertdata  = array("changes" => base64_encode( serialize($changes['change']) ), "deliverable_id" => $id, "insertuser" => $_SESSION['tid'] );
		  $res         = $this->deliverableObj->editDeliverable($id ,$insertdata, $updatedata );
		} else {
			$res = 0;
		}      

		if( $res > 0){
		  $response = array("text" => "Deliverable updated" , "error" => false);
		} else if($res == 0){
		  $response = array("text" => "No change was made to the deliverable" , "error" => false);
		} else {
		  $response = array("text" => "Error updating deliverable" , "error" => true );
		}
		return $response;
	}

  function deleteDeliverable( $deliverableId )
  {
		$response = array();
		$insertdata = array();
		$deliverable = $this->deliverableObj->getADeliverable($deliverableId);
		$insertdata['status'] = $deliverable['status'] + 2;
		$res     = $this->deliverableObj->updateDeliverable($insertdata, $deliverableId);
		if( $res > 0){
			$response = array("text" => "Deliverable deleted", "error" => false);
		} else {
			$response = array("text" => "Error deleting deliverables", "error" => true);
		}
		return $response;
  }
	
}
?>
