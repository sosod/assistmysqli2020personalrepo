<?php
class HtmlResponse extends Response
{
	public static $reportTitle;
	
	function showResponse( $data )
	{		
		ob_start();
		 echo "<center>";
		 	echo "<h1>".ucwords($_SESSION['cc'])."</h1>";
		 	echo "<h2>".ucwords(self::$reportTitle)."</h2>";
		 	echo "<table>";
		 	echo "<tr>";
		 	foreach($data['headers']  as $key => $head)
		 	{
		 		echo "<th>".$head."</th>";
		 	}
		 	echo "</tr>";
			foreach( $data['data'] as $index => $valArr)
			{
				echo "<tr>";
				foreach( $valArr as $key => $value)
				{
					echo "<td>".$value."</td>";
				}
				echo "</tr>";
			}
			echo "<table>";
			echo "<br /><font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>";
		echo "</center>";
		ob_end_flush();
	}
}