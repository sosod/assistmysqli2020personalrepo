<?php 
abstract class LegislationFactory
{
      protected $legislationObj = null;
   
      function __construct()
      {
           $this->legislationObj = new Legislation();
      }   
      
      function __call( $methodname , $args )
      {
         $classname  = substr($methodname, 3, -4);
         $resultList = array();
         if(class_exists($classname))
         {
            $classObj = new $classname();
            $results  = $classObj -> fetchAll( $args[0] );
            if(isset($results) && !empty($results))
            {
               //load a result list
               foreach($results as $index => $val)
               {
                  if(isset($val['name']))
                  {
                    $resultList[$val['id']] = $val['name'];
                  }
               }    
            }
         }
         return $resultList;
      }
      
         
      abstract function getLegislations($start, $limit, $options = array());
      
      function sortLegislation( $legislations , Naming $naming)
      {  
         $headers = $naming -> sortedNaming( $naming );	                
         $data    = array();
	foreach($legislations as $key => $value)
	{
	   $categories = $this->getLegislativeCategoriesList($value['ref']);  	   
	   $orgsizes   = $this->getOrganisationSizeList($value['ref']);	   
	   $types      = $this->getOrganisationTypesList($value['ref']);	   
	   $capacities = $this->getOrganisationCapacityList($value['ref']);
   
	   foreach($value as $k => $val)
	   {
               if($k == "business_partner"){
                  //if the user logged is the admin , then make the legislation owner 
                  if($_SESSION['tid'] == "0001") {
	                  $data['owners'][$value['ref']] =  $_SESSION['tid'];
                  } else {
	                  $data['owners'][$value['ref']] =  $val;
                  }
               }			   
	    }
			   
	   foreach($headers as $index => $val)
	   {
	      if( $index == "legislation_category")
	      {
		   $data['headers'][$index]  = $val;
		   $data['legislations'][$value['ref']][$index] = implode(",", $categories);
	      } else if($index == "organisation_size") {
		   $data['headers'][$index]  = $val;
		   $data['legislations'][$value['ref']][$index] = implode(",", $orgsizes);                  	        
	      } else if($index == "organisation_type"){
		   $data['headers'][$index]  = $val;
		   $data['legislations'][$value['ref']][$index] = implode(",", $types);                  	        
	      } else if($index == "organisation_capacity") {	         
		   $data['headers'][$index]  = $val;
		   $data['legislations'][$value['ref']][$index] = implode(",", $capacities);                  	        
	      } else if(isset($value[$index]) || array_key_exists($index, $value)) {
		   $data['headers'][$index]  = $val;
		   $data['legislations'][$value['ref']][$index] = $value[$index];
	     }
	  }
	}			
	$data['columns'] = (isset($data['headers']) ? count($data['headers']) : "");
	return $data;
      }
   
      function processLegislationChanges( $data, $legislation)
      {
         $naming    = new Naming();
         $naming->getNaming();     
         $changeArr     = array();
         $changeMessage = "";
         $updateData    = array();
         $multiple 	= array("category", "organisation_size", "organisation_type", "organisation_capacity");
         print "<pre>";
            print_r($data);
            print_r($legislation);
         print "</pre>";
         foreach($data as $key => $value)
         {
            if( in_array($key, $multiple))
            {    
               if( $key == "category") {
               	  $catObj     = new Categories();
               	  $catList    = $catObj -> fetchList();
                  $categories = $this->getLegislativeCategoriesList( $legislation['ref'] );
                  $catDiff = array();
                  $catAdd  = array(); 
                  if(isset($value) && !empty($value))
                  {	
                     $catDiff = array_diff($categories, $value); 	
                     foreach($value as $cIndex => $cValue)
                     {
                     	if(!array_key_exists($cValue, $categories))
                     	{
                     	  $catAdd[] = $cValue;
                     	  $changesArr['categoryadded'][$cIndex] = "Added category ".$catList[$cValue];
                     	}                     	
                     }
                  }   
		  if(!empty($catDiff))
		  {
		    foreach($catDiff as $diffIndex => $diffValue)
		    {
		  	$changesArr['categoryremoved'][$diffIndex] = "Added category ".$diffValue;	
		  	$updateCat = array("category_id" => $diffIndex, "legislation_id" => $legislation['ref'], "status" => 2);
		    }	
		  }
               } else if($key == "organisation_size"){
                  $sizes = $this->getOrganisationSizeList( $legislation['ref'] );
                  $sizeDiff = array_diff( $sizes, $value);
                  if(isset($sizeDiff) && !empty($sizeDiff))
                  {
                     print "<pre>";
                        print_r($sizeDiff);
                     print "</pre>";                  
                  }
               } else if($key == "organisation_type"){
                  $orgtypes = $this->getOrganisationTypesList( $legislation['ref']);
                  $typeDiff = array_diff($orgtypes, $value);
                  if(isset($typeDiff) && !empty($typeDiff))
                  {
                     print "<pre>";
                        print_r($typeDiff);
                     print "</pre>";                  
                  }
               } else if( $key == "organisation_capacity"){
                  $orgcapacity = $this->getOrganisationCapacityList( $legislation['ref']);
                  $capacityDiff = array_diff( $orgcapacity, $value );
                  if(isset($capacityDiff) && !empty($capacityDiff))
                  {
                     print "<pre>";
                        print_r($capacityDiff);
                     print "</pre>";                  
                  }                  
               }                    
            } else {
		 if( array_key_exists($key, $legislation))
		 {
		 if($value != $legislation[$key])
		 {
			 if($key == "type"){
			 $typeObj  = new LegislativeTypes();
			 $fromType = $typeObj->getALegislativeType( $legislation[$key] );
			 $from 	  = $fromType['name'];
			 $toType   = $typeObj->getALegislativeType( $value );
			 $to 	  = $toType['name'];						

			 $updateData[$key] = $value;
			 $changeArr[$key]  = array("from" => $from, "to" => $to);
			 $changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";

			 } else if($key == "responsible_organisation"){
			 $respoOrg = new ResponsibleOrganisation();

			 $fromOrg = $respoOrg->getAResponsibleOrganisation( $legislation['responsibleorganisation'] );
			 $from 	 = $fromOrg['name'];
			 $toOrg   = $respoOrg->getAResponsibleOrganisation( $value );
			 $to 	 = $toOrg['name'];						

			 $updateData[$key] = $value;
			 $changeArr[$key]  = array("from" => $from, "to" => $to);
			 $changeMessage   .= $naming->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";						
			 } else {

			 $updateData[$key] = $value;						
			 $changeArr[$key]  = array("from" => $legislation[$key], "to" => $value);						
			 $changeMessage   .= $naming->setHeader($key)." has changed to ".$legislation[$key]." from ".$value."\r\n\n";						
			 }				         
		 }
	      }
            }
         }
         
      }
   
}
?>
