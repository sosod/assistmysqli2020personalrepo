<?php
class LegislativeCategories extends DBConnect
{
	
	function fetchAll( $options )
	{
	   $optionSql   = "";
	   if(!empty($options))
	   {
	     $innerJoin = "INNER JOIN #_category_legislation CL ON CL.category_id = LC.id";
	     $where     = " AND CL.legislation_id = $options";
	   }
		$results = $this->get("SELECT * FROM #_legislative_categories LC $innerJoin WHERE 1 AND LC.status & 2 <> 2 $where");
		return $results;	
	}
	
	function fetchList()
	{
	  $results = $this->get("SELECT * FROM #_legislative_categories LC WHERE LC.status & 2 <> 2");
	  $list    = array(); 
	  foreach($results as $rIndex => $rValue)
	  {
	    $list[$rValue['id']] = $rValue['name'];	
	  }
	  return $list;	
	}
	
	function getLegislativeCategories()
	{
		$results = $this->get("SELECT * FROM #_legislative_categories WHERE status & 2 <> 2");
		return $results;
	}
	
	function getALegislativeCategory( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_legislative_categories WHERE id = $id");
		return $result;
	}
	
	function getLegislativeCategory( $ids )
	{
		$result   = $this->get("SELECT * FROM #_legislative_categories WHERE id IN( ".$ids." )");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}
		
	function save( $insertdata )
	{
		$this->insert( "legislative_categories", $insertdata );
		return $this->insertedId();
	}
	
	function updateLegislativeCategories( $id, $updatedata)
	{
        $data    = $this->getALegislativeCategory( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "legislative_categories");
        $logsObj -> createLog();			
		$res = $this->update("legislative_categories", $updatedata, "id=$id");
		return $res;
	}

	function searchCategories( $text )
	{
		$results = $this->get("SELECT id 
							   FROM #_legislative_categories 
							   WHERE name LIKE '%".$text."%'
							   OR description LIKE '%".$text."%'
							   ");
		return $results;		
	}
	
}
?>
