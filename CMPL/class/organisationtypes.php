<?php
class OrganisationTypes extends DBConnect
{
	
	function fetchAll( $options )
	{
	   $innerJoin   = "";
	   $where = "";
	   if(!empty($options))
	   {
	     $innerJoin = "INNER JOIN #_orgtype_legislation TL ON TL.orgtype_id = OT.id";
	     $where     = " AND TL.legislation_id = $options";
	   }		
		$results = $this->get("SELECT * FROM #_organisation_types OT $innerJoin WHERE 1 AND OT.status & 2 <> 2 $where ORDER BY OT.name");
		return $results;	   
	}
	
	function getOrganisationTypes()
	{
		$results = $this->get("SELECT * FROM #_organisation_types WHERE status & 2 <> 2 ORDER BY name");
		return $results;
	}
	
	function sortedOrgTypes()
	{
		$types = $this->getOrganisationTypes();
		$appTypes = array();
		foreach($types as $k => $type)
		{
			$appTypes[$type['id']] = array("name" => $type['name'], "id" => $type['id'] );
		}
		return $appTypes;
	}

	function getOrganisaitonTypesList()
	{
		$types = $this->getOrganisationTypes();
		$typesArr = array();
		foreach($types as $k => $type)
		{
			$typesArr[$type['id']] = $type['name'];
		}
		ksort($typesArr);
		return $typesArr;
	}
	
	function getOrganisationType( $id )
	{
		$result  = $this->get("SELECT * FROM #_organisation_types WHERE id IN(".($id).")");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}	

	
	function getAOrganisationType( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_organisation_types WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "organisation_types", $insertdata );
		return $this->insertedId();
	}
	
	function updateOrganisationType( $id, $updatedata)
	{
        $data    = $this->getAOrganisationType( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "organisation_types");
        $logsObj -> createLog();			
		$res = $this->update("organisation_types", $updatedata, "id=$id");
		return $res;
	}

}
?>
