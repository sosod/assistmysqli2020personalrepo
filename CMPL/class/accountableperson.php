<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/16/11
 * Time: 7:43 PM
 * To change this template use File | Settings | File Templates.
 */
 
class AccountablePerson extends DBConnect{

	function fetchAll($options = "")
	{
		$innerJoin = "";
		$where = "";
		if(!empty($options))
		{
			$innerJoin = " INNER JOIN #_accountableperson_deliverable AD ON AD.accountableperson_id = A.id ";
			$where = " AND AD.deliverable_id = '".$options."'";
		}
		$results = $this->get("SELECT * FROM #_accountable_person A $innerJoin WHERE 1 $where AND A.status & 2 <> 2");
		return $results;	
	}

	function getAccountablePersons()
	{
		$results = $this->get("SELECT * FROM #_accountable_person WHERE status & 2 <> 2 ORDER BY id");
		return $results;
	}

	function save( $data )
	{
		$res = $this->insert("accountable_person", $data);
		return $this->insertedId();
	}
	
	function sortedOrgAccountablePersons()
	{
		$types = $this->getAccountablePersons();
		$appTypes = array();
		foreach($types as $k => $type)
		{
			$appTypes[$type['id']] = array("name" => $type['name'], "id" => $type['id'] );
		}
		return $appTypes;
	}
		
	function getAccountablePersonsList()
	{
		$types = $this->getAccountablePersons();
		$appTypes = array();
		foreach($types as $k => $type)
		{
			$appTypes[$type['id']] = $type['name'];
		}
		ksort($appTypes);
		return $appTypes;	
	}
	
	function getAccountablePerson( $id )
	{
		$results = $this->getRow("SELECT * FROM #_accountable_person WHERE id = '".$id."'");
		return $results;
	}
	
	function fetch( $options )
	{
		$sql = "";
		if(!empty($options))
		{
			$sql = " AND id IN(".$options.")";
		} else {
			$sql = "";
		}
		$results = $this->get("SELECT * FROM #_accountable_person WHERE 1 $sql");
		$res = array();
		foreach ($results as $c => $accountable)
		{
			$res[$accountable['id']] = $accountable['name'];		
		}
		$res = implode(",", $res);
		return $res;	
	}
	
	function updateAccountablePerson( $id, $updateData )
	{
    	$data    = $this->getAccountablePerson( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "accountable_person");
        $logsObj -> createLog();	      		
		$res = $this->update("accountable_person", $updateData, "id=$id" );
		return $res;
	}


}
