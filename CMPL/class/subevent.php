<?php 
class SubEvent extends DBConnect
{
	function fetchAll($options = "")
	{
		$where = "";
		$innerJoin = "";
		if(!empty($options))
		{
			$innerJoin  = " INNER JOIN #_subevent_deliverable SD ON SD.subevent_id = S.id";
			$where = " AND SD.deliverable_id = '".$options."'";		
		}
		$results = $this->get("SELECT * FROM #_sub_event S $innerJoin WHERE 1 $where AND S.status & 2 <> 2 ");
		return $results;
	}	
		
	function getSubEvents()
	{
		$results = $this->get("SELECT SE.id , SE.name, SE.description, SE.status, E.name AS main_event
							   FROM #_sub_event SE
							   INNER JOIN #_events E ON E.id = SE.main_event 
							   WHERE SE.status & 2 <> 2");
		return $results;
	}

    function getMainSubEvents( $id )
    {
    	$idS = "";
    	$sql = "";
    	if(is_array($id))
    	{
    		if( count($id) == 1 )
    		{
    			$sql = ($id[0] == 9 ? "" : " AND main_event = {$id[0]}");
    		} else {
	    		$idS = "(";
	    		foreach ($id as $value){
	    			$idS .= "'".$value."',"; 	    			
	    		}
	    		$idS = rtrim($idS, ",");
	    		$idS .= ")";
	    		$sql = " AND main_event IN {$idS} "; 
    		}   		
    	} else {
    		//$idS = "'".$id."'";
    		$sql = "AND main_event = {$id}";
    	}
        $result  = $this->get("SELECT * FROM #_sub_event WHERE 1 $sql ");
        return $result;
    }


	function getAEvent( $id )
	{
		//echo "SELECT * FROM #_sub_event WHERE id = {$id}";
		$result  = $this->getRow("SELECT * FROM #_sub_event WHERE id = {$id}");
		return $result;
	}
	
	function getASubEvent( $id )
	{
		
		$result  = $this->get("SELECT * FROM #_sub_event WHERE id IN('".$id."')");
		$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
			$response = "";
		}
		return trim($response, ",");		
	}
	
	function sortedSubEvents()
	{
		$evs 	= $this->getSubEvents();
		$events = array();
		foreach($evs as $k => $ev)
		{
			$events[$ev['id']] = array("name" => $ev['name'] , "id" => $ev['id']);
		}
		return $events;
	}
	
	function SubEventsList()
	{
		$evs 	= $this->getSubEvents();
		$events = array();
		foreach($evs as $k => $ev)
		{
			$events[$ev['id']] = $ev['name'];
		}
		ksort($events);
		return $events;				
	}
	
	function save( $insertdata )
	{
		$this->insert( "sub_event", $insertdata );
		return $this->insertedId();
	}
	
	function updateEvent( $id, $updatedata)
	{
        $data    = $this->getAEvent( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "sub_event");
        $logsObj -> createLog();			
		$res = $this->update("sub_event", $updatedata, "id=$id");
		return $res;
	}
}
?>
