<?php
class JsonResponse extends Response{
	
	function showResponse( $data )
	{
		return json_encode( $data );
	}
	
}
