<?php 
class LegislativeFrequency extends DBConnect
{
	
	function getLegislativeFrequency()
	{
		$results = $this->get("SELECT * FROM #_legislative_frequency WHERE status & ".DBCOnnect::DELETE." <> ".DBConnect::DELETE."");
		return $results;
	}
	
	function getALegislativeFrequency( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_legislative_frequency WHERE id = $id");
		return $result;
	}
	
	
	function save( $insertdata )
	{
		$this->insert( "legislative_frequency", $insertdata );
		return $this->insertedId();
	}
	
	function updateLegislativeFrequency( $id, $updatedata)
	{
		$res = $this->update("legislative_frequency", $updatedata, "id=$id");
		return $res;
	}
}
?>