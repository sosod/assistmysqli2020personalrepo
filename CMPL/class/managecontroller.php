<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	}
}
class ManageController extends DBConnect {
	
	var $headers = array();
	
	function getLegislativeTypes()
	{
		$legtypeObj = new LegislativeTypes();
		$legtypes   = $legtypeObj -> getLegislativeTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getLegislativeCategories()
	{
		$legcatObj = new LegislativeCategories();
		$legcats   = $legcatObj -> getLegislativeCategories();
		echo json_encode( $legcats ); 
	}

	function getOrganisationSizes()
	{
		$orgsizeObj = new OrganisationSize();
		$orgsizes   = $orgsizeObj -> getOrganisationSize();
		echo json_encode( $orgsizes ); 
	}

	function getOrganisationTypes()
	{
		$orgtypeObj = new OrganisationTypes();
		$legtypes   = $orgtypeObj -> getOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$legtypes   = $orgtypeObj -> getOrganisationCapacity();
		echo json_encode( $legtypes ); 
	}
	
	function getUsers()
	{
		$obj = new UserAccess();
		echo json_encode( $obj -> getUsers() );					
	}
	
	function saveLegislation()
	{
		$ligObj = new Legislation();
		$insertdata = array();
		foreach( $_POST['data'] as $key => $value)
		{
			$insertdata[$key] = $value;		
		}
		$res 	  = $ligObj -> save( $insertdata );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Legislation saved", "error" => false, "id" => $res );
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true);
		}
		echo json_encode( $response );
	}
	

	function getLegislations()
	{
		$ligObj = new Legislation();

		$response = array();
		$legislations = $ligObj -> getLegislations($_POST['start'], $_POST['limit'], "");
		$legs = $this->_sortData( $legislations );
		echo json_encode( array("legislation" => $legs, "headers" => $this -> headers ));
	}


	function _sortData( $data )
	{
		$headerObj = new Naming();
		$headers   = $headerObj -> sortedNaming();
		$returnData = array();
		foreach( $data  as $key => $value){
		   foreach( $headers as $index => $val){
			if(isset($value[$index]) || array_key_exists($index, $value)){
					$this->headers[$index]  = $val;
					$returnData[$value['ref']][$index] = $value[$index];
				}
			}
		}
		return $returnData;
	}

    function updateDeliverable()
    {
        $response = array();
        $insertdata = array();
        $deliObj    = new Deliverable();
        foreach( $_POST['data'] as $key => $deliverable)
        {
                $insertdata[$key] = $deliverable;
        }
        $res     = $deliObj -> save($insertdata);
        if( $res > 0){
            $response = array("text" => "Deliverable saved", "error" => false);
        } else {
            $response = array("text" => "Error saving deliverables", "error" => true);
        }
        echo json_encode($response);
    }

    function getDeliverables()
    {
        $deliObj      = new Deliverable();
        $deliverables = $deliObj -> getDeliverables($_POST['id'], "");
        $dels         = $this->_sortData( $deliverables );
        echo json_encode( array("deliverables" => $dels, "headers" => $this -> headers ));
    }

    function editDeliverable()
    {
		$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();
        $insertdata  = array();
        $updatedata  = array();
        $response    = array();
        $deliverable = $deliObj ->getDeliverable($_POST['id'], "");
        $changes     = $this  -> _processDeliverableChanges( $_POST['data'], $deliverable);
        $insertdata  = array("changes" => serialize($changes['change']), "deliverable_id" => $_POST['id'], "insertuser" => $_SESSION['tid'] );
        $updatedata  = $_POST['data'];
        $res         = $deliObj -> editDeliverable($_POST['id'] ,$insertdata, $updatedata );

        if( $res > 0){
            $response = array("text" => "Deliverable updated" , "error" => false);
        } else {
            $response = array("text" => "Error updating deliverable" , "error" => true );
        }
        echo json_encode($response);
    }

    function getDeliverableEdits()
    {
		$delObj 	     = new Deliverable();
		$deliverableEdit = $delObj -> getEditLogs ( $_POST['id'] );
		$changes 	     = array();
		foreach($deliverableEdit as $key => $delArr){
			$changeMessage = $this->_extractChanges( $delArr );
			$changes[$delArr['insertdate']] = array(
													   "date" 	 => $delArr['insertdate'],
													   "changes" => $changeMessage,
													 );
		}
		echo json_encode($changes);
    }

    function getActions()
    {
        $deliObj   = new Action();
        $actions    = $deliObj -> getActions( $_POST['id'], "");
        $dels         = $this->_sortData( $actions );
        echo json_encode( array("actions" => $dels, "headers" => $this -> headers ));
    }
    
	function editAction()
	{
		$actionObj 	 = new Action();
		$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();
        
		$updatedata  = array();
        $insertdata  = array();
		$remindon    = (isset($_POST['reminder']) ? $_POST['reminder'] : "" );

		$action      = $actionObj -> getAction( $_POST['id'] );
        
		if( $this->_checkValidity($_POST['deadline'], $remindon)){

                $changes                = $this->_changeProcessor($_POST, $action);
                $insertdata['action_id']= $_POST['id'];
                $insertdata['changes']  = serialize( $changes['changes'] );
				foreach( $_POST as $field => $value ){
					$updatedata[$field] = $value;
				} 
				$res 	  = $actionObj -> editActionChanges( $_POST['id'], $insertdata, $updatedata  );
				$mailText = "";
				if( $res == 1){
					$response = array("text" => "Action updated successfully <br />".$mailText , "error" => false);
				} else {
					$response = array("text" => "Error updating the action" , "error" => true);
				}
		} else {
			$response = array("error" => true, "text" => "Remind on date cannot be after than deadline date");
		}
		echo json_encode( $response );
	}

    function getActionEdits()
    {
		$act 		   = new Action();
		$actionEdits   = $act->getActionEdit ($_POST['id']);
		$changes 	   = array();
		foreach($actionEdits as $key => $editArr){
			$changeMessage = $this->_extractChanges( $editArr );
			$changes[$editArr['insertdate']] = array(
													   "date" 	 => $editArr['insertdate'],
													   "changes" => $changeMessage,
													 );
		}
		echo json_encode($changes);
    }

    function _processDeliverableChanges( $postArr, $deliverable)
    {
        $headerObj  = new Naming();
        $headerObj->getNaming();
        $updatedata = array();
        $changeMesssage = "";
        $changeArray    = array();
        $changeArray['user'] = $_SESSION['tkn']." has made the following changes \r\n\n";
        $changeMesssage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
        $usersGroup = array("responsible", "responsibility_owner", "compliance_to", "accountable_person");
        foreach( $deliverable as $key => $delVal)
        {
            if(isset($postArr[$key]) || array_key_exists($key, $postArr))
            {
                if($postArr[$key] !== $delVal)
                {
                    if($key == "compliance_frequency") {
                        $secObj  = new ComplianceFrequency();
                        $fromSec = $secObj ->getComplianceFrequency( $delVal );
                        $from    = $fromSec['name'];

                        $toSec   = $secObj ->getComplianceFrequency( $postArr[$key] );
                        $to      = $toSec['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "applicable_org_type") {
                        
                        $typeObj  = new OrganisationTypes();
                        $fromType = $typeObj ->getAOrganisationType( $delVal );
                        $from     = $fromType['name'];

                        $toType   = $typeObj ->getAOrganisationType( $postArr[$key] );
                        $to       = $toType['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "functional_service") {
                        
                        $funcObj  = new FunctionalService();
                        $fromFunc = $funcObj -> getAFunctionalService( $delVal );
                        $from     = $fromFunc['name'];

                        $toFunc   = $funcObj -> getAFunctionalService( $postArr[$key] );
                        $to       = $toFunc['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if(in_array($key, $usersGroup)) {
                        $userObj = new UserAccess();
                        $fromUser= $userObj ->getUser( $delVal );
                        $from    = $fromUser['user'];

                        $toUser  = $userObj ->getUser( $postArr[$key] );
                        $to      = $toUser['user'];
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "main_event") {
                        $eventObj  = new Events();
                        $fromEvent = $eventObj ->getAEvent( $delVal );
                        $from      = $fromEvent['name'];

                        $toEvent   = $eventObj ->getAEvent( $postArr[$key] );
                        $to        = $toEvent['name'];
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "sub_event") {
                        
                        $subEventObj  = new SubEvent();
                        $fromEvent    = $subEventObj ->getAEvent( $delVal );
                        $from         = $fromEvent['name'];

                        $toEvent      = $subEventObj -> getAEvent( $postArr[$key] );
                        $to           = $toEvent['name'];
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMesssage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";

                    }  else {
                        $changeArray[$key] = array("from" => $deliverable[$key], "to" => $postArr[$key] );
                        $changeMesssage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$deliverable[$key]."\n\n\n\n";
                        //$insertdata[$key]  = ;
                    }
                }
            }
        }
        $changes = array("change" => $changeArray, "message" => $changeMesssage );
        return $changes;
    }


    function _extractChanges( $updateArray )
	{
		$naming		= new Naming();
        $naming->getNaming();
		$adds		= array("user", "date_completed", "saving", "attachments");
		$changeMessage = "";
		if( isset($updateArray['changes']) && !empty($updateArray['changes']) ){
			$changesArr = unserialize( $updateArray['changes'] );

			foreach( $changesArr  as $index => $val ){
				if( in_array($index,$adds) ){
					$changeMessage .= $val."<br /><br />";
				} else if( $index == "remindon" ){
					if(is_array($val)){
						$changeMessage .= (isset($headers[$index]) ? $headers[$index] : ucwords(str_replace("_"," ", $index))) ." has changed to ".$val['to']." from ".$val['from']."<br />";
					} else {
						$changeMessage .= ucwords(str_replace("_"," ", $index))." ".$val."<br />";
					}
				} else if( $index == "response" ){
					$changeMessage .= $val."<br />";
				} else {
					if( isset($val['to']) && isset($val['from'])){
						$changeMessage .= $naming -> setHeader( $index ) ." has changed to ".$val['to']." from ".$val['from']."<br />";
					}
				}
			}
		}
		return $changeMessage;
	}

	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;

		}
	}

    function _changeProcessor( $postArr , $action)
    {
        $headerObj  = new Naming();
        $headerObj->getNaming();
		$headers    = $headerObj -> sortedNaming();
        $updatedata = array();
        $changeMesssage = "";
        $changeArray    = array();
        $changeArray['user'] = $_SESSION['tkn']." has made the following changes \r\n\n";
        $changeMesssage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
         foreach( $action as $key => $val)
            {
                if(isset($postArr[$key]) || array_key_exists($key, $postArr))
                {
                    if( $postArr[$key] != $val)
                    {
                        if($key  == "owner"){
                            $userObj = new UserAccess();
                            $fromUser= $userObj ->getUser( $val );
                            $from    = $fromUser['user'];
                            
                            $toUser  = $userObj ->getUser( $postArr[$key] );
                            $to      = $toUser['user'];

                            $changeArray[$key] = array("from" => $from, "to" => $to );
                            $changeMesssage   .= $headerObj -> setHeader('action_owner')." changed to ".$to." from ".$from."\n\n\n\n";
                        } else {
                            $changeArray[$key] = array("from" => $action[$key], "to" => $postArr[$key] );
                            $changeMesssage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$action[$key]."\n\n\n\n";
                            //$insertdata[$key]  = ;
                        }
                    }
                }
         }
        $changes = array("changes" => $changeArray, "message" => $changeMesssage);
        return $changes;
    }

}
$method = $_GET['action'];
$setup = new ManageController();
if( method_exists($setup, $method )){
	$setup -> $method();
}
?>