<?php
class OrganisationCapacity extends DBConnect
{
	
	function fetchAll( $options )
	{
	   $innerJoin   = "";
	   $where = "";
	   if(!empty($options))
	   {
	     $innerJoin = "INNER JOIN #_orgcapacity_legislation CL ON CL.orgcapacity_id = OC.id";
	     $where     = " AND CL.legislation_id = $options";
	   }	
		$results = $this->get("SELECT * FROM #_organisation_capacity OC $innerJoin WHERE 1 AND OC.status & 2 <> 2 $where");
		return $results;	   
	}
	
	function getOrganisationCapacity()
	{
		$results = $this->get("SELECT * FROM #_organisation_capacity WHERE status & 2 <> 2");
		return $results;
	}
	
	function getAOrganisationCapacity( $id )
	{
		$result  = $this->getRow("SELECT * FROM #_organisation_capacity WHERE id = $id");
		return $result;
	}
	
	function getOrganisationCapacities( $ids )
	{
		$result  = $this->get("SELECT * FROM #_organisation_capacity WHERE id IN (".$ids.")");
				$response = "";
		if(!empty($result))
		{
			foreach($result as $key => $value)
			{
				$response .= $value['name'].",";
			}
		} else {
				$response = "";
		}
		return trim($response, ",");		
	}	
	
	function save( $insertdata )
	{
		$this->insert( "organisation_capacity", $insertdata );
		return $this->insertedId();
	}
	
	function updateOrganisationCapacity( $id, $updatedata)
	{
        $data    = $this->getAOrganisationCapacity( $id );
        $postArr = (isset($_POST['data']) ? $_POST['data'] : $_POST);
        $logsObj = new Logs( $postArr , $data, "organisation_capacity");
        $logsObj -> createLog();			
		$res = $this->update("organisation_capacity", $updatedata, "id=$id");
		return $res;
	}
	
	function searchCapacities( $text )
	{
		$results = $this->get("SELECT id  FROM #_organisation_capacity
							   WHERE name LIKE '%".$text."%'
							   OR description LIKE '%".$text."%'
							  ");
		return $results;
	}

}
?>
