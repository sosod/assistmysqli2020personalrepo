<?php
class ManageLegislation extends LegislationFactory
{
            
     function __construct()
     {
         parent::__construct();
     }       
      
     function getLegislations( $start, $limit , $options = array())
     {
         Naming::$section  = $options['section'];
         $optionSql = "";
         $optionSql = " AND L.status & ".Legislation::CONFIRMED."  = ".Legislation::CONFIRMED."";
         
	   if($_SESSION['tid'] != "0001") {
		   $businessPatnerObj = new ResponsibleOrganisation();
		   $businessPartner = $businessPatnerObj -> getBussinessPartners();
		   $bpId = "";
		   foreach( $businessPartner as $bIndex => $bValue)
		   {
			   $userIds = array();
			   $userIds = explode(",", $bValue['user_id']);
			   if( in_array($_SESSION['tid'], $userIds))
			   {
				   $bpId = $bValue['business_partner_id'];	
			   }	
		   }		
		   $optionSql .= " AND L.responsible_organisation = ".($bpId == "" ? 0 : $bpId);  
	   }         
	   $legislations = $this->legislationObj->getLegislations($start, $limit, $optionSql ); 	
	   $leg          = $this->sortLegislation( $legislations, new LegislationColumns() );
	   $total        = $this->legislationObj->getTotalLegislation($optionSql);;
	   $leg['total'] = $total['total'];
         return $leg;		   		
     } 
      
     function editLegislation( $data, $id )
     {
		   $changes = array(); 
		   $legislation = $this->legislationObj->getALegislation( $id );
		   $insertdata = array();
		   $res		= "";
         $changes   = $this->processLegislationChanges( $data, $legislation );
         /*
		   if( isset($changes['changeArray']) && !empty($changes['changeArray'])) {
			   $changes['changeArray']['user'] = $_SESSION['tkn'];
			   $changes['changeArray']['currentstatus'] = "New";
			   $insertdata = array("legislation_id" => $_POST['id'], "changes" =>  base64_encode(serialize($changes['changeArray'])), "insertuser" => $_SESSION['tid'] );
			   $res 	 	= $this->legislationObj->editLegislation($insertdata, $changes['update'], $_POST['id'] );
		   } else {
			   $res = 0;
		   }	
		   */	
		   $response = array();
		   if( $res > 0)
		   {
			   $response = array("text" => "Legislation updated", "error" => false, "id" => $res );
		   } else if($res == 0){
			   $response = array("text" => "No change was made to the legislation", "error" => false);
		   } else {
			   $response = array("text" => "Error updating the legislation", "error" => true);
		   }
		   return $response;
     }      
      
}
?>
