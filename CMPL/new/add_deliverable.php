<?php 
$scripts = array("deliverable.js","jquery.ui.deliverable.js");
include("../header.php");
$legislationObj = new Legislation();
$legislation    = $legislationObj -> getALegislation( $_GET['id'] );
$nObj = new Naming();
$nObj -> setThColuomns(); 
$legObj     = new NewLegislation();

$categories = $legObj->getLegislativeCategoriesList($legislation['ref']);

$capacities = $legObj->getOrganisationCapacityList( $legislation['ref'] );

$sizes  = $legObj->getOrganisationSizeList( $legislation['ref'] );

$orgtypes = $legObj->getOrganisationTypesList( $legislation['ref'] );
?>
<?php JSdisplayResultObj(""); ?>
<table class='noborder' width="100%">
	<tr>
    	<td valign="top" width="40%" class='noborder'>
            <table width='100%'>
            	<tr>
            		<td colspan="2"><h4>Legislation Details</h4></td>
            	</tr>
                <tr>
                    <th width="30%"><?php echo $nObj->setHeader("legislation_name"); ?>: </th>    
                    <td width="70%"><?php echo $legislation['legislation_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_reference"); ?>:</th>    
                    <td><?php echo $legislation['legislation_reference']; ?></td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_number"); ?>:</th>    
                    <td><?php echo $legislation['legislation_number']; ?></td>
                </tr>                    
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_type"); ?>:</th>    
                    <td><?php echo $legislation['legislation_type']; ?></td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_category"); ?>:</th>    
                    <td><?php echo implode(",",$categories); ?></td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("organisation_size"); ?>:</th>    
                    <td><?php echo implode(",",$sizes); ?></td>
                </tr>         
                <tr>
                    <th><?php echo $nObj->setHeader("organisation_type"); ?>:</th>    
                    <td><?php echo implode(",",$orgtypes); ?></td>
                </tr>   
                <tr>
                    <th><?php echo $nObj->setHeader("organisation_capacity"); ?>:</th>    
                    <td><?php echo implode(",",$capacities); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("responsible_business_patner"); ?>:</th>    
                    <td><?php echo $legislation['responsible_organisation']; ?></td>
                </tr>                  
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_date"); ?>:</th>    
                    <td><?php echo $legislation['legislation_date']; ?></td>
                </tr>                            
                <tr>
                    <th><?php echo $nObj->setHeader("hyperlink_act"); ?>:</th>    
                    <td><?php echo $legislation['hyperlink_to_act']; ?></td>
                </tr> 
                <tr>
                    <td class="noborder"><?php displayGoBack("", ""); ?></td>    
                    <td class="noborder"></td>
                </tr> 
                <tr>
                	<td colspan="2" class="noborder">
			<script language="javascript">
			$(function(){
			  $("#display_deliverable").deliverable({editDeliverable:true, id:$("#legislationid").val(), section:"new", url:"../class/request.php?action=NewDeliverable.getDeliverables"});
			})
			</script>
			<div id="display_deliverable"></div>                	
                	</td>
                </tr>                   
            </table>
        </td>
        <td class='noborder' width="50%">
            <table width="100%">
            	<tr>
            		<td colspan="2">
            			<p>*All fileds marked with * are required fields</p>
            		</td>
            	</tr>             
            	<tr>
            		<td colspan="2"><h4>Add Deliverable</h4></td>
            	</tr>            
            	<tr>
                	<th width="50%"><?php echo $nObj->setHeader("deliverable_ref"); ?>#:</th>
                	<td width="50%"><?php echo $_GET['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("short_description_of_legislation"); ?>:*</th>    
                    <td>
                    	<textarea name="short_description" id="short_description"></textarea>
                    	<!-- <span class="textcounter" id="count_short_description"></span> -->
                    </td>
                </tr>                  
                <tr>
                    <th><?php echo $nObj->setHeader("description_of_legislation"); ?>:</th>    
                    <td>
                    	<textarea name="description" id="description"></textarea>
                    </td>
                </tr>  
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_section"); ?>:*</th>    
                    <td>
				   <input type="text" name="legislation_section" id="legislation_section" value=""  />
                    </td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>:*</th>    
                    <td>
						<select name="compliance_frequency" id="compliance_frequency">
							<option value="">--select compliance frequency--</option>
						</select>
					</td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("applicable_org_type"); ?>:</th>    
                    <td>
                        <select name="applicable_org_type" id="applicable_org_type">
                        </select>        
                    </td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>:*</th>    
                    <td>
                      <div style="padding-top:10px;">
	                      <table width="100%">
	                        <tr>
	                       	  <td>Is the deadline date recurring within the financial year:</td>
	                       	  <td>
		                       	<select id="recurring" name="recurring">
		                      		<option value="">--recurring--</option>
		                      		<option value="1">Yes</option>
		                      		<option value="0">No</option>
	                      		</select>
                      		</td>
	                       	</tr>
	                      	<!-- <tr class="recurr fixed" style="display:none;">
	                      		<td>
	                      			<input type="radio" name="recurring_type" id="fixed" value="fixed" />Fixed<br />
	                      		</td>
	                      		<td>
                                     <input type="text" name="compliance_date" id="compliance_date" value="" class="idatepicker" readonly="readonly" />
	                      		</td>
	                      	</tr> -->
	                      	<tr id="days" class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="text" name="recurring_period" id="recurring_period" value="" />
	                      			<select name="period" id="period">
										<option value="days">Days</option>
										<option value="weekly">Working Days</option>
	                      			</select>
	                      		</td>
	                      	</tr>	                      	
	                    	<tr class="recurr" style="display:none; text-align:center;">
	                    		<td colspan="2" align="center">
	                    			Days From : 
	                    			<select name="recurring_type" id="recurring_type">
	                    				<option>-- select -- </option>
	                    				<option value="fixed" class="fixedoption">Fixed</option>
	                    				<option value="start" class="nonfixed">Start Of Financial Year</option>
	                    				<option value="end" class="nonfixed">End Of Financial Year</option>
	                    				<option value="startofmonth" class="nonfixed">Start Of Month</option>
	                    				<option value="endofmonth" class="nonfixed">End Of Month</option>
	                    				<option value="startofquarter" class="nonfixed">Start Of Quarter</option>
	                    				<option value="endofquarter" class="nonfixed">End Of Quarter</option>
	                    				<option value="startofsemester" class="nonfixed">Start Of Semester</option>
	                    				<option value="endofsemester" class="nonfixed">End Of Semester</option>
	                    				<option value="startofweek" class="nonfixed">Start Of Week</option>
	                    				<option value="endofweek" class="nonfixed">End Of Week</option>
	                    			</select>
	                    		</td>
	                    	</tr>
	                      	 <tr class="fixed" style="display:none;">
	                      		<td colspan="2">
	                      		Fixed <input type="text" name="compliance_date" id="compliance_date" value="" class="idatepicker" readonly="readonly" />
	                      		</td>
	                      	</tr> 
	                      	<!-- <tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="start" value="start" />Start Of Financial Year</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="end" value="end" />End Of Financial Year</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="startofmonth" value="startofmonth" />Start Of Month</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="endofmonth" value="endofmonth" />End Of Month</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="startofquarter" value="startofquarter" />Start Of Quarter</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="endofquarter" value="endofquarter" />End Of Quarter</td>
	                      	</tr>							
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="startofsemester" value="startofsemester" />Start Of Semester</td>
	                      	</tr>
	                      	<tr class="recurr nonfixed" style="display:none;">
	                      		<td colspan="2"><input type="radio" name="recurring_type" id="endofsemester" value="endofsemester" />End Of Semester</td>
	                      	</tr>	
	                      	 -->													

	                      </table>
                      </div>
                    </td>
                </tr>        
                <tr>
                    <th><?php echo $nObj->setHeader("responsible_department"); ?>:</th>    
                    <td>
                        <select name="responsible" id="responsible">
                        </select>
                    </td>
                </tr>            
                <tr>
                    <th><?php echo $nObj->setHeader("functional_service"); ?>:</th>    
                    <td>
                        <select name="functional_service" id="functional_service">
                        </select>        
                    </td>
                </tr>   
                <tr>
                    <th><?php echo $nObj->setHeader("accountable_person"); ?>:*</th>    
                    <td>
                        <select name="accountable_person" id="accountable_person" multiple="multiple">
                        </select>        
                    </td>
                </tr>            
                <tr>
                    <th><?php echo $nObj->setHeader("responsibility_ower"); ?>:</th>    
                    <td>
                        <select name="responsibility_owner" id="responsibility_owner">
                        </select>        
                    </td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?>:</th>    
                    <td>
                      <input type="text" name="legislation_deadline" id="legislation_deadline" value="<?php echo $legislation['legislation_date']; ?>" class="datepicker" readonly="readonly" />
                    </td>
                </tr>                 
                <!-- <tr>
                    <th><?php echo $nObj->setHeader("action_deadline_date"); ?>:</th>    
                    <td>
                      <input type="text" name="action_deadline" id="action_deadline" value="" class="datepicker" />
                    </td>
                </tr>                                  
                <tr>
                    <th><?php echo $nObj->setHeader("reminder_date"); ?>:</th>    
                    <td>
                      <input type="text" name="reminder" id="reminder" value="" class="datepicker" />
                    </td>
                </tr> -->
                <tr>
                    <th><?php echo $nObj->setHeader("sanction"); ?>:</th>    
                    <td>
                      <textarea name="sanction" id="sanction"></textarea>
                     <!-- <span class="textcounter" id="count_sanction"></span> -->
                    </td>
                </tr>                                                           
                <tr>
                    <th><?php echo $nObj->setHeader("reference_link"); ?>:</th>    
                    <td>
                      <input type="text" name="reference_link" id="reference_link" value="" />
                    </td>
                </tr> 
               <!-- <tr>
                    <th><?php echo $nObj->setHeader("org_kpi_ref"); ?>:</th>
                    <td>
                      <input type="text" name="org_kpi_ref" id="org_kpi_ref" value="" />
                    </td>
                </tr>                                                           
                <tr>
                    <th><?php echo $nObj->setHeader("individual_ref"); ?>:</th>    
                    <td>
                      <input type="text" name="individual_kpi_ref" id="individual_kpi_ref" value="" />
                    </td>
                </tr> -->   
                <tr>
                    <th><?php echo $nObj->setHeader("assurance"); ?>:</th>    
                    <td>
                     <textarea  name="assurance" id="assurance"></textarea>
                     <!-- <span class="textcounter" id="count_assurance"></span> -->
                    </td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?>:<em>*</</em></th>
                    <td>
                        <select name="compliance_to" id="compliance_to" multiple="multiple">
                        </select>        
                    </td>
                </tr>                
                <tr>
                    <th><?php echo $nObj->setHeader("main_event"); ?>:<em>*</em></th>    
                    <td>
                        <select name="main_event" id="main_event">
                        </select>        
                    </td>
                </tr>              
                <tr>
                    <th><?php echo $nObj->setHeader("sub_event"); ?>:<em>*</em></th>    
                    <td>
                        <select name="sub_event" id="sub_event" multiple="multiple">
                        </select>        
                    </td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("guidance"); ?>:</th>
                    <td>
                      <textarea  name="guidance" id="guidance"></textarea>
                      <!-- <span class="textcounter" id="count_guidance"></span> -->
                    </td>
                </tr>  
                <tr>
                    <th><?php echo $nObj->setHeader("does_this_deliverable_have_actions"); ?>:</th>
                    <td>
                      <select id="has_actions" name="has_actions">
                      	<option value="1">Yes</option>
                      	<option value="0">No</option>
                      </select>
                    </td>
                </tr>                              
                <tr>
                    <th><?php echo $nObj->setHeader("import"); ?>:</th>
                    <td>
                      <input type="submit" name="import" id="import" value="Import" class="isubmit"/>
                    </td>
                </tr>                 
                <tr>
                    <td></td>    
                    <td>
                      <input type="submit" name="save_deliverable" id="save_deliverable" value="Save" class="isubmit" />
                      <input type="hidden" name="legislationid" id="legislationid" value="<?php echo $_GET['id']; ?>" />
                      <input type="hidden" name="actionowner" id="actionowner" value="" />
                      <input type="hidden" name="actiondeadlinedate" id="actiondeadlinedate" value="" />
					  <input type="button" name="cancel" id="cancel" value=" Cancel " class="idelete " />	
                    </td>
                </tr>                    
            </table>        
        </td>
    </tr>
</table>
