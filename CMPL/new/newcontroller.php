<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	} else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")){
		require_once("../../library/dbconnect/".strtolower($classname).".php");
	} else {
		require_once("../../../library/dbconnect/".strtolower($classname).".php");
	}
}
class NewController extends DBConnect {
	
	var $headers = array();
	
	function getLegislativeTypes()
	{
		$legtypeObj = new LegislativeTypes();
		$legtypes   = $legtypeObj -> getLegislativeTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getLegislativeCategories()
	{
		$legcatObj = new LegislativeCategories();
		$legcats   = $legcatObj -> getLegislativeCategories();
		echo json_encode( $legcats ); 
	}

	function getOrganisationSizes()
	{
		$orgsizeObj = new OrganisationSize();
		$orgsizes   = $orgsizeObj -> getOrganisationSize();
		echo json_encode( $orgsizes ); 
	}

	function getLegOrganisationTypes()
	{
		$orgtypeObj = new LegOrganisationTypes();
		$legtypes   = $orgtypeObj -> getLegOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getOrganisationTypes()
	{
		$orgtypeObj = new OrganisationTypes();
		$legtypes   = $orgtypeObj -> getOrganisationTypes();
		echo json_encode( $legtypes ); 
	}
	
	function getOrganisationCapacity()
	{
		$orgtypeObj = new OrganisationCapacity();
		$legtypes   = $orgtypeObj -> getOrganisationCapacity();
		echo json_encode( $legtypes ); 
	}

	function getResponsibleOrganisations()
	{
		$orgObj = new ResponsibleOrganisation();
		$orgs   = $orgObj -> getResponsibleOrganisations();
		echo json_encode( $orgs );
	}
	
	function getBusinessUsers()
	{
		$orgObj   = new ResponsibleOrganisation();
		$usersObj = new UserAccess();
		$resUsers = $orgObj -> getResponsibleOrganisationUsers( $_POST['id'] );
		$bPartnerUser = "";
		foreach($resUsers as $k => $valArr)
		{
			$bPartnerUser .= $valArr['user_id'].",";
		}
		$partnerUsers = rtrim($bPartnerUser, ",");	
		$users 	  = $usersObj -> getUsersByIds( $partnerUsers );
		echo json_encode( $users );
	}
	
    function getSections()
    {
		$sectionObj = new LegislationSection();
        $sections = $sectionObj -> getLegislationSections();
        echo json_encode($sections);
    }

	 function getComplianceFrequency()
    {
		$complianceObj = new ComplianceFrequency();
        $compliances = $complianceObj -> getComplianceFrequencies();
        echo json_encode($compliances);
    }
	    
    function getDirectorate()
    {
        $dir = new Directorate();
		$directorates 	 = $dir -> getDirectorates();
        $subdirectorate  = $dir ->_getSubDirectorates();
		$dirSubs = array();
		foreach($directorates as $i => $directorate)
		{
			if($directorate['id'] == 1)
			{
				$dirSubs[$directorate['id']] = array("id" => 1, "name" => $directorate['name']);
			} else {
				foreach($subdirectorate as $s => $subDir)
				{
					if($subDir['dirid'] == $directorate['id'] ) {
						$dirSubs[$subDir['dirid']] = array("id" =>$directorate['id'] , "name" => $directorate['name']." - ".$subDir['name']);
					}
				}
			}
		}
        echo json_encode( $dirSubs );
    }

	function getUsers()
	{
		$obj = new UserAccess();
		echo json_encode( $obj -> getUsers() );					
	}

	function getActionOwners()
	{
		$obj = new ActionOwner();
		echo json_encode( $obj -> getActionOwners() );					
	}	
	
	function getMainEvents()
	{
		$obj = new Events();
		echo json_encode( $obj -> getEvents() );
	}

    function getSubEvents()
	{
		$obj = new SubEvent();
		echo json_encode( $obj -> getMainSubEvents( $_POST['id']) );
	}

    function getAccountablePerson()
	{
		$obj = new AccountablePerson();
		echo json_encode( $obj -> getAccountablePersons());
	}

    function getResponsibleOwner()
	{
		$obj = new ResponsibleOwner();
		echo json_encode( $obj -> getResponsibleOwners() );
	}
	
    function getComplianceTo()
	{
		$freqObj = new ComplianceTo();
		$freq    = $freqObj -> getComplianceTo();
		echo json_encode( $freq );
	}
		
	
    function getFunctionalService()
	{
		$obj = new FunctionalService();
		echo json_encode( $obj -> getFunctionalService());
	}
	
    function deleteDeliverable()
    {
        $response = array();
        $insertdata = array();
        $deliObj    = new Deliverable();
        $deliverable = $deliObj ->getADeliverable($_POST['deliverableId']);
        $id 		= $_POST['deliverableId'];
        $insertdata['status'] = $deliverable['status'] + 2;
        //foreach( $_POST['data'] as $key => $deliverable)
        //{
           //$insertdata[$key] = $deliverable;
        //}
        $res     = $deliObj -> updateDeliverable($insertdata, $id);
        if( $res > 0){
            $response = array("text" => "Deliverable deleted", "error" => false);
        } else {
            $response = array("text" => "Error deleting deliverables", "error" => true);
        }
        echo json_encode($response);
    }
    	
	
	function saveLegislation()
	{
		$ligObj = new Legislation();
		$insertdata = array();
      $multiple = array("category"              => array("name" => "category_legislation", "key" => "category_id"), 
                        "organisation_size"     => array("name" => "orgsize_legislation", "key" => "orgsize_id"),
                        "organisation_capacity" => array("name" => "orgcapacity_legislation", "key" => "orgcapacity_id"),
                        "organisation_type"     => array("name" => "orgtype_legislation", "key" => "orgtype_id")
                       );
      $multipledata  = array();
		foreach( $_POST['data'] as $key => $value)
		{
         if(array_key_exists($key, $multiple))
         {
         	if(is_array($value) && !empty($value)){
             	$multipledata[$key] = implode(",",$value);
         	}
         } else{
             $insertdata[$key] = $value;
         }
		}
		$res 	  = $ligObj -> save( $insertdata );
		$response = array();
		if( $res > 0)
		{
		   if(isset($multipledata) && !empty($multipledata))
		   {
		      foreach($multipledata as $key => $valArr)
		      {
		         if( isset($multiple[$key]) && !empty($multiple[$key]))
		         {
		            $values = explode(",", $valArr);
		            {
		               foreach($values as $i => $val)
		               {
		                  $r = $ligObj -> insert($multiple[$key]['name'], array($multiple[$key]['key'] => $val, 'legislation_id' => $res));
		                  $r = $r++;
		               }
		            }
		         }
		      }  
		   }
			$response = array("text" => "Legislation saved", "error" => false, "id" => $res );
		} else {
			$response = array("text" => "Error saving the legislation", "error" => true);
		}
		echo json_encode( $response );
	}
	/*
	function getLegislations()
	{
		$ligObj = new Legislation();
		$options = "";
		//if( $_POST['page'] == "confirm")
		//{
			$options = " AND L.status & ".Legislation::CONFIRMED." <> ".Legislation::CONFIRMED."";			
		//}
		if($_SESSION['tid'] != "0001"){
			$businessPatnerObj = new ResponsibleOrganisation();
			$businessPartner = $businessPatnerObj -> getBussinessPartners();
			$bpId = "";
			foreach( $businessPartner as $bIndex => $bValue)
			{
				$userIds = array();
				$userIds = explode(",", $bValue['user_id']);
				if( in_array($_SESSION['tid'], $userIds))
				{
					$bpId = $bValue['business_partner_id'];	
				}	
			}		
			$options .= " AND L.responsible_organisation = ".($bpId == "" ? 0 : $bpId); 
		}		
		Naming::$section  = $_POST['section'];
		$legislations = $ligObj -> getLegislations($_POST['start'], $_POST['limit'], $options ); 		

		$catObj = new LegislativeCategories();
		$capObj = new OrganisationCapacity();
		$sizeObj = new OrganisationSize();
		$typeObj = new LegOrganisationTypes();
		$legislationArray = array();
		$legOwners = array();

		foreach($legislations as $key => $value)
		{
			foreach($value as $index => $val){
			   
				if($index == "category" )
				{
					$legislationArray[$key]['legislation_category'] = $catObj -> getLegislativeCategory( $val );
				}
				if($index == "organisationsize"){
					$legislationArray[$key]['organisation_size'] = $sizeObj -> getOrganisationSizes( $val );
				}
				if($index == "organisationcapacity"){
					$legislationArray[$key]['organisation_capacity'] = $capObj -> getOrganisationCapacities( $val );
				}  
				if($index == "organisationtype"){
					$legislationArray[$key]['organisation_type'] = $typeObj -> getOrganisationType( $val );
				}
				if($index == "business_partner")
				{
					if($_SESSION['tid'] == "0001"){
						//echo "Coming here and ffrom ddb is ".$val." and logged on is ".$_SESSION['tid']."  ---------  ";
						$legOwners[$value['ref']] =  $_SESSION['tid'];
						//echo "After assigning to ref , it becomes ".$legOwners[$value['ref']]."\r\n\n";
					} else {
						$legOwners[$value['ref']] =  $val;
					}
				} else {
					$legislationArray[$key][$index] = $val;
				}
		    }			
		}			
		$response = array();		
		$legs = $this->_sortData( $legislationArray, array(), new LegislationColumns());
		echo json_encode( array("legislation" => $legs, "owners" => $legOwners, "headers" => $this -> headers, "cols" => count($this->headers), "total" => $ligObj->getTotalLegislation($options) ));
	}
	*/
	
	function _sortData( $data, $optionsHeaders = array(), Naming $naming )
	{
		$headers   = $naming -> sortedNaming( $naming );
		if( !empty($optionsHeaders))
		{
			foreach($headers as $i => $head)
			{		
				if(in_array($i, $optionsHeaders))
				{
					$this->headers[$i]  = $head;
					$headersArr[$i]     = $head; 
					
				}				
			}
		}
		
		if(!empty($headersArr))
		{
			$headers = $headersArr;
		} else {
			$headers = $headers;
		}
		
		$returnData = array();
		if(empty($data)){
			$this->headers  = $headers;
		} else {
			foreach( $data  as $key => $value){
			   foreach( $headers as $index => $val){
				if(isset($value[$index]) || array_key_exists($index, $value)){
						$this->headers[$index]  = $val;
						$returnData[$value['ref']][$index] = $value[$index];
					}
				}
			}
		}
		return $returnData;
	}

    function saveDeliverable()
    {
        $response   = array();
        $insertdata = array();
        $deliObj    = new Deliverable();
        $multiple   = array("applicable_org_type", "main_event", "sub_event", "compliance_to", "accountable_person");
        $createAction = FALSE;
        foreach( $_POST['data'] as $key => $deliverable)
        {
        	if( $key == "hasActions" && $deliverable == 0){
        		$createAction = TRUE;
        		$actionArr = array( "action"			 => $_POST['data']['short_description'],
        							"action_deliverable" => $_POST['data']['short_description'],
        							"deadline" 			 => $_POST['data']['actiondeadlinedate'],
        							"owner" 		 	 => $_POST['data']['actionowner'],	   
        						  );				  				  
        	} 
        }
        
        unset($_POST['data']['hasActions'] );				  
        unset($_POST['data']['actiondeadlinedate'] );
        unset($_POST['data']['actionowner'] );
        foreach($_POST['data'] as $key => $deliverable)
        {
        	if(in_array($key, $multiple)) {
            	if(is_array($deliverable) && !empty($deliverable)){
                	$insertdata[$key] = implode(",",$deliverable);
            	}
            } else {
                $insertdata[$key] = $deliverable;
            }       		
        }
        
        $res     = $deliObj -> saveDeliverable($insertdata);
        if( $res > 0){
        	if( $createAction )
        	{
        		$actionArr['deliverable_id'] = $res;
				$actionObj = new Action();        		
        		$actionRes = $actionObj -> save($actionArr); 
        	}
            $response = array("text" => "Deliverable saved", "error" => false);
        } else {
            $response = array("text" => "Error saving deliverables", "error" => true);
        }
        echo json_encode($response);
    }

    function getDeliverables()
    {
        $deliObj      	  = new Deliverable();
        Naming::$section  = $_POST['section'];
        $deliverables = $deliObj -> getDeliverables($_POST['start'], $_POST['limit'], $_POST['id'], "");
        $delArr		  = array();
        foreach ($deliverables as $index => $deliverable)
        {
        	foreach( $deliverable as $key => $val)
        	{
        		if($key == "accountable_person")
        		{
        		    if(!empty($val))
        			{
                		$accObj = new AccountablePerson();
                		$accountables = $accObj->fetch($val);
                		$delArr[$index][$key] = $accountables;	
        			} else {
        				$delArr[$index][$key] = "";
        			}
        		} else if($key == "responsible_department")
        		{
        			if(!empty($val))
        			{
                		$dir = new Directorate();
                		$directorate = $dir -> fetch($val);
                		$delArr[$index][$key] = $directorate['name'];	
        			} else {
        				$delArr[$index][$key] = "";
        			}
        		} else if( $key == "compliance_name_given_to")
        		{
        			if(!empty($val))
        			{
        				$compObj = new ComplianceTo();
        				$delArr[$index][$key] = $compObj -> getCompliances( $val );        				
        			} else {
        				$delArr[$index][$key] = "";
        			}        			
        		} else if( $key == "applicable_organisation_type"){
        		  if($val == ""){
				  	$delArr[$index][$key] = "";
        		  } else {
        		  	$orgObj = new OrganisationTypes();
        		  	$delArr[$index][$key] = $orgObj -> getOrganisationType( $val );	
        		  }	        			        			
        		} else if($key == "main_event") {
        		  if( $val == ""){                     
        		  	$delArr[$index][$key] = "";
        		  } else {
        		  	$evenObj = new Events();
        		  	$delArr[$index][$key] = $evenObj -> getEvent( $val );
        		  }	
        		} else if( $key == "sub_event"){
        		  if( $val == ""){                     
        		  	$delArr[$index][$key] = "";
        		  } else {
        		  	$evenObj = new SubEvent();
        		  	$delArr[$index][$key] = $evenObj -> getASubEvent( $val );
        		  }	        			
        		}  else {
        			$delArr[$index][$key] = $val;
        		}        		
        	}        	
        }
        $dels         = $this->_sortData( $delArr, array(), new DeliverableColumns() );
        $totalDel = $deliObj -> getTotalDeliverables($_POST['id'], "");
        $delArr   = array();
  
        foreach($dels as $dKey => $dArr)
        {
        	foreach($delArr as $k => $dVal)
        	{	
                $delArr[$dKey][$k] = htmlspecialchars($dVal, ENT_QOUTES, 'UTF-8');	
        	}         	
        }    
        echo json_encode( array("deliverables" => $dels, "headers" => $this -> headers, "total" => $totalDel['total'], "cols" => count($this->headers) ));
    }
    function getActions()
    {
         $deliObj   = new Action();
         Naming::$section = "new";
         $actions    = $deliObj -> getActions( $_POST['start'], $_POST['limit'],$_POST['id'], "");
         $dels         = $this->_sortData( $actions , array(), new ActionColumns());
         echo json_encode( array("actions" => $dels, "headers" => $this -> headers,"cols" => count($this->headers), "total" => $deliObj -> getTotalActions( $_POST['id'], "") ));
    }
    
	function saveAction()
	{
		$action 	 = new Action();
		$insertdata  = array();
		//$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();		
		//$email 		 = new Email();					
		//$deliverable = $deliObj->getDeliverable( $_POST['deliverable_id']);			
		//$contractDetails = $contract -> getContract( $deliverable['contract_id'] );		
		
		if( $this->_checkValidity($_POST['deadline'], $_POST['reminder'])){
			//if( $this->_checkValidity($deliverable['deadline'], $_POST['deadline'])) {
			
				foreach( $_POST as $field => $value ){
					if($field == "action_name")
					{
						$insertdata['action'] = $value;
					} else {
						$insertdata[$field] = $value;
					}
				} 
				$res 	  = $action -> save( $insertdata );
				$mailText = "";
				if( $res > 0){
					//$ownerEmail   = $user -> getUser( $contractDetails['owner']); 
					//$managerEmail = $user -> getUser( $contractDetails['contract_manager']);
	
					//$body = $_SESSION['tkn']." added a new action";
					
					//if( $email -> to( (isset($ownerEmail['email']) ? $ownerEmail['email']."," : "" )."".(isset($managerEmail['email']) ? $managerEmail['email'] : "") ) -> from("admin@ignite4u.com") -> subject("Action") -> body( $body ) -> send()  == 1){
						//$mailText = "Mail sent successfully";	
					//}	else {
						//$mailText = "Error sending mail";	
					//}	
									
					$response = array("text" => "Action saved successfully <br />".$mailText , "id" => $res, "error" => false);
				} else {
					$response = array("text" => "Error saving the action" , "error" => true);		
				}
			//} else {
				//$response = array("error" => true, "text" => "Action Deadline date cannot be after than Deliverable deadline date <span class='error'>(".$deliverable['deadline'].")</span>");				
			//}
		} else {
			$response = array("error" => true, "text" => "Remind on date cannot be after than deadline date");
		}
		echo json_encode( $response );
	}

	function _checkValidity( $deadline, $remindon){
		if( strtotime($remindon) > strtotime($deadline)){
			return FALSE;
		} else {
			return TRUE;

		}
	}
	
	function confirm()
	{
		$ligObj = new Legislation();
		$legislation = $ligObj -> getALegislation($_POST['id'] ); 
		$updatedata  = array("status" => $legislation['status'] + Legislation::CONFIRMED);
		$res 		 = $ligObj -> updateLegislation( $updatedata, $_POST['id'] );
		$response 	 = array(); 
		if( $res > 0 )
		{
			$response	= array("text" => "Legislation successfully confirmed" , "error" => false);
		} else if( $res == 0) {
			$response	= array("text" => "Legislation successfully confirmed" , "error" => false);			
		} else {
			$response	= array("text" => "Error updating the legislation" , "error" => true);
		}
		echo json_encode( $response );
	}

	
	function getActivation()
	{
		$headerKeys = array("ref", "legislation_name", "legislation_reference", "legislation_type", "legislation_number");

		$ligObj = new Legislation();
		$conditions = " AND L.status & ".Legislation::CONFIRMED." = ".Legislation::CONFIRMED." AND  L.status & ".Legislation::ACTIVATED." <> ".Legislation::ACTIVATED."";
		$awaitingActivation = $ligObj -> getLegislations($_GET['start'], $_GET['limit'], $conditions);

		$awaitingActivation = $this->_sortData( $awaitingActivation ,$headerKeys, new LegislationColumns());

		$condition  =  " AND L.status & ".Legislation::ACTIVATED." = ".Legislation::ACTIVATED." ";
		$activated  = $ligObj -> getLegislations($_GET['start'], 10000, $condition);
		$activated  = $this->_sortData( $activated , $headerKeys, new LegislationColumns());

		$response = array("awaiting" => $awaitingActivation, "activated" => $activated, "headers" => $this->headers );
		echo json_encode( $response );
	}
	
	function activateLegislation()
	{
		$ligObj = new Legislation();
		$legislation = $ligObj -> getALegislation($_POST['id'] ); 
		$updatedata  = array("status" => $legislation['status'] + Legislation::ACTIVATED);
		$res 		 = $ligObj -> updateLegislation( $updatedata, $_POST['id'] );
		$response 	 = array(); 
		if( $res > 0 )
		{
			$response	= array("text" => "Legislation successfully activated" , "error" => false);
		} else if( $res == 0) {
			$response	= array("text" => "Legislation successfully activated" , "error" => false);			
		} else {
			$response	= array("text" => "Error updating the legislation" , "error" => true);
		}
		echo json_encode( $response );
		
	}
	
	function deleteAction()
	{
		$id = $_POST['id'];
		$actionObj  = new Action(); 
		$updatedata = array("actionstatus" => 2);
		$res 	   = $actionObj -> updateAction( $updatedata, $id);
		$response  = array();
		if( $res == 1 )
		{
			$response = array("text" => "Action successfully deleted", "error" => false);
		} else if( $res == 0){
			$response = array("text" => "There was no change to the action", "error" => true);
		} else {
			$response = array("text" => "Error deleting the action", "error" => true);			
		}
		echo json_encode($response);
	}
	
	function getLegislationlist()
	{
		$legObj = new Legislation();
		$options = " AND parent_id = 0 AND status & 2 <> 2";
		$legislations = $legObj -> fetchAll( $options );
		echo json_encode( $legislations );
	}
	
    function editDeliverable()
    {
		$deliObj 	 = new Deliverable();
		$user 		 = new UserAccess();
        $insertdata  = array();
        $updatedata  = array();
        $response    = array();
        $multiple   = array("applicable_org_type", "main_event", "sub_event", "compliance_to");       
        foreach( $_POST['data'] as $key => $deliverable)
        {
            if(in_array($key, $multiple))
            {
            	if( is_array($deliverable) && !empty($deliverable)){
                	$updatedata[$key] = implode(",",$deliverable);
            	} else {
            		$updatedata[$key] = 0;
            	}
            } else{
                $updatedata[$key] = $deliverable;
            }       	
        }           
        $deliverable = $deliObj ->getADeliverable($_POST['id']);
        $changes     = $this  -> _processDeliverableChanges( $updatedata, $deliverable);
        if(isset($changes['change']) && !empty($changes['change'])){
            $changes['change']['user'] = $_SESSION['tkn']." has made the following changes \r\n\n";
        	$insertdata  = array("changes" => serialize($changes['change']), "deliverable_id" => $_POST['id'], "insertuser" => $_SESSION['tid'] );
            $res         = $deliObj -> editDeliverable($_POST['id'] ,$insertdata, $updatedata );
        } else {
        	$res = 0;
        }      

        if( $res > 0){
            $response = array("text" => "Deliverable updated" , "error" => false);
        } else if($res == 0){
        	$response = array("text" => "No change was made to the deliverable" , "error" => false);
        } else {
            $response = array("text" => "Error updating deliverable" , "error" => true );
        }
        echo json_encode($response);
    }

   function _processDeliverableChanges( $postArr, $deliverable)
    {
        $headerObj  = new Naming();
        $headerObj->getNaming();
        $updatedata = array();
        $changeMessage = "";
        $changeArray    = array();
        $changeMessage     .= $_SESSION['tkn']." has made the following changes \r\n\n";
        //$usersGroup = array("responsible", "responsibility_owner", "compliance_to", "accountable_person");
       /* if( is_array($postArr['main_event']) && !empty($postArr['main_event'])){
        	$postArr['main_event'] = implode(",", $postArr['main_event'] ); 
        }
        if( is_array($postArr['applicable_org_type']) && !empty($postArr['applicable_org_type'])){
        	$postArr['applicable_org_type'] = implode(",", $postArr['applicable_org_type'] ); 
        } */   
        $postArr['sub_event'] 		= ($postArr['sub_event'] == "" ? 0 : $postArr['sub_event']);
        $deliverable['sub_event']	= ($deliverable['sub_event'] == "" ? 0 : $deliverable['sub_event'] ); 
        foreach( $deliverable as $key => $delVal)
        {
            if(isset($postArr[$key]) || array_key_exists($key, $postArr))
            {
            	//echo $key." --- ".$postArr[$key]." => ".$delVal." \r\n\n";
                if($postArr[$key] !== $delVal)
                {
                    if($key == "compliance_frequency") {
                        $secObj  = new ComplianceFrequency();
                        $fromSec = $secObj ->getComplianceFrequency( $delVal );
                        $from    = $fromSec['name'];

                        $toSec   = $secObj ->getComplianceFrequency( $postArr[$key] );
                        $to      = $toSec['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "responsible") {
                        
						$dirObj      = new Directorate();
						$fromDir     = $dirObj -> getSubDirectorate( $delVal );
                        $from     	 = $fromDir['dirtxt'];

                        $toDir   	= $dirObj -> getSubDirectorate( $postArr[$key]);
                        $to       	= $toDir['dirtxt'];

                        $changeArray['responsible_department'] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader('responsible_department')." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "responsibility_owner") {
                        
						$respoObj	= new ResponsibleOwner();
						$fromOwner  = $respoObj -> getResponsibleOwner( $delVal );
                        $from     	= $fromOwner['name'];

                        $toOwner   	= $respoObj -> getResponsibleOwner( $postArr[$key] );
                        $to       	= $toOwner['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "compliance_to") {
                    	
						$compObj    = new ComplianceTo();
						$fromCompl  = $compObj -> getAComplianceTo( $delVal );
                        $from     	= $fromCompl['name'];

                        $toCompl   	= $compObj -> getAComplianceTo( $postArr[$key] );
                        $to       	= $toCompl['name'];

                        $changeArray['compliance_name_given_to'] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader('compliance_name_given_to')." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "applicable_org_type") {
						$typeObj = new OrganisationTypes();
						$from  = "";
						$to    = "";
						if( $delVal == "" ) {
							$from = "";
						} else {
							$from  = $typeObj->getOrganisationType( $delVal );
						}
						
						if( $postArr[$key] == "") {
							$to = "";
						} else {
							$to = $typeObj->getOrganisationType( $postArr[$key] );	
						}
						//echo "from => ".$from." and to ".$to." \r\n\n";
						$changeArray[$key] = array("from" => $from, "to" => $to);
						$changeMessage  .= $headerObj->setHeader($key)." has changed to ".$to." from ".$from."\r\n\n";
                    } else if($key == "functional_service") {
                        
                        $funcObj  = new FunctionalService();
                        $fromFunc = $funcObj -> getAFunctionalService( $delVal );
                        $from     = $fromFunc['name'];

                        $toFunc   = $funcObj -> getAFunctionalService( $postArr[$key] );
                        $to       = $toFunc['name'];

                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                        
                    } else if($key == "accountable_person") {
                    	
						$accObj     = new AccountablePerson();
                        $fromUser	= $accObj ->getAccountablePerson( $delVal );
                        $from    	= $fromUser['name'];

                        $toUser  	= $accObj ->getAccountablePerson( $postArr[$key] );
                        $to      	= $toUser['name'];
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "main_event") {
                        $eventObj  = new Events();
                        $to   = "";
                        $from = "";
						if( $delVal == ""){
							$from = "";
						} else {
                        	$from = $eventObj ->getEvent( $delVal );
						}
                        if( $postArr[$key] == ""){
                        	$to = "";
                        } else {
                       		$to = $eventObj ->getEvent( $postArr[$key] );
                        }
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";
                    } else if($key == "sub_event") {
                        
                        $subEventObj  = new SubEvent();
                        $from    = $subEventObj ->getASubEvent( $delVal );

                        $to      = $subEventObj -> getASubEvent( $postArr[$key] );
                        $changeArray[$key] = array("from" => $from, "to" => $to );
                        $changeMessage   .= $headerObj -> setHeader($key)." changed to ".$to." from ".$from."\n\n\n\n";

                    }  else {
                        $changeArray[$key] = array("from" => $deliverable[$key], "to" => $postArr[$key] );
                        $changeMessage   .= $headerObj -> setHeader( $key )." changed to ".$postArr[$key]." from ".$deliverable[$key]."\n\n\n\n";
                        //$insertdata[$key]  = ;
                    }
                }
            }
        }
        $changes = array("change" => $changeArray, "message" => $changeMessage );
        return $changes;
    }   
}
$method = $_GET['action'];
$setup = new NewController();
if( method_exists($setup, $method )){
	$setup -> $method();
}
?>
