<?php 
$scripts = array("legislation.js");
include("../header.php");
$nObj = new Naming();
$nObj -> setThColuomns();
?>
<?php JSdisplayResultObj(""); ?>
<table>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_name"); ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" id="name" name="name" class="checkcounter"></textarea>
    		<span id="count_name" class="textcounter"></span>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_reference"); ?>:</th>    
    	<td>
    		<input type="text" name="reference" id="reference" value="" />
    	</td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_number"); ?>:</th>    
    	<td>
          <textarea rows="5" cols="30" name="legislation_number" id="legislation_number" class="checkcounter"></textarea>
          <span id="count_legislation_number" class="textcounter"></span>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_type"); ?>:</th>    
    	<td>
        	<select name="type" id="type">
            	<option value="">--type--</option>
            </select>
        </td>
    </tr>        
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_category"); ?>:</th>    
    	<td>
        	<select name="category" id="category" multiple="multiple">
            	<option value="" selected="selected">--category--</option>
            </select>        
        </td>
    </tr>    
	<tr>
    	<th><?php echo $nObj->setHeader("organisation_size"); ?>:</th>    
    	<td>
        	<select name="organisation_size" id="organisation_size" multiple="multiple">
            	<option value="" selected="selected">--organisation size--</option>
            </select>        
        </td>
    </tr> 
    <tr>   
    	<th><?php echo $nObj->setHeader("organisation_type"); ?>:</th>    
    	<td>
        	<select name="organisation_type" id="organisation_type" multiple="multiple">
            	<option value="" selected="selected">--organisation type--</option>
            </select>        
        </td>
    </tr>   
	<tr>
    	<th><?php echo $nObj->setHeader("organisation_capacity"); ?>:</th>    
    	<td>
        	<select name="organisation_capacity" id="organisation_capacity" multiple="multiple">
            	<option value="" selected="selected">--organisation capacity--</option>
            </select>        
        </td>
    </tr>
	<tr>
    	<th><?php echo $nObj->setHeader("responsible_business_patner"); ?>:</th>
    	<td>
        	<select name="responsible_organisation" id="responsible_organisation">
            	<option value="">--responsible business patner--</option>
            </select>
        </td>
    </tr>      
	<tr>
    	<th><?php echo $nObj->setHeader("legislation_date"); ?>:</th>    
    	<td> 
          <input type="text" name="legislation_date" id="legislation_date" value="" class="datepicker" readonly="readonly" />
        </td>
    </tr>                      
	<tr>
    	<th><?php echo $nObj->setHeader("hyperlink_act"); ?>:</th>    
    	<td>
          <input type="text" name="hyperlink_to_act" id="hyperlink_to_act" value="" />
        </td>
    </tr> 
	<tr>
    	<td></td>    
    	<td>
          <input type="button" name="save" id="save" value=" Save " class="isubmit" />
        </td>
    </tr>                    
</table>