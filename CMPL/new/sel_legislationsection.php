<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/5/11
 * Time: 2:11 AM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("deliverable.js");
include("../header.php");
$legObj  = new LegislationSection();
$leg     = $legObj -> getLegislationSections( );
?>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
    </tr>
<?php
foreach($leg as $key => $legSections)
{
    ?>
        <tr>
            <td><?php echo $legSections['id']; ?></td>
            <td><?php echo $legSections['description']; ?></td>
        </tr>
    <?php
}
?>
</table>
<div><?php displayGoBack("", ""); ?></div>