<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/17/11
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */
$scripts = array("jquery.ui.deliverable.js");
include("../header.php");
?>
<script language="javascript">
$(function(){
		$("#display_deliverable").deliverable({addAction:true, id:$("#delid").val(), section:"new", url:"../class/request.php?action=NewDeliverable.getDeliverables", user:$("#userId").val()});	
})
</script>
<div id="display_deliverable"></div>
<input type="hidden" id="delid" name="delid" value="<?php echo $_GET['id']; ?>" />
<input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION['tid']; ?>" />
