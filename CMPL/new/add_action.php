<?php 
$scripts = array("action.js","jquery.ui.action.js");
include("../header.php");
$delObj = new Deliverable();
$deliverable = $delObj ->getDeliverable( $_GET['id'] , "");
$nObj = new Naming();
$nObj -> setThColuomns();

$delObj     = new NewDeliverable();
$orgtypeObj = new OrganisationTypes();
$eventObj   = new Events();

$orgtype      = $orgtypeObj->getAOrganisationType($deliverable['applicable_org_type']); 	   	  
$complianceto = $delObj->getComplianceToList($_GET['id']);	  	    
$mainevent    = $eventObj->getAEvent($deliverable['main_event']);		   
$subevents    = $delObj->getSubEventList($_GET['id']);        
$accountables = $delObj->getAccountablePersonList($_GET['id']);  

$deptObj = new Directorate();
?>
<?php JSdisplayResultObj(""); ?>
<table width="100%" class='noborder'>
	<tr>
    	<td valign="top" width="50%" class='noborder'>
           
            <table width="100%">
			   <tr>
			   	  <td colspan="2"> <h4>Deliverable Details</h4></td>
			   </tr>            
                  <tr>
                    <th>Ref #:</th>
                    <td><?php echo $deliverable['ref']; ?></td>
                </tr>
               <tr>
                    <th><?php echo $nObj->setHeader("short_description_of_legislation"); ?>:</th>
                    <td><?php echo $deliverable['short_description_of_legislation']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("description_of_legislation"); ?>:</th>
                    <td><?php echo $deliverable['description_of_legislation']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_section"); ?>:</th>
                    <td><?php echo $deliverable['legislation_section']; ?></td>
                </tr>				
                <tr>
                    <th><?php echo $nObj->setHeader("frequency_of_compliance"); ?>:</th>
                    <td><?php echo $deliverable['compliance_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("applicable_organisation_type"); ?>:</th>
                    <td><?php echo $orgtype['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislation_compliance_date"); ?>:</th>
                    <td><?php echo $deliverable['compliance_date']; ?></td>
                </tr> 
                <tr>
                    <th><?php echo $nObj->setHeader("responsible_department"); ?>:</th>
                    <td><?php 
                    	$dir =  $deptObj -> fetch( $deliverable['responsible_department'] );
                    	$subDir = $deptObj -> getSubDirectorate($deliverable['responsible_department']);
                    	echo $dir['name']; 
                    	?></td>
                </tr>                
                <tr>
                    <th><?php echo $nObj->setHeader("functional_service"); ?>:</th>
                    <td><?php echo $deliverable['functional_service']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("accountable_person"); ?>:</th>
                    <td><?php
                     		echo implode(",", $accountables);
                     	?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("responsibility_ower"); ?>:</th>
                    <td><?php echo $deliverable['responsibility_ower']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("legislative_deadline_date"); ?>:</th>
                    <td><?php echo $deliverable['legislation_deadline']; ?></td>
                </tr>
                <!-- <tr>
                    <th>Organisation KPI Ref</th>
                    <td><?php echo $deliverable['org_kpi_ref']; ?>:</td>
                </tr> --> 
                <tr>
                    <th><?php echo $nObj->setHeader("sanction"); ?>:</th>    
                    <td>
                      <?php echo $deliverable['sanction']; ?>
                    </td>
                </tr>     
                <tr>
                    <th><?php echo $nObj->setHeader("reference_link"); ?>:</th>    
                    <td><?php echo $deliverable['reference_link']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("assurance"); ?>:</th>    
                    <td><?php echo $deliverable['assurance']; ?></td>
                </tr>                                             
                <tr>
                    <th><?php echo $nObj->setHeader("compliance_name_given_to"); ?>:</th>
                    <td><?php echo implode(",", $complianceto); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("main_event"); ?>:</th>
                    <td><?php echo $mainevent['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("sub_event"); ?>:</th>
                    <td><?php echo implode(",", $subevents); ?></td>
                </tr>
                <tr>
                    <th><?php echo $nObj->setHeader("guidance"); ?>:</th>
                    <td><?php echo $deliverable['guidance']; ?></td>
                </tr> 
				<tr>
					<td class="noborder" colspan="2"><?php displayGoBack("", ""); ?></td>
				</tr>
            </table>

        </td>
        <td valign="top" class='noborder' width="50%">
            <table width="100%">
				<tr>
					<td colspan='2' class='noborder'><h4>Add New Action</h4></td>
				</tr>
                <tr>
                    <th><?php echo $nObj->setHeader("action_required"); ?>:</th>    
                    <td>
                        <textarea rows="7" cols="80" id="action_name" name="action_name"></textarea>
                    </td>
                </tr>
   
                <tr>
                    <th><?php echo $nObj->setHeader("action_owner"); ?>:</th>    
                    <td>
                        <select name="action_owner" id="action_owner">
                            <option value="">--action owner--</option>
                        </select>        
                    </td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("action_deliverable"); ?>:</th>    
                    <td>
                    	<textarea rows="7" cols="80" id="deliverable" name="deliverable"></textarea>
                    </td>
                </tr>   
                <tr>
                    <th><?php echo $nObj->setHeader("action_deadline_date"); ?>:</th>    
                    <td><input type="text" name="deadline" id="deadline" value="" class="datepicker" readonly="readonly" /></td>
                </tr>    
                <tr>
                    <th><?php echo $nObj->setHeader("action_reminder"); ?>:</th>    
                    <td><input type="text" name="reminder" id="reminder" value="" class="datepicker" readonly="readonly" /></td>
                </tr>
                 <tr>
                    <th>Recurring:</th>    
                    <td>
                      <input type="checkbox" name="recurring" id="recurring" value="1" />
                    </td>
                </tr>                                                 
                <tr>
                    <td></td>    
                    <td>
                      <input type="button" name="save" id="save" value=" Save " class='isubmit' />
                      <input type="hidden" name="deliverableid" id="deliverableid" value="<?php echo $_GET['id']; ?>">
                    </td>
                </tr>
				<tr>
					<td colspan="2"  class='noborder'>
					
					<script language="javascript">
					$(function(){
						$("#display_action").action({editAction:true,deleteActions:true, id:$("#deliverableid").val(), controller : "newcontroller", section:"new"});
					})
					</script>
					<div id="display_action"></div>
					</td>
				</tr>                  
            </table>        
        </td>
    </tr>
</table>
