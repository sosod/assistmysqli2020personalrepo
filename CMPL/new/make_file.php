<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/19/11
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */
include_once("../../library/dbconnect/dbconnect.php");
include_once("../class/naming.php");
$nObj = new Naming();
$nObj -> setThColuomns(); 
  $fieldsList = array(
                    array(
                               	$nObj->setHeader("short_description_of_legislation"),
                                $nObj->setHeader("description_of_legislation"),
                                $nObj->setHeader("legislation_section"),
                                $nObj->setHeader("frequency_of_compliance"),
                                $nObj->setHeader("applicable_org_type"),
                                $nObj->setHeader("is_date_reccuring"),
                                $nObj->setHeader("legislation_compliance_date"),
                                $nObj->setHeader("number_of_days"),
                                $nObj->setHeader("days_option"),                       
                                $nObj->setHeader("days_from"),         
                                $nObj->setHeader("responsible_department"),
                                $nObj->setHeader("functional_service"),
                                $nObj->setHeader("accountable_person"),
                                $nObj->setHeader("responsibility_ower"),
                                $nObj->setHeader("legislative_deadline_date"),
                                $nObj->setHeader("sanction"),
                                $nObj->setHeader("reference_link"),
                                $nObj->setHeader("assurance"),
                                $nObj->setHeader("compliance_name_given_to"),
                                $nObj->setHeader("main_event"),
                                $nObj->setHeader("sub_event"),
                                $nObj->setHeader("guidance"),
                                $nObj->setHeader("does_this_deliverable_has_actions")
                           ),
                      array(
                                "text",
                                "text",
                                "text",
                                "text - select options",
                                "text - select options",
                                "text(Yes/No)",
                                "date YYYY/MM/DD eg. (2012/06/17)",
                      			"number - days",                      
                      			"text - (Days/Working Days)",
                      			"text - select options",
                                "text - select options",
                                "text - select options",
                                "text - select options",
                                "text - select options",
                                "date YYYY/MM/DD eg. (2012/06/17)",
                                "text",
                                "text",
                                "text",
                                "text - select options",
                                "text - select options",
                                "text - select options",
                                "text",
                      			"text(Yes/No)"
                           )

                    );
   $filename =  "deliverable.csv";
   $fp       =  fopen($filename, "w+");
   foreach($fieldsList as $fields){
        fputcsv($fp, $fields);
   }
   fclose($fp);
   header('Content-Type: application/csv'); 
   header("Content-length: " . filesize($filename)); 
   header('Content-Disposition: attachment; filename="' . $filename . '"'); 
   readfile($filename);
   unlink($filename);
   exit();	 