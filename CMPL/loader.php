<?php
class Loader {

	public static function autoload( $class )
	{
		self::requireFileName( $class );
	}
	
	public static function requireFileName( $filename )
	{
		if( file_exists( "../class/".strtolower($filename).".php" ) ){
			require_once( "../class/".strtolower($filename).".php" );
		} else if( file_exists( "../../".strtolower($filename).".php" ) ) {
			require_once( "../../".strtolower($filename).".php" );		
		} else if( file_exists( "../".strtolower($filename).".php" ) ) {
			require_once( "../".strtolower($filename).".php" );		
		}  else if(file_exists("../../library/dbconnect/".strtolower($filename).".php")) {
			require_once( "../../library/dbconnect/".strtolower($filename).".php" );		
		} else if(file_exists("../../library/database/".strtolower($filename).".php")) {
			require_once( "../../library/database/".strtolower($filename).".php" );		
		} else {
			require_once( strtolower($filename).".php" );		
		}
	}
	
}