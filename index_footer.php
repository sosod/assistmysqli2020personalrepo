      <div id="footer">
        <div id="footer-top-row">
          <a href="http://www.actionassist.co.za/" style='float:right'>www.actionassist.co.za</a>
          <a id="action-bookmark" href="#" onclick="return bookmark();" class="action-bookmark">Bookmark</a>
        </div>
        <div id="footer-middle-row">
          <a href="index.php" >Login</a> |
          <a href="action_assist_terms_and_conditions.pdf" target=_blank >Terms &amp; Conditions</a> |
          <a href="privacy.php">Privacy Policy</a> |
          <a href="legal.php">Legal</a> |
          <a href="IRS - Promotion of Access to Information 20111206.pdf" target=_blank>PAIA Manual</a>
        </div>
		<div>Action iT (Pty) Ltd is an authorised Financial Services Provider (FSP no. 40003).</div>
        <div id="footer-bottom-row"><p>&copy; <?php echo date("Y"); ?> Action iT (Pty) Ltd. All Rights Reserved.</p></div>
      </div><!-- #footer -->
