<?php
$no_title = array("TD");

function echoContentsLink($link) {
	global $menurow;
	$mloc = $menurow['modlocation'];
	if(isset($link[1]) && isset($link[2])) {
		echo "<span style=\"font-size: 10pt;\"><img src='pics/bullet_w.gif'><a href=\"$mloc/".strtolower($link['2'])."\" target=main>".$link['1']."</a>&nbsp;&nbsp;</span>";
	}
}
function echoContentsLinkFormatted($link) {
    echo "<span style=\"font-size: 10pt;\" class=show_processing><img src='pics/bullet_w.gif'><a href=\"/".$link['link']."\" target=main>".$link['name']."</a>&nbsp;&nbsp;</span>";
}
function displayContentsTitle() {
	global $menurow;
	global $isAdminUser;
	$mloc = $menurow['modlocation'];
	echo "<span style=\"font-weight: bold; font-size: 10pt;\">".($isAdminUser?"":"<a href='$mloc/main.php' style=\"text-decoration: none; color: #ffffff\">").$menurow['modtext']."</a>:&nbsp;&nbsp;</span>";
}

//require 'inc_session.php';
//error_reporting(0);
//require_once 'inc_db.php';
//require_once 'inc_db_conn.php';
/*
require_once 'library/class/assist_helper.php';
require_once 'library/class/assist_dbconn.php';
require_once 'library/class/assist_db.php';
require_once 'library/class/assist.php';
require_once 'library/class/assist_module_helper.php';
*/
require_once 'module/autoloader.php';

$me = new ASSIST_MODULE_HELPER();
$isSupportUser = $me->isSupportUser();
$isAdminUser = $me->isAdminUser();
$isAssistHelp = $me->isAssistHelp();
$isActiveReseller = $me->isActiveReseller();
$isDemoCompany = $me->isDemoCompany();
$cmpcode = $me->getCmpCode();
$tkid = $me->getUserID();
$tkname = $me->getUserName();
$jean = $me->getJeanModules();
//$isSupportUser = false;
//echo $isSupportUser;


function menuBlank() { echo "<option value=X>----------------------------</option>"; }
$menuref = "";
$cust = isset($_REQUEST['cust']) ? $_REQUEST['cust'] : "N";
if(isset($_REQUEST['m'])) {
	$menuref = $_REQUEST['m'];
if($menuref!="link") {
	//activity log
		if(isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
			$unique_id = "PHPSESSID=".$_COOKIE['PHPSESSID'];
		} elseif(isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
			$unique_id = $_SERVER["HTTP_COOKIE"];
		} else {
			$unique_id = "";
		}
	$me->db_insert("INSERT INTO assist_".$cmpcode."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) VALUES (null,now(),'$tkid','$tkname','$menuref',1,'".$unique_id."','".$_SERVER['REMOTE_ADDR']."')");
}

    if($menuref == "userman" || $menuref == "changepass" || $menuref == "action_dashboard") {
        $mr = "main";
		if($menuref=="action_dashboard") { $menuref = "main"; }
    } else {
        $mr = $menuref;
    }
    $_SESSION['ref'] = $mr;
    $_SESSION['modref'] = $mr;

    $contents = $mr;
	$set_session = true;
    switch($menuref)
    {
		case "link":
			$set_session = false;
			$url = "/main/link.php?act=stop&modref=".$_REQUEST['mr'];
			break;
        case "main":
			$set_session = false;
            $url = "main_login.php";
            break;
        case "man":
			$set_session = false;
            $url = "man/main.php";
            break;
        case "userman":
			$set_session = false;
            $url = "manuals.php";
            break;
        case "changepass":
			$set_session = false;
            $url = "TK/password.php";
            break;
        case "user_profile":
			$set_session = false;
            $url = "main/user_profile.php";
            break;
/*		case "supporthelp":
			if($_SESSION['ia_reseller']) {
				$url = "ignite_help_reseller/main.php";
				$contents = "ignite_help_reseller";
				$_SESSION['ref'] = $contents;
				$menuloc = $contents;
				$menurow = array(
					'modlocation'=>"ignite_help_reseller",
					'modref'=>"IHR",
					'modtext'=>"Ignite Help for Resellers",
				);
			} else {
				$url = "ignite_help_client/main.php";
				$contents = "ignite_help_client";
				$_SESSION['ref'] = $contents;
				$menuloc = $contents;
				$menurow = array(
					'modlocation'=>"ignite_help_client",
					'modref'=>"IH",
					'modtext'=>"Ignite Help",
				);
			}
			break;*/
		case "supporthelp":
			if($isAssistHelp) {
				$url = "ignite_help_assist/main.php";
				$contents = "ignite_help_assist";
				$_SESSION['ref'] = $contents;
				$menuloc = $contents;
				$menurow = array(
					'modlocation'=>"ignite_help_assist",
					'modref'=>"IHA",
					'modtext'=>"Ignite Help for Ignite Assist",
				);
			} elseif($isActiveReseller) {
				$url = "ignite_help_reseller/main.php";
				$contents = "ignite_help_reseller";
				$_SESSION['ref'] = $contents;
				$menuloc = $contents;
				$menurow = array(
					'modlocation'=>"ignite_help_reseller",
					'modref'=>"IHR",
					'modtext'=>"Ignite Help for Resellers",
				);
			} else {
				$url = "ignite_help_client/main.php";
				$contents = "ignite_help_client";
				$_SESSION['ref'] = $contents;
				$menuloc = $contents;
				$menurow = array(
					'modlocation'=>"ignite_help_client",
					'modref'=>"IH",
					'modtext'=>"Ignite Help",
				);
			}
			break;
        case "REPORT":
            $url = "report/main.php";
			$menuloc = $menuref;
			$set_session = false;
            break;
        case "S":
            $url = "S/main.php";
			$menuloc = $menuref;
				$_SESSION['ref'] = $menuref;
				$menurow = array(
					'modlocation'=>$menuref,
					'modref'=>$menuref,
					'modtext'=>"Setups",
				);
            break;
		case "TKSU":
            $url = "TKSU/main.php";
			$menuloc = $menuref;
				$_SESSION['ref'] = $menuref;
				$menurow = array(
					'modlocation'=>$menuref,
					'modref'=>$menuref,
					'modtext'=>"Users",
				);

				break;
		case "reports":
            $url = "reports/main.php";
			$menuloc = $menuref;
				$_SESSION['ref'] = $menuref;
				$menurow = array(
					'modlocation'=>$menuref,
					'modref'=>$menuref,
					'modtext'=>"System Reports",
				);

				break;
		case "module_management":
		case "dashboard_management":
			$url = $menuref."/main.php";
			$menuloc = $menuref;
			$_SESSION['ref'] = $menuref;
			$x = explode("_",$menuref);
			$t = ucfirst(implode(" ",$x));
			$menurow = array(
				'modlocation'=>$menuref,
				'modref'=>$menuref,
				'modtext'=>$t,
			);

			break;
		case "helpdesk":
			$url = "helpdesk/main.php";
			$menuloc = $menuref;
			$_SESSION['ref'] = $menuref;
			$menurow = array(
				'modlocation'=>$menuref,
				'modref'=>$menuref,
				'modtext'=>"Helpdesk",
			);
			break;

        case "TK":
            $url = "TK/main.php";
			$menuloc = $menuref;
				$_SESSION['ref'] = $menuref;
				$menurow = array(
					'modlocation'=>$menuref,
					'modref'=>$menuref,
					'modtext'=>"Users",
				);
            break;
        default:
            $sql = "SELECT * FROM assist_menu_modules WHERE modref = '$menuref' AND (modyn = 'Y' OR modadminyn = 'Y')";
			$menurow = $me->mysql_fetch_one($sql);
			if(count($menurow)==0) {
				//what to do if module isn't found?
				$set_session = false;
				$redirect_me = "redirectMe('$menuref');";
			} else {

	            $menuloc = $menurow['modlocation'];
	            $url = $menuloc."/main.php";
	            if($isAdminUser && file_exists($menuloc."/setup.php")) {
	                $url = $menuloc."/setup.php";
	            }
				$_SESSION['modref'] = strtoupper($menurow['modref']);
				$_SESSION['modtext'] = $menurow['modtext'];
				$_SESSION['modlocation'] = isset($menuloc) ? $menuloc : "";
				$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
				$_SESSION['dbref'] = $dbref;
			}
            break;
    }
if($set_session) {
	$me->setSessionDetails($menuref,$menurow);
}
	if(!isset($_REQUEST['src'])) {
		if(isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0) {
			$src = explode("/",$_SERVER['HTTP_REFERER']);
			$src = $src[count($src)-1];
			if(strpos($src,"?")!==false) {
				$src = explode("?",$src);
				$src = $src[0];
			}
		} else {
			$src = "title_login.php";
		}
	} else {
		$src = $_REQUEST['src'];
	}

    echo "<script type=text/javascript> ";
	//echo "parent.footer.location.href = 'footer_login.php'; ";
	if(!isset($redirect_me)) {
		echo "parent.footer.location.href = 'footer_login.php?mr=".strtoupper(isset($menurow['modref'])?$menurow['modref']:"")."'; ";
		if($src != "main_login.php" || (isset($_REQUEST['act']) && $_REQUEST['act']=="change")) {
			echo "console.log('$url');";
			echo "setTimeout(\"parent.main.location.href = '".$url."'\",1000);";
		}
	} else {
		echo $redirect_me;
	}
	echo "</script>";
} else {
    $contents = "main";
}
$ck = "";
//if(strlen($cmpcode) > 6)
//{
    $ck = "Y";
    $cmpname = $_SESSION['ia_cmp_name'];
    $cmpdisp = $_SESSION['ia_cmp_dispname'];
    $cmplogo = $_SESSION['ia_cmp_logo'];
//}
//else
//{
//    $ck = "N";
//    killMe("Company code error: $cmpcode");
//}

switch($contents)
{
    case "HP":
        include("inc_hp.php");
        break;
    default:
        break;
}

/*
if($_SESSION['ia_cmp_reseller']=="ARM0001") {
$title_logo = "/pics/bp_logos/armsassist2.png";
} else {
$title_logo = "/pics/assist_logo_h75.jpg";
}
*/
$title_logo = $me->getIgniteTitleLogo();

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Assist</title>
	<link href="/library/jquery-ui-1.10.0/css/jquery-ui.css?1456142536" rel="stylesheet" type="text/css"/>

<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery.min.js"></script>
<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery-ui.min.js"></script>

<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
<script type ="text/javascript" src="/library/js/assiststring.js"></script>
<script type ="text/javascript" src="/library/js/assistform.js"></script>
<link rel="stylesheet" href="/assist.css" type="text/css">

<script type=text/javascript>
/*function changeMenu(me) {
    switch(me.value)
    {
        case "logout":
            parent.main.location.href = "logout.php";
            break;
        case "X":
            break;
        default:
            parent.header.location.href = 'title_login.php?m='+me.value;
            break;
    }
}
*/
$(function() {
	$(".show_processing").click(function() {
		var sending_module = '<?php echo $menuref; ?>';//check current module - if PMSPS then don't use jquery because they are running old version and can't display popup IGN0001/HDR57
		//parent.main.AssistHelper.processing();
		/* decentralised code (above) to here to deal with modules who don't yet include AssistHelper central class by default [JC 22 Jan 2019] */
		/* ---- original code ---- 
			//only display if not already open
			if($("#dlg_msg").length==0) {
					$('<div />',{id:"dlg_msg",html:"<p class=center>Processing...</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>"})
					.dialog({modal:true,closeOnEscape:false});
					//$('.ui-dialog-titlebar').hide();
					AssistHelper.hideDialogTitlebar("id","dlg_msg");
			} else {
				AssistHelper.showProcessing();
			}
		*/
//console.log(typeof(jQuery));			
//console.log(typeof(parent.main.jQuery)); //- should return function if present			
			if(typeof parent.main.jQuery == 'undefined' || sending_module=="PMSPS") {
				//do nothing - ("can't find jquery");
			} else {
				//show processing box
				$('<div />',{id:"dlg_msg_from_titlebar",html:"<p class=center>Processing...</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>"}).appendTo(parent.main.document.body);
				parent.main.$("#dlg_msg_from_titlebar").dialog({modal:true});
				parent.main.$("#dlg_msg_from_titlebar").siblings("div.ui-dialog-titlebar").hide();
			}

	});
	$("#div_overlay").dialog({
		modal:true,
		width: "0px",
		height: "0px",
		position:  [0, 0],
		resizeable: false,
		dialogClass: 'timeout-dialog',
		autoOpen:false
	});
	AssistHelper.hideDialogTitlebar("id","div_overlay");

	function redirectMe(mr) {
		parent.main.AssistHelper.processing();
		parent.header.location.href = "/title_login.php?m=link&mr="+mr;
	}
	$("#backHome").click(function() {
		parent.main.AssistHelper.processing();
		parent.header.location.href = "/title_login.php?m=action_dashboard";
	});
	$("#main_menu").change(function() {
		var v = $(this).val();
		var sending_module = '<?php echo $menuref; ?>';	//check if the current module is an IAS module - if it is then don't use the jquery IGN0001/HDR57

		if(v=="X") {
			//do nothing
		} else if(v=="logout") {
					console.log("logout triggered");
			//parent.main.AssistHelper.processing(); //- see note in show_processing.click function [JC 22 Jan 2019]

			if(typeof parent.main.jQuery == 'undefined' || sending_module=="PMSPS") {
				//do nothing - ("can't find jquery");
			} else {
				//show processing box
				$('<div />',{id:"dlg_msg_from_titlebar",html:"<p class=center>Processing...</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>"}).appendTo(parent.main.document.body);
				parent.main.$("#dlg_msg_from_titlebar").dialog({modal:true});
				parent.main.$("#dlg_msg_from_titlebar").siblings("div.ui-dialog-titlebar").hide();
			}
			parent.main.location.href = "logout.php";
		} else {
			//parent.main.AssistHelper.processing(); //- see note in show_processing.click function [JC 22 Jan 2019]
			if(typeof parent.main.jQuery == 'undefined' || sending_module=="PMSPS") {
				//do nothing - ("can't find jquery");
			} else {
				//show processing box
				$('<div />',{id:"dlg_msg_from_titlebar",html:"<p class=center>Processing...</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>"}).appendTo(parent.main.document.body);
				parent.main.$("#dlg_msg_from_titlebar").dialog({modal:true});
				parent.main.$("#dlg_msg_from_titlebar").siblings("div.ui-dialog-titlebar").hide();
			}
			document.location.href = 'title_login.php?m='+v;
		}
	});


	$(window).resize(function() {
		//setOverlayDimensionsToCurrentDocumentDimensions();
	})


});
function showOverlay() {
	$(function() {
		$("#div_overlay").dialog("open");
	});
}
function hideOverlay() {
	$(function() {
		$("#div_overlay").dialog("close");
	});
}

</script>

<style type=text/css>
.timeout-dialog { border: 0px solid #dedede; background: #dedede; }
table { border-width: 0px; }
table td { padding: 0px; vertical-align: middle; border-width: 0px; font-style: Arial; }
a, a:visited { color: #FFFFFF; text-decoration: none; }
a:hover { color: #FFFFFF; }
</style>
<base target=main>
</head>
<body topmargin=2 leftmargin=0 bottommargin=0 rightmargin=0>
<table width="100%" cellpadding=0 cellspacing=0 >
	<tr>
		<td width=25% class=left>&nbsp;<img src="<?php echo "/".$title_logo; ?>" ALT="Ignite Assist" USEMAP=#logo_shortcut border=0 height=75 /><span id=backHome style="cursor: pointer;"></span></td>
    	<td width=50% class="center title"><?php if($ck == "Y") { echo($cmpdisp); } ?></td>
		<td width=25% class=right>&nbsp;<?php if($ck == "Y") { echo("<img src=/pics/logos/".$cmplogo.">"); }?></td>
	</tr>
	<tr>
		<td colspan="3" height="25" class=backcolor>
			<table width=100% cellpadding=0 cellspacing=0 >
				<tr>
					<td width=10 ><img src=/pics/blank.gif height=10 width=1></td>
<?php
    if(strlen($contents) > 0 && $contents!="main")
    {
		$inccont = ($cust=="Y") ? $contents."/".$cmpcode."/contents.php" : "contents/".$menuloc.".php";
		if(file_exists($inccont) == true) {
			if(!in_array($contents,$jean)) {
				echo "<td style=\"color: #ffffff;\">";
				if(!in_array($contents,$no_title) || $tkid=="0000") {
					displayContentsTitle();
				}
				$dbref = "assist_".$cmpcode."_".strtolower($menurow['modref']);
				$mloc = $menurow['modlocation'];
				$_SESSION['modref'] = strtoupper($menurow['modref']);
				$_SESSION['modtext'] = $menurow['modtext'];
				$_SESSION['modlocation'] = isset($menuloc) ? $menuloc : "";
				$_SESSION['dbref'] = $dbref;
			}
            include($inccont);
            echo "</td>";
        }
        else
        {
            ?>
					<td width=50>&nbsp;</td>
					<td>&nbsp;</td>
        <?php
        }
    }
?>
					<td class=right>
<?php
if($ck == "Y")
{
    ?>
    <select id=main_menu>
          <option selected value=X>MAIN MENU</option>
          <?php menuBlank(); ?>
<?php
if($isAdminUser) {
    $sql = "SELECT * FROM assist_menu_modules WHERE modadminyn = 'Y' ORDER BY modtext";
} else {
    $sql = "SELECT DISTINCT m.* FROM assist_menu_modules m, assist_".$cmpcode."_menu_modules_users mu WHERE m.modyn = 'Y' AND m.modref = mu.usrmodref AND mu.usrtkid = '$tkid' ORDER BY m.modtext";
}
$rows = $me->mysql_fetch_all($sql);
foreach($rows as $row) {
            $mr = $row['modref'];
            $mt = $row['modtext'];
        ?>
          <option value="<?php echo $mr; ?>"><?php echo $mt; ?></option>
        <?php
        }
    if($isAdminUser) {
		menuBlank();
    	$admin_modules = $me->getAdminSystemMenu();
    	foreach($admin_modules as $ref => $name) {
    		echo "<option value=".$ref.">".$name."</option>";
		}
    } elseif($isSupportUser) {
		//maybe unlock admin session?
    }
?>
          <?php menuBlank(); ?>
		  <?php if(!$isAdminUser) { ?>
          <option value=changepass>Change Password</option>
          <option value=user_profile>My Profile</option>
          <?php } ?>
           <option value=man>Need Help?</option>

          <?php menuBlank(); ?>
          <option value=logout>Logout</option>
          <?php menuBlank(); ?>
    </select>&nbsp;<?php
}
?>
                    </font></i></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<map name="logo_shortcut">
	<area shape=circle COORDS="23,22,23" HREF="title_login.php?m=action_dashboard" target=_self ALT="Back to Action Dashboard">
	<area shape=default NOHREF>
</map>

<div id=div_overlay style='width:0px;height:0px'></div>

</body>

</html>
