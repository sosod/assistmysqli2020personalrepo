<?php

require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);


$displayObject = new PM6_DISPLAY();
$js = '';

//echo '<pre style="font-size: 18px">';
//echo '<p>REQUEST</p>';
//print_r($_REQUEST);
//echo '</pre>';

/****************************************************/
$emp_obj = new PM6_EMPLOYEE();

$job_titles = $emp_obj->getAllJobTitlesFormattedForSelect();
$departments = $emp_obj->getAllDepartmentsFormattedForSelect();
$job_levels = $emp_obj->getAllJobLevelsFormattedForSelect();

//Fields to sort by
$sort_by = array();
$sort_by['employee'] = 'Employee';
$sort_by['job_title'] = 'Job Title';

$sort_by['job_manager'] = 'Manager';
$sort_by['job_dept'] = 'Department';
$sort_by['job_level'] = 'Job Level';

//$sort_asc_desc
$sort_asc_desc = array();
$sort_asc_desc['asc'] = 'ASC';
$sort_asc_desc['desc'] = 'DESC';

//echo '<pre style="font-size: 18px">';
//echo '<p>JOB TITLES</p>';
//print_r($job_titles);
//echo '</pre>';
//
//echo '<pre style="font-size: 18px">';
//echo '<p>ALL DEPS</p>';
//print_r($departments);
//echo '</pre>';
//
//echo '<pre style="font-size: 18px">';
//echo '<p>JOB LEVELS</p>';
//print_r($job_levels);
//echo '</pre>';

?>

<h1>Report Filters</h1>
<table class="form">
    <input type="hidden" id="i" name="i" value="<?php echo $_REQUEST['i']; ?>">
    <input type="hidden" id="period_id" name="period_id" value="<?php echo $_REQUEST['filter']['period_id']; ?>">
    <tr>
        <th>Manager:</th>
        <td>
            <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'job_manager_title', 'name' => 'job_manager_title', 'required' => true, 'unspecified' => false,  'options' => $job_titles ));?>
        </td>
    </tr>
    <tr>
        <th>Department:</th>
        <td>
            <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'job_dept', 'name' => 'job_dept', 'required' => true, 'unspecified' => false,  'options' => $departments ));?>
        </td>
    </tr>
    <tr>
        <th>Job Level:</th>
        <td>
            <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'job_level', 'name' => 'job_level', 'required' => true, 'unspecified' => false,  'options' => $job_levels ));?>
        </td>
    </tr>
    <tr>
        <th>Sort By:</th>
        <td>
            <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'sort_by', 'name' => 'sort_by', 'required' => true, 'unspecified' => false,  'options' => $sort_by ));?>
        </td>
    </tr>
    <tr>
        <th>Sort - ASC / DESC:</th>
        <td>
            <?php $js .= $displayObject->drawFormField('LIST', array('id' => 'sort_asc_desc', 'name' => 'sort_asc_desc', 'required' => true, 'unspecified' => false,  'options' => $sort_asc_desc ));?>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(function() {
        <?php echo $js; ?>

        //Tell parent window to open the dialog once the page has finished loading
        window.parent.openDialog();
    });
</script>