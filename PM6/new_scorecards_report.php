<?php

require_once("inc_header.php");

//
//$info_message = "This page allows you to build a new |scorecard| or approve/reject a new |scorecard| for anyone below you in your organisational structure.  Once the |scorecard| has been approved by both you and the relevant employee, the |scorecard| will then be available in the ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_manage"),"|"))." section and any further amendments and approvals will take place there.
//				<br /><br />If you believe that there is an error on this page, please contact the Employee Assist administrator or Assist Support.";
//ASSIST_HELPER::displayResult(array("info",$helper->replaceAllNames($info_message)));


ASSIST_HELPER::displayResult(isset($_REQUEST['r'])?$_REQUEST['r']:array());

$settingObject = new PM6_SETUP_EMPLOYEE();
$empObject = new PM6_EMPLOYEE();
$scdObject = new PM6_SCORECARD();

//1 Does the user have access to create their own scorecard? OR is there a SCD waiting Review?
$can_i_create_active_users_staff_scorecard = $settingObject->canICreateActiveUserStaffScorecards();
$can_i_create_non_users_staff_scorecard = $settingObject->canICreateNonUserStaffScorecards();
$are_any_scorecards_waiting_for_my_review = $scdObject->areAnyScorecardsWaitingMyReview();

//Get financial year info
$start_date = $empObject->getFinancialYearStartDate();
$end_date = $empObject->getFinancialYearEndDate();

//2 Get JOB details that fall into the selected financial year
$staff_data = $empObject->getAllStaffDetailsForReport($start_date,$end_date);

$jobs = $staff_data['jobs'];
$employee_tkids = $staff_data['employee_tkids'];

//3 Get SCD details that relate to JOBs
$all_scorecards = $scdObject->getAllCurrentScorecardEmployeesAndJobsWithStatus($employee_tkids,false,true);


//4 Format Jobs & Scorecards for display
$available_objects = array();
foreach($jobs as $job) {
	$active_scorecard = true;
	$active_employee = $job['employee_status'];
	$employee_user_id = $job['user_id'];
	$my_scorecards = isset($all_scorecards[$employee_user_id]) ? $all_scorecards[$employee_user_id] : array();
	$emp_id = $job['emp_id'];
	$job_id = $job['job_id'];
	$bonus_scale = isset($my_scorecards[$job_id]['bonus_scale_id']) ? $my_scorecards[$job_id]['bonus_scale_id'] : 0;
	$scd_id = (isset($my_scorecards[$job_id])?$my_scorecards[$job_id]['id']:"0");
	//Set everything needed for redirect by start page
	$page_id = "employee_id=";
	$page_id.= $employee_user_id."&emp_id=";
	$page_id.= $emp_id."&job_id=";
	$page_id.=$job_id."&obj_id=".$scd_id;
	$page_id.="&bonus_scale_id=".$bonus_scale;

	$name = $job['user_name'];

	$raw_status = isset($my_scorecards[$job_id]['new_status']) ? $my_scorecards[$job_id]['new_status'] : PM6::NOT_YET_STARTED;
	$page_id.="&new_status=".$raw_status;
	if($raw_status==PM6::NOT_YET_STARTED) {
		$active_scorecard = false;
	}
	$scd_status = isset($my_scorecards[$job_id]['status']) ? $my_scorecards[$job_id]['status'] : PM6::ACTIVE;
	$page_id.="&scd_status=".$scd_status;
	$status = $scdObject->convertNewStatusToText($raw_status);
	$general_status = $scdObject->convertSCDStatusToText($scd_status);
	$colour = $scdObject->convertNewStatusToColour($raw_status);
	$general_colour = $scdObject->convertSCDStatusToColour($scd_status);
	if(($scd_status&PM6::ACTIVATED)==PM6::ACTIVATED) {
		$step = "view";
	} elseif(($scd_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
		$step = "review";
	} elseif(($raw_status&PM6::IP_MANAGER)==PM6::IP_MANAGER) {
		$step = "continue";
	} elseif($raw_status==0) {
		$step = "new";
	} else {
		$step = "view";
	}
	$page_id.="&step=".$step;

	$extra_info_arr = array();
	$extra_info_arr['Job Title'] = (strlen($job['job_title'])>0) ? $job['job_title']:"";
	$extra_info_arr['Department'] = (strlen($job['job_dept'])>0) ? $job['job_dept']: "";
	$extra_info_arr['Location'] = (strlen($job['job_loc'])>0)?$job['job_loc']:"";
	$extra_info_arr['Manager'] = (strlen($job['job_manager'])>0?$job['job_manager']."<br />[<span class='i'>":"").$job['job_manager_title'].(strlen($job['job_manager'])>0?"</span>]":"");
	$job_dates = date("d M Y",strtotime($job['job_start']))." - ".(($job['job_end']!="1970-01-01" && $job['job_end']!="0000-00-00") ? date("d M Y",strtotime($job['job_end'])) : "On-going");
	$extra_info_arr['Job Dates'] = $job_dates;
	$extra_info_arr['Description']="".$job['job_description']."";
	if(isset($my_scorecards[$job_id])) {



		$process_key = "created";
		if(isset($my_scorecards[$job_id]['logs'][$process_key])) {
			$extra_info_arr[ucfirst($process_key)] = date("d M Y H:i",strtotime($my_scorecards[$job_id]['logs'][$process_key]['date']))."<br /><span class=i>[".($my_scorecards[$job_id]['logs'][$process_key]['user'])."]</span>";
		} else {
			$extra_info_arr['Created'] = date("d M Y H:i",strtotime($my_scorecards[$job_id]['created_on']))."<br />";
			$extra_info_arr['Created'].= "<span class=i>[".((isset($my_scorecards[$job_id]['created_by']) && !is_null($my_scorecards[$job_id]['created_by']) && strlen($my_scorecards[$job_id]['created_by'])>0) ? $my_scorecards[$job_id]['created_by'] : "Unknown User")."]</span>";
		}

		$process_key = "confirmed";
		if($my_scorecards[$job_id]['is_confirmed']==true && isset($my_scorecards[$job_id]['logs'][$process_key])) {
			$extra_info_arr[ucfirst($process_key)] = date("d M Y H:i",strtotime($my_scorecards[$job_id]['logs'][$process_key]['date']))."<br /><span class=i>[".($my_scorecards[$job_id]['logs'][$process_key]['user'])."]</span>";
		} else {
			$extra_info_arr[ucfirst($process_key)] = "";
		}

		$extra_info_arr['Approved'] = "";
		$test_keys = array(
				"_digitally_approved",
				"_sign_approved",
				"_sign_attached",
				"_approved_no_poe",
		);
		$approved = array();
		$e = "employee";
		if($my_scorecards[$job_id]['is_employee_approved']==true) {
			foreach($test_keys as $test) {
				$test = $e.$test;
				if(isset($my_scorecards[$job_id]['logs'][$test]) && !isset($approved[$e])) {
					$log = $my_scorecards[$job_id]['logs'][$test];
					if(strpos($test, "digitally") !== false && strpos($test, "no_poe") !== false) {
						$d = $log['comment'];
					} else {
						$d = $log['date'];
					}
					$approved[$e] = date("d M Y H:i", strtotime($d))."<br /><span class='i'>[".$log['user']."]</span>";
					break;
				}
			}
		}
		if(!isset($approved[$e])) {
			$approved[$e] = ucfirst($e).": Outstanding";
		} else {
			$approved[$e] = ucfirst($e).": ".$approved[$e];
		}

		$e = "manager";
		if($my_scorecards[$job_id]['is_manager_approved']==true) {
			foreach($test_keys as $test) {
				$test = $e.$test;
				if(isset($my_scorecards[$job_id]['logs'][$test]) && !isset($approved[$e])) {
					$log = $my_scorecards[$job_id]['logs'][$test];
					if(strpos($test, "digitally") !== false && strpos($test, "no_poe") !== false) {
						$d = $log['comment'];
					} else {
						$d = $log['date'];
					}
					$approved[$e] = date("d M Y H:i", strtotime($d))."<br /><span class='i'>[".$log['user']."]</span>";
					break;
				}
			}
		}
		if(!isset($approved[$e])) {
			$approved[$e] = ucfirst($e).": Outstanding";
		} else {
			$approved[$e] = ucfirst($e).": ".$approved[$e];
		}

		$process_key = "approved";
		if($my_scorecards[$job_id]['is_confirmed']==true) {
			$extra_info_arr[ucfirst($process_key)] = implode("<br />",$approved);
		} else {
			$extra_info_arr[ucfirst($process_key)] = "";
		}

		$process_key = "activated";
		if($my_scorecards[$job_id]['is_activated']==true && isset($my_scorecards[$job_id]['logs'][$process_key])) {
			$extra_info_arr[ucfirst($process_key)] = date("d M Y H:i",strtotime($my_scorecards[$job_id]['logs'][$process_key]['date']))."<br /><span class=i>[".($my_scorecards[$job_id]['logs'][$process_key]['user'])."]</span>";
		} else {
			$extra_info_arr[ucfirst($process_key)] = "";
		}
	} else {
		$extra_info_arr['Created'] = "";
		$extra_info_arr['Confirmed'] = "";
		$extra_info_arr['Approved'] = "";
		$extra_info_arr['Activated'] = "";
	}

	if($active_employee || $active_scorecard) {
		$available_objects[$page_id] = array(
			'id' => $employee_user_id,
			'scd_id' => ($scd_id > 0 ? $scdObject->getModRef()."/".$scdObject->getRefTag().$scd_id : ""),
			'name' => $name,
			'extra_info' => $extra_info_arr,
			'status' => array_merge($general_status, $status),
			'colour' => array_merge($general_colour, $colour)
		);
	}
}


$headings = array(
	'Emp Ref',
	'SCD Ref',
	'Name',
	'Job Title',
	'Department',
	'Location',
	'Manager',
	'Description',
	'Job Dates',
	'Created',
	'Confirmed',
	'Approved',
	'Activated',
	'Scorecard Status',
);


//5 Display SDBIP chooser style button to access SCD
//$include_status = true;
//$include_colour = false;
//$url = "new_staff_all_start.php?";
//$button_name = "Start";
//include("common/object_chooser.php");

//5 SCD Options:  New/IP -> Send to Create process; Waiting Approval -> Option to send reminder; Waiting Review -> Send to Confirmation page (step 10?)




?>
<table>
	<tr>
		<?php
		foreach($headings as $fld) {
			echo "<th>".$fld."</th>";
		}
//		<th></th>
		?>
	</tr>
	<?php
	foreach($available_objects as $page_id => $object) {
			$status = $object['status'];
			$colour = $object['colour'];
			if(is_array($status)) {
				foreach($status as $si => $stat) {
					$colr = $colour[$si];
					$status[$si] = "<span class='".$colr." b'>".$stat."</span>";
				}
				$status = implode("<br />",$status);
			} else {
				$status = "<span class='".$colour." b'>".$status."</span>";
			}

		echo "<tr>";
		foreach($headings as $fld) {
			if($fld=="Emp Ref") {
				echo "<td class=b>".$object['id']."</td>";
			} elseif($fld=="SCD Ref") {
				echo "<td class=b>".$object['scd_id']."</td>";
			} elseif($fld=="Name") {
				echo "<td class=b>".$object['name']."</td>";
			} elseif($fld=="Scorecard Status") {
				echo "<td class='b center'>".$status."</td>";
			} else {
				echo "<td>".$object['extra_info'][$fld]."</td>";
			}
		}
//		echo "<td class='center'><button class='btn-start' page_id='".$page_id."'>Open</button></td>";
		echo "</tr>";
	}
	?>
</table>
<p>&nbsp;</p>
<script type="text/javascript">
	$(function() {
		$(".btn-start").button({
			icons:{primary:"ui-icon-newwin"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey").hover(
			function() {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
			},function() {
				$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
			}
		).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var p = $(this).attr("page_id");
			document.location.href = '<?php echo $url; ?>'+p;
		});
	})
</script>