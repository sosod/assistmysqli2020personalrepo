<?php
require_once("inc_header.php");

echo "<div id=div_info class='info-divs'>";
ASSIST_HELPER::displayResult(array("info",$helper->replaceAllNames("This page allows you to build a new |scorecard| for yourself or approve/reject your new |scorecard| which has been built/amended by someone else.  Once your |scorecard| has been approved by both you and your manager, the |scorecard| will be available in the ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_manage"),"|"))." section and any further amendments and approvals will take place there.")));
echo "</div>";

ASSIST_HELPER::displayResult(isset($_REQUEST['r'])?$_REQUEST['r']:array());

$settingObject = new PM6_SETUP_EMPLOYEE();
$empObject = new PM6_EMPLOYEE();
$scdObject = new PM6_SCORECARD();

//1 Does the user have access to create their own scorecard? OR is there a SCD waiting Review?
$can_i_create_my_own_scorecard = $settingObject->canICreateMyOwnScorecard();
$is_my_scorecard_waiting_for_my_review = $scdObject->isMyOwnScorecardWaitingMyReview();



//2 Get JOB details that fall into the selected financial year
$start_date = $empObject->getFinancialYearStartDate();
$end_date = $empObject->getFinancialYearEndDate();
$jobs = $empObject->getJobsForFinancialYear($start_date,$end_date);

if($jobs===false) {
	ASSIST_HELPER::displayResult(array("info","You do not have access to manage your own Scorecard as you have not been setup as an Employee on Assist.<br />If you believe this to be incorrect, please contact your Assist Administrator."));
	die();
}

//3 Get SCD details that relate to JOBs
$all_scorecards = $scdObject->getAllCurrentScorecardEmployeesAndJobsWithStatus($scdObject->getUserID());
$my_scorecards = isset($all_scorecards[$scdObject->getUserID()]) ? $all_scorecards[$scdObject->getUserID()] : array();

//4 Format Jobs & Scorecards for display
$available_objects = array();
foreach($jobs as $job) {
	$display_me = true;
	$page_id = "emp_id=";
	$emp_id = $job['emp_id'];
	$page_id.= $emp_id."&job_id=";
	$id = $job['job_id'];
	$page_id.=$id."&obj_id=".(isset($my_scorecards[$id])?$my_scorecards[$id]['id']:"0");

	$name = $job['job_title']."";
	$raw_status = isset($my_scorecards[$id]['new_status']) ? $my_scorecards[$id]['new_status'] : PM6::NOT_YET_STARTED;
	$page_id.="&new_status=".$raw_status;
	$scd_status = isset($my_scorecards[$id]['status']) ? $my_scorecards[$id]['status'] : PM6::NOT_YET_STARTED;
	$page_id.="&scd_status=".$scd_status;
	if(($scd_status&PM6::ACTIVATED)==PM6::ACTIVATED) {
		$step = "view";
	} elseif(($scd_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
		$step = "review";
	} elseif(($raw_status&PM6::IP_EMPLOYEE)==PM6::IP_EMPLOYEE) {
		$step = "continue";
	} elseif($raw_status==0) {
		if(!$can_i_create_my_own_scorecard) { $display_me = false; }
		$step = "new";
	} else {
		$step = "view";
	}
	$page_id.="&step=".$step;
	$status = $scdObject->convertNewStatusToText($raw_status);
	$general_status = $scdObject->convertSCDStatusToText($scd_status);
	//$colour = isset($my_scorecards[$id]['status']) ? "orange" : "red";
	$colour = $scdObject->convertNewStatusToColour($raw_status);
	$general_colours = $scdObject->convertSCDStatusToColour($scd_status);
	$bonus_scale = isset($my_scorecards[$id]['bonus_scale_id']) ? $my_scorecards[$id]['bonus_scale_id'] : 0;
	$page_id.="&bonus_scale_id=".$bonus_scale;
	$job_dates = date("d F Y",strtotime($job['job_start']))." - ".(($job['job_end']!="1970-01-01" && $job['job_end']!="0000-00-00") ? date("d F Y",strtotime($job['job_end'])) : "On-going");
	$extra_info_arr = array();
	if(strlen($job['job_dept'])>0) {
		$extra_info_arr['Department'] = $job['job_dept'];
	}
	if(strlen($job['job_loc'])>0) {
		$extra_info_arr['Location'] = $job['job_loc'];
	}
	$extra_info_arr['Manager'] = (strlen($job['job_manager'])>0?$job['job_manager']." / ":"").$job['job_manager_title'];
	$extra_info_arr['Dates'] = $job_dates;
	$extra_info = "";
	foreach($extra_info_arr as $fld => $value) {
		$extra_info.="<span class=b>".$fld.":</span> ".$value."<br />";
	}
	if(strlen($job['job_description'])>0) {
		$extra_info.="<br /><span class=i>".$job['job_description']."</span>";
	}
	if($display_me) {
		$available_objects[$page_id] = array(
			'name'=>$name,
			'extra_info'=>$extra_info,
			'status'=>array_merge($general_status,$status),
			'colour'=>array_merge($general_colours,$colour)
		);
	}
}
if(count($available_objects)>0) {
	//5 Display SDBIP chooser style button to access SCD
	$include_status = true;
	$include_colour = false;
	$url = "new_my_start.php?";
	$button_name = "Open";
	include("common/object_chooser.php");
} else {
	ASSIST_HELPER::displayResult(array("info","No Scorecards waiting for review."));
}
//5 SCD Options:  New/IP -> Send to Create process; Waiting Approval -> Option to send reminder; Waiting Review -> Send to Confirmation page (step 10?)




?>