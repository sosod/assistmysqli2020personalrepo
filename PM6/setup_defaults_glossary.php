<?php
require_once("inc_header.php");

echo ASSIST_HELPER::getFloatingDisplay(array("info","Any blank headings will be replaced with the default terminology."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$mod = new ASSIST_MODULE_HELPER();
$db = new ASSIST_DB();
$dbref = $mod->getDBRef()."_";
$glossObj = new PM6_GLOSSARY();

//All glossary terms
$gloss = $glossObj->getGlossary();

$anchor = array();

foreach($gloss as $key=>$data){
	//Save keys for anchor bar
	$name = ucwords(str_ireplace("_"," ",$key));
	$anchor[]="<a style='text-decoration:none' href='#$key'>".$name."</a>";	
}
echo "<p><a name=toplink></a>".implode(" | ",$anchor)."</p>";


ASSIST_HELPER::displayResult(array("warn","Any changes made on this page will take 30 minutes to propogate to all users currently logged in.  Changes will reflect immediately for any user who logs in after they are made."));

$counter =0;
foreach($gloss as $key => $n) {
	$head = ucwords(str_ireplace("_"," ",$key));
	$log_section = strtoupper($key);

echo "<h2><a name='$key'></a>$head</h2>";
?>
<table class='tbl-container not-max'><tr><td>
<?php
?>
<form name=frm_<?php echo $key; ?>>
	<input type=hidden name=section value='<?php echo strtoupper($key); ?>' />
	<table id=tbl_<?php echo $key; ?>>
		<tr>
			<th>Ref</th>
			<th>System Name</th>
			<th>Your Name</th>
			<th>System Description</th>
			<th>Your Description</th>
			<th></th>
		</tr>
		<?php
		foreach($n as $k=>$object ){
			$orig = strlen($object['gloss'])>0?$object['gloss']:"";
			$ff = $displayObject->createFormField("LRGVC",array('id'=>$k,'name'=>$object['field'],'orig'=>$orig,'class'=>"valid8me"),$object['gloss']);
			echo "
			<tr id=tr_".$k.">
				<td>".$object['id']."</td>
				<td>".$object['system']."</td>
				<td>".$object['client']."</td>
				<td>".$object['system_gloss']."</td>
				<td id=td_".$k.">".$ff['display']."</td>
				<td><input type=button value=Save class=btn_save ref='".$k."' /></td>
			</tr>";
		}
		//Back to top
		?>
		<tr>
			<td colspan=5></td>
			<td colspan=1 class=center><input type=button value="Save All" class=btn_save_all /></td>
		</tr>	
	</table>
		<?php
		//if($counter >0){
			echo "<p><a href=#toplink><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
			echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
		//}
		//$counter++;
		?>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"GL_".strtoupper($log_section)),$key); ?></td>
	</tr>
</table>
<?php
} //end foreach $names
?>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	
	//alert($("#tbl_object_names").css("width"));
	$("#tbl_object_names").parents("tr").next().children("td").children("table").css("width",$("#tbl_object_names").css("width"));
	
	$("#tbl_menu_names tr.contents td").css("background-color","#e6e6f5");
	//$("#tbl_menu_names tr.has_child td").css("background-color","#e6e6f5");
	
	$("textarea.valid8me").keyup(function() {
		if($(this).val()!=$(this).attr('orig')) {
			$(this).addClass("orange-border");
		} else {
			$(this).removeClass("orange-border");
		}
		var pi = $(this).parents("table").prop("id");
		var c = $("#"+pi+" .orange-border").length;
		if(c==0) {
			$("#"+pi+" tr:last input:button").val("No Changes to Save");
			$("#"+pi+" tr:last input:button").prop("disabled",true);
		} else { 
			$("#"+pi+" tr:last input:button").prop("disabled",false);
			var v = (c>1 ? "Save All "+c+" Changes" : "Save The "+c+" Change");
			$("#"+pi+" tr:last input:button").val(v);
		}
	});
	
	$(".btn_save_all, .btn_save").click(function() { 
		var pi = $(this).parents("form").prop("name");
		$form = $("form[name="+pi+"]");
		if($(this).hasClass("btn_save")) {
			var dta = "section="+$form.find("input:hidden[name=section]").val();
			var r = $(this).attr("ref");
			//var v = $("#"+r).val();
			//dta+="&"+r+"="+v;
			dta += "&"+AssistForm.serialize($("#"+r));
		} else {
			var dta = AssistForm.serialize($form);
		}
		//alert(dta);
		//debugger
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Glossary.Edit",dta);
		//console.log(result);
		if(result[0]=="ok" && !$(this).hasClass("btn_save")) {
			document.location.href = 'setup_defaults_glossary.php?r[]='+result[0]+'&r[]='+result[1];
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	
	$("#tbl_object_names, #tbl_activity_names").find("input:text.valid8me:first").each(function() {
		$(this).trigger("keyup");
	});
	
});
</script> 