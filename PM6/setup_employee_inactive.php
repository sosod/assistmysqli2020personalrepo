<?php 
include "inc_header.php";

$active_users = false;

$empObj = new PM6_EMPLOYEE();
$employee_data = $empObj->getAvailableEmployees($active_users);
$available_employees = $employee_data['employees'];

$settingObject = new PM6_SETUP_EMPLOYEE();
$employee_settings = $settingObject->getEmployeeSettingsFormattedForSetup($active_users);


?>
<table class="tbl-new-format">
	<tr>
		<th colspan="4">Employee Details</th>
	</tr>
	<tr>
		<th>System<br />Ref</th>
		<th>Name</th>
		<th>Current Job Details</th>
		<th>Current Manager</th>
	</tr>
	<!-- start of employees -->
	<?php
	foreach($available_employees as $tkid => $employee) {
		$emp_id = $employee['emp_id'];
		$job_title = isset($employee_data['job_titles'][$employee['job_title']]) ? $employee_data['job_titles'][$employee['job_title']] : "Unknown Job Title";
		$department = isset($employee_data['departments'][$employee['job_dept']]) ? $employee_data['departments'][$employee['job_dept']] : "Unknown Department";
		$manager = isset($employee_data['job_titles'][$employee['job_manager_title']]) ? $employee_data['job_titles'][$employee['job_manager_title']] : "Unknown Job Title";
		if(!isset($employee_data['managers'][$employee['job_manager_title']]) || !is_array($employee_data['managers'][$employee['job_manager_title']]) || count($employee_data['managers'][$employee['job_manager_title']])==0) {
			$manager_name = "No Allocated Employee";
		} else {
			$my_managers = $employee_data['managers'][$employee['job_manager_title']];
			$manager_name = implode("; ",$my_managers);
		}
		echo "
	<tr>
		<td class='b'>".$tkid."<input type='hidden' id='emp_id' value='$emp_id' /><input type='hidden' id='tkid' value='$tkid' /></td>
		<td class='b'>".$employee['emp_name']."</td>
		<td>".$job_title."<br />[$department]</td>
		<td>".$manager."<br />[$manager_name]</td>
	</tr>		
		";
	}
	?>

</table>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	var btnSaveClass = "ui-button-minor-grey";
	$(".btn-save")
		.button({icons:{primary:"ui-icon-disk"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {
				if($(this).hasClass("ui-button-state-orange")) {
					btnSaveClass = "ui-button-state-orange";
				} else {
					btnSaveClass = "ui-button-minor-grey";
				}
				$(this).removeClass(btnSaveClass).addClass("ui-button-minor-orange");
			},function() {
				$(this).removeClass("ui-button-minor-orange").addClass(btnSaveClass);
			}
		).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = "temp=data";
			var $row = $(this).closest("tr");
			$row.find("input:hidden").each(function() {
				dta+="&"+$(this).prop("id")+"="+$(this).val();
			});
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUPEMPLOYEE.SAVE",dta);
			$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
			AssistHelper.finishedProcessing(result[0],result[1]);
	});
	function triggerYesNoBtnChange($me) {
		$me.closest("tr").find("button.btn-save").removeClass("ui-button-minor-grey").addClass("ui-button-state-orange");
	}
});
</script>