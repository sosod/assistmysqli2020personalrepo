<?php
/**
 * from inc_header
 * @var array $module_setup
 * @var PM6_SETUP_MODULE $moduleSetupObject
 *
 */

require_once("inc_header.php");


$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
$module_setup['FIN_YEAR']['setting'] = $masterObject->getAListItemName($module_setup['FIN_YEAR']['value']);
unset($masterObject);

//Added " && strlen($module_setup[$fld]['value'])!==0" to IF to specifically pick up blank module_setup values [AA-535] JC
$fld = "ORG_SOURCE";
if($module_setup[$fld]!==false && $module_setup[$fld]['value']!==false && strlen($module_setup[$fld]['value'])!==0) {
	$org_source_name = $moduleSetupObject->getSourceModuleDetails($module_setup[$fld]['object_id'],$module_setup[$fld]['value'],$module_setup[$fld]['setting']);
} else {
	$org_source_name = "[Not available / Not selected]";
}
$fld = "IKPI_SOURCE";
if($module_setup[$fld]!==false && $module_setup[$fld]['value']!==false && strlen($module_setup[$fld]['value'])!==0) {
	$ikpi_source_name = $moduleSetupObject->getSourceModuleDetails($module_setup[$fld]['object_id'],$module_setup[$fld]['value'],$module_setup[$fld]['setting']);
} else {
	$ikpi_source_name = "[Not available / Not selected]";
}
/* Display grouping info in Setup for reference purposes [AA-535] JC */
$fld = "GROUPING";
$primary_source = $module_setup['PRIMARY']['value']."_SOURCE";
$grouping_field_name = $moduleSetupObject->getGroupingDetails($module_setup[$fld]['value'],$module_setup[$primary_source]['value'],$module_setup[$primary_source]['setting']);
$grouping_field_name = $grouping_field_name[$module_setup[$fld]['value']];

$module_setup_log = $moduleSetupObject->getMOduleSetupLogDetailsForDisplay();
?>
<h2>Module Setup</h2>
<div id="div_notice" class="tbl-info light-orange-back float" style="width:300px;margin:10px;margin-right: 50px">
	<h3 class="orange">Please Note</h3>
	<p>If you need to change anything on this page, please contact your Assist Administrator / Business Partner.  </p><p>Note that if any Scorecards have been created, then they will be removed if any changes are made to this section.</p>
</div>
<?php
//ASSIST_HELPER::arrPrint($module_setup);



?>
<table class="form" id="tbl_setup">
	<tr>
		<th>Setup Status:</th>
		<td><?php echo ($module_setup['SETUP_COMPLETE']===true?"Complete":"Not Yet Complete"); ?></td>
	</tr>
	<tr>
		<th>Financial Year:</th>
		<td><?php echo $module_setup['FIN_YEAR']['setting']." (".ASSIST_MASTER_FINANCIALYEARS::REFTAG.$module_setup['FIN_YEAR']['value'].")"; ?></td>
	</tr>
	<tr>
		<th>Primary KPI Source:</th>
		<td><?php echo $moduleSetupObject->getASectionName($module_setup['PRIMARY']['value']); ?></td>
	</tr>
	<tr>
		<th><?php echo $moduleSetupObject->getASectionName("ORG"); ?> KPI Source:</th>
		<td><?php echo $org_source_name; ?></td>
	</tr>
	<tr>
		<th><?php echo $moduleSetupObject->getASectionName("IKPI"); ?> KPI Source:</th>
		<td><?php echo $ikpi_source_name; ?></td>
	</tr>
	<tr>
		<th>KPI Grouping:</th>
		<td><?php echo $grouping_field_name; ?></td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		$("#div_notice p").css({"font-size":"130%","line-height":"140%"});
		$("#tbl_setup tr td, #tbl_setup tr th").css("padding","7px");
	});
</script>
<?php
echo "<P class='i'>Module Setup was performed by ".$module_setup_log['user']." on ".$module_setup_log['time'].".</P>";
?>