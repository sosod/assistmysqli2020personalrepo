<?php
/**
 * @var string $modref - from calling page
 * @var int $today - from calling page
 */
$now = strtotime(date("d F Y",($today))); //echo date("d F Y H:i:s",$now);
/** @var INT $next_due - from main_login.php = number of days to display */
$future = strtotime(date("d F Y",($today+( ($next_due)*24*3600)))); //echo date("d F Y H:i:s",$future);


/**
 * AA-557 - PM6 upgrades - Janet Currie - 10 March 2021
 * Added option to include PM6 on dashboard
 * Decided to keep it as simple as possible and use the existing functionality in the getTriggersForCompletion function
 */

$pm6Object = new PM6_TRIGGER(0,$modref);
$pm6_incomplete_triggers = $pm6Object->getTriggersForCompletion("FPD");
//echo "<hr /><h1 class='red'>Helllloooo $modref from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
$remember_pm6_incomplete_triggers_for_multiple_modules[$modref] = $pm6_incomplete_triggers;
//ASSIST_HELPER::arrPrint($pm6_incomplete_triggers);

foreach($pm6_incomplete_triggers['objects'] as $trgr) {
	$d = strtotime($trgr['deadline']);
	if($d < $now) {
		$count['past']+=1;
	} elseif($d==$now) {
		$count['present']+=1;
	} else {
		$count['future']+=1;
	}
}

unset($pm6Object);

?>