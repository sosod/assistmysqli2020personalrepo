<?php

//TODO - Program this page - where to next?

$no_print_breadcrumbs = isset($no_print_breadcrumbs) ? $no_print_breadcrumbs : false;
require_once("inc_header.php");
$create_step = 10;
//$create_type = "PROJ";
//$create_name = "Project";
$obj_id = $_REQUEST['obj_id'];
$scdObj = new PM6_SCORECARD();

$sources = $scdObj->getKPASources();
$line_weight_precision_allowed = $scdObj->getLineWeightPrecisionAllowed();
$weight_blank_zero_echo = "";
for($i=0;$i<$line_weight_precision_allowed;$i++){
	$weight_blank_zero_echo.="0";
}

$primary_source = $scdObj->getPrimaryKPASource();
$source_modlocation = $scdObj->getPrimarySourceModLoc();
$pa = $scdObj->getAObjectSummary($obj_id);
$kpas = $scdObj->getKPAs();
$types = $scdObj->getObjectTypes(); //array("KPI","TOP","PROJ");
$names = $scdObj->getObjectNames(); //array('KPI'=>"Operational Indicators",'TOP'=>"Strategic Indicators",'PROJ'=>"Capital Indicators");

$class_name = $source_modlocation."_PMS";
$sdbp5Obj = new $class_name();
$head = $sdbp5Obj->getGenericHeadings($scdObj->getPrimaryKPASource(),true,false,SDBP6_DEPTKPI::OBJECT_TYPE);


$page_action = !isset($page_action) ? "create" : $page_action;

$page_top = $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id'],$page_action,true);
echo $page_top['echo'];
$raw_job_details = $page_top['data']['job'];
?>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$lineObj = new PM6_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id']);
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);

$scorecard_not_complete_error = false;
$total_line_count=0;

$fld_prefix = $sdbp5Obj->getFieldPrefix($types);


$display_kpa = false;
foreach($kpas as $k => $a) {
	foreach($types as $kt) {
		if(isset($lines[$k][$kt]) && count($lines[$k][$kt]) > 0) {
			$display_kpa = true;
		}
	}
}
if($display_kpa) {
	?>
	<h3>Indicators</h3>
	<table id=tbl_kpa class=list>
		<thead>
		<tr>
			<?php
			foreach($head['main'] as $fld => $name) {
				echo "<th>".$name."</th>";
			}
			?>
			<th>Indicator<br/>Weight</th>
			<th>Overall<br/>Weight</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$grand_tot = 0;

		foreach($kpas as $k => $a) {
			foreach($types as $kt) {
				$fp = $fld_prefix[$kt];
				$tot = 0;
				if(isset($lines[$k][$kt]) && count($lines[$k][$kt]) > 0) {
					$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
					//echo "<h3>".$a."</h3>";
					echo "<tr class=subth><td colspan=".(count($head['main']) + 2).">".$a." - ".$names[$kt]."</td></tr>";
					//ASSIST_HELPER::arrPrint($lines[$k]);
					$objects = $lines[$k][$kt];
					foreach($objects as $i => $obj) {
						$total_line_count++;
						echo "
			<tr  class=\"".($obj['active'] == 1 ? "" : "inactive")."\">";
						foreach($head['main'] as $generic_fld=>$name) {
							if($generic_fld!="sys_ref") {
								if(isset($obj[$fp."_".$generic_fld])) {
									$fld = $fp."_".$generic_fld;
								} else {
									$fld = $generic_fld;
								}
							} else {
								$fld = "ref";
							}
							echo "<td>".$obj[$fld].($fld == "ref" && $obj['active'] != 1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : "")."</td>";
						}
						$w = (isset($weights[$i]) ? $weights[$i] : -1);
						//if weight is a negative then assume that KPA weighting step not done.
						if($w < 0) {
							$scorecard_not_complete_error = true;
						}
						echo "
				<td id=td_".$k.$kt."_".$i." class=right>".number_format(round(($w / $kw) * 100, $line_weight_precision_allowed),$line_weight_precision_allowed)."%</td>
				<td class=right><div style='width:60px' class='float'>".number_format($w,$line_weight_precision_allowed)."%</div></td>
			</tr>";
						$tot += (isset($weights[$i]) ? $weights[$i] : 1);
					}
					echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>100.".$weight_blank_zero_echo."%</th>
			<th class='right kpa_tot ' id=td_".$k.$kt.">".number_format($tot,$line_weight_precision_allowed)."%</th>
		</tr>";
				}
				$grand_tot += $tot;
			}
		}
		$grand_tot_error = false;
		if(number_format($grand_tot,$line_weight_precision_allowed)!=number_format(100,$line_weight_precision_allowed)) {
			$scorecard_not_complete_error = true;
			$grand_tot_error = true;
		}
		?>
		</tbody>
		<tr class=gtotal>
			<th class='right b' colspan=<?php echo(count($head['main'])); ?>>Total Weight:</th>
			<th class=right>-</th>
			<th class='right' style="<?php echo ($grand_tot_error?"background-color:#CC0001;color:white":"");?>" id=th_gtot><?php echo number_format($grand_tot,$line_weight_precision_allowed); ?>%</th>
		</tr>
		<!--<tfoot>
		</tfoot>-->
	</table>


	<?php

} //end display KPA




$sources = $scdObj->getJALSources();
//$kpas = $assessObj->getKPAList();
unset($sdbp5Obj);

$primary_source = $scdObj->getPrimaryJALSource();

	$sdbp5Obj = new JAL1_PMS();
if($primary_source!==false) {
	$kpas = $sdbp5Obj->getKPAs($primary_source);
	$head = $sdbp5Obj->getHeadings($primary_source, true, false);
	unset($head['main']['activity_subfunction_id']);

} else {
	$kpas = array();
	$head = array();
}

$lineObj = new PM6_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id'],false,array(),"JAL");
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);

//ASSIST_HELPER::arrPrint($kpas);
//ASSIST_HELPER::arrPrint($lines);

$display_jal = true;
if(count($kpas)==0 || count($lines)==0) {
	$display_jal = false;
}

if($display_jal) {
?>
<h3 style="page-break-before: always">Activities</h3>

<table id=tbl_jal class=list width=100%>
<thead>
	<tr>
		<?php
		foreach($head['main'] as $fld=>$name) {
			echo "<th>".$name."</th>";
		}
		?>
		<th>Function<br />Weight</th>
		<th>Overall<br />Weight</th>
	</tr>
</thead>
<tbody>
<?php
$grand_tot = 0;
$types = array("JAL");
$names = array('JAL'=>"Activities");

foreach($kpas as $k => $a) {
foreach($types as $kt) {
	$tot = 0;
	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
		//echo "<h3>".$a."</h3>";
		echo "<tr class=subth><td colspan=".(count($head['main'])+2).">".$a." - ".$names[$kt]."</td></tr>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k][$kt];
		foreach($objects as $i => $obj) { $total_line_count++;
			echo "
			<tr  class=\"".($obj['active']==1 ? "" : "inactive")."\">";
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				$w = (isset($weights[$i]) ? $weights[$i] : -1);
				//if weight is a negative then assume that KPA weighting step not done.
				if($w<0) { $scorecard_not_complete_error = true; }
			echo "
				<td id=td_".$k.$kt."_".$i." class=right>".number_format(round(($w/$kw)*100,$line_weight_precision_allowed),$line_weight_precision_allowed)."%</td>
				<td class=right><div style='width:60px' class='float'>".number_format($w,$line_weight_precision_allowed)."%</div></td>
			</tr>";
			$tot+=(isset($weights[$i]) ? $weights[$i] : 1);
		}
		echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>".number_format(100,$line_weight_precision_allowed)."%</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".number_format($tot,$line_weight_precision_allowed)."%</th>
		</tr>";
	}
	$grand_tot+=$tot;
}
}
?>
	</tbody>
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=<?php echo (count($head['main'])); ?>>Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot><?php echo number_format($grand_tot,$line_weight_precision_allowed); ?>%</th>
		</tr>
	</tfoot>
</table>

<?php
}//end display JAL






$listObj = new PM6_LIST("competency_category");
$category_objects = $listObj->getActiveListItemsFormattedForSelect();
$listObj->changeListType("competencies");
$competency_objects = $listObj->getListItemsGroupedByParent($listObj->getParentField());
$lines = $lineObj->getLineSrcIDs($_REQUEST['obj_id'], $lineObj->getModRef(),"CC");

$display_cc = true;
if(count($objects)==0 || count($lines)==0) {
	$display_cc = false;
}
if($display_cc) {

	/**
	 * Check that each category has lines otherwise get rid of the category for display purposes
	 */
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		$line_count = 0;
		if(count($objects) > 0) {
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$line_count++;
				}
			}
		}
		if($line_count==0) {
			unset($category_objects[$cate_id]);
		}
	}

	?>

<h3 style="page-break-before: always">Core Competencies</h3>
<table class=list id=tbl_cc>
	<tr>
		<th>Core Competency</th>
		<th>Description</th>
		<th>Weight</th>
	</tr>
	<?php
	$colspan = 3;
	$tot = 0;
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		if(count($objects) > 0) {
			echo "<tr class=subth><td colspan='".$colspan."'>$cate</td></tr>";
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$total_line_count++;
					$li = $lines[$i];
					$w = isset($weights[$li]) ? $weights[$li] : -1;
					//if weight is a negative then assume that CC weighting step not done.
					if($w < 0) {
						$scorecard_not_complete_error = true;
					}
					$tot += $w;
					echo "
					<tr>
						<td>".$obj['name']."</td>
						<td>".$obj['description']."</td>
						<td class=right>".number_format($w,$line_weight_precision_allowed)."%</td>
					</tr>";
				}
			}
		}
	}
	?>
	<tr class=total>
		<td></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot><?php echo number_format($tot,$line_weight_precision_allowed); ?>%</td>
	</tr>
</table>

<?php

}//end display CC
?>










<h3 style="page-break-before: always">Component Weights</h3>
<?php

$echo = $displayObject->getComponentWeighting($obj_id,$pa,false);
echo $echo['display'];



if($total_line_count==0) {
	$scorecard_not_complete_error = true;
}



?>
</form>



<span style='width:100%;margin-right: 25px;border:1px solid #ffffff'>
	<p class='right'><button class='no-print' id=btn_sign act=display>Display Space for Signing</button></p>
	<table style='margin: 0 auto; margin-top: 10px; page-break-inside: avoid;' id=tbl_sign>
		<tr class=head>
			<td class=b>On Behalf of Employee</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td class=b>On Behalf of Organisation</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Name</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Name</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Position / Job Title</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Position / Job Title</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Signature</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Signature</td>
		</tr>
		<tr class=handwrite>
			<td>________________________________________</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>________________________________________</td>
		</tr>
		<tr class=comment>
			<td>Date</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>Date</td>
		</tr>
	</table>
</span>

<?php


//REVIEW NEXT STEP
/**
 * if user id = employee user id [new_my]
 * 		display review box
 * 		if manager_approve = yes
 * 			options = approve & activate || reject & fix || reject & return
 * 		else if manager_approve = reject
 * 			display reject reason & option to return to creation process
 * 		else if created by = me
 * 			options = approve & send to manager || go back to creation process
 * 		else
 * 			options = approve & send to manager || reject & fix || reject & return
 * if user id = manager user id [new_staff]
 * 		display review box
 * 		if employee_approve = yes
 * 			options = approve & activate || reject & fix || reject & return
 * 		else if employee_approve = reject
 * 			display reject reason & option to return to creation process
 * 		else if created by = me
 * 			if employee is active user
 * 				options = approve & send to employee || go back to creation process
 * 			else if employee is not an active user
 * 				options = approve & confirm employee approval		//TODO - ATTACHMENT PROCESS TO HAPPEN HERE
 * 		else
 * 			if employee is active user
 * 				options = approve & send to employee || reject & fix || reject & return
 * 			else
 * 				options = approve & confirm employee approval		//TODO - ATTACHMENT PROCESS TO HAPPEN HERE
 * if super user [new_scorecards]
 *
 */

//don't display save buttons & creation steps if page is being called in the Manage > View section - check also done in function as backup
$include_approval_js = false;	//include the JS relating to the approve/reject form used in the New / Edit sections
$include_attach_js = false;	//include the JS relating to attaching of documents - functionality outstanding
if($page_action!="view") {
	$include_approval_js = true;
	echo "<div class='no-print'>";

//	ASSIST_HELPER::arrPrint($pa);

	$prefObject = new PM6_SETUP_PREFERENCES();
	$force_employee_digital = $prefObject->getAnswerToQuestionByCode("force_employee_digital");
	$force_manager_digital = $prefObject->getAnswerToQuestionByCode("force_manager_digital");


	$scd_status = $pa['scd_status'];
	$new_status = $pa['scd_new_status'];
	$continue_process = true;

	$actions = array();//what actions must the process take on submission?
	$back_actions = array();//what actions must the process take on go back submission?
	$button_options = array();

	$built_by_employee = (($new_status & PM6::IP_EMPLOYEE) == PM6::IP_EMPLOYEE);
	$built_by_manager = (($new_status & PM6::IP_MANAGER) == PM6::IP_MANAGER);
	$built_by_champion = (($new_status & PM6::IP_CHAMPION) == PM6::IP_CHAMPION);
	$built_by_super = (($new_status & PM6::IP_SUPER) == PM6::IP_SUPER);

	$already_confirmed = (($scd_status & PM6::CONFIRMED) == PM6::CONFIRMED);	//will force the page to act as a review page and not a create page
	if($already_confirmed) { $page_action = "review"; }
	$waiting_someones_approval = (($new_status & PM6::AWAITING_APPROVAL) == PM6::AWAITING_APPROVAL);

	$employee_approved_digitally = (($new_status & PM6::EMPLOYEE_DIGITALLY_APPROVED) == PM6::EMPLOYEE_DIGITALLY_APPROVED);
	$employee_approved_signed = (($new_status & PM6::EMPLOYEE_SIGN_APPROVED) == PM6::EMPLOYEE_SIGN_APPROVED);
	$employee_approved_signed_poe = (($new_status & PM6::EMPLOYEE_SIGN_ATTACHED) == PM6::EMPLOYEE_SIGN_ATTACHED);
	$employee_approved_no_poe = (($new_status & PM6::EMPLOYEE_APPROVED_NO_POE) == PM6::EMPLOYEE_APPROVED_NO_POE);
	$employee_approved = $employee_approved_digitally || $employee_approved_no_poe || $employee_approved_signed || $employee_approved_signed_poe;
	$employee_rejected = (($new_status & PM6::EMPLOYEE_REJECTED) == PM6::EMPLOYEE_REJECTED);

	$manager_approved_digitally = (($new_status & PM6::MANAGER_DIGITALLY_APPROVED) == PM6::MANAGER_DIGITALLY_APPROVED);
	$manager_approved_signed = (($new_status & PM6::MANAGER_SIGN_APPROVED) == PM6::MANAGER_SIGN_APPROVED);
	$manager_approved_signed_poe = (($new_status & PM6::MANAGER_SIGN_ATTACHED) == PM6::MANAGER_SIGN_ATTACHED);
	$manager_approved_no_poe = (($new_status & PM6::MANAGER_APPROVED_NO_POE) == PM6::MANAGER_APPROVED_NO_POE);
	$manager_approved = $manager_approved_digitally || $manager_approved_no_poe || $manager_approved_signed || $manager_approved_signed_poe;
	$manager_rejected = (($new_status & PM6::MANAGER_REJECTED) == PM6::MANAGER_REJECTED);

	$employee_user_id = $pa['employee_tkid'];
	$is_employee_active_user = $scdObj->isEmployeeActiveUser($employee_user_id);
	$is_employee_module_user = $scdObj->isEmployeeModuleUser($employee_user_id);

	$manager_user_id = $raw_job_details['job_manager_tkid'];
	if(!is_null($manager_user_id) && strlen($manager_user_id)>0 || !empty($manager_user_id)) {
		$is_manager_active_user = $scdObj->isManagerActiveUser($manager_user_id);
		$is_manager_module_user = $scdObj->isManagerModuleUser($manager_user_id);
	} else {
		$is_manager_active_user = false;
		$is_manager_module_user = false;
	}

	$display_employee_approval_alt_options = false;
	$display_manager_approval_alt_options = false;
	$employee_module_user_error = false;
	$manager_module_user_error = false;

	$rejection_next_steps = array(
			"fix_self"=>"Claim the |scorecard| to fix the issue myself",
			"fix_builder"=>"Return the |scorecard| back to the original builder (".$scdObj->getAUserName($pa['scd_current_builder']).")",
	);
	$rejection_next_steps = $scdObj->replaceAllNames($rejection_next_steps);

	$non_user_employee_approval = array(
			'attached'=>"Approved (Signed POE attached)",
			'signed'=>"Approved (Signed POE available)",
			'no_poe'=>"Approved (No signed POE available)",
	);
	//temporarily remove "attached" option until EMP1 Phase 2 complete
	unset($non_user_employee_approval['attached']);

	$non_user_manager_approval = array(
			'attached'=>"Approved (Signed POE attached)",
			'signed'=>"Approved (Signed POE available)",
			'no_poe'=>"Approved (No signed POE available)",
	);
	//temporarily remove "attached" option until EMP1 Phase 2 complete
	unset($non_user_manager_approval['attached']);


	//ASSUMPTION - If no information is provided, assume that: (1) the current user looking at this page is a Super User & (2) any status is SU status
	if(!isset($actor)) {
		$actor = "super_user";
	}
	switch($actor) {
		case "employee":
			if($employee_approved) {
				if($manager_approved) {
					//NOTHING TO SEE HERE - ALREADY DONE
					$button_options['all_done'] = true;
				} else {
					$button_options['undo_approve'] = true;
					$button_options['notify_manager'] = $manager_user_id;
				}
				$undo_approval_actions = array(
							"-employee_approval",
							"undo_approval_notify_manager_".$manager_user_id,
				);

			} elseif($page_action=="review") {
				$button_options['approve_form'] = true;

				$approve_actions = array("employee_digitally_approved");
				$reject_actions = array("employee_rejected");
				$reject_actions[] = "-confirm";
				$reject_actions[] = "-awaiting_approval";
				if($manager_approved) {
					$approve_actions[] = "-awaiting_approval";
					$approve_actions[] = "activate";
					$reject_actions[] = "reject_manager_".$manager_user_id;
				} else {
					$approve_actions[] = "notify_manager_".$manager_user_id;
					$rejected_actions[] = "notify_manager_".$manager_user_id;
				}

			} elseif($page_action=="create") {
				$button_options['complete'] = true;
				if($built_by_employee) {
					$button_options['back'] = true;
					$actions[] = "confirm";
					$actions[] = "awaiting_approval";
					$actions[] = "employee_digitally_approved";
				}
				if($is_manager_active_user) {
					if($is_manager_module_user) {
						$actions[] = "notify_manager_".$manager_user_id;
					} else {
						$actions[] = "manager_module_user_error";
						$continue_process = "manager_module_user_error";
					}
				} else {
					if($actions[1] == "awaiting_approval") {
						unset($actions[1]);
					} //no need for waiting_approve if no one is going to approve
					$actions[] = "non_user_manager";
					$actions[] = "activate";
				}
			} else {
				echo "Oops, I don't know what to do with $page_action ?";
				$continue_process = $actor.".".$page_action;
			}
			break;
		case "manager":
			if($manager_approved) {
				if($employee_approved) {
					//NOTHING TO SEE HERE - ALREADY DONE
					$button_options['all_done'] = true;
				} else {
					$button_options['undo_approve'] = true;
					$button_options['notify_employee'] = $employee_user_id;
				}
				$undo_approval_actions = array(
							"-manager_approval",
							"undo_approval_notify_employee_".$employee_user_id,
				);

			} elseif($page_action=="review" || $already_confirmed) {
				$button_options['approve_form'] = true;

				$approve_actions = array("manager_digitally_approved");
				$reject_actions = array("manager_rejected");
				$reject_actions[] = "-confirm";
				$reject_actions[] = "-awaiting_approval";
				if($employee_approved) {
					$approve_actions[] = "-awaiting_approval";
					$approve_actions[] = "activate";
					$reject_actions[] = "reject_employee_".$employee_user_id;
				} else {
					$approve_actions[] = "notify_employee_".$employee_user_id;
					$rejected_actions[] = "notify_employee_".$employee_user_id;
				}


			} elseif($page_action=="create") {
				$button_options['complete'] = true;
				if($built_by_manager) {
					$button_options['back'] = true;
					$actions[] = "confirm";
					$actions[] = "awaiting_approval";
					$actions[] = "manager_digitally_approved";
				}
				if($is_employee_active_user) {
					if($is_employee_module_user) {
						$actions[] = "notify_employee_".$employee_user_id;
					} else {
						//treat as non_user_employee
						//$actions[] = "employee_module_user_error";
						//$continue_process = "employee_module_user_error";
						$actions[] = "non_user_employee";
						$actions[] = "activate";
						$button_options['non_user_employee'] = true;
						$button_options['complete'] = false;
					}
				} else {
					if($actions[1] == "awaiting_approval") {
						unset($actions[1]);
					} //no need for waiting_approve if no one is going to approve
					$actions[] = "non_user_employee";
					$actions[] = "activate";
					$button_options['non_user_employee'] = true;
					$button_options['complete'] = false;
				}
			} else {
				echo "Oops, I don't know what you want me to do with $page_action ?";
				$continue_process = $page_action;
			}
			break;
		case "super_user":
		case "champion":
			if($page_action=="review" || $already_confirmed) {

				if($manager_approved && $employee_approved) {
					$button_options['all_done'] = true;
				} else {
					$button_options['undo_confirm'] = true;
					$undo_confirmation_actions = array(
							"-confirm",
							"-awaiting_approval",
							"-approval",
							"return_builder",
							"undo_confirm_notify_employee_".$employee_user_id,
							"undo_confirm_notify_manager_".$manager_user_id,
					);
					if(!$employee_approved) {
						if($is_employee_module_user) {
							$button_options['notify_employee'] = $employee_user_id;
						} else {
							$continue_process = "employee_module_user_error";
						}
					}
					if(!$manager_approved) {
						if($is_manager_module_user) {
							$button_options['notify_manager'] = $manager_user_id;
						} else {
							$continue_process = "manager_module_user_error";
						}
					}

				}

				//TODO - What actions to take for SU/Champ approval?
				$approve_actions = array();
				$reject_actions = array();

			} elseif($page_action=="create") {
				if(($actor == "super_user" && $built_by_super) || ($actor == "champion" && $built_by_champion)) {
					$button_options['back'] = true;//echo "<button id=btn_back>Back</button>";	//display back button if this is the builder role
					$actions[] = "confirm";
					$actions[] = "awaiting_approval";
					$button_options['complete'] = true;
				}
				if($is_employee_active_user) {
					if($is_employee_module_user) {
						if($force_employee_digital==true) {
							$actions[] = "notify_employee_".$employee_user_id;
						} else {
							$actions[] = "non_user_employee";
							$actions['allow_employee_digital_signature'] = true;
						}
					} else {
						//treat non module user as non user
						$actions[] = "non_user_employee";
						$actions[] = "activate";
						$button_options['non_user_employee'] = true;
						$button_options['complete'] = false;
					}
				} else {
					$actions[] = "non_user_employee";
				}
				if($is_manager_active_user) {
					if($is_manager_module_user) {
						if($force_manager_digital==true) {
							$actions[] = "notify_manager_".$manager_user_id;
						} else {
							$actions[] = "non_user_manager";
							$actions['allow_manager_digital_signature'] = true;
						}
					} else {
						//treat non module user as non user
						$actions[] = "non_user_manager";
						$actions[] = "activate";
						$button_options['non_user_manager'] = true;
						$button_options['complete'] = false;
					}
				} else {
					$actions[] = "non_user_manager";
				}
				if(!$is_employee_active_user && !$is_manager_active_user) {
					if($actions[1] == "awaiting_approval") {
						unset($actions[1]);
					} //no need for waiting_approve if no one is going to approve
					$actions[] = "activate";
				}
			} else {
				echo "Oops, I don't know what you want me to do with $page_action ?";
				$continue_process = "PM6S10".__LINE__.":".$actor.$page_action;
			}
		default:
			break;
	}





	if(isset($button_options['back']) && $button_options['back']==true) {
		echo "<p class='".(!$scorecard_not_complete_error?"":"center")."'><button id=btn_back >Back</button></p>";
	}
	if(!$scorecard_not_complete_error) {

		if($continue_process===true) {

			/*************************************************************
			 * COMPLETION FORM
			 */

			if(isset($button_options['complete']) && $button_options['complete']==true) {
				$finish_confirm_message = "I hereby confirm that this |scorecard| is complete and the steps detailed below can take place";
				$finish_confirm_message = $scdObj->replaceAllNames($finish_confirm_message);
				if($force_employee_digital!=true) {
					echo "<p class=center><span class='b'>Employee Approval: <select name=employee_approval id=employee_approval><option selected value=\"X\">--- SELECT ---</option>";
							if(isset($actions['allow_employee_digital_signature'])) {
								echo "<option value=request_employee_".$employee_user_id.">Request digital approval</option>";
								unset($actions['allow_employee_digital_signature']);
							}
								foreach($non_user_employee_approval as $key => $option) {
									echo "<option value=".$key.">".$option."</option>";
								}
					echo "</select></span> <span class='b' id='spn_employee_date'>Date Signed: ";
						$echo['js'].=$displayObject->drawFormField("DATE",array('id'=>"employee_approval_date",'name'=>"employee_approval_date",'options'=>array('maxDate'=>"'+0D'")),"");
					echo "</span></p>";
				}
				if($force_manager_digital!=true) {
					echo "<p class=center><span class='b'>Manager Approval: <select name=manager_approval id=manager_approval><option selected value=\"X\">--- SELECT ---</option>";
							if(isset($actions['allow_manager_digital_signature'])) {
								echo "<option value=request_manager_".$manager_user_id.">Request digital approval</option>";
								unset($actions['allow_manager_digital_signature']);
							}
							foreach($non_user_manager_approval as $key => $option) {
									echo "<option value=".$key.">".$option."</option>";
								}
					echo "</select></span> <span class='b' id='spn_manager_date'>Date Signed: ";
						$echo['js'].=$displayObject->drawFormField("DATE",array('id'=>"manager_approval_date",'name'=>"manager_approval_date",'options'=>array('maxDate'=>"'+0D'")),"");
					echo "</span></p>";
				}
				echo "<p class='center'><span class='i'><input type=checkbox name=chk_confirm id=chk_confirm value='confirmed' /> ".$finish_confirm_message."</span><br />&nbsp;<br />";
				echo "<button id=btn_finish>Finish</button><input type='hidden' name='finish_actions' id='finish_actions' value='".implode("|",$actions)."' />";
				echo "</p>";
				echo "<div style='margin:0 auto; width:500px;'>";
				$info_message = "<p>Clicking the Finish button will do the following:</p><ol style='color:black'>";
				foreach($actions as $action) {
					$extra1 = "";
					if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
					elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
					$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
				}
				ASSIST_HELPER::displayResult(array("info",$scdObj->replaceAllNames($info_message)));
				echo "</div>";
			}


			/*************************************************************
			 * NON-USER APPROVAL
			 */


			if((isset($button_options['non_user_employee']) && $button_options['non_user_employee']==true)
				|| isset($button_options['non_user_manager']) && $button_options['non_user_manager']==true) {
				?>
				</p>
				<table style="margin:0 auto" id="tbl_non_user" class="tbl-container not-max">
					<?php if(isset($button_options['non_user_employee']) && $button_options['non_user_employee']==true) { ?>
					<tr>
						<td class="b">Non-User Employee Approval:</td>
						<td><select name="non_user_employee_approval" id="non_user_employee_approval"><option selected value="X">--- SELECT ---</option><?php
								foreach($non_user_employee_approval as $key => $option) {
									echo "<option value=".$key.">".$option."</option>";
								}
								?></select></td>
					</tr>
						<tr id="tr_non_user_employee_approval_date">
							<td class="b">Date Signed:</td>
							<td><?php
								$echo['js'].=$displayObject->drawFormField("DATE",array('id'=>"non_user_employee_approval_date",'name'=>"non_user_employee_approval_date",'options'=>array('maxDate'=>"'+0D'")),"");
								?>
							</td>
						</tr>
					<?php
					}
					if(isset($button_options['non_user_manager']) && $button_options['non_user_manager']==true) {
						?>
					<tr>
						<td class="b">Non-User Manager Approval:</td>
						<td><select name="non_user_manager_approval" id="non_user_manager_approval"><option selected value="X">--- SELECT ---</option><?php
								foreach($non_user_manager_approval as $key => $option) {
									echo "<option value=".$key.">".$option."</option>";
								}
								?></select></td>
					</tr>
						<tr id="tr_non_user_manager_approval_date">
							<td class="b">Date Signed:</td>
							<td><?php
								$echo['js'].=$displayObject->drawFormField("DATE",array('id'=>"non_user_manager_approval_date",'name'=>"non_user_manager_approval_date",'options'=>array('maxDate'=>"'+0D'")),"");
								?></td>
						</tr>
					<?php
					}
					if(isset($actions) && count($actions)>0) {
						if(in_array("manager_digitally_approved",$actions) || in_array("employee_digitally_approved",$actions)) {
							echo "
							<tr>
								<td class='center' colspan='2' id='tr_approval_chk_confirm'><input type='checkbox' id='chk_confirm' name='chk_confirm' value='confirmed' /> I hereby confirm my Approval</td>
							</tr>
							<tr>
								<td class='center' colspan='2'><button id=btn_nonuser_approve>Approve</button>
									<input type='hidden' name='actions' id='actions' value='".implode("|",$actions)."' /></td>
							</tr>
							<tr>
								<td colspan='2'><div style='margin:0 auto; '>";
							$info_message = "<p>Clicking the Approve button will do the following:</p><ol style='color:black'>";
							foreach($actions as $action) {
								$extra1 = "";
								if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
								elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
								$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
							}
							ASSIST_HELPER::displayResult(array("ok",$scdObj->replaceAllNames($info_message)));
							echo "</div></td>
							</tr>
							";
						}
					}
					?>
				</table>
				<?php
			}


			/*************************************************************
			 * APPROVAL FORM
			 */

			if(isset($button_options['approve_form']) && $button_options['approve_form']==true) {
				?>
				&nbsp;</p>
				<table style="margin:0 auto" class="tbl-container not-max">
					<tr>
						<td width="50%" class=""><?php
							echo "<p class='center tr_approval_chk_confirm'><input type='checkbox' id='chk_confirm' name='chk_confirm' value='confirmed' /> I hereby confirm my Approval</p>
								<p class='center'><button id=btn_approve>Approve</button>
									<input type='hidden' name='approve_actions' id='approve_actions' value='".implode("|",$approve_actions)."' />";
							echo "</p><div style='margin:0 auto; width:500px;'>";
							$info_message = "<p>Clicking the Approve button will do the following:</p><ol style='color:black'>";
							foreach($approve_actions as $action) {
								$extra1 = "";
								if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
								elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
								$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
							}
							ASSIST_HELPER::displayResult(array("ok",$scdObj->replaceAllNames($info_message)));
							echo "</div>";
							?>
						</td>
						<td rowspan="1" width="2%">&nbsp;&nbsp;</td>
						<td width="50%"><?php
							echo "
									<input type='hidden' name='reject_actions' id='reject_actions' value='".implode("|",$reject_actions)."' />";
						?><p><span class="b">Reason for Rejection:</span><br />
								<textarea rows="5" cols="75" name="reject_reason" id="reject_reason"></textarea></p>
							<p class="b">Next Steps: <select id="reject_next"><?php
								$selected = false;
								foreach($rejection_next_steps as $v => $t) {
									echo "<option ".($selected===false?"selected":"")." value='".$v."'>".$t."</option>";
									$selected = true;
								}
									?></select></p>
							<?php
							echo "<p class=center><button id=btn_reject>Reject</button></p>";
				echo "<div style='margin:0 auto; width:500px;'>";
				$info_message = "<p>Clicking the Reject button will do the following:</p><ol style='color:black'>";
				foreach($reject_actions as $action) {
					$extra1 = "";
					if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
					elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
					$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
				}
				ASSIST_HELPER::displayResult(array("error",$scdObj->replaceAllNames($info_message)));
				echo "</div>";
							?></td>
					</tr>
				</table>
				<?php
			}



			/*************************************************************
			 * UNDO APPROVAL FORM
			 */

			$open_table = false;
			if(isset($button_options['undo_approve']) && $button_options['undo_approve']==true) {
				if(isset($undo_approval_actions)) {
					$open_table = true;
					$close_table = true;
					if((isset($button_options['notify_manager']) && $button_options['notify_manager']!==false) || (isset($button_options['notify_employee']) && $button_options['notify_employee']!==false)) {
						$close_table = false;
					}
				?>
				<table class="tbl-container not-max" style="margin: 0 auto"><tr><td width="50%">
					<p class="center" id="tr_chk_confirm"><input type="checkbox" value="i_confirm" name="chk_confirm" id="chk_confirm" /> I hereby confirm that I wish to cancel my previous approval.</p>
					<p class="center"><button id=btn_undoapprove >Cancel Approval</button><input type="hidden" name="unapprove_actions" id="unapprove_actions" value="<?php echo implode("|",$undo_approval_actions); ?>" /></p>
					<div style='margin:0 auto; width:500px;'>
					<?php
						$info_message = "<p>Clicking the button will do the following:</p><ol style='color:black'>";
						foreach($undo_approval_actions as $action) {
							$extra1 = "";
							if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
							elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
							$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
						}
						ASSIST_HELPER::displayResult(array("error",$scdObj->replaceAllNames($info_message)));
					?>
					</div></td>
					<?php
					if($close_table==true) {
						echo "</tr></table>";
					} else {
						echo "<td width='50%'>";
					}
				} else {
					ASSIST_HELPER::displayResult(array("error","An error has occurred [Code: PM6S10UA".__LINE__."].  Please reload the page and try again."));
				}
			}



			/*************************************************************
			 * UNDO CONFIRMATION FORM
			 */

			if(isset($button_options['undo_confirm']) && $button_options['undo_confirm']==true) {
				if(isset($undo_confirmation_actions)) {
					$open_table = true;
					$close_table = true;
					if((isset($button_options['notify_manager']) && $button_options['notify_manager']!==false) || (isset($button_options['notify_employee']) && $button_options['notify_employee']!==false)) {
						$close_table = false;
					}
				?>
				<table class="tbl-container not-max" style="margin: 0 auto"><tr><td width="50%">
					<p class="center" id="tr_chk_confirm"><input type="checkbox" value="i_confirm" name="chk_confirm" id="chk_confirm" /> I hereby confirm that I wish to cancel the previous confirmation.</p>
					<p class="center"><button id=btn_undoconfirm >Cancel Confirmation</button><input type="hidden" name="unconfirm_actions" id="unconfirm_actions" value="<?php echo implode("|",$undo_confirmation_actions); ?>" /></p>
					<div style='margin:0 auto; width:500px;'>
					<?php
						$info_message = "<p>Clicking the button will do the following:</p><ol style='color:black'>";
						foreach($undo_confirmation_actions as $action) {
							$extra1 = "";
							if(strpos($action,"manager")!==false) { $extra1 = $raw_job_details['job_manager_name']; }
							elseif(strpos($action,"employee")!==false) { $extra1 = $pa['employee']; }
							$info_message.="<li>".$scdObj->processInfoMessage($action,$extra1)."</li>";
						}
						ASSIST_HELPER::displayResult(array("error",$scdObj->replaceAllNames($info_message)));
					?>
					</div></td>
					<?php
					if($close_table==true) {
						echo "</tr></table>";
					} else {
						echo "<td width='50%'>";
					}
				} else {
					ASSIST_HELPER::displayResult(array("error","An error has occurred [Code: PM6S10UC".__LINE__."].  Please reload the page and try again."));
				}
			}



			/*************************************************************
			 * NOTIFY MANAGER FORM
			 */


			if(isset($button_options['notify_manager']) && $button_options['notify_manager']!==false) {
				echo "<p class='center'><button id=btn_notify_manager notify_user='".$button_options['notify_manager']."' user_role='manager'>Send Reminder Notification to Manager</button></p>";
			}



			/*************************************************************
			 * NOTIFY EMPLOYEE
			 */

			if(isset($button_options['notify_employee']) && $button_options['notify_employee']!==false) {
				echo "<p class='center'><button id=btn_notify_employee notify_user='".$button_options['notify_employee']."' user_role='employee'>Send Reminder Notification to Employee</button></p>";
			}
			if(isset($close_table) && $close_table==false) {
				echo "</tr></table>";
			}
		} else {
			echo "<div style='margin:0 auto; width:500px;'>";
			ASSIST_HELPER::displayResult(array("error","There's a problem with finalising this scorecard.  Please contact your Assist Helpdesk / Support with the following error message: PM6S10".__LINE__.":".$continue_process."."));
			echo "</div>";
		}
	}
	?>
	<!-- <p class='center no-print'><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Confirm & Activate</button></p> -->
	<?php

	if($page_action!="review") {
		$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id'],$page_redirect_url);
	}
	echo "</div>";
} else {
	if(!isset($actor)) {
		$actor = "employee";
	}
}//end if check for Manage > View section


if($_SESSION['cc']=="hes444s") {
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($button_options);
//ASSIST_HELPER::arrPrint($actions);
//ASSIST_HELPER::arrPrint($approve_actions);
//ASSIST_HELPER::arrPrint($reject_actions);
//ASSIST_HELPER::arrPrint($pa);
//ASSIST_HELPER::arrPrint($raw_job_details);
//echo "<hr />";
}


?>

<p>&nbsp;</p>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
	var scorecard_object_name = "<?php echo $scdObj->replaceAllNames("|scorecard|"); ?>";


	<?php
	echo $echo['js'];
	?>


	$("#tbl_sign").addClass("noborder").find("td").addClass("center").addClass("noborder");
	$("#tbl_sign").hide();
	$("#tbl_sign tr.handwrite").find("td").height(70).css("vertical-align","bottom");
	$("#tbl_sign tr.comment").find("td").addClass("i");

	$("#btn_sign").button({
		icons:{primary:"ui-icon-pencil"}
	}).click(function(e) {
		e.preventDefault();
		var act = $(this).attr("act");
		if(act=="display") {
			$("#tbl_sign").show();
			$(this).attr("act","hide");
			$(this).button("option","label","Hide Space for Signing");
		} else {
			$("#tbl_sign").hide();
			$(this).attr("act","display");
			$(this).button("option","label","Display Space for Signing");
		}
	});

	$("table.noborder, table.noborder td").css("border","0px");

	$("#btn_next").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		AssistHelper.processing();
		var err = false;
		/*$("td.total_weight").each(function() {
			if(($(this).html()*1)!=100) {
				err = true;
			}
		});*/
		if(err) {
			//alert("Not all KPA Weights total 100.  Please review the weights again.");
			AssistHelper.finishedProcessing("error","An error has occurred.  Please reload the page and try again.");
		} else {
			var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>";
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Activate",dta);
			if(result[0]=="ok") {
				var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php?r[]=ok&r[]="+result[1];
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");//.css({"color":"#009900","border":"1px solid #009900"});

<?php
	if($include_approval_js===true) {
	?>

	$("#btn_undoapprove").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function() {
		AssistHelper.processing();
		$("#tr_chk_confirm").removeClass("required");
		var err = false;
		var aa = $("#unapprove_actions").val();
		if(aa.length==0) { err = true; }
		if(err) {
			AssistHelper.finishedProcessing("error","An error has occurred [Code: PM6S10UA<?php echo __LINE__;?>].  Please reload the page and try again.");
		} else {
			if($("#chk_confirm").is(':checked')) {
				var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>&unapprove_actions="+aa;
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.createUndoApprove",dta);
				if(result[0]=="ok") {
					var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			} else {
				$("#tr_chk_confirm").addClass("required");
				AssistHelper.finishedProcessing("error","Please tick the checkbox to confirm your action.");
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red")
	.hover(function() {
		$(this).removeClass("ui-button-bold-red").addClass("ui-button-bold-orange");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-red");
	});//.css({"color":"#009900","border":"1px solid #009900"});


	$("#btn_undoconfirm").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function() {
		AssistHelper.processing();
		$("#tr_chk_confirm").removeClass("required");
		var err = false;
		var aa = $("#unconfirm_actions").val();
		if(aa.length==0) { err = true; }
		if(err) {
			AssistHelper.finishedProcessing("error","An error has occurred [Code: PM6S10AA<?php echo __LINE__;?>].  Please reload the page and try again.");
		} else {
			if($("#chk_confirm").is(':checked')) {
				var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>&unconfirm_actions="+aa;
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.createUndoConfirm",dta);
				if(result[0]=="ok") {
					var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			} else {
				$("#tr_chk_confirm").addClass("required");
				AssistHelper.finishedProcessing("error","Please tick the checkbox to confirm your action.");
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red")
	.hover(function() {
		$(this).removeClass("ui-button-bold-red").addClass("ui-button-bold-orange");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-red");
	});//.css({"color":"#009900","border":"1px solid #009900"});


	$("#btn_notify_manager, #btn_notify_employee").button({
		icons: {primary: "ui-icon-mail-closed"},
	}).click(function() {
		AssistHelper.processing();
		var role = $(this).attr("user_role");
		var user_id = $(this).attr("notify_user");
		var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>";
		dta = dta+"&notification_type=create_approval_reminder&notify_user="+user_id+"&user_role="+role;
		var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.CreateNotifyUser", dta);
		if (result[0] == "ok") {
			AssistHelper.finishedProcessing(result[0], result[1]);
		} else {
			AssistHelper.finishedProcessing(result[0], result[1]);
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-blue")
	.hover(function() {
		$(this).removeClass("ui-button-bold-blue").addClass("ui-button-bold-orange");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-blue");
	});//.css({"color":"#009900","border":"1px solid #009900"});

	$("#btn_nonuser_approve").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		AssistHelper.processing();
		$("#non_user_employee_approval").removeClass("required");
		$("#non_user_employee_approval_date").removeClass("required");
		$("#non_user_manager_approval").removeClass("required");
		$("#non_user_manager_approval_date").removeClass("required");
		$("#tr_approval_chk_confirm").removeClass("required");
		var err = false;
		var aa = $("#actions").val();
		if(aa.length==0) { err = true; }
		if(err) {
			AssistHelper.finishedProcessing("error","An error has occurred [Code: PM6S10AA<?php echo __LINE__;?>].  Please reload the page and try again.");
		} else {
			if($("#non_user_employee_approval").length>0) {
				if($("#non_user_employee_approval").val()=="X" || $("#non_user_employee_approval").val().length==0) {
					err = true;
					$("#non_user_employee_approval").addClass("required");
					AssistHelper.finishedProcessing("error", "Please indicate the type of Employee approval.");
				} else if(($("#non_user_employee_approval").val()=="signed" || $("#non_user_employee_approval").val()=="attached") && $("#non_user_employee_approval_date").val().length==0) {
					err = true;
					$("#non_user_employee_approval_date").addClass("required");
					AssistHelper.finishedProcessing("error", "Please indicate the date of the Employee approval.");
				}
			}
			if($("#non_user_manager_approval").length>0) {
				if($("#non_user_manager_approval").val()=="X" || $("#non_user_manager_approval").val().length==0) {
					err = true;
					$("#non_user_manager_approval").addClass("required");
					AssistHelper.finishedProcessing("error", "Please indicate the type of Manager approval.");
				} else if(($("#non_user_manager_approval").val()=="signed" || $("#non_user_manager_approval").val()=="attached") && $("#non_user_manager_approval_date").val().length==0) {
					err = true;
					$("#non_user_manager_approval_date").addClass("required");
					AssistHelper.finishedProcessing("error", "Please indicate the date of the Manager approval.");
				}
			}
			if(!err) {
				if ($("#chk_confirm").is(':checked')) {
					var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>";
					$("#tbl_non_user").find("input, select").each(function() {
						dta+="&"+$(this).prop("id")+"="+$(this).val();
					});
					var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.CreateNonUserApprove", dta);
					//result = ["error",dta];
					if (result[0] == "ok") {
						var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				} else {
					$("#tr_approval_chk_confirm").addClass("required");
					AssistHelper.finishedProcessing("error", "Please tick the checkbox to confirm the completion of the "+scorecard_object_name+".");
				}
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");

	$("#btn_approve").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		AssistHelper.processing();
		$("#tr_approval_chk_confirm").removeClass("required");
		var err = false;
		var aa = $("#approve_actions").val();
		if(aa.length==0) { err = true; }
		if(err) {
			AssistHelper.finishedProcessing("error","An error has occurred [Code: PM6S10AA<?php echo __LINE__;?>].  Please reload the page and try again.");
		} else {
			if($("#chk_confirm").is(':checked')) {
				var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>&approve_actions="+aa;
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.CreateApprove",dta);
				if(result[0]=="ok") {
					var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			} else {
				$("#tr_approval_chk_confirm").addClass("required");
				AssistHelper.finishedProcessing("error","Please tick the checkbox to confirm the completion of the SCD.");
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");//.css({"color":"#009900","border":"1px solid #009900"});


	$("#btn_reject").button({
		icons: {primary: "ui-icon-closethick"},
	}).click(function() {
		AssistHelper.processing();
		var err = false;
		var reason = $("#reject_reason").val(); $("#reject_reason").removeClass("required");
		var fixit = $("#reject_next").val(); $("#reject_next").removeClass("required");
		if(reason.length==0) {
			var err_msg = "Please provide a reason for your rejection.";
			$("#reject_reason").addClass("required");
			err = true;
		} else if(fixit.length==0) {
			var err_msg = "Please indicate the next steps to be taken.";
			$("#reject_next").addClass("required");
			err = true;
		}
		if(err) {
			AssistHelper.finishedProcessing("error",err_msg);
		} else {
			var ra = $("#reject_actions").val();
			var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&reject_actions="+ra+"&reason="+AssistString.code(reason)+"&actor=<?php echo $actor; ?>&fixit="+fixit;
			console.log(dta);
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.CreateReject",dta);
			if(result[0]=="ok") {
				var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");//.css({"color":"#009900","border":"1px solid #009900"});

	$("#btn_finish").button({
		icons: {primary: "ui-icon-check"},
	}).click(function() {
		AssistHelper.processing();
		var err = false;
		var fa = $("#finish_actions").val();
		if(fa.length==0) { err = true; }
		if(err) {
			AssistHelper.finishedProcessing("error","An error has occurred [Code: PM6S10FA<?php echo __LINE__;?>].  Please reload the page and try again.");
		} else {
			if($("#chk_confirm").is(':checked')) {
				var dta = "obj_id=<?php echo $_REQUEST['obj_id']; ?>&actor=<?php echo $actor; ?>&finish_actions="+fa;
				if($("#manager_approval").length==0) {
					dta+="&manager_approval=request";
				} else {
					dta+="&manager_approval="+$("#manager_approval").val();
					if(testIfDateIsRequired($("#manager_approval").val())) {
						dta+="&manager_approval_date="+$("#manager_approval_date").val();
					}
				}
				if($("#employee_approval").length==0) {
					dta+="&employee_approval=request";
				} else {
					dta+="&employee_approval="+$("#employee_approval").val();
					if(testIfDateIsRequired($("#employee_approval").val())) {
						dta+="&employee_approval_date="+$("#employee_approval_date").val();
					}
				}
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.FinishCreate",dta);
				if(result[0]=="ok") {
					var url = "<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>.php";
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			} else {
				AssistHelper.finishedProcessing("error","Please tick the checkbox to confirm the completion of the "+scorecard_object_name+".");
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");//.css({"color":"#009900","border":"1px solid #009900"});


	$("#non_user_employee_approval").change(function() {
		if(testIfDateIsRequired($(this).val())) {
			$("#tr_non_user_employee_approval_date").show();
		} else {
			$("#tr_non_user_employee_approval_date").hide();
		}
	});
	$("#non_user_employee_approval").trigger("change");

	$("#non_user_manager_approval").change(function() {
		if(testIfDateIsRequired($(this).val())) {
			$("#tr_non_user_manager_approval_date").show();
		} else {
			$("#tr_non_user_manager_approval_date").hide();
		}
	});
	$("#non_user_manager_approval").trigger("change");


	function testIfDateIsRequired(v) {
		if(v=="signed" || v=="attached") {
			return true;
		} else {
			return false;
		}
	}

	$("#employee_approval").change(function() {
		if(testIfDateIsRequired($(this).val())) {
			$("#spn_employee_date").show();
		} else {
			$("#spn_employee_date").hide();
		}
	});
	$("#employee_approval").trigger("change");

	$("#manager_approval").change(function() {
		if(testIfDateIsRequired($(this).val())) {
			$("#spn_manager_date").show();
		} else {
			$("#spn_manager_date").hide();
		}
	});
	$("#manager_approval").trigger("change");

	<?php
	}//endif include_approval_js
	?>

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		AssistHelper.processing();
		//if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = '<?php echo (isset($page_redirect_url)?$page_redirect_url:$page); ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		//}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");


	<?php
	if($scorecard_not_complete_error) {
		?>
	$("#btn_next").hide();//.prop("disabled","disabled").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
	$("#btn_sign").hide();
	AssistHelper.processing();
	AssistHelper.finishedProcessing("error","This Scorecard is not complete.  Please review all steps and ensure that each step has been completed and saved.");
		<?php
	}
	?>
});

</script>

<?php
$create_logs = $scdObj->getCreateProcessLogs($obj_id);
if(count($create_logs)>0) {
	echo "<div class='no-print'>";
?>
<table style="margin: 0 auto;">
	<tr><th>Date</th><th>User</th><th>Activity</th></tr>
	<?php
	foreach($create_logs as $log) {
		echo "
		<tr>
			<td class='center'>".$log['scdl_insertdate']."</td>
			<td class='center'>".$log['scdl_insertusername']."</td>
			<td>".$log['activity']."</td>
		</tr>";
	}
	?>
</table>
<?php
	echo "</div>";
}
?>