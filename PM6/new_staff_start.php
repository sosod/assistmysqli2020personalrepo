<?php
require_once("inc_header.php");
$create_status = PM6::IP_MANAGER;

ASSIST_HELPER::displayResult(array("info","Redirecting..."));

//$_REQUEST['employee_id'] = $helper->getUserID();
$scdObject = new PM6_SCORECARD();

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($_REQUEST);
//echo "<hr />";

if(!isset($_REQUEST['step'])) {
	$_REQUEST['step'] = "review";
}
$step = $_REQUEST['step'];
if($step==false) { $step = "new"; }
//echo $step;
switch($step) {
	case "0": case "new":
		//start new scorecard:
		$var = $_REQUEST;
		$var['create_status'] = $create_status;
		$var['step'] = 0;	//PM6_SCORECARD->addObject requires $var['step']==0 in order to trigger the add function otherwise it assumes that it was an accidental add
						//get bonus scale from employee_settings
		$empObject = new PM6_SETUP_EMPLOYEE();
		$employee_settings = $empObject->getFormattedEmployeeSetting();
		//bonus_scale should be isset catch just in case anyway
		$bonus_scale = isset($employee_settings['bonus_scale']) ? $employee_settings['bonus_scale'] : 0;
		$var['bonus_id'] = $bonus_scale;
						//trigger PM6_SCORECARD->addObject
		$scdObject = new PM6_SCORECARD();
		$result = $scdObject->addObject($var);
						//redirect to Step 1 - $page_redirect_url."_step1.php?obj_id=".r['obj_id']."&r[]=ok&r[]=".r[1]
		if($result[0]=="ok") {
			$where_to_next = $page_redirect_url."_step1.php?obj_id=".$result['obj_id']."&create_status=".$create_status."&r[]=ok&r[]=".$result[1];
		} else {
			$where_to_next = $page_redirect_url.".php?&r[]=error&r[]=".$result[1];
		}
		break;
	case 1:case "1":case "continue":
		//continue scorecard in progress:
		$var = $_REQUEST;
		//redirect to step 1
		if(isset($var['obj_id']) && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($var['obj_id'])) {
			if(isset($_REQUEST['new_status']) && ($_REQUEST['new_status'] & PM6::IP_MANAGER) == PM6::IP_MANAGER) {
				$where_to_next = $page_redirect_url."_step1.php?obj_id=".$var['obj_id']."&create_status=".$create_status;
			} else {
				$where_to_next = $page_redirect_url."_step10.php?page_action=review&obj_id=".$var['obj_id']."&create_status=".$create_status;
			}
		} else {
			$where_to_next = $page_redirect_url.".php?&r[]=error&r[]=An+error+occurred+trying+to+identify+your+".$scdObject->getObjectName($scdObject->getMyObjectName()).".+Please+try+again.";
		}
						//TODO - consider how to pick up where user was in build process and redirect them there?
		break;
	case "review":case "view":
//		echo "<hr /><h1 class='green'>GOING TO REVIEW from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

		$var = $_REQUEST;
		//redirect to step 1
		if(isset($var['obj_id']) && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($var['obj_id'])) {
			$where_to_next = $page_redirect_url."_step10.php?page_action=".$step."&obj_id=".$var['obj_id']."&create_status=".$create_status;
		} else {
			$where_to_next = $page_redirect_url.".php?&r[]=error&r[]=An+error+occurred+trying+to+identify+your+".$scdObject->getObjectName($scdObject->getMyObjectName()).".+Please+try+again.";
		}
		break;
//TODO - program these steps! -  edit, delete
	case "edit":
	case "delete":
		ASSIST_HELPER::displayResult(array("error","Oops!  To Be Programmed."));
		break;
}
//echo $where_to_next;
?>
<!--<p>NEW Scorecard: Create the Scorecard record and route to Step 1</p>
<ul>
	<li>step=0&obj_id=0&employee_id=(tkid)&emp_id=(emp_id)&job_id=&bonus_id=</li>
	<li>EMPLOYEE ID</li>
	<li>BONUS SCALE ID - from employee settings</li>
</ul>


<p>IN PROGRESS: Route to step 1 (or can I route to last completed step?</p>
<p>Review: Route to Step 10</p>
<p>Edit: Display Confirmation box asking to undo all approvals and route to Step 1</p>
<p class="red">Delete: How does the user trigger a delete to start over?</p>-->
<?php
if(isset($where_to_next)) {
?>
<script type="text/javascript">
	AssistHelper.processing();
	document.location.href = "<?php echo $where_to_next; ?>";
</script>
<?php
}
?>