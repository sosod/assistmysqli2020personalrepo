<?php
/**
 * To manage the ASSESSMENT object
 *
 * Created on: 1 January 2016
 * Authors: Janet Currie
 *
 */

class PM6_SCORECARD extends PM6 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $id_field = "_id";					//scorecard ref
	protected $parent_field = "_empid";				//employee id from EMP module
	protected $name_field = "_jobid";				//job id from EMP module
	protected $owner_field = "_tkid";				//tkid from timekeep table
	protected $deadline_field = "_bonusscaleid";	//bonus scale
	protected $status_field = "_status";			//status



	/*
	protected $progress_status_field = "_status_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/


    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "SCORECARD";
    const TABLE = "scorecards";
    const TABLE_FLD = "scd";
    const REFTAG = "SCD";
	const LOG_TABLE = "scorecards";


    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;

		/*$this->sources = array(
			array('active'=>true,'modref'=>"SDP15"),
			array('active'=>true,'modref'=>"SDP15I"),
		);*/

/*
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }

public function getObjectType() { return self::OBJECT_TYPE; }
public function getEmpIDTableFieldName() { return $this->getParentFieldName(); }
public function getJobIDTableFieldName() { return $this->getNameFieldName(); }
public function getBonusScaleTableFieldName() { return $this->getDeadlineFieldName(); }
public function getUserIDTableFieldName() { return $this->getOwnerFieldName(); }

	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		//$sql = serialize($var);
		if(isset($var['step'])) {
			$result = array("error","Start of addObject function");
			$step = $var['step'];
			switch($step) {
				case 0:
				case "0":
					$employee_id = $var['employee_id'];
					$emp_id = $var['emp_id'];
					$job_id = $var['job_id'];
					$bonus_id = $var['bonus_id'];
					$create_status = isset($var['create_status']) ? $var['create_status'] : PM6::IP_SUPER;
					switch($create_status) {
						case PM6::IP_EMPLOYEE: $actor = "employee"; break;
						case PM6::IP_MANAGER: $actor = "manager"; break;
						case PM6::IP_CHAMPION: $actor = "champion"; break;
						case PM6::IP_SUPER: default: $actor = "super_user"; break;
					}
					$sql = "INSERT INTO ".$this->getTableName()." SET
							".self::TABLE_FLD."_tkid = '".$employee_id."',
							".self::TABLE_FLD."_empid = '".$emp_id."',
							".self::TABLE_FLD."_jobid = '".$job_id."',
							".self::TABLE_FLD."_bonusscaleid = '".$bonus_id."',
							".self::TABLE_FLD."_comment = '',
							".self::TABLE_FLD."_status = ".self::ACTIVE.",
							".self::TABLE_FLD."_new_status = ".$create_status.",
							".self::TABLE_FLD."_current_builder = '".$this->getUserID()."',
							".self::TABLE_FLD."_insertuser = '".$this->getUserID()."',
							".self::TABLE_FLD."_insertdate = now()";
					$id = $this->db_insert($sql);
					$result = array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$id." creation started successfully. Please continue with the next step.",'obj_id'=>$id);
					$insert_data = array(
							self::TABLE_FLD.'_tkid' => $employee_id,
							self::TABLE_FLD.'_status' => self::ACTIVE,
					);
					//$logObj = new PM6_LOG_OBJECT();
					//$logObj->addObject(array(),$this->getMyObjectType(),$logObj->getCreateLogAction(),"Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id,"",$insert_data,$id);
					$this->logProcess($id,$actor,"create","create","created","",0,PM6::ACTIVE,0,$create_status);
					$this->logMyAction($id, "CREATE", "", $insert_data);
					break;
				default:
					$sql = "hello";
					break;
			}
		} else {
			$result = array("error","Sorry, I couldn't work out where you were in the Create process.  Please try again.");
		}
		return $result;
	}
/*
	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}

	*/

	//Move assessment from Create (pending) to Edit
	public function activateObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old[self::TABLE_FLD.'_status'];
		//Add ACTIVATED status IF NOT PRESENT
		if(($old_status&PM6::ACTIVATED)!=PM6::ACTIVATED) {
			$new_status = $old_status + self::ACTIVATED;
		} else {
			$new_status = $old_status;
		}
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." + ".self::ACTIVATED.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $new_status,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $old_status,
					);
				$this->logMyAction($object_id, "ACTIVATE", $old_data, $insert_data);
				//$this->logProcess NOT required as it is done in the calling function AA-482 [JC]

			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully activated.  It is now available for triggering.");
		}
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Undo activation of scorecard to make it available to edit - AA-481
	public function undoActivation($object_id) {
		if(is_array($object_id)) {
			$object_id = $object_id['obj_id'];
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old[self::TABLE_FLD.'_status'];
		$new_status = $old_status - self::ACTIVATED;
		if(($new_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
			$new_status-=PM6::CONFIRMED;
		}
		$old_process_status = $old[self::TABLE_FLD.'_new_status'];
		$new_process_status = PM6::IP_SUPER;
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = (".$new_status.") 
				, ".self::TABLE_FLD."_new_status = ".self::IP_SUPER."
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $new_status,
							self::TABLE_FLD.'_new_status' => $new_process_status,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $old_status,
							self::TABLE_FLD.'_new_status' => $old_process_status,
					);
				$this->logMyAction($object_id, "REVERSEACTIVATE", $old_data, $insert_data);
				//AA-482 JC
				//Setting page_action==create here as this is done to restore the SCD to the Create process & not as part of the true EDIT process
				//&& Manually removing any approval status
				$this->logProcess($object_id,"super_user","edit","create","undo_activation","",$old_status,$new_status,$old_process_status,$new_process_status);

			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." activation reversed.  It is now available for editing.");
		}
		return array("error","An error occurred while trying to undo the activation of ".$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Deactivate an assessment which has been already triggered and therefore can't be deleted
	public function deactivateObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as - self::ACTIVE + self::INACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "DEACTIVATE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully deactivated.");
		}
		return array("error","An error occurred while trying to deactivate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Restore a previously DEACTIVATED assessment
	public function restoreObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as + self::ACTIVE - self::INACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "RESTORE", $old_data, $insert_data);
			return array("ok",$this->getObjectName($this->getMyObjectType())." ".self::REFTAG.$object_id." successfully restored.");
		}
		return array("error","An error occurred while trying to restore ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}


	//Delete an assessment which has NOT been triggered
	public function deleteObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as - self::ACTIVE + self::DELETED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "DELETE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully deleted.");
		}
		return array("error","An error occurred while trying to delete ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Assign an assessment to a different employee
	public function reassignObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getTableField()."_tkid as tkid FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "tkid");
		$tkid = $var['employee'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getTableField()."_tkid = '".$tkid."' WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_tkid' => $tkid,
							'name' => $this->getAUserName($tkid),
					);
					$old_data = array(
							self::TABLE_FLD.'_tkid' => $as,
							'name' => $this->getAUserName($as),
					);
				$this->logMyAction($object_id, "EDIT", $old_data, $insert_data, "name");
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully reassigned.");
		}
		return array("error","An error occurred while trying to reassign ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Change Employees Bonus Scale
	public function editBonus($var) {
		$object_id = $var['obj_id'];
		$result = array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
		$bonus_id = $var['bonus_id'];
		$sql = "SELECT ".$this->getBonusScaleTableFieldName()." as bonus FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$original = $this->mysql_fetch_one($sql);
		$original_bonus_id = $original['bonus'];
		if($original_bonus_id!=$bonus_id) {
			$bonusObject = new PM6_SETUP_BONUS();
			$bonus_scales = $bonusObject->getListOfAllBonusScales(true);
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getBonusScaleTableFieldName()." = ".$bonus_id." WHERE ".$this->getIDFieldName()." = ".$object_id;
			$mar = $this->db_update($sql);

			$insert_data = array(
				$this->getBonusScaleTableFieldName() => $bonus_scales[$bonus_id],
				$this->getBonusScaleTableFieldName().'_raw' => $bonus_id,
			);
			$old_data = array(
				$this->getBonusScaleTableFieldName() => $bonus_scales[$original_bonus_id],
				$this->getBonusScaleTableFieldName().'_raw' => $original_bonus_id,
			);
			$this->logMyAction($object_id, "EDIT_BONUS", $old_data, $insert_data,$this->getBonusScaleTableFieldName());
			$result = array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully edited.");
		} else {
			$result = array("info","No change was found to be saved.");
		}
		return $result;
	}





	public function finishCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['finish_actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['finish_actions']);
		$actor = $var['actor'];

		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $new_status;
		$scd_status = $old['scd_status'];
		$old_scd_status = $scd_status;

		$log_actions = array();

		if(isset($actions["confirm"])) {
			if(!$this->validateStatus($scd_status,PM6::CONFIRMED)){
				$scd_status+=PM6::CONFIRMED;
				$this->logProcess($object_id,$actor,"finish","create","confirmed","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			$log_actions[] = "CONFIRM";
			unset($actions['confirm']);
		}
		if(isset($actions["awaiting_approval"]) && !isset($actions["activate"])) {
			if(!$this->validateStatus($new_status,PM6::AWAITING_APPROVAL)){
				$new_status+=PM6::AWAITING_APPROVAL;
			}
			unset($actions["awaiting_approval"]);
		}
		$is_employee_approved = false;
		if(isset($actions["employee_digitally_approved"])) {
			if(!$this->validateStatus($new_status,PM6::EMPLOYEE_DIGITALLY_APPROVED)){
				$new_status+=PM6::EMPLOYEE_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"finish","create","employee_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::EMPLOYEE_REJECTED)) {
				$new_status-=PM6::EMPLOYEE_REJECTED;
			}
			$log_actions['employee'] = "APPROVE";
			$is_employee_approved = true;
			unset($actions["employee_digitally_approved"]);
		} elseif(isset($var['employee_approval'])  && strpos($var['employee_approval'],"request")===false) {
			switch($var['employee_approval']) {
				//'attached'=>"Approved (Signed POE attached)",
				case "attached":
					//TODO approved POE attached
					break;
				//'signed'=>"Approved (Signed POE available)",
				case "signed":
					if(!$this->validateStatus($new_status,PM6::EMPLOYEE_SIGN_APPROVED)){
						$new_status+=PM6::EMPLOYEE_SIGN_APPROVED;
					}
					$approval_date = $var['employee_approval_date'];
					$this->logProcess($object_id,$actor,"approve","create","employee_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
					break;
				//'no_poe'=>"Approved (No signed POE available)",
				case "no_poe":
					if(!$this->validateStatus($new_status,PM6::EMPLOYEE_APPROVED_NO_POE)){
						$new_status+=PM6::EMPLOYEE_APPROVED_NO_POE;
					}
					$this->logProcess($object_id,$actor,"approve","create","employee_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
					break;
				case "request":
					//do nothing - digital is to be requested
					//placeholder - shouldn't even trigger
					break;
			}
			$log_actions['employee'] = "APPROVE";
			$is_employee_approved = true;
		} elseif(isset($var['employee_approval']) && strpos($var['employee_approval'],"request")!==false) {
			$actions[] = $var['employee_approval'];
		}
		$is_manager_approved = false;
		if(isset($actions["manager_digitally_approved"]) || (isset($var['manager_approval']) && $var['manager_approval']=="digital")) {
			if(!$this->validateStatus($new_status,PM6::MANAGER_DIGITALLY_APPROVED)){
				$new_status+=PM6::MANAGER_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"finish","create","manager_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::MANAGER_REJECTED)) {
				$new_status-=PM6::MANAGER_REJECTED;
			}
			$log_actions['manager'] = "APPROVE";
			$is_manager_approved = true;
			unset($actions["manager_digitally_approved"]);
		} elseif(isset($var['manager_approval']) && strpos($var['manager_approval'],"request")===false) {
			switch($var['manager_approval']) {
				//'attached'=>"Approved (Signed POE attached)",
				case "attached":
					//TODO approved POE attached
					break;
				//'signed'=>"Approved (Signed POE available)",
				case "signed":
					if(!$this->validateStatus($new_status,PM6::MANAGER_SIGN_APPROVED)){
						$new_status+=PM6::MANAGER_SIGN_APPROVED;
						$this->logProcess($object_id,$actor,"approve","create","manager_sign_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
					}
					$approval_date = $var['manager_approval_date'];
					$this->logProcess($object_id,$actor,"approve","create","manager_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
					break;
				//'no_poe'=>"Approved (No signed POE available)",
				case "no_poe":
					if(!$this->validateStatus($new_status,PM6::MANAGER_APPROVED_NO_POE)){
						$new_status+=PM6::MANAGER_APPROVED_NO_POE;
					}
					$this->logProcess($object_id,$actor,"approve","create","manager_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
					break;
				case "request":
					//do nothing - digital is to be requested
					//placeholder - shouldn't even trigger
					break;
			}
			$log_actions['manager'] = "APPROVE";
			$is_manager_approved = true;
		} elseif(isset($var['manager_approval']) && strpos($var['manager_approval'],"request")!==false) {
			$actions[] = $var['manager_approval'];
		}

		//remove awaiting approval if approval is complete
		if($is_manager_approved && $is_employee_approved) {
			if($this->validateStatus($new_status,PM6::AWAITING_APPROVAL)){
				$new_status-=PM6::AWAITING_APPROVAL;
			}
		}

		//update statuses locally first then trigger activation if necessary
		$insert_data = array();
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$a)));
		}

		//Activate SCD if needed
		if(isset($actions['activate']) || ($is_employee_approved && $is_manager_approved)){
			$result = $this->activateObject(array("obj_id"=>$object_id));
			$this->logProcess($object_id,$actor,"approve","create","activated","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			unset($actions['activate']);
		} else {
			$result = array("ok","Finish process completed successfully.");
		}
		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false || strpos($action,"request")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				$notification_var = array(
					'user_role'=>$notify_actor,
					'notify_user'=>$notify_user_id,
					'actor'=>$actor,
					'obj_id'=>$object_id,
					'notification_type'=>"create_approval_reminder",
				);
				$this->notifyUserCreateProcess($notification_var);
			}
		}
		return $result;
	}

	public function approveCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['approve_actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['approve_actions']);
		$actor = $var['actor'];

		$log_actions = array();

		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $old['scd_new_status'];
		$scd_status = $old['scd_status'];
		$old_scd_status = $old['scd_status'];

		if(isset($actions["confirm"])) {
			if(!$this->validateStatus($scd_status,PM6::CONFIRMED)){
				$scd_status+=PM6::CONFIRMED;
			}
			$log_actions[] = "CONFIRM";
			unset($actions['confirm']);
		}
		if(isset($actions["-confirm"])) {
			$scd_status-=$this->validateStatus($scd_status,PM6::CONFIRMED,"needle");
			unset($actions['-confirm']);
		}
		if(isset($actions["awaiting_approval"]) && !isset($actions["activate"])) {
			if(!$this->validateStatus($new_status,PM6::AWAITING_APPROVAL)){
				$new_status+=PM6::AWAITING_APPROVAL;
			}
			unset($actions["awaiting_approval"]);
		}
		if(isset($actions["-awaiting_approval"])) {
			$new_status-=$this->validateStatus($new_status,PM6::AWAITING_APPROVAL,"needle");
			unset($actions["-awaiting_approval"]);
		}
		if(isset($actions["employee_digitally_approved"])) {
			if(!$this->validateStatus($new_status,PM6::EMPLOYEE_DIGITALLY_APPROVED)){
				$new_status+=PM6::EMPLOYEE_DIGITALLY_APPROVED;
			}
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::EMPLOYEE_REJECTED)) {
				$new_status-=PM6::EMPLOYEE_REJECTED;
			}
			$log_actions['employee'] = "APPROVE";
			unset($actions["employee_digitally_approved"]);
		}
		if(isset($actions["manager_digitally_approved"])) {
			if(!$this->validateStatus($new_status,PM6::MANAGER_DIGITALLY_APPROVED)){
				$new_status+=PM6::MANAGER_DIGITALLY_APPROVED;
			}
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::MANAGER_REJECTED)) {
				$new_status-=PM6::MANAGER_REJECTED;
			}
			$log_actions['manager'] = "APPROVE";
			unset($actions["manager_digitally_approved"]);
		}

		$insert_data = array();
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;

		//update statuses locally first then trigger activation if necessary
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//Log process
		$this->logProcess($object_id,$actor,"approve","create",$actor."_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);

		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$a)));
		}

		//Activate SCD if needed
		if(isset($actions['activate'])){
			$result = $this->activateObject(array("obj_id"=>$object_id));
			unset($actions['activate']);
			$this->logProcess($object_id,$actor,"approve","create","activated","",$old_scd_status,$scd_status,$old_new_status,$new_status);
		} else {
			$result = array("ok","Approve process completed successfully.");
		}

		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				$notification_var = array(
					'user_role'=>$notify_actor,
					'notify_user'=>$notify_user_id,
					'actor'=>$actor,
					'obj_id'=>$object_id,
					'notification_type'=>"create_approval_reminder",
				);
				$this->notifyUserCreateProcess($notification_var);

			}
		}

		return $result;
	}

	public function approveNonUserCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['actions']);
		$actor = $var['actor'];

		$log_actions = array();

		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $old['scd_new_status'];
		$scd_status = $old['scd_status'];
		$old_scd_status = $old['scd_status'];

		if(isset($actions["confirm"])) {
			if(!$this->validateStatus($scd_status,PM6::CONFIRMED)){
				$scd_status+=PM6::CONFIRMED;
			}
			$this->logProcess($object_id,$actor,"finish","create","confirmed","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			$log_actions[] = "CONFIRM";
			unset($actions['confirm']);
		}
		if(isset($actions["-confirm"])) {
			$scd_status-=$this->validateStatus($scd_status,PM6::CONFIRMED,"needle");
			unset($actions['-confirm']);
		}
		if(isset($actions["awaiting_approval"]) && !isset($actions["activate"])) {
			if(!$this->validateStatus($new_status,PM6::AWAITING_APPROVAL)){
				$new_status+=PM6::AWAITING_APPROVAL;
			}
			unset($actions["awaiting_approval"]);
		}
		if(isset($actions["-awaiting_approval"])) {
			$new_status-=$this->validateStatus($new_status,PM6::AWAITING_APPROVAL,"needle");
			unset($actions["-awaiting_approval"]);
		}
		if(isset($actions["employee_digitally_approved"])) {
			if(!$this->validateStatus($new_status,PM6::EMPLOYEE_DIGITALLY_APPROVED)){
				$new_status+=PM6::EMPLOYEE_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"approve","create","employee_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			$log_actions['employee'] = "APPROVE";
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::EMPLOYEE_REJECTED)) {
				$new_status-=PM6::EMPLOYEE_REJECTED;
			}
			if(isset($var['non_user_manager_approval'])) {
				switch($var['non_user_manager_approval']) {
					//'attached'=>"Approved (Signed POE attached)",
					case "attached":
						//TODO approved POE attached
						break;
					//'signed'=>"Approved (Signed POE available)",
					case "signed":
						if(!$this->validateStatus($new_status,PM6::MANAGER_SIGN_APPROVED)){
							$new_status+=PM6::MANAGER_SIGN_APPROVED;
							$this->logProcess($object_id,$actor,"approve","create","manager_sign_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
						}
						$approval_date = $var['non_user_manager_approval_date'];
						$this->logProcess($object_id,$actor,"approve","create","manager_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
					//'no_poe'=>"Approved (No signed POE available)",
					case "no_poe":
						if(!$this->validateStatus($new_status,PM6::MANAGER_APPROVED_NO_POE)){
							$new_status+=PM6::MANAGER_APPROVED_NO_POE;
						}
						$this->logProcess($object_id,$actor,"approve","create","manager_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
				}
				$log_actions['manager'] = "APPROVE";
			}
			unset($actions["employee_digitally_approved"]);
		} elseif(isset($actions["manager_digitally_approved"])) {
			if(!$this->validateStatus($new_status,PM6::MANAGER_DIGITALLY_APPROVED)){
				$new_status+=PM6::MANAGER_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"approve","create","manager_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			$log_actions['manager']="APPROVE";
			//remove any rejections just in case it's on a reject loop
			if($this->validateStatus($new_status,PM6::MANAGER_REJECTED)) {
				$new_status-=PM6::MANAGER_REJECTED;
			}
			if(isset($var['non_user_employee_approval'])) {
				switch($var['non_user_employee_approval']) {
					//'attached'=>"Approved (Signed POE attached)",
					case "attached":
						//TODO approved POE attached
						break;
					//'signed'=>"Approved (Signed POE available)",
					case "signed":
						if(!$this->validateStatus($new_status,PM6::EMPLOYEE_SIGN_APPROVED)){
							$new_status+=PM6::EMPLOYEE_SIGN_APPROVED;
						}
						$approval_date = $var['non_user_employee_approval_date'];
						$this->logProcess($object_id,$actor,"approve","create","employee_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
					//'no_poe'=>"Approved (No signed POE available)",
					case "no_poe":
						if(!$this->validateStatus($new_status,PM6::EMPLOYEE_APPROVED_NO_POE)){
							$new_status+=PM6::EMPLOYEE_APPROVED_NO_POE;
						}
						$this->logProcess($object_id,$actor,"approve","create","employee_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
				}
				$log_actions['employee'] = "APPROVE";
			}
			unset($actions["manager_digitally_approved"]);
		} else {
			//possibility of both manager & employee being manually signed off by SU/Champion
			//Process employee
			if(isset($var['non_user_employee_approval'])) {
				$log_actions['employee'] = "APPROVE";
				//remove any rejections just in case it's on a reject loop
				if($this->validateStatus($new_status,PM6::EMPLOYEE_REJECTED)) {
					$new_status-=PM6::EMPLOYEE_REJECTED;
				}
				switch($var['non_user_employee_approval']) {
					//'attached'=>"Approved (Signed POE attached)",
					case "attached":
						//TODO approved POE attached
						break;
					//'signed'=>"Approved (Signed POE available)",
					case "signed":
						if(!$this->validateStatus($new_status,PM6::EMPLOYEE_SIGN_APPROVED)){
							$new_status+=PM6::EMPLOYEE_SIGN_APPROVED;
						}
						$approval_date = $var['non_user_employee_approval_date'];
						$this->logProcess($object_id,$actor,"approve","create","employee_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
					//'no_poe'=>"Approved (No signed POE available)",
					case "no_poe":
						if(!$this->validateStatus($new_status,PM6::EMPLOYEE_APPROVED_NO_POE)){
							$new_status+=PM6::EMPLOYEE_APPROVED_NO_POE;
						}
						$this->logProcess($object_id,$actor,"approve","create","employee_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
				}
			}
			//Process manager
			if(isset($var['non_user_manager_approval'])) {
				$log_actions['manager'] = "APPROVE";
				//remove any rejections just in case it's on a reject loop
				if($this->validateStatus($new_status,PM6::MANAGER_REJECTED)) {
					$new_status-=PM6::MANAGER_REJECTED;
				}
				switch($var['non_user_manager_approval']) {
					//'attached'=>"Approved (Signed POE attached)",
					case "attached":
						//TODO approved POE attached
						break;
					//'signed'=>"Approved (Signed POE available)",
					case "signed":
						if(!$this->validateStatus($new_status,PM6::MANAGER_SIGN_APPROVED)){
							$new_status+=PM6::MANAGER_SIGN_APPROVED;
						}
						$approval_date = $var['non_user_manager_approval_date'];
						$this->logProcess($object_id,$actor,"approve","create","manager_sign_approved",$approval_date,$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
					//'no_poe'=>"Approved (No signed POE available)",
					case "no_poe":
						if(!$this->validateStatus($new_status,PM6::MANAGER_APPROVED_NO_POE)){
							$new_status+=PM6::MANAGER_APPROVED_NO_POE;
						}
						$this->logProcess($object_id,$actor,"approve","create","manager_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
						break;
				}
			}
		}

		$insert_data = array();
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;

		//update statuses locally first then trigger activation if necessary
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//Log process
		$this->logProcess($object_id,$actor,"approve","create",$actor."_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$a)));
		}

		//Activate SCD if needed
		if(isset($actions['activate'])){
			$result = $this->activateObject(array("obj_id"=>$object_id));
			unset($actions['activate']);
			$this->logProcess($object_id,$actor,"approve","create","activated","",$old_scd_status,$scd_status,$old_new_status,$new_status);
		} else {
			$result = array("ok","Approve process completed successfully.");
		}

//		$result[1].=$sql;

		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				$notification_var = array(
					'user_role'=>$notify_actor,
					'notify_user'=>$notify_user_id,
					'actor'=>$actor,
					'obj_id'=>$object_id,
					'notification_type'=>"create_approval_reminder",
				);
				$this->notifyUserCreateProcess($notification_var);

			}
		}

		return $result;

	}


	public function rejectCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['reject_actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['reject_actions']);

		$reason = $var['reason'];
		$fixit = $var['fixit'];
		$actor = $var['actor'];

		$log_actions = array();

		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $new_status;
		$scd_status = $old['scd_status'];
		$old_scd_status = $scd_status;

		if(isset($actions["-confirm"])) {
			$scd_status-=$this->validateStatus($scd_status,PM6::CONFIRMED,"needle");
			unset($actions['-confirm']);
		}
		if(isset($actions["-awaiting_approval"])) {
			$new_status-=$this->validateStatus($new_status,PM6::AWAITING_APPROVAL,"needle");
			unset($actions["-awaiting_approval"]);
		}
		if(isset($actions["employee_rejected"])) {
			if(!$this->validateStatus($new_status,PM6::EMPLOYEE_REJECTED)) {
				$new_status+=PM6::EMPLOYEE_REJECTED;
			}
			if($this->validateStatus($new_status,PM6::MANAGER_DIGITALLY_APPROVED)) {
				$new_status-=PM6::MANAGER_DIGITALLY_APPROVED;
			}
			unset($actions["employee_rejected"]);
			$log_actions['employee']="REJECT";
		}
		if(isset($actions["manager_rejected"])) {
			if(!$this->validateStatus($new_status,PM6::MANAGER_REJECTED)) {
				$new_status+=PM6::MANAGER_REJECTED;
			}
			if($this->validateStatus($new_status,PM6::EMPLOYEE_DIGITALLY_APPROVED)) {
				$new_status-=PM6::EMPLOYEE_DIGITALLY_APPROVED;
			}
			unset($actions["manager_rejected"]);
			$log_actions['manager']="REJECT";
		}


		$insert_data = array();

		switch($fixit) {
			case "fix_self":
				//remove any IP statuses and add local user status
				foreach($this->ip_statuses as $key => $ip) {
					if(($new_status & $ip) == $ip) {
						$new_status -= $ip;
					}
				}
				$new_status+=$this->ip_statuses[$actor];

				//change current builder to local user
				$insert_data['scd_current_builder'] = $this->getUserID();

				break;
			case "fix_builder":
				//don't do anything but notify the builder of the rejection
				//TODO - notify builder of rejection
				break;
		}
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;


		//update statuses locally first then trigger activation if necessary
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//Log process
		$this->logProcess($object_id,$actor,"reject","create",$actor."_rejected",$reason,$old_scd_status,$scd_status,$old_new_status,$new_status);

		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$f)));
		}


		$result = array("ok","Rejection process completed successfully.");

//		$result[1].=$sql;

		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false || strpos($action,"reject")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				//TODO - Write emails to notify actors of scorecards waiting approval

			}
		}

		return $result;
	}



	public function undoConfirmationCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['unconfirm_actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['unconfirm_actions']);
		$actor = $var['actor'];


		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $new_status;
		$scd_status = $old['scd_status'];
		$old_scd_status = $scd_status;

		if(isset($actions["-confirm"])) {
			$scd_status-=$this->validateStatus($scd_status,PM6::CONFIRMED,"needle");
			unset($actions['-confirm']);
		}
		if(isset($actions["-awaiting_approval"])) {
			$new_status-=$this->validateStatus($new_status,PM6::AWAITING_APPROVAL,"needle");
			unset($actions['-awaiting_approval']);
		}
		if(isset($actions["-approval"])) {
			if($this->validateStatus($new_status, PM6::EMPLOYEE_DIGITALLY_APPROVED)) {
				$new_status -= PM6::EMPLOYEE_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_employee_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_SIGN_ATTACHED)) {
				$new_status -= PM6::EMPLOYEE_SIGN_ATTACHED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_employee_sign_attached","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_SIGN_APPROVED)) {
				$new_status -= PM6::EMPLOYEE_SIGN_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_employee_sign_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_APPROVED_NO_POE)) {
				$new_status -= PM6::EMPLOYEE_APPROVED_NO_POE;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_employee_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}

			if($this->validateStatus($new_status, PM6::MANAGER_DIGITALLY_APPROVED)) {
				$new_status -= PM6::MANAGER_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_SIGN_ATTACHED)) {
				$new_status -= PM6::MANAGER_SIGN_ATTACHED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_sign_attached","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_SIGN_APPROVED)) {
				$new_status -= PM6::MANAGER_SIGN_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_sign_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_APPROVED_NO_POE)) {
				$new_status -= PM6::MANAGER_APPROVED_NO_POE;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			unset($actions['-approval']);
		}
		//return_builder - happens automatically - original IP_* status still in new_status and removing the CONFIRMED status will default back to IP
		unset($actions['return_builder']);

		$insert_data = array();
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;



		//update statuses locally first then trigger activation if necessary
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//Log process
		$this->logProcess($object_id,$actor,"undo_confirm","create","undo_confirm","",$old_scd_status,$scd_status,$old_new_status,$new_status);

		$log_actions = array();
		$log_actions[] = "UNDOCONFIRM";
		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$a)));
		}


		$result = array("ok","Undo process completed successfully.");

//		$result[1].=$sql;

		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false || strpos($action,"undo_confirm")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				//TODO - Write emails to notify actors of scorecards waiting approval

			}
		}

		return $result;
	}



	public function undoApprovalCreateProcess($var) {
		$object_id = $var['obj_id'];
		$acts = explode("|",$var['unapprove_actions']);
		$actions = array_flip($acts);
		$result = array("error","Oops, not yet programmed: ".$var['unapprove_actions']);
		$actor = $var['actor'];


		$old = $this->getRawObject($object_id);
		$new_status = $old['scd_new_status'];
		$old_new_status = $new_status;
		$scd_status = $old['scd_status'];
		$old_scd_status = $scd_status;

		if(isset($actions["-employee_approval"])) {
			if($this->validateStatus($new_status, PM6::EMPLOYEE_DIGITALLY_APPROVED)) {
				$new_status -= PM6::EMPLOYEE_DIGITALLY_APPROVED;
				$this->logProcess($object_id, $actor, "undo_confirm", "create", "undo_employee_digitally_approved", "", $old_scd_status, $scd_status, $old_new_status, $new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_SIGN_ATTACHED)) {
				$new_status -= PM6::EMPLOYEE_SIGN_ATTACHED;
				$this->logProcess($object_id, $actor, "undo_confirm", "create", "undo_employee_sign_attached", "", $old_scd_status, $scd_status, $old_new_status, $new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_SIGN_APPROVED)) {
				$new_status -= PM6::EMPLOYEE_SIGN_APPROVED;
				$this->logProcess($object_id, $actor, "undo_confirm", "create", "undo_employee_sign_approved", "", $old_scd_status, $scd_status, $old_new_status, $new_status);
			}
			if($this->validateStatus($new_status, PM6::EMPLOYEE_APPROVED_NO_POE)) {
				$new_status -= PM6::EMPLOYEE_APPROVED_NO_POE;
				$this->logProcess($object_id, $actor, "undo_confirm", "create", "undo_employee_approved_no_poe", "", $old_scd_status, $scd_status, $old_new_status, $new_status);
			}
			unset($actions['-employee_approval']);
		}
		if(isset($actions["-manager_approval"])) {
			if($this->validateStatus($new_status, PM6::MANAGER_DIGITALLY_APPROVED)) {
				$new_status -= PM6::MANAGER_DIGITALLY_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_digitally_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_SIGN_ATTACHED)) {
				$new_status -= PM6::MANAGER_SIGN_ATTACHED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_sign_attached","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_SIGN_APPROVED)) {
				$new_status -= PM6::MANAGER_SIGN_APPROVED;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_sign_approved","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			if($this->validateStatus($new_status, PM6::MANAGER_APPROVED_NO_POE)) {
				$new_status -= PM6::MANAGER_APPROVED_NO_POE;
				$this->logProcess($object_id,$actor,"undo_confirm","create","undo_manager_approved_no_poe","",$old_scd_status,$scd_status,$old_new_status,$new_status);
			}
			unset($actions['-manager_approval']);
		}
		//return_builder - happens automatically - original IP_* status still in new_status and removing the CONFIRMED status will default back to IP
		unset($actions['return_builder']);

		$insert_data = array();
		$insert_data['scd_status'] = $scd_status;
		$insert_data['scd_new_status'] = $new_status;



		//update statuses locally first then trigger activation if necessary
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->convertArrayToSQLForSave($insert_data)." 
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//Log process
		$this->logProcess($object_id,$actor,"undo_approval","create","undo_approval","",$old_scd_status,$scd_status,$old_new_status,$new_status);

		$log_actions = array();
		$log_actions[$actor] = "UNDOAPPROVE";
		foreach($log_actions as $f => $a) {
			$this->logMyAction($object_id, $a, $old, $insert_data,ucfirst(str_replace("_"," ",$a)));
		}

		$result = array("ok","Undo process completed successfully.");

//		$result[1].=$sql;

		//Email Notifications
		foreach($actions as $action => $key) {
			if(strpos($action,"notify")!==false || strpos($action,"undo_approval")!==false) {
				//send email to someone
				$notify = explode("_",$action);
				$notify_user_id = $notify[2];
				$notify_actor = $notify[1];
				//TODO - Write emails to notify actors of scorecards waiting approval

			}
		}

		return $result;
	}



	public function notifyUserCreateProcess($var) {
		$user_role = $var['user_role'];
		$user_id = $var['notify_user'];
		$actor = $var['actor'];
		$object_id = $var['obj_id'];
		$notification_type = $var['notification_type'];

		//Notify user based on role & type
		$x = $this->getUserRecord($user_id,$this->getCmpCode());
		if(isset($x['tkemail']) && strlen($x['tkemail'])>0 && isset($x['tkstatus']) && $x['tkstatus']==1) {
			$message = "Dear ".($x['tkname']." ".$x['tksurname']).chr(10).chr(10);
			$to = array('email'=>$x['tkemail'],'name'=>($x['tkname']." ".$x['tksurname']));
			$from = array('name'=>$this->getUserName(),'email'=>$this->getUserEmail($this->getUserID()));

			switch($notification_type) {
				case "create_approval_reminder":
					$subject = $this->getModTitle()." Scorecard awaiting your approval";
					$message.= $this->getModTitle()." Scorecard ".$this->getRefTag().$object_id." is awaiting your review & approval.".chr(10).chr(10);
					$message.= "To review the Scorecard please log onto Assist, go to ".$this->getModTitle()." > New > ";
					switch($user_role) {
						case "employee": $message.="My Scorecards"; break;
						case "manager": $message.="Staff Scorecards"; break;
						default: $message.=$user_role;break;
//						case "champion": $message.="Departmental Scorecards"; break;
//						case "super":default: $message.="All Scorecards"; break;
					}
					$message.= " and click the Scorecard's Open button.  This will display the complete scorecard with the Approve & Reject form.  Once you've reviewed it, you can choose to either Approve or Reject the scorecard by completing the relevant form.".chr(10).chr(10);
			}

			$message.="Kind Regards".chr(10).$this->getSiteName();

			$emailObject = new ASSIST_EMAIL($to,$subject,$message);
			$emailObject->setSender($from);
			$emailObject->sendEmail();
			$result = array("ok","Notification sent successfully.");
		} else {
			$result = array("error","No notification sent.  Recipient email address could not be found.");
		}
		return $result;

	}

	private function logProcess($scd_id,$actor,$process,$page_action,$result,$reason,$scd_old,$scd_new,$process_old,$process_new) {
		$tbl_field = "scdl_";
		$data = array(
			'scdid'=>$scd_id,
			'insertdate' => date("Y-m-d H:i:s"),
			'insertuser' => $this->getUserID(),
			'insertusername' => $this->getUserName(),
			'role' => $actor,
			'process' => $process,
			'page_action' =>$page_action,
			'result' => $result,
			'comment' => $reason,
			'scd_status_old' => $scd_old,
			'scd_status_new' => $scd_new,
			'process_status_old' => $process_old,
			'process_status_new' => $process_new,
		);
		$insert_data = array();
		foreach($data as $key => $v) {
			$insert_data[$tbl_field.$key] = $v;
		}
		$sql = "INSERT INTO ".$this->getTableName()."_log SET ".$this->convertArrayToSQLForSave($insert_data);
		$this->db_insert($sql);
	}


	public function getCreateProcessLogs($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()."_log WHERE (scdl_page_action = 'create' OR scdl_page_action = 'edit') AND scdl_scdid = ".$object_id." ORDER BY scdl_insertdate DESC, scdl_id DESC";
		$rows = $this->mysql_fetch_all($sql);

		foreach($rows as $key => $row) {
			switch($row['scdl_result']) {
				case "created":
					$activity = "|scorecard| created.";
					break;
				case "confirmed":
					$activity = "|scorecard| marked as completed and sent for approval.";
					break;
				case "undo_confirm":
					$activity = "|scorecard| marked as no longer completed returned for further changes.";
					break;
				case "awaiting_approval":
					$activity = "|scorecard| sent for approval.";
					break;
				case "undo_approval":
					$activity = "|scorecard| previous approval cancelled.";
					break;
				case "manager_rejected":
					$activity = "Manager rejected |scorecard|.  Reason given: ".$row['scdl_comment'];
					break;
				case "manager_digitally_approved":
					$activity = "Manager digitally approved.";
					break;
				case "undo_manager_digitally_approved":
					$activity = "Reversed previous manager digital approval.";
					break;
				case "manager_sign_approved":
					$activity = "Manager approved (signed document).  Approval Date: ".$row['scdl_comment'];
					break;
				case "manager_sign_attached":
					$activity = "Manager approved (signed document attached).  Approval Date: ".$row['scdl_comment'];
					break;
				case "manager_approved_no_poe":
					$activity = "Manager approved (No POE).";
					break;
				case "employee_rejected":
					$activity = "Employee rejected |scorecard|.  Reason given: ".$row['scdl_comment'];
					break;
				case "employee_digitally_approved":
					$activity = "Employee digitally approved.";
					break;
				case "undo_employee_digitally_approved":
					$activity = "Reversed previous employee digital approval.";
					break;
				case "employee_sign_approved":
					$activity = "Employee approved (signed document).  Approval Date: ".$row['scdl_comment'];
					break;
				case "employee_sign_attached":
					$activity = "Employee approved (signed document attached).  Approval Date: ".$row['scdl_comment'];
					break;
				case "employee_approved_no_poe":
					$activity = "Employee approved (No POE).";
					break;
				default:
					$activity = ucfirst(str_replace("_"," ",$row['scdl_result']));
			}
			if(isset($activity)) {
				$rows[$key]['activity'] = $this->replaceAllNames($activity);
			}
		}

		return $rows;
	}


	/*


	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}

	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];

		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}


*/

    /*************************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }

/*	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}


	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}

	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}





	/**
	 * Get assessments still to be activated
	 */
	public function getPendingObjects($limit=false) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") <> ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		if($limit==true) {
			$where.= "
			O.".$this->getTableField()."_insertuser = '".$this->getUserID()."'
			";
		}
		return $this->getSpecificObjects($where);
	}
	public function getActivatedObjects($limit=false) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		if($limit==true) {
			$where.= "
			O.".$this->getTableField()."_insertuser = '".$this->getUserID()."'
			";
		}
		$objects = $this->getSpecificObjects($where,true);
		return $objects;
	}
	private function getSpecificObjects($where,$include_assessment_details=false) {
		//Added assist_status to accommodate display of status & different process handling of non-users #AA-128 JC 1 Apr 2021
		$sql = "SELECT O.* , O.".$this->getIDFieldName()." as obj_id
					, CONCAT(TK.tkname,' ',TK.tksurname) as employee
					, TK.tkid as employee_tkid
					, TK.tkstatus as assist_status
					, CONCAT('".$this->getFullRefTag()."', O.".$this->getIDFieldName().") as ref
					, O.".$this->getTableField()."_bonusscaleid as bonus_scale_id
					, O.".$this->getTableField()."_jobid as job_id
					, CONCAT(CU.tkname,' ',CU.tksurname) as create_username
					, O.".$this->getTableField()."_new_status as new_status
					, O.".$this->getTableField()."_edit_status as edit_status
					, O.".$this->getStatusFieldName()." as status
				FROM ".$this->getTableName()." O
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK
				  ON TK.tkid = O.".$this->getTableField()."_tkid
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep CU
				  ON CU.tkid = O.".$this->getTableField()."_insertuser
				WHERE $where
				ORDER BY TK.tkname, TK.tksurname
				";
		$objects = $this->mysql_fetch_all_by_id($sql,"obj_id");
		$object_keys = array_keys($objects);

		if(count($object_keys) >0 ) {
			//set bonus scale text & strip out IP status if confirmed
			$listObject = new PM6_SETUP_BONUS();
			$bonus_list = $listObject->getListOfAllBonusScales(true);
			$job_id_list = array();
			foreach($objects as $obj_id => $object) {
				$objects[$obj_id]['bonus_scale'] = $bonus_list[$object['bonus_scale_id']];
				$job_id_list[] = $object['job_id'];
				//If confirmed - then strip out IP status & store separately
				$scd_status = $object['status'];
				$new_status = $object['new_status'];
				foreach($this->ip_statuses as $ip) {
					if(($new_status & $ip) == $ip) {
						$objects[$obj_id]['ip_status'] = $ip;
						if(($scd_status & PM6::CONFIRMED)==PM6::CONFIRMED) {
							$objects[$obj_id]['new_status'] -= $ip;
						}
					}
				}
			}

			//populate job titles
			$empObject = new PM6_EMPLOYEE();
			$job_list = $empObject->getAllJobTitlesByJobIDFormattedForSelect($job_id_list);
			if($include_assessment_details) {
				$assessment_details = $this->getAssessmentDetails($object_keys);
			} else {
				$assessment_details = array();
			}
			foreach($objects as $obj_id => $object) {
				$objects[$obj_id]['job'] = isset($job_list[$object['job_id']])?$job_list[$object['job_id']]:$this->getUnspecified();
				if($include_assessment_details) {
					$objects[$obj_id]['assessments'] = isset($assessment_details[$obj_id]) ? $assessment_details[$obj_id] : 0;
				}
			}
			//get line counts
			$lineObj = new PM6_LINE();
			$line_fld = $lineObj->getTableField();
			$line_id_fld = $lineObj->getIDFieldName();
			$line_status_fld = $lineObj->getStatusFieldName();
			$line_parent_fld = $lineObj->getParentFieldName();
			$sql = "SELECT ".$line_parent_fld."
						, ".$line_fld."_srctype
						, count(".$line_id_fld.") as lc
						, IF(".$line_fld."_srcmodref LIKE '%I', 'I', 'O') as mod_type
					FROM `".$lineObj->getTableName()."`
					WHERE ".$line_status_fld." = ".self::ACTIVE."
					AND ".$line_parent_fld." IN (".implode(",",$object_keys).")
					GROUP BY ".$line_parent_fld.", ".$line_fld."_srcmodref, ".$line_fld."_srctype
					ORDER BY ".$line_parent_fld.", ".$line_fld."_srctype
					";
			$counts = $this->mysql_fetch_all($sql);
			foreach($counts as $c) {
				$obj_id = $c[$line_parent_fld];
				$type = $c[$line_fld.'_srctype'];
				$t = $c['lc'];
				$mod_type = $c['mod_type'];
				$objects[$obj_id]['count'][$type][$mod_type] = $t;
			}
		}
		return $objects;
	}


	public function getUsersWithActiveAssessments() {
		$sql = "SELECT DISTINCT ".$this->getTableField()."_tkid as tkid
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$all = $this->mysql_fetch_all_by_value($sql,"tkid");
		return $all;
	}

	public function getAllEmployeesAndJobsWithCurrentScorecards() {
		$sql = "SELECT DISTINCT ".$this->getTableField()."_tkid as tkid, ".$this->getTableField()."_empid as emp_id, ".$this->getTableField()."_jobid as job_id
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$data = $this->mysql_fetch_all_by_id2($sql,"tkid","job_id");
		return $data;
	}

	public function getObjectsForEdit($limit=false) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		if($limit==true) {
			$where.= "
			O.".$this->getTableField()."_insertuser = '".$this->getUserID()."'
			";
		}
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
			$logObject = new PM6_LOG_OBJECT();
			$activation_date = $logObject->getScorecardActivationDate($object_keys);
		} else {
			$assessments = array();
			$activation_date = array();
		}
		foreach($objects as $key => $obj) { //echo isset($activation_date[$key]) && $activation_date[$key]!=0 ? date("d M Y H:i",$activation_date[$key]) : "N/A";
			$objects[$key]['activation_date'] = isset($activation_date[$key]) && $activation_date[$key]!=0 ? date("d M Y H:i",$activation_date[$key]) : "N/A";
			$s = $obj[$this->getStatusFieldName()];
			if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
				if(isset($assessments[$key]) && $assessments[$key]>0) {
					$objects[$key]['can_edit'] = false;
					$objects[$key]['can_reassign'] = false;
					$objects[$key]['can_delete'] = false;
					$objects[$key]['can_deactivate'] = true;
					$objects[$key]['can_restore'] = false;
					$objects[$key]['status'] = "Triggered";
				} else {
					$objects[$key]['can_edit'] = true;
					$objects[$key]['can_reassign'] = true;
					$objects[$key]['can_delete'] = true;
					$objects[$key]['can_deactivate'] = false;
					$objects[$key]['can_restore'] = false;
					$objects[$key]['status'] = "Activated";
				}
				$objects[$key]['can_edit_bonus'] = false;
			} else {
				$objects[$key]['can_edit'] = false;
				$objects[$key]['can_reassign'] = false;
				$objects[$key]['can_delete'] = false;
				$objects[$key]['can_deactivate'] = false;
				$objects[$key]['can_restore'] = false;
				$objects[$key]['can_restore'] = true;
				$objects[$key]['status'] = "Inactive";
				$objects[$key]['can_edit_bonus'] = false;
			}
			//For now, only allow editing of bonus scale this way for SDP18/19 financial year because bonus scale functionality rolled out mid-year
			if($this->getModRef()=="SDP18PM") {
				$objects[$key]['can_edit_bonus'] = true;
			}
		}

		return $objects;
	}


	public function getMyObjects($view_type="mine") {
			$where = "";
			$continue = true;
			if($view_type=="mine") {
					$where.= " O.".$this->getTableField()."_tkid = '".$this->getUserID()."' AND ";
			} elseif(substr($view_type,0,5)=="staff") {
				$empObject = new PM6_EMPLOYEE();
				$staff_details = $empObject->getMyStaffJobs(substr($view_type,strpos($view_type,"_")+1,10));
				if(count($staff_details['staff_job_ids'])>0) {
					$where.= " O.".$this->getTableField()."_jobid IN (".implode(",",$staff_details['staff_job_ids']).") AND ";
				} else {
					$where.= " O.".$this->getTableField()."_jobid = 'No staff to display' AND ";
					$continue = false;
				}
			} else {
				//view_type = all = don't filter
			}
			if($continue) {
				$where.= "
						 (O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
						AND (O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
				$objects = $this->getSpecificObjects($where);
				$object_keys = array_keys($objects);
				if(count($object_keys)>0) {
					$assessments = $this->getAssessmentDetails($object_keys);
				} else {
					$assessments = array();
				}
				foreach($objects as $key => $obj) {
					$s = $obj[$this->getStatusFieldName()];
                    $objects[$key]['status']  = $this->getMyScorecardStatusName($s,isset($assessments[$key])?$assessments[$key]:0);
				}
			} else {
				$objects = array();
			}
		if(isset($staff_details)) {
			$objects = $staff_details+array('objects'=>$objects);
		}
		return $objects;
	}

	public function getMyScorecardStatusName($s,$assessments) {
        if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
            if($assessments>0) {
                return "Triggered";
            } else {
                return  "New";
            }
        } else {
            return "Inactive";
        }
    }


	/**
	 * Get a list of SCDs which are available for individual triggering on Admin > Assessments > Triggers
	 * @param bool $filter_out_grouped_scorecards - Added AA-581 12 April 2021 so that only non-grouped SCDs are displayed
	 * @return array
	 */
	public function getObjectsForTrigger($filter_out_grouped_scorecards=true) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);

		//Added for AA-581 - filter out scorecards allocated to groups to that Assessment > Per Scorecard only allows for triggering of non-grouped scorecards
		if($filter_out_grouped_scorecards && count($object_keys)>0) {
			$groupObject = new PM6_ASSESSMENT_GROUP();
			$grouped_scorecards = $groupObject->getScorecardsAlreadyInAGroup();
			if(count($grouped_scorecards)>0) {
				foreach($grouped_scorecards as $scd_id) {
					unset($objects[$scd_id]);
				}
				$object_keys = array_keys($objects);
			}
		}

		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		foreach($objects as $key => $obj) {
			$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
		}

		return $objects;
	}


	public function getObjectsByGroup($group_id) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys) > 0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		$groupObject = new PM6_ASSESSMENT_GROUP();
		$grouped_scorecards = $groupObject->getScorecardsAlreadyInAGroup($group_id);
		foreach($objects as $key => $obj) {
			if(!in_array($key,$grouped_scorecards)) {
				unset($objects[$key]);
			}
		}

		return $objects;
	}

	public function getObjectsForAssessmentReport() {
		$where = "(O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys) > 0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		$groupObject = new PM6_ASSESSMENT_GROUP();
		$grouped_scorecards = $groupObject->getScorecardsAlreadyInAGroupWithGroupName();
		foreach($objects as $key => $obj) {
			$s = $obj[$this->getStatusFieldName()];
			if((($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
				if(isset($grouped_scorecards[$key])) {
					$objects[$key]['status'] = "Assessed By Group:<br />".$grouped_scorecards[$key];
				} elseif(isset($assessments[$key]) && $assessments[$key] > 0) {
					$objects[$key]['status'] = "Triggered";
				} elseif(($s&self::ACTIVATED)==self::ACTIVATED) {
					$objects[$key]['status'] = "Available";
				} else {
					$objects[$key]['status'] = "New";
				}
			} else {
				$objects[$key]['status'] = "Inactive";
			}
			$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
		}

		return $objects;
	}


    public function getObjectsForReport() {
        $where = "(O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
        $objects = $this->getSpecificObjects($where);
        $object_keys = array_keys($objects);
        if(count($object_keys)>0) {
            $assessments = $this->getAssessmentDetails($object_keys);
        } else {
            $assessments = array();
        }
        foreach($objects as $key => $obj) {
            $s = $obj[$this->getStatusFieldName()];
            if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
                if(isset($assessments[$key]) && $assessments[$key]>0) {
                    $objects[$key]['status'] = "Triggered";
                } else {
                    $objects[$key]['status'] = "New";
                }
            } else {
                $objects[$key]['status'] = "Inactive";
            }
            $objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
        }

        return $objects;
    }


	public function getAObjectSummary($obj_id,$get_assessment_details=true) {
		$where = "
			 (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."";
			$where.= " AND O.".$this->getIDFieldName()." = ".$obj_id;
		$swObject = new PM6_SCORECARD_WEIGHT();
		$weights = $swObject->getComponentWeightForAScorecard($obj_id);
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if($get_assessment_details && count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
			foreach($objects as $key => $obj) {
				$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
			}
		}
		$objects[$obj_id]['weights'] = $weights;
		return $objects[$obj_id];
	}


	public function getObjectsSummary($obj_id,$get_assessment_details=true) {
		//Added to catch blank arrays of IDs where being called by trigger class otherwise the function seems to return all scorecard details which is an unnecessary waste of resources [JC] AA-557 10 March 2021
		if($obj_id===false) {
			return array();
		}
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."";
		if(is_array($obj_id) && count($obj_id)>0) {
			$where.= " AND O.".$this->getIDFieldName()." IN (".implode(",",$obj_id).")";
		} elseif(!is_array($obj_id)) {
			//$where.= " AND O.".$this->getIDFieldName()." = ".$obj_id;
		}
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if($get_assessment_details && count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
			foreach($objects as $key => $obj) {
				$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
			}
		}
		//$objects['ids'] = $obj_id;
		return $objects;
	}

	private function getAssessmentDetails($object_keys) {
			$asmtObj = new PM6_ASSESSMENT();
			$asmt_id_fld = $asmtObj->getIDFieldName();
			$asmt_parent_fld = $asmtObj->getParentFieldName();
			$asmt_status_fld = $asmtObj->getStatusFieldName();
			$sql = "SELECT count(".$asmt_id_fld.") as c, ".$asmt_parent_fld." as obj_id
					FROM ".$asmtObj->getTableName()."
					WHERE ".$asmt_parent_fld." IN (".implode(",",$object_keys).") AND (".$asmt_status_fld." & ".self::ACTIVE.") = ".self::ACTIVE."
					GROUP BY ".$asmt_parent_fld."";
			return $this->mysql_fetch_value_by_id($sql, "obj_id","c");
	}



	public function getEmployee($scorecard_id) {
		$row = $this->getRawObject($scorecard_id);
		$tkid = $row[$this->getTableField().'_tkid'];
		$tkObject = new ASSIST_TK($tkid);
		$name = $tkObject->getObjectUserName();
		return array('id'=>$tkid,'name'=>$name);
	}


	/***
	 * Returns an unformatted array of an object
	 */

	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);

		return $data;
	}
	  /*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}









	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
			//status = activated
		//return $this->getStatusSQL("ALL",$t,false);
		$where = "(".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;

		return $where;
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}



	/****
	 * functions to check on the status of a contract
	 */
	/*public function coreCompInUse() {
		$sql = "SELECT line_srcid, count(line_id) as cc FROM ".$this->getTableName()." WHERE line_srctype = 'CC' GROUP BY line_srcid";
		$data = $this->mysql_fetch_value_by_id($sql, "line_srcid", "cc");
		return $data;
	}*/





    /***
     * SET / UPDATE Functions
     */










































    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /***********************
     * PRIVATE functions: functions only for use within the class
     */



  	/************
	 * Generate a standard heading for the creation of assessments
	 */
	public function getAssessmentCreateHeading($create_step,$obj_id,$page_action="create",$return_raw_data=false) {
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name
				, ".$this->getTableField()."_empid as emp_id
				, ".$this->getTableField()."_jobid as job_id
				, ".$this->getTableField()."_bonusscaleid as bonus_scale
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN ".$this->getTableName()." A ON A.".$this->getTableField()."_tkid = TK.tkid AND A.".$this->getIDFieldName()." = ".$obj_id;
		$employee_details = $this->mysql_fetch_one($sql);
		$empObject = new PM6_EMPLOYEE();
		$job_details = $empObject->getJobDetail($employee_details['job_id']);

		if($employee_details['bonus_scale']>0) {
			$bonusObject = new PM6_SETUP_BONUS();
			$bonus_details = $bonusObject->getBonusScaleByID($employee_details['bonus_scale']);
		} else {
			$bonus_details = false;
		}

		$logObject = new PM6_LOG_OBJECT();
		$activation_details = $logObject->getScorecardActivationDetails($obj_id);
		$activation_date = $activation_details['date'];
		$activation_user = $activation_details['user'];

		$creation_details = $logObject->getScorecardCreationDetails($obj_id);
		$creation_date = $creation_details['date'];
		$creation_user = $creation_details['user'];

		if($page_action=="view") {
			$str = "
			<h2>".$employee_details['name']."</h2>
			<table>
				<tr>
					<td colspan=2 class='sub-heading'>".$this->getObjectName(self::OBJECT_TYPE)." Details:</td>
				</tr>
				<tr>
					<td class=b>Ref:</td>
					<td>".($this->getFullRefTag().$obj_id)."</td>
				</tr>".($bonus_details!==false ?"
				<tr>
					<td class=b>".$this->replaceObjectNames("|bonusscale|").":</td>
					<td>".($bonus_details['name'])."</td>
				</tr>":"")."
				<tr>
					<td class=b>Created By:</td>
					<td>".$creation_user."</td>
				</tr>
				<tr>
					<td class=b>Created On:</td>
					<td>".date("d F Y H:i",($creation_date))."</td>
				</tr>
				<tr>
					<td class=b>Activated By:</td>
					<td>".$activation_user."</td>
				</tr>
				<tr>
					<td class=b>Activated On:</td>
					<td>".($activation_date!="N/A" && $activation_date>0 ?date("d F Y H:i",$activation_date):$activation_date)."</td>
				</tr>
			";
		} else {
			$str = "
			<h2>Step ".$create_step.": ".$this->getStep($create_step)."</h2>
			<table>
				<tr>
					<td class=b>Employee:</td>
					<td>".$employee_details['name']."</td>
				</tr>
				<tr>
					<td colspan=2 class='sub-heading'>".$this->getObjectName(self::OBJECT_TYPE)." Details:</td>
				</tr>
				<tr>
					<td class=b>Ref:</td>
					<td>".($this->getFullRefTag().$obj_id)."</td>
				</tr>".($bonus_details!==false ?"
				<tr>
					<td class=b>".$this->replaceObjectNames("|bonusscale|").":</td>
					<td>".($bonus_details['name'])."</td>
				</tr>":"")."
				<tr>
					<td class=b>Created By:</td>
					<td>".$creation_user."</td>
				</tr>
				<tr>
					<td class=b>Created On:</td>
					<td>".date("d F Y H:i",($creation_date))."</td>
				</tr>

				";
		}
		$job_start = $job_details['job_start'];
		if(strlen($job_start)==0 || strtotime($job_start)==0 || date("d F Y",strtotime($job_start))=="01 January 1970") {
			$job_start = $this->getUnspecified();
		} else {
			$job_start = date("d F Y",strtotime($job_start));
		}
		$manager = "";
		if(isset($job_details['job_manager_name']) && strlen($job_details['job_manager_name'])>0) {
			$manager.=$job_details['job_manager_name'].' '.$job_details['job_manager_surname'];
			$job_details['job_manager_name'] = $manager;
			unset($job_details['job_manager_surname']);
		}
		if(isset($job_details['job_manager_title']) && strlen($job_details['job_manager_title'])>0) {
			$manager.=(strlen($manager)>0?" / ":"").$job_details['job_manager_title'];
		}
		$str.="
				<tr>
					<td colspan=2 class='sub-heading'>Employee Job Details:</td>
				</tr>
				<tr>
					<td class=b>Job Start Date:</td>
					<td>".$job_start."</td>
				</tr>
				<tr>
					<td class=b>Department:</td>
					<td>".$job_details['job_dept']."</td>
				</tr>
				<tr>
					<td class=b>Job Title:</td>
					<td>".$job_details['job_title']."</td>
				</tr>
				<tr>
					<td class=b>Manager:</td>
					<td>".$manager."</td>
				</tr>
			</table>
		";
		if($return_raw_data===true) {
			return array('echo'=>$str,'data'=>array('job'=>$job_details));
		} else {
			return $str;
		}
	}

	public function getAssessmentScoreHeading($obj_id,$assess_id=false,$trgr_id=false,$return_display_only=true,$time_periods=false) {
		//Added assist_status to accommodate display of status & different process handling of non-users #AA-128 JC 1 Apr 2021
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name
				, TK.tkstatus as assist_status
				, ".$this->getTableField()."_empid as emp_id
				, ".$this->getTableField()."_jobid as job_id
				, ".$this->getTableField()."_bonusscaleid as bonus_scale
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN ".$this->getTableName()." A ON A.".$this->getTableField()."_tkid = TK.tkid AND A.".$this->getIDFieldName()." = ".$obj_id;
		$employee_details = $this->mysql_fetch_one($sql);
		$employee_details['assist_status_in_words'] = $this->getAssistStatusInWords($employee_details['assist_status']); //AA-128

		$empObject = new PM6_EMPLOYEE();
		$job_details = $empObject->getJobDetail($employee_details['job_id']);

		if($employee_details['bonus_scale']>0) {
			$bonusObject = new PM6_SETUP_BONUS();
			$bonus_details = $bonusObject->getBonusScaleByID($employee_details['bonus_scale']);
		} else {
			$bonus_details = false;
		}

		$logObject = new PM6_LOG_OBJECT();
		$activation_details = $logObject->getScorecardActivationDetails($obj_id);
		$activation_date = $activation_details['date'];
		$activation_user = $activation_details['user'];

		$creation_details = $logObject->getScorecardCreationDetails($obj_id);
		$creation_date = $creation_details['date'];
		$creation_user = $creation_details['user'];

		$show_period_under_assessment = false;
		if($time_periods!==false) {
			$show_period_under_assessment = true;
			$tp = array_keys($time_periods);
			$time_name = $time_periods[$tp[0]]." - ".$time_periods[$tp[count($tp)-1]];
		}
			$str = "
			<h2>".$employee_details['name'].($show_period_under_assessment?"&nbsp;".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER."&nbsp;".$time_name."":"")."</h2>
			<table>
				<tr>
					<td colspan=2 class='sub-heading'>".$this->getObjectName(self::OBJECT_TYPE)." Details:</td>
				</tr>
				<tr>
					<td class=b>Ref:</td>
					<td>".($this->getFullRefTag().$obj_id)."</td>
				</tr>".($bonus_details!==false ?"
				<tr>
					<td class=b>".$this->replaceObjectNames("|bonusscale|").":</td>
					<td>".($bonus_details['name'])."</td>
				</tr>":"")."
				<tr>
					<td class=b>Created By:</td>
					<td>".$creation_user."</td>
				</tr>
				<tr>
					<td class=b>Created On:</td>
					<td>".date("d F Y H:i",($creation_date))."</td>
				</tr>
				<tr>
					<td class=b>Activated By:</td>
					<td>".$activation_user."</td>
				</tr>
				<tr>
					<td class=b>Activated On:</td>
					<td>".($activation_date!="N/A" && $activation_date>0 ?date("d F Y H:i",$activation_date):$activation_date)."</td>
				</tr>
			";
/**
 * Added Employee Details section to give a place to add the Assist User Status field #AA-128 JC 1 Apr 2021
 * 		This info is needed so that the user can tell what process the assessment will follow once triggered - non-users will follow a different path
 */
		$str.="
				<tr>
					<td colspan=2 class='sub-heading'>Employee Details:</td>
				</tr>
				<tr>
					<td class=b>Name:</td>
					<td>".$employee_details['name']."</td>
				</tr>
				<tr>
					<td class=b>Assist Status:</td>
					<td>".$employee_details['assist_status_in_words']."</td>
				</tr>
		";
		$str.="
				<tr>
					<td colspan=2 class='sub-heading'>Employee Job Details:</td>
				</tr>
				<tr>
					<td class=b>Department:</td>
					<td>".$job_details['job_dept']."</td>
				</tr>
				<tr>
					<td class=b>Job Title:</td>
					<td>".$job_details['job_title']."</td>
				</tr>
				<tr>
					<td class=b>Manager:</td>
					<td>".$job_details['job_manager_title']."</td>
				</tr>
		";
		if($assess_id!==false) {
			$assessObj = new PM6_ASSESSMENT();
			$assessment = $assessObj->getSingleAssessmentDetails($assess_id);
			$str.="
				<tr>
					<td colspan=2 class='sub-heading'>Assessment Details:</td>
				</tr>
				<tr>
					<td class=b>Ref:</td>
					<td>".$assessObj->getRefTag().$assess_id."</td>
				</tr>
				<tr>
					<td class=b>Period:</td>
					<td>".$assessment['period_name']."</td>
				</tr>
			";
			if($trgr_id!==false) {
				$triggerObj = new PM6_TRIGGER();
				$trigger = $triggerObj->getTriggerDetails($trgr_id);
					$str.="
					<tr>
						<td class=b>Trigger Type:</td>
						<td>".$trigger['type']."</td>
					</tr>
					<tr>
						<td class=b>Trigger Status:</td>
						<td>".($trigger['status_icon']!==false?ASSIST_HELPER::getDisplayIconAsDiv($trigger['status_icon']):"")." ".$trigger['status']."</td>
					</tr>
					<tr>
						<td class=b>Trigger Deadline:</td>
						<td>".$trigger['deadline']."</td>
					</tr>
					<tr>
						<td class=b>Trigger Date Completed:</td>
						<td>".$trigger['datecompleted']."</td>
					</tr>
				";
			}
		}
		$str.="
			</table>
			";
		if($return_display_only){
			return $str;
		} else {
			//Added assist_status to accommodate display of status & different process handling of non-users #AA-128 JC 1 Apr 2021 [preparation for AA-570]
			return array('display'=>$str,'trigger'=>isset($trigger)?$trigger:false,'assist_status'=>$employee_details['assist_status']);
		}
	}














	private function logMyAction($id,$action,$old,$new,$fld="") {
		$logObj = new PM6_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "CONFIRM":
				$action = $logObj->getConfirmLogAction();
				$display = "Confirmed |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "UNDOCONFIRM":
				$action = $logObj->getUndoConfirmLogAction();
				$display = "Undo Confirmation of |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "APPROVE":
				$action = $logObj->getApproveLogAction();
				$display = $fld." approved |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "UNDOAPPROVE":
				$action = $logObj->getUndoApproveLogAction();
				$display = "Undo Approval of |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REJECT":
				$action = $logObj->getRejectLogAction();
				$display = $fld." rejected |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "ACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Activated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REVERSEACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Reversed previous activation of |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "RESTORE":
				$action = $logObj->getRestoreLogAction();
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$action = $logObj->getDeactivateLogAction();
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$action = $logObj->getEditLogAction();
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;
			case "EDIT_BONUS":
				$action = $logObj->getEditLogAction();
				$display = "Edited |".$this->getMyObjectType()."| ".$this->getRefTag().$id.".  Changed |".PM6_SETUP_BONUS::OBJECT_TYPE."| to '".$new[$fld]."' from '".$old[$fld]."'";
				break;
		}
		$logObj->addObject(
			array(),
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			$id
		);

	}

	public function determineLastActivationStatusUpdateDate($scd_id){
        $logObj = new PM6_LOG_OBJECT();
        $creation = $logObj->getScorecardLogDetails($scd_id, 'C');

        $activation_dates = array();

        if(isset($creation) && is_array($creation) && count($creation) > 0){
            $activation_dates[] = date("Y-m-d H:i:s", $creation['date']);;;
        }

        $activations = $logObj->getScorecardLogDetails($scd_id, 'V');

        if(isset($creation) && is_array($creation) && count($creation) > 0){
            $activation_dates[] = date("Y-m-d H:i:s", $activations['date']);;
        }

        if(count($activation_dates) > 0){
            usort($activation_dates, array('PM6', 'date_sort'));
            krsort($activation_dates);
            $activation_date = $activation_dates[0];

        }else{
            $activation_date = 'N/A';
        }

        return $activation_date;
    }










	public function isEmployeeActiveUser($tkid) {
		return $this->isUserActiveUser($tkid);
	}
	public function isManagerActiveUser($tkid) {
		return $this->isUserActiveUser($tkid);
	}
	public function isUserActiveUser($tkid) {
		$sql = "SELECT tkstatus FROM ".$this->getUserTableName()." WHERE tkid = '$tkid'";
		$row = $this->mysql_fetch_one($sql);
		if($row['tkstatus']==1) {
			return true;
		}
		return false;
	}


	public function isEmployeeModuleUser($tkid) {
		return $this->isUserModuleUser($tkid);
	}
	public function isManagerModuleUser($tkid) {
		return $this->isUserModuleUser($tkid);
	}
	public function isUserModuleUser($tkid) {
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_menu_modules_users WHERE usrtkid = '$tkid' AND usrmodref = '".$this->getModRef()."'";
		$rows = $this->mysql_fetch_all($sql);
		if(count($rows)>0) {
			return true;
		}
		return false;
	}






	public function isMyOwnScorecardWaitingMyReview() {
		$tkid = $this->getUserID();
		//TODO - isMyOwnScorecardWaitingMyReview - still TBP AA-404
		$return = false;
		return $return;
	}

	public function areAnyScorecardsWaitingMyReview() {
		$tkid = $this->getUserID();
		//TODO - areAnyScorecardsWaitingMyReview - still TBP AA-404
		$return = false;
		return $return;
	}

	public function getAllCurrentScorecardEmployeesAndJobsWithStatus($user_id=false,$emp_id=false,$include_log_info=false) {
		$user_filter = "";
		if($user_id!==false) {
			if(is_array($user_id) && count($user_id)>0) {
				$user_filter = $this->getOwnerFieldName()." IN ('".implode("','",$user_id)."') AND ";
			} elseif(!is_array($user_id)) {
				$user_filter = $this->getOwnerFieldName()." = '".$user_id."' AND ";
			}
		}
		$emp_filter = "";
		if($emp_id!==false) {
			if(is_array($emp_id) && count($emp_id)>0) {
				$emp_filter = $this->getParentFieldName()." IN ('".implode("','",$emp_id)."') AND ";
			} elseif(!is_array($user_id)) {
				$emp_filter = $this->getParentFieldName()." = '".$emp_id."' AND ";
			}
		}
		if($include_log_info) {
			$sql = "SELECT * FROM ".$this->getTableName()."_log ORDER BY scdl_scdid ASC, scdl_result ASC, scdl_insertdate DESC";
			$rows = $this->mysql_fetch_all($sql);
			$logs = array();
			foreach($rows as $row) {
				$scd = $row['scdl_scdid'];
				$result = $row['scdl_result'];
				if(!isset($logs[$scd][$result])) {
					$logs[$scd][$result] = array(
						'scd_id'=>$scd,
						'date'=>$row['scdl_insertdate'],
						'user'=>$row['scdl_insertusername'],
						'user_id'=>$row['scdl_insertuser'],
						'role'=>$row['scdl_role'],
						'result'=>$result,
						'process'=>$row['scdl_process'],
						'page_action'=>$row['scdl_page_action'],
						'comment'=>$row['scdl_comment'],
					);
				}
			}
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getStatusFieldName()." as status
				, ".$this->getTableField()."_new_status as new_status
				, ".$this->getTableField()."_edit_status as edit_status
				, ".$this->getTableField()."_tkid as tkid
				, ".$this->getTableField()."_empid as emp_id
				, ".$this->getTableField()."_jobid as job_id
				, ".$this->getTableField()."_bonusscaleid as bonus_scale_id
				, CONCAT(tkname,' ',tksurname) as created_by
				, ".$this->getTableField()."_insertdate as created_on
				FROM ".$this->getTableName()."
				LEFT OUTER JOIN ".$this->getUserTableName()."
				  ON ".$this->getTableField()."_insertuser = tkid
				WHERE ".$user_filter."
				".$emp_filter."
				(".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$data = $this->mysql_fetch_all_by_id2($sql,"tkid","job_id");
		//If confirmed - then strip out IP status & store separately
		foreach($data as $tkid => $jobs) {
			foreach($jobs as $job_id => $job) {
				$scd_status = $job['status'];
				$new_status = $job['new_status'];
				$is_employee_approved = false;
				if($this->validateStatus($new_status,PM6::EMPLOYEE_DIGITALLY_APPROVED)
					|| $this->validateStatus($new_status,PM6::EMPLOYEE_SIGN_APPROVED)
					|| $this->validateStatus($new_status,PM6::EMPLOYEE_SIGN_ATTACHED)
					|| $this->validateStatus($new_status,PM6::EMPLOYEE_APPROVED_NO_POE)
				) {
					$is_employee_approved = true;
				}

				$is_manager_approved = false;
				if($this->validateStatus($new_status,PM6::MANAGER_DIGITALLY_APPROVED)
					|| $this->validateStatus($new_status,PM6::MANAGER_SIGN_APPROVED)
					|| $this->validateStatus($new_status,PM6::MANAGER_SIGN_ATTACHED)
					|| $this->validateStatus($new_status,PM6::MANAGER_APPROVED_NO_POE)
				) {
					$is_manager_approved = true;
				}

				$is_confirmed = false;
				if($this->validateStatus($scd_status,PM6::CONFIRMED)) {
					$is_confirmed = true;
				}

				$is_activated = false;
				if($this->validateStatus($scd_status,PM6::ACTIVATED)) {
					$is_activated = true;
				}

				$scd_id = $job['id'];
				$data[$tkid][$job_id]['is_employee_approved'] = $is_employee_approved;
				$data[$tkid][$job_id]['is_manager_approved'] = $is_manager_approved;
				$data[$tkid][$job_id]['is_confirmed'] = $is_confirmed;
				$data[$tkid][$job_id]['is_activated'] = $is_activated;
				$data[$tkid][$job_id]['logs'] = isset($logs[$scd_id]) ? $logs[$scd_id] : array();
				foreach($this->ip_statuses as $ip) {
					if(($new_status & $ip) == $ip) {
						$data[$tkid][$job_id]['ip_status'] = $ip;
						if(($scd_status & PM6::CONFIRMED)==PM6::CONFIRMED) {
							$data[$tkid][$job_id]['new_status'] -= $ip;
						}
					}
				}
			}
		}

		return $data;
	}




}


?>