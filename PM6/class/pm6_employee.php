<?php
/**
 * To manage the link between EMPLOYEE Assist and PM6
 * Glossary explanations added on 1 April 2021 #AA-238 JC
 *
 * Created on: 1 January 2016
 * Authors: Janet Currie
 *
 */

class PM6_EMPLOYEE extends PM6 {

	private $employee_modref = "EMP";
	private $employee_modref_confirmed = false;
	private $user_ids_already_processed = array();//used for any instances where multiple levels of org hierarchy are processed through - use this to check for user_ids already processed to prevent infinite loops where managers are staff are managers
	private $loops = 0;

	private $fields_glossary = array();

    public function __construct() {
		parent::__construct();
		//Add user access glossary explanations #AA-238 JC 1 April 2021
		$this->fields_glossary = array(
			'create_own' => "User has access to build their own |scorecard| in ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_new_my"),"|")).".",
			'create_users' => "User has access to build the |scorecard| of any employee which they manage in ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_new_staff"),"|"))." where that employee is an Assist User.",
			'create_non' => "User has access to build the |scorecard| of any employee which they manage in ".$this->replaceAllNames($this->createPageTitleBreadcrumb($this->createMenuTrailFromLastLink("menu_new_staff"),"|"))." where that employee is not an Assist User.",
		);
		$this->fields_glossary = $this->replaceAllNames($this->fields_glossary);

		//Set the Employee Assist modref - to catch clients with EMP1 instead of EMP as the modref #AA-644 JC 9 July 2021
		$is_there_a_session_variable_about_employee_modref = isset($_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']);

		if($is_there_a_session_variable_about_employee_modref) {
			$this->employee_modref_confirmed = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'];
			$this->employee_modref = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'];
		} else {
			//Look in the menu table for EMP & EMP1 and get the oldest record
			$sql = "SELECT * FROM assist_menu_modules WHERE modref IN ('EMP','EMP1') AND modyn = 'Y' ORDER BY modid DESC";
			$row = $this->mysql_fetch_one($sql);
			if(isset($row['modref'])) {
				$this->employee_modref = $row['modref'];
				$this->employee_modref_confirmed = true;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'] = $this->employee_modref;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'] = $this->employee_modref_confirmed;
			}
		}

    }

	public function getFieldGlossary() { return $this->fields_glossary; }

	/**
	 * Get list of available employees
	 */
	public function getAvailableEmployees($assist_users=true) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array(
				'employees'=>array(),
				'job_titles'=>array(),
				'departments'=>array(),
				'managers'=>array(),
			);
		}
		$local_modref = $this->getModRef();
		$data = array();
		//get list of employees
		$empObj = new EMP1_INTERNAL($this->employee_modref);
		$all_employees = $empObj->getCurrentEmployeesWithStatusAndCurrentJobDetails();
		$job_titles = $empObj->getAllJobTitlesFormattedForSelect();
		$departments = $empObj->getAllDepartmentsFormattedForSelect();
		$employees = array();
		$managers = array();
		foreach($all_employees as $tkid => $employee) {
			if(($employee['is_system_user'] && $assist_users===true) || (!$employee['is_system_user'] && $assist_users!==true)) {
				$employees[$tkid] = $employee;
			}
			if(!isset($managers[$employee['job_title']])) {
				$managers[$employee['job_title']] = array($tkid=>$employee['emp_name']);
			} else {
				$managers[$employee['job_title']][$tkid] = $employee['emp_name'];
			}
		}

		$data = array(
			'employees'=>$employees,
			'job_titles'=>$job_titles,
			'departments'=>$departments,
			'managers'=>$managers,
		);
		$this->setDBRef($local_modref);
		return $data;
	}


	/**
	 * Get list of employees who can have scorecards created
	 */
	public function getAvailableEmployeesForCreate($start_date, $end_date) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		$data = array();
		//get list of current scorecards which are not deactivated - either active or pending
		$scdObject = new PM6_SCORECARD();
		$active_scorecards = $scdObject->getAllEmployeesAndJobsWithCurrentScorecards();
		//get list of active users with menu access
		$userObj = new PM6_USERACCESS();
		$users = $userObj->getMenuUsersFormattedForSelect();
		//get list of non-system employees && active user employees with menu access who aren't in the above list
		$empObj = new EMP1_INTERNAL($this->employee_modref);
		$employees = $empObj->getAllCurrentEmployeesWithStatusAndJobs();

		//loop through employees and
		foreach($employees as $tkid => $jobs) {
			//double check needed as filter might return blank array
			if(count($jobs)>0) {
				$refs = array_keys($jobs);
				$raw_data = $jobs[$refs[0]];
				$jobs = $empObj->filterJobsForSpecificFinancialYear($jobs, $start_date, $end_date);
			}
			if(count($jobs)>0) {
				foreach($jobs as $key => $job_detail) {
					//remove any users with active scorecards
					if(isset($active_scorecards[$tkid][$key])) {
						//ignore job
					} else {
						//remove any active users without menu access
						if($job_detail['is_system_user'] == 1 && !isset($users[$tkid])) {
							//ignore user
						} else {
							$job_id = $job_detail['job_id'];
							if(isset($data[$tkid])) {
								$data[$tkid]['job'][] = $job_id;
							} else {
								$data[$tkid] = array(
									'tkid' => $tkid,
									'emp_id' => $job_detail['emp_id'],
									'name' => $job_detail['emp_name'],
									'is_system_user' => $job_detail['is_system_user'],
									'job' => array($job_id),
								);
							}
						}
					}
				}
			} else {
				$data[$tkid] = array(
					'tkid' => $tkid,
					'emp_id' => $raw_data['emp_id'],
					'name' => $raw_data['emp_name'],
					'is_system_user' => $raw_data['is_system_user'],
					'job' => false,
				);
			}
		}
		$this->setDBRef($local_modref);
/*
		echo "<h3 class=red>active scorecards</h3>";
		ASSIST_HELPER::arrPrint($active_scorecards);
		echo "<h3 class=red>users</h3>";
		ASSIST_HELPER::arrPrint($users);
		echo "<h3 class=red>employees</h3>";
		ASSIST_HELPER::arrPrint($employees);
*/
		return $data;
	}





	public function getJobsFormattedForSelect($var) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		$job_id = explode("|",$var['job_id']);
		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getListOfJobs($job_id);

		$this->setDBRef($local_modref);

		return $data;
	}





	public function getJobDetail($job_id) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getJobDetail($job_id);

		$this->setDBRef($local_modref);

		return $data;
	}

	public function getJobsForFinancialYear($start_date,$end_date,$tkid=false) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		if($tkid===false) {
			$tkid = $this->getUserID();
		}

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getJobsForFinancialYearByUserID($start_date,$end_date,$tkid);

		$this->setDBRef($local_modref);

		return $data;
	}


	public function getDirectStaffDetailsByUserID($tkid=false,$start_date=false,$end_date=false,$active_users=true,$non_users=true) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		if($tkid===false) {
			$tkid = $this->getUserID();
		}
		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getDirectStaffDetailsByUserID($tkid,$start_date,$end_date,$active_users,$non_users);

		$this->setDBRef($local_modref);

		return $data;
	}

	public function getAllStaffDetailsForReport($start_date,$end_date) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		//get data
		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$raw_data = $empObject->getAllStaffWithJobsForCurrentFinancialYear($start_date,$end_date);

		//change sort order for display
		$final_data = array('jobs'=>array(),'employee_tkids'=>array());
		foreach($raw_data as $key => $row) {
			$key = $this->formatTextForSorting($row['user_name'].":".$row['job_start'].":".$row['job_id']);
			$final_data['jobs'][$key] = $row;
			$final_data['employee_tkids'][$row['user_id']] = $row['user_id'];
		}
		ksort($final_data['jobs']);
		$this->setDBRef($local_modref);
		return $final_data;
	}
	public function getBelowStaffDetailsByUserID($tkid=false,$start_date=false,$end_date=false,$active_users=true,$non_users=true) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		$this->loops++;

		if($tkid===false) {
			$tkid = $this->getUserID();
		}

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$new_data = $empObject->getDirectStaffDetailsByUserID($tkid,$start_date,$end_date,$active_users,$non_users);

		if(!in_array($tkid,$this->user_ids_already_processed)) {
			$this->user_ids_already_processed[] = $tkid;
		}
		foreach($new_data as $key => $d) {
			$data[$key][$tkid] = $d;
		}
		if($this->loops>1000) {
			$data['too_many_loops'] = true;
		} else {
			$current_tkid = array_keys($new_data['employee_tkids']);
			foreach($current_tkid as $new_tkid) {
				if(!in_array($new_tkid,$this->user_ids_already_processed)) {
					$this->user_ids_already_processed[] = $new_tkid;
					$new_data = $this->getBelowStaffDetailsByUserID($new_tkid,$start_date,$end_date,$active_users,$non_users);
					foreach($new_data as $key => $d) {
						$data[$key][$new_tkid] = $d[$new_tkid];
					}
				}
			}
		}
		$this->setDBRef($local_modref);
		return $data;
	}



	public function getMyStaffJobs($type) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getMyStaffJobs($type);

		$this->setDBRef($local_modref);

		return $data;
	}

	public function getAllJobTitlesByJobIDFormattedForSelect($job_id_list=array()) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		//$data = $empObject->getAllJobTitlesFormattedForSelect();
		$data = $empObject->getListOfJobs($job_id_list);
		$this->setDBRef($local_modref);

		return $data;
	}



	private function emp1GetHeadingsByField($fields=array()){
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		$this->setDBRef($this->employee_modref);
		$sql = "SELECT *, IF(LENGTH(h_client)>0,h_client,h_default) as name 
				FROM ".$this->getDBRef()."_setup_headings 
				WHERE h_field IN ('".implode("','",$fields)."')
				AND (h_status & ".EMP1_HEADINGS::ACTIVE.") = ".EMP1_HEADINGS::ACTIVE;
		$this->setDBRef($local_modref);

		$data = $this->mysql_fetch_all_by_id($sql, "h_field");

		return $data;
	}


	public function getHeadings($fields=array()) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$data = array();
		$data = $this->emp1GetHeadingsByField($fields);
		return $data;
	}


	public function getListItems($fld) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		//if type = manager then get from employee list else get from list
		if($fld=="job_manager") {
			$empObject = new EMP1_EMPLOYEE(0,$this->employee_modref);
			$jobObject = new EMP1_JOB(0,$this->employee_modref);
			$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name, TK.tkid as id
						FROM ".$empObject->getTableName()." E
						INNER JOIN ".$this->getUserTableName()." TK
						ON TK.tkid = E.emp_tkid
						WHERE TK.tkstatus = 1
						AND ".$empObject->getActiveStatusSQL("E")."
						AND (TK.tkid IN (SELECT job_manager FROM ".$jobObject->getTableName()." J WHERE ".$jobObject->getActiveStatusSQL("J")."))
						ORDER BY TK.tkname, TK.tksurname";
			$data = $this->mysql_fetch_value_by_id($sql,"id","name");
		} else {
			$listObject = new EMP1_LIST($fld,$this->employee_modref);
			$data = $listObject->getActiveListItemsFormattedForSelect();
			asort($data);
		}
		$this->setDBRef($local_modref);
		return $data;
	}


	public function getJobsFilteredByGroup($group_type,$group_item) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
		$local_modref = $this->getModRef();
		if($group_type=="CUSTOM") {
			$filter = "";
		} else {
			$filter = $group_type." = ".$group_item;
		}
		$start_date = $this->getFinancialYearStartDate();
		$end_date = $this->getFinancialYearEndDate();

//		$empObject = new EMP1_EMPLOYEE(0,$this->employee_modref);
		$jobObject = new EMP1_JOB(0,$this->employee_modref);
		$intObject = new EMP1_INTERNAL($this->employee_modref);

		$details = $jobObject->getJobFullDetails($filter,true);

		$data = $intObject->filterJobsForSpecificFinancialYear($details,$start_date,$end_date);

		$this->setDBRef($local_modref);
		return $data;
	}

	/*
	 * TSHEGO REPORT CODE STARTS HERE*/

    public function getAllJobTitlesFormattedForSelect() {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllJobTitlesFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getAllDepartmentsFormattedForSelect() {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllDepartmentsFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getAllJobLevelsFormattedForSelect() {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllJobLevelsFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getEmployee($emp_id) {
		//Catch faulty Employee Assist module links #AA-644 JC 9 July 2021
		if($this->employee_modref_confirmed!==true) {
			return array();
		}
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getEmployee($emp_id);

        $this->setDBRef($local_modref);

        return $data;
    }




	public function __destruct() {
		parent::__destruct();
	}

}


?>