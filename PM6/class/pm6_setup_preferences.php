<?php

class PM6_SETUP_PREFERENCES extends PM6 {

	private $questions = array();

	private $table_name = "_setup_preferences";
	private $field_prefix = "pref";

	private $fields = array(
		'pref_id'=>"id",
		'pref_code'=>"code",
		'pref_name'=>"name",
		'pref_description'=>"description",
		'pref_type'=>"type",
		'pref_value'=>"value",
		'pref_default'=>"default_value",
		'pref_options'=>"options",
		'pref_sort'=>"sort",
		'pref_status'=>"status",
	);
	private $field_names = array();

	const LOG_TABLE = "setup";

	public function __construct() {
		parent::__construct();
		$this->questions = $this->getQuestionsFromDB();
		$this->prepQuestionsToGiveAnswers();
		$this->field_names = array_flip($this->fields);
	}




	/********************
	 * CONTROLLER Functions
	 */
	public function updateObject($var,$attach=array()) {
		unset($attach);	//remove unnecessary array from the inc_controller
		$questions = $this->getQuestions();
		$mar = 0;
		$r_err = array();
		$r_ok = array();
		$err = false;
		$changes = array();
		foreach($questions as $key => $q) {
			$new = $var['Q_'.$key];

			$old = $q['value'];
			if($new != $old) {
				//update database
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->field_names['value']." = '".$new."' WHERE ".$this->field_names['id']." = ".$key;
				$mar = $this->db_update($sql);
				if($mar>0) {
					$changes[$key] = array('to'=>"",'from'=>"");
					if(is_array($q['options'])) {
						$opt = $q['options'];
						$changes[$key]['to'] = $opt[$new];
						$changes[$key]['from'] = $opt[$old];
					} else {

					$options = explode("|",$q['options']);
					foreach($options as $opt) {
						$opts = explode("_",$opt);
						if($new==$opts[0]) {
							$changes[$key]['to'] = $opts[1];
						} elseif($old==$opts[0]) {
							$changes[$key]['from'] = $opts[1];
						}
						if($changes[$key]['to']!="" && $changes[$key]['from']!="") { break; }
					}
					}
					$r_ok[] = $key;
				} else {
					$r_err[] = $key;
					$err = true;
				}
			}
		}
		if(count($changes)>0) {
			$changes['user']=$this->getUserName();

			$log_var = array(
				'section'	=> "PREF",
				'object_id'	=> "",
				'changes'	=> $changes,
				'log_type'	=> PM6_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			if(!$err) {
				return array("ok","Changes saved successfully.");
			} elseif(count($r_ok)==0) {
				return array("error","An error occurred while trying to save changes.");
			} elseif(count($r_err)>0) {
				$return = array("okerror",count($r_ok)." changes saved successfully, but the following changes did not save: PREF".implode(", PREF",$r_err).".");
				return $return;
			} else {
				return array("okerror","An unknown error occurred.  Please review the page and confirm that your changes have been saved.");
			}
		} elseif($err) {
			return array("error","An error occurred while trying to save changes.");
		}
		return array("info","No change found to be saved.");
	}






	private function prepQuestionsToGiveAnswers() {
		$questions = $this->questions;
		foreach($questions as $q_id => $q) {
			$this->answers_by_id[$q_id] = $q['value'];
			$this->answers_by_code[strtoupper($q['code'])] = $q['value'];
		}
	}





	/***************************
	 * GET functions
	 */
	public function getTableName() { return $this->getDBRef().$this->table_name; }
	public function getTableFieldPrefix() { return $this->field_prefix; }
	/**
	 * Returns the list of questions to be displayed on the Setup > Preferences page
	 */
	public function getQuestions() {
		return $this->questions;
	}
	public function getAnswerToQuestion($needle) {
		if($this->checkIntRef($needle)) {
			return $this->getAnswerToQuestionByID($needle);
		} else {
			return $this->getAnswerToQuestionByCode($needle);
		}
	}
	public function getAnswerToQuestionByCode($code) {
		return $this->answers_by_code[strtoupper($code)];
	}
	public function getAnswerToQuestionByID($id) {
		return $this->answers_by_id[$id];
	}

	/**
	 * Returns the questions from the database
	 */
	public function getQuestionsFromDB() {
		$sql = "SELECT ";
		$f = array();
		foreach($this->fields as $fld => $key) {
			$f[] = $fld." as ".$key;
		}
		$sql.=implode(", ",$f)." FROM ".$this->getTableName()." WHERE pref_status & ".PM6::ACTIVE." = ".PM6::ACTIVE." ORDER BY pref_sort";
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		foreach($rows as $q_id => $q) {
			if($q['type'] == "LIST") {
				$options = explode("|", $q['options']);
				$rows[$q_id]['options'] = array();
				foreach($options as $opt) {
					$opt2 = explode("_", $opt);
					if(count($opt2) == 1) {
						$rows[$q_id]['options'][$opt2[0]] = $opt2[0];
					} elseif(count($opt2) == 2) {
						$rows[$q_id]['options'][$opt2[0]] = $opt2[1];
					}
				}
			} elseif($q['type'] == "NUMLIST") {
				$options = explode(";", $q['options']);
				$rows[$q_id]['options'] = array();
				$start = $options[0];
				$end = $options[1];
				$inc = $options[2];
				for($i = $start; $i <= $end; $i += $inc) {
					$rows[$q_id]['options'][$i] = $i;
				}
				$rows[$q_id]['type'] = "LIST";
			}
		}
		return $rows;
	}

	public function getQuestionForLogs()  {
		$data = array();
		foreach($this->questions as $key => $q) {
			$data[$key] = $q['name'];
		}
		return $data;
	}




	/******************
	 * SET functions
	 */
	private function updateQuestionsWithAnswersFromDB() {
		$questions = $this->getQuestions();
		foreach($questions as $key => $q) {

		}
	}









	public function __destruct() {
		parent::__destruct();
	}
}




?>