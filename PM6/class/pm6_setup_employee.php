<?php

class PM6_SETUP_EMPLOYEE extends PM6 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_empid";
	protected $name_field = "_tkid";
	/*
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/

	protected $titles = array();

	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "ESETTING";
	const TABLE = "setup_employee";
	const TABLE_FLD = "es";
	const REFTAG = "ES";
	const LOG_TABLE = "setup";


	public function __construct() {
		parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->titles = array(
			'own' => "Own Scorecard",
			'users' => "Staff Scorecards (Assist Users)",
			'non' => "Staff Scorecards (Non-Assist Users)",
		);
	}

	public function getDefaultSettings() {
		$default = array(
			'bonus_scale' => 0,
			'create' => array(
				'own' => false,
				'users' => false,
				'non' => false,
			),
			'manage' => array(
				'own' => false,
				'users' => false,
				'non' => false,
			),
		);
		return $default;
	}

	/*************************************
	 * CONTROLLER functions
	 */
	public function saveObject($var) {
		$bonusObject = new PM6_SETUP_BONUS();
		$bonus_scales = $bonusObject->getListOfActiveBonusScales();

		$emp_id = $var['emp_id'];
		$tkid = $var['tkid'];
		$bonus_scale = isset($var['bonus_scale']) ? $var['bonus_scale'] : 0;
		$original = $this->getRawObject($emp_id);
		$settings = $this->getDefaultSettings();
		$insert_data = array(
			'es_create' => 0,
			'es_manage' => 0,
			'es_bonus_scale' => $bonus_scale
		);

		if($original === false) {
			$action = PM6_LOG::CREATE;
			$is_create = true;
		} else {
			$action = PM6_LOG::EDIT;
			$is_create = false;
			$original_settings = $settings;
			$changes = array();
		}
		foreach($settings as $section => $fields) {
			if($section == "bonus_scale") {
				if(!$is_create && $original[$this->getTableField().'_'.$section] != $bonus_scale) {
					$changes[$section] = array('to' => $bonus_scale, 'from' => $original[$this->getTableField().'_'.$section]);
				}
			} else {
				foreach($fields as $field => $value) {
					switch(strtoupper($field)) {
						case "OWN":
							$test_value = PM6::OWN;
							break;
						case "USERS":
							$test_value = PM6::STAFF_USERS;
							break;
						case "NON":
							$test_value = PM6::STAFF_NON;
							break;
					}
					if($var[$section."_".$field."_".$tkid] == true) {
						$settings[$section][$field] = true;
						$insert_data['es_'.$section] += $test_value;
					} else {
						$settings[$section][$field] = false;
					}
					if(!$is_create) {
						if(($original['es_'.$section] & $test_value) == $test_value) {
							$original_settings[$section][$field] = true;
						} else {
							$original_settings[$section][$field] = false;
						}
						if($original_settings[$section][$field] != $settings[$section][$field]) {
							$changes[$section][$field] = $settings[$section][$field];
						}
					}
				}
			}
		}
		if($is_create) {
			$insert_data[$this->getStatusFieldName()] = PM6::ACTIVE;
			$insert_data[$this->getParentFieldName()] = $emp_id;
			$insert_data[$this->getNameFieldName()] = $tkid;
			$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
			$id = $this->db_insert($sql);
			$display = "Saved settings for Employee ".$tkid.".";
			$changes = array(
				'user' => $this->getUserName(),
				'response' => $display,
				'raw' => array(
					'old' => "",
					'new' => $settings
				)
			);
			$log_var = array(
				'section' => self::OBJECT_TYPE,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => PM6_LOG::CREATE,
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
		} else {
			$id = $original[$this->getIDFieldName()];
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$id;
			$this->db_update($sql);
			$display = "Edited settings for Employee ".$tkid.":";
			foreach($changes as $section => $fields) {
				if($section == "bonus_scale") {
					$display .= "<br />- Bonus Scale changed to ".(isset($bonus_scales[$changes[$section]['to']]) ? $bonus_scales[$changes[$section]['to']] : "Unknown Bonus Scale")." from ".(isset($bonus_scales[$changes[$section]['from']]) ? $bonus_scales[$changes[$section]['from']] : "Unknown Bonus Scale");
				} else {
					foreach($fields as $field => $new_value) {
						$display .= "<br />- ".ucfirst($section)." ".$this->titles[$field]." turned ".($new_value ? "on" : "off");
					}
				}
			}
			$changes = array(
				'user' => $this->getUserName(),
				'response' => $display,
				'raw' => array(
					'old' => $original_settings,
					'new' => $settings
				)
			);
			$log_var = array(
				'section' => self::OBJECT_TYPE,
				'object_id' => $id,
				'changes' => $changes,
				'log_type' => PM6_LOG::EDIT,
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
		}
		return array("ok", "Save completed successfully.");
	}


	/*************************************
	 * GET functions
	 * */

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getRootTableName() {
		return $this->getDBRef();
	}

	public function getRefTag() {
		return self::REFTAG;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getTitles() {
		return $this->titles;
	}

	public function getRawObject($emp_id) {
		if(ASSIST_HELPER::checkIntRefForWholeIntegersOnly($emp_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = ".$emp_id." AND (".$this->getStatusFieldName()." & ".PM6::ACTIVE.") = ".PM6::ACTIVE." ORDER BY ".$this->getIDFieldName()." DESC LIMIT 1";
			$row = $this->mysql_fetch_one($sql);
			if(is_array($row) && count($row) > 0) {
				//Do I need to do anything here?
			} else {
				$row = false;
			}
		} else {
			$row = false;
		}

		return $row;
	}


	/**
	 * @param bool $assist_users - only get active users or only inactive/non-users?
	 * @param bool $tkid - get for specific employee by user_id - overrides $assist_users
	 * @return array|array[]|mixed
	 */
	public function getEmployeeSettingsFormattedForSetup($assist_users = true,$tkid=false) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE ".($tkid!==false?" ".$this->getNameFieldName()." = '".$tkid."' AND ":"")."
				(".$this->getStatusFieldName()." & ".PM6::ACTIVE.") = ".PM6::ACTIVE;
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		if(count($rows)>0) {
			foreach($rows as $row) {
				$emp_id = $row[$this->getParentFieldName()];
				$bonus_scale = $row[$this->getTableField().'_bonus_scale'];
				$data[$emp_id] = array(
					'settings' => $this->getDefaultSettings(),
				);
				$data[$emp_id]['bonus_scale'] = $bonus_scale;
				$create = $row['es_create'];
				$manage = $row['es_manage'];

				$tests = array();
				$tests[] = array('test' => PM6::OWN, 'section' => "create", 'field' => "own");
				$tests[] = array('test' => PM6::STAFF_USERS, 'section' => "create", 'field' => "users");
				$tests[] = array('test' => PM6::STAFF_NON, 'section' => "create", 'field' => "non");

				foreach($tests as $test) {
					$section = $test['section'];
					$field = $test['field'];
					$test_value = $test['test'];
					if(($create & $test_value) == $test_value) {
						$data[$emp_id]['settings'][$section][$field] = true;
					}
				}

				$tests = array();
				$tests[] = array('test' => PM6::OWN, 'section' => "manage", 'field' => "own");
				$tests[] = array('test' => PM6::STAFF_USERS, 'section' => "manage", 'field' => "users");
				$tests[] = array('test' => PM6::STAFF_NON, 'section' => "manage", 'field' => "non");

				foreach($tests as $test) {
					$section = $test['section'];
					$field = $test['field'];
					$test_value = $test['test'];
					if(($manage & $test_value) == $test_value) {
						$data[$emp_id]['settings'][$section][$field] = true;
					}
				}
			}
			if($tkid !== false) {
				//assume that most recently set $emp_id is the one for the given user
				$data = $data[$emp_id];
			}
		}
		return $data;
	}


	public function canICreateMyOwnScorecard() {
		$tkid = $this->getUserID();
		$row = $this->getRawEmployeesSetting($tkid);
		if(($row['es_create'] & PM6::OWN) == PM6::OWN) {
			return true;
		} else {
			return false;
		}
	}

	public function canICreateActiveUserStaffScorecards() {
		$tkid = $this->getUserID();
		$row = $this->getRawEmployeesSetting($tkid);
		if(($row['es_create'] & PM6::STAFF_USERS) == PM6::STAFF_USERS) {
			return true;
		} else {
			return false;
		}
	}
	public function canICreateNonUserStaffScorecards() {
		$tkid = $this->getUserID();
		$row = $this->getRawEmployeesSetting($tkid);
		if(($row['es_create'] & PM6::STAFF_NON) == PM6::STAFF_NON) {
			return true;
		} else {
			return false;
		}
	}

	public function getRawEmployeesSetting($tkid) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
				WHERE ".$this->getNameFieldName()." = '".$tkid."' 
				AND (".$this->getStatusFieldName()." & ".PM6::ACTIVE.") = ".PM6::ACTIVE;
		$row = $this->mysql_fetch_one($sql);
		return $row;
	}

	public function getFormattedEmployeeSetting($tkid=false) {
		if($tkid===false) {
			$tkid = $this->getUserID();
		}
		return $this->getEmployeeSettingsFormattedForSetup(true,$tkid);
	}


	public function __destruct() {
		parent::__destruct();
	}
}


?>