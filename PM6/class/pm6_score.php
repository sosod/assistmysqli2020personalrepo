<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class PM6_SCORE extends PM6 {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_trgrid";
	protected $secondary_parent_field = "_lneid";
    /*
	protected $progress_status_field = "_statusid";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "SCORE";
    const TABLE = "scores";
    const TABLE_FLD = "scr";
    const REFTAG = "SC";
	const LOG_TABLE = "scores";
	/**
	 * Type CONSTANTS
	 */
	/**
	 * STATUS CONSTANTS
	 */
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
/*
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	/*
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		$sql = "hello";
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type']; 
		$ref = $var['ref'];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}
		
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
    
	
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}
	
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	/*public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */


	/**
	 * Normal score saving function
	 * @param $var
	 * @return bool
	 */
	public function saveScore($var) {
		$tbl_fld = $this->getTableField();
		
		$trigger_id = $var['trigger_id'];
		$existing_scores = $var['existing_scores']>0 ? true : false;
		$result = false;
		
		$kpa_score = isset($var['kpa_score']) ? $var['kpa_score'] : array();
		$kpa_comment = isset($var['kpa_comment']) ? $var['kpa_comment'] : array();
		$cc_score = isset($var['cc_score']) ? $var['cc_score'] : array();
		$cc_comment = isset($var['cc_comment']) ? $var['cc_comment'] : array();
		$jal_score = isset($var['jal_score']) ? $var['jal_score'] : array();
		$jal_comment = isset($var['jal_comment']) ? $var['jal_comment'] : array();
		
		$score_ids = $var['score_id'];
		
		$scr_status = self::ACTIVE;
		
		if($existing_scores) {
			$changes = 0;
			//KPAs
			$line_ids = array_keys($kpa_score);
			foreach($line_ids as $l) {
				if(!isset($score_ids[$l]) || $score_ids[$l]==0) {
					if(strlen($kpa_score[$l])>0) {
						$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trigger_id , $l , ".$kpa_score[$l]." , '".($kpa_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
						$changes+=$this->db_insert($sql);
					}
				} else {
					$sid = $score_ids[$l];
					if(strlen($kpa_score[$l])>0) {
						$s = $kpa_score[$l];
					} else {
						$s = 0;
					}
					$sql = "UPDATE ".$this->getTableName()." SET ".$tbl_fld."_value = $s , ".$tbl_fld."_comment = '".($kpa_comment[$l])."' WHERE ".$this->getIDFieldName()." = $sid ";
					$changes+=$this->db_update($sql);
				}
			}
			//CCs
			$line_ids = array_keys($cc_score);
			foreach($line_ids as $l) {
				if(!isset($score_ids[$l]) || $score_ids[$l]==0) {
					if(strlen($cc_score[$l])>0) {
						$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trigger_id , $l , ".$cc_score[$l]." , '".($cc_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
						$changes+=$this->db_insert($sql);
					}
				} else {
					$sid = $score_ids[$l];
					if(strlen($cc_score[$l])>0) {
						$s = $cc_score[$l];
					} else {
						$s = 0;
					}
					$sql = "UPDATE ".$this->getTableName()." SET ".$tbl_fld."_value = $s , ".$tbl_fld."_comment = '".($cc_comment[$l])."' WHERE ".$this->getIDFieldName()." = $sid ";
					$changes+=$this->db_update($sql);
				}
			}
			//JALs
			$line_ids = array_keys($jal_score);
			foreach($line_ids as $l) {
				if(!isset($score_ids[$l]) || $score_ids[$l]==0) {
					if(strlen($jal_score[$l])>0) {
						$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trigger_id , $l , ".$jal_score[$l]." , '".($jal_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
						$changes+=$this->db_insert($sql);
					}
				} else {
					$sid = $score_ids[$l];
					if(strlen($jal_score[$l])>0) {
						$s = $jal_score[$l];
					} else {
						$s = 0;
					}
					$sql = "UPDATE ".$this->getTableName()." SET ".$tbl_fld."_value = $s , ".$tbl_fld."_comment = '".($jal_comment[$l])."' WHERE ".$this->getIDFieldName()." = $sid ";
					$changes+=$this->db_update($sql);
				}
			}
			if($changes>0) { $result = true; }
		} else {
			$insert_data = array();
			//KPAs
			$line_ids = array_keys($kpa_score);
			foreach($line_ids as $l) {
				//if(strlen($kpa_score[$l])>0) {
					$insert_data[] = "(null, $trigger_id , $l , ".(strlen($kpa_score[$l])>0 ? $kpa_score[$l] : 0)." , '".($kpa_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}
			//CCs
			$line_ids = array_keys($cc_score);
			foreach($line_ids as $l) {
				//if(strlen($cc_score[$l])>0) {
					$insert_data[] = "(null, $trigger_id , $l , ".(strlen($cc_score[$l])>0 ? $cc_score[$l] : 0)." , '".($cc_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}
			//JALs
			$line_ids = array_keys($jal_score);
			foreach($line_ids as $l) {
				//if(strlen($cc_score[$l])>0) {
				$insert_data[] = "(null, $trigger_id , $l , ".(strlen($jal_score[$l])>0 ? $jal_score[$l] : 0)." , '".($jal_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}
			//JALs
			$line_ids = array_keys($jal_score);
			foreach($line_ids as $l) {
				//if(strlen($cc_score[$l])>0) {
				$insert_data[] = "(null, $trigger_id , $l , ".(strlen($jal_score[$l])>0 ? $jal_score[$l] : 0)." , '".$this->code($jal_comment[$l])."', ".self::ACTIVE." , '".$this->getUserID()."', now())";
				//}
			}			
			if(count($insert_data)>0) {
				$sql = "INSERT INTO ".$this->getTableName()." VALUES ".implode(",",$insert_data);
				$mid = $this->db_insert($sql);
				$result = true;
			}
		}
		
		return $result;
	}

	/**
	 * Function to save-on-the-go
	 * @param $var
	 * @return bool
	 */
	public function saveTempScore($var) {
		$current_trigger_status = $var['current_trigger_status'];
		$score_record_id = $var['score_id'];
		$line_id = $var['line_id'];
		$trigger_id = $var['trigger_id'];
		$trigger_type = $var['trigger_type'];
		$field_type = $var['field_type'];
		$value = str_replace("|_|","&",$var['save_value']);

		if(ASSIST_HELPER::checkIntRefForWholeIntegersOnly($score_record_id)
		&& ASSIST_HELPER::checkIntRefForWholeIntegersOnly($line_id)
		&& ASSIST_HELPER::checkIntRefForWholeIntegersOnly($trigger_id)) {
			$sql = "UPDATE ".$this->getTableName()." SET ";
			switch($field_type) {
				case "SCORE":
					$sql.= " ".$this->getTableField()."_value = '".$value;
					break;
				case "COMMENT":
				default:
					$sql.= " ".$this->getTableField()."_comment = '".$value;
			}
			$sql.= "' WHERE ".$this->getParentFieldName()." = ".$trigger_id." 
					AND ".$this->getSecondaryParentFieldName()." = ".$line_id." 
					AND ".$this->getIDFieldName()." = ".$score_record_id;
			$this->db_update($sql);
			$result = array("ok","Last Saved: ".date("H:i:s"));
		} else {
			$result = array("error","Error on last save.");
		}

		//if trigger status is currently 1 (New) then update it to IP (2)
		if($current_trigger_status==1) {
			$triggerObject = new PM6_TRIGGER();
			$trgr_var = array(
				'trigger_id'=>$trigger_id,
				'trigger_type'=>$trigger_type,
				'post_action'=>"PENDING",
			);
			$triggerObject->saveScore($trgr_var,false);
		}

		return $result;
	}

	/**
	 * Check if score records exist in the DB & create them if they don't
	 * @param $trigger_id
	 * @param $scorecard_id
	 */
	public function prepTempScores($trigger_id,$scorecard_id) {
		$existing_scores = $this->getScoresForCompletingAssessment($trigger_id);

		if(count($existing_scores)==0) {
			$lineObject = new PM6_LINE();
			$sql = "INSERT INTO ".$this->getTableName()." 
					SELECT null, ".$trigger_id.", ".$lineObject->getIDFieldName().", 0, '', ".PM6::ACTIVE.",'".$this->getUserID()."',now()
					FROM ".$lineObject->getTableName()." 
					WHERE ".$lineObject->getParentFieldName()." = ".$scorecard_id."
					AND ".$lineObject->getIDFieldName()." NOT IN (
						SELECT ".$this->getSecondaryParentFieldName()." 
						FROM ".$this->getTableName()."
						WHERE ".$this->getParentFieldName()." = ".$trigger_id."
					)";
			$this->db_insert($sql);
		}

	}


	public function getScoresForCompletingAssessment($trigger_id) {
		$tbl_fld = $this->getTableField();
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$tbl_fld."_value as score
				, ".$tbl_fld."_comment as comment
				, ".$tbl_fld."_lneid as line_id 
				FROM ".$this->getTableName()." 
				WHERE ".$this->getParentFieldName()." = $trigger_id 
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		return $this->mysql_fetch_all_by_id($sql, "line_id");
	}
	 
	public function getAverageModeratorsScoreByAssessmentID($assess_id) {
		$value_fld = $this->getTableField()."_value";
		$line_fld = $this->getTableField()."_lneid";
		$triggerObj = new PM6_TRIGGER();
		$sql = "SELECT AVG($value_fld) as ave , $line_fld as id 
				FROM `".$this->getTableName()."` S 
				INNER JOIN ".$triggerObj->getTableName()." T 
					ON T.".$triggerObj->getIDFieldName()." = S.".$this->getParentFieldName()." 
					AND T.".$triggerObj->getTableField()."_type LIKE '".$triggerObj->getModeratorType()."%' 
					AND T.".$triggerObj->getParentFieldName()." = $assess_id
					AND (".$triggerObj->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				AND $value_fld > 0
				GROUP BY S.".$line_fld; //echo $sql;
		return $this->mysql_fetch_value_by_id($sql, "id", "ave");
	}
     

}


?>