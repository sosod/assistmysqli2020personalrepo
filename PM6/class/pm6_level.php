<?php
/**
 * To manage the ASSESSMENT object
 * 
 * Created on: 1 January 2016
 * Authors: Janet Currie
 * 
 */
 
class PM6_LEVEL extends PM6 {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "id";
	protected $status_field = "status";
	protected $parent_field = "competency_id";
	protected $secondary_parent_field = "proficiency_id";
	protected $name_field = "name";
	
	
    /*
	protected $progress_status_field = "_status_id";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "LEVEL";
    const TABLE = "list_competencies_levels";
    const TABLE_FLD = "";
    const REFTAG = "LVL";
	const LOG_TABLE = "";
	/**
	 * STATUS CONSTANTS
	 */


    
    public function __construct($object_id=0) {
        parent::__construct();
		
		/*$this->sources = array(
			array('active'=>true,'modref'=>"SDP15"),
			array('active'=>true,'modref'=>"SDP15I"),
		);*/
		
/*		NO FIELD PREFIX IN THIS TABLE	
 * 
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
 * */
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }

public function getObjectType() { return self::OBJECT_TYPE; }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function confirmObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function activateObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function undoActivation($object_id) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function deactivateObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function restoreObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function deleteObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function reassignObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function updateObject($var) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	public function editObject($var,$attach=array()) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}	
	
	
	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
	public function getList($cc_id,$prof_id=array()) {
		$result = array("error","Oops!  This function has not yet been developed.");
		
		//get records for one or more PARENT Core Competency (CC_ID) and one or more SECONDARY_PARENT Proficiency Level (PROF_ID)
		$where = "WHERE ".$this->getActiveStatusSQL("L");
		if(is_array($cc_id)) {
			$cc_id = ASSIST_HELPER::removeBlanksFromArray($cc_id);
			if(count($cc_id)>0) {
				$where.=" AND ".$this->getParentFieldName()." IN (".implode(",",$cc_id).")";
			}
		} elseif(ASSIST_HELPER::checkIntRef($cc_id)) {
			$where.=" AND ".$this->getParentFieldName()." = ".$cc_id;
		}
		if(is_array($prof_id)) {
			$prof_id = ASSIST_HELPER::removeBlanksFromArray($prof_id);
			if(count($prof_id)>0) {
				$where.=" AND ".$this->getSecondaryParentFieldName()." IN (".implode(",",$prof_id).")";
			}
		} elseif(ASSIST_HELPER::checkIntRef($prof_id)) {
			$where.=" AND ".$this->getSecondaryParentFieldName()." = ".$prof_id;
		}
		$sql = "SELECT L.".$this->getIDFieldName()." as id
				, L.".$this->getNameFieldName()." as name
				, L.".$this->getParentFieldName()." as cc
				, L.".$this->getSecondaryParentFieldName()." as prof 
				FROM ".$this->getTableName()." as L
				".$where;
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		if(count($rows)>0) {
			foreach($rows as $r) {
				$data[$r['cc']][$r['prof']][$r['id']] = $r['name'];
			}
		} else {
			$data = array("error","No Competency/Proficiency Achievement Levels available.");
		}
		$result = $data;
		return $result;
	}
	
	public function getAObject($id=0,$options=array()) {
		//return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	
	public function getSimpleDetails($id=0) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
		/*
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
		 * */
	}
	
	
	
	
	private function getSpecificObjects($where) {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	
	
	
	
	public function getObjectsForEdit() {
		$result = array("error","Oops!  This function has not yet been developed.");
		return $result;
	}
	
	



	

		
	/***
	 * Returns an unformatted array of an object 
	 */
	  
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
			//status = activated
		//return $this->getStatusSQL("ALL",$t,false);
		$where = "(".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		
		return $where;
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a contract
	 */ 
	/*public function coreCompInUse() {
		$sql = "SELECT line_srcid, count(line_id) as cc FROM ".$this->getTableName()." WHERE line_srctype = 'CC' GROUP BY line_srcid";
		$data = $this->mysql_fetch_value_by_id($sql, "line_srcid", "cc");
		return $data;
	}*/
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	private function logMyAction($id,$action,$old,$new,$fld="") {
		$logObj = new PM6_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "ACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Activated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REVERSEACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Reversed previous activation of |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "RESTORE":
				$action = $logObj->getRestoreLogAction();
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$action = $logObj->getDeactivateLogAction();
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$action = $logObj->getEditLogAction();
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;
		}
		$logObj->addObject(
			array(),
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			$id
		);
		
	}
}


?>