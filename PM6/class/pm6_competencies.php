<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class PM6_COMPETENCIES extends PM6 {

	private $reftag = array("C"=>"C","P"=>"P","CL"=>"CL");


    public $treeview_datastructure;

    public function __construct() {
        parent::__construct();
    }

    private function getReftag($type) {
    	return $this->reftag[$type];
	}
    public function getCompetencyTreeViewDataStructureByCompetencyID($competency_id){
        $competency = $this->getCompetencyByID($competency_id);
        $this->treeview_datastructure = $competency;
        $this->addProficienciesToTreeViewDataStructure($competency_id);
        $this->addCompetencyLevelsToTreeViewDataStructure($competency_id);

        return $this->treeview_datastructure;
    }

    public function getCompetencyByID($competency_id){
        $list_id = 'competencies';
        $listObj = new PM6_LIST($list_id);
        //Removed 14 Feb 2020 [JC] AA-267 - Only fetching one competency so don't need to get them all
//        $list_data = $listObj->getListItemsForSetup();
        $competency = array(
        	$competency_id => $listObj->getAListItemForSetup($competency_id),
		); //simplified code so that array create & element set happen in a single command rather than 2 steps [JC] 14 Feb 2020 AA-267
        return $competency;
    }

    public function addProficienciesToTreeViewDataStructure($competency_id){
        $proficiencies = $this->getAllProficiencies();

        foreach($proficiencies as $key => $val){
			//check if item is active so that display page knows whether to display option or not
			$val['is_active'] = (($val['status'] & PM6::ACTIVE) == PM6::ACTIVE);
			$val['is_inactive'] = (($val['status'] & PM6::INACTIVE) == PM6::INACTIVE);
			$this->treeview_datastructure[$competency_id]['proficiencies'][$key] = $val;
        }
    }

    public function getAllProficiencies(){
        $list_id = 'proficiencies';
        $listObj = new PM6_LIST($list_id);
        $list_data = $listObj->getListItems();
        ksort($list_data);
        return $list_data;
    }

    public function addCompetencyLevelsToTreeViewDataStructure($competency_id){
        $competency_levels = $this->getCompetencyLevelsByCompetencyID($competency_id);
        foreach($competency_levels as $key => $val){
            $proficiency_id = $val['proficiency_id'];
			$val['is_active'] = (($val['status'] & PM6::ACTIVE) == PM6::ACTIVE);
			$val['is_inactive'] = (($val['status'] & PM6::INACTIVE) == PM6::INACTIVE);
            $this->treeview_datastructure[$competency_id]['proficiencies'][$proficiency_id]['competency_levels'][$key] = $val;
        }
    }


    public function getCompetencyLevelsByCompetencyID($competency_id){
        $list_id = 'competencies_levels';
        $listObj = new PM6_LIST($list_id);
        $field_name = 'competency_id';
        $field_value = $competency_id;
        $list_data = $listObj->getListItemsByFieldID($field_name, $field_value);
        foreach($list_data as $key => $data) {
        	$list_data[$key]['ref'] = $this->getModRef()."/".$this->getRefTag("CL").$key;
		}
        return $list_data;
    }

    public function editCompetencyLevels($var){
        $object_id = $var['object_id'];
        unset($var['object_id']);

        $list_vars = array();
        $list_vars['id'] = $object_id;

        foreach($var as $key => $val){
            $lv_key = ($key == 'proficiency' ? $key . '_id' : $key);
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies_levels';
        $listObj = new PM6_LIST($list_id);

        return $listObj->editObject($list_vars);

    }

    public function addCompetencyLevel($var){
        unset($var['object_id']);

        $list_vars = array();
        foreach($var as $key => $val){
            $lv_key = ($key == 'proficiency' || $key == 'competency' ? $key . '_id' : $key);
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies_levels';
        $listObj = new PM6_LIST($list_id);

        return $listObj->addObject($list_vars);

    }

    public function editCompetency($var){
        $object_id = $var['object_id'];
        unset($var['object_id']);

        $list_vars = array();
        $list_vars['id'] = $object_id;

        foreach($var as $key => $val){
            $lv_key = $key;
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies';
        $listObj = new PM6_LIST($list_id);

        return $listObj->editObject($list_vars);

    }

    public function addCompetency($var){
        unset($var['object_id']);
        unset($var['list_id']);

        $list_vars = array();
        foreach($var as $key => $val){
            $lv_key = $key;
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies';
        $listObj = new PM6_LIST($list_id);

        return $listObj->addObject($list_vars);

    }


	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$list_id = $var['object_type'];
		switch($list_id) {
			case "competencies_levels": $t = "CL"; break;
			case "proficiencies": $t = "P"; break;
			case "competencies": $t = "C"; break;
		}
		$reftag = $this->getModRef()."/".$this->getReftag($t);

		$listObject = new PM6_LIST($list_id);
		return $listObject->deactivateListItem($object_id,$reftag);
	}


	public function restoreObject($var) {
		$object_id = $var['object_id'];
		$list_id = $var['object_type'];
		switch($list_id) {
			case "competencies_levels": $t = "CL"; break;
			case "proficiencies": $t = "P"; break;
			case "competencies": $t = "C"; break;
		}
		$reftag = $this->getModRef()."/".$this->getReftag($t);
		$listObject = new PM6_LIST($list_id);
		return $listObject->restoreListItem($object_id,$reftag);
	}


}


?>