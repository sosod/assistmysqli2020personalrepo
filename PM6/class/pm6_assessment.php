<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 23 May 2017
 * Authors: Janet Currie
 * 
 */
 
class PM6_ASSESSMENT extends PM6 {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_scdid";
	protected $name_field = "_periodid";
    /*
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "ASSESSMENT";
    const TABLE = "assessments";
    const TABLE_FLD = "asmt";
    const REFTAG = "SA";
	const LOG_TABLE = "assessments";
	/**
	 * STATUS CONSTANTS
	 */
	
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
/*		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);
			$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$sql = "INSERT INTO ".$this->getTableName()." SET
				asmt_scdid = ".$var['scd_id']."
				, asmt_periodid = ".$var['period']."
				, asmt_status = ".self::INACTIVE."
				, asmt_insertuser = '".$this->getUserID()."'
				, asmt_insertdate = now()
				";
		$id = $this->db_insert($sql);
		if($id>0) {
			$log_var = array(
				'scd_id'=>$var['scd_id'],
				'prd_id'=>$var['period'],
			);
			$this->logMyAction($id, "CREATE", "", $var,$log_var);
			$var['assess_id'] = $id;
			$triggerObj = new PM6_TRIGGER();
			$result = $triggerObj->addObject($var);
			if($result[0]=="ok") {
				$sql = "UPDATE ".$this->getTableName()." SET asmt_status = ".self::ACTIVE." WHERE asmt_id = ".$id;
				$mar = $this->db_update($sql);
				if($mar>0) {
					return array("ok","New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." successfully created.",$id);
				}
			}
		}
		return array("error","An error occurred.  Please reload the page and try again.");
	}

	public function deleteObject($id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$id;
		$old = $this->mysql_fetch_one($sql);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$mar = $this->db_update($sql);
			$log_var = array(
				'scd_id'=>$old[$this->getTableField().'_scdid'],
				'prd_id'=>$old[$this->getTableField().'_periodid'],
			);
			$var = array('status'=>self::INACTIVE);
			$this->logMyAction($id, "DELETE", $old, $var,$log_var);		
	}

	/*

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type']; 
		$ref = $var['ref'];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}
		
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
    
	public function getAssessmentsForEdit($parent_id) {
		$tbl_fld = $this->getTableField();
		$prdObj = new PM6_PERIOD();
		$periods = $prdObj->getAllAssessmentPeriodsForSelect();
		$sql = "SELECT A.*
				, CONCAT('".$this->getRefTag()."', A.".$this->getIDFieldName().") as ref
				, A.".$tbl_fld."_periodid as period_id
				FROM ".$this->getTableName()." A 
				WHERE A.".$this->getParentFieldName()." = $parent_id 
				AND (A.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$triggers = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		foreach($triggers as $key => $trig) {
			$triggers[$key]['period'] = $periods[$trig['period_id']];
		}
		return $triggers;
	}

	public function getAssessmentDetails($object_keys=array()) {
        $asmt_id_fld = $this->getIDFieldName();
        $asmt_parent_fld = $this->getParentFieldName();
        $asmt_status_fld = $this->getStatusFieldName();
        $sql = "SELECT count(".$asmt_id_fld.") as c, ".$asmt_parent_fld." as obj_id
					FROM ".$this->getTableName()."
					WHERE ".$asmt_parent_fld." IN (".implode(",",$object_keys).") AND (".$asmt_status_fld." & ".self::ACTIVE.") = ".self::ACTIVE."
					GROUP BY ".$asmt_parent_fld."";
        return $this->mysql_fetch_value_by_id($sql, "obj_id","c");
	}

	public function getSingleAssessmentDetails($assess_id) {
    	$row = $this->getRawObject($assess_id);

    	$periodObject = new PM6_PERIOD();
    	$period_id = $row[$this->getNameFieldName()];
    	$period = $periodObject->getAObject($period_id);
    	$row['period_name'] = $period[$period_id]['name'];

    	return $row;
	}

	public function getTimePeriodsForAssessment($period_id=0,$obj_id=0) {
		if($period_id==0) {
			$data = array();
			$row = $this->getRawObject($obj_id);
			$period_id = $row[$this->getTableField().'_periodid'];
		}
		$prdObj = new PM6_PERIOD();
		$periods = $prdObj->getTimePeriodsByAssessmentPeriod($period_id);
		
		return $periods;
	}
	
	
	/**
	 * Function to get related object ids based on an assessment id
	 * @param [] $id = Assessment object ID to use as starting point
	 * @param [SELF] $trigger_type = Type of the trigger that is looking for related IDs
	 * @param [false] $user_id = TKID of the user who is to complete the triggered assessment (false = don't filter)
	 * @param [false] $page_section = secondary trigger_type where trigger_type is set to VIEW (for a specific Manage section)
	 * @return Array(_assessment details, 'assessment'=>ID, 'scorecard'=>ID, 'period'=>ID, 'atrigger'=>ID, 'trigger'=>ID) 
	 */
	public function getRelatedIDs($id,$trigger_type="SELF",$user_id=false,$page_section=false) {
		$row = array();
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		$trgrObj = new PM6_TRIGGER();
		//if($trigger_type==$trgrObj->getViewType() && $page_section!==false) {
		//	$trigger_type = PM6_TRIGGER::FINAL_ASSESS;
		//	$user_id=false;
		//} else
		if($trigger_type==$trgrObj->getReportType() || $trigger_type==$trgrObj->getViewType()) {
			$trigger_type = $trgrObj->getFinalReviewType(); //echo $trigger_type;
			$user_id=false;
		}
		$sql = "SELECT 
				A.*
				, ".$this->getIDFieldName()." as assessment
				, ".$this->getParentFieldName()." as scorecard
				, ".$trgrObj->getIDFieldName()." as atrigger
				, ".$this->getTableField()."_periodid as period
				FROM ".$this->getTableName()." A
				INNER JOIN ".$trgrObj->getTableName()." 
				  ON ".$this->getIDFieldName()." = ".$trgrObj->getParentFieldName()." 
				  AND ".$trgrObj->getTableField()."_type LIKE '".$trigger_type."%' 
				  ".($user_id!==false ? "AND ".$trgrObj->getTableField()."_tkid = '".$user_id."'":"")." 
				  AND (".$trgrObj->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				WHERE ".$this->getIDFieldName()." = ".$id;
//				echo $sql;
		$row = $this->mysql_fetch_one($sql);
		//assessment_start_date is used to check if source deleted items are applicable to this assessment or not.  #AA-568 JC
		$row['assess_start_date'] = $row[$this->getTableField().'_insertdate'];

		if(!is_array($row) || count($row)==0) {
			$sql = "SELECT 
				A.*
				, ".$this->getIDFieldName()." as assessment
				, ".$this->getParentFieldName()." as scorecard
				, ".$trgrObj->getIDFieldName()." as atrigger
				, ".$this->getTableField()."_periodid as period
				FROM ".$this->getTableName()." A
				INNER JOIN ".$trgrObj->getTableName()." 
				  ON ".$this->getIDFieldName()." = ".$trgrObj->getParentFieldName()." 
				  AND ".$trgrObj->getTableField()."_type LIKE '".$page_section."%' 
				  ".($user_id!==false ? "AND ".$trgrObj->getTableField()."_tkid = '".$user_id."'":"")." 
				  AND (".$trgrObj->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." 
				WHERE ".$this->getIDFieldName()." = ".$id;
//			echo "<P>".$sql;
			$row = $this->mysql_fetch_one($sql);
		}
		$row['trigger'] = isset($row['atrigger']) ? $row['atrigger'] : 0;
		return $row;
	}







	public function getEmployeeBeingAssessed($assess_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$assess_id;
		$assessment = $this->mysql_fetch_one($sql);
		$scorecard_id = $assessment[$this->getParentFieldName()];
		$scrObject = new PM6_SCORECARD();
		$employee = $scrObject->getEmployee($scorecard_id);
		return $employee;
	}

    public function getAssessmentForStatusReportByPeriod($period_id, $filters) {
        $scdObj = new PM6_SCORECARD();
        $empObject = new PM6_EMPLOYEE();
        $assessments = $this->getAssessmentsByPeriod($period_id, $filters);

        $active_objects = $scdObj->getObjectsForReport();

        if(isset($assessments) && is_array($assessments) && count($assessments)){
            $assessment_keys = array_keys($assessments);
            $triggerObj = new PM6_TRIGGER();
            $triggers = $triggerObj->getTriggersByParentID($assessment_keys);

            foreach($assessments as $key => $val){
                $s = $val[$scdObj->getStatusFieldName()];
                $assessments[$key]['count'] = $active_objects[$val[$scdObj->getIDFieldName()]]['count'];
                $assessments[$key]['status'] = $scdObj->getMyScorecardStatusName($s,isset($assessments[$key])?$assessments[$key]:0);
                $assessments[$key]['status_last_update'] = $scdObj->determineLastActivationStatusUpdateDate($val[$scdObj->getIDFieldName()]);
                $assessments[$key]['assessments'] = $active_objects[$val[$scdObj->getIDFieldName()]]['assessments'];
                $assessments[$key] = array_merge($assessments[$key], $empObject->getEmployee($val['scd_empid']));
                $assessments[$key]['eval_status'] = $this->determineEvaluationStatus($val['asmt_id'], $triggers);
                $assessments[$key]['eval_last_update'] = $this->determineLastEvaluationStatusUpdateDate($val['asmt_id'], $triggers);
            }

            //Not Applicable to PM3
            $assessments = $this->applyEmployeeBasedReportFilters($assessments, $filters);
        }
        return $assessments;
    }

    public function getAssessmentForPerformanceReportByPeriod($period_id, $filters) {
        $scdObj = new PM6_SCORECARD();
        $empObject = new PM6_EMPLOYEE();
        $assessments = $this->getAssessmentsByPeriod($period_id, $filters);

        $active_objects = $scdObj->getObjectsForReport();

        if(isset($assessments) && is_array($assessments) && count($assessments)){
            $assessment_keys = array_keys($assessments);
            $triggerObj = new PM6_TRIGGER();
            $triggers = $triggerObj->getTriggersByParentID($assessment_keys);

            foreach($assessments as $key => $val){
                $s = $val[$scdObj->getStatusFieldName()];
                $assessments[$key]['count'] = $active_objects[$val[$scdObj->getIDFieldName()]]['count'];
                $assessments[$key]['status'] = $scdObj->getMyScorecardStatusName($s,isset($assessments[$key])?$assessments[$key]:0);
                $assessments[$key]['assessments'] = $active_objects[$val[$scdObj->getIDFieldName()]]['assessments'];
                $assessments[$key] = array_merge($assessments[$key], $empObject->getEmployee($val['scd_empid']));
                $assessments[$key]['eval_status'] = $this->determineEvaluationStatus($val['asmt_id'], $triggers);

                $assessments[$key]['self_score_average'] = $triggerObj->getOverallSelfScoreAverageByAssessmentID($val['asmt_id']);
                $assessments[$key]['moderation_score_average'] = $triggerObj->getOverallModerationScoreAverageByAssessmentID($val['asmt_id']);
                $assessments[$key]['final_score_average'] = $triggerObj->getOverallFinalScoreAverageByAssessmentID($val['asmt_id']);
            }

            //Not Applicable to PM3
            $assessments = $this->applyEmployeeBasedReportFilters($assessments, $filters);
        }
        return $assessments;
    }
	
	public function getAssessmentsByPeriod($period_id, $filters) {
		$scdObj = new PM6_SCORECARD();
		$sql = "SELECT a.* , s.*, CONCAT(tk.tkname,' ',tk.tksurname) as employee
				FROM ".$this->getTableName()." a
				INNER JOIN  ".$scdObj->getTableName()." s
				  ON s.".$scdObj->getIDFieldName()." = a.".$this->getParentFieldName()."
				  AND (".$scdObj->getActiveStatusSQL("s").")
				INNER JOIN ".$this->getUserTableName()." tk
				  ON s.".$scdObj->getTableField()."_tkid = tk.tkid AND tk.tkstatus = 1
				WHERE a.".$this->getNameFieldName()." = ".$period_id." 
				AND (".$this->getActiveStatusSQL("a").") ";

        if(isset($filters['sort_by']) && ($filters['sort_by'] == 'employee' || $filters['sort_by'] == 'xxx')){
            $sql .= "ORDER BY " . $filters['sort_by'] . " " . (isset($filters['sort_asc_desc']) && $filters['sort_asc_desc'] == 'desc' ? "DESC" : "ASC" );
        }

		return $this->mysql_fetch_all_by_id($sql, 'asmt_id');
	}

    private function determineEvaluationStatus($assessment_id, $triggers){//When it turns out that all is well, move this to PM6trigger

	    if(isset($triggers[$assessment_id]) && is_array($triggers[$assessment_id]) && count($triggers[$assessment_id]) > 0){
	        $assessment_triggers = $triggers[$assessment_id];
            if(count($assessment_triggers['SELF']) > 0 && count($assessment_triggers['MOD']) == 0 && count($assessment_triggers['FINAL']) == 0){
                $evaluation_status = 'Self Assessment';
            }elseif(count($assessment_triggers['SELF']) > 0 && count($assessment_triggers['MOD']) > 0 && count($assessment_triggers['FINAL']) == 0){
                $evaluation_status = 'Moderation';
            }elseif(count($assessment_triggers['SELF']) > 0 && count($assessment_triggers['MOD']) > 0 && count($assessment_triggers['FINAL']) > 0){
                $evaluation_status = 'Final Review';
            }else{
                $evaluation_status = 'Not Yet Triggered';//Logic says that this should never happen
            }
        }else{
            $evaluation_status = 'Not Yet Triggered';//Logic says that this should never happen
        }

        return $evaluation_status;
    }

    private function determineLastEvaluationStatusUpdateDate($assessment_id, $triggers){//When it turns out that all is well, move this to PM6trigger

        if(isset($triggers[$assessment_id]) && is_array($triggers[$assessment_id]) && count($triggers[$assessment_id]) > 0){
            $assessment_triggers = $triggers[$assessment_id];

            $update_dates = array();
            if(count($assessment_triggers['SELF']) > 0){
                $update_dates[] = $assessment_triggers['SELF']['trgr_insertdate'];
            }

            if(count($assessment_triggers['MOD']) > 0){
                foreach($assessment_triggers['MOD'] as $key => $val){
                    $update_dates[] = $val['trgr_insertdate'];
                }
            }

            if(count($assessment_triggers['FINAL']) > 0){
                $evaluation_status = 'N/A';
            }

            if(count($update_dates) > 0){
                usort($update_dates, array('PM6', 'date_sort'));
                krsort($update_dates);
                $evaluation_date = $update_dates[0];

            }else{
                $evaluation_date = 'N/A';
            }
        }else{
            $evaluation_date = 'N/A';//Logic says that this should never happen
        }

        return $evaluation_date;
    }

    private function applyEmployeeBasedReportFilters($assessments, $filters){

	    if($this->isSortedByEmployeeFields($filters)){
            $field_linked_items = $this->getEmployeeFieldLinkedItems($filters['sort_by']);
            $assessments_by_sort_by = $this->convertAssessmentToSortByFieldFormat($assessments, $field_linked_items, $filters['sort_by']);
            $assessments_by_sort_by = $this->sortFormattedAssessmentsAccordingly($assessments_by_sort_by, $filters['sort_asc_desc']);
            $assessments = $this->returnAssessmentsToOriginalFormat($assessments_by_sort_by);
        }

        $assessments = $this->applyFieldBasedFilters($assessments, $filters);

	    return $assessments;
    }

    private function isSortedByEmployeeFields($filters){
	    $isSortedByEmployeeFields = false;
        if(isset($filters['sort_by']) && ($filters['sort_by'] == 'job_manager_title' || $filters['sort_by'] == 'job_dept' || $filters['sort_by'] == 'job_level' || $filters['sort_by'] == 'job_title')){
            $isSortedByEmployeeFields = true;
        }
        return $isSortedByEmployeeFields;
    }

    private function getEmployeeFieldLinkedItems($sort_by_field){
        $emp_obj = new PM6_EMPLOYEE();
        $field_linked_items = '';
        if($sort_by_field == 'job_manager_title' || $sort_by_field == 'job_title'){
            $field_linked_items = $emp_obj->getAllJobTitlesFormattedForSelect();
        }elseif($sort_by_field == 'job_dept'){
            $field_linked_items = $emp_obj->getAllDepartmentsFormattedForSelect();
        }elseif($sort_by_field == 'job_level'){
            $field_linked_items = $emp_obj->getAllJobLevelsFormattedForSelect();
        }

        return $field_linked_items;
    }

    private function convertAssessmentToSortByFieldFormat($assessments, $field_linked_items, $sort_by_field){
        $assessments_by_sort_by = array();
        foreach($assessments as $key => $val){
            $assessments_by_sort_by[$field_linked_items[$val[$sort_by_field]]][$val['asmt_id']] = $val;
        }
        return $assessments_by_sort_by;
    }

    private function sortFormattedAssessmentsAccordingly($assessments_by_sort_by, $sort_asc_desc){
        if($sort_asc_desc == 'asc'){
            ksort($assessments_by_sort_by);
        }else{
            krsort($assessments_by_sort_by);
        }
        return $assessments_by_sort_by;
    }

    private function returnAssessmentsToOriginalFormat($assessments_by_sort_by){
        $sorted_assessments = array();
        foreach($assessments_by_sort_by as $key => $val){
            foreach($val as $key2 => $val2){
                $sorted_assessments[$key2] = $val2;
            }
        }

        return $sorted_assessments;
    }

    private function applyFieldBasedFilters($assessments, $filters){
        $job_manager_filter = $filters['job_manager_title'];
        $job_dept_filter = $filters['job_dept'];
        $job_level_filter = $filters['job_level'];


        //Now apply the field based filters
        $assessments_filtered_by_field = array();
        foreach($assessments as $key => $val){
            if(
                (//ABC
                    ($job_manager_filter != 'X' && $job_dept_filter != 'X' && $job_level_filter != 'X')
                    &&
                    ($val['job_manager_title'] == $job_manager_filter && $val['job_dept'] == $job_dept_filter && $val['job_level'] == $job_level_filter)
                )
                ||
                (//ABc
                    ($job_manager_filter != 'X' && $job_dept_filter != 'X' && $job_level_filter == 'X')
                    &&
                    ($val['job_manager_title'] == $job_manager_filter && $val['job_dept'] == $job_dept_filter && $val['job_level'] != $job_level_filter)
                )
                ||
                (//AbC
                    ($job_manager_filter != 'X' && $job_dept_filter == 'X' && $job_level_filter != 'X')
                    &&
                    ($val['job_manager_title'] == $job_manager_filter && $val['job_dept'] != $job_dept_filter && $val['job_level'] == $job_level_filter)
                )
                ||
                (//Abc
                    ($job_manager_filter != 'X' && $job_dept_filter == 'X' && $job_level_filter == 'X')
                    &&
                    ($val['job_manager_title'] == $job_manager_filter && $val['job_dept'] != $job_dept_filter && $val['job_level'] != $job_level_filter)
                )
                ||
                (//aBC
                    ($job_manager_filter == 'X' && $job_dept_filter != 'X' && $job_level_filter != 'X')
                    &&
                    ($val['job_manager_title'] != $job_manager_filter && $val['job_dept'] == $job_dept_filter && $val['job_level'] == $job_level_filter)
                )
                ||
                (//aBc
                    ($job_manager_filter == 'X' && $job_dept_filter != 'X' && $job_level_filter == 'X')
                    &&
                    ($val['job_manager_title'] != $job_manager_filter && $val['job_dept'] == $job_dept_filter && $val['job_level'] != $job_level_filter)
                )
                ||
                (//abC
                    ($job_manager_filter == 'X' && $job_dept_filter == 'X' && $job_level_filter != 'X')
                    &&
                    ($val['job_manager_title'] != $job_manager_filter && $val['job_dept'] != $job_dept_filter && $val['job_level'] == $job_level_filter)
                )
                ||
                (//abc
                    ($job_manager_filter == 'X' && $job_dept_filter == 'X' && $job_level_filter == 'X')
                    &&
                    ($val['job_manager_title'] != $job_manager_filter && $val['job_dept'] != $job_dept_filter && $val['job_level'] != $job_level_filter)
                )
            ){
                $assessments_filtered_by_field[$key] = $val;
            }
        }

        return (count($assessments_filtered_by_field) > 0 ? $assessments_filtered_by_field : $assessments );
    }






        /*
            public function getActiveSQLScript($tn = "C") {
                return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
            }


            public function getList($section,$options=array()) {
                return $this->getMyList(self::OBJECT_TYPE, $section,$options);
            }
            public function getAObject($id=0,$options=array()) {
                $sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$id;
                $row = $this->mysql_fetch_one($sql);
                return $row;
            }
            public function getSimpleDetails($id=0) {
                $obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
                $data = array(
                    'parent_id'=>0,
                    'id'=>$id,
                    'name' => $obj['rows'][$this->getNameFieldName()]['display'],
                    'reftag' => $obj['rows'][$this->getIDFieldName()],
                    'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
                    'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
                );
                return $data;
            }*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Object where 
			//status = active and
			//status <> deleted
		$sql = " ( ( ".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
		 		AND  ( ".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE." )";
		return $sql;
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */

     
     
     
     
     
     
     
     
     
     
     
     
     
	private function logMyAction($id,$action,$old,$new,$var) {
		$logObj = new PM6_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id." for |".PM6_PERIOD::OBJECT_TYPE."| ".PM6_PERIOD::REFTAG.$var['prd_id']." on |".PM6_SCORECARD::OBJECT_TYPE."| ".PM6_SCORECARD::REFTAG.$var['scd_id'].".";
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id." for |".PM6_PERIOD::OBJECT_TYPE."| ".PM6_PERIOD::REFTAG.$var['prd_id']." on |".PM6_SCORECARD::OBJECT_TYPE."| ".PM6_SCORECARD::REFTAG.$var['scd_id'].".";
//				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			/*case "RESTORE":
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;*/
		}
		$logObj->addObject(
			$var,
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			0,
			$id
		);
		
	}
     
}


?>