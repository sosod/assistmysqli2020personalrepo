<?php
//Have to bypass PM6 class as it calls this class;

class PM6_SETUP_MODULE extends PM6_HELPER {

	private $source_modules = array();
	private $section_names = array();
	private $source_sections = array(
		"ORG","IKPI"
	);
	private $available_grouping_fields = array(
		"function_id",
		"concept_id",
		"munkpa_id",
		"natkpa_id",
		"pdo_id",
		"riskrating_id",
		"type_id",
		"subdir_id",
		"owner_id",
		"reportcycle_id",
		"expresult_id",
	);
	private $available_grouping_field_types = array(
		"function_id"=>"SEGMENT",
		"concept_id"=>"LIST",
		"munkpa_id"=>"LIST",
		"natkpa_id"=>"LIST",
		"pdo_id"=>"LIST",
		"riskrating_id"=>"LIST",
		"type_id"=>"LIST",
		"subdir_id"=>"LIST",
		"owner_id"=>"LIST",
		"reportcycle_id"=>"LIST",
		"expresult_id"=>"LIST",
	);
	private $pages_which_dont_need_module_setup_check = array(
		"manage_view_mine",
		"manage_view_all",
		"manage_self",
		"manage_moderation",
		"manage_final",
	);
	private $sections_which_dont_need_module_setup_check = array("manage","setup");

	const TABLE_NAME = "setup_module";
	const TABLE_FLD = "mod";
	const LOG_SECTION = "MODSET";
	const ACTIVE = 2;

	public function __construct($modref="") {
		parent::__construct($modref);

		$this->source_modules = array(
			'ORG'=>array("SDBP6"),
			'IKPI'=>array("IKPI2"),
			'JAL'=>array("JAL1")
		);

		$this->section_names = array(
			'ORG'=>"Organisational Performance",
			'IKPI'=>"Individual KPIs",
			'JAL'=>"Activities"
		);


	}




	public function getSourceModules() { return $this->source_modules; }
	public function getSectionNames() { return $this->section_names; }
	public function getASectionName($section) { return $this->section_names[$section]; }
	public function getSectionTypes() { return $this->source_sections; }
	public function getTableName() { return $this->getDBRef()."_".self::TABLE_NAME; }
	public function getTableField() { return self::TABLE_FLD; }
	public function getGroupingFieldType($fld) { return $this->available_grouping_field_types[$fld]; }
	public function checkIfPageNeedsToDieIfNoSetupDone($page) {
		if(in_array($page, $this->pages_which_dont_need_module_setup_check)) {
			return false;
		} else {
			$p = explode("_",$page);
			$p = $p[0];
			if(in_array($p, $this->sections_which_dont_need_module_setup_check)) {
				return false;
			}
		}
		return true;
	}


	public function getSourceModuleDetails($object_id,$src_modref, $src_modloc) {
		//get module name - only get modules that are not archived
		$sql = "SELECT * FROM assist_menu_modules WHERE modref = '$src_modref' AND modyn <> 'R'";
		$row = $this->mysql_fetch_one($sql);
		//only proceed if a valid response was received from the DB
		if(is_array($row) && count($row)>0) {
			//get object name
			switch($src_modloc) {
				case "IKPI2":
					$extObject = new IKPI2_INTERNAL($src_modref);
					break;
				case "SDBP6":
					$extObject = new SDBP6_INTERNAL($src_modref);
					break;
			}
			$info = $extObject->getASDBIPName($object_id);
			if($info!==false) {
				$object_name = $info." (Module: ".$row['modtext'].")";
			} else {
				$object_name = "Error - Annual Plan $object_id could not be found on the given module.";
			}
		} else {
			//return message showing that module wasn't found in assist_menu_modules
			$object_name = "Error - given module ($src_modref) does not appear to be present on the database or has been archived.";
		}
		return $object_name;
	}

	/**
	 * Get name of field chosen for KPI Grouping [AA-535] JC
	 * @param $group_field
	 * @param $src_modref
	 * @param $src_modloc
	 * @return array
	 */
	public function getGroupingDetails($group_field,$src_modref,$src_modloc) {
		switch($src_modloc) {
			case "IKPI2":
				$extObject = new IKPI2_INTERNAL($src_modref);
				break;
			case "SDBP6":
				$extObject = new SDBP6_INTERNAL($src_modref);
				break;
		}
		$primary_grouping_fields = $extObject->getAvailableHeadingsForPerformanceGrouping(array($group_field));
		return $primary_grouping_fields;

	}

	/**
	 * getSettings() = get module settings as saved in setup_module table - financial year, primary source etc.
	 * @param bool = force update of module settings (true) or use stored data if exists (false)
	 * @return array|bool = array if settings found, else bool if not found
	 */
	public function getSettings($force=false) { //$force=true;//overwrite & always force for testing purposes
		//always get latest from DB if: force = true || session variable not set || timestamp expired || previous check returned false
		if($force===true
			|| !isset($_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp'])
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp']<time()
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data']===false
		) {
			$sql = "SELECT * FROM ".$this->getTableName()." A
					LEFT OUTER JOIN assist_menu_modules B
					ON A.mod_value = B.modref
					WHERE A.mod_status = 2 ORDER BY A.mod_insertdate DESC";
			$rows = $this->mysql_fetch_all($sql);
			if(count($rows)>0) {
				$data = array(
					'SETUP_COMPLETE'=>false,
					'FIN_YEAR'=>array('value'=>0),
					'ORG_SOURCE'=>false,
					'IKPI_SOURCE'=>false,
					'GROUPING'=>array('value'=>""),
					'PRIMARY'=>array('value'=>"ORG")
				);
				foreach($rows as $r) {
					if($r['mod_code']=="FIN_YEAR" && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($r['mod_value'])) {
						$masterObject = new ASSIST_MASTER_FINANCIALYEARS();
						$row = $masterObject->getAListItem($r['mod_value']);
						$r['mod_setting'] = $row;
						$r['modtext'] = $row['value'];
					}
					$data[$r['mod_code']] = array(
						'value'=>$r['mod_value'],
						'setting'=>$r['mod_setting'],
						'object_id'=>$r['mod_object_id'],
						'name'=>((isset($r['modtext']) && !is_null($r['modtext'])) ? $r['modtext'] : "")
					);
				}
				if(ASSIST_HELPER::checkIntRef($data['FIN_YEAR']['value']) && strlen($data['GROUPING']['value'])>0 && ($data['ORG_SOURCE']!==false || $data['IKPI_SOURCE']!==false)) {
					$data['SETUP_COMPLETE'] = true;
				}
			} else {
				$data = false;
			}
			$_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data'] = $data;
			$_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp'] = time()+15*60;	//set timestamp to check again in 15 minutes
			return $data;
		} else {
			return $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data'];
		}
	}



	/**
	 * getPrimaryKPISourcesAvailableForSelection() - get possible options as primary sources for KPI for module setup
	 * @param string|bool - either pass modref of module to be ignored (if calling from getSecondaryModuleOptions) otherwise false to indicate get all options
	 * @return array - array(key=>text)
	 */
	public function getPrimaryKPISourcesAvailableForSelection($exclude_modref=false, $limit_to_fin_year_id=false) {
		$data = array();

		foreach($this->section_names as $src => $name) {
			if(in_array($src,$this->source_sections) && isset($this->source_modules[$src]) && count($this->source_modules[$src])>0) {
				$mods = $this->source_modules[$src];
				$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$mods)."') AND modyn = 'Y' ";
				if($exclude_modref!==false) {
					$sql.=" AND modref <> '$exclude_modref'";
				}
				$sql.=" ORDER BY modtext";
				$rows = $this->mysql_fetch_all($sql);
				foreach($rows as $r) {
					if($limit_to_fin_year_id===false) {
						$data[$src."_".$r['modlocation']."_".$r['modref']] = $r['modtext']." (".$name.")";
					} else {
						$sub_objects = $this->getFinYearOptions(array('module'=>"SECOND_".$r['modlocation']."_".$r['modref']),$limit_to_fin_year_id);
						if(count($sub_objects)>0) {
							foreach($sub_objects as $i => $s) {
								$x = explode("_",$s['id']);
								$v = $s['value'];
								$data[$src."_".$r['modlocation']."_".$r['modref']."_".$x[0]] = $r['modtext']." [Plan: ".$v."]";
							}
						}
					}
				}
			}
		}

		return $data;
	}


	/**
	 * getFinYearOptions() - get possible financial year options from source module (called by setup_defaults.php)
	 * @param array['module']=>SRC_MODREF
	 * @return array[] => array('id'=>#,'value'=>SDBIP_NAME.(FinYEAR NAME)
	 */
	public function getFinYearOptions($var,$limit_by_finyear=false) {
		$data = array();
		$source_module = $var['module'];
		$source = explode("_",$source_module);
		$modlocation = $source[1];
		$modref = $source[2];

		switch($modlocation) {
			case "IKPI2":
				$extObject = new IKPI2_INTERNAL($modref);
				break;
			case "SDBP6":
				$extObject = new SDBP6_INTERNAL($modref);
				break;
		}

		$rows = $extObject->getMainObjectsForPerformanceModuleSetup($limit_by_finyear);

		unset($extObject);
		return $rows;
	}



	public function getSecondaryModuleOptions($var) {
		$primary_module = $var['primary'];
		$source = explode("_",$primary_module);
		$modref = $source[2];
		$fin = $var['fin_year'];
		$f = explode("_",$fin);
		$fin_year_id = $f['1'];
		$rows = $this->getPrimaryKPISourcesAvailableForSelection($modref,$fin_year_id);
		//reformat to comply with JS on setup_defaults
		$data = array();
		if(count($rows)>0) {
			foreach($rows as $row_key => $row) {
				$data[] = array('id'=>$row_key,'value'=>$row);
			}
		}
		return $data;
	}


	/**
	 * getGroupingOptions - called from setup_defaults
	 * @param $var
	 * @return array
	 */
	public function getGroupingOptions($var) {
		//Process Primary - required
		$primary = $var['primary'];
		$p = explode("_",$primary);
		$primary_type = $p[0];
		$primary_modloc = $p[1];
		$primary_modref = $p[2];
		switch($primary_modloc) {
			case "IKPI2":
				$extObject = new IKPI2_INTERNAL($primary_modref);
				break;
			case "SDBP6":
				$extObject = new SDBP6_INTERNAL($primary_modref);
				break;
		}
		$primary_grouping_fields = $extObject->getAvailableHeadingsForPerformanceGrouping($this->available_grouping_fields);
		unset($extObject);

		//Process secondary if applicable
		$secondary = $var['secondary'];
		if($secondary=="X") {
			$secondary_type = false;
			$secondary_modloc = false;
			$secondary_modref = false;
			$final_data = $primary_grouping_fields;
		} else {
			$s = explode("_",$secondary);
			$secondary_type = $s[0];
			$secondary_modloc = $s[1];
			$secondary_modref = $s[2];
			switch($secondary_modloc) {
				case "IKPI2":
					$secExtObject = new IKPI2_INTERNAL($secondary_modref);
					break;
				case "SDBP6":
					$secExtObject = new SDBP6_INTERNAL($secondary_modref);
					break;
			}
			$secondary_grouping_fields = $secExtObject->getAvailableHeadingsForPerformanceGrouping(array_keys($primary_grouping_fields));
			unset($secExtObject);
			foreach($this->available_grouping_fields as $fld) {
				if(isset($secondary_grouping_fields[$fld]) && isset($primary_grouping_fields[$fld])) {
					if($primary_grouping_fields[$fld]==$secondary_grouping_fields[$fld]) {
						$final_data[$fld] = $primary_grouping_fields[$fld];
					} else {
						$final_data[$fld] = $primary_grouping_fields[$fld]." / ".$secondary_grouping_fields[$fld];
					}
				}
			}
		}
		foreach($final_data as $fld => $fd) {
			$data[] = array('id'=>$fld,'value'=>$fd);
		}
		return $data;
	}




	public function saveSettings($var) {
		$result = array("error","Oops! Something went very wrong.  Please try saving your settings again.");
		//raw
		/*$data = array(
			'SETUP_COMPLETE'=>array('value'=>false),
			'FIN_YEAR'=>array('value'=>0),
			'ORG_SOURCE'=>array('value'=>false),
			'IKPI_SOURCE'=>array('value'=>false),
			'GROUPING'=>array('value'=>""),
			'PRIMARY'=>array('value'=>"ORG")
		);*/
		$insert_data = array();
		//primary source
		$primary = $var['primary'];
		$p = explode("_",$primary);
		$primary_type = $p[0];
		$primary_modloc = $p[1];
		$primary_modref = $p[2];
		//fin year source
		$fin_year = $var['fin_year'];
		$f = explode("_",$fin_year);
		$fin_year_id = $f[1];
		$primary_object_source = $f[0];
		//secondary source
		$secondary = $var['secondary'];
		if($secondary=="X") {
			$secondary_type = false;
			$secondary_modloc = false;
			$secondary_modref = false;
			$secondary_object_id = false;
		} else {
			$s = explode("_", $secondary);
			$secondary_type = $s[0];
			$secondary_modloc = $s[1];
			$secondary_modref = $s[2];
			$secondary_object_id = $s[3];
		}
		//grouping
		$grouping = $var['grouping'];

		//process data
			//org_source
			//ikpi_source
			if($primary_type=="ORG") {
				$insert_data['ORG_SOURCE'] = array(
					'mod_value'=>$primary_modref,
					'mod_setting'=>$primary_modloc,
					'mod_object_id'=>$primary_object_source,
					'mod_raw'=>""
				);
				$insert_data['IKPI_SOURCE'] = array(
					'mod_value'=>"",
					'mod_setting'=>"",
					'mod_object_id'=>0,
					'mod_raw'=>""
				);
				if($secondary_type!==false) {
					$insert_data['IKPI_SOURCE'] = array(
						'mod_value'=>$secondary_modref,
						'mod_setting'=>$secondary_modloc,
						'mod_object_id'=>$secondary_object_id,
						'mod_raw'=>""
					);
				}
			} else {
				$insert_data['IKPI_SOURCE'] = array(
					'mod_value'=>$primary_modref,
					'mod_setting'=>$primary_modloc,
					'mod_object_id'=>$primary_object_source,
					'mod_raw'=>""
				);
				$insert_data['ORG_SOURCE'] = array(
					'mod_value'=>"",
					'mod_setting'=>"",
					'mod_object_id'=>0,
					'mod_raw'=>""
				);
				if($secondary_type!==false) {
					$insert_data['ORG_SOURCE'] = array(
						'mod_value'=>$secondary_modref,
						'mod_setting'=>$secondary_modloc,
						'mod_object_id'=>$secondary_object_id,
						'mod_raw'=>""
					);
				}
			}
			//fin_year
			$insert_data['FIN_YEAR'] = array(
				'mod_value'=>$fin_year_id,
				'mod_setting'=>"",
				'mod_object_id'=>0,
				'mod_raw'=>$fin_year,
			);
			//grouping
			$insert_data['GROUPING'] = array(
				'mod_value'=>$grouping,
				'mod_setting'=>$this->getGroupingFieldType($grouping),
				'mod_object_id'=>0,
				'mod_raw'=>$grouping,
			);
			//primary
			$insert_data['PRIMARY'] = array(
				'mod_value'=>$primary_type,
				'mod_setting'=>"",
				'mod_object_id'=>0,
				'mod_raw'=>$primary."|".$secondary,
			);
		//save to db
		foreach($insert_data as $key => $data) {
			$data['mod_code'] = $key;
			$data['mod_status'] = self::ACTIVE;
			$data['mod_insertuser'] = $this->getUserID();
			$data['mod_insertdate'] = date("Y-m-d H:i:s");

			$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($data);
			$this->db_insert($sql);
			$result = array("ok","Settings saved successfully.  The module is now ready for use.");
		}
		return $result;
	}


	/**
	 * Used to get the name of the user and the date that the module setup was completed to display in Setup > Defaults > Module Setup
	 * AA-676 [JC] 18 August 2021
	 */
	public function getModuleSetupLogDetailsForDisplay() {
		$sql = "SELECT * FROM ".$this->getTableName()." LIMIT 1";
		$row = $this->mysql_fetch_one($sql);

		$user_name_for_display = "";
		$user_id = $row['mod_insertuser'];
		//if super user then hack the log files to see who was logged in otherwise get the name from the timekeep table
		if($this->isAdminUser($user_id) || $this->isSupportUser($user_id)) {


			//ALERT!!! ALERT!!! MUST PASS WHERE_SUPER_USER_SQL with limit to SUPER USER IDs otherwise the returning data gets confused where there are multiple logs timestamped with the same time
			if($this->isAdminUser($user_id)) {
				//TODO - write code here to get support user id - needed to make the getSuperUserActivityLogs to work
				$support_user_id = $this->getSupportUserID();
				$where_super_user_sql = "(A.tkid = '0000' OR A.tkid = '".$support_user_id."')";
			} else {
				$where_super_user_sql = "(A.tkid = '0000' OR A.tkid = '".$user_id."')";
			}

			$tkReportObject = new TK_REPORT();
			$test_date = strtotime($row['mod_insertdate']);
			$super_user_activity_logs = $tkReportObject->getSuperUserActivityLogs(date("Y-m-d",$test_date),date("Y-m-d",$test_date),$where_super_user_sql);
			$found_log_time = 0;
			foreach($super_user_activity_logs as $key => $activity_log) {
				if($activity_log['tkid']==$user_id) {
					$super_user_activity_logs[$key]['date'] = date("d F Y H:i:s",$activity_log['date']);
					foreach($activity_log['activity'] as $log_date => $log_module) {
						if($log_module==$this->getModRef()) {
							if($log_date<$test_date && $log_date>$found_log_time) {
								$found_log_time = $log_date;
								$user_name_for_display = $activity_log['super_user']." [".$activity_log['user']."]";
							}
						}
					}
				}
				else {
					unset($super_user_activity_logs[$key]);
				}
			}



		} else {
			echo $user_id;
			$user_name_for_display = $this->getAUserName($user_id);
		}



		return array('user'=>$user_name_for_display,'time'=>date("d F Y H:i:s",strtotime($row['mod_insertdate'])));

	}


	/*
	public function getListOfAvailableModulesBySection($section=false) {
		if($section===false || strlen($section)==0) {
			$section = $this->getSectionTypes();
		} elseif(!is_array($section)) {
			$section = array($section);
		}

		$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modlocation, modref";
		$menu = $this->mysql_fetch_all_by_id2($sql,"modlocation","modref");

		$modules = array();
		foreach($section as $sec) {
			$mod_loc = $this->source_modules[$sec];
			foreach($mod_loc as $ml) {
				if(isset($menu[$ml])) {
					foreach($menu[$ml] as $mr => $m) {
						$modules[$sec][$ml."_".$mr] = $m;
					}
				} else {
					//do nothing now just in case another module loc is available
				}
			}
			if(!isset($modules[$sec]) || count($modules[$sec])==0) {
				$modules[$sec] = false;
			}
		}

		return $modules;
	}



















	public function updateObject($var) {
		$fin_year = $var['fin_year'];
		$use = $var['use'];
		$source = $var['src'];

		$sql = "UPDATE ".$this->getTableName()." SET mod_status = 0";
		$this->db_update($sql);

		$sql = "INSERT INTO ".$this->getTableName()." SET
				mod_section = 'FIN_YEAR',
				mod_use = 0,
				mod_source = ".$fin_year.",
				mod_primary = 0,
				mod_status = 2,
				mod_insertuser = '".$this->getUserID()."',
				mod_insertdate = now()";
		$this->db_insert($sql);

		foreach($use as $sec => $u) {
			$src = $u==true && isset($source[$sec]) ? $source[$sec] : "";
			$prim = $sec=="ORG" ? 1 : 0;
			$sql = "INSERT INTO ".$this->getTableName()." SET
				mod_section = '".$sec."',
				mod_use = ".$u.",
				mod_source = '".$src."',
				mod_primary = ".$prim.",
				mod_status = 2,
				mod_insertuser = '".$this->getUserID()."',
				mod_insertdate = now()";
			$this->db_insert($sql);
		}

		$changes = array();
		$changes['response']="New module settings saved.";
		$changes['raw']=base64_encode(serialize($var));
		$changes['user']=$this->getUserName();

		$log_var = array(
			'section'	=> self::LOG_SECTION,
			'object_id'	=> "",
			'changes'	=> $changes,
			'log_type'	=> PM6_LOG::EDIT,
		);
		$this->addActivityLog("setup", $log_var);


		return array("ok","Settings saved successfully.");
	}
*/










}