<?php
/**
 * To manage the PM6:ASSESSMENT PER GROUP object
 *
 * Created on: July 2020 / April 2021
 * Authors: Janet Currie
 * AA-406 / AA-581
 */

class PM6_ASSESSMENT_GROUP extends PM6 {

	protected $object_id = 0;
	protected $object_details = array();

	private $group_types_sources = array("job_loc", "job_dept", "job_title", "job_level", "job_skill", "job_manager", "job_manager_title", "job_leveltwo", "job_team");

	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_type";
	protected $owner_field = "_item";
	protected $name_field = "_name";
	/*
	protected $progress_status_field = "_status_id";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "GROUP";
	const TABLE = "groups";
	const TABLE_FLD = "grp";
	const REFTAG = "SAG";
	const LOG_TABLE = "groups";


	/**
	 * STATUS CONSTANTS
	 */


	public function __construct($object_id = 0) {
		parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		if($object_id > 0) {
			$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);
			//$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
	}

	/*******************************************
	 * CONTROLLER FUNCTIONS
	 */


	/**
	 * Add a new group
	 * @param $var
	 * @return array
	 */
	public function addObject($var) {
		$result = array("info", "Oops, I got stuck on PM6AG.".__LINE__);


		$insert_data = array(
			'grp_name' => $var['grp_name'],
			'grp_type' => $var['grp_type'],
			'grp_item' => $var['grp_item'],
			'grp_status' => self::ACTIVE,
			'grp_insertdate' => date("Y-m-d H:i:s"),
			'grp_insertuser' => $this->getUserID()
		);

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$id = $this->db_insert($sql);

		foreach($var['gs_scd'] as $scd_id => $v) {
			$sub_insert_data = array(
				'gs_grpid' => $id,
				'gs_scdid' => $scd_id,
				'gs_status' => self::ACTIVE
			);
			$sql = "INSERT INTO ".$this->getTableName()."_scorecards SET ".$this->convertArrayToSQLForSave($sub_insert_data);
			$this->db_insert($sql);
		}

		$result = array("ok", "Group ".self::REFTAG.$id." created successfully.", $id);

		//TODO - Log group creation

		return $result;
	}

	public function addScorecard($var) {
		$object_id = $var['object_id'];
		$scd_ids = $var['gs_scd'];
		if(count($scd_ids)>0) {
			$sql = "SELECT * FROM ".$this->getTableName()."_scorecards WHERE gs_status = ".self::DELETED." AND gs_grpid = ".$object_id." AND gs_scdid IN (".implode(",",array_keys($scd_ids)).")";
			$deleted_scds = $this->mysql_fetch_all_by_id($sql,"gs_scdid");
			foreach($scd_ids as $scd_id => $v) {
				if(isset($deleted_scds[$scd_id])) {
					$i = $deleted_scds[$scd_id]['gs_id'];
					$sql = "UPDATE ".$this->getTableName()."_scorecards SET gs_status = ".self::ACTIVE." WHERE gs_id = ".$i;
					$this->db_update($sql);
				} else {
					$sub_insert_data = array(
						'gs_grpid' => $object_id,
						'gs_scdid' => $scd_id,
						'gs_status' => self::ACTIVE
					);
					$sql = "INSERT INTO ".$this->getTableName()."_scorecards SET ".$this->convertArrayToSQLForSave($sub_insert_data);
					$this->db_insert($sql);
				}
			}
			return array("ok","Scorecards added successfully.");
		}
		return array("info","No new scorecards found to be added.");
	}

	/**
	 * Edit a group
	 * @param array $var
	 * @param array $attach - unused in this instance but kept for compatibility with other objects
	 */
	public function editGroupObject($var,$attach=array()) {
		$group_id = $var['object_id'];
		$new_name = $var['name'];	//CAN'T BE coded for DB before being sent via AJAX - resulting & gets dropped in urlencode format
		$new_name = ASSIST_HELPER::code($new_name);
		$old = $this->getRawObject($group_id);
		if($old[$this->getNameFieldName()]!==$new_name) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getNameFieldName()." = '$new_name' WHERE ".$this->getIDFieldName()." = ".$group_id;
			$this->db_update($sql);
			//TODO - log change to group name
			return array("ok","Group name saved successfully.",$new_name);
		}
		return array("info","No changes found to be saved.",$new_name);
	}


	/**
	 * Delete a group
	 * @param array $var
	 */
	public function deleteObject($var) {
		$group_id = $var['object_id'];
		//delete group
		$old = $this->getRawObject($group_id);
		$new_status = $old[$this->getStatusFieldName()]-self::ACTIVE+self::DELETED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = '$new_status' WHERE ".$this->getIDFieldName()." = ".$group_id;
		$this->db_update($sql);
		//TODO - log deletion

		//delete link between scorecard & group
		$sql = "UPDATE ".$this->getTableName()."_scorecards SET gs_status = (gs_status-".self::ACTIVE."+".self::DELETED.") WHERE gs_grpid = ".$group_id;
		$this->db_update($sql);

		return array("ok","Group deleted successfully.");
	}

	/**
	 * Remove Scorecard from group
	 * @param array $var
	 */
	public function removeScorecard($var) {
		$group_id = $var['object_id'];
		$scd_id = $var['scd_id'];
		$sql = "UPDATE ".$this->getTableName()."_scorecards SET gs_status = (gs_status-".self::ACTIVE."+".self::DELETED.") WHERE gs_grpid = ".$group_id." AND gs_scdid = ".$scd_id;
		$this->db_update($sql);
		//TODO - log removal of scorecard
		return array("ok","Scorecard removed successfully.");
	}

	/**
	 * Trigger a new assessment on a group of scorecards
	 * @param array $var
	 */
	public function addAssessment($var) {
		$var_for_scd_assessment_add = $var;
		$group_id = $var['scd_id'];
		$period_id = $var['period'];
		//Add groups_assessment record
		$sql = "INSERT INTO ".$this->getTableName()."_assessments SET
		gasmt_grpid = ".$group_id."
		, gasmt_periodid = ".$period_id."
		, gasmt_status = ".self::ACTIVE."
		, gasmt_insertuser = '".$this->getUserID()."'
		, gasmt_insertdate = now()
		";
		$id = $this->db_insert($sql);

		//get list of linked scorecards
		$linked_scorecards = $this->getScorecardsAlreadyInAGroup($group_id,true);
		//foreach scorecard, create assessment & trigger
		$asmtObject = new PM6_ASSESSMENT();
		foreach($linked_scorecards as $scd_id => $scorecard) {
			//create assessment
			$scd_var = $var_for_scd_assessment_add;
			$scd_var['scd_id'] = $scd_id;
			$scd_var['tkid'] = $scorecard['employee_tkid'];
			$asmt_result = $asmtObject->addObject($scd_var);
			$asmt_id = $asmt_result[2];
			//record link between gasmt & scd-asmt
			$sql = "INSERT INTO ".$this->getTableName()."_assessments_scorecards SET 
					gas_gasmtid = ".$id.",
					gas_asmtid = ".$asmt_id.",
					gas_status = ".self::ACTIVE.",
					gas_insertuser = '".$this->getUserID()."',
					gas_insertdate = now()";
			$this->db_insert($sql);
		}
		return array("ok","Group Assessments have been triggered.");
	}


	/**
	 * Trigger next phase of an existing assessment on a group of scorecards
	 * @param array $var
	 */
	public function addTrigger($var) {
		$var_for_scd_assessment_add = $var;
		$calling_assess_id = $var['assess_id'];
		//get parent group assess id
		$sql = "SELECT * FROM ".$this->getTableName()."_assessments_scorecards WHERE gas_asmtid = ".$calling_assess_id;
		$calling_assess_row = $this->mysql_fetch_one($sql);
		$group_assess_id = $calling_assess_row['gas_gasmtid'];
		//get linked scorecard assess id
		$sql = "SELECT gas_asmtid as scd_asmt_id FROM ".$this->getTableName()."_assessments_scorecards WHERE gas_gasmtid = ".$group_assess_id;
		$linked_assess_id = $this->mysql_fetch_all_by_value($sql,"scd_asmt_id");

		//foreach scorecard, create assessment & trigger
		$triggerObject = new PM6_TRIGGER();
		foreach($linked_assess_id as $key => $assess_id) {
			//create assessment
			$scd_var = $var_for_scd_assessment_add;
			$scd_var['assess_id'] = $assess_id;
			$asmt_result = $triggerObject->addObject($scd_var);
		}
		return array("ok","Group Assessments have been triggered.");
	}


	/**
	 * Trigger next phase of an existing assessment on a group of scorecards
	 * @param array $var
	 */
	public function deleteTrigger($var) {
		$var_for_scd_assessment_add = $var;
		$calling_assess_id = $var['assess_id'];
		$trigger_type = $var['type'];
		//get parent group assess id
		$sql = "SELECT * FROM ".$this->getTableName()."_assessments_scorecards WHERE gas_asmtid = ".$calling_assess_id;
		$calling_assess_row = $this->mysql_fetch_one($sql);
		$group_assess_id = $calling_assess_row['gas_gasmtid'];
		//get linked scorecard assess id
		$sql = "SELECT gas_asmtid as scd_asmt_id FROM ".$this->getTableName()."_assessments_scorecards WHERE gas_gasmtid = ".$group_assess_id;
		$linked_assess_id = $this->mysql_fetch_all_by_value($sql,"scd_asmt_id");

		//foreach scorecard, create assessment & trigger
		$triggerObject = new PM6_TRIGGER();
		foreach($linked_assess_id as $key => $assess_id) {
			//create assessment
			$scd_var = $var_for_scd_assessment_add;
			$scd_var['assess_id'] = $assess_id;
			$asmt_result = $triggerObject->deleteObject($scd_var);
		}
		//if Self then delete group assessments as well
		if($trigger_type==$triggerObject->getSelfType()) {
			$sql = "UPDATE ".$this->getTableName()."_assessments_scorecards SET gas_status = ".self::INACTIVE." WHERE gas_gasmtid = ".$group_assess_id;
			$this->db_update($sql);
			$sql = "UPDATE ".$this->getTableName()."_assessments SET gasmt_status = ".self::INACTIVE." WHERE gasmt_id = ".$group_assess_id;
			$this->db_update($sql);
		}
		return array("ok","Group Triggers have been deleted.");
	}



	/*************************************
	 * GET functions
	 * */

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getRootTableName() {
		return $this->getDBRef();
	}

	public function getRefTag() {
		return self::REFTAG;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getRawObject($object_id) {
		if($this->checkIntRefForWholeIntegersOnly($object_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $object_id";
			$row = $this->mysql_fetch_one($sql);
		} else {
			$row = array();
		}
		return $row;
	}

	public function getGroupTypesFormattedForSelect() {
		$empObject = new PM6_EMPLOYEE();
		$headings = $empObject->getHeadings($this->group_types_sources);
		$data = array();
		foreach($headings as $fld => $head) {
			$data[$fld] = $head['name'];
		}
		asort($data);
		$data['CUSTOM'] = "Custom Group Type";
		return $data;
	}

	public function getGroupItems($var) {
		if(is_array($var)) {
			$type = $var['group_type'];
		} else {
			$type = $var;
		}
		if($type=="CUSTOM") {
			$data = array("0"=>"Custom Group");
		} else {
			$empObject = new PM6_EMPLOYEE();
			$data = $empObject->getListItems($type);
		}
		return $data;
	}

	public function getActiveGroups($include_groups_being_built = true) {
		$headings = $this->getGroupTypesFormattedForSelect();
		$items = array();
		foreach($headings as $fld => $head) {
			$items[$fld] = $this->getGroupItems($fld);
		}
		$sql = "SELECT * FROM ".$this->getTableName()." G 
				WHERE ".$this->getActiveStatusSQL("G")." 
				".($include_groups_being_built !== true ? "AND ".$this->checkStatusSQL("G", PM6::BUILDING, false) : "")." 
				ORDER BY grp_type, grp_item";
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$sql = "SELECT gs_grpid as group_id, count(gs_scdid) as incl_scd 
				FROM ".$this->getTableName()."_scorecards GS
				WHERE (gs_status & ".self::ACTIVE.") = ".self::ACTIVE."
				GROUP BY gs_grpid";
		$incl_scd = $this->mysql_fetch_value_by_id($sql, "group_id", "incl_scd");
		$sql = "SELECT gasmt_grpid as group_id, count(gasmt_periodid) as assessments 
				FROM ".$this->getTableName()."_assessments GS
				WHERE (gasmt_status & ".self::ACTIVE.") = ".self::ACTIVE."
				GROUP BY gasmt_grpid";
		$triggered_assessments = $this->mysql_fetch_value_by_id($sql, "group_id", "assessments");
		$data = array();
		foreach($rows as $id => $row) {
			$data[$id] = $row;
			$data[$id]['id'] = $id;
			$data[$id]['ref'] = $this->getFullRefTag().$id;
			$data[$id]['group_name'] = $row['grp_name'];
			$data[$id]['group_type'] = $row['grp_type'];
			$data[$id]['group_item'] = $row['grp_item'];
			$data[$id]['group_type_text'] = $headings[$row['grp_type']];
			$data[$id]['group_item_text'] = $items[$row['grp_type']][$row['grp_item']];
			$data[$id]['is_active'] = (($data[$id]['grp_status'] & PM6::ACTIVE) == PM6::ACTIVE);
			$data[$id]['is_building'] = (($data[$id]['grp_status'] & PM6::BUILDING) == PM6::BUILDING);
			$data[$id]['status'] = $data[$id]['is_building'] ? "Creation In Progress" : "Active";
			$data[$id]['included_scorecards'] = isset($incl_scd[$id]) ? $incl_scd[$id] : 0;
			$data[$id]['triggered_assessments'] = isset($triggered_assessments[$id]) ? $triggered_assessments[$id] : 0;
		}
		return $data;
	}

	public function getFullGroupDetails($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$data = $this->mysql_fetch_one($sql);
		$headings = $this->getGroupTypesFormattedForSelect();
		$data['group_name'] = $data['grp_name'];
		$data['group_type'] = $data['grp_type'];
		$data['group_type_text'] = $headings[$data['grp_type']];
		$items = $this->getGroupItems($data['grp_type']);
		$data['group_item'] = $data['grp_item'];
		$data['group_item_text'] = $items[$data['grp_item']];
		return $data;
	}

	public function getPossibleGroupChildren($group_type = "", $group_item = "") {
		$local_modref = $this->getModRef();
		$return = array(
			'available' => array(),
			'not_available' => array(),
		);

		$empObject = new PM6_EMPLOYEE();
		$data = $empObject->getJobsFilteredByGroup($group_type, $group_item);

		if(count($data) > 0) {
			$job_ids = array();
			foreach($data as $key => $datum) {
				$job_ids[$key] = $datum['job_id'];
			}
			$flipped_jobs = array_flip($job_ids);
			$this->setDBRef($local_modref);

			$scdObject = new PM6_SCORECARD();
			$sql = "SELECT * FROM ".$scdObject->getTableName()." S 
					WHERE ".$scdObject->getNameFieldName()." IN (".implode(",", $job_ids).")
					AND ".$scdObject->getActiveStatusSQL("S");
			$all_scorecards = $this->mysql_fetch_all_by_id($sql, $scdObject->getIDFieldName());

			if(count($all_scorecards) > 0) {
				$scd_ids = array_keys($all_scorecards);
				/* GET SCORECARDS ALREADY INCLUDED IN A GROUP */
				$grouped_scorecards = $this->getScorecardsAlreadyInAGroup();

				/* GET SCORECARDS ALREADY WITH ASSESSMENTS TRIGGERED */
				$asmtObject = new PM6_ASSESSMENT();
				$sql = "SELECT * FROM ".$asmtObject->getTableName()." A 
						WHERE ".$asmtObject->getParentFieldName()." IN (".implode(",", $scd_ids).")
						AND ".$asmtObject->getActiveStatusSQL("A");
				$assessments = $this->mysql_fetch_all_by_id($sql, $asmtObject->getParentFieldName());
$sorting_array = array('available'=>array(),'not_available'=>array());
				foreach($all_scorecards as $scd_id => $scd) {
					$job_id = $scd[$scdObject->getNameFieldName()];
					if(isset($assessments[$scd_id]) || isset($grouped_scorecards[$scd_id])) {
						$return['not_available'][$scd_id] = $scd;
						$return['not_available'][$scd_id]['ref'] = $scdObject->getFullRefTag().$scd_id;
						$return['not_available'][$scd_id]['employee_name'] = $data[$flipped_jobs[$job_id]]['user_name'];
						$return['not_available'][$scd_id]['job'] = $data[$flipped_jobs[$job_id]];
						$sorting_array['not_available'][$scd_id] = $this->formatTextForSorting($data[$flipped_jobs[$job_id]]['user_name']);
					} else {
						$return['available'][$scd_id] = $scd;
						$return['available'][$scd_id]['ref'] = $scdObject->getFullRefTag().$scd_id;
						$return['available'][$scd_id]['employee_name'] = $data[$flipped_jobs[$job_id]]['user_name'];
						$return['available'][$scd_id]['job'] = $data[$flipped_jobs[$job_id]];
						$sorting_array['available'][$scd_id] = $this->formatTextForSorting($data[$flipped_jobs[$job_id]]['user_name']);
					}
				}
				//Sort Available SCDs by Employee Name
				asort($sorting_array['available']);
				$return_available = $return['available'];
				$return['available'] = array();
				foreach($sorting_array['available'] as $key => $v) {
					$return['available'][$key] = $return_available[$key];
				}
				//Sort Not Available SCDs by Employee Name
				asort($sorting_array['not_available']);
				$return_notavailable = $return['not_available'];
				$return['not_available'] = array();
				foreach($sorting_array['not_available'] as $key => $v) {
					$return['not_available'][$key] = $return_notavailable[$key];
				}
			}
		}

		return $return;
	}
	/**
	 * Get list of Scorecards already allocated to a group (group not specified OR group_id provided) [AA-581]
	 * @return array(scd_id=>scd_id);
	 */
	public function getScorecardsAlreadyInAGroup($group_id=false,$include_scd=false) {
		if($include_scd) {
			$scdObject = new PM6_SCORECARD();
			$select = "gs_scdid, SCD.*, SCD.".$scdObject->getTableField()."_tkid as employee_tkid";
		} else {
			$select = "gs_scdid";
		}
		$sql = "SELECT ".$select."
				FROM ".$this->getTableName()."_scorecards GS 
				INNER JOIN ".$this->getTableName()." G
				ON G.grp_id = GS.gs_grpid 
				".($include_scd?"INNER JOIN ".$scdObject->getTableName()." SCD ON GS.gs_scdid = SCD.".$scdObject->getIDFieldName():"")."
				WHERE (gs_status & ".self::ACTIVE.") = ".self::ACTIVE."
				".($group_id!==false?" AND ".$this->getIDFieldName()." = ".$group_id:"")."
				AND ".$this->getActiveStatusSQL("G");
		if($include_scd) {
			$grouped_scorecards = $this->mysql_fetch_all_by_id($sql,"gs_scdid");
		} else {
			$grouped_scorecards = $this->mysql_fetch_value_by_id($sql,"gs_scdid","gs_scdid");
		}
		return $grouped_scorecards;
	}
	/**
	 * Get list of Scorecards already allocated to a group (group name returned) [AA-581]
	 * @return array(scd_id=>scd_id);
	 */
	public function getScorecardsAlreadyInAGroupWithGroupName() {
		$sql = "SELECT gs_scdid, CONCAT(grp_name,' [".$this->getRefTag()."',grp_id,']') as grp_name 
				FROM ".$this->getTableName()."_scorecards GS 
				INNER JOIN ".$this->getTableName()." G
				ON G.grp_id = GS.gs_grpid
				WHERE (gs_status & ".self::ACTIVE.") = ".self::ACTIVE."
				AND ".$this->getActiveStatusSQL("G");
		$grouped_scorecards = $this->mysql_fetch_value_by_id($sql,"gs_scdid","grp_name");
		return $grouped_scorecards;
	}


	/**
	 * Get the assessments which have been triggered for the group
	 * @param $parent_id - group id
	 * @return array
	 */
	public function getAssessmentsForEditPerGroup($parent_id) {
		//get a single active scd in the group
		$scorecards = $this->getScorecardsAlreadyInAGroup($parent_id);
		if(count($scorecards)>0) {
			$scd_ids = array_keys($scorecards);
			$scd_id = $scd_ids[0];
			//get the triggers for that scd from PM6_ASSESSMENT
			$asmtObject = new PM6_ASSESSMENT();
			$triggers = $asmtObject->getAssessmentsForEdit($scd_id);
			//return that to the calling page
			return $triggers;
		} else {
			return array();
		}
	}



	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted
	 */
	private function getActiveStatusSQL($t = "") {
		//Object where
		//status = active and
		//status <> deleted
		$sql = " ( ( ".(strlen($t) > 0 ? $t."." : "").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
		 		AND  ( ".(strlen($t) > 0 ? $t."." : "").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE." 
		 		AND  ( ".(strlen($t) > 0 ? $t."." : "").$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED." )";
		return $sql;
	}

	private function checkStatusSQL($table_alias, $status_to_check, $check_if_and = true) {
		$sql = "( ( ".(strlen($table_alias) > 0 ? $table_alias."." : "").$this->getStatusFieldName()." & ".$status_to_check.") ".($check_if_and ? "=" : "<>")." ".$status_to_check.")";
		return $sql;
	}

}


?>