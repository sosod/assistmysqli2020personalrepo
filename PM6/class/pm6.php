<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class PM6 extends PM6_HELPER {
	/***
	 * Module Wide variables
	 */
	protected $weighted_score_rounding_precision = 3;
	protected $object_form_extra_js = "";

	protected $deliverable_types = array('DEL'  => "Deliverable With Actions",
										 'MAIN' => "Deliverable With Sub-Deliverables",
										 'SUB'  => "Sub-Deliverable With Actions",);

	protected $kpa_field;

	protected $copy_function_protected_fields = array("del_type", "del_parent_id");
	protected $copy_function_protected_heading_types = array("ATTACH", "DEL_TYPE");

	protected $has_target = false;

	protected $bonus_scale_updated_from_central_class = false;
	protected $kpa_weight = 0.8;
	protected $max_score = 5;
	protected $mid_score = 3;
	protected $default_score = 3;
	protected $min_score = 0;
	protected $bonus_scale = array('0'     => 0,
								   '1.30'  => 5,
								   '1.339' => 67,
								   '1.377' => 7,
								   '1.415' => 8,
								   '1.453' => 9,
								   '1.50'  => 10,
								   '1.535' => 11,
								   '1.569' => 12,
								   '1.603' => 13,
								   '1.637' => 14,);
	protected $default_bonus_scale = array('0'     => 0,
										   '1.30'  => 5,
										   '1.339' => 6,
										   '1.377' => 7,
										   '1.415' => 8,
										   '1.453' => 9,
										   '1.50'  => 10,
										   '1.535' => 11,
										   '1.569' => 12,
										   '1.603' => 13,
										   '1.637' => 14,);
	protected $bonus_scale_headings = array('bonus'  => "Bonus",
											'start'  => "% Performance",
											'status' => "In Use",);
	protected $bonus_max = 14;

	protected $sources = array();
	protected $jal_sources = array();
	protected $primary_source = "";
	protected $jal_primary_source = false;
	protected $src_ref = "";

	protected $line_weight_precision_allowed = 4;
	protected $score_precision_allowed = 3;

	protected $module_settings;
	protected $types = array("KPI","TOP","PROJ");
	protected $names = array('KPI'=>"Operational Indicators",'TOP'=>"Strategic Indicators",'PROJ'=>"Capital Indicators");

	protected $steps = array("",
							 "Select Operational Indicators",
							 "Select Strategic Indicators",
							 "Select Capital Indicators",
							 "Weight Indicators",
							 "Select Core Competencies",
							 "Weight Core Competencies",
							 "Select Job Activities",
							 "Weight Job Activities",
							 "Weight Components",
							 "Review",);
	protected $ip_statuses = array();

	/**
	 * Module Wide Constants
	 * */
	const CC_REFTAG = "CC";

	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;


	/**
	 * CORE STATUSES
	 */
	const BUILDING = 256;
	const EDIT = 512;
	const MANAGE = 1024;
	const CONFIRMED = 32;
	const ACTIVATED = 64;
	const TRIGGERED = 2048;
	const NOT_YET_TRIGGERED = 4096;
	const CHANGES_WAITING_REVIEW = 8192;	//changes after activation


	/**
	 * PROCESS STATUS
	 */
	const NOT_YET_STARTED = 0;
	const IP_SUPER = 16;
	const IP_MANAGER = 32;
	const IP_EMPLOYEE = 64;
	const IP_CHAMPION = 128;
	const AWAITING_APPROVAL = 2**8;//256
	const APPROVED = 2**9;//512
	const EMPLOYEE_DIGITALLY_APPROVED = 2**10;//1024;
	const EMPLOYEE_REJECTED = 2**11;//2048;
	const EMPLOYEE_SIGN_APPROVED = 2**12;//4096;
	const EMPLOYEE_SIGN_ATTACHED = 2**13;//8192;
	const EMPLOYEE_APPROVED_NO_POE = 2**14;//16384;
	const MANAGER_DIGITALLY_APPROVED = 2**15;//32768;
	const MANAGER_REJECTED = 2**16;//65536;
	const MANAGER_SIGN_APPROVED = 2**17;//131082;
	const MANAGER_SIGN_ATTACHED = 2**18;//262144;
	const MANAGER_APPROVED_NO_POE = 2**19;//524288;



	/**
	 * SETUP > EMPLOYEE_SETTINGS
	 */
	const OWN = 512;
	const STAFF_USERS = 1024;
	const STAFF_NON = 2048;

	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref="") {
		parent::__construct($modref);
		unset($this->steps[0]);
		$this->checkSettings();
		$this->ip_statuses['employee'] = self::IP_EMPLOYEE;
		$this->ip_statuses['manager'] = self::IP_MANAGER;
		$this->ip_statuses['champion'] = self::IP_CHAMPION;
		$this->ip_statuses['super_user'] = self::IP_SUPER;
	}

	private function checkSettings() {
		if(!isset($_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp'])
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['timestamp']<time()
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data']===false
			|| $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data']['SETUP_COMPLETE']===false
		) {
			$setupObject = new PM6_SETUP_MODULE($this->getLocalModRef());
			$this->module_settings = $setupObject->getSettings(true);
		} else {
			$this->module_settings = $_SESSION[$this->getLocalModRef()]['SETUP_MODULE']['data'];
		}
	}

	public function getWeightedScoreRoundingPrecision() {
		return $this->weighted_score_rounding_precision;
	}

	/**
	 * Get Assist Status in Words
	 * AA-128 JC 1 April 2021
	 */
	public function getAssistStatusInWords($a) {
		switch($a) {
			case 1:
			case "1":
			case true:
				return "Active Assist User";
				break;
			default:
				return "Non-Assist User";
				break;
		}
	}

	/*******
	 * Functions to manage Bonus Scale
	 * Added by JC in March 2018 - based on Custom Result Settings in SDBP5B_HELPER
	 *
	 * Partially MADE OBSOLETE BY CHANGES IMPLEMENTED IN May 2019 - JC + TM - AA-90 + AA-106
	 *
	 */

	public function getDefaultBonusScale() {
		return $this->default_bonus_scale;
	}

	public function getBonusScaleHeadings() {
		return $this->bonus_scale_headings;
	}

	public function getBonusMax() {
		return $this->bonus_max;
	}

	public function canICustomBonusScale() {/*
		$sql = "SHOW TABLES LIKE '".$this->getDBRef()."_setup_bonus_scale'";
		$check_for_result_settings_table = $this->mysql_fetch_all($sql);
		if(count($check_for_result_settings_table)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_setup_bonus_scale";
			$rows = $this->mysql_fetch_all($sql);
			if(count($rows)>0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}*/
		return true;
	}

	public function getCustomBonusScaleDirectlyFromDatabase($for_editing = false) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_bonus_scale ".(!$for_editing ? " WHERE status=2" : "")." ORDER BY bonus";
		$data = $this->mysql_fetch_all_by_id($sql, "bonus");
		if($for_editing) {
			$bonus_scale = $data;
		} else {
			$bonus_scale = $this->convertBonusScaleFromEditToView($data);
		}
		return $bonus_scale;
	}

	public function getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, $for_editing = false) {
		$sql = "SELECT * 
				FROM ".$this->getDBRef()."_setup_bonus_scale ".(!$for_editing ? " WHERE status=2" : "").(!$for_editing ? " AND" : " WHERE")." bonus_scale_id = $bonus_scale_id ORDER BY start";
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		if($for_editing) {
			$bonus_scale = $data;
		} else {
			$bonus_scale = $this->convertBonusScaleFromEditToView($data);
		}
		return $bonus_scale;
	}

	protected function convertBonusScaleFromEditToView($result_settings) {
		$bonus_scale = array();
		foreach($result_settings as $i => $setting) {
			if($setting['status'] > 0) {
				$key = (($setting['start']) * 1) / 100;
				$bonus = $setting['bonus'];
				$bonus_scale[strval($key)] = $bonus;
			}
		}
		return $bonus_scale;
	}

	public function saveCustomBonusScale($var) {
		$result = array("error", "An error occured while trying to save your update.  Please try again.");
//return array('result_message'=>$result);
		$old = $this->getCustomBonusScaleDirectlyFromDatabase(true);
		$result_settings = $old;
		$changes = array();

		foreach($result_settings as $i => $setting) {
			if((int)$i != 0) {
				$key = "status";
				$var[$key][$i] *= 2;
				if($var[$key][$i] != $result_settings[$i][$key]) {
					$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
					$result_settings[$i][$key] = $var[$key][$i];
				}
				if($var[$key][$i] > 0) {
					$key = "start";
					if($var[$key][$i] != $result_settings[$i][$key]) {
						$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
					}
				}
			}
		}

		foreach($result_settings as $i => $setting) {
			$id = $setting['id'];
			$key = "start";
			$value = $setting[$key];
			$setting[$key] = ASSIST_HELPER::code($value);

			$sql = "UPDATE ".$this->getDBRef()."_setup_bonus_scale SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = ".$id;
			$this->db_update($sql);

			//LOG CHANGES!!!
			if(isset($changes[$i]) && count($changes[$i]) > 0) {
				foreach($changes[$i] as $key => $value) {
					if($key == "start") {
						$v_log = array('text' => "Updated ".$i."% Bonus percentage performance bracket to start at <i>".number_format($value['new'], 2)."%</i> from <i>".number_format($value['old'], 2)."%</i>.",
									   'old'  => $value['old'],
									   'new'  => $value['new'],);
					} else {
						if($value['old'] == 0) {
							$v_log = array('text' => "Activated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						} else {
							$v_log = array('text' => "Deactivated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						}
					}
					//$this->logChanges('SETUP',0,$v_log,code($sql));
					$c = array('response' => $v_log['text'],);
					$log_changes = array_merge(array('user' => $this->getUserName()), $c);
					$log_var = array('section'   => "BS",
									 'object_id' => $id,
									 'changes'   => $log_changes,
									 'log_type'  => PM6_LOG::EDIT,);
					$this->addActivityLog("setup", $log_var);

				}
			}
		}

		//update session
		$this->updateMyActiveBonusScale($result_settings, true);

		$result = array("ok",
						"Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message' => $result, 'bonus_scale' => $result_settings);
	}

	public function saveCustomBonusScaleListItemByBonusScaleID($var) {
		$bonus_scale_id = $var['bonus_scale_id'];
		$old = $this->getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, true);
		$result_settings = $old;
		$changes = array();

		foreach($result_settings as $i => $setting) {
			$key = "status";
			if(in_array($i, $var[$key])) {
				$var[$key][$i] *= 2;
				if($var[$key][$i] != $result_settings[$i][$key]) {
					$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
					$result_settings[$i][$key] = $var[$key][$i];
				}
				if($var[$key][$i] > 0) {
					$key = "start";
					if($var[$key][$i] != $result_settings[$i][$key]) {
						$changes[$i][$key] = array('old' => $result_settings[$i][$key], 'new' => $var[$key][$i]);
						$result_settings[$i][$key] = $var[$key][$i];
					}
				}
			}
		}

		foreach($result_settings as $i => $setting) {
			$id = $setting['id'];
			$key = "start";
			$value = $setting[$key];
			$setting[$key] = ASSIST_HELPER::code($value);

			$sql = "UPDATE ".$this->getDBRef()."_setup_bonus_scale SET ".$this->convertArrayToSQLForSave($setting)." WHERE id = $id AND bonus_scale_id = $bonus_scale_id";
			$this->db_update($sql);

			//LOG CHANGES!!!
			if(isset($changes[$i]) && count($changes[$i]) > 0) {
				foreach($changes[$i] as $key => $value) {
					if($key == "start") {
						$v_log = array('text' => "Updated ".$i."% Bonus percentage performance bracket to start at <i>".number_format($value['new'], 2)."%</i> from <i>".number_format($value['old'], 2)."%</i>.",
									   'old'  => $value['old'],
									   'new'  => $value['new'],);
					} else {
						if($value['old'] == 0) {
							$v_log = array('text' => "Activated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						} else {
							$v_log = array('text' => "Deactivated ".$i."% Bonus percentage.",
										   'old'  => $value['old'],
										   'new'  => $value['new'],);
						}
					}
					//$this->logChanges('SETUP',0,$v_log,code($sql));
					$c = array('response' => $v_log['text'],);
					$log_changes = array_merge(array('user' => $this->getUserName()), $c);
					$log_var = array('section'   => "BS",
									 'object_id' => $id,
									 'changes'   => $log_changes,
									 'log_type'  => PM6_LOG::EDIT,);
					$this->addActivityLog("setup", $log_var);

				}
			}
		}

		//update session
		$this->updateMyActiveBonusScale($result_settings, true);

		$result = array("ok",
						"Your changes have been saved successfully.  Please note that the changes can take up to 30 minutes to update for all users.");
		return array('result_message' => $result, 'bonus_scale' => $result_settings);
	}

	public function updateMyActiveBonusScale($result_settings = array(), $convert = false) {
		if(count($result_settings) > 0) {
			$_SESSION[$this->getModRef()]['bonus_scale']['data'] = $convert ? $this->convertBonusScaleFromEditToView($result_settings) : $result_settings;
		} else {
			$_SESSION[$this->getModRef()]['bonus_scale']['data'] = $this->default_result_settings;
		}
		$_SESSION[$this->getModRef()]['bonus_scale']['expiration'] = time() + 30 * 60;
	}

	public function getMyActiveBonusScale($return = false) {
		//if the session has been set & the expiration time hasn't expired
		if(isset($_SESSION[$this->getModRef()]['bonus_scale']['expiration']) && $_SESSION[$this->getModRef()]['bonus_scale']['expiration'] > time() && isset($_SESSION[$this->getModRef()]['bonus_scale']['data'])) {
			//use the current settings
			$this->bonus_scale = $_SESSION[$this->getModRef()]['bonus_scale']['data'];
		} else {
			//check if table exists & is populated
			$check_for_result_settings_table = $this->canICustomBonusScale();
			if($check_for_result_settings_table) {
				//get from database
				$this->bonus_scale = $this->getCustomBonusScaleDirectlyFromDatabase();
				//update session
				$this->updateMyActiveBonusScale($this->bonus_scale, false);
			} else {
				//else use default
				$this->bonus_scale = $this->default_bonus_scale;
				//update session to prevent multiple database checks
				$this->updateMyActiveBonusScale($this->bonus_scale, false);
			}
		}
		if($return) {
			return $this->bonus_scale;
		}
	}









	public function getLineWeightPrecisionAllowed() { return $this->line_weight_precision_allowed; }
	public function getScorePrecisionAllowed() { return $this->score_precision_allowed; }
	public function getObjectTypes() { return $this->types; }
	public function getObjectNames() { return $this->names; }



	/**************************
	 * SDBIP functions
	 */


	/**
	 * Function to check the SESSION for sources of KPIs/Projects and update if expired
	 * Applicable to SDBIP modules
	 */
	private function checkKPASources($force=false) { //$force=true; //ASSIST_HELPER::arrPrint($this->module_settings);
		if($force || !isset($_SESSION[$this->getModRef()]['SOURCES']['KPA']) || $_SESSION[$this->getModRef()]['SOURCES']['KPA']['expiration']<time()) {
			$this->checkSettings();
			if($this->module_settings !== false) {
				$settings = $this->module_settings;
				//set sources
				if($settings['ORG_SOURCE'] != false && strlen($settings['ORG_SOURCE']['value'])>0) {
					$set = $settings['ORG_SOURCE'];
					$org_source = array('active' => true,
						'modref' => $set['value'],
						'modlocation' => $set['setting'],
						'modtext' => $set['name'],
						'modobject' => $set['object_id'],
						'type' => "ORG",
					);
					$this->sources[$set['value']] = $org_source;
				}
				if($settings['IKPI_SOURCE'] != false && strlen($settings['IKPI_SOURCE']['value']) > 0) {
					$set = $settings['IKPI_SOURCE'];
					$ikpi_source = array('active' => true,
						'modref' => $set['value'],
						'modlocation' => $set['setting'],
						'modtext' => $set['name'],
						'modobject' => $set['object_id'],
						'type' => "IKPI",
					);
					$this->sources[$set['value']] = $ikpi_source;
				}
				//set primary source
				switch($settings['PRIMARY']['value']) {
					case "ORG":
						$this->primary_source = $org_source['modref'];
						break;
					case "IKPI":
						$this->primary_source = $ikpi_source['modref'];
						break;
				}
				//kpa field
				$this->kpa_field = array('field'=>$settings['GROUPING']['value'],'type'=>$settings['GROUPING']['setting']);
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['data'] = $this->sources;
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['field'] = $this->kpa_field;
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['expiration'] = strtotime(date("d F Y H:i:s")) + (30 * 60);
				$_SESSION[$this->getModRef()]['SOURCES']['KPA']['primary'] = $this->primary_source;
			}
		} else {
			$this->sources = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['data'];
			$this->kpa_field = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['field'];
			$this->primary_source = $_SESSION[$this->getModRef()]['SOURCES']['KPA']['primary'];
		}
	}

	public function getKPAField() {

	}


	/**
	 * Function to retrieve KPI/Project sources
	 * Applicable to SDBIP modules
	 */
	public function getKPASources() {
		$this->checkKPASources();
		$data = array();
		foreach($this->sources as $src) {
			if($src['active']) {
				$data[] = $src;
			}
		}
		return $data;
	}

	/**
	 * Applicable to SDBIP modules
	 */
	public function getAKPASourceName($src) {
		$this->getKPASources();
		return $this->sources[$src]['modtext'];
	}

	/**
	 * Applicable to SDBIP modules
	 */
	public function getPrimaryKPASource() {
		$this->getKPASources();
		return $this->primary_source;
	}

	public function getPrimarySourceModLoc() {
		switch($this->module_settings['PRIMARY']['value']) {
			case "ORG":
				return $this->module_settings['ORG_SOURCE']['setting'];
				break;
			case "IKPI":
				return $this->module_settings['IKPI_SOURCE']['setting'];
				break;
		}
	}

	public function getSourceModloc($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['setting'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['setting'];
		}
	}
	public function getSourceObject($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['object_id'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['object_id'];
		}
	}
	public function getSourceSDBIPID($mr) {
		if($this->module_settings['ORG_SOURCE']['value']==$mr) {
			return $this->module_settings['ORG_SOURCE']['object_id'];
		} else {
			return $this->module_settings['IKPI_SOURCE']['object_id'];
		}
	}
	public function getKPAs() {
		return $this->getGroupList();
	}
	public function getGroupList() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_group ORDER BY name DESC";
		$rows =  $this->mysql_fetch_value_by_id($sql, "id", "name");
		$rows[0] = $this->getUnspecified();
		return $rows;
	}

	public function getGroupMapping($filter_modref="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_group_map WHERE (grp_status & 2) = 2 ".(strlen($filter_modref)>0?" AND grp_src_modref = '$filter_modref'":"");
		return $this->mysql_fetch_all($sql);
	}

	public function getCompleteGroupingMap($filter_modref="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_setup_group_map GM
				INNER JOIN ".$this->getDBRef()."_list_group G
				ON G.id = GM.grp_local_id
				WHERE (GM.grp_status & 2) = 2
				AND (G.status & 2) = 2 
				";
		if(strlen($filter_modref)>0) {
			$sql.=" AND GM.grp_src_modref = '".$filter_modref."'";
		}
		$list = $this->mysql_fetch_all_by_id($sql, "grp_id");
		foreach($list as $i => $l) {
			$list[$i]['comparison'] = $this->formatTextForComparison($l['name']);
		}
		return $list;
	}

	public function getGroupMapConvertingSourceToLocal($modref) {
		$map = $this->getGroupMapping($modref);
		$data = array();
		foreach($map as $m) {
			$data[$m['grp_src_id']] = $m['grp_local_id'];
		}
		return $data;
	}

	public function getGroupingField() {
		return $this->module_settings['GROUPING']['value'];
	}


	public function updateGroupingList($group,$src_modref,$src_modloc) {
		$current_list = $this->getGroupList();
		$current_list_for_comparison = array();
		foreach($current_list as $i => $n) {
			$current_list_for_comparison[$this->formatTextForComparison($n)] = $i;
		}
		$current_map = $this->getGroupMapping($src_modref);
		$processed_map = array();
		foreach($current_map as $map) {
			$processed_map[$map['grp_src_id']] = $map['grp_local_id'];
		}
		foreach($group as $src_id => $name) {
			if(!isset($current_list_for_comparison[$this->formatTextForComparison($name)])) {
				//insert new record - no coding - should have come raw out of the DB and already been encoded before being saved originally
				$sql = "INSERT ".$this->getDBRef()."_list_group SET name = '$name', status = '".self::ACTIVE."', code = ''";
				$i = $this->db_insert($sql);
				$current_list_for_comparison[$this->formatTextForComparison($name)] = $i;
				//insert grouping map
				$sql = "INSERT INTO ".$this->getDBRef()."_setup_group_map SET grp_src_modref = '$src_modref', grp_src_modloc = '$src_modloc', grp_src_id = '$src_id', grp_local_id = '$i', grp_status = ".self::ACTIVE.", grp_insertuser = '".$this->getUserID()."', grp_insertdate = now()";
				$this->db_insert($sql);
			} elseif(!isset($processed_map[$src_id])) {
				$local_id = $current_list_for_comparison[$this->formatTextForComparison($name)];
				//insert grouping map
				$sql = "INSERT INTO ".$this->getDBRef()."_setup_group_map SET grp_src_modref = '$src_modref', grp_src_modloc = '$src_modloc', grp_src_id = '$src_id', grp_local_id = '$local_id', grp_status = ".self::ACTIVE.", grp_insertuser = '".$this->getUserID()."', grp_insertdate = now()";
				$this->db_insert($sql);
			}
		}

	}




	/**************************
	 * JAL functions
	 */


	/**
	 * Function to check the SESSION for sources of Activities and update if expired
	 * Applicable to JAL modules
	 */
	private function checkJALSources() {
		//If SESSION doesn't hold any sources then get fresh from DB
		unset($_SESSION[$this->getModRef()]['SOURCES']['JAL']); //dev purposes
		if(!isset($_SESSION[$this->getModRef()]['SOURCES']['JAL'])) {
			$this->updateJALSources();
		} else {
			$s = $_SESSION[$this->getModRef()]['SOURCES']['JAL'];
			$now = strtotime(date("d F Y H:i:s"));
			//Check if expired
			if($s['expiration'] < $now) {
				$this->updateJALSources();
			} else {
				$this->jal_sources = $s['data'];
				$this->jal_primary_source = $s['primary'];
			}
		}
	}

	/**
	 * Function to update the SESSION stored sources of KPIs/Projects if expired or not yet created
	 * Applicable to JAL modules
	 */
	private function updateJALSources() {
		$this->getJALSourcesFromDB();
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['data'] = $this->sources;
		//Set expiration of data to now + 30 minutes
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['expiration'] = strtotime(date("d F Y H:i:s")) + (30 * 60);
		$_SESSION[$this->getModRef()]['SOURCES']['JAL']['primary'] = $this->primary_source;
	}

	/**
	 * Function to get latest sources of KPIs/Projects
	 * Applicable to JAL modules
	 */
	private function getJALSourcesFromDB() {
		$this->jal_sources = array();
		$sql = "SELECT * FROM assist_menu_modules
				WHERE (
					modlocation LIKE 'JAL%'
				) AND modyn = 'Y'
				ORDER BY modref"; //echo $sql;
		$srcs = $this->mysql_fetch_all_by_id($sql, "modref");  //$this->arrPrint($srcs);
		$found_primary = false;
		foreach($srcs as $s => $row) {
			if($found_primary === false) {
				$this->jal_primary_source = $s;
				$found_primary = true;
			}
			$this->jal_sources[$s] = array('active'      => true,
										   'modref'      => $s,
										   'modlocation' => $srcs[$s]['modlocation'],
										   'modtext'     => $srcs[$s]['modtext']);
		}
	}

	/**
	 * Function to retrieve Activity sources
	 * Applicable to JAL modules
	 */
	public function getJALSources() {
		$this->checkJALSources();
		$data = array();
		foreach($this->jal_sources as $src) {
			if($src['active']) {
				$data[] = $src;
			}
		}
		return $data;
	}

	/**
	 * Applicable to JAL modules
	 */
	public function getAJALSourceName($src) {
		$this->getJALSources();
		return $this->jal_sources[$src]['modtext'];
	}

	/**
	 * Applicable to JAL modules
	 */
	public function getPrimaryJALSource() {
		$this->getJALSources();
		return $this->jal_primary_source;
	}


	/****************************
	 * GENERAL FUNCTIONS
	 */

	public function getSteps() {
		return $this->steps;
	}

	public function getStep($s) {
		return $this->steps[$s];
	}

	public function getNumberOfSteps() {
		return (count($this->steps));
	}

	public function getMidScore() {
		return $this->mid_score;
	}

	public function getDefaultScore() {
		return $this->default_score;
	}

	public function getFinalWeights() {
		$result = array('kpa' => $this->kpa_weight,);
		$result['cc'] = 1 - $result['kpa'];
		return $result;
	}


	public function calcBonusPercentage($r, $bonus_scale_id = 0) {
		$result = "0%";
		if($this->bonus_scale_updated_from_central_class === false) {
			$listObject = new PM6_SETUP_BONUS();
			$this->bonus_scale = $listObject->getBonusScaleForProcessing($bonus_scale_id);
		}
		foreach($this->bonus_scale as $min => $bonus) {
			if(($min * 1) <= $r) {
				$result = $bonus;
			} else {
				break;
			}
		}
		return $result;
	}


	public function getBonusRatings($bonus_scale_id = 0) {
		if($this->bonus_scale_updated_from_central_class === false) {
			$listObject = new PM6_SETUP_BONUS();
			$this->bonus_scale = $listObject->getBonusScaleForProcessing($bonus_scale_id);
		}
		$b = array();
		$a = 0;
		foreach($this->bonus_scale as $min => $bonus) {
			if($a > 0) {
				$b[$a - 1]['max'] = ($min - 0.0001);
			}
			$x = array('min'   => $min,
					   'max'   => false,
					   'bonus' => $bonus,);
			$b[$a] = $x;
			$a++;
		}
		return $b;
	}

	/**
	 * Quick test to confirm if a bonus scale has been assigned to a scorecard or not
	 * @param int $bonus_scale_id
	 * @return bool
	 */
	public function isValidBonusScale($bonus_scale_id=0) {
		if($bonus_scale_id>0) {
			return true;
		}
		return false;
	}

	public function getBonusRatingTable($bonus_scale_id = 0) {

		if($this->isValidBonusScale($bonus_scale_id)) {

//			$bonusObject = new PM6_SETUP_BONUS();
//			$bonus_scale_details = $bonusObject->getBonusScaleByID($bonus_scale_id);

//		ASSIST_HELPER::arrPrint($bonus_scale_details);

			$bonusperc = $this->getBonusRatings($bonus_scale_id);
			$echo = "
			<h3>Bonus Table</h3>
		<table>
			<tr>
				<th width='120px'>% Performance</th>
				<th>Bonus</th>
			</tr>
			";
			foreach($bonusperc as $b) {
				//if($b['bonus']>0) {
				$min = number_format(($b['min'] * 100), 2);
				$max = $b['max'] !== false ? " - ".number_format(($b['max'] * 100), 2)."%" : "+";
				$td_size = "";
				if(strlen($b['bonus'])>50) {
					$td_size = " width=150px ";
				}
				$echo .= "
				<tr>
					<td>".$min."%".$max."</td>
					<td $td_size >".$b['bonus']."</td>
				</tr>";
				//}
			}
			$echo .= "
		</table>";
		} else {
			$echo = "";
		}
		return $echo;
	}


	/*********************
	 * MODULE OBJECT functions
	 * for CONTRACT, DELIVERABLE AND ACTION
	 */
	public function getObject($var) { //$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'], $options);
				if($var['type'] == "LIST" || $var['type'] == "ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data, $this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details) > 0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'], $var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'], $var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}

	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}

	public function getProgressFieldName() {
		return $this->progress_field;
	}

	public function getIDFieldName() {
		return $this->id_field;
	}

	public function getParentFieldName() {
		return $this->parent_field;
	}

	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}

	public function getNameFieldName() {
		return $this->name_field;
	}

	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}

	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}

	public function getOwnerFieldName() {
		return $this->owner_field;
	}

	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}

	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}

	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}

	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}

	public function getPageLimit() {
		$profileObject = new PM6_PROFILE();
		return $profileObject->getProfileTableLength();
	}

	public function getDateOptions($fld) {
		if(stripos($fld, "action_on") !== FALSE) {
			$fld = "action_on";
		}
		switch($fld) {
			case "action_on":
				return array('maxDate' => "'+0D'");
				break;
			default:
				return array();
		}
	}

	public function getExtraObjectFormJS() {
		return $this->object_form_extra_js;
	}

	public function getAllDeliverableTypes() {
		return $this->deliverable_types;
	}

	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key => $t) {
			$arr[] = $t;
		}
		return $arr;
	}

	public function getDeliverableTypes($contract_id) {
		$contractObject = new PM6_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables() == FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types;
	}

	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt, $ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE", $options);
		//$this->arrPrint($data);
		$result = array('past'    => 0,
						'present' => 0,
						'future'  => 0,);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z < $now) {
				$result['past']++; //echo "past";
			} elseif($z > $now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE", $options);
		return $data;
	}

	public function getMyList($obj_type, $section, $options = array()) {
		$final_data = array();
		/** All function usage within module is currently commented out - function left as placeholder.  If needed to be restored, get a newer copy from another module or rewrite.  [AA-404] JC 7 July 2020 */
		return $final_data;
	}

	function formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, $status_id_fld, $paging) {
		/** All function usage within module is currently commented out - function left as placeholder.  If needed to be restored, get a newer copy from another module or rewrite.  [AA-404] JC 7 July 2020 */
		return $final_data;
	}


	public function getDetailedObject($obj_type, $id, $options = array()) { //ASSIST_HELPER::arrPrint($options);
		$final_data = array();
		/** All function usage within module is currently commented out - function left as placeholder.  If needed to be restored, get a newer copy from another module or rewrite.  [AA-404] JC 7 July 2020 */
		return $final_data;
	}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}



	/***********************************
	 * GET functions
	 */

	/**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
	public function drawPageFooter($left = "", $log_table = "", $var = "") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
		echo "WRONG PAGE FOOTER";
		return "";
	}

	/**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
	public function getPageFooter($left = "", $log_table = "", $var = "") {
		/*    	$echo = "";
				$js = "";
				if(is_array($var)) {
					$x = $var;
					$d = array();
					unset($var);
					foreach($x as $f => $v) {
						$d[] = $f."=".$v;
					}
					$var = implode("&",$d);
				}
				$echo = "
				<table width=100% class=tbl-subcontainer><tr>
					<td width=50%>".$left."</td>
					<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
						<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
					</span>" : "")."</td>
				</tr></table><div id=div_audit_log></div>";
				if(strlen($log_table)>0){
					$js = "
					$(\"#disp_audit_log\").click(function() {
						var state = $(this).attr('state');
						if(state==\"show\"){
							$(this).find('img').prop('src','/pics/tri_down.gif');
							$(this).attr('state','hide');
							$(\"#div_audit_log\").html(\"\");
							$(\"#log_txt\").html('Display Activity Log');
						} else {
							$(this).find('img').prop('src','/pics/tri_up.gif');
							$(this).attr('state','show');
							var dta = '".$var."&log_table='+$(this).attr('table');
							var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
							$(\"#div_audit_log\").html(result[0]);
							$(\"#log_txt\").html('Hide Activity Log');
						}

					});
					";
				}
				$data = array('display'=>$echo,'js'=>$js);
				return $data;
		 **/
		return array('display' => "WRONG PAGE FOOTER", 'js' => "");
	}

	public function drawActivityLog() {

	}




	/*********************************************
	 * SET/UPDATE functions
	 */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new PM6_LOG($log_table);
	//	$logObject->addObject($var);
	//}


	public function notify($data, $type, $object) {
		$noteObj = new PM6_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
	}


	public function editMyObject($var, $attach = array(), $recipients = array(), $noter = "", $include = array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new PM6_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(), "FORM");
		$mar = 0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld => $v) {
			if(isset($headings['rows'][$fld]) || in_array($fld, array("contract_assess_status",
																	  "contract_assess_type",
																	  "contract_assess_other_name"))) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type'] == "DATE") {
					if(strlen($v) > 0) {
						$insert_data[$fld] = date("Y-m-d", strtotime($v));
					}
				} elseif($fld == "del_type") {
					if($v == "DEL") {
						$var['del_parent_id'] = 0;
						$insert_data['del_parent_id'] = 0;
					}
					$insert_data[$fld] = $v;
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}


		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

		if($mar > 0 || count($attach) > 0) {
			$changes = array('user' => $this->getUserName(),);
			foreach($insert_data as $fld => $v) {
				$head = $headings['rows'][$fld];
				if($old[$fld] != $v || $headings['rows'][$fld]['type'] == "HEADING") {
					$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type' => "TEXT");
					if(in_array($h['type'], array("LIST", "MASTER", "USER", "OWNER", "DEL_TYPE", "DELIVERABLE"))) {
						$list_items = array();
						$ids = array($v, $old[$fld]);
						switch($h['type']) {
							case "LIST":
								$listObject = new PM6_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new PM6_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "OWNER":
								$ownerObject = new PM6_CONTRACT_OWNER();
								$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new PM6_MASTER($head['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "DELIVERABLE":
								$delObject = new PM6_DELIVERABLE();
								$list_items = $delObject->getDeliverableNamesForSubs($ids);
								break;
							case "DEL_TYPE":
								$delObject = new PM6_DELIVERABLE();
								$list_items = $delObject->getDeliverableTypes($ids);
								break;
						}

						unset($listObject);
						$changes[$fld] = array('to'   => $list_items[$v],
											   'from' => $list_items[$old[$fld]],
											   'raw'  => array('to' => $v, 'from' => $old[$fld]));
					} elseif($h['type'] == "HEADING") {
						if($fld == "contract_assess_type") {
							$shead = $headings['sub'][$h['id']];
							foreach($shead as $sh) {
								$sfld = $sh['field'];
								switch($sh['type']) {
									case "BOOL":
										$bitwise_value = 0;
										$b = explode("_", $sfld);
										$f = end($b);
										switch($f) {
											case "other":
												$bitwise_value = PM6_CONTRACT::OTHER;
												break;
											case "qual":
												$bitwise_value = PM6_CONTRACT::QUALITATIVE;
												break;
											case "quan":
												$bitwise_value = PM6_CONTRACT::QUANTITATIVE;
												break;
										}
										$new_val = 0;
										if(($v & $bitwise_value) == $bitwise_value) {
											$new_val = 1;
										}
										$old_val = 0;
										if(($old[$fld] & $bitwise_value) == $bitwise_value) {
											$old_val = 1;
										}
										if($old_val != $new_val) {
											$changes[$sfld] = array('to' => $new_val, 'from' => $old_val);
										}
										break;
									default:
										if($old[$sfld] != $insert_data[$sfld]) {
											$changes[$sfld] = array('to' => $insert_data[$sfld], 'from' => $old[$sfld]);
										}
										break;
								}
							}
						} elseif($fld == "contract_assess_status") {
							$listObject = new PM6_LIST("deliverable_status");
							$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
							unset($listObject);
							foreach($list_items as $key => $status) {
								$bitwise_value = pow(2, $key);
								$new_val = 0;
								if(($v & $bitwise_value) == $bitwise_value) {
									$new_val = 1;
								}
								$old_val = 0;
								if(($old[$fld] & $bitwise_value) == $bitwise_value) {
									$old_val = 1;
								}
								if($old_val != $new_val) {
									$changes[$fld][$key] = array('to' => $new_val, 'from' => $old_val);
								}
							}
						}
					} else {
						$changes[$fld] = array('to' => $v, 'from' => $old[$fld]);
					}
				}
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getAttachmentFieldName()] = $x;
			$log_var = array('object_id' => $object_id,
							 'changes'   => $changes,
							 'log_type'  => PM6_LOG::EDIT,
							 'progress'  => $old[$this->getTableField()."_progress"],
							 'status_id' => $old[$this->getTableField()."_status_id"],);
			//**For Notifications and subsequent logging**//
			if(strlen($noter) > 0) {
				$log_var['recipients'] = $recipients;
				$log_var['include'] = $include;
				$note = $this->notify($log_var, $noter, $this->getMyObjectType());
				unset($log_var['recipients']);
				unset($log_var['include']);
				if($note[0] == "ok") {
					$log_var['changes']['notification'] = "no notifications sent";
				} else if($note[0] == "info") {
					$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
				} else {
					$log_var['changes']['notification'] = $note;
				}
			}
			//****//
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok",
						 $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been updated successfully.",
						 array($log_var, $this->getMyObjectType()));
		}
		return array("info", "No changes were found to be saved.  Please try again.");
	}


	public static function date_sort($a, $b) {
		return strtotime($a) - strtotime($b);
	}








	/*******************************************************************************************
	 * STATUS NAMING FUNCTIONS
	 */




	public function convertNewStatusToText($raw_status) {
		return $this->convertNewStatus($raw_status,"text");
	}

	public function convertNewStatusToColour($raw_status) {
		return $this->convertNewStatus($raw_status,"colour");
	}

	public function convertNewStatusToCode($raw_status) {
		return $this->convertNewStatus($raw_status,"code");
	}


	private function convertNewStatus($raw_status,$type) {
		$return = array();
		if($raw_status == PM6::NOT_YET_STARTED ) {	//can't do bitwise test here as NYS = 0 which always triggers
			switch($type) {
				case "code": case "text": case "name":
					$return[] = "not_yet_started";
					break;
				case "colour": case "color":
					$return[] = "red";
					break;
			}
		} else {
			if(($raw_status & PM6::IP_SUPER) == PM6::IP_SUPER) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "ip_super";
						break;
					case "colour":
					case "color":
						$return[] = "orange";
						break;
				}
			}
			if(($raw_status & PM6::IP_MANAGER) == PM6::IP_MANAGER) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "ip_manager";
						break;
					case "colour":
					case "color":
						$return[] = "orange";
						break;
				}
			}
			if(($raw_status & PM6::IP_EMPLOYEE) == PM6::IP_EMPLOYEE) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "ip_employee";
						break;
					case "colour":
					case "color":
						$return[] = "orange";
						break;
				}
			}
			if(($raw_status & PM6::IP_CHAMPION) == PM6::IP_CHAMPION) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "ip_champion";
						break;
					case "colour":
					case "color":
						$return[] = "orange";
						break;
				}
			}
			if(($raw_status & PM6::AWAITING_APPROVAL) == PM6::AWAITING_APPROVAL) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "awaiting_approval";
						break;
					case "colour":
					case "color":
						$return[] = "red";
						break;
				}
			}
			if(($raw_status & PM6::APPROVED) == PM6::APPROVED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "ip_approved";
						break;
					case "colour":
					case "color":
						$return[] = "blue";
						break;
				}
			}
			if(($raw_status & PM6::EMPLOYEE_DIGITALLY_APPROVED) == PM6::EMPLOYEE_DIGITALLY_APPROVED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "employee_digitally_approved";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::EMPLOYEE_REJECTED) == PM6::EMPLOYEE_REJECTED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "employee_rejected";
						break;
					case "colour":
					case "color":
						$return[] = "red";
						break;
				}
			}
			if(($raw_status & PM6::EMPLOYEE_SIGN_APPROVED) == PM6::EMPLOYEE_SIGN_APPROVED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "employee_sign_approved";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::EMPLOYEE_SIGN_ATTACHED) == PM6::EMPLOYEE_SIGN_ATTACHED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "employee_sign_attached";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::EMPLOYEE_APPROVED_NO_POE) == PM6::EMPLOYEE_APPROVED_NO_POE) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "employee_approved_no_poe";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::MANAGER_DIGITALLY_APPROVED) == PM6::MANAGER_DIGITALLY_APPROVED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "manager_digitally_approved";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::MANAGER_REJECTED) == PM6::MANAGER_REJECTED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "manager_rejected";
						break;
					case "colour":
					case "color":
						$return[] = "red";
						break;
				}
			}
			if(($raw_status & PM6::MANAGER_SIGN_APPROVED) == PM6::MANAGER_SIGN_APPROVED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "manager_sign_approved";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::MANAGER_SIGN_ATTACHED) == PM6::MANAGER_SIGN_ATTACHED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "manager_sign_attached";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
			if(($raw_status & PM6::MANAGER_APPROVED_NO_POE) == PM6::MANAGER_APPROVED_NO_POE) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "manager_approved_no_poe";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}
		}
		if($type=="text" || $type=="name") {
			$modref = $this->own_modref;
			foreach($return as $key => $r) {
				if(isset($_SESSION[$modref]['STATUS_NAMES'][$r])) {
					$return[$key] = $_SESSION[$modref]['STATUS_NAMES'][$r];
				} else {
					$return[$key] = $r;
				}
			}
		}

		return $return;
	}

	public function convertSCDStatusToText($raw_status) {
		return $this->convertSCDStatus($raw_status,"text");
	}

	public function convertSCDStatusToColour($raw_status) {
		return $this->convertSCDStatus($raw_status,"colour");
	}

	public function convertSCDStatusToCode($raw_status) {
		return $this->convertSCDStatus($raw_status,"code");
	}


	private function convertSCDStatus($raw_status,$type) {
		$return = array();
/*		if($raw_status == PM6::NOT_YET_STARTED ) {	//can't do bitwise test here as NYS = 0 which always triggers
			switch($type) {
				case "code": case "text": case "name":
					$return[] = "not_yet_started";
					break;
				case "colour": case "color":
					$return[] = "red";
					break;
			}
		} else {*/
			if(($raw_status & PM6::ACTIVATED) == PM6::ACTIVATED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "activated";
						break;
					case "colour":
					case "color":
						$return[] = "green";
						break;
				}
			}elseif(($raw_status & PM6::CONFIRMED) == PM6::CONFIRMED) {
				switch($type) {
					case "code":
					case "text":
					case "name":
						$return[] = "confirmed";
						break;
					case "colour":
					case "color":
						$return[] = "orange";
						break;
				}
			}

//		}
		if($type=="text" || $type=="name") {
			$modref = $this->own_modref;
			foreach($return as $key => $r) {
				if(isset($_SESSION[$modref]['STATUS_NAMES'][$r])) {
					$return[$key] = $_SESSION[$modref]['STATUS_NAMES'][$r];
				} else {
					$return[$key] = ucfirst($r);
				}
			}
		}
		return $return;
	}




	public function validateStatus($haystack,$needle,$return_type="bool") {
		if(($haystack & $needle)==$needle) {
			switch(strtolower($return_type)) {
				case "needle": return $needle; break;
				default: return true;
			}
		} else {
			switch(strtolower($return_type)) {
				case "needle": return 0; break;
				default: return false;
			}
		}
	}
	public function processInfoMessage($action,$extra1="") {
		$info_message = "";
		switch($action) {
			case "activate":	$info_message.="Activate the |scorecard| so that it is ready for assessment;"; 	break;
			case "confirm":	$info_message.="Confirm the |scorecard| as complete;"; 	break;
			case "-confirm":	$info_message.="Undo confirmation that the |scorecard| is complete & return it to In Progress status;"; 	break;
			case "awaiting_approval":	$info_message.="Mark the |scorecard| as waiting on review and approval by either/both the employee/manager;"; 	break;
			case "-awaiting_approval":	$info_message.="Mark the |scorecard| as no longer waiting on approval."; break;
			case "-approval":	$info_message.="Undo any |scorecard| approvals that might already have been done."; break;
			case "-employee_approval":	$info_message.="Undo your |scorecard| approval.  You will then have the option to redo your approval or to reject the |scorecard|."; break;
			case "-manager_approval":	$info_message.="Undo your |scorecard| approval.  You will then have the option to redo your approval or to reject the |scorecard|."; break;
			case "return_builder":	$info_message.="Return the |scorecard| to the original builder;"; break;
			case "employee_digitally_approved":	$info_message.="Mark that you have digitally approved the |scorecard|;"; break;
			case "manager_digitally_approved":	$info_message.="Mark that you have digitally approved the |scorecard|;"; break;
			case "manager_rejected":	$info_message.="Mark that you have rejected the |scorecard| with the reason you've given."; break;
			case "employee_rejected":	$info_message.="Mark that you have rejected the |scorecard| with the reason you've given."; break;
			case "non_user_employee":	$info_message.="Process the employee approval as per your selection above;"; break;
			case "non_user_manager":	$info_message.="Process the manager approval as per your selection above;"; break;
			default:
				if(strpos($action,"notify_manager")!==false) {
					if(strpos($action,"undo_confirm")!==false) {
						$info_message.="Notify the manager (".$extra1.") that the |scorecard| confirmation has been cancelled;";
					} elseif(strpos($action,"undo_approval")!==false) {
						$info_message .= "Notify the manager (".$extra1.") that your |scorecard| approval has been cancelled;";
					} else {
						$info_message.="Notify the manager (".$extra1.") that the |scorecard| is waiting on their review & approval;";
					}
				} elseif(strpos($action,"notify_employee")!==false) {
					if(strpos($action,"undo_confirm")!==false) {
						$info_message .= "Notify the employee (".$extra1.") that the |scorecard| confirmation has been cancelled;";
					} elseif(strpos($action,"undo_approval")!==false) {
						$info_message .= "Notify the employee (".$extra1.") that your |scorecard| approval has been cancelled;";
					} else {
						$info_message .= "Notify the employee (".$extra1.") that the |scorecard| is waiting on their review & approval;";
					}
				} elseif(strpos($action,"reject_manager")!==false) {
					$info_message.="Notify the manager (".$extra1.") that you have rejected the |scorecard|;";
				} elseif(strpos($action,"reject_employee")!==false) {
					$info_message.="Notify the employee (".$extra1.") that you have rejected the |scorecard|;";
				} else {
					$info_message.="".$action."";
				}
		}
		return $info_message;
	}




	/********************************************
	 * PROTECTED functions
	 */


	/*******************************************
	 * PRIVATE functions
	 */


	public function __destruct() {
		parent::__destruct();
	}


}


?>