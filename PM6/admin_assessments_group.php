<?php
$page_action = "group_trigger";

require_once("inc_header.php");



$apObject = new PM6_PERIOD();
$periods = $apObject->getAllAssessmentPeriods();
if(count($periods)==0) {
	ASSIST_HELPER::displayResult(array("error","No Assessment Periods have been created.  Please go to Assessments > Assessment Periods to auto-generate Assessment Periods."));
	die();
}
unset($apObject);

//$scdObj = new PM6_SCORECARD();
//$scorecards = array();$scdObj->getObjectsForTrigger();

$groupObject = new PM6_ASSESSMENT_GROUP();
$groups = $groupObject->getActiveGroups();
$group_types = $groupObject->getGroupTypesFormattedForSelect();
$test = $groupObject->getGroupItems(array("group_type"=>"job_dept"));
//ASSIST_HELPER::arrPrint($groups);
?>
<table class="tbl-container not-max"><tr><td>
<p style="margin-top:0px;padding-top:0px;padding-bottom:5px;">&nbsp;&nbsp;<span class="float"><button id="btn_group_new">New</button></span></p>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Group Name</th>
		<th>Group Type</th>
		<th>Group</th>
		<th>Included<br /><?php echo $groupObject->getObjectName(PM6_SCORECARD::OBJECT_TYPE,true); ?></th>
		<th><?php echo $groupObject->getObjectName(PM6_ASSESSMENT::OBJECT_TYPE,true); ?></th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
$found = false;
if(count($groups)>0) {
	$found= true;
	foreach($groups as $pa) {
		if($pa['is_building']) {
			$button = "<button class=btn_continue obj_id=".$pa['id'].">Step 2: Confirm Employees</button>";
		} else {
			$button = "<button class=btn_open obj_id=".$pa['id'].">Open</button>";
		}
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class=''>".$pa['group_name']."</td>
			<td class=''>".$pa['group_type_text']."</td>
			<td class=''>".$pa['group_item_text']."</td>
			<td class=center>".$pa['included_scorecards']."</td>
			<td class=center>".($pa['triggered_assessments']>0?$pa['triggered_assessments']:"-")."</td>
			<td class=center>".$pa['status']."</td>
			<td class=center>".$button."</td>
		</tr>";
	}
} else {

	echo "
	<tr>
		<td id=td_none colspan=7>No groups available.</td>
	</tr>";
}

?>
</table>
<?php
ASSIST_HELPER::displayResult(array("info","
<p>Information about Groups</p>
<ul class='black'>
<li>Groups allow Assessments to be triggered on multiple Scorecards simultaneously.</li>
<li>Scorecards can only belong to one group.</li>
<li>Any Scorecard which has been triggered for Assessment can not be added to a Group.</li>
</ul>"))
?>
</td></tr></table>
<div id="dlg_group_new" title="New Group">
	<h2>Step 1: Create Group</h2>
	<table class="form">
		<tr>
			<th>Group Type:</th>
			<td><select id="sel_group_type"><option selected value="X">--- SELECT ---</option>
				<?php
				foreach($group_types as $fld => $gt) {
					echo "<option value='".$fld."'>".$gt."</option>";
				}
				?>
				</select></td>
		</tr>
		<tr>
			<th>Group:</th>
			<td><select id="sel_group"><option value="X">--- Please select a Type above ---</option></select></td>
		</tr>
		<tr>
			<th></th>
			<td><button id="btn_create_group">Step 2: Confirm Employees</button></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list td").addClass("middle");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");
	$("#tbl_list tr").find("td:eq(2)").removeClass("center");
	$("#tbl_list tr").find("td:eq(3)").removeClass("center");

	$("#dlg_group_new").dialog({
		autoOpen: false,
		modal: true,
		width: "750px"
	});

	$(".btn_open").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "admin_assessments_group_details.php?object_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

	$("#btn_group_new").button({
		icons: {primary: "ui-icon-plus"},
	}).click(function(e) {
		e.preventDefault();
		$("#dlg_group_new").dialog("open");
	}).hover(function() {
		$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");

	$("#btn_create_group").button({
		icons: {secondary: "ui-icon-arrow-1-e"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$("#sel_group_type").removeClass("required");
		$("#sel_group").removeClass("required");
		var group_type = $("#sel_group_type").val();
		var group_item = $("#sel_group").val();
		var err = false;
		if(group_type=="X") {
			err = true;
			$("#sel_group_type").addClass("required");
		}
		if(group_item=="X") {
			err= true;
			$("#sel_group").addClass("required");
		}
		if(err) {
			AssistHelper.finishedProcessing("error","Please complete the required information.");
		} else {
			var gt = $("#sel_group_type option:selected").text();
			var gi = $("#sel_group option:selected").text();
			var dta= "group_type="+group_type+"&group_item="+group_item+"&group_type_text="+AssistString.code(gt)+"&group_item_text="+AssistString.code(gi);
			document.location.href="admin_assessments_group_step2.php?"+dta;
		}
	}).hover(function() {
		$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");


	$("#sel_group_type").change(function(e) {
		e.preventDefault();
		if($(this).val()=="X") {
			$("#sel_group option:gt(0)").remove();
			$("#sel_group option:eq(0)").text("--- Please select a Type above ---");
		} else {
			var group_type = $(this).val();
			var group_items = AssistHelper.doAjax("inc_controller_assessment.php?action=ASSESSMENTS_GROUP.getGroupItems","group_type="+group_type);
			var sortable = [];
			for (var i in group_items) {
				sortable.push([group_items[i],i]);
			}
			sortable.sort(function(a, b) {
				return a[1] - b[1];
			});
			sortable.sort();
			var c = 0;
			$("#sel_group option:gt(0)").remove();
			$("#sel_group option:eq(0)").text("--- SELECT ---");
			for(x in sortable) {
				console.log(x+":"+sortable[x]);
				c++;
				$("#sel_group").append("<option value='"+sortable[x][1]+"'>"+sortable[x][0]+"</option>");
			}
			if(c==0) {
				$("#sel_group option:eq(0)").text("--- Please select a Type above ---");
			}
		}
	});

	$(".btn_continue").button({
		icons: {secondary: "ui-icon-arrow-1-e"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).attr("obj_id");
		document.location.href = "admin_assessments_group_step2.php?object_id="+i;
	}).removeClass("ui-state-default").addClass("ui-button-minor-grey").hover(function(){
		$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
	},function(){
		$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
	});

<?php
if(!$found) {
	?>
	var c = $("#tbl_list tr:first").children("th").size();
	$("#td_none").prop("colspan",c).removeClass("center");
	<?php
}
?>
});
</script>