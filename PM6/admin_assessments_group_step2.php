<?php
/**
 * @var string $js - catchall for centralised JS, source:inc_header
 * @var PM6_DISPLAY $displayObject - source: inc_header
 */
$page_action = "group_trigger";
require_once("inc_header.php");


$groupObject = new PM6_ASSESSMENT_GROUP();

$var = $_REQUEST;
$group_id = 0;
$adding_scorecards_to_existing_group = false;
if(isset($_REQUEST['adding_scorecards_to_existing_group']) && isset($_REQUEST['group_id'])) {
	$group_id = $_REQUEST['group_id'];
	$adding_scorecards_to_existing_group = true;
	$raw_group_details = $groupObject->getRawObject($group_id);
	if(!isset($var['group_type'])) {
		$var['group_type'] = $raw_group_details['grp_type'];
	}
	if(!isset($var['group_item'])) {
		$var['group_item'] = $raw_group_details['grp_item'];
	}
}



$possible_scorecards = $groupObject->getPossibleGroupChildren($var['group_type'],$var['group_item']);

$headings = array(
	'ref'=>"Ref",
	'user_name'=>"Employee",
);
$group_type_headings = $groupObject->getGroupTypesFormattedForSelect();
foreach($group_type_headings as $fld => $head) {
	if($fld!=="CUSTOM") {
		$headings[$fld] = $head;
	}
}


function drawTable($scds,$include_checkbox) {
	global $headings;
	global $groupObject;
	?>
	<table id="tbl_list">
	<tr>
		<?php
		if($include_checkbox) {
		 echo "<th><input type=\"checkbox\" value=\"ALL\" id=\"chk_all\" name=\"chk_all\" /></th>";
		}
		foreach($headings as $fld => $head) {
			echo "<th>".$head."</th>";
		}
		?>
	</tr>
	<?php
	if(count($scds)>0) {
		foreach($scds as $scd_id => $scorecard) {
			echo "<tr>";
			if($include_checkbox) {
				echo "<td class='center'><input type='checkbox' name='gs_scd[".$scd_id."]' class='chk-impacted-by-all' value='1' /></td>";
			}
			foreach($headings as $fld=> $head) {
				echo "<td>";
				echo isset($scorecard[$fld]) ? $scorecard[$fld] : $scorecard['job'][$fld];
				echo "</td>";
			}
			echo "<tr>";
		}
	} else {
		echo "<tr><td id='td_no_scd'>No ".$groupObject->replaceObjectNames("|SCORECARD|")."s available</td></tr>";
	}
	?>
</table>
	<?php
}

?>
<h2>Create Group</h2>
<form name="frm_group" style="margin-left:10px">
	<input type="hidden" name="object_id" value="<?php echo $group_id; ?>" />
	<input type="hidden" name="grp_type" value="<?php echo $var['group_type']; ?>" />
	<input type="hidden" name="grp_item" value="<?php echo $var['group_item']; ?>" />
	<h4>Group Name:</h4>
	<?php
	if($adding_scorecards_to_existing_group) {
		echo "<p>".$raw_group_details['grp_name']."</p>";
	} else {
		?>
	<p style="margin-bottom: 25px;"><?php $js.=$displayObject->drawFormField("LRGVC",array('id'=>"grp_name",'name'=>"grp_name"),$var['group_type_text']." - ".$var['group_item_text']); ?></p>
		<?php
	}
	?>
<h4><?php echo $groupObject->replaceObjectNames("|SCORECARD|"); ?>s Available For Inclusion</h4>
<table class="tbl-container not-max"><tr><td>
<?php
drawTable($possible_scorecards['available'],true);
?>
		</td></tr>
	<tr>
		<td class="center"><button id="btn_save"><?php echo $adding_scorecards_to_existing_group?"Add Scorecards":"Create Group";?></button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="btn_cancel">Cancel</button></td>
	</tr>
	<tr><td>
	<?php
	if(count($possible_scorecards['not_available'])>0) {
		echo "
		<h4>".$groupObject->replaceObjectNames("|SCORECARD|")."s Not Available</h4>
		<p>These are ".$groupObject->replaceObjectNames("|SCORECARD|")."s which meet the group settings but which can not be included as they have already been triggered or are already in another group.</p>";
		drawTable($possible_scorecards['not_available'],false);
	}
	?>
			</td></tr>
</table>
</form>
<script type="text/javascript">
$(function() {
	var adding_scorecards_to_existing_group = <?php echo $adding_scorecards_to_existing_group?"true":"false"; ?>;
	<?php echo $js; ?>
	$("#btn_save").button({
		icons: {primary: "ui-icon-disk"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var c = $(".chk-impacted-by-all:checked").length;
		console.log(c);
		if(c==0) {
			AssistHelper.finishedProcessing("error","Please selected at least 1 scorecard to be included in this group.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_group]"));
			if(adding_scorecards_to_existing_group) {
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=ASSESSMENTS_GROUP.ADDSCORECARD",dta);
			} else {
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=ASSESSMENTS_GROUP.ADD",dta);
			}
			if(result[0]=="ok") {
				if(adding_scorecards_to_existing_group) {
					AssistHelper.finishedProcessingWithRedirect("ok",result[1],"admin_assessments_group_details.php?object_id=<?php echo $group_id; ?>");
				} else {
					AssistHelper.finishedProcessingWithRedirect("ok", result[1], "admin_assessments_group.php");
				}
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).hover(function() {
		$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
	},function() {
		$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");

	$("#btn_cancel").button({
		icons:{primary:"ui-icon-closethick"}
	}).hover(function() {
		$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
	},function() {
		$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		if(adding_scorecards_to_existing_group) {
			document.location.href = "admin_assessments_group_details.php?object_id=<?php echo $group_id; ?>";
		} else {
			document.location.href = 'admin_assessments_group.php';
		}
	}).removeClass("ui-state-default").addClass("ui-button-minor-grey");
	$("#chk_all").click(function() {
		$("input:checkbox.chk-impacted-by-all").prop("checked",$(this).prop("checked"));
	});
	var cols = $("#tbl_list tr:first").find("th").length;
	$("#td_no_scd").prop("colspan",cols);
});
</script>