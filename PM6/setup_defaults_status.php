<?php
require_once("inc_header.php");

$current_status_names = $nameObject->getStatusNames();
$current_process_names = $nameObject->getProcessNames();




?>
<h1>General Status Names</h1>
<table>
	<tr>
		<th>Ref</th>
		<th>Default Assist Terminology</th>
		<th>Your Terminology</th>
		<th></th>
	</tr>
	<?php
	foreach($current_status_names as $key => $row) {
		echo "
	<tr>
		<td class='center b'>".$row['id']."</td>
		<td class='b'>".$row['mdefault']."</td>
		<td><input type='text' id='$key' value='".$row['client']."' size='50' /></td>
		<td><button class='btn-save'>Save</button></td>
	</tr>";
	}
	?>
</table>

<h1>New / Edit Process Names</h1>
<table>
	<tr>
		<th>Ref</th>
		<th>Default Assist Terminology</th>
		<th>Your Terminology</th>
		<th></th>
	</tr>
	<?php
	foreach($current_process_names as $key => $row) {
		echo "
	<tr>
		<td class='b'>".$row['id']."</td>
		<td>".$row['mdefault']."</td>
		<td><input type='text' id='$key' value='".$row['client']."' size='50' /></td>
		<td><button class='btn-save'>Save</button></td>
	</tr>";
	}
	?>
</table>
<script type="text/javascript">
	$(function() {
		var tbl_header_width = 50;
		$("table").each(function() {
			var $cell = $(this).find("tr:first").find("th:eq(1)");
			if($cell.width()>tbl_header_width) {
				tbl_header_width = $cell.width();
			}
		});
		$("table").each(function() {
			$(this).find("tr:first").find("th:eq(1)").width(tbl_header_width);
		});

		var btnSaveClass = "ui-button-minor-grey";
		$(".btn-save")
			.button({icons:{primary:"ui-icon-disk"}})
			.removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function() {
					if($(this).hasClass("ui-button-state-orange")) {
						btnSaveClass = "ui-button-state-orange";
					} else {
						btnSaveClass = "ui-button-minor-grey";
					}
					$(this).removeClass(btnSaveClass).addClass("ui-button-minor-orange");
				},function() {
					$(this).removeClass("ui-button-minor-orange").addClass(btnSaveClass);
				}
			).click(function(e) {
				e.preventDefault();
				AssistHelper.processing();
				var dta = "temp=data";
				var $row = $(this).closest("tr");
				$row.find("input:text").each(function() {
					dta+="&section[]="+$(this).prop("id")+"&client[]="+AssistString.code($(this).val());
				});
				var result = AssistHelper.doAjax("inc_controller.php?action=NAMES.saveStatus",dta);
				$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
				$(this).closest("tr").find("input:text").removeClass("orange-border");
				AssistHelper.finishedProcessing(result[0],result[1]);
		});
		$("input:text").change(function() {
			$(this).addClass("orange-border");
			$(this).closest("tr").find("button.btn-save").addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
		});
	});
</script>


