<?php
require_once("inc_header.php");
$create_step = 8;

$types = array("JAL",);
$names = array('JAL'=>"Activities",);

$create_type = $types[0];
$create_names = $names[$create_type];

$scdObj = new PM6_SCORECARD();
$sources = $scdObj->getJALSources();
$line_weight_precision_allowed = $scdObj->getLineWeightPrecisionAllowed();
$kpa_weight_precision = $line_weight_precision_allowed-1;
$kpa_zero_decimal_echo = "";
for($i=1;$i<=$kpa_weight_precision;$i++) {
	$kpa_zero_decimal_echo.="0";
}
$sdbp5Obj = new JAL1_PMS();

$primary_source = $scdObj->getPrimaryJALSource();
$kpas = $sdbp5Obj->getKPAs($primary_source);
$head = $sdbp5Obj->getHeadings($primary_source,true,false);

unset($head['main']['kpi_natkpaid']);

echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);
?>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$lineObj = new PM6_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id'],false,array(),"JAL");
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);

//calculate default weights if not available
$c = 0;
if(count($lines)>0) {
	foreach($kpas as $k => $a) {
		foreach($types as $kt) {
			if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
				$objects = $lines[$k][$kt];
				foreach($objects as $i => $obj) {
					$c++;
				}
			}
		}
	}
}
if($c>0) {
	$blank = false;
	$default_weight = round(100/$c,$line_weight_precision_allowed);
} else {
	$blank = true;
	$default_weight = 0;
}


if(!$blank) {
?>
<h3>Weight <?php echo $create_names; ?></h3>
<table id=tbl_list class=list>
<thead>
	<tr>
		<?php
		foreach($head['main'] as $fld=>$name) {
			echo "<th>".$name."</th>";
		}
		?>
		<th title="Up to $line_weight_precision_allowed decimal places allowed.">Overall<br />Weight*</th>
		<th>Function<br />Weight</th>
	</tr>
</thead>
<tbody>
<?php
$grand_tot = 0;
foreach($kpas as $k => $a) {
foreach($types as $kt) {
	$tot = 0;
	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$kw = isset($kpa_weights[$k][$kt]) && !($kpa_weights[$k][$kt]<0) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt])*$default_weight;
		//echo "<h3>".$a."</h3>";
		echo "<tr class=subth><td colspan=".(count($head['main'])+2).">".$a." - ".$names[$kt]."</td></tr>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k][$kt];
		foreach($objects as $i => $obj) {
			echo "
			<tr  class=\"".($obj['active']==1 ? "" : "inactive")."\">";
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				$w = (isset($weights[$i]) && !($weights[$i]<0) ? $weights[$i] : $default_weight);
			echo "
				<td class=right><div style='width:70px' class='float'><input type=text name=weight[".$i."] size=6 kpa=".$k." kt=".$kt." line_id=".$i." class='right txt_weight kpa".$k.$kt."' value='".number_format($w,$line_weight_precision_allowed)."' />%</div></td>
				<td id=td_".$k.$kt."_".$i." class='kpa_line_perc right'>".number_format((count($objects)>1 ? ($kw>0 ? round(($w/$kw)*100,$line_weight_precision_allowed):0) : 100),$kpa_weight_precision)."%</td>
			</tr>";
			$tot+=(isset($weights[$i]) && !($weights[$i]<0) ? $weights[$i] : $default_weight);
		}
		echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".number_format($tot,$line_weight_precision_allowed)."%</th>
			<th class=right>100.".$kpa_zero_decimal_echo."%</th>
		</tr>";
	}
	$grand_tot+=$tot;
}
}
?>
	</tbody>
	<tfoot>
		<tr class=gtotal><th class='right b' colspan=<?php echo (count($head['main'])); ?>>Total Weight:</th><th class=right id=th_gtot><?php echo $grand_tot; ?>%</th><th class=right>-</th></tr>
	</tfoot>
</table>
<p>* Up to <?php echo $line_weight_precision_allowed; ?> decimal places allowed.</p>

<?php
} else {
	echo "<p>No $create_names have been added.</p>";
}




?>

</form>

<p class='center no-print'><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next><?php if(!$blank) { echo "Save & "; } ?>Next Step</button></p>






<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id'],$page_redirect_url);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;

	var line_count = <?php echo (count($lines)>0 ? "true" : "false"); ?>;

	$(".txt_weight").addClass("right").blur(function() {
		//get ids for sub-totalling
		var k = $(this).attr("kpa");
		var t = $(this).attr("kt");
		//check value
		var new_val = parseFloat($(this).val());
		if(new_val < 0) {
			//highlight errors
			$(this).addClass("required");
			$("#td_"+k+t).html("Error").css({"background-color":"#cc0001","color":"#ffffff"});
			$("#td_"+k+t+"_"+$(this).attr("line_id")).html("Error").addClass("required");
			$("#th_gtot").html("Error").css({"background-color":"#cc0001","color":"#ffffff"});
			//disable next button
			$("#btn_next").removeClass("ui-button-state-ok").addClass("ui-state-default").css({"color":"","border":""}).prop("disabled","disabled");
			//notify user of error
			alert("Invalid weight.  Only positive numbers are permitted.");
		} else {
			//remove red highlighting from current line (if current line successful)
			$(this).removeClass("required");
			$("#td_"+k+t+"_"+$(this).attr("line_id")).removeClass("required");

			//Check for other errors
			var line_error = false;
			$(".kpa"+k+t).each(function() {
				if($("#td_"+k+t+"_"+$(this).attr("line_id")).hasClass("required")) {
					line_error = true;
				}
			});

			//If no other lines have errors then continue to recalculate totales
			if(!line_error) {
				//remove red
				$("#td_"+k+t).css({"background-color":"","color":""});
				$("#th_gtot").removeClass("required");

				//update kpa total
				var tot = 0;
				$(".kpa"+k+t).each(function() {
					tot+=parseFloat($(this).val());
				});
				$("#td_"+k+t).html(tot.toFixed(<?php echo $line_weight_precision_allowed; ?>)+"%");

				//update row totals
				var x = 0;
				$(".kpa"+k+t).each(function() {
					x=parseFloat($(this).val());
					$("#td_"+k+t+"_"+$(this).attr("line_id")).html(((x/tot)*100).toFixed(<?php echo $kpa_weight_precision; ?>)+"%");
				});

				//update grand total
				var gt = 0;
				$(".kpa_tot").each(function() {
					gt+=parseFloat(AssistString.substr($(this).html(),0,-1));
				});
				$("#th_gtot").html(gt.toFixed(<?php echo $line_weight_precision_allowed; ?>)+"%");
				if(gt!=100) {
					$("#th_gtot").css({"background-color":"#CC0001","color":"#FFFFFF"});
					$("#btn_next").removeClass("ui-button-state-ok").addClass("ui-state-default").css({"color":"","border":""}).prop("disabled","disabled");
				} else {
					$("#th_gtot").css({"background-color":"","color":""});
					$("#btn_next").prop("disabled","").removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"});
				}
			} else {
				$("#th_gtot").css({"background-color":"","color":""});
				$("#btn_next").removeClass("ui-button-state-ok").addClass("ui-state-default").css({"color":"","border":""}).prop("disabled","disabled");
			}
		}
	});






	$("table.noborder, table.noborder td").css("border","0px");

	$("#btn_next").button({
		icons: {primary: "ui-icon-disk", secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		AssistHelper.processing();
		var err = false;
		var sign_err = false;

		if(line_count==true) {

		if($("#th_gtot").html().toLowerCase()=="error" || $("#th_gtot").hasClass("required")) {
			sign_err = true;
		} else {
			$("td.kpa_tot").each(function() {
				if($(this).html().toLowerCase()=="error" || $(this).hasClass("required")) {
					sign_err = true;
				}
			});
			if(!sign_err) {
				$("td.kpa_line_perc").each(function() {
					if($(this).html().toLowerCase()=="error" || $(this).hasClass("required")) {
						sign_err = true;
					}
				});
			}
		}
		if(sign_err) {
			AssistHelper.finishedProcessing("error","There is an invalid weight (highlighted in red).  Please review the weights again.");
		} else if(err) {
			AssistHelper.finishedProcessing("error","Not all KPA Weights total 100.  Please review the weights again.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_weights]"));
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.saveWeights",dta);
			if(result[0]=="ok") {
				var url = "<?php echo $page_redirect_url; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}


		} else {
			AssistHelper.processing();
				var url = "<?php echo $page_redirect_url; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
			AssistHelper.finishedProcessingWithRedirect("info","No <?php echo $create_names; ?> found to be weighted.  Moving on to step <?php echo ($create_step+1); ?>.",url);
		}

		//document.location.href = 'assessment_create_step3.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			AssistHelper.processing();
			document.location.href = '<?php echo $page_redirect_url; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");

	if(line_count==true) {
		$(".txt_weight:first").trigger("blur");
	} else {
		$("#btn_next").trigger("click");
	}
});

</script>