<?php 
include "inc_header.php";

$active_users = true;

$empObj = new PM6_EMPLOYEE();
$employee_data = $empObj->getAvailableEmployees($active_users);
$available_employees = $employee_data['employees'];

$fields_glossary = $empObj->getFieldGlossary();
$user_access_fields = array(
		'create_own'=>"Create Own |scorecard|",
		'create_users'=>"Create Staff |scorecard| (Assist Users)",
		'create_non'=>"Create Staff |scorecard| (Non-Assist Users)",
);
$user_access_fields = $empObj->replaceAllNames($user_access_fields);

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($available_employees);
//echo "<hr />";
//TODO - limit user access buttons to users with menu access to the module.
//ASSIST_HELPER::displayResult(array("info","DEV NOTE: Only display user access buttons for users with menu access to the module."));

$settingObject = new PM6_SETUP_EMPLOYEE();
$employee_settings = $settingObject->getEmployeeSettingsFormattedForSetup($active_users);

$bonusObject = new PM6_SETUP_BONUS();
$bonus_scales = $bonusObject->getListOfActiveBonusScales();

?>
<div class="horscroll">
<table class='tbl-container not-max'><tr><td>
<table class="tbl-new-format">
	<tr>
		<th colspan="6">Employee Details</th>
		<th colspan="3">Create Scorecards</th>
		<th colspan="3">Manage Scorecards</th>
		<th rowspan="2"></th>
	</tr>
	<tr>
		<th>System<br />Ref</th>
		<th>Name</th>
		<th>Current Job Details</th>
		<th>Current Manager (By Name)</th>
		<th>Current Manager (By Job Title)</th>
		<th>Bonus Scale</th>
		<th title='<?php $fld="create_own"; echo $fields_glossary[$fld]; ?>'>Own Scorecard</th>
		<th title='<?php $fld="create_users"; echo $fields_glossary[$fld]; ?>'>Staff Scorecards<br />(Assist Users)</th>
		<th title='<?php $fld="create_non"; echo $fields_glossary[$fld]; ?>'>Staff Scorecards<br />(Non-Assist Users)</th>
		<th>Own Scorecard</th>
		<th>Staff Scorecards<br />(Assist Users)</th>
		<th>Staff Scorecards<br />(Non-Assist Users)</th>
	</tr>
	<!-- start of employees -->
	<?php
	foreach($available_employees as $tkid => $employee) {
		$emp_id = $employee['emp_id'];
		$job_title = isset($employee_data['job_titles'][$employee['job_title']]) ? $employee_data['job_titles'][$employee['job_title']] : "Unknown Job Title";
		$department = isset($employee_data['departments'][$employee['job_dept']]) ? $employee_data['departments'][$employee['job_dept']] : "Unknown Department";
		$manager = isset($employee_data['job_titles'][$employee['job_manager_title']]) ? $employee_data['job_titles'][$employee['job_manager_title']] : "Unknown Job Title";
		if(!isset($employee_data['managers'][$employee['job_manager_title']]) || !is_array($employee_data['managers'][$employee['job_manager_title']]) || count($employee_data['managers'][$employee['job_manager_title']])==0) {
			$manager_name_for_job_title = "No Allocated Employee";
		} else {
			$my_managers = $employee_data['managers'][$employee['job_manager_title']];
			$manager_name_for_job_title = implode("; ",$my_managers);
		}
		$manager_by_name = isset($available_employees[$employee['job_manager']]) ? $available_employees[$employee['job_manager']]['emp_name'] : $empObj->getUnspecified();
		if(isset($employee_settings[$emp_id])) {
			$my_settings = $employee_settings[$emp_id];
		} else {
			$my_settings = array('settings'=>$settingObject->getDefaultSettings());
		}
		$buttons = array(
			"create"=>array("own","users","non"),
			"manage"=>array("own","users","non"),
		);
		echo "
	<tr>
		<td class='b'>".$tkid."<input type='hidden' id='emp_id' value='$emp_id' /><input type='hidden' id='tkid' value='$tkid' /></td>
		<td class='b'>".$employee['emp_name']."</td>
		<td>".$job_title."<br />[$department]</td>
		<td>".$manager_by_name."</td>
		<td>".$manager."<br />[$manager_name_for_job_title]</td>
		<td><select id='bonus_scale'>";
		foreach($bonus_scales as $b => $s) {
			echo "<option value=".$b." ".($b==$my_settings['bonus_scale']?"selected":"").">".$s."</option>";
		}
		echo "</select></td>";
		foreach($buttons as $section => $fields) {
			foreach ($fields as $field) {
				echo "
				<td class='center'>";
				$options = array('id' => $section."_".$field."_".$tkid,'name'=>$section."_".$field."[".$tkid."]", 'small_size' => true,'extra'=>"triggerYesNoBtnChange");
				$js .= $displayObject->drawFormField("BOOL", $options, $my_settings['settings'][$section][$field]);
				echo "</td>";
			}
		}
		echo "
		<td class='center'><button class='btn-save'>Save</button></td>
	</tr>		
		";
	}
	?>

</table>
</td></tr><tr><td>
	<?php $js.= $displayObject->drawPageFooter("","setup",array('section'=>"ESETTING")); ?>
</td></tr>
</table>
<h2>Explanation of User Access Fields</h2>
<table class=form id=tbl_fields_glossary>
<?php
foreach($fields_glossary as $fld => $explain) {
	echo "
	<tr>
		<th>".$user_access_fields[$fld]."</th>
		<td>".$explain."</td>
	</tr>";
}
?>
</table>
	<p>&nbsp;</p>
</div>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	var btnSaveClass = "ui-button-minor-grey";
	$(".btn-save")
		.button({icons:{primary:"ui-icon-disk"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {
				if($(this).hasClass("ui-button-state-orange")) {
					btnSaveClass = "ui-button-state-orange";
				} else {
					btnSaveClass = "ui-button-minor-grey";
				}
				$(this).removeClass(btnSaveClass).addClass("ui-button-minor-orange");
			},function() {
				$(this).removeClass("ui-button-minor-orange").addClass(btnSaveClass);
			}
		).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = "temp=data";
			var $row = $(this).closest("tr");
			$row.find("input:hidden, select").each(function() {
				dta+="&"+$(this).prop("id")+"="+$(this).val();
			});
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUPEMPLOYEE.SAVE",dta);
			$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
			AssistHelper.finishedProcessing(result[0],result[1]);
	});
	function triggerYesNoBtnChange($me) {
		$me.closest("tr").find("button.btn-save").removeClass("ui-button-minor-grey").addClass("ui-button-state-orange");
	}
});
</script>