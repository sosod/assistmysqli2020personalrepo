<?php
require_once("inc_header.php");

$me = new PM6_SETUP_PREFERENCES();
$questions = $me->getQuestions();

//ASSIST_HELPER::arrPrint($questions);

?>
<h2>Module Preferences</h2>
<?php
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_pref>
<table id=tbl_pref>
<?php
foreach($questions as $key => $q) {
	$options = array(
		'name'=>"Q_".$key,
		'id'=>"Q_".$key,
	);
	switch($q['type']) {
		case "BOOL":
			$options['form'] = "horizontal";
			$options['small_size'] = true;
			break;
		case "LIST":
			$options['options'] = $q['options'];
			$options['req'] = true;
			$options['allow_unspecified'] = false;
			break;
	}

	echo "
	<tr>
		<th>$key</th>
		<td><span class='b'>".$q['name']."</span> ".(strlen($q['description'])>0 ? "<span class='i' ><br />".($q['description'])."</span>" : "")."</td>
		<td class=center>";
			$js.=$displayObject->drawFormField($q['type'],$options,$q['value']);
		echo "</td>
	</tr>";
}
?>
	<tr>
		<th></th>
		<td></td>
		<td class=center><button class=saveform><?php echo $helper->getActivityName("save"); ?></button></td>
	</tr>	
</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"preferences",array('section'=>"PREF")); ?></td>
	</tr>
</table>

<style type="text/css" >
	.saveform {
		font-size: 90%;
	}
	.saveform-active .ui-icon { background-image: url(/library/images/ui-icons_009900_256x240.png); }
	.saveform-active {
		border: 1px solid #009900;
		color: #009900;
		background-color: #ffffff;
		background-image: url();
	}
</style>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	$("#tbl_pref td").css("vertical-align","middle");
	$("img.help_me").click(function() {
		var t = $(this).prop("title");
		$("<div />",{id:"dlg_help",html:"<p><img src='../pics/help_icon.png' style='margin-right: 10px;' />"+t+"</p>"}).dialog({
			modal: true,
			buttons: [{text:"OK",click:function() { $(this).dialog("destroy"); }}]
		});
		AssistHelper.hideDialogTitlebar("id","dlg_help");
	});
	$(".saveform").button({ icons: { primary: "ui-icon-disk" } })
		.addClass("saveform-active")
		.hover(function() { $(this).removeClass("saveform-active"); },function() { $(this).addClass("saveform-active"); })
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_pref]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=SetupPreferences.Update",dta);
			if(result[0]=="ok") {
				document.location.href = "setup_defaults_preferences.php?r[]=ok&r[]="+result[1];
			} else if(result[0]=="okerror") {
				document.location.href = "setup_defaults_preferences.php?r[]=error&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		});
});
</script>